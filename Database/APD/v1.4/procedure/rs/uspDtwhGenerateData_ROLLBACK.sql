-- Begin the transaction for dropping and creating the stored procedure


BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhGenerateData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhGenerateData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhGenerateData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Extracts claim data from APD and populates the data warehouse
*
* PARAMETERS:  
* (I) @Command  Set to "Init" to initialize the warehouse.  THIS WILL DELETE ALL EXISTING DATA IN THE WAREHOUSE!
*               Set to "Rfsh" to force a fact refresh without reinitializing all the dimensions
*               Set to "Test" to perform unit testing.  In this case, the next parameter is required.
*               Set to "Itst"; combines "Init" and "Test" options
* (I) @TestParm Comma delimited list of claimaspects to update.
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspDtwhGenerateData]
    @Command    udt_std_cd  = NULL,
    @TestParm   udt_std_desc_mid = NULL
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Command))) = 0 SET @Command = NULL


    -- Declare internal variables  
    
    DECLARE @tmpAssignment TABLE
    (
        AssignmentID                    int,
        SelectionDate                   datetime,
        AssignmentDate                  datetime,
        CancellationDate                datetime,
        AppraiserID                     bigint,
        ShopLocationID                  bigint,
        ProgramTypeCD                   varchar(4),
        CertifiedFirstFlag              bit
    )

    DECLARE @tmpDocument TABLE
    (
        DocumentID                      bigint,
        AssignmentID                    int,
        DocumentSourceName              varchar(50),
        VanSourceFlag                   bit,
        SequenceNumber                  tinyint,
        CreatedDate                     datetime,
        EstimateTypeCD                  varchar(4),
        DuplicateFlag                   bit,
        OriginalLaborRateBody           money,
        OriginalLaborRateFrame          money,
        OriginalLaborRateMech           money,
        OriginalLaborRateRefinish       money,
        OriginalLaborAmountRepair       money,
        OriginalLaborAmountReplace      money,
        OriginalLaborAmountTotal        money,
        OriginalPartsAmountAFMK         money,
        OriginalPartsAmountLKQ          money,
        OriginalPartsAmountOEM          money,
        OriginalPartsAmountReman        money,
        OriginalPartsAmountTotal        money,
        OriginalPartsDiscount           money,
        OriginalRepairTotal             money,
        OriginalBetterment              money,
        OriginalRepairTotalwoBetterment money,
        OriginalDeductible              money,
        OriginalOtherAdjustmentTotal    money,
        OriginalNetTotal                money,
        AgreedRepairTotal               money,
        AgreedBetterment                money,
        AgreedRepairTotalwoBetterment   money,
        AgreedDeductible                money,
        AgreedOtherAdjustmentTotal      money,
        AgreedNetTotal                  money,
        AgreedPriceMetCD                varchar(4),
        ApprovedFlag                    bit,
        FinalBillEstimateFlag           bit
    )    

    DECLARE @tmpEstimateTotals TABLE
    (
        EstimateTotalID                 int     IDENTITY(1,1),
        SequenceNumber                  int,
        EstimateTypeCD                  varchar(4),
        LaborRateBody                   money,
        LaborRateFrame                  money,
        LaborRateMech                   money,
        LaborRateRefinish               money,
        LaborAmountRepair               money,
        LaborAmountReplace              money,
        LaborAmountTotal                money,
        PartsAmountAFMK                 money,
        PartsAmountLKQ                  money,
        PartsAmountOEM                  money,
        PartsAmountReman                money,
        PartsAmountTotal                money,
        PartsDiscount                   money,
        RepairAmount                    money,
        Betterment                      money,
        RepairAmountwoBetterment        money,
        Deductible                      money,
        OtherAdj                        money,
        NetAmount                       money,
        AgreedFlag                      bit,
        CreateDate                      datetime,
        ApprovedFlag                    bit,
        FinalBillEstimateFlag           bit
    )
    

    -- Claim Fact Table Work Variables
    
    DECLARE @AssignmentTypeClosingID            udt_std_id
    DECLARE @AssignmentTypeID                   udt_std_id
    DECLARE @ClaimLocationID                    udt_std_id
    DECLARE @CoverageTypeID                     udt_std_id
    DECLARE @CustomerID                         udt_std_id_big
    DECLARE @DispositionTypeID                  udt_std_id
    DECLARE @LynxHandlerOwnerID                 udt_std_id_big
    DECLARE @LynxHandlerAnalystID               udt_std_id_big
    DECLARE @LynxHandlerSupportID               udt_std_id_big
    DECLARE @RepairLocationIDClaim              udt_std_id_big
    DECLARE @ServiceChannelID                   udt_std_id
    DECLARE @TimeIDAssignDownload               udt_std_id_big
    DECLARE @TimeIDAssignSent                   udt_std_id_big
    DECLARE @TimeIDCancelled                    udt_std_id_big
    DECLARE @TimeIDClosed                       udt_std_id_big
    DECLARE @TimeIDEstimate                     udt_std_id_big
    DECLARE @TimeIDInspection                   udt_std_id_big      -- (SWR 201448)
    DECLARE @TimeIDNew                          udt_std_id_big
    DECLARE @TimeIDReclosed                     udt_std_id_big
    DECLARE @TimeIDRepairComplete               udt_std_id_big
    DECLARE @TimeIDRepairStarted                udt_std_id_big
    DECLARE @TimeIDReopened                     udt_std_id_big
    DECLARE @TimeIDVoided                       udt_std_id_big
    DECLARE @VehicleLicensePlateStateID         udt_std_id
    DECLARE @AuditedEstimateAgreedFlag          udt_std_flag
    DECLARE @AuditedEstimateBettermentAmt       udt_std_money
    DECLARE @AuditedEstimateDeductibleAmt       udt_std_money    
    DECLARE @AuditedEstimateGrossAmt            udt_std_money
    DECLARE @AuditedEstimateNetAmt              udt_std_money
    DECLARE @AuditedEstimateOtherAdjustmentAmt  udt_std_money    
    DECLARE @AuditedEstimatewoBettAmt           udt_std_money
    DECLARE @CFProgramFlag                      udt_std_flag
    DECLARE @ClaimStatusCD                      udt_std_cd
    DECLARE @ClientClaimNumber                  udt_cov_claim_number
    DECLARE @CycleTimeAssignToEstBusDay         udt_std_int_small
    DECLARE @CycleTimeAssignToEstCalDay         udt_std_int_small
    DECLARE @CycleTimeAssignToEstHrs            udt_std_int
    DECLARE @CycleTimeEstToCloseBusDay          udt_std_int_small
    DECLARE @CycleTimeEstToCloseCalDay          udt_std_int_small
    DECLARE @CycleTimeEstToCloseHrs             udt_std_int
    DECLARE @CycleTimeInspectionToEstBusDay     udt_std_int_small       -- (SWR 201448)
    DECLARE @CycleTimeInspectionToEstCalDay     udt_std_int_small       -- (SWR 201448)
    DECLARE @CycleTimeInspectionToEstHrs        udt_std_int             -- (SWR 201448)
    DECLARE @CycleTimeNewToCloseBusDay          udt_std_int_small
    DECLARE @CycleTimeNewToCloseCalDay          udt_std_int_small
    DECLARE @CycleTimeNewToCloseHrs             udt_std_int
    DECLARE @CycleTimeNewToEstBusDay            udt_std_int_small
    DECLARE @CycleTimeNewToEstCalDay            udt_std_int_small
    DECLARE @CycleTimeNewToEstHrs               udt_std_int
    DECLARE @CycleTimeNewToInspectionBusDay     udt_std_int_small       -- (SWR 201448)
    DECLARE @CycleTimeNewToInspectionCalDay     udt_std_int_small       -- (SWR 201448)
    DECLARE @CycleTimeNewToInspectionHrs        udt_std_int             -- (SWR 201448)
    DECLARE @CycleTimeNewToRecloseBusDay        udt_std_int_small
    DECLARE @CycleTimeNewToRecloseCalDay        udt_std_int_small
    DECLARE @CycleTimeNewToRecloseHrs           udt_std_int
    DECLARE @CycleTimeRepairStartToEndBusDay    udt_std_int_small
    DECLARE @CycleTimeRepairStartToEndCalDay    udt_std_int_small
    DECLARE @CycleTimeRepairStartToEndHrs       udt_std_int
    DECLARE @DemoFlag                           udt_std_flag
	DECLARE @EarlyBillAuditedGrossAmt           udt_std_money
-- -----------------------------------------------------------
--  MAH 07-jun-2012 Obsolete:
-- -----------------------------------------------------------
--	DECLARE @EarlyBillAuditedAddtlLineItemAmt   udt_std_money
--	DECLARE @EarlyBillAuditedLineItemCorrectionAmt udt_std_money
--	DECLARE @EarlyBillAuditedMissingLineItemAmt udt_std_money
-- -----------------------------------------------------------
    DECLARE @EnabledFlag                        udt_std_flag
    DECLARE @ExposureCD                         udt_std_cd
    DECLARE @FeeRevenueAmt                      udt_std_money
    DECLARE @FinalAuditedSuppAgreedFlag         udt_std_flag
    DECLARE @FinalAuditedSuppBettermentAmt      udt_std_money
    DECLARE @FinalAuditedSuppDeductibleAmt      udt_std_money    
    DECLARE @FinalAuditedSuppGrossAmt           udt_std_money
    DECLARE @FinalAuditedSuppNetAmt             udt_std_money
    DECLARE @FinalAuditedSuppOtherAdjustmentAmt udt_std_money    
    DECLARE @FinalAuditedSuppwoBettAmt          udt_std_money
    DECLARE @FinalEstimateBettermentAmt         udt_std_money
    DECLARE @FinalEstimateDeductibleAmt         udt_std_money
    DECLARE @FinalEstimateGrossAmt              udt_std_money
    DECLARE @FinalEstimateNetAmt                udt_std_money
    DECLARE @FinalEstimateOtherAdjustmentAmt    udt_std_money
    DECLARE @FinalEstimatewoBettAmt             udt_std_money
    DECLARE @FinalSupplementBettermentAmt       udt_std_money
    DECLARE @FinalSupplementDeductibleAmt       udt_std_money
    DECLARE @FinalSupplementGrossAmt            udt_std_money
    DECLARE @FinalSupplementNetAmt              udt_std_money
    DECLARE @FinalSupplementOtherAdjustmentAmt  udt_std_money
    DECLARE @FinalSupplementwoBettAmt           udt_std_money
    DECLARE @IndemnityAmount                    udt_std_money
    DECLARE @LaborRateBodyAmt                   udt_std_money
    DECLARE @LaborRateFrameAmt                  udt_std_money
    DECLARE @LaborRateMechAmt                   udt_std_money
    DECLARE @LaborRateRefinishAmt               udt_std_money
    DECLARE @LaborRepairAmt                     udt_std_money
    DECLARE @LaborReplaceAmt                    udt_std_money
    DECLARE @LaborTotalAmt                      udt_std_money
    DECLARE @LossDate                           udt_std_datetime
    DECLARE @MaxEstSuppSequenceNumber           udt_std_int_tiny
    DECLARE @OriginalAuditedEstimateBettermentAmt      udt_std_money
    DECLARE @OriginalAuditedEstimateDeductibleAmt      udt_std_money
    DECLARE @OriginalAuditedEstimateGrossAmt           udt_std_money
    DECLARE @OriginalAuditedEstimateNetAmt             udt_std_money
    DECLARE @OriginalAuditedEstimateOtherAdjustmentAmt udt_std_money
    DECLARE @OriginalAuditedEstimatewoBettAmt          udt_std_money
    DECLARE @OriginalEstimateBettermentAmt      udt_std_money
    DECLARE @OriginalEstimateDeductibleAmt      udt_std_money
    DECLARE @OriginalEstimateGrossAmt           udt_std_money
    DECLARE @OriginalEstimateNetAmt             udt_std_money
    DECLARE @OriginalEstimateOtherAdjustmentAmt udt_std_money
    DECLARE @OriginalEstimatewoBettAmt          udt_std_money
    DECLARE @PartsAFMKReplacedAmt               udt_std_money
    DECLARE @PartsLKQReplacedAmt                udt_std_money
    DECLARE @PartsOEMDiscountAmt                udt_std_money
    DECLARE @PartsOEMReplacedAmt                udt_std_money
    DECLARE @PartsRemanReplacedAmt              udt_std_money
    DECLARE @PartsTotalReplacedAmt              udt_std_money
    DECLARE @PhotosAttachedFlag                 udt_std_flag            -- (SWR 201448) 
    DECLARE @PolicyNumber                       udt_cov_policy_number
    DECLARE @PolicyDeductibleAmt                udt_cov_deductible
    DECLARE @PolicyLimitAmt                     udt_std_money
    DECLARE @PolicyRentalDeductibleAmt          udt_cov_deductible
    DECLARE @PolicyRentalLimitAmt               udt_std_money
    DECLARE @PolicyRentalMaxDays                udt_std_int_small
    DECLARE @ProgramCD                          udt_std_cd
    DECLARE @ReinspectionCount                  udt_std_int_tiny
    DECLARE @ReinspectionDeviationAmt           udt_std_money
    DECLARE @ReinspectionDeviationCount         udt_std_int_tiny
    DECLARE @RentalDays                         udt_std_int_small
    DECLARE @RentalAmount                       udt_std_money
    DECLARE @RentalCostPerDay                   udt_std_money
    DECLARE @ServiceLossOfUseFlag               udt_std_flag
    DECLARE @ServiceSubrogationFlag             udt_std_flag
    DECLARE @ServiceTotalLossFlag               udt_std_flag
    DECLARE @ServiceTotalTheftFlag              udt_std_flag
    DECLARE @SupplementTotalAmt                 udt_std_money
    DECLARE @VehicleDriveableFlag               udt_std_flag
    DECLARE @VehicleLicensePlateNumber          udt_auto_plate_number
    DECLARE @VehicleMake                        udt_auto_make
    DECLARE @VehicleModel                       udt_auto_model
    DECLARE @VehicleYear                        udt_dt_year
    DECLARE @VehicleVIN                         udt_auto_vin
    
    -- Estimate Fact Table Work Variables
    
    DECLARE @DocumentSourceID               udt_std_id_big
    DECLARE @RepairLocationIDEstimate       udt_std_id_big
    DECLARE @TimeIDReceived                 udt_std_id_big
    DECLARE @AdjAppearanceAllowanceAmount   udt_std_money
    DECLARE @AdjBettermentAmount            udt_std_money
    DECLARE @AdjDeductibleAmount            udt_std_money
    DECLARE @AdjOtherAmount                 udt_std_money
    DECLARE @AdjRelatedPriorDamAmount       udt_std_money
    DECLARE @AdjUnrelatedPriorDamAmount     udt_std_money
    DECLARE @DaysSinceAssignment            udt_std_int
    DECLARE @EstSuppSequenceNumber          udt_std_int_tiny
    DECLARE @LaborBodyHours                 udt_std_hours
    DECLARE @LaborBodyRate                  udt_std_money
    DECLARE @LaborBodyAmount                udt_std_money
    DECLARE @LaborFrameHours                udt_std_hours
    DECLARE @LaborFrameRate                 udt_std_money
    DECLARE @LaborFrameAmount               udt_std_money
    DECLARE @LaborMechHours                 udt_std_hours
    DECLARE @LaborMechRate                  udt_std_money
    DECLARE @LaborMechAmount                udt_std_money
    DECLARE @LaborRefinishHours             udt_std_hours
    DECLARE @LaborRefinishRate              udt_std_money
    DECLARE @LaborRefinishAmount            udt_std_money
    DECLARE @LaborTotalHours                udt_std_hours
    DECLARE @LaborTotalAmount               udt_std_money
    DECLARE @MaterialsHours                 udt_std_hours
    DECLARE @MaterialsRate                  udt_std_money
    DECLARE @MaterialsAmount                udt_std_money
    DECLARE @NetAmount                      udt_std_money
    DECLARE @OtherAmount                    udt_std_money
    DECLARE @PartDiscountAmount             udt_std_money
    DECLARE @PartMarkupAmount               udt_std_money
    DECLARE @PartsNewAmount                 udt_std_money
    DECLARE @PartsLKQAmount                 udt_std_money
    DECLARE @PartsAFMKAmount                udt_std_money
    DECLARE @PartsRemanAmount               udt_std_money
    DECLARE @PartsTotalAmount               udt_std_money
    DECLARE @StorageAmount                  udt_std_money
    DECLARE @SubletAmount                   udt_std_money
    DECLARE @TaxLaborAmount                 udt_std_money
    DECLARE @TaxPartsAmount                 udt_std_money
    DECLARE @TaxMaterialsAmount             udt_std_money
    DECLARE @TaxTotalAmount                 udt_std_money
    DECLARE @TotalAmount                    udt_std_money
    DECLARE @TowingAmount                   udt_std_money
    DECLARE @BodyRepairHrs                  udt_std_hours
    DECLARE @BodyReplaceHrs                 udt_std_hours
    DECLARE @FrameRepairHrs                 udt_std_hours
    DECLARE @FrameReplaceHrs                udt_std_hours
    DECLARE @MechanicalRepairHrs            udt_std_hours
    DECLARE @MechanicalReplaceHrs           udt_std_hours
    DECLARE @RefinishRepairHrs              udt_std_hours
    DECLARE @RefinishReplaceHrs             udt_std_hours
    DECLARE @EstimateBalancedFlag           udt_std_flag


        
    -- Cursor Work variables
    
    DECLARE @AgreedFlag                     udt_std_flag
    DECLARE @AgreedRepairTotal              udt_std_money
    DECLARE @AgreedBetterment               udt_std_money
    DECLARE @AgreedRepairTotalwoBetterment  udt_std_money
    DECLARE @AgreedDeductible               udt_std_money
    DECLARE @AgreedOtherAdjTotal            udt_std_money
    DECLARE @AgreedNetTotal                 udt_std_money
--     DECLARE @AppraiserID                    udt_std_id_big
    DECLARE @AnalystUserID                  udt_std_id_big
    DECLARE @AssignmentID                   udt_std_id_big
    DECLARE @AssignmentIDWork               udt_std_id_big
    DECLARE @AssignmentDate                 udt_std_datetime
    DECLARE @CarrierRepUserID               udt_std_id_big
    DECLARE @ClaimantName                   udt_std_name			-- (SWR 203304)
    DECLARE @ClaimAspectID                  udt_std_id_big
    DECLARE @ClaimAspectTypeID              udt_std_id
    DECLARE @ClaimAspectNumber              udt_std_int
    DECLARE @ClosingAssignmentTypeID        udt_std_id
    DECLARE @ClosingServiceChannelCD        udt_std_cd
    DECLARE @ClientCoverageTypeID           udt_std_id
    DECLARE @CreatedDate                    udt_std_datetime
    DECLARE @DateAssignSentWork             udt_std_datetime
    DECLARE @DateEstWork                    udt_std_datetime
    DECLARE @DateReclosedWork               udt_std_datetime
    DECLARE @DateReopenedWork               udt_std_datetime
    DECLARE @DispositionTypeCD              udt_std_cd
    DECLARE @DocumentID                     udt_std_id_big
    DECLARE @DocumentCreatedDate            udt_std_datetime
    DECLARE @DocumentSourceName             udt_std_name
    DECLARE @DocumentSourceNameClaim        udt_std_name
    DECLARE @DocumentSourceNameStandard     udt_std_name
    DECLARE @EstimateTypeCD                 udt_std_cd
    DECLARE @InitialAssignmentTypeID        udt_std_id
    DECLARE @InspectionDate                 udt_std_datetime        -- (SWR 201448)
    DECLARE @InsuredName                    udt_std_name			-- (SWR 203304)
    DECLARE @VehicleCreatedDate             udt_std_datetime
    DECLARE @LicensePlateStateCode          udt_std_cd
    DECLARE @LossState                      udt_addr_state
    DECLARE @LynxID                         udt_std_id
    DECLARE @OriginalCompleteDate           udt_std_datetime
    DECLARE @OriginalLaborRateBody          udt_std_money
    DECLARE @OriginalLaborRateFrame         udt_std_money
    DECLARE @OriginalLaborRateMech          udt_std_money
    DECLARE @OriginalLaborRateRefinish      udt_std_money
    DECLARE @OriginalLaborAmountRepair      udt_std_money
    DECLARE @OriginalLaborAmountReplace     udt_std_money
    DECLARE @OriginalLaborAmountTotal       udt_std_money
    DECLARE @OriginalPartsAmountAFMK        udt_std_money
    DECLARE @OriginalPartsAmountLKQ         udt_std_money
    DECLARE @OriginalPartsAmountOEM         udt_std_money
    DECLARE @OriginalPartsAmountReman       udt_std_money
    DECLARE @OriginalPartsAmountTotal       udt_std_money
    DECLARE @OriginalPartsDiscount          udt_std_money
    DECLARE @OriginalRepairTotal            udt_std_money
    DECLARE @OriginalBetterment             udt_std_money
    DECLARE @OriginalRepairTotalwoBetterment udt_std_money
    DECLARE @OriginalDeductible             udt_std_money
    DECLARE @OriginalOtherAdjTotal          udt_std_money
    DECLARE @OriginalNetTotal               udt_std_money
    DECLARE @OwnerUserID                    udt_std_id_big
    DECLARE @PartsOtherNonTaxableAmount     udt_std_money
    DECLARE @PartsOtherTaxableAmount        udt_std_money
    DECLARE @PartsTaxableAmount             udt_std_money
    DECLARE @PricingLaborRateBody           udt_std_money
    DECLARE @PricingLaborRateFrame          udt_std_money
    DECLARE @PricingLaborRateMech           udt_std_money
    DECLARE @PricingLaborRateRefinish       udt_std_money
    DECLARE @RepairEndDate                  udt_std_datetime
    DECLARE @RepairStartDate                udt_std_datetime
    DECLARE @SequenceNumber                 udt_std_id_tiny
    DECLARE @ServiceChannelCD               udt_std_cd
    DECLARE @ShopLocationID                 udt_std_id_big
    DECLARE @StandardDocumentSourceID       udt_std_id
    DECLARE @SupportUserID                  udt_std_id_big
    DECLARE @TaxRateEstimate                udt_std_pct
    DECLARE @VanSourceFlag                  udt_std_flag
    DECLARE @VanSourceFlagClaim             udt_std_flag
    DECLARE @VanSourceFlagStandard          udt_std_flag
    DECLARE @ClaimAspectServiceChannelID    udt_std_id_big
    DECLARE @TotalConcessionLaborRate        udt_std_money
    DECLARE @TotalConcessionLaborRateComment varchar(3000)
    DECLARE @TotalConcessionUnrelatedDamage  udt_std_money
    DECLARE @TotalConcessionUnrelatedDamageComment varchar(3000)
    DECLARE @TotalConcessionPartsUsage       udt_std_money
    DECLARE @TotalConcessionPartsUsageComment varchar(3000)
    DECLARE @TotalConcessionTotalLoss        udt_std_money
    DECLARE @TotalConcessionTotalLossComment varchar(3000)
    
    
    -- Miscellaneous
    
    --DECLARE @ClaimAspectTypeIDAssignment        udt_std_id
    DECLARE @ClaimAspectTypeIDClaim             udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle           udt_std_id
    DECLARE @DateLastRun                        udt_std_datetime
    DECLARE @DateValue                          udt_std_int_big
    
    DECLARE @DocumentTypeIDPhotograph           udt_std_id
    
    DECLARE @EstimateSummaryTypeIDAFMKParts     udt_std_id
    DECLARE @EstimateSummaryTypeIDLKQParts      udt_std_id
    DECLARE @EstimateSummaryTypeIDOEMParts      udt_std_id
    DECLARE @EstimateSummaryTypeIDRemanParts    udt_std_id
    DECLARE @EstimateSummaryTypeIDNontaxParts   udt_std_id
    DECLARE @EstimateSummaryTypeIDTaxableParts  udt_std_id
    DECLARE @EstimateSummaryTypeIDPartsDiscount udt_std_id
    DECLARE @EstimateSummaryTypeIDPartsMarkup   udt_std_id
    
    DECLARE @EstimateSummaryTypeIDBodyLabor     udt_std_id
    DECLARE @EstimateSummaryTypeIDFrameLabor    udt_std_id
    DECLARE @EstimateSummaryTypeIDMechLabor     udt_std_id
    DECLARE @EstimateSummaryTypeIDPaintLabor    udt_std_id
    DECLARE @EstimateSummaryTypeIDRefinishLabor udt_std_id
    DECLARE @EstimateSummaryTypeIDRepairLabor   udt_std_id
    DECLARE @EstimateSummaryTypeIDReplaceLabor  udt_std_id
    DECLARE @EstimateSummaryTypeIDTotalLabor    udt_std_id
    
    DECLARE @EstimateSummaryTypeIDAppAllowance  udt_std_id
    DECLARE @EstimateSummaryTypeIDBetterment    udt_std_id
    DECLARE @EstimateSummaryTypeIDDeductible    udt_std_id
    DECLARE @EstimateSummaryTypeIDOtherAdj      udt_std_id
    DECLARE @EstimateSummaryTypeIDNetTotal      udt_std_id
    DECLARE @EstimateSummaryTypeIDRepairTotal   udt_std_id
    DECLARE @EstimateSummaryTypeIDUPD           udt_std_id
    
    -- STARS 216709 BEGIN
    -- Zip code capture for DA assignments 
    DECLARE @EstimateShopLocationID             udt_std_id
    DECLARE @EstimateShopName                   udt_std_name
    DECLARE @EstimateShopState                  udt_addr_state
    DECLARE @EstimateShopStateID                udt_std_id
    DECLARE @EstimateShopZip                    udt_addr_zip_code
    DECLARE @EstimateShopAreaCode               udt_ph_area_code
    DECLARE @EstimateShopExchangeNumber         udt_ph_exchange_number
    DECLARE @EstimateShopUnitNumber             udt_ph_unit_number
    DECLARE @DAShopState                        udt_addr_state
    DECLARE @DAShopCity                         udt_addr_city
    DECLARE @DAShopCounty                       udt_addr_county
    DECLARE @DAShopZip                          udt_addr_zip_code
    -- STARS 216709 END

    DECLARE @ExposuresDifference                udt_std_int
    DECLARE @ExposuresProcessed                 udt_std_int
    DECLARE @HoursDifference                    udt_std_int_big
    DECLARE @Index                              udt_std_int_big
    DECLARE @ClaimSourceIDAdjuster              udt_std_int
    DECLARE @ClaimSourceIDFNOL                  udt_std_int
    DECLARE @ClaimSourceID                      udt_std_int
    DECLARE @ApplicationName                    varchar(50)
    
    DECLARE @ApprovedFlag                       bit
    DECLARE @FinalBillEstimateFlag              bit
    
    DECLARE @DefaultDate                        udt_std_datetime
    
    SELECT @DefaultDate = '4/10/2002 0:00:00'   -- This is an arbitrary date selected from before APD was active.  This is the base date 
                                            -- for warehouse initialization
                                    
    Declare @debug              AS bit
    DECLARE @error              AS int
    DECLARE @rowcount           AS int
    
    DECLARE @now                AS datetime 

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SELECT @ProcName = 'uspDtwhGenerateData'

    SELECT @debug = 0
    
    -- Set Database options
    
    SET NOCOUNT ON

    -- Get current timestamp

    SELECT @now = CURRENT_TIMESTAMP

    
    -- Validate parameters
    
    IF  (@Command IS NOT NULL) AND
        (@Command NOT IN ('Init', 'Rfsh', 'Test', 'Itst'))
    BEGIN
        -- Invalid parameter
        
        RAISERROR('%s: Invalid @Command parameter.  See sp documentation for valid options.', 16, 1, @ProcName)
        RETURN
    END


    IF (@Command IN ('Test', 'Itst')) AND (@TestParm IS NULL)
    BEGIN
        -- Invalid parameter
        
        RAISERROR('%s: Invalid @Testparm parameter.  @Testparm required for "Test" and "Itst" commands.', 16, 1, @ProcName)
        RETURN
    END
    
        
    -- Validating APD data state
    -- This does not exist anymore with MSC.
--    SELECT @ClaimAspectTypeIDAssignment = ClaimAspectTypeID
--      FROM dbo.utb_claim_aspect_type
--      WHERE Name = 'Assignment'
--
--    IF @@ERROR <> 0
--    BEGIN
--        -- SQL Server Error
--    
--        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
--        RETURN
--    END
--
--    IF @ClaimAspectTypeIDAssignment IS NULL
--    BEGIN
--        -- Claim Aspect Not Found
--    
--        RAISERROR('%s: Invalid APD Data State.  "Assignment" not found in utb_claim_aspect_type.', 16, 1, @ProcName)
--        RETURN
--    END
    

    SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type WITH (NOLOCK)
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
        -- Claim Aspect Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Claim" not found in utb_claim_aspect_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type WITH (NOLOCK)
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        -- Claim Aspect Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Vehicle" not found in utb_claim_aspect_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @DocumentTypeIDPhotograph = DocumentTypeID
      FROM dbo.utb_document_type WITH (NOLOCK)
      WHERE Name = 'Photograph'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @DocumentTypeIDPhotograph IS NULL
    BEGIN
        -- Document Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Photograph" not found in utb_document_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDAFMKParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'AFMKParts'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDAFMKParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "AFMKParts" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDLKQParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'LKQParts'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDLKQParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "LKQParts" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDOEMParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'OEMParts'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDOEMParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "OEMParts" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDRemanParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'RemanParts'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRemanParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "RemanParts" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDTaxableParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'PC'
        AND Name = 'PartsTaxable'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDTaxableParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsTaxable" Total type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDNontaxParts = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'PC'
        AND Name = 'PartsNonTaxable'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNontaxParts IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsNonTaxable" Total type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDPartsDiscount = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'PC'
        AND Name = 'PartsDiscount'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDPartsDiscount IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsDiscount" Part type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDPartsMarkup = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'PC'
        AND Name = 'PartsMarkup'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDPartsMarkup IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsMarkup" Part type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDBodyLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'LC'
        AND Name = 'Body'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDBodyLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Body" Labor type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDFrameLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'LC'
        AND Name = 'Frame'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDFrameLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Frame" Labor type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDMechLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'LC'
        AND Name = 'Mechanical'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDMechLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Mechanical" Labor type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDPaintLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'LC'
        AND Name = 'Paint'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDPaintLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Paint" Labor type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDRefinishLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'LC'
        AND Name = 'Refinish'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRefinishLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Refinish" Labor type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDRepairLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'RepairLabor'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "RepairLabor" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDReplaceLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'OT'
        AND Name = 'ReplaceLabor'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDReplaceLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "ReplaceLabor" Other type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDTotalLabor = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'TT'
        AND Name = 'LaborTotal'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDTotalLabor IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "LaborTotal" Total type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'TT'
        AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "RepairTotal" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'TT'
        AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "NetTotal" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDBetterment = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'AJ'
        AND Name = 'Betterment'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDBetterment IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Betterment" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDDeductible = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'AJ'
        AND Name = 'Deductible'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDDeductible IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Deductible" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDAppAllowance = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'AJ'
        AND Name = 'AppearanceAllowance'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDAppAllowance IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "AppearanceAllowance" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDUPD = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'AJ'
        AND Name = 'UnrelatedPriorDamage'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDUPD IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "UnrelatedPriorDamage" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EstimateSummaryTypeIDOtherAdj = EstimateSummaryTypeID
      FROM dbo.utb_estimate_summary_type WITH (NOLOCK)
      WHERE CategoryCD = 'AJ'
        AND Name = 'Other'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDOtherAdj IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Other Adjustment" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END
    

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'PC' AND Name = 'PartsOtherTaxable')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsOtherTaxable" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'PC' AND Name = 'PartsOtherNonTaxable')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsOtherNonTaxable" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'TT' AND Name = 'PartsTotal')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsTotal" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'MC' AND Name = 'Body')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Body" Materials not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'MC' AND Name = 'Paint')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Paint" Materials not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'TT' AND Name = 'PartsAndMaterialTotal')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "PartsAndMaterialTotal" not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'AC' AND Name = 'Storage')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Storage" additional charge not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'AC' AND Name = 'Sublet')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Sublet" additional charge not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'AC' AND Name = 'Towing')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Towing" additional charge not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'TX' AND Name = 'County')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "County" tax type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'TX' AND Name = 'Municipal')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Municipal" tax type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WITH (NOLOCK) WHERE CategoryCD = 'TX' AND Name = 'Sales')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Sales" tax type not found in utb_estimate_summary_type.', 16, 1, @ProcName)
        RETURN
    END


    IF NOT EXISTS(SELECT EventID FROM dbo.utb_event WITH (NOLOCK) WHERE Name = 'Vehicle Cancelled')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Vehicle Cancelled" not found in utb_event.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EventID FROM dbo.utb_event WITH (NOLOCK) WHERE Name = 'Vehicle Voided')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Vehicle Voided" not found in utb_event.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EventID FROM dbo.utb_event WITH (NOLOCK) WHERE Name = 'Vehicle Closed')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Vehicle Closed" not found in utb_event.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EventID FROM dbo.utb_event WITH (NOLOCK) WHERE Name = 'Vehicle Reopened')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Vehicle Reopened" not found in utb_event.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ServiceID FROM dbo.utb_service WITH (NOLOCK) WHERE Name = 'First Party Rental')
    BEGIN
       -- Service Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "First Party Rental" not found in utb_service.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ServiceID FROM dbo.utb_service WITH (NOLOCK) WHERE Name = 'Third Party Loss Of Use')
    BEGIN
       -- Service Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Third Party Loss Of Use" not found in utb_service.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ServiceID FROM dbo.utb_service WITH (NOLOCK) WHERE Name = 'Rental Management')
    BEGIN
       -- Service Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Rental Management" not found in utb_service.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ServiceID FROM dbo.utb_service WITH (NOLOCK) WHERE Name = 'Total Loss Evaluation')
    BEGIN
       -- Service Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Total Loss Evaluation" not found in utb_service.', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT StatusID FROM dbo.utb_status WITH (NOLOCK) WHERE ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Sent' AND StatusTypeCD = 'ELC')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Sent" For Electronic Assignment Claim Aspect Type not found in utb_status.', 16, 1, @ProcName)
        RETURN
    END
        
    IF NOT EXISTS(SELECT StatusID FROM dbo.utb_status WITH (NOLOCK) WHERE ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Received By Appraiser' AND StatusTypeCD = 'ELC')
    BEGIN
       -- Event Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Received By Appraiser" For Electronic Assignment Claim Aspect Type not found in utb_status.', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimSourceIDAdjuster = ClaimSourceID
      FROM dbo.utb_dtwh_dim_claim_source WITH (NOLOCK)
      WHERE ClaimSourceName = 'Adjuster'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimSourceIDAdjuster IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Adjuster" claim source name not found in utb_dtwh_dim_claim_source.', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimSourceIDFNOL = ClaimSourceID
      FROM dbo.utb_dtwh_dim_claim_source WITH (NOLOCK)
      WHERE ClaimSourceName = 'FNOL'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimSourceIDFNOL IS NULL
    BEGIN
        -- Estimate Summary Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "FNOL" claim source name not found in utb_dtwh_dim_claim_source.', 16, 1, @ProcName)
        RETURN
    END

    -- First we need to see if we need to reinitialize the warehouse from scratch.  Because this is a completely
    -- rerunnable operation as 'INIT', we're not going to worry about binding this in a transaction.  This will 
    -- result in better performance
    
    IF @Command IN ('Init', 'Itst')
    BEGIN
        PRINT '.'
        PRINT '.'
        PRINT 'Warehouse initialization commencing...'
        
        DELETE FROM dbo.utb_dtwh_fact_estimate
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_fact_estimate', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_fact_claim
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_fact_claim', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_time
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_time', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_assignment_type
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_assignment_type', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_repair_location
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_repair_location', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_lynx_handler
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_lynx_handler', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_disposition_type
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_disposition_type', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_customer
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_customer', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_coverage_type
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_coverage_type', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_document_source
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_document_source', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM dbo.utb_dtwh_dim_service_channel
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_service_channel', 16, 1, @ProcName)
            RETURN
        END

        DELETE FROM dbo.utb_dtwh_dim_state
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_state', 16, 1, @ProcName)
            RETURN
        END
        

        INSERT INTO dbo.utb_dtwh_dim_state (StateCode, StateName, RegionCD, RegionName, CountryCode, CountryName)
            SELECT  StateCode,
                    StateValue,
                    NULL, 
                    NULL,
                    'USA',
                    'United States'
              FROM  dbo.utb_state_code WITH (NOLOCK)
                      
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_state', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO dbo.utb_dtwh_dim_service_channel (ServiceChannelCD, ServiceChannelDescription)
            SELECT  Code,
                    Name
              FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                      
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_service_channel', 16, 1, @ProcName)
            RETURN
        END
        
        INSERT INTO dbo.utb_dtwh_dim_assignment_type (AssignmentTypeID, AssignmentTypeDescription, ServiceChannelID)
            SELECT  at.AssignmentTypeID,
                    at.Name,
                    dsc.ServiceChannelID
              FROM  dbo.utb_assignment_type at WITH (NOLOCK) 
              LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc WITH (NOLOCK) ON (at.ServiceChannelDefaultCD = dsc.ServiceChannelCD)
              WHERE at.EnabledFlag = 1
                      
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_assignment_type', 16, 1, @ProcName)
            RETURN
        END
        
        DBCC CHECKIDENT('utb_dtwh_dim_coverage_type',RESEED,0)

        INSERT INTO dbo.utb_dtwh_dim_coverage_type (CoverageTypeCD, CoverageTypeDescription, PartyCD, PartyDescription)
            SELECT  CoverageProfileCD,
                    Name,
                    CASE CoverageProfileCD
                      WHEN 'LIAB' THEN '3'
                      ELSE '1'
                    END,
                    CASE CoverageProfileCD
                      WHEN 'LIAB' THEN 'Third Party'
                      ELSE 'First Party'
                    END                    
              FROM  dbo.utb_client_coverage_type WITH (NOLOCK)
              GROUP BY CoverageProfileCD, Name                      
              
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_coverage_type', 16, 1, @ProcName)
            RETURN
        END
              
                  
        INSERT INTO dbo.utb_dtwh_dim_disposition_type (DispositionTypeCD, DispositionTypeDescription)
            SELECT  Code,
                    Name
              FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'DispositionTypeCD')
                      
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_disposition_type', 16, 1, @ProcName)
            RETURN
        END

        SET IDENTITY_INSERT dbo.utb_dtwh_dim_document_source ON
        
        INSERT INTO dbo.utb_dtwh_dim_document_source (DocumentSourceID, DocumentSourceName, ElectronicSourceFlag)
            VALUES  (0, 'Fax', 0)
                      
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_document_source', 16, 1, @ProcName)
            RETURN
        END

        SET IDENTITY_INSERT dbo.utb_dtwh_dim_document_source OFF

        PRINT '.'
        PRINT 'Warehouse Initialization complete.'
    END
    

    IF (@Command IN ('Test', 'Itst'))
    BEGIN 
        DECLARE @tmpTestAspects TABLE
        (
            ClaimAspectID       bigint
        )
        
        
        -- Populate @tmpTestAspects with the list passed in.  Only use ones that are numeric
        
        INSERT INTO @tmpTestAspects (ClaimAspectID)
          SELECT  value
            FROM  dbo.ufnUtilityParseString(@TestParm, ',', 1)   -- Trim spaces
            WHERE IsNumeric(value) = 1 

        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into @tmpTestAspects', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    
    
    -- Begin Update

    IF (@Command IS NULL) OR
       (@Command IS NOT NULL AND @Command NOT IN ('Init', 'Itst'))   -- If we are initializing from scratch, don't worry about transactions
    BEGIN
        BEGIN TRANSACTION DtwhGenerateDataTran1

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END
    
    
    -- We need to determine when this process was last run.  For our purposes we're going to look at the time dimension
    -- table and use that value.  
    
    SELECT  @DateLastRun = Max(DateValue)
      FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error getting last run date.', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

   -- select @DateLastRun = '2014-04-15 22:30:00'  -- Added by MDF 
   -- select @DateLastRun = '2014-04-17 13:00:00'  -- Added by MDF 
      
    IF @DateLastRun IS NULL     -- The table is not populated, initialize the date to the "default" value
    BEGIN
        SELECT @DateLastRun = @DefaultDate
    END
    
    PRINT '.'
    PRINT '.'
    PRINT 'Last Run Date Determined:  ' + Convert(varchar(30), @DateLastRun, 101)

    
    -- Update the time dimension table with dates between @DateLastRun and now
    
    SELECT @HoursDifference = DateDiff(hour, @DateLastRun, @now)

    SELECT @Index = @HoursDifference - 1
    WHILE @Index >= 0
    BEGIN
        INSERT INTO dbo.utb_dtwh_dim_time (DateValue, HourValue, DayOfMonth, JulianDay, MonthOfYear, Quarter, WeekOfYear, YearValue)
            SELECT  DateAdd(hour, DatePart(hour, DateAdd(hour, -1 * @Index, @now)), Convert(datetime, Convert(varchar(15), DateAdd(hour, -1 * @Index, @now), 101))),
                    DatePart(hour, DateAdd(hour, -1 * @Index, @now)),
                    DatePart(day, DateAdd(hour, -1 * @Index, @now)), 
                    DatePart(dayofyear, DateAdd(hour, -1 * @Index, @now)), 
                    DatePart(month, DateAdd(hour, -1 * @Index, @now)), 
                    DatePart(quarter, DateAdd(hour, -1 * @Index, @now)), 
                    DatePart(week, DateAdd(hour, -1 * @Index, @now)), 
                    DatePart(yyyy, DateAdd(hour, -1 * @Index, @now))
                    
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_time.', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        SELECT @Index = @Index - 1
    END


    PRINT '.'
    PRINT '.'
    PRINT 'Warehouse update commencing...'
    

    IF (@Command IS NULL) OR 
       (@Command IS NOT NULL AND @Command <> 'Rfsh')
    BEGIN
        PRINT '  Updating activity for the previous ' + Convert(varchar(5), @HoursDifference / 24) + ' day(s).'
    END
    ELSE
    BEGIN
        -- A non-desctructive refresh of the warehouse has been requested.  For the purposes of selecting all exposure,
        -- reset @DateLastRun to the default date

        SELECT @DateLastRun = @DefaultDate

        PRINT '  Non-Desctructive refresh requested.  Updating activity for all exposures.'
    END
    

   
    IF (@Command IN ('Test', 'Itst'))
    BEGIN 
        SELECT  @ExposuresDifference = Count(*)
          FROM  dbo.utb_claim_aspect ca WITH (NOLOCK)
          LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID)
          LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
        WHERE ca.ClaimAspectID IN (SELECT ClaimAspectID FROM @tmpTestAspects)
    END
    ELSE
    BEGIN
        SELECT  @ExposuresDifference = Count(*)
          FROM  dbo.utb_claim_aspect ca WITH (NOLOCK)
          LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID)
          LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
          WHERE ca.LynxID IN (SELECT  LynxID
                                FROM  dbo.utb_history_log WITH (NOLOCK)
                                WHERE SysLastUpdatedDate >= @DateLastRun)
            AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
            AND ca.ExposureCD IN ('1', '3')
            AND u.OfficeID IS NOT NULL
    END

    PRINT '  Found ' + Convert(varchar(9), @ExposuresDifference) + ' exposures to update.'

    
    -- Initialize process counter
    
    SELECT @ExposuresProcessed = 0
    
    
    -- Now declare a cursor containing a list of claim aspects to update the data for.  To get this list, we 
    -- will review the history log and capture any that have had any activity since @DateLastRun.

    IF @debug = 1
    BEGIN
        PRINT '.'
        PRINT '.'
        PRINT 'Claim Aspects Selected for cursor:'

        IF (@Command IN ('Test', 'Itst'))
        BEGIN 
            SELECT  ca.ClaimAspectID,
                    ca.ClaimAspectTypeID,
                    ca.ClaimAspectNumber,
                    c.LynxID,
                    c.CarrierRepUserID,
                    IsNull(ca.CompletedAnalystUserID, ca.AnalystUserID),
                    IsNull(ca.CompletedOwnerUserID, ca.OwnerUserID),
                    IsNull(ca.CompletedSupportUserID, ca.SupportUserID),
                    c.LossState,
                    ca.CreatedDate,
                    c.LossDate,
                    c.PolicyNumber,
                    c.DemoFlag,
	                caav.AuditedOriginalGrossAmount,
				    -- -------------------------------------------------------------------------------------------------------------
				    -- MAH - 07-jun-2012 OBSOLETE
				    -- -------------------------------------------------------------------------------------------------------------
                    -- caav.AdditionalLineItemValues,
            	    -- caav.LineItemValueCorrection,
            	    -- caav.MissingLineItemValues,
				    -- -------------------------------------------------------------------------------------------------------------
                    ca.EnabledFlag,
                    ca.ExposureCD,
                    ca.ClientCoverageTypeID,
                    casc.DispositionTypeCD,
                    ca.InitialAssignmentTypeID,
                    casc.InspectionDate,                    -- (SWR 201448)                    
                    actl.AssignmentTypeID,
                    actl.ServiceChannelDefaultCD,
                    casc.OriginalCompleteDate,
                    casc.ServiceChannelCD,
                    c.ClientClaimNumber,
                    cc.DeductibleAmt,
                    cc.LimitAmt,
                    ccr.DeductibleAmt,
                    ccr.LimitAmt,
                    ccr.MaximumDays,
                    casc.WorkEndDate,
                    casc.WorkStartDate,
                    cv.DriveableFlag,
                    cv.LicensePlateNumber,
                    cv.LicensePlateState,
                    cv.Make,
                    cv.Model,
                    cv.VehicleYear,
                    cv.Vin,
                    a.Name
              FROM  dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK)
              INNER JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (casc.ClaimAspectID = ca.ClaimAspectID)
              LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID) 
              LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
              LEFT JOIN dbo.utb_claim_coverage cc WITH (NOLOCK) ON (ca.LynxID = cc.LynxID AND ca.CoverageProfileCD = cc.CoverageTypeCD)
              LEFT JOIN dbo.utb_claim_coverage ccr WITH (NOLOCK) ON (ca.LynxID = ccr.LynxID AND ccr.CoverageTypeCD = 'RENT')
              LEFT JOIN dbo.utb_claim_aspect_audit_values caav WITH (NOLOCK) ON (ca.ClaimAspectID = caav.ClaimAspectID)
              LEFT JOIN dbo.utb_claim_vehicle cv WITH (NOLOCK) ON (ca.ClaimAspectID = cv.ClaimAspectID)  
              LEFT JOIN utb_assignment_type actl WITH (NOLOCK) ON (actl.AssignmentTypeID = dbo.ufnUtilityCompletedAssignmentTypeID(ca.InitialAssignmentTypeID, casc.ServiceChannelCD))
              LEFT JOIN utb_application a WITH (NOLOCK) ON ca.SourceApplicationID = a.ApplicationID
              WHERE ca.ClaimAspectID IN (SELECT ClaimAspectID FROM @tmpTestAspects)
                AND casc.PrimaryFlag = 1
        END
        ELSE
        BEGIN
            SELECT  ca.ClaimAspectID,
                    ca.ClaimAspectTypeID,
                    ca.ClaimAspectNumber,
                    c.LynxID,
                    c.CarrierRepUserID,
                    IsNull(ca.CompletedAnalystUserID, ca.AnalystUserID),
                    IsNull(ca.CompletedOwnerUserID, ca.OwnerUserID),
                    IsNull(ca.CompletedSupportUserID, ca.SupportUserID),
                    c.LossState,
                    ca.CreatedDate,
                    c.LossDate,
                    c.PolicyNumber,
                    c.DemoFlag,
	                caav.AuditedOriginalGrossAmount,
				    -- -------------------------------------------------------------------------------------------------------------
				    -- MAH - 07-jun-2012 OBSOLETE
				    -- -------------------------------------------------------------------------------------------------------------
                    -- caav.AdditionalLineItemValues,
            	    -- caav.LineItemValueCorrection,
            	    -- caav.MissingLineItemValues,
				    -- -------------------------------------------------------------------------------------------------------------
                    ca.EnabledFlag,
                    ca.ExposureCD,
                    ca.ClientCoverageTypeID,
                    casc.DispositionTypeCD,
                    ca.InitialAssignmentTypeID,
                    casc.InspectionDate,                    -- (SWR 201448)
                    actl.AssignmentTypeID,
                    actl.ServiceChannelDefaultCD,
                    casc.OriginalCompleteDate,
                    casc.ServiceChannelCD,
                    c.ClientClaimNumber,
                    cc.DeductibleAmt,
                    cc.LimitAmt,
                    ccr.DeductibleAmt,
                    ccr.LimitAmt,
                    ccr.MaximumDays,
                    casc.WorkEndDate,
                    casc.WorkStartDate,
                    cv.DriveableFlag,
                    cv.LicensePlateNumber,
                    cv.LicensePlateState,
                    cv.Make,
                    cv.Model,
                    cv.VehicleYear,
                    cv.Vin,
                    a.Name
              FROM  dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK)
              INNER JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (casc.ClaimAspectID = ca.ClaimAspectID)
              LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID) 
              LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
              LEFT JOIN dbo.utb_claim_coverage cc WITH (NOLOCK) ON (ca.LynxID = cc.LynxID AND ca.CoverageProfileCD = cc.CoverageTypeCD)
              LEFT JOIN dbo.utb_claim_coverage ccr WITH (NOLOCK) ON (ca.LynxID = ccr.LynxID AND ccr.CoverageTypeCD = 'RENT')
              LEFT JOIN dbo.utb_claim_aspect_audit_values caav WITH (NOLOCK) ON (ca.ClaimAspectID = caav.ClaimAspectID)
              LEFT JOIN dbo.utb_claim_vehicle cv WITH (NOLOCK) ON (ca.ClaimAspectID = cv.ClaimAspectID)
              LEFT JOIN utb_assignment_type actl WITH (NOLOCK) ON (actl.AssignmentTypeID = dbo.ufnUtilityCompletedAssignmentTypeID(ca.InitialAssignmentTypeID, casc.ServiceChannelCD))
              LEFT JOIN utb_application a WITH (NOLOCK) ON ca.SourceApplicationID = a.ApplicationID
              WHERE ca.LynxID IN (SELECT  LynxID
                                    FROM  dbo.utb_history_log WITH (NOLOCK)
                                    WHERE SysLastUpdatedDate >= @DateLastRun)
                AND casc.PrimaryFlag = 1
                AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                AND ca.ExposureCD IN ('1', '3')
                AND u.OfficeID IS NOT NULL
        END
    END
    
            
    IF (@Command IN ('Test', 'Itst'))
    BEGIN 
        DECLARE csrClaimAspect CURSOR FAST_FORWARD FOR
          SELECT  ca.ClaimAspectID,
                  ca.ClaimAspectTypeID,
                  ca.ClaimAspectNumber,
                  c.LynxID,
                  c.CarrierRepUserID,
                  IsNull(ca.CompletedAnalystUserID, ca.AnalystUserID),
                  IsNull(ca.CompletedOwnerUserID, ca.OwnerUserID),
                  IsNull(ca.CompletedSupportUserID, ca.SupportUserID),
                  c.LossState,
                  ca.CreatedDate,
                  c.LossDate,
                  c.PolicyNumber,
                  c.DemoFlag,
                  caav.AuditedOriginalGrossAmount,
				  -- -------------------------------------------------------------------------------------------------------------
				  -- MAH - 07-jun-2012 OBSOLETE
				  -- -------------------------------------------------------------------------------------------------------------
                  -- caav.AdditionalLineItemValues,
                  -- caav.LineItemValueCorrection,
                  -- caav.MissingLineItemValues,
				  -- -------------------------------------------------------------------------------------------------------------
                  ca.EnabledFlag,
                  ca.ExposureCD,
                  ca.ClientCoverageTypeID,
                  casc.DispositionTypeCD,
                  ca.InitialAssignmentTypeID,
                  casc.InspectionDate,                  -- (SWR 201448)
                  actl.AssignmentTypeID,
                  actl.ServiceChannelDefaultCD,
                  casc.OriginalCompleteDate,
                  casc.ServiceChannelCD,
                  c.ClientClaimNumber,
                  cc.DeductibleAmt,
                  cc.LimitAmt,
                  ccr.DeductibleAmt,
                  ccr.LimitAmt,
                  ccr.MaximumDays,
                  casc.WorkEndDate,
                  casc.WorkStartDate,
                  cv.DriveableFlag,
                  cv.LicensePlateNumber,
                  cv.LicensePlateState,
                  cv.Make,
                  cv.Model,
                  cv.VehicleYear,
                  cv.Vin,
                  a.Name,
                  casc.ClaimAspectServiceChannelID
            FROM  dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK)
            INNER JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (casc.ClaimAspectID = ca.ClaimAspectID)
            LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID) 
            LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
            LEFT JOIN dbo.utb_claim_coverage cc WITH (NOLOCK) ON (ca.LynxID = cc.LynxID AND ca.CoverageProfileCD = cc.CoverageTypeCD)
            LEFT JOIN dbo.utb_claim_coverage ccr WITH (NOLOCK) ON (ca.LynxID = ccr.LynxID AND ccr.CoverageTypeCD = 'RENT')
            LEFT JOIN dbo.utb_claim_aspect_audit_values caav WITH (NOLOCK) ON (ca.ClaimAspectID = caav.ClaimAspectID)
            LEFT JOIN dbo.utb_claim_vehicle cv WITH (NOLOCK) ON (ca.ClaimAspectID = cv.ClaimAspectID)
            LEFT JOIN utb_assignment_type actl WITH (NOLOCK) ON (actl.AssignmentTypeID = dbo.ufnUtilityCompletedAssignmentTypeID(ca.InitialAssignmentTypeID, casc.ServiceChannelCD))
            LEFT JOIN utb_application a WITH (NOLOCK) ON ca.SourceApplicationID = a.ApplicationID
            WHERE ca.ClaimAspectID IN (SELECT ClaimAspectID FROM @tmpTestAspects)
              AND casc.PrimaryFlag = 1
            ORDER BY ca.ClaimAspectID
    END
    ELSE
    BEGIN
        DECLARE csrClaimAspect CURSOR FAST_FORWARD FOR
          SELECT  ca.ClaimAspectID,
                  ca.ClaimAspectTypeID,
                  ca.ClaimAspectNumber,
                  c.LynxID,
                  c.CarrierRepUserID,
                  IsNull(ca.CompletedAnalystUserID, ca.AnalystUserID),
                  IsNull(ca.CompletedOwnerUserID, ca.OwnerUserID),
                  IsNull(ca.CompletedSupportUserID, ca.SupportUserID),
                  c.LossState,
                  ca.CreatedDate,
                  c.LossDate,
                  c.PolicyNumber,
                  c.DemoFlag,
                  caav.AuditedOriginalGrossAmount,
				  -- -------------------------------------------------------------------------------------------------------------
				  -- MAH - 07-jun-2012 OBSOLETE
				  -- -------------------------------------------------------------------------------------------------------------
                  -- caav.AdditionalLineItemValues,
                  -- caav.LineItemValueCorrection,
                  -- caav.MissingLineItemValues,
				  -- -------------------------------------------------------------------------------------------------------------
                  ca.EnabledFlag,
                  ca.ExposureCD,
                  ca.ClientCoverageTypeID,
                  casc.DispositionTypeCD,
                  ca.InitialAssignmentTypeID,
                  casc.InspectionDate,              -- (SWR 201448)
                  actl.AssignmentTypeID,
                  actl.ServiceChannelDefaultCD,
                  casc.OriginalCompleteDate,
                  casc.ServiceChannelCD,
                  c.ClientClaimNumber,
                  cc.DeductibleAmt,
                  cc.LimitAmt,
                  ccr.DeductibleAmt,
                  ccr.LimitAmt,
                  ccr.MaximumDays,
                  casc.WorkEndDate,
                  casc.WorkStartDate,
                  cv.DriveableFlag,
                  cv.LicensePlateNumber,
                  cv.LicensePlateState,
                  cv.Make,
                  cv.Model,
                  cv.VehicleYear,
                  cv.Vin,
                  a.Name,
                  casc.ClaimAspectServiceChannelID
            FROM  dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK)
            INNER JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (casc.ClaimAspectID = ca.ClaimAspectID)
            LEFT JOIN dbo.utb_claim c WITH (NOLOCK) ON (ca.LynxID = c.LynxID) 
            LEFT JOIN dbo.utb_user u WITH (NOLOCK) ON (c.CarrierRepUserID = u.UserID) 
            LEFT JOIN dbo.utb_claim_coverage cc WITH (NOLOCK) ON (ca.LynxID = cc.LynxID AND ca.CoverageProfileCD = cc.CoverageTypeCD)
            LEFT JOIN dbo.utb_claim_coverage ccr WITH (NOLOCK) ON (ca.LynxID = ccr.LynxID AND ccr.CoverageTypeCD = 'RENT')
            LEFT JOIN dbo.utb_claim_aspect_audit_values caav WITH (NOLOCK) ON (ca.ClaimAspectID = caav.ClaimAspectID)
            LEFT JOIN dbo.utb_claim_vehicle cv WITH (NOLOCK) ON (ca.ClaimAspectID = cv.ClaimAspectID)
            LEFT JOIN utb_assignment_type actl WITH (NOLOCK) ON (actl.AssignmentTypeID = dbo.ufnUtilityCompletedAssignmentTypeID(ca.InitialAssignmentTypeID, casc.ServiceChannelCD))
            LEFT JOIN utb_application a WITH (NOLOCK) ON ca.SourceApplicationID = a.ApplicationID
            WHERE ca.LynxID IN (SELECT  LynxID
                                  FROM  dbo.utb_history_log WITH (NOLOCK)
                                  WHERE SysLastUpdatedDate >= @DateLastRun)
              AND casc.PrimaryFlag = 1
              AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
              AND ca.ExposureCD IN ('1', '3')
              AND u.OfficeID IS NOT NULL
            ORDER BY ca.ClaimAspectID    
    END
              
    OPEN csrClaimAspect

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    SELECT  @AnalystUserID = NULL,
            @ClaimAspectID = NULL,
            @ClaimAspectTypeID = NULL,
            @ClaimAspectNumber = NULL,
            @LynxID = NULL,
            @CarrierRepUserID = NULL,
            @OwnerUserID = NULL,
            @LossState = NULL,
            @VehicleCreatedDate = NULL,
            @LossDate = NULL,
            @PolicyNumber = NULL,
            @DemoFlag = NULL,
	        @OriginalAuditedEstimateGrossAmt = NULL, -- MAH 07-jun-2012 These fields were reversed @EarlyBillAuditedGrossAmt = NULL,
			-- -------------------------------------------------------------------------------------------------------------
			-- MAH - 07-jun-2012 OBSOLETE
			-- -------------------------------------------------------------------------------------------------------------
	        -- @EarlyBillAuditedAddtlLineItemAmt = NULL,
	        -- @EarlyBillAuditedLineItemCorrectionAmt = NULL,
	        -- @EarlyBillAuditedMissingLineItemAmt = NULL,
			-- -------------------------------------------------------------------------------------------------------------
            @EnabledFlag = NULL,
            @ExposureCD = NULL,
            @ClientCoverageTypeID = NULL,
            @DispositionTypeCD = NULL,
            @InitialAssignmentTypeID = NULL,
            @ClosingAssignmentTypeID = NULL,
            @ClosingServiceChannelCD = NULL,
            @InspectionDate = NULL,                 -- (SWR 201448)
            @OriginalCompleteDate = NULL,
            @ServiceChannelCD = NULL,
            @ClientClaimNumber = NULL,
            @PolicyDeductibleAmt = NULL,
            @PolicyLimitAmt = NULL,
            @PolicyRentalDeductibleAmt = NULL,
            @PolicyRentalLimitAmt = NULL,
            @PolicyRentalMaxDays = NULL,
            @RepairEndDate = NULL,
            @RepairStartDate = NULL,
            @SupportUserID = NULL,
            @VehicleDriveableFlag = NULL,
            @VehicleLicensePlateNumber = NULL,
            @LicensePlateStateCode = NULL,
            @VehicleMake = NULL,
            @VehicleModel = NULL,
            @VehicleYear = NULL,
            @VehicleVIN = NULL,
            @ClaimSourceID = NULL,
            @ClaimAspectServiceChannelID = NULL
    
    
    FETCH NEXT FROM csrClaimAspect
      INTO  @ClaimAspectID,
            @ClaimAspectTypeID,
            @ClaimAspectNumber,
            @LynxID,
            @CarrierRepUserID,
            @AnalystUserID,
            @OwnerUserID,
            @SupportUserID,
            @LossState,
            @VehicleCreatedDate,
            @LossDate,
            @PolicyNumber,
            @DemoFlag,
	        @OriginalAuditedEstimateGrossAmt, -- MAH 07-jun-2012 these fields were reversed - @EarlyBillAuditedGrossAmt,
			-- -----------------------------------------------------------
			--  MAH 07-jun-2012 Obsolete:
			-- -----------------------------------------------------------
			-- @EarlyBillAuditedAddtlLineItemAmt,
			-- @EarlyBillAuditedLineItemCorrectionAmt,
			-- @EarlyBillAuditedMissingLineItemAmt,
			-- -----------------------------------------------------------
            @EnabledFlag,
            @ExposureCD,
            @ClientCoverageTypeID,
            @DispositionTypeCD,
            @InitialAssignmentTypeID,
            @InspectionDate,                        -- (SWR 201448)
            @ClosingAssignmentTypeID,
            @ClosingServiceChannelCD,
            @OriginalCompleteDate,
            @ServiceChannelCD,
            @ClientClaimNumber,
            @PolicyDeductibleAmt,
            @PolicyLimitAmt,
            @PolicyRentalDeductibleAmt,
            @PolicyRentalLimitAmt,
            @PolicyRentalMaxDays,
            @RepairEndDate,
            @RepairStartDate,
            @VehicleDriveableFlag,
            @VehicleLicensePlateNumber,
            @LicensePlateStateCode,
            @VehicleMake,
            @VehicleModel,
            @VehicleYear,
            @VehicleVIN,
            @ApplicationName,
            @ClaimAspectServiceChannelID
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Summarize claim data
        
        IF @debug = 1
        BEGIN
            PRINT ''
            PRINT 'Current Fetch Record:'
            SELECT  @ClaimAspectID AS ClaimAspectID,
                    @ClaimAspectTypeID AS ClaimAspectTypeID,
                    @ClaimAspectNumber AS ClaimAspectNumber,
                    @LynxID AS LynxID,
                    @CarrierRepUserID AS CarrierRepUserID,
                    @AnalystUserID AS AnalystUserID,
                    @OwnerUserID AS OwnerUserID,
                    @SupportUserID AS SupportUserID,
                    @LossState AS LossState,
                    @VehicleCreatedDate AS VehicleCreatedDate,
                    @LossDate AS LossDate,
                    @PolicyNumber AS PolicyNumber,
                    @DemoFlag AS DemoFlag,
					@OriginalAuditedEstimateGrossAmt AS OriginalAuditedEstimateGrossAmt, -- MAH 07-jun-2012 these fields were reversed - @EarlyBillAuditedGrossAmt AS EarlyBillAuditedGrossAmt,
	                -- -----------------------------------------------------------
					--  MAH 07-jun-2012 Obsolete:
					-- -----------------------------------------------------------
					--@EarlyBillAuditedAddtlLineItemAmt AS EarlyBillAuditedAddtlLineItemAmt,
					--@EarlyBillAuditedLineItemCorrectionAmt AS EarlyBillAuditedLineItemCorrectionAmt,
					--@EarlyBillAuditedMissingLineItemAmt AS EarlyBillAuditedMissingLineItemAmt,
					-- -----------------------------------------------------------
                    @EnabledFlag AS EnabledFlag, 
                    @ExposureCD AS ExposureCD,
                    @ClientCoverageTypeID AS ClientCoverageTypeID,
                    @DispositionTypeCD AS DispositionTypeCD,
                    @InitialAssignmentTypeID AS InitialAssignmentTypeID,
                    @ClosingAssignmentTypeID AS ClosingAssignmentTypeID,
                    @ClosingServiceChannelCD AS ClosingServiceChannelCD,
                    @InspectionDate AS InspectionDate,                      -- (SWR 201448)
                    @OriginalCompleteDate AS OriginalCompleteDate,
                    @ServiceChannelCD AS ServiceChannelCD,
                    @ClientClaimNumber AS ClientClaimNumber,
                    @PolicyDeductibleAmt AS PolicyDeductibleAmt,
                    @PolicyLimitAmt AS PolicyLimitAmt,
                    @PolicyRentalDeductibleAmt AS PolicyRentalDeductibleAmt,
                    @PolicyRentalLimitAmt AS PolicyRentalLimitAmt,
                    @PolicyRentalMaxDays AS PolicyRentalMaxDays,
                    @RepairEndDate AS RepairEndDate,
                    @RepairStartDate AS RepairStartDate,
                    @VehicleDriveableFlag AS VehicleDriveableFlag,
                    @VehicleLicensePlateNumber AS VehicleLicensePlateNumber,
                    @LicensePlateStateCode AS LicensePlateStateCode,
                    @VehicleMake AS VehicleMake,
                    @VehicleModel AS VehicleModel,
                    @VehicleYear AS VehicleYear,
                    @VehicleVIN AS VehicleVIN,
                    @ApplicationName AS ApplicationName,
                    @ClaimAspectServiceChannelID AS ClaimAspectServiceChannelID
        END
        
--         PRINT 'Currently Processing ClaimAspect: ' + Convert(varchar(10), @ClaimAspectID)
        
        -- Initialize our work variables for this claim summary
        
        SELECT  @AssignmentTypeClosingID = NULL,
                @AssignmentTypeID = NULL,
                @ClaimLocationID = NULL,
                @CoverageTypeID = NULL,
                @CustomerID = NULL,
                @DispositionTypeID = NULL,
                @LynxHandlerAnalystID = NULL,
                @LynxHandlerOwnerID = NULL,
                @LynxHandlerSupportID = NULL,
                @RepairLocationIDClaim = NULL,
                @ServiceChannelID = NULL,
                @TimeIDAssignDownload = NULL,
                @TimeIDAssignSent = NULL,
                @TimeIDCancelled = NULL,
                @TimeIDClosed = NULL,
                @TimeIDEstimate = NULL,
                @TimeIDInspection = NULL,               -- (SWR 201448)
                @TimeIDNew = NULL,
                @TimeIDReclosed = NULL,
                @TimeIDRepairComplete = NULL,
                @TimeIDRepairStarted = NULL,
                @TimeIDReopened = NULL,
                @TimeIDVoided = NULL,
                @VehicleLicensePlateStateID = NULL,
                @AuditedEstimateAgreedFlag = NULL,
                @AuditedEstimateBettermentAmt = NULL,
                @AuditedEstimateDeductibleAmt = NULL,
                @AuditedEstimateGrossAmt = NULL,
                @AuditedEstimateNetAmt = NULL,
                @AuditedEstimateOtherAdjustmentAmt = NULL,
                @AuditedEstimatewoBettAmt = NULL,
                @CFProgramFlag= NULL,
				@ClaimantName = NULL,					-- (SWR 203304)
                @ClaimStatusCD = NULL,
        -- Client Claim Number initialized in cursor variables (DO NOT INITIALIZE HERE)
                @CycleTimeAssignToEstBusDay = NULL,
                @CycleTimeAssignToEstCalDay = NULL,
                @CycleTimeAssignToEstHrs = NULL,
                @CycleTimeEstToCloseBusDay = NULL,
                @CycleTimeEstToCloseCalDay = NULL,
                @CycleTimeEstToCloseHrs = NULL,
                @CycleTimeInspectionToEstBusDay = NULL,         -- (SWR 201448)
                @CycleTimeInspectionToEstCalDay = NULL,         -- (SWR 201448)
                @CycleTimeInspectionToEstHrs = NULL,            -- (SWR 201448)
                @CycleTimeNewToCloseBusDay = NULL,
                @CycleTimeNewToCloseCalDay = NULL,
                @CycleTimeNewToCloseHrs = NULL,
                @CycleTimeNewToEstBusDay = NULL,
                @CycleTimeNewToEstCalDay = NULL,
                @CycleTimeNewToEstHrs = NULL,
                @CycleTimeNewToInspectionBusDay = NULL,         -- (SWR 201448)
                @CycleTimeNewToInspectionCalDay = NULL,         -- (SWR 201448)
                @CycleTimeNewToInspectionHrs = NULL,            -- (SWR 201448)
                @CycleTimeNewToRecloseBusDay = NULL,
                @CycleTimeNewToRecloseCalDay = NULL,
                @CycleTimeNewToRecloseHrs = NULL,
                @CycleTimeRepairStartToEndBusDay = NULL,
                @CycleTimeRepairStartToEndCalDay = NULL,
                @CycleTimeRepairStartToEndHrs = NULL,
        -- DemoFlag, EnabledFlag & ExposureCD initialized in cursor variables (DO NOT INITIALIZE HERE)
                @EarlyBillAuditedGrossAmt = NULL,
			    @FeeRevenueAmt = NULL,
                @FinalAuditedSuppAgreedFlag = NULL,
                @FinalAuditedSuppBettermentAmt = NULL,
                @FinalAuditedSuppDeductibleAmt = NULL,
                @FinalAuditedSuppGrossAmt = NULL,
                @FinalAuditedSuppNetAmt = NULL,
                @FinalAuditedSuppOtherAdjustmentAmt = NULL,
                @FinalAuditedSuppwoBettAmt = NULL,
                @FinalEstimateBettermentAmt = NULL,
                @FinalEstimateDeductibleAmt = NULL,
                @FinalEstimateGrossAmt = NULL,
                @FinalEstimateNetAmt = NULL,
                @FinalEstimateOtherAdjustmentAmt = NULL,
                @FinalEstimatewoBettAmt = NULL,
                @FinalSupplementBettermentAmt = NULL,
                @FinalSupplementDeductibleAmt = NULL,
                @FinalSupplementGrossAmt = NULL,
                @FinalSupplementNetAmt = NULL,
                @FinalSupplementOtherAdjustmentAmt = NULL,
                @FinalSupplementwoBettAmt = NULL,
                @IndemnityAmount = NULL,
				@InsuredName = NULL,					-- (SWR 203304)
                @LaborRateBodyAmt = NULL,
                @LaborRateFrameAmt = NULL,
                @LaborRateMechAmt = NULL,
                @LaborRateRefinishAmt = NULL,
                @LaborRepairAmt = NULL,
                @LaborReplaceAmt = NULL,
                @LaborTotalAmt = NULL,
        -- LossDate, LynxID initialized in cursor variables (DO NOT INITIALIZE HERE)        
                @MaxEstSuppSequenceNumber = NULL,
                @OriginalEstimateBettermentAmt = NULL,
                @OriginalEstimateDeductibleAmt = NULL,
                @OriginalEstimateGrossAmt = NULL,
                @OriginalEstimateNetAmt = NULL,
                @OriginalEstimateOtherAdjustmentAmt = NULL,
                @OriginalEstimatewoBettAmt = NULL,
                @OriginalAuditedEstimateBettermentAmt = NULL,
                @OriginalAuditedEstimateDeductibleAmt = NULL,
        -- OriginalAuditedEstimateGrossAmt initialized in cursor variables (DO NOT INITIALIZE HERE)
                @OriginalAuditedEstimateNetAmt = NULL,
                @OriginalAuditedEstimateOtherAdjustmentAmt = NULL,
                @OriginalAuditedEstimatewoBettAmt = NULL,
                @PartsAFMKReplacedAmt = NULL,
                @PartsLKQReplacedAmt = NULL,
                @PartsOEMDiscountAmt = NULL,
                @PartsOEMReplacedAmt = NULL,
                @PartsRemanReplacedAmt = NULL,
                @PartsTotalReplacedAmt = NULL,
                @PhotosAttachedFlag = NULL,                             -- (SWR 201448)
        -- PolicyNumber, Deductible, Limit, RentalDeductible, RentalLimit, RentalMaxDays initialized in cursor variables (DO NOT INITIALIZE HERE)
                @ProgramCD = NULL,
                @ReinspectionCount = NULL,
                @ReinspectionDeviationAmt = NULL,
                @ReinspectionDeviationCount = NULL,
                @RentalDays = NULL,
                @RentalAmount = NULL,
                @RentalCostPerDay = NULL,
                @ServiceLossOfUseFlag = NULL,
                @ServiceSubrogationFlag = NULL,
                @ServiceTotalLossFlag = NULL,
                @ServiceTotalTheftFlag = NULL,       
                @SupplementTotalAmt = NULL,
       -- VehicleDriveable, LicensePlateNumber, Make, Model, Year, VIN initialized in cursor variables (DO NOT INITIALIZE HERE)
                @TotalConcessionLaborRate = NULL,
                @TotalConcessionLaborRateComment = NULL,
                @TotalConcessionUnrelatedDamage = NULL,
                @TotalConcessionUnrelatedDamageComment = NULL,
                @TotalConcessionPartsUsage = NULL,
                @TotalConcessionPartsUsageComment = NULL,
                @TotalConcessionTotalLoss = NULL,
                @TotalConcessionTotalLossComment = NULL

        

        -- Gather some information about the assignments and estimates for this claim aspect to be used later

        DELETE FROM @tmpAssignment

        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from @tmpAssignment', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        INSERT INTO @tmpAssignment (AssignmentID, SelectionDate, AssignmentDate, CancellationDate, AppraiserID, ShopLocationID, ProgramTypeCD, CertifiedFirstFlag)
            SELECT  a.AssignmentID,
                    a.SelectionDate,
                    a.AssignmentDate,
                    a.CancellationDate,
                    a.AppraiserID,
                    a.ShopLocationID,
                    a.ProgramTypeCD,
                    a.CertifiedFirstFlag
              FROM  dbo.utb_assignment a WITH (NOLOCK)
              INNER JOIN dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK) ON (a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
              INNER JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (casc.ClaimAspectID = ca.ClaimAspectID)
              WHERE ca.ClaimAspectID = @ClaimAspectID
                AND casc.PrimaryFlag = 1
                AND (a.ShopLocationID IS NOT NULL OR a.AppraiserID IS NOT NULL)    -- This eliminates "bad data" we have in production

        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into @tmpAssignment', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @debug = 1
        BEGIN
            PRINT ''
            PRINT 'Assignments for this claim aspect:'
            SELECT * FROM @tmpAssignment
        END
        
              
        DELETE FROM @tmpDocument

        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from @tmpDocument', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
--        Select @ClaimAspectID				-- Added by MDF

/*** This query replaced below because of a need to sum rows from utb_estimate_summary 04/17/2014

        INSERT INTO @tmpDocument 
          (DocumentID, 
           AssignmentID, 
           DocumentSourceName, 
           VanSourceFlag, 
           SequenceNumber, 
           CreatedDate, 
           DuplicateFlag, 
           EstimateTypeCD,
           OriginalLaborRateBody,
           OriginalLaborRateFrame,
           OriginalLaborRateMech,
           OriginalLaborRateRefinish,
           OriginalLaborAmountRepair,
           OriginalLaborAmountReplace,
           OriginalLaborAmountTotal,
           OriginalPartsAmountAFMK,
           OriginalPartsAmountLKQ,
           OriginalPartsAmountOEM,
           OriginalPartsAmountReman,
           OriginalPartsAmountTotal,
           OriginalPartsDiscount,
           OriginalRepairTotal,
           OriginalBetterment, 
           OriginalRepairTotalwoBetterment,
           OriginalDeductible,
           OriginalOtherAdjustmentTotal,
           OriginalNetTotal, 
           AgreedRepairTotal, 
           AgreedBetterment,                                   
           AgreedRepairTotalwoBetterment, 
           AgreedDeductible,
           AgreedOtherAdjustmentTotal,
           AgreedNetTotal, 
           AgreedPriceMetCD,
           ApprovedFlag,
           FinalBillEstimateFlag)
          SELECT  d.DocumentID,
                  d.AssignmentID,
                  ds.Name,
                  ds.VanFlag,
                  d.SupplementSeqNumber,
                  d.CreatedDate,
                  d.DuplicateFlag,
                  d.EstimateTypeCD,
                  IsNull((SELECT es.OriginalUnitAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBodyLabor), 0),
                  IsNull((SELECT es.OriginalUnitAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDFrameLabor), 0),
                  IsNull((SELECT es.OriginalUnitAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDMechLabor), 0),
                  IsNull((SELECT SUM(es.OriginalUnitAmt) FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID IN (@EstimateSummaryTypeIDPaintLabor, @EstimateSummaryTypeIDRefinishLabor)), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairLabor), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDReplaceLabor), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDTotalLabor), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAFMKParts), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDLKQParts), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOEMParts), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRemanParts), 0),                  
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDTaxableParts), 0)
                      + IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNontaxParts), 0)
                      + IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsMarkup), 0)
                      - IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsDiscount), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsDiscount), 0),                  
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0)
                      - IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible), 0),
                  IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAppAllowance), 0)
                      + IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDUPD), 0)
                      + IsNull((SELECT es.OriginalExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOtherAdj), 0),
                  IsNull((SELECT  CASE
                                    WHEN es.OriginalExtendedAmt < 0 THEN 0
                                    ELSE es.OriginalExtendedAmt
                                  END
                            FROM  dbo.utb_estimate_summary es 
                            WHERE es.DocumentID = d.DocumentID 
                              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), 0),
                  IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0),
                  IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0)
                      - IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible), 0),
                  IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAppAllowance), 0)
                      + IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDUPD), 0)
                      + IsNull((SELECT es.AgreedExtendedAmt FROM dbo.utb_estimate_summary es WHERE es.DocumentID = d.DocumentID AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOtherAdj), 0),
                  IsNull((SELECT  CASE
                                    WHEN es.AgreedExtendedAmt < 0 THEN 0
                                    ELSE es.AgreedExtendedAmt
                                  END
                            FROM  dbo.utb_estimate_summary es 
                            WHERE es.DocumentID = d.DocumentID 
                              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), 0),
                  (SELECT TOP 1 sd.AgreedPriceMetCD
                    FROM dbo.utb_claim_aspect_service_channel_document scascd
                    LEFT JOIN utb_claim_aspect_service_channel scasc ON (scascd.ClaimAspectServiceChannelID = scasc.ClaimAspectServiceChannelID)                    
                    LEFT JOIN dbo.utb_document sd ON (scascd.DocumentID = sd.DocumentID)
                    WHERE scasc.ClaimAspectID = casc.ClaimAspectID
                      AND scasc.PrimaryFlag = 1
                      AND sd.SupplementSeqNumber = d.SupplementSeqNumber
                    ORDER BY sd.AgreedpriceMetCD DESC),
                  d.ApprovedFlag,
                  d.FinalBillEstimateFlag
              FROM  dbo.utb_claim_aspect_service_channel_document cascd WITH (NOLOCK)
              INNER JOIN dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK) ON (cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
              LEFT JOIN dbo.utb_document d WITH (NOLOCK) ON (cascd.DocumentID = d.DocumentID)
              LEFT JOIN dbo.utb_document_type dt WITH (NOLOCK) ON (d.DocumentTypeID = dt.DocumentTypeID)
              LEFT JOIN dbo.utb_document_source ds WITH (NOLOCK) ON (d.DocumentSourceID = ds.DocumentSourceID) 
              WHERE casc.ClaimAspectID = @ClaimAspectID
                AND casc.PrimaryFlag = 1
                AND dt.EstimateTypeFlag = 1
                AND d.EnabledFlag = 1
                and @ClaimAspectID != 1016022 
                and @ClaimAspectID != 1030196
                
***/

        INSERT INTO @tmpDocument 
          (DocumentID, 
           AssignmentID, 
           DocumentSourceName, 
           VanSourceFlag, 
           SequenceNumber, 
           CreatedDate, 
           DuplicateFlag, 
           EstimateTypeCD,
           OriginalLaborRateBody,
           OriginalLaborRateFrame,
           OriginalLaborRateMech,
           OriginalLaborRateRefinish,
           OriginalLaborAmountRepair,
           OriginalLaborAmountReplace,
           OriginalLaborAmountTotal,
           OriginalPartsAmountAFMK,
           OriginalPartsAmountLKQ,
           OriginalPartsAmountOEM,
           OriginalPartsAmountReman,
           OriginalPartsAmountTotal,
           OriginalPartsDiscount,
           OriginalRepairTotal,
           OriginalBetterment, 
           OriginalRepairTotalwoBetterment,
           OriginalDeductible,
           OriginalOtherAdjustmentTotal,
           OriginalNetTotal, 
           AgreedRepairTotal, 
           AgreedBetterment,                                   
           AgreedRepairTotalwoBetterment, 
           AgreedDeductible,
           AgreedOtherAdjustmentTotal,
           AgreedNetTotal, 
           AgreedPriceMetCD,
           ApprovedFlag,
           FinalBillEstimateFlag)
              SELECT  d.DocumentID,
                  d.AssignmentID,
                  ds.Name,
                  ds.VanFlag,
                  d.SupplementSeqNumber,
                  d.CreatedDate,
                  d.DuplicateFlag,
                  d.EstimateTypeCD,
                  IsNull((SELECT SUM(es.OriginalUnitAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBodyLabor), 0),
                  IsNull((SELECT SUM(es.OriginalUnitAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDFrameLabor), 0),
                  IsNull((SELECT SUM(es.OriginalUnitAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDMechLabor), 0),
                  IsNull((SELECT SUM(es.OriginalUnitAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                         AND es.EstimateSummaryTypeID IN (@EstimateSummaryTypeIDPaintLabor, @EstimateSummaryTypeIDRefinishLabor)), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                         AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairLabor), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDReplaceLabor), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDTotalLabor), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                         AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAFMKParts), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDLKQParts), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOEMParts), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                        FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRemanParts), 0),                  
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDTaxableParts), 0)
                      + IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNontaxParts), 0)
                      + IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                               WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsMarkup), 0)
                      - IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsDiscount), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDPartsDiscount), 0),                  
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt)
                                        FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0)
                      - IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible), 0),
                  IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAppAllowance), 0)
                      + IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDUPD), 0)
                      + IsNull((SELECT SUM(es.OriginalExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOtherAdj), 0),
                  IsNull((SELECT SUM(CASE WHEN es.OriginalExtendedAmt < 0 THEN 0
                                          ELSE es.OriginalExtendedAmt
                                     END)
                            FROM dbo.utb_estimate_summary es 
                           WHERE es.DocumentID = d.DocumentID 
                             AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), 0),
                  IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0),
                  IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), 0)
                      - IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), 0),
                  IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible), 0),
                  IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                          FROM dbo.utb_estimate_summary es 
                                       WHERE es.DocumentID = d.DocumentID 
                                           AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDAppAllowance), 0)
                      + IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDUPD), 0)
                      + IsNull((SELECT SUM(es.AgreedExtendedAmt) 
                                                  FROM dbo.utb_estimate_summary es 
                                                 WHERE es.DocumentID = d.DocumentID 
                                                   AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDOtherAdj), 0),
                  IsNull((SELECT SUM(CASE WHEN es.AgreedExtendedAmt < 0 THEN 0
                                                              ELSE es.AgreedExtendedAmt
                                     END)
                            FROM dbo.utb_estimate_summary es 
                           WHERE es.DocumentID = d.DocumentID 
                             AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), 0),
                  (SELECT TOP 1 sd.AgreedPriceMetCD
                     FROM dbo.utb_claim_aspect_service_channel_document scascd
                                      LEFT JOIN utb_claim_aspect_service_channel scasc 
                                                 ON (scascd.ClaimAspectServiceChannelID = scasc.ClaimAspectServiceChannelID)                    
                                      LEFT JOIN dbo.utb_document sd 
                                                 ON (scascd.DocumentID = sd.DocumentID)
                    WHERE scasc.ClaimAspectID = casc.ClaimAspectID
                      AND scasc.PrimaryFlag = 1
                      AND sd.SupplementSeqNumber = d.SupplementSeqNumber
                    ORDER 
                                 BY sd.AgreedpriceMetCD DESC),
                              d.ApprovedFlag,
                              d.FinalBillEstimateFlag
              FROM  dbo.utb_claim_aspect_service_channel_document cascd WITH (NOLOCK)
                              INNER JOIN dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK) 
                                        ON (cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
                              LEFT JOIN dbo.utb_document d WITH (NOLOCK) 
                                       ON (cascd.DocumentID = d.DocumentID)
                              LEFT JOIN dbo.utb_document_type dt WITH (NOLOCK) 
                                       ON (d.DocumentTypeID = dt.DocumentTypeID)
                              LEFT JOIN dbo.utb_document_source ds WITH (NOLOCK) 
                                       ON (d.DocumentSourceID = ds.DocumentSourceID) 
              WHERE casc.ClaimAspectID  = @ClaimAspectID
                AND casc.PrimaryFlag    = 1
                AND dt.EstimateTypeFlag = 1
                AND d.EnabledFlag       = 1

                    
                             
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error inserting into @tmpDocument', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @debug = 1
        BEGIN
            PRINT ''
            PRINT 'Estimates for this claim aspect:' + convert(varchar,@ClaimAspectID)
            SELECT * FROM @tmpDocument
        END

        
        -- Now compile the information on the claim fact.  Add dimension records if missing.
    
        -- Service Channel

        IF @ServiceChannelCD IS NOT NULL
        BEGIN        
            SELECT  @ServiceChannelID = ServiceChannelID
              FROM  dbo.utb_dtwh_dim_service_channel WITH (NOLOCK)
              WHERE ServiceChannelCD = @ServiceChannelCD
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @ServiceChannelID IS NULL
            BEGIN
                -- Doesn't exist, add the dimension record
            
                INSERT INTO dbo.utb_dtwh_dim_service_channel (ServiceChannelCD, ServiceChannelDescription)
                  SELECT  Code,
                          Name
                    FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                    WHERE Code = @ServiceChannelCD
        
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_service_channel', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @ServiceChannelID = SCOPE_IDENTITY()
            END
        END

        
        -- Closing (Current) Assigment Type
       
        IF (@ClosingAssignmentTypeID IS NOT NULL) AND (@ClosingAssignmentTypeID <> 0)
        BEGIN
            SELECT  @AssignmentTypeClosingID = AssignmentTypeID
              FROM  dbo.utb_dtwh_dim_assignment_type WITH (NOLOCK)
              WHERE AssignmentTypeID = @ClosingAssignmentTypeID
                  
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @AssignmentTypeClosingID IS NULL
            BEGIN
                -- Doesn't exist, add the dimension record

                IF (SELECT  dsc.ServiceChannelID
                      FROM  dbo.utb_assignment_type at WITH (NOLOCK) 
                      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc WITH (NOLOCK) ON (at.ServiceChannelDefaultCD = dsc.ServiceChannelCD)
                      WHERE at.AssignmentTypeID = @ClosingAssignmentTypeID) IS NULL
                BEGIN
                    IF @@ERROR <> 0
                    BEGIN
                        RAISERROR('%s: ServiceChannelID IS NULL..ClaimAspectID = %u', 16, 1, @ProcName, @ClaimAspectID)
                        ROLLBACK TRANSACTION
                        RETURN
                    END
                END
                
                INSERT INTO dbo.utb_dtwh_dim_assignment_type (AssignmentTypeID, AssignmentTypeDescription, ServiceChannelID)
                    SELECT  @ClosingAssignmentTypeID,
                            at.Name,
                            dsc.ServiceChannelID
                      FROM  dbo.utb_assignment_type at WITH (NOLOCK) 
                      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc WITH (NOLOCK) ON (at.ServiceChannelDefaultCD = dsc.ServiceChannelCD)
                      WHERE at.AssignmentTypeID = @ClosingAssignmentTypeID
                              
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_assignment_type', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @AssignmentTypeClosingID = SCOPE_IDENTITY()
            END
        END
                                        
                
        -- Initial Assigment Type
       
        IF (@InitialAssignmentTypeID IS NOT NULL) AND (@InitialAssignmentTypeID <> 0)
        BEGIN
            SELECT  @AssignmentTypeID = AssignmentTypeID
              FROM  dbo.utb_dtwh_dim_assignment_type WITH (NOLOCK)
              WHERE AssignmentTypeID = @InitialAssignmentTypeID
                  
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @AssignmentTypeID IS NULL
            BEGIN
                -- Doesn't exist, add the dimension record

                IF (SELECT  dsc.ServiceChannelID
                      FROM  dbo.utb_assignment_type at WITH (NOLOCK) 
                      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc WITH (NOLOCK) ON (at.ServiceChannelDefaultCD = dsc.ServiceChannelCD)
                      WHERE at.AssignmentTypeID = @InitialAssignmentTypeID) IS NULL
                BEGIN
                    IF @@ERROR <> 0
                    BEGIN
                        RAISERROR('%s: ServiceChannelID IS NULL..ClaimAspectID = %u', 16, 1, @ProcName, @ClaimAspectID)
                        ROLLBACK TRANSACTION
                        RETURN
                    END
                END
                
                INSERT INTO dbo.utb_dtwh_dim_assignment_type (AssignmentTypeID, AssignmentTypeDescription, ServiceChannelID)
                    SELECT  @InitialAssignmentTypeID,
                            at.Name,
                            dsc.ServiceChannelID
                      FROM  dbo.utb_assignment_type at  WITH (NOLOCK)
                      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc WITH (NOLOCK) ON (at.ServiceChannelDefaultCD = dsc.ServiceChannelCD)
                      WHERE at.AssignmentTypeID = @InitialAssignmentTypeID
                              
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_assignment_type', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @AssignmentTypeID = SCOPE_IDENTITY()
            END
        END
                                        
       
        -- Claim Location (State)

        SELECT  @ClaimLocationID = StateID
          FROM  dbo.utb_dtwh_dim_state WITH (NOLOCK)
          WHERE StateCode = @LossState
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @ClaimLocationID IS NULL
        BEGIN
            -- Doesn't exist, this is an error
            
            RAISERROR('%s: Loss State not found in utb_dtwh_dim_state.  Rollup Failed.', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        -- Coverage Type
       
        SELECT  @CoverageTypeID = CoverageTypeID
          FROM  dbo.utb_dtwh_dim_coverage_type dct WITH (NOLOCK)
          INNER JOIN dbo.utb_client_coverage_type ct WITH (NOLOCK) ON (dct.CoverageTypeCD = ct.CoverageProfileCD AND dct.CoverageTypeDescription = ct.Name)          
          WHERE ct.ClientCoverageTypeID = @ClientCoverageTypeID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @CoverageTypeID IS NULL
        BEGIN
            -- Doesn't exist, add the dimension record
            
            INSERT INTO dbo.utb_dtwh_dim_coverage_type (CoverageTypeCD, CoverageTypeDescription, PartyCD, PartyDescription)
              SELECT  CoverageProfileCD,
                      Name,
                      CASE CoverageProfileCD
                        WHEN 'LIAB' THEN '3'
                        ELSE '1'
                      END,
                      CASE CoverageProfileCD
                        WHEN 'LIAB' THEN 'Third Party'
                        ELSE 'First Party'
                      END                    
                FROM  dbo.utb_client_coverage_type WITH (NOLOCK)
                WHERE ClientCoverageTypeID = @ClientCoverageTypeID

            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_coverage_type', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        
            SELECT @CoverageTypeID = SCOPE_IDENTITY()
        END

                
        -- Customer
        
        SELECT  @CustomerID = CustomerID
          FROM  dbo.utb_dtwh_dim_customer WITH (NOLOCK)
          WHERE UserID = @CarrierRepUserID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @CustomerID IS NULL
        BEGIN
            -- Doesn't exist, add the dimension record
            
            INSERT INTO dbo.utb_dtwh_dim_customer (UserID, RepNameFirst, RepNameLast, ClientUserId, OfficeID, OfficeClientId, OfficeName, InsuranceCompanyID, InsuranceCompanyName)
              SELECT  u.UserID,
                      u.NameFirst,
                      u.NameLast,
                      u.ClientUserId,
                      u.OfficeID,
                      o.ClientOfficeID,
                      o.Name,
                      o.InsuranceCompanyID,
                      i.Name
                FROM  dbo.utb_user u WITH (NOLOCK)
                LEFT JOIN dbo.utb_office o WITH (NOLOCK) ON (u.OfficeID = o.OfficeID)
                LEFT JOIN dbo.utb_insurance i WITH (NOLOCK) ON (o.InsuranceCompanyID = i.InsuranceCompanyID)
                WHERE UserID = @CarrierRepUserID
        
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_customer', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @CustomerID = SCOPE_IDENTITY()
        END        
        ELSE
        BEGIN
            -- Update the dimension record
            UPDATE dbo.utb_dtwh_dim_customer
            SET RepNameFirst = u.NameFirst,
                RepNameLast = u.NameLast,
                ClientUserID = u.ClientUserId,
                OfficeID = u.OfficeID,
                OfficeClientID = o.ClientOfficeID,
                OfficeName = o.Name,
                InsuranceCompanyID = o.InsuranceCompanyID,
                InsuranceCompanyName = i.Name
             FROM  (SELECT UserID as CarrierRepID, NameFirst, NameLast, ClientUserID, OfficeID 
                    FROM utb_user WITH (NOLOCK) 
                    WHERE userid = @CarrierRepUserID) as u
             LEFT JOIN dbo.utb_office o WITH (NOLOCK) ON (u.OfficeID = o.OfficeID)
             LEFT JOIN dbo.utb_insurance i WITH (NOLOCK) ON (o.InsuranceCompanyID = i.InsuranceCompanyID)
             WHERE UserID = u.CarrierRepID

        
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error updating utb_dtwh_dim_customer', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END

                
        -- Disposition Type
       
        IF @DispositionTypeCD IS NOT NULL
        BEGIN        
            SELECT  @DispositionTypeID = DispositionTypeID
              FROM  dbo.utb_dtwh_dim_disposition_type WITH (NOLOCK)
              WHERE DispositionTypeCD = @DispositionTypeCD
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @DispositionTypeID IS NULL
            BEGIN
                -- Doesn't exist, add the dimension record
            
                INSERT INTO dbo.utb_dtwh_dim_disposition_type (DispositionTypeCD, DispositionTypeDescription)
                  SELECT  Code,
                          Name
                    FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'DispositionTypeCD')
                    WHERE Code = @DispositionTypeCD

                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_disposition_type', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
        
                SELECT @DispositionTypeID = SCOPE_IDENTITY()
            END
        END
        
        
        -- Lynx Handlers (Owner, Analyst, Support)
     
        SELECT  @LynxHandlerOwnerID = LynxHandlerID
          FROM  dbo.utb_dtwh_dim_lynx_handler WITH (NOLOCK)
          WHERE UserID = @OwnerUserID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @LynxHandlerOwnerID IS NULL
        BEGIN
            -- Doesn't exist, add the dimension record
            
            INSERT INTO dbo.utb_dtwh_dim_lynx_handler (UserID, UserNameFirst, UserNameLast)
              SELECT  UserID,
                      NameFirst,
                      NameLast
                FROM  dbo.utb_user  WITH (NOLOCK)
                WHERE UserID = @OwnerUserID
        
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_lynx_handler', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @LynxHandlerOwnerID = SCOPE_IDENTITY()
        END
        
        
        SELECT  @LynxHandlerAnalystID = LynxHandlerID
          FROM  dbo.utb_dtwh_dim_lynx_handler WITH (NOLOCK)
          WHERE UserID = @AnalystUserID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @LynxHandlerAnalystID IS NULL
        BEGIN
            -- Doesn't exist, add the dimension record
            
            INSERT INTO dbo.utb_dtwh_dim_lynx_handler (UserID, UserNameFirst, UserNameLast)
              SELECT  UserID,
                      NameFirst,
                      NameLast
                FROM  dbo.utb_user  WITH (NOLOCK)
                WHERE UserID = @AnalystUserID
        
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_lynx_handler', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @LynxHandlerAnalystID = SCOPE_IDENTITY()
        END


        SELECT  @LynxHandlerSupportID = LynxHandlerID
          FROM  dbo.utb_dtwh_dim_lynx_handler WITH (NOLOCK)
          WHERE UserID = @SupportUserID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @LynxHandlerSupportID IS NULL
        BEGIN
            -- Doesn't exist, add the dimension record
            
            INSERT INTO dbo.utb_dtwh_dim_lynx_handler (UserID, UserNameFirst, UserNameLast)
              SELECT  UserID,
                      NameFirst,
                      NameLast
                FROM  dbo.utb_user  WITH (NOLOCK)
                WHERE UserID = @SupportUserID
        
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_lynx_handler', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @LynxHandlerSupportID = SCOPE_IDENTITY()
        END


        -- Initialize some work variables
        
        SELECT  @AssignmentIDWork = NULL,
                @AssignmentDate = NULL,
                @CFProgramFlag = NULL,
                @DocumentSourceNameClaim = NULL,
                @ProgramCD = NULL,
                @ShopLocationID = NULL,
                @StandardDocumentSourceID = NULL,
                @VanSourceFlagClaim = NULL,
                @RepairLocationIDClaim = NULL

        IF (EXISTS(SELECT * FROM @tmpAssignment WHERE ShopLocationID IS NOT NULL)) AND
           (@ClosingServiceChannelCD NOT IN ('DA', 'DR')) 
        BEGIN
            -- We're interested in just the last assignment sent.  Get the assignment id and save.  It will be used later 
            -- when determing estimate time as well.  We'll also grab other information we'll need for the fact and 
            -- repair location dimension record while we're here.  If the assignment was cancelled, we donWe have to check on the shop's communication mechanism 
            -- with us, default it to "Fax" if no communications is set up.

            SELECT TOP 1 @AssignmentIDWork  = tmp.AssignmentID,                        
                         @ShopLocationID    = tmp.ShopLocationID,
                         @AssignmentDate    = tmp.AssignmentDate,
                         @ProgramCD         = tmp.ProgramTypeCD,
                         @CFProgramFlag     = tmp.CertifiedFirstFlag,
                         @DocumentSourceNameClaim = IsNull(cm.RoutingCD, 'Fax'),
                         @VanSourceFlagClaim = CASE 
                                                 WHEN cm.RoutingCD IS NOT NULL THEN 1
                                                 ELSE 0
                                               END 
               FROM @tmpAssignment tmp
                    LEFT JOIN dbo.utb_shop_location sl WITH (NOLOCK) ON (tmp.ShopLocationID = sl.ShopLocationID)
                    LEFT JOIN dbo.utb_communication_method cm WITH (NOLOCK) ON (sl.PreferredCommunicationMethodID = cm.CommunicationMethodID)
			  WHERE tmp.ShopLocationID IS NOT NULL  -- 23-MAY-2012 MAH - Added for RRP because there are 2 assignments and the 1st one has the Shop.
              ORDER BY AssignmentID DESC
                           
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Verify the dimension record for the communication mechanism used by the chosen shop location exists
                                                  
            SELECT @StandardDocumentSourceID = NULL

            SELECT  @StandardDocumentSourceID = DocumentSourceID
              FROM  dbo.utb_dtwh_dim_document_source WITH (NOLOCK)
              WHERE @DocumentSourceNameClaim IS NOT NULL AND DocumentSourceName = @DocumentSourceNameClaim
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @StandardDocumentSourceID IS NULL AND @DocumentSourceNameClaim IS NOT NULL
            BEGIN
                -- Doesn't exist, add the document source dimension record
        
                INSERT INTO dbo.utb_dtwh_dim_document_source (DocumentSourceName, ElectronicSourceFlag)
                  VALUES (@DocumentSourceNameClaim,
                          ISNULL(@VanSourceFlagClaim, 0))
    
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into utb_dtwh_dim_document_source', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @StandardDocumentSourceID = SCOPE_IDENTITY()
            END


            -- Get Repair Location information for the claim aspect
        
            SELECT  @RepairLocationIDClaim = RepairLocationID
              FROM  dbo.utb_dtwh_dim_repair_location WITH (NOLOCK)
              WHERE ShopLocationID = @ShopLocationID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

		--cmaloney 12/5/11-----------------------------------------------------------------------------------------
		--RRP is the first service channel with multiple assignments.  
		--i am adding this code to clean up missing shop data for RRP.  I chose to do this because the code above
		--scares the $hit out of me and i didn't want to mess up any existing service channels. 
-- MAH -- Comment Start 23-may-2012
--		declare @CreateShop bit
--		set @CreateShop = 0
--
--		if (@AssignmentTypeID = 16) and (@RepairLocationIDClaim is null)
--		begin
--			if exists (select a.* from utb_claim_aspect ca
--						inner join utb_claim_aspect_service_channel casc on ca.ClaimAspectId = casc.ClaimAspectId
--																		and ca.ClaimAspectId = @ClaimAspectID
--						inner join utb_assignment a on a.ClaimAspectServiceChannelId = casc.ClaimAspectServiceChannelId
--						inner join utb_shop_location rl on a.ShopLocationId = rl.ShopLocationId
--						where a.AssignmentSequenceNumber = 1
--							and a.CancellationDate is null
--							and a.ShopLocationId is not null)
--
--			begin
--
--			select	@RepairLocationIDClaim = rl.RepairLocationId from utb_claim_aspect ca
--						inner join utb_claim_aspect_service_channel casc on ca.ClaimAspectId = casc.ClaimAspectId
--																		and ca.ClaimAspectId = @ClaimAspectID
--						inner join utb_assignment a on a.ClaimAspectServiceChannelId = casc.ClaimAspectServiceChannelId
--						inner join utb_shop_location rl on a.ShopLocationId = rl.ShopLocationId
--						where a.AssignmentSequenceNumber = 1
--							and a.CancellationDate is null
--							and a.ShopLocationId is not null
--			
--			set @CreateShop = 1
--			end
--		end
--            IF ((@RepairLocationIDClaim IS NULL) or (@CreateShop = 1))
-- MAH -- Comment End 23-may-2012

            IF (@RepairLocationIDClaim IS NULL)
            BEGIN
                -- Doesn't exist, add the dimension record
            
                INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode) -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                  SELECT  sl.ShopLocationID,
                          @StandardDocumentSourceID,
                          (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                          sl.ReferralFlag,  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                          sl.CertifiedFirstFlag,
                          IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                          IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                          sl.ProgramFlag,
                          sl.PPGCTSCustomerFlag,
                          LTrim(RTrim(sl.Name)),
                          ep.Name,
                          sl.AddressZip 
                    FROM  dbo.utb_shop_location sl WITH (NOLOCK)
                    LEFT JOIN dbo.utb_estimate_package ep WITH (NOLOCK) ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                    LEFT JOIN dbo.utb_zip_code zc WITH (NOLOCK) ON (sl.AddressZip = zc.Zip)
                    WHERE sl.ShopLocationID = @ShopLocationID  -- MAH 24-may-2012 - No sense in doing the sub-select below when we already have the shop.
--                    WHERE sl.ShopLocationID = (SELECT TOP 1 ShopLocationID
--                                                 FROM @tmpAssignment
--                                                 ORDER BY AssignmentID DESC)
        
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting shop location id %u into utb_dtwh_dim_repair_location for claim aspect id %u', 16, 1, @ProcName, @RepairLocationIDClaim, @ClaimAspectID)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            
                SELECT @RepairLocationIDClaim = SCOPE_IDENTITY()
            END
            ELSE
            BEGIN
                -- We need to update the repair facilities information
                
                UPDATE  dbo.utb_dtwh_dim_repair_location
                  SET   StandardDocumentSourceID = @StandardDocumentSourceID,
                        StateID = (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                        RRPProgramFlag = sl.ReferralFlag,   -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                        CFProgramFlag = sl.CertifiedFirstFlag,
                        City = IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                        County = IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                        LYNXSelectProgramFlag = sl.ProgramFlag,
                        PPGPaintBuyerFlag = sl.PPGCTSCustomerFlag,
                        ShopLocationName = sl.Name,
                        StandardEstimatePackage = ep.Name,
                        ZipCode = sl.AddressZip 
                  FROM  dbo.utb_dtwh_dim_repair_location drl WITH (NOLOCK)
                  LEFT JOIN dbo.utb_shop_location sl WITH (NOLOCK) ON (drl.ShopLocationID = sl.ShopLocationID)
                  LEFT JOIN dbo.utb_estimate_package ep WITH (NOLOCK) ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                  LEFT JOIN dbo.utb_zip_code zc WITH (NOLOCK) ON (sl.AddressZip = zc.Zip)
                  WHERE drl.RepairLocationID = @RepairLocationIDClaim

                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error updating shop location id %u in utb_dtwh_dim_repair_location for claim aspect id %u', 16, 1, @ProcName, @RepairLocationIDClaim, @ClaimAspectID)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END                                   
        END
        ELSE
        BEGIN
            -- No assignments found, we still need to set the @ProgramCD, we'll set it to "Non"
            
            SELECT @ProgramCD = 'NON'
        END
        
         
        -- Time ID New
      
        SELECT  @TimeIDNew = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @VehicleCreatedDate), Convert(datetime, Convert(varchar(15), @VehicleCreatedDate, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Time ID Closed

        SELECT  @TimeIDClosed = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @OriginalCompleteDate), Convert(datetime, Convert(varchar(15), @OriginalCompleteDate, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Time ID Cancelled

        SELECT  @TimeIDCancelled = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue =  (SELECT  Max(DateAdd(hour, DatePart(hour, hl.CompletedDate), Convert(datetime, Convert(varchar(15), hl.CompletedDate, 101))))
                                FROM  dbo.utb_history_log hl WITH (NOLOCK)
                                LEFT JOIN dbo.utb_event e WITH (NOLOCK) ON (hl.EventID = e.EventID)
                                WHERE hl.LynxID = @LynxID
                                  AND ((hl.ClaimAspectTypeID = @ClaimAspectTypeID AND hl.ClaimAspectNumber = @ClaimAspectNumber AND e.Name = 'Vehicle Cancelled') OR
                                       (hl.ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND e.Name = 'Claim Cancelled')) )

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

          
        -- Time ID Voided

        SELECT  @TimeIDVoided = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue =  (SELECT  Max(DateAdd(hour, DatePart(hour, hl.CompletedDate), Convert(datetime, Convert(varchar(15), hl.CompletedDate, 101))))
                                FROM  dbo.utb_history_log hl WITH (NOLOCK)
                                LEFT JOIN dbo.utb_event e WITH (NOLOCK) ON (hl.EventID = e.EventID)
                                WHERE hl.LynxID = @LynxID
                                  AND ((hl.ClaimAspectTypeID = @ClaimAspectTypeID AND hl.ClaimAspectNumber = @ClaimAspectNumber AND e.Name = 'Vehicle Voided') OR
                                       (hl.ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND e.Name = 'Claim Voided')) )
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Time ID Reopened, Populate a work variable seperately because we need it to evaluate reclosed

        SELECT @DateReopenedWork = NULL

        SELECT  @DateReopenedWork = Max(hl.CompletedDate)
          FROM  dbo.utb_history_log hl WITH (NOLOCK)
          LEFT JOIN dbo.utb_event e WITH (NOLOCK) ON (hl.EventID = e.EventID)
          WHERE hl.LynxID = @LynxID
            AND hl.ClaimAspectTypeID = @ClaimAspectTypeID
            AND hl.ClaimAspectNumber = @ClaimAspectNumber
            AND e.Name = 'Vehicle Reopened'
                         
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        SELECT  @TimeIDReopened = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @DateReopenedWork), Convert(datetime, Convert(varchar(15), @DateReopenedWork, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

          
        -- Time ID Reclosed

        SELECT @DateReclosedWork = NULL

        IF @TimeIDReopened IS NOT NULL
        BEGIN
            -- Recloses are only valid if there is a reopen.  We need to make this check because the system does not
            -- inherantly distinguish between a close and a reclose.  Moreover, if a claim is cancelled, it looks like a 
            -- standard close from the exposure's perspective.
        
            SELECT  @DateReclosedWork = Max(hl.CompletedDate)
              FROM  dbo.utb_history_log hl WITH (NOLOCK)
              LEFT JOIN dbo.utb_event e WITH (NOLOCK) ON (hl.EventID = e.EventID)
              WHERE hl.LynxID = @LynxID
                AND hl.ClaimAspectTypeID = @ClaimAspectTypeID
                AND hl.ClaimAspectNumber = @ClaimAspectNumber
                AND e.Name = 'Vehicle Closed'
                             
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF (Convert(varchar(30), @DateReclosedWork) = Convert(varchar(30), @OriginalCompleteDate)) OR
               (@DateReopenedWork > @DateReclosedWork)  
            BEGIN
                -- This means that the claim was never reopened after a regular close.  It's the original close, or the
                -- claim has been reopened and not yet closed
            
                SELECT @TimeIDReclosed = NULL, @DateReclosedWork = NULL
            END
            ELSE
            BEGIN
                SELECT  @TimeIDReclosed = TimeID
                  FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
                  WHERE DateValue = DateAdd(hour, DatePart(hour, @DateReclosedWork), Convert(datetime, Convert(varchar(15), @DateReclosedWork, 101)))
                 
                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
        END
        
                  
        -- Time ID Assignment Sent
      
        SELECT @DateAssignSentWork = NULL
        
        IF (SELECT Count(*) FROM @tmpAssignment WHERE AssignmentDate IS NOT NULL AND ShopLocationID IS NOT NULL) = 0
        BEGIN
            -- No assignments, check for estimates
            
            IF (SELECT Count(*) FROM @tmpDocument) = 0
            BEGIN
                -- No estimates either, set Assignment to NULL
                
                SELECT @TimeIDAssignSent = NULL, @DateAssignSentWork = NULL
            END
            ELSE
            BEGIN
                -- There's at least 1 estimate.  This is probably a desk review.  For report purposes, set Assignment date
                -- to when the claim was received by Lynx
                
                SELECT @TimeIDAssignSent = @TimeIDNew, @DateAssignSentWork = @VehicleCreatedDate
            END
        END
        ELSE
        BEGIN
            -- There is at least one assignment, look to see if all the assignments were sent to the same shop.

            IF (SELECT COUNT(DISTINCT ShopLocationID) FROM @tmpAssignment) = 1
            BEGIN
                -- All the assignments were sent to the same shop. It's probable that we cancelled and reassigned
                -- because of some internal snafu. Go ahead and use the earliest assignment date available.

                SELECT @DateAssignSentWork = Min(AssignmentDate)
                  FROM @tmpAssignment

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
                
        
                IF @DateAssignSentWork IS NULL
                BEGIN
                    -- The assignment date is missing; this was the case on some older claims, we'll use the selection date instead
                    
                    SELECT @DateAssignSentWork = Min(SelectionDate)
                      FROM @tmpAssignment

                    IF @@ERROR <> 0
                    BEGIN
                       -- SQL Server Error
    
                        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                        ROLLBACK TRANSACTION
                        RETURN
                    END
                END
            END
            ELSE
            BEGIN
                -- The assignments are to different shops, set the date to the Assignment date of the most recent assignment
                -- If the assignment date is missing, as was the case on some older claims, we'll use the selection date instead
                
                SELECT TOP 1 @DateAssignSentWork = IsNull(AssignmentDate, SelectionDate)
                  FROM @tmpAssignment
                  ORDER BY AssignmentID DESC

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
            
            SELECT  @TimeIDAssignSent = TimeID
              FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
              WHERE DateValue = DateAdd(hour, DatePart(hour, @DateAssignSentWork), Convert(datetime, Convert(varchar(15), @DateAssignSentWork, 101)))

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END


        -- Time ID Estimate

        SELECT @DateEstWork = NULL
        
        IF (SELECT Count(*) FROM @tmpDocument) > 0
        BEGIN
            -- Look to see whether all assignment made were made to the same shop (or no shop)
            
            IF (SELECT COUNT(DISTINCT ShopLocationID) FROM @tmpAssignment WHERE ShopLocationID IS NOT NULL) <= 1
            BEGIN
                -- Assignment all made to the same shop (or in some cases, no shop).  Set the estimate date by taking
                -- the TOP 1 of the estimates sorted into the following order:
                --     AssignmentID DESC, VanSourceFlag DESC, SequenceNumber ASC, CreatedDate DESC
                
                SELECT Top 1 @DateEstWork = CreatedDate
                  FROM  @tmpDocument
                  ORDER BY SequenceNumber, CreatedDate

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
            ELSE
            BEGIN
                -- Assignments were made to multiple shops.  In this case we're only interested in the estimates from
                -- the latest assignment.  Ignore the others.  The last assignment id is stored as @AssignmentIDWork ans was 
                -- retrieved earlier
                
                -- Get the first estimate date for this assignment

                SELECT Top 1 @DateEstWork = CreatedDate
                  FROM  @tmpDocument
                  WHERE AssignmentID = @AssignmentIDWork
                  ORDER BY SequenceNumber, CreatedDate

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END

            SELECT  @TimeIDEstimate = TimeID
              FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
              WHERE DateValue = DateAdd(hour, DatePart(hour, @DateEstWork), Convert(datetime, Convert(varchar(15), @DateEstWork, 101)))

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
        
                
        -- Time ID Assignment Downloaded
		-------------------------------------------
		-- 23Apr2012- TVD - Changed this to select only one row in
		-- the where clause
		-------------------------------------------
        SELECT  @TimeIDAssignDownload = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue =  (SELECT  TOP 1
									DateAdd(hour, 
                                    DatePart(hour, cas.StatusStartDate), 
                                    Convert(datetime, Convert(varchar(15), cas.StatusStartDate, 101)))
                              FROM  dbo.utb_claim_aspect_status cas WITH (NOLOCK)
                                    INNER JOIN dbo.utb_claim_aspect_service_channel casc WITH (NOLOCK) 
										ON (cas.ClaimAspectID = casc.ClaimAspectID 
                                        AND  cas.ServiceChannelCD = casc.ServiceChannelCD)
                                    LEFT JOIN dbo.utb_status s WITH (NOLOCK) 
                                        ON (cas.StatusID = s.StatusID)                                
                               WHERE  cas.ClaimAspectID = @ClaimAspectID
                                 AND  cas.StatusTypeCD = 'ELC'
                                 AND  casc.PrimaryFlag = 1
                                 AND  s.Name IN ('Sent', 'Received By Appraiser')
                               ORDER
                                 BY  cas.SysLastUpdatedDate ASC)

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF (@TimeIDAssignDownload IS NULL) AND (@TimeIDEstimate IS NOT NULL)
        BEGIN
            -- We have an estimate but for whatever reason we don't have a sent status for the assignment, set the download
            -- date the same as the assignment sent date
            
            SELECT @TimeIDAssignDownload = @TimeIDAssignSent
        END

        
        -- Code for SWR 201448
        -- Inspection  

        SELECT  @TimeIDInspection = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @InspectionDate), Convert(datetime, Convert(varchar(15), @InspectionDate, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        -- End Code for SWR 201448

        
        -- Time ID Repair Started  

        SELECT  @TimeIDRepairStarted = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @RepairStartDate), Convert(datetime, Convert(varchar(15), @RepairStartDate, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        -- Time ID Repair Complete  

        SELECT  @TimeIDRepairComplete = TimeID
          FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
          WHERE DateValue = DateAdd(hour, DatePart(hour, @RepairEndDate), Convert(datetime, Convert(varchar(15), @RepairEndDate, 101)))

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Compute cycles times

        SELECT @CycleTimeAssignToEstBusDay      = CASE
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@DateAssignSentWork, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@DateAssignSentWork, @DateEstWork)
                                                    ELSE NULL
                                                  END,
               @CycleTimeAssignToEstCalDay      = CASE
                                                    WHEN DATEDIFF(day, @DateAssignSentWork, @DateEstWork) >= 0 THEN DATEDIFF(day, @DateAssignSentWork, @DateEstWork)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeAssignToEstHrs         = CASE
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@DateAssignSentWork, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@DateAssignSentWork, @DateEstWork)
                                                    ELSE NULL
                                                  END,
               @CycleTimeEstToCloseBusDay       = CASE 
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@DateEstWork, @OriginalCompleteDate) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@DateEstWork, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,
               @CycleTimeEstToCloseCalDay       = CASE
                                                    WHEN DATEDIFF(day, @DateEstWork, @OriginalCompleteDate) >= 0 THEN DATEDIFF(day, @DateEstWork, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeEstToCloseHrs          = CASE 
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@DateEstWork, @OriginalCompleteDate) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@DateEstWork, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,

               @CycleTimeInspectionToEstBusDay  = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @InspectionDate) < 0  THEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @DateEstWork)    -- If Inspection occurs before assignment to LYNX, start cycle time at assignment instead
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@InspectionDate, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@InspectionDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,
               @CycleTimeInspectionToEstCalDay  = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @InspectionDate) < 0  THEN DATEDIFF(day, @VehicleCreatedDate, @DateEstWork)    -- If Inspection occurs before assignment to LYNX, start cycle time at assignment instead
                                                    WHEN DATEDIFF(day, @InspectionDate, @DateEstWork) >= 0 THEN DATEDIFF(day, @InspectionDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeInspectionToEstHrs     = CASE
                                                    WHEN DATEDIFF(hour, @VehicleCreatedDate, @InspectionDate) < 0  THEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @DateEstWork)    -- If Inspection occurs before assignment to LYNX, start cycle time at assignment instead
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@InspectionDate, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@InspectionDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,

               @CycleTimeNewToCloseBusDay       = CASE 
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @OriginalCompleteDate) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,
               @CycleTimeNewToCloseCalDay       = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @OriginalCompleteDate) >= 0 THEN DATEDIFF(day, @VehicleCreatedDate, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeNewToCloseHrs          = CASE 
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @OriginalCompleteDate) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @OriginalCompleteDate)
                                                    ELSE NULL
                                                  END,
               @CycleTimeNewToEstBusDay         = CASE
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,
               @CycleTimeNewToEstCalDay         = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @DateEstWork) >= 0 THEN DATEDIFF(day, @VehicleCreatedDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeNewToEstHrs            = CASE
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @DateEstWork) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @DateEstWork)
                                                    ELSE NULL
                                                  END,

               @CycleTimeNewToInspectionBusDay  = CASE
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @InspectionDate) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @InspectionDate)
                                                    ELSE NULL
                                                  END,
               @CycleTimeNewToInspectionCalDay  = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @InspectionDate) >= 0 THEN DATEDIFF(day, @VehicleCreatedDate, @InspectionDate)
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @InspectionDate) < 0 THEN 0
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeNewToInspectionHrs     = CASE
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @InspectionDate) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @InspectionDate)
                                                    ELSE NULL
                                                  END,

               @CycleTimeNewToRecloseBusDay     = CASE
                                                    WHEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @DateReclosedWork) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@VehicleCreatedDate, @DateReclosedWork)
                                                    ELSE NULL
                                                  END,
               @CycleTimeNewToRecloseCalDay     = CASE
                                                    WHEN DATEDIFF(day, @VehicleCreatedDate, @DateReclosedWork) >= 0 THEN DATEDIFF(day, @VehicleCreatedDate, @DateReclosedWork)
                                                    ELSE NULL
                                                  END,                                           
               @CycleTimeNewToRecloseHrs        = CASE
                                                    WHEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @DateReclosedWork) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@VehicleCreatedDate, @DateReclosedWork)
                                                    ELSE NULL
                                                  END
                                                  
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- Now Repair cycle times... we do these seperately because these are entered by the user.  As such, they could
        -- be completely wacky, so we don't want to take the chance of blowing out the process because of erroneous user input
        
        IF @TimeIDRepairStarted IS NOT NULL AND @TimeIDRepairComplete IS NOT NULL
        BEGIN
            -- We should be able to compute these cycle times safely 
                                                     
            SELECT @CycleTimeRepairStartToEndBusDay = CASE 
                                                        WHEN dbo.ufnUtilityBusinessDaysDiff(@RepairStartDate, @RepairEndDate) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(@RepairStartDate, @RepairEndDate)
                                                        ELSE NULL
                                                      END,                                              
                   @CycleTimeRepairStartToEndCalDay = CASE
                                                        WHEN DATEDIFF(day, @RepairStartDate, @RepairEndDate) >= 0 THEN DATEDIFF(day, @RepairStartDate, @RepairEndDate)
                                                        ELSE NULL
                                                      END,
                   @CycleTimeRepairStartToEndHrs    = CASE 
                                                        WHEN dbo.ufnUtilityBusinessHoursDiff(@RepairStartDate, @RepairEndDate) >= 0 THEN dbo.ufnUtilityBusinessHoursDiff(@RepairStartDate, @RepairEndDate)
                                                        ELSE NULL
                                                      END
        END
            
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        -- Claim Status
        
        SELECT @ClaimStatusCD = CASE
                                  WHEN s.Name LIKE '%Open%' THEN 'OPEN'
                                  WHEN s.Name LIKE '%Closed%' THEN 'CLSD'
                                  WHEN s.Name LIKE '%Cancelled%' THEN 'CANC'
                                  WHEN s.Name LIKE '%Voided%' THEN 'VOID'
                                END
          FROM  dbo.utb_claim_aspect_status cas WITH (NOLOCK)
          LEFT JOIN utb_claim_aspect ca WITH (NOLOCK) ON (cas.ClaimAspectID = ca.ClaimAspectID)
          LEFT JOIN dbo.utb_status s WITH (NOLOCK) ON (cas.StatusID = s.StatusID)
          WHERE ca.LynxID = @LynxID
            AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
            
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Does the claim have photos?  We will look at the attached documents to determine this
        IF EXISTS(SELECT cascd.DocumentID 
                    FROM dbo.utb_claim_aspect_service_channel casc
                    INNER JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)
                    INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                    WHERE casc.ClaimAspectID = @ClaimAspectID
                      AND d.DocumentTypeID = @DocumentTypeIDPhotograph)
        BEGIN
            -- At least one photograph is attached to this vehicle
            SET @PhotosAttachedFlag = 1
        END
        ELSE
        BEGIN
            SET @PhotosAttachedFlag = 0
        END        
        
                    
        -- Get estimate totals
        
        -- Compile a list together of all entered estimate totals for all documents.  These will be sequenced so that
        -- estimate amounts can be determined.

        DELETE FROM @tmpEstimateTotals

        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: SQL Server Error deleting from @tmpEstimateTotals', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
                
        DECLARE csrEstimateAmounts CURSOR FAST_FORWARD FOR
          SELECT  DocumentID,
                  SequenceNumber,
                  EstimateTypeCD,
                  OriginalLaborRateBody,
                  OriginalLaborRateFrame,
                  OriginalLaborRateMech,
                  OriginalLaborRateRefinish,                  
                  OriginalLaborAmountRepair,
                  OriginalLaborAmountReplace,
                  OriginalLaborAmountTotal,
                  OriginalPartsAmountAFMK,
                  OriginalPartsAmountLKQ,
                  OriginalPartsAmountOEM,
                  OriginalPartsAmountReman,
                  OriginalPartsAmountTotal,
                  OriginalPartsDiscount,
                  OriginalRepairTotal,
                  OriginalBetterment,
                  OriginalRepairTotalwoBetterment,
                  OriginalDeductible,
                  OriginalOtherAdjustmentTotal,
                  OriginalNetTotal,
                  AgreedRepairTotal,
                  AgreedBetterment,
                  AgreedRepairTotalwoBetterment,
                  AgreedDeductible,
                  AgreedOtherAdjustmentTotal,
                  AgreedNetTotal,
                  CASE
                    WHEN AgreedPriceMetCD = 'Y' THEN 1
                    ELSE 0
                  END,
                  CreatedDate,
                  ApprovedFlag,
                  FinalBillEstimateFlag
            FROM  @tmpDocument
            WHERE DuplicateFlag = 0
              AND (AgreedRepairTotal IS NOT NULL AND AgreedRepairTotal > 0) OR (OriginalRepairTotal IS NOT NULL AND OriginalRepairTotal > 0)
            ORDER BY SequenceNumber, EstimateTypeCD DESC, VANSourceFlag DESC, CreatedDate
 
       
        OPEN csrEstimateAmounts

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        SELECT  @DocumentID = NULL,
                @SequenceNumber = NULL,
                @EstimateTypeCD = NULL,
                @OriginalLaborRateBody = NULL,
                @OriginalLaborRateFrame = NULL,
                @OriginalLaborRateMech = NULL,
                @OriginalLaborRateRefinish = NULL,
                @OriginalLaborAmountRepair = NULL,
                @OriginalLaborAmountReplace = NULL,
                @OriginalLaborAmountTotal = NULL,
                @OriginalPartsAmountAFMK = NULL,
                @OriginalPartsAmountLKQ = NULL,
                @OriginalPartsAmountOEM = NULL,
                @OriginalPartsAmountReman = NULL,
                @OriginalPartsAmountTotal = NULL,
                @OriginalPartsDiscount = NULL,
                @OriginalRepairTotal = NULL,
                @OriginalBetterment = NULL,
                @OriginalRepairTotalwoBetterment = NULL,
                @OriginalDeductible = NULL,
                @OriginalOtherAdjTotal = NULL,
                @OriginalNetTotal = NULL,
                @AgreedRepairTotal = NULL,
                @AgreedBetterment = NULL,
                @AgreedRepairTotalwoBetterment = NULL,
                @AgreedDeductible = NULL,
                @AgreedOtherAdjTotal = NULL,
                @AgreedNetTotal = NULL,
                @AgreedFlag = NULL,
                @DocumentCreatedDate = NULL,
                @ApprovedFlag = NULL,
                @FinalBillEstimateFlag = NULL
        
    
        FETCH NEXT FROM csrEstimateAmounts
          INTO  @DocumentID,
                @SequenceNumber,
                @EstimateTypeCD,
                @OriginalLaborRateBody,
                @OriginalLaborRateFrame,
                @OriginalLaborRateMech,
                @OriginalLaborRateRefinish,
                @OriginalLaborAmountRepair,
                @OriginalLaborAmountReplace,
                @OriginalLaborAmountTotal,
                @OriginalPartsAmountAFMK,
                @OriginalPartsAmountLKQ,
                @OriginalPartsAmountOEM,
                @OriginalPartsAmountReman,
                @OriginalPartsAmountTotal,
                @OriginalPartsDiscount,
                @OriginalRepairTotal,
                @OriginalBetterment,
                @OriginalRepairTotalwoBetterment,
                @OriginalDeductible,
                @OriginalOtherAdjTotal,
                @OriginalNetTotal,
                @AgreedRepairTotal,
                @AgreedBetterment,
                @AgreedRepairTotalwoBetterment,
                @AgreedDeductible,
                @AgreedOtherAdjTotal,
                @AgreedNetTotal,
                @AgreedFlag,
                @DocumentCreatedDate,
                @ApprovedFlag,
                @FinalBillEstimateFlag

    
        WHILE @@FETCH_STATUS = 0
        BEGIN
            -- Compile the estimate amounts
            
            IF @OriginalNetTotal IS NOT NULL 
            BEGIN
                INSERT INTO @tmpEstimateTotals 
                    (SequenceNumber, 
                     EstimateTypeCD, 
                     LaborRateBody, 
                     LaborRateFrame, 
                     LaborRateMech, 
                     LaborRateRefinish, 
                     LaborAmountRepair,
                     LaborAmountReplace,
                     LaborAmountTotal,
                     PartsAmountAFMK,
                     PartsAmountLKQ,
                     PartsAmountOEM,
                     PartsAmountReman,
                     PartsAmountTotal,
                     PartsDiscount,
                     RepairAmount, 
                     Betterment, 
                     RepairAmountwoBetterment, 
                     Deductible, 
                     OtherAdj, 
                     NetAmount, 
                     AgreedFlag, 
                     CreateDate,
                     ApprovedFlag,
                     FinalBillEstimateFlag)
                  VALUES(@SequenceNumber, 
                         @EstimateTypeCD, 
                         @OriginalLaborRateBody, 
                         @OriginalLaborRateFrame, 
                         @OriginalLaborRateMech, 
                         @OriginalLaborRateRefinish, 
                         @OriginalLaborAmountRepair,
                         @OriginalLaborAmountReplace,
                         @OriginalLaborAmountTotal,
                         @OriginalPartsAmountAFMK,
                         @OriginalPartsAmountLKQ,
                         @OriginalPartsAmountOEM,
                         @OriginalPartsAmountReman,
                         @OriginalPartsAmountTotal,
                         @OriginalPartsDiscount,
                         @OriginalRepairTotal, 
                         @OriginalBetterment, 
                         @OriginalRepairTotalwoBetterment, 
                         @OriginalDeductible, 
                         @OriginalOtherAdjTotal, 
                         @OriginalNetTotal, 
                         @AgreedFlag, 
                         @DocumentCreatedDate,
                         @ApprovedFlag,
                         @FinalBillEstimateFlag)
        
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into @tmpEstimateTotals', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END

            IF @AgreedNetTotal IS NOT NULL AND @AgreedNetTotal <> @OriginalNetTotal
            BEGIN
                INSERT INTO @tmpEstimateTotals 
                    (SequenceNumber, 
                     EstimateTypeCD, 
                     RepairAmount,
                     Betterment, 
                     RepairAmountwoBetterment, 
                     Deductible, 
                     OtherAdj, 
                     NetAmount, 
                     AgreedFlag, 
                     CreateDate,
                     ApprovedFlag,
                     FinalBillEstimateFlag)
                  VALUES(@SequenceNumber, 
                         @EstimateTypeCD, 
                         @AgreedRepairTotal, 
                         @AgreedBetterment, 
                         @AgreedRepairTotalwoBetterment, 
                         @AgreedDeductible, 
                         @AgreedOtherAdjTotal, 
                         @AgreedNetTotal, 
                         @AgreedFlag, 
                         @DocumentCreatedDate,
                         @ApprovedFlag,
                         @FinalBillEstimateFlag)
        
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error inserting into @tmpEstimateTotals', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END


            SELECT  @DocumentID = NULL, 
                    @SequenceNumber = NULL, 
                    @EstimateTypeCD = NULL, 
                    @OriginalLaborRateBody = NULL,
                    @OriginalLaborRateFrame = NULL, 
                    @OriginalLaborRateMech = NULL, 
                    @OriginalLaborRateRefinish = NULL,
                    @OriginalLaborAmountRepair = NULL, 
                    @OriginalLaborAmountReplace = NULL, 
                    @OriginalLaborAmountTotal = NULL,
                    @OriginalPartsAmountAFMK = NULL,
                    @OriginalPartsAmountLKQ = NULL,
                    @OriginalPartsAmountOEM = NULL,
                    @OriginalPartsAmountReman = NULL,
                    @OriginalPartsAmountTotal = NULL,
                    @OriginalPartsDiscount = NULL,
                    @OriginalRepairTotal = NULL,
                    @OriginalBetterment = NULL,
                    @OriginalRepairTotalwoBetterment = NULL,
                    @OriginalDeductible = NULL,
                    @OriginalOtherAdjTotal = NULL,
                    @OriginalNetTotal = NULL,
                    @AgreedRepairTotal = NULL,
                    @AgreedBetterment = NULL,
                    @AgreedRepairTotalwoBetterment = NULL,
                    @AgreedDeductible = NULL,
                    @AgreedOtherAdjTotal = NULL,
                    @AgreedNetTotal = NULL,
                    @AgreedFlag = NULL,
                    @DocumentCreatedDate = NULL,
                    @ApprovedFlag = NULL,
                    @FinalBillEstimateFlag = NULL
        
    
            FETCH NEXT FROM csrEstimateAmounts
              INTO  @DocumentID,
                    @SequenceNumber,
                    @EstimateTypeCD,
                    @OriginalLaborRateBody,
                    @OriginalLaborRateFrame,
                    @OriginalLaborRateMech,
                    @OriginalLaborRateRefinish,
                    @OriginalLaborAmountRepair,
                    @OriginalLaborAmountReplace,
                    @OriginalLaborAmountTotal,
                    @OriginalPartsAmountAFMK,
                    @OriginalPartsAmountLKQ,
                    @OriginalPartsAmountOEM,
                    @OriginalPartsAmountReman,
                    @OriginalPartsAmountTotal,
                    @OriginalPartsDiscount,
                    @OriginalRepairTotal,
                    @OriginalBetterment,
                    @OriginalRepairTotalwoBetterment,
                    @OriginalDeductible,
                    @OriginalOtherAdjTotal,
                    @OriginalNetTotal,
                    @AgreedRepairTotal,
                    @AgreedBetterment,
                    @AgreedRepairTotalwoBetterment,
                    @AgreedDeductible,
                    @AgreedOtherAdjTotal,
                    @AgreedNetTotal,
                    @AgreedFlag,
                    @DocumentCreatedDate,
                    @ApprovedFlag,
                    @FinalBillEstimateFlag
        END        

        
        -- Close and deallocate the estimate amount cursor
    
        CLOSE csrEstimateAmounts
        DEALLOCATE csrEstimateAmounts
                
        
        IF @debug = 1
        BEGIN
            PRINT ''
            PRINT 'Compiled Estimate Totals:'
            SELECT * FROM @tmpEstimateTotals
        END
        
        
        -- Original Estimate Amounts (These are the first "Original" estimate total amounts - EstimateTypeCD = 'O')
        
        SELECT TOP 1 @OriginalEstimateGrossAmt = RepairAmount,
                     @OriginalEstimateBettermentAmt = Betterment,
                     @OriginalEstimatewoBettAmt = RepairAmountwoBetterment,
                     @OriginalEstimateDeductibleAmt = Deductible,
                     @OriginalEstimateOtherAdjustmentAmt = OtherAdj,
                     @OriginalEstimateNetAmt = NetAmount
          FROM  @tmpEstimateTotals
          WHERE EstimateTypeCD = 'O'
          ORDER BY EstimateTotalID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 Begin Comment:        
		-- Per meeting w/ Linda/Tom/Andrew Audited Original Estimate Gross comes from the 
		-- --------------------------------------------------------------------------------------------------
		--SELECT TOP 1 @OriginalAuditedEstimateGrossAmt = RepairAmount,
		--             @OriginalAuditedEstimateBettermentAmt = Betterment,
		--             @OriginalAuditedEstimatewoBettAmt = RepairAmountwoBetterment,
		--             @OriginalAuditedEstimateDeductibleAmt = Deductible,
		--             @OriginalAuditedEstimateOtherAdjustmentAmt = OtherAdj,
		--             @OriginalAuditedEstimateNetAmt = NetAmount
		--  FROM  @tmpEstimateTotals
		--  WHERE ApprovedFlag = 1
		--  ORDER BY EstimateTotalID
		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 End Comment:        
		-- --------------------------------------------------------------------------------------------------

		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 - Audited Original Estimate Amounts - Per meeting w/ Linda/Tom/Andrew the 
		-- OriginalAuditedEstimateGrossAmount from the utb_claim_aspect_audit_values table (CURSOR select), 
		-- then pull the rest of the Audited Original values from the Original Estimate.          
		-- --------------------------------------------------------------------------------------------------

        SELECT	TOP 1 -- MAH 07-jun-2012 Pulled in CURSOR select @OriginalAuditedEstimateGrossAmt = RepairAmount,
				@OriginalAuditedEstimateBettermentAmt = Betterment,
				@OriginalAuditedEstimatewoBettAmt = RepairAmountwoBetterment,
				@OriginalAuditedEstimateDeductibleAmt = Deductible,
				@OriginalAuditedEstimateOtherAdjustmentAmt = OtherAdj,
				@OriginalAuditedEstimateNetAmt = NetAmount
          FROM  @tmpEstimateTotals
         WHERE	EstimateTypeCD = 'O'
         ORDER 
		    BY	EstimateTotalID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        -- ------------------------------------------------------------------------------------------
		-- MAH - 07-jun-2012 - Select the Early Bill Audited Gross Estimate Amount:
        -- ------------------------------------------------------------------------------------------

        SELECT	TOP 1 
				@EarlyBillAuditedGrossAmt = RepairAmount
          FROM  @tmpEstimateTotals
         WHERE	ApprovedFlag = 1
         ORDER 
		    BY	EstimateTotalID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        -- Audited Estimate Amounts and AgreedFlag (This is extracted from the first audited estimate - EstimateTypeCD = 'A')
        
        SELECT TOP 1 @AuditedEstimateAgreedFlag = AgreedFlag,
                     @AuditedEstimateGrossAmt = RepairAmount,
                     @AuditedEstimateBettermentAmt = Betterment,
                     @AuditedEstimatewoBettAmt = RepairAmountwoBetterment,
                     @AuditedEstimateDeductibleAmt = Deductible,
                     @AuditedEstimateOtherAdjustmentAmt = OtherAdj,
                     @AuditedEstimateNetAmt = NetAmount
          FROM  @tmpEstimateTotals
          WHERE EstimateTypeCD = 'A'
          ORDER BY EstimateTotalID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 Begin Comment:        
		-- --------------------------------------------------------------------------------------------------
		--SELECT TOP 1 @FinalEstimateGrossAmt = RepairAmount,
		--             @FinalEstimateBettermentAmt = Betterment,
		--             @FinalEstimatewoBettAmt = RepairAmountwoBetterment,
		--             @FinalEstimateDeductibleAmt = Deductible,
		--             @FinalEstimateOtherAdjustmentAmt = OtherAdj,
		--             @FinalEstimateNetAmt = NetAmount
		--  FROM  @tmpEstimateTotals
		--  WHERE EstimateTypeCD = 'O'
		--    AND CreateDate <= IsNull(@OriginalCompleteDate, @Now)
		--  ORDER BY EstimateTotalID DESC
		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 End Comment:        
		-- --------------------------------------------------------------------------------------------------

        -- Final Estimate Amount  (This is the final "original" estimate before original claim close)

		-- --------------------------------------------------------------------------------------------------
		-- MAH 07-jun-2012 New Logic so RRP claims ignore the Original Claim Closed Date.
		-- Changed query for RRP to ignore the data logic per meetings w/ Linda/Tom/Andrew.  Per meeting for
		-- RRP the Final Estimate should always be the last estimate received regardless of whether the claim
		-- has already closed or not.
		-- --------------------------------------------------------------------------------------------------
        SELECT TOP 1 @FinalEstimateGrossAmt = RepairAmount,
                     @FinalEstimateBettermentAmt = Betterment,
                     @FinalEstimatewoBettAmt = RepairAmountwoBetterment,
                     @FinalEstimateDeductibleAmt = Deductible,
                     @FinalEstimateOtherAdjustmentAmt = OtherAdj,
                     @FinalEstimateNetAmt = NetAmount
          FROM  @tmpEstimateTotals
          WHERE EstimateTypeCD = 'O'
			AND ((@AssignmentTypeID  = 16)		-- If RRP do not care about Orig Claim Closed Date.
			 OR  (@AssignmentTypeID != 16
            AND   CreateDate <= IsNull(@OriginalCompleteDate, @Now)))
          ORDER BY EstimateTotalID DESC

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Final supplement amount  (This is the final "original" estimate on the claim which could includes supplements
        -- received after the original claim closure date)
        
        SELECT TOP 1 @FinalSupplementGrossAmt = RepairAmount,
                     @FinalSupplementBettermentAmt = Betterment,
                     @FinalSupplementwoBettAmt = RepairAmountwoBetterment,
                     @FinalSupplementDeductibleAmt = Deductible,
                     @FinalSupplementOtherAdjustmentAmt = OtherAdj,
                     @FinalSupplementNetAmt = NetAmount
          FROM  @tmpEstimateTotals
          WHERE EstimateTypeCD = 'O'
          ORDER BY EstimateTotalID DESC

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Final audited supplement amount  (This is the final "audited" estimate on the claim which could includes supplements
        -- received after the original claim closure date)
        
        SELECT TOP 1 @FinalAuditedSuppAgreedFlag = AgreedFlag,
                     @FinalAuditedSuppGrossAmt = RepairAmount,
                     @FinalAuditedSuppBettermentAmt = Betterment,
                     @FinalAuditedSuppwoBettAmt = RepairAmountwoBetterment,
                     @FinalAuditedSuppDeductibleAmt = Deductible,
                     @FinalAuditedSuppOtherAdjustmentAmt = OtherAdj,
                     @FinalAuditedSuppNetAmt = NetAmount
          FROM  @tmpEstimateTotals
          WHERE EstimateTypeCD = 'A'
          ORDER BY EstimateTotalID DESC

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Indemnity Amount
        
        IF EXISTS (SELECT  InvoiceID
                     FROM  dbo.utb_invoice i WITH (NOLOCK)
                     WHERE i.ClaimAspectID = @ClaimAspectID
                       AND i.EnabledFlag = 1
                       AND i.EntryDate <= IsNull(@OriginalCompleteDate, @Now)
                       AND i.ItemTypeCD = 'I')
        BEGIN
            -- At least one payment exists, use the sum of these as the indemnity.  Indemnity is counted until the
            -- claim is closed, so ignore any payments beyond that.
            
            SELECT  @IndemnityAmount = SUM(i.Amount)
              FROM  dbo.utb_invoice i WITH (NOLOCK)
              WHERE i.ClaimAspectID = @ClaimAspectID
                AND i.EnabledFlag = 1
                AND i.EntryDate <= IsNull(@OriginalCompleteDate, @Now)
                AND i.ItemTypeCD = 'I'

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
        ELSE
        BEGIN
            -- No payment exists.  For reporting purposes, we'll use the final original estimate net amount obtained 
            -- before claim close for Program Shop, and the final Audited estimate net amount up until claim close for DA.

            IF @ClosingServiceChannelCD = 'DA'
            BEGIN
                -- Set Indemnity to final Audited estimate Net

                SELECT TOP 1 @IndemnityAmount = NetAmount
                  FROM  @tmpEstimateTotals
                  WHERE CreateDate <= IsNull(@OriginalCompleteDate, @Now)
                    AND EstimateTypeCD = 'A'
                  ORDER BY EstimateTotalID DESC
            END
            ELSE
            BEGIN
                -- Set Indemnity to final estimate Net
                
                SELECT TOP 1 @IndemnityAmount = NetAmount
                  FROM  @tmpEstimateTotals
                  WHERE CreateDate <= IsNull(@OriginalCompleteDate, @Now)
                    AND EstimateTypeCD = 'O'
                  ORDER BY EstimateTotalID DESC
            END
            
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END

        
        -- Fee Revenue Amount
        
        SELECT  @FeeRevenueAmt = SUM(Amount)
          FROM  dbo.utb_invoice WITH (NOLOCK)
          WHERE ClaimAspectID = @ClaimAspectID
            AND EnabledFlag = 1
            AND ItemTypeCD = 'F'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        -- Miscellaneous Labor and Part Metrics.  Retrieve them from the audited estimate for desk audits, the final estimate for everything else  
        
        IF @ClosingServiceChannelCD = 'DA'
        BEGIN
            SELECT TOP 1 @LaborRateBodyAmt = LaborRateBody,
                         @LaborRateFrameAmt = LaborRateFrame,
                         @LaborRateMechAmt = LaborRateMech,
                         @LaborRateRefinishAmt = LaborRateRefinish, 
                         @LaborRepairAmt = LaborAmountRepair,
                         @LaborReplaceAmt = LaborAmountReplace,
                         @LaborTotalAmt = LaborAmountTotal,
                         @PartsAFMKReplacedAmt = PartsAmountAFMK,
                         @PartsLKQReplacedAmt = PartsAmountLKQ,
                         @PartsOEMDiscountAmt = PartsDiscount,
                         @PartsOEMReplacedAmt = PartsAmountOEM,
                         @PartsRemanReplacedAmt = PartsAmountReman,
                         @PartsTotalReplacedAmt = PartsAmountTotal
              FROM  @tmpEstimateTotals
              WHERE EstimateTypeCD = 'A'
              ORDER BY EstimateTotalID
        END
        ELSE
        BEGIN
            SELECT TOP 1 @LaborRateBodyAmt = LaborRateBody,
                         @LaborRateFrameAmt = LaborRateFrame,
                         @LaborRateMechAmt = LaborRateMech,
                         @LaborRateRefinishAmt = LaborRateRefinish,
                         @LaborRepairAmt = LaborAmountRepair,
                         @LaborReplaceAmt = LaborAmountReplace,
                         @LaborTotalAmt = LaborAmountTotal,
                         @PartsAFMKReplacedAmt = PartsAmountAFMK,
                         @PartsLKQReplacedAmt = PartsAmountLKQ,
                         @PartsOEMDiscountAmt = PartsDiscount,
                         @PartsOEMReplacedAmt = PartsAmountOEM,
                         @PartsRemanReplacedAmt = PartsAmountReman,
                         @PartsTotalReplacedAmt = PartsAmountTotal
              FROM  @tmpEstimateTotals
              WHERE EstimateTypeCD = 'O'
                AND CreateDate <= IsNull(@OriginalCompleteDate, @Now)
              ORDER BY EstimateTotalID DESC
        END
        
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- Check to see if we have gotten the rates.  For a number of reasons, we may not have them.  (Manual
        -- estimates, for example, or rates missing from the data.)  If any of them are missing (NULL), grab
        -- the labor rates out of the shop's pricing record

        IF  @LaborRateBodyAmt IS NULL OR
            @LaborRateFrameAmt IS NULL OR
            @LaborRateMechAmt IS NULL OR
            @LaborRateRefinishAmt IS NULL
        BEGIN
            -- Grab the pricing information        

            IF EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location_pricing_history WITH (NOLOCK) WHERE ShopLocationID = @ShopLocationID)
            BEGIN
                SELECT TOP 1 @PricingLaborRateMech = HourlyRateMechanical,   
                             @PricingLaborRateRefinish = HourlyRateRefinishing,
                             @PricingLaborRateBody = HourlyRateSheetMetal,   
                             @PricingLaborRateFrame = HourlyRateUnibodyFrame      
                  FROM  dbo.utb_shop_location_pricing_history WITH (NOLOCK)
                  WHERE ShopLocationID = @ShopLocationID
                    AND SysLastUpdatedDate < @AssignmentDate
                  ORDER BY SysLastUpdatedDate DESC
            END
            ELSE
            BEGIN
                SELECT  @PricingLaborRateMech = HourlyRateMechanical,   
                        @PricingLaborRateRefinish = HourlyRateRefinishing,
                        @PricingLaborRateBody = HourlyRateSheetMetal,   
                        @PricingLaborRateFrame = HourlyRateUnibodyFrame      
                  FROM  dbo.utb_shop_location_pricing WITH (NOLOCK)
                  WHERE ShopLocationID = @ShopLocationID
            END     

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            -- Check to see where we have missing labor rates and populate where appropriate
        
            IF @LaborRateBodyAmt IS NULL SELECT @LaborRateBodyAmt = @PricingLaborRateBody
            IF @LaborRateFrameAmt IS NULL SELECT @LaborRateFrameAmt = @PricingLaborRateFrame
            IF @LaborRateMechAmt IS NULL SELECT @LaborRateMechAmt = @PricingLaborRateMech
            IF @LaborRateRefinishAmt IS NULL SELECT @LaborRateRefinishAmt = @PricingLaborRateRefinish
        END
                

        -- Supplement Total Amount.  For Desk Audits, this is defined as the difference between the Original Audited Estimate gross and the Final 
        -- Audited estimate gross.  For other service channels, this is defined as the difference between the final estimate gross and the final
        -- supplement gross.  Note: We use the gross amt without betterment to try to obtain a more accurate metric.
        
        IF @ClosingServiceChannelCD = 'DA'
        BEGIN 
            SELECT @SupplementTotalAmt = @FinalAuditedSuppwoBettAmt - @AuditedEstimatewoBettAmt
        END
        ELSE
        BEGIN
            SELECT @SupplementTotalAmt = @FinalSupplementwoBettAmt - @FinalEstimatewoBettAmt
        END
        
        
        -- Maximum Sequence Number
        
        SELECT  @MaxEstSuppSequenceNumber = Max(SequenceNumber)
          FROM  @tmpDocument
          WHERE DuplicateFlag = 0
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        IF (@MaxEstSuppSequenceNumber IS NULL) AND ((SELECT Count(*) FROM @tmpDocument) > 0)
        BEGIN
            -- We have estimates but we didn't get a sequence number.  We'll go ahead and set this to 0 for these.
            
            SELECT @MaxEstSuppSequenceNumber = 0
        END
        
          
        -- Reinspection Information
        
        SELECT  @ReinspectionCount = Count(*)
          FROM  dbo.utb_reinspect WITH (NOLOCK)
          WHERE EstimateDocumentID IN (SELECT DocumentID FROM @tmpDocument)
            AND ReinspectTypeCD <> 'SUP'
            
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        SELECT  @ReinspectionDeviationCount = Count(*),
                @ReinspectionDeviationAmt = SUM(Abs(EstimateAmt - ReinspectAmt))
          FROM  dbo.utb_reinspect_detail rd WITH (NOLOCK)
          LEFT JOIN dbo.utb_reinspect r WITH (NOLOCK) ON (rd.ReinspectID = r.ReinspectID)          
          WHERE r.EstimateDocumentID IN (SELECT DocumentID FROM @tmpDocument)
            AND r.ReinspectTypeCD <> 'SUP'
            
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        
        -- Rental Information.  These are reserved for future use.

        SELECT @RentalDays = NULL, @RentalAmount = NULL, @RentalCostPerDay = NULL 
                    

        -- Service Flags
        
        IF EXISTS(SELECT *
                    FROM dbo.utb_invoice i
                    LEFT JOIN dbo.utb_invoice_service isv WITH (NOLOCK) ON (i.InvoiceID = isv.InvoiceID)
                    WHERE i.ClaimAspectID = @ClaimAspectID
                      AND isv.ServiceID IN (SELECT ServiceID 
                                              FROM dbo.utb_service WITH (NOLOCK)
                                              WHERE Name IN ('First Party Rental', 'Third Party Loss Of Use', 'Rental Management')))
        BEGIN
            -- We have rental
            
            SELECT @ServiceLossOfUseFlag = 1
        END
        ELSE
        BEGIN
            SELECT @ServiceLossOfUseFlag = 0
        END
        
        IF EXISTS(SELECT *
                    FROM dbo.utb_invoice i WITH (NOLOCK) 
                    LEFT JOIN dbo.utb_invoice_service isv WITH (NOLOCK) ON (i.InvoiceID = isv.InvoiceID)
                    WHERE i.ClaimAspectID = @ClaimAspectID
                      AND isv.ServiceID = (SELECT  ServiceID 
                                             FROM  dbo.utb_service WITH (NOLOCK)
                                             WHERE Name = 'Total Loss Evaluation'))
        BEGIN
            -- We have Total Loss
            
            SELECT @ServiceTotalLossFlag = 1
        END
        ELSE
        BEGIN
            SELECT @ServiceTotalLossFlag = 0
        END

        -- At this time, we cannot determine subrogation or total theft.  For now, we're just going to set the flags off
        
        SELECT @ServiceSubrogationFlag = 0, @ServiceTotalTheftFlag = 0      -- Reserved for future use
        
        
        -- Vehicle License Plate ID

        IF @LicensePlateStateCode IS NOT NULL
        BEGIN        
            SELECT  @VehicleLicensePlateStateID = StateID
              FROM  dbo.utb_dtwh_dim_state WITH (NOLOCK)
              WHERE StateCode = @LicensePlateStateCode
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
        
        -- Concession Amount
        
        -- Calculate the Concessions Labor Rate
        SELECT @TotalConcessionLaborRate = SUM(cascc.Amount)
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Labor Rate'
          AND cascc.EnabledFlag = 1
          
        -- Calculate the Concession Labor Rate comments
        SELECT @TotalConcessionLaborRateComment = isNull(@TotalConcessionLaborRateComment, '') + '$' + convert(varchar, cascc.Amount) + ' - ' + replace(replace(cascc.Comments, '|', ' '), '"', '') + '; '
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Labor Rate'
          AND cascc.EnabledFlag = 1
        
        IF LEN(@TotalConcessionLaborRateComment) > 2500
        BEGIN
            SET @TotalConcessionLaborRateComment = SUBSTRING(@TotalConcessionLaborRateComment, 1, 2500)
        END
        
        -- Calculate the Concessions Labor Rate
        SELECT @TotalConcessionUnrelatedDamage = SUM(cascc.Amount)
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Unrelated Damage'
          AND cascc.EnabledFlag = 1
          
        -- Calculate the Concession Labor Rate comments
        SELECT @TotalConcessionUnrelatedDamageComment = isNull(@TotalConcessionUnrelatedDamageComment, '') + '$' + convert(varchar, cascc.Amount) + ' - ' + replace(replace(cascc.Comments, '|', ' '), '"', '') + '; '
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Unrelated Damage'
          AND cascc.EnabledFlag = 1
        
        IF LEN(@TotalConcessionUnrelatedDamageComment) > 2500
        BEGIN
            SET @TotalConcessionUnrelatedDamageComment = SUBSTRING(@TotalConcessionUnrelatedDamageComment, 1, 2500)
        END
        
        -- Calculate the Concessions Parts Usage
        SELECT @TotalConcessionPartsUsage = SUM(cascc.Amount)
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Parts Usage'
          AND cascc.EnabledFlag = 1
          
        -- Calculate the Concession Parts Usage comments
        SELECT @TotalConcessionPartsUsageComment = isNull(@TotalConcessionPartsUsageComment, '') + '$' + convert(varchar, cascc.Amount) + ' - ' + replace(replace(cascc.Comments, '|', ' '), '"', '') + '; '
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Parts Usage'
          AND cascc.EnabledFlag = 1

        IF LEN(@TotalConcessionPartsUsageComment) > 2500
        BEGIN
            SET @TotalConcessionPartsUsageComment = SUBSTRING(@TotalConcessionPartsUsageComment, 1, 2500)
        END
        

        -- Calculate the Concessions Total Loss
        SELECT @TotalConcessionTotalLoss = SUM(cascc.Amount)
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Total Loss'
          AND cascc.EnabledFlag = 1
          
        -- Calculate the Concession Total Loss comments
        SELECT @TotalConcessionTotalLossComment = isNull(@TotalConcessionTotalLossComment, '') + '$' + convert(varchar, cascc.Amount) + ' - ' + replace(replace(cascc.Comments, '|', ' '), '"', '') + '; '
        FROM dbo.utb_claim_aspect_service_channel_concession cascc
        LEFT JOIN dbo.utb_concession_reason cr ON cascc.ConcessionReasonID = cr.ConcessionReasonID
        WHERE cascc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND cr.ConcessionReasonTypeCD = 'I'
          AND cr.Description = 'Total Loss'
          AND cascc.EnabledFlag = 1

        IF LEN(@TotalConcessionTotalLossComment) > 2500
        BEGIN
            SET @TotalConcessionTotalLossComment = SUBSTRING(@TotalConcessionTotalLossComment, 1, 2500)
        END
        
        set @TotalConcessionLaborRateComment = replace(replace(@TotalConcessionLaborRateComment, char(13), ''), char(10), '')
        set @TotalConcessionUnrelatedDamageComment = replace(replace(@TotalConcessionUnrelatedDamageComment, char(13), ''), char(10), '')
        set @TotalConcessionPartsUsageComment = replace(replace(@TotalConcessionPartsUsageComment, char(13), ''), char(10), '')
        set @TotalConcessionTotalLossComment = replace(replace(@TotalConcessionTotalLossComment, char(13), ''), char(10), '')

		-- -----------------------------------------------------------------------------------------
		-- SWR 203304 01-30-13 MAH - Set the ClaimantName:
		-- -----------------------------------------------------------------------------------------

		SELECT  @ClaimantName = REPLACE(ISNULL(i.BusinessName, i.NameLast + ', ' + i.NameFirst),'"','')
		  FROM  dbo.utb_claim_aspect ca
				LEFT JOIN dbo.utb_claim_aspect_involved cai	
					   ON ca.ClaimAspectID = cai.ClaimAspectID
				LEFT JOIN dbo.utb_involved i 
					   ON cai.InvolvedID = i.InvolvedID
				LEFT JOIN dbo.utb_involved_role ir 
					   ON i.InvolvedID = ir.InvolvedID
				LEFT JOIN dbo.utb_involved_role_type irt 
					   ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
		 WHERE  ca.ClaimAspectID     = @ClaimAspectID
		   AND  ca.ClaimAspectTypeID = 9 -- Vehicle Aspect Type
		   AND  cai.EnabledFlag      = 1 
		   AND (irt.Name             = 'Claimant' 
			OR  irt.Name             = 'Insured')
		   
		-- -----------------------------------------------------------------------------------------
		-- SWR 203304 01-30-13 MAH - Set the InsuredName:
		-- -----------------------------------------------------------------------------------------

		SELECT  TOP 1 
				@InsuredName = REPLACE(ISNULL(i.BusinessName, i.NameLast + ', ' + i.NameFirst),'"','')
		  FROM  dbo.utb_claim_aspect ca 
			    LEFT JOIN dbo.utb_claim_aspect_involved cai 
					  ON (ca.ClaimAspectID = cai.ClaimAspectID)
			    LEFT JOIN dbo.utb_involved i 
					   ON (cai.InvolvedID = i.InvolvedID)
		  	    LEFT JOIN dbo.utb_involved_role ir 
					   ON (i.InvolvedID = ir.InvolvedID)
			    LEFT JOIN dbo.utb_involved_role_type irt 
				  	   ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
		 WHERE  ca.LynxID            = @LynxID
		   AND  ca.ClaimAspectTypeID = 0   -- Claim Aspect Type
		   AND  cai.EnabledFlag      = 1
		   AND  irt.Name             = 'Insured'

		-- -------------------------------------------------------------------------------------------

        IF @debug = 1
        BEGIN
            PRINT ''
            PRINT 'Resulting claim fact record:'
            SELECT @AssignmentTypeClosingID AS AssignmentTypeClosingID,
                   @AssignmentTypeID AS AssignmentTypeID,
                   @ClaimLocationID AS ClaimLocationID,
                   @CoverageTypeID AS CoverageTypeID,
                   @CustomerID AS CustomerID,
                   @DispositionTypeID AS DispositionTypeID,
                   @LynxHandlerAnalystID AS LynxhandlerAnalystID,
                   @LynxHandlerOwnerID AS LynxhandlerOwnerID,
                   @LynxHandlerSupportID AS LynxhandlerSupportID,
                   @RepairLocationIDClaim AS RepairLocationID,
                   @ServiceChannelID AS ServiceChannelID,
                   @TimeIDAssignDownload AS TimeIDAssignDownload,
                   @TimeIDAssignSent AS TimeIDAssignSent,
                   @TimeIDCancelled AS TimeIDCancelled,
                   @TimeIDClosed AS TimeIDClosed,
                   @TimeIDEstimate AS TimeIDEstimate,
                   @TimeIDInspection AS TimeIDInspection,
                   @TimeIDNew AS TimeIDNew,
                   @TimeIDReclosed AS TimeIDReclosed,
                   @TimeIDRepairComplete AS TimeIDRepairComplete,
                   @TimeIDRepairStarted AS TimeIDRepairStarted,
                   @TimeIDReopened AS TimeIDReopened,
                   @TimeIDVoided AS TimeIDVoided,
                   @ClaimAspectNumber AS VehicleNumber,
                   @VehicleLicensePlateStateID AS VehicleLicensePlateStateID,
                   @AuditedEstimateAgreedFlag AS AuditedEstimateAgreedFlag,
                   @AuditedEstimateBettermentAmt AS AuditedEstimateBettermentAmt,
                   @AuditedEstimateDeductibleAmt AS AuditedEstimateDeductibleAmt,
                   @AuditedEstimateGrossAmt AS AuditedEstimateGrossAmt,
                   @AuditedEstimateNetAmt AS AuditedEstimateNetAmt,
                   @AuditedEstimateOtherAdjustmentAmt AS AuditedEstimateOtherAdjustmentAmt,
                   @AuditedEstimatewoBettAmt AS AuditedEstimatewoBettAmt,
                   @CFProgramFlag AS CFProgramFlag,
				   @ClaimantName AS ClaimantName,							-- (SWR 203304)
                   @ClaimStatusCD AS ClaimStatusCD,
                   @ClientClaimNumber AS ClientClaimNumber,
                   @CycleTimeAssignToEstBusDay AS CycleTimeAssignToEstBusDay,
                   @CycleTimeAssignToEstCalDay AS CycleTimeAssignToEstCalDay,
                   @CycleTimeAssignToEstHrs AS CycleTimeAssignToEstHrs,
                   @CycleTimeEstToCloseBusDay AS CycleTimeEstToCloseBusDay,
                   @CycleTimeEstToCloseCalDay AS CycleTimeEstToCloseCalDay,
                   @CycleTimeEstToCloseHrs AS CycleTimeEstToCloseHrs,
                   @CycleTimeInspectionToEstBusDay AS CycleTimeInspectionToEstBusDay,
                   @CycleTimeInspectionToEstCalDay AS CycleTimeInspectionToEstCalDay,
                   @CycleTimeInspectionToEstHrs AS CycleTimeInspectionToEstHrs,
                   @CycleTimeNewToCloseBusDay AS CycleTimeNewToCloseBusDay,
                   @CycleTimeNewToCloseCalDay AS CycleTimeNewToCloseCalDay,
                   @CycleTimeNewToCloseHrs AS CycleTimeNewToCloseHrs,
                   @CycleTimeNewToEstBusDay AS CycleTimeNewToEstBusDay,
                   @CycleTimeNewToEstCalDay AS CycleTimeNewToEstCalDay,
                   @CycleTimeNewToEstHrs AS CycleTimeNewToEstHrs,
                   @CycleTimeNewToInspectionBusDay AS CycleTimeNewToInspectionBusDay,
                   @CycleTimeNewToInspectionCalDay AS CycleTimeNewToInspectionCalDay,
                   @CycleTimeNewToInspectionHrs AS CycleTimeNewToInspectionHrs,
                   @CycleTimeNewToRecloseBusDay AS CycleTimeNewToRecloseBusDay,
                   @CycleTimeNewToRecloseCalDay AS CycleTimeNewToRecloseCalDay,
                   @CycleTimeNewToRecloseHrs AS CycleTimeNewToRecloseHrs,
                   @CycleTimeRepairStartToEndBusDay AS CycleTimeRepairStartToEndBusDay,
                   @CycleTimeRepairStartToEndCalDay AS CycleTimeRepairStartToEndCalDay,
                   @CycleTimeRepairStartToEndHrs AS CycleTimeRepairStartToEndHrs,
                   @DemoFlag AS DemoFlag,
	               @EarlyBillAuditedGrossAmt AS EarlyBillAuditedGrossAmt,
	               -- -----------------------------------------------------------
				   --  MAH 07-jun-2012 Obsolete:
				   -- -----------------------------------------------------------
	               -- @EarlyBillAuditedAddtlLineItemAmt AS EarlyBillAuditedAddtlLineItemAmt,
	               -- @EarlyBillAuditedLineItemCorrectionAmt AS EarlyBillAuditedLineItemCorrectionAmt,
	               -- @EarlyBillAuditedMissingLineItemAmt AS EarlyBillAuditedMissingLineItemAmt,
				   -- -----------------------------------------------------------
                   @EnabledFlag AS EnabledFlag,
                   @ExposureCD AS ExposureCD,
                   @FeeRevenueAmt AS FeeRevenueAmt,
                   @FinalAuditedSuppAgreedFlag AS FinalAuditedSuppAgreedFlag,
                   @FinalAuditedSuppGrossAmt AS FinalAuditedSuppGrossAmt,
                   @FinalAuditedSuppBettermentAmt AS FinalAuditedSuppBettermentAmt,
                   @FinalAuditedSuppwoBettAmt AS FinalAuditedSuppwoBettAmt,
                   @FinalAuditedSuppDeductibleAmt AS FinalAuditedSuppDeductibleAmt,
                   @FinalAuditedSuppOtherAdjustmentAmt AS FinalAuditedSuppOtherAdjustmentAmt,
                   @FinalAuditedSuppNetAmt AS FinalAuditedSuppNetAmt,
                   @FinalEstimateBettermentAmt AS FinalEstimateBettermentAmt,
                   @FinalEstimateDeductibleAmt AS FinalEstimateDeductibleAmt,
                   @FinalEstimateGrossAmt AS FinalEstimateGrossAmt,
                   @FinalEstimateNetAmt AS FinalEstimateNetAmt,
                   @FinalEstimateOtherAdjustmentAmt AS FinalEstimateOtherAdjustmentAmt,
                   @FinalEstimatewoBettAmt AS FinalEstimatewoBettAmt,
                   @FinalSupplementBettermentAmt AS FinalSupplementBettermentAmt,
                   @FinalSupplementDeductibleAmt AS FinalSupplementDeductibleAmt,
                   @FinalSupplementGrossAmt AS FinalSupplementGrossAmt,
                   @FinalSupplementNetAmt AS FinalSupplementNetAmt,
                   @FinalSupplementOtherAdjustmentAmt AS FinalSupplementOtherAdjustmentAmt,
                   @FinalSupplementwoBettAmt AS FinalSupplementwoBettAmt,
                   @IndemnityAmount AS IndemnityAmount,
				   @InsuredName AS InsuredName,							-- (SWR 203304)
                   @LaborRateBodyAmt AS LaborRateBodyAmt,
                   @LaborRateFrameAmt AS LaborRateFrameAmt,
                   @LaborRateMechAmt AS LaborRateMechAmt,
                   @LaborRateRefinishAmt AS LaborRateRefinishAmt,
                   @LaborRepairAmt AS LaborRepairAmt,
                   @LaborReplaceAmt AS LaborReplaceAmt,
                   @LaborTotalAmt AS LaborTotalAmt,
                   @LossDate AS LossDate,
                   @LynxID AS LynxID,
                   @MaxEstSuppSequenceNumber AS MaxEstSuppSequenceNumber,
                   @OriginalEstimateBettermentAmt AS OriginalEstimateBettermentAmt,
                   @OriginalEstimateDeductibleAmt AS OriginalEstimateDeductibleAmt,                   
                   @OriginalEstimateGrossAmt AS OriginalEstimateGrossAmt,
                   @OriginalEstimateNetAmt AS OriginalEstimateNetAmt,
                   @OriginalEstimateOtherAdjustmentAmt AS OriginalEstimateOtherAdjustmentAmt,
                   @OriginalEstimatewoBettAmt AS OriginalEstimatewoBettAmt,
                   @OriginalAuditedEstimateBettermentAmt AS OriginalAuditedEstimateBettermentAmt,
                   @OriginalAuditedEstimateDeductibleAmt AS OriginalAuditedEstimateDeductibleAmt,                   
                   @OriginalAuditedEstimateGrossAmt AS OriginalAuditedEstimateGrossAmt,
                   @OriginalAuditedEstimateNetAmt AS OriginalAuditedEstimateNetAmt,
                   @OriginalAuditedEstimateOtherAdjustmentAmt AS OriginalAuditedEstimateOtherAdjustmentAmt,
                   @OriginalAuditedEstimatewoBettAmt AS OriginalAuditedEstimatewoBettAmt,
                   @PartsAFMKReplacedAmt AS PartsAFMKReplacedAmt,
                   @PartsLKQReplacedAmt AS PartsLKQReplacedAmt,
                   @PartsOEMDiscountAmt AS PartsOEMDiscountAmt,
                   @PartsOEMReplacedAmt AS PartsOEMReplacedAmt,
                   @PartsRemanReplacedAmt AS PartsRemanReplacedAmt,
                   @PartsTotalReplacedAmt AS PartsTotalReplacedAmt,
                   @PhotosAttachedFlag AS PhotosAttachedFlag,
                   @PolicyNumber AS PolicyNumber,
                   @PolicyDeductibleAmt AS PolicyDeductibleAmt,
                   @PolicyLimitAmt AS PolicyLimitAmt,
                   @PolicyRentalDeductibleAmt AS PolicyRentalDeductibleAmt,
                   @PolicyRentalLimitAmt AS PolicyRentalLimitAmt,
                   @PolicyRentalMaxDays AS PolicyRentalMaxDays,
                   @ProgramCD AS ProgramCD,
                   @ReinspectionCount AS ReinspectionCount,
                   @ReinspectionDeviationAmt AS ReinspectionDeviationAmt,
                   @ReinspectionDeviationCount AS ReinspectionDeviationCount,
                   @RentalDays AS RentalDays,
                   @RentalAmount AS RentalAmount,
                   @RentalCostPerDay AS RentalCostPerDay,
                   @ServiceLossOfUseFlag AS ServiceLossOfUseFlag,
                   @ServiceSubrogationFlag AS ServiceSubrogationFlag,
                   @ServiceTotalLossFlag AS ServiceTotalLossFlag,
                   @ServiceTotalTheftFlag AS ServiceTotalTheftFlag,
                   @SupplementTotalAmt AS SupplementTotalAmt,
                   @VehicleDriveableFlag AS VehicleDriveableFlag,
                   @VehicleLicensePlateNumber AS VehicleLicensePlateNumber,
                   @VehicleMake AS VehicleMake,
                   @VehicleModel AS VehicleModel,
                   @VehicleYear AS VehicleYear,
                   @VehicleVIN AS VehicleVIN,
                   @TotalConcessionLaborRate AS TotalConcessionLaborRate,
                   @TotalConcessionLaborRateComment AS TotalConcessionLaborRateComment,
                   @TotalConcessionUnrelatedDamage AS TotalConcessionUnrelatedDamage,
                   @TotalConcessionUnrelatedDamageComment AS TotalConcessionUnrelatedDamageComment,
                   @TotalConcessionPartsUsage AS TotalConcessionPartsUsage,
                   @TotalConcessionPartsUsageComment AS TotalConcessionPartsUsageComment,
                   @TotalConcessionTotalLoss AS TotalConcessionTotalLoss,
                   @TotalConcessionTotalLossComment AS TotalConcessionTotalLossComment
                   
        END
        
        
        -- Validate that no required columns are NULL
        
        IF @ClaimLocationID IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @ClaimLocationID is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        IF @CustomerID IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @CustomerID is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @LynxHandlerSupportID IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @LynxHanderSupportID is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @TimeIDNew IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @TimeIDNew is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF @ClaimStatusCD IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @ClaimStatusCD is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        IF @ProgramCD IS NULL
        BEGIN
            RAISERROR('%s: Invalid data state found.  Required column @ProgramCD is NULL for Claim Aspect %u', 16, 1, @ProcName, @ClaimAspectID)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        -- WHEW!!!   Now we're finally ready to save this fact record.  We must first check to see if this ClaimAspectID
        -- already exists in the fact table

        -- Adjust the claim source
        IF @ApplicationName = 'FNOL'
        BEGIN
            SELECT @ClaimSourceID = @ClaimSourceIDFNOL
        END
        ELSE
        BEGIN
            SELECT @ClaimSourceID = @ClaimSourceIDAdjuster
        END 
        
        IF EXISTS(SELECT ClaimAspectID FROM dbo.utb_dtwh_fact_claim WITH (NOLOCK) WHERE ClaimAspectID = @ClaimAspectID)
        BEGIN
            -- The record already exists, let's update
            
            UPDATE dbo.utb_dtwh_fact_claim              
              SET  AssignmentTypeClosingID              = @AssignmentTypeClosingID,
                   AssignmentTypeID                     = @AssignmentTypeID,
                   ClaimLocationID                      = @ClaimLocationID,
                   ClaimSourceID                        = @ClaimSourceID,
                   CoverageTypeID                       = @CoverageTypeID,
                   CustomerID                           = @CustomerID,
                   DispositionTypeID                    = @DispositionTypeID,
                   LynxHandlerAnalystID                 = @LynxhandlerAnalystID,
                   LynxHandlerOwnerID                   = @LynxhandlerOwnerID,
                   LynxHandlerSupportID                 = @LynxhandlerSupportID,
                   RepairLocationID                     = @RepairLocationIDClaim,
                   ServiceChannelID                     = @ServiceChannelID,
                   TimeIDAssignDownload                 = @TimeIDAssignDownload,
                   TimeIDAssignSent                     = @TimeIDAssignSent,
                   TimeIDCancelled                      = @TimeIDCancelled,
                   TimeIDClosed                         = @TimeIDClosed,
                   TimeIDEstimate                       = @TimeIDEstimate,
                   TimeIDInspection                     = @TimeIDInspection,
                   TimeIDNew                            = @TimeIDNew,
                   TimeIDReclosed                       = @TimeIDReclosed,
                   TimeIDRepairComplete                 = @TimeIDRepairComplete,
                   TimeIDRepairStarted                  = @TimeIDRepairStarted,
                   TimeIDReopened                       = @TimeIDReopened,
                   TimeIDVoided                         = @TimeIDVoided,
                   VehicleNumber                        = @ClaimAspectNumber,
                   VehicleLicensePlateStateID           = @VehicleLicensePlateStateID,
                   AuditedEstimateAgreedFlag            = @AuditedEstimateAgreedFlag,
                   AuditedEstimateBettermentAmt         = @AuditedEstimateBettermentAmt,
                   AuditedEstimateDeductibleAmt         = @AuditedEstimateDeductibleAmt,
                   AuditedEstimateGrossAmt              = @AuditedEstimateGrossAmt,
                   AuditedEstimateNetAmt                = @AuditedEstimateNetAmt,
                   AuditedEstimateOtherAdjustmentAmt    = @AuditedEstimateOtherAdjustmentAmt,
                   AuditedEstimatewoBettAmt             = @AuditedEstimatewoBettAmt,
                   CFProgramFlag                        = @CFProgramFlag,
				   ClaimantName							= @ClaimantName,		-- (SWR 203304)
                   ClaimStatusCD                        = @ClaimStatusCD,
                   ClientClaimNumber                    = @ClientClaimNumber,
                   CycleTimeAssignToEstBusDay           = @CycleTimeAssignToEstBusDay,
                   CycleTimeAssignToEstCalDay           = @CycleTimeAssignToEstCalDay,
                   CycleTimeAssignToEstHrs              = @CycleTimeAssignToEstHrs,
                   CycleTimeEstToCloseBusDay            = @CycleTimeEstToCloseBusDay,
                   CycleTimeEstToCloseCalDay            = @CycleTimeEstToCloseCalDay,
                   CycleTimeEstToCloseHrs               = @CycleTimeEstToCloseHrs,
                   CycleTimeInspectionToEstBusDay       = @CycleTimeInspectionToEstBusDay,
                   CycleTimeInspectionToEstCalDay       = @CycleTimeInspectionToEstCalDay,
                   CycleTimeInspectionToEstHrs          = @CycleTimeInspectionToEstHrs,
                   CycleTimeNewToCloseBusDay            = @CycleTimeNewToCloseBusDay,
                   CycleTimeNewToCloseCalDay            = @CycleTimeNewToCloseCalDay,
                   CycleTimeNewToCloseHrs               = @CycleTimeNewToCloseHrs,
                   CycleTimeNewToEstBusDay              = @CycleTimeNewToEstBusDay,
                   CycleTimeNewToEstCalDay              = @CycleTimeNewToEstCalDay,
                   CycleTimeNewToEstHrs                 = @CycleTimeNewToEstHrs,
                   CycleTimeNewToInspectionBusDay       = @CycleTimeNewToInspectionBusDay,
                   CycleTimeNewToInspectionCalDay       = @CycleTimeNewToInspectionCalDay,
                   CycleTimeNewToInspectionHrs          = @CycleTimeNewToInspectionHrs,
                   CycleTimeNewToRecloseBusDay          = @CycleTimeNewToRecloseBusDay,
                   CycleTimeNewToRecloseCalDay          = @CycleTimeNewToRecloseCalDay,
                   CycleTimeNewToRecloseHrs             = @CycleTimeNewToRecloseHrs,
                   CycleTimeRepairStartToEndBusDay      = @CycleTimeRepairStartToEndBusDay,
                   CycleTimeRepairStartToEndCalDay      = @CycleTimeRepairStartToEndCalDay,
                   CycleTimeRepairStartToEndHrs         = @CycleTimeRepairStartToEndHrs,
                   DemoFlag                             = @DemoFlag,
	               EarlyBillAuditedGrossAmt             = @EarlyBillAuditedGrossAmt,
	               -- -----------------------------------------------------------
				   --  MAH 07-jun-2012 Obsolete:
				   -- -----------------------------------------------------------
	               EarlyBillAuditedAddtlLineItemAmt     = NULL,
	               EarlyBillAuditedLineItemCorrectionAmt = NULL,
	               EarlyBillAuditedMissingLineItemAmt   = NULL,
				   -- -----------------------------------------------------------
                   EnabledFlag                          = @EnabledFlag,
                   ExposureCD                           = @ExposureCD,
                   FeeRevenueAmt                        = @FeeRevenueAmt,
                   FinalAuditedSuppAgreedFlag           = @FinalAuditedSuppAgreedFlag,
                   FinalAuditedSuppGrossAmt             = @FinalAuditedSuppGrossAmt,
                   FinalAuditedSuppBettermentAmt        = @FinalAuditedSuppBettermentAmt,
                   FinalAuditedSuppwoBettAmt            = @FinalAuditedSuppwoBettAmt,
                   FinalAuditedSuppDeductibleAmt        = @FinalAuditedSuppDeductibleAmt,
                   FinalAuditedSuppOtherAdjustmentAmt   = @FinalAuditedSuppOtherAdjustmentAmt,
                   FinalAuditedSuppNetAmt               = @FinalAuditedSuppNetAmt,
                   FinalEstimateBettermentAmt           = @FinalEstimateBettermentAmt,
                   FinalEstimateDeductibleAmt           = @FinalEstimateDeductibleAmt,
                   FinalEstimateGrossAmt                = @FinalEstimateGrossAmt,
                   FinalEstimateNetAmt                  = @FinalEstimateNetAmt,
                   FinalEstimateOtherAdjustmentAmt      = @FinalEstimateOtherAdjustmentAmt,
                   FinalEstimatewoBettAmt               = @FinalEstimatewoBettAmt,
                   FinalSupplementBettermentAmt         = @FinalSupplementBettermentAmt,
                   FinalSupplementDeductibleAmt         = @FinalSupplementDeductibleAmt,
                   FinalSupplementGrossAmt              = @FinalSupplementGrossAmt,
                   FinalSupplementNetAmt                = @FinalSupplementNetAmt,
                   FinalSupplementOtherAdjustmentAmt    = @FinalSupplementOtherAdjustmentAmt,
                   FinalSupplementwoBettAmt             = @FinalSupplementwoBettAmt,
                   IndemnityAmount                      = @IndemnityAmount,
				   InsuredName							= @InsuredName,		-- (SWR 203304)
                   LaborRateBodyAmt                     = @LaborRateBodyAmt,
                   LaborRateFrameAmt                    = @LaborRateFrameAmt,
                   LaborRateMechAmt                     = @LaborRateMechAmt,
                   LaborRateRefinishAmt                 = @LaborRateRefinishAmt,
                   LaborRepairAmt                       = @LaborRepairAmt,
                   LaborReplaceAmt                      = @LaborReplaceAmt,
                   LaborTotalAmt                        = @LaborTotalAmt,
                   LossDate                             = @LossDate,
                   LynxID                               = @LynxID,
                   MaxEstSuppSequenceNumber             = @MaxEstSuppSequenceNumber,
                   OriginalEstimateBettermentAmt        = @OriginalEstimateBettermentAmt,
                   OriginalEstimateDeductibleAmt        = @OriginalEstimateDeductibleAmt,
                   OriginalEstimateGrossAmt             = @OriginalEstimateGrossAmt,
                   OriginalEstimateNetAmt               = @OriginalEstimateNetAmt,
                   OriginalEstimateOtherAdjustmentAmt   = @OriginalEstimateOtherAdjustmentAmt,
                   OriginalEstimatewoBettAmt            = @OriginalEstimatewoBettAmt,
				   -- -----------------------------------------------------------------------------------------------
				   -- Begin MAH 06-Jun-2012 Comment:
				   -- The following code was incorrectly setting the Original Audited Amount to the Original Amounts.  
				   -- -----------------------------------------------------------------------------------------------
				   -- OriginalAuditedEstimateBettermentAmt = @OriginalEstimateBettermentAmt,
				   -- OriginalAuditedEstimateDeductibleAmt = @OriginalEstimateDeductibleAmt,
				   -- OriginalAuditedEstimateGrossAmt      = @OriginalEstimateGrossAmt,
				   -- OriginalAuditedEstimateNetAmt        = @OriginalEstimateNetAmt,
				   -- OriginalAuditedEstimateOtherAdjustmentAmt = @OriginalEstimateOtherAdjustmentAmt,
				   -- OriginalAuditedEstimatewoBettAmt     = @OriginalEstimatewoBettAmt,
				   -- -----------------------------------------------------------------------------------------------
                   OriginalAuditedEstimateBettermentAmt = @OriginalAuditedEstimateBettermentAmt, -- MAH 06-Jun-2012
                   OriginalAuditedEstimateDeductibleAmt = @OriginalAuditedEstimateDeductibleAmt, -- MAH 06-Jun-2012
                   OriginalAuditedEstimateGrossAmt      = @OriginalAuditedEstimateGrossAmt, -- MAH 06-Jun-2012
                   OriginalAuditedEstimateNetAmt        = @OriginalAuditedEstimateNetAmt, -- MAH 06-Jun-2012
                   OriginalAuditedEstimateOtherAdjustmentAmt = @OriginalAuditedEstimateOtherAdjustmentAmt, -- MAH 06-Jun-2012
                   OriginalAuditedEstimatewoBettAmt     = @OriginalAuditedEstimatewoBettAmt, -- MAH 06-Jun-2012
				   -- -----------------------------------------------------------------------------------------------
				   -- End MAH 06-Jun-2012 Comment:
				   -- -----------------------------------------------------------------------------------------------
                   PartsAFMKReplacedAmt                 = @PartsAFMKReplacedAmt,
                   PartsLKQReplacedAmt                  = @PartsLKQReplacedAmt,
                   PartsOEMDiscountAmt                  = @PartsOEMDiscountAmt,
                   PartsOEMReplacedAmt                  = @PartsOEMReplacedAmt,
                   PartsRemanReplacedAmt                = @PartsRemanReplacedAmt,
                   PartsTotalReplacedAmt                = @PartsTotalReplacedAmt,
                   PhotosAttachedFlag                   = @PhotosAttachedFlag,
                   PolicyNumber                         = @PolicyNumber,
                   PolicyDeductibleAmt                  = @PolicyDeductibleAmt,
                   PolicyLimitAmt                       = @PolicyLimitAmt,
                   PolicyRentalDeductibleAmt            = @PolicyRentalDeductibleAmt,
                   PolicyRentalLimitAmt                 = @PolicyRentalLimitAmt,
                   PolicyRentalMaxDays                  = @PolicyRentalMaxDays,
                   ProgramCD                            = @ProgramCD,
                   ReinspectionCount                    = @ReinspectionCount,
                   ReinspectionDeviationAmt             = @ReinspectionDeviationAmt,
                   ReinspectionDeviationCount           = @ReinspectionDeviationCount,
                   RentalDays                           = @RentalDays,
                   RentalAmount                         = @RentalAmount,
                   RentalCostPerDay                     = @RentalCostPerDay,
                   ServiceLossOfUseFlag                 = @ServiceLossOfUseFlag,
                   ServiceSubrogationFlag               = @ServiceSubrogationFlag,
                   ServiceTotalLossFlag                 = @ServiceTotalLossFlag,
                   ServiceTotalTheftFlag                = @ServiceTotalTheftFlag,
                   SupplementTotalAmt                   = @SupplementTotalAmt,
                   TotalConcessionLaborRate             = @TotalConcessionLaborRate,
                   TotalConcessionLaborRateComment      = @TotalConcessionLaborRateComment,
                   TotalConcessionUnrelatedDamage       = @TotalConcessionUnrelatedDamage,
                   TotalConcessionUnrelatedDamageComment = @TotalConcessionUnrelatedDamageComment,
                   TotalConcessionPartsUsage            = @TotalConcessionPartsUsage,
                   TotalConcessionPartsUsageComment     = @TotalConcessionPartsUsageComment,
                   TotalConcessionTotalLoss             = @TotalConcessionTotalLoss,
                   TotalConcessionTotalLossComment      = @TotalConcessionTotalLossComment,
                   VehicleDriveableFlag                 = @VehicleDriveableFlag,
                   VehicleLicensePlateNumber            = @VehicleLicensePlateNumber,
                   VehicleMake                          = @VehicleMake,
                   VehicleModel                         = @VehicleModel,
                   VehicleYear                          = @VehicleYear,
                   VehicleVIN                           = @VehicleVIN
              WHERE ClaimAspectID = @ClaimAspectID

            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error updating utb_dtwh_fact_claim', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
        ELSE
        BEGIN
            -- We have to add a new fact record
            
            INSERT INTO dbo.utb_dtwh_fact_claim
            (
                ClaimAspectID,
                AssignmentTypeClosingID,
                AssignmentTypeID,
                ClaimLocationID,
                ClaimSourceID,
                CoverageTypeID,
                CustomerID,
                DispositionTypeID,
                LynxHandlerAnalystID,
                LynxHandlerOwnerID,
                LynxHandlerSupportID,
                RepairLocationID,
                ServiceChannelID,
                TimeIDAssignDownload,
                TimeIDAssignSent,
                TimeIDCancelled,
                TimeIDClosed,
                TimeIDEstimate,
                TimeIDInspection,
                TimeIDNew,
                TimeIDReclosed,
                TimeIDRepairComplete,
                TimeIDRepairStarted,
                TimeIDReopened,
                TimeIDVoided,
                VehicleNumber,
                VehicleLicensePlateStateID,
                AuditedEstimateAgreedFlag,
                AuditedEstimateBettermentAmt,
                AuditedEstimateDeductibleAmt,
                AuditedEstimateGrossAmt,
                AuditedEstimateNetAmt,
                AuditedEstimateOtherAdjustmentAmt,
                AuditedEstimatewoBettAmt,
                CFProgramFlag,
				ClaimantName,							-- (SWR 203304)
                ClaimStatusCD,
                ClientClaimNumber,
                CycleTimeAssignToEstBusDay,
                CycleTimeAssignToEstCalDay,
                CycleTimeAssignToEstHrs,
                CycleTimeEstToCloseBusDay,
                CycleTimeEstToCloseCalDay,
                CycleTimeEstToCloseHrs,
                CycleTimeInspectionToEstBusDay,
                CycleTimeInspectionToEstCalDay,
                CycleTimeInspectionToEstHrs,
                CycleTimeNewToCloseBusDay,
                CycleTimeNewToCloseCalDay,
                CycleTimeNewToCloseHrs,
                CycleTimeNewToEstBusDay,
                CycleTimeNewToEstCalDay,
                CycleTimeNewToEstHrs,
                CycleTimeNewToInspectionBusDay,
                CycleTimeNewToInspectionCalDay,
                CycleTimeNewToInspectionHrs,
                CycleTimeNewToRecloseBusDay,
                CycleTimeNewToRecloseCalDay,
                CycleTimeNewToRecloseHrs,
                CycleTimeRepairStartToEndBusDay,
                CycleTimeRepairStartToEndCalDay,
                CycleTimeRepairStartToEndHrs,
                DemoFlag,
	            EarlyBillAuditedAddtlLineItemAmt,
	            EarlyBillAuditedGrossAmt,
	            EarlyBillAuditedLineItemCorrectionAmt,
	            EarlyBillAuditedMissingLineItemAmt,
                EnabledFlag,
                ExposureCD,
                FeeRevenueAmt,
                FinalAuditedSuppAgreedFlag,
                FinalAuditedSuppGrossAmt,
                FinalAuditedSuppBettermentAmt,
                FinalAuditedSuppwoBettAmt,
                FinalAuditedSuppDeductibleAmt,
                FinalAuditedSuppOtherAdjustmentAmt,
                FinalAuditedSuppNetAmt,
                FinalEstimateBettermentAmt,
                FinalEstimateDeductibleAmt,
                FinalEstimateGrossAmt,
                FinalEstimateNetAmt,
                FinalEstimateOtherAdjustmentAmt,
                FinalEstimatewoBettAmt,
                FinalSupplementBettermentAmt,
                FinalSupplementDeductibleAmt,
                FinalSupplementGrossAmt,
                FinalSupplementNetAmt,
                FinalSupplementOtherAdjustmentAmt,
                FinalSupplementwoBettAmt,
                IndemnityAmount,
				InsuredName,							-- (SWR 203304)
                LaborRateBodyAmt,
                LaborRateFrameAmt,
                LaborRateMechAmt,
                LaborRateRefinishAmt,
                LaborRepairAmt,
                LaborReplaceAmt,
                LaborTotalAmt,
                LossDate,
                LynxID,
                MaxEstSuppSequenceNumber,
                OriginalEstimateBettermentAmt,
                OriginalEstimateDeductibleAmt,
                OriginalEstimateGrossAmt,
                OriginalEstimateNetAmt,
                OriginalEstimateOtherAdjustmentAmt,
                OriginalEstimatewoBettAmt,
                OriginalAuditedEstimateBettermentAmt,
                OriginalAuditedEstimateDeductibleAmt,
                OriginalAuditedEstimateGrossAmt,
                OriginalAuditedEstimateNetAmt,
                OriginalAuditedEstimateOtherAdjustmentAmt,
                OriginalAuditedEstimatewoBettAmt,
                PartsAFMKReplacedAmt,
                PartsLKQReplacedAmt,
                PartsOEMDiscountAmt,
                PartsOEMReplacedAmt,
                PartsRemanReplacedAmt,
                PartsTotalReplacedAmt,
                PhotosAttachedFlag,
                PolicyNumber,
                PolicyDeductibleAmt,
                PolicyLimitAmt,
                PolicyRentalDeductibleAmt,
                PolicyRentalLimitAmt,
                PolicyRentalMaxDays,
                ProgramCD,
                ReinspectionCount,
                ReinspectionDeviationAmt,
                ReinspectionDeviationCount,
                RentalDays,
                RentalAmount,
                RentalCostPerDay,
                ServiceLossOfUseFlag,
                ServiceSubrogationFlag,
                ServiceTotalLossFlag,
                ServiceTotalTheftFlag,
                SupplementTotalAmt,
                TotalConcessionLaborRate,
                TotalConcessionLaborRateComment,
                TotalConcessionUnrelatedDamage,
                TotalConcessionUnrelatedDamageComment,
                TotalConcessionPartsUsage,
                TotalConcessionPartsUsageComment,
                TotalConcessionTotalLoss,
                TotalConcessionTotalLossComment,
                VehicleDriveableFlag,
                VehicleLicensePlateNumber,
                VehicleMake,
                VehicleModel,
                VehicleYear,
                VehicleVIN
            )
            VALUES
            (                
                @ClaimAspectID,
                @AssignmentTypeClosingID,
                @AssignmentTypeID,
                @ClaimLocationID,
                @ClaimSourceID,
                @CoverageTypeID,
                @CustomerID,
                @DispositionTypeID,
                @LynxhandlerAnalystID,
                @LynxhandlerOwnerID,
                @LynxhandlerSupportID,
                @RepairLocationIDClaim,
                @ServiceChannelID,
                @TimeIDAssignDownload,
                @TimeIDAssignSent,
                @TimeIDCancelled,
                @TimeIDClosed,
                @TimeIDEstimate,
                @TimeIDInspection,
                @TimeIDNew,
                @TimeIDReclosed,
                @TimeIDRepairComplete,
                @TimeIDRepairStarted,
                @TimeIDReopened,
                @TimeIDVoided,
                @ClaimAspectNumber,
                @VehicleLicensePlateStateID,
                @AuditedEstimateAgreedFlag,
                @AuditedEstimateBettermentAmt,
                @AuditedEstimateDeductibleAmt,
                @AuditedEstimateGrossAmt,
                @AuditedEstimateNetAmt,
                @AuditedEstimateOtherAdjustmentAmt,
                @AuditedEstimatewoBettAmt,
                @CFProgramFlag,
				@ClaimantName,							-- (SWR 203304)
                @ClaimStatusCD,
                @ClientClaimNumber,
                @CycleTimeAssignToEstBusDay,
                @CycleTimeAssignToEstCalDay,
                @CycleTimeAssignToEstHrs,
                @CycleTimeEstToCloseBusDay,
                @CycleTimeEstToCloseCalDay,
                @CycleTimeEstToCloseHrs,
                @CycleTimeInspectionToEstBusDay,
                @CycleTimeInspectionToEstCalDay,
                @CycleTimeInspectionToEstHrs,
                @CycleTimeNewToCloseBusDay,
                @CycleTimeNewToCloseCalDay,
                @CycleTimeNewToCloseHrs,
                @CycleTimeNewToEstBusDay,
                @CycleTimeNewToEstCalDay,
                @CycleTimeNewToEstHrs,
                @CycleTimeNewToInspectionBusDay,
                @CycleTimeNewToInspectionCalDay,
                @CycleTimeNewToInspectionHrs,
                @CycleTimeNewToRecloseBusDay,
                @CycleTimeNewToRecloseCalDay,
                @CycleTimeNewToRecloseHrs,
                @CycleTimeRepairStartToEndBusDay,
                @CycleTimeRepairStartToEndCalDay,
                @CycleTimeRepairStartToEndHrs,
                @DemoFlag,
 	            NULL, -- @EarlyBillAuditedAddtlLineItemAmt, -- MAH 07-jun-2012 OBSOLETE
	            @EarlyBillAuditedGrossAmt,
	            NULL, -- @EarlyBillAuditedLineItemCorrectionAmt, -- MAH 07-jun-2012 OBSOLETE
	            NULL, -- @EarlyBillAuditedMissingLineItemAmt, -- MAH 07-jun-2012 OBSOLETE
                @EnabledFlag,
                @ExposureCD,
                @FeeRevenueAmt,
                @FinalAuditedSuppAgreedFlag,
                @FinalAuditedSuppGrossAmt,
                @FinalAuditedSuppBettermentAmt,
                @FinalAuditedSuppwoBettAmt,
                @FinalAuditedSuppDeductibleAmt,
                @FinalAuditedSuppOtherAdjustmentAmt,
                @FinalAuditedSuppNetAmt,
                @FinalEstimateBettermentAmt,
                @FinalEstimateDeductibleAmt,
                @FinalEstimateGrossAmt,
                @FinalEstimateNetAmt,
                @FinalEstimateOtherAdjustmentAmt,
                @FinalEstimatewoBettAmt,
                @FinalSupplementBettermentAmt,
                @FinalSupplementDeductibleAmt,
                @FinalSupplementGrossAmt,
                @FinalSupplementNetAmt,
                @FinalSupplementOtherAdjustmentAmt,
                @FinalSupplementwoBettAmt,
                @IndemnityAmount,
				@InsuredName,							-- (SWR 203304)
                @LaborRateBodyAmt,
                @LaborRateFrameAmt,
                @LaborRateMechAmt,
                @LaborRateRefinishAmt,
                @LaborRepairAmt,
                @LaborReplaceAmt,
                @LaborTotalAmt,
                @LossDate,
                @LynxID,
                @MaxEstSuppSequenceNumber,
                @OriginalEstimateBettermentAmt,
                @OriginalEstimateDeductibleAmt,
                @OriginalEstimateGrossAmt,
                @OriginalEstimateNetAmt,
                @OriginalEstimateOtherAdjustmentAmt,
                @OriginalEstimatewoBettAmt,
                @OriginalAuditedEstimateBettermentAmt,
                @OriginalAuditedEstimateDeductibleAmt,
                @OriginalAuditedEstimateGrossAmt,
                @OriginalAuditedEstimateNetAmt,
                @OriginalAuditedEstimateOtherAdjustmentAmt,
                @OriginalAuditedEstimatewoBettAmt,
                @PartsAFMKReplacedAmt,
                @PartsLKQReplacedAmt,
                @PartsOEMDiscountAmt,
                @PartsOEMReplacedAmt,
                @PartsRemanReplacedAmt,
                @PartsTotalReplacedAmt,
                @PhotosAttachedFlag,
                @PolicyNumber,
                @PolicyDeductibleAmt,
                @PolicyLimitAmt,
                @PolicyRentalDeductibleAmt,
                @PolicyRentalLimitAmt,
                @PolicyRentalMaxDays,
                @ProgramCD,
                @ReinspectionCount,
                @ReinspectionDeviationAmt,
                @ReinspectionDeviationCount,
                @RentalDays,
                @RentalAmount,
                @RentalCostPerDay,
                @ServiceLossOfUseFlag,
                @ServiceSubrogationFlag,
                @ServiceTotalLossFlag,
                @ServiceTotalTheftFlag,
                @SupplementTotalAmt,
                @TotalConcessionLaborRate,
                @TotalConcessionLaborRateComment,
                @TotalConcessionUnrelatedDamage,
                @TotalConcessionUnrelatedDamageComment,
                @TotalConcessionPartsUsage,
                @TotalConcessionPartsUsageComment,
                @TotalConcessionTotalLoss,
                @TotalConcessionTotalLossComment,
                @VehicleDriveableFlag,
                @VehicleLicensePlateNumber,
                @VehicleMake,
                @VehicleModel,
                @VehicleYear,
                @VehicleVIN
            )
            
            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_fact_claim', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
       
        
        -- Now using the table containing the estimates, let's go ahead and create a cursor to compile estimate fact data
        
        DECLARE csrEstimate CURSOR FAST_FORWARD FOR
          SELECT  DocumentID,
                  AssignmentID,
                  DocumentSourceName,
                  VanSourceFlag,
                  SequenceNumber,
                  CreatedDate
            FROM  @tmpDocument
            WHERE OriginalRepairTotal IS NOT NULL
              AND VanSourceFlag = 1
            
        OPEN csrEstimate

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        SELECT  @DocumentID = NULL,
                @AssignmentID = NULL,
                @DocumentSourceName = NULL,
                @DocumentSourceNameStandard=NULL,
                @VanSourceFlag = NULL,
                @VanSourceFLagStandard = NULL,
                @SequenceNumber = NULL,
                @CreatedDate = NULL,
				@RepairLocationIDEstimate = NULL -- MAH 30-may-2012 - Initialize so the utb_dtwh_fact_claim update at the end does not pickup the wrong Shop.
                    
        FETCH NEXT FROM csrEstimate
          INTO  @DocumentID,
                @AssignmentID,
                @DocumentSourceName,
                @VanSourceFlag,
                @SequenceNumber,
                @CreatedDate
    
        WHILE @@FETCH_STATUS = 0
        BEGIN
            -- Summarize Estimate data

            -- Initialize the work variables for this estimate
                        
            SELECT  @DocumentSourceID = NULL,
                    @RepairLocationIDEstimate = NULL,
                    @TimeIDReceived = NULL,
                    @AdjAppearanceAllowanceAmount = NULL,
                    @AdjBettermentAmount = NULL,
                    @AdjDeductibleAmount = NULL,
                    @AdjOtherAmount = NULL,
                    @AdjRelatedPriorDamAmount = NULL,
                    @AdjUnrelatedPriorDamAmount = NULL,
                    @DaysSinceAssignment = NULL,
                    @EstSuppSequenceNumber = NULL,
                    @LaborBodyHours = NULL,
                    @LaborBodyRate = NULL,
                    @LaborBodyAmount = NULL,
                    @LaborFrameHours = NULL,
                    @LaborFrameRate = NULL,
                    @LaborFrameAmount = NULL,
                    @LaborMechHours = NULL,
                    @LaborMechRate = NULL,
                    @LaborMechAmount = NULL,
                    @LaborRefinishHours = NULL,
                    @LaborRefinishRate = NULL,
                    @LaborRefinishAmount = NULL,
                    @LaborTotalHours = NULL,
                    @LaborTotalAmount = NULL,
                    @MaterialsHours = NULL,
                    @MaterialsRate = NULL,
                    @MaterialsAmount = NULL,
                    @OtherAmount = NULL,
                    @NetAmount = NULL,
                    @PartDiscountAmount = NULL,
                    @PartMarkupAmount = NULL,
                    @PartsNewAmount = NULL,
                    @PartsLKQAmount = NULL,
                    @PartsAFMKAmount = NULL,
                    @PartsRemanAmount = NULL,
                    @PartsTotalAmount = NULL,
                    @StorageAmount = NULL,
                    @SubletAmount = NULL,
                    @TaxLaborAmount = NULL,
                    @TaxPartsAmount = NULL,
                    @TaxMaterialsAmount = NULL,
                    @TaxTotalAmount = NULL,
                    @TotalAmount = NULL,
                    @TowingAmount = NULL,
                    @BodyRepairHrs = NULL,
                    @BodyReplaceHrs = NULL,
                    @FrameRepairHrs = NULL,
                    @FrameReplaceHrs = NULL,
                    @MechanicalRepairHrs = NULL,
                    @MechanicalReplaceHrs = NULL,
                    @RefinishRepairHrs = NULL,
                    @RefinishReplaceHrs = NULL,
                    @EstimateBalancedFlag = 0
                        
            
            -- Now compile the information on the estimate fact.  Add dimension records if missing.
    
            -- Estimate Source
            
            SELECT @DocumentSourceID = NULL

            SELECT  @DocumentSourceID = DocumentSourceID
              FROM  dbo.utb_dtwh_dim_document_source WITH (NOLOCK)
              WHERE @DocumentSourceName IS NOT NULL AND DocumentSourceName = @DocumentSourceName
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @DocumentSourceID IS NULL AND @DocumentSourceName IS NOT NULL
            BEGIN
                -- Doesn't exist, add the dimension record
            
                INSERT INTO dbo.utb_dtwh_dim_document_source (DocumentSourceName, ElectronicSourceFlag)
                  VALUES (@DocumentSourceName,
                          ISNULL(@VanSourceFlag,0))
        
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error(s) inserting into utb_dtwh_dim_document_source: DocID=%i DocSourceName=%s', 16, 1, @ProcName,@DocumentID,@DocumentSourceName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @DocumentSourceID = SCOPE_IDENTITY()
            END


            -- Now get the "standard" source for this shop
            
            SELECT  @DocumentSourceNameStandard = CASE
                                                    WHEN tmp.ShopLocationID IS NOT NULL THEN IsNull(cmsl.RoutingCD, 'Fax')
                                                    WHEN tmp.AppraiserID IS NOT NULL THEN IsNull(cma.RoutingCD, 'Fax')
                                                    ELSE 'Fax'
                                                  END,
                    @VanSourceFlagStandard = CASE 
                                               WHEN tmp.ShopLocationID IS NOT NULL AND cmsl.RoutingCD IS NOT NULL THEN 1
                                               WHEN tmp.AppraiserID IS NOT NULL AND cma.RoutingCD IS NOT NULL THEN 1
                                               ELSE 0
                                             END 
              FROM  @tmpAssignment tmp
              LEFT JOIN dbo.utb_shop_location sl WITH (NOLOCK) ON (tmp.ShopLocationID = sl.ShopLocationID)
              LEFT JOIN dbo.utb_communication_method cmsl WITH (NOLOCK) ON (sl.PreferredCommunicationMethodID = cmsl.CommunicationMethodID)
              LEFT JOIN dbo.utb_appraiser a WITH (NOLOCK) ON (tmp.AppraiserID = a.AppraiserID)
              LEFT JOIN dbo.utb_communication_method cma WITH (NOLOCK) ON (a.PreferredCommunicationMethodID = cma.CommunicationMethodID)
              WHERE AssignmentID = @AssignmentID
                           
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Verify the dimension record for the communication mechanism used by the chosen shop location exists
            
            SELECT @StandardDocumentSourceID = NULL
                                                       
            SELECT  @StandardDocumentSourceID = DocumentSourceID
              FROM  dbo.utb_dtwh_dim_document_source WITH (NOLOCK)
              WHERE @DocumentSourceNameStandard IS NOT NULL AND DocumentSourceName = @DocumentSourceNameStandard
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @StandardDocumentSourceID IS NULL AND @DocumentSourceNameStandard IS NOT NULL
            BEGIN
                -- Doesn't exist, add the document source dimension record
                INSERT INTO dbo.utb_dtwh_dim_document_source (DocumentSourceName, ElectronicSourceFlag)
                  VALUES (@DocumentSourceNameStandard,
                          ISNULL(@VanSourceFlagStandard, 0))
    
                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error(s) inserting into utb_dtwh_dim_document_source: DocID=%i DocSourceName=%s', 16, 1, @ProcName,@DocumentID,@DocumentSourceName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @StandardDocumentSourceID = SCOPE_IDENTITY()
            END


            -- Repair Location for the estimate            
                    
            IF (@AssignmentID IS NOT NULL) AND ((SELECT ShopLocationID FROM @tmpAssignment WHERE AssignmentID = @AssignmentID AND ShopLocationID IS NOT NULL) IS NOT NULL)
            BEGIN
                SELECT  @RepairLocationIDEstimate = RepairLocationID
                  FROM  dbo.utb_dtwh_dim_repair_location WITH (NOLOCK)
                  WHERE ShopLocationID = (SELECT  ShopLocationID
                                            FROM  @tmpAssignment
                                            WHERE AssignmentID = @AssignmentID)

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                IF @RepairLocationIDEstimate IS NULL
                BEGIN
                    -- Doesn't exist, add the dimension record
            
                    INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode)  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                      SELECT  sl.ShopLocationID,
                              @StandardDocumentSourceID,
                              (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                              sl.ReferralFlag,  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                              sl.CertifiedFirstFlag,
                              IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                              IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                              sl.ProgramFlag,
                              sl.PPGCTSCustomerFlag,
                              sl.Name,
                              ep.Name,
                              sl.AddressZip 
                        FROM  dbo.utb_shop_location sl WITH (NOLOCK)
                        LEFT JOIN dbo.utb_estimate_package ep WITH (NOLOCK) ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                        LEFT JOIN dbo.utb_zip_code zc WITH (NOLOCK) ON (sl.AddressZip = zc.Zip)
                        WHERE sl.ShopLocationID = (SELECT  ShopLocationID
                                                     FROM  @tmpAssignment
                                                     WHERE AssignmentID = @AssignmentID)
        
                    IF @@ERROR <> 0
                    BEGIN
                        RAISERROR('%s: SQL Server Error inserting shop location id %u into utb_dtwh_dim_repair_location for claim aspect id %u', 16, 1, @ProcName, @RepairLocationIDEstimate, @ClaimAspectID) -- MAH 23-may-2012 Changed from @RepairLocationIDClaim to @RepairLocationIDEstimate
                        ROLLBACK TRANSACTION
                        RETURN
                    END
            
                    SELECT @RepairLocationIDEstimate = SCOPE_IDENTITY() -- MAH 23-may-2012 Changed from @RepairLocationIDClaim to @RepairLocationIDEstimate
                END
                ELSE
                BEGIN
                    -- We need to update the repair facilities information
                
                    UPDATE  dbo.utb_dtwh_dim_repair_location
                      SET   StandardDocumentSourceID = @StandardDocumentSourceID,
                            StateID = (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                            RRPProgramFlag = sl.ReferralFlag,  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                            CFProgramFlag = sl.CertifiedFirstFlag,
                            City = IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                            County = IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                            LYNXSelectProgramFlag = sl.ProgramFlag,
                            PPGPaintBuyerFlag = sl.PPGCTSCustomerFlag,
                            ShopLocationName = sl.Name,
                            StandardEstimatePackage = ep.Name,
                            ZipCode = sl.AddressZip 
                      FROM  dbo.utb_dtwh_dim_repair_location drl
                      LEFT JOIN dbo.utb_shop_location sl ON (drl.ShopLocationID = sl.ShopLocationID)
                      LEFT JOIN dbo.utb_estimate_package ep ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                      LEFT JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.Zip)
                      WHERE drl.RepairLocationID = @RepairLocationIDEstimate  -- MAH 23-may-2012 Changed from @RepairLocationIDClaim to @RepairLocationIDEstimate

                    IF @@ERROR <> 0
                    BEGIN
                        RAISERROR('%s: SQL Server Error updating shop location id %u in utb_dtwh_dim_repair_location for claim aspect id %u', 16, 1, @ProcName, @RepairLocationIDEstimate, @ClaimAspectID)  -- MAH 23-may-2012 Changed from @RepairLocationIDClaim to @RepairLocationIDEstimate
                        ROLLBACK TRANSACTION
                        RETURN
                    END
                END                                   
            END
                 
        
            -- Time ID Received

            SELECT  @TimeIDReceived = TimeID
              FROM  dbo.utb_dtwh_dim_time WITH (NOLOCK)
              WHERE DateValue = DateAdd(hour, DatePart(hour, @CreatedDate), Convert(datetime, Convert(varchar(15), @CreatedDate, 101)))

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Days Since Assignment
            
            SELECT  @DaysSinceAssignment = dbo.ufnUtilityBusinessDaysDiff(tmp.AssignmentDate, @CreatedDate)
              FROM  @tmpAssignment tmp
              WHERE AssignmentID = @AssignmentID
              
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

              
            -- Sequence Number
           
            SELECT @EstSuppSequenceNumber = IsNull(@SequenceNumber, 0)
           
           
            -- Body Repair and Replace hours
            SELECT @BodyRepairHrs = OriginalHrsRepair,
                   @BodyReplaceHrs = OriginalHrsReplace
            FROM dbo.utb_estimate_summary es WITH (NOLOCK)
            WHERE es.DocumentID = @DocumentID
              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDBodyLabor

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
            -- Frame Repair and Replace hours
            SELECT @FrameRepairHrs = OriginalHrsRepair,
                   @FrameReplaceHrs = OriginalHrsReplace
            FROM dbo.utb_estimate_summary es WITH (NOLOCK)
            WHERE es.DocumentID = @DocumentID
              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDFrameLabor

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
            -- Mechanical Repair and Replace hours
            SELECT @MechanicalRepairHrs = OriginalHrsRepair,
                   @MechanicalReplaceHrs = OriginalHrsReplace
            FROM dbo.utb_estimate_summary es WITH (NOLOCK)
            WHERE es.DocumentID = @DocumentID
              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDMechLabor

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
            -- Refinish Repair and Replace hours
            SELECT @RefinishRepairHrs = OriginalHrsRepair,
                   @RefinishReplaceHrs = OriginalHrsReplace
            FROM dbo.utb_estimate_summary es WITH (NOLOCK)
            WHERE es.DocumentID = @DocumentID
              AND es.EstimateSummaryTypeID = @EstimateSummaryTypeIDRefinishLabor

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
            -- Estimate balanced flag
            SELECT @EstimateBalancedFlag = DetailEditableFlag
            FROM dbo.utb_estimate WITH (NOLOCK)
            WHERE DocumentID = @DocumentID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
            
            -- Body Labor Hours/Rate/Amount
           
            SELECT  @LaborBodyHours  = OriginalHrs,
                    @LaborBodyRate   = OriginalUnitAmt,
                    @LaborBodyAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDBodyLabor
               
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

               
            -- Frame Labor Hours/Rate/Amount
           
            SELECT  @LaborFrameHours  = OriginalHrs,
                    @LaborFrameRate   = OriginalUnitAmt,
                    @LaborFrameAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDFrameLabor
               
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

               
            -- Mech Labor Hours/Rate/Amount
           
            SELECT  @LaborMechHours  = OriginalHrs,
                    @LaborMechRate   = OriginalUnitAmt,
                    @LaborMechAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDMechLabor
               
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

               
            -- Refinish Labor Hours/Amount
           
            SELECT  @LaborRefinishHours  = SUM(OriginalHrs),
                    @LaborRefinishRate   = SUM(OriginalUnitAmt),
                    @LaborRefinishAmount = SUM(OriginalExtendedAmt)
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID IN (@EstimateSummaryTypeIDPaintLabor, @EstimateSummaryTypeIDRefinishLabor)
           
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Total Labor Hours
           
            SELECT  @LaborTotalHours = SUM(OriginalHrs)
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID IN (@EstimateSummaryTypeIDBodyLabor,
                                              @EstimateSummaryTypeIDFrameLabor,
                                              @EstimateSummaryTypeIDMechLabor,
                                              @EstimateSummaryTypeIDPaintLabor, 
                                              @EstimateSummaryTypeIDRefinishLabor)
           
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Total Labor Amount
           
            SELECT  @LaborTotalAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'TT'
                                                 AND Name = 'LaborTotal')
           
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- OEM Parts Amount
           
            SELECT  @PartsNewAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'OT'
                                                 AND Name = 'OEMParts')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

             
            -- LKQ Parts Amount
           
            SELECT  @PartsLKQAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'OT'
                                                 AND Name = 'LKQParts')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Aftermarket Parts Amount
           
            SELECT  @PartsAFMKAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'OT'
                                                 AND Name = 'AFMKParts')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Remanufactured Parts Amount
           
            SELECT  @PartsRemanAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'OT'
                                                 AND Name = 'RemanParts')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Parts Discount
           
            SELECT  @PartDiscountAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'PC'
                                                 AND Name = 'PartsDiscount')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

             
            -- Parts Markup
           
            SELECT  @PartMarkupAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'PC'
                                                 AND Name = 'PartsMarkup')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

             
            -- Parts Total Amount (Note for warehousing purposes, we must extract the "Parts Other" amounts prevalent on CCC
            -- estimates from the Parts Total Amount.  This amount will be reclassified as simply "Other".  
           
            SELECT  @PartsOtherTaxableAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'PC'
                                                 AND Name = 'PartsOtherTaxable')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT  @PartsOtherNonTaxableAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'PC'
                                                 AND Name = 'PartsOtherNonTaxable')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @OtherAmount = @PartsOtherTaxableAmount + @PartsOtherNonTaxableAmount
            
            SELECT  @PartsTotalAmount = OriginalExtendedAmt - @OtherAmount
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'TT'
                                                 AND Name = 'PartsTotal')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

             
            -- Materials Total Amount
           
            SELECT  @MaterialsHours  = OriginalHrs,
                    @MaterialsRate   = OriginalUnitAmt,
                    @MaterialsAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'MC'
                                                 AND Name = 'Paint')
            
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            
            -- Storage Amount
           
            SELECT  @StorageAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'AC'
                                                 AND Name = 'Storage')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Sublet Amount
           
            SELECT  @SubletAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'AC'
                                                 AND Name = 'Sublet')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Towing Amount
           
            SELECT  @TowingAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'AC'
                                                 AND Name = 'Towing')

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Tax Amounts
            
            SELECT @TaxRateEstimate = NULL
            
            SELECT  @TaxTotalAmount = Sum(OriginalExtendedAmt),
                    @TaxRateEstimate = SUM(OriginalPct)
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID IN (SELECT  EstimateSummaryTypeID
                                               FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                               WHERE CategoryCD = 'TX'
                                                 AND Name IN ('County', 'Municipal', 'Sales','Other'))

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            IF (@DocumentSourceName = 'CCC' AND EXISTS(SELECT  * 
                                                         FROM  dbo.utb_estimate_summary WITH (NOLOCK) 
                                                         WHERE DocumentID = @DocumentID
                                                           AND OriginalTaxableFlag = 1
                                                           AND EstimateSummaryTypeID IN (SELECT  EstimateSummaryTypeID
                                                                                           FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                                                                           WHERE CategoryCD = 'LC'
                                                                                             AND Name IN ('Body', 'Mechanical', 'Frame', 'Paint')))) OR
               (EXISTS(SELECT  * 
                         FROM  dbo.utb_estimate_summary  WITH (NOLOCK)
                         WHERE DocumentID = @DocumentID
                           AND OriginalTaxableFlag = 1
                           AND EstimateSummaryTypeID IN (SELECT  EstimateSummaryTypeID
                                                           FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                                           WHERE CategoryCD = 'LC'
                                                             AND Name IN ('Body', 'Mechanical', 'Frame', 'Refinish'))))
            BEGIN 
                -- Compute tax on labor
                
                --SELECT @TaxLaborAmount = Round(@LaborTotalAmount * @TaxRateEstimate / 100, 2)
                SELECT @TaxLaborAmount = @LaborTotalAmount  -- does not need to be computed, just extract as is
            END
            ELSE 
            BEGIN 
                -- No tax on labor
                
                SELECT @TaxLaborAmount = 0
            END

            
            SELECT  @PartsTaxableAmount = es.OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary es WITH (NOLOCK)
              LEFT JOIN dbo.utb_estimate_summary_type est WITH (NOLOCK) ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID) 
              WHERE es.DocumentID = @DocumentID
                AND est.CategoryCD = 'PC'
                AND est.Name = 'PartsTaxable'
           
            IF @PartsTaxableAmount > 0
            BEGIN
                -- Compute tax on parts
    
                --SELECT @TaxPartsAmount = Round((@PartsTaxableAmount + @PartsOtherTaxableAmount) * @TaxRateEstimate / 100, 2)
                SELECT @TaxPartsAmount = (@PartsTaxableAmount + @PartsOtherTaxableAmount) -- does not need to be computed for tax rate percentage
            END
            ELSE 
            BEGIN 
                -- No tax on parts
                
                SELECT @TaxPartsAmount = 0
            END                                                            
            
            
            IF EXISTS(SELECT  * 
                        FROM  dbo.utb_estimate_summary WITH (NOLOCK) 
                        WHERE DocumentID = @DocumentID
                          AND OriginalTaxableFlag = 1
                          AND EstimateSummaryTypeID IN (SELECT  EstimateSummaryTypeID
                                                          FROM  dbo.utb_estimate_summary_type WITH (NOLOCK)
                                                          WHERE CategoryCD = 'MC'
                                                            AND Name = 'Paint'))
            BEGIN
                -- Compute tax on Parts and Materials

                --SELECT @TaxMaterialsAmount = Round(@MaterialsAmount * @TaxRateEstimate / 100, 2)
                SELECT @TaxMaterialsAmount = @MaterialsAmount	-- does not need to be computed
            END
            ELSE 
            BEGIN 
                -- No tax on materials
            
                SELECT @TaxMaterialsAmount = 0
            END                    


            -- Total Amount
           
            SELECT  @TotalAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            
            -- Appearance Allowance
           
            SELECT  @AdjAppearanceAllowanceAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDAppAllowance

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Betterment
           
            SELECT  @AdjBettermentAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Deductible
           
            SELECT  @AdjDeductibleAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Other Adjustment
           
            SELECT  @AdjOtherAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDOtherAdj

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Unrelated Prior Damage
           
            SELECT  @AdjUnrelatedPriorDamAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDUPD

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- Related Prior Damage (We currently do not capture this in estimates, set to 0 for now)
            
            SELECT @AdjRelatedPriorDamAmount = 0
            
            
            -- Total Amount
           
            SELECT  @NetAmount = OriginalExtendedAmt
              FROM  dbo.utb_estimate_summary WITH (NOLOCK)
              WHERE DocumentID = @DocumentID
                AND EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            
            IF @Debug = 1
            BEGIN
                PRINT ''
                PRINT 'Estimate Fact Record:'
                SELECT @DocumentID AS DocumentID,
                       @ClaimAspectID AS ClaimAspectID,
                       @CustomerID AS CustomerID,
                       @DocumentSourceID AS DocumentSourceID,
                       @RepairLocationIDEstimate AS RepairLocationID,
                       @TimeIDAssignSent AS TimeIDAssignment,
                       @TimeIDReceived AS TimeIDReceived,
                       IsNull(@AdjAppearanceAllowanceAmount, 0) AS AdjAppearanceAllowanceAmount,
                       IsNull(@AdjBettermentAmount, 0) AS AdjBettermentAmount,
                       IsNull(@AdjDeductibleAmount, 0) AS AdjDeductibleAmount,
                       IsNull(@AdjOtherAmount, 0) AS AdjOtherAmount,
                       IsNull(@AdjRelatedPriorDamAmount, 0) AS AdjRelatedPriorDamAmount,
                       IsNull(@AdjUnrelatedPriorDamAmount, 0) AS AdjUnrelatedPriorDamAmount,
                       IsNull(@BodyRepairHrs, 0) AS BodyRepairHrs,
                       IsNull(@BodyReplaceHrs, 0) AS BodyReplaceHrs,
                       @DaysSinceAssignment AS DaysSinceAssignment,
                       IsNull(@EstimateBalancedFlag, 0) AS EstimateBalancedFlag,
                       @EstSuppSequenceNumber AS EstSuppSequenceNumber,
                       IsNull(@FrameRepairHrs, 0) AS FrameRepairHrs,
                       IsNull(@FrameReplaceHrs, 0) AS FrameReplaceHrs,
                       IsNull(@LaborBodyHours, 0) AS LaborBodyHours,
                       IsNull(@LaborBodyRate, 0) AS LaborBodyRate,
                       IsNull(@LaborBodyAmount, 0) AS LaborBodyAmount,
                       IsNull(@LaborFrameHours, 0) AS LaborFrameHours,
                       IsNull(@LaborFrameRate, 0) AS LaborFrameRate,
                       IsNull(@LaborFrameAmount, 0) AS LaborFrameAmount,
                       IsNull(@LaborMechHours, 0) AS LaborMechHours,
                       IsNull(@LaborMechRate, 0) AS LaborMechRate,
                       IsNull(@LaborMechAmount, 0) AS LaborMechAmount,
                       IsNull(@LaborRefinishHours, 0) AS LaborRefinishHours,
                       IsNull(@LaborRefinishRate, 0) AS LaborRefinishRate,
                       IsNull(@LaborRefinishAmount, 0) AS LaborRefinishAmount,
                       IsNull(@LaborTotalHours, 0) AS LaborTotalHours,
                       IsNull(@LaborTotalAmount, 0) AS LaborTotalAmount,
                       IsNull(@MaterialsHours, 0) AS MaterialsHours,
                       IsNull(@MaterialsRate, 0) AS MaterialsRate,
                       IsNull(@MaterialsAmount, 0) AS MaterialsAmount,
                       IsNull(@MechanicalRepairHrs, 0) AS MechanicalRepairHrs,
                       IsNull(@MechanicalReplaceHrs, 0) AS MechanicalReplaceHrs,
                       IsNull(@OtherAmount, 0) AS OtherAmount,
                       IsNull(@PartDiscountAmount, 0) AS PartDiscountAmount,
                       IsNull(@PartMarkupAmount, 0) AS PartMarkupAmount,
                       IsNull(@PartsNewAmount, 0) AS PartsNewAmount,
                       IsNull(@PartsLKQAmount, 0) AS PartsLKQAmount,
                       IsNull(@PartsAFMKAmount, 0) AS PartsAFMKAmount,
                       IsNull(@PartsRemanAmount, 0) AS PartsRemanAmount,
                       IsNull(@PartsTotalAmount, 0) AS PartsTotalAmount,
                       IsNull(@RefinishRepairHrs, 0) AS RefinishRepairHrs,
                       IsNull(@RefinishReplaceHrs, 0) AS RefinishReplaceHrs,
                       IsNull(@StorageAmount, 0) AS StorageAmount,
                       IsNull(@SubletAmount, 0) AS SubletAmount,
                       IsNull(@TowingAmount, 0) AS TowingAmount,
                       IsNull(@TaxLaborAmount, 0) AS TaxLaborAmount,
                       IsNull(@TaxPartsAmount, 0) AS TaxPartsAmount,
                       IsNull(@TaxMaterialsAmount, 0) AS TaxMaterialsAmount,
                       IsNull(@TaxTotalAmount, 0) AS TaxTotalAmount,                                              
                       IsNull(@TotalAmount, 0) AS TotalAmount,
                       IsNull(@NetAmount, 0) AS NetAmount                       
            END
            

            -- Validate that no required columns are NULL
        
            IF @CustomerID IS NULL
            BEGIN
                RAISERROR('%s: Invalid data state found.  Required column @CustomerID is NULL for Document %u', 16, 1, @ProcName, @DocumentID)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @DocumentSourceID IS NULL
            BEGIN
                RAISERROR('%s: Invalid data state found.  Required column @DocumentSourceID is NULL for Document %u', 16, 1, @ProcName, @DocumentID)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @TimeIDReceived IS NULL
            BEGIN
                RAISERROR('%s: Invalid data state found.  Required column @TimeIDReceived is NULL for Document %u', 16, 1, @ProcName, @DocumentID)
                ROLLBACK TRANSACTION
                RETURN
            END

            IF @EstSuppSequenceNumber IS NULL
            BEGIN
                RAISERROR('%s: Invalid data state found.  Required column @EstSuppSequenceNumber is NULL for Document %u', 16, 1, @ProcName, @DocumentID)
                ROLLBACK TRANSACTION
                RETURN
            END


            -- STARS 216709 BEGIN
            -- Zip code capture for DA assignments (DJS)
            
            -- Check that the estimate does not already have a repair location, and has an associated document 
            IF @RepairLocationIDEstimate IS NULL AND @DocumentID IS NOT NULL
            BEGIN
                -- Initialize our lookup variables
    
                SELECT  @EstimateShopLocationID = NULL,
                        @EstimateShopName = NULL,
                        @EstimateShopState = NULL,
                        @EstimateShopStateID = NULL,
                        @EstimateShopZip = NULL,
                        @EstimateShopAreaCode = NULL,
                        @EstimateShopExchangeNumber = NULL,
                        @EstimateShopUnitNumber = NULL,
                        @DAShopState = NULL,
                        @DAShopCity = NULL,
                        @DAShopCounty = NULL,
                        @DAShopZip = NULL

                IF @ClaimAspectServiceChannelID IS NOT NULL
                BEGIN
                  SELECT @DAShopState = RepairLocationState,
                         @DAShopCity = RepairLocationCity,
                         @DAShopCounty = RepairLocationCounty
                  FROM dbo.utb_claim_aspect_service_channel WITH (NOLOCK)
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                  
                 
                  -- get the default zip code from the melissa database
                  select top 1 @DAShopZip = mz.Zip
                  from dbo.utb_melissa_zip mz WITH (NOLOCK)
                  left join dbo.utb_melissa_cnty mc  WITH (NOLOCK) on mz.fips = mc.fips
                  where mz.State = @DAShopState
                    and mz.City = @DAShopCity
                    and mc.Name = @DAShopCounty
                    and mz.TYP = 'U'
                    
                  IF @DAShopZip IS NULL
                  BEGIN 
                     -- NO default zip code. so pick the first one that matches
                     select top 1 @DAShopZip = mz.Zip
                     from dbo.utb_melissa_zip mz WITH (NOLOCK)
                     left join dbo.utb_melissa_cnty mc  WITH (NOLOCK) on mz.fips = mc.fips
                     where mz.State = @DAShopState
                       and mz.City = @DAShopCity
                       and mc.Name = @DAShopCounty
                  END
                  
                  -- there is no zip code that could translate to city/state/county. so default it to 99999.
                  IF @DAShopZip IS NULL SET @DAShopZip = '99999'
                  
                  SELECT @EstimateShopStateID = StateID FROM dbo.utb_dtwh_dim_state WITH (NOLOCK) WHERE StateCode = @DAShopState

                  IF @@ERROR <> 0
                  BEGIN
                    -- SQL Server Error

                     RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                     ROLLBACK TRANSACTION
                     RETURN
                  END

                  SELECT  @RepairLocationIDEstimate = RepairLocationID
                    FROM  dbo.utb_dtwh_dim_repair_location WITH (NOLOCK)
                   WHERE  ShopLocationName = '*LYNX Desk Audit - Generic Shop'
                     AND  ZipCode = @DAShopZip
                     AND  City = @DAShopCity
                     AND  County = @DAShopCounty
                     AND  StateID = @EstimateShopStateID

                  IF @RepairLocationIDEstimate IS NULL AND
                    (@DAShopZip IS NOT NULL AND
                     @DAShopCity IS NOT NULL AND
                     @DAShopCounty IS NOT NULL AND
                     @EstimateShopStateID IS NOT NULL)
                  BEGIN
                      INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode)  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                      SELECT  NULL,
                              @StandardDocumentSourceID,
                              @EstimateShopStateID,
                              0,
                              0,
                              RTRIM(LTRIM(@DAShopCity)),
                              RTRIM(LTRIM(@DAShopCounty)),
                              0,
                              0,
                              '*LYNX Desk Audit - Generic Shop',
                              NULL,
                              @DAShopZip

                    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

                    IF @error <> 0
                    BEGIN
                        RAISERROR('%s: SQL Server Error inserting repair location %s into utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @EstimateShopName, @DocumentID)
                        ROLLBACK TRANSACTION
                        RETURN
                    END

                    IF @rowcount = 1
                        SELECT @RepairLocationIDEstimate = SCOPE_IDENTITY()

                  END
                END
                ELSE
                BEGIN
                   -- Lookup an estimate's shop using the associated document
       
                   SELECT @EstimateShopName = REPLACE(ShopName, '"', ''''),
                          @EstimateShopState = ShopState,
                          @EstimateShopZip = ShopZip,
                          @EstimateShopAreaCode = ShopAreaCode,
                          @EstimateShopExchangeNumber = ShopExchangeNumber,
                          @EstimateShopUnitNumber = ShopUnitNumber
                     FROM dbo.utb_estimate WITH (NOLOCK) 
                    WHERE DocumentID = @DocumentID

                   IF @@ERROR <> 0
                   BEGIN
                      -- SQL Server Error

                       RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                       ROLLBACK TRANSACTION
                       RETURN
                   END

                   -- Check that the estimate has a shop
       
                   IF @EstimateShopName IS NOT NULL AND @EstimateShopState IS NOT NULL AND @EstimateShopZip IS NOT NULL AND
                      @EstimateShopAreaCode IS NOT NULL AND @EstimateShopExchangeNumber IS NOT NULL AND @EstimateShopUnitNumber IS NOT NULL
                   BEGIN 
                       -- Get Shop Location information for the estimate's shop

                       SELECT  @EstimateShopLocationID = ShopLocationID
                         FROM  dbo.utb_shop_location WITH (NOLOCK)
                        WHERE  PhoneAreaCode = @EstimateShopAreaCode
                          AND  PhoneExchangeNumber = @EstimateShopExchangeNumber
                          AND  PhoneUnitNumber = @EstimateShopUnitNumber
                         
                       IF @@ERROR <> 0
                       BEGIN
                          -- SQL Server Error

                           RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                           ROLLBACK TRANSACTION
                           RETURN
                       END

                       SELECT @EstimateShopStateID = StateID FROM dbo.utb_dtwh_dim_state WITH (NOLOCK) WHERE StateCode = @EstimateShopState

                       IF @@ERROR <> 0
                       BEGIN
                          -- SQL Server Error

                           RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                           ROLLBACK TRANSACTION
                           RETURN
                       END


                       IF @EstimateShopStateID IS NULL
                       BEGIN
                           SELECT @EstimateShopStateID = StateID FROM dbo.utb_dtwh_dim_state WITH (NOLOCK) WHERE StateCode = 'FL'

                           IF @@ERROR <> 0
                           BEGIN
                              -- SQL Server Error

                               RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                               ROLLBACK TRANSACTION
                               RETURN
                           END
                       END

                       -- Check that the estimate's shop has shop location information
           
                       IF @EstimateShopLocationID IS NULL
                       BEGIN
                           -- The estimate's shop is not in our shop location table
                           -- Check if the estimate's shop is already in the warehouse repair locations
       
                           SELECT  @RepairLocationIDEstimate = RepairLocationID
                             FROM  dbo.utb_dtwh_dim_repair_location WITH (NOLOCK)
                            WHERE  ShopLocationName = @EstimateShopName
                              AND  ZipCode= @EstimateShopZip

                           IF @@ERROR <> 0
                           BEGIN
                              -- SQL Server Error

                               RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                               ROLLBACK TRANSACTION
                               RETURN
                           END

                           IF @RepairLocationIDEstimate IS NULL
                           BEGIN
                               -- The estimate's shop doesn't exist in the warehouse repair locations, add the dimension record
                               
                               IF EXISTS (SELECT zip FROM dbo.utb_zip_code WITH (NOLOCK) WHERE zip = @EstimateShopZip)
                               BEGIN
                                     INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode)  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                     SELECT  NULL,
                                             @StandardDocumentSourceID,
                                             @EstimateShopStateID,
                                             0,
                                             0,
                                             RTRIM(LTRIM(zc.City)),
                                             RTRIM(LTRIM(zc.County)),
                                             0,
                                             0,
                                             @EstimateShopName,
                                             NULL,
                                             @EstimateShopZip 
                                       FROM  dbo.utb_zip_code zc WITH (NOLOCK)
                                       WHERE zc.Zip = @EstimateShopZip
       
                                   SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

                                   IF @error <> 0
                                   BEGIN
                                       RAISERROR('%s: SQL Server Error inserting repair location %s into utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @EstimateShopName, @DocumentID)
                                       ROLLBACK TRANSACTION
                                       RETURN
                                   END
           
                                   IF @rowcount = 1
                                       SELECT @RepairLocationIDEstimate = SCOPE_IDENTITY()
                               END
                               ELSE
                               BEGIN
                                   INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode)  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                     VALUES ( 
                                             NULL,  
                                             @StandardDocumentSourceID, 
                                             @EstimateShopStateID,
                                             0,
                                             0,
                                             'Unknown',
                                             'Unknown',
                                             0,
                                             0,
                                             @EstimateShopName,
                                             NULL,
                                             @EstimateShopZip 
                                             )
       
                                   SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

                                   IF @error <> 0
                                   BEGIN
                                       RAISERROR('%s: SQL Server Error inserting repair location %s into utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @EstimateShopName, @DocumentID)
                                       ROLLBACK TRANSACTION
                                       RETURN
                                   END
           
                                   IF @rowcount = 1
                                       SELECT @RepairLocationIDEstimate = SCOPE_IDENTITY()
                             END
                           END
                           ELSE
                           BEGIN
                               -- The estimate's shop exists in the warehouse repair locations
                               -- We need to update the repair facilities information
               
                               UPDATE  dbo.utb_dtwh_dim_repair_location
                                 SET   StandardDocumentSourceID = @StandardDocumentSourceID,
                                       StateID = @EstimateShopStateID,
                                       RRPProgramFlag = 0,  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                       CFProgramFlag = 0,
                                       City = IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                                       County = IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                                       LYNXSelectProgramFlag = 0,
                                       PPGPaintBuyerFlag = 0,
                                       ShopLocationName = @EstimateShopName,
                                       StandardEstimatePackage = NULL,
                                       ZipCode = IsNull(zc.Zip, @EstimateShopZip) 
                                 FROM  dbo.utb_dtwh_dim_repair_location drl
                                 LEFT JOIN dbo.utb_zip_code zc ON (@EstimateShopZip = zc.Zip)
                                 WHERE drl.RepairLocationID = @RepairLocationIDEstimate

                               IF @@ERROR <> 0
                               BEGIN
                                   RAISERROR('%s: SQL Server Error updating repair location id %u in utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @RepairLocationIDEstimate, @DocumentID)
                                   ROLLBACK TRANSACTION
                                   RETURN
                               END
                           END
                       END
                       ELSE
                       BEGIN
                           -- The estimate's shop is in our shop location table
                           -- Check if the estimate's shop is already in the warehouse repair locations
           
                           SELECT  @RepairLocationIDEstimate = RepairLocationID
                             FROM  dbo.utb_dtwh_dim_repair_location WITH (NOLOCK)
                             WHERE ShopLocationID = @EstimateShopLocationID

                           IF @@ERROR <> 0
                           BEGIN
                              -- SQL Server Error
       
                               RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                               ROLLBACK TRANSACTION
                               RETURN
                           END

                           IF @RepairLocationIDEstimate IS NULL
                           BEGIN
                               -- The estimate's shop doesn't exist in the warehouse repair locations, add the dimension record

                               INSERT INTO dbo.utb_dtwh_dim_repair_location (ShopLocationID, StandardDocumentSourceID, StateID, RRPProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationName, StandardEstimatePackage, ZipCode)  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                 SELECT  sl.ShopLocationID,
                                         @StandardDocumentSourceID,
                                         (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                                         sl.ReferralFlag,  -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                         sl.CertifiedFirstFlag,
                                         IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                                         IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                                         sl.ProgramFlag,
                                         sl.PPGCTSCustomerFlag,
                                         LTrim(RTrim(sl.Name)),
                                         ep.Name,
                                         sl.AddressZip 
                                   FROM  dbo.utb_shop_location sl WITH (NOLOCK)
                                   LEFT JOIN dbo.utb_estimate_package ep WITH (NOLOCK) ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                                   LEFT JOIN dbo.utb_zip_code zc WITH (NOLOCK) ON (sl.AddressZip = zc.Zip)
                                   WHERE sl.ShopLocationID = @EstimateShopLocationID
           
                               SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

                               IF @error <> 0
                               BEGIN
                                   RAISERROR('%s: SQL Server Error inserting repair location %s into utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @EstimateShopName, @DocumentID)
                                   ROLLBACK TRANSACTION
                                   RETURN
                               END
               
                               IF @rowcount = 1
                                   SELECT @RepairLocationIDEstimate = SCOPE_IDENTITY()
                           END
                           ELSE
                           BEGIN
                               -- The estimate's shop exists in the warehouse repair locations
                               -- We need to update the repair facilities information
                   
                               UPDATE  dbo.utb_dtwh_dim_repair_location
                                 SET   StandardDocumentSourceID = @StandardDocumentSourceID,
                                       StateID = (SELECT StateID FROM dbo.utb_dtwh_dim_state WHERE StateCode = sl.AddressState),
                                       RRPProgramFlag = sl.ReferralFlag,   -- MAH 20-jun-2012 - Renamed CEIProgramFlag.
                                       CFProgramFlag = sl.CertifiedFirstFlag,
                                       City = IsNull(RTRIM(LTRIM(zc.City)), 'Unknown'),
                                       County = IsNull(RTRIM(LTRIM(zc.County)), 'Unknown'),
                                       LYNXSelectProgramFlag = sl.ProgramFlag,
                                       PPGPaintBuyerFlag = sl.PPGCTSCustomerFlag,
                                       ShopLocationName = sl.Name,
                                       StandardEstimatePackage = ep.Name,
                                       ZipCode = sl.AddressZip 
                                 FROM  dbo.utb_dtwh_dim_repair_location drl
                                 LEFT JOIN dbo.utb_shop_location sl ON (drl.ShopLocationID = sl.ShopLocationID)
                                 LEFT JOIN dbo.utb_estimate_package ep ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
                                 LEFT JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.Zip)
                                 WHERE drl.RepairLocationID = @RepairLocationIDEstimate

                               IF @@ERROR <> 0
                               BEGIN
                                   RAISERROR('%s: SQL Server Error updating repair location id %u in utb_dtwh_dim_repair_location for estimate document id %u', 16, 1, @ProcName, @RepairLocationIDEstimate, @DocumentID)
                                   ROLLBACK TRANSACTION
                                   RETURN
                               END
                           END                                   
                       END
                   END
                END
            END

            -- STARS 216709 END
            

            -- We're ready to save the estimate fact record.  We must first check to see if this DocumentID
            -- already exists in the fact table
        
            IF EXISTS(SELECT DocumentID FROM dbo.utb_dtwh_fact_estimate WITH (NOLOCK) WHERE DocumentID = @DocumentID)
            BEGIN
                -- The record already exists, let's update
                if @debug = 1
                begin
                  print 'updating existing estimate fact record'
                end
            
                UPDATE dbo.utb_dtwh_fact_estimate
                  SET  ClaimAspectID                    = @ClaimAspectID,
                       CustomerID                       = @CustomerID,
                       DocumentSourceID                 = @DocumentSourceID,
                       RepairLocationID                 = @RepairLocationIDEstimate,
                       TimeIDAssignment                 = @TimeIDAssignSent,
                       TimeIDReceived                   = @TimeIDReceived,
                       AdjAppearanceAllowanceAmount     = IsNull(@AdjAppearanceAllowanceAmount, 0),
                       AdjBettermentAmount              = IsNull(@AdjBettermentAmount, 0),
                       AdjDeductibleAmount              = IsNull(@AdjDeductibleAmount, 0),
                       AdjOtherAmount                   = IsNull(@AdjOtherAmount, 0),
                       AdjRelatedPriorDamAmount         = IsNull(@AdjRelatedPriorDamAmount, 0),
                       AdjUnrelatedPriorDamAmount       = IsNull(@AdjUnrelatedPriorDamAmount, 0) ,
                       BodyRepairHrs                    = IsNull(@BodyRepairHrs, 0),
                       BodyReplaceHrs                   = IsNull(@BodyReplaceHrs, 0),
                       DaysSinceAssignment              = @DaysSinceAssignment,
                       EstimateBalancedFlag             = IsNull(@EstimateBalancedFlag, 0),
                       EstSuppSequenceNumber            = @EstSuppSequenceNumber,
                       FrameRepairHrs                   = IsNull(@FrameRepairHrs, 0),
                       FrameReplaceHrs                  = IsNull(@FrameReplaceHrs, 0),
                       LaborBodyHours                   = IsNull(@LaborBodyHours, 0),
                       LaborBodyRate                    = IsNull(@LaborBodyRate, 0),
                       LaborBodyAmount                  = IsNull(@LaborBodyAmount, 0),
                       LaborFrameHours                  = IsNull(@LaborFrameHours, 0),
                       LaborFrameRate                   = IsNull(@LaborFrameRate, 0),
                       LaborFrameAmount                 = IsNull(@LaborFrameAmount, 0),
                       LaborMechHours                   = IsNull(@LaborMechHours, 0),
                       LaborMechRate                    = IsNull(@LaborMechRate, 0),
                       LaborMechAmount                  = IsNull(@LaborMechAmount, 0),
                       LaborRefinishHours               = IsNull(@LaborRefinishHours, 0),
                       LaborRefinishRate                = IsNull(@LaborRefinishRate, 0),
                       LaborRefinishAmount              = IsNull(@LaborRefinishAmount, 0),
                       LaborTotalHours                  = IsNull(@LaborTotalHours, 0),
                       LaborTotalAmount                 = IsNull(@LaborTotalAmount, 0),
                       MaterialsHours                   = IsNull(@MaterialsHours, 0),
                       MaterialsRate                    = IsNull(@MaterialsRate, 0),
                       MaterialsAmount                  = IsNull(@MaterialsAmount, 0),
                       MechanicalRepairHrs              = IsNull(@MechanicalRepairHrs, 0),
                       MechanicalReplaceHrs             = IsNull(@MechanicalReplaceHrs, 0),
                       OtherAmount                      = IsNull(@OtherAmount, 0),
                       PartDiscountAmount               = IsNull(@PartDiscountAmount, 0),
                       PartMarkupAmount                 = IsNull(@PartMarkupAmount, 0),
                       PartsNewAmount                   = IsNull(@PartsNewAmount, 0),
                       PartsLKQAmount                   = IsNull(@PartsLKQAmount, 0),
                       PartsAFMKAmount                  = IsNull(@PartsAFMKAmount, 0),
                       PartsRemanAmount                 = IsNull(@PartsRemanAmount, 0),
                       PartsTotalAmount                 = IsNull(@PartsTotalAmount, 0),
                       RefinishRepairHrs                = IsNull(@RefinishRepairHrs, 0),
                       RefinishReplaceHrs               = IsNull(@RefinishReplaceHrs, 0),
                       StorageAmount                    = IsNull(@StorageAmount, 0),
                       SubletAmount                     = IsNull(@SubletAmount, 0),
                       TowingAmount                     = IsNull(@TowingAmount, 0),
                       TaxLaborAmount                   = IsNull(@TaxLaborAmount, 0),
                       TaxPartsAmount                   = IsNull(@TaxPartsAmount, 0),
                       TaxMaterialsAmount               = IsNull(@TaxMaterialsAmount, 0),
                       TaxTotalAmount                   = IsNull(@TaxTotalAmount, 0),                                              
                       TotalAmount                      = IsNull(@TotalAmount, 0), 
                       NetAmount                        = IsNull(@NetAmount, 0)                       
                  WHERE DocumentID = @DocumentID

                IF @@ERROR <> 0
                BEGIN
                    RAISERROR('%s: SQL Server Error updating utb_dtwh_fact_estimate', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
            ELSE
            BEGIN
                -- We have to add a new fact record
                if @debug = 1
                begin
                  print 'Inserting new estimate fact record'
                end
                INSERT INTO dbo.utb_dtwh_fact_estimate
                (
                    DocumentID,
                    ClaimAspectID,
                    CustomerID,
                    DocumentSourceID,
                    RepairLocationID,
                    TimeIDAssignment,
                    TimeIDReceived,
                    AdjAppearanceAllowanceAmount,
                    AdjBettermentAmount,
                    AdjDeductibleAmount,
                    AdjOtherAmount,
                    AdjRelatedPriorDamAmount,
                    AdjUnrelatedPriorDamAmount,
                    BodyRepairHrs,
                    BodyReplaceHrs,
                    DaysSinceAssignment,
                    EstimateBalancedFlag,
                    EstSuppSequenceNumber,
                    FrameRepairHrs,
                    FrameReplaceHrs,
                    LaborBodyHours,
                    LaborBodyRate,
                    LaborBodyAmount,
                    LaborFrameHours,
                    LaborFrameRate,
                    LaborFrameAmount,
                    LaborMechHours,
                    LaborMechRate,
                    LaborMechAmount,
                    LaborRefinishHours,
                    LaborRefinishRate,
                    LaborRefinishAmount,
                    LaborTotalHours,
                    LaborTotalAmount,
                    MaterialsHours,
                    MaterialsRate,
                    MaterialsAmount,
                    MechanicalRepairHrs,
                    MechanicalReplaceHrs,
                    OtherAmount,
                    PartDiscountAmount,
                    PartMarkupAmount,
                    PartsNewAmount,
                    PartsLKQAmount,
                    PartsAFMKAmount,
                    PartsRemanAmount,
                    PartsTotalAmount,
                    RefinishRepairHrs,
                    RefinishReplaceHrs,
                    StorageAmount,
                    SubletAmount,
                    TowingAmount,
                    TaxLaborAmount,
                    TaxPartsAmount,
                    TaxMaterialsAmount,
                    TaxTotalAmount,
                    TotalAmount, 
                    NetAmount 
                )
                VALUES
                (
                    @DocumentID,
                    @ClaimAspectID,
                    @CustomerID,
                    @DocumentSourceID,
                    @RepairLocationIDEstimate,
                    @TimeIDAssignSent,
                    @TimeIDReceived,
                    IsNull(@AdjAppearanceAllowanceAmount, 0),
                    IsNull(@AdjBettermentAmount, 0),
                    IsNull(@AdjDeductibleAmount, 0),
                    IsNull(@AdjOtherAmount, 0),
                    IsNull(@AdjRelatedPriorDamAmount, 0),
                    IsNull(@AdjUnrelatedPriorDamAmount, 0) ,
                    IsNull(@BodyRepairHrs, 0),
                    IsNull(@BodyReplaceHrs, 0),
                    @DaysSinceAssignment,
                    @EstimateBalancedFlag,
                    @EstSuppSequenceNumber,
                    IsNull(@FrameRepairHrs, 0),
                    IsNull(@FrameReplaceHrs, 0),
                    IsNull(@LaborBodyHours, 0),
                    IsNull(@LaborBodyRate, 0),
                    IsNull(@LaborBodyAmount, 0),
                    IsNull(@LaborFrameHours, 0),
                    IsNull(@LaborFrameRate, 0),
                    IsNull(@LaborFrameAmount, 0),
                    IsNull(@LaborMechHours, 0),
                    IsNull(@LaborMechRate, 0),
                    IsNull(@LaborMechAmount, 0),
                    IsNull(@LaborRefinishHours, 0),
                    IsNull(@LaborRefinishRate, 0),
                    IsNull(@LaborRefinishAmount, 0),
                    IsNull(@LaborTotalHours, 0),
                    IsNull(@LaborTotalAmount, 0),
                    IsNull(@MaterialsHours, 0),
                    IsNull(@MaterialsRate, 0),
                    IsNull(@MaterialsAmount, 0),
                    IsNull(@MechanicalRepairHrs, 0),
                    IsNull(@MechanicalReplaceHrs, 0),
                    IsNull(@OtherAmount, 0),
                    IsNull(@PartDiscountAmount, 0),
                    IsNull(@PartMarkupAmount, 0),
                    IsNull(@PartsNewAmount, 0),
                    IsNull(@PartsLKQAmount, 0),
                    IsNull(@PartsAFMKAmount, 0),
                    IsNull(@PartsRemanAmount, 0),
                    IsNull(@PartsTotalAmount, 0),
                    IsNull(@RefinishRepairHrs, 0),
                    IsNull(@RefinishReplaceHrs, 0),
                    IsNull(@StorageAmount, 0),
                    IsNull(@SubletAmount, 0),
                    IsNull(@TowingAmount, 0),
                    IsNull(@TaxLaborAmount, 0),
                    IsNull(@TaxPartsAmount, 0),
                    IsNull(@TaxMaterialsAmount, 0),
                    IsNull(@TaxTotalAmount, 0),                                              
                    IsNull(@TotalAmount, 0), 
                    IsNull(@NetAmount, 0)                       
                )
            END

            IF @@ERROR <> 0
            BEGIN
                RAISERROR('%s: SQL Server Error inserting into utb_dtwh_fact_estimate', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            
            -- Get next estimate to process

            SELECT  @DocumentID = NULL,
                    @AssignmentID = NULL,
                    @DocumentSourceName = NULL,
                    @DocumentSourceNameStandard=NULL,
                    @VanSourceFlag = NULL,
                    @VanSourceFLagStandard = NULL,
                    @SequenceNumber = NULL,
                    @CreatedDate = NULL
                    
            FETCH NEXT FROM csrEstimate
              INTO  @DocumentID,
                    @AssignmentID,
                    @DocumentSourceName,
                    @VanSourceFlag,
                    @SequenceNumber,
                    @CreatedDate
        END
    
    
        -- Close and deallocate the estimate cursor
    
        CLOSE csrEstimate
        DEALLOCATE csrEstimate

        -- STARS 216709 BEGIN
        -- Zip code capture for DA assignments (DJS)

        -- Update fact claim with last repair location for Desk Audits
        IF @RepairLocationIDEstimate IS NOT NULL
        BEGIN
            UPDATE dbo.utb_dtwh_fact_claim
               SET RepairLocationID = @RepairLocationIDEstimate
             WHERE ClaimAspectID = @ClaimAspectID
               AND AssignmentTypeID IN (SELECT AssignmentTypeID FROM dbo.utb_dtwh_dim_assignment_type WITH (NOLOCK) WHERE ServiceChannelID IN 
                                       (SELECT ServiceChannelID FROM dbo.utb_dtwh_dim_service_channel WITH (NOLOCK) WHERE ServiceChannelCD = 'DA'))

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error updating repair location %u into utb_dtwh_fact_claim for claim aspect id %u', 16, 1, @ProcName, @RepairLocationIDEstimate, @ClaimAspectID)
                ROLLBACK TRANSACTION
                RETURN
            END
        END

        -- STARS 216709 END

        -- Update the process counter and display updates to the user of progress
        
        SELECT @ExposuresProcessed = @ExposuresProcessed + 1
        
        IF (@ExposuresProcessed % 100) = 0
        BEGIN
            PRINT Convert(varchar(10), @ExposuresProcessed) + ' exposures processed...'
        END
        

        -- Get Next claim aspect to process

        SELECT  @ClaimAspectID = NULL,
                @ClaimAspectTypeID = NULL,
                @ClaimAspectNumber = NULL,
                @LynxID = NULL,
                @CarrierRepUserID = NULL,
                @AnalystUserID = NULL,
                @OwnerUserID = NULL,
                @SupportUserID = NULL,
                @LossState = NULL,
                @VehicleCreatedDate = NULL,
                @LossDate = NULL,
                @PolicyNumber = NULL,
                @DemoFlag = NULL,
	            @OriginalAuditedEstimateGrossAmt = NULL, -- MAH 07-jun-2012 Fields were reversed @EarlyBillAuditedGrossAmt = NULL,
				-- -----------------------------------------------------------
				-- MAH 07-jun-2012 OBSOLETE
				-- -----------------------------------------------------------
	            -- @EarlyBillAuditedAddtlLineItemAmt = NULL,
	            -- @EarlyBillAuditedLineItemCorrectionAmt = NULL,
	            -- @EarlyBillAuditedMissingLineItemAmt = NULL,
				-- -----------------------------------------------------------
                @EnabledFlag = NULL,
                @ExposureCD = NULL,
                @ClientCoverageTypeID = NULL,
                @DispositionTypeCD = NULL,
                @InitialAssignmentTypeID = NULL,
                @ClosingAssignmentTypeID = NULL,
                @ClosingServiceChannelCD = NULL,
                @InspectionDate = NULL,
                @OriginalCompleteDate = NULL,
                @ServiceChannelCD = NULL,
                @ClientClaimNumber = NULL,
                @PolicyDeductibleAmt = NULL,
                @PolicyLimitAmt = NULL,
                @PolicyRentalDeductibleAmt = NULL,
                @PolicyRentalLimitAmt = NULL,
                @PolicyRentalMaxDays = NULL,
                @RepairEndDate = NULL,
                @RepairStartDate = NULL,
                @VehicleDriveableFlag = NULL,
                @VehicleLicensePlateNumber = NULL,
                @LicensePlateStateCode = NULL,
                @VehicleMake = NULL,
                @VehicleModel = NULL,
                @VehicleYear = NULL,
                @VehicleVIN = NULL,
                @ApplicationName = NULL,
                @ClaimAspectServiceChannelID = NULL

    
        FETCH NEXT FROM csrClaimAspect
          INTO  @ClaimAspectID,
                @ClaimAspectTypeID,
                @ClaimAspectNumber,
                @LynxID,
                @CarrierRepUserID,
                @AnalystUserID,
                @OwnerUserID,
                @SupportUserID,
                @LossState,
                @VehicleCreatedDate,
                @LossDate,
                @PolicyNumber,
                @DemoFlag,
	            @OriginalAuditedEstimateGrossAmt, -- MAH 07-jun-2012 Fields were reversed @EarlyBillAuditedGrossAmt,
				-- -----------------------------------------------------------
				-- MAH 07-jun-2012 OBSOLETE
				-- -----------------------------------------------------------
    	        -- @EarlyBillAuditedAddtlLineItemAmt,
	            -- @EarlyBillAuditedLineItemCorrectionAmt,
	            -- @EarlyBillAuditedMissingLineItemAmt,
				-- -----------------------------------------------------------
                @EnabledFlag,
                @ExposureCD,
                @ClientCoverageTypeID,
                @DispositionTypeCD,
                @InitialAssignmentTypeID,
                @InspectionDate,
                @ClosingAssignmentTypeID,
                @ClosingServiceChannelCD,
                @OriginalCompleteDate,
                @ServiceChannelCD,
                @ClientClaimNumber,
                @PolicyDeductibleAmt,
                @PolicyLimitAmt,
                @PolicyRentalDeductibleAmt,
                @PolicyRentalLimitAmt,
                @PolicyRentalMaxDays,
                @RepairEndDate,
                @RepairStartDate,
                @VehicleDriveableFlag,
                @VehicleLicensePlateNumber,
                @LicensePlateStateCode,
                @VehicleMake,
                @VehicleModel,
                @VehicleYear,
                @VehicleVIN,
                @ApplicationName,
                @ClaimAspectServiceChannelID
    END
    
    
    -- Close and deallocate the claim aspect cursor
    
    CLOSE csrClaimAspect
    DEALLOCATE csrClaimAspect
    
    
    IF (@Command IS NULL) OR
       (@Command IS NOT NULL AND @Command NOT IN ('Init', 'Itst'))   -- If we are initializing from scratch, we didn't start a transaction
    BEGIN
        -- Commit the transaction

        COMMIT TRANSACTION DtwhGenerateDataTran1

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END
    
    PRINT '.'
    PRINT '.'
    PRINT 'Warehouse update complete...'
END
        

GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhGenerateData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhGenerateData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
