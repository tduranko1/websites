-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'GetClientClaimAspects' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.GetClientClaimAspects 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    GetClientClaimAspects
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client claim aspect information from the database
*
* PARAMETERS:  
*				InsuranceCompanyID 
*
* RESULT SET:
*   All data related to insurance client aspects
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.GetClientClaimAspects
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		ccat.InsuranceCompanyID
		, ccat.ClaimAspectTypeID
		, ISNULL(cat.Code,'') Code
		, ISNULL(cat.DefaultExposureFlag,'') DefaultExposureFlag
		, ISNULL(cat.DisplayOrder,'') DisplayOrder
		, ISNULL(cat.EnabledFlag,'') EnabledFlag
		, ISNULL(cat.EnumeratedFlag,'') EnumeratedFlag
		, ISNULL(cat.GeneratesPertainsToFlag,'') GeneratesPertainsToFlag
		, ISNULL(cat.[Name],'') AspectName
		, ISNULL(cat.ProfileRequiredFlag,'') ProfileRequiredFlag
		, cat.SysMaintainedFlag
		, cat.SysLastUpdatedDate
	FROM
		utb_client_claim_aspect_type as ccat
		INNER JOIN dbo.utb_claim_aspect_type cat
		ON cat.ClaimAspectTypeID = ccat.ClaimAspectTypeID
	WHERE 
		cat.EnabledFlag = 1	 
		AND ccat.InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		[Name] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'GetClientClaimAspects' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.GetClientClaimAspects TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.GetClientClaimAspects TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/