-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowCompleteServiceChannel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowCompleteServiceChannel 
END

GO

/****** Object:  StoredProcedure [dbo].[uspWorkflowCompleteServiceChannel]    Script Date: 06/12/2014 06:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowCompleteServiceChannel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Closes 1 or more claim exposures.  
*
* PARAMETERS:
* (I) @LynxID               LynxID of the claim to operate on  
* (I) @ClaimAspectList      Comma delimited list of exposures (ClaimAspectIDs) to close
* (I) @UserID               The User performing this action
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE [dbo].[uspWorkflowCompleteServiceChannel]
    @LynxID                         udt_std_id_big,
    @ClaimAspectServiceChannelList  varchar(500),
    @UserID                         udt_std_id,
    @ApplicationCD                  udt_std_cd='APD'
AS
BEGIN
    -- Set database options

    set NOCOUNT ON
    set ANSI_WARNINGS OFF
    
      
    declare @tmpServiceChannels table
    (
      ClaimAspectServiceChannelID       bigint,
      ClaimAspectID                     bigint,      
      Valid                             bit,
      PrimaryFlag                       bit,     
      ClientAuthorizesPaymentFlag       bit
    )
    
    declare @debug                            bit
    declare @ProcName                         varchar(30)
    declare @now                              udt_std_datetime
    
    declare @InsuranceCompanyID               udt_std_id
    declare @ClaimAspectTypeIDClaim           udt_std_id
    declare @ClaimAspectTypeIDVehicle         udt_std_id
    declare @TaskIDCloseClaim                 udt_std_id
    declare @TaskIDCloseVehicle               udt_std_id
    declare @TaskIDCloseAndBill               udt_std_id
    
    declare @CurrentFeeServiceChannelCD       udt_std_cd
    declare @PertainsToWork                   varchar(30)
    declare @InvoiceMethodCD                  udt_std_cd
    declare @NoEstimateData                   bit
    declare @NoEstimateDataMessage            udt_std_desc_long
    declare @RepairTotalID                    udt_std_id
    declare @InvoicingModelPaymentCD          udt_std_cd
    declare @EstimateDocumentID               udt_std_id_big
    declare @EventIDCloseClaim                udt_std_id
    declare @EventIDCloseVehicle              udt_std_id
    declare @EventIDServiceChannelCompleted   udt_std_id            
    
    declare @DispositionTypeCDWork            udt_std_cd
    declare @ServiceChannelCDWork             udt_std_cd
    declare @ServiceChannelName               udt_std_desc_short
    
    declare @EarlyBillFlag                    udt_std_flag
    
    
    declare @w_ClaimAspectID                  udt_std_id_big
    declare @w_ClaimAspectIDClaim             udt_std_id_big
    declare @w_ClaimAspectServiceChannelID    udt_std_id_big
    declare @w_VehicleWillClose               udt_std_flag
    declare @w_ClaimWillClose                 udt_std_flag
    declare @w_HistoryDescription             udt_std_desc_long
    declare @w_ChecklistID                    udt_std_id_big
    
    declare @w_DocumentTypeID				  udt_std_id
    declare @w_SupplementSeqNumber			  udt_std_id
    declare @w_DocumentIDOriginal			  udt_std_id_big
    declare @w_DocumentIDAudited 			  udt_std_id_big
    declare @w_DocumentOriginalLocked       udt_std_flag
    declare @w_DocumentAuditedLocked        udt_std_flag
    declare @w_DocumentOriginalVANFlag      udt_std_flag
    declare @w_DocumentAuditedVANFlag       udt_std_flag
    declare @QCCheckFailedFlag				  udt_std_flag
    declare @QCCheckFailedReason			  varchar(8000)
    declare @w_crlf							  varchar(2)
    declare @w_InitialAssignmentTypeId    int
    declare @AssignmentTypeIdPursuitAudit int
    declare @ClosingDocumentCount int
    declare @ClosingDocumentTypeID int
    declare @TotalLossBundleDocumentTypeID int
    declare @PSAssignmentTypeID int
    declare @CurrentFeeAssignmentTypeID int
    declare @CurrentFeeAssignmentType varchar(4)
    declare @ClaimDispositionCD varchar(4)
    declare @InvoicingModelBillingCD varchar(2)
    
    set @ProcName = 'uspWorkflowCompleteServiceChannel'    
    set @debug = 0
    
    
    IF @debug = 1
    BEGIN
      Print 'BEGIN PROCEDURE uspWorkflowCompleteServiceChannel'
      Print ''
      Print 'Parameters:'
      Print '@ClaimAspectServiceChannelID = '-- + CONVERT(VARCHAR(10),@ClaimAspectServiceChannelID)
      Print '@UserID = ' + CONVERT(varchar(10),@UserID)
    END
    
    select @AssignmentTypeIdPursuitAudit = AssignmentTypeID
    from utb_assignment_type
    where Name = 'Pursuit Audit'
    
    -- valid LYNX ID must be passed
    if (@LynxID is null) OR
       (not exists(select LynxID from dbo.utb_claim where LynxID = @LynxID))
    begin
      raiserror('101|%s|@LynxID|%u',16,1,@ProcName,@LynxID)
      return
    end        
    
    select @w_ClaimAspectIDClaim = ClaimAspectID
    from dbo.utb_claim_aspect
    where LynxID = @LynxID 
      and ClaimAspectTypeID in (select ClaimAspectTypeID from dbo.utb_claim_aspect_type where Name = 'Claim')
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @w_ClaimAspectIDClaim is null
    begin
      raiserror('101|%s|Unable to determine Claim ClaimAspect from Lynx ID %u',16,1,@ProcName,@LynxID)
      return
    end
    
    -- validate user id
    
    if (dbo.ufnUtilityIsUserActive(@UserID,@ApplicationCD, NULL) = 0)
    begin
      -- Invalid User ID
      raiserror('101|%s|@UserID|%u',16,1,@ProcName,@UserID)
      return
    end
    
            
    -- validate passed ClaimAspectServiceChannelList
    if (@ClaimAspectServiceChannelList IS NULL) OR
       (len(ltrim(rtrim(@ClaimAspectServiceChannelList))) = 0)
    begin
      --empty service channel list
      raiserror('101|%s|@ClaimAspectServiceChannelList|%s',16,1,@ProcName,@ClaimAspectServiceChannelList)
      return
    end

    -- Task ID: Close Claim
    select @TaskIDCloseClaim = TaskID from dbo.utb_task where Name = 'Close Claim'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @TaskIDCloseClaim is null
    begin
      raiserror('102|%s|"Close Claim"|utb_task',16,1,@ProcName)
      return
    end

    -- Task ID: Close Vehicle
    select @TaskIDCloseVehicle = TaskID from dbo.utb_task where Name = 'Close Vehicle'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @TaskIDCloseVehicle is null
    begin
      raiserror('102|%s|"Close Vehicle"|utb_task',16,1,@ProcName)
      return
    end

    -- Task ID: Close and BIll
    select @TaskIDCloseAndBill = TaskID from dbo.utb_task where Name = 'Close and Bill File'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @TaskIDCloseAndBill is null
    begin
      raiserror('102|%s|"Close and Bill File"|utb_task',16,1,@ProcName)
      return
    end
    
    -- Get the ClaimAspectTypeID for Claim
    SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Claim'

    -- Get the ClaimAspectTypeID for Vehicle
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Vehicle'

    
    -- All Claim

    -- Get Insurance Company ID for claim
    select @InsuranceCompanyID = InsuranceCompanyID
    from utb_claim
    where LynxID = @LynxID

    -- Get InvoiceMethodCD and InvoicingModelPaymentCD for carrier
    select @InvoiceMethodCD = InvoiceMethodCD,
           @InvoicingModelPaymentCD = InvoicingModelPaymentCD,
           @EarlyBillFlag = EarlyBillFlag
    from dbo.utb_insurance
    where InsuranceCompanyID = @InsuranceCompanyID
    
                                 
    -- Get list of service channels, validating them.   
    insert into @tmpServiceChannels (ClaimAspectServiceChannelID, ClaimAspectID ,Valid, PrimaryFlag, ClientAuthorizesPaymentFlag)
    select ufnPS.value,
           casc.ClaimAspectID,
           case when casc.ClaimAspectServiceChannelID is null then 0 else 1 end,
           ISNULL(casc.PrimaryFlag,0),
           (select ISNULL(ClientAuthorizesPaymentFlag,0)
            from utb_client_service_channel
            where InsuranceCompanyID = @InsuranceCompanyID
              and ServiceChannelCD = casc.ServiceChannelCD)
    from dbo.ufnUtilityParseString(@ClaimAspectServiceChannelList,',',1) ufnPS
    left join utb_claim_aspect_service_channel casc on (ufnPS.value = casc.ClaimAspectServiceChannelID)

    if @@error <> 0
    begin
      -- insertion failed
      raiserror('105|%s|@tmpServiceChannels',16,1,@ProcName)
      return
    end
            
    if @debug = 1
    begin
      print ''
      print '@tmpServiceChannels table:'
      select * from @tmpServiceChannels
    end

    -- Now validate that each service channel id passed maps to a claim aspect id 
    if exists (select ClaimAspectServiceChannelID from @tmpServiceChannels where ClaimAspectID IS NULL)
    begin
      raiserror('101|%s|A ClaimAspectServiceChannelID passed is not valid.',16,1,@ProcName)
      return
    end
    
    -- now validate they all map to same ClaimAspect ID
    if (select count(distinct ClaimAspectID) from @tmpServiceChannels) <> 1
    begin
      raiserror('101|%s|ClaimAspectServiceChannelIDs in passed list must all map to same ClaimAspectID.',16,1,@ProcName)
      return
    end
    
    select top 1 @w_ClaimAspectID = ClaimAspectID from @tmpServiceChannels
    
    -- now we need to determine if closing of passed service channels will cause vehicle to be closed
    -- first we will check to see if there are any open service channels outside the list sent in
    
    set @w_VehicleWillClose = 0
    set @w_ClaimWillClose = 0
    
    if not exists (select ClaimAspectServiceChannelID
                   from dbo.utb_claim_aspect_service_channel casc 
                    inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
                    inner join dbo.utb_claim_aspect_status cas on ((ca.ClaimAspectID = cas.ClaimAspectID) and
                                                                   (casc.ServiceChannelCD = cas.ServiceChannelCD))                                                   
                    where ca.ClaimAspectID = @w_ClaimAspectID 
                      and casc.ClaimAspectServiceChannelID NOT IN (select ClaimAspectServiceChannelID from @tmpServiceChannels)
                      and cas.StatusID IN (select StatusID from utb_status where Name='Active' and StatusTypeCD = 'SC'))
    begin
      set @w_VehicleWillClose = 1
    end
    
    -- Verify that there are no tasks except Close Claim/Vehicle tasks
    if exists (select CheckListID
               from dbo.utb_checklist 
               where ClaimAspectServiceChannelID in (select ClaimAspectServiceChannelID from @tmpServiceChannels))
    begin
      -- existing tasks other then close claim / vehicle                 
      raiserror ('1|There are still uncompleted tasks attached to service channel(s) specified.  Please complete outstanding tasks before attempting to complete.',16,1)
      return
    end                      
    
      
    if @w_VehicleWillClose = 1 and 
       (not exists (select *
                    from dbo.utb_claim_aspect ca                            
                     inner join dbo.utb_claim_aspect_status cas on (ca.ClaimAspectID = cas.ClaimAspectID)
                    where ca.LynxID = @LynxID 
                      and ca.ClaimAspectID <> @w_ClaimAspectID
                      and cas.StatusID = (select StatusID 
                                          from dbo.utb_status 
                                          where ClaimAspectTypeID = @ClaimAspectTypeIDVehicle and Name = 'Open')))
    begin 
      set @w_ClaimWillClose = 1
            
    end
    
    if @w_VehicleWillClose = 1
    begin
      --  We've determined that closing the passed service channels will close this vehicle
      
      -- Check to see if Primary Service Channel is not acti
      if exists (select cas.StatusID
                 from dbo.utb_claim_aspect_service_channel casc
                 inner join dbo.utb_claim_aspect_status cas on ((casc.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD))
                 inner join dbo.utb_status s on (cas.StatusID = s.StatusID)
                 where casc.ClaimAspectID = @w_ClaimAspectID
                   and casc.PrimaryFlag = 1
                   and cas.StatusTypeCD = 'SC'
                   and s.Name IN ('Cancelled','Voided'))
      begin
        raiserror('1|Closing specified service channels(s) will cause closure of vehicle.  The primary service channel for this vehicle is cancelled therefore this action can not be completed.',15,1)                  
        return
      end
      
      -- we need to check to see if anything was billed
      if not exists (select InvoiceID
                     from dbo.utb_invoice 
                     where ClaimAspectID = @w_ClaimAspectID
                       and EnabledFlag = 1
                       and ItemTypeCD = 'F') -- Fee
      begin
        -- There is no billing record for this claim aspect therefore we can not close
        raiserror('1|Closing specified service channel(s) will cause closure of vehicle.  There is no billing information for this vehicle therefore this action can not be completed.',16,1)
        return
      end
      
      -- if client requires payment authorization, check for Authorized or Awaiting Client Authorization codes
      if exists (select ClaimAspectServiceChannelID
                 from @tmpServiceChannels
                 where ClientAuthorizesPaymentFlag = 1)
      begin
        if exists (select InvoiceID
                   from dbo.utb_invoice
                   where ClaimAspectID = @w_ClaimAspectID
                     and ItemTypeCD in ('I','E') -- Indemnity and Expense
                     and StatusCD = 'APD'
                     and EnabledFlag = 1)
        begin
          -- trying to close vehicle with invoices waiting to be authorized
          raiserror('1|Closing specified service channel(s) will cause closure of vehicle. Invoice(s) need to be authorized by client. Please request authorization for invoices on this vehicle before closing.',16,1)
          return
         end
      end
      
      -- check for any payment in APD Status
      if exists (select InvoiceID
                 from dbo.utb_invoice
                 where ClaimAspectID = @w_ClaimAspectID
                   and ItemTypeCD = 'P'
                   and StatusCD = 'APD')
      begin
        --trying to close vehicle without invoices created
        raiserror('1|Closing specified service channel(s) will cause closure of vehicle.  Invoice(s) need to be generated first. Please generate invoice(s) before closing.',16,1)
        return
      end 
      
      -- check for any fee in APD Status and its Biling model is not bulk and not paper
      
      set @CurrentFeeServiceChannelCD = null
      set @PertainsToWork = null
      set @InvoicingModelBillingCD = null
      set @CurrentFeeAssignmentType = null
      set @CurrentFeeAssignmentTypeID = null
      set @ClaimDispositionCD = null
      set @ClosingDocumentTypeID = null
      set @TotalLossBundleDocumentTypeID = null
      
	  select top 1 @CurrentFeeAssignmentTypeID = ca.InitialAssignmentTypeID
			from dbo.utb_claim_aspect ca
			where ca.ClaimAspectID = @w_ClaimAspectID
			
	  select top 1 @CurrentFeeAssignmentType = at.ServiceChannelDefaultCD
			from dbo.utb_assignment_type at
			where AssignmentTypeID = @CurrentFeeAssignmentTypeID 
			
	  select top 1 @PSAssignmentTypeID = at.AssignmentTypeID
			from dbo.utb_assignment_type at
			where at.Name = 'Program Shop Assignment' and at.ServiceChannelDefaultCD = 'PS'
			
	  select top 1 @ClosingDocumentTypeID = dt.DocumentTypeID
			from dbo.utb_document_type dt
			where dt.Name = 'Closing Documents'
			
	  select top 1 @TotalLossBundleDocumentTypeID = dt.DocumentTypeID
			from dbo.utb_document_type dt
			where dt.Name = 'Total Loss Bundle'
			 
      select @ClosingDocumentCount =  COUNT(d.DocumentTypeID)
				 from utb_claim_aspect ca 
				 inner join utb_claim_aspect_service_channel casc on ca.ClaimAspectID= casc.ClaimAspectID
				  inner join utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
				   inner join utb_document d on cascd.DocumentID = d.DocumentID
				    where ca.ClaimAspectID=@w_ClaimAspectID and ( d.DocumentTypeID = @ClosingDocumentTypeID or d.DocumentTypeID = @TotalLossBundleDocumentTypeID ) and d.SendToCarrierStatusCD = 'S'
			
		
      
      select top 1 @CurrentFeeServiceChannelCD = is1.ServiceChannelCD
      from dbo.utb_invoice i
      left join dbo.utb_invoice_service is1 on (i.InvoiceID = is1.InvoiceID)
      where i.ClaimAspectID = @w_ClaimAspectID
        and i.ItemTypeCD = 'F' 
        and i.StatusCD = 'APD'
        and i.EnabledFlag = 1

      if @CurrentFeeServiceChannelCD is not null
      begin
      select top 1  @InvoicingModelBillingCD = csc.InvoicingModelBillingCD
             from dbo.utb_client_service_channel csc
             where csc.ServiceChannelCD = @CurrentFeeServiceChannelCD 
               and csc.InsuranceCompanyID = @InsuranceCompanyID
           
			if ((@InvoicingModelBillingCD <> 'B') AND @InvoiceMethodCD = 'P' )
			begin
			raiserror('1|Invoice(s) need to be generated for vehicle. Please generate invoice(s) before closing vehicle.',16,1)
			return
			end
			
			if ((@InvoicingModelBillingCD = 'B') AND @InvoiceMethodCD = 'P' )
			begin
			select top 1  @ClaimDispositionCD = casc.DispositionTypeCD
             from dbo.utb_claim_aspect_service_channel casc
             where casc.ClaimAspectID = @w_ClaimAspectID
			
				if(@CurrentFeeAssignmentTypeID is not null AND @ClaimDispositionCD is not null)
				begin
					if ((select COUNT(*)
						from utb_invoice i
						where i.ClaimAspectID = @w_ClaimAspectID
							and i.ItemTypeCD = 'I' 
							and i.StatusCD = 'FS'
							and i.EnabledFlag = 1) = 0 AND @CurrentFeeAssignmentTypeID = @PSAssignmentTypeID AND @ClaimDispositionCD ='RC' )
					begin
						raiserror('1|Invoice(s) need to be generated for vehicle. Please generate invoice(s) before closing vehicle.',16,1)
						return
					end
				end
		
			end
		        
      end
    				    
				    if (@CurrentFeeAssignmentType = 'DA' AND @ClosingDocumentCount = 0)
				    begin
						raiserror('1|Closing Document(s) need to be generated for vehicle. Please generate Closing Document(s) before closing vehicle.',16,1)
						return
					end
                                
                        
    end 
    

    select @RepairTotalID = EstimateSummaryTypeID
    from dbo.utb_estimate_summary_type
    where Name='RepairTotal'
    
    if @@error <> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @RepairTotalID is null
    begin
      raiserror('102|%s|"RepairTotalID"|utb_estimate_summary_type',16,1,@ProcName)
      return
    end
    
    set @NoEstimateData = 0
    
    -- check if any service channels have estiamte attached
    
    if exists (select cascd.DocumentID
               from dbo.utb_claim_aspect_service_channel_document cascd
               left join dbo.utb_document d on (cascd.DocumentID = d.DocumentID)
               left join dbo.utb_document_type dt on (dt.DocumentTypeID = d.DocumentTypeID)
               left join dbo.utb_claim_aspect_service_channel casc on (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)
               where cascd.ClaimAspectServiceChannelID in (select ClaimAspectServiceChannelID from @tmpServiceChannels)                 
                 and dt.EstimateTypeFlag = 1
                 and d.EnabledFlag = 1
                 and casc.EnabledFlag = 1)
    begin
    
      --temp table to hold all estimate documentid for claim aspect sc list
      
      declare @tmpAllEstimates table (
        DocumentID                  bigint,
        ClaimAspectServiceChannelID bigint,
        ServiceChannelCD            varchar(2),
        DocumentTypeID              int,
        DirectionalCD               varchar(2),
        DuplicateFlag               bit,
        EstimateTypeCD              varchar(1),
        SupplementSeqNumber         int,
        EstimateLockedFlag          bit,
        VANFlag                     bit
      )

      -- temp table to hold eximtate documentid for claim aspect sc list and has repair total
      declare @tmpEstimatesWithData table (DocumentID bigint)
      
      -- temp table to hold eximtate documentid that we need to lock
      declare @tmpEstimatesToLock table (lockDocumentID bigint)
      
      insert into @tmpAllEstimates
      select cascd.DocumentID,
             cascd.ClaimAspectServiceChannelID,
             casc.ServiceChannelCD,
             d.DocumentTypeID,
             d.DirectionalCD,
             d.DuplicateFlag,
             d.EstimateTypeCD,
             d.SupplementSeqNumber,
             d.EstimateLockedFlag,
             ds.VANFlag
      from dbo.utb_claim_aspect_service_channel_document cascd
      left join dbo.utb_document d on (cascd.DocumentID = d.DocumentID)
      left join dbo.utb_document_type dt on (dt.DocumentTypeID = d.DocumentTypeID)
      left join dbo.utb_claim_aspect_service_channel casc on (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)
      left join dbo.utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
      where cascd.ClaimAspectServiceChannelID in (select ClaimAspectServiceChannelID from @tmpServiceChannels) 
        and dt.EstimateTypeFlag = 1
        and d.EnabledFlag = 1
        and casc.EnabledFlag = 1
        
      insert into @tmpEstimatesWithData
      select ae.DocumentID
      from @tmpAllEstimates ae left join dbo.utb_estimate_summary es on (ae.DocumentID = es.DocumentID)
      where es.EstimateSummaryTypeID = @RepairTotalID

      if @debug = 1
      begin
         select * from @tmpAllEstimates
          
         select distinct DocumentTypeID, SupplementSeqNumber 
         from @tmpAllEstimates
         where ServiceChannelCD in ('DA', 'DR')
          
      end

        -- QC Check (applies to DA and DR only)
        -- For every claim aspect service channel: 
		  --  1. there must exist an original and audited estimate/supplement for each seq. number
        --  2. For each seq number there must be atleast 1 non-duplicate
        
      set @QCCheckFailedFlag = 0
      set @QCCheckFailedReason = ''
      set @w_crlf = char(13) + char(10)
      --set @w_crlf = '|'

		declare csrClaimAspectServiceChannel cursor for
		  select distinct ClaimAspectServiceChannelID 
		  from @tmpAllEstimates
		  where ServiceChannelCD in ('DA', 'DR')
	    
		open csrClaimAspectServiceChannel
	    
		if @@error <> 0
		begin
		  raiserror('99|%s',16,1,@ProcName)
		  return
		end
	    
		fetch next from csrClaimAspectServiceChannel into @w_ClaimAspectServiceChannelID
	          
		if @@error <> 0
		begin
		  raiserror('99|%s',16,1,@ProcName)
		  return
		end

		if @@error <> 0
		begin
		  raiserror('99|%s',16,1,@ProcName)
		  return
		end
	    
	      
		while @@fetch_status = 0
		begin
		
			if @debug = 1
			begin
			   print '@w_ClaimAspectServiceChannelID = ' + convert(varchar, @w_ClaimAspectServiceChannelID)
			end
			
			SELECT @w_InitialAssignmentTypeId = InitialAssignmentTypeID
			FROM dbo.utb_claim_aspect ca
			inner join dbo.utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
			where ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
			
			SELECT @QCCheckFailedReason = @QCCheckFailedReason + 
										  cat.Name + ' ' + convert(varchar, ca.ClaimAspectNumber) + 
										  ' [' + casc.ServiceChannelCD + ']: ' + @w_crlf
			FROM utb_claim_aspect_service_channel casc 
			LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
			LEFT JOIN utb_claim_aspect_type cat on ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
			WHERE casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
			
			declare csrEstimateList cursor for
			  select distinct DocumentTypeID, SupplementSeqNumber 
			  from @tmpAllEstimates
			  where ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
		    
			open csrEstimateList
		    
			if @@error <> 0
			begin
			  raiserror('99|%s',16,1,@ProcName)
			  return
			end
		    
			fetch next from csrEstimateList into @w_DocumentTypeID, @w_SupplementSeqNumber
		          
			if @@error <> 0
			begin
			  raiserror('99|%s',16,1,@ProcName)
			  return
			end

			if @@error <> 0
			begin
			  raiserror('99|%s',16,1,@ProcName)
			  return
			end
		    
		      
			while @@fetch_status = 0
			begin
				set @w_DocumentIDOriginal = null
				set @w_DocumentIDAudited = null
				set @w_DocumentOriginalLocked = 0
            set @w_DocumentAuditedLocked = 0
            set @w_DocumentOriginalVANFlag = 0
            set @w_DocumentAuditedVANFlag = 0
				
				-- get the original estimate/supplement documentID
				SELECT @w_DocumentIDOriginal = DocumentID,
				       @w_DocumentOriginalLocked = EstimateLockedFlag,
				       @w_DocumentOriginalVANFlag = VANFlag
				FROM @tmpAllEstimates
				WHERE ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
				  AND DocumentTypeID = @w_DocumentTypeID
				  AND SupplementSeqNumber = @w_SupplementSeqNumber
				  AND EstimateTypeCD = 'O'
				  AND DuplicateFlag = 0
				
				-- get the audited estimate/supplement documentID
				SELECT @w_DocumentIDAudited = DocumentID,
				       @w_DocumentAuditedLocked = EstimateLockedFlag,
				       @w_DocumentAuditedVANFlag = VANFlag
				FROM @tmpAllEstimates
				WHERE ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
				  AND DocumentTypeID = @w_DocumentTypeID
				  AND SupplementSeqNumber = @w_SupplementSeqNumber
				  AND EstimateTypeCD = 'A'
				  AND DuplicateFlag = 0
				  
				if @debug = 1
				begin
				   PRINT '@w_DocumentTypeID = ' + isNull(CONVERT(varchar, @w_DocumentTypeID), 'NULL')
				   PRINT '@w_DocumentIDOriginal = ' + isNull(CONVERT(varchar, @w_DocumentIDOriginal), 'NULL')
				   PRINT '@w_DocumentIDAudited = ' + isNull(CONVERT(varchar, @w_DocumentIDAudited), 'NULL')
				end
				  
				-- must have a original estimate/supplement (non-duplicate)
				IF @w_DocumentIDOriginal IS NULL
				BEGIN
					SELECT @QCCheckFailedReason = @QCCheckFailedReason + 
												  '* Non-duplicate Original ' + 
												  Name +
												  CASE 
													WHEN DocumentTypeID = 10 THEN ' ' + convert(varchar, @w_SupplementSeqNumber)
													ELSE ''
												  END + 
												  ' is missing.' + @w_crlf
					FROM utb_document_type
					WHERE DocumentTypeID = @w_DocumentTypeID
					
					SET @QCCheckFailedFlag = 1
				END
				ELSE
				BEGIN
				   IF @w_DocumentOriginalVANFlag = 0 AND @w_DocumentOriginalLocked = 0
				   BEGIN
					   /*SELECT @QCCheckFailedReason = @QCCheckFailedReason + 
												     '* Non-duplicate Original ' + 
												     Name +
												     CASE 
													   WHEN DocumentTypeID = 10 THEN ' ' + convert(varchar, @w_SupplementSeqNumber)
													   ELSE ''
												     END + 
												     ' is not locked.' + @w_crlf
					   FROM utb_document_type
					   WHERE DocumentTypeID = @w_DocumentTypeID
   					
					   SET @QCCheckFailedFlag = 1*/
					   
					   INSERT INTO @tmpEstimatesToLock
					   (lockDocumentID)
					   VALUES 
					   (@w_DocumentIDOriginal)
				   END
				END
				-- must have an audited estimate/supplement (non-duplicate)
				IF @w_DocumentIDAudited IS NULL
				BEGIN
					SELECT @QCCheckFailedReason = @QCCheckFailedReason + 
												  '* Non-duplicate Audited ' + 
												  Name +
												  CASE 
													WHEN DocumentTypeID = 10 THEN ' ' + convert(varchar, @w_SupplementSeqNumber)
													ELSE ''
												  END + 
												  ' is missing.' + @w_crlf
					FROM utb_document_type
					WHERE DocumentTypeID = @w_DocumentTypeID
					
					SET @QCCheckFailedFlag = 1
				END
				ELSE
				BEGIN
				   IF @w_DocumentAuditedVANFlag = 0 AND @w_DocumentAuditedLocked = 0
				   BEGIN
					   /*SELECT @QCCheckFailedReason = @QCCheckFailedReason + 
												     '* Non-duplicate Audited ' + 
												     Name +
												     CASE 
													   WHEN DocumentTypeID = 10 THEN ' ' + convert(varchar, @w_SupplementSeqNumber)
													   ELSE ''
												     END + 
												     ' is not locked.' + @w_crlf
					   FROM utb_document_type
					   WHERE DocumentTypeID = @w_DocumentTypeID
   					
					   SET @QCCheckFailedFlag = 1*/

					   INSERT INTO @tmpEstimatesToLock
					   (lockDocumentID)
					   VALUES 
					   (@w_DocumentIDAudited)
				      
				   END
				END
								
				fetch next from csrEstimateList into @w_DocumentTypeID, @w_SupplementSeqNumber

				if @@error <> 0
				begin
					raiserror ('99|%s',16,1,@ProcName)
					return
				end
			end

			close csrEstimateList
			deallocate csrEstimateList


			fetch next from csrClaimAspectServiceChannel
			into @w_ClaimAspectServiceChannelID

			if @@error <> 0
			begin
				raiserror ('99|%s',16,1,@ProcName)
				return
			end
		end

		close csrClaimAspectServiceChannel
		deallocate csrClaimAspectServiceChannel
		
		if @debug = 1
		begin
		   print '@QCCheckFailedFlag = ' + convert(varchar, @QCCheckFailedFlag)
		   print '@QCCheckFailedReason = ' + @w_crlf+ @QCCheckFailedReason
		end
		

		-- for Pursuit audit we dont perform QC check that are applicable to DA.
		-- lets set the check failed flag to 0
		if @w_InitialAssignmentTypeId = @AssignmentTypeIdPursuitAudit 
		begin
		  select  @QCCheckFailedFlag = 0
		          , @QCCheckFailedReason = ''
		end
		
		IF @QCCheckFailedFlag = 1
		BEGIN
		   -- QC check failed. raise an error.
			SET @QCCheckFailedReason = '1|Can not close service channel(s) for the following reason(s).' + @w_crlf + @QCCheckFailedReason
			RAISERROR(@QCCheckFailedReason,16,1)
         --raiserror('1|Closing specified service channel(s) will cause closure of vehicle.  Invoice(s) need to be generated first. Please generate invoice(s) before closing.',16,1)
			RETURN
		END
		
		
		-- End of QC Check
		     
      -- get first document with missing repair total
      
      select @NoEstimateData = 1,
             @EstimateDocumentID = DocumentID
      from @tmpAllEstimates
      where DocumentID not in (select DocumentID from @tmpEstimatesWithData)
    end
    else
    begin
      -- No estimate data found
      if @w_InitialAssignmentTypeId <> @AssignmentTypeIdPursuitAudit and
         exists (select casc.DispositionTypeCD
                 from dbo.utb_claim_aspect_service_channel casc
                 where casc.ClaimAspectServiceChannelID in (select ClaimAspectServiceChannelID from @tmpServiceChannels)
                   and casc.DispositionTypeCD in ('RC','CO'))
      begin
        -- Cannot complete service channel w/o an estimate                 
        raiserror('1|Can not close service channel(s) without an estimate. Please attach estimate to service channel(s) before closing.',16,1)
        return
      
      end            
    end              
    
    if @NoEstimateData = 1 -- Missing Estiamte Date
    begin
      if @EstimateDocumentID is not null
      begin
        select @NoEstimateDataMessage = 'Can not close service channel ' + ufnGRC.Name + ' with missing Estimate data. Vehicle ' + convert(varchar,ca.ClaimAspectNumber) + ' ' + convert(varchar, ca.ClaimAspectNumber) + 
               ' - Document Source: ' + ds.Name + ' - Imaged on: ' + convert(varchar,d.CreatedDate,101) +
               ' ' + convert(varchar, d.createddate, 108)
        from dbo.utb_claim_aspect_service_channel casc 
        left join utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
        left join dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel','ServiceChannelCD') ufnGRC on (casc.ServiceChannelCD = ufnGRC.Code)
        left join utb_claim_aspect_service_channel_document cascd on (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)
        left join utb_document d on (cascd.DocumentID = d.DocumentID)
        left join utb_document_source ds on (d.DocumentSourceID = ds.DocumentSourceID)
        where cascd.DocumentID = @EstimateDocumentID
        
--        raiserror('1|%s',16,1,@ProcName)
--        return
      end
      else
      begin
        raiserror('1|Cannot close service channel with missing Estimate data. Please fill-in the estimate data for service channels.',16,1)
        return
      end
    end
    
    -- Early Bill check
    IF @EarlyBillFlag = 1
    BEGIN
      -- *****
      DECLARE @WarrantyExistsFlag as bit
      SET @WarrantyExistsFlag = 0
      IF EXISTS (SELECT wa.AssignmentID
                  FROM dbo.utb_warranty_assignment wa
                  LEFT JOIN @tmpServiceChannels sc ON wa.ClaimAspectServiceChannelID = sc.ClaimAspectServiceChannelID
                  WHERE wa.CancellationDate IS NULL)
      BEGIN
         SET @WarrantyExistsFlag = 1
      END
      
      
      -- warranty assignment check
      IF EXISTS(SELECT wa.AssignmentID
                  FROM @tmpServiceChannels sc 
                  LEFT JOIN dbo.utb_warranty_assignment wa ON sc.ClaimAspectServiceChannelID = wa.ClaimAspectServiceChannelID
                  WHERE wa.CancellationDate IS NULL
                    AND wa.WarrantyEndConfirmFlag = 0)
      BEGIN
        -- some warranty assignments are not complete yet
        raiserror('1|Cannot close Program Shop service channel with warranty assignments in progress.',16,1)
        return
         
      END

      -- Funds request/release check
      DECLARE @DispatchCount as int
      DECLARE @DispatchNumber_work as varchar(50)
      DECLARE @TotalRequestedAmount as decimal(9, 2)
      DECLARE @TotalReleasedAmount as decimal(9, 2)
      DECLARE @DispatchNumberIssues as varchar(250)
      
      DECLARE @tmpDispatchNumbers TABLE (
         DispatchNumber       varchar(50),
         CheckedFlag          bit,
         RequestedAmount      decimal(9, 2) NULL,
         ReleasedAmount       decimal(9, 2) NULL,
         ResidualAmount       decimal(9, 2) NULL
      )
      
      INSERT INTO @tmpDispatchNumbers (
         DispatchNumber,
         CheckedFlag
      )
      SELECT DISTINCT DispatchNumber, 0
      FROM utb_invoice i
      WHERE i.ClaimAspectID in (SELECT DISTINCT ClaimAspectID
                                 FROM @tmpServiceChannels)
        AND i.ItemTypeCD = 'I'
        AND i.EnabledFlag = 1
        
      SELECT @DispatchCount = COUNT(DispatchNumber) 
      FROM @tmpDispatchNumbers
      
      WHILE @DispatchCount > 0
      BEGIN
         SELECT @DispatchNumber_work = DispatchNumber 
         FROM @tmpDispatchNumbers
         WHERE CheckedFlag = 0
         
         SET @TotalRequestedAmount = NULL
         SET @TotalReleasedAmount = NULL
         
         SELECT @TotalRequestedAmount = TotalRequestedAmount,
                @TotalReleasedAmount = TotalReleasedAmount
         FROM dbo.ufnDispatchGetSummary(@DispatchNumber_work)
         
         UPDATE @tmpDispatchNumbers
         SET CheckedFlag = 1,
             RequestedAmount = @TotalRequestedAmount,
             ReleasedAmount = @TotalReleasedAmount,
             ResidualAmount = @TotalRequestedAmount - @TotalReleasedAmount
         WHERE DispatchNumber = @DispatchNumber_work
         
         
         SET @DispatchCount = @DispatchCount - 1
      END
      
      SET @DispatchNumberIssues = ''
      -- Make sure a release has been made for each request
      IF EXISTS(SELECT DispatchNumber
                  FROM @tmpDispatchNumbers
                  WHERE RequestedAmount > 0
                    AND ReleasedAmount IS NULL)
      BEGIN
          SELECT @DispatchNumberIssues = @DispatchNumberIssues + DispatchNumber + ', '
          FROM @tmpDispatchNumbers
          WHERE RequestedAmount > 0
            AND ReleasedAmount IS NULL

          -- remove the last comma
          --SET @DispatchNumberIssues = SUBSTRING(@DispatchNumberIssues, 1, LEN(@DispatchNumberIssues) - 3)

          -- some dispatches don't have payments released.
          raiserror('1|Cannot close service channel with no payments released for the following Dispatch Numbers: %s.',16,1, @DispatchNumberIssues)
          return
      END
        
      
      -- repairs must be completed.
      IF EXISTS(SELECT sc.ClaimAspectID
                  FROM @tmpServiceChannels sc
                  LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON sc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                  WHERE ServiceChannelCD = 'PS'
                    AND DispositionTypeCD not in ('TL', 'CO', 'ST')
                    AND WorkEndConfirmFlag = 0
                    AND EnabledFlag = 1)
      BEGIN
         -- repairs completion has not been comfirmed for some program shop channels
        raiserror('1|Cannot close service channel with Repair End Date not confirmed.',16,1)
        return
         
      END
      
      -- mandatory documents check
      DECLARE @tmpMissingDocs TABLE (
         DocumentTypeID       int,
         Name                 varchar(100)
      )
      
      INSERT INTO @tmpMissingDocs
      SELECT rd.DocumentTypeID, dt.Name
      FROM dbo.utb_client_required_document_type rd
      LEFT JOIN dbo.utb_document_type dt ON rd.DocumentTypeID = dt.DocumentTypeID
      WHERE rd.DocumentTypeID NOT IN (SELECT DISTINCT DocumentTypeID
                                    FROM @tmpServiceChannels sc
                                    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON sc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                                    LEFT JOIN dbo.utb_document d ON cascd.DocumentID = d.DocumentID
                                    WHERE ServiceChannelCD = 'PS'
                                      AND casc.EnabledFlag = 1
                                      AND d.EnabledFlag = 1)
        AND rd.InsuranceCompanyID = @InsuranceCompanyID
        AND rd.ServiceChannelCD = 'PS'
        AND rd.MandatoryFlag = 1
        AND rd.EnabledFlag = 1
      
      IF EXISTS (SELECT DocumentTypeID 
                  FROM @tmpMissingDocs)
      BEGIN
        DECLARE @missingDocuments varchar(500)
        
        SET  @missingDocuments = ''
        
        SELECT @missingDocuments = @missingDocuments + Name + ', '
        FROM @tmpMissingDocs
        
        IF LEN(@MissingDocuments) > 0 SELECT @missingDocuments = LEFT(@missingDocuments, len(@missingDocuments) - 2)
        
        -- program shop service channel is missing some required documents
        raiserror('1|Cannot close Program Shop service channel with missing required documents. The following documents are missing: %s',16,1, @missingDocuments)
        return
         
      END
      
    END
             
    -- Define event IDs
    -- Claim Closed
    select @EventIDCloseClaim = EventID from dbo.utb_event where Name = 'Claim Closed'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @EventIDCloseClaim is null
    begin
      raiserror('102|%s|"Claim Closed"|utb_event',16,1,@ProcName)
      return
    end
    
    -- Vehicle Closed
    select @EventIDCloseVehicle = EventID from dbo.utb_event where Name = 'Vehicle Closed'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @EventIDCloseVehicle is null
    begin
      raiserror('102|%s|"Vehicle Closed"|utb_event',16,1,@ProcName)
      return
    end
    
    select @EventIDServiceChannelCompleted = EventID from dbo.utb_event where Name = 'Vehicle Service Channel Completed'
    
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    if @EventIDServiceChannelCompleted is null
    begin
      raiserror('102|%s|"Vehicle Service Channel Completed"|utb_event',16,1,@ProcName)
      return
    end

    if @debug = 1
    begin
      print ''
      print 'Derived Variables:'
      print '@EventIDCloseClaim = ' + convert(varchar(10),@EventIDCloseClaim)
      print '@EventIDCloseVehicle = ' + convert(varchar(10),@EventIDCloseVehicle)
      print '@EventIDServiceChannelCompleted = ' + convert(varchar(10), @EventIDServiceChannelCompleted)
    end
    
    declare csrServiceChannels cursor for
      select ClaimAspectServiceChannelID from @tmpServiceChannels order by PrimaryFlag
      
    
    open csrServiceChannels
    
    if @@error <> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
    fetch next from csrServiceChannels into @w_ClaimAspectServiceChannelID
          
    if @@error <> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end

    set @now = CURRENT_TIMESTAMP
    
    
    --Begin Update
    
    begin transaction CompleteServiceChannelTran1
    
    if @@error <> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      return
    end
    
      
    while @@fetch_status = 0
    begin
      -- intialize variables for this iteration
            
      set @DispositionTypeCDWork = null
      set @ServiceChannelCDWork = null
      set @ServiceChannelName =null
      
      
      select @ServiceChannelCDWork = casc.ServiceChannelCD,
             @DispositionTypeCDWork = casc.DispositionTypeCD,
             @ServiceChannelName = ufnGRC.Name
      from dbo.utb_claim_aspect_service_channel casc
      left join dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel','ServiceChannelCD') ufnGRC on (casc.ServiceChannelCD = ufnGRC.Code)
      where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID

      if @@error <> 0
      begin
        raiserror('99|%s',16,1,@ProcName)
        return
      end
      
      if @debug = 1
      begin
        print ''
        print 'Start Loop Iteration:'
        print '    @ServiceChannelCDWork = ' + convert(varchar(10), @ServiceChannelCDWork)
        print '    @DispositionTypeCDWork = ' + convert(varchar(10),@DispositionTypeCDWork)
      end
      
      -- Verify the electronic assignments
      if exists (select cas.StatusID
                 from dbo.utb_claim_aspect_service_channel casc
                 inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
                 inner join dbo.utb_claim_aspect_status cas on ((ca.ClaimAspectID = cas.ClaimAspectID) and
                                                                (casc.ServiceChannelCD = cas.ServiceChannelCD))
                 where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
                   and cas.StatusID in (select StatusID
                                        from utb_status 
                                        where Name in ('Assignment Failure - OK to Resend',
                                                       'Assignment Failure')))
      begin
        -- There are failed assignments
        
        raiserror('1|The assignment for %s failed. Please send assignment before completing service channel.',16,1,@ServiceChannelName)
        rollback transaction
        return
      end
      else
      begin
        -- verify electronic assignment was sent, if not check if a fax assignment was sent
        
        if exists (select cas.StatusID
                   from dbo.utb_claim_aspect_service_channel casc
                   inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
                   inner join dbo.utb_claim_aspect_status cas on ((ca.ClaimAspectID = cas.ClaimAspectID) and
                                                                  (casc.ServiceChannelCD = cas.ServiceChannelCD))
                   where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
                     and cas.StatusID in (select StatusID
                                          from utb_status 
                                          where Name = 'Shop Selected - Assignment Not Sent')) and
          exists (select cas.StatusID
                  from dbo.utb_claim_aspect_service_channel casc
                  inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
                  inner join dbo.utb_claim_aspect_status cas on ((ca.ClaimAspectID = cas.ClaimAspectID) and
                                                                 (casc.ServiceChannelCD = cas.ServiceChannelCD))
                  where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
                    and cas.StatusID in (select StatusID
                                         from utb_status 
                                         where Name in ('Shop Selected - Fax Not Sent',
                                                        'Fax Failed - OK To Resend')))
        begin
            -- Electronic assignment was not sent and fax was not sent or failed
            raiserror ('1|The fax assignment for %s failed or was never sent. Please send the assignment before completing service channel.',16,1,@ServiceChannelName)
            rollback transaction
            return
        end
        
        -- Check to make sure repair cycle time data on repaired PS vehicles has been populated
        if (@ServiceChannelCDWork = 'PS') and -- program shop
           (@DispositionTypeCDWork = 'RC')    -- vehicle was repaired
           
        begin
          if ((select WorkEndDate 
               from dbo.utb_claim_vehicle v 
               left outer join utb_claim_aspect_service_channel casc on (v.ClaimAspectID = casc.ClaimAspectID) 
               where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID) IS NULL) or
             ((select WorkStartDate               
               from dbo.utb_claim_vehicle v 
               left outer join utb_claim_aspect_service_channel casc on (v.ClaimAspectID = casc.ClaimAspectID) 
               where casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID) IS NULL)
          begin
            -- Repair state information not entered
            raiserror('1|Repair start and end dates are required for program shop service channel before it can be completed.',16,1)
            rollback transaction
            return
          end
        end
      end 
      
      -- Check to see if this is the first time service channel is completed.
      if (select casc.OriginalCompleteDate
          from dbo.utb_claim_aspect_service_channel casc 
          where ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID) is null
      begin
        -- update original complete date on service channel record
        
        update dbo.utb_claim_aspect_service_channel
        set OriginalCompleteDate = @now
        where ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
        
        if @@error <> 0
        begin
          raiserror('104|%s|utb_claim_aspect_service_channel',16,1,@ProcName)
          rollback transaction
          return
        end
         IF @ServiceChannelCDWork = 'TL'
         begin
             -- update the total loss metric.
             EXEC dbo.uspTLUpdateMetrics @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
							                    @MetricDataPoint = 'OriginalClosingDate',
								                 @DataValue = @now

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspTLUpdateMetrics')
               ROLLBACK TRANSACTION
               RETURN
              END
         end
      end
      else
      begin
         IF @ServiceChannelCDWork = 'TL'
         begin
             -- update the total loss metric.
             EXEC dbo.uspTLUpdateMetrics @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
							                    @MetricDataPoint = 'ClosingDate',
								                 @DataValue = @now

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspTLUpdateMetrics')
               ROLLBACK TRANSACTION
               RETURN
              END
         end
      end
      
      
      -- throw event notifying of service channel completion
      select @w_HistoryDescription = 'Service Channel ' + @ServiceChannelName + ' for Vehicle ' + convert(varchar(3),ClaimAspectNumber) + ' completed.'
      from utb_claim_aspect_service_channel casc inner join utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
      where ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
      
      exec uspWorkflowNotifyEvent @EventID = @EventIDServiceChannelCompleted,
                                  @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                  @Description = @w_HistoryDescription,
                                  @UserID = @UserID,
                                  @ConditionValue = @ServiceChannelCDWork

      if @@error <> 0 
      begin
        raiserror ('107|%s|%s',16,1,@ProcName,'Vehicle Service Channel Completed')
        rollback transaction
        return
      end
      
 
      
      fetch next from csrServiceChannels
      into @w_ClaimAspectServiceChannelID
      
      if @@error <> 0
      begin
        raiserror ('99|%s',16,1,@ProcName)
        rollback transaction
        return
      end
   end
   
   close csrServiceChannels
   deallocate csrServiceChannels
     
   
   if @w_VehicleWillClose = 1
   begin
   
    if (select max(OriginalCompleteDate) 
       from dbo.utb_claim_aspect_service_channel
       where ClaimAspectID = @w_ClaimAspectID) = @now
    begin         
      -- one of vehicle's service channels original close date is now therefore since vehicle
      -- is closing this is the original close date for vehicle.
      update dbo.utb_claim_aspect
      set CompletedAnalystUserID = AnalystUserID,
          CompletedOwnerUserID = OwnerUserID,
          CompletedSupportUserID = SupportUserID
      where ClaimAspectID = @w_ClaimAspectID
      
      if @@error <> 0 
      begin
        raiserror('104|%s|utb_claim_aspect',16,1,@ProcName)
        rollback transaction
        return
      end
    end        
    -- Now that all tasks for vehicle are closed.. throw Vehicle Closed event
    select @w_HistoryDescription = 'Vehicle ' + convert(varchar(10),ca.ClaimAspectNumber) + ' Closed.'
    from @tmpServiceChannels sc 
    inner join utb_claim_aspect_service_channel casc on (sc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
    inner join utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)      
    where casc.PrimaryFlag = 1
    
    exec uspWorkflowNotifyEvent @EventID = @EventIDCloseVehicle,
                                @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                @Description = @w_HistoryDescription,
                                @UserID = @UserID                                                  
      
    if @@error <> 0 
    begin
      raiserror('99|%s',16,1,@ProcName)
      rollback transaction
      return
    end
   end
   
   -- lock the estimates
   IF EXISTS(SELECT * FROM @tmpEstimatesToLock)
   BEGIN

      UPDATE utb_document
      SET EstimateLockedFlag = 1
      FROM @tmpEstimatesToLock tmp
      WHERE DocumentID = tmp.lockDocumentID

   END
   
   update utb_claim_aspect
   set NewAssignmentAnalystFlag = 0,
       NewAssignmentOwnerFlag = 0,
       NewAssignmentSupportFlag = 0
   where ClaimAspectID = @w_ClaimAspectID       
    
   -- Add an audit log entry
   INSERT INTO dbo.utb_audit_log
   (AuditTypeCD, KeyID, KeyDescription, LogComment, SysLastUserID)
   VALUES
   ('C', @UserID, @w_ClaimAspectID, 'UserID : ' + convert(varchar, @UserID) + ' is completing the service channel ' + convert(varchar, @w_ClaimAspectServiceChannelID) + '. NewAssignmentOwnerFlag, NewAssignmentAnalystFlag and NewAssignmentSupportFlag are set to 0.', 0)

   if @w_ClaimWillClose = 1
   begin
      
      select @w_ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
      from @tmpServiceChannels
      where PrimaryFlag = 1
      
      exec uspWorkflowNotifyEvent @EventID = @EventIDCloseClaim,
                                  @ClaimAspectID = @w_ClaimAspectIDClaim,
                                  @Description = 'Claim Closed.',
                                  @UserID = @UserID
                                  
                                    
      if @@error <> 0 
      begin
        raiserror('99|%s',16,1,@ProcName)
        rollback transaction
        return
      end        
    
   end
    
    commit transaction CompleteServiceChannelTran1
    
    if @@error <> 0
    begin
      raiserror('99|%s',16,1)
      return
    end
  

    return  
end          

go

-- permissions

if exists(select name from sysobjects
          where name = 'uspWorkflowCompleteServiceChannel' and type='P')
begin
  grant execute on dbo.uspWorkflowCompleteServiceChannel to ugr_lynxapd
  commit
end
else
begin
  raiserror ('Stored proc creation failure.',16,1)
  rollback
end

go