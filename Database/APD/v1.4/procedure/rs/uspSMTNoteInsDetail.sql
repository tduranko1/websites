-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTNoteInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTNoteInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Inserts a new note into APD
*
* PARAMETERS:
* (I) @ClaimAspectID    The claim aspect this note is being inserted for  
* (I) @NoteTypeID       The note type
* (I) @StatusID         The status the note pertains to
* (I) @Note             The note itself
* (I) @UserID           The user entering the note
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTNoteInsDetail
    @ShopLocationID    udt_std_id_big,
    @Note             udt_std_note,
    @UserID           udt_std_id
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Note))) = 0 SET @Note = NULL


    -- Declare internal variables

    DECLARE @error      AS int
    DECLARE @now        AS datetime
    
    DECLARE @DocumentID                 AS udt_std_int_big,
            @CreatedUserRoleID          AS udt_std_id,
            @DocumentSourceID           AS udt_std_id,
            @DocumentTypeID             AS udt_std_id,
            @CreatedUserSupervisorFlag  AS udt_std_flag

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTNoteInsDetail'

    
    -- Set Database options
    
    SET NOCOUNT ON
    

    -- Check to make sure a valid Shop Location id was passed in
    IF  (@ShopLocationID IS NULL) OR
        (NOT EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END
    
        

    -- Get the Document Source = 'Note Entry'

    SELECT  @DocumentSourceID = DocumentSourceID
    FROM    utb_document_source
    WHERE   Name = 'Note Entry'


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @DocumentSourceID IS NULL
    BEGIN
        -- Document Source Record missing
    
        RAISERROR('102|%s|"Note Entry"|utb_document_source', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get the Document Type = 'Note'

    SELECT  @DocumentTypeID = DocumentTypeID
    FROM    utb_document_type
    WHERE   Name = 'Note'


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @DocumentTypeID IS NULL
    BEGIN
        -- SQL Server Error
    
        RAISERROR('102|%s|"Note"|utb_document_type', 16, 1, @ProcName)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Get User's primary role

    SELECT  @CreatedUserRoleID = RoleID
    FROM    utb_user_role
    WHERE   UserID = @UserID
    AND     PrimaryRoleFlag = 1


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @CreatedUserRoleID IS NULL
    BEGIN
        -- No Primary Role Found
    
        RAISERROR  ('110|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get User's SupervisorFlag

    SELECT  @CreatedUserSupervisorFlag = SupervisorFlag
    FROM    utb_user
    WHERE   UserID = @UserID


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    BEGIN TRANSACTION NoteInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- To insert a note, two tables must be populated.  The note itself is inserted into utb_document.  It is then attached
    -- to the particular claim aspect through a record inserted into utb_claim_aspect_document.

    -- Insert the note first

    INSERT INTO dbo.utb_document
   	(
        CreatedUserID,
        CreatedUserRoleID,
        DocumentSourceID,
        DocumentTypeID,
        --NoteTypeID,
        --StatusID,
        CreatedDate,
        CreatedUserSupervisorFlag,
        Note,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @UserID,
        @CreatedUserRoleID,
        @DocumentSourceID,
        @DocumentTypeID,
        --@NoteTypeID,
        --@StatusID,
        @now,
        @CreatedUserSupervisorFlag,
        @Note,
        @UserID,
        @now
    )
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    SELECT @DocumentID = SCOPE_IDENTITY()


    -- Attach the note to a claim aspect
    
    INSERT INTO dbo.utb_shop_location_document
    (
        ShopLocationID,
        DocumentID,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES
    (
        @ShopLocationID,
        @DocumentID,
        @UserID,
        @now
    )
        

    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|utb_claim_aspect_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION NoteInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN @DocumentID
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTNoteInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/