-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestHistoryProcessedInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestHistoryProcessedInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestHistoryProcessedInsDetail
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Inserts processed events into the utb_hyperquest_history_processed table
* Date:			03Dec2016
*
* PARAMETERS:  
*
* REVISIONS:	03Dec2016 - V1.0 - TVD - Initial development
*				06Jan2017 - V1.1 - TVD - Added new column EventStatus
*				03May2019 - V1.2 - TVD - Check if row exists 1st before insert
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestHistoryProcessedInsDetail]
	@LogID							udt_std_id_big
	, @LynxID						udt_std_int_big
	, @EventID						udt_std_int_small
	, @ClaimAspectIDClaim			udt_std_id_big
	, @ClaimAspectIDVehicle			udt_std_id_big
	, @ClaimAspectServiceChannelID	udt_std_id_big
	, @CompletedDate				udt_std_datetime
	, @ClaimAspectNumber			udt_std_int
	, @EventStatus					VARCHAR(50) = 'Processed'
AS
BEGIN
    DECLARE @error                  INT
    DECLARE @rowcount               INT
    DECLARE @debug                  BIT
    DECLARE @ProcName               VARCHAR(30)       -- Used for raise error stmts 
	DECLARE @SysLastUpdatedDate		DATETIME

    SET @ProcName = 'uspHyperquestHistoryProcessedInsDetail'

	SET @SysLastUpdatedDate = CURRENT_TIMESTAMP
    SET @debug = 0

	IF (@debug = 1)
	BEGIN
		PRINT 'Incoming Parameters...'
		PRINT '   @LogID: ' + CONVERT(VARCHAR(50),@LogID)
		PRINT '   @LynxID: ' + CONVERT(VARCHAR(50),@LynxID)
		PRINT '   @EventID: ' + CONVERT(VARCHAR(50),@EventID)
		PRINT '   @ClaimAspectIDClaim: ' + CONVERT(VARCHAR(50),@ClaimAspectIDClaim)
		PRINT '   @ClaimAspectIDVehicle: ' + CONVERT(VARCHAR(50),@ClaimAspectIDVehicle)
		PRINT '   @ClaimAspectServiceChannelID: ' + CONVERT(VARCHAR(50),@ClaimAspectServiceChannelID)
		PRINT '   @CompletedDate: ' + CONVERT(VARCHAR(50),@CompletedDate)
		PRINT '   @SysLastUserID: 0'
		PRINT '   @SysLastUpdatedDate: ' + CONVERT(VARCHAR(50),@SysLastUpdatedDate)
		PRINT '   @EventStatus: ' + @EventStatus
	END    

	----------------------------------------------------
    -- Start the Transaction
	----------------------------------------------------
	----------------------------------------------------
    -- Check if row exists first before processing
	----------------------------------------------------
	IF NOT EXISTS (
		SELECT 
			LogID
		FROM
			utb_hyperquest_history_processed WITH(NOLOCK)
		WHERE
			LogID = @LogID
			AND LynxID = @LynxID
	)
	BEGIN
		BEGIN TRANSACTION ProcessHistoryInsDetailTran1

		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
    
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END
    
		----------------------------------------------------
		-- Insert a record into hyperquest history processed
		----------------------------------------------------
		INSERT INTO dbo.utb_hyperquest_history_processed
		(
			LogID
			, LynxID
			, EventID
			, ClaimAspectIDClaim
			, ClaimAspectIDVehicle
			, ClaimAspectServiceChannelID
			, CompletedDate
			, ClaimAspectNumber
			, SysLastUserID
			, SysLastUpdatedDate
			, EventStatus
		)
		VALUES
		(   
			@LogID
			, @LynxID
			, @EventID
			, @ClaimAspectIDClaim
			, @ClaimAspectIDVehicle
			, @ClaimAspectServiceChannelID
			, @CompletedDate				
			, @ClaimAspectNumber			
			, 0				
			, @SysLastUpdatedDate	
			, @EventStatus		
		)

		SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
		IF @error <> 0
		BEGIN
		   -- Insertion failure
    
			RAISERROR('105|%s|utb_history_log', 16, 1, @ProcName)
			ROLLBACK TRANSACTION 
			RETURN
		END


		COMMIT TRANSACTION ProcessHistoryInsDetailTran1

		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
    
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END


		RETURN @rowcount
	END
	ELSE
	BEGIN
		RETURN 999
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestHistoryProcessedInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestHistoryProcessedInsDetail TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/