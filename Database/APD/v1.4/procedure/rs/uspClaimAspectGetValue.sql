-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectGetValue' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimAspectGetValue 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimAspectGetValue
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     This will get the claim aspect ID for a claim
*
* PARAMETERS:  
*				@DocumentID
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimAspectGetValue
    @DocumentID  udt_std_int_big
AS
BEGIN
    DECLARE @now DATETIME
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimAspectGetValue'

    SET NOCOUNT ON
    SET @now = CURRENT_TIMESTAMP
   
/*    SELECT 
           ca.ClaimAspectID
    FROM 
		dbo.utb_claim_aspect ca
		LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc 
			ON casc.ClaimAspectID = ca.ClaimAspectID
		LEFT JOIN dbo.utb_claim_aspect_Service_Channel_document cascd 
			ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)
    WHERE 
		cascd.DocumentID = @DocumentID
*/    
	SELECT 
		casc.ClaimAspectID
	FROM
		utb_claim_aspect_Service_Channel_document cascd
		INNER JOIN utb_Claim_Aspect_Service_Channel casc 
			ON cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
	WHERE
		cascd.DocumentID = @DocumentID
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectGetValue' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimAspectGetValue TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/