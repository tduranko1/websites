-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientBundling' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientBundling 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientBundling
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client assignments from the database by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID 
* RESULT SET:
*   All data related to insurance client assignments
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClientBundling
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
		b.BundlingID
		, ISNULL(b.Name,'') BundlingName
		, ISNULL(b.DocumentTypeID,'') DocumentTypeID
		, b.MessageTemplateID
		, ISNULL(b.EnabledFlag,'') EnabledFlag
		, cb.BundlingID
		, cb.InsuranceCompanyID
		, ISNULL(cb.OfficeID,'') OfficeID
		, cb.AutoNotifyAdjusterFlag
		, ISNULL(cb.ReturnDocPackageTypeCD,'') ReturnDocPackageTypeCD
		, ISNULL(cb.ReturnDocRoutingCD,'') ReturnDocRoutingCD
		, ISNULL(cb.ReturnDocRoutingValue,'') ReturnDocRoutingValue
		, ISNULL(cb.ServiceChannelCD,'') ServiceChannelCD
		, ISNULL(dt.DisplayOrder,'') DisplayOrder
		, ISNULL(dt.DocumentClassCD,'') DocumentClassCD
		, dt.EnabledFlag
		, dt.EstimateTypeFlag
		, ISNULL(dt.Name,'') DocumentTypeName
		, dt.SysMaintainedFlag
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_document_type dt
			ON dt.DocumentTypeId = b.DocumentTypeId
	WHERE 
		b.EnabledFlag = 1	 
		AND cb.InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		b.[Name] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientBundling' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientBundling TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientBundling TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/