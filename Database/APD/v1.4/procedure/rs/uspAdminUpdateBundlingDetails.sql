-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateBundlingDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminUpdateBundlingDetails 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminUpdateBundlingDetails
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Updates existing client information in the utb_insurance table.
*
* PARAMETERS:  
*
*	@iBundlingID INT
*	@iMessageTemplateID INT
*	@iDocumentTypeID INT
*   @vName VARCHAR(50)
*   @vDescription VARCHAR(8000)
*   @vDirectionalCD VARCHAR(4)
*   @bDirectionToPayFlag BIT
*   @bDuplicateFlag BIT
*   @bFinalEstimateFlag BIT
*   @bMandatoryFlag BIT
*   @bVANFlag BIT
*   @bWarrantyFlag BIT
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminUpdateBundlingDetails
	@iBundlingID INT
	, @iMessageTemplateID INT
	, @iDocumentTypeID INT
    , @vName VARCHAR(50)
    , @vDescription VARCHAR(8000)
    , @vDirectionalCD VARCHAR(4)
    , @bDirectionToPayFlag BIT
    , @bDuplicateFlag BIT
    , @bFinalEstimateFlag BIT
    , @bMandatoryFlag BIT
    , @bVANFlag BIT
    , @bWarrantyFlag BIT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 
    DECLARE @ProcName AS varchar(30)
    DECLARE @bRequiredExists AS BIT

    SET @ProcName = 'uspAdminUpdateBundlingDetails'
    SET @bRequiredExists = 0

    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	SET @error = 0

    -- Check to make sure a valid bundling id was passed in
    IF  (@iBundlingID IS NULL) 
        OR (NOT EXISTS(SELECT BundlingID FROM utb_Bundling WHERE BundlingID = @iBundlingID))
    BEGIN
        -- Invalid Bundling ID
        RAISERROR('101|%s|@iBundlingID|%u', 16, 1, @ProcName, @iBundlingID)
        RETURN
    END

    -- Check to make sure a valid Message Template ID was passed in
    IF  (@iMessageTemplateID IS NULL) 
        OR (NOT EXISTS(SELECT MessageTemplateID FROM utb_message_template WHERE MessageTemplateID = @iMessageTemplateID))
    BEGIN
        -- Invalid Message Template ID
        RAISERROR('101|%s|@iMessageTemplateID|%u', 16, 1, @ProcName, @iMessageTemplateID)
        RETURN
    END

    -- Check to make sure a valid Document Template ID was passed in
    IF  (@iDocumentTypeID IS NULL) 
        OR (NOT EXISTS(SELECT DocumentTypeID FROM utb_document_type WHERE DocumentTypeID = @iDocumentTypeID))
    BEGIN
        -- Invalid Message Template ID
        RAISERROR('101|%s|@iDocumentTypeID|%u', 16, 1, @ProcName, @iDocumentTypeID)
        RETURN
    END

    -- Check to see if required docs already exist or not
    IF  (@vDirectionalCD = '99')
        OR (NOT EXISTS(SELECT BundlingID FROM utb_bundling_document_type WHERE BundlingID = @iBundlingID AND DocumentTypeID = @iDocumentTypeID))
    BEGIN
        -- No record exists in utb_bundling_document_type
		SET @bRequiredExists = 0
    END
    ELSE
    BEGIN
        -- Record exists in utb_bundling_document_type
		SET @bRequiredExists = 1
    END

	------------------------------------
	-- Do the utb_Bundling table updates	
	------------------------------------
    IF  (@vName IS NOT NULL) 
    BEGIN
		UPDATE dbo.utb_Bundling
		SET
			 [Name] = @vName
			 , SysLastUpdatedDate = @now
		WHERE
			BundlingID = @iBundlingID

		SET @error = @error + @@ERROR
	END
	ELSE
	BEGIN
        -- Invalid Name
        RAISERROR('101|%s|@vName|%u', 16, 1, @ProcName, @vName)
        RETURN
	END
	
	------------------------------------
	-- Do the utb_message_template table updates	
	------------------------------------
    IF  (@vDescription IS NOT NULL) 
    BEGIN
		UPDATE dbo.utb_message_template
		SET
			 [Description] = @vDescription
			 , SysLastUpdatedDate = @now
		WHERE
			MessageTemplateID = @iMessageTemplateID

		SET @error = @error + @@ERROR
	END
	ELSE
	BEGIN
        -- Invalid Description
        RAISERROR('101|%s|@vDescription|%u', 16, 1, @ProcName, @vDescription)
        RETURN
	END

	------------------------------------
	-- Do the utb_bundling_document_type table updates	
	------------------------------------
    IF  (@bRequiredExists = 1)
		AND (@bDirectionToPayFlag IS NOT NULL)
		AND (@bDuplicateFlag IS NOT NULL)
		AND (@bFinalEstimateFlag IS NOT NULL )
		AND (@bMandatoryFlag  IS NOT NULL)
		AND (@bVANFlag  IS NOT NULL)
		AND (@bWarrantyFlag  IS NOT NULL) 
    BEGIN
		UPDATE dbo.utb_bundling_document_type
		SET
			 DirectionalCD = @vDirectionalCD
			 , DirectionToPayFlag = @bDirectionToPayFlag
			 , DuplicateFlag = @bDuplicateFlag
			 , FinalEstimateFlag = @bFinalEstimateFlag
			 , MandatoryFlag = @bMandatoryFlag
			 , VANFlag = @bVANFlag
			 , WarrantyFlag = @bWarrantyFlag
			 , SysLastUpdatedDate = @now
		WHERE
			BundlingID = @iBundlingID 
			AND DocumentTypeID = @iDocumentTypeID

			SET @error = @error + @@ERROR
	END

	SELECT @error AS RetCode

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateBundlingDetails' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminUpdateBundlingDetails TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminUpdateBundlingDetails TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/