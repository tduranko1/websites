-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopLoadInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopLoadInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Insert a new Shop
*

*
* RESULT SET:
* ShopLocationID                          The newly created Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopLoadInsDetail
(
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @AlignmentFourWheel                 udt_std_money=NULL,
    @AlignmentTwoWheel                  udt_std_money=NULL,
    @BusinessTypeCD                     udt_std_cd=NULL,
    @CaulkingSeamSealer                 udt_std_money=NULL,
    @CertifiedFirstId                   udt_std_desc_short,
    @ChipGuard                          udt_std_money=NULL,
    @CoolantGreen                       udt_std_money=NULL,
    @CoolantRed                         udt_std_money=NULL,
    @CorrosionProtection                udt_std_money=NULL,
    @CountyTaxPct                       udt_std_pct=NULL,
    @CoverCar                           udt_std_money=NULL,
    @DailyStorage                       udt_std_money=NULL,
    @DiscountDomestic                   udt_std_pct=NULL,
    @DiscountImport                     udt_std_pct=NULL,
    @DiscountPctSideBackGlass           udt_std_pct=NULL,
    @DiscountPctWindshield              udt_std_pct=NULL,
    @DrivingDirections                  udt_std_desc_long=NULL,
    @EmailAddress                       udt_web_email=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @FedTaxId                           udt_fed_tax_id=NULL,
    @FinalSubmitFlag                    udt_std_flag=0,
    @FlexAdditive                       udt_std_money=NULL,
    @GlassOutsourceServiceFee           udt_std_money=NULL,
    @GlassReplacementChargeTypeCD       udt_std_cd=NULL,
    @GlassSubletServiceFee              udt_std_money=NULL,
    @GlassSubletServicePct              udt_std_pct=NULL,
    @HazardousWaste                     udt_std_money=NULL,
    @HourlyRateMechanical               udt_std_money=NULL,
    @HourlyRateRefinishing              udt_std_money=NULL,
    @HourlyRateSheetMetal               udt_std_money=NULL,
    @HourlyRateUnibodyFrame             udt_std_money=NULL,
    @MaxWeeklyAssignments               udt_std_int=NULL,
    @MunicipalTaxPct                    udt_std_pct=NULL,
    @Name                               udt_std_name=NULL,
    @OEMDiscounts                       udt_std_desc_mid=NULL,
    @OperatingFridayEndTime             udt_std_time=NULL,
    @OperatingFridayStartTime           udt_std_time=NULL,
    @OperatingMondayEndTime             udt_std_time=NULL,
    @OperatingMondayStartTime           udt_std_time=NULL,
    @OperatingSaturdayEndTime           udt_std_time=NULL,
    @OperatingSaturdayStartTime         udt_std_time=NULL,
    @OperatingSundayEndTime             udt_std_time=NULL,
    @OperatingSundayStartTime           udt_std_time=NULL,
    @OperatingThursdayEndTime           udt_std_time=NULL,
    @OperatingThursdayStartTime         udt_std_time=NULL,
    @OperatingTuesdayEndTime            udt_std_time=NULL,
    @OperatingTuesdayStartTime          udt_std_time=NULL,
    @OperatingWednesdayEndTime          udt_std_time=NULL,
    @OperatingWednesdayStartTime        udt_std_time=NULL,
    @OtherTaxPct                        udt_std_pct=NULL,
    @PartsRecycledMarkupPct             udt_std_pct=NULL,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @PreferredEstimatePackageID         udt_std_int_tiny=NULL,
    @PullSetUp                          udt_std_hours=NULL,
    @PullSetUpMeasure                   udt_std_hours=NULL,
    @R12EvacuateRecharge                udt_std_money=NULL,
    @R12EvacuateRechargeFreon           udt_std_money=NULL,
    @R134EvacuateRecharge               udt_std_money=NULL,
    @R134EvacuateRechargeFreon          udt_std_money=NULL,
    @RefinishSingleStageHourly          udt_std_money=NULL,
    @RefinishSingleStageMax             udt_std_money=NULL,
    @RefinishThreeStageCCMaxHrs         udt_std_hours=NULL,
    @RefinishThreeStageHourly           udt_std_money=NULL,
    @RefinishThreeStageMax              udt_std_money=NULL,
    @RefinishTwoStageCCMaxHrs           udt_std_hours=NULL,
    @RefinishTwoStageHourly             udt_std_money=NULL,
    @RefinishTwoStageMax                udt_std_money=NULL,
    @RepairFacilityTypeCD               udt_std_cd=NULL,
    @SalesTaxPct                        udt_std_pct=NULL,
    @ShopID                             udt_std_id_big=NULL,
    @BusinessInfoID                     udt_std_id_big=NULL,
    @SLPCellAreaCode                    udt_ph_area_code=NULL,
    @SLPCellExchangeNumber              udt_ph_exchange_number=NULL,
    @SLPCellExtensionNumber             udt_ph_extension_number=NULL,
    @SLPCellUnitNumber                  udt_ph_unit_number=NULL,
    @SLPEmailAddress                    udt_web_email=NULL,
    @SLPFaxAreaCode                     udt_ph_area_code=NULL,                     
    @SLPFaxExchangeNumber               udt_ph_exchange_number=NULL,
    @SLPFaxExtensionNumber              udt_ph_extension_number=NULL,
    @SLPFaxUnitNumber                   udt_ph_unit_number=NULL,
    @SLPGenderCD                        udt_per_gender_cd=NULL,
    @SLPName                            udt_std_name,
    @SLPPagerAreaCode                   udt_ph_area_code=NULL,
    @SLPPagerExchangeNumber             udt_ph_exchange_number=NULL,
    @SLPPagerExtensionNumber            udt_ph_extension_number=NULL,
    @SLPPagerUnitNumber                 udt_ph_unit_number=NULL,
    @SLPPersonnelID                     udt_std_id_big=NULL,              
    @SLPPersonnelTypeID                 udt_std_int_tiny=NULL,
    @SLPPhoneAreaCode                   udt_ph_area_code=NULL,
    @SLPPhoneExchangeNumber             udt_ph_exchange_number=NULL,
    @SLPPhoneExtensionNumber            udt_ph_extension_number=NULL,
    @SLPPhoneUnitNumber                 udt_ph_unit_number=NULL,
    @SLPPreferredContactMethodID        udt_std_int_tiny=NULL,
    @SLPShopManagerFlag                 udt_std_flag,
    @SPCellAreaCode                     udt_ph_area_code=NULL,
    @SPCellExchangeNumber               udt_ph_exchange_number=NULL,
    @SPCellExtensionNumber              udt_ph_extension_number=NULL,
    @SPCellUnitNumber                   udt_ph_unit_number=NULL,
    @SPEmailAddress                     udt_web_email=NULL,
    @SPFaxAreaCode                      udt_ph_area_code=NULL,
    @SPFaxExchangeNumber                udt_ph_exchange_number=NULL,
    @SPFaxExtensionNumber               udt_ph_extension_number=NULL,
    @SPFaxUnitNumber                    udt_ph_unit_number=NULL,
    @SPGenderCD                         udt_std_cd=NULL,                        
    @SPName                             udt_std_name,
    @SPPagerAreaCode                    udt_ph_area_code=NULL,
    @SPPagerExchangeNumber              udt_ph_exchange_number=NULL,
    @SPPagerExtensionNumber             udt_ph_extension_number=NULL,
    @SPPagerUnitNumber                  udt_ph_unit_number=NULL,
    @SPPersonnelID                      udt_std_id_big=NULL,
    @SPPersonnelTypeID                  udt_std_int_tiny=NULL,
    @SPPhoneAreaCode                    udt_ph_area_code=NULL,
    @SPPhoneExchangeNumber              udt_ph_exchange_number=NULL,
    @SPPhoneExtensionNumber             udt_ph_extension_number=NULL,
    @SPPhoneUnitNumber                  udt_ph_unit_number=NULL,
    @SPPreferredContactMethodID         udt_std_int_tiny=NULL,
    @SPShopManagerFlag                  udt_std_flag,
    @StripePaintPerPanel                udt_std_money=NULL,
    @StripePaintPerSide                 udt_std_money=NULL,
    @StripeTapePerPanel                 udt_std_money=NULL,
    @StripeTapePerSide                  udt_std_money=NULL,
    @TireMountBalance                   udt_std_money=NULL,  
    @TowInChargeTypeCD                  udt_std_cd=NULL,
    @TowInFlatFee                       udt_std_money=NULL,
    @Undercoat                          udt_std_money=NULL,
    @WarrantyPeriodRefinishCD           udt_std_cd=NULL,
    @WarrantyPeriodWorkmanshipCD        udt_std_cd=NULL,
    @WebSiteAddress                     udt_web_address=NULL    
)

AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @DataEntryDate      AS DATETIME
    DECLARE @FinalSubmitDate    AS DATETIME
    DECLARE @tDataReviewDate    AS DATETIME 
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 
    DECLARE @WorkmanshipID      AS tinyint
    DECLARE @RefinishID         AS tinyint
    DECLARE @ShopLoadID         AS udt_std_id_big
    DECLARE @now                AS datetime

    SET @ProcName = 'uspSMTShopLoadInsDetail'
    SET @now = CURRENT_TIMESTAMP
    SET @FinalSubmitDate = NULL

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0                     SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0                     SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0                  SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0                SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0                 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0                   SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@BusinessTypeCD))) = 0               SET @BusinessTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@CertifiedFirstId))) = 0             SET @CertifiedFirstId = NULL
    IF LEN(RTRIM(LTRIM(@DrivingDirections))) = 0            SET @DrivingDirections = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0                 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FedTaxId))) = 0                     SET @FedTaxId = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0                  SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0            SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0           SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0                SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0                         SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0                SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0          SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0         SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0              SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@RepairFacilityTypeCD))) = 0         SET @RepairFacilityTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@WebSiteAddress))) = 0               SET @WebSiteAddress = NULL
    IF LEN(RTRIM(LTRIM(@SLPPersonnelID))) = 0               SET @SLPPersonnelID = NULL
    IF LEN(RTRIM(LTRIM(@SPPersonnelID))) = 0                SET @SPPersonnelID = NULL
    IF LEN(RTRIM(LTRIM(@SLPCellAreaCode))) = 0              SET @SLPCellAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SLPCellExchangeNumber))) = 0        SET @SLPCellExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPCellExtensionNumber))) = 0       SET @SLPCellExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPCellUnitNumber))) = 0            SET @SLPCellUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPEmailAddress))) = 0              SET @SLPEmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@SLPFaxAreaCode))) = 0               SET @SLPFaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SLPFaxExchangeNumber))) = 0         SET @SLPFaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPFaxExtensionNumber))) = 0        SET @SLPFaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPFaxUnitNumber))) = 0             SET @SLPFaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPName))) = 0                      SET @SLPName = NULL
    IF LEN(RTRIM(LTRIM(@SLPPagerAreaCode))) = 0             SET @SLPPagerAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SLPPagerExchangeNumber))) = 0       SET @SLPPagerExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPPagerExtensionNumber))) = 0      SET @SLPPagerExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPPagerUnitNumber))) = 0           SET @SLPPagerUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPPhoneAreaCode))) = 0             SET @SLPPhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SLPPhoneExchangeNumber))) = 0       SET @SLPPhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPPhoneExtensionNumber))) = 0      SET @SLPPhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SLPPhoneUnitNumber))) = 0           SET @SLPPhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPCellAreaCode))) = 0               SET @SPCellAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SPCellExchangeNumber))) = 0         SET @SPCellExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPCellExtensionNumber))) = 0        SET @SPCellExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPCellUnitNumber))) = 0             SET @SPCellUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPEmailAddress))) = 0               SET @SPEmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@SPFaxAreaCode))) = 0                SET @SPFaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SPFaxExchangeNumber))) = 0          SET @SPFaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPFaxExtensionNumber))) = 0         SET @SPFaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPFaxUnitNumber))) = 0              SET @SPFaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPGenderCD))) = 0                   SET @SPGenderCD = NULL
    IF LEN(RTRIM(LTRIM(@SPName))) = 0                       SET @SPName = NULL
    IF LEN(RTRIM(LTRIM(@SPPagerAreaCode))) = 0              SET @SPPagerAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SPPagerExchangeNumber))) = 0        SET @SPPagerExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPPagerExtensionNumber))) = 0       SET @SPPagerExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPPagerUnitNumber))) = 0            SET @SPPagerUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPPhoneAreaCode))) = 0              SET @SPPhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@SPPhoneExchangeNumber))) = 0        SET @SPPhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPPhoneExtensionNumber))) = 0       SET @SPPhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@SPPhoneUnitNumber))) = 0            SET @SPPhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@TowInChargeTypeCD))) = 0            SET @TowInChargeTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@WarrantyPeriodRefinishCD))) = 0     SET @WarrantyPeriodRefinishCD = NULL
    IF LEN(RTRIM(LTRIM(@WarrantyPeriodWorkmanshipCD))) = 0  SET @WarrantyPeriodWorkmanshipCD = NULL
    IF LEN(RTRIM(LTRIM(@WebSiteAddress))) = 0               SET @WebSiteAddress = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayEndTime))) = 0       SET @OperatingFridayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayStartTime))) = 0     SET @OperatingFridayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayEndTime))) = 0       SET @OperatingMondayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayStartTime))) = 0     SET @OperatingMondayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayEndTime))) = 0     SET @OperatingSaturdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayStartTime))) = 0   SET @OperatingSaturdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayEndTime))) = 0       SET @OperatingSundayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayStartTime))) = 0     SET @OperatingSundayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayEndTime))) = 0     SET @OperatingThursdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayStartTime))) = 0   SET @OperatingThursdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayEndTime))) = 0      SET @OperatingTuesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayStartTime))) = 0    SET @OperatingTuesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayEndTime))) = 0    SET @OperatingWednesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayStartTime))) = 0  SET @OperatingWednesdayStartTime = NULL


    IF (@ShopID = 0) OR (LEN(RTRIM(LTRIM(@ShopID))) = 0) SET @ShopID = NULL
    IF (@BusinessInfoID = 0) OR (LEN(RTRIM(LTRIM(@BusinessInfoID))) = 0) SET @BusinessInfoID = NULL
    

    -- Apply edits
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Location Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @PreferredEstimatePackageID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT EstimatePackageID FROM utb_estimate_package WHERE EstimatePackageID = @PreferredEstimatePackageID
                                                                            AND EnabledFlag = 1)
        BEGIN
           -- Invalid Estimate Package
            RAISERROR('101|%s|@PreferredEstimatePackageID|%u', 16, 1, @ProcName, @PreferredEstimatePackageID)
            RETURN
        END
    END

    

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END


     -- Determine if a record exists in the utb_shop_load table for this CertifiedFirstID.  If so, update it.
    -- Otherwise create a new record.
    SET @ShopLoadID = ISNULL((SELECT ShopLoadID FROM dbo.utb_shop_load WHERE CertifiedFirstID = @CertifiedFirstID), 0)
        
    
    
    IF (@FinalSubmitFlag = 1)
    BEGIN
      SET @FinalSubmitDate = @now
      SET @DataEntryDate = (SELECT DataEntryDate FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID)
    END
    ELSE 
      SET @DataEntryDate = @now
      


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocationInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

       
    IF (@ShopLoadID = 0)
    BEGIN

    INSERT INTO dbo.utb_shop_load
   	(
        Address1,
        Address2,
        AddressCity,
        AddressCounty,
        AddressState,
        AddressZip,
        AlignmentFourWheel,
        AlignmentTwoWheel,
        BusinessTypeCD,
        CaulkingSeamSealer,
        CertifiedFirstId,
        ChipGuard,
        CoolantGreen,
        CoolantRed,
        CorrosionProtection,
        CountyTaxPct,
        CoverCar,
        DailyStorage,
        DataEntryDate,
        DiscountDomestic,
        DiscountImport,
        DiscountPctSideBackGlass,
        DiscountPctWindshield,
        DrivingDirections,
        EmailAddress,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        FedTaxId,
        FinalSubmitDate,
        FinalSubmitFlag,
        FlexAdditive,
        GlassOutsourceServiceFee,
        GlassReplacementChargeTypeCD,
        GlassSubletServiceFee,
        GlassSubletServicePct,
        HazardousWaste,
        HourlyRateMechanical,
        HourlyRateRefinishing,
        HourlyRateSheetMetal,
        HourlyRateUnibodyFrame,
        MaxWeeklyAssignments,
        MunicipalTaxPct,
        Name,
        OEMDiscounts,
        OperatingFridayEndTime,
        OperatingFridayStartTime,
        OperatingMondayEndTime,
        OperatingMondayStartTime,
        OperatingSaturdayEndTime,
        OperatingSaturdayStartTime,
        OperatingSundayEndTime,
        OperatingSundayStartTime,
        OperatingThursdayEndTime,
        OperatingThursdayStartTime,
        OperatingTuesdayEndTime,
        OperatingTuesdayStartTime,
        OperatingWednesdayEndTime,
        OperatingWednesdayStartTime,
        OtherTaxPct,
        PartsRecycledMarkupPct,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        PreferredEstimatePackageID,
        PullSetUp,
        PullSetUpMeasure,
        R12EvacuateRecharge,
        R12EvacuateRechargeFreon,
        R134EvacuateRecharge,
        R134EvacuateRechargeFreon,
        RefinishSingleStageHourly,
        RefinishSingleStageMax,
        RefinishThreeStageCCMaxHrs,
        RefinishThreeStageHourly,
        RefinishThreeStageMax,
        RefinishTwoStageCCMaxHrs,
        RefinishTwoStageHourly,
        RefinishTwoStageMax,
        RepairFacilityTypeCD,
        SalesTaxPct,
        ShopID,
        ShopLocationID,
        SLPCellAreaCode,
        SLPCellExchangeNumber,
        SLPCellExtensionNumber,
        SLPCellUnitNumber,
        SLPEmailAddress,
        SLPFaxAreaCode,
        SLPFaxExchangeNumber,
        SLPFaxExtensionNumber,
        SLPFaxUnitNumber,
        SLPGenderCD,
        SLPName,
        SLPPagerAreaCode,
        SLPPagerExchangeNumber,
        SLPPagerExtensionNumber,
        SLPPagerUnitNumber,
        SLPPersonnelID,
        SLPPersonnelTypeID,
        SLPPhoneAreaCode,
        SLPPhoneExchangeNumber,
        SLPPhoneExtensionNumber,
        SLPPhoneUnitNumber,
        SLPPreferredContactMethodID,
        SLPShopManagerFlag,
        SPCellAreaCode,
        SPCellExchangeNumber,
        SPCellExtensionNumber,
        SPCellUnitNumber,
        SPEmailAddress,
        SPFaxAreaCode,
        SPFaxExchangeNumber,
        SPFaxExtensionNumber,
        SPFaxUnitNumber,
        SPGenderCD,
        SPName,
        SPPagerAreaCode,
        SPPagerExchangeNumber,
        SPPagerExtensionNumber,
        SPPagerUnitNumber,
        SPPersonnelID,
        SPPersonnelTypeID,
        SPPhoneAreaCode,
        SPPhoneExchangeNumber,
        SPPhoneExtensionNumber,
        SPPhoneUnitNumber,
        SPPreferredContactMethodID,
        SPShopManagerFlag,
        StripePaintPerPanel,
        StripePaintPerSide,
        StripeTapePerPanel,
        StripeTapePerSide,
        TireMountBalance,
        TowInChargeTypeCD,
        TowInFlatFee,
        Undercoat,
        WarrantyPeriodRefinishCD,
        WarrantyPeriodWorkmanshipCD,
        WebSiteAddress,
        SysLastUpdatedDate        
    )
    VALUES	
    (
        @Address1,
        @Address2,
        @AddressCity,
        @AddressCounty,
        @AddressState,
        @AddressZip,
        @AlignmentFourWheel,
        @AlignmentTwoWheel,
        @BusinessTypeCD,
        @CaulkingSeamSealer,
        @CertifiedFirstId,
        @ChipGuard,
        @CoolantGreen,
        @CoolantRed,
        @CorrosionProtection,
        @CountyTaxPct,
        @CoverCar,
        @DailyStorage,
        @DataEntryDate,
        @DiscountDomestic,
        @DiscountImport,
        @DiscountPctSideBackGlass,
        @DiscountPctWindshield,
        @DrivingDirections,
        @EmailAddress,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @FedTaxId,
        @FinalSubmitDate,
        @FinalSubmitFlag,
        @FlexAdditive,
        @GlassOutsourceServiceFee,
        @GlassReplacementChargeTypeCD,
        @GlassSubletServiceFee,
        @GlassSubletServicePct,
        @HazardousWaste,
        @HourlyRateMechanical,
        @HourlyRateRefinishing,
        @HourlyRateSheetMetal,
        @HourlyRateUnibodyFrame,
        @MaxWeeklyAssignments,
        @MunicipalTaxPct,
        @Name,
        @OEMDiscounts,
        @OperatingFridayEndTime,
        @OperatingFridayStartTime,
        @OperatingMondayEndTime,
        @OperatingMondayStartTime,
        @OperatingSaturdayEndTime,
        @OperatingSaturdayStartTime,
        @OperatingSundayEndTime,
        @OperatingSundayStartTime,
        @OperatingThursdayEndTime,
        @OperatingThursdayStartTime,
        @OperatingTuesdayEndTime,
        @OperatingTuesdayStartTime,
        @OperatingWednesdayEndTime,
        @OperatingWednesdayStartTime,
        @OtherTaxPct,
        @PartsRecycledMarkupPct,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @PreferredEstimatePackageID,
        @PullSetUp,
        @PullSetUpMeasure,
        @R12EvacuateRecharge,
        @R12EvacuateRechargeFreon,
        @R134EvacuateRecharge,
        @R134EvacuateRechargeFreon,
        @RefinishSingleStageHourly,
        @RefinishSingleStageMax,
        @RefinishThreeStageCCMaxHrs,
        @RefinishThreeStageHourly,
        @RefinishThreeStageMax,
        @RefinishTwoStageCCMaxHrs,
        @RefinishTwoStageHourly,
        @RefinishTwoStageMax,
        @RepairFacilityTypeCD,
        @SalesTaxPct,
        @BusinessInfoID,
        @ShopID,
        @SLPCellAreaCode,
        @SLPCellExchangeNumber,
        @SLPCellExtensionNumber,
        @SLPCellUnitNumber,
        @SLPEmailAddress,
        @SLPFaxAreaCode,
        @SLPFaxExchangeNumber,
        @SLPFaxExtensionNumber,
        @SLPFaxUnitNumber,
        @SLPGenderCD,
        @SLPName,
        @SLPPagerAreaCode,
        @SLPPagerExchangeNumber,
        @SLPPagerExtensionNumber,
        @SLPPagerUnitNumber,
        @SLPPersonnelID,
        @SLPPersonnelTypeID,
        @SLPPhoneAreaCode,
        @SLPPhoneExchangeNumber,
        @SLPPhoneExtensionNumber,
        @SLPPhoneUnitNumber,
        @SLPPreferredContactMethodID,
        @SLPShopManagerFlag,
        @SPCellAreaCode,
        @SPCellExchangeNumber,
        @SPCellExtensionNumber,
        @SPCellUnitNumber,
        @SPEmailAddress,
        @SPFaxAreaCode,
        @SPFaxExchangeNumber,
        @SPFaxExtensionNumber,
        @SPFaxUnitNumber,
        @SPGenderCD,
        @SPName,
        @SPPagerAreaCode,
        @SPPagerExchangeNumber,
        @SPPagerExtensionNumber,
        @SPPagerUnitNumber,
        @SPPersonnelID,
        @SPPersonnelTypeID,
        @SPPhoneAreaCode,
        @SPPhoneExchangeNumber,
        @SPPhoneExtensionNumber,
        @SPPhoneUnitNumber,
        @SPPreferredContactMethodID,
        @SPShopManagerFlag,
        @StripePaintPerPanel,
        @StripePaintPerSide,
        @StripeTapePerPanel,
        @StripeTapePerSide,
        @TireMountBalance,
        @TowInChargeTypeCD,
        @TowInFlatFee,
        @Undercoat,
        @WarrantyPeriodRefinishCD,
        @WarrantyPeriodWorkmanshipCD,
        @WebSiteAddress,
        @now        
    )
    
    END
    ELSE
    BEGIN
      UPDATE dbo.utb_shop_load
   	  SET
        Address1 = @Address1,
        Address2 = @Address2,
        AddressCity = @AddressCity,
        AddressCounty = @AddressCounty,
        AddressState = @AddressState,
        AddressZip = @AddressZip,
        AlignmentFourWheel = @AlignmentFourWheel,
        AlignmentTwoWheel = @AlignmentTwoWheel,
        BusinessTypeCD = @BusinessTypeCD,
        CaulkingSeamSealer = @CaulkingSeamSealer,
        CertifiedFirstId = @CertifiedFirstId,
        ChipGuard = @ChipGuard,
        CoolantGreen = @CoolantGreen,
        CoolantRed = @CoolantRed,
        CorrosionProtection = @CorrosionProtection,
        CountyTaxPct = @CountyTaxPct,
        CoverCar = @CoverCar,
        DailyStorage = @DailyStorage,
        DataEntryDate = @DataEntryDate,
        DiscountDomestic = @DiscountDomestic,
        DiscountImport = @DiscountImport,
        DiscountPctSideBackGlass = @DiscountPctSideBackGlass,
        DiscountPctWindshield = @DiscountPctWindshield,
        DrivingDirections = @DrivingDirections,
        EmailAddress = @EmailAddress,
        FaxAreaCode = @FaxAreaCode,
        FaxExchangeNumber = @FaxExchangeNumber,
        FaxExtensionNumber = @FaxExtensionNumber,
        FaxUnitNumber = @FaxUnitNumber,
        FedTaxId = @FedTaxId,
        FinalSubmitDate = @FinalSubmitDate,
        FinalSubmitFlag = @FinalSubmitFlag,
        FlexAdditive = @FlexAdditive,
        GlassOutsourceServiceFee = @GlassOutsourceServiceFee,
        GlassReplacementChargeTypeCD = @GlassReplacementChargeTypeCD,
        GlassSubletServiceFee = @GlassSubletServiceFee,
        GlassSubletServicePct = @GlassSubletServicePct,
        HazardousWaste = @HazardousWaste,
        HourlyRateMechanical = @HourlyRateMechanical,
        HourlyRateRefinishing = @HourlyRateRefinishing,
        HourlyRateSheetMetal = @HourlyRateSheetMetal,
        HourlyRateUnibodyFrame = @HourlyRateUnibodyFrame,
        MaxWeeklyAssignments = @MaxWeeklyAssignments,
        MunicipalTaxPct = @MunicipalTaxPct,
        Name = @Name,
        OEMDiscounts = @OEMDiscounts,
        OperatingFridayEndTime = @OperatingFridayEndTime,
        OperatingFridayStartTime = @OperatingFridayStartTime,
        OperatingMondayEndTime = @OperatingMondayEndTime,
        OperatingMondayStartTime = @OperatingMondayStartTime,
        OperatingSaturdayEndTime = @OperatingSaturdayEndTime,
        OperatingSaturdayStartTime = @OperatingSaturdayStartTime,
        OperatingSundayEndTime = @OperatingSundayEndTime,
        OperatingSundayStartTime = @OperatingSundayStartTime,
        OperatingThursdayEndTime = @OperatingThursdayEndTime,
        OperatingThursdayStartTime = @OperatingThursdayStartTime,
        OperatingTuesdayEndTime = @OperatingTuesdayEndTime,
        OperatingTuesdayStartTime = @OperatingTuesdayStartTime,
        OperatingWednesdayEndTime = @OperatingWednesdayEndTime,
        OperatingWednesdayStartTime = @OperatingWednesdayStartTime,
        OtherTaxPct = @OtherTaxPct,
        PartsRecycledMarkupPct = @PartsRecycledMarkupPct,
        PhoneAreaCode = @PhoneAreaCode,
        PhoneExchangeNumber = @PhoneExchangeNumber,
        PhoneExtensionNumber = @PhoneExtensionNumber,
        PhoneUnitNumber = @PhoneUnitNumber,
        PreferredEstimatePackageID = @PreferredEstimatePackageID,
        PullSetUp = @PullSetUp,
        PullSetUpMeasure = @PullSetUpMeasure,
        R12EvacuateRecharge = @R12EvacuateRecharge,
        R12EvacuateRechargeFreon = @R12EvacuateRechargeFreon,
        R134EvacuateRecharge = @R134EvacuateRecharge,
        R134EvacuateRechargeFreon = @R134EvacuateRechargeFreon,
        RefinishSingleStageHourly = @RefinishSingleStageHourly,
        RefinishSingleStageMax = @RefinishSingleStageMax,
        RefinishThreeStageCCMaxHrs = @RefinishThreeStageCCMaxHrs,
        RefinishThreeStageHourly = @RefinishThreeStageHourly,
        RefinishThreeStageMax = @RefinishThreeStageMax,
        RefinishTwoStageCCMaxHrs = @RefinishTwoStageCCMaxHrs,
        RefinishTwoStageHourly = @RefinishTwoStageHourly,
        RefinishTwoStageMax = @RefinishTwoStageMax,
        RepairFacilityTypeCD = @RepairFacilityTypeCD,
        SalesTaxPct = @SalesTaxPct,
        ShopID = @BusinessInfoID,
        ShopLocationID = @ShopID,
        SLPCellAreaCode = @SLPCellAreaCode,
        SLPCellExchangeNumber = @SLPCellExchangeNumber,
        SLPCellExtensionNumber = @SLPCellExtensionNumber,
        SLPCellUnitNumber = @SLPCellUnitNumber,
        SLPEmailAddress = @SLPEmailAddress,
        SLPFaxAreaCode = @SLPFaxAreaCode,
        SLPFaxExchangeNumber = @SLPFaxExchangeNumber,
        SLPFaxExtensionNumber = @SLPFaxExtensionNumber,
        SLPFaxUnitNumber = @SLPFaxUnitNumber,
        SLPGenderCD = @SLPGenderCD,
        SLPName = @SLPName,
        SLPPagerAreaCode = @SLPPagerAreaCode,
        SLPPagerExchangeNumber = @SLPPagerExchangeNumber,
        SLPPagerExtensionNumber = @SLPPagerExtensionNumber,
        SLPPagerUnitNumber = @SLPPagerUnitNumber,
        SLPPersonnelID = @SLPPersonnelID,
        SLPPersonnelTypeID = @SLPPersonnelTypeID,
        SLPPhoneAreaCode = @SLPPhoneAreaCode,
        SLPPhoneExchangeNumber = @SLPPhoneExchangeNumber,
        SLPPhoneExtensionNumber = @SLPPhoneExtensionNumber,
        SLPPhoneUnitNumber = @SLPPhoneUnitNumber,
        SLPPreferredContactMethodID = @SLPPreferredContactMethodID,
        SLPShopManagerFlag = @SLPShopManagerFlag,
        SPCellAreaCode = @SPCellAreaCode,
        SPCellExchangeNumber = @SPCellExchangeNumber,
        SPCellExtensionNumber = @SPCellExtensionNumber,
        SPCellUnitNumber = @SPCellUnitNumber,
        SPEmailAddress = @SPEmailAddress,
        SPFaxAreaCode = @SPFaxAreaCode,
        SPFaxExchangeNumber = @SPFaxExchangeNumber,
        SPFaxExtensionNumber = @SPFaxExtensionNumber,
        SPFaxUnitNumber = @SPFaxUnitNumber,
        SPGenderCD = @SPGenderCD,
        SPName = @SPName,
        SPPagerAreaCode = @SPPagerAreaCode,
        SPPagerExchangeNumber = @SPPagerExchangeNumber,
        SPPagerExtensionNumber = @SPPagerExtensionNumber,
        SPPagerUnitNumber = @SPPagerUnitNumber,
        SPPersonnelID = @SPPersonnelID,
        SPPersonnelTypeID = @SPPersonnelTypeID,
        SPPhoneAreaCode = @SPPhoneAreaCode,
        SPPhoneExchangeNumber = @SPPhoneExchangeNumber,
        SPPhoneExtensionNumber = @SPPhoneExtensionNumber,
        SPPhoneUnitNumber = @SPPhoneUnitNumber,
        SPPreferredContactMethodID = @SPPreferredContactMethodID,
        SPShopManagerFlag = @SPShopManagerFlag,
        StripePaintPerPanel = @StripePaintPerPanel,
        StripePaintPerSide = @StripePaintPerSide,
        StripeTapePerPanel = @StripeTapePerPanel,
        StripeTapePerSide = @StripeTapePerSide,
        TireMountBalance = @TireMountBalance,
        TowInChargeTypeCD = @TowInChargeTypeCD,
        TowInFlatFee = @TowInFlatFee,
        Undercoat = @Undercoat,
        WarrantyPeriodRefinishCD = @WarrantyPeriodRefinishCD,
        WarrantyPeriodWorkmanshipCD = @WarrantyPeriodWorkmanshipCD,
        WebSiteAddress = @WebSiteAddress,
        SysLastUpdatedDate = @now   
      WHERE ShopLoadID = @ShopLoadID     
    END
    
    
    
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    SET @ShopLoadID = SCOPE_IDENTITY()
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_load', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    COMMIT TRANSACTION AdmShopLocationInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopLoadID AS ShopLoadID

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopLoadInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO



