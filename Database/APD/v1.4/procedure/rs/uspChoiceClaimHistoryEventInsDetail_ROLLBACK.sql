-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceClaimHistoryEventInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceClaimHistoryEventInsDetail 
END

GO
