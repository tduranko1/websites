-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateOrphanGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateOrphanGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure

/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateOrphanGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Orphaned estimate records in partner data that did not get transferred to APD
*
* PARAMETERS:  
* (I) @PartnerDB    The partner data database name
* (I) @Minutes      The number of minutes from receipt after which the orphans shall be reported (avoids false positives)
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspEstimateOrphanGetList
(
    @PartnerDB    SYSNAME,
    @Minutes      INT = 15      -- Default 15 minutes
)
AS
BEGIN
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @now                            AS DATETIME

    DECLARE @ProcName                       AS VARCHAR(30)

    DECLARE @ExternalReferenceID   VARCHAR(20)
    DECLARE @Sequence              VARCHAR(30)
    DECLARE @cmd AS NVARCHAR(1000)
    DECLARE @StartDate as datetime

    SET @ProcName = 'uspEstimateOrphanGetList'  


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    SET @StartDate = dateadd(month, -3, convert(datetime, CONVERT(varchar, @now, 101))) -- 3 months worth to check


    -- This script will identify any orphaned estimate records in partner data that did not get transferred to APD

    CREATE TABLE #tmpID
    (
        TransID         varchar(20)     NOT NULL,
        Sequence        varchar(30)     NULL
    )


    DECLARE csrExtRef CURSOR FAST_FORWARD FOR
    SELECT DISTINCT d.ExternalReferenceDocumentID,
                    Convert(varchar(20), d.AssignmentID) + '-' + Convert(varchar(5), d.SupplementSeqNumber)
      FROM  dbo.utb_document d WITH (NOLOCK)
      WHERE d.ExternalReferenceDocumentID IS NOT NULL
        AND d.CreatedDate > @startDate -- restrict the number of records to check
        AND d.DocumentTypeID in (3, 10) -- Estimate and Print Images
        AND d.DocumentSourceID in (1, 2) -- ADP and CCC

    OPEN csrExtRef

    FETCH NEXT FROM csrExtRef 
      INTO  @ExternalReferenceID,
            @Sequence

    WHILE @@Fetch_Status = 0
    BEGIN
        INSERT INTO #tmpID 
        SELECT  CASE
                  WHEN CHARINDEX (':', value) > 0 THEN RIGHT(value, LEN(value) - CHARINDEX (':', value))
                  ELSE value
                END,
                @Sequence
          FROM  dbo.ufnUtilityParseString(@ExternalReferenceID, ',', 1)
    
        FETCH NEXT FROM csrExtRef
          INTO  @ExternalReferenceID,
                @Sequence
    END

    CLOSE csrExtRef
    DEALLOCATE csrExtRef

    SET @cmd = 'INSERT INTO #tmpID ' +
                 'SELECT  PartnerTransID, ' +
                         'Convert(varchar(20), AssignmentID) + ''-'' + Convert(varchar(5), SequenceNumber) ' +
                   'FROM ' + @partnerDB + '.dbo.utb_transaction_header WITH (NOLOCK) ' +
                   'WHERE TransactionType IN (''EstimatePI'', ''EstimateTransaction'') ' +
                     'AND Convert(varchar(20), AssignmentID) + ''-'' + Convert(varchar(5), SequenceNumber) IN (SELECT Sequence FROM #tmpID)'

    EXEC sp_executesql @stmt=@cmd

    SET @cmd   =' SELECT  PartnerTransID, ' +
                         'AssignmentID, ' +
                         'LynxID, ' +
                         'VehicleNumber, ' +
                         'SequenceNumber, ' +
                         'TransactionDate, ' +
                         'TransactionSource, ' +
                         'TransactionType, ' +
                         'SysCreatedDate ' +
                   'FROM ' + @partnerDB + '.dbo.utb_transaction_header WITH (NOLOCK) ' +
                   'WHERE SysCreatedDate > ''10/1/2009'' ' +
                     'AND DateDiff(minute, SysCreatedDate, ''' +  Convert(varchar(30), @now) + ''') > ' + Convert(varchar(5), @Minutes) + ' ' +      -- Only grab those that have been here at least 45 min...this eliminates false positives from transactions that straddle the check
                     'AND CharIndex( ''-'', Convert(varchar(20), AssignmentID)) = 0 ' +     -- Eliminates old-style Assignment IDs of the form (LynxID-VehicleNumber)
                     'AND TransactionType IN (''EstimatePI'', ''EstimateTransaction'') ' +
                     'AND Convert(varchar(20), PartnerTransID) NOT IN (SELECT TransID FROM #tmpID) ' +
                     'AND SysCreatedDate >= ''' + convert(varchar, @StartDate, 101) + ''' ' +
                   'ORDER BY LynxID, VehicleNumber, SequenceNumber'
    
         
    EXEC sp_executesql @stmt=@cmd

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting orphaned estimates', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateOrphanGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateOrphanGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
