-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClaimsReceivedLast7Days' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptClaimsReceivedLast7Days 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptClaimsReceivedLast7Days
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the claims recevied in the last 7 days
*
* PARAMETERS:  
* (I) @InsuranceCompanyID  The InsuranceCompanyID 
* (I) @ShopState           The Shop state
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptClaimsReceivedLast7Days
    @InsuranceCompanyID   udt_std_id_big,
    @ShopState            varchar(250) = NULL
AS
BEGIN

   SET NOCOUNT ON
   
   DECLARE @RptStart as datetime
   DECLARE @RptEnd as datetime
   DECLARE @InsuranceCompanyName as varchar(100)
   DECLARE @State as varchar(15)
   
   DECLARE @tmpStates TABLE (
      StateCode   varchar(2)
   )
   

   SET @RptEnd = dateadd(second, -1, convert(datetime, convert(varchar, current_timestamp, 101)))
   SET @RptStart = dateadd(day, -7, convert(datetime, convert(varchar, current_timestamp, 101)))
   --SET @ShopState = 'NY'
   --SET @InsuranceCompanyID = 259
   
   IF @ShopState IS NULL 
   BEGIN
      --SET @ShopState = '%'
      
      INSERT INTO @tmpStates
      SELECT StateCode
      FROM utb_state_code
      WHERE EnabledFlag = 1

      SET @State = 'ALL'
   END
   ELSE
   BEGIN
      INSERT INTO @tmpStates
      SELECT distinct sc.StateCode
      FROM dbo.ufnUtilityParseString(@ShopState, ',', 1) tmp
      LEFT JOIN utb_state_code sc ON tmp.value = sc.StateCode
      WHERE sc.StateValue is not null

      IF LEN(@ShopState) = 2
      BEGIN
         SET @State = @ShopState
      END
      ELSE
      BEGIN
         SET @State = 'Multi State'
      END
      
      --SET @State = @ShopState
   END
   
   
   SELECT @InsuranceCompanyName = Name
   FROM utb_insurance
   WHERE InsuranceCompanyID = @InsuranceCompanyID

   SELECT   @InsuranceCompanyName as 'InsuranceCompanyName',
            @State as 'ShopState',
            c.LynxID, 
            convert(varchar, c.IntakeFinishDate, 101) as 'ClaimReceivedDate', 
            c.ClientClaimNumber, 
            ca.ClaimAspectID,
            ca.ClaimAspectNumber, 
            case 
               when cv.DriveableFlag = 1 then 'Y'
               else 'N'
            end as 'DriveableFlag',
            isNull(cv.VehicleYear, '') as 'VehicleYear',
            isNull(cv.Make, '') as 'Make',
            isNull(cv.Model, '') as 'Model',
            sl.Name,
            sl.AddressCity,
            sl.AddressState,
            sl.AddressCounty,
            convert(varchar, casc.WorkStartDate, 101) as 'RepairDate',
            casc.WorkStartConfirmFlag,
            (select top 1 convert(varchar, d.CreatedDate, 101)
               from utb_document d
               left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
               left join utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
               where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
                 and d.DocumentTypeID in (3, 10)
                 and d.EnabledFlag = 1
               order by ds.VANFlag desc, d.SupplementSeqNumber asc) as 'OrigEstReceivedDate',
            (select top 1 es.OriginalExtendedAmt
               from utb_document d
               left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
               left join utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
               left join utb_estimate_summary es on d.DocumentID = es.DocumentID
               where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
                 and d.DocumentTypeID in (3, 10)
                 and d.EnabledFlag = 1
                 and es.EstimateSummaryTypeID = 27
               order by ds.VANFlag desc, d.SupplementSeqNumber asc) as 'OrigEstAmount',
             IsNull((SELECT TOP 1 i.NameLast + ', ' + i.NameFirst
                FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID
                                        INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                        INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                WHERE irt.Name = 'Owner'
                  AND cai.ClaimAspectID = ca.ClaimAspectID), '') as OwnerName
   FROM utb_claim c
   LEFT JOIN utb_claim_aspect ca on c.LynxID = ca.LynxID
   LEFT JOIN utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID
   LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
   LEFT JOIN utb_assignment a on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
   LEFT JOIN utb_shop_location sl on a.ShopLocationID = sl.ShopLocationID
   LEFT JOIN utb_claim_aspect_status cas on casc.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD and cas.StatusTypeCD = 'SC'
   LEFT JOIN utb_status s on cas.StatusID = s.StatusID
   WHERE c.IntakeFinishDate between @RptStart and @RptEnd
     AND c.InsuranceCompanyID = @InsuranceCompanyID
     AND ca.ClaimAspectTypeID = 9
     AND casc.ServiceChannelCD = 'PS'AND a.assignmentsequencenumber = 1
     AND a.CancellationDate is null
     AND a.ShopLocationID is not null
     AND sl.AddressState IN (SELECT StateCode FROM @tmpStates) --like @ShopState
     AND s.Name in ('Active', 'Complete')
   order by sl.AddressCounty


END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClaimsReceivedLast7Days' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptClaimsReceivedLast7Days TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
