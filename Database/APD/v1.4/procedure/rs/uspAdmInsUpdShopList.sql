-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsUpdShopList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsUpdShopList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsUpdShopList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will update the shop include/exclude list for an insurance company
*
* PARAMETERS:  
* (I) @InsuranceCompanyID  The insurance company id
* (O) @ShopIncludeList     A list of Shop location ids that are included for the insurance company
* (I) @ShopExcludeList     A list of Shop location ids that are excluded for the insurance company
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmInsUpdShopList
(
    @InsuranceCompanyID     udt_std_int_small,
    @ShopIncludeList        text,
    @ShopExcludeList        text,
    @SysUserID              udt_std_id
)
AS
BEGIN
    SET NOCOUNT ON

    -- Declare internal variables

    DECLARE @error              AS INT

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                AS datetime

    DECLARE @Debug             as bit
    
    DECLARE @Duplicates         AS varchar(500)
    
    SET @Debug = 1

    SET @ProcName = 'uspAdmInsUpdShopList'
    SET @now = CURRENT_TIMESTAMP
    
    IF @Debug = 1
    BEGIN
        Print 'Input parameters:'
        Print ''
        Print '@InsuranceCompanyID = ' + convert(varchar, @InsuranceCompanyID)

    END
    
    IF NOT EXISTS (SELECT * 
                    FROM dbo.utb_insurance 
                    WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        RAISERROR  ('1|Invalid Insurance Company. Insurance company could not be located in the APD system. ', 16, 1, @ProcName)
        RETURN
    END
    
    DECLARE @tmpExcludeShops TABLE
    (
        shopLocationID      int
    )

    DECLARE @tmpIncludeShops TABLE
    (
        shopLocationID      int
    )

    DECLARE @tmpOverlapShops TABLE
    (
        shopLocationName    varchar(100)
    )
    
    INSERT INTO @tmpExcludeShops
    SELECT value 
    FROM dbo.ufnUtilityParseString( @ShopExcludeList, ',', 1 ) -- 1=trim spaces


    INSERT INTO @tmpIncludeShops
    SELECT value 
    FROM dbo.ufnUtilityParseString( @ShopIncludeList, ',', 1 ) -- 1=trim spaces
    
    -- Validate the Include/Exclude list
    
    -- Shops included must not be in the excluded list
    --INSERT INTO @tmpOverlapShops
    
    SET @Duplicates = ''
    
    SELECT TOP 5 @Duplicates = @Duplicates + sl.Name
    FROM @tmpExcludeShops tmp
    LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
    WHERE tmp.shopLocationID in (SELECT shopLocationID FROM @tmpIncludeShops)
    
    IF LEN(@Duplicates) > 0
    BEGIN
        RAISERROR  ('1|Following shops exist in both the Include and Exclude List. %s. Please remove the reference in one of the list and try saving again.', 16, 1, @Duplicates)
        RETURN
    END
    
    -- Shops excluded must not be in the included list

    SET @Duplicates = ''
    
    SELECT TOP 5 @Duplicates = @Duplicates + sl.Name
    FROM @tmpIncludeShops tmp
    LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
    WHERE tmp.shopLocationID in (SELECT shopLocationID FROM @tmpExcludeShops)
    
    IF LEN(@Duplicates) > 0
    BEGIN
        RAISERROR  ('1|Following shops exist in both the Include and Exclude List. %s. Please remove the reference in one of the list and try saving again.', 16, 1, @Duplicates)
        RETURN
    END
    
    -- Data is fine now. Go ahead and save the data.
    BEGIN TRANSACTION
    
    -- delete the old client shop data
    DELETE FROM dbo.utb_client_shop_location
    WHERE InsuranceCompanyID = @InsuranceCompanyID

    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('106|%s|utb_client_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    -- insert the Excluded shops
    INSERT INTO dbo.utb_client_shop_location
    SELECT @InsuranceCompanyID, shopLocationID, 1, 0, @SysUserID, @now
    FROM @tmpExcludeShops
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_client_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- insert the Included shops
    INSERT INTO dbo.utb_client_shop_location
    SELECT @InsuranceCompanyID, shopLocationID, 0, 1, @SysUserID, @now
    FROM @tmpIncludeShops

    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_client_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsUpdShopList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsUpdShopList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/