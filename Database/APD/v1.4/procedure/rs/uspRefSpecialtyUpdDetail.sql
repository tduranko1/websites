-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefSpecialtyUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefSpecialtyUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a specific Specialty
*
* PARAMETERS:  
* (I) @SpecialtyID              Specialty unique identity
* (I) @DisplayOrder             Specialty display order
* (I) @EnabledFlag              Specialty active/inactive flag
* (I) @Name                     Specialty name
* (I) @SysLastUserID            Specialty user performing action
* (I) @SysLastUpdatedDate       Specialty last update date as string (VARCHAR(30))
*
* RESULT SET:
* SpecialtyID     The updated Specialty unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefSpecialtyUpdDetail
(
	@SpecialtyID              udt_std_int_tiny,
	@DisplayOrder             udt_std_int_tiny,
	@EnabledFlag              udt_std_flag,
	@Name                     udt_std_name,
	@SysLastUserID            udt_std_id,
  @SysLastUpdatedDate       VARCHAR(30),
  @ApplicationCD            udt_std_cd = 'APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 

    DECLARE @SysMaintainedFlag AS udt_sys_maintained_flag

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefSpecialtyUpdDetail'

    
    -- Validate the updated date parameter
    
    IF @SysLastUpdatedDate IS NULL OR 
       ISDATE(@SysLastUpdatedDate) = 0
    BEGIN
        -- Invalid updated date value
    
        RAISERROR  ('%s: Invalid Last Updated Date parameter', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    
    -- Validate the updated date
    
    IF @tupdated_date IS NULL
    BEGIN 
        -- Invalid updated date value
    
        RAISERROR  ('%s: Invalid Last Updated Date value', 16, 1, @ProcName)
        RETURN
    END 
    
    
    -- Check whether the Specialty exists in the table
    
    SELECT @temp_updated_date = SysLastUpdatedDate, @SysMaintainedFlag = SysMaintainedFlag
      FROM dbo.utb_specialty 
     WHERE SpecialtyID = @SpecialtyID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @temp_updated_date IS NULL
    BEGIN
        RAISERROR  ('1|Specialty record not found', 16, 1, @ProcName)
        RETURN
    END 
    
    IF @temp_updated_date <> @tupdated_date
    BEGIN
        RAISERROR  ('1|Database record has been modified', 16, 1, @ProcName)
        RETURN
    END 

    IF @SysMaintainedFlag = 1
    BEGIN
        RAISERROR  ('1|Database record may not be modified', 16, 1, @ProcName)
        RETURN
    END 

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
                         AND ua.AccessEndDate IS NULL
                         AND a.Code = @ApplicationCD)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END


    BEGIN TRANSACTION RefSpecialtyUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    UPDATE dbo.utb_specialty
       SET 
		DisplayOrder              = @DisplayOrder,
		EnabledFlag               = @EnabledFlag,
		Name                      = @Name,
		SysLastUserID             = @SysLastUserID,
		SysLastUpdatedDate        = CURRENT_TIMESTAMP
     WHERE 
		SpecialtyID = @SpecialtyID AND
		SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('%s: Cannot update utb_specialty', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION RefSpecialtyUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    SELECT @SpecialtyID AS SpecialtyID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefSpecialtyUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/