-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectServiceChannelCoverageDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimAspectServiceChannelCoverageDel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimAspectServiceChannelCoverageDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Deletes a row from utb_Claim_Aspect_Service_Channel_Coverage table using the parameter values provided
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID
* (I) @ClaimCoverageID..............The id for the row that must be modified
* (I) @DeductibleAppliedAmt.........The deductible amount applied for the coverage
* (I) @UserID.......................The user updating the note
*
* RESULT SET:
* 
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE dbo.uspClaimAspectServiceChannelCoverageDel	
   @ClaimAspectServiceChannelID    udt_std_int_big,
   @ClaimCoverageID                udt_std_int_big,
   @DocumentID                     udt_std_id_big = null,
   @UserID                         udt_std_id

AS

BEGIN

    SET nocount on
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspClaimAspectServiceChannelCoverageDel'

   -- If the parameter set, that was passed by the caller was invalid raise an error
   IF  ( SELECT     count(*) 
         FROM       utb_Claim_Aspect_Service_Channel_Coverage 
         WHERE      ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
         AND        ClaimCoverageID = @ClaimCoverageID
       ) = 0
   -- then raise error and return/exit
   BEGIN
      -- Invalid Lynx ID
      RAISERROR('101|%s|No records available for the parameters provided:@ClaimAspectServiceChannelID, @ClaimCoverageID|%u|%u', 16, 1, @ProcName,@ClaimAspectServiceChannelID,@ClaimCoverageID)
      RETURN
   END


    BEGIN TRANSACTION
    
    DELETE    from utb_Claim_Aspect_Service_Channel_Coverage
    where     ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    AND       ClaimCoverageID = @ClaimCoverageID

    if @@error <> 0
        BEGIN
            -- Raise an error
            Raiserror('103|%s|utb_Claim_Aspect_Service_Channel_Coverage: Record not deleted',16,1,@ProcName)
            ROLLBACK TRANSACTION
            Return
        END

    IF (@DocumentID IS NOT NULL) AND
       EXISTS(SELECT DocumentID
               FROM dbo.utb_estimate_summary
               WHERE DocumentID = @DocumentID)
    BEGIN
      EXEC uspRefreshEstimateDedLimitEffects @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                             @ClaimCoverageID = @ClaimCoverageID,
                                             @DocumentID = @DocumentID,
                                             @UserID = @UserID


       IF @@ERROR <> 0
       BEGIN
          -- SQL Server Error
       
           RAISERROR  ('99|%s', 16, 1, @ProcName)
           ROLLBACK TRANSACTION
           RETURN
       END    
   
    END
    
    COMMIT
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectServiceChannelCoverageDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimAspectServiceChannelCoverageDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO




