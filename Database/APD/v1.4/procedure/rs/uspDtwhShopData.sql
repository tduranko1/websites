-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION

-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhShopData 
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhShopData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Mark A. Homison
* FUNCTION:     Extracts Shop data from APD and populates the Data Warehouse.
*
* PARAMETERS:   None
*
* RESULT SET:   None
*
* SWR 201988 09-25-12 MAH - Initial Version.
*
************************************************************************************************************************/

CREATE PROCEDURE dbo.uspDtwhShopData	
AS
BEGIN
    Declare @debug              AS bit
    DECLARE @error              AS int
    DECLARE @rowcount           AS int
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SELECT @ProcName = 'uspDtwhShopData'

    SELECT @debug = 0
    
    -- Set Database options
    
    SET NOCOUNT ON

    -- Get current timestamp

    PRINT 'Deleting from utb_dtwh_dim_shop...'
    
    DELETE FROM dbo.utb_dtwh_dim_shop
    
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_shop', 16, 1, @ProcName)
        RETURN
    END

	-- -----------------------------------------------------------------------------------------
	-- Insert Shop records where a Shop Location exists and it meets the following requirements:
	-- 1) Shop Location is Enabled and one or more of the following are true
	-- 2) LYNX Select Program Member
	-- 3) CEI Program Member
	-- 4) RRP Program Member
	-- 5) Shop is Targeted for a current Campaign (CertifiedFirstID was repurposed for this - YUCK!)
	-- -----------------------------------------------------------------------------------------
 	
	INSERT INTO dbo.utb_dtwh_dim_shop
		   (ShopID,
			ShopParentID,
			Address1,
			Address2,
			AddressCity,
			AddressCounty,
			AddressState,
			AddressZip,
			BusinessOwner1EmailAddr,
			BusinessOwner1Name,
			BusinessOwner1PhoneNumber,
			BusinessOwner1PhoneExtNumber,
			BusinessOwner2EmailAddr,
			BusinessOwner2Name,
			BusinessOwner2PhoneNumber,
			BusinessOwner2PhoneExtNumber,
			BusinessTypeCD,
			BusinessTypeDesc,
			FaxNumber,
			FaxNumberExtension,
			FedTaxId,
			LastUserID,
			LastUpdatedByName,
			LastUpdatedDate,
			PhoneNumber,
			PhoneNumberExtension,
			ShopName)
	SELECT  s.ShopID,
			s.ShopParentID,
			s.Address1,
			s.Address2,
			s.AddressCity,
			s.AddressCounty,
			s.AddressState,
			s.AddressZip,
			NULL, -- BusinessOwner1EmailAddr,
			NULL, -- BusinessOwner1Name,
			NULL, -- BusinessOwner1PhoneNumber,
			NULL, -- BusinessOwner1PhoneExtNumber,
			NULL, -- BusinessOwner2EmailAddr,
			NULL, -- BusinessOwner2Name,
			NULL, -- BusinessOwner2PhoneNumber,
			NULL, -- BusinessOwner2PhoneExtNumber,
			s.BusinessTypeCD,
			ISNULL(
			(SELECT  [Name] 
			   FROM  dbo.ufnUtilityGetReferenceCodes('utb_shop', 'BusinessTypeCD') 
			  WHERE  Code = s.BusinessTypeCD),'UNKNOWN')	AS BusinessTypeDesc,
			s.FaxAreaCode + ' ' + 
			s.FaxExchangeNumber + ' ' + 
			s.FaxUnitNumber, -- FaxNumber,
			s.FaxExtensionNumber,
			s.FedTaxId,
			s.SysLastUserID,
			ISNULL(
		    (SELECT CASE WHEN (u.UserID = 0)
						 THEN NameLast
						 ELSE NameLast+', '+NameFirst
				    END
			   FROM utb_user u
			  WHERE u.UserID = s.SysLastUserID),'N/A') AS LastUpdatedByName,
			s.SysLastUpdatedDate,
			s.PhoneAreaCode + ' ' + 
			s.PhoneExchangeNumber + ' ' + 
			s.PhoneUnitNumber, -- PhoneNumber,
			s.PhoneExtensionNumber,
			s.Name
	  FROM  dbo.utb_shop s
     WHERE  EXISTS
           (SELECT  'x'
			  FROM  dbo.utb_shop_location l
			 WHERE  l.ShopID           = s.ShopID
               AND  l.EnabledFlag      = 1
			   AND (l.ProgramFlag      = 1 
			    OR  l.CEIProgramFlag   = 1 
			    OR  l.ReferralFlag     = 1
			    OR  l.CertifiedFirstId IS NOT NULL))

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error Inserting into utb_dtwh_dim_shop', 16, 1, @ProcName)
        RETURN
    END

    PRINT 'Inserted ' + CONVERT(varchar(10), @rowcount) + ' records into utb_dtwh_dim_shop...'
    
	-- -----------------------------------------------------------------------------------------

	UPDATE  dbo.utb_dtwh_dim_shop
	   SET  BusinessOwner1EmailAddr		 = pe.EmailAddress,
			BusinessOwner1Name			 = pe.Name,
			BusinessOwner1PhoneNumber	 = pe.PhoneAreaCode + ' ' + 
										   pe.PhoneExchangeNumber + ' ' + 
										   pe.PhoneUnitNumber,
			BusinessOwner1PhoneExtNumber = pe.PhoneExtensionNumber
	  FROM  dbo.utb_personnel      pe,
			dbo.utb_shop_personnel lp
     WHERE  dbo.utb_dtwh_dim_shop.ShopID = lp.ShopID
       AND  lp.PersonnelID               = pe.PersonnelID
	   AND  lp.PersonnelID               =
		   (SELECT  TOP 1 x.PersonnelID
			  FROM  dbo.utb_personnel x,
				    dbo.utb_shop_personnel y 
			 WHERE  dbo.utb_dtwh_dim_shop.ShopID = y.ShopID
               AND  x.PersonnelID                = y.PersonnelID
			   AND  x.PersonnelTypeID            = 5)		-- Owner/Office 1st

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error updating utb_dtwh_dim_shop w/ Business Owner 1', 16, 1, @ProcName)
        RETURN
    END

    PRINT 'Updated ' + CONVERT(varchar(10), @rowcount) + ' utb_dtwh_dim_shop records w/ Owner 1.'
    
	-- -----------------------------------------------------------------------------------------

	UPDATE  dbo.utb_dtwh_dim_shop
	   SET  BusinessOwner2EmailAddr		 = pe.EmailAddress,
			BusinessOwner2Name			 = pe.Name,
			BusinessOwner2PhoneNumber	 = pe.PhoneAreaCode + ' ' + 
										   pe.PhoneExchangeNumber + ' ' + 
										   pe.PhoneUnitNumber,
			BusinessOwner2PhoneExtNumber = pe.PhoneExtensionNumber
	  FROM  dbo.utb_personnel      pe,
			dbo.utb_shop_personnel lp
     WHERE  dbo.utb_dtwh_dim_shop.ShopID = lp.ShopID
       AND  lp.PersonnelID               = pe.PersonnelID
	   AND  lp.PersonnelID               =
		   (SELECT  TOP 1 x.PersonnelID
			  FROM  dbo.utb_personnel x,
				    dbo.utb_shop_personnel y 
			 WHERE  dbo.utb_dtwh_dim_shop.ShopID = y.ShopID
			   AND  x.PersonnelID                = y.PersonnelID
			   AND  x.PersonnelTypeID            = 6)	-- Owner/Office 2nd

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error updating utb_dtwh_dim_shop w/ Business Owner 2', 16, 1, @ProcName)
        RETURN
    END

	PRINT 'Updated ' + CONVERT(varchar(10), @rowcount) + ' utb_dtwh_dim_shop records w/ Owner 2.'
    
	-- -----------------------------------------------------------------------------------------

	PRINT 'Shop Data Warehouse Extract complete...'

END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhShopData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
