
BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptAPDKeyIndicator' AND type = 'P')
BEGIN
    DROP PROCEDURE uspRptAPDKeyIndicator
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptAPDKeyIndicator
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles information to produce a key indicator report for APD
*
* PARAMETERS:   None
*
* RESULT SET:   Key Indicator information
* 1)  New Claims/Vehicles (LynxID-Vehicle, Carrier, Service Channel)
* 2)  Closed claims by (LynxID-Vehicle, Carrier, Service Channel, LynxRep, Cycle Time)
* 3)  Cancelled/Voided Claims  (LynxID-Vehicle, Carrier, Service Channel, LynxRep, Cycle Time)
* 4)  Aging Claims (LynxID-Vehicle, Carrier, Service Channel, LynxRep, Time Open)
* 5)  Excessively Delayed Tasks (LynxID-Vehicle, LynxRep, Task, Task Assignee, Number of Delays, Total Time Delayed)
* 6)  Desk Audit Supplements Received (LynxID-Vehicle, Carrier, LynxRep, Audit Performed)
* 7)  Assignment Type/Service Channel Changes (LynxID-Vehicle, Old Assignment Type/Service Channel, New Assignment Type/Service Channel)
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptAPDKeyIndicator
    @ReportDate AS udt_std_datetime = NULL
AS
BEGIN
    -- Set Database Options

    SET NOCOUNT ON


    DECLARE @tmpNewClaims TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        CreateDate              datetime,
        LynxRep                 varchar(50)
    )

    DECLARE @tmpClosedClaims TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        CompletedDate           datetime,
        LynxRep                 varchar(50),
        ClosedRep               varchar(50),
        CycleTime               int
    )

    DECLARE @tmpCancelledVoidedClaims TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        CancelVoidDate          datetime,
        LynxRep                 varchar(50),
        CycleTime               int
    )

    DECLARE @tmpAgingClaims TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        CreateDate              datetime,
        EstimateReceiptDate     datetime,
        LynxRep                 varchar(50),
        TimeOpen                int             NULL
    )

    DECLARE @tmpAgingClaimsSort TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        CreateDate              datetime,
        EstimateReceiptDate     datetime,
        LynxRep                 varchar(50),
        TimeOpen                int
    )

    DECLARE @tmpExcessiveTaskDelays TABLE
    (
        ChecklistID             bigint,
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        ServiceChannelName      varchar(20),
        LynxRep                 varchar(50),
        TaskName                varchar(50),
        TaskAssignedTo          varchar(50),
        TaskAlarmDate           datetime,
        TaskOriginalAlarmDate   datetime,
        TaskDelayCount          int
    )

    DECLARE @tmpDASupplementsReceived TABLE
    (
        LynxID                  varchar(12),
        SequenceNumber          int,
        InsuranceCompanyName    varchar(50),
        LynxRep                 varchar(50),
        ReceivedDate            datetime,
        ProcessedFlag           bit
    )

    DECLARE @tmpAssignmentTypeSwitched TABLE
    (
        LynxID                  varchar(12),
        InsuranceCompanyName    varchar(50),
        OldAssignmentType       varchar(30),
        NewAssignmentType       varchar(30),
        CreateDate              datetime,
        LynxRep                 varchar(50)
    )

    DECLARE @ClaimAspectTypeIDVehicle           udt_std_id
    DECLARE @EstimateSummaryTypeIDGrossTotal    udt_std_id
    DECLARE @EstimateSummaryTypeIDNetTotal      udt_std_id

    DECLARE @LBoundDate                         udt_std_datetime
    DECLARE @UBoundDate                         udt_std_datetime
    DECLARE @VLBoundDate                        udt_std_datetime


    -- If @ReportDate was not passed in, use yesterday (CURRENT_TIMESTAMP - 1 day)

    IF @ReportDate IS NULL
    BEGIN
        SET @ReportDate = DateDiff(d, -1, CURRENT_TIMESTAMP)
    END


    -- Validate APD Data State

    SELECT  @EstimateSummaryTypeIDGrossTotal = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'TT' AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    IF @EstimateSummaryTypeIDGrossTotal IS NULL
    BEGIN
        RAISERROR('Invalid APD Data State: EstimateSummaryTypeID not found for "RepairTotal"', 16, 1)
        RETURN
    END

    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'TT' AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
        RAISERROR('Invalid APD Data State: EstimateSummaryTypeID not found for "NetTotal"', 16, 1)
        RETURN
    END

    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        RAISERROR('Invalid APD Data State: ClaimAspectTypeID not found for "Vehicle"', 16, 1)
        RETURN
    END

    -- Set LBound and UBound

    SET @VLBoundDate = DateAdd(d, -5, Convert(datetime, Convert(varchar(12), @ReportDate, 101) + ' 0:00:00'))
    SET @LBoundDate = Convert(datetime, Convert(varchar(12), @ReportDate, 101) + ' 0:00:00')
    SET @UBoundDate = Convert(datetime, Convert(varchar(12), @ReportDate, 101) + ' 23:59:59')

    -- This procedure encompasses a disparate set of queries designed to provide APD operations with a snapshot
    -- of key activity from the previous day.

    SELECT 'APD Key Indicator Report for ' + DateName(dw, @ReportDate) + ', ' + Convert(varchar(20), @ReportDate, 107)  -- Ex.  Jan 12, 2004
    SELECT ''

    -- New Claims

    INSERT INTO @tmpNewClaims
      SELECT  Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
              i.Name,
              (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                WHERE Code = casc.ServiceChannelCD),
              casc.CreatedDate,
            CASE
              WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
              WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
              ELSE 'Unknown'
            END
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
          AND ca.EnabledFlag = 1
          AND c.DemoFlag = 0
          AND ca.CreatedDate BETWEEN @LBoundDate AND @UBoundDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT  'New Claims Received: ' + Convert(varchar(10), Count(*))
      FROM  @tmpNewClaims

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT  ServiceChannelName AS 'Grouped By Service Channel',
            Count(*) AS Count
      FROM  @tmpNewClaims
      GROUP BY ServiceChannelName
      ORDER BY ServiceChannelName

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT  InsuranceCompanyName AS 'Grouped By Carrier',
            ServiceChannelName AS 'Service Channel',
            Count(*) AS Count
      FROM  @tmpNewClaims
      GROUP BY InsuranceCompanyName, ServiceChannelName
      ORDER BY InsuranceCompanyName, ServiceChannelName

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END
    
    SELECT ''
    SELECT * 
    FROM @tmpNewClaims

    -- Closed Claims

    SELECT ''
    SELECT ''

    DECLARE @tmpLynxID                       int
    DECLARE @tmpClaimAspectServiceChannelID  int
    DECLARE @tmpLynxAspect                   varchar(12)
    DECLARE @tmpInsuranceCompanyName         varchar(50)
    DECLARE @tmpServiceChannelName           varchar(20)
    DECLARE @tmpCompletedDate                datetime
    DECLARE @tmpLynxRep                      varchar(50)
    DECLARE @tmpClosedRep                    varchar(50)
    DECLARE @tmpCycleTime                    int

    DECLARE csrClosedClaims CURSOR FOR
      SELECT ca.LynxID,
        casc.ClaimAspectServiceChannelID,
        Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
        i.Name,
        (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
          WHERE Code = casc.ServiceChannelCD),
        casc.OriginalCompleteDate,
        CASE
          WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
          WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
          ELSE 'Unknown'
        END,
        CASE
          WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN DateDiff(hh, casc.CreatedDate, casc.OriginalCompleteDate)
          ELSE DateDiff(d, casc.CreatedDate, casc.OriginalCompleteDate)
        END
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user ou ON (ca.CompletedOwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.CompletedAnalystUserID = au.UserID)
        WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
          AND ca.EnabledFlag = 1
          AND c.DemoFlag = 0
          AND casc.OriginalCompleteDate BETWEEN @LBoundDate AND @UBoundDate

    OPEN csrClosedClaims

    FETCH NEXT FROM csrClosedClaims
      INTO @tmpLynxID, @tmpClaimAspectServiceChannelID, @tmpLynxAspect, @tmpInsuranceCompanyName,
        @tmpServiceChannelName, @tmpCompletedDate, @tmpLynxRep, @tmpCycleTime

    WHILE @@Fetch_Status = 0
    BEGIN

        SELECT @tmpClosedRep =
            CASE
              WHEN ( u.UserID IS NOT NULL ) THEN u.nameLast + ', ' + u.NameFirst
              ELSE 'Unknown'
            END
        FROM utb_history_log hist
            INNER JOIN dbo.utb_user u ON ( u.UserID = hist.CompletedUserID )
            WHERE (
                ( @tmpClaimAspectServiceChannelID = hist.ClaimAspectServiceChannelID ) AND
                ( hist.EventID = '28' ) -- Vehicle Closed
            ) OR (
                ( @tmpLynxID = hist.LynxID ) AND
                ( hist.EventID = '5' ) -- Claim Closed
            )
        ORDER BY hist.CompletedDate DESC

        INSERT INTO @tmpClosedClaims
        SELECT
            @tmpLynxAspect, @tmpInsuranceCompanyName, @tmpServiceChannelName, @tmpCompletedDate,
            @tmpLynxRep, @tmpClosedRep, @tmpCycleTime

        FETCH NEXT FROM csrClosedClaims
          INTO @tmpLynxID, @tmpClaimAspectServiceChannelID, @tmpLynxAspect, @tmpInsuranceCompanyName,
            @tmpServiceChannelName, @tmpCompletedDate, @tmpLynxRep, @tmpCycleTime
    END

    CLOSE csrClosedClaims
    DEALLOCATE csrClosedClaims

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT  'Claims Closed: ' + Convert(varchar(10), Count(*))
      FROM  @tmpClosedClaims

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT  DISTINCT LynxRep AS 'Claims Closed Per Lynx Rep',
            Count(*) AS Count
      FROM  @tmpClosedClaims
      GROUP BY LynxRep
      ORDER BY LynxRep

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT  ServiceChannelName AS 'Grouped By Service Channel',
            LynxRep AS 'Lynx Rep',
            Count(*) AS Count
      FROM  @tmpClosedClaims
      GROUP BY ServiceChannelName, LynxRep
      ORDER BY ServiceChannelName, LynxRep

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    PRINT 'Detail:'
    SELECT  LynxID,
            InsuranceCompanyName AS Carrier,
            ServiceChannelName AS 'Service Channel',
            Convert(varchar(10), CompletedDate, 108) AS 'Closed',
            LynxRep AS 'Assigned To',
            ClosedRep AS 'Closed By',
            CASE
              WHEN ServiceChannelName IN ('Desk Audit', 'Damage Reinspection') THEN Convert(varchar(5), CycleTime) + ' hours'
              ELSE Convert(varchar(5), CycleTime) + ' days'
            END AS 'Cycle Time'
      FROM  @tmpClosedClaims
      ORDER BY LynxRep, CompletedDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    -- Cancelled/Voided Claims

    SELECT ''
    SELECT ''

    INSERT INTO @tmpCancelledVoidedClaims
      SELECT  Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
              i.Name,
              (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                WHERE Code = casc.ServiceChannelCD),
              cas.StatusStartDate,
            CASE
              WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
              WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
              ELSE 'Unknown'
            END,
              CASE
                WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN DateDiff(hh, ca.CreatedDate, cas.StatusStartDate)
                ELSE DateDiff(d, ca.CreatedDate, cas.StatusStartDate)
              END
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim_aspect_status cas ON (cas.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
          AND ca.EnabledFlag = 1
          AND c.DemoFlag = 0
          AND cas.StatusID in (994, 995)
          AND cas.StatusStartDate BETWEEN @LBoundDate AND @UBoundDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT  'Claims Cancelled or Voided: ' + Convert(varchar(10), Count(*))
      FROM  @tmpCancelledVoidedClaims

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT  InsuranceCompanyName AS 'Grouped By Carrier',
            ServiceChannelName AS 'Service Channel',
            Count(*) AS Count
      FROM  @tmpCancelledVoidedClaims
      GROUP BY InsuranceCompanyName, ServiceChannelName
      ORDER BY InsuranceCompanyName, ServiceChannelName

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    PRINT 'Detail:'
    SELECT  LynxID,
            InsuranceCompanyName AS Carrier,
            ServiceChannelName AS 'Service Channel',
            Convert(varchar(10), CancelVoidDate, 108) AS 'Cancelled/Voided',
            LynxRep AS 'Assigned To',
            CASE
              WHEN ServiceChannelName IN ('Desk Audit', 'Damage Reinspection') THEN Convert(varchar(5), CycleTime) + ' hours'
              ELSE Convert(varchar(5), CycleTime) + ' days'
            END AS 'Time Until Cancel/Void'
      FROM  @tmpCancelledVoidedClaims
      ORDER BY InsuranceCompanyName, CancelVoidDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END


    --Aging Claims

    INSERT INTO @tmpAgingClaims
      SELECT  Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
              i.Name,
              (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                WHERE Code = casc.ServiceChannelCD),
              ca.CreatedDate,
              (SELECT  TOP 1 d.CreatedDate
                 FROM  dbo.utb_claim_aspect_service_channel_document cad
                 LEFT JOIN dbo.utb_document d ON (cad.DocumentID = d.DocumentID)
                 LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
                 WHERE cad.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                   AND dt.EstimateTypeFlag = 1
                   AND d.EnabledFlag = 1
                   AND d.DuplicateFlag = 0
                 ORDER BY d.CreatedDate),
                CASE
                  WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
                  WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
                  ELSE 'Unknown'
                END,
              NULL
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        LEFT JOIN dbo.utb_claim_aspect_status cas ON (cas.ClaimAspectID = ca.ClaimAspectID)
        WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
          AND ca.EnabledFlag = 1
          AND c.DemoFlag = 0
          --AND cas.StatusID NOT IN (@StatusIDCancelled, @StatusIDClosed, @StatusIDVoided)
          AND cas.StatusID IN (SELECT StatusID FROM dbo.utb_status WHERE Name IN ('Active'))
          AND cas.StatusTypeCD = 'SC'
          AND casc.OriginalCompleteDate IS NULL     -- Claim was never closed

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    UPDATE @tmpAgingClaims
       SET TimeOpen = CASE
                        WHEN ServiceChannelName IN ('Desk Audit', 'Damage Reinspection') THEN DateDiff(hh, EstimateReceiptDate, CURRENT_TIMESTAMP)
                        ELSE DateDiff(d, CreateDate, CURRENT_TIMESTAMP)
                      END

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    INSERT INTO @tmpAgingClaimsSort
      SELECT   *
      FROM     @tmpAgingClaims
      ORDER BY ServiceChannelName,
               TimeOpen DESC

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END


    SELECT ''
    SELECT ''
    SELECT 'Aged Open Claims Grouped By Service Channel:'
    SELECT ''

    SELECT  ServiceChannelName AS 'Service Channel',
            LynxID,
            InsuranceCompanyName AS Carrier,
            LynxRep AS 'Assigned To',
            Convert(varchar(12), CreateDate, 101) AS 'Received',
            Convert(varchar(5), TimeOpen) + ' hours' AS 'Time Open'
      FROM  @tmpAgingClaimsSort
      WHERE ServiceChannelName IN ('Desk Audit', 'Damage Reinspection')
        AND TimeOpen > 120

    UNION ALL

    SELECT  TOP 15 ServiceChannelName,
            LynxID,
            InsuranceCompanyName,
            LynxRep,
            Convert(varchar(12), CreateDate, 101),
            Convert(varchar(5), TimeOpen) + ' days'
      FROM  @tmpAgingClaimsSort
      WHERE ServiceChannelName = 'Program Shop'
        AND TimeOpen > 90

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END


    -- Excessively Delayed Tasks

    INSERT INTO @tmpExcessiveTaskDelays
      SELECT che.ChecklistID,
             Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
             i.Name,
             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                WHERE Code = casc.ServiceChannelCD),
            CASE
              WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
              WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
              ELSE 'Unknown'
            END,
             t.Name,
             tu.nameLast + ', ' + tu.NameFirst,
             che.AlarmDate,
             (SELECT Min(chi.AlarmDateOld) FROM dbo.utb_checklist_history chi WHERE chi.ChecklistID = che.ChecklistID),
             (SELECT Count(*) FROM dbo.utb_checklist_history chi WHERE chi.ChecklistID = che.ChecklistID)
        FROM dbo.utb_checklist che
        LEFT JOIN dbo.utb_user tu ON (che.AssignedUserID = tu.UserID)
        LEFT JOIN dbo.utb_task t ON (che.TaskID = t.TaskID)
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectServiceChannelID = che.ClaimAspectServiceChannelID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        WHERE (SELECT Count(*) FROM dbo.utb_checklist_history chi WHERE chi.ChecklistID = che.ChecklistID) > 0
          AND c.DemoFlag = 0

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT ''
    SELECT  'Excessively Delayed Tasks: '

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT  LynxID,
            InsuranceCompanyName AS Carrier,
            ServiceChannelName AS 'Service Channel',
            LynxRep AS 'File Assigned To',
            TaskName AS Task,
            TaskAssignedTo AS 'Task Assigned To',
            TaskDelayCount AS 'Delays',
            DateDiff(day, TaskOriginalAlarmDate, TaskAlarmDate) AS 'Days'
      FROM  @tmpExcessiveTaskDelays
      WHERE TaskDelayCount > 2
        OR  DateDiff(day, TaskOriginalAlarmDate, TaskAlarmDate) > 7

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END


    -- Desk Audit Supplements Received

    INSERT INTO @tmpDASupplementsReceived
      SELECT  Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
              d.SupplementSeqNumber,
              i.Name,
              au.NameLast + ', ' + au.NameFirst,
              Convert(varchar(20), d.CreatedDate, 101),
              CASE
                WHEN EXISTS (SELECT sd.DocumentID
                                FROM  utb_claim_aspect_service_channel scasc
                                LEFT JOIN dbo.utb_claim_aspect_service_channel_document scad on (scasc.ClaimAspectServiceChannelID = scad.ClaimAspectServiceChannelID)
                                LEFT JOIN dbo.utb_document sd on (scad.DocumentID = sd.DocumentID)
                                LEFT JOIN dbo.utb_document_type sdt on (sd.DocumentTypeID = sdt.DocumentTypeID)
                                WHERE scasc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                  AND sdt.EstimateTypeFlag = 1
                                  AND sd.SupplementSeqNumber = d.SupplementSeqNumber
                                  AND sd.EstimateTypeCD = 'A') THEN 1
                ELSE 0
              END
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        LEFT JOIN dbo.utb_claim_aspect_service_channel_document cad ON (casc.ClaimAspectServiceChannelID = cad.ClaimAspectServiceChannelID)
        LEFT JOIN dbo.utb_document d ON (cad.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE casc.ServiceChannelCD = 'DA'
          AND ca.EnabledFlag = 1
          AND c.DemoFlag = 0
          AND d.EnabledFlag = 1
          AND dt.EstimateTypeFlag = 1
          AND d.EstimateTypeCD = 'O'
          AND d.SupplementSeqNumber > 0
          AND d.CreatedDate BETWEEN @VLBoundDate AND @UBoundDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT ''
    SELECT  'Desk Audit Supplements Received: ' + Convert(varchar(10), Count(*))
      FROM  @tmpDASupplementsReceived
      WHERE ReceivedDate BETWEEN @LBoundDate AND @UBoundDate

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    SELECT ''
    SELECT  'Desk Audit Supplements Received Past 5 days: ' + Convert(varchar(10), Count(*))
      FROM  @tmpDASupplementsReceived

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END

    SELECT ''
    PRINT 'Detail:'
    SELECT  LynxID,
            SequenceNumber AS Supplement,
            InsuranceCompanyName AS Carrier,
            LynxRep AS 'Assigned To',
            Convert(varchar(10), ReceivedDate, 101) AS 'Received',
            CASE ProcessedFlag
              WHEN 1 THEN 'Yes'
              ELSE 'No'
            END AS 'Audited?'
      FROM  @tmpDASupplementsReceived
      ORDER BY  ReceivedDate DESC

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('SQL Server Error', 16, 1)
        RETURN
    END


     -- Assignment Type/Service Channel Changes

    INSERT INTO @tmpAssignmentTypeSwitched
      SELECT  Convert(varchar(10), ca.LynxID) + '-' + Convert(varchar(5), ca.ClaimAspectNumber),
              i.Name,
              ati.Name,
              (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
                WHERE Code = casc.ServiceChannelCD),
              ca.CreatedDate,
            CASE
              WHEN ( au.UserID IS NOT NULL AND casc.ServiceChannelCD IN ('DA', 'DR') ) THEN au.nameLast + ', ' + au.NameFirst
              WHEN ( ou.UserID IS NOT NULL ) THEN ou.nameLast + ', ' + ou.NameFirst
              ELSE 'Unknown'
            END
        FROM  dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
        LEFT JOIN dbo.utb_assignment_type ati ON (ca.InitialAssignmentTypeID = ati.AssignmentTypeID)
        LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
        LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
        LEFT JOIN dbo.utb_claim_aspect_status cas ON (cas.ClaimAspectID = ca.ClaimAspectID)
      --WHERE ca.StatusID NOT IN (@StatusIDCancelled, @StatusIDClosed, @StatusIDVoided)
        WHERE cas.StatusID IN (SELECT StatusID FROM dbo.utb_status WHERE Name IN ('Active'))
        and ati.ServiceChannelDefaultCD <> casc.ServiceChannelCD
        and casc.PrimaryFlag = 1

    SELECT ''
    SELECT ''
    SELECT  'Open Files with Switched Assignment Types: ' + Convert(varchar(10), Count(*))
      FROM @tmpAssignmentTypeSwitched

    SELECT ''
    PRINT 'Detail:'
    SELECT  LynxID,
            InsuranceCompanyName AS Carrier,
            OldAssignmentType AS 'Initial Assignment',
            NewAssignmentType AS 'Changed To',
            LynxRep AS 'Assigned To',
            Convert(varchar(10), CreateDate, 101) AS 'Received'
      FROM  @tmpAssignmentTypeSwitched
      ORDER BY CreateDate DESC


    SELECT ''

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptAPDKeyIndicator' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptAPDKeyIndicator TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/


