-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperDeskAuditDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2OperDeskAuditDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2OperDeskAuditDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns Desk Audit Details
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ServiceChannelCD     ServiceChannelCD (Indicates whether we pull Desk Audit or Desk Review data)
* (I) @OfficeID             Optional Office ID
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2OperDeskAuditDetail
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    
    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @MonthName              udt_std_name
    DECLARE @ServiceChannelCDWork   udt_std_cd
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    
    DECLARE @Debug      			udt_std_flag
    DECLARE @now        			udt_std_datetime
    DECLARE @DataWarehouseDate    	udt_std_datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2OperDeskAuditDetail'

    SET @Debug = 0


    SET @ServiceChannelCDWork = 'DA'
    
/*    SELECT  @ServiceChannelName = Name
      FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'ServiceChannelCD')
      WHERE Code = @ServiceChannelCDWork
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END*/
   
    -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2002 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    IF @Debug = 1
    BEGIN
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@ServiceChannelCDWork = ' + @ServiceChannelCDWork
    END
    
    
    -- Compile data needed for the report

    SELECT  @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            dc.InsuranceCompanyName as InsuranceCompanyName,
            CASE 
              WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
              ELSE dc.OfficeName
            END AS Office,
            Convert(varchar(15), ca.LynxID) AS LynxID,
            dc.RepNameFirst AS CarrierRepNameFirst,
            dc.RepNameLast AS CarrierRepNameLast,
            dlh.UserNameFirst AS LYNXRepNameFirst,
            dlh.UserNameLast AS LYNXRepNameLast,
            dat.AssignmentTypeDescription as AssignmentType,
            cv.VehicleYear as VehicleYear,
            cv.Make as VehicleMake,
            cv.Model as VehicleModel,
            clm.LossState,
            fc.OriginalEstimateGrossAmt,
            fc.AuditedEstimatewoBettAmt,
            AuditedEstimateAgreedFlag AS EstimateAgreedFlag,
            Convert(varchar(15), dtn.DateValue, 1) AS DateOpened,
            Convert(varchar(15), dtc.DateValue, 1) AS DateClosed,
            Convert(varchar(15), dte.DateValue, 1) AS DateFirstEstimate,
            @DataWareHouseDate as DataWareDate 
      FROM  dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID) -- new time
      LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID) -- close time
      LEFT JOIN dbo.utb_dtwh_dim_time dte ON (fc.TimeIDClosed = dte.TimeID) -- first estimate time
      LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
      LEFT JOIN dbo.utb_dtwh_dim_lynx_handler dlh ON (fc.LynxHandlerAnalystID = dlh.LynxHandlerID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (fc.ServiceChannelID = dsc.ServiceChannelID)
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim_vehicle cv ON (fc.ClaimAspectID = cv.ClaimAspectID)
      LEFT JOIN dbo.utb_claim clm ON (ca.LynxID = clm.LynxID)
      WHERE dtc.MonthOfYear = @RptMonth
        AND dtc.YearValue = @RptYear
        AND dsc.ServiceChannelCD = @ServiceChannelCDWork
        AND fc.TimeIDVoided IS NULL
        AND fc.TimeIDCancelled IS NULL
		AND fc.EnabledFlag = 1
		AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        
      ORDER BY LynxID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperDeskAuditDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2OperDeskAuditDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/