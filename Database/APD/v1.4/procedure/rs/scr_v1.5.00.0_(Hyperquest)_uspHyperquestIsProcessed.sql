-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestIsProcessed' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestIsProcessed 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetDAJobDocuments
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			23Oct2014
* FUNCTION:     Checks to see if the LynxID is already processed to Hyperquest
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestIsProcessed]
	@iLynxID INT
	, @iDocumentID INT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @iLastDocumentID   AS INT
	DECLARE @dtNow DATETIME

    SET @ProcName = 'uspHyperquestIsProcessed'
    SET @ApplicationCD = 'APDHyperquestPartner'
	SET @dtNow = CURRENT_TIMESTAMP

    -- Validation
    -- PreSQL
    
    -- Main 
	IF EXISTS (
		SELECT
			*
		FROM
			utb_hyperquest_processed	
		WHERE
			LynxID = @iLynxID
			AND DocumentID = @iDocumentID
	)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0 
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestIsProcessed' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestIsProcessed TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/