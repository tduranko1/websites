-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowWarrantySelectShop' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowWarrantySelectShop 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowWarrantySelectShop
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh ishegu
* FUNCTION:     Assigns a shop as warranty to a claim vehicle
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  ClaimAspectId of the vehicle being assigned
* (I) @SelectOperationCD            'S' - Select Shop/Appraiser, 'C' - Cancel Shop/Appraiser Selection
* (I) @ShopLocationID               The Shop Location ID to assign
* (I) @AppraiserID                  The Appraiser ID to assign
* (I) @AssignmentRemarks            Remarks sent to the shop    
* (I) @UserID                       The user doing the assign
* (I) @NotifyEvent                  Specifies whether the procedure should send an event notification to APD workflow
* (I) @ShopSearchLogID              The search log id that produced this selection
* (I) @SelectedShopRank             The rank of the shop that produced this selection
* (I) @SelectedShopScore            The score of the shop that produced this selection
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: uspWorkflowWarrantySelectShop.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/uspWorkflowWarrantySelectShop.SQL $
* $Revision: 1 $
* $Author: Jonathan $
* $Date: 9/06/01 4:26p $
* Changes:
* RV - 9/5/07 - Any CEI shop assignments should have the ProgramShopCd set to 'CEI' rather than 'LS' for proper
*               routing of the payment. However there is a potential that a LS shop only could be assigned to the
*			    assignment. 
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowWarrantySelectShop
    @ClaimAspectID                  udt_std_id_big,
    @SelectOperationCD              udt_std_cd,
    @ShopLocationID                 udt_std_id_big = NULL,
    @AssignmentRemarks              udt_std_desc_xlong = NULL,
    @UserID                         udt_std_id,
    @WarrantyAssignmentID           udt_std_id_big = NULL,
    @NotifyEvent                    udt_std_flag = 1
AS
BEGIN
    -- Declare internal variables

    --DECLARE @AppraiserName                      udt_std_name
    DECLARE @AssignmentID                       udt_std_id_big
    DECLARE @AssignmentSuffix                   udt_std_cd
    DECLARE @AssignmentSuffixOld                udt_std_cd
    --DECLARE @CarrierUsesCEIFlag                 udt_std_flag
    --DECLARE @CEIProgramFlag                     udt_std_flag
    DECLARE @ClaimAspectTypeID                  udt_std_id
    DECLARE @ClaimAspectNumber                  udt_std_int
    --DECLARE @CurrentAppraiserID                 udt_std_id_big
    DECLARE @CurrentShopLocationID              udt_std_id_big
    DECLARE @Description                        udt_std_desc_long
    --DECLARE @DeskAuditAppraiserType             udt_std_cd
    --DECLARE @DeskAuditID                        udt_std_id_big
    DECLARE @EventID                            udt_std_id
    DECLARE @EventName                          udt_std_name
    DECLARE @ProgramFlag                        udt_std_flag
    DECLARE @ProgramTypeCD                      udt_std_cd
    DECLARE @ShopLocationName                   udt_std_name
    DECLARE @ClaimAspectServiceChannelID        udt_std_id_big
    
    DECLARE @error                      AS int
    DECLARE @now                        AS datetime

    DECLARE @debug                      AS bit
    DECLARE @StateCode                  AS udt_addr_state
    DECLARE @ProcName                   AS varchar(30)       -- Used for raise error stmts 
    --DECLARE @CertifiedFirstFlag         AS udt_std_flag
    DECLARE @LynxID                     as udt_std_int_big
    DECLARE @DeductibleAmt              as udt_std_money


    SET @debug = 0
    SET @ProcName = 'uspWorkflowWarrantySelectShop'
    

    -- Set Database options
    
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    -- Debug Code
    IF @debug = 1
    BEGIN
        Print 'Parameters:'
        Print '@ClaimAspectServiceChannelID = ' + Convert(varchar(10), @ClaimAspectServiceChannelID)
        Print '@SelectOperationCD = ' + @SelectOperationCD
        Print '@ShopLocationID = ' + Convert(varchar(10), @ShopLocationID)
        Print '@AssignmentRemarks = ' + @AssignmentRemarks
        Print '@UserID = ' + Convert(varchar(10), @UserID)
        Print '@NotifyEvent = ' + Convert(varchar(10), @NotifyEvent)
    END
    -- End Debug


    -- Check to make sure a ClaimAspectID OR LynxID/VehicleNumber combination was passed in

    IF (@ClaimAspectID IS NULL)
    BEGIN
        -- Invalid parameters
    
        RAISERROR('%s: Parameters invalid.  @ClaimAspectID is required.', 16, 1, @ProcName)
        RETURN
    END
    
    
    IF @ClaimAspectID IS NOT NULL
    BEGIN
        -- Validate Claim Aspect Service Channel
        IF NOT EXISTS(
            SELECT ClaimAspectID 
                FROM dbo.utb_claim_aspect
                WHERE ClaimAspectID = @ClaimAspectID)
        BEGIN
            -- Invalid Claim Aspect ID
    
            RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
        
        -- Validate that the claim aspect has warranty turned on
        IF NOT EXISTS(
            SELECT ClaimAspectID 
                FROM dbo.utb_claim_aspect
                WHERE ClaimAspectID = @ClaimAspectID
                  AND WarrantyExistsFlag = 1)
        BEGIN
            -- Invalid Claim Aspect ID
    
            RAISERROR('101|%s|Warranty has not been activated for @ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
        
        -- Validate that the claim aspect has a PS channel
        IF NOT EXISTS(
            SELECT ClaimAspectID 
                FROM dbo.utb_claim_aspect_service_channel casc
                WHERE casc.ClaimAspectID = @ClaimAspectID
                  AND casc.ServiceChannelCD = 'PS'
                  AND casc.EnabledFlag = 1)
        BEGIN
            -- Invalid Claim Aspect ID
    
            RAISERROR('101|%s|Program Shop channel not found for @ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
        
    END

    -- Validate SelectOperationCD
    
    IF (@SelectOperationCD NOT IN ('S', 'C'))
    BEGIN
        -- Invalid @SelectOperationCD
    
        RAISERROR('101|%s|@SelectOperationCD|%s', 16, 1, @ProcName, @SelectOperationCD)
        RETURN
    END


    -- If selection, verify that either ShopLocationID or AppraiserID is passed in
    
    IF @SelectOperationCD = 'S'
    BEGIN
        IF (@ShopLocationID IS NULL)
        BEGIN
            -- Missing either shop location id

            RAISERROR('%s|@ShopLocationID required.', 16, 1, @ProcName)
            RETURN
        END


        -- If Shop Location ID passed in, Check to make sure it is valid

        IF (@ShopLocationID IS NOT NULL) AND
           (NOT EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID))
        BEGIN
            -- Invalid Shop Location ID

            RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
            RETURN
        END
        
        
    END
    IF @SelectOperationCD = 'C'
    BEGIN
      IF @WarrantyAssignmentID IS NULL
      BEGIN
            -- Invalid Warranty Assignment ID

            RAISERROR('101|%s|@WarrantyAssignmentID|%u', 16, 1, @ProcName, @WarrantyAssignmentID)
            RETURN
      END
    END
    
        
    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Get the ClaimAspectType/Number for the claim aspect of this service channel

    SELECT  @ClaimAspectTypeID = ca.ClaimAspectTypeID,
            @ClaimAspectNumber = ca.ClaimAspectNumber,
            @ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      FROM  dbo.utb_claim_aspect ca
        INNER JOIN dbo.utb_claim_aspect_service_channel casc
            ON ca.ClaimAspectID = casc.ClaimAspectID
      WHERE ca.ClaimAspectID = @ClaimAspectID
        AND casc.ServiceChannelCD = 'PS'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update the shop search log
    
    
    /*IF @SelectOperationCD = 'S'
    BEGIN
    
      SET @CarrierUsesCEIFlag = 0
  
    END*/
          


    -- Debug Code
    IF @debug = 1
    BEGIN
        Print ''
        Print 'Derived Variables:'
        Print '@ClaimAspectServiceChannelID = ' + Convert(varchar(10), @ClaimAspectServiceChannelID)
        Print '@ClaimAspectTypeID = ' + Convert(varchar(10), @ClaimAspectTypeID)
        Print '@ClaimAspectNumber = ' + Convert(varchar(10), @ClaimAspectNumber)
        Print ''
        Print 'Insurance Company Configuration:'
        --Print '@CarrierUsesCEIFlag = ' + Convert(varchar(10), @CarrierUsesCEIFlag)
        Print ''
        --Print 'App Variable Settings:'
        --Print '@DeskAuditAppraiserType = ' + @DeskAuditAppraiserType
        --Print '@DeskAuditID = ' + Convert(varchar(10), @DeskAuditID)
    END
    --End Debug


    -- Interpret the Select Operation CD

    IF (@SelectOperationCD = 'S')
    BEGIN
        -- Selection Operation ("NEW")
        
        -- Check to see if there isn't an uncancelled assignment already in progress for this claim aspect
    
        /*IF EXISTS( 
            SELECT AssignmentID FROM dbo.utb_warranty_assignment
                WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                    AND CancellationDate IS NULL )
        BEGIN
            -- Previous assignment must be cancelled first before new assignment can be made
    
            RAISERROR('%s: An Active Assignment exists for this Vehicle.  The existing assignment must be cancelled before another may be made.', 16, 1, @ProcName)
            RETURN
        END*/
    
        
        -- Get Assignment Suffix.  This is simply NULL or letter designation which indicates to the user which assignment this is
        -- for this vahicle/property.  The first assignment has a suffix of NULL (or "").  The first successive assignment has value
        -- "A", the next "B", and so on until "Z".  Any assignment after "Z" will remain "Z".
        
        IF NOT EXISTS(
            SELECT AssignmentID FROM dbo.utb_warranty_assignment
                 WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
        BEGIN
            -- This is the first assignment for this aspect.
            
            SET @AssignmentSuffix = NULL
        END
        ELSE
        BEGIN
            -- At least one assignment record already exists, get the suffix from the lastest one and determine the new one
            
            SELECT TOP 1 @AssignmentSuffixOld = AssignmentSuffix
              FROM  dbo.utb_warranty_assignment asn
                WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
              ORDER BY AssignmentID DESC

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END
            
            
            SELECT @AssignmentSuffix =  CASE 
                                          WHEN @AssignmentSuffixOld IS NULL THEN 'A'
                                          WHEN ASCII(@AssignmentSuffixOld) BETWEEN 65 AND 89 THEN CHAR(ASCII(@AssignmentSuffixOld) + 1)
                                          WHEN @AssignmentSuffixOld = 'Z' THEN 'Z'
                                        END

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END           
        END
        
                                                    
        -- Set up workflow

        -- Check if this is an assignment to the desk audit unit
        
        /*IF ((@ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
           ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))
        BEGIN
            -- This is a desk audit assignment
        
            -- Set event name for workflow

            SET @EventName = 'LYNX Desk Audit Unit Selected for Vehicle'
        
            -- History Description
        
            SET @Description = 'DESK AUDIT (ASSIGNMENT): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + ' was fowarded to LYNX Services desk audit unit.'
            SET @ProgramTypeCD = 'NON'
            SET @CertifiedFirstFlag = 0
        END
        ELSE
        BEGIN*/
            -- This is a regular assignment, check to see if it a Shop assignment or an appraiser assignment
            
            IF @ShopLocationID IS NOT NULL
            BEGIN
                -- For shop selections
                       
                SELECT  @ProgramFlag        = ProgramFlag,
                        @ShopLocationName   = Name
                  FROM  dbo.utb_shop_location 
                  WHERE ShopLocationID = @ShopLocationID

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END

                -- Set event name for workflow

                SET @EventName = 'Shop Selected for Vehicle'
        
                -- History Description
                
                SELECT @Description = CASE
                                        WHEN @ProgramFlag = 1 THEN 
                                          'PROGRAM SHOP (ASSIGNMENT): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + ' was assigned to ' +
                                          @ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @ShopLocationID) + ')'
                                        ELSE 
                                          'NON-PROGRAM SHOP (ASSIGNMENT): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + ' was assigned to ' +
                                          @ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @ShopLocationID) + ')'
                                      END,
                       @ProgramTypeCD = CASE
                                          -- CEI takes precendence with LYNX shops.
                                          -- per JP: 9/5/07 - if the carrier is setup to use CEI shop then mark it is as CEI no matter 
                                          --                  what the CEIProgramFlag for the shop is so that CEI will get the payment 
                                          --                  and they can pay the shop.
                                          WHEN @ProgramFlag = 1 THEN 'LS'
                                          ELSE 'NON'
                                        END

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            /*ELSE
            BEGIN
                -- Appraiser
                       
                SELECT  @AppraiserName = Name
                  FROM  dbo.utb_appraiser
                  WHERE AppraiserID = @AppraiserID

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END

                -- Set event name for workflow

                SET @EventName = 'IA Selected for Vehicle'
        
                -- History Description
                           
                SET @Description = 'APPRAISER (ASSIGNMENT): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + ' was assigned to ' +
                                          @AppraiserName + ' (Appraiser ID ' + Convert(varchar(5), @AppraiserID) + ')'
                SET @ProgramTypeCD = 'NON'
                SET @CertifiedFirstFlag = 0
            END*/
        --END            
    END
    ELSE
    BEGIN
        -- This is a cancellation ("CANCEL")
        
        -- Get the ShopLocationID/AppraiserID of the currently active assignment
        
        SELECT  @ProgramTypeCD          = a.ProgramTypeCD,
                @CurrentShopLocationID  = a.ShopLocationID,
                @ShopLocationName       = sl.Name
        FROM  dbo.utb_warranty_assignment a 
        LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
        WHERE a.AssignmentID = @ClaimAspectServiceChannelID
          AND a.CancellationDate IS NULL

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        -- Check if this was an assignment to the desk audit unit
        
            IF @CurrentShopLocationID IS NOT NULL
            BEGIN
                -- This was a shop assignment

                -- Set event name for workflow

                SET @EventName = 'Vehicle Assignment to Shop Cancelled'

                -- History Description
                
                SELECT @Description = CASE
                                        WHEN @ProgramTypeCD = 'LS' THEN 'PROGRAM SHOP (CANCELLATION): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + '''s assignment to ' +
                                                      @ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @CurrentShopLocationID) + ') was cancelled'
                                        ELSE 'NON-PROGRAM SHOP (CANCELLATION): Vehicle ' + Convert(varchar(5), @ClaimAspectNumber) + '''s assignment to ' +
                                                      @ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @CurrentShopLocationID) + ') was cancelled'
                                      END

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
    END

    --Get the lynx ID first before we can extract the value for @DeductibleAmt

    select    @LynxID = ca.LynxID

    from      utb_CLaim_Aspect ca

    inner join utb_Claim_Aspect_Service_Channel casc
    on         casc.ClaimAspectID = ca.ClaimAspectID

    where      casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    -- Determine the deductible amount by matching the claim aspect coverage with the claim coverage and 
    --   get the first base coverage deductible with Additional flag = 0
    /*select      top 1 
                @DeductibleAmt = cc.DeductibleAmt
    
    from        utb_Claim_Aspect ca
    inner join  utb_Client_Coverage_Type cct
    on          ca.ClientCoverageTypeID =  cct.ClientCoverageTypeID
    and         ca.CoverageProfileCD = cct.CoverageProfileCD
    and         cct.AdditionalCoverageFlag = 0
    
    inner join  utb_Claim_Aspect_Service_Channel casc
    on          ca.ClaimAspectID = casc.ClaimAspectID
    
    inner join  utb_Claim_Coverage cc
    on          ca.LynxID = cc.LynxID
    and         ca.ClientCoverageTypeID = cc.ClientCoverageTypeID
    and         ca.CoverageProfileCD = cc.CoverageTypeCD
    
    
    where       casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    and         cc.addtlcoverageflag = 0*/
    
    SET @DeductibleAmt = 0


    -- Begin the transaction

    BEGIN TRANSACTION WorkflowAssignShopTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF (@SelectOperationCD = 'S')   -- New Assignment
    BEGIN
        -- Create new assignment record

        INSERT INTO dbo.utb_warranty_assignment
        (
            --AppraiserID,
            AssignmentRemarks,
            AssignmentSuffix,
            --CertifiedFirstFlag,
            CommunicationAddress,
            ClaimAspectServiceChannelID,
            EffectiveDeductibleSentAmt,
            ProgramTypeCD,
            SelectionDate,
            ShopLocationID,
            SysLastUserID,
            SysLastUpdatedDate
        )
        VALUES
        (
            --@AppraiserID,
            @AssignmentRemarks,
            @AssignmentSuffix,
            --@CertifiedFirstFlag,
            'F=;E=NA;D=',
            @ClaimAspectServiceChannelID,
            @DeductibleAmt,
            @ProgramTypeCD,
            @now,
            @ShopLocationID,
            @UserID,
            @now
        )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@utb_warranty_assignment', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
        ELSE
        BEGIN
            SELECT @AssignmentID = SCOPE_IDENTITY()
        END
        

        -- If this claim is not a demo claim, update the Shop's Last Assignment Date.  We won't do this on demo claims so
        -- the shop selected won't be penalized when a search on a real claim is performed.  Also, note that we're not going 
        -- to update SysLastUpdatedDate here because if we did, anyone who happened to really be updating the shop location's
        -- information would not be able to because of differing SysLastUpdatedDates.

        /*IF ( SELECT c.DemoFlag FROM dbo.utb_claim_aspect_service_channel casc
                INNER JOIN dbo.utb_claim_aspect ca ON ca.ClaimAspectID = casc.ClaimAspectID
                INNER JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID) 
                WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID) = 0
        BEGIN
            -- This is not a demo claim, update the shop or appraiser's Last Assignment Date

            IF @ShopLocationID IS NOT NULL
            BEGIN
                UPDATE  dbo.utb_shop_location
                  SET   LastAssignmentDate = @now,
                        SysLastUserID = @UserID
                  WHERE ShopLocationID = @ShopLocationID

                IF @@ERROR <> 0
                BEGIN
                    -- Update failure

                    RAISERROR('104|%s|utb_shop_location', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END        
            END
            ELSE
            BEGIN
                UPDATE  dbo.utb_appraiser
                  SET   LastAssignmentDate = @now,
                        SysLastUserID = @UserID
                  WHERE AppraiserID = @AppraiserID

                IF @@ERROR <> 0
                BEGIN
                    -- Update failure

                    RAISERROR('104|%s|utb_appraiser', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END        
            END     
        END        */
    END
    ELSE    -- Cancel
    BEGIN
        -- Set the assignment as inactive

        UPDATE  dbo.utb_warranty_assignment
          SET   CancellationDate = @Now,       -- Set Inactive assignment
                SysLastUserID = @UserID,
                SysLastUpdatedDate = @now
          WHERE AssignmentID = @WarrantyAssignmentID

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
        
        SET @AssignmentID = @WarrantyAssignmentID
    END


    -- Notify APD of the selection event if we have been instructed to do so.  (If not, it is up to the caller to notify
    -- APD of the event.)

    /*IF @NotifyEvent = 1
    BEGIN
        -- Throw the event here

        SELECT  @EventID = EventID
          FROM  dbo.utb_event
          WHERE Name = @EventName
    
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    
        IF @EventID IS NULL
        BEGIN
            -- Invalid APD Data
    
            RAISERROR('102|%s|%s|utb_event', 16, 1, @ProcName, @EventName)
            RETURN
        END
    
        -- Debug Code
        IF @debug = 1
        BEGIN
            Print ''
            Print 'Variables For Workflow:'
            Print '@EventName = ' + @EventName
            Print '@EventID = ' + Convert(varchar(10), @EventID)
            Print '@Description = ' + @Description
        END
        --End Debug

    
        exec uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                    @Description = @Description,
                                    @UserID = @UserID

        -- Check error value
        
        IF @@ERROR <> 0
        BEGIN
            -- Error notifying APD of event
        
            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END*/


    COMMIT TRANSACTION WorkflowAssignShopTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @AssignmentID
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowWarrantySelectShop' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowWarrantySelectShop TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: uspWorkflowWarrantySelectShop.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/uspWorkflowWarrantySelectShop.SQL $
* $Revision: 1 $
* $Author: Jonathan $
* $Date: 9/06/01 4:26p $
* $History: uspWorkflowWarrantySelectShop.SQL $
 * 
 * *****************  Version 1  *****************
 * User: Jonathan     Date: 9/06/01    Time: 4:26p
 * Created in $/Database/APD/v1.0.0/procedure
 * Initial Entry
*
************************************************************************************************************************/
