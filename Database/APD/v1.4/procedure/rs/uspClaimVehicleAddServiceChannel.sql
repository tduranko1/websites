-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleAddServiceChannel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVehicleAddServiceChannel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVehicleAddServiceChannel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Add a new service channel to the vehicle
*
* PARAMETERS:  
* (I) @ClaimAspectId        The Claim Aspect Id of the vehicle being retrieved
* (I) @ServiceChannelCD     The new service channel code
* (I) @PrimaryFlag          The Primary flag for the new service channel
* (I) @UserID               The user id performing this operation
*
* RESULT SET:
* An XML Data stream containing the new service channel information
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimVehicleAddServiceChannel
    @ClaimAspectID            udt_std_int_big,
    @ServiceChannelCD         udt_std_cd,
    @PrimaryFlag              udt_std_flag,
    @UserID                   udt_std_id
AS
BEGIN
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now               AS datetime
    DECLARE @ClaimAspectServiceChannelID AS bigint
    DECLARE @ServiceChannelReactivated  AS bit

    SET @ProcName = 'uspClaimVehicleAddServiceChannel'

    SET NOCOUNT ON
    
    SET @now = CURRENT_TIMESTAMP
    
    
    -- Call Workflow
    
    EXEC uspWorkflowActivateServiceChannel @ClaimAspectServiceChannelID,
                                           @ServiceChannelReactivated,
                                           @ClaimAspectID,
                                           @ServiceChannelCD,
                                           @PrimaryFlag,
                                           @UserID,
                                           1


    -- Create XML Document to return the new service channel
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Claim Aspect Service Channel
            NULL AS [ClaimAspectServiceChannel!2!ClaimAspectServiceChannelID],
            NULL AS [ClaimAspectServiceChannel!2!SysLastUpdatedDate]


    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- Claim Aspect Service Channel
            @ClaimAspectServiceChannelID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing

   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleAddServiceChannel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVehicleAddServiceChannel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
