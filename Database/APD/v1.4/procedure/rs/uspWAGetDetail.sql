-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWAGetDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWAGetDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWAGetDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWAGetDetail
    @partnerDB  SYSNAME,
    @LynxID     bigint
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @cmd AS NVARCHAR(1000)
    
    SET @cmd = 'SELECT isNULL(XML, '''') as XML ' +
                'FROM ' + @partnerDB + '.dbo.utb_fnol_load ' +
                'WHERE LynxID = ''' + convert(varchar, @LynxID) + ''''
                
    EXEC sp_executesql @stmt=@cmd
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWAGetDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWAGetDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/