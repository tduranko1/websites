-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleSetPrimaryChannel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVehicleSetPrimaryChannel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVehicleSetPrimaryChannel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Sets a service channel as the primary
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID    The Claim Aspect Service Channel Id of the vehicle
* (I) @ClaimAspectID                  The Claim Aspect Id of the vehicle. Just to check if the Service Channel ID was passed correct
* (I) @UserID                         The User id performing this operation
* (I) @SysLastUpdatedDate             The Last updated date on the record 
*
* RESULT SET:
* An XML Data stream containing the service channel updated information
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimVehicleSetPrimaryChannel
    @ClaimAspectServiceChannelID udt_std_int_big,
    @ClaimAspectID               udt_std_int_big,
    @UserID                      udt_std_int_big,
    @SysLastUpdatedDate          varchar(30)
AS
BEGIN
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now               AS datetime
    DECLARE @error             AS int
    DECLARE @rowcount          AS int

    SET @ProcName = 'uspClaimVehicleSetPrimaryChannel'

    SET NOCOUNT ON
    
    -- Validate the Claim Aspect Service Channel ID passed in. It should exist in our system and must belong the Claim AspectID
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                  FROM dbo.utb_claim_aspect_service_channel
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                    AND ClaimAspectID = @ClaimAspectID)
                    
    BEGIN
        -- Invalid Claim Aspect Service Channel ID
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u is invalid or does not belong to ClaimAspectID %u', 16, 1, @ProcName, @ClaimAspectServiceChannelID, @ClaimAspectID)
        RETURN
    END

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Validate the updated date parameter for Claim
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_aspect_service_channel', @ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END

    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION SvcChannelUpdTrn

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Reset all other Service Channel primary flag to zero
    UPDATE dbo.utb_claim_aspect_service_channel
       SET  PrimaryFlag = 0
     WHERE ClaimAspectID = @ClaimAspectID
    

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Now set the current service channel id to primary
    UPDATE dbo.utb_claim_aspect_service_channel
       SET  PrimaryFlag               = 1,
            SysLastUserID             = @UserID,
            SysLastUpdatedDate        = @now
     WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION SvcChannelUpdTrn

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Claim Level
            NULL AS [ClaimAspectServiceChannel!2!ClaimAspectServiceChannelID],
            NULL AS [ClaimAspectServiceChannel!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Claim Level
            @ClaimAspectServiceChannelID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount    
 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleSetPrimaryChannel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVehicleSetPrimaryChannel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
