-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCoverageTypeID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetCoverageTypeID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetCoverageTypeID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the client coverage type ID from the utb_client_coverage_type table by InsuranceCompanyID 
*				and Coverage Type
*
* PARAMETERS:  
*				InsuranceCompanyID
*				, CoverageType 
* RESULT SET:
*				Integer Value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetCoverageTypeID
	@iInsuranceCompanyID INT
	, @vCoverageProfileCD VARCHAR(6)
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
      ClientCoverageTypeID
	FROM
		 dbo.utb_client_coverage_type
	WHERE
		InsuranceCompanyID = @iInsuranceCompanyID
		AND CoverageProfileCD = @vCoverageProfileCD
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCoverageTypeID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetCoverageTypeID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetCoverageTypeID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/