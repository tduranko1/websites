-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSpecialtyUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopSpecialtyUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopSpecialtyUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Creates a new Shop Location Specialty relationship
*
* PARAMETERS:  
* (I) @ShopID                   The Shop Location's unique identity
* (I) @SpecialtyIDs             The Specialty's unique identity
* (I) @SysLastUserID            The Shop Location Specialty's updating user identity
*
* RESULT SET:
* ShopLocationID                The Shop Location unique identity
* SpecialtyID                   The Specialty unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopSpecialtyUpdDetail
(
	@ShopID                    	        udt_std_int_big,
	@SpecialtyIDs                       varchar(50),
  @SysLastUserID                      udt_std_id,
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                    udt_std_int  
    DECLARE @rowcount                 udt_std_int
    DECLARE @now                      udt_std_datetime
    DECLARE @ProcName                 varchar(30)       -- Used for raise error stmts 
    DECLARE @LogComment               udt_std_desc_long
    
    
    DECLARE @tmpSpecialtyIDs          TABLE (SpecialtyID    tinyint       NOT NULL)
    
    DECLARE @tmpSpecialtyChange       TABLE (SpecialtyID    tinyint       NOT NULL,
                                             Name           varchar(50)   NOT NULL,
                                             StateFlag      bit           NOT NULL)
                                             
    
                                             

    SET @ProcName = 'uspSMTShopSpecialtyUpdDetail'
    SET @now = CURRENT_TIMESTAMP


    -- Apply edits
    IF @ShopID IS NOT NULL
    BEGIN
      IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID)
      BEGIN
        -- Invalid Shop Location
    
        RAISERROR  ('101|%s|@ShopID|%u', 16, 1, @procname, @ShopID)
        RETURN
      END
    END
    ELSE
    BEGIN
      -- Invalid Shop Location
  
      RAISERROR  ('101|%s|@ShopID|%u', 16, 1, @procname)
      RETURN
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
      IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
      BEGIN
        -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|%u', 16, 1, @procname, @SysLastUserID)
        RETURN
      END
    END
    ELSE
    BEGIN
      -- Invalid User
  
      RAISERROR  ('101|%s|@SysLastUserID|%u', 16, 1, @procname)
      RETURN
    END


    INSERT INTO @tmpSpecialtyIDs SELECT value FROM dbo.ufnUtilityParseString(@SpecialtyIDs, ',', 1)
    
        
    IF (@@ERROR <> 0)
    BEGIN
      -- Error inserting into @tmpSpecialtyIDs
      
      RAISERROR  ('105|%s|@tmpSpecialtyIDs', 16, 1, @procname)
      RETURN
    END
    
    
    -- Verify the passed in SpecialtyID(s) are valid.
    IF EXISTS(SELECT SpecialtyID FROM @tmpSpecialtyIDs WHERE SpecialtyID NOT IN (SELECT SpecialtyID FROM dbo.utb_specialty))
    BEGIN
      -- Invalid SpecialtyID(s)
        
      RAISERROR  ('101|%s|@tmpSpecialtyIDs| One or more SpecialyIDs are invalid: %s', 16, 1, @procname, @SpecialtyIDs)
      RETURN
    END


    -- Gather specialties that are being turned on in this update.
    INSERT INTO @tmpSpecialtyChange 
    SELECT t.SpecialtyID, s.Name, 1 
    FROM @tmpSpecialtyIDs t INNER JOIN dbo.utb_specialty s ON t.SpecialtyID = s.SpecialtyID
    WHERE t.SpecialtyID NOT IN (SELECT SpecialtyID 
                              FROM dbo.utb_shop_location_specialty
                              WHERE ShopLocationID = @ShopID)
                              
    UNION ALL
    
    SELECT s.SpecialtyID, s.Name, 0 
    FROM dbo.utb_shop_location_specialty t INNER JOIN dbo.utb_specialty s ON t.SpecialtyID = s.SpecialtyID
    WHERE ShopLocationID = @ShopID
      AND t.SpecialtyID NOT IN (SELECT SpecialtyID 
                              FROM @tmpSpecialtyIDs)
                              
    IF (@@ERROR <> 0)
    BEGIN
      -- Invalid User
      
      RAISERROR  ('105|%s|@tmpSpecialtyChange', 16, 1, @procname)
      RETURN
    END
       

    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocSpecInsDetailTran1

    
    -- Delete all specialties for this shop.
    DELETE dbo.utb_shop_location_specialty
    WHERE ShopLocationID = @ShopID

    IF (@@ERROR <> 0)
    BEGIN
      -- Invalid User
      
      RAISERROR  ('106|%s|utb_shop_location_specialty', 16, 1, @procname)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    
    
    -- Insert new list of specialties that are turned on for this shop.
    INSERT INTO dbo.utb_shop_location_specialty (ShopLocationID, 
                                                 SpecialtyID, 
                                                 SysLastUserID, 
                                                 SysLastUpdatedDate) 
                                          SELECT @ShopID, 
                                                 SpecialtyID, 
                                                 @SysLastUserID, 
                                                 @now
                                          FROM @tmpSpecialtyIDs
    
    IF (@@ERROR <> 0)
    BEGIN
      -- Error inserting into utb_shop_location_specialty
      
      RAISERROR  ('105|%s|utb_shop_location_specialty', 16, 1, @procname)
      RETURN
    END
    
    
    -- Begin Audit Log code  -----------------------------------------------------------------------------------
        
    INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                   KeyDescription,
                                   KeyID,
                                   LogComment,
                                   SysLastUserID,
                                   SysLastUpdatedDate)
                            SELECT 'P',
                                   Name,
                                   @ShopID,
                                   '''' + Name + ''' specialty was turned ' + CASE StateFlag WHEN 1 THEN 'ON' ELSE 'OFF' END,
                                   @SysLastUserID,
                                   @now
                            FROM @tmpSpecialtyChange
                            ORDER BY StateFlag DESC 
    
    IF @@ERROR <> 0
    BEGIN
      -- Error raised in called procedure
      ROLLBACK TRANSACTION
      RETURN
    END
    
    -- End of Audit Log code  ----------------------------------------------------------------------------------
    
    
    COMMIT TRANSACTION AdmShopLocSpecInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
      -- SQL Server Error
  
      RAISERROR  ('99|%s', 16, 1, @procname)
      RETURN
    END

    SELECT @ShopID AS ShopID

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSpecialtyUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopSpecialtyUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/