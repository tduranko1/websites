-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInsClientTowing' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInsClientTowing 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspInsClientTowing
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Inserts towing information
*
* PARAMETERS:  
*		@InsuranceCompanyID
*		@AnalystUserID
*		@ClaimNumber
*		@CusAddress
*		@CusAltPhone
*		@CusCity
*		@CusEmail
*		@CusHomePhone
*		@CusName
*		@CusState
*		@CusWorkPhone
*		@CusZip
*		@DesAddress
*		@DesCity
*		@DesFacilityContactName
*		@DesHoursOfOperation
*		@DesPhone
*		@DesState
*		@DesZip
*		@LynxID
*		@LocAddress
*		@LocCity
*		@LocFacilityContactName
*		@LocHoursOfOperation
*		@LocPhone
*		@LocState
*		@LocZip
*		@VehConditionDescription
*		@VehLicensePlateNumber
*		@VehMake
*		@VehModel
*		@VehVINState
*		@VehYear
*		@Comments
*		@SysLastUserID
*
* RESULT SET:
*   All data related to towing
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInsClientTowing
		@InsuranceCompanyID			udt_std_int_small
		, @AnalystUserID			udt_std_int
		, @ClaimNumber				VARCHAR(50)
		, @CusAddress				udt_addr_line_1
		, @CusAltPhone				VARCHAR(15)
		, @CusCity					udt_addr_city
		, @CusEmail					udt_web_email
		, @CusHomePhone				VARCHAR(15)
		, @CusName					VARCHAR(100)
		, @CusState					udt_addr_state
		, @CusWorkPhone				VARCHAR(15)
		, @CusZip					udt_addr_zip_code
		, @DesAddress				udt_addr_line_1
		, @DesCity					udt_addr_city
		, @DesFacilityContactName	VARCHAR(100)
		, @DesHoursOfOperation		VARCHAR(150)
		, @DesPhone					VARCHAR(15)
		, @DesState					udt_addr_state
		, @DesZip					udt_addr_zip_code
		, @LynxID					udt_std_int_big
		, @LocAddress				udt_addr_line_1
		, @LocCity					udt_addr_city
		, @LocFacilityContactName	VARCHAR(100)
		, @LocHoursOfOperation		VARCHAR(150)
		, @LocPhone					VARCHAR(15)
		, @LocState					udt_addr_state
		, @LocZip					udt_addr_zip_code
		, @VehConditionDescription	VARCHAR(255)
		, @VehLicensePlateNumber	udt_auto_plate_number
		, @VehMake					VARCHAR(50)
		, @VehModel					VARCHAR(50)
		, @VehVINState				udt_auto_vin
		, @VehYear					udt_std_int_small
		, @Comments					VARCHAR(1000)
		, @SysLastUserID			udt_std_id
		, @TowingID					INT OUTPUT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    SET @TowingID = 0
    
    DECLARE @Now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspInsClientTowing'
	SET @Now = CURRENT_TIMESTAMP
    
    -- Set Database options
    
    SET NOCOUNT ON
    

    -- Check to make sure a valid Insurance Company id was passed in
    
    IF  (@InsuranceCompanyID IS NULL) OR
        (NOT EXISTS(SELECT @InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))

    BEGIN
        -- Invalid Insurance Company ID
		SELECT 'Insurance company ID ' + @InsuranceCompanyID + ' is not a valid insurance company ID.' AS TowingID
        --RAISERROR('100|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

	BEGIN TRANSACTION

	INSERT INTO 
		dbo.utb_client_towing
	(
		InsuranceCompanyID
		, AnalystUserID
		, ClaimNumber
		, CusAddress
		, CusAltPhone
		, CusCity
		, CusEmail
		, CusHomePhone
		, CusName
		, CusState
		, CusWorkPhone
		, CusZip
		, DesAddress
		, DesCity
		, DesFacilityContactName
		, DesHoursOfOperation
		, DesPhone
		, DesState
		, DesZip
		, EnabledFlag
		, LynxID
		, LocAddress
		, LocCity
		, LocFacilityContactName
		, LocHoursOfOperation
		, LocPhone
		, LocState
		, LocZip
		, VehConditionDescription
		, VehLicensePlateNumber
		, VehMake
		, VehModel
		, VehVINState
		, VehYear
		, Comments
		, SysLastUserID
		, SysLastUpdatedDate
	)
	VALUES
	(
		@InsuranceCompanyID
		, @AnalystUserID
		, @ClaimNumber
		, @CusAddress
		, @CusAltPhone
		, @CusCity
		, @CusEmail
		, @CusHomePhone
		, @CusName
		, @CusState
		, @CusWorkPhone
		, @CusZip
		, @DesAddress
		, @DesCity
		, @DesFacilityContactName
		, @DesHoursOfOperation
		, @DesPhone
		, @DesState
		, @DesZip
		, 1
		, @LynxID
		, @LocAddress
		, @LocCity
		, @LocFacilityContactName
		, @LocHoursOfOperation
		, @LocPhone
		, @LocState
		, @LocZip
		, @VehConditionDescription
		, @VehLicensePlateNumber
		, @VehMake
		, @VehModel
		, @VehVINState
		, @VehYear
		, @Comments
		, @SysLastUserID
		, @Now	
	)

    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_client_towing', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION

	SET @TowingID = @@IDENTITY
	SELECT @TowingID AS TowingID
	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInsClientTowing' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInsClientTowing TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspInsClientTowing TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/