-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetColumnNames' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetColumnNames 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetColumnNames
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all column names table
*
* PARAMETERS:  
*
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetColumnNames
	@vTableName AS VARCHAR(255)
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 

    -- Set Database options
	SELECT DISTINCT
		column_name=syscolumns.name
    FROM 
		sysobjects 
		JOIN syscolumns 
			ON sysobjects.id = syscolumns.id
		JOIN systypes 
			ON syscolumns.xtype=systypes.xtype
    WHERE 
		sysobjects.xtype='U'
		AND sysobjects.name = @vTableName
	ORDER BY 
		column_name
    
	SET NOCOUNT ON
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetColumnNames' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetColumnNames TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetColumnNames TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/