-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION

-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopParentData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhShopParentData 
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhShopParentData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Mark A. Homison
* FUNCTION:     Extracts Shop Parent data from APD and populates the Data Warehouse.
*
* PARAMETERS:   None
*
* RESULT SET:   None
*
* SWR 201988 09-26-12 MAH - Initial Version.
*
************************************************************************************************************************/

CREATE PROCEDURE dbo.uspDtwhShopParentData	
AS
BEGIN
    Declare @debug              AS bit
    DECLARE @error              AS int
    DECLARE @rowcount           AS int
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SELECT @ProcName = 'uspDtwhShopParentData'

    SELECT @debug = 0
    
    -- Set Database options
    
    SET NOCOUNT ON

    -- Get current timestamp

    PRINT 'Deleting from utb_dtwh_dim_shop_parent...'
    
    DELETE FROM dbo.utb_dtwh_dim_shop_parent
    
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_shop_parent', 16, 1, @ProcName)
        RETURN
    END

	-- -----------------------------------------------------------------------------------------
 	
	INSERT INTO dbo.utb_dtwh_dim_shop_parent
		   (ShopParentID,
			Address1,
			Address2,
			AddressCity,
			AddressCounty,
			AddressState,
			AddressZip,
			FaxNumber,
			FaxNumberExtension,
			LastUserID,
			LastUpdatedByName,
			LastUpdatedDate,
			PhoneNumber,
			PhoneNumberExtension,
			ShopParentName)
	SELECT  p.ShopParentID,
			p.Address1,
			p.Address2,
			p.AddressCity,
			p.AddressCounty,
			p.AddressState,
			p.AddressZip,
			p.FaxAreaCode + ' ' + 
			p.FaxExchangeNumber + ' ' + 
			p.FaxUnitNumber, -- FaxNumber,
			p.FaxExtensionNumber,
			p.SysLastUserID,
			ISNULL(
		    (SELECT CASE WHEN (u.UserID = 0)
						 THEN NameLast
						 ELSE NameLast+', '+NameFirst
				    END
			   FROM utb_user u
			  WHERE u.UserID = p.SysLastUserID),'N/A') AS LastUpdatedByName,
			p.SysLastUpdatedDate,
			p.PhoneAreaCode + ' ' + 
			p.PhoneExchangeNumber + ' ' + 
			p.PhoneUnitNumber, -- PhoneNumber,
			p.PhoneExtensionNumber,
			p.Name
	  FROM  dbo.utb_shop_parent p
     WHERE  EXISTS
           (SELECT  'x'
			  FROM  dbo.utb_shop s,
					dbo.utb_shop_location l
			 WHERE  s.ShopParentID     = p.ShopParentID
			   AND  s.ShopID		   = l.ShopID
               AND  l.EnabledFlag      = 1
			   AND (l.ProgramFlag      = 1 
			    OR  l.CEIProgramFlag   = 1 
			    OR  l.ReferralFlag     = 1
			    OR  l.CertifiedFirstId IS NOT NULL))

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error Inserting into utb_dtwh_dim_shop_parent', 16, 1, @ProcName)
        RETURN
    END

    PRINT 'Inserted ' + CONVERT(varchar(10), @rowcount) + ' records into utb_dtwh_dim_shop_parent...'
    
	-- -----------------------------------------------------------------------------------------
 	
	PRINT 'Shop Parent Data Warehouse Extract complete...'

END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopParentData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhShopParentData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
