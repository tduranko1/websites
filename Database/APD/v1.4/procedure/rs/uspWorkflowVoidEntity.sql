-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspWorkflowVoidEntity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowVoidEntity
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowVoidEntity
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will void the entity
*
* PARAMETERS:
* (I) @ClaimAspectID  The ClaimAspectID of the entity for which the operation is being performed
* (I) @InsuranceID    The Insurance company ID to which the LynxID belongs
* (I) @Comment        The claim rep's comment for canceling the claim
* (I) @UserID         The ID of the user that is making this change
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- ALTER  the stored procedure


CREATE PROCEDURE dbo.uspWorkflowVoidEntity
    @ClaimAspectID       udt_std_id_big,
    @InsuranceCompanyID  udt_std_id_big,
    @Comment             udt_std_note,
    @UserID              udt_std_id,
    @ApplicationCD       udt_std_cd='APD'
AS
BEGIN
    DECLARE @ProcName                       AS varchar(30)
    DECLARE @debug                          AS bit
    DECLARE @now                            AS udt_std_datetime
    DECLARE @error                          AS int
    DECLARE @rowcount                       AS int
    
    DECLARE @InsuranceCompanyIDClaim        AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       AS udt_std_id
    DECLARE @ClaimAspectTypeIDClaim         AS udt_std_id
    DECLARE @ClaimAspectIDClaim             AS udt_std_id_big
    DECLARE @NoteTypeIDClaim                AS udt_std_id
    DECLARE @EventIDVoidServiceChannel      AS udt_std_id
    DECLARE @EventIDVoidVehicle             AS udt_std_id
    DECLARE @EventIDCloseClaim              AS udt_std_id
    DECLARE @EventIDReICancelled            AS udt_std_id
    DECLARE @EventNameReICancelled          AS udt_std_desc_short
    
    DECLARE @w_ClaimAspectServiceChannelID  AS udt_std_id_big
    DECLARE @w_ServiceChannelCD             AS udt_std_cd
    DECLARE @w_EventDesc                    AS udt_std_desc_mid
    DECLARE @w_ChecklistID                  AS udt_std_id_big
    DECLARE @w_ChecklistLastUpdatedDate     AS udt_std_desc_mid
    DECLARE @w_ChecklistIDNote              AS udt_std_note
    
    

    SET @ProcName = 'uspWorkflowVoidEntity'    
    SET @debug = 0
    
    IF @debug = 1
    BEGIN
        Print ''
        Print 'Stored Proc Input Params:'
        Print '@ClaimAspectID: ' + convert(varchar, @ClaimAspectID)
        Print '@InsuranceCompanyID: ' + convert(varchar, @InsuranceCompanyID)
        Print '@Comment: ' + isNull(@Comment, '[null]')
        Print '@UserID: ' + convert(varchar, @UserID)
    END

    -- Get Claim Aspect Type ID For Vehicle
    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- ClaimAspectTypeID Not Found for Claim

        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Validate Claim Aspect ID Passed exists and is a vehicle claim aspect        
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID AND ClaimAspectTypeID = @ClaimAspectTypeIDVehicle))
    BEGIN
        -- Invalid ClaimAspect ID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END  
    
    -- Validate User ID passed
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Validate Insurance Company ID passed with that for the claim
    SELECT @InsuranceCompanyIDClaim = c.InsuranceCompanyID
    FROM dbo.utb_claim_aspect ca INNER JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    WHERE ca.ClaimAspectID = @ClaimAspectID
    
    IF @@ERROR <> 0 
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      RETURN
    END
    
    IF (@InsuranceCompanyID <> @InsuranceCompanyIDClaim)
    BEGIN
      -- Doesnt match
      RAISERROR('111|%s|%u|%u',16,1,@ProcName,@InsuranceCompanyIDClaim,@InsuranceCompanyID)
      RETURN
    END
    
    -- Get ClaimAspectTypeID for the Claim
    SELECT  @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- ClaimAspectTypeID Not Found for Claim

        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END  
    
    -- Get reference to claim aspect for claim
    SELECT @ClaimAspectIDClaim = ca1.ClaimAspectID
    FROM utb_claim_aspect ca1 
    INNER JOIN utb_claim_aspect ca2 ON (ca1.LynxID = ca2.LynxID)
    WHERE ca1.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
      AND ca2.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Has the vehicle itself been cancelled?
    IF EXISTS (SELECT cas.StatusID 
               FROM dbo.utb_claim_aspect_status cas
               WHERE cas.ClaimAspectID = @ClaimAspectID
                 AND cas.StatusTypeCD IS NULL
                 AND cas.StatusID IN (SELECT StatusID
                                      FROM dbo.utb_status
                                      WHERE Name IN ('Vehicle Cancelled','Vehicle Closed')))
    BEGIN
      RAISERROR('1|This vehicle has been cancelled or is closed and can not be voided.',16,1)
      RETURN
    END
    
    -- Does vehicle have any fees already attached?
    IF EXISTS (SELECT InvoiceID
               FROM dbo.utb_invoice 
               WHERE ClaimAspectID = @ClaimAspectID
                 AND EnabledFlag = 1
                 AND ItemTypeCD = 'F')
    BEGIN
      -- There are billing records for vehicle
      RAISERROR('1|This vehicle can not be voided because it already has fees applied to it.',16,1)
      RETURN
    END
    
    -- Does vehicle have assignments that have already been sent to shop/appraiser?
    
    IF EXISTS (SELECT StatusID
               FROM dbo.utb_claim_aspect_status
               WHERE ClaimAspectID = @ClaimAspectID
                 AND StatusID IN (SELECT StatusID
                                  FROM dbo.utb_status
                                  WHERE StatusTypeCD IS NOT NULL
                                    AND StatusTypeCD <> 'SC'
                                    AND Name IN ('Enroute','Sent','Received By Appraiser')))
    BEGIN
      -- Assignment has been sent for one or more service channels.
      RAISERROR('1|This vehicle can not be voided since assignments have already been sent our for one or more service channels.',16,1)
      RETURN
    END
    
    -- Validate that a comment was entered concerning the voiding
    IF (@Comment IS NULL) OR (LEN(RTRIM(LTRIM(@Comment))) = 0)
    BEGIN
      RAISERROR('1|APD System requires a non-blank comment be entered in order to void a vehicle. Please enter a comment and try voiding again.',16,1)
      RETURN
    END
    ELSE
    BEGIN
      IF UPPER(SUBSTRING(@Comment,1,14)) <> SUBSTRING('VEHICLE VOIDED:',1,14)
      BEGIN
        SET @Comment = 'VEHICLE VOIDED: ' + @Comment
      END
    END
    
    IF @debug = 1 PRINT 'Comment = ' + @Comment
    
    -- All validations are cleared,  start prepping to do voiding
    -- Get Event ID for "Vehicle Service Channel Voided"
    SELECT @EventIDVoidServiceChannel = EventID
    FROM dbo.utb_event
    WHERE Name = 'Vehicle Service Channel Voided'
    
    IF @@ERROR <> 0
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      RETURN
    END
    
    IF @EventIDVoidServiceChannel IS NULL
    BEGIN
      RAISERROR('102|%s|"Vehicle Service Channel Voided"|utb_event',16,1,@ProcName)
      RETURN
    END

    -- Get Event ID for "Vehicle Voided"

    SELECT @EventIDVoidVehicle = EventID
      FROM dbo.utb_event
      WHERE Name = 'Vehicle Voided'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDVoidVehicle IS NULL
    BEGIN
       -- Event ID Not Found

        RAISERROR('102|%s|"Vehicle Voided"|utb_event', 16, 1, @ProcName)
        RETURN
    END


    -- Get Event ID for "Claim Closed"

    SELECT @EventIDCloseClaim = EventID
      FROM dbo.utb_event
      WHERE Name = 'Claim Closed'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDCloseClaim IS NULL
    BEGIN
       -- Event ID Not Found

        RAISERROR('102|%s|"Claim Closed"|utb_event', 16, 1, @ProcName)
        RETURN
    END
    -- Get Note Type ID for Claim
    SELECT  @NoteTypeIDClaim = NoteTypeID
      FROM  dbo.utb_note_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @NoteTypeIDClaim IS NULL
    BEGIN
       -- Note Type ID for Claim

        RAISERROR('102|%s|"Note Type: Claim"|utb_note_type', 16, 1, @ProcName)
        RETURN
    END
        
    SET @now = CURRENT_TIMESTAMP
    
    IF @debug = 1
    BEGIN
      PRINT ''
      PRINT 'Stored Proc variables:'
        Print '@ProcName = ' + convert(varchar, @ProcName)
        Print '@InsuranceCompanyIDClm = ' + convert(varchar, @InsuranceCompanyIDClaim)
        Print '@ClaimAspectTypeIDClaim = ' + convert(varchar, @ClaimAspectTypeIDClaim)
        Print '@ClaimAspectTypeIDVehicle = ' + convert(varchar, @ClaimAspectTypeIDVehicle)
        Print '@NoteTypeIDClaim = ' + convert(varchar, @NoteTypeIDClaim)        
        Print '@ClaimAspectIDClaim = ' + convert(varchar, @ClaimAspectIDClaim)
        Print '@EventIDVoidVehicle = ' + convert(varchar, @EventIDVoidVehicle)
        Print '@EventIDCloseClaim = ' + convert(varchar, @EventIDCloseClaim)
        Print ''
    END

    BEGIN TRANSACTION WorkflowVoidEntity1
    if @debug = 1
    begin
      SELECT casc.ClaimAspectServiceChannelID,
             casc.ServiceChannelCD
      FROM dbo.utb_claim_aspect_service_channel casc
      INNER JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      WHERE ca.ClaimAspectID = @ClaimAspectID
    
    end
    
    
    -- First we need to walk through service channels and void each one out
    DECLARE csrServiceChannels CURSOR FOR
      SELECT casc.ClaimAspectServiceChannelID,
             casc.ServiceChannelCD
      FROM dbo.utb_claim_aspect_service_channel casc
      INNER JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      WHERE ca.ClaimAspectID = @ClaimAspectID
      
    OPEN csrServiceChannels
    
    IF @@ERROR <> 0
    BEGIN
      RAISERROR('99|%s',16,1)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    FETCH NEXT FROM csrServiceChannels INTO @w_ClaimAspectServiceChannelID, @w_ServiceChannelCD

    IF @@ERROR <> 0
    BEGIN
      RAISERROR('99|%s',16,1)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    WHILE @@FETCH_STATUS=0
    BEGIN
      
      -- First thing we need to do is cleanup any outstanding task
      if @debug = 1
      begin
        SELECT cl.ChecklistID,
               dbo.ufnUtilityGetDateString(cl.SysLastUpdatedDate),
               t.Name
        FROM dbo.utb_checklist cl INNER JOIN dbo.utb_task t ON (cl.TaskID = t.TaskID)
        WHERE cl.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
      end      
      DECLARE csrTasks CURSOR FOR
      SELECT cl.ChecklistID,
             dbo.ufnUtilityGetDateString(cl.SysLastUpdatedDate),
             t.Name
      FROM dbo.utb_checklist cl INNER JOIN dbo.utb_task t ON (cl.TaskID = t.TaskID)
      WHERE cl.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
      
      OPEN csrTasks

      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1)
        ROLLBACK TRANSACTION
        RETURN
      END
      
      FETCH NEXT FROM csrTasks INTO @w_ChecklistID, @w_ChecklistLastUpdatedDate, @w_ChecklistIDNote
      
      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1)
        ROLLBACK TRANSACTION
        RETURN
      END
      
      WHILE @@FETCH_STATUS = 0
      BEGIN
        
        IF @debug = 1
        BEGIN
          PRINT 'Closing ChecklistID: ' + CONVERT(varchar,@w_ChecklistID)
        END
        
        SET @w_ChecklistIDNote = 'Task ' + @w_ChecklistIDNote + ' marked as Not Applicable because the vehicle was voided.'
        
        -- Mark Task as not applicable due to voiding
        EXEC uspWorkflowCompleteTask @ChecklistID = @w_ChecklistID,
                                     @NotApplicableFlag = 1,
                                     @NoteRequiredFlag = 1,
                                     @Note = @w_ChecklistIDNote,
                                     @NoteTypeID = @NoteTypeIDClaim,
                                     @UserID = @UserID,
                                     @SysLastUpdatedDate = @w_ChecklistLastUpdatedDate,
                                     @ApplicationCD = @ApplicationCD
        IF @@ERROR <> 0
        BEGIN
          RAISERROR('104|%s|%s',16,1,@ProcName,'uspWorkflowCompleteTask')
          ROLLBACK TRANSACTION
          RETURN
        END
        
        FETCH NEXT FROM csrTasks INTO @w_ChecklistID, @w_ChecklistLastUpdatedDate, @w_ChecklistIDNote

        IF @@ERROR <> 0
        BEGIN
          RAISERROR('99|%s',16,1)
          ROLLBACK TRANSACTION
          RETURN
        END
                                          
      END
      
      CLOSE csrTasks
      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1)
        ROLLBACK TRANSACTION
        RETURN
      END

      DEALLOCATE csrTasks        
      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1)
        ROLLBACK TRANSACTION
        RETURN
      END

      -- Now that tasks for service channel are marked NA.  Void the Service Channel
      SET @w_EventDesc = 'Vehicle Service Channel Voided'
      EXEC uspWorkflowNotifyEvent @EventID = @EventIDVoidServiceChannel,
                                  @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                  @Description = @w_EventDesc,
                                  @UserID = @UserID,
                                  @ConditionValue = @w_ServiceChannelCD
                                        
      IF @@ERROR <> 0
      BEGIN
          -- Error notifying APD of event

          RAISERROR  ('107|%s|%s', 16, 1, @ProcName, 'Vehicle Service Channel Voided')
          ROLLBACK TRANSACTION
          RETURN
      END
      
      -- Cancel any reinspection rquested for service channel
      IF EXISTS (SELECT cascd.DocumentID
                 FROM dbo.utb_claim_aspect_service_channel_document cascd
                 INNER JOIN utb_document d ON (cascd.DocumentID = d.DocumentID)
                 WHERE cascd.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
                   AND d.ReinspectionRequestFlag = 1)
      BEGIN
        UPDATE dbo.utb_document
        SET ReinspectionRequestFlag = 0
        WHERE DocumentID in (SELECT cascd.DocumentID
                             FROM dbo.utb_claim_aspect_service_channel_document cascd
                             INNER JOIN utb_document d ON (cascd.DocumentID = d.DocumentID)
                             WHERE cascd.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
                               AND d.ReinspectionRequestFlag = 1)
        IF @@ERROR <> 0
        BEGIN
            -- Update failure
            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        SELECT @EventIDReICancelled = EventID,
               @EventNameReICancelled = Name
          FROM dbo.utb_event
          WHERE Name = 'Reinspection Request Cancelled'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        -- Now throw the Reinspection request cancelled event
        EXEC uspWorkflowNotifyEvent @EventID = @EventIDReICancelled,
                                    @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                    @Description = @EventNameReICancelled,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- Error notifying APD of event

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventNameReICancelled)
            ROLLBACK TRANSACTION
            RETURN
        END
        
      END                                       
      
      FETCH NEXT FROM csrServiceChannels INTO @w_ClaimAspectServiceChannelID, @w_ServiceChannelCD

      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1)
        ROLLBACK TRANSACTION
        RETURN
      END
    END                                                                                            

    -- All Service Channels for vehicle should now be voided -- verify this
    IF @debug = 1
    BEGIN
    
     SELECT s.Name, cas.*
     FROM dbo.utb_claim_aspect_status cas inner join dbo.utb_status s on (cas.statusid = s.statusid)
     WHERE cas.ClaimAspectID = @ClaimAspectID
       AND cas.StatusTypeCD = 'SC'
    END                              
    IF EXISTS (SELECT StatusID
               FROM dbo.utb_claim_aspect_status
               WHERE ClaimAspectID = @ClaimAspectID
                 AND StatusTypeCD = 'SC'
                 AND StatusID NOT IN (SELECT StatusID 
                                      FROM dbo.utb_status 
                                      WHERE StatusTypeCD = 'SC' 
                                        AND Name = 'Voided'))
    BEGIN
      -- Seems a service channel for some reason got missed
      RAISERROR('101|%s|APD System was unable to void all service channels.',16,1,@ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    -- All service channels are voided -- throw vehicle void event
    
    SET @w_EventDesc = 'Vehicle Voided'
    EXEC uspWorkflowNotifyEvent @EventID = @EventIDVoidVehicle,
                                           @ClaimAspectID = @ClaimAspectID,
                                           @Description = @w_EventDesc,
                                           @UserID = @UserID                                  

                                      
    IF @@ERROR <> 0
    BEGIN
        -- Error notifying APD of event

        RAISERROR  ('107|%s|%s', 16, 1, @ProcName, 'Vehicle Voided')
        ROLLBACK TRANSACTION
        RETURN
    END
    
    IF dbo.ufnUtilityWillCloseClaim(@ClaimAspectID) = 1
    BEGIN
      -- Claim is going to close
      
      EXEC uspWorkflowNotifyEvent @EventID = @EventIDCloseClaim,
                                  @ClaimAspectID = @ClaimAspectIDClaim,
                                  @Description = 'Claim Closed',
                                  @UserID = @UserID
                                  
      IF @@ERROR <> 0
      BEGIN
          -- Error notifying APD of event

          RAISERROR  ('107|%s|%s', 16, 1, @ProcName, 'Claim Closed')
          ROLLBACK TRANSACTION
          RETURN
      END
    END 
    
    
    COMMIT TRANSACTION WorkflowVoidEntity1  
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspWorkflowVoidEntity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowVoidEntity TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
