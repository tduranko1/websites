-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetEventLog' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetEventLog 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetEventLog
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the events from the utb_apd_event_log table
*
* PARAMETERS:  
*				NONE
*				
* RESULT SET:
*				Integer Value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetEventLog
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT TOP 50
		EventID
		, EventType
		, EventStatus
		, EventDescription
		, ISNULL(EventDetailedDescription,'') EventDetailedDescription
		, ISNULL(EventXML,'') EventXML
		, SysLastUserID
		, SysLastUpdatedDate
	FROM
		utb_apd_event_log
	ORDER BY
		EventID DESC
		, SysLastUpdatedDate DESC

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetEventLog' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetEventLog TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetEventLog TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/