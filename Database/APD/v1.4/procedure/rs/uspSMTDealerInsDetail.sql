-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTDealerInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTDealerInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Dealer
*
* PARAMETERS:  
* (I) @Address1                           The Dealer's address line 1
* (I) @Address2                           The Dealer's address line 2
* (I) @AddressCity                        The Dealer's address city
* (I) @AddressCounty                      The Dealer's address county
* (I) @AddressState                       The Dealer's address state
* (I) @AddressZip                         The Dealer's address zip code
* (I) @FaxAreaCode                        The Dealer's fax area code
* (I) @FaxExchangeNumber                  The Dealer's fax exchange number
* (I) @FaxExtensionNumber                 The Dealer's fax extension number
* (I) @FaxUnitNumber                      The Dealer's fax unit number
* (I) @Name                               The Dealer's name
* (I) @PhoneAreaCode                      The Dealer's phone area code
* (I) @PhoneExchangeNumber                The Dealer's phone exchange number
* (I) @PhoneExtensionNumber               The Dealer's phone extension number
* (I) @PhoneUnitNumber                    The Dealer's phone unit number
* (I) @SysLastUserID                      The Dealer's updating user identity
*
* RESULT SET:
* DealerID                                The newly created Dealer unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTDealerInsDetail
(
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                      udt_std_int
    DECLARE @rowcount                   udt_std_int

    DECLARE @DealerID                   udt_std_id_big
    DECLARE @LogComment                 udt_std_desc_long
  
    DECLARE @ProcName                   varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTDealerInsDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Dealer Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Dealer Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END


    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
        -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    
    -- Begin Update(s)

    BEGIN TRANSACTION AdmDealerInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_dealer
   	(
        Address1,
        Address2,
        AddressCity,
        AddressCounty,
        AddressState,
        AddressZip,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        Name,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @Address1,
        @Address2,
        @AddressCity,
        @AddressCounty,
        @AddressState,
        @AddressZip,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @Name,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    SET @DealerID = SCOPE_IDENTITY()
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_dealer', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    -- Begin Audit Log code  -------------------------------------------------------------------------------------
    
       
    IF @Name IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer created: ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @Address1 IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Address 1 established: ' + @Address1
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @Address2 IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Address 2 established: ' + @Address2
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressZip IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Zip Code established: ' + @AddressZip
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCity IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer City established: ' + @AddressCity
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF @AddressState IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer State established: ' + @AddressState
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCounty IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer County established: ' + @AddressCounty
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Phone Area Code established: ' + @PhoneAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Phone Exchange Number established: ' + @PhoneExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
      
    
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Phone Unit Number established: ' + @PhoneUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Phone Extension Number established: ' + @PhoneExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Fax Area Code established: ' + @FaxAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Fax Exchange Number established: ' + @FaxExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Fax Unit Number established: ' + @FaxUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END
    END
    
    
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Dealer Fax Extension Number established: ' + @FaxExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    -- End of Audit Log code  ------------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmDealerInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @DealerID AS DealerID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTDealerInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
