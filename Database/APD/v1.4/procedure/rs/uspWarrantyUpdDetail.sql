-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWarrantyUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWarrantyUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the warranty details.
*
* PARAMETERS:  

* RESULT SET:
* An XML document containing the new SysLastUpdatedDate value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWarrantyUpdDetail
    @txtAssignmentID     udt_std_id_big,
    @txtRemarks          udt_std_desc_xlong = NULL,
    @txtDisposition      udt_std_cd = NULL,
    @txtRepairStartDate  udt_std_datetime = NULL,
    @txtRepairEndDate    udt_std_datetime = NULL,
    @txtUserID           udt_std_id
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables
    
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    DECLARE @AssignmentStatus       AS varchar(300)
    
    DECLARE @now                    AS datetime 

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWarrantyUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    IF @txtDisposition IS NULL SET @txtDisposition = ''
    
    -- Check to make sure a valid Assignment id was passed in

    IF  (@txtAssignmentID IS NULL) OR
        (@txtAssignmentID = 0) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_warranty_assignment WHERE AssignmentID = @txtAssignmentID))
    BEGIN
        -- Invalid Assignment ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @txtAssignmentID)
        RETURN
    END

    -- Check to make sure a valid User id was passed in

    IF  (@txtUserID IS NULL) OR
        (@txtUserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @txtUserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @txtUserID)
        RETURN
    END
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    
    SELECT @AssignmentStatus = isNull(CommunicationAddress, '')
    FROM utb_warranty_assignment
    WHERE AssignmentID = @txtAssignmentID

    declare @tmpAssignmentStatus table
    (
      strIndex       int,
      strValue       varchar(30)
    )
    
    
    INSERT INTO @tmpAssignmentStatus
    SELECT strIndex, Value FROM dbo.ufnUtilityParseString(@AssignmentStatus, ';', 0)
    
    IF EXISTS(SELECT strIndex FROM @tmpAssignmentStatus WHERE strIndex = 3)
    BEGIN
        UPDATE @tmpAssignmentStatus
        SET strValue = 'D=' + @txtDisposition
        WHERE strIndex = 3
        
        SET @AssignmentStatus = ''
        
        SELECT @AssignmentStatus = @AssignmentStatus + strValue + ';'
        FROM @tmpAssignmentStatus
        
        SELECT @AssignmentStatus = LEFT(@AssignmentStatus, LEN(@AssignmentStatus)-1)
        
        --PRINT '@AssignmentStatus=' + @AssignmentStatus
    END
    
    -- Begin Update

    BEGIN TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update claim aspect
    UPDATE utb_warranty_assignment
    SET AssignmentRemarks  = @txtRemarks,
        CommunicationAddress = @AssignmentStatus,
        WarrantyEndDate = @txtRepairEndDate,
        WarrantyStartDate = @txtRepairStartDate,
        SysLastUserID      = @txtUserID,
        SysLastUpdatedDate = @now
    WHERE AssignmentID = @txtAssignmentID
    
    SET @error = @@ERROR
    SET @rowCount = @@ROWCOUNT
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return new updated date time
    /*SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Warranty Assignment Level
            NULL AS [Assignment!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Warranty Assignment Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END*/

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWarrantyUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
