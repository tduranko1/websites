-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperDeskAuditPerf' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2OperDeskAuditPerf 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2OperDeskAuditPerf
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Compiles data displayed on the Desk Audit Performance
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The Insurance company id. NULL = all insurance companies
* (I) @RptMonth             The ending month of the report. The starting month will be calculated 5 months before
* (I) @RptYear              The ending year of the report.
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2OperDeskAuditPerf
    @InsuranceCompanyID int = NULL,
    @RptMonth           int = NULL,
    @RptYear            int = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportData TABLE
    (
        AssignedTo                      varchar(100) NOT NULL,
        OrderNo                         tinyInt      NOT NULL,
        InsuranceCompanyName            varchar(100) NULL,
        LYNXTelNumber                   varchar(15)  NULL,
        ReportToDate                    datetime     NOT NULL,
        TimePeriod                      datetime     NULL,
        TotalClaimClosed                int          NULL,
        TotalOriginalEstimateAmount     money        NULL,
        AvgOriginalEstimateAmount       money        NULL,
        TotalAuditedEstimateAmount      money        NULL,
        AvgAuditedEstimateAmount        money        NULL,
        AvgSavings                      money        NULL,
        AvgSavingsPct                   money        NULL,
        AgreedPct                       decimal(9,2) NULL
    )

    DECLARE @tmpLYNXHandlerDataRaw TABLE
    (
        LYNXHandlerID               bigint       NOT NULL,
        TimePeriodPositionID        int          NOT NULL,
        OriginalEstimateAmount      money        NULL,
        AuditedEstimateAmount       money        NULL,
        AuditedEstimateAgreedFlag   bit          NULL,
        MonthYear                   varchar(10)  NULL
    )

    DECLARE @tmpLYNXHandlerDataGrouped TABLE
    (
        LYNXHandlerID               bigint       NOT NULL,
        TimePeriodPositionID        int          NOT NULL,
        TotalClosed                 bigint       NULL,
        TotalOrgEstAmt              money        NULL,
        AvgOrgEstAmt                money        NULL,
        TotalAuditedEstAmt          money        NULL,
        AvgAuditedEstAmt            money        NULL,
        AvgDifference               money        NULL,
        AvgDiffPercent              money        NULL,
        AgreedPct                   decimal(9,2) NULL
    )

    DECLARE @tmpTimePeriods TABLE
    (
        PositionID                  int         NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        RptTimePeriod               varchar(8)  NOT NULL
    )

    DECLARE @ServiceChannelCDWork   udt_std_cd
    DECLARE @RptFromDate            DATETIME
    DECLARE @RptToDate              DATETIME
    DECLARE @frmDate                DATETIME
    DECLARE @toDate                 DATETIME
    DECLARE @tmpDate                DATETIME
    DECLARE @DateWorking            DATETIME
    DECLARE @LYNXHandlerWorking     bigint
    DECLARE @InsuranceCompanyName   varchar(100)
    DECLARE @LYNXPhone              varchar(12)
    DECLARE @InsuranceCompanyIDWorking  varchar(5)
    DECLARE @DateTotal              DATETIME

    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
    DECLARE @counter    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2OperDeskAuditPerf'

    SET @Debug = 0

    SET @ServiceChannelCDWork = 'DA'


    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Set a date in far future. This is used for the totals time period. Setting this date in
    --  far future will make this line item appear at the end of each assigned to and make up the
    --  the total line item.

    SET @DateTotal = convert(datetime, '1/1/3000')

    
    SET @InsuranceCompanyName = NULL
    SET @LYNXPhone = '239-337-4300'


    -- Validate Insurance Company ID
        
    IF (@InsuranceCompanyID IS NOT NULL) AND
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
        
        IF @InsuranceCompanyID IS NULL
        BEGIN
            SET @InsuranceCompanyIDWorking = '%'
        END
        ELSE
        BEGIN
            SET @InsuranceCompanyIDWorking = convert(varchar, @InsuranceCompanyID)
        END
    END          
   
    
    -- Validate the input dates
    -- If the month was not specified or invalid, assume the current month
    
    IF @RptMonth IS NULL OR @RptMonth < 1 OR @RptMonth > 12
    BEGIN
        SET @RptMonth = Month(@now)
    END
     
    -- If the year was not specified or greater than current year or less than 2002, assume current year
    IF @RptYear IS NULL OR @RptYear > YEAR(@now) OR @RptYear < 2002
    BEGIN
        SET @RptYear = Year(@now)
    END


    -- Calculate the from and to date for the report
    -- Set the to date for the report to the 1st of the month and year specifed.

    SET @RptToDate = convert(datetime, convert(varchar, @RptMonth) + '/1/' + convert(varchar, @RptYear))

    -- Set the from date for the report 5 months prior (includes current month)
    SET @RptFromDate = DATEADD(mm, -4, @RptToDate)
    
    -- Calculate the last day of the to date
    
    SET @tmpDate = DATEADD(mm, 1, @RptToDate)
    SET @RptToDate = DATEADD(dd, -1, @tmpDate)
    
    
    IF @Debug = 1
    BEGIN
        PRINT '@RptMonth: ' + CONVERT(varchar, @RptMonth)
        PRINT '@RptYear: ' + CONVERT(varchar, @RptYear)
        PRINT '@InsuranceCompanyID: ' + CONVERT(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyIDWorking: ' + @InsuranceCompanyIDWorking
        PRINT '@RptFromDate: ' + CONVERT(varchar, @RptFromDate, 101)
        PRINT '@RptToDate: ' + CONVERT(varchar, @RptToDate, 101)
    END

    -- Compile time periods
    SET @DateWorking = @RptFromDate
    SET @counter = 1
    
    WHILE (@DateWorking <= @RptToDate)
    BEGIN    
        INSERT INTO @tmpTimePeriods
        SELECT @counter,
               MONTH(@DateWorking),
               YEAR(@DateWorking),
               CONVERT(VARCHAR, MONTH(@DateWorking)) + '/' + CONVERT(VARCHAR, YEAR(@DateWorking)),
               CASE
                    WHEN Month(@DateWorking) < 10 THEN '0' + CONVERT(VARCHAR, MONTH(@DateWorking))
                    ELSE CONVERT(VARCHAR, MONTH(@DateWorking))
               END + '/' + CONVERT(VARCHAR, YEAR(@DateWorking))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @DateWorking = DATEADD(mm, 1, @DateWorking)
        SET @counter = @counter + 1
    END
    
    IF @Debug = 1
    BEGIN
        PRINT '@tmpTimePeriods Data:'
        select * from @tmpTimePeriods
    END


    -- Select Raw subset of data
    
    INSERT INTO @tmpLYNXHandlerDataRaw
        SELECT (case when fc.LynxHandlerAnalystID is null then fc.LynxHandlerSupportID else fc.LynxHandlerAnalystID end),
               tmpTP.PositionID,
               fc.OriginalEstimateGrossAmt,
               fc.AuditedEstimatewoBettAmt,
               fc.AuditedEstimateAgreedFlag,
               convert(varchar, dt.MonthOfYear) + '/' + convert(varchar, dt.YearValue)
            FROM dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            LEFT JOIN dbo.utb_dtwh_dim_lynx_handler dlh ON (fc.LynxHandlerAnalystID = dlh.LynxHandlerID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              AND convert(varchar(5), dc.InsuranceCompanyID) LIKE @InsuranceCompanyIDWorking
              AND fc.DemoFlag = 0 -- exclude demo claims
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( SELECT DispositionTypeID from utb_dtwh_dim_disposition_type WHERE DispositionTypeCD IN ('CA','VD'))
        ORDER BY fc.LynxHandlerAnalystID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataRaw', 16, 1, @ProcName)
        RETURN
    END
    

    -- Get Aggregates for the detail lines
    
    INSERT INTO @tmpLYNXHandlerDataGrouped
    SELECT tmpRaw.LynxHandlerID,
           tmpTP.PositionID,
           COUNT(*),
           IsNull(SUM(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(AVG(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(Sum(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),           
           IsNull(Avg(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),
           IsNull(Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                             WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                             ELSE tmpRaw.AuditedEstimateAmount
                                                           END), 0),
           IsNull((Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                              WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                              ELSE tmpRaw.AuditedEstimateAmount
                                                            END)) / Avg(tmpRaw.OriginalEstimateAmount) * 100, 0),
           SUM(CASE 
                  WHEN AuditedEstimateAgreedFlag = 1 THEN 1
                  ELSE 0
               END) * 100.00/COUNT(*)
    FROM @tmpLYNXHandlerDataRaw tmpRaw
    LEFT JOIN @tmpTimePeriods tmpTP ON (tmpRaw.TimePeriodPositionID = tmpTP.PositionID)
    GROUP BY tmpRaw.LynxHandlerID, tmpTP.PositionID, tmpTP.MonthValue, tmpTP.YearValue

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataGrouped', 16, 1, @ProcName)
        RETURN
    END


    -- Populate any holes with default data
    
    INSERT INTO @tmpLYNXHandlerDataGrouped
    SELECT tmpLHG.LYNXHandlerID,
           tmpTP.PositionID,
           NULL, NULL, NULL, NULL, NULL,
           NULL, NULL, NULL
    FROM (SELECT DISTINCT LYNXHandlerID FROM @tmpLYNXHandlerDataGrouped) tmpLHG, @tmpTimePeriods tmpTP
    WHERE Convert(varchar(10), tmpLHG.LYNXHandlerID) + '-' + Convert(varchar(10), tmpTP.PositionID) NOT IN
            (SELECT Convert(varchar(10), LYNXHandlerID) + '-' + Convert(varchar(10), TimePeriodPositionID) FROM @tmpLYNXHandlerDataGrouped)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataGrouped', 16, 1, @ProcName)
        RETURN
    END


    -- Now Get Totals for each handler

    INSERT INTO @tmpLYNXHandlerDataGrouped
    SELECT tmpRaw.LynxHandlerID,
           0,
           COUNT(*),
           IsNull(SUM(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(AVG(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(Sum(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),           
           IsNull(Avg(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),
           IsNull(Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                             WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                             ELSE tmpRaw.AuditedEstimateAmount
                                                           END), 0),
           IsNull((Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                              WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                              ELSE tmpRaw.AuditedEstimateAmount
                                                            END)) / Avg(tmpRaw.OriginalEstimateAmount) * 100, 0),
           SUM(CASE 
                  WHEN tmpRaw.AuditedEstimateAgreedFlag = 1 THEN 1
                  ELSE 0
               END) * 100.00/COUNT(*)
    FROM @tmpLYNXHandlerDataRaw tmpRaw
    GROUP BY tmpRaw.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataGrouped', 16, 1, @ProcName)
        RETURN
    END
    

    -- Get Grand Totals 

    INSERT INTO @tmpLYNXHandlerDataGrouped
    SELECT 0,
           0,
           COUNT(*),
           IsNull(SUM(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(AVG(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(Sum(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),           
           IsNull(Avg(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),
           IsNull(Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                             WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                             ELSE tmpRaw.AuditedEstimateAmount
                            END), 0),
           IsNull((Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                              WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                              ELSE tmpRaw.AuditedEstimateAmount
                                                            END)) / Avg(tmpRaw.OriginalEstimateAmount) * 100, 0),
           SUM(CASE 
                  WHEN tmpRaw.AuditedEstimateAgreedFlag = 1 THEN 1
                  ELSE 0
               END) * 100.00/COUNT(*)
    FROM @tmpLYNXHandlerDataRaw tmpRaw

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataGrouped', 16, 1, @ProcName)
        RETURN
    END

    
    -- Now Get Totals for each time period

    INSERT INTO @tmpLYNXHandlerDataGrouped
    SELECT 0,
           tmpTP.PositionID,
           COUNT(*),
           IsNull(SUM(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(AVG(tmpRaw.OriginalEstimateAmount), 0),
           IsNull(Sum(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),           
           IsNull(Avg(CASE
                        WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                        ELSE tmpRaw.AuditedEstimateAmount
                      END), 0),
           IsNull(Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                             WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                             ELSE tmpRaw.AuditedEstimateAmount
                                                           END), 0),
           IsNull((Avg(tmpRaw.OriginalEstimateAmount) - Avg(CASE
                                                              WHEN tmpRaw.AuditedEstimateAmount > tmpRaw.OriginalEstimateAmount THEN tmpRaw.OriginalEstimateAmount
                                                              ELSE tmpRaw.AuditedEstimateAmount
                                                            END)) / Avg(tmpRaw.OriginalEstimateAmount) * 100, 0),
           SUM(CASE 
                  WHEN tmpRaw.AuditedEstimateAgreedFlag = 1 THEN 1
                  ELSE 0
               END) * 100.00/COUNT(*)
    FROM @tmpLYNXHandlerDataRaw tmpRaw
    LEFT JOIN @tmpTimePeriods tmpTP ON (tmpRaw.TimePeriodPositionID = tmpTP.PositionID)
    GROUP BY tmpTP.PositionID, tmpTP.MonthValue, tmpTP.YearValue

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpLYNXHandlerDataGrouped', 16, 1, @ProcName)
        RETURN
    END    


    IF @Debug = 1
    BEGIN
        PRINT '@tmpLYNXHandlerDataGrouped Data:'
        select * from @tmpLYNXHandlerDataGrouped
    END


    -- Compile the data for return
    
    -- The first line contains the report information.
    -- This will be helpful when the above data collection was empty and
    -- report will have the insurance company, telephone info etc.
    INSERT INTO @tmpReportData
    SELECT '',
           0,
           @InsuranceCompanyName,
           @LYNXPhone,
           @RptToDate,
           NULL, NULL, NULL, NULL, NULL,
           NULL, NULL, NULL, NULL
           

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName)
        RETURN
    END


    -- the actual report data
    INSERT INTO @tmpReportData
    SELECT  CASE
              WHEN LYNXHandlerID = 0 THEN 'GRAND TOTALS'
              ELSE (SELECT  dlh.UserNameFirst + ' ' + dlh.UserNameLast
                      FROM  dbo.utb_dtwh_dim_lynx_handler dlh
                      WHERE LynxHandlerID = t.LynxHandlerID)
            END,
            CASE
              WHEN LYNXHandlerID = 0 THEN 2
              ELSE 1
            END,
            @InsuranceCompanyName,
            NULL,
            @RptToDate,
            CASE 
              WHEN TimePeriodPositionID = 0 THEN @DateTotal
              ELSE convert(datetime, convert(varchar, tmpTP.MonthValue) + '/1/' + convert(varchar, tmpTP.YearValue))
            END,
            t.TotalClosed,
            t.TotalOrgEstAmt,
            t.AvgOrgEstAmt,
            t.TotalAuditedEstAmt,
            t.AvgAuditedEstAmt,
            t.AvgDifference,
            t.AvgDiffPercent,
            t.AgreedPct
    FROM @tmpLYNXHandlerDataGrouped t
    LEFT JOIN @tmpTimePeriods tmpTP ON (t.TimePeriodPositionID = tmpTP.PositionID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName)
        RETURN
    END


    -- Output the records
    
    SELECT *,  
      @DataWareHouseDate as DataWareDate 
      FROM @tmpReportData
      ORDER BY OrderNo, AssignedTo, TimePeriod
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END       
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperDeskAuditPerf' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2OperDeskAuditPerf TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/