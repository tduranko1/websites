-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmCFShopMigrate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmCFShopMigrate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmCFShopMigrate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Retrieves the CF shops.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* None.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspAdmCFShopMigrate
AS
BEGIN
    
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT
    DECLARE @ModifiedDateTime AS DATETIME

    SET @ModifiedDateTime = CURRENT_TIMESTAMP
    

    -- Create temporary storage to hold cf data
    
    CREATE TABLE #CFData
    (
        Address1 udt_addr_line_1 NULL,
        Address2 udt_addr_line_2 NULL,
        AddressCity udt_addr_city NULL,
        AddressState udt_addr_state NULL,
        AddressZip VARCHAR(500) NULL,
        CertifiedFirstId udt_std_desc_short NULL,
        EmailAddress VARCHAR(500) NULL,
        FaxAreaCode udt_ph_area_code NULL,
        FaxExchangeNumber udt_ph_exchange_number NULL,
        FaxUnitNumber udt_ph_unit_number NULL,
        FaxExtensionNumber udt_ph_extension_number NULL,
        Name VARCHAR(500) NULL,
        OperatingFridayEndTime udt_std_time NULL,
        OperatingFridayStartTime udt_std_time NULL,
        OperatingMondayEndTime udt_std_time NULL,
        OperatingMondayStartTime udt_std_time NULL,
        OperatingSaturdayEndTime udt_std_time NULL,
        OperatingSaturdayStartTime udt_std_time NULL,
        OperatingSundayEndTime udt_std_time NULL,
        OperatingSundayStartTime udt_std_time NULL,
        OperatingThursdayEndTime udt_std_time NULL,
        OperatingThursdayStartTime udt_std_time NULL,
        OperatingTuesdayEndTime udt_std_time NULL,
        OperatingTuesdayStartTime udt_std_time NULL,
        OperatingWednesdayEndTime udt_std_time NULL,
        OperatingWednesdayStartTime udt_std_time NULL,
        PhoneAreaCode udt_ph_area_code NULL,
        PhoneExchangeNumber udt_ph_exchange_number NULL,
        PhoneUnitNumber udt_ph_unit_number NULL,
        PhoneExtensionNumber udt_ph_extension_number NULL,
        PreferredEstimatePackageID udt_std_name NULL,
        SLPEmailAddress VARCHAR(500) NULL,
        SLPFaxAreaCode udt_ph_area_code NULL,
        SLPFaxExchangeNumber udt_ph_exchange_number NULL,
        SLPFaxExtensionNumber udt_ph_extension_number NULL,
        SLPFaxUnitNumber udt_ph_unit_number NULL,
        SLPName VARCHAR(500) NULL,
        SLPPhoneAreaCode udt_ph_area_code NULL,
        SLPPhoneExchangeNumber udt_ph_exchange_number NULL,
        SLPPhoneExtensionNumber udt_ph_extension_number NULL,
        SLPPhoneUnitNumber udt_ph_unit_number NULL,
        SLPPreferredContactMethodID udt_std_name NULL,
        SysLastUpdatedDate udt_sys_last_updated_date NULL,
        WebSiteAddress udt_web_address NULL,
        PPGTerritory udt_std_cd NULL,
        PPGRegion udt_std_cd NULL,
        PPGZone udt_std_cd NULL
    )

    SET @error = 0
    
    IF 0 = @error
    BEGIN
        DECLARE @update AS BIT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        SET @update = 0

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    

    IF 0 = @error
    BEGIN
        SELECT 'utb_shop_location CF Total', COUNT(*) FROM utb_shop_location WHERE CertifiedFirstFlag = 1

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
        
        
    IF 0 = @error
    BEGIN
        SELECT 'utb_shop_load CF Total', COUNT(*) FROM utb_shop_load WHERE CertifiedFirstFlag = 1

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    --
    --
    -- Script for updating APD CF shop data.
    --

    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        INSERT INTO #CFData SELECT * FROM CFShops
        
        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- 1) Shop Location: turn off CF Flag for all enabled records
    
    IF 0 = @error
    BEGIN
        UPDATE utb_shop_location
           SET CertifiedFirstFlag = 0,
           SysLastUserID = 0,
           SysLastUpdatedDate = @ModifiedDateTime
         WHERE CertifiedFirstFlag = 1
           AND EnabledFlag = 1
         
        -- Capture any error
        
        SELECT @error = @@ERROR
    END
           
           
    -- 2) Shop Load: turn off CF Flag for all records with a CF ID, and a SysLastUpdatedDate

    IF 0 = @error
    BEGIN
        UPDATE utb_shop_load
           SET CertifiedFirstFlag = 0
         WHERE CertifiedFirstId IS NOT NULL
           AND SysLastUpdatedDate IS NOT NULL
           AND EnabledFlag = 1

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

           
    -- 3) Shop Load: Delete all records with a CF ID, and a SysLastUpdatedDate = NULL

    IF 0 = @error
    BEGIN
        DELETE FROM dbo.utb_shop_load
         WHERE CertifiedFirstId IS NOT NULL
           AND SysLastUpdatedDate IS NULL

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- 4) Process CF list
	--     a) Shop Location: IF CFId = CF CFId, Enable CFFlag for this record

    
    IF 0 = @error
    BEGIN
        UPDATE dbo.utb_shop_location
           SET CertifiedFirstFlag = 1,
               SysLastUserID = 0,
               SysLastUpdatedDate = @ModifiedDateTime
          WHERE EnabledFlag = 1
            AND CertifiedFirstID IN (SELECT DISTINCT CertifiedFirstID FROM #CFData WHERE ASCII(Left(AddressZip, 1)) BETWEEN 48 AND 57)

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
       
    --     b) Shop Load: IF CFId = CF CFId, Enable CFFlag for this Record

    IF 0 = @error
    BEGIN
        UPDATE dbo.utb_shop_load
           SET CertifiedFirstFlag = 1
          WHERE CertifiedFirstID IN (SELECT DISTINCT CertifiedFirstID FROM #CFData WHERE ASCII(Left(AddressZip, 1)) BETWEEN 48 AND 57)
           AND EnabledFlag = 1

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
       
	--     c) Shop Load: IF CF CFId NOT FOUND, Add record
    
    
    IF 0 = @error
    BEGIN
        INSERT INTO dbo.utb_shop_load
                    (MergeUserID,
                    PreferredCommunicationMethodID,
                    PreferredEstimatePackageID,
                    ShopID,
                    ShopLocationID,
                    SLPPersonnelID,
                    SLPPersonnelTypeID,
                    SLPPreferredContactMethodID,
                    SPPersonnelID,
                    SPPersonnelTypeID,
                    SPPreferredContactMethodID,
                    Address1,
                    Address2,
                    AddressCity,
                    AddressCounty,
                    AddressState,
                    AddressZip,
                    Age,
                    AlignmentFourWheel,
                    AlignmentTwoWheel,
                    ApprovedFlag,
                    ASEFlag,
                    BusinessRegsFlag,
                    BusinessStabilityFlag,
                    BusinessTypeCD,
                    CaulkingSeamSealer,
                    CertifiedFirstFlag,
                    CertifiedFirstId,
                    ChipGuard,
                    CommercialLiabilityFlag,
                    CompetentEstimatorsFlag,
                    CoolantGreen,
                    CoolantRed,
                    CorrosionProtection,
                    CountyTaxPct,
                    CoverCar,
                    CustomaryHoursFlag,
                    DataEntryDate,
                    DigitalPhotosFlag,
                    DiscountDomestic,
                    DiscountImport,
                    DiscountPctSideBackGlass,
                    DiscountPctWindshield,
                    DrivingDirections,
                    EFTFlag,
                    ElectronicEstimatesFlag,
                    EmailAddress,
                    EmployeeEducationFlag,
                    EmployerLiabilityFlag,
                    EnabledFlag,
                    FaxAreaCode,
                    FaxExchangeNumber,
                    FaxExtensionNumber,
                    FaxUnitNumber,
                    FedTaxId,
                    FinalSubmitDate,
                    FinalSubmitFlag,
                    FlexAdditive,
                    FrameAnchoringPullingEquipFlag,
                    FrameDiagnosticEquipFlag,
                    GlassOutsourceServiceFee,
                    GlassReplacementChargeTypeCD,
                    GlassSubletServiceFee,
                    GlassSubletServicePct,
                    HazardousWaste,
                    HazMatFlag,
                    HourlyRateMechanical,
                    HourlyRateRefinishing,
                    HourlyRateSheetMetal,
                    HourlyRateUnibodyFrame,
                    HydraulicLiftFlag,
                    ICARFlag,
                    MaxWeeklyAssignments,
                    MechanicalRepairFlag,
                    MergeActionBillingCD,
                    MergeActionPersonnelCD,
                    MergeActionShopCD,
                    MergeActionShopLocationCD,
                    MergeActionShopLocationHoursCD,
                    MergeActionShopLocationOEMDiscountCD,
                    MergeActionShopLocationPersonnelCD,
                    MergeActionShopLocationPricingCD,
                    MergeActionShopLocationSurveyCD,
                    MergeActionShopPersonnelCD,
                    MergeDate,
                    MIGWeldersFlag,
                    MinimumWarrantyFlag,
                    MunicipalTaxPct,
                    Name,
                    NoFeloniesFlag,
                    OEMDiscounts,
                    OperatingFridayEndTime,
                    OperatingFridayStartTime,
                    OperatingMondayEndTime,
                    OperatingMondayStartTime,
                    OperatingSaturdayEndTime,
                    OperatingSaturdayStartTime,
                    OperatingSundayEndTime,
                    OperatingSundayStartTime,
                    OperatingThursdayEndTime,
                    OperatingThursdayStartTime,
                    OperatingTuesdayEndTime,
                    OperatingTuesdayStartTime,
                    OperatingWednesdayEndTime,
                    OperatingWednesdayStartTime,
                    OtherTaxPct,
                    PartsRecycledMarkupPct,
                    PhoneAreaCode,
                    PhoneExchangeNumber,
                    PhoneExtensionNumber,
                    PhoneUnitNumber,
                    PPGRegion,
                    PPGTerritory,
                    PPGZone,
                    PullSetUp,
                    PullSetUpMeasure,
                    QualificationsSubmitedDate,
                    ReceptionistFlag,
                    RefinishCapabilityFlag,
                    --RefinishSingleStageHourly,
                    --RefinishSingleStageMax,
                    --RefinishThreeStageCCMaxHrs,
                    --RefinishThreeStageHourly,
                    --RefinishThreeStageMax,
                    RefinishTrainedFlag,
                    RefinishTwoStageCCMaxHrs,
                    RefinishTwoStageHourly,
                    RefinishTwoStageMax,
                    RepairFacilityTypeCD,
                    R12EvacuateRecharge,
                    R12EvacuateRechargeFreon,
                    R134EvacuateRecharge,
                    R134EvacuateRechargeFreon,
                    SalesTaxPct,
                    SecureStorageFlag,
                    SiteFirstVisitDate,
                    SLPCellAreaCode,
                    SLPCellExchangeNumber,
                    SLPCellExtensionNumber,
                    SLPCellUnitNumber,
                    SLPEmailAddress,
                    SLPFaxAreaCode,
                    SLPFaxExchangeNumber,
                    SLPFaxExtensionNumber,
                    SLPFaxUnitNumber,
                    SLPGenderCD,
                    SLPName,
                    SLPPagerAreaCode,
                    SLPPagerExchangeNumber,
                    SLPPagerExtensionNumber,
                    SLPPagerUnitNumber,
                    SLPPhoneAreaCode,
                    SLPPhoneExchangeNumber,
                    SLPPhoneExtensionNumber,
                    SLPPhoneUnitNumber,
                    SLPShopManagerFlag,
                    SPCellAreaCode,
                    SPCellExchangeNumber,
                    SPCellExtensionNumber,
                    SPCellUnitNumber,
                    SPEmailAddress,
                    SPFaxAreaCode,
                    SPFaxExchangeNumber,
                    SPFaxExtensionNumber,
                    SPFaxUnitNumber,
                    SPGenderCD,
                    SPName,
                    SPPagerAreaCode,
                    SPPagerExchangeNumber,
                    SPPagerExtensionNumber,
                    SPPagerUnitNumber,
                    SPPhoneAreaCode,
                    SPPhoneExchangeNumber,
                    SPPhoneExtensionNumber,
                    SPPhoneUnitNumber,
                    SPShopManagerFlag,
                    StripePaintPerPanel,
                    StripePaintPerSide,
                    StripeTapePerPanel,
                    StripeTapePerSide,
                    TireMountBalance,
                    TowInChargeTypeCD,
                    TowInFlatFee,
                    TransferFlag,
                    Undercoat,
                    ValidEPALicenseFlag,
                    VANInetFlag,
                    WarrantyPeriodRefinishCD,
                    WarrantyPeriodWorkmanshipCD,
                    WebSiteAddress,
                    WorkersCompFlag,
                    SysLastUpdatedDate)

                  SELECT 	NULL, -- MergeUserID
                      sl.PreferredCommunicationMethodID,
                    CASE (RTRIM(LTRIM(cf.PreferredEstimatePackageID)))
                        WHEN 'ADP' THEN 1
                        WHEN 'CCC' THEN 2
                        WHEN 'Mitchell' THEN 4
                        ELSE sl.PreferredEstimatePackageID
                    END,
	                s.ShopId, -- 	        ShopID,
	                sl.ShopLocationID, -- 	        ShopLocationID,
	                slpp.PersonnelID, -- 	        SLPPersonnelID,
 	                slpp.PersonnelTypeID, -- 	        SLPPersonnelTypeID,
                    CASE (RTRIM(LTRIM(cf.SLPPreferredContactMethodID)))
                        WHEN 'EMail' THEN 2
                        WHEN 'Phone' THEN 6
                        ELSE slpp.PreferredContactMethodID
                    END, 
	                spp.PersonnelID, -- SPPersonnelID,
	                spp.PersonnelTypeID, -- SPPersonnelTypeID,
	                spp.PreferredContactMethodID, -- SPPreferredContactMethodID,
                    ISNULL(RTRIM(LTRIM(cf.Address1)), sl.Address1),
                    ISNULL(RTRIM(LTRIM(cf.Address2)), sl.Address2),
                    ISNULL(RTRIM(LTRIM(cf.AddressCity)), sl.AddressCity),
                    sl.AddressCounty,
                    ISNULL(RTRIM(LTRIM(cf.AddressState)), sl.AddressState),
                    CASE
                        WHEN LEFT(RTRIM(LTRIM(cf.AddressZip)), 5) IS NULL  THEN sl.AddressZip
                        WHEN LEN(RTRIM(LTRIM(cf.AddressZip))) < 5 THEN '0' + cf.AddressZip COLLATE database_default 
                        ELSE LEFT(RTRIM(LTRIM(cf.AddressZip)), 5)
                    END,
                    NULL, --Age
                    slp.AlignmentFourWheel,
                    slp.AlignmentTwoWheel,
                    0,  -- ApprovedFlag
	                NULL, -- ASEFlag,
	                NULL, -- BusinessRegsFlag,
	                NULL, -- BusinessStabilityFlag,
                    s.BusinessTypeCD,
                    slp.CaulkingSeamSealer,
                    1,  -- CertifiedFirstFlag
                    ISNULL(RTRIM(LTRIM(cf.CertifiedFirstID)), sl.CertifiedFirstID),
                    slp.ChipGuard,
	                NULL, -- CommercialLiabilityFlag,
	                NULL, -- CompetentEstimatorsFlag,
                    slp.CoolantGreen,
                    slp.CoolantRed,
                    slp.CorrosionProtection,
                    slp.CountyTaxPct,
                    slp.CoverCar,
                    NULL, -- CustomaryHoursFlag,
                    NULL, -- DataEntryDate,
                    NULL, -- DigitalPhotosFlag,
                    NULL, -- DiscountDomestic,
                    NULL, -- DiscountImport,
                    slp.DiscountPctSideBackGlass,
                    slp.DiscountPctWindshield,
                    NULL, -- DrivingDirections,
                    NULL, -- EFTFlag,
                    NULL, -- ElectronicEstimatesFlag,
                    ISNULL(LEFT(RTRIM(LTRIM(cf.EmailAddress)), 50), sl.EmailAddress),
                    NULL, -- EmployeeEducationFlag,
                    NULL, -- EmployerLiabilityFlag,
                    1,  -- EnabledFlag
                    ISNULL(RTRIM(LTRIM(cf.FAXAreaCode)), sl.FAXAreaCode),
                    ISNULL(LEFT(RTRIM(LTRIM(cf.FAXExchangeNumber)), 3), sl.FAXExchangeNumber),
                    sl.FAXExtensionNumber,
                    ISNULL(dbo.ufnUtilityPadChars(RTRIM(LTRIM(cf.FAXUnitNumber)), 4, '0', 1 ), sl.FAXUnitNumber),  -- FAXUnitNumber
                    s.FedTaxId,
                    NULL, -- FinalSubmitDate,
                    0,  -- FinalSubmitFlag
                    slp.FlexAdditive,
                    NULL, -- FrameAnchoringPullingEquipFlag,
                    NULL, -- FrameDiagnosticEquipFlag,
                    slp.GlassOutsourceServiceFee,
                    slp.GlassReplacementChargeTypeCD,
                    slp.GlassSubletServiceFee,
                    slp.GlassSubletServicePct,
                    slp.HazardousWaste,
                    NULL, -- HazMatFlag,
                    slp.HourlyRateMechanical,
                    slp.HourlyRateRefinishing,
                    slp.HourlyRateSheetMetal,
                    slp.HourlyRateUnibodyFrame,
                    NULL, -- HydraulicLiftFlag,
                    NULL, -- ICARFlag,
                    sl.MaxWeeklyAssignments,
                    NULL, -- MechanicalRepairFlag,
                    NULL, -- MergeActionBillingCD,
                    NULL, -- MergeActionPersonnelCD,
                    NULL, -- MergeActionShopCD,
                    NULL, -- MergeActionShopLocationCD,
                    NULL, -- MergeActionShopLocationHoursCD,
                    NULL, -- MergeActionShopLocationOEMDiscountCD,
                    NULL, -- MergeActionShopLocationPersonnelCD,
                    NULL, -- MergeActionShopLocationPricingCD,
                    NULL, -- MergeActionShopLocationSurveyCD,
                    NULL, -- MergeActionShopPersonnelCD,
                    NULL, -- MergeDate,
                    NULL, -- MIGWeldersFlag,
                    NULL, -- MinimumWarrantyFlag,
                    slp.MunicipalTaxPct,
                    LEFT(RTRIM(LTRIM(cf.Name)), 50),
                    NULL, -- NoFeloniesFlag,
                    NULL, -- OEMDiscounts,
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingFridayEndTime)), 4, '0', 1 ), slh.OperatingFridayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingFridayStartTime)) , 4, '0', 1 ),  slh.OperatingFridayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingMondayEndTime)) , 4, '0', 1 ),  slh.OperatingMondayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingMondayStartTime)) , 4, '0', 1 ),  slh.OperatingMondayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingSaturdayEndTime)) , 4, '0', 1 ),  slh.OperatingSaturdayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingSaturdayStartTime)) , 4, '0', 1 ),  slh.OperatingSaturdayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingSundayEndTime)) , 4, '0', 1 ),  slh.OperatingSundayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingSundayStartTime)) , 4, '0', 1 ),  slh.OperatingSundayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingThursdayEndTime)) , 4, '0', 1 ),  slh.OperatingThursdayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingThursdayStartTime)) , 4, '0', 1 ),  slh.OperatingThursdayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingTuesdayEndTime)) , 4, '0', 1 ),  slh.OperatingTuesdayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingTuesdayStartTime)) , 4, '0', 1 ),  slh.OperatingTuesdayStartTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingWednesdayEndTime)) , 4, '0', 1 ),  slh.OperatingWednesdayEndTime), '0000'),
                    NULLIF( ISNULL(dbo.ufnUtilityPadChars( RTRIM(LTRIM(cf.OperatingWednesdayStartTime)) , 4, '0', 1 ),  slh.OperatingWednesdayStartTime), '0000'),
                    slp.OtherTaxPct,
                    slp.PartsRecycledMarkupPct,
                    ISNULL(RTRIM(LTRIM(cf.PhoneAreaCode)), sl.PhoneAreaCode),
                    ISNULL(LEFT(RTRIM(LTRIM(cf.PhoneExchangeNumber)), 3), sl.PhoneExchangeNumber),
                    sl.PhoneExtensionNumber,
                    ISNULL(dbo.ufnUtilityPadChars(RTRIM(LTRIM(cf.PhoneUnitNumber)), 4, '0', 1 ), sl.PhoneUnitNumber), --PhoneUnitNumber
                    RTRIM(LTRIM(cf.PPGRegion)),
                    RTRIM(LTRIM(cf.PPGTerritory)),
                    RTRIM(LTRIM(cf.PPGZone)),
                    slp.PullSetUp,
                    slp.PullSetUpMeasure,
                    NULL, -- QualificationsSubmitedDate,
                    NULL, -- ReceptionistFlag,
                    NULL, -- RefinishCapabilityFlag,
                    --slp.RefinishSingleStageHourly,
                    --slp.RefinishSingleStageMax,
                    --slp.RefinishThreeStageCCMaxHrs,
                    --slp.RefinishThreeStageHourly,
                    --slp.RefinishThreeStageMax,
                    NULL, -- RefinishTrainedFlag,
                    slp.RefinishTwoStageCCMaxHrs,
                    slp.RefinishTwoStageHourly,
                    slp.RefinishTwoStageMax,
                    NULL, -- RepairFacilityTypeCD
                    slp.R12EvacuateRecharge,
                    slp.R12EvacuateRechargeFreon,
                    slp.R134EvacuateRecharge,
                    slp.R134EvacuateRechargeFreon,
                    slp.SalesTaxPct,
                    NULL, -- SecureStorageFlag,
                    NULL, -- SiteFirstVisitDate,
                    slpp.CellAreaCode, -- SLPCellAreaCode,
                    slpp.CellExchangeNumber, -- SLPCellExchangeNumber,
                    slpp.CellExtensionNumber, -- SLPCellExtensionNumber,
                    slpp.CellUnitNumber, -- SLPCellUnitNumber,
                    ISNULL(LEFT(RTRIM(LTRIM(cf.SLPEmailAddress)), 50), slpp.EmailAddress),
                    ISNULL(RTRIM(LTRIM(cf.SLPFAXAreaCode)), slpp.FAXAreaCode), 
                    ISNULL(LEFT(RTRIM(LTRIM(cf.SLPFAXExchangeNumber)), 3), slpp.FAXExchangeNumber),
                    slpp.FAXExtensionNumber, -- SLPFAXExtensionNumber
                    ISNULL(dbo.ufnUtilityPadChars(RTRIM(LTRIM(cf.SLPFaxExtensionNumber)), 4, '0', 1 ), slpp.FAXUnitNumber), -- SLPFAXUnitNumber
                    slpp.GenderCD, -- SLPGenderCD,
                    ISNULL(RTRIM(LTRIM(cf.SLPName)), slpp.Name),
                    slpp.PagerAreaCode, -- SLPPagerAreaCode,
                    slpp.PagerExchangeNumber, -- SLPPagerExchangeNumber,
                    slpp.PagerExtensionNumber, -- SLPPagerExtensionNumber,
                    slpp.PagerUnitNumber, -- SLPPagerUnitNumber,
                    ISNULL(RTRIM(LTRIM(cf.SLPPhoneAreaCode)), slpp.PhoneAreaCode), 
                    ISNULL(LEFT( RTRIM(LTRIM(cf.SLPPhoneExchangeNumber)), 3), slpp.PhoneExchangeNumber),
                    slpp.PhoneExtensionNumber, -- SLPPhoneExtensionNumber
                    ISNULL(dbo.ufnUtilityPadChars(RTRIM(LTRIM(cf.SLPPhoneExtensionNumber)), 4, '0', 1 ), slpp.PhoneUnitNumber),  -- SLPPhoneUnitNumber
                    slpp.ShopManagerFlag, -- SLPShopManagerFlag,
                    spp.CellAreaCode, -- SPCellAreaCode,
                    spp.CellExchangeNumber, -- SPCellExchangeNumber,
                    spp.CellExtensionNumber, -- SPCellExtensionNumber,
                    spp.CellUnitNumber, -- SPCellUnitNumber,
                    spp.EmailAddress, -- SPEmailAddress,
                    spp.FAXAreaCode, -- SPFaxAreaCode,
                    spp.FAXExchangeNumber, -- SPFaxExchangeNumber,
                    spp.FAXExtensionNumber, -- SPFaxExtensionNumber,
                    spp.FAXUnitNumber, -- SPFaxUnitNumber,
                    spp.GenderCD, -- SPGenderCD,
                    spp.Name, -- SPName,
                    spp.PagerAreaCode, -- SPPagerAreaCode,
                    spp.PagerExchangeNumber, -- SPPagerExchangeNumber,
                    spp.PagerExtensionNumber, -- SPPagerExtensionNumber,
                    spp.PagerUnitNumber, -- SPPagerUnitNumber,
                    spp.PhoneAreaCode, -- SPPhoneAreaCode,
                    spp.PhoneExchangeNumber, -- SPPhoneExchangeNumber,
                    spp.PhoneExtensionNumber, -- SPPhoneExtensionNumber,
                    spp.PhoneUnitNumber, -- SPPhoneUnitNumber,
                    spp.ShopManagerFlag, -- SPShopManagerFlag,
                    slp.StripePaintPerPanel,
                    slp.StripePaintPerSide,
                    slp.StripeTapePerPanel,
                    slp.StripeTapePerSide,
                    slp.TireMountBalance,
                    slp.TowInChargeTypeCD,
                    slp.TowInFlatFee,
                    0, -- TransferFlag
                    slp.Undercoat,
                    NULL, -- ValidEPALicenseFlag,
                    NULL, -- VANInetFlag,
                    CASE
                        WHEN sl.WarrantyPeriodRefinishCD IN ('0', '1') THEN NULL
                        ELSE sl.WarrantyPeriodRefinishCD
                    END,
                    CASE
                        WHEN sl.WarrantyPeriodWorkmanshipCD IN ('0', '1') THEN NULL
                        ELSE sl.WarrantyPeriodWorkmanshipCD
                    END,
                    ISNULL(RTRIM(LTRIM(cf.WebSiteAddress)), sl.WebSiteAddress),
                    NULL, -- WorkersCompFlag,
                    NULL -- SysLastUpdatedDate
        FROM #CFData cf
        LEFT JOIN dbo.utb_shop_location sl ON (CAST(cf.CertifiedFirstId AS VARCHAR(50)) = sl.CertifiedFirstId AND sl.enabledflag = 1)
        LEFT JOIN dbo.utb_shop_location_pricing slp ON (sl.ShopLocationId = slp.ShopLocationId)
        LEFT JOIN dbo.utb_shop_location_hours slh ON (sl.ShopLocationId = slh.ShopLocationId)
        LEFT JOIN dbo.utb_shop s ON (sl.ShopId = s.ShopId AND s.enabledflag = 1)
        LEFT JOIN (SELECT slp.ShopLocationID, p.*
                     FROM (SELECT outer_slp.*
                             FROM dbo.utb_shop_location_personnel outer_slp
                             JOIN (SELECT ShopLocationID, COUNT(*) sl_count
                                     FROM utb_shop_location_personnel 
                                    GROUP BY ShopLocationID 
                                   HAVING COUNT(*) = 1
                                   ) inner_slp ON (outer_slp.ShopLocationID = inner_slp.ShopLocationID)
                           ) slp 
                     JOIN dbo.utb_personnel p 
                       ON (slp.PersonnelID = p.PersonnelID)
                   ) slpp ON (sl.ShopLocationId = slpp.ShopLocationId)
        LEFT JOIN (SELECT sp.ShopID, p.* 
                     FROM (SELECT outer_sp.*
                             FROM dbo.utb_shop_personnel outer_sp
                             JOIN (SELECT ShopID, COUNT(*) s_count
                                     FROM utb_shop_personnel 
                                    GROUP BY ShopID 
                                   HAVING COUNT(*) = 1
                                   ) inner_sp ON (outer_sp.ShopID = inner_sp.ShopID)
                           ) sp 
                     JOIN dbo.utb_personnel p 
                       ON (sp.PersonnelID = p.PersonnelID)
                   ) spp ON (s.ShopId = spp.ShopId)
        WHERE ASCII(Left(cf.AddressZip, 1)) BETWEEN 48 AND 57    -- Only US Shops, No Canadian           
         AND cf.CertifiedFirstID NOT IN (SELECT CertifiedFirstID FROM utb_shop_load)
    
        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        SELECT 'utb_shop_location CF Total', COUNT(*) FROM utb_shop_location WHERE CertifiedFirstFlag = 1

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        SELECT 'utb_shop_load CF Total', COUNT(*) FROM utb_shop_load WHERE CertifiedFirstFlag = 1
        
        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmCFShopMigrate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmCFShopMigrate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/