-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLFollowupScript' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLFollowupScript 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLFollowupScript
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL details
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID                        ClaimAspectServiceChannelID
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLFollowupScript
AS
BEGIN
   SET NOCOUNT ON

   DECLARE @now as datetime
   DECLARE @ProcName as varchar(50)
   DECLARE @OpenClaimCount as int
   DECLARE @TaskIDVOFollowup as int
   DECLARE @TaskIDLHLoGFollowup as int
   DECLARE @TaskIDLHPOFollowup as int

   DECLARE @TaskIDVOFollowupAlarm as datetime
   DECLARE @TaskIDLHLoGFollowupAlarm as datetime
   DECLARE @TaskIDLHPOFollowupAlarm as datetime

   DECLARE @DocumentTypeIDLoG as int
   DECLARE @DocumentTypeIDLHPoA as int
   DECLARE @DocumentTypeIDLHTitle as int
   DECLARE @DocumentTypeIDVOPoA as int
   DECLARE @DocumentTypeIDVOTitle as int

   DECLARE @VOFollowUpCTDays as int
   DECLARE @LHLoGFollowupCTDays as int
   DECLARE @LHPOFollowupCTDays as int

   DECLARE @ClaimAspectServiceChannelID as bigint
   DECLARE @LienHolderExistsFlag as bit
   DECLARE @DaysSinceRecentContact as int
   DECLARE @DaysSinceTLCreated as int

   DECLARE @tmpOpenTLClaims TABLE (
      ClaimAspectServiceChannelID   bigint NOT NULL,
      LienHolderExistsFlag          bit    NOT NULL,
      RecentCOntactDate             datetime   NULL,
      LHInitialContactDate          datetime   NULL,
      LHRecentContactDate           datetime   NULL,
      VOInitialContactDate          datetime   NULL,
      VORecentContactDate           datetime   NULL,
      DaysSinceRecentContact        int        NULL,
      DaysSinceTLCreated            int        NULL,
      DocumentIDLoG                 bigint     NULL,
      DocumentIDLHPoA               bigint     NULL,
      DocumentIDLHTitle             bigint     NULL,
      DocumentIDVOPoA               bigint     NULL,
      DocumentIDVOTitle             bigint     NULL,
      ValidatedFlag                 bit        NOT NULL
   )


   Set @now = CURRENT_TIMESTAMP
   Set @ProcName = 'Test'

   -- We will have a day less from the original spec threshold since this will be a nightly job.
   
   -- production values
   SET @VOFollowUpCTDays = 4
   SET @LHLoGFollowupCTDays = 2
   SET @LHPOFollowupCTDays = 7
   

   
   -- staging values
   /*
   SET @VOFollowUpCTDays = 0
   SET @LHLoGFollowupCTDays = 0
   SET @LHPOFollowupCTDays = 0
   */
   

   SELECT @TaskIDVOFollowup = TaskID,
          @TaskIDVOFollowupAlarm = Dateadd(minute, EscalationMinutes, @now)
   FROM dbo.utb_task
   WHERE Name = 'Vehicle Owner Docs Follow Up Required'

   SELECT @TaskIDLHLoGFollowup = TaskID,
          @TaskIDLHLoGFollowupAlarm = Dateadd(minute, EscalationMinutes, @now)
   FROM dbo.utb_task
   WHERE Name = 'Lien Holder Letter of Guarantee Follow Up Required'

   SELECT @TaskIDLHPOFollowup = TaskID,
          @TaskIDLHPOFollowupAlarm = Dateadd(minute, EscalationMinutes, @now)
   FROM dbo.utb_task
   WHERE Name = 'Lien Holder Payoff Follow Up Required'

   SELECT @DocumentTypeIDLoG = DocumentTypeID
   From utb_document_type
   WHERE Name = 'Letter of Guarantee'

   SELECT @DocumentTypeIDLHPoA = DocumentTypeID
   From utb_document_type
   WHERE Name = 'Lien Holder POA'

   SELECT @DocumentTypeIDLHTitle = DocumentTypeID
   From utb_document_type
   WHERE Name = 'Title'

   SELECT @DocumentTypeIDVOPoA = DocumentTypeID
   From utb_document_type
   WHERE Name = 'Vehicle Owner POA'

   SELECT @DocumentTypeIDVOTitle = DocumentTypeID
   From utb_document_type
   WHERE Name = 'Title'

   --select * from utb_document_type order by name
   INSERT INTO @tmpOpenTLClaims
   SELECT casc.ClaimASpectServiceChannelID,
          CASE 
            WHEN LienHolderID IS NOT NULL THEN 1
            ELSE 0
          END as LienHolderExistsFlag,
          CASE
            WHEN LienHolderID IS NOT NULL THEN isNull(tlm.LHRecentContactDate, tlm.LHInitialContactDate)
            ELSE isNull(tlm.VORecentContactDate, tlm.VOInitialContactDate)
          END as 'RecentContactDate',
          tlm.LHInitialContactDate,
          tlm.LHRecentContactDate,
          tlm.VOInitialContactDate,
          tlm.VORecentContactDate,
          Datediff(day, CASE
                           WHEN LienHolderID IS NOT NULL THEN isNull(tlm.LHRecentContactDate, tlm.LHInitialContactDate)
                           ELSE isNull(tlm.VORecentContactDate, tlm.VOInitialContactDate)
                         END, @now) as 'DaysSinceRecentContact',
          Datediff(day, casc.CreatedDate, @now),
          (SELECT top 1 d.DocumentID
           from utb_claim_aspect_service_channel_document cascd 
           LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
           where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
             and d.DocumentTypeID = @DocumentTypeIDLoG) as 'DocumentIDLoG',
          (SELECT top 1 d.DocumentID
           from utb_claim_aspect_service_channel_document cascd 
           LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
           where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
             and d.DocumentTypeID = @DocumentTypeIDLHPoA) as 'DocumentIDLHPoA',
          (SELECT top 1 d.DocumentID
           from utb_claim_aspect_service_channel_document cascd 
           LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
           where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
             and d.DocumentTypeID = @DocumentTypeIDLHTitle) as 'DocumentIDLHTitle',
          (SELECT top 1 d.DocumentID
           from utb_claim_aspect_service_channel_document cascd 
           LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
           where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
             and d.DocumentTypeID = @DocumentTypeIDVOPoA) as 'DocumentIDVOPoA',
          (SELECT top 1 d.DocumentID
           from utb_claim_aspect_service_channel_document cascd 
           LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
           where cascd.ClaimASpectServiceChannelID = casc.ClaimASpectServiceChannelID
             and d.DocumentTypeID = @DocumentTypeIDVOTitle) as 'DocumentIDVOTitle',
           0
   FROM utb_claim_aspect_service_channel casc
   LEFT JOIN utb_claim_aspect_status cas ON casc.ClaimAspectID = cas.ClaimAspectID
   LEFT JOIN utb_status s ON cas.StatusID = s.StatusID
   LEFT OUTER JOIN utb_total_loss_metrics tlm ON casc.ClaimASpectServiceChannelID = tlm.ClaimASpectServiceChannelID
   WHERE casc.ServiceChannelCD = 'TL'
     AND cas.StatusTypeCD = 'SC'
     AND s.Name = 'Active'
     --AND casc.ClaimASpectServiceChannelID = 211723
     
   SELECT @OpenClaimCount = count(ClaimASpectServiceChannelID) FROM @tmpOpenTLClaims

   --select * from @tmpOpenTLClaims

   --
   WHILE @OpenClaimCount > 0
   BEGIN
      SELECT TOP 1 @ClaimAspectServiceChannelID = ClaimASpectServiceChannelID
      FROM @tmpOpenTLClaims
      WHERE ValidatedFlag = 0
      
      PRINT 'ClaimAspectServiceChannelID=' + convert(varchar, @ClaimAspectServiceChannelID)
      -- Vehicle Owner Followup
      IF EXISTS (SELECT ClaimAspectServiceChannelID
                  FROM @tmpOpenTLClaims
                  WHERE ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
                    AND ((DocumentIDVOPoA IS NULL) 
                      OR (LienHolderExistsFlag = 0 AND DocumentIDVOTitle IS NULL))
                    AND (DaysSinceRecentContact > @VOFollowUpCTDays
                      OR DaysSinceTLCreated > @VOFollowUpCTDays))
      BEGIN
          IF NOT EXISTS(SELECT ChecklistID
                        FROM utb_checklist
                        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                          AND TaskID = @TaskIDVOFollowup)
          BEGIN
               EXEC dbo.uspDiaryInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                      @TaskID = @TaskIDVOFollowup,
                      @AlarmDate = @TaskIDVOFollowupAlarm,
                      @UserTaskDescription = '',
                      @AssignedUserID = NULL,
                      @AssignmentPoolFunctionCD = NULL,
                      @SupervisorFlag = 0,
                      @UserID = 0

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspDiaryInsDetail')
               --ROLLBACK TRANSACTION
               RETURN
              END
          END
      END
      
      -- LH LoG Followup
      IF EXISTS (SELECT ClaimAspectServiceChannelID
                  FROM @tmpOpenTLClaims
                  WHERE ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
                    AND LienHolderExistsFlag = 1
                    AND DocumentIDLoG IS NULL
                    AND (DaysSinceRecentContact > @LHLoGFollowupCTDays
                      OR DaysSinceTLCreated > @LHLoGFollowupCTDays))
      BEGIN
          IF NOT EXISTS(SELECT ChecklistID
                        FROM utb_checklist
                        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                          AND TaskID = @TaskIDLHLoGFollowup)
          BEGIN
               EXEC dbo.uspDiaryInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                      @TaskID = @TaskIDLHLoGFollowup,
                      @AlarmDate = @TaskIDLHLoGFollowupAlarm,
                      @UserTaskDescription = '',
                      @AssignedUserID = NULL,
                      @AssignmentPoolFunctionCD = NULL,
                      @SupervisorFlag = 0,
                      @UserID = 0

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspDiaryInsDetail')
               --ROLLBACK TRANSACTION
               RETURN
              END
          END
          --PRINT ' Lien Holder LoG Followup required.'
      END
      
      -- LH Payoff Followup
      IF EXISTS (SELECT ClaimAspectServiceChannelID
                  FROM @tmpOpenTLClaims
                  WHERE ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
                    AND LienHolderExistsFlag = 1
                    AND (DocumentIDLHPoA IS NULL OR DocumentIDLHTitle is NULL) 
                    AND (DaysSinceRecentContact > @LHPOFollowupCTDays
                      OR DaysSinceTLCreated > @LHPOFollowupCTDays))
      BEGIN
          IF NOT EXISTS(SELECT ChecklistID
                        FROM utb_checklist
                        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                          AND TaskID = @TaskIDLHPOFollowup)
          BEGIN
               EXEC dbo.uspDiaryInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                      @TaskID = @TaskIDLHPOFollowup,
                      @AlarmDate = @TaskIDLHPOFollowupAlarm,
                      @UserTaskDescription = '',
                      @AssignedUserID = NULL,
                      @AssignmentPoolFunctionCD = NULL,
                      @SupervisorFlag = 0,
                      @UserID = 0

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspDiaryInsDetail')
               --ROLLBACK TRANSACTION
               RETURN
              END
          END
          --PRINT ' Lien Holder Payoff Followup required.'
      END
      
      
      UPDATE @tmpOpenTLClaims
      SET ValidatedFlag = 1
      WHERE ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
      
      SET @OpenClaimCount = @OpenClaimCount - 1
   END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLFollowupScript' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLFollowupScript TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/