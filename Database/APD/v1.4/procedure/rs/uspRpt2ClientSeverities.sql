-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientSeverities' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientSeverities 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientSeverities
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Severity Report
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ShowAllClientsFlag   Flag indicating whether to compile all clients information
* (I) @StateCode            Optional state code
* (I) @ServiceChannelCD     Optional ServiceChannelCD
* (I) @OfficeID             Optional Office ID
*
* RESULT SET:
*   LynxPhoneNumber         Customer call in number for Lynx
*   State                   State (or "All" if the report is for all states)
*   ServiceGroup            Service Channel (or "All" if the report is for all channels)
*   OfficeName              Office Name
*   Pos1Header              The header name of column 1
*   Pos2Header              The header name of column 2
*   Pos3Header              The header name of column 3
*   Pos4Header              The header name of column 4
*   Pos5Header              The header name of column 5
*   Pos6Header              The header name of column 6
*   Pos7Header              The header name of column 7
*   Pos8Header              The header name of column 8
*   Pos9Header              The header name of column 9
*   Pos10Header             The header name of column 10
*   Pos11Header             The header name of column 11
*   Pos12Header             The header name of column 12
*   Level2Display           The display name of the detail lines
*   Level2Sort              The sort order of the detail lines
*   Level1Group             Display name for the insurance company header
*   Level2Group             Detail Line code ("COLL", "COMP", "LIAB", "UIM")
*   Pos1Severity            Severity for column 1
*   Pos2Severity            Severity for column 2
*   Pos3Severity            Severity for column 3
*   Pos4Severity            Severity for column 4
*   Pos5Severity            Severity for column 5
*   Pos6Severity            Severity for column 6
*   Pos7Severity            Severity for column 7
*   Pos8Severity            Severity for column 8
*   Pos9Severity            Severity for column 9
*   Pos10Severity           Severity for column 10
*   Pos11Severity           Severity for column 11
*   Pos12Severity           Severity for column 12
*   TotalSeverity           Severity for column 13
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientSeverities
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ShowAllClients         udt_std_flag = 0,
    @StateCode              udt_addr_state = NULL,
    @ServiceChannelCD       udt_std_cd = NULL,
    @OfficeID               udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    
    DECLARE @tmpSeverityDataAll TABLE
    (
        Level1Group                 varchar(50)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(100) NOT NULL,
        Pos1Severity                int          NULL,
        Pos2Severity                int          NULL,
        Pos3Severity                int          NULL,
        Pos4Severity                int          NULL,
        Pos5Severity                int          NULL,
        Pos6Severity                int          NULL,
        Pos7Severity                int          NULL,
        Pos8Severity                int          NULL,
        Pos9Severity                int          NULL,
        Pos10Severity               int          NULL,
        Pos11Severity               int          NULL,
        Pos12Severity               int          NULL,
        TotalSeverity               int          NULL,
        Pos1Count                   int          NULL,
        Pos2Count                   int          NULL,
        Pos3Count                   int          NULL,
        Pos4Count                   int          NULL,
        Pos5Count                   int          NULL,
        Pos6Count                   int          NULL,
        Pos7Count                   int          NULL,
        Pos8Count                   int          NULL,
        Pos9Count                   int          NULL,
        Pos10Count                  int          NULL,
        Pos11Count                  int          NULL,
        Pos12Count                  int          NULL,
        TotalCount                  int          NULL
    )

    DECLARE @tmpSeverityDataClient TABLE
    (
        Level1Group                 varchar(50)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(100) NOT NULL,
        Pos1Severity                int          NULL,
        Pos2Severity                int          NULL,
        Pos3Severity                int          NULL,
        Pos4Severity                int          NULL,
        Pos5Severity                int          NULL,
        Pos6Severity                int          NULL,
        Pos7Severity                int          NULL,
        Pos8Severity                int          NULL,
        Pos9Severity                int          NULL,
        Pos10Severity               int          NULL,
        Pos11Severity               int          NULL,
        Pos12Severity               int          NULL,
        TotalSeverity               int          NULL,
        Pos1Count                   int          NULL,
        Pos2Count                   int          NULL,
        Pos3Count                   int          NULL,
        Pos4Count                   int          NULL,
        Pos5Count                   int          NULL,
        Pos6Count                   int          NULL,
        Pos7Count                   int          NULL,
        Pos8Count                   int          NULL,
        Pos9Count                   int          NULL,
        Pos10Count                  int          NULL,
        Pos11Count                  int          NULL,
        Pos12Count                  int          NULL,
        TotalCount                  int          NULL
    )
        
    DECLARE @tmpSummaryClosedData TABLE
    (
        CoverageTypeCD              varchar(4)   NOT NULL,
        PositionID                  tinyint      NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        ClosedCount                 int          NOT NULL,
        AvgSeverity                 int          NOT NULL,
        LevelPageNo                 tinyint      NOT NULL
    )

    DECLARE @tmpTimePeriods TABLE
    (
        PositionID                  tinyint     NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )


    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @StateCodeWork          varchar(2)
    DECLARE @StateName              udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    DECLARE @TotalLossDispositionID int
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int
    
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
    DECLARE @Page1DataDesc as varchar(100)
    DECLARE @Page2DataDesc as varchar(100)

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientSeverities'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate State Code
    
    IF @StateCode IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @StateCode)
        BEGIN
            -- Invalid StateCode
        
            RAISERROR  ('101|%s|@StateCode|%s', 16, 1, @ProcName, @StateCode)
            RETURN
        END
        ELSE
        BEGIN
            SET @StateCodeWork = @StateCode
            
            SELECT  @StateName = StateValue
              FROM  dbo.utb_state_code
              WHERE StateCode = @StateCode
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @StateCodeWork = '%'
        SET @StateName = 'All'
    END          
        
   
    -- Validate Service Channel Code
    
    IF @ServiceChannelCD IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') WHERE Code = @ServiceChannelCD)
        BEGIN
            -- Invalid Service Channel CD
        
            RAISERROR  ('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
            RETURN
        END
        ELSE
        BEGIN
            SELECT  @ServiceChannelName = Name
              FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
              WHERE Code = @ServiceChannelCD
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @ServiceChannelCD = '%'
        SET @ServiceChannelName = 'All'
    END          
        
    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = convert(varchar, @OfficeID)
        
            SELECT  @OfficeName = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @OfficeIDWork = '%'
        SET @OfficeName = 'All'
    END          

    SET @TotalLossDispositionID = NULL
    
    SELECT @TotalLossDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Total Loss'
    
    IF @TotalLossDispositionID IS NULL OR LEN(@TotalLossDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Total Loss in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END


	SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
   
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END

    -- Compile time periods
    
    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO @tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END

           
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@ShowAllClients = ' + Convert(varchar, @ShowAllClients)
        PRINT '@StateCode = ' + @StateCode
        PRINT '@StateCodeWork = ' + @StateCodeWork
        PRINT '@ServiceChannelCD = ' + @ServiceChannelCD
        PRINT '@OfficeID = ' + convert(varchar, @OfficeID)
        PRINT '@OfficeIDWork = ' + convert(varchar, @OfficeIDWork)
        PRINT '@TotalLossDispositionID = ' + convert(varchar, @TotalLossDispositionID)
    END
    
    SET @Page1DataDesc = 'Excludes Total Loss, Cash Out, Stale, Cancelled and Voided'
    SET @Page2DataDesc = 'Excludes Total Loss, Cancelled and Voided'
    
    -- Create a true temp table. The data can be large.
    CREATE TABLE #tmpClosedData
    (
        CoverageTypeCD              varchar(4)   NULL,
        DispositionTypeCD           varchar(2)   NULL,
        PositionID                  tinyint      NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        SeverityAmount              money        NULL
    )
    
    -- Create the indexes for faster data access
    
    CREATE INDEX uix_ie_tmpClosedDataCoverageTypeCD ON #tmpClosedData ( CoverageTypeCD )
    
    CREATE INDEX uix_ie_tmpClosedDataDispositionTypeCD ON #tmpClosedData ( DispositionTypeCD )
    
    -- Compile data needed for the report

    -- Client Data for damage assessment
    INSERT INTO #tmpClosedData
    SELECT dct.CoverageTypeCD,
          ddt.DispositionTypeCD,
          tmpTP.PositionID,
          dt.MonthOfYear,
          dt.YearValue,
          CASE @ServiceChannelCD
            WHEN 'DA' THEN fc.AuditedEstimateNetAmt
            ELSE fc.IndemnityAmount
          END
    FROM  dbo.utb_dtwh_fact_claim fc
    LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
    LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
    LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
    LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
    LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
    LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
    LEFT JOIN dbo.utb_dtwh_dim_disposition_type ddt ON (fc.DispositionTypeID = ddt.DispositionTypeID)
    INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
    WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
      AND ds.StateCode LIKE @StateCodeWork
      AND dsc.ServiceChannelCD LIKE @ServiceChannelCD
      AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
	  AND fc.EnabledFlag = 1
      
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END
    
    -- Compile the summary for Page 1
    -- Page 1 will exclude the following coverage types 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    
    -- monthly data  per coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT t.CoverageTypeCD,
           t.PositionID,
           t.MonthValue,
           t.YearValue,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
    GROUP BY t.CoverageTypeCD, t.PositionID, t.MonthValue, t.YearValue
      HAVING t.CoverageTypeCD IS NOT NULL
    
    -- monthly data - combined coverage type code.
    INSERT INTO @tmpSummaryClosedData
    SELECT 'YCMB',
           t.PositionID,
           t.MonthValue,
           t.YearValue,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
    GROUP BY t.PositionID, t.MonthValue, t.YearValue
    
    -- 12 month data per coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT t.CoverageTypeCD,
           13,
           99,
           9999,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
    GROUP BY t.CoverageTypeCD
      HAVING t.CoverageTypeCD IS NOT NULL
    
    -- 12 month data - combined coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT 'YCMB',
           13,
           99,
           9999,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
    

    -- Compile the summary for Page 2
    -- Page 2 will exclude the following coverage types 'TL', 'CA', 'VO', 'NA'
    
    -- monthly data  per coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT t.CoverageTypeCD,
           t.PositionID,
           t.MonthValue,
           t.YearValue,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE (t.DispositionTypeCD = 'RC'
      OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0))
    GROUP BY t.CoverageTypeCD, t.PositionID, t.MonthValue, t.YearValue
      HAVING t.CoverageTypeCD IS NOT NULL
    
    -- monthly data - combined coverage type code.
    INSERT INTO @tmpSummaryClosedData
    SELECT 'YCMB',
           t.PositionID,
           t.MonthValue,
           t.YearValue,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
      OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
    GROUP BY t.PositionID, t.MonthValue, t.YearValue
    
    -- 12 month data per coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT t.CoverageTypeCD,
           13,
           99,
           9999,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
      OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
    GROUP BY t.CoverageTypeCD
      HAVING t.CoverageTypeCD IS NOT NULL
    
    -- 12 month data - combined coverage type
    INSERT INTO @tmpSummaryClosedData
    SELECT 'YCMB',
           13,
           99,
           9999,
           IsNull(count(*), 0) as ClosedCount,
           IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
           2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
    FROM #tmpClosedData t
    WHERE t.DispositionTypeCD = 'RC'
      OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
    
    
    -- Pivot and save the data extracted
    
    INSERT INTO @tmpSeverityDataClient
    SELECT  @InsuranceCompanyName,
          CoverageTypeCD,
          CASE LevelPageNo
            WHEN 1 THEN @Page1DataDesc
            WHEN 2 THEN @Page2DataDesc
          END,
          SUM(CASE PositionID WHEN 1 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 2 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 3 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 4 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 5 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 6 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 7 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 8 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 9 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 10 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 11 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 12 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 13 THEN AvgSeverity ELSE 0 END),
          SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
          SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END)
    FROM  @tmpSummaryClosedData
    GROUP BY CoverageTypeCD, LevelPageNo

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName) 
        RETURN
    END

    -- Add the missing coverage type data for page 1
    INSERT INTO @tmpSeverityDataClient
    SELECT  @InsuranceCompanyName,
          dct.CoverageTypeCD,
          @Page1DataDesc,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0
    FROM  dbo.utb_dtwh_dim_coverage_type dct
    WHERE dct.CoverageTypeCD NOT IN (SELECT Level2Group FROM @tmpSeverityDataClient WHERE Level3Group = @Page1DataDesc)
    AND dct.CoverageTypeID  IN (SELECT ClientCoverageTypeID FROM utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName)
        RETURN
    END        
    
    -- Add the missing coverage type data for page 2
    INSERT INTO @tmpSeverityDataClient
    SELECT  @InsuranceCompanyName,
          dct.CoverageTypeCD,
          @Page2DataDesc,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0
    FROM  dbo.utb_dtwh_dim_coverage_type dct
    WHERE dct.CoverageTypeCD NOT IN (SELECT Level2Group FROM @tmpSeverityDataClient WHERE Level3Group = @Page2DataDesc)
    AND dct.CoverageTypeID  IN (SELECT ClientCoverageTypeID FROM utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ShowAllClients = 1
    BEGIN
        -- Reset the temporary tables
        DELETE FROM #tmpClosedData
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|#tmpClosedData', 16, 1, @ProcName)
            RETURN
        END
        
        DELETE FROM @tmpSummaryClosedData
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|@tmpSummaryClosedData', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO #tmpClosedData
        SELECT dct.CoverageTypeCD,
              ddt.DispositionTypeCD,
              tmpTP.PositionID,
              dt.MonthOfYear,
              dt.YearValue,
              CASE @ServiceChannelCD
                WHEN 'DA' THEN fc.AuditedEstimateNetAmt
                ELSE IsNull(fc.IndemnityAmount, 0)
              END
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type ddt ON (fc.DispositionTypeID = ddt.DispositionTypeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE ds.StateCode LIKE @StateCodeWork
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCD
          AND fc.DemoFlag = 0
		  AND fc.EnabledFlag = 1
          --AND dct.CoverageTypeCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
    
        -- Compile the summary for Page 1
        -- Page 1 will exclude the following coverage types 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
    
        -- monthly data  per coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT t.CoverageTypeCD,
               t.PositionID,
               t.MonthValue,
               t.YearValue,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
        GROUP BY t.CoverageTypeCD, t.PositionID, t.MonthValue, t.YearValue
          HAVING t.CoverageTypeCD IS NOT NULL
    
        -- monthly data - combined coverage type code.
        INSERT INTO @tmpSummaryClosedData
        SELECT 'YCMB',
               t.PositionID,
               t.MonthValue,
               t.YearValue,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
        GROUP BY t.PositionID, t.MonthValue, t.YearValue
    
        -- 12 month data per coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT t.CoverageTypeCD,
               13,
               99,
               9999,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
        GROUP BY t.CoverageTypeCD
          HAVING t.CoverageTypeCD IS NOT NULL
    
        -- 12 month data - combined coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT 'YCMB',
               13,
               99,
               9999,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               1 -- LevelPageNo. First page will exclude 'TL', 'CO', 'ST', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
    

        -- Compile the summary for Page 2
        -- Page 2 will exclude the following coverage types 'TL', 'CA', 'VO', 'NA'
    
        -- monthly data  per coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT t.CoverageTypeCD,
               t.PositionID,
               t.MonthValue,
               t.YearValue,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
          OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
        GROUP BY t.CoverageTypeCD, t.PositionID, t.MonthValue, t.YearValue
          HAVING t.CoverageTypeCD IS NOT NULL
    
        -- monthly data - combined coverage type code.
        INSERT INTO @tmpSummaryClosedData
        SELECT 'YCMB',
               t.PositionID,
               t.MonthValue,
               t.YearValue,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
          OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
        GROUP BY t.PositionID, t.MonthValue, t.YearValue
    
        -- 12 month data per coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT t.CoverageTypeCD,
               13,
               99,
               9999,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
          OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
        GROUP BY t.CoverageTypeCD
          HAVING t.CoverageTypeCD IS NOT NULL
    
        -- 12 month data - combined coverage type
        INSERT INTO @tmpSummaryClosedData
        SELECT 'YCMB',
               13,
               99,
               9999,
               IsNull(count(*), 0) as ClosedCount,
               IsNull(Round(Avg(t.SeverityAmount), 0), 0) AS AvgSeverity,
               2 -- LevelPageNo. Second page will exclude 'TL', 'CA', 'VO', 'NA'
        FROM #tmpClosedData t
        WHERE t.DispositionTypeCD = 'RC'
          OR (t.DispositionTypeCD in ('CO', 'ST') AND t.SeverityAmount > 0)
    
    
        -- Pivot and save the data extracted
    
        INSERT INTO @tmpSeverityDataAll
        SELECT  '* All Clients *',
              CoverageTypeCD,
              CASE LevelPageNo
                WHEN 1 THEN @Page1DataDesc
                WHEN 2 THEN @Page2DataDesc
              END,
              SUM(CASE PositionID WHEN 1 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN AvgSeverity ELSE 0 END),
              SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END)
        FROM  @tmpSummaryClosedData
        GROUP BY CoverageTypeCD, LevelPageNo

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName) 
            RETURN
        END

        -- Add the missing coverage type data for page 1
        INSERT INTO @tmpSeverityDataAll
        SELECT  '* All Clients *',
              dct.CoverageTypeCD,
              @Page1DataDesc,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0
        FROM  dbo.utb_dtwh_dim_coverage_type dct
        WHERE dct.CoverageTypeCD NOT IN (SELECT Level2Group FROM @tmpSeverityDataClient WHERE Level3Group = @Page1DataDesc)
        AND dct.CoverageTypeID  IN (SELECT ClientCoverageTypeID FROM utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName)
            RETURN
        END        
    
        -- Add the missing coverage type data for page 2
        INSERT INTO @tmpSeverityDataAll
        SELECT  '* All Clients *',
              dct.CoverageTypeCD,
              @Page2DataDesc,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0
        FROM  dbo.utb_dtwh_dim_coverage_type dct
        WHERE dct.CoverageTypeCD NOT IN (SELECT Level2Group FROM @tmpSeverityDataClient WHERE Level3Group = @Page2DataDesc)
        AND dct.CoverageTypeID  IN (SELECT ClientCoverageTypeID FROM utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpSeverityDataClient', 16, 1, @ProcName)
            RETURN
        END
    END
    
    DROP INDEX #tmpClosedData.uix_ie_tmpClosedDataCoverageTypeCD
    
    DROP INDEX #tmpClosedData.uix_ie_tmpClosedDataDispositionTypeCD

    DROP TABLE #tmpClosedData
    
--    select * from #tmpClosedData
    
--    select * from @tmpSummaryClosedData

--    select * from @tmpSeverityDataClient
    
--    select * from @tmpSeverityDataAll

    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            @StateName AS State,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'COMP' THEN 'Comprehensive'
              WHEN 'COLL' THEN 'Collision'
              WHEN 'LIAB' THEN 'Liability'
              WHEN 'UIM' THEN 'UIM'
              WHEN 'UM' THEN 'UM'
			  WHEN 'YCMB' THEN 'Combined'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'COLL' THEN 1
              WHEN 'COMP' THEN 2
              WHEN 'LIAB' THEN 3
              WHEN 'UIM' THEN 4
              WHEN 'UM' THEN 5
		      WHEN 'YCMB' THEN 6
            END AS Level2Sort,
            tmp.*,
            @DataWarehouseDate as DataWareDate
      FROM  @tmpSeverityDataClient tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            @StateName AS State,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'COMP' THEN 'Comprehensive'
              WHEN 'COLL' THEN 'Collision'
              WHEN 'LIAB' THEN 'Liability'
              WHEN 'UIM' THEN 'UIM'
              WHEN 'UM' THEN 'UM'
			  WHEN 'YCMB' THEN 'Combined'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'COLL' THEN 1
              WHEN 'COMP' THEN 2
              WHEN 'LIAB' THEN 3
              WHEN 'UIM' THEN 4
              WHEN 'UM' THEN 5
			  WHEN 'YCMB' THEN 6
            END AS Level2Sort,
            tmp.*,
            @DataWarehouseDate as DataWareDate 
      FROM  @tmpSeverityDataAll tmp
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientSeverities' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientSeverities TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
