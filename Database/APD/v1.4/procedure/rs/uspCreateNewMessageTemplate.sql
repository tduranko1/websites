-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewMessageTemplate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewMessageTemplate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewMessageTemplate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client bundling message template by inserting a new record into the 
*				utb_message_template table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*   @vCompanyNameShort
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewMessageTemplate
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @vCompanyNameShort VARCHAR(6)
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewMessageTemplate'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure no other template is using the same short name
    IF (EXISTS(SELECT *	FROM utb_message_template WHERE	[Description] LIKE '%' + @vCompanyNameShort + '%'))
    BEGIN
        -- Invalid Existing Short Name
        RAISERROR('101|%s|@vCompanyNameShort|%u', 16, 1, @ProcName, @vCompanyNameShort)
        RETURN
    END

	-- Add the new
	INSERT INTO utb_message_template 
	SELECT
		t.AppliesToCD
		, t.EnabledFlag
		, SUBSTRING(t.[Description],0,CHARINDEX('|',t.[Description])) + '|Messages/' + SUBSTRING(REPLACE(SUBSTRING(t.[Description],CHARINDEX('|',t.[Description])+1,LEN(t.[Description])),'Messages/',''),0,4) + @vCompanyNameShort + t.ServiceChannelCD + REPLACE(SUBSTRING(t.[Description],0,CHARINDEX('|',t.[Description])),' ','') + '.xls' AS NewFileName
		, t.ServiceChannelCD
		, @iCreatedByUserID
		, @dtNow
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @iModelInsuranceCompanyID
	 
	IF EXISTS(
		SELECT 
			* 
		FROM 
			utb_message_template
		WHERE
			[Description] LIKE '%' + @vCompanyNameShort + '%'
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewMessageTemplate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewMessageTemplate TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewMessageTemplate TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/