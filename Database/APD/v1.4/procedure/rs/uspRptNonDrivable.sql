-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptNonDrivable' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptNonDrivable 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptNonDrivable
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Return non drivable claim information
*
* PARAMETERS:  
*
* RESULT SET: None
* 
* RETURNS:  Recordset of non drivable claim information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptNonDrivable
AS
BEGIN
    set nocount on

    declare @StartDate as datetime
    declare @EndDate as datetime

    set @EndDate = convert(datetime, convert(varchar, current_timestamp, 110))
    set @StartDate = dateadd(day, -1, dateadd(week, -1, @EndDate))

    select convert(varchar, @StartDate, 110) as 'Report Start Date',
           convert(varchar, @EndDate, 110) as 'Report End Date',
           convert(varchar(10), c.LynxID) as 'LynxID', 
           i.Name as 'Insurance Company',
           isNull(tmp.Name, '') as 'Shop Name',
           isNull(tmp.AddressCity, '') as 'Shop City',
           isNull(tmp.AddressState, '') as 'Shop State',
           isNull(convert(varchar(10), casc.WorkStartDate, 110), '') as 'Repair Start',
           isNull(convert(varchar(10), casc.WorkEndDate, 110), '') as 'Repair End',
           isNull((select top 1 convert(varchar(12), es.AgreedExtendedAmt)
                from utb_claim_aspect_service_channel_document cascd
                left join utb_document d on cascd.DocumentID = d.DocumentID
                left join utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
                left join utb_estimate_summary es on d.DocumentID = es.DocumentID
                where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                  and d.DocumentTypeID in (3, 10)
                  and d.EnabledFlag = 1
                  and es.EstimateSummaryTypeID = 27
                 order by d.SupplementSeqNumber desc, ds.VANFlag desc), '') as 'Gross Amt'
    from utb_claim_aspect_service_channel casc 
    left join utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
    left join utb_claim c on ca.LynxID = c.LynxID
    left join utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    left join utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID
    left join (select a.ClaimAspectServiceChannelID, sl.Name, sl.AddressCity, sl.AddressState
                from utb_assignment a
                left join utb_shop_location sl on a.ShopLocationID = sl.ShopLocationID
                where a.CancellationDate is null AND a.assignmentsequencenumber = 1) tmp on casc.ClaimAspectServiceChannelID = tmp.ClaimAspectServiceChannelID
    where casc.CreatedDate between @StartDate and @EndDate
      and casc.ServiceChannelCD = 'PS'
      and cv.DriveableFlag = 0
    order by c.LynxID desc

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptNonDrivable' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptNonDrivable TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/