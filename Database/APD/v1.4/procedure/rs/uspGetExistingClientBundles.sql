-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingClientBundles' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetExistingClientBundles 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetExistingClientBundles
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets existing bundling information by InsuranceCompanyID
*
* PARAMETERS:  
*			@iInsuranceCompanyID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetExistingClientBundles
	@iInsuranceCompanyID INT = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	IF (@iInsuranceCompanyID = 0)
	BEGIN
		SELECT
		--	* 
		--	b.BundlingID 
			b.BundlingID
			, b.DocumentTypeID
			--, b.MessageTemplateID 
			, b.[Name]
			, mt.[Description]
			, b.EnabledFlag
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		--WHERE 
		--	cb.InsuranceCompanyID = 374
		--			AND b.BundlingID = 298
		ORDER BY
			b.[Name]

/*		SELECT DISTINCT
			b.BundlingID
			--, b.DocumentTypeID
			--, b.MessageTemplateID 
			, b.[Name]
			, t.[Description]
			, b.EnabledFlag
			, bdt.DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, bdt.EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag
		FROM
			utb_bundling b
			INNER JOIN utb_client_bundling cb 
				ON b.BundlingId = cb.BundlingID
			INNER JOIN utb_message_template t 
				ON t.MessageTemplateID = b.MessageTemplateID
			INNER JOIN utb_document_type dt
				ON dt.DocumentTypeID = b.DocumentTypeID 
			INNER JOIN utb_bundling_document_type bdt
				ON bdt.DocumentTypeID = dt.DocumentTypeID 
		ORDER BY
			b.[Name]
*/
	END
	ELSE
	BEGIN
		SELECT
		--	* 
		--	b.BundlingID 
			b.BundlingID
			, b.DocumentTypeID
			--, b.MessageTemplateID 
			, b.[Name]
			, mt.[Description]
			, b.EnabledFlag
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @iInsuranceCompanyID
		--			AND b.BundlingID = 298
		ORDER BY
			b.[Name]

/*		SELECT DISTINCT
			b.BundlingID
			--, b.DocumentTypeID
			--, b.MessageTemplateID 
			, b.[Name]
			, t.[Description]
			, b.EnabledFlag
			, bdt.DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, bdt.EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag
		FROM
			utb_bundling b
			INNER JOIN utb_client_bundling cb 
				ON b.BundlingId = cb.BundlingID
			INNER JOIN utb_message_template t 
				ON t.MessageTemplateID = b.MessageTemplateID
			INNER JOIN utb_document_type dt
				ON dt.DocumentTypeID = b.DocumentTypeID 
			INNER JOIN utb_bundling_document_type bdt
				ON bdt.DocumentTypeID = dt.DocumentTypeID 
		WHERE
			cb.InsuranceCompanyID = @iInsuranceCompanyID
		ORDER BY
			b.[Name]
*/
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingClientBundles' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetExistingClientBundles TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetExistingClientBundles TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/