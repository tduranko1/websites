-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleAssignEnrouteGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspVehicleAssignEnrouteGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure

/************************************************************************************************************************
*
* PROCEDURE:    uspVehicleAssignEnrouteGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve enroute shop assignments over the given grace period
*
* PARAMETERS:  
* (I) @minutes              The number of minutes allowed before enroute assignements are listed in the result set
*
* RESULT SET:
* LynxID                        The Shop Assignment's claim unique identity
* VehicleNumber                 The Shop Assignment claim's vehicle number
* StatusStartDate               The starting date/time of the Shop Assignment
* MinutesInStatus               The number of minutes the Shop Assignement has been enroute
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspVehicleAssignEnrouteGetList
(
    @minutes       INT = 15       -- Default: 15 minutes
)
AS
BEGIN
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ClaimAspectTypeIDAssignment    AS INT
    DECLARE @ClaimAspectTypeIDVehicle       AS INT
    DECLARE @StatusIDEnroute                AS INT

    DECLARE @now                            AS DATETIME

    DECLARE @ProcName                       AS VARCHAR(30)

    SET @ProcName = 'uspVehicleAssignEnrouteGetList'  


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Get claim aspect for vehicle

    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting claim aspect type ID for "Vehicle"', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        -- Claim Aspect Type not found
    
        RAISERROR('APD - %s: Invalid Data State - Claim Aspect Type ID For "Vehicle" not found.', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get Status ID for Enroute

    SELECT  @StatusIDEnroute = StatusID
      FROM  dbo.utb_status
      WHERE ClaimAspecttypeID = @ClaimAspectTypeIDVehicle
        AND StatusTypeCD = 'ELC'
        AND Name = 'Enroute'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting Status ID for "Enroute"', 16, 1, @ProcName)
        RETURN
    END
    
    IF @StatusIDEnroute IS NULL
    BEGIN
        -- Status not found
    
        RAISERROR('APD - %s: Invalid Data State - Status ID For "Enroute" not found.', 16, 1, @ProcName)
        RETURN
    END


    -- See if there are any assignments that have not yet been acked by the VAN

    SELECT  ca.LynxID, 
            ca.ClaimAspectNumber AS VehicleNumber,
            (SELECT  cm.RoutingCD 
               FROM  dbo.utb_claim_aspect cas 
            	/*********************************************************************************
            	Project: 210474 APD - Enhancements to support multiple concurrent service channels
            	Note:	Added reference to utb_Claim_Aspect_Service_Channel to provide a join
                        between utb_claim_aspect and utb_assignment tables
            		    M.A. 20061120
            	*********************************************************************************/
               LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON cas.ClaimAspectID = casc.ClaimAspectID
               LEFT JOIN dbo.utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
               LEFT JOIN dbo.utb_communication_method cm ON (a.CommunicationMethodID = cm.CommunicationMethodID)
               WHERE cas.LynxID = ca.LynxID
                 AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                 AND cas.ClaimAspectNumber = ca.ClaimAspectNumber
                 AND a.AssignmentDate IS NOT NULL
                 AND a.CancellationDate IS NULL) AS Partner,
            dbo.ufnUtilityGetDateString(cas.StatusStartDate) AS StatusStartDate,
            DateDiff(minute, cas.StatusStartDate, @now) AS MinutesInStatus
      FROM  dbo.utb_claim_aspect ca
      /*********************************************************************************
      Project: 210474 APD - Enhancements to support multiple concurrent service channels
      Note:	    Added reference to utb_Claim_Aspect_Status to provide a the value of
                "StatusStartDate" used above in the SELECT stmnt and the WHERE clause
                below.
      	        M.A. 20061120
      *********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Status cas on ca.ClaimAspectID = cas.ClaimAspectID
      WHERE cas.StatusID = @StatusIDEnroute
        and   cas.ServiceChannelCD is null
        and   cas.StatusTypeCD is null
        AND DateDiff(minute, cas.StatusStartDate, @now) > @minutes    -- In status more than x minutes
      ORDER BY 5 DESC

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting Shop Assignments for "Enroute"', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleAssignEnrouteGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspVehicleAssignEnrouteGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
