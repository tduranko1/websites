-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopDocumentInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopDocumentInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopDocumentInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspShopDocumentInsDetail
(
    @ShopLocationID     bigint,
    @EffectiveDate      datetime,
    @ExpirationDate     datetime,
    @ImageDate          varchar(30),
    @ImageLocation      udt_std_desc_long,
    @DocumentSource     udt_std_desc_short,
    @DocumentType       udt_std_desc_short,
    @DirectionalCD      udt_std_cd             = 'I',
    @ImageType          udt_std_file_extension,
    @Comments           udt_std_note,
    @UserID             udt_std_id
)
AS
BEGIN
    DECLARE @now                        udt_std_datetime
    DECLARE @ImageDateWork              datetime
    DECLARE @DocumentTypeID             int
    DECLARE @DocumentSourceID           int
    DECLARE @CreatedUserID              udt_std_id
    DECLARE @CreatedUserRoleID          udt_std_id
    DECLARE @CreatedUserSupervisorFlag  udt_std_id
    DECLARE @DocumentID                 udt_std_id
        
    DECLARE @ProcName                   udt_std_desc_short
    SET @ProcName = 'uspShopDocumentInsDetail'

    -- Validate that either an NTUserID or UserID was passed in 
    
    IF @UserID IS NULL
    BEGIN
       -- No user passed in

        RAISERROR('%s: @UserID required', 16, 1, @ProcName)
        RETURN
    END
    

    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid UserID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    IF NOT EXISTS(SELECT  ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID)
    BEGIN
    
            RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
            RETURN
    END

    -- Validate ImageDate
    
    IF @ImageDate IS NOT NULL
    BEGIN
        IF ISDATE(@ImageDate) = 0
        BEGIN
            -- Invalid ImageDate

            RAISERROR('101|%s|@ImageDate|%s', 16, 1, @ProcName, @ImageDate)
            RETURN
        END
        ELSE
        BEGIN
           SET @ImageDateWork = CONVERT(DATETIME, @ImageDate)
        END
    END


    -- Check Document Type
    
    SELECT  @DocumentTypeID = DocumentTypeID
      FROM  dbo.utb_document_type 
      WHERE Name = @DocumentType
        AND EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @DocumentTypeID IS NULL
    BEGIN
        -- Invalid Document Type
        
        RAISERROR('101|%s|@DocumentType|%s', 16, 1, @ProcName, @DocumentType)
        RETURN
    END


    -- Check Document Source
    
    SELECT  @DocumentSourceID = DocumentSourceID
      FROM  dbo.utb_document_source 
      WHERE Name = @DocumentSource
        AND EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @DocumentSourceID IS NULL
    BEGIN
        -- Invalid Document Source
        
        RAISERROR('101|%s|@DocumentSource|%s', 16, 1, @ProcName, @DocumentSource)
        RETURN
    END
            

    -- Check DirectionalCD
    
    IF @DirectionalCD IS NULL
    BEGIN
        SET @DirectionalCD = 'I'
    END
    
    IF Upper(@DirectionalCD) NOT IN ('I', 'O')
    BEGIN
        RAISERROR('101|%s|@DirectionalCD|%s', 16, 1, @ProcName, @DirectionalCD)
        RETURN
    END

    SET @CreatedUserID = @UserID
     
    SELECT  @CreatedUserRoleID = RoleID 
      FROM  dbo.utb_user_role
      WHERE UserID = @UserID 
        AND PrimaryRoleFlag = 1
        
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
        
    SELECT  @CreatedUserSupervisorFlag = SupervisorFlag 
      FROM  dbo.utb_user u 
      WHERE UserID = @UserID 

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET @now = CURRENT_TIMESTAMP

    -- Begin insert
    
    BEGIN TRANSACTION InsertDocument
    
    INSERT INTO utb_Document
        (CreatedUserID, 
         CreatedUserRoleID, 
         DocumentSourceID, 
         DocumentTypeID,
         CreatedDate, 
         CreatedUserSupervisorFlag, 
         DirectionalCD, 
         EnabledFlag,
         ImageLocation, 
         ImageType, 
         ReceivedDate,
         Note, 
         SysLastUserID, 
         SysLastUpdatedDate)
      VALUES (@CreatedUserID, 
              @CreatedUserRoleID, 
              @DocumentSourceID, 
              @DocumentTypeID,
              @now, 
              @CreatedUserSupervisorFlag, 
              @DirectionalCD, 
              1,    -- Set document as enabled
              @ImageLocation, 
              @ImageType, 
              @ImageDateWork, 
              @Comments,
              @CreatedUserID, 
              @Now)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    ELSE
    BEGIN
        SELECT @DocumentID = SCOPE_IDENTITY()
    END
    

    INSERT INTO dbo.utb_shop_location_document
        (ShopLocationID,
        DocumentID,
        EffectiveDate,
        ExpirationDate,
        SysLastUserID,
        SysLastUpdatedDate
        )
      VALUES (@ShopLocationID, 
              @DocumentID, 
              @EffectiveDate,
              @ExpirationDate,
              @CreatedUserID, 
              @Now)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_shop_location_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
            
    
    COMMIT TRANSACTION InsertDocument

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              


    RETURN @documentID 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopDocumentInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopDocumentInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/