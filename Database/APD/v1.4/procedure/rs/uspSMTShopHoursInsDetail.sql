-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopHoursInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopHoursInsDetail 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopHoursInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert new Shop Location Operating Hours
*
* PARAMETERS:  
* (I) @ShopID                             The Shop Location's unique identity
* (I) @OperatingMondayStartTime           The Shop Location's Monday Open Time
* (I) @OperatingMondayEndTime             The Shop Location's Monday Close Time
* (I) @OperatingTuesdayStartTime          The Shop Location's Tuesday Open Time
* (I) @OperatingTuesdayEndTime            The Shop Location's Tuesday Close Time
* (I) @OperatingWednesdayStartTime        The Shop Location's Wednesday Open Time
* (I) @OperatingWednesdayEndTime          The Shop Location's Wednesday Close Time
* (I) @OperatingThursdayStartTime         The Shop Location's Thursday Open Time
* (I) @OperatingThursdayEndTime           The Shop Location's Thursday Close Time
* (I) @OperatingFridayStartTime           The Shop Location's Friday Open Time
* (I) @OperatingFridayEndTime             The Shop Location's Friday Close Time
* (I) @OperatingSaturdayStartTime         The Shop Location's Saturday Open Time
* (I) @OperatingSaturdayEndTime           The Shop Location's Saturday Close Time
* (I) @OperatingSundayStartTime           The Shop Location's Sunday Open Time
* (I) @OperatingSundayEndTime             The Shop Location's Sunday Close Time
* (I) @SysLastUserID                      The Shop Location's updating user identity
*
* RESULT SET:
* ShopLocationID                          The Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopHoursInsDetail
(
    @ShopID                             udt_std_id_big,
    @OperatingMondayStartTime           udt_std_time=NULL,
    @OperatingMondayEndTime             udt_std_time=NULL,
    @OperatingTuesdayStartTime          udt_std_time=NULL,
    @OperatingTuesdayEndTime            udt_std_time=NULL,
    @OperatingWednesdayStartTime        udt_std_time=NULL,
    @OperatingWednesdayEndTime          udt_std_time=NULL,
    @OperatingThursdayStartTime         udt_std_time=NULL,
    @OperatingThursdayEndTime           udt_std_time=NULL,
    @OperatingFridayStartTime           udt_std_time=NULL,
    @OperatingFridayEndTime             udt_std_time=NULL,
    @OperatingSaturdayStartTime         udt_std_time=NULL,
    @OperatingSaturdayEndTime           udt_std_time=NULL,
    @OperatingSundayStartTime           udt_std_time=NULL,
    @OperatingSundayEndTime             udt_std_time=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error            INT
    DECLARE @rowcount         INT

    DECLARE @ProcName         VARCHAR(30)       -- Used for raise error stmts 
    DECLARE @Name             udt_std_name
    DECLARE @LogComment       udt_std_desc_long 

    SET @ProcName = 'uspSMTShopHoursInsDetail'
    SET @Name = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopID)

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@OperatingMondayStartTime))) = 0 SET @OperatingMondayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayEndTime))) = 0 SET @OperatingMondayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayStartTime))) = 0 SET @OperatingTuesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayEndTime))) = 0 SET @OperatingTuesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayStartTime))) = 0 SET @OperatingWednesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayEndTime))) = 0 SET @OperatingWednesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayStartTime))) = 0 SET @OperatingThursdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayEndTime))) = 0 SET @OperatingThursdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayStartTime))) = 0 SET @OperatingFridayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayEndTime))) = 0 SET @OperatingFridayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayStartTime))) = 0 SET @OperatingSaturdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayEndTime))) = 0 SET @OperatingSaturdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayStartTime))) = 0 SET @OperatingSundayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayEndTime))) = 0 SET @OperatingSundayEndTime = NULL
       

    -- Apply edits
    
    IF @ShopID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID
                                                                      AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location
            RAISERROR('101|%s|@ShopID|%u', 16, 1, @ProcName, @ShopID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop Location
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopID)
        RETURN
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

        BEGIN TRANSACTION AdmShopLocationInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    -- Begin Audit Log code  -----------------------------------------------------------------------------------------
    
    
    
    IF @OperatingMondayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Monday Start time established: ' + @OperatingMondayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF @OperatingMondayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Monday End time established: ' + @OperatingMondayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END         
    
    IF @OperatingTuesdayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Tuesday Start time established: ' + @OperatingTuesdayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    IF @OperatingTuesdayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Tuesday End time established: ' + @OperatingTuesdayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    IF @OperatingWednesdayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Wednesday Start time established: ' + @OperatingWednesdayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    IF @OperatingWednesdayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Wednesday End time established: ' + @OperatingWednesdayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
          
      
    IF @OperatingThursdayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Thursday Start time established: ' + @OperatingThursdayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF @OperatingThursdayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Thursday End time established: ' + @OperatingThursdayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @OperatingFridayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Friday Start time established: ' + @OperatingFridayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
         
    
    IF @OperatingFridayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Friday End time established: ' + @OperatingFridayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END     
       
      
    IF @OperatingSaturdayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Saturday Start time established: ' + @OperatingSaturdayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END        
        
    
    IF @OperatingSaturdayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Saturday End time established: ' + @OperatingSaturdayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @OperatingSundayStartTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Sunday Start time established: ' + @OperatingSundayStartTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END         
      
    
    IF @OperatingSundayEndTime IS NOT NULL
    BEGIN
      SET @LogComment = 'Sunday End time established: ' + @OperatingSundayEndTime
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END           
        
    
    
    -- End Audit Log Code  -------------------------------------------------------------------------------------------


    INSERT INTO dbo.utb_shop_location_hours
   	(
        ShopLocationID,
        OperatingFridayEndTime,
        OperatingFridayStartTime,
        OperatingMondayEndTime,
        OperatingMondayStartTime,
        OperatingSaturdayEndTime,
        OperatingSaturdayStartTime,
        OperatingSundayEndTime,
        OperatingSundayStartTime,
        OperatingThursdayEndTime,
        OperatingThursdayStartTime,
        OperatingTuesdayEndTime,
        OperatingTuesdayStartTime,
        OperatingWednesdayEndTime,
        OperatingWednesdayStartTime,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @ShopID,
        dbo.ufnUtilityPadChars( @OperatingFridayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingFridayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingMondayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingMondayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingSaturdayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingSaturdayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingSundayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingSundayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingThursdayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingThursdayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingTuesdayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingTuesdayStartTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingWednesdayEndTime, 4, '0', 1 ),
        dbo.ufnUtilityPadChars( @OperatingWednesdayStartTime, 4, '0', 1 ),
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_location_hours', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION AdmShopLocationInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    --SELECT @ShopID AS ShopID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopHoursInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopHoursInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
