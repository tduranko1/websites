-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptAuditedQCCheck' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptAuditedQCCheck 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptAuditedQCCheck
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a list of Audited estimates that are different from the electronic version.
*
* PARAMETERS:  
*
* RESULT SET:
*   Returns a list of Audited estimates that are different from the electronic version.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptAuditedQCCheck
AS
BEGIN
   set nocount on

   DECLARE @ReportStart as datetime
   DECLARE @ReportEnd as datetime
   DECLARE @now as datetime

   DECLARE @EstimateSummaryTypeID_Repair int
   DECLARE @EstimateSummaryTypeID_Betterment int
   DECLARE @EstimateSummaryTypeID_Deductible int
   DECLARE @EstimateSummaryTypeID_OtherAdj int
   DECLARE @EstimateSummaryTypeID_Net int


   SET @now = CURRENT_TIMESTAMP
   SET @ReportEnd = convert(datetime, convert(varchar, @now, 101))
   SET @ReportStart = dateadd(day, -3, @ReportEnd)
   SET @ReportEnd = dateadd(second, -1, @ReportEnd)

   -- SET @ReportStart = convert(datetime, '3/13/2009')
   -- SET @ReportEnd = convert(datetime, '3/21/2009')

   -- select @ReportStart, @ReportEnd


   SELECT @EstimateSummaryTypeID_Repair = EstimateSummaryTypeID 
   FROM  dbo.utb_estimate_summary_type 
   WHERE CategoryCD = 'TT' 
     AND Name = 'RepairTotal'
           
   SELECT @EstimateSummaryTypeID_Betterment = EstimateSummaryTypeID 
   FROM  dbo.utb_estimate_summary_type 
   WHERE CategoryCD = 'AJ' 
     AND Name = 'Betterment'
     
   SELECT @EstimateSummaryTypeID_Deductible = EstimateSummaryTypeID 
   FROM  dbo.utb_estimate_summary_type 
   WHERE CategoryCD = 'AJ' 
     AND Name = 'Deductible'
     
   SELECT @EstimateSummaryTypeID_OtherAdj = EstimateSummaryTypeID 
   FROM  dbo.utb_estimate_summary_type 
   WHERE CategoryCD = 'AJ' 
     AND Name = 'Other'
     
   SELECT @EstimateSummaryTypeID_Net = EstimateSummaryTypeID 
   FROM  dbo.utb_estimate_summary_type 
   WHERE CategoryCD = 'TT' 
     AND Name = 'NetTotal'

   -- Get all audited estimates during this period
   DECLARE @tmpAuditedDocument TABLE (
	   tmpDocumentID                 bigint,
	   ClaimAspectServiceChannelID   bigint,
	   LynxID                        bigint,
	   ClaimAspectNumber             int,
	   ServiceChannelCD              varchar(2),
	   CreatedDate                   datetime,
	   DocumentTypeID                int,
	   DocumentSourceID              int,
	   VANFlag                       bit,
	   DuplicateFlag                 bit,
	   EstimateTypeCD                varchar(2),
	   SupplementSeqNumber           tinyint,
	   RepairTotal                   decimal(10,2) NULL,
	   BettermentAmt                 decimal(10,2) NULL,
	   DeductibleAmt                 decimal(10,2) NULL,
	   OtherAdjustmentAmt            decimal(10,2) NULL,
	   NetTotal                      decimal(10,2) NULL
   )

   DECLARE @tmpOriginalDocument TABLE (
	   tmpDocumentID                 bigint,
	   ClaimAspectServiceChannelID   bigint,
	   CreatedDate                   datetime,
	   DocumentTypeID                int,
	   DocumentSourceID              int,
	   VANFlag                       bit,
	   DuplicateFlag                 bit,
	   EstimateTypeCD                varchar(2),
	   SupplementSeqNumber           tinyint,
	   RepairTotal                   decimal(10,2) NULL,
	   BettermentAmt                 decimal(10,2) NULL,
	   DeductibleAmt                 decimal(10,2) NULL,
	   OtherAdjustmentAmt            decimal(10,2) NULL,
	   NetTotal                      decimal(10,2) NULL
   )

   DECLARE @tmpElectronicAuditedDocument TABLE (
	   tmpDocumentID                 bigint,
	   ClaimAspectServiceChannelID   bigint,
	   CreatedDate                   datetime,
	   DocumentTypeID                int,
	   DocumentSourceID              int,
	   VANFlag                       bit,
	   DuplicateFlag                 bit,
	   EstimateTypeCD                varchar(2),
	   SupplementSeqNumber           tinyint,
	   RepairTotal                   decimal(10,2) NULL,
	   BettermentAmt                 decimal(10,2) NULL,
	   DeductibleAmt                 decimal(10,2) NULL,
	   OtherAdjustmentAmt            decimal(10,2) NULL,
	   NetTotal                      decimal(10,2) NULL
   )


   INSERT INTO @tmpAuditedDocument (
	   tmpDocumentID,
	   ClaimAspectServiceChannelID,
	   LynxID,
	   ClaimAspectNumber,
	   ServiceChannelCD,
	   CreatedDate,
	   DocumentTypeID,
	   DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   SupplementSeqNumber
   ) 
   SELECT
 	   d.DocumentID,
 	   casc.ClaimAspectServiceChannelID,
 	   ca.LynxID,
 	   ca.ClaimAspectNumber,
 	   casc.ServiceChannelCD,
	   d.CreatedDate,
	   DocumentTypeID,
	   ds.DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   SupplementSeqNumber
   FROM utb_document d
   LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
   LEFT JOIN utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
   LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   WHERE d.CreatedDate between @ReportStart and @ReportEnd
     AND EstimateTypeCD = 'A'
     AND ds.VANFlag = 0
     
   INSERT INTO @tmpOriginalDocument (
	   tmpDocumentID,
      ClaimAspectServiceChannelID,
	   CreatedDate,
	   DocumentTypeID,
	   DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   SupplementSeqNumber
   ) 
   SELECT
 	   d.DocumentID,
 	   cascd.ClaimAspectServiceChannelID,
	   d.CreatedDate,
	   d.DocumentTypeID,
	   ds.DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   d.SupplementSeqNumber
   FROM (select distinct ClaimAspectServiceChannelID,
                         DocumentTypeID,
                         SupplementSeqNumber
         from @tmpAuditedDocument) tmpD
   LEFT JOIN utb_claim_aspect_service_channel_document cascd on tmpD.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
   left join utb_document d on cascd.DocumentID = d.DocumentID
   LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
   WHERE tmpD.DocumentTypeID = d.DocumentTypeID
     AND tmpD.SupplementSeqNumber = d.SupplementSeqNumber
     AND EstimateTypeCD = 'O'
     AND d.EnabledFlag = 1

     
   INSERT INTO @tmpElectronicAuditedDocument (
	   tmpDocumentID,
      ClaimAspectServiceChannelID,
	   CreatedDate,
	   DocumentTypeID,
	   DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   SupplementSeqNumber
   ) 
   SELECT
 	   d.DocumentID,
 	   cascd.ClaimAspectServiceChannelID,
	   d.CreatedDate,
	   DocumentTypeID,
	   ds.DocumentSourceID,
	   VANFlag,
	   DuplicateFlag,
	   EstimateTypeCD,
	   SupplementSeqNumber
   FROM utb_document d
   LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
   LEFT JOIN utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
   WHERE cascd.ClaimAspectServiceChannelID in (SELECT distinct ClaimAspectServiceChannelID
                                                FROM @tmpAuditedDocument)
     AND EstimateTypeCD = 'A'
     AND d.EnabledFlag = 1
     AND ds.VANFlag = 1
     
   UPDATE @tmpAuditedDocument
   SET RepairTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Repair

   UPDATE @tmpAuditedDocument
   SET BettermentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Betterment

   UPDATE @tmpAuditedDocument
   SET DeductibleAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Deductible

   UPDATE @tmpAuditedDocument
   SET OtherAdjustmentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_OtherAdj

   UPDATE @tmpAuditedDocument
   SET NetTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Net

   -- update the original document data
   UPDATE @tmpOriginalDocument
   SET RepairTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Repair

   UPDATE @tmpOriginalDocument
   SET BettermentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Betterment

   UPDATE @tmpOriginalDocument
   SET DeductibleAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Deductible

   UPDATE @tmpOriginalDocument
   SET OtherAdjustmentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_OtherAdj

   UPDATE @tmpOriginalDocument
   SET NetTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Net
     
   -- Update the Electronic Audited Data  
   UPDATE @tmpElectronicAuditedDocument
   SET RepairTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Repair

   UPDATE @tmpElectronicAuditedDocument
   SET BettermentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Betterment

   UPDATE @tmpElectronicAuditedDocument
   SET DeductibleAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Deductible

   UPDATE @tmpElectronicAuditedDocument
   SET OtherAdjustmentAmt = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_OtherAdj

   UPDATE @tmpElectronicAuditedDocument
   SET NetTotal = OriginalExtendedAmt
   FROM utb_estimate_summary es
   WHERE tmpDocumentID = es.DocumentID
     AND es.EstimateSummaryTypeID = @EstimateSummaryTypeID_Net
     
     
   /*SELECT * FROM @tmpAuditedDocument
   SELECT * FROM @tmpOriginalDocument
   SELECT * FROM @tmpElectronicAuditedDocument
   */
   /*select * from @tmpAuditedDocument
   where ClaimAspectServiceChannelID not in (select distinct ClaimAspectServiceChannelID from @tmpOriginalDocument)*/
   /*select top 100 *
   from utb_document
   where EstimateTypeCD = 'A'
   order by documentID desc*/

   print 'Audit difference QC check ' + convert(varchar, @ReportStart, 101) + ' to ' + convert(varchar, @ReportEnd, 101)
   print '--------------------------------------------------'
   print ''
   SELECT convert(varchar(9), LynxID) + '-' + convert(varchar(2), ClaimAspectNumber) as 'Lynx ID',
          case
            when ta.DocumentTypeID = 3 then 'E'
            when ta.DocumentTypeID = 10 then 'S'
          end + convert(varchar(2), ta.SupplementSeqNumber) as 'Document',
          convert(varchar(12), ta.RepairTotal) + ' / ' + convert(varchar(12), eat.RepairTotal) + ' / ' + isNull(convert(varchar(12), ot.RepairTotal), 'Missing') as 'Repair Total (A/EA/O)',
          convert(varchar(12), ta.BettermentAmt) + ' / ' + convert(varchar(12), eat.BettermentAmt) + ' / ' + isNull(convert(varchar(12), ot.BettermentAmt), 'Missing') as 'Betterment (A/EA/O)',
          convert(varchar(12), ta.DeductibleAmt) + ' / ' + convert(varchar(12), eat.DeductibleAmt) + ' / ' + isNull(convert(varchar(12), ot.DeductibleAmt), 'Missing') as 'Deductible (A/EA/O)',
          convert(varchar(12), ta.OtherAdjustmentAmt) + ' / ' + convert(varchar(12), eat.OtherAdjustmentAmt) + ' / ' + isNull(convert(varchar(12), ot.OtherAdjustmentAmt), 'Missing') as 'Other Adjustments (A/EA/O)',
          convert(varchar(12), ta.NetTotal) + ' / ' + convert(varchar(12), eat.NetTotal) + ' / ' + isNull(convert(varchar(12), ot.NetTotal), 'Missing') as 'Net Total (A/EA/O)'
   FROM @tmpAuditedDocument ta
   LEFT JOIN @tmpOriginalDocument ot ON ta.ClaimAspectServiceChannelID = ot.ClaimAspectServiceChannelID AND ta.DocumentTypeID = ot.DocumentTypeID AND ta.SupplementSeqNumber = ot.SupplementSeqNumber
   LEFT JOIN @tmpElectronicAuditedDocument eat ON ta.ClaimAspectServiceChannelID = eat.ClaimAspectServiceChannelID and ta.DocumentTypeID = eat.DocumentTypeID and ta.SupplementSeqNumber = eat.SupplementSeqNumber
   WHERE ta.RepairTotal <> eat.RepairTotal 
      OR ta.BettermentAmt <> eat.BettermentAmt
      OR ta.DeductibleAmt <> eat.DeductibleAmt
      OR ta.OtherAdjustmentAmt <> eat.OtherAdjustmentAmt
      OR ta.NetTotal <> eat.NetTotal

   print '----- END -----'

END



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptAuditedQCCheck' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptAuditedQCCheck TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
