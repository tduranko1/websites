-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMEClaimActivity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptMEClaimActivity 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure

/************************************************************************************************************************
*
* PROCEDURE:    uspRptMEClaimActivity
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Report CF shops.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMEClaimActivity
AS
BEGIN
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName                       AS VARCHAR(30)

    SET @ProcName = 'uspRptMEClaimActivity'  

    set nocount on

    declare @FromDate as datetime
    declare @ToDate as datetime
    declare @yesterday as varchar(10)
    set @yesterday = convert(varchar, dateadd(d, -1, current_timestamp), 101)
    set @FromDate = convert(datetime, @yesterday + ' 00:00')
    set @ToDate = convert(datetime, @yesterday + ' 23:59:59')
    
    SELECT 'Monthy total for ' + DateName(month, @FromDate) + ' ' + DateName(year, @FromDate)

    select count(ca.LynxID) as 'Monthy total'
    from utb_claim_aspect ca
    where ca.InitialAssignmentTypeID = 11 -- Initial ME Assignment
      and ca.LynxID not in (396227, 396240) -- igonore crap claims
      and Month(CreatedDate) = Month(@FromDate)
      and Year(CreatedDate) = Year(@FromDate)
    
    SELECT ''

    SELECT 'ME Claims: ' + @yesterday + ' 00:00 to ' + @yesterday + ' 23:59:59' AS 'Daily ME Activity Report'
    
    SELECT ' '

    -- output this so that the report will be generated even if no claims during
    -- the period.
    select count(ca.LynxID) as 'ME Claims total'
    from utb_claim_aspect ca
    where ca.InitialAssignmentTypeID = 11 -- Initial ME Assignment
      and ca.LynxID not in (396227, 396240) -- igonore crap claims
      and CreatedDate between @FromDate and @ToDate

    SELECT ''

    select distinct convert(varchar(7), ca.LynxID) as 'Lynx ID',
	    case
	      when CharIndex('Glass Damage indicated but not dispatched', AssignmentRemarks) > 0 then 'Yes'
	      when CharIndex('Dispatch', AssignmentRemarks) > 0  then 'Yes'
	      else 'No'
	    end as hasGlassDamage,
	    case
	      when CharIndex('Glass Damage indicated but not dispatched', AssignmentRemarks) > 0 then 'Not Dispatched'
	      when CharIndex('Dispatch', AssignmentRemarks) > 0 then substring(AssignmentRemarks, CharIndex('Dispatch', AssignmentRemarks) + 9, 9)
	      else ''
	    end as 'Glass Dispatch',
	    convert(varchar(10), ca.CreatedDate, 101) as 'DateCreated',
	    convert(varchar(18), iu.NameFirst + ' ' + iu.NameLast) as 'FNOL Handler',
	    convert(varchar(15), cu.NameFirst + ' ' + cu.NameLast) as 'APD Claim Owner'
    from utb_assignment a
    left join utb_claim_aspect_service_channel casc on (a.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
    left join utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
    left join utb_claim c on (ca.LynxID = c.LynxID)
    left join utb_user cu on (ca.OwnerUserID = cu.UserID)
    left join utb_user iu on (c.IntakeUserID = iu.UserID)
    where ca.InitialAssignmentTypeID = 11 -- Initial ME Assignment
     AND a.assignmentsequencenumber = 1 
      and ca.LynxID not in (396227, 396240) -- igonore crap claims
      and ca.CreatedDate between @FromDate and @ToDate

    SELECT ''
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting ME Activity', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMEClaimActivity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMEClaimActivity TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

