-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLClearLoadTables' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFNOLClearLoadTables
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFNOLClearLoadTables
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Clears any existing load tables records matching the LynxID
*
* PARAMETERS:  
* (I) @LynxID               LynxID to clear from the load tables
*
* RESULT SET:
* None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFNOLClearLoadTables
    @LynxID     udt_std_id_big
AS
BEGIN
    -- This proc clears all load records for @LynxID from the load tables.  This is a safeguard mechanism so that resubmits
    -- from errors can be handled correctly
    
    BEGIN TRANSACTION FNOLLoadClearTrans1
    
    DELETE FROM dbo.utb_fnol_claim_load
      WHERE LynxID = @LynxiD

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Failure
        ROLLBACK TRANSACTION
        RAISERROR('Error clearing record(s) from utb_fnol_claim_load.', 16, 1)
        RETURN
    END


    DELETE FROM dbo.utb_fnol_claim_vehicle_load
      WHERE LynxID = @LynxiD

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Failure
        ROLLBACK TRANSACTION
        RAISERROR('Error clearing record(s) from utb_fnol_claim_vehicle_load.', 16, 1)
        RETURN
    END


    DELETE FROM dbo.utb_fnol_involved_load
      WHERE LynxID = @LynxiD

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Failure
        ROLLBACK TRANSACTION
        RAISERROR('Error clearing record(s) from utb_fnol_involved_load.', 16, 1)
        RETURN
    END


    DELETE FROM dbo.utb_fnol_claim_coverage_load
      WHERE LynxID = @LynxiD

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Failure
        ROLLBACK TRANSACTION
        RAISERROR('Error clearing record(s) from utb_fnol_claim_coverage_load.', 16, 1)
        RETURN
    END


    DELETE FROM dbo.utb_fnol_property_load
      WHERE LynxID = @LynxiD

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Failure
        ROLLBACK TRANSACTION
        RAISERROR('Error clearing record(s) from utb_fnol_property_load.', 16, 1)
        RETURN
    END

    COMMIT TRANSACTION FNOLLoadClearTrans1
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLClearLoadTables' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLClearLoadTables TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There WAS an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/