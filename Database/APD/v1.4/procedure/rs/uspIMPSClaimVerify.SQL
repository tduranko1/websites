-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIMPSClaimVerify' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspIMPSClaimVerify 
END

GO

SET ANSI_NULLS ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspIMPSClaimVerify
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     This procedure is used by IMPS to verify that a document and it's given 'Applies To'
*               exists.
*
* PARAMETERS:
* (I) @LynxID               LynxID
* (O) @AppliesTo            Entity  ('Vehicle', 'Property', etc.)
* (I) @Number               Number for Applies to (1, 2, etc)  Vehicle Number, Property Number, ect.
*
* RESULT SET:
* count of matching records.  This should only be 1 unless the entity doesn't exist.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspIMPSClaimVerify
    @LynxID     udt_std_id_big,
    @AppliesTo  udt_std_desc_short,
    @Number     udt_std_int

AS
BEGIN
    declare @Entity varchar(10)
    if upper(@AppliesTo) = 'CLM' 
    BEGIN
        SET @Entity = 'clm'
    END
    ELSE
    BEGIN
        SET @Entity = @AppliesTo + convert(varchar(5), @Number)
    END
     select
         count(*)

     from
         dbo.ufnUtilityGetClaimEntityList(@LynxID, 0, 0)  -- (0, 0) means all aspects, open or closed

     WHERE
         EntityCode = @Entity
end
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIMPSClaimVerify' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspIMPSClaimVerify TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/