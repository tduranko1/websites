-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionExists' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionExists 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionExists]    Script Date: 08/08/2014 12:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionExists
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Retrieves session key for the specified session key (to determine if the session exists)
*
* UPDATES:						1.1 - 20Mar2017 - Sergey - Modified to fix deadlock issues
*
* PARAMETERS:  
* (I) @SessionID                The session key
*
* RESULT SET:
* SessionID                     The session key
*
*
* VSS
* $Workfile: uspSessionExists.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionExists.sql $
* $Revision: 1 $
* $Author: Jim $
* $Date: 10/12/01 12:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionExists]
    @SessionID         AS udt_session_id
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    SET NOCOUNT ON
    
    -- Declare internal variables
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
	SET @trancount = @@TRANCOUNT
   
    -- Start the new transaction
	BEGIN TRANSACTION
    
	SELECT SessionID
	FROM dbo.utb_session
	WHERE SessionID = @SessionID

	-- Capture any error
	SELECT @error = @@ERROR

    IF 0 = @error
    BEGIN
        -- Update the "Last Access Date/Time"
        EXECUTE dbo.uspSessionUpd @SessionID

        -- Capture any error
        SELECT @error = @@ERROR
    END
    
    -- Final transaction success determination
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionExists' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionExists TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO