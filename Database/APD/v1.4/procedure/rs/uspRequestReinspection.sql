-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRequestReinspection' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRequestReinspection 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRequestReinspection
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRequestReinspection
(
    @DocumentID     bigint,
    @Comments       varchar(1000),
    @UserID         int
)
AS
BEGIN
    DECLARE @now                    AS datetime
    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @EventIDReIRequested    AS int
    DECLARE @EventNameReIRequested  AS varchar(100)
    DECLARE @ClaimAspectID          AS bigint
    DECLARE @VehicleStatusID        AS int
    DECLARE @NoteTypeIDClaim        AS int
    DECLARE @NotesComment           AS varchar(150)
    DECLARE @ShopLocationID         AS bigint

    SET @ProcName = 'uspRequestReinspection'


    -- Set Database options
    SET NOCOUNT ON
    
    IF LEN(RTRIM(LTRIM(@Comments))) = 0 SET @Comments = NULL

    -- Validate Parameter List

    -- Check to make sure a valid User id was passed in
    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Check to make sure the document id is valid
    IF NOT EXISTS(SELECT DocumentID
                    FROM dbo.utb_document
                    WHERE DocumentID = @DocumentID)
    BEGIN
        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
        RETURN
    END

    -- Get Note Type ID for Claim
    SELECT  @NoteTypeIDClaim = NoteTypeID
      FROM  dbo.utb_note_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @NoteTypeIDClaim IS NULL
    BEGIN
       -- Note Type ID for Claim

        RAISERROR('102|%s|"Note Type: Claim"|utb_note_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT @EventIDReIRequested = EventID,
           @EventNameReIRequested = Name
      FROM dbo.utb_event
      WHERE Name = 'Reinspection Requested'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectID = casc.ClaimAspectID
    FROM utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    WHERE cascd.DocumentID = @DocumentID
    AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Get StatusID for Vehicle
    SELECT  @VehicleStatusID = cas.StatusID,
            @NotesComment = 'Reinspection has been requested for ' + cat.Name + convert(varchar, ca.ClaimAspectNumber)
      FROM  dbo.utb_claim_aspect ca
	  /*********************************************************************************
	  Project: 210474 APD - Enhancements to support multiple concurrent service channels
	  Note:	Added reference to utb_Claim_Aspect_Status to get the value of "StatusID"
		    M.A. 20061117
      *********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
      LEFT JOIN dbo.utb_Claim_Aspect_Type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
      WHERE ca.ClaimAspectID = @ClaimAspectID
      and ServiceChannelCD is null
      and StatusTypeCD is null

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Joined utb_Claim_Aspect_Service_Channel to utb_assignment to provide
		the ClaimAspectID referenced in the WHERE clause
		M.A. 20061113
	*********************************************************************************/
    SELECT top 1 @ShopLocationID = a.ShopLocationID
    FROM dbo.utb_assignment a
    INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    WHERE casc.ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
      AND a.CancellationDate IS NULL
    ORDER BY a.AssignmentDate DESC

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
    
    BEGIN TRANSACTION
    
    UPDATE dbo.utb_document
    SET ReinspectionRequestComment = @Comments,
        ReinspectionRequestDate = @now,
        ReinspectionRequestFlag = 1,
        ReinspectionRequestingUserID = @UserID,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE DocumentID = @DocumentID
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure
        RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- Add a note with the comments supplied
    EXEC uspNoteInsDetail @ClaimAspectID, @NoteTypeIDClaim, @VehicleStatusID, @NotesComment, @UserID
    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR('104|%s|%s', 16, 1, @ProcName, 'uspNoteInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END    

    -- Now throw the Reinspection requested event
    EXEC uspWorkflowNotifyEvent @EventID = @EventIDReIRequested,
                                @ClaimAspectID = @ClaimAspectID,
                                @Description = @EventNameReIRequested,
                                @UserID = @UserID

    IF @@ERROR <> 0
    BEGIN
        -- Error notifying APD of event

        RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventNameReIRequested)
        ROLLBACK TRANSACTION
        RETURN
    END    

    COMMIT TRANSACTION
    
    SELECT  1    AS Tag,
            NULL AS Parent,
            -- Root
            @DocumentID AS [Root!1!DocumentID],
            @now AS [Root!1!SysLastUpdatedDate],
            -- Reinspection details
            NULL AS [Reinspection!2!LynxID],
            NULL AS [Reinspection!2!ClaimAspectNumber],
            NULL AS [Reinspection!2!UserNameFirst],
            NULL AS [Reinspection!2!UserNameLast],
            NULL AS [Reinspection!2!ShopName],
            NULL AS [Reinspection!2!ShopCity],
            NULL AS [Reinspection!2!ProgramManagerEmailAddress]

    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL, NULL,
            -- Reinspection details
            ca.LynxID,
            ca.ClaimAspectNumber,
            (SELECT NameFirst FROM dbo.utb_user WHERE UserID = @UserID),
            (SELECT NameLast FROM dbo.utb_user WHERE UserID = @UserID),
            (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID),
            (SELECT AddressCity FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID),
            (SELECT u.EmailAddress 
                FROM dbo.utb_user u
                LEFT JOIN dbo.utb_shop_location sl ON (u.UserID = sl.ProgramManagerUserID)
                WHERE sl.ShopLocationID = @ShopLocationID)
    FROM utb_claim_aspect ca
    WHERE ca.ClaimAspectID = @ClaimAspectID
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRequestReinspection' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRequestReinspection TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/