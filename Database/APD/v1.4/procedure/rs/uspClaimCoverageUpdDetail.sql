-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCoverageUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCoverageUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu/M. Ahmed
* FUNCTION:     Updates Coverage data into usp_claim_coverage Table (Coverage Info Tab on Claim Detail Screen).  If no 
*               coverage record exists, this proc will insert one and link it to the claim.
*
* PARAMETERS:
* (I) @ClaimCoverageID.......The row id to be used for the update of a particular row in the table. For new row this is zero
* (I) @ClientCoverageTypeID..The client's coverage type id from utb_Client_Coverage_Type
* (I) @LynxID................Lynx Id for the claim
* (I) @InsuranceCompanyID....The Insurance Company ID for the claim
* (I) @AddtlCoverageFlag.....The additional coverage flag
* (I) @CoverageTypeCD........The coverage type code for the row
* (I) @Description...........The description for the coverage
* (I) @DeductibleAmt.........The deductible amount for the coverage
* (I) @LimitAmt..............The coverage limit amount
* (I) @LimitDailyAmt.........The daily limit amount for a rental car
* (I) @MaximumDays...........The maximum number of days that the claimant can rent a car
* (I) @UserID................The user updating the note
* (I) @SysLastUpdatedDate....The "previous" updated date for coverage information
*
* RESULT SET:
*       An XML document containing last updated date time values for Coverage
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimCoverageUpdDetail
	@ClaimCoverageID			  		udt_std_id_big,
	@ClientCoverageTypeID		  	udt_std_id = null,
   @LynxID                       udt_std_int_big,
	@InsuranceCompanyID			  	udt_std_int_small,
	@AddtlCoverageFlag			  	udt_std_flag,
	@CoverageTypeCD				  	udt_std_cd,
	@Description				  		udt_std_desc_mid = null,
	@DeductibleAmt				  		decimal(9,2) = null,
	@LimitAmt					  		decimal(9,2) = null,
	@LimitDailyAmt				  		decimal(9,2) = null,
	@MaximumDays				  		decimal(9,2) = null,
   @UserID                       udt_std_id,
   @SysLastUpdatedDate           varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @ClaimCoverageIDWork AS bigint

    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspClaimCoverageUpdDetail'

    -- SET NOCOUNT to ON and no longer display the count message or warnings

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF
    
    IF @DeductibleAmt IS NULL SET @DeductibleAmt = 0
    IF @LimitAmt IS NULL SET @LimitAmt = 0
    IF @LimitDailyAmt IS NULL SET @LimitDailyAmt = 0
    IF @MaximumDays IS NULL SET @MaximumDays = 0

	--*******************************************************************
	--Make sure that all the codes and IDs that are being sent are valid.
	--*******************************************************************

	-- 1 -- Verify that the Lynx id is valid and the Insurance Company ID matches the one given against the corresponding claim row
	--Is it a valid Lynx ID?
	 IF NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
    BEGIN
        -- Invalid Lynx ID
	        RAISERROR('10|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END
	--Is it a valid Insurance Company ID?
	 IF (SELECT count(*) FROM dbo.utb_claim WHERE LynxID = @LynxID and InsuranceCompanyID=@InsuranceCompanyID) = 0
    BEGIN
        -- Invalid Insurance Company ID
	        RAISERROR('20|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

	-- 2 -- Verify that the client coverage type id is valid
	/*IF (SELECT count(*) from dbo.utb_client_coverage_type where ClientCoverageTypeID = @ClientCoverageTypeID and InsuranceCompanyID=@InsuranceCompanyID) = 0
    BEGIN
        -- Invalid client coverage type id
	        RAISERROR('30|%s|@ClientCoverageTypeID|%u', 16, 1, @ProcName, @ClientCoverageTypeID)
        RETURN
    END

	-- 3 -- Verify coverage type code is valid
	IF (SELECT count(*) from dbo.utb_client_coverage_type where ClientCoverageTypeID = @ClientCoverageTypeID and InsuranceCompanyID=@InsuranceCompanyID and CoverageProfileCD = @CoverageTypeCD) = 0
    BEGIN
        -- Invalid coverage profile code
	        RAISERROR('40|%s|@CoverageTypeCD|%u', 16, 1, @ProcName, @CoverageTypeCD)
        RETURN
    END*/

    -- 4 -- Check to make sure a valid User id was passed in
    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    
   -- Get current timestamp
   SET @now = CURRENT_TIMESTAMP
	SET @rowcount = 0
	
	IF @ClaimCoverageID > 0 
	BEGIN
		exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_coverage', @ClaimCoverageID
	END

	BEGIN TRANSACTION trnUpdCoverage
	--If the data being passed, has a "ZERO" for the @ClaimCoverageID then we need to insert into the table
	If @ClaimCoverageID = 0 
	Begin --Block I --Start of INSERT into utb_Claim_Coverage
			INSERT into utb_Claim_Coverage
					(
					ClientCoverageTypeID,
					LynxID,
					AddtlCoverageFlag,
					CoverageTypeCD,
					Description,
					DeductibleAmt,
					LimitAmt,
					LimitDailyAmt,
					MaximumDays,
					SysLastUserID,
					SysLastUpdatedDate
					)
			Values	(
					@ClientCoverageTypeID,
				   @LynxID,
					@AddtlCoverageFlag,
					@CoverageTypeCD,
					@Description,
					@DeductibleAmt,
					@LimitAmt,
					@LimitDailyAmt,
					@MaximumDays,
				   @UserID,
					@now
					)
			
			IF @@ERROR <> 0
			BEGIN
				-- Problem inserting a row into the table.
				ROLLBACK tran 
				RAISERROR('102|%s|utb_claim_coverage', 16, 1, @ProcName)
				RETURN
			END
			
			SET @ClaimCoverageIDWork = SCOPE_IDENTITY()
			
	End  --Block I --End of insert into utb_Claim_Coverage

	Else --Update the existing row
	BEGIN --Block II
			UPDATE	utb_Claim_Coverage
			set		DeductibleAmt = @DeductibleAmt,
						LimitAmt = @LimitAmt,
						LimitDailyAmt = @LimitDailyAmt,
						MaximumDays = @MaximumDays,
						SysLastUserID = @UserID,
						SysLastUpdatedDate = @now
			where	ClaimCoverageID = @ClaimCoverageID

			IF @@ERROR <> 0
			BEGIN
			   -- Problem updating a row into the table.
				ROLLBACK tran 
				RAISERROR('103|%s|utb_claim_coverage', 16, 1, @ProcName)
				RETURN
			END
		   SET @ClaimCoverageIDWork = @ClaimCoverageID
	END --Block II

 	COMMIT TRANSACTION trnUpdCoverage

	IF @@ERROR <> 0
	BEGIN
	 -- SQL Server Error
	
	  RAISERROR('99|%s', 16, 1, @ProcName)
	  RETURN
	END


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Coverage Level
				NULL AS [Coverage!2!ClaimCoverageID],
            NULL AS [Coverage!2!SysLastUpdatedDate]


    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- Coverage Level
            @ClaimCoverageIDWork,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCoverageUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
