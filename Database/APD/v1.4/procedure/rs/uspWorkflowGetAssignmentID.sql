-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowGetAssignmentID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowGetAssignmentID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowGetAssignmentID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves the assignment id when passed in the LynxID, VehicleNumber, and AssignmentSuffix.  This proc is
*               required until CCC fixes the BusinessEvent transaction to include echo field data.
*
* PARAMETERS:  
* (I) @LynxID               LynxID
* (I) @VehicleNumber        Vehicle Number
* (I) @AssignmentSuffix     Assignment Suffix
*
* RESULT SET:   Returns AssignmentID as a return value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowGetAssignmentID
    @LynxID             udt_std_id_big,
    @VehicleNumber      udt_std_int_tiny,
    @AssignmentSuffix   udt_std_cd
AS
BEGIN
    -- Declare internal variables

    DECLARE @AssignmentID               AS udt_std_id_big
    DECLARE @ClaimAspectTypeIDVehicle   AS udt_std_id
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWorkflowGetAssignmentID'


    -- Set Database options
    
    SET NOCOUNT ON


    -- Check to make sure a valid Lynx ID and Vehicle Number was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    IF  (@VehicleNumber IS NULL) OR
        (NOT EXISTS(SELECT LynxID, ClaimAspectNumber FROM dbo.utb_claim_aspect WHERE LynxID = @LynxID AND ClaimAspectNumber = @VehicleNumber))
    BEGIN
        -- Invalid Vehicle Number
    
        RAISERROR('101|%s|@VehicleNumber|%u', 16, 1, @ProcName, @VehicleNumber)
        RETURN
    END


    -- Get Claim Aspect Type for "Vehicle"
    
    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        -- Invalid APD Data state
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

      
    -- Try to come up with an AssignmentID

    IF @AssignmentSuffix IS NOT NULL
    BEGIN     
        SELECT  @AssignmentID = a.AssignmentID
          FROM  dbo.utb_claim_aspect ca
			/*********************************************************************************
			Project: 210474 APD - Enhancements to support multiple concurrent service channels
			Note:	Added reference to utb_Claim_Aspect_Service_Channel to join utb_Claim_Aspect
					and utb_Assignment tables
				M.A. 20061124
			*********************************************************************************/
		  INNER JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
          LEFT JOIN dbo.utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
          WHERE ca.LynxID = @LynxID
            AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
            AND ca.ClaimAspectNumber = @VehicleNumber
            AND a.AssignmentSuffix = @AssignmentSuffix
    END
    ELSE
    BEGIN
        SELECT  @AssignmentID = a.AssignmentID
          FROM  dbo.utb_claim_aspect ca
			/*********************************************************************************
			Project: 210474 APD - Enhancements to support multiple concurrent service channels
			Note:	Added reference to utb_Claim_Aspect_Service_Channel to join utb_Claim_Aspect
					and utb_Assignment tables
				M.A. 20061124
			*********************************************************************************/
		  INNER JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
          LEFT JOIN dbo.utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
          WHERE ca.LynxID = @LynxID
            AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
            AND ca.ClaimAspectNumber = @VehicleNumber
            AND a.AssignmentSuffix IS NULL
    END
                   
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @AssignmentID IS NULL
    BEGIN
        -- Assignment not found, simply return nothing

        RETURN
    END
    ELSE
    BEGIN    
        -- Return the Assignment ID found
        
        RETURN @AssignmentID        
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowGetAssignmentID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowGetAssignmentID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
