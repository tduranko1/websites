-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminEnableDisableOffice' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminEnableDisableOffice 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminEnableDisableOffice
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Enables/Disables a client office by setting the EnabledFlag to 0 in the utb_office table.
*
* PARAMETERS:  
*
*	OfficeID
*
* RESULT SET:
*			row updated
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminEnableDisableOffice
	@iOfficeID INT
	, @bIsEnabled BIT
AS
BEGIN
    DECLARE @now AS datetime 
    
    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	
    --Verify that a valid office already exist
    IF (@iOfficeID IS NOT Null) OR
    NOT EXISTS (SELECT OfficeID FROM utb_office WHERE OfficeID = @iOfficeID)
    BEGIN
		UPDATE dbo.utb_office SET EnabledFlag = @bIsEnabled, SysLastUpdatedDate = @now WHERE OfficeID = @iOfficeID

		IF EXISTS(SELECT * FROM utb_office WHERE OfficeID = @iOfficeID AND EnabledFlag = @bIsEnabled)
		BEGIN
			SELECT 1 AS RC
		END
		ELSE
		BEGIN
			SELECT 0 AS RC
		END
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminEnableDisableOffice' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminEnableDisableOffice TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminEnableDisableOffice TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/