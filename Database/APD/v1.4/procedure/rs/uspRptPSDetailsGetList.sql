-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptPSDetailsGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptPSDetailsGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptPSDetailsGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will return a recordset of detailed Program Shop information.
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptPSDetailsGetList
    @InsuranceCompanyID     as udt_std_int = null,
    @OfficeID               as udt_std_int = null,
    @StateCode              udt_addr_state = NULL,    
    @RptMonth               as udt_std_int_small = null,
    @RptYear                as udt_std_int_small = null
AS
BEGIN
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    DECLARE @error                  AS INT
    DECLARE @rowcount               AS INT

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                    AS datetime
    DECLARE @RptDate                AS datetime
    
    DECLARE @Debug                  AS bit
    DECLARE @Month                  AS tinyint
    DECLARE @Year                   AS int

    DECLARE @tmpDay                 AS tinyint
    DECLARE @tmpDate                AS datetime
    DECLARE @ClaimAspectIDWorking   AS int

    DECLARE @OfficeName             AS udt_std_name
    DECLARE @InsuranceCompanyName   AS varchar(50)
    DECLARE @ClaimNumber            AS varchar(50)
    DECLARE @RepName                AS varchar(50)
    DECLARE @LYNXid                 AS int
    DECLARE @LYNXRepId              AS int
    DECLARE @VehicleOwner           AS varchar(50)
    DECLARE @FinalEstimateAmt       AS money
    DECLARE @DateReceived           AS datetime
    DECLARE @DateClosed             AS datetime
    DECLARE @CycleTime              AS int
    DECLARE @tmpFinalDocumentID     AS int

    DECLARE @NetTotal_CD            AS int
    DECLARE @VehicleAspectID        AS tinyint
    DECLARE @BusinessName           AS varchar(100)
    DECLARE @InsuredName            AS varchar(100)
    DECLARE @ClaimAspectNumber      AS tinyint
    DECLARE @Office                 AS udt_std_name
    DECLARE @Last12MonthDate        AS datetime
    DECLARE @OfficeIDWorking        AS varchar(10)
    
    DECLARE @OfficeIDCsrWorking     AS varchar(5)
    DECLARE @OfficeNameCsrWorking   AS varchar(50)
    DECLARE @ExposureNameCsrWorking AS varchar(50)
    DECLARE @DispositionCsrWorking  AS varchar(50)
    DECLARE @DataWarehouseDate      udt_std_datetime
    DECLARE @StateCodeWork          varchar(2)
    DECLARE @StateName              udt_std_name

    SET @Debug = 0

    SET @ProcName = 'uspRptPSDetailsGetList'
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
        
            SELECT  @Office = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
            
            SET @OfficeIDWorking = CONVERT(varchar(10), @OfficeID)
        END
    END
    ELSE
    BEGIN
        SET @Office = 'All'
        SET @OfficeIDWorking = '%'
    END          
    
    -- Validate State Code
    
    IF @StateCode IS NOT NULL
    BEGIN
     IF NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @StateCode)
        BEGIN
            -- Invalid StateCode
        
            RAISERROR  ('101|%s|@StateCode|%s', 16, 1, @ProcName, @StateCode)
            RETURN
        END
        ELSE
        BEGIN
            SET @StateCodeWork = @StateCode
            
            SELECT  @StateName = StateValue
              FROM  dbo.utb_state_code
              WHERE StateCode = @StateCode
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @StateCodeWork = '%'
        SET @StateName = 'All'
    END          
                

    IF @RptMonth is NULL
        SET @RptMonth = Month(@now)

    IF @RptYear is NULL
        SET @RptYear = Year(@now)

    IF (@RptMonth < MONTH(@now) and @RptYear = YEAR(@now)) or (@RptYear < YEAR(@now))
    BEGIN
        set @tmpDate = convert(datetime, convert(varchar, @RptMonth) + '/1/' +  convert(varchar, @RptYear))
        set @tmpDate = DATEADD(Month, 1, @tmpDate)
        set @tmpDate = DATEADD(Day, -1, @tmpDate)
        set @tmpDay = Day(@tmpDate)
    END
    
    IF @RptMonth = MONTH(@now) and @RptYear = YEAR(@now)
        set @tmpDay = Day(@now)

    IF @RptMonth > MONTH(@now) and @RptYear >= YEAR(@now)
        set @tmpDay = 1
        
    set @RptDate = convert(varchar, @RptMonth) + '/' + convert(varchar, @tmpDay) + '/' + convert(varchar, @RptYear)
    
    -- rptdate is always the last day of the reports month/year.
    
    -- Get the last date of the 13th month prior to rptdate
    set @Last12MonthDate = DATEADD(mm, -12, @RptDate)
    -- Add 1 day to the above to get the start of the 12th month prior to rptdate
    set @Last12MonthDate = DATEADD(dd, 1, @Last12MonthDate)
    
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID=' + convert(varchar, @InsuranceCompanyID)
        PRINT '@RptDate=' + convert(varchar, @RptDate, 101)
        PRINT '@Last12MonthDate=' + convert(varchar, @Last12MonthDate, 101)
    END

    SET @Month = Month(@RptDate)
    SET @Year  = Year(@RptDate)

    SELECT @NetTotal_CD = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE Name = 'NetTotal'
    
    SELECT @VehicleAspectID = ClaimAspectTypeID
    FROM utb_claim_aspect_type
    WHERE Name = 'Vehicle'
    
    SELECT @InsuranceCompanyName = Name
    FROM utb_insurance
    WHERE InsuranceCompanyID = @InsuranceCompanyID
    
    IF @OfficeID = 0 
        SET @OfficeID = NULL
    
    DECLARE @tmpReportData TABLE 
    (
        ClaimAspectID           int             NULL,
        InsuranceCompanyName    varchar(50)     NULL,
        SummaryTitle            varchar(100)    NULL,
        SummaryLevel            int             NULL,
        OfficeName              varchar(100)    NULL,
        OfficeLevel             tinyint         NULL,
        Exposure                varchar(100)    NULL,
        Disposition             varchar(100)    NULL,
        DispositionLevel        tinyInt         NULL,
        ClaimNumber             varchar(50)     NULL,
        RepName                 varchar(50)     NULL,
        LYNXid                  int             NULL,
        LYNXRepId               int             NULL,
        LossState               varchar(50)     NULL,
        VehicleInfo             varchar(150)    NULL,
        ShopName                varchar(50)     NULL,
        InitialEstimateAmt      money           NULL,
        FinalEstimateAmt        money           NULL,
        IndemnityAmount         money           NULL,
        DateReceived            datetime        NULL,
        DateClosed              datetime        NULL,
        CycleTime               int             NULL,
	    KeysToKeys				int				NULL,
        TotalClaims             int             NULL
    )

    DECLARE @tmpReportDataRaw TABLE 
    (
        ClaimAspectID           int             NULL,
        InsuranceCompanyName    varchar(50)     NULL,
        OfficeName              varchar(50)     NULL,
        Exposure                varchar(50)     NULL,
        ClaimNumber             varchar(50)     NULL,
        RepName                 varchar(50)     NULL,
        LYNXid                  int             NULL,
        LYNXRepId               int             NULL,
        Disposition             varchar(50)     NULL,
        DispositionId           int             NULL,
        LossState               varchar(50)     NULL,
        VehicleInfo             varchar(150)    NULL,
        ShopName                varchar(50)     NULL,
        InitialEstimateAmt      money           NULL,
        FinalEstimateAmt        money           NULL,
        IndemnityAmount         money           NULL,
        DateReceived            datetime        NULL,
        DateClosed              datetime        NULL,
        CycleTime               int             NULL,
	    KeysToKeys				int				NULL
    )
    
    DECLARE @tmpValidDisposition TABLE
    (
        DispositionTypeID       int         NOT NULL,
        Disposition             varchar(50) NOT NULL
    )
    
    DECLARE @tmpReportExposures TABLE
    (
        ExposureCD          int         NOT NULL,
        Name                varchar(50) NOT NULL
    )
    
    DECLARE @tmpOffices TABLE 
    (
        OfficeID            int             NOT NULL,
        OfficeName          varchar(50)     NOT NULL
    )
    
    DECLARE @tmpGroups TABLE
    (
        OfficeName          varchar(100)    NOT NULL,
        DispositionName     varchar(100)    NOT NULL,
        ExposureName        varchar(100)    NOT NULL
    )
    
    -- For this report, valid disposition are Repair Complete,
    --  Cash-out and Total Loss. So load their codes into a table
    
    INSERT INTO @tmpValidDisposition
    SELECT DispositionTypeID, DispositionTypeDescription
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription in ('Cash-Out', 'Repair Complete', 'Total Loss')
    
    INSERT INTO @tmpReportExposures
    SELECT Code, Name 
    FROM ufnUtilityGetReferenceCodes('utb_claim_aspect', 'ExposureCD')
    WHERE Code in ('1', '3')
    
    IF @OfficeIDWorking = '%'
    BEGIN
        INSERT INTO @tmpOffices
        SELECT OfficeID, Name
        FROM utb_office
        WHERE InsuranceCompanyID = @InsuranceCompanyID
          AND EnabledFlag = 1
    END
    ELSE
    BEGIN
        INSERT INTO @tmpOffices
        SELECT @OfficeIDWorking, @Office
    END
    
    -- Details data for report month/year
    INSERT INTO @tmpReportDataRaw
    SELECT fc.ClaimAspectID, 
           dc.InsuranceCompanyName, 
           dc.OfficeName, 
           CASE
            WHEN fc.ExposureCD = 1 THEN '1st Party'
            WHEN fc.ExposureCD = 3 THEN '3rd Party' 
           END,
           fc.ClientClaimNumber, 
           dc.RepNameLast + ', ' + dc.RepNameFirst,
           ca.LynxId, 
           fc.LynxHandlerOwnerID, 
           ddt.DispositionTypeDescription,
           ddt.DispositionTypeID,
           ds.StateName,
           convert(varchar(4), cv.VehicleYear) + ' ' + cv.Make + ' ' + cv.Model,
           rl.ShopLocationName,
           fc.OriginalEstimateGrossAmt, 
           fc.FinalEstimateGrossAmt, 
           fc.IndemnityAmount,
           dtn.DateValue, 
           dtc.DateValue,
           fc.CycleTimeNewToCloseCalDay,
	       fc.CycleTimeRepairStartToEndCalDay    
    FROM dbo.utb_dtwh_fact_claim fc
    LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
    LEFT JOIN dbo.utb_dtwh_dim_repair_location rl ON (fc.RepairLocationID = rl.RepairLocationID)
    LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
    LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
    LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
    LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
    LEFT JOIN dbo.utb_dtwh_dim_disposition_type ddt ON (fc.DispositionTypeID = ddt.DispositionTypeID)
    LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
    LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (fc.ClaimAspectID = cv.ClaimAspectID)
    -- LEFT JOIN dbo.utb_claim_coverage cc ON (ca.LynxID = cc.LynxID) -- Data from this table not used. So removing it from here
    WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
      AND dtc.DateValue >= @Last12MonthDate
      AND dtc.DateValue < dateadd(Day, 1, @RptDate)
      AND dc.OfficeID LIKE @OfficeIDWorking
      AND dsc.ServiceChannelCD = 'PS'
      AND ca.EnabledFlag = 1
      AND fc.DispositionTypeID in (SELECT DispositionTypeID FROM @tmpValidDisposition)
	  AND fc.EnabledFlag = 1
      AND ds.StateCode LIKE @StateCodeWork
    ORDER BY dc.OfficeName, ca.LynxID

    -- Load the detail stuff for the current month/year
    INSERT INTO @tmpReportData
    SELECT ClaimAspectID,
            InsuranceCompanyName,
            NULL, NULL,
            OfficeName,
            1,
            Exposure,
            Disposition,
            CASE
                WHEN Disposition = 'Repair Complete' THEN 1
                WHEN Disposition = 'Cash-Out' THEN 2
                ELSE 3
            END,
            ClaimNumber,
            RepName,
            LYNXid,
            LYNXRepId,
            LossState,
            VehicleInfo,
            ShopName,
            InitialEstimateAmt,
            FinalEstimateAmt,
            IndemnityAmount,
            DateReceived,
            DateClosed,
            CycleTime,
		    KeysToKeys,
            NULL
    FROM @tmpReportDataRaw trd
    WHERE Month(DateClosed) = @Month
      AND Year(DateClosed) = @Year
      
    

    -- Fill in the missing sections
    
    INSERT INTO @tmpGroups
    SELECT OfficeName, Disposition, Name
    FROM @tmpOffices, @tmpValidDisposition, @tmpReportExposures
    
    DECLARE csrGroups CURSOR FOR
        SELECT * FROM @tmpGroups
        
    OPEN csrGroups

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrGroups
    INTO @OfficeNameCsrWorking, @DispositionCsrWorking, @ExposureNameCsrWorking

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        IF @Debug = 1
        BEGIN
            PRINT '@OfficeNameCsrWorking: ' + @OfficeNameCsrWorking
            PRINT '@DispositionCsrWorking: ' + @DispositionCsrWorking
            PRINT '@ExposureNameCsrWorking: ' + @ExposureNameCsrWorking
        END

        IF NOT EXISTS(SELECT * 
                        FROM @tmpReportData
                        WHERE OfficeName = @OfficeNameCsrWorking
                          AND Disposition = @DispositionCsrWorking
                          AND Exposure = @ExposureNameCsrWorking)
        BEGIN
            INSERT INTO @tmpReportData
            SELECT NULL, @InsuranceCompanyName, NULL, NULL, @OfficeNameCsrWorking, 
                   1, @ExposureNameCsrWorking,
                   @DispositionCsrWorking, 
                    CASE
                        WHEN @DispositionCsrWorking = 'Repair Complete' THEN 1
                        WHEN @DispositionCsrWorking = 'Cash-Out' THEN 2
                        ELSE 3
                    END,
                   NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, 
                   NULL, NULL, NULL, NULL, NULL
        END
        
        FETCH next
        FROM csrGroups
        INTO @OfficeNameCsrWorking, @DispositionCsrWorking, @ExposureNameCsrWorking

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE csrGroups
    DEALLOCATE csrGroups
          
    IF @OfficeIDWorking <> '%'
    BEGIN
        -- Summary Totals for the Office - Current Month/Year
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate), 
                1,
                tmpO.OfficeName + ' summary',
                2,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                (SELECT avg(trd.InitialEstimateAmt)
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                ),
                (SELECT avg(trd.FinalEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                ),
                (SELECT avg(trd.IndemnityAmount) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                ),
                NULL,
                NULL,
                (SELECT avg(trd.CycleTime) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                ),
		        (SELECT avg(trd.KeysToKeys) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                ),
                (SELECT count(*) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tmpO.OfficeName and Month(DateClosed) = @Month AND Year(DateClosed) = @Year
                )
        FROM @tmpOffices tmpO
    
        -- Summary Totals for the Office - Year to Date
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                'January ' + DATENAME(yyyy, @RptDate) + ' - ' + DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate),
                2,
                tg.OfficeName + ' summary',
                2,
                tg.ExposureName,
                tg.DispositionName,
                CASE
                    WHEN tg.DispositionName = 'Repair Complete' THEN 1
                    WHEN tg.DispositionName = 'Cash-Out' THEN 2
                    ELSE 3
                END,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                (SELECT avg(trd.InitialEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT avg(trd.FinalEstimateAmt)  
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT avg(trd.IndemnityAmount)  
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                NULL,
                NULL,
                (SELECT avg(trd.CycleTime)  
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
		        (SELECT avg(trd.KeysToKeys) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT count(*) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Year(DateClosed) = @Year AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                )
        FROM @tmpGroups tg
    
        -- Summary Totals for the Office - 12 Months
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                '12 Months (' + DATENAME(mm, @Last12MonthDate) + ' ' + DATENAME(yyyy, @Last12MonthDate) + ' - ' + DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate) + ')',
                3,
                tg.OfficeName + ' summary',
                2,
                tg.ExposureName,
                tg.DispositionName,
                CASE
                    WHEN tg.DispositionName = 'Repair Complete' THEN 1
                    WHEN tg.DispositionName = 'Cash-Out' THEN 2
                    ELSE 3
                END,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                (SELECT avg(trd.InitialEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT avg(trd.FinalEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT avg(trd.IndemnityAmount) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                NULL,
                NULL,
                (SELECT avg(trd.CycleTime) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
		        (SELECT avg(trd.KeysToKeys) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                ),
                (SELECT count(*) 
                    FROM @tmpReportDataRaw trd
                    WHERE trd.OfficeName = tg.OfficeName AND Exposure = tg.ExposureName AND Disposition = tg.DispositionName
                )
        FROM @tmpGroups tg
    END
    
    IF @OfficeIDWorking = '%'
    BEGIN
        -- Summary Totals for the Insurance Company - Current Month/Year
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate), 
                1,
                @InsuranceCompanyName + ' summary',
                3,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                avg(trd.InitialEstimateAmt),
                avg(trd.FinalEstimateAmt),
                avg(trd.IndemnityAmount),
                NULL,
                NULL,
                avg(trd.CycleTime),
		        avg(trd.KeysToKeys),
                count(*)
        FROM @tmpReportDataRaw trd
        WHERE Month(DateClosed) = @Month AND Year(DateClosed) = @Year

        -- Summary Totals for the Insurance company - Year to Date
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                'January ' + DATENAME(yyyy, @RptDate) + ' - ' + DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate),
                2,
                @InsuranceCompanyName + ' summary',
                3,
                te.Name,
                td.Disposition,
                CASE
                    WHEN td.Disposition = 'Repair Complete' THEN 1
                    WHEN td.Disposition = 'Cash-Out' THEN 2
                    ELSE 3
                END,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                (SELECT avg(trd.InitialEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT avg(trd.FinalEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT avg(trd.IndemnityAmount) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                ),
                NULL,
                NULL,
                (SELECT avg(trd.CycleTime) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                ),
		        (SELECT avg(trd.KeysToKeys) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT count(*) 
                    FROM @tmpReportDataRaw trd
                    WHERE Year(DateClosed) = @Year AND Exposure = te.Name AND Disposition = td.Disposition
                )
        FROM @tmpValidDisposition td, @tmpReportExposures te
    
        -- Summary Totals for the Insurance company - 12 Months
        INSERT INTO @tmpReportData
        SELECT NULL,
                NULL,
                '12 Months (' + DATENAME(mm, @Last12MonthDate) + ' ' + DATENAME(yyyy, @Last12MonthDate) + ' - ' + DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate) + ')',
                3,
                @InsuranceCompanyName + ' summary',
                3,
                te.Name,
                td.Disposition,
                CASE
                    WHEN td.Disposition = 'Repair Complete' THEN 1
                    WHEN td.Disposition = 'Cash-Out' THEN 2
                    ELSE 3
                END,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                (SELECT avg(trd.InitialEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT avg(trd.FinalEstimateAmt) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT avg(trd.IndemnityAmount) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                ),
                NULL,
                NULL,
                (SELECT avg(trd.CycleTime) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                ),
		        (SELECT avg(trd.KeysToKeys) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                ),
                (SELECT count(*) 
                    FROM @tmpReportDataRaw trd
                    WHERE Exposure = te.Name AND Disposition = td.Disposition
                )
        FROM @tmpValidDisposition td, @tmpReportExposures te
    END
    
    -- Final Select
    select @Office as OfficeCriteria, 
           DATENAME(mm, @RptDate) + ' ' + DATENAME(yyyy, @RptDate) as ReportDate,
           CASE
            WHEN Exposure = '1st Party' THEN 1
            ELSE 2
           END AS ExposureLevel,
           case
            when OfficeName = @InsuranceCompanyName + ' summary' then 1
            else 0
           end AS OverallSummary,
           *,
           @DataWarehouseDate as DataWareDate,
           @StateName as StateName
           
    from @tmpReportData
    order by OfficeName, DispositionLevel, Exposure
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptPSDetailsGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptPSDetailsGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
