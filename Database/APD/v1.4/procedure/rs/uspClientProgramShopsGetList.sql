-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientProgramShopsGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClientProgramShopsGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClientProgramShopsGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Generates a shop list to be used sent to an insurance carrier
*
* PARAMETERS:   (I) @InsuranceCompanyID - The insurance company to obtain the shop list for
*        
*
* RESULT SET:   A dataset to be converted to Email format
*
*
* VSS
* $Workfile:  $
* $Archive:  $
* $Revision:  $
* $Author:  $
* $Date:  $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspClientProgramShopsGetList
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Set database options

    SET ANSI_NULLS ON
    SET CONCAT_NULL_YIELDS_NULL OFF


    -- Declare variables
    
    DECLARE @WarrantyRefinishMinYrs     udt_std_int_tiny
    DECLARE @WarrantyWorkmanshipMinYrs  udt_std_int_tiny
    
    DECLARE @ProcName                   AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClientProgramShopsGetList'


    -- Validate input parameter
    
    IF (@InsuranceCompanyID IS NULL) OR 
       (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))
    BEGIN
        -- Invalid InsuranceCompany ID
    
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

        
    -- Get the minimum warranty for the insurance company.  The shop's warranties must meet or exceed the
    -- insurance company's minimums.  

    SELECT  @WarrantyRefinishMinYrs = Convert(tinyint, WarrantyPeriodRefinishMinCD),
            @WarrantyWorkmanshipMinYrs = Convert(tinyint, WarrantyPeriodWorkmanshipMinCD)
      FROM  dbo.utb_insurance
      WHERE InsuranceCompanyID = @InsuranceCompanyID
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @WarrantyRefinishMinYrs IS NULL
    BEGIN
        -- The carrier had no configuration for a minimum refinish warranty, set minimum to 0
        
        SET @WarrantyRefinishMinYrs = 0
    END
    
    IF @WarrantyWorkmanshipMinYrs IS NULL
    BEGIN
        -- The carrier had no configuration for a minimum workmanship warranty, set minimum to 0
        
        SET @WarrantyWorkmanshipMinYrs = 0
    END

    
    -- Now get list of shops
    
    SELECT sl.Name AS Name,
           sl.ShopLocationID AS Alias, 
           p.Name AS Contact,
           sl.Address1 AS Address,
           sl.AddressCity AS City,
           sl.AddressState AS State,
           sl.AddressZip AS Zip,
           sl.PhoneAreaCode + '' + sl.PhoneExchangeNumber + '' + sl.PhoneUnitNumber AS Phone,
           sl.FaxAreaCode + '' + sl.FaxExchangeNumber + '' + sl.FaxUnitNumber AS Fax
      FROM dbo.utb_shop_location sl
      LEFT JOIN dbo.utb_shop_location_personnel slp ON (sl.ShopLocationID = slp.ShopLocationID)
      LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
      LEFT JOIN dbo.utb_personnel_type pt ON (p.PersonnelTypeID = pt.PersonnelTypeID)
      WHERE sl.addressstate IN (SELECT StateCode FROM utb_client_contract_state WHERE InsuranceCompanyID = @InsuranceCompanyID)
       AND sl.programflag = 1
       AND sl.AvailableForSelectionFlag = 1
       AND sl.EnabledFlag = 1
       AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
       AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
       AND (pt.Name IS NULL OR pt.Name = 'Shop Manager') 


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END       
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientProgramShopsGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClientProgramShopsGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile:  $
* $Archive:  $
* $Revision:  $
* $Author:  $
* $Date:  $
* $History:  $
************************************************************************************************************************/
