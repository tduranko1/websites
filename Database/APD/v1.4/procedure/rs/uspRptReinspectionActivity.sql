-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptReinspectionActivity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptReinspectionActivity 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptReinspectionActivity
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptReinspectionActivity
(
    @RptMonth           tinyInt,
    @RptYear            int,
    @InsuranceCompanyID int = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @Now        AS datetime
    DECLARE @ProcName   AS varchar(30)       -- Used for raise error stmts 
    DECLARE @InsuranceCompanyIDWork varchar(5)
    DECLARE @ReportTitle            varchar(250)
    DECLARE @ReportDate AS datetime
    DECLARE @RptPrevMonth AS datetime
    

    SET @ProcName = 'uspRptReinspectionActivity'
    SET @Now = CURRENT_TIMESTAMP
    
    IF @InsuranceCompanyID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT InsuranceCompanyID
                        FROM dbo.utb_insurance
                        WHERE InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Insurance Company ID
        
            RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
            RETURN
        END
        
        SET @InsuranceCompanyIDWork = CONVERT(varchar, @InsuranceCompanyID)
        SET @ReportTitle = ''
    END
    
    SET @ReportDate = CONVERT(datetime, convert(varchar, @RptMonth) + '/1/' + convert(varchar, @RptYear))
    IF @RptMonth > 1
    BEGIN
        SET @RptPrevMonth = CONVERT(datetime, convert(varchar, (@RptMonth - 1)) + '/1/' + convert(varchar, @RptYear))
    END
    ELSE
    BEGIN
        SET @RptPrevMonth = CONVERT(datetime, '1/1/' + convert(varchar, @RptYear))
    END
    
    DECLARE @tmpReportData TABLE 
    (
        LynxID                  bigint          NOT NULL,
        ClaimAspectID           bigint          NOT NULL,
        ClaimAspectNumber       tinyint         NOT NULL,
        ReinspectID             bigint          NOT NULL,
        ReportTitle             varchar(250)    NOT NULL,
        ReportDate              varchar(100)    NOT NULL,
        ReportGrouping          varchar(3)      NOT NULL,
        DateMonth               tinyint         NOT NULL,
        InsuranceCompanyName    varchar(100)    NOT NULL,
        ClientClaimNumber       varchar(30)     NULL,
        ProgramManagerName      varchar(200)    NULL,
        ShopName                varchar(100)    NULL,
        ProgramFlag             tinyint         NULL,
        EstimateDate            datetime        NULL,
        ReinspectionDate        datetime        NULL,
        ReIRepairStatus         varchar(15)     NULL,
        Repairable              varchar(3)      NULL,
        OrgEstimateAmount       money           NULL,
        ReinspectionAmount      money           NULL,
        TotalAdditions          money           NULL,
        TotalSubractions        money           NULL,
        ActualDifference        money           NULL,
        AbsoluteDifference      money           NULL,
        EstimateAccuracy        decimal(5,2)    NULL
    )
    
    IF @InsuranceCompanyID IS NULL
    BEGIN
        SET @InsuranceCompanyIDWork = '%'
        SET @ReportTitle = 'All Clients'
    END
    
    -- Get all the reinspections completed during the report period
    
    INSERT INTO @tmpReportData
    (   LynxID, 
        ClaimAspectID, 
        ClaimAspectNumber,
        ClientClaimNumber,
        ReinspectID,
        ReportTitle,
        ReportDate,
        ReportGrouping,
        DateMonth,
        InsuranceCompanyName, 
        ProgramManagerName, 
        ShopName, 
        ProgramFlag,
        EstimateDate,
        ReinspectionDate, 
        ReIRepairStatus,
        Repairable,
        OrgEstimateAmount,
        EstimateAccuracy)
    SELECT ca.LynxID,
           r.ClaimAspectID,
           ca.ClaimAspectNumber,
           c.ClientClaimNumber,
           r.ReinspectID,
           @ReportTitle,
           DateName(month, r.ReinspectionDate) + ' ' + convert(varchar, Year(r.ReinspectionDate)),
           'CUR',
           MONTH(r.ReinspectionDate),
           i.Name,
           isNull((u.NameLast + ', ' + LEFT(u.NameFirst, 1)), ''),
           sl.Name,
           sl.ProgramFlag,
           d.CreatedDate,
           r.ReinspectionDate,
           isNull((SELECT rc.Name
                FROM dbo.ufnUtilityGetReferenceCodes('utb_reinspect', 'RepairStatusCD') rc
                WHERE rc.Code = r.RepairStatusCD), ''),
           CASE
                WHEN casc.DispositionTypeCD = '' OR casc.DispositionTypeCD = 'RC' THEN 'Yes'
                ELSE 'No'
           END,
           BaseEstimateAmt,
           isNull(r.ReinspectSatisfactionPercent, 1.00) * 100.00
    FROM dbo.utb_reinspect r
    LEFT JOIN utb_claim_aspect ca ON (r.ClaimAspectID = ca.ClaimAspectID)
    Left Outer Join utb_CLaim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    LEFT JOIN utb_shop_location sl ON (r.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN utb_user u ON (r.ReinspectorUserID = u.UserID)
    LEFT JOIN utb_document d ON (r.EstimateDocumentID = d.DocumentID)
    --LEFT JOIN utb_claim_coverage cc ON (ca.LynxID = cc.LynxID)
    WHERE Month(r.ReinspectionDate) = @RptMonth
      AND Year(r.ReinspectionDate) = @RptYear
      AND c.InsuranceCompanyID like @InsuranceCompanyIDWork

      and casc.EnabledFlag = 1


    -- Get all the reinspections completed YTD
    
    INSERT INTO @tmpReportData
    (   LynxID, 
        ClaimAspectID, 
        ClaimAspectNumber,
        ClientClaimNumber,
        ReinspectID,
        ReportTitle,
        ReportDate,
        ReportGrouping,
        DateMonth,
        InsuranceCompanyName, 
        ProgramManagerName, 
        ShopName, 
        ProgramFlag,
        EstimateDate,
        ReinspectionDate, 
        ReIRepairStatus,
        Repairable,
        OrgEstimateAmount,
        EstimateAccuracy)
    SELECT ca.LynxID,
           r.ClaimAspectID,
           ca.ClaimAspectNumber,
           c.ClientClaimNumber,
           r.ReinspectID,
           @ReportTitle,
           'January ' + convert(varchar, Year(r.ReinspectionDate)) + ' through ' + DateName(month, @RptPrevMonth) + ' ' + convert(varchar, Year(r.ReinspectionDate)),
           'YTD',
           MONTH(r.ReinspectionDate),
           i.Name,
           isNull((u.NameLast + ', ' + LEFT(u.NameFirst, 1)), ''),
           sl.Name,
           sl.ProgramFlag,
           d.CreatedDate,
           r.ReinspectionDate,
           isNull((SELECT rc.Name
                FROM dbo.ufnUtilityGetReferenceCodes('utb_reinspect', 'RepairStatusCD') rc
                WHERE rc.Code = r.RepairStatusCD), ''),
           CASE
                WHEN casc.DispositionTypeCD = '' OR casc.DispositionTypeCD = 'RC' THEN 'Yes'
                ELSE 'No'
           END,
           BaseEstimateAmt,
           isNull(r.ReinspectSatisfactionPercent, 1.00) * 100.00
    FROM dbo.utb_reinspect r
    LEFT JOIN utb_claim_aspect ca ON (r.ClaimAspectID = ca.ClaimAspectID)
    Left Outer Join utb_CLaim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    LEFT JOIN utb_shop_location sl ON (r.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN utb_user u ON (r.ReinspectorUserID = u.UserID)
    LEFT JOIN utb_document d ON (r.EstimateDocumentID = d.DocumentID)
    --LEFT JOIN utb_claim_coverage cc ON (ca.LynxID = cc.LynxID)
    WHERE Month(r.ReinspectionDate) <= @RptMonth
      AND Year(r.ReinspectionDate) = @RptYear
      AND c.InsuranceCompanyID like @InsuranceCompanyIDWork

      and casc.EnabledFlag = 1

      
    -- Now update the reinspection amounts
    UPDATE @tmpReportData
    SET TotalAdditions = IsNull((Select SUM(rd.ReinspectAmt - rd.EstimateAmt)
                                From dbo.utb_reinspect_detail rd 
                                Where rd.ReinspectID = tmp.ReinspectID
                                    And (rd.ReinspectAmt - rd.EstimateAmt) > 0), 0),
        TotalSubractions =  IsNull((Select SUM(rd.ReinspectAmt - rd.EstimateAmt)
                                From dbo.utb_reinspect_detail rd 
                                Where rd.ReinspectID = tmp.ReinspectID
                                    And (rd.ReinspectAmt - rd.EstimateAmt) < 0), 0)
    FROM @tmpReportData tmp
    
    -- Calculate the reinspection amount
    UPDATE @tmpReportData
    SET ReinspectionAmount = tmp.OrgEstimateAmount + tmp.TotalAdditions + tmp.TotalSubractions
    FROM @tmpReportData tmp

    -- Calculate the actual difference
    UPDATE @tmpReportData
    SET ActualDifference = ABS(tmp.OrgEstimateAmount - tmp.ReinspectionAmount),
        AbsoluteDifference = tmp.TotalAdditions - tmp.TotalSubractions
    FROM @tmpReportData tmp

    -- Final Select
    SELECT * 
    FROM @tmpReportData
    
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptReinspectionActivity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptReinspectionActivity TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/