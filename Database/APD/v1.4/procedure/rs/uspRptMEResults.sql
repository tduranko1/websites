-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMEResults' AND type = 'P')
BEGIN
    DROP PROCEDURE uspRptMEResults 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptMEResults
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     
*
* PARAMETERS:   None  
*
* RESULT SET:   ME Results information
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMEResults
    @ReportDate AS udt_std_datetime = NULL
AS
BEGIN
    -- Set Database Options
    
    SET NOCOUNT ON 
    
    DECLARE @date AS VARCHAR(30) 

    SET @date = CURRENT_TIMESTAMP - 1


    /*SELECT CONVERT(VARCHAR(10), ca.LynxID) + '-' + CONVERT(VARCHAR(2), ca.ClaimAspectNumber) AS LynxID, mer.* 
      FROM dbo.utb_mobile_electronics_results mer
      LEFT JOIN dbo.utb_claim_aspect ca ON (mer.ClaimAspectID = ca.ClaimAspectID)
     ORDER BY CONVERT(VARCHAR(10), ca.LynxID) + '-' + CONVERT(VARCHAR(2), ca.ClaimAspectNumber) DESC
    */


    select              c.*,
                        i.*
    from                uvw_Claim_Info c
    
    inner join          (
                        select   distinct lynxid
                        from     uvw_Claim_Info
                        where    ServiceChannelCD = 'ME'
                        )me
    on                  me.LynxID = c.LynxID
    
    left outer join     (
                        select        ClaimAspectServiceChannelID, 
                                      PaymentChannelCD,
                                      sum(isnull(Amount,0)) as 'InvoiceAmount'
                        from          utb_Invoice
    
                        where         ItemTypeCD = 'I' --Exclude (F)ee and consider (I)nvoices only
                        and           EnabledFlag = 1
                        group by      ClaimAspectServiceChannelID, 
                                      PaymentChannelCD
    
                        ) i
    on                  i.ClaimAspectServiceChannelID = c.ClaimAspectServiceChannelID
    
    order by c.Lynxid, c.ClaimAspectID_Vehicle, c.ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMEResults' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMEResults TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

