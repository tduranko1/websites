-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopDealerDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopDealerDel 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopDealerDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Removes a Shop Dealer relationship
*
* PARAMETERS:  
* (I) @BusinessInfoID                   The Shop's unique identity
* (I) @DealerID                 The Dealer's unique identity
* (I) @SysLastUserID            The Shop Dealer's updating user identity
* (I) @SysLastUpdatedDate       The Shop Dealer's last update date as string (VARCHAR(30))
*
* RESULT SET:
* ShopID                     The Shop unique identity
* DealerID                   The Dealer unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopDealerDel
(
	@Updates                            udt_std_desc_long,
	@DealerID                           udt_std_int_big,
  @SysLastUserID                      udt_std_id,
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                    udt_std_int
    DECLARE @rowcount                 udt_std_int

    DECLARE @ProcName                 varchar(30),       -- Used for raise error stmts 
            @KeyDescription           udt_std_name,
            @now                      udt_std_datetime
    
    DECLARE @tmpBusiness        TABLE (BusinessID       bigint          NOT NULL,
                                       Name             varchar(50)         NULL)
    
    
    SET @ProcName = 'uspSMTShopDealerDel'
    SET @now = CURRENT_TIMESTAMP
    
    -- Apply edits
      

    IF @DealerID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT DealerID FROM utb_Dealer WHERE DealerID = @DealerID)
        BEGIN
           -- Invalid Dealer
            RAISERROR('101|%s|@DealerID|%u', 16, 1, @ProcName, @DealerID)	
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Dealer
        RAISERROR('101|%s|@DealerID|%u', 16, 1, @ProcName, @DealerID)
        RETURN
    END

    --select dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default)
    --return
        
    IF ((SELECT dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default)) = 0)
    BEGIN
        -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)	
        RETURN
    END
   
    
   
    INSERT INTO @tmpBusiness (BusinessID, Name)
    SELECT value,
           (SELECT Name FROM dbo.utb_shop WHERE ShopID = f.value)    
    FROM dbo.ufnUtilityParseString(@Updates, ',', 1) f 
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|@tmpBusiness', 16, 1, @ProcName)
        RETURN
    END
    
    IF EXISTS (SELECT ShopID 
               FROM @tmpBusiness t LEFT JOIN dbo.utb_shop s ON t.BusinessID = s.ShopID
               WHERE s.ShopID IS NULL)
    BEGIN
      -- Invalid Business ID in @Updates string
        RAISERROR('101|%s|@Updates|%u', 16, 1, @ProcName, @Updates)	
        RETURN
    END
               
    
    
    SET @KeyDescription = (SELECT Name FROM dbo.utb_dealer WHERE DEalerID = @DealerID) 

    
    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopDealerDelDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    DELETE dbo.utb_shop_dealer
    FROM dbo.utb_shop_dealer s INNER JOIN @tmpBusiness t on s.ShopID = t.BusinessID
        
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('106|%s|utb_shop_dealer', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- Begin Audit Log code  -----------------------------------------------------------------------------------
    
    INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                   KeyDescription,
                                   KeyID,
                                   LogComment,
                                   SysLastUserID,
                                   SysLastUpdatedDate)
                            SELECT 'P',
                                   @KeyDescription,
                                   @DealerID,
                                   ' Business removed from Dealer: ' + Name,
                                   @SysLastUserID,
                                   @now
                            FROM @tmpBusiness
    

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        ROLLBACK TRANSACTION 
        RETURN
    END
      
    -- End of Audit Log code  ----------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmShopDealerDelDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @Updates AS Updates, @DealerID AS DealerID

    RETURN @rowcount

END


GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopDealerDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopDealerDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
