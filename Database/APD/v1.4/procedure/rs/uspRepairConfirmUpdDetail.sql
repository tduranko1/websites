-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRepairConfirmUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRepairConfirmUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRepairConfirmUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Durankof
* FUNCTION:     Proc to update the repair start/end confirmation
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID   The ClaimAspect ID
* (O) @RepairDateCD                  S=Start date; E=End Date confirmed
* (I) @UserID                        The User id 
*
* RESULT SET:
* [result set details here]
*
* MODIFICATIONS:  01Sep2015 - TVD - Updated the utb_checklist to force the "Repair Completion Verification Required"
*									task to the virtual user "Confirm Repair End Task" - TTP-23
*				  12Feb2016 - TVD - Removed the utb_checklist to force the "Repair Completion Verification Required"
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRepairConfirmUpdDetail
(
    @ClaimAspectServiceChannelID udt_std_id_big,
    @RepairDateCD                varchar(1),
    @UserID                      udt_std_id
)
AS
BEGIN
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    
    DECLARE @now                    AS datetime 

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @EventID                AS udt_std_int
    DECLARE @EventName              AS varchar(50)
    DECLARE @HistoryDescription     AS udt_std_desc_long
    DECLARE @PertainsToName         AS varchar(50)
    DECLARE @RepairEndDate          AS datetime
    DECLARE @CheckListID            AS udt_std_int_big
    DECLARE @DiaryComment           AS udt_std_desc_mid
    DECLARE @DiarySysLastUpdateDate AS udt_std_datetime
    DECLARE @RepairEndDateOriginal  AS udt_std_datetime
    DECLARE @ClaimAspectID          AS udt_std_int_big
    DECLARE @EarlyBillFlag          AS udt_std_flag

    SET @ProcName = 'uspRepairConfirmUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectServiceChannelID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
    BEGIN
        -- Invalid ClaimAspectServiceChannelID
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Check the repair date code being updated
    IF @RepairDateCD <> 'S' AND @RepairDateCD <> 'E'
    BEGIN
        -- Invalid Repair date code
    
        RAISERROR('101|%s|@RepairDateCD|%s', 16, 1, @ProcName, @RepairDateCD)
        RETURN
    END
    
    SELECT @RepairEndDate = WorkEndDate,
           @RepairEndDateOriginal = WorkEndDateOriginal,
           @ClaimAspectID = casc.ClaimAspectID,
           @EarlyBillFlag = i.EarlyBillFlag
    FROM utb_Claim_Aspect_Service_Channel casc
    LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              
    
    -- Check the presence of End Date when the start is confirmed
    IF @RepairDateCD = 'S' AND @RepairEndDate IS NULL
    BEGIN
        -- Repair End date must be set when the start date is confirmed
    
        RAISERROR('1|Repair End Date must be set when the Repair Start Date is confirmed.', 16, 1)
        RETURN
    END
    
    IF @EarlyBillFlag = 1
    BEGIN
       IF @RepairDateCD = 'E' AND EXISTS(SELECT i.InvoiceID
                                          FROM utb_invoice i
                                          WHERE ClaimAspectID = @ClaimAspectID
                                            AND ItemTypeCD = 'I'
                                            AND StatusCD = 'APD'
                                            AND EnabledFlag = 1)
       BEGIN
           -- Repair End date must be set when the start date is confirmed
       
           RAISERROR('1|Cannot confirm Repair End Date. There are Indemnity Payments that have not been invoiced. Please validate the Payment amount and generate the Invoice before confirming the Repair End Date', 16, 1)
           RETURN
       END
    END

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    -- Begin the update process
    
    BEGIN TRANSACTION
    
    IF @RepairDateCD = 'S'
    BEGIN
        UPDATE dbo.utb_Claim_Aspect_Service_Channel
        SET WorkStartConfirmFlag = 1
        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
        
        IF @RepairEndDateOriginal IS NULL
        BEGIN
            UPDATE dbo.utb_Claim_Aspect_Service_Channel
            SET WorkEndDateOriginal = @RepairEndDate
            WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            
            IF @@ERROR <> 0
            BEGIN
                -- Update failure

                RAISERROR('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN
            END
        END 
         
        SET @EventName = 'Repair Start Confirmed'
    END
    ELSE IF @RepairDateCD = 'E'
    BEGIN
        UPDATE dbo.utb_Claim_Aspect_Service_Channel
        SET WorkEndConfirmFlag = 1
        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
        
        SET @EventName = 'Repair Completion Confirmed'
    END
    
    SELECT @EventID = EventID
    FROM dbo.utb_event
    WHERE Name = @EventName
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END              

    IF @EventID IS NULL
    BEGIN
        -- Event Name not found
    
        RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
        ROLLBACK TRANSACTION
        RETURN
    END

    SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber) + ' [' + casc.ServiceChannelCD + ']'
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      
    SET @HistoryDescription = @EventName + ' for ' + @PertainsToName

    -- Now throw the event
    EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                @Description = @HistoryDescription,
                                @UserID = @UserID

    IF @@ERROR <> 0
    BEGIN
        -- APD Workflow Notification failed

        RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    -- If the start date was confirmed then we need to set the alarm date for "Vehicle Repairs Completed: task that 
    -- was created by the above workflow call to the repair completion date.
    
    IF @RepairDateCD = 'S'
    BEGIN
        IF @RepairEndDate IS NOT NULL
        BEGIN
            SELECT @CheckListID = cl.CheckListID,
                   @DiaryComment = cl.UserTaskDescription,
                   @DiarySysLastUpdateDate = cl.SysLastUpdatedDate
            FROM utb_checklist cl
            LEFT JOIN utb_Claim_Aspect_Service_Channel casc ON casc.ClaimAspectServiceChannelID = cl.ClaimAspectServiceChannelID
            WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
              AND TaskID = (SELECT t.TaskID 
                            FROM dbo.utb_task t
                            WHERE t.Name = 'Repair Completion Verification Required')

            IF @CheckListID IS NOT NULL
            BEGIN
                -- Defect 4221 - Task alaram date to be set a day before the repair end date
                SET @RepairEndDate = DATEADD(day, -1, @RepairEndDate)
                -- Defect 4221 - End of changes

                UPDATE dbo.utb_checklist
                SET AlarmDate = @RepairEndDate
                WHERE CheckListID = @CheckListID

                IF @@ERROR <> 0
                BEGIN
                    -- Update failure

                    RAISERROR('104|%s|utb_checklist', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END
            END
        END
    END
    
    COMMIT TRANSACTION
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRepairConfirmUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRepairConfirmUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
