-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCustomForms' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetCustomForms 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetWuspGetCustomFormsorkflow
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all Custom Forms by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetCustomForms
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		fs.FormSupplementID
		, f.[Name] AS FormType
		, ISNULL(fs.ShopStateCode,'') AS ShopStateCode
		, fs.EnabledFlag
		, fs.[Name] AS FormName
		, fs.PDFPath
		, ISNULL(fs.ServiceChannelCD, '') AS ServiceChannelCD
		, ISNULL(fs.SQLProcedure, '') AS SQLProcedure
		, fs.SysLastUpdatedDate
	FROM 
		utb_form_supplement fs 
		INNER JOIN utb_form f
		ON f.FormID = fs.FormID
	WHERE 
		fs.FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @iInsuranceCompanyID	
		) 
	ORDER BY
		fs.[Name]
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCustomForms' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetCustomForms TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetCustomForms TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/