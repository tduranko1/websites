-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDExtractForAGC' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAPDExtractForAGC 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAPDExtractForAGC
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Extracts APD data for AGC (DRP and Lien Holder)
*
* PARAMETERS:  
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAPDExtractForAGC
AS
BEGIN

   -- Export the APD DRP network
   SELECT sl.ShopID, 
          s.BusinessTypeCD,
          sl.ShopLocationID, 
          sl.Name ShopLocationName, 
          RTRIM(SUBSTRING(o.Name, 1, CHARINDEX(' ', o.Name))) AS OwnerFirst,
          RTRIM(SUBSTRING(o.Name, CHARINDEX(' ', o.Name) + 1, LEN(o.Name))) AS OwnerLast,
          dbo.ufnUtilitySquishString(s.FedTaxId, 1, 1, 0, Null) AS FedTaxId, 
          RTRIM(SUBSTRING(c.Name, 1, CHARINDEX(' ', c.Name))) AS ContactFirst,
          RTRIM(SUBSTRING(c.Name, CHARINDEX(' ', c.Name) + 1, LEN(c.Name))) AS ContactLast,
          sl.Address1 ShopLocationAddress1,
          sl.Address2 ShopLocationAddress2, 
          sl.AddressCity ShopLocationAddressCity, 
          sl.AddressState ShopLocationAddressState, 
          sl.AddressZip ShopLocationAddressZip,
          sl.PhoneAreaCode ShopLocationPhoneAreaCode, 
          sl.PhoneExchangeNumber ShopLocationPhoneExchangeNumber, 
          sl.PhoneUnitNumber ShopLocationPhoneUnitNumber,
          sl.FaxAreaCode ShopLocationFaxAreaCode, 
          sl.FaxExchangeNumber ShopLocationFaxExchangeNumber, 
          sl.FaxUnitNumber ShopLocationFaxUnitNumber,
          b.BillingID, 
          b.Name BillingName, 
          b.Address1 BillingAddress1,
          b.Address2 BillingAddress2, 
          b.AddressCity BillingAddressCity, 
          b.AddressState BillingAddressState, 
          b.AddressZip BillingAddressZip,
          b.PhoneAreaCode BillingPhoneAreaCode, 
          b.PhoneExchangeNumber BillingPhoneExchangeNumber, 
          b.PhoneUnitNumber BillingPhoneUnitNumber,
          b.FaxAreaCode BillingFaxAreaCode, 
          b.FaxExchangeNumber BillingFaxExchangeNumber, 
          b.FaxUnitNumber BillingFaxUnitNumber
     FROM utb_shop_location AS sl
    INNER JOIN utb_billing AS b ON (sl.BillingID = b.BillingID)
    INNER JOIN utb_shop AS s ON (sl.ShopId = s.ShopId AND s.EnabledFlag = 1)
    LEFT JOIN (SELECT slp.ShopLocationID, p.Name
                  FROM utb_shop_location_personnel AS slp
                 INNER JOIN utb_personnel AS p ON (slp.PersonnelID = p.PersonnelID)
                 INNER JOIN utb_personnel_type AS pt ON (p.PersonnelTypeID = pt.PersonnelTypeID AND pt.Name = 'Business Contact 1')
                 WHERE slp.personnelid IN (SELECT TOP 1 personnelid FROM utb_shop_location_personnel slp2 WHERE slp.shoplocationid = slp2.shoplocationid)
                ) AS c ON (sl.ShopLocationID = c.ShopLocationID)
    LEFT JOIN (SELECT sp.ShopID, p.Name
                  FROM utb_shop_personnel AS sp
                 INNER JOIN utb_personnel AS p ON (sp.PersonnelID = p.PersonnelID)
                 INNER JOIN utb_personnel_type AS pt ON (p.PersonnelTypeID = pt.PersonnelTypeID AND pt.Name = 'Ower/officer Contact 1')
                 WHERE sp.personnelid IN (SELECT TOP 1 personnelid FROM utb_shop_personnel sp2 WHERE sp.ShopID = sp2.ShopID)
                ) AS o ON (sl.ShopID = o.ShopID)
      
    WHERE sl.ProgramFlag = 1
      AND sl.EnabledFlag = 1
    
    
    UNION ALL
    
    -- Export the Lien Holder data
   SELECT case 
            when lh.ParentLienHolderID is null then lh.LienHolderID
            else lh.ParentLienHolderID
          end, 
          lh.BusinessTypeCD,
          lh.LienHolderID, 
          lh.Name, 
          NULL,
          NULL,
          lh.FedTaxId, 
          lh.ContactName,
          NULL,
          lh.Address1,
          lh.Address2, 
          replace(lh.AddressCity, ',', ''), 
          lh.AddressState, 
          lh.AddressZip,
          lh.PhoneAreaCode, 
          lh.PhoneExchangeNumber,
          lh.PhoneUnitNumber,
          lh.FaxAreaCode,
          lh.FaxExchangeNumber,
          lh.FaxUnitNumber,
          b.BillingID, 
          b.Name BillingName, 
          b.Address1 BillingAddress1,
          b.Address2 BillingAddress2, 
          replace(b.AddressCity, ',', '') BillingAddressCity, 
          b.AddressState BillingAddressState, 
          b.AddressZip BillingAddressZip,
          b.PhoneAreaCode BillingPhoneAreaCode, 
          b.PhoneExchangeNumber BillingPhoneExchangeNumber, 
          b.PhoneUnitNumber BillingPhoneUnitNumber,
          b.FaxAreaCode BillingFaxAreaCode, 
          b.FaxExchangeNumber BillingFaxExchangeNumber, 
          b.FaxUnitNumber BillingFaxUnitNumber
     FROM utb_lien_holder AS lh
     INNER JOIN utb_billing AS b ON (lh.BillingID = b.BillingID)
      
    WHERE lh.EnabledFlag = 1
      AND lh.FedTaxId IS NOT NULL
      
    ORDER BY 1, 3    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDExtractForAGC' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAPDExtractForAGC TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/