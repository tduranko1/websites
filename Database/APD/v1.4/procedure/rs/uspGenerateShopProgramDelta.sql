-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGenerateShopProgramDelta' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGenerateShopProgramDelta 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGenerateShopProgramDelta
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This will generate the shop delta for all active clients and store them in the delta table
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGenerateShopProgramDelta
AS
BEGIN
    declare @InsuranceCompanyID          udt_std_int_small
    declare @ShopSelector                varchar(1)
    declare @WarrantyRefinishMinYrs      tinyint
    declare @WarrantyWorkmanshipMinYrs   tinyint
    declare @now                         datetime
    declare @CCAV_Client                 bit
    declare @RRP						 bit

    set @now = CURRENT_TIMESTAMP
   
    declare curInsCoList cursor for
        select InsuranceCompanyID 
        from utb_insurance 
        where EnabledFlag = 1
          and DemoFlag = 0
          --and InsuranceCompanyID in (282)
    
    declare @tbl_CurrentList table (ShopLocationID        bigint,
                                    InsuranceCompanyID    smallint,
                                    ProgramFlag            bit,
                                    CEIProgramFlag         bit)

    declare @tbl_DeltaData table ( ShopLocationID         bigint,
                               InsuranceCompanyID     smallint,
                               ActionCD               varchar(4))


    begin transaction

    open curInsCoList
    
    fetch next from curInsCoList into @InsuranceCompanyID
    
    while @@fetch_status = 0
    begin
                               
		set @RRP = 0
		if exists(select * from utb_client_assignment_type where InsuranceCompanyID = @InsuranceCompanyID and AssignmentTypeID=16)
		begin
			set @RRP = 1
		end
		
		print '@InsuranceCompanyID = ' + convert(varchar, @InsuranceCompanyID) + ' RRP = ' + convert(varchar, @RRP)
		
		
		
        -- clear the temp table
        delete from @tbl_CurrentList
        delete from @tbl_DeltaData

        -- Get Warranty requirements for carrier
        
        select @WarrantyRefinishMinYrs = convert(tinyint, WarrantyPeriodRefinishMinCD),
               @WarrantyWorkmanshipMinYrs = convert(tinyint, WarrantyPeriodWorkmanshipMinCD),
               @CCAV_Client =  case
                                    when ReturnDocRoutingCD = 'CCAV' then 1
                                    else 0
                               end
        from dbo.utb_insurance
        where InsuranceCompanyID = @InsuranceCompanyID
        
        if @WarrantyRefinishMinYrs is null
        begin
            -- The carrier had no configuration for a minimum refinish warranty, set minimum to 0
        
            set @WarrantyRefinishMinYrs = 0
        end
        
        if @WarrantyWorkmanshipMinYrs is null
        begin
            -- The carrier had no configuration for a minimum workmanship warranty, set minimum to 0
        
            set @WarrantyWorkmanshipMinYrs = 0
        end
        if @RRP = 1
        begin
			insert into @tbl_CurrentList
			select  sl.ShopLocationID, @InsuranceCompanyID, sl.ProgramFlag, sl.CEIProgramFlag
				  from  dbo.utb_shop_location sl
				  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
				  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1) csli on (sl.ShopLocationID = csli.ShopLocationID)
				  inner join dbo.utb_client_business_state cbs on (@InsuranceCompanyID = cbs.InsuranceCompanyID and sl.AddressState = cbs.StateCode)
				  where sl.EnabledFlag = 1
				    and sl.ReferralFlag = 1
					and sl.AvailableForSelectionFlag = 1
					and sl.AddressState <> 'MA'
					and isnull(convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
					and isnull(convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
					and csle.InsuranceCompanyID is null
					and (csli.InsuranceCompanyID = @InsuranceCompanyID or csli.InsuranceCompanyID is null)        
        end
        else
        begin
			if @CCAV_Client = 1
			begin
				insert into @tbl_CurrentList
				select  sl.ShopLocationID, @InsuranceCompanyID, sl.ProgramFlag, sl.CEIProgramFlag
					  from  dbo.utb_shop_location sl
					  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
					  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1) csli on (sl.ShopLocationID = csli.ShopLocationID)
					  inner join dbo.utb_client_contract_state ccs on (@InsuranceCompanyID = ccs.InsuranceCompanyID and sl.AddressState = ccs.StateCode)
					  where ((ccs.UseCEIShopsFlag = 1 and sl.CEIProgramFlag = 1) or (ccs.UseCEIShopsFlag = 0 and sl.ProgramFlag = 1))							
						and sl.EnabledFlag = 1
						and sl.AvailableForSelectionFlag = 1
						and sl.AddressState <> 'MA'
						and isnull(convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
						and isnull(convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
						and csle.InsuranceCompanyID is null
						and (csli.InsuranceCompanyID = @InsuranceCompanyID or csli.InsuranceCompanyID is null)
			end
			else
			begin
				insert into @tbl_CurrentList
				select  sl.ShopLocationID, @InsuranceCompanyID, sl.ProgramFlag, sl.CEIProgramFlag
					  from  dbo.utb_shop_location sl
					  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
					  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1) csli on (sl.ShopLocationID = csli.ShopLocationID)
					  inner join dbo.utb_client_business_state cbs on (@InsuranceCompanyID = cbs.InsuranceCompanyID and sl.AddressState = cbs.StateCode)
					  where sl.EnabledFlag = 1
						and sl.AvailableForSelectionFlag = 1
						and sl.AddressState <> 'MA'
						and isnull(convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
						and isnull(convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
						and csle.InsuranceCompanyID is null
						and (csli.InsuranceCompanyID = @InsuranceCompanyID or csli.InsuranceCompanyID is null)
			end
		end

        /*-- First we look for ADDS
        insert into @tbl_DeltaData
        select cl.ShopLocationID,
               cl.InsuranceCompanyID,
               'ADD'
        from @tbl_CurrentList cl left outer join utb_shop_location_program_delta slpd
                                    on (cl.ShopLocationID = slpd.ShopLocationID and cl.InsuranceCompanyID = slpd.InsuranceCompanyID)
        where slpd.ShopLocationID is null
        
        -- Now we add in the deletes
        insert into @tbl_DeltaData
        select slpd.ShopLocationID,
               slpd.InsuranceCompanyID,
               'REMV'
        from utb_shop_location_program_delta slpd left outer join @tbl_CurrentList cl
                        on (slpd.ShopLocationID = cl.ShopLocationID and slpd.InsuranceCompanyID = cl.InsuranceCompanyID)
        where slpd.InsuranceCompanyID = @InsuranceCompanyID
          and cl.ShopLocationID is null*/

        -- First we look for ADDS
        -- Add shops that are in the current list and not in the previous delta
        insert into @tbl_DeltaData
        select cl.ShopLocationID,
               cl.InsuranceCompanyID,
               'ADD'
        from @tbl_CurrentList cl 
        where cl.ShopLocationID not in (select ShopLocationID
                                        from utb_shop_location_program_delta slpd
                                        where slpd.InsuranceCompanyID = @InsuranceCompanyID)
        
        -- Now we add in the deletes
        -- delete shops that were updated/added to the previous delta and not in the current list.
        insert into @tbl_DeltaData
        select slpd.ShopLocationID,
               slpd.InsuranceCompanyID,
               'REMV'
        from utb_shop_location_program_delta slpd
        where slpd.ShopLocationID not in (select ShopLocationID
                                          from @tbl_CurrentList cl
                                          where cl.InsuranceCompanyID = @InsuranceCompanyID)
          and slpd.InsuranceCompanyID = @InsuranceCompanyID          
          and slpd.ActionCD <> 'D'
          
        delete from utb_shop_location_program_delta where InsuranceCompanyID = @InsuranceCompanyID
        
        if @@error <> 0
        begin
            raiserror ('Error occured update previous view',16,1)
            rollback
            return
        end

        /*insert into utb_shop_location_program_delta 
        select  cl.ShopLocationID, 
                cl.InsuranceCompanyID,
                case
                    when ActionCD = 'ADD' then 'A'
                    when ActionCD = 'REMV' then 'D'
                    else 'U'
                end,
                @now
        from @tbl_CurrentList cl
        left join @tbl_DeltaData rd on cl.ShopLocationID = rd.ShopLocationID and cl.InsuranceCompanyID = rd.InsuranceCompanyID
        where cl.InsuranceCompanyID = @InsuranceCompanyID*/
        
        insert into utb_shop_location_program_delta
        select  cl.ShopLocationID, 
                cl.InsuranceCompanyID,
                'U',
                @now
        from @tbl_CurrentList cl
        where cl.ShopLocationID not in (select ShopLocationID from @tbl_DeltaData)
        
        insert into utb_shop_location_program_delta
        select  rd.ShopLocationID, 
                rd.InsuranceCompanyID,
                case
                    when rd.ActionCD = 'ADD' then 'A'
                    when rd.ActionCD = 'REMV' then 'D'
                    else 'U'
                end,
                @now
        from @tbl_DeltaData rd
        where rd.InsuranceCompanyID = @InsuranceCompanyID

        if @@error <> 0
        begin
            raiserror ('Error occured update previous view',16,1)
            rollback
            return
        end

        -- select * from @DeltaData
        -- select * from utb_shop_location_program_delta where InsuranceCompanyID = @InsuranceCompanyID

        fetch next from curInsCoList into @InsuranceCompanyID
    end
    
    close curInsCoList
    
    commit
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGenerateShopProgramDelta' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGenerateShopProgramDelta TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/