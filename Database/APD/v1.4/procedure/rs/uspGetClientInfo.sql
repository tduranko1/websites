-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientInfo' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientInfo 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientInfo
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client information from the utb_insurance table
*
* PARAMETERS:  
*
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClientInfo
	@iInsuranceCompanyID INT = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	IF (@iInsuranceCompanyID = 0)
	BEGIN
		SELECT
			InsuranceCompanyID
			, ISNULL(DeskAuditPreferredCommunicationMethodID,'') DeskAuditPreferredCommunicationMethodID
			, ISNULL(Address1,'') Address1
			, ISNULL(Address2,'') Address2
			, ISNULL(AddressCity,'') AddressCity
			, ISNULL(AddressState,'') AddressState
			, ISNULL(AddressZip,'') AddressZip
			, ISNULL(AssignmentAtSelectionFlag,'') AssignmentAtSelectionFlag
			, ISNULL(AuthorizeLaborRatesFlag,'') AuthorizeLaborRatesFlag
			, ISNULL(BillingModelCD,'') BillingModelCD
			, ISNULL(BusinessTypeCD,'') BusinessTypeCD
			, ISNULL(CarrierLynxContactPhone,'') CarrierLynxContactPhone
			, ISNULL(CFLogoDisplayFlag,'') CFLogoDisplayFlag 
			, ISNULL(ClaimPointCarrierRepSelFlag,'') ClaimPointCarrierRepSelFlag 
			, ISNULL(ClaimPointDocumentUploadFlag,'') ClaimPointDocumentUploadFlag 
			, ISNULL(ClientAccessFlag,'') ClientAccessFlag 
			, ISNULL(DemoFlag,'') DemoFlag 
			, ISNULL(DeskAuditCompanyCD,'') DeskAuditCompanyCD
			, ISNULL(DeskReviewLicenseReqFlag,'') DeskReviewLicenseReqFlag 
			, ISNULL(EarlyBillFlag,'') EarlyBillFlag 
			, ISNULL(EarlyBillStartDate,'') EarlyBillStartDate  
			, ISNULL(EnabledFlag,'') EnabledFlag 
			, ISNULL(FaxAreaCode,'') FaxAreaCode
			, ISNULL(FaxExchangeNumber,'') FaxExchangeNumber
			, ISNULL(FaxExtensionNumber,'') FaxExtensionNumber
			, ISNULL(FaxUnitNumber,'') FaxUnitNumber
			, ISNULL(FedTaxId,'') FedTaxId    
			, ISNULL(IngresAccountingId ,'') IngresAccountingId
			, ISNULL(InvoicingModelPaymentCD,'') InvoicingModelPaymentCD 
			, ISNULL(InvoiceMethodCD,'') InvoiceMethodCD 
			, ISNULL(LicenseDeterminationCD,'') LicenseDeterminationCD
			, ISNULL([Name],'') [Name]                                               
			, ISNULL(PhoneAreaCode,'') PhoneAreaCode
			, ISNULL(PhoneExchangeNumber,'') PhoneExchangeNumber
			, ISNULL(PhoneExtensionNumber,'') PhoneExtensionNumber
			, ISNULL(PhoneUnitNumber,'') PhoneUnitNumber
			, ISNULL(ReturnDocDestinationCD,'') ReturnDocDestinationCD
			, ISNULL(ReturnDocPackageTypeCD,'') ReturnDocPackageTypeCD
			, ISNULL(ReturnDocRoutingCD ,'') ReturnDocRoutingCD
			, ISNULL(TotalLossValuationWarningPercentage,0) TotalLossValuationWarningPercentage   
			, ISNULL(TotalLossWarningPercentage,0) TotalLossWarningPercentage         
			, ISNULL(WarrantyPeriodRefinishMinCD,'') WarrantyPeriodRefinishMinCD
			, ISNULL(WarrantyPeriodWorkmanshipMinCD ,'') WarrantyPeriodWorkmanshipMinCD
			, ISNULL(SysLastUserID ,0) SysLastUserID
			, SysLastUpdatedDate
		FROM
			 dbo.utb_insurance
		ORDER BY
			[Name] 
	END
	ELSE
	BEGIN
		SELECT
			InsuranceCompanyID
			, ISNULL(DeskAuditPreferredCommunicationMethodID,'') DeskAuditPreferredCommunicationMethodID
			, ISNULL(Address1,'') Address1
			, ISNULL(Address2,'') Address2
			, ISNULL(AddressCity,'') AddressCity
			, ISNULL(AddressState,'') AddressState
			, ISNULL(AddressZip,'') AddressZip
			, ISNULL(AssignmentAtSelectionFlag,'') AssignmentAtSelectionFlag
			, ISNULL(AuthorizeLaborRatesFlag,'') AuthorizeLaborRatesFlag
			, ISNULL(BillingModelCD,'') BillingModelCD
			, ISNULL(BusinessTypeCD,'') BusinessTypeCD
			, ISNULL(CarrierLynxContactPhone,'') CarrierLynxContactPhone
			, ISNULL(CFLogoDisplayFlag,'') CFLogoDisplayFlag 
			, ISNULL(ClaimPointCarrierRepSelFlag,'') ClaimPointCarrierRepSelFlag 
			, ISNULL(ClaimPointDocumentUploadFlag,'') ClaimPointDocumentUploadFlag 
			, ISNULL(ClientAccessFlag,'') ClientAccessFlag 
			, ISNULL(DemoFlag,'') DemoFlag 
			, ISNULL(DeskAuditCompanyCD,'') DeskAuditCompanyCD
			, ISNULL(DeskReviewLicenseReqFlag,'') DeskReviewLicenseReqFlag 
			, ISNULL(EarlyBillFlag,'') EarlyBillFlag 
			, ISNULL(EarlyBillStartDate,'') EarlyBillStartDate  
			, ISNULL(EnabledFlag,'') EnabledFlag 
			, ISNULL(FaxAreaCode,'') FaxAreaCode
			, ISNULL(FaxExchangeNumber,'') FaxExchangeNumber
			, ISNULL(FaxExtensionNumber,'') FaxExtensionNumber
			, ISNULL(FaxUnitNumber,'') FaxUnitNumber
			, ISNULL(FedTaxId,'') FedTaxId    
			, ISNULL(IngresAccountingId ,'') IngresAccountingId
			, ISNULL(InvoicingModelPaymentCD,'') InvoicingModelPaymentCD 
			, ISNULL(InvoiceMethodCD,'') InvoiceMethodCD 
			, ISNULL(LicenseDeterminationCD,'') LicenseDeterminationCD
			, ISNULL([Name],'') [Name]                                               
			, ISNULL(PhoneAreaCode,'') PhoneAreaCode
			, ISNULL(PhoneExchangeNumber,'') PhoneExchangeNumber
			, ISNULL(PhoneExtensionNumber,'') PhoneExtensionNumber
			, ISNULL(PhoneUnitNumber,'') PhoneUnitNumber
			, ISNULL(ReturnDocDestinationCD,'') ReturnDocDestinationCD
			, ISNULL(ReturnDocPackageTypeCD,'') ReturnDocPackageTypeCD
			, ISNULL(ReturnDocRoutingCD ,'') ReturnDocRoutingCD
			, ISNULL(TotalLossValuationWarningPercentage,0) TotalLossValuationWarningPercentage   
			, ISNULL(TotalLossWarningPercentage,0) TotalLossWarningPercentage         
			, ISNULL(WarrantyPeriodRefinishMinCD,'') WarrantyPeriodRefinishMinCD
			, ISNULL(WarrantyPeriodWorkmanshipMinCD ,'') WarrantyPeriodWorkmanshipMinCD
			, ISNULL(SysLastUserID ,0) SysLastUserID
			, SysLastUpdatedDate
		FROM
			 dbo.utb_insurance
		WHERE
			InsuranceCompanyID = @iInsuranceCompanyID
		ORDER BY
			[Name] 
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientInfo' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientInfo TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientInfo TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/