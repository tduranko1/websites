-- Begin the transaction for dropping and creating the stored procedure

Go

-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFUpdateFormDataDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFUpdateFormDataDetail
END


GO
/****** Object:  StoredProcedure [dbo].[uspCFUpdateFormDataDetail]    Script Date: 02/28/2013 10:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspCFUpdateFormDataDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       glsd451
* FUNCTION:     Update form data one field at a time
*
* PARAMETERS:  
* (I) @FormDataID           Parent Form Data ID
* (I) @FieldName            Field Name as defined by the control
* (I) @FieldValue           The value of the field
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
--EXEC [uspCFUpdateFormDataDetail] 29964,'SequenceNumber',1

--ALTER PROCEDURE [dbo].[uspCFUpdateFormDataDetail]
--    @FormDataID             udt_std_id_big,
--    @FieldName              udt_sys_login,
--    @FieldValue             udt_std_desc_huge
--AS
--BEGIN
--    -- SET NOCOUNT to ON and no longer display the count message
    
--    SET NOCOUNT ON

--    -- Declare internal variables

--    DECLARE @error AS int
--    DECLARE @rowcount AS int
    
--    DECLARE @KeyValue               AS varchar(40)
--    DECLARE @now                    AS datetime
--    DECLARE @FieldID                AS udt_std_int_big

--    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

--    SET @ProcName = 'uspCFUpdateFormDataDetail'

    
--    -- Check to make sure a valid Form Data Id was passed
--    IF  (@FormDataID IS NULL) OR
--        (NOT EXISTS(SELECT FormDataID FROM dbo.utb_form_data WHERE FormDataID = @FormDataID))
--    BEGIN
--        -- Invalid Form Data ID
    
--        RAISERROR('101|%s|@FormDataID|%u', 16, 1, @ProcName, @FormDataID)
--        RETURN
--    END

--    -- Begin Update

--    BEGIN TRANSACTION

--    IF @@ERROR <> 0
--    BEGIN
--       -- SQL Server Error
    
--        RAISERROR  ('99|%s', 16, 1, @ProcName)
--        RETURN
--    END


--    -- Get the Field ID
--     SELECT @FieldID = FDD.FormFieldID
--		FROM dbo.utb_form_field AS ff 
--			INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  
--			where formdataid = @FormDataID and Name =@FieldName
   
   
--     -- Check to make sure a valid Form Data Id was passed
--    IF  (@FieldID IS NULL) OR
--        (NOT EXISTS(SELECT [FormFieldID] FROM dbo.utb_form_data_detail WHERE [FormDataID] =@FormDataID AND [FormFieldID] = @FieldID))
--    BEGIN
--        -- Invalid Field ID
    
--        RAISERROR('101|%s|@FieldID|%u', 16, 1, @ProcName, @FieldID)
--        RETURN
--    END
   
   
   

--    UPDATE dbo.utb_form_data_detail 
--    SET FieldData = @FieldValue
--    WHERE [FormDataID] =@FormDataID AND 
--           [FormFieldID] = @FieldID
           
----update utb_form_data_detail set fielddata = '0' where formdataid = 29964 and formfieldid = 277
--    IF @@error <> 0
--    BEGIN
--        -- SQL Server Error

--        RAISERROR  ('105|%s|utb_form_data_detail', 16, 1, @ProcName)
--        ROLLBACK TRANSACTION 
--        RETURN
--    END

--    COMMIT TRANSACTION

--    IF @@ERROR <> 0
--    BEGIN
--       -- SQL Server Error
    
--        RAISERROR  ('99|%s', 16, 1, @ProcName)
--        RETURN
--    END

--    RETURN @FormDataID 

--END


--GO

-- Permissions

--IF EXISTS (SELECT name FROM sysobjects 
--            WHERE name = 'uspCFUpdateFormDataDetail' AND type = 'P')
--BEGIN
--    GRANT EXECUTE ON dbo.uspCFUpdateFormDataDetail TO 
--        ugr_lynxapd

--    -- Commit the transaction for dropping and creating the stored procedure

--    COMMIT
--END
--ELSE
--BEGIN
--    -- There was an error...rollback the transaction for dropping and creating the stored procedure

--    RAISERROR ('Stored procedure creation failure.', 16, 1)
--    ROLLBACK
--END




