-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTNoteUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTNoteUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates an APD note
*
* PARAMETERS:  
* (I) @ClaimAspectID        The claim aspect this note is being updated to
* (I) @DocumentID           The note to update  
* (I) @NoteTypeID           The note type
* (I) @StatusID             The status the note pertains to
* (I) @Note                 The note itself
* (I) @OldClaimAspectID     The claim aspect this note is being updated for
* (I) @UserID               The user updating the note
* (I) @SysLastUpdatedDate   The "previous" updated date
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTNoteUpdDetail
    @ShopLocationID     udt_std_id_big,
    @DocumentID         udt_std_id_big,  
    @Note               udt_std_note,
    @UserID             udt_std_id,
    @SysLastUpdatedDate varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Note))) = 0 SET @Note = NULL

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTNoteUpdDetail'

    
    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Shop Location id was passed in
    IF  (@ShopLocationID IS NULL) OR
        (NOT EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID))
    BEGIN
        -- Invalid Shop Location ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END
    
        

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate the Last updated date

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_document', @DocumentID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.
    
        RETURN
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION NoteUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update the note

    UPDATE dbo.utb_document
    SET Note                      = @Note,
		    SysLastUserID             = @UserID,
		    SysLastUpdatedDate        = @now
    WHERE DocumentID = @DocumentID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    

    COMMIT TRANSACTION NoteUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTNoteUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/