-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientDeskAuditSummary' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientDeskAuditSummary 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientDeskAuditSummary
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Desk Audit Summary
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ReportTypeCD         Specifies whether the user is looking for the (C)lient report or the (O)ffice report
* (I) @ServiceChannelCD     Specifies the Service Channel "DA" - Desk Audit or "DR" - Desk Review
* (I) @OfficeID             Optional office the report may be run for 
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientDeskAuditSummary
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ReportTypeCD           udt_std_cd        = 'C',
    @ServiceChannelCD       udt_std_cd        = 'DA',
    @OfficeID               udt_std_int_small = NULL,
    @AssignmentTypeID       udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportDataMonth TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2GroupID               int          NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        TotalOriginalEstimate       decimal(15, 2) NULL,
        AvgOriginalEstimate         decimal(15, 2) NULL,
        TotalAuditedEstimate        decimal(15, 2) NULL,
        AvgAuditedEstimate          decimal(15, 2) NULL,
        TotalEstimateSavings        decimal(15, 2) NULL,
        AvgEstimateSavings          decimal(15, 2) NULL,
        EstimateSavingsPercent      decimal(5,2) NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,
        AvgCycleTimeEstToClose      decimal(6,1) NULL
    )

    DECLARE @tmpReportDataYTD TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2GroupID               int          NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        TotalOriginalEstimate       decimal(15, 2) NULL,
        AvgOriginalEstimate         decimal(15, 2) NULL,
        TotalAuditedEstimate        decimal(15, 2) NULL,
        AvgAuditedEstimate          decimal(15, 2) NULL,
        TotalEstimateSavings        decimal(15, 2) NULL,
        AvgEstimateSavings          decimal(15, 2) NULL,
        EstimateSavingsPercent      decimal(5,2) NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,
        AvgCycleTimeEstToClose      decimal(6,1) NULL
    )
    
    DECLARE @tmpReportData12Months TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2GroupID               int          NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        TotalOriginalEstimate       decimal(15, 2) NULL,
        AvgOriginalEstimate         decimal(15, 2) NULL,
        TotalAuditedEstimate        decimal(15, 2) NULL,
        AvgAuditedEstimate          decimal(15, 2) NULL,
        TotalEstimateSavings        decimal(15, 2) NULL,
        AvgEstimateSavings          decimal(15, 2) NULL,
        EstimateSavingsPercent      decimal(5,2) NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,
        AvgCycleTimeEstToClose      decimal(6,1) NULL
    )

    DECLARE @tmpNewData TABLE
    (
        OfficeID                     int          NOT NULL,
        OfficeName                   varchar(50)  NOT NULL,
        NewCount                     int          NOT NULL,
        NewPercent                   int          NULL
    )
        
    DECLARE @tmpClosedData TABLE
    (
        OfficeID                    int          NOT NULL,
        OfficeName                  varchar(50)  NOT NULL,
        ClosedCount                 int          NOT NULL,
        ClosedPercent               int          NULL,
        TotalOriginalEstimate       decimal(15,2) NOT NULL,
        AvgOriginalEstimate         decimal(15,2) NOT NULL,
        TotalAuditedEstimate        decimal(15,2) NOT NULL,
        AvgAuditedEstimate          decimal(15,2) NOT NULL,
        TotalEstimateSavings        decimal(15,2) NOT NULL,
        AvgEstimateSavings          decimal(15,2) NOT NULL,
        EstimateSavingsPercent      decimal(5,2) NOT NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NOT NULL,
        AvgCycleTimeEstToClose      decimal(6,1) NOT NULL
    )

    DECLARE @tmpTimePeriods TABLE
    (
        PositionID                  tinyint     NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )

    
    DECLARE @ClosedCountMonth       udt_std_int
    DECLARE @ClosedCountYTD         udt_std_int
    DECLARE @ClosedCount12Month     udt_std_int
    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @NewCountMonth          udt_std_int
    DECLARE @NewCountYTD            udt_std_int
    DECLARE @NewCount12Month        udt_std_int
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    DECLARE @ServiceChannelCDWork   udt_std_cd
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @AssignmentTypeName     udt_std_name

    DECLARE @Debug      			udt_std_flag
    DECLARE @now        			udt_std_datetime
    DECLARE @DataWarehouseDate    	udt_std_datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int
    

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientDeskAuditSummary'

    SET @Debug = 0

    DECLARE @tmpValidAssignmentTypeID TABLE 
    (
        AssignmentTypeID        int            NOT NULL
    )

    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
   WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate Report Type
    
    IF @ReportTypeCD NOT IN ('C', 'O')
    BEGIN
        -- Invalid Report Type
        
        RAISERROR  ('101|%s|@ReportTypeCD|%s', 16, 1, @ProcName, @ReportTypeCD)
        RETURN
    END
        
   
    -- Validate Service Channel Code
    
    IF @ServiceChannelCD NOT IN ('DA', 'DR')
    BEGIN
        -- Invalid Service Channel CD
    
        RAISERROR  ('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
        RETURN
    END
    ELSE
    BEGIN
        SET @ServiceChannelCDWork = @ServiceChannelCD
        
        SELECT  @ServiceChannelName = Name
          FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
          WHERE Code = @ServiceChannelCD
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END

    
    -- Validate Office ID
    
    IF @ReportTypeCD = 'O'
    BEGIN
        IF @OfficeID IS NOT NULL
        BEGIN
            IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
            BEGIN
                -- Invalid Office ID
        
                RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
                RETURN
            END
            ELSE
            BEGIN
                SET @OfficeIDWork = @OfficeID
            
                SELECT  @OfficeName = Name
                  FROM  dbo.utb_office
                  WHERE OfficeID = @OfficeID
                  
                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error
    
                    RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                    RETURN
                END
            END
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = '%'
            SET @OfficeName = 'All'
        END          
    END
    
    -- Validate AssignmentTypeID
    IF @AssignmentTypeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT AssignmentTypeID
                        FROM dbo.utb_dtwh_dim_assignment_type
                        WHERE AssignmentTypeID = @AssignmentTypeID
                          AND AssignmentTypeDescription in (select Name 
                                                            from utb_assignment_type 
                                                            where ServiceChannelDefaultCD = @ServiceChannelCD)
                     )
        BEGIN
            RAISERROR  ('101|%s|@AssignmentTypeID|%u', 16, 1, @ProcName, @AssignmentTypeID)
            RETURN
        END
        
        -- AssignmentTypeID is valid. Add it to the temp table.
        INSERT INTO @tmpValidAssignmentTypeID
        VALUES(@AssignmentTypeID)

        SELECT @AssignmentTypeName = AssignmentTypeDescription
        FROM dbo.utb_dtwh_dim_assignment_type
        WHERE AssignmentTypeID = @AssignmentTypeID
    END
    ELSE
    BEGIN
        -- no AssignmentTypeID was passed. So we need to show all Audits
        INSERT INTO @tmpValidAssignmentTypeID
        SELECT AssignmentTypeID
        FROM dbo.utb_dtwh_dim_assignment_type
        WHERE AssignmentTypeDescription in (select Name 
                                            from utb_assignment_type 
                                            where ServiceChannelDefaultCD = @ServiceChannelCD)    
        
        SELECT @AssignmentTypeName = 'All Audits'
    END

    -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods
    
    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO @tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END


    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptMonthName = ' + @MonthName
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear, 101)
        PRINT '@OfficeID = ' + Convert(varchar, @OfficeID)
        PRINT '@OfficeName = ' + @OfficeName
        PRINT '@ServiceChannelCD = ' + @ServiceChannelCD
        PRINT '@ServiceChannelName = ' + @ServiceChannelName

        PRINT 'table: @tmpValidAssignmentTypeID'
        SELECT * FROM @tmpValidAssignmentTypeID
    END
    
    
    -- Compile data needed for the report
    
    -- Monthly Totals
    
    -- New 
    
    INSERT INTO @tmpNewData
      SELECT  0,
              'Company',
              Count(*) AS OpenCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          -- we should not look for Closed claims for the new count
          --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN      
        SELECT @NewCountMonth = NewCount
          FROM @tmpNewData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpNewData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS OpenCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @NewCountMonth), 2) * 100, 0) AS NewPercent
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dt.MonthOfYear = @RptMonth
              AND dt.YearValue = @RptYear
              AND dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              -- we should not look for Closed claims for the new count
              --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
    
    -- Closed
    
    -- Coding Note:  CASE is being used here when determining whether to use the Original Estimate or Audited Estimate
    -- amount in calculations.  Basically, if the audited amount is more than the original amount, the audited amount 
    -- is thrown away and the original is used.  This is because the insurance company pays the LOWEST amount between
    -- the original shop estimate and the estimate drawn up by LYNX.  This code allows the numbers to reflect this.
    
    INSERT INTO @tmpClosedData
      SELECT  0,
              'Company',
              Count(*) AS ClosedCount,
              NULL,
              IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Sum(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Avg(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                             WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                             ELSE fc.AuditedEstimatewoBettAmt
                                                           END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtc.MonthOfYear = @RptMonth
          AND dtc.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN      
        SELECT @ClosedCountMonth = ClosedCount
          FROM @tmpClosedData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpClosedData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS ClosedCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @ClosedCountMonth), 2) * 100, 0) AS ClosedPercent,
                  IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Sum(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Avg(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                 WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                 ELSE fc.AuditedEstimatewoBettAmt
                                                               END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dtc.MonthOfYear = @RptMonth
              AND dtc.YearValue = @RptYear
              AND dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
    END

    -- Save the data extracted

    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              OfficeID, 
              OfficeName,
              CASE OfficeID
                WHEN 0 THEN 1
                ELSE 0
              END,
              0,
              0,
              ClosedCount,
              ClosedPercent,
              TotalOriginalEstimate,
              AvgOriginalEstimate,
              TotalAuditedEstimate,
              AvgAuditedEstimate,
              TotalEstimateSavings,
              AvgEstimateSavings,
              EstimateSavingsPercent,
              AvgCycleTimeAssToEst,
              AvgCycleTimeEstToClose
        FROM  @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END
          
    UPDATE  @tmpReportDataMonth
      SET   NewTotal = nd.NewCount,
            NewPercent = nd.NewPercent
      FROM  @tmpNewData nd
      WHERE Level2GroupID = nd.OfficeID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonth', 16, 1, @ProcName)
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN
        INSERT INTO @tmpReportDataMonth
          SELECT  'Month',
                  @MonthName + ' ' + Convert(varchar(4), @RptYear),
                  OfficeID,
                  OfficeName,
                  0,
                  NewCount,
                  NewPercent,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  @tmpNewData
            WHERE OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportDataMonth)
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
            RETURN
        END

       INSERT INTO @tmpReportDataMonth
          SELECT  'Month',
                  @MonthName + ' ' + Convert(varchar(4), @RptYear),
                  OfficeID,
                  CASE
                    WHEN Len(Ltrim(RTrim(ClientOfficeId))) > 0 THEN ClientOfficeId
                    ELSE Name
                  END,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  dbo.utb_office
            WHERE InsuranceCompanyID = @InsuranceCompanyID
              AND OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportDataMonth)
              AND OfficeID LIKE @OfficeIDWork
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
            RETURN
        END
        
        IF @OfficeIDWork = '%'
        BEGIN
            DELETE FROM @tmpReportDataMonth
            WHERE Level2GroupID in (SELECT OfficeID 
                                 FROM utb_office 
                                 WHERE InsuranceCompanyID = @InsuranceCompanyID 
                                   AND EnabledFlag = 0)
        END
    END
              

    -- Reset the temporary tables for next cycle
    
    DELETE FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- YTD Totals
    
    INSERT INTO @tmpNewData
      SELECT  0,
              'Company',
              Count(*) AS OpenCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          -- we should not look for Closed claims for the new count
          --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END
          
    IF @ReportTypeCD = 'O'
    BEGIN
        SELECT @NewCountMonth = NewCount
          FROM @tmpNewData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpNewData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS OpenCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @NewCountMonth), 2) * 100, 0) AS NewPercent
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dt.MonthOfYear <= @RptMonth
              AND dt.YearValue = @RptYear
              AND dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              -- we should not look for Closed claims for the new count
              --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
    
    -- Closed
    
    INSERT INTO @tmpClosedData
      SELECT  0,
              'Company',
              Count(*) AS ClosedCount,
              NULL,
              IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Sum(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Avg(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                             WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                             ELSE fc.AuditedEstimatewoBettAmt
                                                             END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtc.MonthOfYear <= @RptMonth
          AND dtc.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN
        SELECT @ClosedCountMonth = ClosedCount
          FROM @tmpClosedData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpClosedData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS ClosedCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @ClosedCountMonth), 2) * 100, 0) AS ClosedPercent,
                  IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Sum(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Avg(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                 WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                 ELSE fc.AuditedEstimatewoBettAmt
                                                               END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dtc.MonthOfYear <= @RptMonth
              AND dtc.YearValue = @RptYear
              AND dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
    END

    -- Save the data extracted

    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              OfficeID, 
              OfficeName,
              CASE OfficeID
                WHEN 0 THEN 1
                ELSE 0
              END,              
              0,
              0,
              ClosedCount,
              ClosedPercent,
              TotalOriginalEstimate,
              AvgOriginalEstimate,
              TotalAuditedEstimate,
              AvgAuditedEstimate,
              TotalEstimateSavings,
              AvgEstimateSavings,
              EstimateSavingsPercent,
              AvgCycleTimeAssToEst,
              AvgCycleTimeEstToClose
        FROM  @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END
          
    UPDATE  @tmpReportDataYTD
      SET   NewTotal = nd.NewCount,
            NewPercent = nd.NewPercent
      FROM  @tmpNewData nd
      WHERE Level2GroupID = nd.OfficeID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataYTD', 16, 1, @ProcName)
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN
        INSERT INTO @tmpReportDataYTD
          SELECT  'YTD',
                  CASE 
                    WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                    ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                  END,
                  OfficeID,
                  OfficeName,
                  0,
                  NewCount,
                  NewPercent,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  @tmpNewData
            WHERE OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportDataYTD)
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
            RETURN
        END

       INSERT INTO @tmpReportDataYTD
          SELECT  'YTD',
                  CASE 
                    WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                    ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                  END,
                  OfficeID,
                  CASE
                    WHEN Len(Ltrim(RTrim(ClientOfficeId))) > 0 THEN ClientOfficeId
                    ELSE Name
                  END,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  dbo.utb_office
            WHERE InsuranceCompanyID = @InsuranceCompanyID
              AND OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportDataYTD)
              AND OfficeID LIKE @OfficeIDWork
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
            RETURN
        END

        IF @OfficeIDWork = '%'
        BEGIN
            DELETE FROM @tmpReportDataYTD
            WHERE Level2GroupID in (SELECT OfficeID 
                                 FROM utb_office 
                                 WHERE InsuranceCompanyID = @InsuranceCompanyID 
                                   AND EnabledFlag = 0)
        END
    END
              

    -- Reset the temporary tables for next cycle
    
    DELETE FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- Previous 12 Month Totals
    
    INSERT INTO @tmpNewData
      SELECT  0,
              'Company',
              Count(*) AS OpenCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          -- we should not look for Closed claims for the new count
          --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END
          
    IF @ReportTypeCD = 'O'
    BEGIN
        SELECT @NewCountMonth = NewCount
          FROM @tmpNewData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpNewData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS OpenCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @NewCountMonth), 2) * 100, 0) AS NewPercent
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              -- we should not look for Closed claims for the new count
              --AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
    
    -- Closed
    
    INSERT INTO @tmpClosedData
      SELECT  0,
              'Company',
              Count(*) AS ClosedCount,
              NULL,
              IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
              IsNull(Sum(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Avg(CASE
                           WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                           ELSE fc.AuditedEstimatewoBettAmt
                         END), 0),
              IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                            WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                            ELSE fc.AuditedEstimatewoBettAmt
                                                          END), 0),
              IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                             WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                             ELSE fc.AuditedEstimatewoBettAmt
                                                           END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dtc.MonthOfYear) + '/' + Convert(varchar(4), dtc.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN
        SELECT @ClosedCountMonth = ClosedCount
          FROM @tmpClosedData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO @tmpClosedData
          SELECT  dc.OfficeID,
                  CASE 
                    WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
                    ELSE dc.OfficeName
                  END,
                  Count(*) AS ClosedCount,
                  IsNull(Round(Convert(decimal(15, 2), Count(*)) / Convert(decimal(15, 2), @ClosedCountMonth), 2) * 100, 0) AS ClosedPercent,
                  IsNull(Sum(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt), 0),
                  IsNull(Sum(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Avg(CASE
                               WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                               ELSE fc.AuditedEstimatewoBettAmt
                             END), 0),
                  IsNull(Sum(fc.OriginalEstimateGrossAmt) - Sum(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull(Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                ELSE fc.AuditedEstimatewoBettAmt
                                                              END), 0),
                  IsNull((Avg(fc.OriginalEstimateGrossAmt) - Avg(CASE
                                                                 WHEN fc.AuditedEstimatewoBettAmt > fc.OriginalEstimateGrossAmt THEN fc.OriginalEstimateGrossAmt
                                                                 ELSE fc.AuditedEstimatewoBettAmt
                                                               END)) / Avg(fc.OriginalEstimateGrossAmt) * 100, 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToEstHrs)), 1), 0),
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeEstToCloseHrs)), 1), 0)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        	LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dtc.MonthOfYear) + '/' + Convert(varchar(4), dtc.YearValue) = tmpTP.TimePeriod) 
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
              AND fc.CoverageTypeID IS NOT NULL
              AND fc.AssignmentTypeClosingID IN (SELECT AssignmentTypeID FROM @tmpValidAssignmentTypeID)
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dc.OfficeID, dc.OfficeClientId, dc.OfficeName
            HAVING dc.OfficeID LIKE @OfficeIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
    END

    -- Save the data extracted

    INSERT INTO @tmpReportData12Months
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              OfficeID, 
              OfficeName,
              CASE OfficeID
                WHEN 0 THEN 1
                ELSE 0
              END,                            
              0,
              0,
              ClosedCount,
              ClosedPercent,
              TotalOriginalEstimate,
              AvgOriginalEstimate,
              TotalAuditedEstimate,
              AvgAuditedEstimate,
              TotalEstimateSavings,
              AvgEstimateSavings,
              EstimateSavingsPercent,
              AvgCycleTimeAssToEst,
              AvgCycleTimeEstToClose
        FROM  @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Months', 16, 1, @ProcName) 
        RETURN
    END
          
    UPDATE  @tmpReportData12Months
      SET   NewTotal = nd.NewCount,
            NewPercent = nd.NewPercent
      FROM  @tmpNewData nd
      WHERE Level2GroupID = nd.OfficeID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportData12Months', 16, 1, @ProcName)
        RETURN
    END

    IF @ReportTypeCD = 'O'
    BEGIN
        INSERT INTO @tmpReportData12Months
          SELECT  '12Mth',
                  '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
                  OfficeID,
                  OfficeName,
                  0, 
                  NewCount,
                  NewPercent,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  @tmpNewData
            WHERE OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportData12Months)
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportData12Months', 16, 1, @ProcName) 
            RETURN
        END

       INSERT INTO @tmpReportData12Months
          SELECT  '12Mth',
                  '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
                  OfficeID,
                  CASE
                    WHEN Len(Ltrim(RTrim(ClientOfficeId))) > 0 THEN ClientOfficeId
                    ELSE Name
                  END,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  dbo.utb_office
            WHERE InsuranceCompanyID = @InsuranceCompanyID
              AND OfficeID NOT IN (SELECT Level2GroupID FROM @tmpReportData12Months)
              AND OfficeID LIKE @OfficeIDWork
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportData12Months', 16, 1, @ProcName) 
            RETURN
        END

        IF @OfficeIDWork = '%'
        BEGIN
            DELETE FROM @tmpReportData12Months
            WHERE Level2GroupID in (SELECT OfficeID 
                                 FROM utb_office 
                                 WHERE InsuranceCompanyID = @InsuranceCompanyID 
                                   AND EnabledFlag = 0)
        END
    END
    
        
    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            @AssignmentTypeName AS AssignmentTypeName,
            1 AS Level1Sort,
            tmp.*,
            @DataWareHouseDate as DataWareDate   
      FROM  @tmpReportDataMonth tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            @AssignmentTypeName AS AssignmentTypeName,
            2 AS Level1Sort,
            tmp.*,
            @DataWareHouseDate as DataWareDate   
      FROM  @tmpReportDataYTD tmp
    
    UNION ALL

    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            @AssignmentTypeName AS AssignmentTypeName,
            3 AS Level1Sort,
            tmp.*,
            @DataWareHouseDate as DataWareDate   
      FROM  @tmpReportData12Months tmp
             
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientDeskAuditSummary' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientDeskAuditSummary TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
