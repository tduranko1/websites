-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptPendingTasks' AND type = 'P')
BEGIN
    DROP PROCEDURE uspRptPendingTasks 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptPendingTasks
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Produces a pending task report for APD
*
* PARAMETERS:   None  
*
* RESULT SET:   Report information
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptPendingTasks
AS
BEGIN
    -- Set Database Options
    
    SET NOCOUNT ON 
    
    
    -- Final selects

    SELECT Convert(varchar, CURRENT_TIMESTAMP, 100) AS 'Run Date'
    
    
    SELECT  Convert(varchar(50), tu.NameLast + ', ' + tu.NameFirst) AS 'Task Assigned To',
            t.Name AS Task,
            Convert(varchar, ca.LynxID) + '-' + Convert(varchar, ca.ClaimAspectNumber) AS LynxID,
            Convert(varchar(20), c.AlarmDate, 101) AS 'Due',
            i.Name AS 'Insurance Company',
            casc.ServiceChannelCD AS 'Service Channel',
            CASE 
              WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN Convert(varchar(50), au.NameLast + ', ' + au.NameFirst)
              ELSE Convert(varchar(50), ou.NameLast + ', ' + ou.NameFirst)
            END AS 'File Assigned To',
            1 AS 'Task Count'
      FROM  dbo.utb_checklist c
      LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)
      LEFT JOIN dbo.utb_user tu ON (c.AssignedUserID = tu.UserID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
      LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)      
      LEFT JOIN dbo.utb_claim cl ON (ca.LynxID = cl.LynxID)
      LEFT JOIN dbo.utb_insurance i ON (cl.InsuranceCompanyID = i.InsuranceCompanyID)
      WHERE cl.DemoFlag = 0
      ORDER BY Convert(varchar(50), tu.NameLast + ', ' + tu.NameFirst), Convert(varchar(20), c.AlarmDate, 101)
      
    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptPendingTasks' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptPendingTasks TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


  