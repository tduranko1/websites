-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadMerge' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopLoadMerge 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopLoadMerge
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Updates a reinspect detail record.
*
*
* RESULT SET:       
*       rowcount
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspSMTShopLoadMerge
    @ShopLoadID             udt_std_id_big,
    @ShopID                 udt_std_id_big=null,
    @ShopLocationID         udt_std_id_big=null,
    @UserID                 udt_std_id,
    @ApplicationCD          udt_std_cd='APD'
    
AS
BEGIN
    
    IF LEN(LTRIM(RTRIM(@ShopID))) = 0 SET @ShopID = NULL
    IF LEN(LTRIM(RTRIM(@ShopLocationID))) = 0 SET @ShopLocationID = NULL
    
    
    -- Declare internal variables
    DECLARE @error                              udt_std_int,
            @rowcount                           udt_std_int,
            @now                                udt_std_datetime,
            @ProcName                           varchar(30),       -- Used for raise error stmts 
            @Address1                           udt_addr_line_1,
            @Address2                           udt_addr_line_2,
            @AddressCity                        udt_addr_city,
            @AddressCounty                      udt_addr_county, 
            @AddressState                       udt_addr_state,
            @AddressZip                         udt_addr_zip_code,
            @AutoverseId                        udt_std_desc_mid,
            @BillingID                          udt_std_id_big,
            @BusinessTypeCD                     udt_std_cd, 
            @CellAreaCode                       udt_ph_area_code,
            @CellExchangeNumber                 udt_ph_exchange_number,
            @CellExtensionNumber                udt_ph_extension_number,
            @CellUnitNumber                     udt_ph_unit_number,
            @CertifiedFirstFlag                 udt_std_flag,
            @CertifiedFirstId                   udt_std_desc_short,
            @DataReviewDate                     varchar(30),
            @DataReviewStatusCD                 udt_std_cd,
            @DataReviewUserID                   udt_std_id,
            @DrivingDirections                  udt_std_desc_long,
            @EFTContractSignedFlag              udt_std_flag,
            @EmailAddress                       udt_web_email,
            @EnabledFlag                        udt_std_flag,
            @FaxAreaCode                        udt_ph_area_code, 
            @FaxExchangeNumber                  udt_ph_exchange_number, 
            @FaxExtensionNumber                 udt_ph_extension_number, 
            @FaxUnitNumber                      udt_ph_unit_number,
            @FedTaxID                           udt_fed_tax_id, 
            @GenderCD                           udt_per_gender_cd,
            @Latitude                           udt_addr_latitude,
            @Longitude                          udt_addr_longitude,
            @MaxWeeklyAssignments               udt_std_int_tiny,
            @MergeDate                          udt_std_datetime,
            @MergeUserID                        udt_std_id,
            @MinorityFlag                       udt_std_flag,
            @Name                               udt_std_name, 
            @PagerAreaCode                      udt_ph_area_code,
            @PagerExchangeNumber                udt_ph_exchange_number,
            @PagerExtensionNumber               udt_ph_extension_number,
            @PagerUnitNumber                    udt_ph_unit_number,
            @PersonnelCount                     udt_std_int_tiny,
            @PersonnelID                        udt_std_id_big,
            @PersonnelTypeID                    udt_std_int_tiny,
            @PhoneAreaCode                      udt_ph_area_code,
            @PhoneExchangeNumber                udt_ph_exchange_number, 
            @PhoneExtensionNumber               udt_ph_extension_number, 
            @PhoneUnitNumber                    udt_ph_unit_number,
            @PPGCTSCustomerFlag                 udt_std_flag,
            @PPGCTSFlag                         udt_std_flag,
            @PPGCTSId                           udt_std_desc_short,
            @PPGCTSLevelCD                      udt_std_cd,
            @PPGRegion                          udt_std_cd,    
            @PPGTerritory                       udt_std_cd,
            @PPGZone                            udt_std_cd,      
            @PreferredCommunicationAddress      udt_web_address,
            @PreferredCommunicationMethodID     udt_std_int_tiny,
            @PreferredContactMethodID           udt_std_int_tiny,
            @PreferredEstimatePackageID         udt_std_int_tiny,
            @PrimaryContactID                   udt_std_int_big,
            @ProgramFlag                        udt_std_flag,
            @ReferralFlag						bit,			--added referral flag (WJC)
            @ProgramManagerUserID               udt_std_id,
            @ProgramScore                       udt_std_int_tiny,
            @ShopManagerFlag                    udt_std_flag,
            @WebSiteAddress                     udt_web_address,
            @WarrantyPeriodRefinishCD           udt_std_cd,
            @WarrantyPeriodWorkmanshipCD        udt_std_cd,
            @OperatingMondayStartTime           udt_std_time,
            @OperatingMondayEndTime             udt_std_time,    
            @OperatingTuesdayStartTime          udt_std_time,
            @OperatingTuesdayEndTime            udt_std_time,
            @OperatingWednesdayStartTime        udt_std_time,
            @OperatingWednesdayEndTime          udt_std_time,
            @OperatingThursdayStartTime         udt_std_time,
            @OperatingThursdayEndTime           udt_std_time,
            @OperatingFridayStartTime           udt_std_time,
            @OperatingFridayEndTime             udt_std_time,
            @OperatingSaturdayStartTime         udt_std_time,
            @OperatingSaturdayEndTime           udt_std_time,
            @OperatingSundayStartTime           udt_std_time,
            @OperatingSundayEndTime             udt_std_time,
            @NewPersonnelID                     udt_std_id_big,
            @NewBillingID                       udt_std_id_big,
            @BusinessID                         udt_std_id
            
            
    DECLARE @KeyDescription                     udt_std_name,
            @LogComment                         udt_std_desc_long,
            @oldValue                           udt_std_desc_long,
            @newValue                           udt_std_desc_long,
            @dbBusinessTypeCD                   udt_std_cd,
            @dbFedTaxID                         varchar(15),
            @dbName                             udt_std_name,
            @AvailableForSelectionFlag          udt_std_flag
     
            
    DECLARE @AlignmentFourWheel                 udt_std_money,
            @AlignmentTwoWheel                  udt_std_money,
            @CaulkingSeamSealer                 udt_std_money,
            @ChipGuard                          udt_std_money,
            @CoolantGreen                       udt_std_money,
            @CoolantRed                         udt_std_money,
            @CorrosionProtection                udt_std_money,
            @CountyTaxPct                       udt_std_pct,
            @CoverCar                           udt_std_money,
            @DailyStorage                       udt_std_money,
            @DiscountPctSideBackGlass           udt_std_pct,
            @DiscountPctWindshield              udt_std_pct,
            @FlexAdditive                       udt_std_money,
            @GlassOutsourceServiceFee           udt_std_money,
            @GlassReplacementChargeTypeCD       udt_std_cd,
            @GlassSubletServiceFee              udt_std_money,
            @GlassSubletServicePct              udt_std_pct,
            @HazardousWaste                     udt_std_money,
            @HourlyRateMechanical               udt_std_money,
            @HourlyRateRefinishing              udt_std_money,
            @HourlyRateSheetMetal               udt_std_money,
            @HourlyRateUnibodyFrame             udt_std_money,
            @MunicipalTaxPct                    udt_std_pct,
            @OtherTaxPct                        udt_std_pct,
            @PartsRecycledMarkupPct             udt_std_pct,
            @PullSetUp                          udt_std_hours,
            @PullSetUpMeasure                   udt_std_hours,
            @R12EvacuateRecharge                udt_std_money,
            @R12EvacuateRechargeFreon           udt_std_money,
            @R134EvacuateRecharge               udt_std_money,
            @R134EvacuateRechargeFreon          udt_std_money,
            @RefinishSingleStageHourly          udt_std_money,
            @RefinishSingleStageMax             udt_std_money,
            @RefinishThreeStageCCMaxHrs         udt_std_hours,
            @RefinishThreeStageHourly           udt_std_money,
            @RefinishThreeStageMax              udt_std_money,
            @RefinishTwoStageCCMaxHrs           udt_std_hours,
            @RefinishTwoStageHourly             udt_std_money,
            @RefinishTwoStageMax                udt_std_money,
            @SalesTaxPct                        udt_std_pct,
            @StripePaintPerPanel                udt_std_money,
            @StripePaintPerSide                 udt_std_money,
            @StripeTapePerPanel                 udt_std_money,
            @StripeTapePerSide                  udt_std_money,
            @TireMountBalance                   udt_std_money,
            @TowInChargeTypeCD                  udt_std_cd,
            @TowInFlatFee                       udt_std_money,
            @Undercoat                          udt_std_money

       
    
    SET @ProcName = 'uspSMTShopLoadMerge'
    SET @now = CURRENT_TIMESTAMP
    SET @CertifiedFirstFlag = 1
    SET @AvailableForSelectionFlag = 1

    -- Set Database options
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid User shop load id was received
    IF (NOT EXISTS(SELECT ShopLoadID FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID))
    BEGIN
        RAISERROR('101|%s|@ShopLoadID|%u', 16, 1, @ProcName, @ShopLoadID)
        RETURN
    END
    
    
    -- Check to make sure a valid User id was received
    IF  (dbo.ufnUtilityIsUserActive(@UserID, 'APD', NULL) = 0)
    BEGIN
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    SET @FedTaxID = (SELECT FedTaxID FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID)  
    
    
    -- Check for duplicate FedTaxID  
      IF EXISTS(SELECT TOP 1 FedTaxID 
                FROM dbo.utb_shop 
                WHERE EnabledFlag = 1
                  AND FedTaxID IS NOT NULL
                  AND LEN(LTRIM(RTRIM(FedTaxID))) > 0
                  AND FedTaxID = @FedTaxID
                  AND ShopID <> @ShopID)
      BEGIN
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
            RAISERROR('99|%s Duplicate Fed Tax ID', 16, 1, @ProcName)
            RETURN
        END  
      END
                   
    
    -- Begin Update
    BEGIN TRANSACTION ShopLoadMergeTran1
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    CREATE TABLE #tblTmpBilling   (ShopBillingID      bigint)
    CREATE TABLE #tblTmpBusiness  (BusinessID         bigint)
    CREATE TABLE #tblTmpShop      (ShopID             bigint)
    
    -- To catch IDs returned from called procs that we don't want to be returned from this proc.
    CREATE TABLE #tblTmpBitBucket       (ID                 bigint)
    

    IF (@ShopID IS NOT NULL)
    /***********************************************************************************************************************/
     -- MERGE Data  -- There is an existing Business.  There may or may not be an existing shop.
    /***********************************************************************************************************************/
    BEGIN
    
      SELECT @BusinessTypeCD      = l.BusinessTypeCD,
             @FedTaxID            = l.FedTaxID,
             @EnabledFlag         = s.EnabledFlag,
             @dbBusinessTypeCD    = s.BusinessTypeCD,
             @dbFedTaxID          = s.FedTaxID,
             @dbName              = s.Name
      FROM dbo.utb_shop_load l,
           dbo.utb_shop s 
      WHERE ShopLoadID = @ShopLoadID
        AND s.ShopID = @ShopID
        
      
      -- merge business info
      UPDATE dbo.utb_shop
      SET   BusinessTypeCD                           = ISNULL(@BusinessTypeCD, @dbBusinessTypeCD),
            EnabledFlag                              = 1,
            FedTaxID                                 = ISNULL(@FedTaxID, @dbFedTaxID),
            SysLastUserID                            = @UserID,
            SysLastUpdatedDate                       = @now
      FROM dbo.utb_shop_load l,
           dbo.utb_shop s 
      WHERE ShopLoadID = @ShopLoadID
        AND s.ShopID = @ShopID
      
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop', 16, 1, @ProcName)
        RETURN
      END 
      
      UPDATE dbo.utb_shop_load
      SET MergeActionShopCD = 'U'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
      
      
      -- Begin Audit Log code  ----------------------------------------------------------------------------------
      
      SET @KeyDescription = 'Business-' + @dbName
      
      SET @oldValue = UPPER(LTRIM(RTRIM(ISNULL(@dbFedTaxId, ''))))
      SET @newValue = UPPER(LTRIM(RTRIM(ISNULL(@FedTaxId, ''))))
    
      IF @oldValue <> @newValue
      BEGIN
        SET @LogComment = 'SSN\EIN changed from ' + @oldValue + ' to ' + @newValue
    
        EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @UserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
          
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
      
      
      IF @dbBusinessTypeCD <> @BusinessTypeCD
      BEGIN
        SET @oldValue = ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop', 'BusinessTypeCD') 
                                     WHERE Code = @dbBusinessTypeCD), '')
        SET @newValue = ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop', 'BusinessTypeCD') 
                                     WHERE Code = @BusinessTypeCD), '')
    
        SET @LogComment = 'Business Type changed from ' + @oldValue + ' to ' + @newValue
    
        EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @UserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
          
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
      
      
      IF @EnabledFlag = 0
      BEGIN
        SET @LogComment = 'Deleted Business ''undeleted'' by Merge process'
    
        EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @UserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
          
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
      
      -- End of Audit Log code  -------------------------------------------------------------------------------------
            
    END  
    ELSE
    BEGIN
    /*******************************************************************************************************************/
     -- There is no existing Business, create new business record
    /*******************************************************************************************************************/
        
      SELECT @BusinessTypeCD          = BusinessTypeCD, 
             @Address1                = Address1, 
             @Address2                = Address2, 
             @AddressCity             = AddressCity, 
             @AddressCounty           = AddressCounty, 
             @AddressState            = AddressState, 
             @AddressZip              = AddressZip,
             @EnabledFlag             = 1, 
             @FaxAreaCode             = FaxAreaCode, 
             @FaxExchangeNumber       = FaxExchangeNumber,
             @FaxExtensionNumber      = FaxExtensionNumber,
             @FaxUnitNumber           = FaxUnitNumber,
             @FedTaxID                = FedTaxID,
             @Name                    = Name,
             @PhoneAreaCode           = PhoneAreaCode,
             @PhoneExchangeNumber     = PhoneExchangeNumber,
             @PhoneExtensionNumber    = PhoneExtensionNumber,
             @PhoneUnitNumber         = PhoneUnitNumber
      FROM dbo.utb_shop_load
      WHERE ShopLoadID = @ShopLoadID                       
      
      
      INSERT #tblTmpBusiness
      EXEC dbo.uspSMTBusinessInfoInsDetail NULL, @BusinessTypeCD, @Address1, @Address2, @AddressCity, @AddressCounty, 
                                           @AddressState, @AddressZip, @EnabledFlag, @FaxAreaCode, @FaxExchangeNumber, 
                                           @FaxExtensionNumber, @FaxUnitNumber, @FedTaxID, @Name, @PhoneAreaCode, 
                                           @PhoneExchangeNumber, @PhoneExtensionNumber, @PhoneUnitNumber, @UserID
                                           
         
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
      
      SELECT @ShopID = BusinessID FROM #tblTmpBusiness
      
      
      IF @ShopID is null
      BEGIN
        RAISERROR('101|%s|@ShopID|%s', 16, 1, @ProcName, @ShopID)
        ROLLBACK TRANSACTION
        RETURN
      END
                        
            
      UPDATE dbo.utb_shop_load
      SET MergeActionShopCD = 'I'
      WHERE ShopLoadID = @ShopLoadID
    
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
    END  
           
    
    
    IF (@ShopLocationID IS NOT NULL)
    /***********************************************************************************************************************/
     -- MERGE Data -- There is an existing Shop
    /***********************************************************************************************************************/
    BEGIN
    
      -- merge shop location  
      --UPDATE dbo.utb_shop_location
      SELECT @Address1                                 = ISNULL(l.Address1, s.Address1),
             @Address2                                 = ISNULL(l.Address2, s.Address2),
             @AddressCity                              = ISNULL(l.AddressCity, s.AddressCity),
             @AddressCounty                            = ISNULL(l.AddressCounty, s.AddressCounty),
             @AddressState                             = ISNULL(l.AddressState, s.AddressState),
             @AddressZip                               = ISNULL(l.AddressZip, s.AddressZip),
             @AutoverseId                              = ISNULL(s.AutoverseId, ''),
             @BillingID                                = s.BillingID,
             @BusinessID                               = s.ShopID,
             @CertifiedFirstFlag                       = ISNULL(l.CertifiedFirstFlag, s.CertifiedFirstFlag),
             @CertifiedFirstID                         = ISNULL(l.CertifiedFirstID, s.CertifiedFirstID),
             @DataReviewDate                           = @now,
             @DataReviewStatusCD                       = CASE
                                                           WHEN s.ProgramFlag = 1 THEN 'A'
                                                           ELSE 'I'
                                                         END,
             @DataReviewUserID                         = @UserID,
             @DrivingDirections                        = ISNULL(l.DrivingDirections, s.DrivingDirections),
             @EmailAddress                             = ISNULL(l.EmailAddress, s.EmailAddress),
             @EnabledFlag                              = 1,
             @FaxAreaCode                              = ISNULL(l.FaxAreaCode, s.FaxAreaCode),
             @FaxExchangeNumber                        = ISNULL(l.FaxExchangeNumber, s.FaxExchangeNumber),
             @FaxExtensionNumber                       = ISNULL(l.FaxExtensionNumber, s.FaxExtensionNumber),
             @FaxUnitNumber                            = ISNULL(l.FaxUnitNumber, s.FaxUnitNumber),
             @Latitude                                 = s.Latitude,
             @Longitude                                = s.Longitude,
             @MaxWeeklyAssignments                     = ISNULL(l.MaxWeeklyAssignments, s.MaxWeeklyAssignments),
             @MergeDate                                = @now,
             @MergeUserID                              = @UserID,
             @Name                                     = ISNULL(l.Name, s.Name),
             @PhoneAreaCode                            = ISNULL(l.PhoneAreaCode, s.PhoneAreaCode),
             @PhoneExchangeNumber                      = ISNULL(l.PhoneExchangeNumber, s.PhoneExchangeNumber),
             @PhoneExtensionNumber                     = ISNULL(l.PhoneExtensionNumber, s.PhoneExtensionNumber),
             @PhoneUnitNumber                          = ISNULL(l.PhoneUnitNumber, s.PhoneUnitNumber),
             @PPGCTSCustomerFlag                       = s.PPGCTSCustomerFlag,
             @PPGCTSFlag                               = s.PPGCTSFlag,
             @PPGCTSId                                 = s.PPGCTSId,
             @PPGCTSLevelCD                            = s.PPGCTSLevelCD,
             @PPGRegion                                = ISNULL(l.PPGRegion, s.PPGRegion),
             @PPGTerritory                             = ISNULL(l.PPGTerritory, s.PPGTerritory),
             @PPGZone                                  = ISNULL(l.PPGZone, s.PPGZone),
             @PreferredCommunicationAddress            = s.PreferredCommunicationAddress,
             @PreferredCommunicationMethodID           = ISNULL(l.PreferredCommunicationMethodID, s.PreferredCommunicationMethodID),
             @PreferredEstimatePackageID               = ISNULL(l.PreferredEstimatePackageID, s.PreferredEstimatePackageID),
             @ProgramFlag                              = l.SLProgramFlag,		--s.ProgramFlag,
             @ReferralFlag							   = l.SLReferralFlag,		--add referral flag (WJC)
             @ProgramScore                             = s.ProgramScore,
             @ProgramManagerUserID                     = s.ProgramManagerUserID,
             @WarrantyPeriodRefinishCD                 = ISNULL(l.WarrantyPeriodRefinishCD, s.WarrantyPeriodRefinishCD),
             @WarrantyPeriodWorkmanshipCD              = ISNULL(l.WarrantyPeriodWorkmanshipCD, s.WarrantyPeriodWorkmanshipCD),
             @WebSiteAddress                           = ISNULL(l.WebSiteAddress, s.WebSiteAddress)
      FROM dbo.utb_shop_load l,
           dbo.utb_shop_location s
      WHERE ShopLoadID = @ShopLoadID
        AND s.ShopLocationID = @ShopLocationID
      
      
      
      SELECT @OperatingMondayStartTime                 = ISNULL(l.OperatingMondayStartTime, h.OperatingMondayStartTime),
             @OperatingMondayEndTime                   = ISNULL(l.OperatingMondayEndTime, h.OperatingMondayEndTime),
             @OperatingTuesdayStartTime                = ISNULL(l.OperatingTuesdayStartTime, h.OperatingTuesdayStartTime),
             @OperatingTuesdayEndTime                  = ISNULL(l.OperatingTuesdayEndTime, h.OperatingTuesdayEndTime),
             @OperatingWednesdayStartTime              = ISNULL(l.OperatingWednesdayStartTime, h.OperatingWednesdayStartTime),
             @OperatingWednesdayEndTime                = ISNULL(l.OperatingWednesdayEndTime, h.OperatingWednesdayEndTime),
             @OperatingThursdayStartTime               = ISNULL(l.OperatingThursdayStartTime, h.OperatingThursdayStartTime),
             @OperatingThursdayEndTime                 = ISNULL(l.OperatingThursdayEndTime, h.OperatingThursdayEndTime),
             @OperatingFridayStartTime                 = ISNULL(l.OperatingFridayStartTime, h.OperatingFridayStartTime),
             @OperatingFridayEndTime                   = ISNULL(l.OperatingFridayEndTime, h.OperatingFridayEndTime),
             @OperatingSaturdayStartTime               = ISNULL(l.OperatingSaturdayStartTime, h.OperatingSaturdayStartTime),
             @OperatingSaturdayEndTime                 = ISNULL(l.OperatingSaturdayEndTime, h.OperatingSaturdayEndTime),
             @OperatingSundayStartTime                 = ISNULL(l.OperatingSundayStartTime, h.OperatingSundayStartTime),
             @OperatingSundayEndTime                   = ISNULL(l.OperatingSundayEndTime, h.OperatingSundayEndTime)
      FROM dbo.utb_shop_load l,
           dbo.utb_shop_location_hours h
      WHERE ShopLoadID = @ShopLoadID
        AND h.ShopLocationID = @ShopLocationID
      
           
      INSERT INTO #tblTmpBitBucket
      EXEC uspSMTShopInfoUpdDetail @ShopLocationID, @BillingID, @DataReviewUserID, @PreferredCommunicationMethodID, 
                                   @PreferredEstimatePackageID, @ProgramManagerUserID, @BusinessID, @Address1, @Address2,
                                   @AddressCity, @AddressCounty, @AddressState, @AddressZip, @AutoverseId, @AvailableForSelectionFlag, 
                                   @CertifiedFirstFlag, @CertifiedFirstId, @DataReviewDate, @DataReviewStatusCD, NULL, @DrivingDirections, 
                                   @EmailAddress, @EnabledFlag, @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, @FaxUnitNumber, 
                                   @Latitude, @Longitude, @MaxWeeklyAssignments, @Name, @PhoneAreaCode, @PhoneExchangeNumber, 
                                   @PhoneExtensionNumber, @PhoneUnitNumber, @PPGCTSCustomerFlag, @PPGCTSFlag, @PPGCTSId, 
                                   @PPGCTSLevelCD, @PreferredCommunicationAddress, @ProgramFlag, @ReferralFlag, --add referral flag (WJC)
                                   @ProgramScore, NULL, NULL, NULL,
                                   @WebSiteAddress, @WarrantyPeriodRefinishCD, @WarrantyPeriodWorkmanshipCD, @UserID, @now,
                                   @OperatingMondayStartTime, @OperatingMondayEndTime, @OperatingTuesdayStartTime,
                                   @OperatingTuesdayEndTime, @OperatingWednesdayStartTime, @OperatingWednesdayEndTime,
                                   @OperatingThursdayStartTime, @OperatingThursdayEndTime, @OperatingFridayStartTime,
                                   @OperatingFridayEndTime, @OperatingSaturdayStartTime, @OperatingSaturdayEndTime,
                                   @OperatingSundayStartTime, @OperatingSundayEndTime, @UserID, @now, 'APD', 1
                                     
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END 
           
      UPDATE dbo.utb_shop_load
      SET MergeActionShopLocationCD = 'U'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
    END
    ELSE -- There is no existing shop
    BEGIN
      /*******************************************************************************************************************/
       -- Create a new shop -- It will be attached to the preexisting Business if there was one.  Otherwise it will be
       -- attached to the Business that was just created.
      /*******************************************************************************************************************/
        
      -- Create billing record
      SELECT @Address1                = Address1, 
             @Address2                = Address2, 
             @AddressCity             = AddressCity, 
             @AddressCounty           = AddressCounty, 
             @AddressState            = AddressState, 
             @AddressZip              = AddressZip, 
             @EFTContractSignedFlag   = 0, 
             @FaxAreaCode             = FaxAreaCode, 
             @FaxExchangeNumber       = FaxExchangeNumber, 
             @FaxExtensionNumber      = FaxExtensionNumber, 
             @FaxUnitNumber           = FaxUnitNumber, 
             @Name                    = Name, 
             @PhoneAreaCode           = PhoneAreaCode, 
             @PhoneExchangeNumber     = PhoneExchangeNumber, 
             @PhoneExtensionNumber    = PhoneExtensionNumber,
             @PhoneUnitNumber         = PhoneUnitNumber
      FROM dbo.utb_shop_load
      WHERE ShopLoadID = @ShopLoadID
      
      INSERT #tblTmpBilling
      EXEC dbo.uspSMTShopBillingInsDetail @Address1, @Address2, @AddressCity, @AddressCounty, @AddressState, @AddressZip,
                                          NULL, NULL, @EFTContractSignedFlag, NULL, NULL, @FaxAreaCode, @FaxExchangeNumber,
                                          @FaxExtensionNumber, @FaxUnitNumber, @Name, @PhoneAreaCode, @PhoneExchangeNumber,
                                          @PhoneExtensionNumber, @PhoneUnitNumber, @UserID
      
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
     
      SET @NewBillingID = (SELECT ShopBillingID FROM #tblTmpBilling)
      
      
      IF @NewBillingID is null
      BEGIN
          RAISERROR('101|%s|@NewBillingID|%s', 16, 1, @ProcName, @NewBillingID)
          ROLLBACK TRANSACTION
          RETURN
      END
      
           
      UPDATE dbo.utb_shop_load
      SET MergeActionBillingCD = 'I'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
      
      SELECT @Address1                                 = Address1,
             @Address2                                 = Address2,
             @AddressCity                              = AddressCity,
             @AddressCounty                            = AddressCounty,
             @AddressState                             = AddressState,
             @AddressZip                               = AddressZip,
             @CertifiedFirstFlag                       = CertifiedFirstFlag,
             @CertifiedFirstID                         = CertifiedFirstID,
             @DataReviewDate                           = @now,
             @DataReviewStatusCD                       = 'I',
             @DataReviewUserID                         = @UserID,
             @DrivingDirections                        = DrivingDirections,
             @EmailAddress                             = EmailAddress,
             @EnabledFlag                              = 1,
             @FaxAreaCode                              = FaxAreaCode,
             @FaxExchangeNumber                        = FaxExchangeNumber,
             @FaxExtensionNumber                       = FaxExtensionNumber,
             @FaxUnitNumber                            = FaxUnitNumber,
             @Latitude                                 = NULL,
             @Longitude                                = NULL,
             @MaxWeeklyAssignments                     = MaxWeeklyAssignments,
             @MergeDate                                = @now,
             @MergeUserID                              = @UserID,
             @Name                                     = Name,
             @PhoneAreaCode                            = PhoneAreaCode,
             @PhoneExchangeNumber                      = PhoneExchangeNumber,
             @PhoneExtensionNumber                     = PhoneExtensionNumber,
             @PhoneUnitNumber                          = PhoneUnitNumber,
             @PPGCTSCustomerFlag                       = 0,
             @PPGCTSFlag                               = 0,
             @PPGCTSId                                 = NULL,
             @PPGCTSLevelCD                            = NULL,
             @PreferredCommunicationAddress            = NULL,
             @PreferredCommunicationMethodID           = PreferredCommunicationMethodID,
             @PreferredEstimatePackageID               = PreferredEstimatePackageID,
             @ProgramFlag                              = SLProgramFlag,		--0,
             @ReferralFlag							   = SLReferralFlag,	--add referral flag (WJC)
             @ProgramScore                             = NULL,
             @ProgramManagerUserID                     = NULL,
             @WarrantyPeriodRefinishCD                 = WarrantyPeriodRefinishCD,
             @WarrantyPeriodWorkmanshipCD              = WarrantyPeriodWorkmanshipCD,
             @WebSiteAddress                           = WebSiteAddress
      FROM dbo.utb_shop_load
      WHERE ShopLoadID = @ShopLoadID
      
      
           
      INSERT #tblTmpShop     
      EXEC dbo.uspSMTShopInfoInsDetail @NewBillingID, @DataReviewUserID, @PreferredCommunicationMethodID, 
                                       @PreferredEstimatePackageID, @ProgramManagerUserID, @ShopID, @Address1, @Address2, 
                                       @AddressCity, @AddressCounty, @AddressState, @AddressZip, @AutoverseId, @AvailableForSelectionFlag, 
                                       @CertifiedFirstFlag, @CertifiedFirstID, @DataReviewDate, @DataReviewStatusCD, NULL, @DrivingDirections, 
                                       @EmailAddress, @EnabledFlag, @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, 
                                       @FaxUnitNumber, @Latitude, @Longitude, @MaxWeeklyAssignments, @MergeDate, @MergeUserID, 
                                       @Name, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneExtensionNumber, @PhoneUnitNumber, 
                                       @PPGCTSCustomerFlag, @PPGCTSFlag, @PPGCTSId, @PPGCTSLevelCD, 
                                       @PreferredCommunicationAddress, @ProgramFlag, @ReferralFlag,	--add referral flag (WJC)
                                       @ProgramScore, NULL, NULL, NULL, @WebSiteAddress, 
                                       @WarrantyPeriodRefinishCD, @WarrantyPeriodWorkmanshipCD, @UserID
      
      
                      
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
            
      
      SET @ShopLocationID = (SELECT ShopID FROM #tblTmpShop)
      
           
      IF @ShopLocationID is null
      BEGIN
          RAISERROR('101|%s|@ShopLocationID|%s', 16, 1, @ProcName, @ShopLocationID)
          ROLLBACK TRANSACTION
          RETURN
      END
      
            
      UPDATE dbo.utb_shop_load
      SET MergeActionShopLocationCD = 'I'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
    END         
       
    
    /********************************************************************************************************************/
     -- Merge or create additional records related to businesses and shops as appropriate.
    /********************************************************************************************************************/
       
    -- merge/insert shop personnel  - determine if Person exists as a shop_location_personnel
    IF (EXISTS(SELECT p.PersonnelID 
               FROM dbo.utb_personnel p INNER JOIN dbo.utb_shop_load l ON p.PersonnelID = l.SPPersonnelID
                                        INNER JOIN dbo.utb_shop_location_personnel slp ON p.PersonnelID = slp.PersonnelID
                                                                                      AND slp.ShopLocationID = @ShopLocationID
               WHERE ShopLoadID = @ShopLoadID))
         
    BEGIN
      SELECT  @PersonnelID                              = p.PersonnelID,
              @CellAreaCode                             = ISNULL(l.SPCellAreaCode, p.CellAreaCode),
              @CellExchangeNumber                       = ISNULL(l.SPCellExchangeNumber, p.CellExchangeNumber),
              @CellExtensionNumber                      = ISNULL(l.SPCellExtensionNumber, p.CellExtensionNumber),
              @CellUnitNumber                           = ISNULL(l.SPCellUnitNumber, p.CellUnitNumber),
              @EmailAddress                             = ISNULL(l.SPEmailAddress, p.EmailAddress),
              @FaxAreaCode                              = ISNULL(l.SPFaxAreaCode, p.FaxAreaCode),
              @FaxExchangeNumber                        = ISNULL(l.SPFaxExchangeNumber, p.FaxExchangeNumber),
              @FaxExtensionNumber                       = ISNULL(l.SPFaxExtensionNumber, p.FaxExtensionNumber),
              @FaxUnitNumber                            = ISNULL(l.SPFaxUnitNumber, p.FaxUnitNumber),
              @GenderCD                                 = ISNULL(l.SPGenderCD, p.GenderCD),
              @Name                                     = ISNULL(l.SPName, p.Name),
              @PagerAreaCode                            = ISNULL(l.SPPagerAreaCode, p.PagerAreaCode),
              @PagerExchangeNumber                      = ISNULL(l.SPPagerExchangeNumber, p.PagerExchangeNumber),
              @PagerExtensionNumber                     = ISNULL(l.SPPagerExtensionNumber, p.PagerExtensionNumber),
              @PagerUnitNumber                          = ISNULL(l.SPPagerUnitNumber, p.PagerUnitNumber),
              @PersonnelTypeID                          = ISNULL(l.SPPersonnelTypeID, p.PersonnelTypeID),
              @PhoneAreaCode                            = ISNULL(l.SPPhoneAreaCode, p.PhoneAreaCode),
              @PhoneExchangeNumber                      = ISNULL(l.SPPhoneExchangeNumber, p.PhoneExchangeNumber),
              @PhoneExtensionNumber                     = ISNULL(l.SPPhoneExtensionNumber, p.PhoneExtensionNumber),
              @PhoneUnitNumber                          = ISNULL(l.SPPhoneUnitNumber, p.PhoneUnitNumber),
              @PreferredContactMethodID                 = ISNULL(l.SPPreferredContactMethodID, p.PreferredContactMethodID),
              @ShopManagerFlag                          = ISNULL(l.SPShopManagerFlag, p.ShopManagerFlag)
      FROM dbo.utb_shop_load l INNER JOIN dbo.utb_personnel p ON l.SPPersonnelID = p.PersonnelID
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('99|%s|', 16, 1, @ProcName)
        RETURN
      END  
      
            
      EXEC dbo.uspSMTPersonnelUpdDetail @PersonnelID, @PersonnelTypeID, @PreferredContactMethodID, @CellAreaCode,
                                        @CellExchangeNumber, @CellExtensionNumber, @CellUnitNumber, @EmailAddress,
                                        @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, @FaxUnitNumber, @GenderCD,
                                        @MinorityFlag, @Name, @PagerAreaCode, @PagerExchangeNumber, @PagerExtensionNumber,
                                        @PagerUnitNumber, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneExtensionNumber,
                                        @PhoneUnitNumber, @ShopManagerFlag, @UserID, @now, 'APD', 1
                                        
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END

                 
      UPDATE dbo.utb_shop_load
      SET MergeActionShopPersonnelCD = 'U'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END   
    END
    ELSE
    BEGIN
      SET @PersonnelCount = (SELECT COUNT(*) FROM dbo.utb_shop_location_personnel WHERE ShopLocationID = @ShopLocationID)
       
      SET @PrimaryContactID = (SELECT p.PersonnelID 
                               FROM dbo.utb_personnel p 
                                    INNER JOIN dbo.utb_shop_location_personnel slp ON p.PersonnelID = slp.PersonnelID
                                    INNER JOIN dbo.utb_personnel_type t ON p.PersonnelTypeID = t.PersonnelTypeID
                               WHERE slp.ShopLocationID = @ShopLocationID
                                 AND t.Name = 'Shop Manager')
                           
      
      IF (@PersonnelCount < 4) -- this is just in case someone used SMT to create the max number of personnel for a shop
      BEGIN                  -- between now and the time shop_load was loaded.
        IF @PrimaryContactID IS NULL
          SET @PersonnelTypeID = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Shop Manager')
        ELSE
          SET @PersonnelTypeID = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Estimator')
          
        SELECT  @CellAreaCode                             = SPCellAreaCode,
                @CellExchangeNumber                       = SPCellExchangeNumber,
                @CellExtensionNumber                      = SPCellExtensionNumber,
                @CellUnitNumber                           = SPCellUnitNumber,
                @EmailAddress                             = SPEmailAddress,
                @FaxAreaCode                              = SPFaxAreaCode,
                @FaxExchangeNumber                        = SPFaxExchangeNumber,
                @FaxExtensionNumber                       = SPFaxExtensionNumber,
                @FaxUnitNumber                            = SPFaxUnitNumber,
                @GenderCD                                 = SPGenderCD,
                @Name                                     = SPName,
                @PagerAreaCode                            = SPPagerAreaCode,
                @PagerExchangeNumber                      = SPPagerExchangeNumber,
                @PagerExtensionNumber                     = SPPagerExtensionNumber,
                @PagerUnitNumber                          = SPPagerUnitNumber,
                @PhoneAreaCode                            = SPPhoneAreaCode,
                @PhoneExchangeNumber                      = SPPhoneExchangeNumber,
                @PhoneExtensionNumber                     = SPPhoneExtensionNumber,
                @PhoneUnitNumber                          = SPPhoneUnitNumber,
                @PreferredContactMethodID                 = SPPreferredContactMethodID,
                @ShopManagerFlag                          = SPShopManagerFlag
        FROM dbo.utb_shop_load l
        WHERE ShopLoadID = @ShopLoadID
        
        IF (@@ERROR <> 0)
        BEGIN
          -- Update failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RAISERROR('99|%s|', 16, 1, @ProcName)
          RETURN
        END 
        
        --select * from utb_shop_location where ShopLocationID = @ShopLocationID
    --rollback tran
    --return
           
        EXEC dbo.uspSMTPersonnelInsDetail @ShopLocationID, 'S', @PersonnelTypeID, @PreferredContactMethodID, @CellAreaCode, 
                                          @CellExchangeNumber, @CellExtensionNumber, @CellUnitNumber, @EmailAddress,
                                          @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, @FaxUnitNumber, @GenderCD, 
                                          @MinorityFlag, @Name, @PagerAreaCode, @PagerExchangeNumber, @PagerExtensionNumber, 
                                          @PagerUnitNumber, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneExtensionNumber,
                                          @PhoneUnitNumber, @ShopManagerFlag, @UserID
                                               
        IF (@@ERROR <> 0)
        BEGIN
          -- Insertion failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RETURN
        END
             
        
        UPDATE dbo.utb_shop_load
        SET MergeActionShopPersonnelCD = 'I'
        WHERE ShopLoadID = @ShopLoadID
      
        IF (@@ERROR <> 0)
        BEGIN
          -- Update failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
          RETURN
        END 
      END      
    END
    
    
    -- merge/insert shop location personnel  -- see if Person exists as a shop_location_personnel
    IF (EXISTS(SELECT p.PersonnelID 
               FROM dbo.utb_personnel p INNER JOIN dbo.utb_shop_load l ON p.PersonnelID = l.SLPPersonnelID
                                        INNER JOIN dbo.utb_shop_location_personnel slp ON p.PersonnelID = slp.PersonnelID
                                                                                      AND slp.ShopLocationID = @ShopLocationID
               WHERE ShopLoadID = @ShopLoadID))
    BEGIN
      SELECT @PersonnelID                              = p.PersonnelID,
             @CellAreaCode                             = ISNULL(l.SLPCellAreaCode, p.CellAreaCode),
             @CellExchangeNumber                       = ISNULL(l.SLPCellExchangeNumber, p.CellExchangeNumber),
             @CellExtensionNumber                      = ISNULL(l.SLPCellExtensionNumber, p.CellExtensionNumber),
             @CellUnitNumber                           = ISNULL(l.SLPCellUnitNumber, p.CellUnitNumber),
             @EmailAddress                             = ISNULL(l.SLPEmailAddress, p.EmailAddress),
             @FaxAreaCode                              = ISNULL(l.SLPFaxAreaCode, p.FaxAreaCode),
             @FaxExchangeNumber                        = ISNULL(l.SLPFaxExchangeNumber, p.FaxExchangeNumber),
             @FaxExtensionNumber                       = ISNULL(l.SLPFaxExtensionNumber, p.FaxExtensionNumber),
             @FaxUnitNumber                            = ISNULL(l.SLPFaxUnitNumber, p.FaxUnitNumber),
             @GenderCD                                 = ISNULL(l.SLPGenderCD, p.GenderCD),
             @Name                                     = ISNULL(l.SLPName, p.Name),
             @PagerAreaCode                            = ISNULL(l.SLPPagerAreaCode, p.PagerAreaCode),
             @PagerExchangeNumber                      = ISNULL(l.SLPPagerExchangeNumber, p.PagerExchangeNumber),
             @PagerExtensionNumber                     = ISNULL(l.SLPPagerExtensionNumber, p.PagerExtensionNumber),
             @PagerUnitNumber                          = ISNULL(l.SLPPagerUnitNumber, p.PagerUnitNumber),
             @PersonnelTypeID                          = ISNULL(l.SLPPersonnelTypeID, p.PersonnelTypeID),
             @PhoneAreaCode                            = ISNULL(l.SLPPhoneAreaCode, p.PhoneAreaCode),
             @PhoneExchangeNumber                      = ISNULL(l.SLPPhoneExchangeNumber, p.PhoneExchangeNumber),
             @PhoneExtensionNumber                     = ISNULL(l.SLPPhoneExtensionNumber, p.PhoneExtensionNumber),
             @PhoneUnitNumber                          = ISNULL(l.SLPPhoneUnitNumber, p.PhoneUnitNumber),
             @PreferredContactMethodID                 = ISNULL(l.SLPPreferredContactMethodID, p.PreferredContactMethodID),
             @ShopManagerFlag                          = ISNULL(l.SLPShopManagerFlag, p.ShopManagerFlag)
      FROM dbo.utb_shop_load l INNER JOIN dbo.utb_personnel p ON l.SLPPersonnelID = p.PersonnelID
      WHERE ShopLoadID = @ShopLoadID
    
    
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('99|%s|', 16, 1, @ProcName)
        RETURN
      END 
      
      
      EXEC dbo.uspSMTPersonnelUpdDetail @PersonnelID, @PersonnelTypeID, @PreferredContactMethodID, @CellAreaCode,
                                        @CellExchangeNumber, @CellExtensionNumber, @CellUnitNumber, @EmailAddress,
                                        @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, @FaxUnitNumber, @GenderCD,
                                        @MinorityFlag, @Name, @PagerAreaCode, @PagerExchangeNumber, @PagerExtensionNumber,
                                        @PagerUnitNumber, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneExtensionNumber,
                                        @PhoneUnitNumber, @ShopManagerFlag, @UserID, @now, 'APD', 1
                                        
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
      
      UPDATE dbo.utb_shop_load
      SET MergeActionShopLocationPersonnelCD = 'U'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END    
    END
    ELSE
    BEGIN
      SET @PersonnelCount = (SELECT COUNT(*) FROM dbo.utb_shop_location_personnel WHERE ShopLocationID = @ShopLocationID) 
      SET @PrimaryContactID = (SELECT p.PersonnelID 
                               FROM dbo.utb_personnel p 
                                    INNER JOIN dbo.utb_shop_location_personnel slp ON p.PersonnelID = slp.PersonnelID
                                    INNER JOIN dbo.utb_personnel_type t ON p.PersonnelTypeID = t.PersonnelTypeID
                               WHERE slp.ShopLocationID = @ShopLocationID
                                 AND t.Name = 'Shop Manager')
                                 
      IF (@PersonnelCount < 4) -- this is just in case someone used SMT to create the max number of personnel for a location
      BEGIN                  -- between now and the time shop_load was loaded.
        IF @PrimaryContactID IS NULL
          SET @PersonnelTypeID = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Shop Manager')
        ELSE
          SET @PersonnelTypeID = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Estimator')
        
        
        -- Create personnel record  
        SELECT  @CellAreaCode                             = SLPCellAreaCode,
                @CellExchangeNumber                       = SLPCellExchangeNumber,
                @CellExtensionNumber                      = SLPCellExtensionNumber,
                @CellUnitNumber                           = SLPCellUnitNumber,
                @EmailAddress                             = SLPEmailAddress,
                @FaxAreaCode                              = SLPFaxAreaCode,
                @FaxExchangeNumber                        = SLPFaxExchangeNumber,
                @FaxExtensionNumber                       = SLPFaxExtensionNumber,
                @FaxUnitNumber                            = SLPFaxUnitNumber,
                @GenderCD                                 = SLPGenderCD,
                @Name                                     = SLPName,
                @PagerAreaCode                            = SLPPagerAreaCode,
                @PagerExchangeNumber                      = SLPPagerExchangeNumber,
                @PagerExtensionNumber                     = SLPPagerExtensionNumber,
                @PagerUnitNumber                          = SLPPagerUnitNumber,
                @PhoneAreaCode                            = SLPPhoneAreaCode,
                @PhoneExchangeNumber                      = SLPPhoneExchangeNumber,
                @PhoneExtensionNumber                     = SLPPhoneExtensionNumber,
                @PhoneUnitNumber                          = SLPPhoneUnitNumber,
                @PreferredContactMethodID                 = SLPPreferredContactMethodID,
                @ShopManagerFlag                          = SLPShopManagerFlag
        FROM dbo.utb_shop_load l
        WHERE ShopLoadID = @ShopLoadID
        
        
        IF (@@ERROR <> 0)
        BEGIN
          -- Update failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RAISERROR('99|%s|', 16, 1, @ProcName)
          RETURN
        END 
        
        EXEC dbo.uspSMTPersonnelInsDetail @ShopLocationID, 'S', @PersonnelTypeID, @PreferredContactMethodID, @CellAreaCode, 
                                          @CellExchangeNumber, @CellExtensionNumber, @CellUnitNumber, @EmailAddress,
                                          @FaxAreaCode, @FaxExchangeNumber, @FaxExtensionNumber, @FaxUnitNumber, @GenderCD, 
                                          @MinorityFlag, @Name, @PagerAreaCode, @PagerExchangeNumber, @PagerExtensionNumber, 
                                          @PagerUnitNumber, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneExtensionNumber,
                                          @PhoneUnitNumber, @ShopManagerFlag, @UserID
                                               
        IF (@@ERROR <> 0)
        BEGIN
          -- Insertion failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RETURN
        END
        
        
        UPDATE dbo.utb_shop_load
        SET MergeActionShopLocationPersonnelCD = 'I'
        WHERE ShopLoadID = @ShopLoadID
      
        IF (@@ERROR <> 0)
        BEGIN
          -- Update failure
          ROLLBACK TRANSACTION ShopLoadMergeTran1
          RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
          RETURN
        END 
      END  
    END
    
    
    -- merge/insert shop location pricing
    IF (EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location_pricing WHERE ShopLocationID = @ShopLocationID))
    BEGIN
      SELECT @AlignmentFourWheel                       = ISNULL(l.AlignmentFourWheel, s.AlignmentFourWheel),
             @AlignmentTwoWheel                        = ISNULL(l.AlignmentTwoWheel, s.AlignmentTwoWheel),
             @CaulkingSeamSealer                       = ISNULL(l.CaulkingSeamSealer, s.CaulkingSeamSealer),
             @ChipGuard                                = ISNULL(l.ChipGuard, s.ChipGuard),
             @CoolantGreen                             = ISNULL(l.CoolantGreen, s.CoolantGreen),
             @CoolantRed                               = ISNULL(l.CoolantRed, s.CoolantRed),
             @CorrosionProtection                      = ISNULL(l.CorrosionProtection, s.CorrosionProtection),
             @CountyTaxPct                             = ISNULL(l.CountyTaxPct, s.CountyTaxPct),
             @CoverCar                                 = ISNULL(l.CoverCar, s.CoverCar),
             @DailyStorage                             = ISNULL(l.DailyStorage, s.DailyStorage),
             @DiscountPctSideBackGlass                 = ISNULL(l.DiscountPctSideBackGlass, s.DiscountPctSideBackGlass),
             @DiscountPctWindshield                    = ISNULL(l.DiscountPctWindshield, s.DiscountPctWindshield),
             @FlexAdditive                             = ISNULL(l.FlexAdditive, s.FlexAdditive),
             @GlassOutsourceServiceFee                 = ISNULL(l.GlassOutsourceServiceFee, s.GlassOutsourceServiceFee),
             @GlassReplacementChargeTypeCD             = ISNULL(l.GlassReplacementChargeTypeCD, s.GlassReplacementChargeTypeCD),
             @GlassSubletServiceFee                    = ISNULL(l.GlassSubletServiceFee, s.GlassSubletServiceFee),
             @GlassSubletServicePct                    = ISNULL(l.GlassSubletServicePct, s.GlassSubletServicePct),
             @HazardousWaste                           = ISNULL(l.HazardousWaste, s.HazardousWaste),
             @HourlyRateMechanical                     = ISNULL(l.HourlyRateMechanical, s.HourlyRateMechanical),
             @HourlyRateRefinishing                    = ISNULL(l.HourlyRateRefinishing, s.HourlyRateRefinishing),
             @HourlyRateSheetMetal                     = ISNULL(l.HourlyRateSheetMetal, s.HourlyRateSheetMetal),
             @HourlyRateUnibodyFrame                   = ISNULL(l.HourlyRateUnibodyFrame, s.HourlyRateUnibodyFrame),
             @MunicipalTaxPct                          = ISNULL(l.MunicipalTaxPct, s.MunicipalTaxPct),
             @OtherTaxPct                              = ISNULL(l.OtherTaxPct, s.OtherTaxPct),
             @PartsRecycledMarkupPct                   = ISNULL(l.PartsRecycledMarkupPct, s.PartsRecycledMarkupPct),
             @PullSetUp                                = ISNULL(l.PullSetUp, s.PullSetUp),
             @PullSetUpMeasure                         = ISNULL(l.PullSetUpMeasure, s.PullSetUpMeasure),
             @R12EvacuateRecharge                      = ISNULL(l.R12EvacuateRecharge, s.R12EvacuateRecharge),
             @R12EvacuateRechargeFreon                 = ISNULL(l.R12EvacuateRechargeFreon, s.R12EvacuateRechargeFreon),
             @R134EvacuateRecharge                     = ISNULL(l.R134EvacuateRecharge, s.R134EvacuateRecharge),
             @R134EvacuateRechargeFreon                = ISNULL(l.R134EvacuateRechargeFreon, s.R134EvacuateRechargeFreon),
             @RefinishTwoStageCCMaxHrs                 = ISNULL(l.RefinishTwoStageCCMaxHrs, s.RefinishTwoStageCCMaxHrs),
             @RefinishTwoStageHourly                   = ISNULL(l.RefinishTwoStageHourly, s.RefinishTwoStageHourly),
             @RefinishTwoStageMax                      = ISNULL(l.RefinishTwoStageMax, s.RefinishTwoStageMax),
             @SalesTaxPct                              = ISNULL(l.SalesTaxPct, s.SalesTaxPct),
             @StripePaintPerPanel                      = ISNULL(l.StripePaintPerPanel, s.StripePaintPerPanel),
             @StripePaintPerSide                       = ISNULL(l.StripePaintPerSide, s.StripePaintPerSide),
             @StripeTapePerPanel                       = ISNULL(l.StripeTapePerPanel, s.StripeTapePerPanel),
             @StripeTapePerSide                        = ISNULL(l.StripeTapePerSide, s.StripeTapePerSide),
             @TireMountBalance                         = ISNULL(l.TireMountBalance, s.TireMountBalance),
             @TowInChargeTypeCD                        = ISNULL(l.TowInChargeTypeCD, s.TowInChargeTypeCD),
             @TowInFlatFee                             = ISNULL(l.TowInFlatFee, s.TowInFlatFee),
             @Undercoat                                = ISNULL(l.Undercoat, s.Undercoat)
      FROM dbo.utb_shop_load l,
           dbo.utb_shop_location_pricing s
      WHERE ShopLoadID = @ShopLoadID
        AND s.ShopLocationID = @ShopLocationID
      
      IF (@@ERROR <> 0)
      BEGIN
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('99|%s|', 16, 1, @ProcName)
        RETURN
      END
      
           
      EXEC dbo.uspSMTShopPricingUpdDetail @ShopLocationID, @AlignmentFourWheel, @AlignmentTwoWheel, @CoverCar,
                                          @CaulkingSeamSealer, @ChipGuard, @CoolantGreen, @CoolantRed, @CorrosionProtection,
                                          @DailyStorage, @DiscountPctSideBackGlass, @DiscountPctWindshield, @FlexAdditive, 
                                          @GlassOutsourceServiceFee, @GlassReplacementChargeTypeCD, @GlassSubletServiceFee,
                                          @GlassSubletServicePct, @HazardousWaste, @HourlyRateMechanical, 
                                          @HourlyRateRefinishing, @HourlyRateSheetMetal, @HourlyRateUnibodyFrame,
                                          @PartsRecycledMarkupPct, @PullSetUp, @PullSetUpMeasure, @RefinishSingleStageHourly,
                                          @RefinishSingleStageMax, @RefinishThreeStageHourly, @RefinishThreeStageMax,
                                          @RefinishThreeStageCCMaxHrs, @RefinishTwoStageHourly, @RefinishTwoStageMax,
                                          @RefinishTwoStageCCMaxHrs, @R12EvacuateRecharge, @R12EvacuateRechargeFreon,
                                          @R134EvacuateRecharge, @R134EvacuateRechargeFreon, @StripePaintPerPanel,
                                          @StripePaintPerSide, @StripeTapePerPanel, @StripeTapePerSide, @TireMountBalance,
                                          @TowInChargeTypeCD, @TowInFlatFee, @Undercoat, @SalesTaxPct, @CountyTaxPct,
                                          @MunicipalTaxPct, @OtherTaxPct, @UserID, @now, 'APD', 1

      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
      
      
      UPDATE dbo.utb_shop_load
      SET MergeActionShopLocationPricingCD = 'U'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
    END
    ELSE
    BEGIN
    
      SELECT  @AlignmentFourWheel                                = AlignmentFourWheel,
              @AlignmentTwoWheel                                 = AlignmentTwoWheel,
              @CaulkingSeamSealer                                = CaulkingSeamSealer,
              @ChipGuard                                         = ChipGuard,
              @CoolantGreen                                      = CoolantGreen,
              @CoolantRed                                        = CoolantRed,
              @CorrosionProtection                               = CorrosionProtection,
              @CountyTaxPct                                      = CountyTaxPct,
              @CoverCar                                          = CoverCar,
              @DailyStorage                                      = DailyStorage,
              @DiscountPctSideBackGlass                          = DiscountPctSideBackGlass,
              @DiscountPctWindshield                             = DiscountPctWindshield,
              @FlexAdditive                                      = FlexAdditive,
              @GlassOutsourceServiceFee                          = GlassOutsourceServiceFee,
              @GlassReplacementChargeTypeCD                      = GlassReplacementChargeTypeCD,
              @GlassSubletServiceFee                             = GlassSubletServiceFee,
              @GlassSubletServicePct                             = GlassSubletServicePct,
              @HazardousWaste                                    = HazardousWaste,
              @HourlyRateMechanical                              = HourlyRateMechanical,
              @HourlyRateRefinishing                             = HourlyRateRefinishing,
              @HourlyRateSheetMetal                              = HourlyRateSheetMetal,
              @HourlyRateUnibodyFrame                            = HourlyRateUnibodyFrame,
              @MunicipalTaxPct                                   = MunicipalTaxPct,
              @OtherTaxPct                                       = OtherTaxPct,
              @PartsRecycledMarkupPct                            = PartsRecycledMarkupPct,
              @PullSetUp                                         = PullSetUp,
              @PullSetUpMeasure                                  = PullSetUpMeasure,
              @R12EvacuateRecharge                               = R12EvacuateRecharge,
              @R12EvacuateRechargeFreon                          = R12EvacuateRechargeFreon,
              @R134EvacuateRecharge                              = R134EvacuateRecharge,
              @R134EvacuateRechargeFreon                         = R134EvacuateRechargeFreon,
              @RefinishSingleStageHourly                         = RefinishSingleStageHourly,
              @RefinishSingleStageMax                            = RefinishSingleStageMax,
              @RefinishThreeStageCCMaxHrs                        = RefinishThreeStageCCMaxHrs,
              @RefinishThreeStageHourly                          = RefinishThreeStageHourly,
              @RefinishThreeStageMax                             = RefinishThreeStageMax,
              @RefinishTwoStageCCMaxHrs                          = RefinishTwoStageCCMaxHrs,
              @RefinishTwoStageHourly                            = RefinishTwoStageHourly,
              @RefinishTwoStageMax                               = RefinishTwoStageMax,
              @SalesTaxPct                                       = SalesTaxPct,
              @StripePaintPerPanel                               = StripePaintPerPanel,
              @StripePaintPerSide                                = StripePaintPerSide,
              @StripeTapePerPanel                                = StripeTapePerPanel,
              @StripeTapePerSide                                 = StripeTapePerSide,
              @TireMountBalance                                  = TireMountBalance,
              @TowInChargeTypeCD                                 = TowInChargeTypeCD,
              @TowInFlatFee                                      = TowInFlatFee,
              @Undercoat                                         = Undercoat
      FROM dbo.utb_shop_load
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('99|%s|', 16, 1, @ProcName)
        RETURN
      END
      
      
      EXEC dbo.uspSMTShopPricingInsDetail @ShopLocationID, @AlignmentFourWheel, @AlignmentTwoWheel, @CoverCar, @CaulkingSeamSealer,
                                          @ChipGuard, @CoolantGreen, @CoolantRed, @CorrosionProtection, @DailyStorage,
                                          @DiscountPctSideBackGlass, @DiscountPctWindshield, @FlexAdditive, 
                                          @GlassOutsourceServiceFee, @GlassReplacementChargeTypeCD, @GlassSubletServiceFee,
                                          @GlassSubletServicePct, @HazardousWaste, @HourlyRateMechanical, 
                                          @HourlyRateRefinishing, @HourlyRateSheetMetal, @HourlyRateUnibodyFrame,
                                          @PartsRecycledMarkupPct, @PullSetUp, @PullSetUpMeasure, @RefinishSingleStageHourly,
                                          @RefinishSingleStageMax, @RefinishThreeStageHourly, @RefinishThreeStageMax,
                                          @RefinishThreeStageCCMaxHrs, @RefinishTwoStageHourly, @RefinishTwoStageMax, 
                                          @RefinishTwoStageCCMaxHrs, @R12EvacuateRecharge, @R12EvacuateRechargeFreon,
                                          @R134EvacuateRecharge, @R134EvacuateRechargeFreon, @StripePaintPerPanel,
                                          @StripePaintPerSide, @StripeTapePerPanel, @StripeTapePerSide, @TireMountBalance,
                                          @TowInChargeTypeCD, @TowInFlatFee, @Undercoat, @SalesTaxPct, @CountyTaxPct, 
                                          @MunicipalTaxPct, @OtherTaxPct, @UserID, 'APD', 'MERG'


           
                  
      IF (@@ERROR <> 0)
      BEGIN
        -- Insertion failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RETURN
      END
      
      
      UPDATE dbo.utb_shop_load
      SET MergeActionShopLocationPricingCD = 'I'
      WHERE ShopLoadID = @ShopLoadID
      
      IF (@@ERROR <> 0)
      BEGIN
        -- Update failure
        ROLLBACK TRANSACTION ShopLoadMergeTran1
        RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
      END 
    END
    
       
    UPDATE dbo.utb_shop_load
    SET MergeDate = @now,
        MergeUserID = @UserID
        --EnabledFlag = 0
    WHERE ShopLoadID = @ShopLoadID
    
    IF (@@ERROR <> 0)
    BEGIN
      -- Update failure
      ROLLBACK TRANSACTION ShopLoadMergeTran1
      RAISERROR('104|%s|utb_shop_load', 16, 1, @ProcName)
      RETURN
    END 
    
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT   
    SELECT @error AS 'Error', ISNULL(@ShopID, 0) AS 'ShopID', ISNULL(@ShopLocationID, 0) AS 'ShopLocationID'
        
    
    COMMIT TRANSACTION ShopLoadMergeTran1
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    RETURN @ShopLocationID
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadMerge' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopLoadMerge TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
