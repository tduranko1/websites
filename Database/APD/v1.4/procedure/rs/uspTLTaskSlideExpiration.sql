-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTaskSlideExpiration' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLTaskSlideExpiration 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLTaskSlideExpiration
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Slide the expiration a locked TL task
*
* PARAMETERS:  
* (I) @ChecklistID  Checklist ID
* (I) @UserID       User ID
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLTaskSlideExpiration
    @ChecklistID  udt_std_id_big,
    @UserID       varchar(50)
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    
    SET @ProcName = 'uspTLTaskSlideExpiration'
    SET @now = CURRENT_TIMESTAMP
    
    BEGIN TRANSACTION
    
    UPDATE utb_checklist_lock
    SET ExpirationDate = DATEADD(minute, 10, ExpirationDate)
    WHERE ChecklistID = @ChecklistID
      AND UserID = @UserID
      AND ExpirationDate IS NOT NULL
      
    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTaskSlideExpiration' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLTaskSlideExpiration TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/