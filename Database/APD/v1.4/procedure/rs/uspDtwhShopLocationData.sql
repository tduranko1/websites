-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION

-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopLocationData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhShopLocationData 
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhShopLocationData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Mark A. Homison
* FUNCTION:     Extracts Shop Location data from APD and populates the Data Warehouse.
*
* PARAMETERS:   None
*
* RESULT SET:   None
*
* SWR 201988 09-26-12 MAH - Initial Version.
* SWR 000000 03-13-19 MAH - Added Cincinnati Insurance DRP Shop Program Status Column logic.
*
************************************************************************************************************************/

CREATE PROCEDURE dbo.uspDtwhShopLocationData	
AS
BEGIN
    Declare @debug              AS bit
    DECLARE @error              AS int
    DECLARE @rowcount           AS int
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SELECT @ProcName = 'uspDtwhShopLocationData'

    SELECT @debug = 0
    
    -- Set Database options
    
    SET NOCOUNT ON

    -- Get current timestamp

    PRINT 'Deleting from utb_dtwh_dim_shop_location...'
    
    DELETE FROM dbo.utb_dtwh_dim_shop_location
    
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error deleting from utb_dtwh_dim_shop_location', 16, 1, @ProcName)
        RETURN
    END

	-- -----------------------------------------------------------------------------------------
	-- Insert Shop records where a Shop Location exists and it meets the following requirements:
	-- 1) Shop Location is Enabled and one or more of the following are true
	-- 2) LYNX Select Program Member
	-- 3) CEI Program Member
	-- 4) RRP Program Member
	-- 5) Shop is Targeted for a current Campaign (CertifiedFirstID was repurposed for this - YUCK!)
	-- -----------------------------------------------------------------------------------------
 	
	INSERT INTO dbo.utb_dtwh_dim_shop_location
		   (ShopLocationID,
			ShopID,
			Address1,
			Address2,
			AddressCity,
			AddressCounty,
			AddressState,
			AddressZip,
			AutoVerseId,
			AvailableForSelectionInd,
			CEIProgramInd,
			DirectionsAvailableInd,
			DrpCincinnatiShopPgmStatus,
			DrpElectricShopPgmStatus,
			DrpElephantShopPgmStatus,
			DrpRepublicShopPgmStatus,
			DrpTexasFBShopPgmStatus,
			DrpUticaShopPgmStatus,
			DrpVirginiaFBShopPgmStatus,
			DrpWesternAGShopPgmStatus,
			DrpWestfieldShopPgmStatus,
			EmailAddress,
			FaxNumber,
			FaxExtensionNumber,
			ICARTrainingInd,
			LaborMechanicalRate,
			LaborRefinishRate,
			LaborRefinishMaterials,
			LaborBodyRate,
			LaborFrameRate,
			LastAssignmentDate,
			LastUserID,
			LastUpdatedByName,
			LastUpdatedDate,
			LynxSelectProgramInd,
			MultiShopOperatorInd,
			MultiShopOperatorName,
			OEMPartsDiscountDomestic,
			OEMPartsDiscountForeign,
			OperatingFridayEndTime,
			OperatingFridayStartTime,
			OperatingMondayEndTime,
			OperatingMondayStartTime,
			OperatingSaturdayEndTime,
			OperatingSaturdayStartTime,
			OperatingSundayEndTime,
			OperatingSundayStartTime,
			OperatingThursdayEndTime,
			OperatingThursdayStartTime,
			OperatingTuesdayEndTime,
			OperatingTuesdayStartTime,
			OperatingWednesdayEndTime,
			OperatingWednesdayStartTime,
			PhoneNumber,
			PhoneExtensionNumber,
			PreferredCommunicationMethodID,
			PreferredCommunicationMethod,
			PreferredEstimatePackageID,
			PreferredEstimatePackage,
			PrimaryContactEmailAddr,
			PrimaryContactName,
			PrimaryContactPhoneNumber,
			PrimaryContactPhoneNumberExt,
			RegistrationExpDate,
			RegistrationNumber,
			RepairFacilityType,
			RrpProgramInd,
			RrpQBEShopPgmStatus,
			SecondaryContactEmailAddr,
			SecondaryContactName,
			SecondaryContactPhoneNumber,
			SecondaryContactPhoneNumberExt,
			ServicesHeavyEquipmentInd,
			ServicesPaintlessDentInd,
			ShopLocationName,
			TargetedComment,
			WebSiteAddress)
	SELECT  l.ShopLocationID,
			l.ShopID,
			l.Address1,
			l.Address2,
			l.AddressCity,
			l.AddressCounty,
			l.AddressState,
			l.AddressZip,
			l.AutoVerseId,
			CASE WHEN (l.AvailableForSelectionFlag = 1)
				 THEN 'Y'
				 ELSE 'N'
			END									AS AvailableForSelectionInd,
			CASE WHEN (l.CEIProgramFlag = 1)
				 THEN 'Y'
				 ELSE 'N'
			END		    						AS CEIProgramInd,
			CASE WHEN (LEN(ISNULL(l.DrivingDirections,'')) > 0)
				 THEN 'Y'
				 ELSE 'N'
			END									AS DirectionsAvailableInd,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '194'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '194'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			END,
			CASE WHEN (l.ProgramFlag = 1)
				 THEN 'LYNXSELECT'
				 ELSE 'NON-LYNXSELECT'
			END)								AS DrpCincinnatiShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '158'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '158'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpElectricShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '374'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '374'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpElephantShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '277'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '277'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpRepublicShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '184'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '184'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpTexasFBShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '259'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '259'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpUticaShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '192'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '192'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpVirginiaFBShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '178'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '178'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpWesternAGShopPgmStatus,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '387'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '387'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ProgramFlag = 1)
				  THEN 'LYNXSELECT'
				  ELSE 'NON-LYNXSELECT'
			 END)								AS DrpWestfieldShopPgmStatus,
			l.EmailAddress,
			l.FaxAreaCode + ' ' + 
			l.FaxExchangeNumber + ' ' + 
			l.FaxUnitNumber						AS FaxNumber,
			l.FaxExtensionNumber,
			ISNULL(
			(SELECT CASE WHEN (s.ICARFlag = 1)
						 THEN 'Y'
						 ELSE 'N'
				    END
			   FROM utb_shop_location_survey s
			  WHERE s.ShopLocationID = l.ShopLocationID),'N') AS ICARTrainingInd,
			ISNULL(p.HourlyRateMechanical,0)	AS LaborMechanicalRate,
			ISNULL(p.HourlyRateRefinishing,0)	AS LaborRefinishRate,
			ISNULL(p.RefinishTwoStageHourly,0)	AS LaborRefinishMaterials,
			ISNULL(p.HourlyRateSheetMetal,0)	AS LaborBodyRate,
			ISNULL(p.HourlyRateUnibodyFrame,0)	AS LaborFrameRate,
			DATEADD(d,0, DATEDIFF(d,0,ISNULL(l.LastAssignmentDate,'')))		AS LastAssignmentDate,
			l.SysLastUserID,
			ISNULL(
		    (SELECT CASE WHEN (u.UserID = 0)
						 THEN NameLast
						 ELSE NameLast+', '+NameFirst
				    END
			   FROM utb_user u
			  WHERE u.UserID = l.SysLastUserID),'N/A') AS LastUpdatedByName,
			l.SysLastUpdatedDate,
			CASE WHEN (l.ProgramFlag = 1)
				 THEN 'Y'
				 ELSE 'N'
			END		    						AS LynxSelectProgramInd,
			CASE WHEN (l.Name LIKE '%CARSTAR%')
			     THEN 'Y'
			     ELSE 'N'
			END								AS MultiShopOperatorInd,
			CASE WHEN (l.Name LIKE '%CARSTAR%')
			     THEN 'CARSTAR'
			     ELSE 'NON-MSO'
			END								AS MultiShopOperatorName,
			ISNULL(
			(SELECT  MAX(d.DiscountPct)
			   FROM  utb_shop_location_oem_discount d,
					 utb_vehicle_make               v
			  WHERE  d.ShopLocationID = l.ShopLocationID
			    AND  d.VehicleMakeID  = v.VehicleMakeID
                AND  v.DomesticFlag   = 1),0)	AS OEMPartsDiscountDomestic,
			ISNULL(
			(SELECT  MAX(d.DiscountPct)
			   FROM  utb_shop_location_oem_discount d,
					 utb_vehicle_make               v
			  WHERE  d.ShopLocationID = l.ShopLocationID
			    AND  d.VehicleMakeID  = v.VehicleMakeID
                AND  v.DomesticFlag   = 0),0)	AS OEMPartsDiscountForeign,
			h.OperatingFridayEndTime,
			h.OperatingFridayStartTime,
			h.OperatingMondayEndTime,
			h.OperatingMondayStartTime,
			h.OperatingSaturdayEndTime,
			h.OperatingSaturdayStartTime,
			h.OperatingSundayEndTime,
			h.OperatingSundayStartTime,
			h.OperatingThursdayEndTime,
			h.OperatingThursdayStartTime,
			h.OperatingTuesdayEndTime,
			h.OperatingTuesdayStartTime,
			h.OperatingWednesdayEndTime,
			h.OperatingWednesdayStartTime,
			l.PhoneAreaCode + ' ' + 
			l.PhoneExchangeNumber + ' ' + 
			l.PhoneUnitNumber					AS PhoneNumber,
			l.PhoneExtensionNumber,
			l.PreferredCommunicationMethodID,
			ISNULL(
			(SELECT Name
			   FROM utb_communication_method c
			  WHERE c.CommunicationMethodID = ISNULL(l.PreferredCommunicationMethodID,0)),'NOT APPLICABLE') AS PreferredCommunicationMethod,
			l.PreferredEstimatePackageID,
			ISNULL(
		    (SELECT Name 
			   FROM utb_estimate_package e
			  WHERE e.EstimatePackageID = ISNULL(l.PreferredEstimatePackageID,0)),'NOT APPLICABLE') AS PreferredEstimatePackage,
			NULL, -- l.PrimaryContactEmailAddr,
			NULL, -- l.PrimaryContactName,
			NULL, -- l.PrimaryContactPhoneNumber,
			NULL, -- l.PrimaryContactPhoneNumberExt,
			l.RegistrationExpDate,
			l.RegistrationNumber,
			NULL, -- RepairFacilityType,
			CASE WHEN (l.ReferralFlag = 1)
				 THEN 'Y'
				 ELSE 'N'
			END		    					AS RrpProgramInd,
			ISNULL(
			CASE WHEN (l.AvailableForSelectionFlag = 0)
				 THEN 'NOT AVAILABLE       '
				 ELSE ISNULL(
					 (SELECT CASE WHEN (d.IncludeFlag = 1)
								  THEN 'INCLUDE             '
								  WHEN (d.ExcludeFlag = 1)
								  THEN 'EXCLUDE                   '
								  ELSE NULL
							  END
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID = '304'
						 and  d.ShopLocationID	 = l.ShopLocationID),
					 (SELECT  MAX(CASE WHEN (d.IncludeFlag = 1)
								   THEN 'EXCLUDE-2                 '
								   ELSE NULL
							  END)
						FROM  utb_client_shop_location d
					   where  d.InsuranceCompanyID != '304'
						 and  d.ShopLocationID	    = l.ShopLocationID))
			 END,
			 CASE WHEN (l.ReferralFlag = 1)
				  THEN 'LYNXADVANTAGE'
				  ELSE 'NON-LYNXADVANTAGE'
			 END)								AS RrpQBEShopPgmStatus,
			NULL, -- l.SecondaryContactEmailAddr,
			NULL, -- l.SecondaryContactName,
			NULL, -- l.SecondaryContactPhoneNumber,
			NULL, -- l.SecondaryContactPhoneNumberExt,
			ISNULL(
			(SELECT 'Y'
			   FROM utb_shop_location_specialty ss
			  WHERE ss.ShopLocationID = l.ShopLocationID
				AND ss.SpecialtyID = 6),'N')	AS ServicesHeavyEquipmentInd,
			ISNULL(
			(SELECT 'Y'
			   FROM utb_shop_location_specialty ss
			  WHERE ss.ShopLocationID = l.ShopLocationID
				AND ss.SpecialtyID = 4),'N')	AS ServicesPaintlessDentInd,
			l.Name								AS ShopLocationName,
			l.CertifiedFirstId					AS TargetedComment,
			l.WebSiteAddress
	  FROM  dbo.utb_shop_location l
		    LEFT OUTER JOIN dbo.utb_shop_location_pricing p
						 ON p.ShopLocationID = l.ShopLocationID
		    LEFT OUTER JOIN utb_shop_location_hours h
						 ON h.ShopLocationID = l.ShopLocationID
	 WHERE  l.EnabledFlag      = 1
	   AND (l.ProgramFlag      = 1 
	    OR  l.CEIProgramFlag   = 1 
	    OR  l.ReferralFlag     = 1
	    OR  l.CertifiedFirstId IS NOT NULL)

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error Inserting into utb_dtwh_dim_shop_location', 16, 1, @ProcName)
        RETURN
    END

    PRINT 'Inserted ' + CONVERT(varchar(10), @rowcount) + ' records into utb_dtwh_dim_shop_location...'
    
	-- -----------------------------------------------------------------------------------------

	UPDATE  dbo.utb_dtwh_dim_shop_location
	   SET  PrimaryContactEmailAddr		 = pe.EmailAddress,
			PrimaryContactName			 = pe.Name,
			PrimaryContactPhoneNumber	 = pe.PhoneAreaCode + ' ' + 
										   pe.PhoneExchangeNumber + ' ' + 
										   pe.PhoneUnitNumber,
			PrimaryContactPhoneNumberExt = pe.PhoneExtensionNumber
	  FROM  dbo.utb_personnel               pe,
			dbo.utb_shop_location_personnel lp
     WHERE  dbo.utb_dtwh_dim_shop_location.ShopLocationID = lp.ShopLocationID
       AND  lp.PersonnelID               = pe.PersonnelID
	   AND  lp.PersonnelID               =
		   (SELECT  TOP 1 x.PersonnelID
			  FROM  dbo.utb_personnel               x,
				    dbo.utb_shop_location_personnel y 
			 WHERE  dbo.utb_dtwh_dim_shop_location.ShopLocationID = y.ShopLocationID
               AND  x.PersonnelID                = y.PersonnelID
			   AND  x.PersonnelTypeID            = 2)		-- Shop Manager

	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error updating utb_dtwh_dim_shop_location w/ Primary Contact', 16, 1, @ProcName)
        RETURN
    END

    PRINT 'Updated ' + CONVERT(varchar(10), @rowcount) + ' utb_dtwh_dim_shop_location records w/ Primary Contact.'
    
	-- -----------------------------------------------------------------------------------------

	UPDATE  dbo.utb_dtwh_dim_shop_location
	   SET  SecondaryContactEmailAddr	   = pe.EmailAddress,
			SecondaryContactName		   = pe.Name,
			SecondaryContactPhoneNumber	   = pe.PhoneAreaCode + ' ' + 
										     pe.PhoneExchangeNumber + ' ' + 
										     pe.PhoneUnitNumber,
			SecondaryContactPhoneNumberExt = pe.PhoneExtensionNumber
	  FROM  dbo.utb_personnel               pe,
			dbo.utb_shop_location_personnel lp
     WHERE  dbo.utb_dtwh_dim_shop_location.ShopLocationID = lp.ShopLocationID
       AND  lp.PersonnelID               = pe.PersonnelID
	   AND  lp.PersonnelID               =
		   (SELECT  TOP 1 x.PersonnelID
			  FROM  dbo.utb_personnel               x,
				    dbo.utb_shop_location_personnel y 
			 WHERE  dbo.utb_dtwh_dim_shop_location.ShopLocationID = y.ShopLocationID
			   AND  x.PersonnelID                = y.PersonnelID
			   AND  x.PersonnelTypeID            = 3)	-- Office Person
	SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
	
    IF @error <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error updating utb_dtwh_dim_shop_location w/ Secondary Contact', 16, 1, @ProcName)
        RETURN
    END

	PRINT 'Updated ' + CONVERT(varchar(10), @rowcount) + ' utb_dtwh_dim_shop_location records w/ Secondary Contact.'
    
	-- -----------------------------------------------------------------------------------------

	PRINT 'Shop Location Data Warehouse Extract complete...'

END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhShopLocationData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhShopLocationData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
