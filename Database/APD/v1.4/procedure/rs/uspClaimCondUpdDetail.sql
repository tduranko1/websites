-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCondUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCondUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCondUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the fields in the Claim tab of ClaimXP
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimCondUpdDetail
    @LynxID                      udt_std_int_big,
    @PolicyNumber                udt_cov_policy_number,
    @ClientClaimNumber           udt_cov_claim_number,
    @ClientClaimNumberSquished   udt_cov_claim_number,
    @LossState                   udt_addr_state,
    @LossDescription             udt_std_desc_xlong,
    @Remarks                     udt_std_desc_xlong,
    @UserID                      udt_std_id,
    @SysLastUpdatedDate          varchar(30)    
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@LossDescription))) = 0 SET @LossDescription = NULL
    IF LEN(RTRIM(LTRIM(@LossState))) = 0 SET @LossState = NULL
    IF LEN(RTRIM(LTRIM(@Remarks))) = 0 SET @Remarks = NULL
    IF LEN(RTRIM(LTRIM(@PolicyNumber))) = 0 SET @PolicyNumber = NULL
    IF LEN(RTRIM(LTRIM(@ClientClaimNumber))) = 0 SET @ClientClaimNumber = NULL
    IF LEN(RTRIM(LTRIM(@ClientClaimNumberSquished))) = 0 SET @ClientClaimNumberSquished = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimCondUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Validate the updated date parameter for Claim
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim', @LynxID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION ClaimUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE dbo.utb_claim
       SET  LossDescription           = @LossDescription,
            LossState                 = @LossState,
            PolicyNumber              = @PolicyNumber,
            ClientClaimNumber         = @ClientClaimNumber,
            ClientClaimNumberSquished = @ClientClaimNumberSquished,
            Remarks                   = @Remarks,
            SysLastUserID             = @UserID,
            SysLastUpdatedDate        = @now
     WHERE 
		LynxID = @LynxID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION ClaimUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Claim Level
            NULL AS [Claim!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Claim Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCondUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCondUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
