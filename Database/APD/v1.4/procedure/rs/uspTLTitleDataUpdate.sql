-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTitleDataUpdate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLTitleDataUpdate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLTitleDataUpdate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Update the Total Loss Title Data
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  Checklist ID
* (I) @UserID       User ID
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLTitleDataUpdate
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @TitleName                      udt_std_desc_mid,
    @TitleState                     udt_addr_state,
    @TitleStatus                    udt_std_name = NULL,
    @UserID                         varchar(50)
AS
BEGIN

    --Initialize string parameters
    IF LEN(LTRIM(RTRIM(@TitleName))) = 0 SET @TitleName = NULL
    IF LEN(LTRIM(RTRIM(@TitleState))) = 0 SET @TitleState = NULL
    IF LEN(LTRIM(RTRIM(@TitleStatus))) = 0 SET @TitleStatus = NULL
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    DECLARE @ClaimAspectID as bigint
    
    
    SET @ProcName = 'uspTLTitleDataUpdate'
    SET @now = CURRENT_TIMESTAMP
    
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                  FROM utb_claim_aspect_service_channel
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        -- Invalid ClaimAspectServiceChannelID
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID.', 16, 1, @ProcName)
        RETURN
    END

    -- Check the user id
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @TitleName IS NULL
    BEGIN
        -- Invalid @Title Name
        RAISERROR('1|Title Name cannot be empty.', 16, 1, @ProcName)
        RETURN
    END

    IF @TitleState IS NULL OR 
    NOT EXISTS (SELECT StateCode
                   FROM dbo.utb_state_code
                   WHERE StateCode = @TitleState)
    BEGIN
        -- Invalid Title State
        RAISERROR('1|Title State cannot be empty and must be a valid state code.', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectID = ClaimAspectID
    FROM dbo.utb_claim_aspect_service_channel
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    BEGIN TRANSACTION
    
    UPDATE dbo.utb_claim_vehicle
    SET TitleName = @TitleName,
        TitleState = @TitleState,
        TitleStatus = @TitleStatus,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE ClaimAspectID = @ClaimAspectID

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
      
    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTitleDataUpdate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLTitleDataUpdate TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/