-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowSetUploadIndicator' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowSetUploadIndicator 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowSetUploadIndicator
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Sets the upload indicator for each of the DocumentIDs in the Document List passed in 
*
* PARAMETERS:  
* (I) @DocList              The DocumentIDs to be updated as a comma seperated list
* (I) @UserID               The user performing the update
*
* RESULT SET:   None
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowSetUploadIndicator
    @DocList    udt_std_desc_mid,
    @UserID     udt_std_id
AS
BEGIN
    -- Set Database options
    
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF

    
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@DocList))) = 0 SET @DocList = NULL
    
    
    -- Declare internal variables

    DECLARE @tmpDocList TABLE
    (
        DocumentID      bigint  NOT NULL
    )
    
    DECLARE @now                    udt_std_datetime
    
    DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWorkflowNotifyEvent'


    -- Verify a valid doclist has been passed in
    
    IF @DocList IS NULL
    BEGIN
        -- Invalid Doc List

        RAISERROR('101|%s|@DocList|%s', 16, 1, @ProcName, @DocList)
        RETURN
    END
    

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Split DocList and convert to a recordset, removing non-numeric values in the process
    
    INSERT INTO @tmpDocList
      SELECT  value 
        FROM  dbo.ufnUtilityParseString(@DocList, ',', 1) -- 1=trim spaces
        WHERE IsNumeric(value) = 1
        
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('105|%s|%s', 16, 1, @ProcName, '@tmpDocList')
        RETURN
    END
            
    
    -- Continue if there is at least one document id in the temp table
    
    IF EXISTS(SELECT * FROM @tmpDocList)
    BEGIN
        -- Get current timestamp
        
        SET @now = CURRENT_TIMESTAMP

        
        -- Update the document records that match
        
        UPDATE  dbo.utb_document 
          SET   SendToCarrierFlag = 1,
                SysLastUserID = @UserID,
                SysLastUpdatedDate = @now
          FROM  @tmpDocList tmp, dbo.utb_document d
          WHERE d.DocumentID = tmp.DocumentID
          
        
        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
                  

        -- Throw an internal warning if any of the document ids in @tmpDoclist do not exist.  If this condition arises,
        -- it's likely a coding error we would need to deal with
        
        IF EXISTS(SELECT DocumentID FROM @tmpDocList WHERE DocumentID NOT IN (SELECT DocumentID FROM dbo.utb_document))
        BEGIN
            -- A document ID passed in does not exist
    
            RAISERROR('3|%s: At least one Document ID passed in does not exist in utb_document.  No error message was generated.', 16, 1, @ProcName)
        END
    END                  
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowSetUploadIndicator' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowSetUploadIndicator TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/