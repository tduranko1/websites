-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDRevenueReport' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAPDRevenueReport 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAPDRevenueReport
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the APD revenue for the duration
*
* PARAMETERS:
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAPDRevenueReport
(
	@RptStartDate	varchar(30) = null,
	@RptEndDate		varchar(30) = null
)
AS
BEGIN
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF 

    DECLARE @ProcName as varchar(50)
    DECLARE @ReportStartDate as datetime
    DECLARE @ReportEndDate as datetime
    DECLARE @ReportRuntime as datetime
    DECLARE @PrevMonth as datetime
    DECLARE @InsuranceCoIDWorking as int
    DECLARE @InsuranceCoNameWorking as varchar(100)
    DECLARE @EndOfMonthPosting as datetime
    DECLARE @ReportMonths as int
    DECLARE @CurrentMonth as int
    DECLARE @CurrentStartDate as datetime
    DECLARE @CurrentEndDate as datetime

    DECLARE @BilledCount as int
    DECLARE @BilledAmount as decimal(9,2)

    /*RENA means revenue earned and not accrued. In other terms if we a claim was received 
    -- on a certain month and the billed date is the subsequent month. Need to note here
    -- that anything billed to Ingres is posted as revenue on the subsequent day. If we 
    -- bill an item on the last day of the month, the revenue is posted on the first of
    -- the month. Ingres posts revenue at round 1:15am for the previous day. Anything billed
    -- between 00hrs to 1:15AM are accounted for the next day.
    */

    DECLARE @RENACount as int
    DECLARE @RENAAmount as decimal(9,2)

    DECLARE @UnbilledCount as int
    DECLARE @UnbilledAmount as decimal(9,2)

    DECLARE @InsID as int

    DECLARE @debug as bit

	 SET @debug = 0
	 SET @InsID = 178

    set @ProcName = ''

    SET @ReportRuntime = CURRENT_TIMESTAMP
    SET @PrevMonth = dateadd(month, -1, @ReportRuntime)

    --set @ReportStartDate = convert(datetime, '6/1/2007')
    --set @ReportEndDate = convert(datetime, '7/31/2007') --CURRENT_TIMESTAMP

    IF isDate(@RptStartDate) = 1
    BEGIN
        SET @ReportStartDate = convert(datetime, @RptStartDate)
    END

    IF isDate(@RptEndDate) = 1
    BEGIN
        SET @ReportEndDate = convert(datetime, @RptEndDate)
    END

    IF @ReportStartDate IS NULL
    BEGIN
	    -- Beginning of the previous month
	    SET @ReportStartDate = convert(datetime, convert(varchar, month(@PrevMonth)) + '/1/' + convert(varchar, year(@PrevMonth)))
    END

    IF @ReportEndDate IS NULL
    BEGIN
	    SET @ReportEndDate = DATEADD(second, -1, DATEADD(month, 1, @ReportStartDate))
    END

    SET @ReportMonths = DATEDIFF(month, @ReportStartDate, @ReportEndDate) + 1

    IF @Debug = 1
    BEGIN
        PRINT '@ReportStartDate = ' + convert(varchar, @ReportStartDate)
        PRINT '@ReportEndDate = ' + convert(varchar, @ReportEndDate)
        PRINT '@ReportMonths = ' + convert(varchar, DATEDIFF(month, @ReportStartDate, @ReportEndDate) + 1)
    END

    DECLARE @tmpInsCo TABLE (
	    InsCoID						varchar(10)		NULL,
	    InsuranceCompanyName		varchar(50)		NULL
    )

    DECLARE @tmpAPDRevenue TABLE (
	    InsCoID						varchar(10)		NULL,
	    InsuranceCompanyName		varchar(50)		NULL,
	    DataStart					datetime		NULL,
	    DataEnd					    datetime		NULL,
	    BilledCount				    int				NULL,
	    BilledAmount				decimal(9,2)	NULL,
	    RENACount  					int				NULL,
	    RENAAmount				    decimal(9,2)	NULL,
	    UnBilledCount			    int				NULL,
	    UnBilledAmount			    decimal(9,2)	NULL
    )

    DECLARE @tmpBillingData TABLE (
	    LynxID					bigint			NULL,
	    RevenueDate			    datetime		NULL,
	    bitBilled				bit				NULL,
	    bitPosted				bit				NULL, -- posted in the current month 
	    BilledDate				datetime		NULL, 
        DispatchNumber          varchar(50)		NULL, 
	    FeeAmount				decimal(9,2)	NULL
    )
	 
	INSERT INTO @tmpInsCo
	SELECT InsuranceCompanyID, Name
	FROM utb_insurance
	WHERE demoFlag = 0
	  AND EnabledFlag = 1
	  --AND @debug = 0)
	  --OR (@debug = 1 AND InsuranceCompanyID = @InsID)
	ORDER BY Name

	IF @debug = 1
	BEGIN
        DELETE FROM @tmpInsCo
        WHERE InsCoID <> @InsID

		SELECT * FROM @tmpInsCo
	END
   
    -- Start gathering data for the whole report duration
    SET @CurrentMonth = 1

    WHILE @CurrentMonth <= @ReportMonths
    BEGIN
	    IF @CurrentMonth = 1
	    BEGIN
		    SET @CurrentStartDate = @ReportStartDate
		    SET @CurrentEndDate = DATEADD(second, -1, DATEADD(month, 1, @CurrentStartDate))
		    IF month(@CurrentStartDate) <> month(@CurrentEndDate)
		    BEGIN
			    SET @CurrentEndDate = DATEADD(second, -1, CONVERT(datetime, convert(varchar, month(@CurrentEndDate)) + '/01/' + convert(varchar, year(@CurrentEndDate))))
		    END
	    END

	    IF @CurrentMonth = @ReportMonths
	    BEGIN
		    SET @CurrentEndDate = @ReportEndDate
	    END
       
        SET @EndOfMonthPosting = CONVERT(datetime, convert(varchar, @CurrentEndDate, 101) + ' 23:59:59')


        IF @debug = 1 
        BEGIN
	        PRINT 'Processing Month: ' + convert(varchar, @CurrentMonth)
	        PRINT 'Processing : ' + convert(varchar, @CurrentStartDate) + ' to ' + convert(varchar, @CurrentEndDate)
        END

        -- Calculate data for the current date for all insurance companies
        --select * from @tmpFNOLRevenue

        DECLARE csrInsurance CURSOR FOR
            SELECT InsCoID, InsuranceCompanyName FROM @tmpInsCo
            
        OPEN csrInsurance

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrInsurance
        INTO @InsuranceCoIDWorking, @InsuranceCoNameWorking

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        WHILE @@Fetch_Status = 0
        BEGIN
            DELETE FROM @tmpBillingData 
            
            SET @BilledCount = 0
            SET @BilledAmount = 0
            SET @RENACount = 0
            SET @RENAAmount = 0
            SET @UnbilledCount = 0
            SET @UnbilledAmount = 0
            
            IF @debug = 1
            BEGIN
               PRINT 'Insurance Company = ' + @InsuranceCoNameWorking
            END

            INSERT INTO @tmpBillingData 
            (LynxID, RevenueDate, bitBilled, bitPosted, BilledDate, DispatchNumber, FeeAmount)
            SELECT c.LynxID, 
                   i.EntryDate, 
                   CASE 
                     WHEN i.DispatchNumber <> '' THEN 1
                     ELSE 0
                   END, 
                   CASE 
                     WHEN i.SentToIngresDate IS NOT NULL AND i.SentToIngresDate < @EndOfMonthPosting THEN 1
                     WHEN i.StatusCD = 'FS' AND i.StatusDate < @EndOfMonthPosting THEN 1
                     ELSE 0
                   END, 
                   CASE 
                     WHEN i.SentToIngresDate IS NOT NULL THEN i.SentToIngresDate
                     WHEN i.StatusCD = 'FS' THEN i.StatusDate
                   END, 
                   i.DispatchNumber,
                   i.Amount
            FROM utb_invoice i
            LEFT JOIN utb_claim_aspect_service_channel casc ON i.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
            LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
            LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
            WHERE i.EntryDate BETWEEN @CurrentStartDate AND @CurrentEndDate
              AND i.EnabledFlag = 1
              AND i.ItemTypeCD = 'F'
              AND c.InsuranceCompanyID = @InsuranceCoIDWorking

            IF @debug = 1
            BEGIN
               SELECT * from @tmpBillingData
            END

            SELECT @BilledCount = COUNT(LynxID),
                   @BilledAmount = isNull(SUM(FeeAmount), 0)
            FROM @tmpBillingData
            WHERE bitBilled = 1
              AND bitPosted = 1
            
            SELECT @RENACount = COUNT(LynxID),
                   @RENAAmount = isNull(SUM(FeeAmount), 0)
            FROM @tmpBillingData
            WHERE bitBilled = 1
              AND bitPosted = 0

            SELECT @UnbilledCount = COUNT(LynxID),
                   @UnbilledAmount = isNULL(SUM(FeeAmount), 0)
            FROM @tmpBillingData
            WHERE bitBilled = 0

            INSERT INTO @tmpAPDRevenue(
               InsCoID,
               InsuranceCompanyName,
               DataStart,
               DataEnd,
               BilledCount,
               BilledAmount,
               RENACount,
               RENAAmount,
               UnBilledCount,
               UnBilledAmount
            ) VALUES (
               @InsuranceCoIDWorking, 
               @InsuranceCoNameWorking,
               @CurrentStartDate,
               @CurrentEndDate,
               @BilledCount,
               @BilledAmount,
               @RENACount,
               @RENAAmount,
               @UnbilledCount,
               @UnbilledAmount
            )
            
            FETCH next
            FROM csrInsurance
            INTO @InsuranceCoIDWorking, @InsuranceCoNameWorking

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR  ('99|%s', 16, 1, @ProcName)
                RETURN
            END    

        END

        CLOSE csrInsurance
        DEALLOCATE csrInsurance

        -- summary
        insert into @tmpAPDRevenue
        select 
	        '',
	        'Summary for ' + convert(varchar, Month(@CurrentStartDate)) + '/' + convert(varchar, Year(@CurrentStartDate)),
	        @CurrentStartDate,
	        @CurrentEndDate,
	        sum(BilledCount),
	        sum(BilledAmount),
	        sum(RENACount),
	        sum(RENAAmount),
	        sum(UnbilledCount),
	        sum(UnbilledAmount)
        from @tmpAPDRevenue
        where DataStart = @CurrentStartDate
          AND DataEnd = @CurrentEndDate
        group by DataEnd

	    SET @CurrentMonth = @CurrentMonth + 1
	    SET @CurrentStartDate = DATEADD(second, 1, @CurrentEndDate)
	    SET @CurrentEndDate = DATEADD(second, -1, DATEADD(month, 1, @CurrentStartDate))
    END

    SELECT * from @tmpAPDRevenue
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDRevenueReport' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAPDRevenueReport TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/