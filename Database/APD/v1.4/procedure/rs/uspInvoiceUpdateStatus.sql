-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceUpdateStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceUpdateStatus 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceUpdateStatus
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     "Deletes" (ie. Disables) an invoice record
*
* PARAMETERS:  
* (I) @InvoiceID            The Invoice ID record to "delete"
* (I) @UserID               The user id performing the "delete"
* (I) @SyslastUpdatedDate   The "previous" updated date
* (I) @NotifyEvent          A flag specifying whether this proc handles workflow notification
*
* RESULT SET:
*       None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspInvoiceUpdateStatus
    @ClaimAspectID    udt_std_id_big,
    @UserID           udt_std_id
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now                AS udt_std_datetime

    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspInvoiceUpdateStatus'

    -- Set Database options    
    SET NOCOUNT ON
    
    
    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP


    -- Check to make sure a valid User id was passed
    IF  (dbo.ufnUtilityIsUserActive(@UserID, 'APD', NULL) = 0)
    BEGIN
        -- Invalid User ID    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
--    -- Validate @StatusCD
--    IF (@StatusCD IS NULL OR NOT EXSISTS (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')))
--    BEGIN
--        -- Invalid StatusCD  
--        RAISERROR('101|%s|@StatusCD|%u', 16, 1, @ProcName, @StatusCD)
--        RETURN
--    END


    -- Validate @ClaimAspectID
    IF (@ClaimAspectID IS NULL OR NOT EXISTS (SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid @ClaimAspectID  
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END
         
    -- Validate that this client requries pre-Authorization before setting the status.
    IF (1 <> (  Select      csc.ClientAuthorizesPaymentFlag
                from       utb_Claim_Aspect_Service_Channel casc
                
                inner join  utb_Claim_Aspect ca
                on          ca.ClaimAspectID = casc.ClaimAspectID
                and         casc.PrimaryFlag = 1 -- pick only the primary service channel for the claim
                and         casc.EnabledFlag = 1 -- Only pick the enabled service channel
                
                inner join  utb_Claim c
                on          c.LynxID = ca.LynxID
                
                inner join  utb_client_service_channel csc
                on          c.InsuranceCompanyID = csc.InsuranceCompanyID
                and         casc.ServiceChannelCD = csc.ServiceChannelCD

                WHERE ca.ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid data state.  
        RAISERROR  ('1|Invalid Status.  This client is not configured to require approval of invoice items.', 16, 1, @ProcName)
        RETURN
    END
         
    
    -- Begin Update
    BEGIN TRANSACTION InvoiceUpdStatTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE  dbo.utb_invoice
      SET   StatusCD            = 'AC',
            StatusDate          = @now,
            SysLastUserID       = @UserID,
            SysLastUpdatedDate  = @now
      WHERE ClaimAspectID = @ClaimAspectID
        AND EnabledFlag = 1
        AND StatusCD = 'APD'
      

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
        
    -- Check error value    
    IF @error <> 0
    BEGIN
        -- Update failed
        RAISERROR('104|%s|utb_invoice', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
      
        
    COMMIT TRANSACTION InvoiceUpdStatTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    RETURN @rowcount
END

GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceUpdateStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceUpdateStatus TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/