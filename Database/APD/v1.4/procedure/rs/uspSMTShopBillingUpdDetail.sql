-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopBillingUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopBillingUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopBillingUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Shop Location Billing record
*
*
* RESULT SET:
* BillingID                               The updated Shop Location billing unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopBillingUpdDetail
(
	  @BillingID                    	    udt_std_int_big,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
	  @EFTAccountNumber                   udt_std_desc_short=NULL,
	  @EFTAccountTypeCD                   udt_std_cd=NULL,
	  @EFTContractSignedFlag              udt_std_flag=0,
	  @EFTEffectiveDate                   VARCHAR(30)=NULL,
	  @EFTRoutingNumber                   udt_std_desc_short=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @SysLastUpdatedDate                 VARCHAR(30),
    @ApplicationCD                      udt_std_cd='APD',
    @MergeFlag                          udt_std_flag=0
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @tEFTEffectiveDate  udt_std_datetime 

    DECLARE @tupdated_date      udt_std_datetime 
    DECLARE @temp_updated_date  udt_std_datetime 

    DECLARE @ProcName           varchar(30)       -- Used for raise error stmts
    
    DECLARE @LogComment         udt_std_desc_long,
            @oldValue           udt_std_desc_short,
            @newValue           udt_std_desc_short,
            @ShopID             udt_std_id_big,
            @KeyDescription     udt_std_desc_short
    
    
    DECLARE @dbAddress1                           udt_addr_line_1,
            @dbAddress2                           udt_addr_line_2,
            @dbAddressCity                        udt_addr_city,
            @dbAddressCounty                      udt_addr_county,
            @dbAddressState                       udt_addr_state,
            @dbAddressZip                         udt_addr_zip_code,
	          @dbEFTAccountNumber                   udt_std_desc_short,
	          @dbEFTAccountTypeCD                   udt_std_cd,
	          @dbEFTContractSignedFlag              udt_std_flag,
	          @dbEFTEffectiveDate                   VARCHAR(30),
	          @dbEFTRoutingNumber                   udt_std_desc_short,
            @dbFaxAreaCode                        udt_ph_area_code,
            @dbFaxExchangeNumber                  udt_ph_exchange_number,
            @dbFaxExtensionNumber                 udt_ph_extension_number,
            @dbFaxUnitNumber                      udt_ph_unit_number,
            @dbName                               udt_std_name,
            @dbPhoneAreaCode                      udt_ph_area_code,
            @dbPhoneExchangeNumber                udt_ph_exchange_number,
            @dbPhoneExtensionNumber               udt_ph_extension_number,
            @dbPhoneUnitNumber                    udt_ph_unit_number
    
    

    SET @ProcName = 'uspSMTShopBillingUpdDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountNumber))) = 0 SET @EFTAccountNumber = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountTypeCD))) = 0 SET @EFTAccountTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@EFTEffectiveDate))) = 0 SET @EFTEffectiveDate = NULL
    IF LEN(RTRIM(LTRIM(@EFTRoutingNumber))) = 0 SET @EFTRoutingNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits

    IF @EFTEffectiveDate IS NOT NULL AND
       ISDATE(@EFTEffectiveDate) = 0
    BEGIN
        -- Convert the value passed in effective date into date format
        
        SET @tEFTEffectiveDate = CAST(@EFTEffectiveDate AS DATETIME)
        
        -- Validate the effective date
        
        IF @tEFTEffectiveDate IS NULL
        BEGIN 
            -- Invalid effective date value
            RAISERROR('101|%s|@EFTEffectiveDate|%s', 16, 1, @ProcName, @EFTEffectiveDate)
            RETURN
        END 
    END
    
    IF @EFTAccountTypeCD IS NOT NULL
    BEGIN
        IF @EFTAccountTypeCD NOT IN ('S', 'C')
        BEGIN
           -- Invalid Account Type
            RAISERROR('101|%s|@EFTAccountTypeCD|%s', 16, 1, @ProcName, @EFTAccountTypeCD)
            RETURN
        END
    END
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Billing Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Billing Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END

    -- Check the date
    IF @MergeFlag = 0
    BEGIN
      EXEC uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_billing', @BillingID
      IF @@ERROR <> 0
      BEGIN
          -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

          RETURN
      END
    END
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
 
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END

    SELECT @dbAddress1 = Address1,
           @dbAddress2 = Address2,
           @dbAddressCity = AddressCity,
           @dbAddressCounty = AddressCounty,
           @dbAddressState = AddressState,
           @dbAddressZip = AddressZip,
           @dbEFTAccountNumber = EFTAccountNumber,
           @dbEFTAccountTypeCD = EFTAccountTypeCD,
           @dbEFTContractSignedFlag = EFTContractSignedFlag,
           @dbEFTEffectiveDate = EFTEffectiveDate,
           @dbEFTRoutingNumber = EFTRoutingNumber,
           @dbFaxAreaCode = FaxAreaCode,
           @dbFaxExchangeNumber = FaxExchangeNumber,
           @dbFaxExtensionNumber = FaxExtensionNumber,
           @dbFaxUnitNumber = FaxUnitNumber,
           @dbName = Name,
           @dbPhoneAreaCode = PhoneAreaCode,
           @dbPhoneExchangeNumber = PhoneExchangeNumber,
           @dbPhoneExtensionNumber = PhoneExtensionNumber
    FROM dbo.utb_billing
    WHERE BillingID = @BillingID
    
    SET @ShopID = (SELECT ShopLocationID FROM dbo.utb_shop_location WHERE BillingID = @BillingID)
    SET @KeyDescription = CONVERT(varchar(10), @BillingID) + ' - ' + @dbName

    -- Begin Update(s)

    BEGIN TRANSACTION AdmBillingUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    UPDATE  dbo.utb_billing SET
            Address1 = @Address1,
            Address2 = @Address2,
            AddressCity = @AddressCity,
            AddressCounty = @AddressCounty,
            AddressState = @AddressState,
            AddressZip = @AddressZip,
    		    EFTAccountNumber = @EFTAccountNumber,
    		    EFTAccountTypeCD = @EFTAccountTypeCD,
    		    EFTContractSignedFlag = @EFTContractSignedFlag,
    		    EFTEffectiveDate = @EFTEffectiveDate,
    		    EFTRoutingNumber = @EFTRoutingNumber,
            FaxAreaCode = @FaxAreaCode,
            FaxExchangeNumber = @FaxExchangeNumber,
            FaxExtensionNumber = @FaxExtensionNumber,
            FaxUnitNumber = @FaxUnitNumber,
            Name = @Name,
            PhoneAreaCode = @PhoneAreaCode,
            PhoneExchangeNumber = @PhoneExchangeNumber,
            PhoneExtensionNumber = @PhoneExtensionNumber,
            PhoneUnitNumber = @PhoneUnitNumber,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE BillingID = @BillingID
        AND SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- Audit Log Code -----------------------------------------------------------------------------------------
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbName, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Name, ''))))
    BEGIN
      SET @LogComment = 'Payment Name changed from ' + @dbName + ' to ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress1, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address1, ''))))
    BEGIN
      SET @LogComment = 'Payment Address 1 was changed from ' + 
                        ISNULL(@dbAddress1, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address1, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress2, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address2, ''))))
    BEGIN
      SET @LogComment = 'Payment Address 2 was changed from ' + 
                        ISNULL(@dbAddress2, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address2, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressZip, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressZip, ''))))
    BEGIN
      SET @LogComment = 'Payment Zip Code was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressZip), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressZip), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCity, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCity, ''))))
    BEGIN
      SET @LogComment = 'Payment City was changed from ' + 
                        ISNULL(@dbAddressCity, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCity, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressState, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressState, ''))))
    BEGIN
      SET @LogComment = 'Payment State was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressState), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressState), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCounty, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCounty, ''))))
    BEGIN
      SET @LogComment = 'Payment County was changed from ' + 
                        ISNULL(@dbAddressCounty, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCounty, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Payment Phone Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Phone Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
      
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Phone Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Phone Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Payment Fax Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Fax Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Fax Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Payment Fax Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
     
    
    IF @dbEFTContractSignedFlag <> @EFTContractSignedFlag
    BEGIN
      
      SET @LogComment = 'EFT Contract Signed Flag was turned ' + (SELECT CASE @dbEFTContractSignedFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbEFTAccountNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@EFTAccountNumber, ''))))
    BEGIN
      SET @LogComment = 'EFT Account Number was changed from ' + 
                        ISNULL(@dbEFTAccountNumber, 'NON-EXISTENT') + ' to ' + 
                        ISNULL(@EFTAccountNumber, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbEFTAccountTypeCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@EFTAccountTypeCD, ''))))
    BEGIN
      SET @oldValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_billing', 'EFTAccountTypeCD') WHERE Code = @dbEFTAccountTypeCD)
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_billing', 'EFTAccountTypeCD') WHERE Code = @EFTAccountTypeCD)
      
      SET @LogComment = 'EFT Account Type was changed from ' + 
                        ISNULL(CONVERT(varchar(50), @oldValue), 'NON-EXISTENT') + ' to ' + 
                        ISNULL(CONVERT(varchar(50), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbEFTRoutingNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@EFTRoutingNumber, ''))))
    BEGIN
      SET @LogComment = 'EFT Routing Number was changed from ' + 
                        ISNULL(@dbEFTRoutingNumber, 'NON-EXISTENT') + ' to ' + 
                        ISNULL(@EFTRoutingNumber, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    SET @oldValue = ISNULL(CONVERT(varchar(12), @dbEFTEffectiveDate), '')
    SET @newValue = ISNULL(CONVERT(varchar(12), @EFTEffectiveDate), '')
    
    
    IF @oldValue <> @newValue
    BEGIN
            
      SET @LogComment = 'EFT Effective Date was changed from ' + @oldValue + ' to ' + @newValue
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
         
      
   --  End of Audit Log code ------------------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmBillingUpdDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @BillingID AS BillingID

    RETURN @rowcount
END


GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopBillingUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopBillingUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
