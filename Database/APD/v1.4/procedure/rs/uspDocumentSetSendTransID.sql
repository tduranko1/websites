-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentSetSendTransID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocumentSetSendTransID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDocumentSetSendTransID
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDocumentSetSendTransID
    @DocumentID     udt_std_id_big,
    @TransactionID  udt_std_desc_mid
AS
BEGIN
    DECLARE @ProcName udt_std_name
    SET @ProcName = 'uspDocumentSetSendTransID'
    

    IF EXISTS (SELECT * FROM utb_document WHERE DocumentID = @DocumentID)
    BEGIN

        BEGIN TRANSACTION        
        UPDATE utb_document
        SET SendToCarrierTransactionID  = @TransactionID,
            SendToCarrierStatusCD = 'UNK'
        WHERE  DocumentID = @DocumentID
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION            
        END            
        ELSE
        BEGIN
            COMMIT
        END           
    END
    ELSE
    BEGIN
         RAISERROR('1|%s:Document ID passed- %d - does not exist', 16, 1, @ProcName,@DocumentID)
    END            
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentSetSendTransID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocumentSetSendTransID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/