-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessInfoInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTBusinessInfoInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTBusinessInfoInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Shop
*
* PARAMETERS:  
* (I) @BusinessParentID                   The shop parent's identity
* (I) @BusinessTypeCD                     The shop's business type code (S = Sole Proprietorship,
*                                         P = Partnership, C = Corporation)
* (I) @Address1                           The shop's address line 1
* (I) @Address2                           The shop's address line 2
* (I) @AddressCity                        The shop's address city
* (I) @AddressCounty                      The shop's address county
* (I) @AddressState                       The shop's address state
* (I) @AddressZip                         The shop's address zip code
* (I) @EnabledFlag                        The shop's active/inactive flag
* (I) @FaxAreaCode                        The shop's fax area code
* (I) @FaxExchangeNumber                  The shop's fax exchange number
* (I) @FaxExtensionNumber                 The shop's fax extension number
* (I) @FaxUnitNumber                      The shop's fax unit number
* (I) @FedTaxId                           The shop's federal tax id
* (I) @Name                               The shop's name
* (I) @PhoneAreaCode                      The shop's phone area code
* (I) @PhoneExchangeNumber                The shop's phone exchange number
* (I) @PhoneExtensionNumber               The shop's phone extension number
* (I) @PhoneUnitNumber                    The shop's phone unit number
* (I) @SysLastUserID                      The shop's updating user identity
*
* RESULT SET:
* ShopID                                  The newly created Shop unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTBusinessInfoInsDetail
(
    @BusinessParentID                   udt_std_id_big=NULL,
    @BusinessTypeCD                     udt_std_cd,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @EnabledFlag                        udt_enabled_flag=1,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @FedTaxId                           udt_fed_tax_id=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @retVal             AS BIT

    DECLARE @BusinessInfoID     AS udt_std_id_big

    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTBusinessInfoInsDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@BusinessTypeCD))) = 0 SET @BusinessTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@FedTaxId))) = 0 SET @FedTaxId = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF @BusinessTypeCD NOT IN ('S','P','C')
    BEGIN
       -- Invalid Business Type
        RAISERROR('101|%s|@BusinessTypeCD|%s', 16, 1, @ProcName, @BusinessTypeCD)
        RETURN
    END

    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @BusinessParentID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopParentID FROM utb_shop_parent WHERE ShopParentID = @BusinessParentID)
        BEGIN
           -- Invalid Shop Parent
            RAISERROR('101|%s|@BusinessParentID|%u', 16, 1, @ProcName, @BusinessParentID)
            RETURN
        END
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END

        
    IF (@FedTaxId is not NULL) and LEN(@FedTaxId) > 0
    BEGIN
        IF EXISTS (SELECT FedTaxId FROM utb_shop WHERE FedTaxId = @FedTaxId AND EnabledFlag = 1)
        BEGIN
           -- Duplicate Federal Tax Id
            RAISERROR('101|%s|@FedTaxId|%s', 16, 1, @ProcName, @FedTaxId)
            RETURN
        END
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_shop
    (
        ShopParentID,
        BusinessTypeCD,
        Address1,
        Address2,
        AddressCity,
        AddressCounty,
        AddressState,
        AddressZip,
        EnabledFlag,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        FedTaxId,
        Name,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES  
    (
        @BusinessParentID,
        @BusinessTypeCD,
        @Address1,
        @Address2,
        @AddressCity,
        @AddressCounty,
        @AddressState,
        @AddressZip,
        @EnabledFlag,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @FedTaxId,
        @Name,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    SET @BusinessInfoID = SCOPE_IDENTITY()
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    -- Begin Audit Log code  --------------------------------------------------------------------------------
    
    DECLARE @KeyDescription         udt_std_desc_mid,
            @LogComment             udt_std_desc_long,
            @newValue               udt_std_name
            
    
    SET @KeyDescription = 'Business-' + @Name
    
    
    SET @LogComment = 'Business ID established: ' + CONVERT(varchar(12), @BusinessInfoID)
  
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
         
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        
        ROLLBACK TRANSACTION
        RETURN
    END
    
    
    SET @LogComment = 'SSN\EIN established: ' + ISNULL(@FedTaxId, 'NONE')
  
    IF @FedTaxID IS NOT NULL
    BEGIN
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
         
    
    SET @LogComment = 'Name established: ' + ISNULL(@Name, 'NONE')
  
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
         
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        
        ROLLBACK TRANSACTION
        RETURN
    END  
    
    
    IF @Address1 IS NOT NULL
    BEGIN   
      SET @LogComment = 'Address 1 established: ' + ISNULL(@Address1, 'NONE')
  
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    

    
    IF @Address2 IS NOT NULL
    BEGIN
      SET @LogComment = 'Address 2 established: ' + ISNULL(@Address2, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressZip IS NOT NULL
    BEGIN
      SET @LogComment = 'Zip Code established: ' + ISNULL(@AddressZip, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCity IS NOT NULL
    BEGIN
      SET @LogComment = 'City established: ' + ISNULL(@AddressCity, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressState IS NOT NULL
    BEGIN
      SET @LogComment = 'State established: ' + ISNULL(@AddressState, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @AddressCounty IS NOT NULL
    BEGIN
      SET @LogComment = 'County established: ' + ISNULL(@AddressCounty, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Area Code established ' + ISNULL(@PhoneAreaCode, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Exchange Number established: ' + ISNULL(@PhoneExchangeNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Unit Number establisned: ' + ISNULL(@PhoneUnitNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Extension Number established: ' + ISNULL(@PhoneExtensionNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Area Code established: ' + ISNULL(@FaxAreaCode, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Exchange Number established: ' + ISNULL(@FaxExchangeNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Unit Number established: ' + ISNULL(@FaxUnitNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Extension Number established: ' + ISNULL(@FaxExtensionNumber, 'NONE')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @BusinessParentID IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.utb_shop_parent WHERE ShopParentID = @BusinessParentID)
    
      SET @LogComment = 'Parent established: ' + @newValue
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @BusinessTypeCD IS NOT NULL
    BEGIN
      SET @newValue = ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop', 'BusinessTypeCD') 
                                   WHERE Code = @BusinessTypeCD), 'NONE')
    
      SET @LogComment = 'Business Type established: ' + @newValue
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @BusinessInfoID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    -- End Audit Log code -----------------------------------------------------------------------------------
    
    

    COMMIT TRANSACTION AdmShopInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @BusinessInfoID AS BusinessInfoID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessInfoInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTBusinessInfoInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END


