-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspApproveDocument' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspApproveDocument 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspApproveDocument
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Approves a document and adds a payment if necessary
*
* PARAMETERS:  
* (I) @DocumentID        Document ID that is being approved
* (I) @ApprovedFlag      Approved flag
* (I) @NetAmount         Net amount on the estimate
* (I) @DeductibleAmount  Deductible amount on the estimate
* (I) @TaxTotalAmount    Total Tax amount on the estimate
* (I) @UserID            User initiating this request
*
* RESULT SET:
*   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspApproveDocument
    @DocumentID        udt_std_id_big,
    @ApprovedFlag      udt_std_flag,
    @NetAmount         decimal(9, 2),
    @DeductibleAmount  decimal(9, 2),
    @TaxTotalAmount    decimal(9, 2),
    @CreatePaymentFlag bit = 1,
    @UserID            udt_std_id_big
AS
BEGIN
    
    -- Declare internal variables

    DECLARE @error                            AS udt_std_int
    DECLARE @rowcount                         AS udt_std_int
    DECLARE @now                              AS udt_std_datetime
    DECLARE @ProcName                         AS varchar(30)
    
    
    DECLARE @EstimateTypeFlag                 AS udt_std_flag
    DECLARE @ClaimAspectID                    AS udt_std_id_big
    DECLARE @ClaimAspectServiceChannelID      AS udt_std_id_big
    DECLARE @PayeeAddress1                    AS udt_addr_line_1
    DECLARE @PayeeAddressCity                 AS udt_addr_city
    DECLARE @PayeeAddressState                AS udt_addr_state
    DECLARE @PayeeAddressZip                  AS udt_addr_zip_code
    DECLARE @PayeeID                          AS udt_std_id_big
    DECLARE @PayeeName                        AS udt_std_name
    DECLARE @EarlyBillFlag                    AS udt_std_flag
    DECLARE @InvoiceDescription               AS varchar(50)
    DECLARE @AgreedPriceMetCD                 AS udt_std_cd
    DECLARE @VANFlag                          AS udt_std_flag
    DECLARE @DispositionTypeCD                AS varchar(5)
    
    -- SET DATABASE OPTIONS
    SET NOCOUNT ON
    
    SET @ProcName = 'uspApproveDocument'
    SET @now = CURRENT_TIMESTAMP
    
    SELECT @EstimateTypeFlag = EstimateTypeFlag,
           @AgreedPriceMetCD = AgreedPriceMetCD,
           @VANFlag = VANFlag
    FROM dbo.utb_document d
    LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN dbo.utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
    WHERE DocumentID = @DocumentID
    
    IF @EstimateTypeFlag = 0 OR @EstimateTypeFlag IS NULL
    BEGIN
        -- Document is not an Estimate/Supplement. Cannot approve for early billing
    
        RAISERROR('101|%s|@DocumentID is not an Estimate/Supplement|%u', 16, 1, @ProcName, @DocumentID)
        RETURN
    END
    
    -- Check to make sure a valid User id was passed in
    IF (dbo.ufnUtilityIsUserActive(@UserID, 'APD', NULL) = 0)
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Collect the information required to setup a payment
    SELECT @ClaimAspectID = ca.ClaimAspectID,
           @ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID,
           @EarlyBillFlag = i.EarlyBillFlag,
           @InvoiceDescription = 'Early Bill: ' + isNull(uf.Name, ''),
           @DispositionTypeCD = isNull(casc.DispositionTypeCD, '')
    FROM dbo.utb_document d
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'CoverageProfileCD') uf ON ca.CoverageProfileCD = uf.Code
    WHERE d.DocumentID = @DocumentID
    
     -- 15May2012 - TVD - Adding SQL to get Service Channel
    DECLARE @vServiceChannel VARCHAR(3)
    SELECT @vServiceChannel= ServiceChannelCD FROM utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    IF @ApprovedFlag = 1
    BEGIN

       -- Check if there is another approved document for this vehicle
       IF EXISTS(SELECT d.DocumentID
                  FROM dbo.utb_document d
                  LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
                  LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                  LEFT JOIN dbo.utb_document_type dt ON d.DOcumentTypeID = dt.DocumentTypeiD
                  WHERE casc.ClaimAspectID = @ClaimAspectID
                    AND d.EnabledFlag = 1
                    AND d.ApprovedFlag = 1
                    AND dt.EstimateTypeFlag = 1
                    AND d.DocumentID <> @DocumentID
                )
       BEGIN
         -- An approved document already exists
         RAISERROR('1|Cannot Approve this document. Another document is already been approved for early billing.', 16, 1)
         RETURN
       END

       --IF @VANFlag = 0 AND (@AgreedPriceMetCD = 'N' OR @AgreedPriceMetCD IS NULL)
       --BEGIN
       --    -- Agreed Price is not met for this document
       
       --    RAISERROR('1|Cannot Approve this document. Agreed Price has not been set or set to No.', 16, 1)
       --    RETURN
       --END
       
       
       SELECT @PayeeID = sl.ShopLocationID,
              @PayeeName = sl.Name,
              @PayeeAddress1 = sl.Address1,
              @PayeeAddressCity = sl.AddressCity,
              @PayeeAddressState = sl.AddressState,
              @PayeeAddressZip = sl.AddressZip
       FROM dbo.utb_document d
       LEFT JOIN dbo.utb_assignment a ON d.AssignmentID = a.AssignmentID
       LEFT JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
       WHERE d.DocumentID = @DocumentID
       
       IF @PayeeID IS NULL
       BEGIN
         -- Select the active assignment shop
          SELECT TOP 1 @PayeeID = sl.ShopLocationID,
                       @PayeeName = sl.Name,
                       @PayeeAddress1 = sl.Address1,
                       @PayeeAddressCity = sl.AddressCity,
                       @PayeeAddressState = sl.AddressState,
                       @PayeeAddressZip = sl.AddressZip
          FROM dbo.utb_document d
          LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
          LEFT JOIN dbo.utb_assignment a ON cascd.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
          LEFT JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
          WHERE d.DocumentID = @DocumentID AND a.assignmentsequencenumber = 1
            AND a.CancellationDate IS NULL
       END
    END
    
    IF @ApprovedFlag = 0
    BEGIN
      -- check if the payment was already invoiced.
      IF EXISTS(SELECT InvoiceID
                  FROM dbo.utb_invoice
                  WHERE DocumentID = @DocumentID
                    AND StatusCD = 'FS'
                    AND EnabledFlag = 1)
      BEGIN
         -- Payment was already invoices. Cannot unapprove this document.
         RAISERROR('1|Cannot Unapprove this document. Payment has already been invoiced.', 16, 1)
         RETURN
      END
    END
    
    BEGIN TRANSACTION
    
    -- mark the document as approved for early billing
    UPDATE dbo.utb_document
    SET ApprovedFlag = @ApprovedFlag,
        ApprovedDate = @now,
        ApprovedUserID = @UserID
    WHERE DocumentID = @DocumentID
    
    IF @@ERROR <> 0
    BEGIN
      -- SQL Server Error
        RAISERROR  ('104|%s|"utb_document"', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    IF @EarlyBillFlag = 1
    BEGIN
       IF @NetAmount > 0 AND 
          @ApprovedFlag = 1 AND 
          @PayeeID IS NOT NULL AND 
          @DispositionTypeCD not in ('CO', 'TL', 'ST') AND
          @CreatePaymentFlag = 1
       BEGIN
         -- check if a payment already exists in APD status
         IF NOT EXISTS(SELECT InvoiceID
                        FROM dbo.utb_invoice
                        WHERE ClaimAspectID = @ClaimAspectID
                          AND StatusCD = 'APD'
                          AND ItemTypeCD = 'I'
                          AND EnabledFlag = 1)
         BEGIN
			-- 15May2012 - TVD - Added RRP If test code
			IF (@vServiceChannel!='RRP')         
			BEGIN
				-- create a payment for the shop on this assignment
				EXEC uspInvoiceInsDetail @ClaimAspectID = @ClaimAspectID, 
										 @DocumentID = @DocumentID,
										 @Amount = @NetAmount, 
										 @DeductibleAmt = @DeductibleAmount, 
										 @TaxTotalAmt = @TaxTotalAmount, 
										 @Description = @InvoiceDescription, 
										 @ItemTypeCD = 'I',
										 @PayeeAddress1 = @PayeeAddress1, 
										 @PayeeAddress2 = NULL,
										 @PayeeAddressCity = @PayeeAddressCity, 
										 @PayeeAddressState = @PayeeAddressState, 
										 @PayeeAddressZip = @PayeeAddressZip, 
										 @PayeeID = @PayeeID,
										 @PayeeName = @PayeeName,
										 @PayeeTypeCD = 'S',
										 @PaymentChannelCD = NULL,
										 @SysLastUserID = @UserID

				 IF @@ERROR <> 0
				 BEGIN
				   -- SQL Server Error
					 RAISERROR  ('104|%s|"utb_invoice"', 16, 1, @ProcName)
					 ROLLBACK TRANSACTION
					 RETURN
				 END
			END
         END
       END
       
       
       IF @ApprovedFlag = 0
       BEGIN
          -- disable the payment that was auto created.
          UPDATE dbo.utb_invoice
          SET EnabledFlag = 0
          WHERE DocumentID = @DocumentID
            AND StatusCD = 'APD'

          IF @@ERROR <> 0
          BEGIN
            -- SQL Server Error
              RAISERROR  ('104|%s|"utb_document"', 16, 1, @ProcName)
              ROLLBACK TRANSACTION
              RETURN
          END
       END
    END


    COMMIT TRANSACTION
    
        
    --RETURN @rowcount
END

GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspApproveDocument' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspApproveDocument TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
