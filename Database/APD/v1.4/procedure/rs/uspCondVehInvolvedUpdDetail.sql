-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCondVehInvolvedUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCondVehInvolvedUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCondVehInvolvedUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     modified from uspVehicleInvolvedUpdDetail
*
* PARAMETERS:  
* (I) @ClaimAspectID                        The Claim aspect the Involved is linked to
* (I) @InvolvedID                           The Involved ID to update
* (I) @Address1                             Address Line 1  
* (I) @Address2                             Address Line 2  
* (I) @AddressCity                          City
* (I) @AddressState                         State
* (I) @AddressZip                           Zip
* (I) @AlternateAreaCode                    Alternate Phone Area Code
* (I) @AlternateExchangeNumber              Alternate Phone Exchange Number  
* (I) @AlternateExtensionNumber             Alternate Phone Extension Number  
* (I) @AlternateUnitNumber                  Alternate Phone Unit Number  
* (I) @BestContactPhoneCode                 Code indicating which is the best number (D, N, A)
* (I) @BestContactTime                      Best Time to Call
* (I) @BirthDate                            Date of Birth
* (I) @Age                                  Age
* (I) @BusinessName                         Business Name
* (I) @BusinessTypeCD                       Business Type Code
* (I) @DayAreaCode                          Day Phone Area Code 
* (I) @DayExchangeNumber                    Day Phone Exchange Number 
* (I) @DayExtensionNumber                   Day Phone Extension Number  
* (I) @DayUnitNumber                        Day Phone Unit Number  
* (I) @FedTaxID                             SSN or EIN Number
* (I) @GenderCD                             Sex of Involved
* (I) @InvolvedTypeList                     Comma delimited list of Involved Types
* (I) @NameFirst                            Involved's First Name  
* (I) @NameLast                             Involved's Last Name
* (I) @NameTitle                            Involved's Salutatuion Title (Mr, Mrs, etc)
* (I) @NightAreaCode                        Night Phone Area Code 
* (I) @NightExchangeNumber                  Night Phone Exchange Number 
* (I) @NightExtensionNumber                 Night Phone Extension Number 
* (I) @NightUnitNumber                      Night Phone Unit Number
* (I) @UserID                               The user updating the note
* (I) @SysLastUpdatedDate                   The "previous" updated date
*
* RESULT SET:       
*       An XML document containing last updated date time values for the Involved
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCondVehInvolvedUpdDetail
    @ClaimAspectID                      udt_std_id_big,
    @InvolvedID                         udt_std_int_big,
    @Address1                           udt_addr_line_1,
    @Address2                           udt_addr_line_2,
    @AddressCity                        udt_addr_city,
    @AddressState                       udt_addr_state,
    @AddressZip                         udt_addr_zip_code,
    @AlternateAreaCode                  udt_ph_area_code,
    @AlternateExchangeNumber            udt_ph_exchange_number,
    @AlternateExtensionNumber           udt_ph_extension_number,
    @AlternateUnitNumber                udt_ph_unit_number,
    @BestContactPhoneCD                 udt_std_cd,
    @BestContactTime                    udt_std_desc_short,
    @BirthDate                          udt_std_datetime,
    @Age                                udt_std_int_tiny,
    @BusinessName                       udt_std_name,
    @BusinessTypeCD                     udt_std_cd,
    @DayAreaCode                        udt_ph_area_code,
    @DayExchangeNumber                  udt_ph_exchange_number,
    @DayExtensionNumber                 udt_ph_extension_number,
    @DayUnitNumber                      udt_ph_unit_number,
    @EmailAddress                       udt_web_email,
    @FedTaxID                           udt_fed_tax_id,
    @GenderCD                           udt_std_cd,
    @InvolvedTypeList                   varchar(30),
    @NameFirst                          udt_per_name,
    @NameLast                           udt_per_name,
    @NameTitle                          udt_per_title,
    @NightAreaCode                      udt_ph_area_code,
    @NightExchangeNumber                udt_ph_exchange_number,
    @NightExtensionNumber               udt_ph_extension_number,
    @NightUnitNumber                    udt_ph_unit_number,
    @UserID                             udt_std_id,
    @SysLastUpdatedDate                 varchar(30)
AS
BEGIN

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@AlternateAreaCode))) = 0 SET @AlternateAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@AlternateExchangeNumber))) = 0 SET @AlternateExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@AlternateExtensionNumber))) = 0 SET @AlternateExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@AlternateUnitNumber))) = 0 SET @AlternateUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@BestContactPhoneCD))) = 0 SET @BestContactPhoneCD = NULL
    IF LEN(RTRIM(LTRIM(@BestContactTime))) = 0 SET @BestContactTime = NULL
    IF LEN(RTRIM(LTRIM(@Birthdate))) = 0 SET @Birthdate = NULL
    IF LEN(RTRIM(LTRIM(@Age))) = 0 or @Age = 0 SET @Age = NULL
    IF LEN(RTRIM(LTRIM(@BusinessName))) = 0 SET @BusinessName = NULL
    IF LEN(RTRIM(LTRIM(@BusinessTypeCD))) = 0 SET @BusinessTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@DayAreaCode))) = 0 SET @DayAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@DayExchangeNumber))) = 0 SET @DayExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@DayExtensionNumber))) = 0 SET @DayExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@DayUnitNumber))) = 0 SET @DayUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FedTaxID))) = 0 SET @FedTaxID = NULL
    IF LEN(RTRIM(LTRIM(@GenderCD))) = 0 SET @GenderCD = NULL
    IF LEN(RTRIM(LTRIM(@InvolvedTypeList))) = 0 SET @InvolvedTypeList = NULL
    IF LEN(RTRIM(LTRIM(@NameFirst))) = 0 SET @NameFirst = NULL
    IF LEN(RTRIM(LTRIM(@NameLast))) = 0 SET @NameLast = NULL
    IF LEN(RTRIM(LTRIM(@NameTitle))) = 0 SET @NameTitle = NULL
    IF LEN(RTRIM(LTRIM(@NightAreaCode))) = 0 SET @NightAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@NightExchangeNumber))) = 0 SET @NightExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@NightExtensionNumber))) = 0 SET @NightExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@NightUnitNumber))) = 0 SET @NightUnitNumber = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now                AS datetime
    DECLARE @Gender             AS udt_std_cd

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCondVehInvolvedUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_vehicle WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate the updated date parameter

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_involved', @InvolvedID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END

 
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    

    -- Default the gender for any records we insert to unknown

    SET @Gender = 'U'

    -- Begin Update

    BEGIN TRANSACTION VehicleInvolvedUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    UPDATE  dbo.utb_involved
    SET Address1                            = @Address1,
        Address2                            = @Address2,
        AddressCity                         = @AddressCity,
        AddressState                        = @AddressState,
        AddressZip                          = @AddressZip,
        AlternateAreaCode                   = @AlternateAreaCode,
        AlternateExchangeNumber             = @AlternateExchangeNumber,
        AlternateExtensionNumber            = @AlternateExtensionNumber,   
        AlternateUnitNumber                 = @AlternateUnitNumber,
        BestContactPhoneCD                  = @BestContactPhoneCD,
        BestContactTime                     = @BestContactTime,
        BirthDate                           = @BirthDate,
        Age                                 = @Age,
        BusinessName                        = @BusinessName,
        BusinessTypeCD                      = @BusinessTypeCD,
        DayAreaCode                         = @DayAreaCode,
        DayExchangeNumber                   = @DayExchangeNumber,
        DayExtensionNumber                  = @DayExtensionNumber,
        DayUnitNumber                       = @DayUnitNumber,
        EmailAddress                        = @EmailAddress,
        FedTaxID                            = @FedTaxID,
        GenderCD                            = @GenderCD,
        NameFirst                           = @NameFirst,
        NameLast                            = @NameLast,
        NameTitle                           = @NameTitle,
        NightAreaCode                       = @NightAreaCode,
        NightExchangeNumber                 = @NightExchangeNumber,
        NightExtensionNumber                = @NightExtensionNumber,
        NightUnitNumber                     = @NightUnitNumber,
        SysLastUserID                       = @UserID,
        SysLastUpdatedDate                  = @now
    WHERE 
        InvolvedID = @InvolvedID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
  
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_involved', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END




    -- Update the Involved Roles.  This is done by deleting the existing Involved Role records and inserting new ones.

    DELETE FROM dbo.utb_involved_role
       WHERE
        InvolvedID = @InvolvedID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('106|%s|utb_involved_role', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    


    INSERT INTO dbo.utb_involved_role
       (
        InvolvedID,
        InvolvedRoleTypeID,
        SysLastUserID,
        SysLastUpdatedDate
       )
       SELECT
        @InvolvedID,
        value,
        @UserID,
        @now
       FROM dbo.ufnUtilityParseString( @InvolvedTypeList, ',', 1 )     -- 1 means to trim spaces
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
        
        RAISERROR('105|%s|utb_involved_role', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    
    
    
    COMMIT TRANSACTION VehicleInvolvedUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [NewVehicleInvolved!2!InvolvedID],
            NULL AS [NewVehicleInvolved!2!SysLastUpdatedDate],
            -- Involved List
            NULL AS [VehicleInvolved!3!InvolvedID],
            NULL AS [VehicleInvolved!3!NameFirst],
            NULL AS [VehicleInvolved!3!NameLast],
            NULL AS [VehicleInvolved!3!BusinessName],
            NULL AS [VehicleInvolved!3!DayAreaCode],
            NULL AS [VehicleInvolved!3!DayExchangeNumber],
            NULL AS [VehicleInvolved!3!AddressCity],
            NULL AS [VehicleInvolved!3!AddressState],
            NULL AS [VehicleInvolved!3!AddressZip],
            -- Involved Type
            NULL AS [VehicleInvolvedType!4!InvolvedTypeName]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @InvolvedID,
            dbo.ufnUtilityGetDateString( @now ),
            -- Involved List
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL


    UNION ALL


    SELECT  3,
            1,
            NULL,
            -- New Involved Level
            NULL, NULL,
            -- Involved List
            IsNull(i.InvolvedID, ''),
            IsNull(i.NameFirst, ''),
            IsNull(i.NameLast, ''),
            IsNull(i.BusinessName, ''),
            IsNull(i.DayAreaCode, ''),
            IsNull(i.DayExchangeNumber, ''),
            IsNull(i.AddressCity, ''),
            IsNull(i.AddressState, ''),
            IsNull(i.AddressZip, ''),
            -- Involved Type
            NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)


    UNION ALL


    SELECT  4,
            3,
            NULL,
            -- New Involved Level
            NULL, NULL,
            -- Involved List
            IsNull(cai.InvolvedID, ''),
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            IsNull(irt.Name, '')

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    
    ORDER BY [VehicleInvolved!3!InvolvedID], tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCondVehInvolvedUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCondVehInvolvedUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/