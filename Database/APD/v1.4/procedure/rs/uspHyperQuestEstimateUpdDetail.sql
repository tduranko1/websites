-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperQuestEstimateUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperQuestEstimateUpdDetail 
END

GO
/****** Object:  StoredProcedure [dbo].[uspEstimateUpdDetail]    Script Date: 01/12/2015 08:15:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspHyperQuestEstimateUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Updates Estimate header record (creates one if one doesn't already exist).
*
* PARAMETERS:  
* (I) @DocumentID                   DocumentID of the Estimate
* (I) @AppraiserLicenseNumber       Appraiser License Number
* (I) @AppraiserName                ummmm....
* (I) @ClaimantName                 "
* (I) @ClaimNumber                  Insurance Companie's claim number
* (I) @CompanyAddress               Appraiser's Company Address
* (I) @CompanyAreaCode              Appraiser's Company Area Code
* (I) @CompanyCity                  Appraiser's Company City
* (I) @CompanyExchangeNumber        Appraiser's Company Exchange number
* (I) @CompanyName                  Appraiser's Company Name
* (I) @CompanyState                 Appraiser's Company State
* (I) @CompanyUnitNumber            Appraiser's Company Unit
* (I) @CompanyZip                   Appraiser's Company Zip Code
* (I) @DetailEditableFlag           Indicates whether the detail section will be editable
* (I) @InspectionAddress            Address of location inspection was performed at
* (I) @InspectionAreaCode           Area Code of location inspection was performed at
* (I) @InspectionCity               City of location inspection was performed at
* (I) @InspectionExchangeNumber     Phone Exchange Number of location inspection was performed at
* (I) @InspectionLocation           Location inspection was performed at
* (I) @InspectionState              (Don't you love cut 'n paste)
* (I) @InspectionType               Type of Inspection 
* (I) @InspectionUnitNumber         Phone Unit Number of location inspection was performed at
* (I) @InspectionZip                Zip of location inspection was performed at
* (I) @InsuranceCompanyName         Name of Insurance Company
* (I) @InsuredName                  Insured's Name
* (I) @LossDate                     Date of Loss
* (I) @LossType                     Type of Loss
* (I) @OwnerAddress                 Address of owner of vehicle
* (I) @OwnerCity                    City of owner of vehicle
* (I) @OwnerDayAreaCode             Day Area Code of owner of vehicle
* (I) @OwnerDayExchangeNumber       Day Exchange number of owner of vehicle
* (I) @OwnerDayExtensionNumber      Day Extension number of owner of vehicle
* (I) @OwnerDayUnitNumber           (singing I love cut 'n paste)
* (I) @OwnerEveningAreaCode         (So put another string in the clipboard, baby)
* (I) @OwnerEveningExchangeNumber   Evening Exchange number of owner of vehicle
* (I) @OwnerEveningExtensionNumber  Evening Extension number of owner of vehicle
* (I) @OwnerEveningUnitNumber       Evening unit number of owner of vehicle
* (I) @OwnerName                    Name of owner of vehicle
* (I) @OwnerState                   State of owner of vehicle
* (I) @OwnerZip                     Zip of owner of vehicle
* (I) @PolicyNumber                 Insured's policy number
* (I) @Remarks                      Comments
* (I) @RepairDays                   Number of days to repair vehicle
* (I) @ShopAddress                  I don't really need to explain this, do I?
* (I) @ShopAreaCode                 See Above
* (I) @ShopCity                     See Above
* (I) @ShopExchangeNumber           See Above
* (I) @ShopName                     See Above
* (I) @ShopRegistrationNumber       See Above
* (I) @ShopState                    See Above
* (I) @ShopUnitNumber               See Above
* (I) @ShopZip                      See Above
* (I) @VehicleBodyStyle             What kind of vehicle
* (I) @VehicleColor                 and it's color?
* (I) @VehicleEngineType            muscle car or grocery getter?
* (I) @VehicleImpactPoint           ummmm.
* (I) @VehicleMake                  Who makes the vehicle
* (I) @VehicleMileage               How many miles does the vehicle have?
* (I) @VehicleModel                 model?
* (I) @VehicleOptions               list of factory options
* (I) @VehicleYear                  what year was it made?
* (I) @VIN                          and lastly, it's VIN (Vehicle Identification Number)
* (I) @WrittenDate                  Date that the estimate was written
* (I) @UserID                       user making the changes.  Use Either this or @userName.
* (I) @UserName                     NT Name of user making changes.  Use Either this or @userID.
* (I) @SysLastUpdatedDate           Last updated date
*
* RESULT SET:
* xml formatted recordset (ready for client side processing) containing the new SysLastUpdatedDate
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspHyperQuestEstimateUpdDetail]
     @sDocumentID                    udt_std_id_big,
     @sAppraiserLicenseNumber        udt_std_desc_short      = Null,
     @sAppraiserName                 udt_per_name            = Null,
     @sClaimantName                  udt_per_name            = Null,
     @sClaimNumber                   udt_cov_claim_number    = Null,
     @sCompanyAddress                udt_addr_line_1         = Null,
     @sCompanyAreaCode               udt_ph_area_code        = Null,
     @sCompanyCity                   udt_addr_city           = Null,
     @sCompanyExchangeNumber         udt_ph_exchange_number  = Null,
     @sCompanyName                   udt_std_name            = Null,
     @sCompanyState                  udt_addr_state          = Null,
     @sCompanyUnitNumber             udt_ph_unit_number      = Null,
     @sCompanyZip                    udt_addr_zip_code       = Null,
     @sDetailEditableFlag            udt_std_flag            = 0,
     @sInspectionAddress             udt_addr_line_1         = Null,
     @sInspectionAreaCode            udt_ph_area_code        = Null,
     @sInspectionCity                udt_addr_city           = Null,
     @sInspectionExchangeNumber      udt_ph_exchange_number  = Null,
     @sInspectionLocation            udt_std_name            = Null,
     @sInspectionState               udt_addr_state          = Null,
     @sInspectionType                udt_std_name            = Null,
     @sInspectionUnitNumber          udt_ph_unit_number      = Null,
     @sInspectionZip                 udt_addr_zip_code       = Null,
     @sInsuranceCompanyName          udt_std_name            = Null,
     @sInsuredName                   udt_per_name            = Null,
     @sLossDate                      varchar(30)             = Null,
     @sLossType                      udt_std_desc_short      = Null,
     @sOwnerAddress                  udt_addr_line_1         = Null,
     @sOwnerCity                     udt_addr_city           = Null,
     @sOwnerDayAreaCode              udt_ph_area_code        = Null,
     @sOwnerDayExchangeNumber        udt_ph_exchange_number  = Null,
     @sOwnerDayExtensionNumber       udt_ph_extension_number = Null,
     @sOwnerDayUnitNumber            udt_ph_unit_number      = Null,
     @sOwnerEveningAreaCode          udt_ph_area_code        = Null,
     @sOwnerEveningExchangeNumber    udt_ph_exchange_number  = Null,
     @sOwnerEveningExtensionNumber   udt_ph_extension_number = Null,
     @sOwnerEveningUnitNumber        udt_ph_unit_number      = Null,
     @sOwnerName                     udt_per_name            = Null,
     @sOwnerState                    udt_addr_state          = Null,
     @sOwnerZip                      udt_addr_zip_code       = Null,
     @sPolicyNumber                  udt_cov_policy_number   = Null,
     @sRemarks                       udt_std_desc_long       = Null,
     @sRepairDays                    udt_std_int_small       = Null,
     @sShopAddress                   udt_addr_line_1         = Null,
     @sShopAreaCode                  udt_ph_area_code        = Null,
     @sShopCity                      udt_addr_city           = Null,
     @sShopExchangeNumber            udt_ph_exchange_number  = Null,
     @sShopName                      udt_std_name            = Null,
     @sShopRegistrationNumber        udt_std_desc_short      = Null,
     @sShopState                     udt_addr_state          = Null,
     @sShopUnitNumber                udt_ph_unit_number      = Null,
     @sShopZip                       udt_addr_zip_code       = Null,
     @sVehicleBodyStyle              udt_auto_body           = Null,
     @sVehicleColor                  udt_auto_color          = Null,
     @sVehicleEngineType             udt_auto_engine         = Null,
     @sVehicleImpactPoint            udt_std_desc_long       = Null,
     @sVehicleMake                   udt_auto_make           = Null,
     @sVehicleMileage                udt_auto_mileage        = Null,
     @sVehicleModel                  udt_auto_model          = Null,
     @sVehicleOptions                udt_std_note            = Null,
     @sVehicleYear                   udt_dt_year             = Null,
     @sVIN                           udt_auto_vin            = Null,
     @sWrittenDate                   varchar(30)             = Null,
     @sUserID                        udt_std_id              = Null,
     @sUserName                      udt_std_desc_short      = Null,
     @sSysLastUpdatedDate            varchar(30)             = Null,
     @sApplicationCD                 udt_std_cd              = 'APD'
AS
BEGIN
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts
    DECLARE @Now                AS Datetime

    SET @ProcName = 'uspHyperQuestEstimateUpdDetail'
    SET @Now = current_timestamp
    
 BEGIN TRANSACTION EstimateDetailUpd
    
    EXEC uspEstimateUpdDetail @DocumentID = @sDocumentID,                  
@AppraiserLicenseNumber			=	@sAppraiserLicenseNumber,
@AppraiserName					=	@sAppraiserName,               
@ClaimantName					=	@sClaimantName,                
@ClaimNumber					=	@sClaimNumber,                 
@CompanyAddress					=	@sCompanyAddress,              
@CompanyAreaCode				=	@sCompanyAreaCode,             
@CompanyCity					=	@sCompanyCity,                 
@CompanyExchangeNumber			=	@sCompanyExchangeNumber,       
@CompanyName					=	@sCompanyName,                 
@CompanyState					=	@sCompanyState,                
@CompanyUnitNumber				=	@sCompanyUnitNumber,           
@CompanyZip						=	@sCompanyZip,                  
@DetailEditableFlag				=	@sDetailEditableFlag,          
@InspectionAddress				=	@sInspectionAddress,           
@InspectionAreaCode				=	@sInspectionAreaCode,          
@InspectionCity					=	@sInspectionCity,              
@InspectionExchangeNumber		=	@sInspectionExchangeNumber,    
@InspectionLocation				=	@sInspectionLocation,          
@InspectionState				=	@sInspectionState,             
@InspectionType					=	@sInspectionType,              
@InspectionUnitNumber			=	@sInspectionUnitNumber,        
@InspectionZip					=	@sInspectionZip,               
@InsuranceCompanyName			=	@sInsuranceCompanyName,        
@InsuredName					=	@sInsuredName,                 
@LossDate						=	@sLossDate,                    
@LossType						=	@sLossType,                    
@OwnerAddress					=	@sOwnerAddress,                
@OwnerCity						=	@sOwnerCity,                   
@OwnerDayAreaCode				=	@sOwnerDayAreaCode,            
@OwnerDayExchangeNumber			=	@sOwnerDayExchangeNumber,      
@OwnerDayExtensionNumber		=	@sOwnerDayExtensionNumber,     
@OwnerDayUnitNumber				=	@sOwnerDayUnitNumber,          
@OwnerEveningAreaCode			=	@sOwnerEveningAreaCode,        
@OwnerEveningExchangeNumber		=	@sOwnerEveningExchangeNumber, 
@OwnerEveningExtensionNumber		=	@sOwnerEveningExtensionNumber, 
@OwnerEveningUnitNumber			=	@sOwnerEveningUnitNumber,      
@OwnerName						=	@sOwnerName,                   
@OwnerState						=	@sOwnerState,                  
@OwnerZip						=	@sOwnerZip,                    
@PolicyNumber					=	@sPolicyNumber,                
@Remarks						=	@sRemarks,                     
@RepairDays						=	@sRepairDays,                  
@ShopAddress					=	@sShopAddress,                 
@ShopAreaCode					=	@sShopAreaCode,                
@ShopCity						=	@sShopCity,                    
@ShopExchangeNumber				=	@sShopExchangeNumber,          
@ShopName						=	@sShopName,                    
@ShopRegistrationNumber			=	@sShopRegistrationNumber,      
@ShopState						=	@sShopState,                   
@ShopUnitNumber					=	@sShopUnitNumber,              
@ShopZip						=	@sShopZip,                     
@VehicleBodyStyle				=	@sVehicleBodyStyle,            
@VehicleColor					=	@sVehicleColor,                
@VehicleEngineType				=	@sVehicleEngineType,           
@VehicleImpactPoint				=	@sVehicleImpactPoint,          
@VehicleMake					=	@sVehicleMake,                 
@VehicleMileage					=	@sVehicleMileage,              
@VehicleModel					=	@sVehicleModel,                
@VehicleOptions					=	@sVehicleOptions,             
@VehicleYear					=	@sVehicleYear,                 
@VIN							=	@sVIN,                         
@WrittenDate					=	@sWrittenDate,                 
@UserID							=	@sUserID,                      
@UserName						=	@sUserName,                    
@SysLastUpdatedDate				=	@sSysLastUpdatedDate,          
@ApplicationCD					=	@sApplicationCD          

COMMIT TRANSACTION EstimateDetailUpd

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END   
    
    SELECT @Now as SysLastUpdatedDate
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperQuestEstimateUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperQuestEstimateUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
