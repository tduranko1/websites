-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberSourceAppGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimNumberSourceAppGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimNumberSourceAppGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will return the vehicle list for the client claim number
*
* PARAMETERS:  
* (I) @ClientClaimNumber    Client Claim Number
* (I) @InsuranceCompanyID   Insurance Company ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimNumberSourceAppGetDetailXML
    @ClientClaimNumber      udt_cov_claim_number,
    @InsuranceCompanyID     udt_std_id,
    @SourceApplicationID    udt_std_id
AS
BEGIN
    -- Set database options

    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables

    DECLARE @LynxID                     udt_std_id_big
    DECLARE @InsuranceCompanyIDClaim    udt_std_id
    DECLARE @ClaimAspectTypeID          udt_std_id

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimNumberSourceAppGetDetailXML'

    SELECT @LynxID = c.LynxID
    FROM dbo.utb_claim_aspect ca
    INNER JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    --LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID) --Project:210474 APD The column ClientClaimNumber is part of utb_Claim, removed the reference to the table utb_Claim_Coverage becuase it is not needed, when we did the code merge M.A.20061211
    WHERE c.ClientClaimNumber = @ClientClaimNumber
      AND c.InsuranceCompanyID = @InsuranceCompanyID
      AND ca.SourceApplicationID = 6

    
    
    -- Get the aspect type id for property for use later
    
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'
      
    -- Begin XML Select

    SELECT
        1 as Tag,
        0 as Parent,
--        Root
        @LynxID as [Root!1!LynxID],
--        VehicleList
        Null as [Vehicle!2!VehicleNumber],
        Null as [Vehicle!2!ClaimAspectID],
        Null as [Vehicle!2!VehicleYear],
        Null as [Vehicle!2!Make],
        Null as [Vehicle!2!Model],
        Null as [Vehicle!2!NameFirst],
        Null as [Vehicle!2!NameLast],
        Null as [Vehicle!2!BusinessName],
        Null as [Vehicle!2!ClosedStatus],
        Null as [Vehicle!2!Status],
        Null as [Vehicle!2!StatusID],
        Null as [Vehicle!2!ExposureCD],
        Null as [Vehicle!2!CoverageProfileCD],
        Null as [Vehicle!2!CurrentAssignmentType]
        

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null,
--        Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.Name,
        vs.StatusID,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, ''),
        --Project:210474 APD Remarked-off the following to support the schema change M.A.20061219
        --(SELECT Name FROM dbo.utb_assignment_type cat WHERE cat.AssignmentTypeID = ca.CurrentAssignmentTypeID)
        --Project:210474 APD Added the following to support the schema change M.A.20061219
        (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD)
    FROM
        (SELECT @LynxID AS LynxID, @ClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID AND 1 = ca.EnabledFlag)
        LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
		LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID  --Project:210474 APD Added reference to the table to provide the join between utb_Claim_Aspect & utb_Status, when we did the code merge M.A.20061211
        LEFT JOIN dbo.utb_status vs ON (cas.StatusID = vs.StatusID )
        left outer join utb_Claim_Aspect_Service_Channel casc
        on    ca.ClaimAspectID = casc.ClaimAspectID
        and    casc.PrimaryFlag = 1

    Order by [Vehicle!2!VehicleNumber], tag
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberSourceAppGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimNumberSourceAppGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/