-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFQBEMemoBillGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFQBEMemoBillGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCFQBEMemoBillGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Stored procedure to retrieve Custom Forms as XML
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCFQBEMemoBillGetXML
    @ClaimAspectID      udt_std_id_big,
    @UserID				udt_std_id_big
AS
BEGIN
    -- Declare internal variables
    DECLARE @LynxID				as VARCHAR(25)
    DECLARE @CarrierRepName		as VARCHAR(50)
	DECLARE @Address1			as VARCHAR(100)
	DECLARE @Address2			as VARCHAR(100)
	DECLARE @Address3			as VARCHAR(100)
	DECLARE @ClaimNumber		as VARCHAR(100)
	DECLARE @PolicyNumber		as VARCHAR(100)
	DECLARE @LossDate			as VARCHAR(25)
	DECLARE @ClaimantName		as VARCHAR(100)
	DECLARE @BusinessName		as VARCHAR(50)
	DECLARE @Vehicle			as VARCHAR(100)
	DECLARE @TotalFees			as VARCHAR(10)
	DECLARE @MemoBillDate		as VARCHAR(25)
	DECLARE @MemoBillNum		as VARCHAR(50)
	DECLARE @SvcChannel			as VARCHAR(50)
    DECLARE @ProcName           as VARCHAR(30)       -- Used for raise error stmts 


    SET @ProcName = 'uspCFQBEMemoBillGetXML'
    
    
	SELECT 
		@LynxID = utb_claim.LynxID,
		@CarrierRepName = (utb_user.NameFirst + ' ' + utb_user.NameLast),
		@Address1 = utb_office.Address1,
		@Address2 = CASE
			WHEN IsNull(utb_office.Address2,'') = '' THEN utb_office.AddressCity + ' ' + utb_office.AddressState + ' ' + utb_office.AddressZip
			ELSE utb_office.Address2
		END,
		@Address3 = CASE
			WHEN IsNull(utb_office.Address2,'') = '' THEN ''
			ELSE utb_office.AddressCity + ' ' + utb_office.AddressState + ' ' + utb_office.AddressZip
		END,
		@ClaimNumber = utb_claim.ClientClaimNumber,
		@PolicyNumber = utb_claim.PolicyNumber,
		@LossDate = Convert(varchar, utb_claim.LossDate, 101),
		@ClaimantName = LTrim(RTrim(IsNull(utb_involved.NameFirst,'') + ' ' + IsNull(utb_involved.NameLast,''))),
		@BusinessName = utb_involved.BusinessName,
		@Vehicle = LTrim(RTrim(Cast(IsNull(utb_claim_vehicle.VehicleYear,'') as varchar) + ' ' + LTrim(RTrim(IsNull(utb_claim_vehicle.Make,''))) + ' ' + LTrim(RTrim(IsNull(utb_claim_vehicle.Model,''))))),
		@MemoBillDate = Convert(varchar, current_timestamp, 101),
		@MemoBillNum = Cast(utb_claim.LynxID as varchar) + '-' + Cast(utb_claim_aspect.ClaimAspectNumber as varchar) + '-MB'
	FROM utb_claim
	INNER JOIN utb_claim_aspect
		ON utb_claim.LynxID = utb_claim_aspect.LynxID
	LEFT OUTER JOIN utb_claim_vehicle
		ON utb_claim_aspect.ClaimAspectID = utb_claim_vehicle.ClaimAspectID
	LEFT OUTER JOIN utb_user
		ON utb_claim.CarrierRepUserID = utb_user.UserID
	LEFT OUTER JOIN utb_office
		ON utb_user.OfficeID = utb_office.OfficeID
	LEFT OUTER JOIN utb_claim_aspect_involved
		ON utb_claim_aspect.ClaimAspectID = utb_claim_aspect_involved.ClaimAspectID
	LEFT OUTER JOIN utb_involved
		ON utb_claim_aspect_involved.InvolvedID = utb_involved.InvolvedID
	WHERE utb_claim_aspect.ClaimAspectID = @ClaimAspectID



	--SELECT
	--	@SvcChannel = CASE 
	--		WHEN ServiceChannelCD = 'DA' THEN 'Desk Audit'
	--		WHEN ServiceChannelCD = 'RRP' THEN 'Repair Referral'
	--		WHEN ServiceChannelCD = 'PS' THEN 'Program Shop'
	--		ELSE 'N/A'
	--	END
	--FROM utb_claim_aspect_service_channel
	--WHERE ClaimAspectID = @ClaimAspectID
	
	SELECT @SvcChannel = Invoicedescription FROM utb_invoice 
inner join 
(
SELECT TOP 1 * FROM utb_invoice_service WHERE ClaimAspectID = @claimaspectid ORDER BY syslastupdatedDate desc ) serv
ON utb_invoice.invoiceid = serv.invoiceid 
WHERE serv.ClaimAspectID = @claimaspectid


	SELECT  @TotalFees = Cast(IsNull(SUM(Amount),0) as VARCHAR)
	FROM utb_invoice 
	
	WHERE ClaimAspectID = @ClaimAspectID and Enabledflag = 1



    SELECT  1 as Tag,
            NULL as Parent,
			IsNull(@LynxID,'') as [Root!1!LynxID],
			IsNull(@CarrierRepName,'') as [Root!1!CarrierRepName],
			IsNull(@Address1,'') as [Root!1!Address1],
			IsNull(@Address2,'') as [Root!1!Address2],
			IsNull(@Address3,'') as [Root!1!Address3],
			IsNull(@ClaimNumber,'') as [Root!1!ClaimNumber],
			IsNull(@PolicyNumber,'') as [Root!1!PolicyNumber],
			IsNull(@LossDate,'') as [Root!1!LossDate],
			IsNull(@ClaimantName,'') as [Root!1!ClaimantName],
			IsNull(@BusinessName,'') as [Root!1!BusinessName],
			IsNull(@Vehicle,'') as [Root!1!Vehicle],
			IsNull(@TotalFees,'') as [Root!1!TotalFees],
			IsNull(@MemoBillDate,'') as [Root!1!MemoBillDate],
			IsNull(@MemoBillNum,'') as [Root!1!MemoBillNum],
			IsNull(@SvcChannel,'N/A') as [Root!1!SvcChannel]


    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFQBEMemoBillGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFQBEMemoBillGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/


