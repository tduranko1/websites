-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminLaborRatesGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminLaborRatesGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the Labor Rates
*
* PARAMETERS:  
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminLaborRatesGetXML
    @InsuranceCompanyID int
AS
BEGIN


   SELECT   1 AS Tag,
            NULL AS Parent,
            -- Root
            isNull(@InsuranceCompanyID, '')  as [Root!1!InsuranceCompanyID],
            -- Labor Rate Details
            NULL AS [LaborRate!2!LaborRateID],
            NULL AS [LaborRate!2!City],
            NULL AS [LaborRate!2!County],
            NULL AS [LaborRate!2!StateCode],
            NULL AS [LaborRate!2!AgreedPriceVariance],
            NULL AS [LaborRate!2!AuthorizedBy],
            NULL AS [LaborRate!2!AuthorizationDate],
            NULL AS [LaborRate!2!BodyRateMin],
            NULL AS [LaborRate!2!BodyRateMax],
            NULL AS [LaborRate!2!FrameRateMin],
            NULL AS [LaborRate!2!FrameRateMax],
            NULL AS [LaborRate!2!MaterialRateMin],
            NULL AS [LaborRate!2!MaterialRateMax],
            NULL AS [LaborRate!2!MechRateMin],
            NULL AS [LaborRate!2!MechRateMax],
            NULL AS [LaborRate!2!RefinishRateMin],
            NULL AS [LaborRate!2!RefinishRateMax],
            NULL AS [LaborRate!2!LaborTax],
            NULL AS [LaborRate!2!PartsTax],
            NULL AS [LaborRate!2!MaterialsTax]

   UNION ALL

   SELECT   2,
            1,
            -- Root
            NULL,
            -- Labor Rate Details
            LaborRateID,
            isNull(City, ''),
            isNull(County, ''),
            isNull(StateCode, ''),
            AgreedPriceVariance,
            u.NameFirst + ' ' + u.NameLast,
            convert(varchar, AuthorizationDate, 101),
            BodyRateMin,
            BodyRateMax,
            FrameRateMin,
            FrameRateMax,
            MaterialRateMin,
            MaterialRateMax,
            MechRateMin,
            MechRateMax,
            RefinishRateMin,
            RefinishRateMax,
            LaborTax,
            PartsTax,
            MaterialsTax
   FROM utb_labor_rate lr
   LEFT JOIN utb_user u on lr.AuthorizedBy = u.UserID
   Where InsuranceCompanyID = @InsuranceCompanyID

   ORDER BY [LaborRate!2!StateCode], [LaborRate!2!County], [LaborRate!2!City], Tag        

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminLaborRatesGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
