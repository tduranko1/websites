-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDAuditGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPMDAuditGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspPMDAuditGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves list of shops with assignments for a given Program Manager
*
* PARAMETERS:  
*   @ShopLocationID     ID of program shop location
*   @DateRange          integer value for number of days 
*   @AssignmentCode     udt_std_cd          
*
* RESULT SET:
* List assignments in given Program Manager's area of responsibility.
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspPMDAuditGetDetailXML
    @ShopLocationID     udt_std_id,
    @BeginDate          udt_std_DateTime = Null,
    @EndDate            udt_std_DateTime = Null,
    @AssignmentCode     udt_std_cd = 'B',
    @UserID             udt_std_id
AS
BEGIN
    DECLARE @ProcName                           udt_std_name 
    DECLARE @Now                                udt_std_datetime
    DECLARE @StatRangeBeginDate                 udt_std_datetime
    DECLARE @VehicleClosedStatusID              udt_std_int
    DECLARE @ViewCRD                            udt_std_flag
    DECLARE @ViewSMD                            udt_std_flag

    --Vars 
    DECLARE @MaxDateRange           as Int
    DECLARE @DayAdded               as Bit  
    DECLARE @BeforeDriveCount       as Int      -- Number of RIs conducted on drivable vehicles before repairs began
    DECLARE @BeforeDriveSat         as Float    -- Average Percent Satisfactory score for drivable RIs before repairs
    DECLARE @BeforeNonDriveCount    as Int      -- Number of RIs conducted on non-drivable vehicles before repairs began
    DECLARE @BeforeNonDriveSat      as Float    -- Average Percent Satisfactory score for non-drivable RIs before repairs
    DECLARE @DuringDriveCount       as Int      -- Number of RIs conducted on drivable vehicles during repairs 
    DECLARE @DuringDriveSat         as Float    -- Average Percent Satisfactory score for drivable RIs during repairs
    DECLARE @DuringNonDriveCount    as Int      -- Number of RIs conducted on non-drivable vehicles during repairs 
    DECLARE @DuringNonDriveSat      as Float    -- Average Percent Satisfactory score for non-drivable RIs during repairs
    DECLARE @AfterDriveCount        as Int      -- Number of RIs conducted on drivable vehicles after repairs 
    DECLARE @AfterDriveSat          as Float    -- Average Percent Satisfactory score for drivable RIs after repairs
    DECLARE @AfterNonDriveCount     as Int      -- Number of RIs conducted on non-drivable vehicles after repairs 
    DECLARE @AfterNonDriveSat       as Float    -- Average Percent Satisfactory score for non-drivable RIs after repairs
    DECLARE @SupplDriveCount        as Int      -- Number of supplemental RIs conducted on drivable vehicles
    DECLARE @SupplDriveSat          as Float    -- Average Percent Satisfactory score for drivable supplemental RIs
    DECLARE @SupplNonDriveCount     as Int      -- Number of supplementalRIs conducted on non-drivable vehicles
    DECLARE @SupplNonDriveSat       as Float    -- Average Percent Satisfactory score for non-drivable supplemental RIs
    
    DECLARE @ClaimAspectCode        as varchar(8)
    
    SET @ProcName = 'uspPMDAuditGetDetailXML'
    SET @Now = current_timestamp

    --Default Date Range
    Declare @DefaultStatRange as Int
    Declare @DefaultAssignmentRange as Int
    

    SET @DefaultStatRange = (SELECT CONVERT(int, Value) FROM dbo.utb_app_variable WHERE Name = 'PMD_Default_Stat_Range')
    SET @DefaultAssignmentRange = (SELECT CONVERT(int, Value) FROM dbo.utb_app_variable WHERE Name = 'PMD_Default_Assignment_Range')
    SET @VehicleClosedStatusID = (SELECT StatusID FROM dbo.utb_status WHERE Name = 'Vehicle Closed')
    --Validate Parameters
    --As of now, there won't be a max date range.
--     set @MaxDateRange = (Select Value FROM dbo.utb_app_variable WHERE Name = 'Max_Program_Manager_Desktop_Date_Range')
--     IF @MaxDateRange < @DateRange and @MaxDateRange is not Null
--     BEGIN
-- 
--         RAISERROR  ('1|Error with date range - exceeds maximum value.', 16, 1, @ProcName)
--         RETURN
--     END
    
    SET @StatRangeBeginDate = DateAdd (day, @DefaultStatRange * -1, @Now)

    IF (@ShopLocationID IS NULL) OR (NOT EXISTS (SELECT ShopLocationID FROM dbo.utb_shop_location 
                                                                    WHERE ShopLocationID = @ShopLocationID))
    BEGIN
      -- Invalid Shop Location ID 
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END
    
        
    IF (@UserID IS NULL) OR (NOT EXISTS (SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
      -- Invalid Shop Location ID 
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END    

    SET @ViewCRD = (SELECT ReadFlag FROM dbo.ufnUtilityGetCRUD(@UserID, 'Action:View Claim Rep Desktop', 0, 0))
    SET @ViewSMD = (SELECT ReadFlag FROM dbo.ufnUtilityGetCRUD(@UserID, 'Action:View Shop Maintenance Desktop', 0, 0))


    IF upper(@AssignmentCode) <> 'O' AND upper(@AssignmentCode) <> 'C' AND upper(@AssignmentCode) <> 'B'
    BEGIN
       -- Missing Assignment Code
        RAISERROR('101|%s|@AssignmentCode|%s', 16, 1, @ProcName, @AssignmentCode)
        RETURN
    END

    IF @EndDate is Null
    BEGIN
        SET @EndDate = GetDate()
    END
    BEGIN
        -- Add 1 day to @EndDate to prevent default 00:00:00:000 time from effectively excluding that day from the search
        SET @EndDate = DateAdd (day, 1, @EndDate)   
        SET @DayAdded = 1
    END

    IF @BeginDate is Null
    BEGIN
        IF @DayAdded = 1
        BEGIN
            SET @BeginDate = DateAdd (day, (@DefaultAssignmentRange + 1) * -1, @EndDate)  
        END
        ELSE
        BEGIN
            SET @BeginDate = DateAdd (day, @DefaultAssignmentRange * -1, @EndDate)
        END
    END

    IF @Enddate is Null or @BeginDate is Null
    BEGIN
        DECLARE @errMessage varchar(50)
        SET @errMessage = (select(convert(varchar(20),@BeginDate) + '/' + 
                convert(varchar(20),@EndDate) + '/' + 
                convert(varchar(20),@DefaultAssignmentRange)))
        RAISERROR('101|%s|@BeginDate/@EndDate/@DefaultAssignmentRange|%s', 16, 1, @ProcName, 
            @errMessage)
        RETURN
        RAISERROR  ('%s: Could not set Date Range.', 16, 1, @ProcName)
        RETURN
    END
/*************************************************************************************
*  Gather Reference Data
**************************************************************************************/
--Look, I know that it's a little wierd doing the assignment codes like this, but they
--aren't stored in a table, and the front end can and will make choices based on this.
    DECLARE @tmpReference TABLE 
    (
        List            varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )

--    AssignmentCodes
    INSERT INTO @tmpReference (List, DisplayOrder, ReferenceId, Name)
        Values('AssignmentCode', Null, 'O', 'Open')

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: Unable to insert into @tmpReference', 16, 1, @ProcName)
        RETURN
    END
    
    INSERT INTO @tmpReference (List, DisplayOrder, ReferenceId, Name)
        Values('AssignmentCode', Null, 'C', 'Closed')

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: Unable to insert into @tmpReference', 16, 1, @ProcName)
        RETURN
    END
    
    INSERT INTO @tmpReference (List, DisplayOrder, ReferenceId, Name)
        Values('AssignmentCode', Null, 'B', 'Both')
    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: Unable to insert into @tmpReference', 16, 1, @ProcName)
        RETURN
    END

    --Build table of assignments for given date range, adding fields for the moving averages
    DECLARE @Assignments Table(
        AssignmentID            bigint          Not Null,
        ClaimAspectID           bigint          Not Null,
        ShopLocationID          bigint          Not Null,
        StatusID                int                 NULL,
        EstimateAmt             Money           Null,
        SupplementAmt           Money           Null,
        CancellationDate        DateTime        Null,
        Parts                   Money           Null,
        Invoices                Money           Null,
        Towing                  Money           Null,
        Agreements              Money           Null,
        LynxID                  bigint          Null,
        ClaimAspectNumber       Int             Null,
        VehicleYear             varchar(5)      Null,
        Make                    varchar(50)     Null,
        Model                   varchar(50)     Null,
        OwnerLastName           varchar(50)     Null,
        OwnerFirstName          varchar(50)     Null,
        OwnerBusinessName       varchar(50)     Null,
        InsuranceName           varchar(50)     NULL)

    INSERT INTO 
        @Assignments
    SELECT DISTINCT
        A.AssignmentID,
        casc.ClaimAspectID,
        @ShopLocationID,
        CAS.StatusID,
        --Original Estimate Total
        (Select Top 1 es.OriginalExtendedAmt
          FROM dbo.utb_document D 
            Left Join dbo.utb_Estimate_Summary ES ON D.DocumentID = ES.DocumentID
            Inner Join dbo.utb_estimate_summary_type ET ON ES.EstimateSummaryTypeID = ET.EstimateSummaryTypeID
            Inner Join dbo.utb_Document_Type DT ON D.DocumentTypeID = DT.DocumentTypeID
            inner join dbo.utb_claim_aspect CA on casc.ClaimAspectID = CA.ClaimAspectID
            inner join dbo.utb_claim_aspect_type CAT on CA.ClaimAspectTypeID = CAT.ClaimAspectTypeID
          WHERE D.AssignmentID = A.AssignmentID
            AND D.EstimateTypeCD <> 'A'
            AND D.DuplicateFlag = 0
            AND CAT.Name = 'Vehicle'
            AND DT.Name = 'Estimate'
            AND D.EnabledFlag = 1
            AND ET.Name = 'RepairTotal'),

        --First Supplement Total
        (Select Top 1 es.OriginalExtendedAmt
          FROM dbo.utb_document D 
            Left Join dbo.utb_Estimate_Summary ES ON D.DocumentID = ES.DocumentID
            Inner Join dbo.utb_estimate_summary_type ET ON ES.EstimateSummaryTypeID = ET.EstimateSummaryTypeID
            Inner Join dbo.utb_Document_Type DT ON D.DocumentTypeID = DT.DocumentTypeID
            inner join dbo.utb_claim_aspect CA on casc.ClaimAspectID = CA.ClaimAspectID
            inner join dbo.utb_claim_aspect_type CAT on CA.ClaimAspectTypeID = CAT.ClaimAspectTypeID
          WHERE D.AssignmentID = A.AssignmentID 
            AND D.EstimateTypeCD <> 'A'
            AND D.DuplicateFlag = 0
            AND CAT.Name = 'Vehicle'
            AND DT.Name = 'Supplement'
            AND D.EnabledFlag = 1 AND a.assignmentsequencenumber = 1
            AND ET.Name = 'RepairTotal'),
        A.CancellationDate,
        Null,       /*  Parts       */
        Null,       /*  Invoices    */
        Null,       /*  Towing      */
        Null,       /*  Agreements  */

        CA.LynxID,
        CA.ClaimAspectNumber,       
        CV.VehicleYear,
        CV.Make,
        CV.Model,
        I.NameLast,
        I.NameFirst,
        I.BusinessName,
        ins.Name

    FROM
        dbo.utb_Assignment A
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Had to add utb_Claim_Aspect_Service_Channel to provide a link between 
		utb_Assignment to utb_Claim_Vehicle and utb_Assignment to utb_claim_aspect
		M.A. 20061109
	*********************************************************************************/
	INNER JOIN utb_Claim_Aspect_Service_Channel casc ON A.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        Inner Join dbo.utb_Claim_Vehicle CV         ON casc.ClaimAspectID= CV.ClaimAspectID
        left join dbo.utb_claim_aspect CA           ON casc.ClaimAspectID = CA.ClaimAspectID 
	INNER JOIN utb_Claim_Aspect_Status cas on ca.ClaimAspectID =cas.ClaimAspectID
        INNER JOIN dbo.utb_claim C on CA.LynxID = C.LynxID 
        INNER JOIN dbo.utb_insurance ins ON c.InsuranceCompanyID = ins.InsuranceCompanyID
        Left Join dbo.utb_claim_aspect_involved CAI ON CAI.InvolvedID = (Select TOP 1
                                                                                CAI2.InvolvedID
                                                                              FROM
                                                                                dbo.utb_claim_aspect_involved CAI2
                                                                                inner join dbo.utb_involved_role IR on CAI2.InvolvedID = IR.InvolvedID
                                                                                inner join dbo.utb_involved_role_type IRT ON IR.InvolvedRoleTypeID = IRT.InvolvedRoleTypeID
                                                                              WHERE
                                                                                IRT.Name = 'Owner'
                                                                                AND CAI2.ClaimAspectID = casc.ClaimAspectID)
        Left Join dbo.utb_Involved I ON CAI.InvolvedID = I.InvolvedID
     WHERE
        A.ShopLocationID = @ShopLocationID
        AND C.DemoFlag = 0
        AND dbo.ufnUtilityGetQualifiedAssignmentDate(A.AssignmentID) >= @BeginDate
        AND dbo.ufnUtilityGetQualifiedAssignmentDate(A.AssignmentID) <= @EndDate
        
    IF @@Error <> 0
    BEGIN

        RAISERROR  ('%s: Error populating assignments.', 16, 1, @ProcName)
        RETURN
    END

    --Now let's filter it.  <INSERT EVIL GRIN HERE>
    --(It's more efficient to do the initial calculations and just kill the records that don't fit)

    IF @AssignmentCode <> 'B'       --if we don't want everything
    BEGIN
        IF @AssignmentCode = 'O'    --Return Open Claims Only
        BEGIN
            DELETE @Assignments WHERE StatusID = @VehicleClosedStatusID
        END
        ELSE                        --Return Closed Claims Only
        BEGIN
            DELETE @Assignments WHERE StatusID <> @VehicleClosedStatusID
        END
    END

    IF @@Error <> 0
    BEGIN

        RAISERROR  ('%s: Error filtering data.', 16, 1, @ProcName)
        RETURN
    END

    
    IF @DayAdded = 1
    BEGIN
        -- if day was added earlier, get rid of it before returning results
        SET @EndDate = DateAdd (day, -1, @EndDate)  
    END

    --Begin Select Statement
    SELECT
        1                     AS Tag,
        Null                  AS Parent,
        --Root
        @ShopLocationID       AS [Root!1!ShopLocationID],
        @BeginDate            AS [Root!1!BeginDate],
        @EndDate              AS [Root!1!EndDate],
        @AssignmentCode       AS [Root!1!AssignmentCode],
        @DefaultStatRange     AS [Root!1!StatRange],
        @ViewCRD              AS [Root!1!ViewCRD],
        @ViewSMD              AS [Root!1!ViewSMD],
        --Shop
        Null                  AS [Shop!2!ShopLocationID],
        Null                  AS [Shop!2!ShopBusinessID],
        Null                  AS [Shop!2!Address1],
        Null                  AS [Shop!2!Address2],
        Null                  AS [Shop!2!AddressCity],
        Null                  AS [Shop!2!AddressCounty],
        Null                  AS [Shop!2!AddressState],
        Null                  AS [Shop!2!AddressZip],
        Null                  AS [Shop!2!ContactName],
        Null                  AS [Shop!2!EmailAddress],
        Null                  AS [Shop!2!FaxAreaCode],
        Null                  AS [Shop!2!FaxExchangeNumber],
        Null                  AS [Shop!2!FaxExtensionNumber],
        Null                  AS [Shop!2!FaxUnitNumber],
        Null                  AS [Shop!2!Name],
        Null                  AS [Shop!2!PhoneAreaCode],
        Null                  AS [Shop!2!PhoneExchangeNumber],
        Null                  AS [Shop!2!PhoneExtensionNumber],
        Null                  AS [Shop!2!PhoneUnitNumber],
        Null                  AS [Shop!2!ProgramScore],
        Null                  AS [Shop!2!ProgramManagerUserID],
        Null                  AS [Shop!2!ProgramManagerName],
        --Assignment
        Null                  AS [Assignment!3!AssignmentID],
        Null                  AS [Assignment!3!ClaimAspectID],
        Null                  AS [Assignment!3!LynxID],
        Null                  AS [Assignment!3!ClaimAspectNumber],
        Null                  AS [Assignment!3!CancellationDate],
        Null                  AS [Assignment!3!AuditAgreements],
        Null                  AS [Assignment!3!AuditInvoices],
        Null                  AS [Assignment!3!AuditParts],
        Null                  AS [Assignment!3!AuditTowing],
        Null                  As [Assignment!3!FirstSupplementAmt],
        Null                  AS [Assignment!3!OriginalEstimateAmt],
        Null                  AS [Assignment!3!OwnerBusinessName],
        Null                  AS [Assignment!3!OwnerFirstName],
        Null                  AS [Assignment!3!OwnerLastName],
        Null                  AS [Assignment!3!VehicleMake],
        Null                  AS [Assignment!3!VehicleModel],
        Null                  AS [Assignment!3!VehicleYear],
        Null                  AS [Assignment!3!InsuranceName],
        --Stats
        Null                  AS [Stats!4!AfterDrivableCount],
        Null                  AS [Stats!4!AfterDrivableSat],
        Null                  AS [Stats!4!AfterNonDrivableCount],
        Null                  AS [Stats!4!AfterNonDrivableSat],
        Null                  AS [Stats!4!BeforeDrivableCount],
        Null                  AS [Stats!4!BeforeDrivableSat],
        Null                  AS [Stats!4!BeforeNonDrivableCount],
        Null                  AS [Stats!4!BeforeNonDrivableSat],
        Null                  AS [Stats!4!DuringDrivableCount],
        Null                  AS [Stats!4!DuringDrivableSat],
        Null                  AS [Stats!4!DuringNonDrivableCount],
        Null                  AS [Stats!4!DuringNonDrivableSat],
        Null                  AS [Stats!4!SupplDrivableCount],
        Null                  AS [Stats!4!SupplDrivableSat],
        Null                  AS [Stats!4!SupplNonDrivableCount],
        Null                  AS [Stats!4!SupplNonDrivableSat],
        --Reference
        Null                  AS [Reference!5!ListName],
        Null                  AS [Reference!5!ReferenceID],
        Null                  AS [Reference!5!Name]

    UNION ALL

    --Shop
    SELECT
        2,
        1,
        --Root
        Null, Null, Null, Null, Null, Null, Null,
        --Shop
        @ShopLocationID,
        IsNull(Sl.shopID, ''),
        IsNull(SL.Address1, ''),
        IsNull(SL.Address2, ''),
        IsNull(SL.AddressCity, ''),
        IsNull(SL.AddressCounty, ''),
        IsNull(SL.AddressState, ''),
        IsNull(SL.AddressZip, ''),
        IsNull((SELECT P.Name 
                FROM dbo.utb_shop_location_personnel LP 
                     INNER JOIN dbo.utb_personnel P ON LP.PersonnelID = P.PersonnelID
                     INNER JOIN dbo.utb_personnel_type PT ON P.PersonnelTypeID = PT.PersonnelTypeID                     
                WHERE LP.ShopLocationID = @ShopLocationID
                  AND PT.Name = 'Shop Manager'), ''),
        IsNull(SL.EmailAddress, ''),
        IsNull(SL.FaxAreaCode, ''),
        IsNull(SL.FaxExchangeNumber, ''),
        IsNull(SL.FaxExtensionNumber, ''),
        IsNull(SL.FaxUnitNumber, ''),
        IsNull(SL.Name, ''),
        IsNull(SL.PhoneAreaCode, ''),
        IsNull(SL.PhoneExchangeNumber, ''),
        IsNull(SL.PhoneExtensionNumber, ''),
        IsNull(SL.PhoneUnitNumber, ''),
        IsNull(SL.ProgramScore, ''),
        IsNull(SL.ProgramManagerUserID, ''),
        IsNull((U.NameFirst + ' ' + U.NameLast), ''),
        --Assignments
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null,
        --Stats
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, 
        --Reference
        Null, Null, Null
    FROM
        dbo.utb_Shop_Location SL 
        LEFT JOIN dbo.utb_user U on SL.ProgramManagerUserID = U.UserID
    WHERE
        SL.ShopLocationID = @ShopLocationID
        


    UNION ALL

    --Assignments
    SELECT
        3,
        2,
        --Root
        Null, Null, Null, Null, Null, Null, Null,
        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, 
        --Assignments
        IsNull(A.AssignmentID, ''),
        IsNull(A.ClaimAspectID, ''),
        IsNull(A.LynxID, ''),
        IsNull(A.ClaimAspectNumber, ''),
        IsNull(dbo.ufnUtilityGetDateString(A.CancellationDate), ''),
        Isnull(convert(varchar(30), A.Agreements), ''),
        Isnull(convert(varchar(30), A.Invoices), ''),
        Isnull(convert(varchar(30), A.Parts), ''),
        Isnull(convert(varchar(30), A.Towing), ''),
        Isnull(convert(varchar(30), A.SupplementAmt), ''),
        Isnull(convert(varchar(30), A.EstimateAmt), ''),
        Isnull(A.OwnerBusinessName, ''),
        Isnull(A.OwnerFirstName, ''),
        Isnull(A.OwnerLastName, ''),
        Isnull(A.Make, ''),
        Isnull(A.Model, ''),
        IsNull(A.VehicleYear, ''),
        IsNull(A.InsuranceName, ''),
        --Stats
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null,  Null, Null, Null, Null,
        --Reference
        Null, Null, Null
    FROM
        @Assignments A


    UNION ALL

    --Stats
    SELECT
        4,
        1,
        --Root
        Null, Null, Null, Null, Null, Null, Null,
        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, 
        --Assignments
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null,
        --Stats
        IsNull(convert(varchar(10), @AfterDriveCount), ''),
        IsNull(convert(varchar(10), @AfterDriveSat), ''),
        IsNull(convert(varchar(10), @AfterNonDriveCount), ''),
        IsNull(convert(varchar(10), @AfterNonDriveSat), ''),
        IsNull(convert(varchar(10), @BeforeDriveCount), ''),
        IsNull(convert(varchar(10), @BeforeDriveSat), ''),
        IsNull(convert(varchar(10), @BeforeNonDriveCount), ''),
        IsNull(convert(varchar(10), @BeforeNonDriveSat), ''),
        IsNull(convert(varchar(10), @DuringDriveCount), ''),
        IsNull(convert(varchar(10), @DuringDriveSat), ''),
        IsNull(convert(varchar(10), @DuringNonDriveCount), ''),
        IsNull(convert(varchar(10), @DuringNonDriveSat), ''),
        IsNull(convert(varchar(10), @SupplDriveCount), ''),
        IsNull(convert(varchar(10), @SupplDriveSat), ''),
        IsNull(convert(varchar(10), @SupplNonDriveCount), ''),
        IsNull(convert(varchar(10), @SupplNonDriveSat), ''),
        --Reference
        Null, Null, Null

    UNION ALL

    --Reference
    SELECT
        5,
        1,
        --Root
        Null, Null, Null, Null, Null, Null, Null,
        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, 
        --Assignments
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null,
        --Stats
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null,
        --Reference
        List, 
        ReferenceID,
        Name
    FROM
        @tmpReference

    ORDER BY Tag, [Assignment!3!LynxID]

    IF @@Error <> 0
    BEGIN

        RAISERROR  ('%s: Error returning XML.', 16, 1, @ProcName)
        RETURN
    END
END



GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDAuditGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPMDAuditGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/