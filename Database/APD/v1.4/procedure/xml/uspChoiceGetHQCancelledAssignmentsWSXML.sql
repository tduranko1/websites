-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQCancelledAssignmentsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetHQCancelledAssignmentsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceGetHQCancelledAssignmentsWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Get a list of waiting HQ Cancelled Choice Shop Assignments from the database.
* Date:			11Dev2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	11Dec2015 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceGetHQCancelledAssignmentsWSXML]
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceGetHQCancelledAssignmentsWSXML'
	SET @Now = CURRENT_TIMESTAMP

	---------------------------------------
	-- Validations
	---------------------------------------
	-- NONE

	---------------------------------------
	-- Tests
	---------------------------------------
	
	---------------------------------------
	-- Main Code - Check if HQ Cancelled 
	-- assignments exist
	---------------------------------------
	-- HQ Cancelled 
	IF EXISTS (
		SELECT TOP 1
			hj.ClaimAspectID
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID = 95 -- HQ Cancelled
	)
	BEGIN
		------------------------------------------------------
		-- Choice Assignment waiting to be processed.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Choice Assignment are waiting to be processed.'
		END
		
		------------------------------------------------------
		-- Create HQ Cancel Assignment list XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!HQCancelID]

		UNION ALL

		SELECT  2 AS Tag,  
				1 AS Parent
				, NULL
				, hj.ClaimAspectID - 1
				, hj.ClaimAspectID
				, hj.ClaimAspectServiceChannelID
				, hj.JobID
				, hj.LynxID
				, hj.JobStatus
				, hj.DocumentTypeID
				, hj.DocumentTypeName
				, hj.AssignmentID
				, hj.VehicleID
				, hl.EventID
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 994) -- Claim/Vehicle not closed/complete
			--AND cas.StatusID IN (994) -- Vehicle closed
			AND hl.EventID = 95 -- HQ Cancelled

		FOR XML EXPLICIT	
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- NO Choice Assignment waiting
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'NO HQ Cancel Assignment waiting'
		END

		------------------------------------------------------
		-- Create NO JOBS XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!HQCancelID]

		FOR XML EXPLICIT	
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQCancelledAssignmentsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceGetHQCancelledAssignmentsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/