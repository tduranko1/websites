-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopFaxTemplateGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopFaxTemplateGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopFaxTemplateGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Generates an XML Stream contiaining fax templates to use
*
* PARAMETERS:  
* (I) @AssignmentID         The AssignmentID being sent
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspShopFaxTemplateGetListXML
    @AssignmentID   udt_std_id_big,
    @WarrantyFlag   udt_std_flag = 0
AS
BEGIN
    -- Set database options

    SET ANSI_NULLS ON
    SET CONCAT_NULL_YIELDS_NULL OFF

    DECLARE @LossState AS varchar(2)
    DECLARE @ShopState AS varchar(2)
    DECLARE @InsuranceCompanyID as int
    DECLARE @ServiceChannelCD as varchar(3)
    DECLARE @FormID as int
    DECLARE @DocumentTypeIDShopfax as int
    DECLARE @LynxID as bigint
	 DECLARE @AssignmentCode as varchar(1)


    -- Declare variables
    DECLARE @now                          AS DateTime
    DECLARE @ProcName                     AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspShopFaxTemplateGetListXML'


    -- Check to make sure a valid AssignmentID ID was passed in

    IF @WarrantyFlag = 0 
    BEGIN
       IF  (@AssignmentID IS NULL) OR
           (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID))
       BEGIN
           -- Invalid Assignment ID
       
           RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
           RETURN
       END

       -- Get the claim information
       SELECT @LossState = c.LossState,
              @ShopState = sl.AddressState,
              @InsuranceCompanyID = c.InsuranceCompanyID,
              @ServiceChannelCD = casc.ServiceChannelCD,
              @LynxID = c.LynxID,
              @AssignmentCode = CASE 
								   WHEN a.CancellationDate IS NOT NULL THEN 'C' -- Assignment cancelled
								   ELSE 'N' -- New/Active assignment
							    END
       FROM utb_assignment a 
       LEFT JOIN utb_claim_aspect_service_channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
       LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
       LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
       LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
       WHERE a.AssignmentID = @AssignmentID
    END
    ELSE
    BEGIN
       IF  (@AssignmentID IS NULL) OR
           (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_warranty_assignment WHERE AssignmentID = @AssignmentID))
       BEGIN
           -- Invalid Assignment ID
       
           RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
           RETURN
       END

       -- Get the claim information
       SELECT @LossState = c.LossState,
              @ShopState = sl.AddressState,
              @InsuranceCompanyID = c.InsuranceCompanyID,
              @ServiceChannelCD = casc.ServiceChannelCD,
              @LynxID = c.LynxID,
              @AssignmentCode = CASE 
								   WHEN a.CancellationDate IS NOT NULL THEN 'C' -- Assignment cancelled
								   ELSE 'N' -- New/Active assignment
							    END
       FROM utb_warranty_assignment a 
       LEFT JOIN utb_claim_aspect_service_channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
       LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
       LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
       LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
       WHERE a.AssignmentID = @AssignmentID
    END

    -- Get the document id that matches the shop assignment
    SELECT @DocumentTypeIDShopfax = DocumentTypeID
    FROM utb_document_type
    WHERE Name = 'Shop Assignment'

    IF @DocumentTypeIDShopfax IS NULL
    BEGIN
        -- Invalid Shop Assignment document type id
    
        RAISERROR('102|%s|"Shop Assignment"|utb_document_type', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the form for a new/active assignment
    IF @AssignmentCode = 'N'
    BEGIN
	--    select @LossState, @ShopState, @InsuranceCompanyID, @ServiceChannelCD
		SELECT @FormID = FormID
		FROM utb_form
		WHERE InsuranceCompanyID = @InsuranceCompanyID
		  AND DocumentTypeID = @DocumentTypeIDShopfax
		  AND (ServiceChannelCD = @ServiceChannelCD
			    OR ServiceChannelCD IS NULL)
		  AND PertainsToCD = 'S'
		  AND EnabledFlag = 1
		  AND Name <> 'Shop Assignment - Cancellation'

		IF @FormID IS NULL
		BEGIN
			SELECT top 1 @FormID = FormID
			FROM utb_form
			WHERE InsuranceCompanyID IS NULL
			  AND DocumentTypeID = @DocumentTypeIDShopfax
			  AND PertainsToCD = 'S'
			  AND EnabledFlag = 1
			  AND Name <> 'Shop Assignment - Cancellation'
		END
    END
    
    -- Get the form for a cancelled assignment
    IF @AssignmentCode = 'C'
    BEGIN
		SELECT @FormID = FormID
		FROM utb_form
		WHERE InsuranceCompanyID = @InsuranceCompanyID
		  AND DocumentTypeID = @DocumentTypeIDShopfax
		  AND (ServiceChannelCD = @ServiceChannelCD
			    OR ServiceChannelCD IS NULL)
		  AND PertainsToCD = 'S'
		  AND EnabledFlag = 1
		  AND Name = 'Shop Assignment - Cancellation'

		IF @FormID IS NULL
		BEGIN
			SELECT top 1 @FormID = FormID
			FROM utb_form
			WHERE InsuranceCompanyID IS NULL
			  AND DocumentTypeID = @DocumentTypeIDShopfax
			  AND PertainsToCD = 'S'
			  AND EnabledFlag = 1
			  AND Name = 'Shop Assignment - Cancellation'
		END
    END
    
    IF @FormID IS NULL AND @ServiceChannelCD IN ('PS', 'RRP')
    BEGIN
        -- Shop Fax template not available

        RAISERROR('99|Invalid Data state for LynxID: %u; AssignmentID: %u. Shop assignment fax template not available.', 16, 1, @LynxID, @AssignmentID)
        RETURN
    END

    DECLARE @tmpFormSupplement TABLE
    (
        FormSupplementID   int,
        Name               varchar(100),
        LossStateCode      varchar(2),
        ShopStateCode      varchar(2),
        ServiceChannelCD   varchar(3)
    )
    
    INSERT INTO @tmpFormSupplement 
    SELECT FormSupplementID,
           Name,
           LossStateCode,
           ShopStateCode,
           ServiceChannelCD
    FROM utb_form_supplement
    WHERE FormID = @FormID
      AND (LossStateCode IS NULL OR LossStateCode = @LossState)
      AND (ShopStateCode IS NULL OR ShopStateCode = @ShopState)
      AND (ServiceChannelCD IS NULL OR ServiceChannelCD = @ServiceChannelCD)
      
    -- delete any duplicate supplement form that are state specific and have the same name
    IF EXISTS(SELECT Name
               FROM @tmpFormSupplement 
               GROUP BY name
               HAVING count(name) > 1)
    BEGIN
      DELETE FROM @tmpFormSupplement
      WHERE Name IN (SELECT Name
                     FROM @tmpFormSupplement 
                     GROUP BY name
                     HAVING count(name) > 1)
        AND (LossStateCode IS NULL OR LossStateCode <> @LossState)
        AND (ShopStateCode IS NULL OR ShopStateCode <> @ShopState)
        AND (ServiceChannelCD IS NULL OR ServiceChannelCD <> @ServiceChannelCD)
    END
    
    SELECT  1 AS Tag,
            NULL AS Parent,
            Convert(varchar(20), @AssignmentID) AS [Assignment!1!AssignmentID], 
            @ServiceChannelCD                   AS [Assignment!1!ServiceChannelCD],
            -- Form Info
            NULL AS [Form!2!FormID],
            NULL AS [Form!2!DocumentTypeID],
            NULL AS [Form!2!InsuranceCompanyID],
            NULL AS [Form!2!LossStateCode],
            NULL AS [Form!2!ShopStateCode],
            NULL AS [Form!2!Name],
            NULL AS [Form!2!PDFPath],
            NULL AS [Form!2!ServiceChannelCD],
            NULL AS [Form!2!SQLProcedure],

            -- Supplement Info
            NULL AS [Supplement!3!FormSupplementID],
            NULL AS [Supplement!3!LossStateCode], 
            NULL AS [Supplement!3!ShopStateCode],
            NULL AS [Supplement!3!Name],
            NULL AS [Supplement!3!PDFPath],
            NULL AS [Supplement!3!ServiceChannelCD],
            NULL As [Supplement!3!SQLProcedure]           

    UNION ALL

    SELECT  2,
            1,
            -- Root
            NULL, NULL,
            -- Form Info
            FormID, 
            DocumentTypeID, 
            InsuranceCompanyID, 
            LossStateCode,
            ShopStateCode,  
            Name,  
            PDFPath,
            ServiceChannelCD,  
            SQLProcedure, 
            -- Supplement Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM utb_form
    WHERE FormID = @FormID

    UNION ALL
    
    SELECT  3,
            2,
            -- Root
            NULL, NULL,
            -- Form Info
            fs.FormID, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Supplement Info
            fs.FormSupplementID,  
            fs.LossStateCode,  
            fs.ShopStateCode,  
            fs.Name,  
            fs.PDFPath,  
            fs.ServiceChannelCD,  
            fs.SQLProcedure
    FROM @tmpFormSupplement tmp
    LEFT JOIN utb_form_supplement fs ON tmp.FormSupplementID = fs.FormSupplementID
    /*WHERE FormID = @FormID
      AND (tmp.LossStateCode IS NULL OR LossStateCode = @LossState)
      AND (ShopStateCode IS NULL OR ShopStateCode = @ShopState)
      AND (ServiceChannelCD IS NULL OR ServiceChannelCD = @ServiceChannelCD)*/

    ORDER BY [Form!2!FormID], Tag    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopFaxTemplateGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopFaxTemplateGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
