-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspZipCodeDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspZipCodeDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspZipCodeDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Generates an XML Stream contiaining zip code information
*
* PARAMETERS:  
* (I) @ZipCode         The Zip code
* (I) @StateCode       The State code
* (I) @City            The City name
* (I) @County          The County name
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspZipCodeDetailXML
    @ZipCode   udt_std_id_big = NULL,
    @StateCode varchar(2) = NULL,
    @City      udt_std_desc_short = NULL,
    @County    udt_std_desc_short = NULL
AS
BEGIN
    -- Set database options

    SET ANSI_NULLS ON
    SET CONCAT_NULL_YIELDS_NULL OFF

    -- Declare variables
    DECLARE @ProcName                     AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspZipCodeDetailXML'
    
    if @StateCode is null or len(rtrim(@StateCode)) = 0  set @StateCode = '%'
    if @City is null or len(rtrim(@City)) = 0 set @City = '%'
    if @County is null or len(rtrim(@County)) = 0 set @County = '%'
    
    if @City <> '%' and RIGHT(RTRIM(@City), 1) <> '%' SET @City = RTRIM(@City) + '%'

    DECLARE @tmpZipInfo TABLE
    (
        ZipCode            bigint,
        StateCode          varchar(2),
        City               varchar(50),
        County             varchar(50)
    )
    
    IF @ZipCode is not null
    begin
      INSERT INTO @tmpZipInfo 
      SELECT mz.Zip, 
             ltrim(rtrim(mz.State)), 
             ltrim(rtrim(mz.City)), 
             ltrim(rtrim(mc.Name))
      from dbo.utb_melissa_zip mz
      LEFT JOIN dbo.utb_melissa_cnty mc on mz.FIPS = mc.FIPS
      where mz.Zip = @ZipCode
        and mz.LL = 'L' -- use the official USPS Name of the city
      order by mz.City, mz.State
    end
    else
    begin
      INSERT INTO @tmpZipInfo 
      SELECT distinct NULL, 
             ltrim(rtrim(mz.State)), 
             ltrim(rtrim(mz.City)), 
             ltrim(rtrim(mc.Name))
      from dbo.utb_melissa_zip mz
      LEFT JOIN dbo.utb_melissa_cnty mc on mz.FIPS = mc.FIPS
      where mz.State like @StateCode
        and mz.City like @City
        and mc.Name like @County
        --and mz.LL = 'L' -- use the official USPS Name of the city
    end

    SELECT  1 AS Tag,
            NULL AS Parent,
            -- Root
            isNull(@ZipCode, '') as [Root!1!ParamZip],
            isNull(@StateCode, '') as [Root!1!ParamState],
            isNull(@City, '') as [Root!1!ParamCity],
            isNull(@County, '') as [Root!1!ParamCounty],
            -- Zip Details
            NULL AS [Detail!2!ZipCode],
            NULL AS [Detail!2!StateCode],
            NULL AS [Detail!2!City],
            NULL AS [Detail!2!County]
            
    UNION ALL

    SELECT  2,
            1,
            -- Root
            NULL, NULL, NULL, NULL,
            -- Form Info
            ZipCode, 
            StateCode, 
            City, 
            County

    FROM @tmpZipInfo
    
    ORDER BY [Detail!2!ZipCode], Tag        

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspZipCodeDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspZipCodeDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
