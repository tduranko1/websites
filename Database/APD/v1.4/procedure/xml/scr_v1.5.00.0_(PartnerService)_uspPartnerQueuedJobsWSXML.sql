-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerQueuedJobsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPartnerQueuedJobsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPartnerQueuedJobsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns a list of jobs waiting in the queues as XML
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspPartnerQueuedJobsWSXML]
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspPartnerQueuedJobsWSXML'
    
    SET @ApplicationCD = 'APDPartner'

    -- Validation
    -- None
    
    -- Begin main code
	DECLARE @iInCnt AS INT
	DECLARE @iOutCnt AS INT
	    
	SELECT 
		@iInCnt = COUNT(JobID)
	FROM
		utb_partnersvc_jobs
	WHERE 
		EnabledFlag = 1	
		AND IODirection = 'I'
		AND UPPER(JobStatus) = 'UNPROCESSED'

	SELECT 
		@iOutCnt = COUNT(JobID)
	FROM
		utb_partnersvc_jobs
	WHERE 
		EnabledFlag = 1	
		AND IODirection = 'O'
		AND UPPER(JobStatus) = 'UNPROCESSED'
		
    SELECT  
		1 AS Tag
        , NULL AS Parent
        , @iInCnt AS [Root!1!InQueueTotal]
        , @iOutCnt AS [Root!1!OutQueueTotal]
        , NULL AS [Job!2!FromPartnerID]
        , NULL AS [Job!2!ToPartnerID]
        , NULL AS [Job!2!IODirection]
        , NULL AS [Job!2!InQueue]

    UNION ALL

    SELECT  
		2 AS Tag
        , 1 AS Parent
        , NULL
        , NULL
        , FromPartnerID AS [Job!2!FromPartnerID]
        , ToPartnerID AS [Job!2!ToPartnerID]
        , IODirection AS [Job!2!IODirection]
        , COUNT(JobID) AS [Job!2!InQueue]
	FROM
		utb_partnersvc_jobs
	WHERE 
		EnabledFlag = 1	
		AND IODirection IN ('I','O')
		AND UPPER(JobStatus) = 'UNPROCESSED'
	GROUP BY
		FromPartnerID
		, IODirection
		, ToPartnerID
	
	FOR XML EXPLICIT
		
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerQueuedJobsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPartnerQueuedJobsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/