-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTWebSignupGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTWebSignupGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTWebSignupGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Search for an Entity -- Business Information (utb_shop), Shop (utb_shop_location) or Dealer (utb_dealer...believe it or not)
*
* RESULT SET:
* ShopLoadID                             The entity's identity
* ShopName                           The entity's name
* ShopLocationID                     The entity location's identity
* AddressCity                        The entity location's address city
* AddressState                       The entity location's address state
* ShopLocationName                   The entity location's name
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSMTWebSignupGetListXML]
(
    @ShopLoadID             udt_std_id_big=NULL,
    @AddressCity            udt_addr_city=NULL,
    @AddressState           udt_addr_state=NULL,
    @AddressZip             udt_addr_zip_code=NULL,
    @CertifiedFirstID       udt_std_desc_short=NULL,
    @FedTaxId               udt_fed_tax_id=NULL,
    @Name                   udt_std_name=NULL,
    @PhoneAreaCode          udt_ph_area_code=NULL,
    @PhoneExchange          udt_ph_exchange_number=NULL,
    @PhoneUnit              udt_ph_unit_number=NULL,
    @ProgramFlag            udt_std_int=0
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message

    SET NOCOUNT ON


    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @WHERE             AS VARCHAR(500)
    DECLARE @SQL               AS NVARCHAR(1000)
    DECLARE @Now               AS DateTime
    
    CREATE TABLE #shop   (ShopLoadID          bigint      NULL,
                          Address1            varchar(50)     NULL,
                          Address2            varchar(50)     NULL,
                          AddressCity         varchar(30)       NULL,
                          AddressState        varchar(2)      NULL,
                          AddressZip          varchar(8)			NULL,
                          CertifiedFirstID    varchar(50)  NULL,
                          Name                varchar(50)        NULL,
                          SysLastUpdatedDate  datetime    NULL)
    

    SET @ProcName = 'uspSMTWebSignupGetListXML'
    SET @Now = Current_Timestamp

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@CertifiedFirstID))) = 0 SET @CertifiedFirstID = NULL
    IF LEN(RTRIM(LTRIM(@FedTaxId))) = 0 SET @FedTaxId = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchange))) = 0 SET @PhoneExchange = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnit))) = 0 SET @PhoneUnit = NULL

    
    


    -- SEARCH WHERE CLAUSE
    IF (@FedTaxId IS NOT NULL) AND (LEN(@FedTaxId) > 0)
    BEGIN
        SET @WHERE = ' FedTaxId LIKE ''%' + @FedTaxId + '%'''
    END
    

    IF (@ShopLoadID IS NOT NULL) AND (@ShopLoadID > 0)
    BEGIN
        SET @WHERE = ' S.ShopLoadID = ' + CONVERT(VARCHAR, @ShopLoadID)
    END


    IF (@CertifiedFirstID is not NULL) AND (LEN(@CertifiedFirstID) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.CertifiedFirstID LIKE ''%' + @CertifiedFirstID + '%'''
        ELSE
            SET @WHERE = ' S.CertifiedFirstID LIKE ''%' + @CertifiedFirstID + '%'''
    END
    

    IF (@Name is not NULL) AND (LEN(@Name) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.Name LIKE ''%' + @Name + '%'''
        ELSE
            SET @WHERE = ' S.Name LIKE ''%' + @Name + '%'''
    END

    IF (@AddressCity IS NOT NULL) AND (LEN(@AddressCity) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.AddressCity LIKE ''%' + @AddressCity + '%'''
        ELSE
            SET @WHERE = ' S.AddressCity LIKE ''%' + @AddressCity + '%'''
    END

    IF (@AddressState IS NOT NULL) and (LEN(@AddressState) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.AddressState =''' + @AddressState + ''''
        ELSE
            SET @WHERE = ' S.AddressState =''' + @AddressState + ''''
    END
    
    
    IF (@AddressZip IS NOT NULL) and (LEN(@AddressZip) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.AddressZip =''' + @AddressZip + ''''
        ELSE
            SET @WHERE = ' S.AddressZip =''' + @AddressZip + ''''
    END
    

    IF (@PhoneAreaCode is not NULL) AND (LEN(@PhoneAreaCode) > 0)
    BEGIN
        IF (@PhoneExchange is not NULL) AND (LEN(@PhoneExchange) > 0)
        BEGIN
            SET @PhoneAreaCode = (select dbo.ufnUtilityVerifyAreaCode (@PhoneAreaCode, @PhoneExchange, @now))
        END
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.PhoneAreaCode LIKE ''%' + @PhoneAreaCode + '%'''
        ELSE
            SET @WHERE = ' S.PhoneAreaCode LIKE ''%' + @PhoneAreaCode + '%'''
    END

    IF (@PhoneExchange is not NULL) AND (LEN(@PhoneExchange) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.PhoneExchangeNumber LIKE ''%' + @PhoneExchange + '%'''
        ELSE
            SET @WHERE = ' S.PhoneExchangeNumber LIKE ''%' + @PhoneExchange + '%'''
    END

    IF (@PhoneUnit is not NULL) AND (LEN(@PhoneUnit) > 0)
    BEGIN
        IF (LEN(@WHERE) > 0)
            SET @WHERE = @WHERE + ' AND S.PhoneUnitNumber LIKE ''%' + @PhoneUnit + '%'''
        ELSE
            SET @WHERE = ' S.PhoneUnitNumber LIKE ''%' + @PhoneUnit + '%'''
    END
    
    
    -- PrgramFlag, actually an integer in this case, determines what subset of shop_load records to pull
    
    IF (@ProgramFlag = 0)   -- All Unmerged Web Signups
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND MergeDate IS NULL '
      ELSE
        SET @WHERE = ' MergeDate IS NULL '
    END
    
    IF (@ProgramFlag = 1)   -- Have Not Visited Web Site
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND S.SiteFirstVisitDate IS NULL AND S.DataEntryDate IS NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
      ELSE
        SET @WHERE = ' S.SiteFirstVisitDate IS NULL AND S.DataEntryDate IS NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
    END
    
    IF (@ProgramFlag = 2)   -- Visited Web Site Only
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
      ELSE
        SET @WHERE = ' S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
    END
    
    IF (@ProgramFlag = 3)   -- Entered Data Only
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NOT NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
      ELSE
        SET @WHERE = ' S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NOT NULL AND FinalSubmitDate IS NULL AND MergeDate IS NULL '
    END
    
    IF (@ProgramFlag = 4)   -- Submitted Data
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NOT NULL AND FinalSubmitDate IS NOT NULL AND MergeDate IS NULL '
      ELSE
        SET @WHERE = ' S.SiteFirstVisitDate IS NOT NULL AND S.DataEntryDate IS NOT NULL AND FinalSubmitDate IS NOT NULL AND MergeDate IS NULL '
    END
    
    IF (@ProgramFlag = 5)   -- Already Merged
    BEGIN
      IF (LEN(@WHERE) > 0)
        SET @WHERE = @WHERE + ' AND MergeDate IS NOT NULL '
      ELSE
        SET @WHERE = ' MergeDate IS NOT NULL '
    END
    

    
    SET @SQL = N'Insert into #shop SELECT S.ShopLoadID,  S.Address1, S.Address2, S.AddressCity, S.AddressState, ' +
                'S.AddressZip, S.CertifiedFirstID, S.Name, S.SysLastUpdatedDate ' +
                'FROM dbo.utb_shop_load S '

      
    SET @SQL = @SQL + 'WHERE ' + @WHERE
           
    
      --select @SQL
      --return

    execute sp_executesql @SQL
    
    
    -- Check error value

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    
    -- Begin Final Select
    SELECT
        1                                       AS Tag,
        Null                                    AS Parent,
        -- Root
        Null                                    AS [Root!1!Dud],
        -- Shop
        Null                                    AS [Shop!2!ShopLoadID],
        Null                                    AS [Shop!2!Address1],
        Null                                    AS [Shop!2!Address2],
        Null                                    AS [Shop!2!AddressCity],
        Null                                    AS [Shop!2!AddressState],
        Null                                    AS [Shop!2!AddressZip],
        Null                                    AS [Shop!2!CertifiedFirstID],
        Null                                    AS [Shop!2!Name],
        Null                                    AS [Shop!2!SysLastUpdatedDate]
   
    UNION ALL

    --Entity Level
    SELECT
        2,
        1,
        --Root
        Null,
        --Shop
        IsNull(s.ShopLoadID, ''),
        IsNull(s.Address1, ''),
        IsNull(s.Address2, ''),
        IsNull(s.AddressCity, ''),
        IsNull(s.AddressState, ''),
        IsNull(s.AddressZip, ''),
        IsNull(s.CertifiedFirstID, ''),
        IsNull(s.Name, ''),
        dbo.ufnUtilityGetDateString(s.SysLastUpdatedDate)
        
    FROM #shop s
        
    ORDER BY [Shop!2!Name], Tag 
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT


    -- Check error value

    IF @error <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END  


go

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTWebSignupGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTWebSignupGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/