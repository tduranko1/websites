-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadGetSurveyXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopLoadGetSurveyXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopLoadGetSurveyXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves Shop survey stuff
*
* PARAMETERS:  
* (I) ClaimAspectID        
*
* RESULT SET:
* An XML Data stream containing payment information
*
*
* VSS
* $Workfile: uspSMTShopLoadGetSurveyXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspSMTShopLoadGetSurveyXML.SQL $
* $Revision: 1 $
* $Author: Dan Price $
* $Date: 10/23/01 5:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopLoadGetSurveyXML
    @CertifiedFirstID       udt_std_desc_short=null,
    @AddressZip             udt_addr_zip_code=null
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts
    DECLARE @now                AS datetime
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    --DECLARE @LynxID             AS udt_std_id_big
    

    SET @ProcName = 'uspSMTShopLoadGetSurveyXML'
    SET @now = CURRENT_TIMESTAMP
    SET @CertifiedFirstID = LTRIM(RTRIM(@CertifiedFirstID))
    SET @AddressZip = LTRIM(RTRIM(@AddressZip))

    -- Validate Parameters
    /*
    IF  (@CertifiedFirstID IS NULL) OR (LEN(LTRIM(RTRIM(@CertifiedFirstID))) = 0)
    BEGIN
        -- Invalid Location ID
        RAISERROR('101|%s|@CertifiedFirstID|%s', 16, 1, @ProcName, @CertifiedFirstID)
        RETURN
    END
    
    IF  (@AddressZip IS NULL) OR (LEN(LTRIM(RTRIM(@AddressZip))) = 0)
    BEGIN
        -- Invalid Location ID
        RAISERROR('101|%s|@AddressZip|%s', 16, 1, @ProcName, @AddressZip)
        RETURN
    END
    */

    DECLARE @QualificationsSubmittedDate      AS varchar(30)
    DECLARE @SiteFirstVisitDate               AS varchar(30)
    DECLARE @MergeDate                        AS datetime
    DECLARE @ShopLoadID                       AS bigint
    DECLARE @PreviousVisitToSiteFlag          AS int

   
    SET @ShopLoadID = ISNULL((SELECT TOP 1 ShopLoadID 
                       FROM dbo.utb_shop_load
                       WHERE CertifiedFirstID = @CertifiedFirstID
                         AND AddressZip = @AddressZip), 0)
   
        
    
    SELECT  @QualificationsSubmittedDate = dbo.ufnUtilityGetDateString(QualificationsSubmitedDate),
            @SiteFirstVisitDate = SiteFirstVisitDate,
            @MergeDate = MergeDate
    FROM dbo.utb_shop_load
    WHERE ShopLoadID = @ShopLoadID
   
   
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
   
    IF (@QualificationsSubmittedDate = '1900-01-01T00:00:00')
      SET @QualificationsSubmittedDate = ''
      
    IF (@SiteFirstVisitDate IS NOT NULL)
      SET @PreviousVisitToSiteFlag = 1
    ELSE
      SET @PreviousVisitToSiteFlag = 0

    IF (@ShopLoadID > 0) AND (@MergeDate IS NOT NULL) SET @ShopLoadID = -1
    
    
    BEGIN TRANSACTION GetSurveyTran1
    
    
    IF (@SiteFirstVisitDate IS NULL) AND (@ShopLoadID > 0)
    BEGIN
      UPDATE dbo.utb_shop_load
      SET SiteFirstVisitDate = @now
      WHERE ShopLoadID = @ShopLoadID
    END
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_load', 16, 1, @ProcName)
        ROLLBACK TRANSACTION GetSurveyTran1
        RETURN
    END
    
   
    -- Select Root Level
    SELECT 	1                             AS Tag,
            NULL                          AS Parent,
            --Root
            @AddressZip                   AS [Root!1!AddressZip],
            @CertifiedFirstID             AS [Root!1!CertifiedFirstID],
            @QualificationsSubmittedDate  AS [Root!1!QualificationsSubmittedDate],
            @ShopLoadID                   AS [Root!1!ShopLoadID],
			@PreviousVisitToSiteFlag      AS [Root!1!PreviousVisitToSiteFlag]
            
            
    --FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION GetSurveyTran1
        RETURN
    END
    
    COMMIT TRANSACTION GetSurveyTran1
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadGetSurveyXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopLoadGetSurveyXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
