-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDActiveWorkGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPMDActiveWorkGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspPMDActiveWorkGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspPMDActiveWorkGetListXML
    @ProgramManagerUserID   varchar(250)
AS
BEGIN
    DECLARE @ProcName                   varchar(30)
    DECLARE @Now                        udt_std_datetime
    DECLARE @ClaimAspectTypeIDVehicle   int
    DECLARE @StatusVehClosed            int
    DECLARE @StatusVehCancelled         int
    DECLARE @StatusVehVoided            int
            
    SET @ProcName = 'uspPMDActiveWorkGetListXML'
    SET @Now = current_timestamp
    SET NOCOUNT ON
    
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Vehicle'
    
    SELECT @StatusVehClosed = StatusID
    FROM dbo.utb_status
    WHERE Name = 'Vehicle Closed'
    
    SELECT @StatusVehCancelled = StatusID
    FROM dbo.utb_status
    WHERE Name = 'Vehicle Cancelled'
    
    SELECT @StatusVehVoided = StatusID
    FROM dbo.utb_status
    WHERE Name = 'Vehicle Voided'
    
    -- Build table to hold all the Program Manager UserIDs.
    DECLARE @ProgramManager TABLE (UserID  INT  NOT NULL,
                                   NameFirst varchar(50) NULL,
                                   NameLast  varchar(50) NULL) 
    
    INSERT INTO @ProgramManager
    SELECT value,
           CASE
                WHEN EXISTS(SELECT NameFirst FROM ufnUtilityProgramManagersGetList() WHERE UserID = value) THEN
                    (SELECT NameFirst FROM ufnUtilityProgramManagersGetList() WHERE UserID = value)
                ELSE
                    (SELECT NameFirst FROM ufnUtilityPMDSupervisorsGetList() WHERE UserID = value)
           END,
           CASE
                WHEN EXISTS(SELECT NameLast FROM ufnUtilityProgramManagersGetList() WHERE UserID = value) THEN
                    (SELECT NameLast FROM ufnUtilityProgramManagersGetList() WHERE UserID = value)
                ELSE
                    (SELECT NameLast FROM ufnUtilityPMDSupervisorsGetList() WHERE UserID = value)
           END
    FROM dbo.ufnUtilityParseString(@ProgramManagerUserID, ',', 1)
        
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot insert into @ProgramManager
    
        RAISERROR('105|%s|@ProgramManager', 16, 1, @ProcName)
        RETURN
    END
    
    -- Build table to hold all the Program Manager UserIDs.
    /*******************************************************************************************
    --Project:210474 APD Remarked-off the following code when we did the code merge M.A.20061117
    *******************************************************************************************/
    /*DECLARE @Shops TABLE (
        ShopLocationID  INT  NOT NULL
    )
    
    INSERT INTO @Shops
    SELECT ShopLocationID
    FROM utb_shop_location
    WHERE ProgramManagerUserID in (SELECT UserID FROM @ProgramManager)
    
    UNION ALL
    
    SELECT r.ShopLocationID 
    FROM utb_reinspect r
    LEFT JOIN utb_shop_location sl ON (r.ShopLocationID = sl.ShopLocationID)
    WHERE r.ReinspectorUserID IN (SELECT UserID FROM @ProgramManager)
      AND r.LockedFlag = 0
      AND ProgramManagerUserID NOT IN (SELECT UserID FROM @ProgramManager)*/
    /***************************************************************************************
    --Project:210474 APD Remarked-off the code above when we did the code merge M.A.20061117
    ***************************************************************************************/    
    
    --SELECT * from @Shops
    
    -- Final Select
    --Begin final SELECT
    SELECT
        1                                       AS Tag,
        NULL                                    AS Parent,
        --Root
        @ProgramManagerUserID                   AS [Root!1!ProgramManagerUserID],
        --Active Work Queue
        NULL                                    AS [ActiveQueue!2!ShopLocationID],
        NULL                                    AS [ActiveQueue!2!ShopID],
        NULL                                    AS [ActiveQueue!2!Name],
        NULL                                    AS [ActiveQueue!2!AddressCity],
        NULL                                    AS [ActiveQueue!2!AddressState],
        NULL                                    AS [ActiveQueue!2!ProgramManagerUserID],
        NULL                                    AS [ActiveQueue!2!LynxID],
        NULL                                    AS [ActiveQueue!2!ClaimAspectNumber],
        NULL                                    AS [ActiveQueue!2!ClaimAspectID],
        NULL                                    AS [ActiveQueue!2!InsuranceCoName],
        NULL                                    AS [ActiveQueue!2!VehicleYear],
        NULL                                    AS [ActiveQueue!2!VehicleMake],
        NULL                                    AS [ActiveQueue!2!VehicleModel],
        NULL                                    AS [ActiveQueue!2!RepairStatus],
        NULL                                    AS [ActiveQueue!2!ReIDate],
        NULL                                    AS [ActiveQueue!2!ReIRequested],
        NULL                                    AS [ActiveQueue!2!ReIRequestedDate],
        NULL                                    AS [ActiveQueue!2!ReILockedFlag],
        NULL                                    AS [ActiveQueue!2!AssignmentID],
        NULL                                    AS [ActiveQueue!2!AssignmentDate],  --Project:210474 APD Added the column when we did the code merge M.A.20061117
        -- Reference Data
        NULL                                    AS [ProgramManager!3!UserID],
        NULL                                    AS [ProgramManager!3!NameFirst],
        NULL                                    AS [ProgramManager!3!NameLast]
        
    UNION ALL        

    -- Select all active assignments where the current program manager is the owner
    SELECT 2, 
           1,
           -- Root
           NULL,
           --Active Work Queue
           sl.shopLocationID,
           sl.shopID,
           sl.Name,
           sl.AddressCity,
           sl.AddressState,
           --Project:210474 APD Modified the code below when we did the code merge M.A.20061117
           CASE 
            WHEN sl.ProgramManagerUserID = 0 THEN t.ProgramManagerUserID
            ELSE sl.ProgramManagerUserID
           END,
           --Project:210474 APD Modified the code above when we did the code merge M.A.20061117
           ca.LynxID,
           ca.ClaimAspectNumber,
           ca.ClaimAspectID,
           i.Name,
           isNull(cv.VehicleYear, ''),
           isNull(cv.Make, ''),
           isNull(cv.Model, ''),
           CASE
            --WHEN RepairEndDate IS NOT NULL AND RepairEndConfirmFlag = 1 AND RepairEndDate <= @now THEN 'Complete' --Project: 210474 APD - Enhancements to support multiple concurrent service channels. M.A.20061109
            WHEN casc.WorkEndDate IS NOT NULL AND casc.WorkEndConfirmFlag = 1 AND casc.WorkEndDate <= @now THEN 'Complete'
            --WHEN RepairStartDate IS NOT NULL AND RepairStartConfirmFlag = 1 THEN 'In Progress'  --Project: 210474 APD - Enhancements to support multiple concurrent service channels. M.A.20061109
            WHEN WorkStartDate IS NOT NULL AND WorkStartConfirmFlag = 1 THEN 'In Progress'
            ELSE 'Not Started'
           END AS 'Repair Status',
           (SELECT isNull(convert(varchar, MAX(ReinspectionDate), 101), '')
                FROM dbo.utb_reinspect r
                WHERE r.ClaimAspectID = ca.ClaimAspectID
                  AND r.EnabledFlag = 1
           ) AS 'ReI Date',
           (SELECT CASE
                        WHEN count(ReinspectionRequestFlag) > 0 THEN 1
                        ELSE 0
                    END
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = ca.ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1
                  AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
            ), 
            (SELECT CASE
                        WHEN count(ReinspectionRequestFlag) > 0 THEN CONVERT(varchar, max(ReinspectionRequestDate), 101)
                        ELSE ''
                    END
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = ca.ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1
                  AND casc.PrimaryFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
            ),
            isNull((SELECT r1.LockedFlag
               FROM utb_reinspect r1
               WHERE r1.CreationDate = (SELECT TOP 1 r2.CreationDate
                                           FROM dbo.utb_reinspect r2
                                            WHERE r2.ClaimAspectID = ca.ClaimAspectID
                                              AND r2.EnabledFlag = 1
                                             ORDER BY r2.CreationDate DESC)
                 AND r1.ClaimAspectID = ca.ClaimAspectID
            ), 99),
            a.AssignmentID,
            convert(varchar, a.AssignmentDate, 101),  --Project:210474 APD Added the column when we did the code merge M.A.20061117
           -- Reference
           NULL, NULL, NULL
    FROM utb_claim_aspect ca
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:	Added utb_Claim_Aspect_Status to support the use of "StatusID" specified
		in the WHERE clause
		M.A. 20061109
    *********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
    LEFT JOIN utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:	Added utb_Claim_Aspect_Service_Channel to join utb_Assignment to 
		utb_Claim_Aspect
		M.A. 20061109
    *********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
    LEFT JOIN utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.claimAspectID)
    
    LEFT JOIN utb_state_code sc ON (sl.AddressState = sc.StateCode) --Project:210474 APD Added reference to the table when we did the code merge M.A.20061117
    LEFT JOIN utb_territory t ON (sc.TerritoryID = t.TerritoryID)  --Project:210474 APD Added reference to the table when we did the code merge M.A.20061117
    WHERE (sl.ProgramManagerUserID in (SELECT UserID FROM @ProgramManager)
       OR (sl.ProgramManagerUserID = 0 AND t.ProgramManagerUserID in (SELECT UserID FROM @ProgramManager)))  --Project:210474 APD Added in the WHERE clause when we did the code merge M.A.20061117
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND a.assignmentsequencenumber = 1
      AND CancellationDate IS NULL
      AND i.DemoFlag = 0
      and cas.ServiceChannelCD is null
      and cas.StatusTypeCD is null
      AND (cas.StatusID <> @StatusVehClosed
          AND cas.StatusID <> @StatusVehCancelled
          AND cas.StatusID <> @StatusVehVoided)

    UNION ALL        

    -- Select all active assignments where the current program manager has started the reinspection
    --  and the shop does not belong to his territory
    SELECT 2, 
           1,
           -- Root
           NULL,
           --Active Work Queue
           sl.shopLocationID,
           sl.shopID,
           sl.Name,
           sl.AddressCity,
           sl.AddressState,
           r.ReinspectorUserID, -- since this is from someone elses queue, we need to use the reinpsect id inorder to show up.
           ca.LynxID,
           ca.ClaimAspectNumber,
           ca.ClaimAspectID,
           i.Name,
           isNull(cv.VehicleYear, ''),
           isNull(cv.Make, ''),
           isNull(cv.Model, ''),
           CASE
            --WHEN RepairEndDate IS NOT NULL AND RepairEndConfirmFlag = 1 AND RepairEndDate <= @now THEN 'Complete'  --Project: 210474 APD - Enhancements to support multiple concurrent service channels. M.A.20061109
            WHEN WorkEndDate IS NOT NULL AND WorkEndConfirmFlag = 1 AND WorkEndDate <= @now THEN 'Complete'
            --WHEN RepairStartDate IS NOT NULL AND RepairStartConfirmFlag = 1 THEN 'In Progress'  --Project: 210474 APD - Enhancements to support multiple concurrent service channels. M.A.20061109
            WHEN WorkStartDate IS NOT NULL AND WorkStartConfirmFlag = 1 THEN 'In Progress'
            ELSE 'Not Started'
           END AS 'Repair Status',
           (SELECT isNull(convert(varchar, MAX(ReinspectionDate), 101), '')
                FROM dbo.utb_reinspect r
                WHERE r.ClaimAspectID = ca.ClaimAspectID
                  AND r.EnabledFlag = 1
           ) AS 'ReI Date',
           (SELECT CASE
                        WHEN count(ReinspectionRequestFlag) > 0 THEN 1
                        ELSE 0
                    END
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = ca.ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1
                  And casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
            ), 
            (SELECT CASE
                        WHEN count(ReinspectionRequestFlag) > 0 THEN CONVERT(varchar, max(ReinspectionRequestDate), 101)
                        ELSE ''
                    END
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = ca.ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1
                  And casc.PrimaryFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
            ),
            isNull((SELECT r1.LockedFlag
               FROM utb_reinspect r1
               WHERE r1.CreationDate = (SELECT TOP 1 r2.CreationDate
                                           FROM dbo.utb_reinspect r2
                                            WHERE r2.ClaimAspectID = ca.ClaimAspectID
                                              AND r2.EnabledFlag = 1
                                             ORDER BY r2.CreationDate DESC)
                 AND r1.ClaimAspectID = ca.ClaimAspectID
            ), 99),            
            a.AssignmentID,
            convert(varchar, a.AssignmentDate, 101),  --Project:210474 APD Added the column when we did the code merge M.A.20061117
           -- Reference
           NULL, NULL, NULL
    FROM utb_reinspect r
    LEFT JOIN utb_claim_aspect ca ON (r.ClaimAspectID = ca.ClaimAspectID)
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:	Added utb_Claim_Aspect_Status to support the use of "StatusID" specified
		in the WHERE clause
		M.A. 20061109
    *********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
    LEFT JOIN utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:	Added utb_Claim_Aspect_Service_Channel to join utb_Assignment to 
		utb_Claim_Aspect
		M.A. 20061109
    *********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
    LEFT JOIN utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.claimAspectID)
    WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND a.assignmentsequencenumber = 1
      AND CancellationDate IS NULL
      AND i.DemoFlag = 0
      and cas.ServiceChannelCD is null
      and cas.StatusTypeCD is null
      AND (   cas.StatusID <> @StatusVehClosed
          AND cas.StatusID <> @StatusVehCancelled
          AND cas.StatusID <> @StatusVehVoided)
      AND r.ReinspectorUserID in (SELECT UserID FROM @ProgramManager)
      AND r.LockedFlag = 0
      AND r.EnabledFlag = 1
      AND sl.ProgramManagerUserID not in (SELECT UserID FROM @ProgramManager)

    UNION ALL
    
    SELECT 3,
           1,
           -- Root
           NULL,
           -- Active Work Queue
           NULL, NULL, NULL, NULL, NULL, 
           NULL, NULL, NULL, NULL, NULL, 
           NULL, NULL, NULL, NULL, NULL, 
           NULL, NULL, NULL, NULL, NULL,
           -- Reference
           UserID,
           NameFirst,
           NameLast
    FROM @ProgramManager
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDActiveWorkGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPMDActiveWorkGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/