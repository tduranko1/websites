-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLQueueGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLQueueGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLQueueGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL Queue
*
* PARAMETERS:  
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLQueueGetListXML
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    
    SET @ProcName = 'uspTLQueueGetListXML'
    SET @now = CURRENT_TIMESTAMP
    
    
    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        Null AS [Root!1!UserID],

        -- TL Queue
        NULL AS [TLQueue!2!ID],
        NULL AS [TLQueue!2!LynxID],
        NULL AS [TLQueue!2!TaskID],
        NULL AS [TLQueue!2!TaskName],
        NULL AS [TLQueue!2!LHName],
        NULL AS [TLQueue!2!VOName],
        NULL AS [TLQueue!2!DueDate],
        NULL AS [TLQueue!2!Locked],
        NULL AS [TLQueue!2!LockedUserID]
        
    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL,
            -- TL Queue
            id,
            LynxID,
            TaskID,
            TaskName,
            LHName,
            VOName,
            DueDate,
            Locked,
            LockedUserID
    FROM ufnTLQueueTasksGet(@now)
    
    ORDER BY TAG

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLQueueGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLQueueGetListXML TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/