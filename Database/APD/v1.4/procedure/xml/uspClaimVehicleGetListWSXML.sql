-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVehicleGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVehicleGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves a list of vehicles for the claim
*
* PARAMETERS:  
* (I) @LynxID               The Lynx ID to get vehicle list for
* (I) @InsuranceCompanyID   The Insurance company to validate against. 
*
* RESULT SET:
*   AN XML Universal Recordset containing vehicle/owner information.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClaimVehicleGetListWSXML]
    @LynxID                 udt_std_id_big,
    @InsuranceCompanyID     udt_std_id
AS

BEGIN
    -- Declare local variables

    DECLARE @ClaimAspectTypeID          udt_std_id
    DECLARE @InsuranceCompanyIDClaim    udt_std_id
    DECLARE @InsuranceCompanyName       varchar(100) --Project:210474 APD Added when we did the code merge M.A.20061114
    DECLARE @DefaultAssignmentUserID    udt_std_id --Project:210474 APD Added when we did the code merge M.A.20061114
    DECLARE @LossState                  varchar(50) --Project:210474 APD Added when we did the code merge M.A.20061114

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimVehicleGetListWSXML'


    -- Check to make sure a valid Lynx ID was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Get the Insurance Company Id for the claim
--Project:210474 APD Modified the SELECT stmnt below when we did the code merge M.A.20061114
    SELECT  @InsuranceCompanyIDClaim = c.InsuranceCompanyID,
            @InsuranceCompanyName = i.Name,
            @LossState = s.StateValue
      FROM  dbo.utb_claim  c
      LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
      LEFT JOIN dbo.utb_state_code s ON (c.LossState = s.StateCode)
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


    -- Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Owner')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Owner"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get the aspect type id for vehicle for use later
    
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @DefaultAssignmentUserID = value
    FROM dbo.utb_app_variable
    WHERE Name = 'Default_Assignment_User'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Begin XML Select

    SELECT
        1 as Tag,
        0 as Parent,
        
        -- Root
        @LynxID as [Root!1!LynxID],
        @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
        @InsuranceCompanyName   AS [Root!1!InsuranceCompanyName], --Project:210474 APD Added the column when we did the code merge M.A.20061114
        @DefaultAssignmentUserID AS [Root!1!DefaultAssignmentUserID],  --Project:210474 APD Added the column when we did the code merge M.A.20061114
        @LossState              AS [Root!1!LossState],  --Project:210474 APD Added the column when we did the code merge M.A.20061114

        -- VehicleList
        Null as [Vehicle!2!VehicleNumber],
        Null as [Vehicle!2!ClaimAspectID],
        NULL AS [Vehicle!2!CreatedDate],
        Null as [Vehicle!2!VehicleYear],
        Null as [Vehicle!2!Make],
        Null as [Vehicle!2!Model],
        Null as [Vehicle!2!VIN], --Project:210474 APD Added the column when we did the code merge M.A.20061114
        Null as [Vehicle!2!CurrentServiceChannelCD], --Project:210474 APD Added the column when we did the code merge M.A.20061114
        Null as [Vehicle!2!CurrentServiceChannelName], --Project:210474 APD Added the column when we did the code merge M.A.20061114
        Null as [Vehicle!2!NameFirst],
        Null as [Vehicle!2!NameLast],
        Null as [Vehicle!2!BusinessName],
        Null as [Vehicle!2!ClosedStatus],
        Null as [Vehicle!2!StatusID], --Project:210474 APD Added the column when we did the code merge M.A.20061114
        Null as [Vehicle!2!Status],
        Null as [Vehicle!2!ExposureCD],
        Null as [Vehicle!2!CoverageProfileCD],
        
        NULL AS [ClaimAspectOwner!3!UserID],
        NULL AS [ClaimAspectOwner!3!UserNameFirst],
        NULL AS [ClaimAspectOwner!3!UserNameLast],
        NULL AS [ClaimAspectOwner!3!UserPhoneAreaCode],
        NULL AS [ClaimAspectOwner!3!UserPhoneExchangeNumber],
        NULL AS [ClaimAspectOwner!3!UserPhoneUnitNumber],
        NULL AS [ClaimAspectOwner!3!UserExtensionNumber],
        NULL AS [ClaimAspectOwner!3!UserEmail], 
	--Project:210474 APD Added the columns below when we did the code merge M.A.20061114
        NULL AS [ClaimAspectOwner!3!AnalystUserID],
        NULL AS [ClaimAspectOwner!3!AnalystUserNameFirst],
        NULL AS [ClaimAspectOwner!3!AnalystUserNameLast],
        NULL AS [ClaimAspectOwner!3!AnalystUserPhoneAreaCode],
        NULL AS [ClaimAspectOwner!3!AnalystUserPhoneExchangeNumber],
        NULL AS [ClaimAspectOwner!3!AnalystUserPhoneUnitNumber],
        NULL AS [ClaimAspectOwner!3!AnalystUserExtensionNumber],
        NULL AS [ClaimAspectOwner!3!AnalystUserEmail],

        NULL AS [ClaimAspectOwner!3!SupportUserID],
        NULL AS [ClaimAspectOwner!3!SupportUserNameFirst],
        NULL AS [ClaimAspectOwner!3!SupportUserNameLast],
        NULL AS [ClaimAspectOwner!3!SupportUserPhoneAreaCode],
        NULL AS [ClaimAspectOwner!3!SupportUserPhoneExchangeNumber],
        NULL AS [ClaimAspectOwner!3!SupportUserPhoneUnitNumber],
        NULL AS [ClaimAspectOwner!3!SupportUserExtensionNumber],
        NULL AS [ClaimAspectOwner!3!SupportUserEmail]

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
        
        -- Root
        Null, Null, Null, Null, Null, --Project:210474 APD Added the columns when we did the code merge M.A.20061114
        
        --  Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        ISNULL(ca.CreatedDate, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull(cv.VIN, ''), --Project:210474 APD Added the column when we did the code merge M.A.20061114
        IsNull(uat.ServiceChannelDefaultCD, ''), --Project:210474 APD Added the column when we did the code merge M.A.20061114
        IsNull(uat.Name, ''), --Project:210474 APD Added the column when we did the code merge M.A.20061114
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.StatusID, --Project:210474 APD Added the column when we did the code merge M.A.20061114
        vs.Name,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, ''),
        
        -- ClaimAspectOwner
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--Project:210474 APD Added the following columns when we did the code merge M.A.20061114
        -- ClaimAspectAnalyst
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        -- ClaimAspectSupport
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
        

    FROM
        (SELECT @LynxID AS LynxID, @ClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID AND 1 = ca.EnabledFlag)
	LEFT OUTER JOIN utb_Assignment_Type uat on ca.InitialAssignmentTypeID = uat.AssignmentTypeID --Project:210474 APD Added reference to the table when we did the code merge M.A.20061114
        LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
        LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
        LEFT JOIN dbo.utb_status vs ON (cas.StatusID = vs.StatusID )
        --LEFT JOIN dbo.utb_status vs ON (ca.StatusID = vs.StatusID )
	WHERE vs.StatusTypeCD is null

    UNION ALL
    
    SELECT
        3,
        2,
        -- Root
        
        Null, Null, Null, Null, Null, --Project:210474 APD Added the columns when we did the code merge M.A.20061114
        
        --  Vehicle List
        ca.ClaimAspectNumber, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        
        -- ClaimAspectOwner
        IsNull(ca.OwnerUserID, ''),
        IsNull(uo.NameFirst, ''),
        IsNull(uo.NameLast, ''),
        IsNull(uo.PhoneAreaCode, ''),
        IsNull(uo.PhoneExchangeNumber, ''),
        IsNull(uo.PhoneUnitNumber, ''),
        IsNull(uo.PhoneExtensionNumber, ''),
        IsNull(uo.EmailAddress, ''),

        -- ClaimAspectAnalyst
        IsNull(ca.AnalystUserID, ''),
        IsNull(ua.NameFirst, ''),
        IsNull(ua.NameLast, ''),
        IsNull(ua.PhoneAreaCode, ''),
        IsNull(ua.PhoneExchangeNumber, ''),
        IsNull(ua.PhoneUnitNumber, ''),
        IsNull(ua.PhoneExtensionNumber, ''),
        IsNull(ua.EmailAddress, ''),

        -- ClaimAspectSupport
        IsNull(ca.SupportUserID, ''),
        IsNull(us.NameFirst, ''),
        IsNull(us.NameLast, ''),
        IsNull(us.PhoneAreaCode, ''),
        IsNull(us.PhoneExchangeNumber, ''),
        IsNull(us.PhoneUnitNumber, ''),
        IsNull(us.PhoneExtensionNumber, ''),
        IsNull(us.EmailAddress, '')
        
    FROM (SELECT @LynxID as LynxID, @ClaimAspectTypeID as ClaimAspectTypeID ) as parms 
           LEFT JOIN dbo.utb_claim_aspect ca ON parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID
           LEFT JOIN dbo.utb_user uo ON ca.OwnerUserID = uo.UserID
           LEFT JOIN dbo.utb_user ua ON ca.AnalystUserID = ua.UserID --Project:210474 APD Added reference to the table when we did the code merge M.A.2006111
           LEFT JOIN dbo.utb_user us ON ca.SupportUserID = us.UserID  --Project:210474 APD Added reference to the table when we did the code merge M.A.2006111
    
    Order by [Vehicle!2!VehicleNumber], tag
    FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVehicleGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspClaimVehicleGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/