-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFAdvSubroAuditGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFAdvSubroAuditGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCFAdvSubroAuditGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Jorge Rodrigues]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCFAdvSubroAuditGetXML
    @LynxID             udt_std_id_big,
    @ClaimAspectID      udt_std_id_big,
    @ServiceChannelCD   varchar(2),
    @UserID             udt_std_id_big
AS
BEGIN
    -- Declare internal variables
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @ClaimNumber as varchar(50)
    DECLARE @ClaimRepName as varchar(100)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)
    DECLARE @ClaimAspectNumber as tinyint
    DECLARE @VehicleYear as smallint
    DECLARE @VehicleMake as varchar(50)
    DECLARE @VehicleModel as varchar(50)
    DECLARE @ServerDateTime as varchar(25)


    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFAdvSubroAuditGetXML'

    SET @ServerDateTime = convert(varchar, CURRENT_TIMESTAMP, 101) + ' ' + convert(varchar, CURRENT_TIMESTAMP, 108)

    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    -- Check to make sure a valid user id was passed in
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    SELECT @ClaimRepName = isNull(uc.NameFirst + ' ' + uc.NameLast, ''),
           @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
           @AuditorTitle = 'Quality Control Representative',
           @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
           @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
           @ClaimAspectNumber = ca.ClaimAspectNumber
    FROM utb_claim_aspect ca 
    LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
    LEFT JOIN utb_user uc ON ca.OwnerUserID = uc.UserID
    WHERE ca.ClaimAspectID = @ClaimAspectID


    SELECT @InsuranceCompanyName = i.Name,
           @ClaimNumber = c.ClientClaimNumber
    FROM utb_claim c 
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID


    SELECT @VehicleYear = cv.VehicleYear,
           @VehicleMake = cv.Make,
           @VehicleModel = cv.Model
    FROM utb_claim_vehicle cv
    WHERE cv.ClaimAspectID = @ClaimAspectID

    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],
            @LynxID AS [Root!1!LynxID],
            @ClaimAspectNumber AS [Root!1!ClaimAspectNumber],
            @ClaimNumber AS [Root!1!ClaimNumber],
            @ClaimRepName AS [Root!1!ClaimRepresentative],
            @AuditorName AS [Root!1!AuditorName],
            @AuditorTitle AS [Root!1!AuditorTitle],
            @AuditorPhone AS [Root!1!AuditorPhone],
            @AuditorEmail AS [Root!1!AuditorEmail],
            @VehicleYear AS [Root!1!VehicleYear],
            @VehicleMake AS [Root!1!VehicleMake],
            @VehicleModel AS [Root!1!VehicleModel],
            @ServerDateTime AS [Root!1!ReportDate]

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFAdvSubroAuditGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFAdvSubroAuditGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/