USE udb_apd
-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves claim billing information for display
*
* PARAMETERS:  
* (I) @LynxID               Lynx ID of the claim
*
* RESULT SET:
*   XML Universal recordset detailing claim billing information
*
* VERSION:		V1.0 - Initial Development	
*				V1.1 - 11Jul2019 - TVD - Adding FeeCreditFlag to attributes
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


Create PROCEDURE [dbo].[uspInvoiceGetDetailWSXML]
    @LynxID     udt_std_id_big,
    @UserID     udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @NoPaymentWarningFlag             AS udt_std_flag

    DECLARE @ProcName                         AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                              AS udt_std_datetime
    DECLARE @ClaimIntakeEndDate               AS udt_std_datetime
    DECLARE @CSRNo                            AS udt_sys_login
    DECLARE @FeeItemTypeCD                    AS udt_std_cd
    DECLARE @LDAUValue                        AS varchar(10)
    DECLARE @LDAUShop                         AS udt_std_id_big    
    DECLARE @MEAppraiserID                    AS udt_std_id_big
    DECLARE @MEPayeeID                        AS udt_std_id_big
    DECLARE @EnterpriseIngresShopID           AS udt_std_id_big
    DECLARE @ClaimAspectTypeIDVehicle         AS udt_std_int_tiny
    DECLARE @InsuranceCompanyID               AS udt_std_id
    DECLARE @InsuranceCompanyName             AS varchar(100)
    
    DECLARE @ClaimAspectID_work               AS udt_std_id
    DECLARE @LastEstDocumentID_work           AS bigint
    DECLARE @LastEstAmt_work                  AS money
    DECLARE @LastEstTotalTaxAmt_work          AS money
    DECLARE @LastEstDeductibleAmt_work        AS money

    DECLARE @ApprovedEstDocumentID_work       AS bigint
    DECLARE @ApprovedEstAmt_work              AS money
    DECLARE @ApprovedEstTotalTaxAmt_work      AS money
    DECLARE @ApprovedEstDeductibleAmt_work    AS money
    
    DECLARE @EarlyBillFlag                    AS bit

    SET @ProcName = 'uspInvoiceGetDetailWSXML'    
    SET @now = CONVERT(datetime, convert(varchar, CURRENT_TIMESTAMP, 101)) --extract just the current date.
    SET @FeeItemTypeCD = (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') WHERE Name = 'Fee')

    -- Check to make sure a valid Lynx ID was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END
    
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    SELECT @ClaimIntakeEndDate = CONVERT(datetime, convert(varchar, IntakeFinishDate, 101)),
           @InsuranceCompanyID = i.InsuranceCompanyID,
           @InsuranceCompanyName = i.Name,
           @EarlyBillFlag = i.EarlyBillFlag
      FROM dbo.utb_claim c
      LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
      WHERE LynxID = @LynxID

  
    SELECT @CSRNo = LogonID
    FROM dbo.utb_user_application ua INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
    WHERE UserID = @UserID
      AND a.Name = 'APD'
    
    
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = 'Vehicle'
    
    
    
    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )
    
    
    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Invoice',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE (Table_Name = 'utb_invoice' AND Column_Name IN 
          ('InvoiceID',
          'AuthorizingUserID',
          'ClaimAspectID',
          'RecordingUserID',
          'Amount',
          'AuthorizedDate',
          'CarrierOfficeCode',
          'CarrierRepName',
          'ClientFeeCode',
          'Description',
          'DispatchNumber',
          'DocumentID',
          'EnabledFlag',
          'EntryDate',
          'FeeCategoryCD',
          'InsuredName',
          'InvoiceDate',
          'InvoiceDescription',
          'ItemizeFlag',
          'ItemTypeCD',
          'PayeeAddress1',
          'PayeeAddress2',
          'PayeeAddressCity',
          'PayeeAddressState',
          'PayeeAddressZip',
          'PayeeID',
          'PayeeName',
          'PayeeTypeCD',
          'PaymentChannelCD',
          'StatusCD',
          'StatusDate',
          'SystemAmount',
          'SysLastUserID'))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50)     NOT NULL,
        DisplayOrder        int                 NULL,  
        ReferenceID         varchar(10)     NOT NULL,
        Name                varchar(50)         NULL,
        ClaimAspectTypeID   int                 NULL,
        CategoryCD          varchar(4)          NULL,
        ClientCode          varchar(50)         NULL,    
        FeeInstructions     varchar(250)        NULL,
        ServiceID           int                 NULL,
        ItemizeFlag         char(1)             NULL,
        MultipleBillingFlag char(1)             NULL,
        RequiredFlag        char(1)             NULL,
        BillingModelCD      varchar(4)          NULL,
        ExposureRequiredFlag char(1)            NULL,
        PartyCD             varchar(4)          NULL,
        ServiceChannelCD    varchar(4)          NULL,
        DispositionTypeCD   varchar(4)          NULL
    )
    
    
    -- Select reference information for all pertinent reference tables and store in the
    -- temporary table    

    INSERT INTO @tmpReference (ListName, 
                               DisplayOrder, 
                               ReferenceID, 
                               Name, 
                               ClaimAspectTypeID, 
                               CategoryCD, 
                               ClientCode,
                               FeeInstructions, 
                               ServiceID, 
                               ItemizeFlag, 
                               MultipleBillingFlag,
                               RequiredFlag, 
                               BillingModelCD, 
                               ExposureRequiredFlag, 
                               PartyCD, 
                               ServiceChannelCD, 
                               DispositionTypeCD)
    
    -- Add all the Fees that apply during the Claim Creation
    SELECT  'ClientFee' AS ListName,
            cf.DisplayOrder AS DisplayOrder,
            CAST(cf.ClientFeeID AS Varchar),
            cf.Description,
            ClaimAspectTypeID, 
            CategoryCD,
            cf.ClientCode,
            IsNull(FeeInstructions, ''),
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
      FROM  dbo.utb_client_fee cf
      WHERE cf.InsuranceCompanyID = (SELECT InsuranceCompanyID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        AND (cf.EffectiveStartDate IS NULL or @ClaimIntakeEndDate >= cf.EffectiveStartDate)
        AND (cf.EffectiveEndDate IS NULL or @ClaimIntakeEndDate <= cf.EffectiveEndDate)
        AND AppliesToCD = 'C' -- Fees that apply during Claim Creation.
        AND cf.EnabledFlag = 1

    UNION ALL
    
    -- Add all the Fees that apply during the Billing Creation
    SELECT  'ClientFee' AS ListName,
            cf.DisplayOrder AS DisplayOrder,
            CAST(cf.ClientFeeID AS Varchar),
            cf.Description,
            ClaimAspectTypeID, 
            CategoryCD,
            cf.ClientCode,
            IsNull(FeeInstructions, ''),
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
      FROM  dbo.utb_client_fee cf
      WHERE cf.InsuranceCompanyID = (SELECT InsuranceCompanyID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        AND (cf.EffectiveStartDate IS NULL or @now >= cf.EffectiveStartDate)
        AND (cf.EffectiveEndDate IS NULL or @now <= cf.EffectiveEndDate)
        AND AppliesToCD = 'B' -- Fees that appluy during Billing Creation.
        AND cf.EnabledFlag = 1

    UNION ALL
    
    SELECT  'ClientFeeDefinition',
            NULL,
            CAST(cfd.ClientFeeID AS Varchar),
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            cfd.ServiceID,
            cfd.ItemizeFlag,
            NULL,
            cfd.RequiredFlag,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
      FROM  dbo.utb_client_fee_definition cfd
      INNER JOIN dbo.utb_client_fee cf ON (cfd.ClientFeeID = cf.ClientFeeID)
      WHERE cf.InsuranceCompanyID = (SELECT InsuranceCompanyID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        AND cf.EnabledFlag = 1                  
            
    UNION ALL
    
    SELECT  'Service',
            DisplayOrder,
            CAST(ServiceID AS Varchar),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            MultipleBillingFlag,
            NULL,
            IsNull(BillingModelCD, ''),
            ExposureRequiredFlag,
            IsNull(PartyCD, ''),
            IsNull(ServiceChannelCD, ''),
            IsNull(DispositionTypeCD, '')
      FROM  dbo.utb_service
      WHERE EnabledFlag = 1
        AND DisplayOrder IS NOT NULL  

    UNION ALL
    
    SELECT  'BillingModel',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_insurance', 'BillingModelCD')
       
    UNION ALL
    
    SELECT  'InvoiceMethod',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_insurance', 'InvoiceMethodCD')   
    
    UNION ALL
    
    SELECT  'InvoicingModel',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_insurance', 'InvoicingModelPaymentCD')

    UNION ALL
    
    SELECT  'Party',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_service', 'PartyCD')          

    UNION ALL
    
    SELECT  'ServiceChannelCD',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_service', 'ServiceChannelCD')
                       
    
    UNION ALL
    
    SELECT  'DispositionTypeCD',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_service', 'DispositionTypeCD')
       
    UNION ALL
    
    SELECT  'StatusCD',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
       
    UNION ALL
    
    SELECT  'ItemTypeCD',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD')
       
    UNION ALL
    
    SELECT  'PayeeTypeCD',
            NULL,
            Code,
            Name, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'PayeeTypeCD')
       
    UNION ALL
    
    SELECT 'CoverageProfile', 
           NULL,
           Code, 
           Name, 
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL
    FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'CoverageProfileCD')
                       
    UNION ALL
    
    SELECT 'ClientPaymentType', 
           NULL,
           rf.Code, 
           rf.Name, 
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL
    FROM dbo.utb_client_payment_type cpt
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_client_payment_type', 'PaymentTypeCD') rf ON cpt.PaymentTypeCD = rf.Code
    WHERE cpt.InsuranceCompanyID = @InsuranceCompanyID
    
    UNION ALL
    
    SELECT 'PaymentChannelCD', 
           NULL,
           Code, 
           Name, 
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL
    FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'PaymentChannelCD')
    
                       
    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
        
    
    
    -- Look up the Desk Audit unit information    
    SELECT  @LDAUShop = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
       
    
    -- Resolve Mobile Electronics Appraiser    
    SELECT  @MEAppraiserID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'Mobile_Electronics_AutoID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Resolve Mobile Electronics PayeeID    
    SELECT  @MEPayeeID = value
      FROM  dbo.utb_app_variable
      WHERE Name = 'Default_Payment_FCS_ShopLocationID'
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    

    SELECT @EnterpriseIngresShopID = value
      FROM  dbo.utb_app_variable
      WHERE Name = 'Default_Payment_Enterprise_ShopLocationID'
      
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
       
    DECLARE @tmpDocuments TABLE
    (
        DocumentID          bigint NOT NULL,
        DocumentTypeID      int    NOT NULL,
        ClaimAspectID       bigint NULL,
        ClaimAspectServiceChannelID bigint   NOT NULL,
        AgreedPriceMetCD    varchar(4) NULL,
        ApprovedFlag        bit        NULL,
        EstimateTypeFlag    bit        NULL
    )
    
    INSERT INTO @tmpDocuments
    SELECT d.DocumentID, 
           d.DocumentTypeID, 
           ca.ClaimAspectID, 
           casc.ClaimAspectServiceChannelID, 
           d.AgreedPriceMetCD,
           d.ApprovedFlag,
           dt.EstimateTypeFlag
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE c.LynxID = @LynxID
      AND d.EnabledFlag = 1
      AND dt.Name <> 'Note'
      --AND dt.EstimateTypeFlag = 1
      --AND d.ApprovedFlag = 1 
    
    
    -- Create temporary table to hold Payee Information
    DECLARE @tmpPayee TABLE
    (
        PayeeID                 bigint          NOT NULL,
        ClaimAspectID           bigint          NOT NULL,
        ClaimAspectServiceChannelID bigint      NULL,
        PayeeType               varchar(4)      NOT NULL,
        PayeeName               varchar(50)         NULL,
        PayeeAddress1           varchar(50)         NULL,
        PayeeAddress2           varchar(50)         NULL,
        PayeeCity               varchar(30)         NULL,
        PayeeState              char(2)             NULL,
        PayeeZip                varchar(8)          NULL,
        PayeePhone              varchar(15)         NULL,
        ActiveAssignmentFlag    bit                 NULL,
        ActiveWarrantyAssignmentFlag bit            NULL,
        RepairEndConfirmFlag    bit                 NULL,
        DocumentationComplete   bit                 NULL,
        MissingDocuments        varchar(500)        NULL,
        MissingDocumentsCompiledFlag bit            NULL,
        PayoffAmountConfirmedFlag    bit            NULL,
        LoGReceivedFlag              bit            NULL,
        FederalTaxIDExistsFlag       bit            NULL
    )
    
        
    INSERT INTO @tmpPayee (PayeeID, ClaimAspectID, ClaimAspectServiceChannelID, PayeeType, PayeeName, PayeeAddress1, 
                           PayeeAddress2, PayeeCity, PayeeState, PayeeZip, PayeePhone, ActiveAssignmentFlag, 
                           ActiveWarrantyAssignmentFlag, RepairEndConfirmFlag, DocumentationComplete, 
                           MissingDocumentsCompiledFlag)  
    
    -- Shops to which vehicle attached to this claim have been assigned.
    SELECT a.ShopLocationID, 
       ca.ClaimAspectID, 
       casc.ClaimAspectServiceChannelID,
       'S', 
       b.Name, 
       b.Address1, 
       b.Address2, 
       b.AddressCity, 
       b.AddressState, 
       b.AddressZip,
       b.PhoneAreaCode + '-' + b.PhoneExchangeNumber + '-' + b.PhoneUnitNumber,
       case
         when a.CancellationDate is null then 1
         else 0
       end,
       0,
       casc.WorkEndConfirmFlag,
       CASE
         WHEN EXISTS (SELECT rd.DocumentTypeID
                        FROM dbo.utb_client_required_document_type rd
                        WHERE rd.InsuranceCompanyID = @InsuranceCompanyID
                          AND (rd.ServiceChannelCD = 'PS' OR rd.ServiceChannelCD IS NULL)
                          AND rd.EnabledFlag = 1
                          AND rd.MandatoryFlag = 1
                          AND rd.WarrantyFlag = 0
                          AND rd.DocumentTypeID not in (SELECT DocumentTypeID
                                                         FROM @tmpDocuments td
                                                         WHERE td.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID))
                        THEN 0
         ELSE 1
       END,
       0
    
    FROM dbo.utb_claim_aspect ca 
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel table to provide the link between
					utb_Claim_Aspect and utb_Assignment
					M.A. 20061109
				*********************************************************************************/
				 INNER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
				 INNER JOIN dbo.utb_assignment a ON casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                                 INNER JOIN dbo.utb_shop_location s ON a.ShopLocationID = s.ShopLocationID   
                                 INNER JOIN dbo.utb_billing b ON s.BillingID = b.BillingID            
    WHERE ca.LynxID = @LynxID  
      --AND a.CancellationDate IS NULL
      AND s.ShopLocationID <> @LDAUShop
      
    UNION ALL
    
    -- Appraisers to which vehicle attached to this claim have been assigned.
    SELECT CASE
              WHEN a.AppraiserID = @MEAppraiserID THEN @MEPayeeID
              ELSE a.AppraiserID
           END,
           ca.ClaimAspectID,
           casc.ClaimAspectServiceChannelID,
           'A',
           a.Name, 
           a.Address1, 
           a.Address2, 
           a.AddressCity, 
           a.AddressState, 
           a.AddressZip,
           a.PhoneAreaCode + '-' + a.PhoneExchangeNumber + '-' + a.PhoneUnitNumber,
           case
             when ass.CancellationDate is null then 1
             else 0
           end,
           0,
           casc.WorkEndConfirmFlag,
           1,
           1
    FROM dbo.utb_claim_aspect ca 
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel table to provide the link between
					utb_Claim_Aspect and utb_Assignment
					M.A. 20061109
				*********************************************************************************/
				 INNER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
				 INNER JOIN dbo.utb_assignment ass ON casc.ClaimAspectServiceChannelID = ass.ClaimAspectServiceChannelID
                                 INNER JOIN dbo.utb_appraiser a ON ass.AppraiserID = a.AppraiserID
    WHERE ca.LynxID = @LynxID
      AND a.AppraiserID <> @LDAUShop
    
    UNION ALL
        
    -- Vendors with which the current client does business and could therefore potentially require a payment.
    SELECT @EnterpriseIngresShopID,
           ca.ClaimAspectID, 
           NULL,
           'V', 
           v.Name, 
           v.Address1, 
           v.Address2, 
           v.AddressCity, 
           v.AddressState, 
           v.AddressZip,
           v.PhoneAreaCode + '-' + v.PhoneExchangeNumber + '-' + v.PhoneUnitNumber,
           0,
           0,
           0,
           1,
           1
    
    FROM dbo.utb_claim c INNER JOIN dbo.utb_client_vendor cv ON c.InsuranceCompanyID = cv.InsuranceCompanyID
                         INNER JOIN dbo.utb_vendor v ON cv.VendorID = v.VendorID  
                         INNER JOIN dbo.utb_claim_aspect ca ON c.LynxID = ca.LynxID                               
    WHERE c.LynxID = @LynxID  
      AND v.EnabledFlag = 1    
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
    
    -- Add the active warranty shops for payee
    INSERT INTO @tmpPayee (PayeeID, ClaimAspectID, ClaimAspectServiceChannelID, PayeeType, PayeeName, PayeeAddress1, 
                           PayeeAddress2, PayeeCity, PayeeState, PayeeZip, PayeePhone, ActiveAssignmentFlag, 
                           ActiveWarrantyAssignmentFlag, RepairEndConfirmFlag, DocumentationComplete,
                           MissingDocumentsCompiledFlag)  
    
    -- Shops to which vehicle attached to this claim have been assigned.
    SELECT a.ShopLocationID, 
       ca.ClaimAspectID, 
       casc.ClaimAspectServiceChannelID,
       'S', 
       b.Name, 
       b.Address1, 
       b.Address2, 
       b.AddressCity, 
       b.AddressState, 
       b.AddressZip,
       b.PhoneAreaCode + '-' + b.PhoneExchangeNumber + '-' + b.PhoneUnitNumber,
       case
         when a.CancellationDate is null then 1
         else 0
       end,
       1,
       a.WarrantyEndConfirmFlag,
       CASE
         WHEN EXISTS (SELECT rd.DocumentTypeID
                        FROM dbo.utb_client_required_document_type rd
                        WHERE rd.InsuranceCompanyID = @InsuranceCompanyID
                          AND (rd.ServiceChannelCD = 'PS' OR rd.ServiceChannelCD IS NULL)
                          AND rd.EnabledFlag = 1
                          AND rd.MandatoryFlag = 1
                          AND rd.WarrantyFlag = 1
                          AND rd.DocumentTypeID not in (SELECT DocumentTypeID
                                                         FROM @tmpDocuments td
                                                         WHERE td.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID))
                        THEN 0
         ELSE 1
       END,
       0
    
    FROM dbo.utb_claim_aspect ca 
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel table to provide the link between
					utb_Claim_Aspect and utb_Assignment
					M.A. 20061109
				*********************************************************************************/
				 INNER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
				 INNER JOIN dbo.utb_warranty_assignment a ON casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
             INNER JOIN dbo.utb_shop_location s ON a.ShopLocationID = s.ShopLocationID   
             INNER JOIN dbo.utb_billing b ON s.BillingID = b.BillingID            
    WHERE ca.LynxID = @LynxID  
      AND a.CancellationDate IS NULL
      AND s.ShopLocationID <> @LDAUShop

    -- Add the active Lien Holder for payee
    INSERT INTO @tmpPayee (PayeeID, ClaimAspectID, ClaimAspectServiceChannelID, PayeeType, PayeeName, PayeeAddress1, 
                           PayeeAddress2, PayeeCity, PayeeState, PayeeZip, PayeePhone, ActiveAssignmentFlag,
                           PayoffAmountConfirmedFlag, LoGReceivedFlag, FederalTaxIDExistsFlag)  
    
    -- Shops to which vehicle attached to this claim have been assigned.
    SELECT lh.LienholderID, 
       ca.ClaimAspectID, 
       casc.ClaimAspectServiceChannelID,
       'S', 
       b.Name, 
       b.Address1, 
       b.Address2, 
       b.AddressCity, 
       b.AddressState, 
       b.AddressZip,
       b.PhoneAreaCode + '-' + b.PhoneExchangeNumber + '-' + b.PhoneUnitNumber,
       1,
       PayoffAmountConfirmedFlag,
       isNull((SELECT case 
                  when d.DocumentID > 0 then 1
                  else 0
               end
       FROM utb_claim_aspect_service_channel_document cascd
       LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
       LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
       WHERE cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
         AND dt.Name = 'Letter of Guarantee'), 0),
       case
         when FedTaxId is not null and len(ltrim(rtrim(FedTaxId))) > 0 then 1
         else 0
       end
    
    FROM dbo.utb_lien_holder lh
    INNER JOIN utb_Claim_Aspect_Service_Channel casc ON lh.LienHolderID = casc.LienHolderID
    INNER JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    INNER JOIN dbo.utb_billing b ON lh.BillingID = b.BillingID            
    WHERE ca.LynxID = @LynxID
      AND datediff(day, convert(datetime, convert(varchar,casc.CreatedDate, 101)), @now) >= 1  
                                                     

    DECLARE @PayeeCount as INT
    DECLARE @MissingDocuments as varchar(500)
    DECLARE @ClaimAspectServiceChannelID as bigint
    DECLARE @PayeeWarrantyFlag as bit
    DECLARE @LoopCount as int
    
    SELECT @PayeeCount = COUNT(PayeeID)
    FROM @tmpPayee
    
    SET @LoopCount = 100 -- restrict the loop so it does not runaway
    
    WHILE @PayeeCount > 0 AND @LoopCount > 0
    BEGIN
      SET @MissingDocuments = ''
      
      SELECT @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID,
             @PayeeWarrantyFlag = isNull(ActiveWarrantyAssignmentFlag, 0)
      FROM @tmpPayee
      WHERE DocumentationComplete = 0
        AND MissingDocumentsCompiledFlag = 0

      -- compile all the missing required documents 
      SELECT @MissingDocuments = @MissingDocuments + dt.Name + ', '
      FROM dbo.utb_client_required_document_type rd
      LEFT JOIN dbo.utb_document_type dt ON rd.DocumentTypeID = dt.DocumentTypeID
      WHERE rd.InsuranceCompanyID = @InsuranceCompanyID
        AND (rd.ServiceChannelCD = 'PS' OR rd.ServiceChannelCD IS NULL)
        AND rd.EnabledFlag = 1
        AND rd.MandatoryFlag = 1
        AND rd.WarrantyFlag = @PayeeWarrantyFlag
        AND rd.DocumentTypeID not in (SELECT DocumentTypeID
                                       FROM @tmpDocuments td
                                       WHERE td.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)

      -- remove the last comma
      IF LEN(@MissingDocuments) > 0 SET @MissingDocuments = SUBSTRING(@MissingDocuments, 1, LEN(@MissingDocuments) - 2)
      
      -- update the payee table with the missing documents
      UPDATE @tmpPayee
      SET MissingDocumentsCompiledFlag = 1,
          MissingDocuments = @MissingDocuments
      WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        AND MissingDocumentsCompiledFlag = 0
      
      SET @PayeeCount = @PayeeCount - 1
      SET @LoopCount = @LoopCount - 1
    END
    
    
    -- Need to determine whether to set the "No Payments Warning" Flag    
    IF (EXISTS(SELECT  d.DocumentID 
                 FROM  dbo.utb_claim_aspect ca
		/*********************************************************************************
		Project: 210474 APD - Enhancements to support multiple concurrent service channels
		Note:	Had to include utb_Claim_Aspect_Service_Channel to get to a column
			"ServiceChannelCD" used in the WHERE clause
			M.A. 20061109
		*********************************************************************************/
		 LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON casc.ClaimAspectID = ca.ClaimAspectID
                 LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID) 
                 LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                 LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
                 WHERE ca.LynxID = @LynxID
                   AND casc.ServiceChannelCD = 'PS'
                   AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                   AND ca.EnabledFlag = 1
                   AND dt.EstimateTypeFlag = 1
                   AND d.EnabledFlag = 1))    -- Program shop Estimates have been received for this claim
        AND (NOT EXISTS(SELECT  i.InvoiceID 
                          FROM  dbo.utb_claim_aspect ca 
			  /*********************************************************************************
			  Project: 210474 APD - Enhancements to support multiple concurrent service channels
			  Note:	Had to include utb_Claim_Aspect_Service_Channel to get to a column
				"ServiceChannelCD" used in the WHERE clause
				M.A. 20061109
			  *********************************************************************************/
			  LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON casc.ClaimAspectID = ca.ClaimAspectID
                          INNER JOIN dbo.utb_invoice i on (ca.ClaimAspectID = i.ClaimAspectID)
                          WHERE ca.LynxID = @LynxID
                            AND casc.ServiceChannelCD = 'PS'
                            AND ca.EnabledFlag = 1
                            AND i.ItemTypeCD <> 'F'
                            AND i.EnabledFlag = 1))   -- No payments have been made for these estimates
    BEGIN
        -- We have estimates, but no payments have been made against them.
        SET @NoPaymentWarningFlag = 1   -- Set the warning flag
    END
    ELSE
    BEGIN
        SET @NoPaymentWarningFlag = 0   -- Do not set the warning flag
    END
    
    -- get the open/closed claimAspects for the claim
    DECLARE @tmpExposures TABLE
    (
        ClaimAspectID                 bigint        NOT NULL,
        LatestEstimateDocumentID      bigint        NULL,
        LatestEstimateAmt             money         NULL,
        LatestEstimateTotalTaxAmt     money         NULL,
        LatestEstimateDeductibleAmt   money         NULL,
        ApprovedEstimateDocumentID    bigint        NULL,
        ApprovedEstimateAmt           money         NULL,
        ApprovedEstimateTotalTaxAmt   money         NULL,
        ApprovedEstimateDeductibleAmt money         NULL
    )


    -- Interested in only the vehicles.
    INSERT INTO @tmpExposures
    (ClaimAspectID)
    SELECT ClaimAspectID
    FROM    dbo.ufnUtilityGetClaimEntityList (@LynxID, 0, 0) cel    -- 0 All aspects, 0 - Open and closed
    WHERE name like 'Vehicle%'

    DECLARE csrExposures CURSOR FOR SELECT ClaimAspectID FROM @tmpExposures
     
    OPEN csrExposures

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    FETCH NEXT FROM csrExposures INTO @ClaimAspectID_work

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    WHILE @@Fetch_Status = 0
    BEGIN

      SET @LastEstDocumentID_work = NULL
      SET @LastEstAmt_work = NULL
      SET @LastEstTotalTaxAmt_work = NULL
      SET @LastEstDeductibleAmt_work = NULL
      
      SET @ApprovedEstDocumentID_work = NULL
      SET @ApprovedEstAmt_work = NULL
      SET @ApprovedEstTotalTaxAmt_work = NULL
      SET @ApprovedEstDeductibleAmt_work = NULL


      SELECT @LastEstAmt_work = OriginalExtendedAmt,
             @LastEstDocumentID_work = DocumentID
      FROM utb_claim_aspect ca
      LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
      LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'C', 'NetTotal', 'TT') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
      WHERE ca.ClaimAspectID = @ClaimAspectID_work
        AND casc.PrimaryFlag = 1
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
         
         RAISERROR  ('99|%s', 16, 1, @ProcName)
         RETURN
      END    

      SELECT @LastEstTotalTaxAmt_work = OriginalExtendedAmt
      FROM utb_claim_aspect ca
      LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
      LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'C', 'TaxTotal', 'TT') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
      WHERE ca.ClaimAspectID = @ClaimAspectID_work
        AND casc.PrimaryFlag = 1
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
         
         RAISERROR  ('99|%s', 16, 1, @ProcName)
         RETURN
      END    

      SELECT @LastEstDeductibleAmt_work = OriginalExtendedAmt
      FROM utb_claim_aspect ca
      LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
      LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'C', 'Deductible', 'AJ') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
      WHERE ca.ClaimAspectID = @ClaimAspectID_work
        AND casc.PrimaryFlag = 1
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
         
         RAISERROR  ('99|%s', 16, 1, @ProcName)
         RETURN
      END    
      
      IF @LastEstDeductibleAmt_work IS NULL
      BEGIN
         SELECT @LastEstDeductibleAmt_work = cc.DeductibleAmt
         FROM utb_claim_aspect ca
         LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
         LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc 
               on    ca.ClaimAspectID = casc.ClaimAspectID 
                 and casc.PrimaryFlag = 1
         LEFT JOIN utb_Client_Coverage_Type cct
               on    c.InsuranceCompanyID = cct.InsuranceCompanyID
                 and cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
                 and cct.CoverageProfileCD = ca.CoverageProfileCD
         LEFT OUTER JOIN utb_Claim_Coverage cc
               on    cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
                and  c.LynxID = cc.LynxID
                and  cc.CoverageTypeCD = cct.CoverageProfileCD
                and  cc.EnabledFlag = 1
                and  cc.AddtlCoverageFlag = 0 -- exclude the additional coverages
         LEFT OUTER JOIN   utb_claim_aspect_service_channel_coverage cascc
               on   cascc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                and cascc.ClaimCoverageID = cc.ClaimCoverageID
         where ca.ClaimAspectID = @ClaimAspectID_work
      END

      -- Now get the approved estimate data only when it pertains to early bill client
      IF @EarlyBillFlag = 1
      BEGIN
         SELECT @ApprovedEstAmt_work = OriginalExtendedAmt,
                @ApprovedEstDocumentID_work = DocumentID
         FROM utb_claim_aspect ca
         LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
         LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'A', 'NetTotal', 'TT') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
         WHERE ca.ClaimAspectID = @ClaimAspectID_work
           AND casc.PrimaryFlag = 1
         
         IF @@ERROR <> 0
         BEGIN
            -- SQL Server Error
            
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
         END    

         SELECT @ApprovedEstTotalTaxAmt_work = OriginalExtendedAmt
         FROM utb_claim_aspect ca
         LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
         LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'A', 'TaxTotal', 'TT') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
         WHERE ca.ClaimAspectID = @ClaimAspectID_work
           AND casc.PrimaryFlag = 1
         
         IF @@ERROR <> 0
         BEGIN
            -- SQL Server Error
            
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
         END    

         SELECT @ApprovedEstDeductibleAmt_work = OriginalExtendedAmt
         FROM utb_claim_aspect ca
         LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
         LEFT JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID_work, 'A', 'Deductible', 'AJ') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
         WHERE ca.ClaimAspectID = @ClaimAspectID_work
           AND casc.PrimaryFlag = 1
         
         IF @@ERROR <> 0
         BEGIN
            -- SQL Server Error
            
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
         END    
         
         IF @ApprovedEstDeductibleAmt_work IS NULL
         BEGIN
            SELECT @ApprovedEstDeductibleAmt_work = cc.DeductibleAmt
            FROM utb_claim_aspect ca
            LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
            LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc 
                  on    ca.ClaimAspectID = casc.ClaimAspectID 
                    and casc.PrimaryFlag = 1
            LEFT JOIN utb_Client_Coverage_Type cct
                  on    c.InsuranceCompanyID = cct.InsuranceCompanyID
                    and cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
                    and cct.CoverageProfileCD = ca.CoverageProfileCD
            LEFT OUTER JOIN utb_Claim_Coverage cc
                  on    cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
                   and  c.LynxID = cc.LynxID
                   and  cc.CoverageTypeCD = cct.CoverageProfileCD
                   and  cc.EnabledFlag = 1
                   and  cc.AddtlCoverageFlag = 0 -- exclude the additional coverages
            LEFT OUTER JOIN   utb_claim_aspect_service_channel_coverage cascc
                  on   cascc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                   and cascc.ClaimCoverageID = cc.ClaimCoverageID
            where ca.ClaimAspectID = @ClaimAspectID_work
         END

      END
      
      UPDATE @tmpExposures
      SET LatestEstimateAmt = @LastEstAmt_work,
          LatestEstimateTotalTaxAmt = @LastEstTotalTaxAmt_work,
          LatestEstimateDeductibleAmt = @LastEstDeductibleAmt_work,
          LatestEstimateDocumentID = @LastEstDocumentID_work,
          ApprovedEstimateAmt = @ApprovedEstAmt_work,
          ApprovedEstimateTotalTaxAmt = @ApprovedEstTotalTaxAmt_work,
          ApprovedEstimateDeductibleAmt = @ApprovedEstDeductibleAmt_work,
          ApprovedEstimateDocumentID = @ApprovedEstDocumentID_work
      WHERE ClaimAspectID = @ClaimAspectID_work                 

      IF @@ERROR <> 0
      BEGIN
         -- Update failure
      
         RAISERROR('104|%s|@tmpExposures', 16, 1, @ProcName)
         RETURN
      END

      FETCH NEXT FROM csrExposures INTO @ClaimAspectID_work
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
         
         RAISERROR  ('99|%s', 16, 1, @ProcName)
         RETURN
      END    

    END    

    CLOSE csrExposures
    DEALLOCATE csrExposures


    DECLARE @tmpApprovedEst TABLE
    (
        DocumentID          bigint NOT NULL,
        ClaimAspectID       bigint NULL,
        AgreedPriceMetCD    varchar(4) NULL
    )
    
    INSERT INTO @tmpApprovedEst
    SELECT d.DocumentID, d.ClaimAspectID, d.AgreedPriceMetCD
    FROM @tmpDocuments d
    WHERE EstimateTypeFlag = 1
      AND ApprovedFlag = 1
      
      
    /*INSERT INTO @tmpApprovedEst
    SELECT d.DocumentID, ca.ClaimAspectID, d.AgreedPriceMetCD
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE c.LynxID = @LynxID
      AND d.EnabledFlag = 1
      AND dt.EstimateTypeFlag = 1
      AND d.ApprovedFlag = 1 */

                                        
           
    -- Select Root Level
    SELECT 	1                           AS Tag,
            NULL                        AS Parent,
            @LynxID                     AS [Root!1!LynxID],
            @CSRNo                      AS [Root!1!CSRNo],
            i.BillingModelCD            AS [Root!1!BillingModelCD],
            i.InvoicingModelPaymentCD   AS [Root!1!InvoicingModelPaymentCD],
            i.InvoiceMethodCD           AS [Root!1!InvoiceMethodCD],
            @NoPaymentWarningFlag       AS [Root!1!NoPaymentWarningFlag],
            i.EarlyBillFlag             AS [Root!1!EarlyBillFlag],
            @InsuranceCompanyName       AS [Root!1!InsuranceCompanyName],
			ISNULL(i.FeeCreditFlag,0)   AS [Root!1!FeeCreditFlag],
            
            -- Invoice
            NULL AS [Invoice!2!InvoiceID],
            NULL AS [Invoice!2!AuthorizingUserName],
            NULL AS [Invoice!2!ClaimAspectID],
            NULL AS [Invoice!2!RecordingUserName],
            NULL AS [Invoice!2!Amount],
            NULL AS [Invoice!2!CarrierOfficeCode],
            NULL AS [Invoice!2!CarrierRepName],
            NULL AS [Invoice!2!ClientAuthorizesPaymentFlag],
            NULL AS [Invoice!2!ClientFeeCode],
            NULL AS [Invoice!2!DeductibleAmt],
            NULL AS [Invoice!2!Description],
            NULL AS [Invoice!2!DispatchNumber],
            NULL AS [Invoice!2!DocumentID],
            NULL AS [Invoice!2!EntryDate],
            NULL AS [Invoice!2!FeeCategoryCD],
            NULL AS [Invoice!2!InsuredName],
            NULL AS [Invoice!2!InvoicingModelBillingCD],
            NULL AS [Invoice!2!ItemizeFlag],
            NULL AS [Invoice!2!ItemTypeCD],
            NULL AS [Invoice!2!PayeeAddress1],
            NULL AS [Invoice!2!PayeeAddress2],
            NULL AS [Invoice!2!PayeeAddressCity],
            NULL AS [Invoice!2!PayeeAddressState],
            NULL AS [Invoice!2!PayeeAddressZip],
            NULL AS [Invoice!2!PayeeID],
            NULL AS [Invoice!2!PayeeName],
            NULL AS [Invoice!2!PayeeTypeCD],
            NULL AS [Invoice!2!PaymentChannelCD],
            NULL AS [Invoice!2!SentToIngresDate],
            NULL AS [Invoice!2!StatusCD],
            NULL AS [Invoice!2!StatusDate],
            NULL AS [Invoice!2!StatusDesc],
            NULL AS [Invoice!2!SystemAmount],
            NULL AS [Invoice!2!TaxTotalAmt],
            NULL AS [Invoice!2!Model],
            NULL AS [Invoice!2!SysLastUpdatedDate],
            
            -- Invoice Service
            NULL AS [InvoiceService!3!InvoiceID],
            NULL AS [InvoiceService!3!ServiceID],
            NULL AS [InvoiceService!3!PertainsToDescription],
            NULL AS [InvoiceService!3!ClaimAspectID],
            NULL AS [InvoiceService!3!ServiceChannelCD],
            NULL AS [InvoiceService!3!DispositionTypeCD],
            
            -- Claim Aspect
            NULL AS [ClaimAspect!4!ClaimAspectID],
            NULL AS [ClaimAspect!4!ClaimAspectNumber],
            NULL AS [ClaimAspect!4!ClaimAspectTypeID],
            NULL AS [ClaimAspect!4!ClientAuthorizesPaymentFlag],
            NULL AS [ClaimAspect!4!CoverageProfileCD],
            NULL AS [ClaimAspect!4!DispositionTypeCD],
            NULL AS [ClaimAspect!4!EntityCode],
            NULL AS [ClaimAspect!4!ExposureCD],
            NULL AS [ClaimAspect!4!InvoicingModelBillingCD],
            NULL AS [ClaimAspect!4!Name],
            NULL AS [ClaimAspect!4!ServiceChannelCD],
            NULL AS [ClaimAspect!4!AssignmentTypeServiceChannelCD],
            NULL AS [ClaimAspect!4!Status],
            NULL AS [ClaimAspect!4!NoPaymentWarningFlag],
            NULL AS [ClaimAspect!4!LatestEstimateDocumentID],
            NULL AS [ClaimAspect!4!LatestEstimateTotal],
            NULL AS [ClaimAspect!4!LatestEstimateTotalTax],
            NULL AS [ClaimAspect!4!LatestEstimateDeductible],
            NULL AS [ClaimAspect!4!ApprovedEstimateDocumentID],
            NULL AS [ClaimAspect!4!ApprovedEstimateTotal],
            NULL AS [ClaimAspect!4!ApprovedEstimateTotalTax],
            NULL AS [ClaimAspect!4!ApprovedEstimateDeductible],
            NULL AS [ClaimAspect!4!WorkStartConfirmFlag],
            NULL AS [ClaimAspect!4!ApprovedDocumentCount],
            NULL AS [ClaimAspect!4!AgreedEstimateCount],
            NULL AS [ClaimAspect!4!WarrantyExistsFlag],
            
            -- Claim Aspect Service Channel
            NULL AS [ClaimAspectServiceChannel!5!ClaimAspectID],
            NULL AS [ClaimAspectServiceChannel!5!ClaimAspectServiceChannelID],
            NULL AS [ClaimAspectServiceChannel!5!ServiceChannelCD],
            NULL AS [ClaimAspectServiceChannel!5!DispositionCD],
            NULL AS [ClaimAspectServiceChannel!5!LetterOfGuaranteeAmount],

            -- Payee                        
            NULL AS [Payee!6!PayeeID], 
            NULL AS [Payee!6!ClaimAspectID], 
            NULL AS [Payee!6!PayeeTypeCD], 
            NULL AS [Payee!6!PayeeName], 
            NULL AS [Payee!6!PayeeAddress1], 
            NULL AS [Payee!6!PayeeAddress2], 
            NULL AS [Payee!6!PayeeAddressCity], 
            NULL AS [Payee!6!PayeeAddressState], 
            NULL AS [Payee!6!PayeeAddressZip], 
            NULL AS [Payee!6!PayeePhone], 
            NULL AS [Payee!6!PayeeActiveAssignmentFlag], 
            NULL AS [Payee!6!PayeeWarrantyActiveAssignmentFlag], 
            NULL AS [Payee!6!PayeeRepairEndConfirmFlag],
            NULL AS [Payee!6!PayeeDocumentationCompleteFlag],
            NULL AS [Payee!6!PayeeMissingDocuments],
            NULL AS [Payee!6!PayoffAmountConfirmedFlag],
            NULL AS [Payee!6!LoGReceivedFlag],
            NULL AS [Payee!6!FederalTaxIDExistsFlag],
            
            -- Metadata Header
            NULL AS [Metadata!7!Entity],
            
            -- Metadata
            NULL AS [Column!8!Name],
            NULL AS [Column!8!DataType],
            NULL AS [Column!8!MaxLength],
            NULL AS [Column!8!Precision],
            NULL AS [Column!8!Scale],
            NULL AS [Column!8!Nullable],
            
            -- Reference Data
            NULL AS [Reference!9!List],
            NULL AS [Reference!9!ReferenceID],
            NULL AS [Reference!9!Name],
            NULL AS [Reference!9!DisplayOrder],
            NULL AS [Reference!9!ClaimAspectTypeID],
            NULL AS [Reference!9!CategoryCD],
            NULL AS [Reference!9!FeeInstructions],
            NULL AS [Reference!9!ServiceID],
            NULL AS [Reference!9!ItemizeFlag],
            NULL AS [Reference!9!MultipleBillingFlag],
            NULL AS [Reference!9!RequiredFlag],
            NULL AS [Reference!9!BillingModelCD],
            NULL AS [Reference!9!ExposureRequiredFlag],
            NULL AS [Reference!9!PartyCD],
            NULL AS [Reference!9!ServiceChannelCD],
            NULL AS [Reference!9!DispositionTypeCD]
            
    FROM    dbo.utb_claim c
    LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    WHERE   c.LynxID = @LynxID
     

    UNION ALL

    -- Select Invoice Level

    SELECT 	2,
            1,
            @LynxID AS [Root!1!LynxID],
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            ISNULL(i.InvoiceID, 0),
            LTRIM(RTRIM(ISNULL(u.NameFirst, '') + ' ' + ISNULL(u.NameLast, ''))),
            i.ClaimAspectID,
            LTRIM(RTRIM(ISNULL(u2.NameFirst, '') + ' ' + ISNULL(u2.NameLast, ''))),
            ISNULL(convert(varchar(50), i.Amount), ''),
            ISNULL(i.CarrierOfficeCode, ''),
            ISNULL(i.CarrierRepName, ''),
            ISNULL(cat.ClientAuthorizesPaymentFlag, '0'),
            ISNULL(i.ClientFeeCode, ''),
            ISNULL(convert(varchar(50), i.DeductibleAmt), ''),
            ISNULL(i.Description, ''),
            ISNULL(i.DispatchNumber, ''),
            ISNULL(i.DocumentID, ''),
            ISNULL(i.EntryDate, ''),
            ISNULL(i.FeeCategoryCD, ''),
            ISNULL(REPLACE(i.InsuredName, '&', ''), ''),
            ISNULL(cat.InvoicingModelBillingCD, ''),
            ISNULL(i.ItemizeFlag, '1'),
            ISNULL(i.ItemTypeCD, ''),
            ISNULL(i.PayeeAddress1, ''),
            ISNULL(i.PayeeAddress2, ''),
            ISNULL(i.PayeeAddressCity, ''),
            ISNULL(i.PayeeAddressState, ''),
            ISNULL(i.PayeeAddressZip, ''),
            ISNULL(i.PayeeID, ''),
            ISNULL(i.PayeeName, ''),
            ISNULL(i.PayeeTypeCD, ''),
            ISNULL(i.PaymentChannelCD, ''),
            ISNULL(i.SentToIngresDate, ''),
            ISNULL(i.StatusCD, ''),
            ISNULL(i.StatusDate, ''),
            ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD') WHERE Code = i.StatusCD), ''),
            ISNULL(i.SystemAmount, 0),
            ISNULL(convert(varchar(50), i.TaxTotalAmt), ''),
            CASE
               WHEN i.ItemTypeCD = 'F' THEN (SELECT cscf.InvoicingModelBillingCD
                                             FROM dbo.utb_claim_aspect_service_channel cascf
                                             INNER JOIN dbo.utb_client_service_channel cscf ON (cscf.InsuranceCompanyID = @InsuranceCompanyID AND cscf.ServiceChannelCD = cascf.ServiceChannelCD)
                                             WHERE cascf.ClaimAspectServiceChannelID = i.ClaimAspectServiceChannelID)
               ELSE (SELECT insp.InvoicingModelPaymentCD
                     FROM dbo.utb_insurance insp
                     WHERE insp.InsuranceCompanyID = @InsuranceCompanyID)
            END,
            dbo.ufnUtilityGetDatestring(i.SyslastUpdatedDate),
            
            -- Invoice Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            NULL,
            
            -- Payee
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM    (SELECT @LynxID AS LynxID) AS parms
    INNER JOIN dbo.utb_claim_aspect ca ON parms.LynxID = ca.LynxID
    INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    /*--Project:210474 APD Removed to support the schema change (ca.CurrentAssignmentTypeID was removed) M.A.20070109
    LEFT JOIN dbo.utb_client_assignment_type cat ON c.InsuranceCompanyID = cat.InsuranceCompanyID 
                                                AND ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
    */
    --Project:210474 APD Added the following to support the schema change (utb_Client_Service_Channel was added) M.A.20070109
    left outer JOIN
    (
    select      c.LynxID,
                c.InsuranceCompanyID,
                ca.ClaimAspectID,
                ca.ClaimAspectTypeID,
                casc.ClaimAspectServiceChannelID,
                casc.ServiceChannelCD,
                csc.ClientAuthorizesPaymentFlag,
                csc.InvoicingModelBillingCD
    
    
    from       utb_Claim_Aspect_Service_Channel casc
    
    inner join  utb_Claim_Aspect ca
    on          ca.ClaimAspectID = casc.ClaimAspectID
    and         casc.PrimaryFlag = 1 -- pick only the primary service channel for the claim
    and         casc.EnabledFlag = 1 -- Only pick the enabled service channel
    
    inner join  utb_Claim c
    on          c.LynxID = ca.LynxID
    
    inner join  utb_client_service_channel csc
    on          c.InsuranceCompanyID = csc.InsuranceCompanyID
    and         casc.ServiceChannelCD = csc.ServiceChannelCD
    ) cat
    on          ca.LynxID  = cat.LynxID
    and         ca.ClaimAspectID = cat.ClaimAspectID
    --Project:210474 APD Added the above to support the schema change (utb_Client_Service_Channel was added) M.A.20070109

    INNER JOIN dbo.utb_invoice i ON (ca.ClaimAspectID = i.ClaimAspectID AND 1 = i.EnabledFlag)
    LEFT JOIN dbo.utb_user u ON (i.AuthorizingUserID = u.UserID)
    LEFT JOIN dbo.utb_user u2 ON (i.RecordingUserID = u2.UserID)
    
    UNION ALL

    -- Select Invoice Service Level

    SELECT 	3,
            2,
            @LynxID AS [Root!1!LynxID],
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            IsNull(i.InvoiceID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            ISNULL(isv.InvoiceID, 0),
            IsNull(isv.ServiceID, 0),
            IsNull(dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 0), ''),
            ca.ClaimAspectID,
            IsNull(isv.ServiceChannelCD, ''),
            IsNull(isv.DispositionTypeCD, ''),
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            NULL,
            
            -- Payee
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM (SELECT @LynxID AS LynxID) AS parms  
          INNER JOIN dbo.utb_claim_aspect ca ON parms.LynxID = ca.LynxID
          INNER JOIN dbo.utb_invoice i ON ca.ClaimAspectID = i.ClaimAspectID 
          INNER JOIN dbo.utb_invoice_service isv ON i.InvoiceID = isv.InvoiceID 
    WHERE i.EnabledFlag = 1
      AND isv.EnabledFlag = 1
      AND i.ItemTypeCD = @FeeItemTypeCD          

    UNION ALL

    -- Select Claim Aspect Level
    SELECT  distinct 4,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            ISNULL(cel.ClaimAspectID, ''),
            ISNULL(ca.ClaimAspectNumber, 0),
            ISNULL(ca.ClaimAspectTypeID, ''),
            ISNULL(cat.ClientAuthorizesPaymentFlag, ''),
            ISNULL(ca.CoverageProfileCD, ''), 
            ISNULL(casc.DispositionTypeCD, ''),
            ISNULL(cel.EntityCode, ''),
            ISNULL(ca.ExposureCD, ''),
            ISNULL(cat.InvoicingModelBillingCD, ''),
            ISNULL(cel.Name, ''),
            ISNULL(casc.ServiceChannelCD, ''),
            --ISNULL(at.ServiceChannelDefaultCD, ''),    --Project:210474 APD removed the following to support the schema change (utb_Client_Service_Channel was added) M.A.20070109
            ISNULL(cat.ServiceChannelCD, ''),
            ISNULL(s.Name, ''),
            
            (CASE     -- NoPaymentsWarningFlag
                WHEN (EXISTS(SELECT  d.DocumentID 
                             FROM  dbo.utb_claim_aspect ca 
				  /*********************************************************************************
				  Project: 210474 APD - Enhancements to support multiple concurrent service channels
				  Note:	Had to include utb_Claim_Aspect_Service_Channel to get to a column
					"ServiceChannelCD" used in the WHERE clause
					M.A. 20061109
				  *********************************************************************************/
				   LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
                                   LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID) --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                   LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                                   LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
                             WHERE ca.ClaimAspectID = cel.ClaimAspectID
                               AND casc.ServiceChannelCD = 'PS'
                               AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                               AND dt.EstimateTypeFlag = 1
                               AND d.EnabledFlag = 1))    -- Program shop Estimates have been received for this vehicle
                      AND (NOT EXISTS(SELECT  i.InvoiceID 
                                      FROM  dbo.utb_invoice i
                                      WHERE i.ClaimAspectID = cel.ClaimAspectID
                                        AND i.ItemTypeCD <> 'F'
                                        AND i.EnabledFlag = 1))   -- No payments have been made for this vehicle
                       THEN 1
                ELSE 0
              END),
              -- Latest estimate total
              IsNull(convert(varchar, tmpExp.LatestEstimateDocumentID), ''),
              IsNull(convert(varchar, tmpExp.LatestEstimateAmt), ''),
              IsNull(convert(varchar, tmpExp.LatestEstimateTotalTaxAmt), ''),
              IsNull(convert(varchar, tmpExp.LatestEstimateDeductibleAmt), ''),
              IsNull(convert(varchar, tmpExp.ApprovedEstimateDocumentID), ''),
              IsNull(convert(varchar, tmpExp.ApprovedEstimateAmt), ''),
              IsNull(convert(varchar, tmpExp.ApprovedEstimateTotalTaxAmt), ''),
              IsNull(convert(varchar, tmpExp.ApprovedEstimateDeductibleAmt), ''),
              casc.WorkStartConfirmFlag,
             (SELECT COUNT(DocumentID)
                FROM @tmpApprovedEst t
                WHERE t.ClaimAspectID = ca.ClaimAspectID),
             (SELECT COUNT(DocumentID)
                FROM @tmpApprovedEst t
                WHERE t.AgreedPriceMetCD = 'Y'),
             CASE 
               WHEN EXISTS(SELECT wa.AssignmentID
                           FROM utb_warranty_assignment wa
                           WHERE wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                             AND wa.CancellationDate IS NULL) THEN 1
               ELSE 0 
             END,
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Payee
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            NULL,
            
            -- Metadata
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM    dbo.ufnUtilityGetClaimEntityList (@LynxID, 0, 0) cel    -- 0 All aspects, 0 - Open and closed

            LEFT JOIN dbo.utb_claim_aspect ca ON cel.ClaimAspectID = ca.ClaimAspectID
		/*********************************************************************************
		Project: 210474 APD - Enhancements to support multiple concurrent service channels
		Note:	Had to add utb_Claim_Aspect_Status to provide a join between
			utb_claim_aspect and utb_Status tables.
			M.A. 20061109
		*********************************************************************************/
	        LEFT OUTER JOIN utb_Claim_Aspect_Status cas 
            ON              cas.ClaimAspectID = ca.ClaimAspectID
            LEFT JOIN       dbo.utb_status s 
            ON              cas.StatusID = s.StatusID
            LEFT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
		/*********************************************************************************
		Project: 210474 APD - Enhancements to support multiple concurrent service channels
		Note:	Had to add utb_Claim_Aspect_Service_Channel to provide a ServiceChannelCD
			M.A. 20061109
		*********************************************************************************/
	    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
            /*--Project:210474 APD Removed to support the schema change (ca.CurrentAssignmentTypeID was removed) M.A.20070109
            LEFT JOIN dbo.utb_assignment_type at ON ca.CurrentAssignmentTypeID = at.AssignmentTypeID
            LEFT JOIN dbo.utb_client_assignment_type cat ON ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
                                                        AND c.InsuranceCompanyID = cat.InsuranceCompanyID
            */
        --Project:210474 APD Added the following to support the schema change (utb_Client_Service_Channel was added) M.A.20070109
        LEFT OUTER JOIN    (
                            select      c.LynxID,
                                        c.InsuranceCompanyID,
                                        ca.ClaimAspectID,
                                        ca.ClaimAspectTypeID,
                                        casc.ClaimAspectServiceChannelID,
                                        casc.ServiceChannelCD,
                                        csc.ClientAuthorizesPaymentFlag,
                                        csc.InvoicingModelBillingCD
                            
                            
                            from       utb_Claim_Aspect_Service_Channel casc
                            
                            inner join  utb_Claim_Aspect ca
                            on          ca.ClaimAspectID = casc.ClaimAspectID
                            and         casc.PrimaryFlag = 1 -- pick only the primary service channel for the claim
                            and         casc.EnabledFlag = 1 -- Only pick the enabled service channel
                            
                            inner join  utb_Claim c
                            on          c.LynxID = ca.LynxID
                            
                            inner join  utb_client_service_channel csc
                            on          c.InsuranceCompanyID = csc.InsuranceCompanyID
                            and         casc.ServiceChannelCD = csc.ServiceChannelCD
                           )cat
        on                cat.LynxID = ca.LynxID
        and               cat.ClaimAspectID = ca.ClaimAspectID
        left join @tmpExposures tmpExp ON ca.ClaimAspectID = tmpExp.ClaimAspectID
        --Project:210474 APD Added the above to support the schema change (utb_Client_Service_Channel was added) M.A.20070109
        where    cas.ServiceChannelCD is null 
        and      cas.StatusTypeCD is null
        and      (casc.PrimaryFlag = 1 OR ca.ClaimAspectTypeID = 0)

    UNION ALL

    -- Select Claim Aspect Service Channel Level
    SELECT DISTINCT 
            5,
            4,
            --Root
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            isNull(cel.ClaimAspectID, ''), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            isNull(casc.ClaimAspectID, ''),
            isNull(casc.ClaimAspectServiceChannelID, ''),
            isNull(casc.ServiceChannelCD, ''),
            isNull(casc.DispositionTypeCD, ''),
            isNull(convert(varchar,casc.LetterOfGuaranteeAmount), ''),
            
            -- Payee            
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            NULL,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM    dbo.ufnUtilityGetClaimEntityList (@LynxID, 0, 0) cel    -- 0 All aspects, 0 - Open and closed
    left join utb_claim_aspect_service_channel casc ON cel.ClaimAspectID = casc.ClaimAspectID
    WHERE casc.EnabledFlag = 1

    UNION ALL
    
    -- Select Payee Level
    SELECT DISTINCT 
            6,
            1,
            --Root
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Payee            
            ISNULL(PayeeID, ''),
            ISNULL(ClaimAspectID, ''),
            ISNULL(PayeeType, ''), 
            ISNULL(PayeeName, ''), 
            ISNULL(PayeeAddress1, ''),
            ISNULL(PayeeAddress2, ''),
            ISNULL(PayeeCity, ''),
            ISNULL(PayeeState, ''),
            ISNULL(PayeeZip, ''),
            ISNULL(PayeePhone, ''),
            ActiveAssignmentFlag,
            ActiveWarrantyAssignmentFlag,
            RepairEndConfirmFlag,
            DocumentationComplete,
            MissingDocuments,
            PayoffAmountConfirmedFlag,
            LoGReceivedFlag,
            FederalTaxIDExistsFlag,

            -- Metadata Header
            NULL,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM @tmpPayee

    UNION ALL

    -- Select Metadata Header Level
    SELECT DISTINCT 
            7,
            1,
            --Root
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Payee            
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata

    UNION ALL

    -- Select Column Metadata Level

    SELECT  8,
            7,
            --Root
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Payee
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata

    UNION ALL

    -- Select Reference Data Level

    SELECT  9,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Billing Service
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspects
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Aspect Service Channel
            NULL, NULL, NULL, NULL, NULL,
            
            -- Payee
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            'ZZ-Reference',
            
            -- Metadata
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            DisplayOrder,
            ClaimAspectTypeID,
            CategoryCD,
            FeeInstructions,
            ServiceID,
            ItemizeFlag,
            MultipleBillingFlag,
            RequiredFlag,
            BillingModelCD,
            ExposureRequiredFlag,
            PartyCD,
            ServiceChannelCD,
            DispositionTypeCD

    FROM    @tmpReference

    
    ORDER BY [Invoice!2!InvoiceID], [ClaimAspect!4!ClaimAspectID], Tag
    FOR XML EXPLICIT      

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspInvoiceGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/