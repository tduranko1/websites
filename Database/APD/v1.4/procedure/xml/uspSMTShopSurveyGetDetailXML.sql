-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopSurveyGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopSurveyGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves Shop survey stuff
*
* PARAMETERS:  
* (I) ClaimAspectID        
*
* RESULT SET:
* An XML Data stream containing payment information
*
*
* VSS
* $Workfile: uspSMTShopSurveyGetDetailXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspSMTShopSurveyGetDetailXML.SQL $
* $Revision: 1 $
* $Author: Dan Price $
* $Date: 10/23/01 5:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopSurveyGetDetailXML
    @ShopLocationID         udt_std_int_big
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts
    DECLARE @now                AS datetime
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    --DECLARE @LynxID             AS udt_std_id_big
    

    SET @ProcName = 'uspSMTShopSurveyGetDetailXML'
    SET @now = CURRENT_TIMESTAMP

    -- Check to make sure a ClaimAspect ID was passed in
    IF  (@ShopLocationID IS NULL) OR 
        ((SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID) IS NULL)
    BEGIN
        -- Invalid Location ID
        RAISERROR('101|%s|@ShopLocationID|%s', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END

        
    

    -- initialize local variables
    --SET @LynxID = (SELECT LynxID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID)
    

       
    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )
    
    
    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'ShopSurvey',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop_location_survey' AND Column_Name IN 
            ('ShopLocationID',
             'Age',
             'ASEFlag',
             'BusinessRegsFlag',
             'BusinessStabilityFlag',
             'CommercialLiabilityFlag',
             'CompetentEstimatorsFlag',
             'CustomaryHoursFlag',
             'DigitalPhotosFlag',
             'ElectronicEstimatesFlag',
             'EFTFlag',
             'EmployeeEducationFlag',
             'EmployerLiabilityFlag',
             'FrameDiagnosticEquipFlag',
             'FrameAnchoringPullingEquipFlag',
             'HazMatFlag',
             'HydraulicLiftFlag',
             'ICARFlag',
             'MechanicalRepairFlag',
             'MIGWeldersFlag',
             'MinimumWarrantyFlag',
             'NoFeloniesFlag',
             'ReceptionistFlag',
             'RefinishCapabilityFlag',
             'RefinishTrainedFlag',
             'SecureStorageFlag',
             'ValidEPALicenseFlag',
             'VANInetFlag',
             'WorkersCompFlag'))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

   
    -- Select Root Level
    SELECT 	1                           AS Tag,
            NULL                        AS Parent,
            --Root
            @ShopLocationID             AS [Root!1!ShopID],
            
            -- Survey
            NULL                        AS [Survey!2!Age],
            NULL                        AS [Survey!2!ASEFlag],
            NULL                        AS [Survey!2!BusinessRegsFlag],
            NULL                        AS [Survey!2!BusinessStabilityFlag],
            NULL                        AS [Survey!2!CommercialLiabilityFlag],
            NULL                        AS [Survey!2!CompetentEstimatorsFlag],
            NULL                        AS [Survey!2!CustomaryHoursFlag],
            NULL                        AS [Survey!2!DigitalPhotosFlag],
            NULL                        AS [Survey!2!ElectronicEstimatesFlag],
            NULL                        AS [Survey!2!EFTFlag],
            NULL                        AS [Survey!2!EmployeeEducationFlag],
            NULL                        AS [Survey!2!EmployerLiabilityFlag],
            NULL                        AS [Survey!2!FrameDiagnosticEquipFlag],
            NULL                        AS [Survey!2!FrameAnchoringPullingEquipFlag],
            NULL                        AS [Survey!2!HazMatFlag],
            NULL                        AS [Survey!2!HydraulicLiftFlag],
            NULL                        AS [Survey!2!ICARFlag],
            NULL                        AS [Survey!2!MechanicalRepairFlag],
            NULL                        AS [Survey!2!MIGWeldersFlag],
            NULL                        AS [Survey!2!MinimumWarrantyFlag],
            NULL                        AS [Survey!2!NoFeloniesFlag],
            NULL                        AS [Survey!2!ReceptionistFlag],
            NULL                        AS [Survey!2!RefinishCapabilityFlag],
            NULL                        AS [Survey!2!RefinishTrainedFlag],
            NULL                        AS [Survey!2!SecureStorageFlag],
            NULL                        AS [Survey!2!ValidEPALicenseFlag],
            NULL                        AS [Survey!2!VANInetFlag],
            NULL                        AS [Survey!2!WorkersCompFlag],
            NULL                        AS [Survey!2!SysLastUpdatedDate],
            
            -- Metadata Header
            NULL                        AS [Metadata!3!Entity],
            
            -- Columns
            NULL                        AS [Column!4!Name],
            NULL                        AS [Column!4!DataType],
            NULL                        AS [Column!4!MaxLength],
            NULL                        AS [Column!4!Precision],
            NULL                        AS [Column!4!Scale],
            NULL                        AS [Column!4!Nullable]


    UNION ALL

    -- Survey Level

    SELECT  2,
            1,
            --Root
            NULL,
            -- Survey
            IsNull(s.Age, ''),
            IsNull(convert(char, s.ASEFlag), ''),
            IsNull(s.BusinessRegsFlag, '0'),
            IsNull(s.BusinessStabilityFlag, '0'),
            IsNull(s.CommercialLiabilityFlag, '0'),
            IsNull(s.CompetentEstimatorsFlag, '0'),
            IsNull(s.CustomaryHoursFlag, '0'),
            IsNull(s.DigitalPhotosFlag, '0'),
            IsNull(s.ElectronicEstimatesFlag, '0'),
            IsNull(s.EFTFlag, '0'),
            IsNull(s.EmployeeEducationFlag, '0'),
            IsNull(s.EmployerLiabilityFlag, '0'),
            IsNull(s.FrameDiagnosticEquipFlag, '0'),
            IsNull(s.FrameAnchoringPullingEquipFlag, '0'),
            IsNull(s.HazMatFlag, '0'),
            IsNull(convert(char, s.HydraulicLiftFlag), ''),
            IsNull(convert(char, s.ICARFlag), ''),
            IsNull(s.MechanicalRepairFlag, '0'),
            IsNull(s.MIGWeldersFlag, '0'),
            IsNull(s.MinimumWarrantyFlag, '0'),
            IsNull(s.NoFeloniesFlag, '0'),
            IsNull(s.ReceptionistFlag, '0'),
            IsNull(s.RefinishCapabilityFlag, '0'),
            IsNull(convert(char, s.RefinishTrainedFlag), ''),
            IsNull(convert(char, s.SecureStorageFlag), ''),
            IsNull(convert(char, s.ValidEPALicenseFlag), ''),
            IsNull(s.VANInetFlag, '0'),
            IsNull(s.WorkersCompFlag, '0'),
            dbo.ufnUtilityGetDateString(s.SysLastUpdatedDate),
            
                        
            -- Metadata Header
            NULL,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL
    
    FROM    (SELECT @ShopLocationID as ShopLocationID) as parms
              LEFT JOIN dbo.utb_shop_location_survey s ON parms.ShopLocationID = s.ShopLocationID
              
       
    UNION ALL

    
    -- Select Metadata Header Level

    SELECT DISTINCT 
            3,
            1,
            --Root
            NULL,
            
            -- Survey
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  4,
            3,
            --Root
            NULL,
            
            -- Survey
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable

    FROM  @tmpMetadata


    
            
    
    ORDER BY Tag, [Metadata!3!Entity]
    --FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopSurveyGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
