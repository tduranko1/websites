-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDRPChoiceAttachmentDetailsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetDRPChoiceAttachmentDetailsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetDRPChoiceAttachmentDetailsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns APD data that will map to Hyperquest as XML
* DATE:			10Feb2017
*
* PARAMETERS:  
*
* REVISIONS:	10Feb2017 - TVD - Initial Development
*				01Jun2017 - TVD - Added a ClaimOwner tags
*				21Sep2017 - TVD - 1.1 - Added ClaimAspectID to the WHERE Clause to fix issues with multi-vehicle claims
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetDRPChoiceAttachmentDetailsWSXML]
	@iLynxID INT
	, @vJobID VARCHAR(50)
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @vAssignmentExists AS VARCHAR(50) = NULL
	DECLARE @ClaimAspectID VARCHAR(50) = NULL

    SET @ProcName = 'uspHyperquestGetDRPChoiceAttachmentDetailsWSXML'
    
    SET @ApplicationCD = 'HyperquestPartner'

    -- Validation
    -- Check if Assignment Exists or not
	SELECT @ClaimAspectID = ClaimAspectID FROM utb_hyperquestsvc_jobs WHERE LynxID = @iLynxID AND JobID = @vJobID
	SELECT
		@vAssignmentExists = JobID
	FROM
		utb_hyperquestsvc_jobs 
	WHERE 
		LynxID = @iLynxID
		AND DocumentTypeID = '22'
		AND JobStatus = 'Processed'
		AND ClaimAspectID = @ClaimAspectID

	IF (@vAssignmentExists IS NOT NULL)
	BEGIN
		-- Assignment Exists process against the assignment
		SELECT TOP 1
			1 AS Tag
			, NULL AS Parent
			, ClientClaimNumber AS [Root!1!ClientClaimNumber]
			, LynxID AS [Root!1!LynxID]
			, CURRENT_TIMESTAMP AS [Root!1!CreateDateTime]
			, CURRENT_TIMESTAMP AS [Root!1!TransmitDateTime]
			, NULL AS [Claim!2!LynxID]
			, NULL AS [Claim!2!ClaimAspectID]
			, NULL AS [Claim!2!InsuranceCompanyID]
			, NULL AS [Claim!2!InsuranceCompanyName]
			, NULL AS [Claim!2!OfficeID]
			, NULL AS [Claim!2!OfficeName]
			, NULL AS [Claim!2!InsuredUserID]
			, NULL AS [Claim!2!InsuredFirstName]
			, NULL AS [Claim!2!InsuredLastName]
			, NULL AS [Claim!2!InsuredBusinessName]
			, NULL AS [Claim!2!AnalystUserID]
			, NULL AS [Claim!2!AnalystFirstName]
			, NULL AS [Claim!2!AnalystLastName]
			, NULL AS [Claim!2!CarrierRepUserID]
			, NULL AS [Claim!2!CarrierRepFirstName]
			, NULL AS [Claim!2!CarrierRepLastName]
			, NULL AS [Claim!2!CarrierRepOfficeCd]
			, NULL AS [Claim!2!CarrierRepOfficeName]
			, NULL AS [Claim!2!CarrierRepPhone]
			, NULL AS [Claim!2!CarrierRepEmailAddress]
			, NULL AS [Claim!2!ClaimOwnerUserID]
			, NULL AS [Claim!2!ClaimOwnerFirstName]
			, NULL AS [Claim!2!ClaimOwnerLastName]
			, NULL AS [Claim!2!ClaimOwnerPhone]
			, NULL AS [Claim!2!ClaimOwnerEmailAddress]
			, NULL AS [Claim!2!IntakeFinishDate]
			, NULL AS [Claim!2!LossDate]
			, NULL AS [Claim!2!WAUserID]
			, NULL AS [Claim!2!WAGroupID]
			, NULL AS [Vehicle!3!ClaimAspectID]
			, NULL AS [Vehicle!3!VehicleNumber]
			, NULL AS [Vehicle!3!ShopLocationID]
			, NULL AS [Vehicle!3!SeqID]
			, NULL AS [Vehicle!3!VehicleYear]
			, NULL AS [Vehicle!3!VehicleMake]
			, NULL AS [Vehicle!3!VehicleModel]
			, NULL AS [Vehicle!3!VIN]
			, NULL AS [Vehicle!3!Drivable]
			, NULL AS [Vehicle!3!RepairLocationCity]
			, NULL AS [Vehicle!3!RepairLocationCounty]
			, NULL AS [Vehicle!3!RepairLocationState]
			, NULL AS [Vehicle!3!OwnerNameFirst]
			, NULL AS [Vehicle!3!OwnerNameLast]
			, NULL AS [Vehicle!3!OwnerBusinessName]
			, NULL AS [Vehicle!3!OwnerAddress1]
			, NULL AS [Vehicle!3!OwnerAddressCity]
			, NULL AS [Vehicle!3!OwnerAddressState]
			, NULL AS [Vehicle!3!OwnerAddressZip]
			, NULL AS [Vehicle!3!OwnerPhone]
			, NULL AS [Vehicle!3!AssignmentType]
			, NULL AS [Vehicle!3!LynxRepUserID]
			, NULL AS [Vehicle!3!LynxRepNameFirst]
			, NULL AS [Vehicle!3!LynxRepNameLast]
			, NULL AS [Vehicle!3!SourceApplicationID]
			, NULL AS [Vehicle!3!SourceApplicationName]
			, NULL AS [Vehicle!3!InitialAssignmentTypeName]
			, NULL AS [Vehicle!3!PassThroughInfo]
			, NULL AS [Document!4!DocumentTypeID]
			, NULL AS [Document!4!JobStatus]
		FROM
			utb_claim c
		WHERE
			c.LynxID = @iLynxID

		UNION ALL
	
		SELECT TOP 1 
			2 AS Tag
			, 1 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, c.LynxID
			, ca.ClaimAspectID
			, c.InsuranceCompanyID
			, i.Name
			, ISNULL(u.OfficeID,'')
			, o.Name
			, ISNULL(caiv.InvolvedID,'')
			, ISNULL(iv.NameFirst,'')
			, ISNULL(iv.NameLast,'')
			, ISNULL(iv.BusinessName,'')
			, ISNULL(ua.UserID,'')
			, ISNULL(ua.NameFirst,'Unknown')
			, ISNULL(ua.NameLast,'Unknown')
			, ISNULL(c.CarrierRepUserID,'')
			, ISNULL(ivcr.NameFirst,'')
			, ISNULL(ivcr.NameLast,'')
			, ISNULL(o.ClientOfficeId,'')
			, ISNULL(o.Name,'')
			, u.PhoneAreaCode + u.PhoneExchangeNumber + u.PhoneUnitNumber
			, u.EmailAddress
			, ISNULL(uco.UserID,'')
			, ISNULL(uco.NameFirst,'Unknown')
			, ISNULL(uco.NameLast,'Unknown')
			, ISNULL(uco.PhoneAreaCode,'') + ISNULL(uco.PhoneExchangeNumber,'') + ISNULL(uco.PhoneUnitNumber,'')
			, ISNULL(uco.EmailAddress,'')
			, ISNULL(c.IntakeFinishDate,'')
			, c.LossDate
			, c.InsuranceCompanyID
			, i.Name
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			INNER JOIN utb_insurance i
				ON i.InsuranceCompanyID = c.InsuranceCompanyID
			LEFT JOIN utb_user u
				ON (u.UserID = c.CarrierRepUserID)
			LEFT JOIN utb_user ua
				ON (ua.UserID = ca.AnalystUserID)
			LEFT JOIN utb_user uco
				ON (uco.UserID = ca.OwnerUserID)
			LEFT JOIN utb_office o
				ON (o.OfficeID = u.OfficeID)
			LEFT JOIN utb_claim_aspect_involved caiv 
				ON (caiv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved iv 
				ON (iv.InvolvedID = caiv.InvolvedID)
			LEFT JOIN utb_involved ivcr 
				ON (ivcr.InvolvedID = c.CarrierRepUserID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1
			AND ca.ClaimAspectID = @ClaimAspectID
			
		UNION ALL
	
		SELECT TOP 1
			3 AS Tag
			, 2 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, ISNULL(ca.ClaimAspectID,'')
			, ISNULL(ca.ClaimAspectNumber,'')
			, ISNULL(hj.ShopLocationID,'')
			, ISNULL(hj.SeqID,'')
			, ISNULL(cv.VehicleYear,'')
			, ISNULL(cv.Make,'')
			, ISNULL(cv.Model,'')
			, ISNULL(cv.VIN,'')
			, cv.DriveableFlag
			, ISNULL(casc.RepairLocationCity,'')
			, ISNULL(casc.RepairLocationCounty,'')
			, ISNULL(casc.RepairLocationState,'')
			, ISNULL(io.NameFirst,'')
			, ISNULL(io.NameLast,'')
			, ISNULL(io.BusinessName,'')
			, ISNULL(io.Address1,'')
			, ISNULL(io.AddressCity,'')
			, ISNULL(io.AddressState,'')
			, ISNULL(io.AddressZip,'')
			, CASE 
				WHEN io.DayAreaCode IS NULL THEN
					''
				ELSE
					ISNULL(io.DayAreaCode,'') + '-' + ISNULL(io.DayExchangeNumber,'') + '-' + ISNULL(io.DayUnitNumber,'')
			  END
			, (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD)
			, ISNULL(ca.AnalystUserID,'')
			, ISNULL(au.NameFirst,'')
			, ISNULL(au.NameLast,'')
			, ISNULL(a.ApplicationID,'')
			, ISNULL(a.Name,'')
			, ISNULL(at.Name,'')
			, 'NONE'
			, NULL
			, NULL
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved io 
				ON (io.InvolvedID = caio.InvolvedID)
			LEFT JOIN utb_application a 
				ON (a.ApplicationID = ca.SourceApplicationID)
			LEFT JOIN utb_user au 
				ON (au.UserID = ca.AnalystUserID) 
			LEFT JOIN utb_assignment_type at 
				ON (at.AssignmentTypeID = ca.InitialAssignmentTypeID)
    		LEFT JOIN utb_hyperquestsvc_jobs hj 
				ON (hj.LynxID = c.LynxID)
				AND (hj.JobStatus = 'Processed')
				AND (hj.DocumentTypeID = 22)
				AND (hj.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
				AND (hj.AssignmentID IS NOT NULL)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1
			AND ca.ClaimAspectID = @ClaimAspectID --hj.ClaimAspectID

		UNION ALL
	
		SELECT TOP 1
			4 AS Tag
			, 1 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, ''
			, hj.JobStatus
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved io 
				ON (io.InvolvedID = caio.InvolvedID)
			LEFT JOIN utb_application a 
				ON (a.ApplicationID = ca.SourceApplicationID)
			LEFT JOIN utb_user au 
				ON (au.UserID = ca.AnalystUserID) 
			LEFT JOIN utb_assignment_type at 
				ON (at.AssignmentTypeID = ca.InitialAssignmentTypeID)
    		LEFT JOIN utb_hyperquestsvc_jobs hj 
				ON (hj.LynxID = c.LynxID)
				AND (hj.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1
 			AND hj.JobID = @vJobID
			AND ca.ClaimAspectID = hj.ClaimAspectID

		FOR XML EXPLICIT	
	END
	ELSE
	BEGIN
		BEGIN TRANSACTION BadOrder
			UPDATE utb_hyperquestsvc_jobs SET JobStatus = 'Waiting' WHERE JobID = @vJobID

		    -- Check error value
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END

		COMMIT TRANSACTION BadOrder

		-- Assignment Doesn't Exist Ignore the Job
		SELECT TOP 1
			1 AS Tag
			, NULL AS Parent
			, ClientClaimNumber AS [Root!1!ClientClaimNumber]
			, LynxID AS [Root!1!LynxID]
			, CURRENT_TIMESTAMP AS [Root!1!CreateDateTime]
			, CURRENT_TIMESTAMP AS [Root!1!TransmitDateTime]
			, NULL AS [Claim!2!LynxID]
			, NULL AS [Claim!2!ClaimAspectID]
			, NULL AS [Claim!2!InsuranceCompanyID]
			, NULL AS [Claim!2!InsuranceCompanyName]
			, NULL AS [Claim!2!OfficeID]
			, NULL AS [Claim!2!OfficeName]
			, NULL AS [Claim!2!InsuredUserID]
			, NULL AS [Claim!2!InsuredFirstName]
			, NULL AS [Claim!2!InsuredLastName]
			, NULL AS [Claim!2!InsuredBusinessName]
			, NULL AS [Claim!2!AnalystUserID]
			, NULL AS [Claim!2!AnalystFirstName]
			, NULL AS [Claim!2!AnalystLastName]
			, NULL AS [Claim!2!CarrierRepUserID]
			, NULL AS [Claim!2!CarrierRepFirstName]
			, NULL AS [Claim!2!CarrierRepLastName]
			, NULL AS [Claim!2!CarrierRepOfficeCd]
			, NULL AS [Claim!2!CarrierRepOfficeName]
			, NULL AS [Claim!2!CarrierRepPhone]
			, NULL AS [Claim!2!CarrierRepEmailAddress]
			, NULL AS [Claim!2!ClaimOwnerUserID]
			, NULL AS [Claim!2!ClaimOwnerFirstName]
			, NULL AS [Claim!2!ClaimOwnerLastName]
			, NULL AS [Claim!2!ClaimOwnerPhone]
			, NULL AS [Claim!2!ClaimOwnerEmailAddress]
			, NULL AS [Claim!2!IntakeFinishDate]
			, NULL AS [Claim!2!LossDate]
			, NULL AS [Claim!2!WAUserID]
			, NULL AS [Claim!2!WAGroupID]
			, NULL AS [Vehicle!3!ClaimAspectID]
			, NULL AS [Vehicle!3!VehicleNumber]
			, NULL AS [Vehicle!3!ShopLocationID]
			, NULL AS [Vehicle!3!SeqID]
			, NULL AS [Vehicle!3!VehicleYear]
			, NULL AS [Vehicle!3!VehicleMake]
			, NULL AS [Vehicle!3!VehicleModel]
			, NULL AS [Vehicle!3!VIN]
			, NULL AS [Vehicle!3!Drivable]
			, NULL AS [Vehicle!3!RepairLocationCity]
			, NULL AS [Vehicle!3!RepairLocationCounty]
			, NULL AS [Vehicle!3!RepairLocationState]
			, NULL AS [Vehicle!3!OwnerNameFirst]
			, NULL AS [Vehicle!3!OwnerNameLast]
			, NULL AS [Vehicle!3!OwnerBusinessName]
			, NULL AS [Vehicle!3!OwnerAddress1]
			, NULL AS [Vehicle!3!OwnerAddressCity]
			, NULL AS [Vehicle!3!OwnerAddressState]
			, NULL AS [Vehicle!3!OwnerAddressZip]
			, NULL AS [Vehicle!3!OwnerPhone]
			, NULL AS [Vehicle!3!AssignmentType]
			, NULL AS [Vehicle!3!LynxRepUserID]
			, NULL AS [Vehicle!3!LynxRepNameFirst]
			, NULL AS [Vehicle!3!LynxRepNameLast]
			, NULL AS [Vehicle!3!SourceApplicationID]
			, NULL AS [Vehicle!3!SourceApplicationName]
			, NULL AS [Vehicle!3!InitialAssignmentTypeName]
			, NULL AS [Vehicle!3!PassThroughInfo]
			, NULL AS [Document!4!DocumentTypeID]
			, NULL AS [Document!4!JobStatus]
		FROM
			utb_claim c
		WHERE
			c.LynxID = @iLynxID

		UNION ALL
	
		SELECT TOP 1 
			2 AS Tag
			, 1 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, c.LynxID
			, ca.ClaimAspectID
			, c.InsuranceCompanyID
			, i.Name
			, ISNULL(u.OfficeID,'')
			, o.Name
			, ISNULL(caiv.InvolvedID,'')
			, ISNULL(iv.NameFirst,'')
			, ISNULL(iv.NameLast,'')
			, ISNULL(iv.BusinessName,'')
			, ISNULL(ua.UserID,'')
			, ISNULL(ua.NameFirst,'Unknown')
			, ISNULL(ua.NameLast,'Unknown')
			, ISNULL(c.CarrierRepUserID,'')
			, ISNULL(ivcr.NameFirst,'')
			, ISNULL(ivcr.NameLast,'')
			, ISNULL(o.ClientOfficeId,'')
			, ISNULL(o.Name,'')
			, u.PhoneAreaCode + u.PhoneExchangeNumber + u.PhoneUnitNumber
			, u.EmailAddress
			, ISNULL(uco.UserID,'')
			, ISNULL(uco.NameFirst,'Unknown')
			, ISNULL(uco.NameLast,'Unknown')
			, ISNULL(uco.PhoneAreaCode,'') + ISNULL(uco.PhoneExchangeNumber,'') + ISNULL(uco.PhoneUnitNumber,'')
			, ISNULL(uco.EmailAddress,'')
			, ISNULL(c.IntakeFinishDate,'')
			, c.LossDate
			, c.InsuranceCompanyID
			, i.Name
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			INNER JOIN utb_insurance i
				ON i.InsuranceCompanyID = c.InsuranceCompanyID
			LEFT JOIN utb_user u
				ON (u.UserID = c.CarrierRepUserID)
			LEFT JOIN utb_user ua
				ON (ua.UserID = ca.AnalystUserID)
			LEFT JOIN utb_user uco
				ON (uco.UserID = ca.OwnerUserID)
			LEFT JOIN utb_office o
				ON (o.OfficeID = u.OfficeID)
			LEFT JOIN utb_claim_aspect_involved caiv 
				ON (caiv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved iv 
				ON (iv.InvolvedID = caiv.InvolvedID)
			LEFT JOIN utb_involved ivcr 
				ON (ivcr.InvolvedID = c.CarrierRepUserID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1
			
		UNION ALL
	
		SELECT TOP 1
			3 AS Tag
			, 2 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, ISNULL(ca.ClaimAspectID,'')
			, ISNULL(ca.ClaimAspectNumber,'')
			, ISNULL(hj.ShopLocationID,'')
			, ISNULL(hj.SeqID,'')
			, ISNULL(cv.VehicleYear,'')
			, ISNULL(cv.Make,'')
			, ISNULL(cv.Model,'')
			, ISNULL(cv.VIN,'')
			, cv.DriveableFlag
			, ISNULL(casc.RepairLocationCity,'')
			, ISNULL(casc.RepairLocationCounty,'')
			, ISNULL(casc.RepairLocationState,'')
			, ISNULL(io.NameFirst,'')
			, ISNULL(io.NameLast,'')
			, ISNULL(io.BusinessName,'')
			, ISNULL(io.Address1,'')
			, ISNULL(io.AddressCity,'')
			, ISNULL(io.AddressState,'')
			, ISNULL(io.AddressZip,'')
			, CASE 
				WHEN io.DayAreaCode IS NULL THEN
					''
				ELSE
					ISNULL(io.DayAreaCode,'') + '-' + ISNULL(io.DayExchangeNumber,'') + '-' + ISNULL(io.DayUnitNumber,'')
			  END
			, (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD)
			, ISNULL(ca.AnalystUserID,'')
			, ISNULL(au.NameFirst,'')
			, ISNULL(au.NameLast,'')
			, ISNULL(a.ApplicationID,'')
			, ISNULL(a.Name,'')
			, ISNULL(at.Name,'')
			, 'NONE'
			, NULL
			, NULL
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved io 
				ON (io.InvolvedID = caio.InvolvedID)
			LEFT JOIN utb_application a 
				ON (a.ApplicationID = ca.SourceApplicationID)
			LEFT JOIN utb_user au 
				ON (au.UserID = ca.AnalystUserID) 
			LEFT JOIN utb_assignment_type at 
				ON (at.AssignmentTypeID = ca.InitialAssignmentTypeID)
    		LEFT JOIN utb_hyperquestsvc_jobs hj 
				ON (hj.LynxID = c.LynxID)
				AND (hj.JobStatus = 'Processed')
				AND (hj.DocumentTypeID = 22)
				AND (hj.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
				AND (hj.AssignmentID IS NOT NULL)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1

		UNION ALL
	
		SELECT TOP 1
			4 AS Tag
			, 1 AS Parent
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, ''
			, hj.JobStatus
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			LEFT JOIN utb_claim_aspect_service_channel casc
				ON (casc.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_vehicle cv 
				ON (cv.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_claim_aspect_involved caio 
				ON (caio.ClaimAspectID = ca.ClaimAspectID)
			LEFT JOIN utb_involved io 
				ON (io.InvolvedID = caio.InvolvedID)
			LEFT JOIN utb_application a 
				ON (a.ApplicationID = ca.SourceApplicationID)
			LEFT JOIN utb_user au 
				ON (au.UserID = ca.AnalystUserID) 
			LEFT JOIN utb_assignment_type at 
				ON (at.AssignmentTypeID = ca.InitialAssignmentTypeID)
    		LEFT JOIN utb_hyperquestsvc_jobs hj 
				ON (hj.LynxID = c.LynxID)
				AND (hj.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
		WHERE
			c.LynxID = @iLynxID
			AND ca.ClaimAspectTypeID = 9
			AND caio.EnabledFlag = 1
 			AND hj.JobID = @vJobID

		FOR XML EXPLICIT	
	END

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDRPChoiceAttachmentDetailsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetDRPChoiceAttachmentDetailsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/