-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCustomFormsGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCustomFormsGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************  
*  
* PROCEDURE:    uspCustomFormsGetListWSXML  
* SYSTEM:       Lynx Services APD  
* AUTHOR:       Thomas Duranko  
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]  
*  
* PARAMETERS:    
* No Parameters  
*  
* RESULT SET:  
* Users List as XML  
*  
*  
* VSS  
* $Workfile: $  
* $Archive:  $  
* $Revision: $  
* $Author:   $  
* $Date:     $  
*  
************************************************************************************************************************/  
  
-- Create the stored procedure  
  
  
CREATE PROCEDURE [dbo].[uspCustomFormsGetListWSXML]  --1600423,5844  
    @LynxID     udt_std_id_big,  
    @UserID     udt_std_id_big  
  
AS  
BEGIN  
    -- Declare internal variables  
    DECLARE @InsuranceCompanyID as int  
    DECLARE @InsuranceCompanyName as varchar(50)  
    DECLARE @ClaimAspectTypeIDVehicle as int  
    DECLARE @LossState as varchar(2)  
	DECLARE @UserName as varchar(100)  
	DECLARE @UserPhone as varchar(10)  
	DECLARE @UserEmail as varchar(100)  
	DECLARE @PolicyNumber as varchar(100)  
	DECLARE @ClientClaimNumber as varchar(100)
	DECLARE @NameFirst as varchar(100)
	DECLARE @NameLast as varchar(100)
	  
      
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts   
  
    SET @ProcName = 'uspCustomFormsGetListWSXML'  
  
    -- Check to make sure a valid Lynx id was passed in  
    IF  (@LynxID IS NULL) OR  
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))  
    BEGIN  
        -- Invalid Lynx ID  
      
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)  
        RETURN  
    END  
  
    -- Check to make sure a valid user id was passed in  
    IF  (@UserID IS NULL) OR  
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))  
    BEGIN  
        -- Invalid User ID  
      
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)  
        RETURN  
    END  
  
    -- Get the claim aspect type id for the vehicle  
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID  
    FROM utb_claim_aspect_type  
    WHERE Name = 'Vehicle'  
  
    -- Get the Insurance Company   
    SELECT @InsuranceCompanyID = c.InsuranceCompanyID,  
           @InsuranceCompanyName = i.Name,  
           @LossState = c.LossState,  
           @PolicyNumber = c.PolicyNumber,  
           @ClientClaimNumber = c.ClientClaimNumber  
    FROM utb_claim c  
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID  
    WHERE LynxID = @LynxID  
      
    SELECT  @UserName = NameFirst + ' ' + NameLast,  
   @UserPhone = PhoneAreaCode + PhoneExchangeNumber + PhoneUnitNumber,  
   @UserEmail = EmailAddress  
    FROM utb_user  
 WHERE UserID = @UserID  
         
  -- Get the Assigned Office Name
 select @NameFirst = i.NameFirst,
		@NameLast = i.NameLast
		From utb_involved i
	INNER JOIN dbo.utb_claim c on i.InvolvedID = c.CallerInvolvedID
	WHERE c.LynxID = @LynxID
	
    -- Begin XML Select  
  
    -- Select Root  
     DECLARE @tmpReference TABLE  
    (  
        FormID                int           NOT NULL,  
        BundlingTaskID        int               NULL,    
        DocumentTypeID      int               NULL,  
        DocName               varchar(50)       NULL,  
        InsuranceCompanyID   int                NULL,  
        EventID              int                NULL,  
        LossStateCode         varchar(10)       NULL,  
        ShopStateCode        varchar(10)        NULL,      
        AutoBundlingFlag    INT                 NULL,  
        DataMiningFlag      int                 NULL,  
        HTMLPath           varchar(50)           NULL,  
        formName           varchar(220)         Not NULL,  
        PDFPath        varchar(220)           Not NULL,  
        PertainsToCD     varchar(4)             NULL,  
        ServiceChannelCD   varchar(4)           NULL,  
        SQLProcedure        varchar(50)          NULL,  
        fax                varchar(5)           Null  
          
    )  
      
      
    DECLARE @ClaimAspectID as int  
      
    /* SELECT    
        @ClaimAspectID = ca.ClaimAspectID    
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID      
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1 */  
      
      
     
                
    IF NOT EXISTS (SELECT InvoiceID FROM utb_invoice I WHERE I.ClaimAspectID IN   
     ( SELECT    
        ca.ClaimAspectID    
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID      
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1) AND EnabledFlag = 1)  
BEGIN  
  
 INSERT INTO @tmpReference ( FormID,                 
                                BundlingTaskID,    
                                DocumentTypeID,       
                                DocName,    
                                EventID,               
                                InsuranceCompanyID,                                             
                                LossStateCode,         
                                ShopStateCode,            
                                AutoBundlingFlag,     
                                DataMiningFlag,      
                                HTMLPath,             
                                FormName,            
                                PDFPath,         
                                PertainsToCD,      
                                ServiceChannelCD,    
                                SQLProcedure ,   
                                Fax)  
                                  
    SELECT  FormID,   
            BundlingTaskID,    
            f.DocumentTypeID,   
            dt.Name,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            f.Name,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END  
             
    FROM utb_form f  
    LEFT JOIN utb_document_type dt ON f.DocumentTypeID = dt.DocumentTypeID  
    WHERE (InsuranceCompanyID IS NULL   
       OR InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
      --AND ServiceChannelCD <> 'RRP'   
      AND (f.Name <> 'QBE Memo Bill' OR ServiceChannelCD = 'DA')  
      --AND (f.Name <> 'BCIF Request' or InsuranceCompanyID <> 387)  
        
       
        
        
END  
ELSE  
BEGIN  
INSERT INTO @tmpReference ( FormID,                 
                                BundlingTaskID,    
                                DocumentTypeID,       
                                DocName,   
                                EventID,               
                                InsuranceCompanyID,  
                                LossStateCode,         
                                ShopStateCode,            
                                AutoBundlingFlag,     
                                DataMiningFlag,      
                                HTMLPath,             
                                FormName,            
                                PDFPath,         
                                PertainsToCD,      
                                ServiceChannelCD,    
                                SQLProcedure,   
                                Fax)  
  
    SELECT   
            -- Form  
            FormID,   
            BundlingTaskID,   
            f.DocumentTypeID,   
            dt.Name,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            f.Name,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END  
             
    FROM utb_form f  
    LEFT JOIN utb_document_type dt ON f.DocumentTypeID = dt.DocumentTypeID  
    WHERE (InsuranceCompanyID IS NULL   
       OR InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
        
      --AND (@InvoiceExists > 0 AND ServiceChannelCD = 'RRP' AND f.Name = 'QBE Memo Bill' )  
END    
      
    SELECT  1 as Tag,  
            NULL as Parent,  
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],  
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],  
            @LynxID AS [Root!1!LynxID],  
            @LossState AS [Root!1!LossState],  
            @ClientClaimNumber AS [Root!1!ClientClaimNumber],  
            @PolicyNumber AS [Root!1!PolicyNumber],  
            @UserName as [Root!1!UserName],  
            @UserPhone as [Root!1!UserPhone],  
            @UserEmail as [Root!1!UserEmail],
            @NameFirst as [Root!1!NameFirst],  
            @NameLast as [Root!1!NameLast],  
            -- Vehicle List  
            NULL as [Vehicle!2!ClaimAspectID],  
            NULL as [Vehicle!2!ClaimAspectNumber],  
            NULL as [Vehicle!2!OwnerNameFirst],  
            NULL as [Vehicle!2!OwnerNameLast],  
            NULL as [Vehicle!2!OwnerBusinessName],  
            NULL as [Vehicle!2!ShopFaxNumber],  
            NULL as [Vehicle!2!AnalystUserID],  
            NULL as [Vehicle!2!InvoiceID],  
            -- Service Channel  
            NULL as [ServiceChannel!3!ClaimAspectServiceChannelID],  
            NULL as [ServiceChannel!3!ServiceChannelCD],  
            NULL as [ServiceChannel!3!ServiceChannelDesc],  
            NULL as [ServiceChannel!3!ShopState],  
            -- Form  
            NULL as [Form!4!FormID],  
            NULL as [Form!4!BundlingTaskID],  
            NULL as [Form!4!DocumentTypeID],  
            NULL as [Form!4!DocumentTypeName],  
            NULL as [Form!4!EventID],  
            NULL as [Form!4!InsuranceCompanyID],  
            NULL as [Form!4!LossStateCode],  
            NULL AS [Form!4!ShopStateCode],  
            NULL AS [Form!4!AutoBundlingFlag],  
            NULL AS [Form!4!DataMiningFlag],  
            NULL as [Form!4!HTMLPath],  
            NULL as [Form!4!Name],  
            NULL as [Form!4!PDFPath],  
            NULL AS [Form!4!PertainsToCD],  
            NULL AS [Form!4!ServiceChannelCD],  
            NULL AS [Form!4!SQLProcedure],  
            NULL AS [Form!4!DestinationType],  
     
            -- Form Supplement  
            NULL AS [Supplement!5!FormSupplementID],  
            NULL AS [Supplement!5!LossStateCode],  
            NULL AS [Supplement!5!ShopStateCode],  
            NULL AS [Supplement!5!HTMLPath],  
            NULL AS [Supplement!5!Name],  
            NULL AS [Supplement!5!PDFPath],  
            NULL AS [Supplement!5!ServiceChannelCD],  
            NULL AS [Supplement!5!SQLProcedure],  
              
              
            -- Added dataset for Estimate and Supplement glsd451  
            NULL AS [DataSet!6!SupplementSeqNumber],
            NULL AS [DataSet!6!Name],  
            NULL AS [DataSet!6!ClaimAspectID],        
            NULL AS [DataSet!6!ServiceChannelCD],
            NULL AS [DataSet!6!DuplicateFlag]           
              
              
  
    UNION ALL  
  
    SELECT  2,  
            1,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            ca.ClaimAspectID,  
            ca.ClaimAspectNumber,  
            IsNull((SELECT  Top 1 i.NameFirst  
                    FROM  dbo.utb_claim_aspect_involved cai  
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT  Top 1 i.NameLast  
                    FROM  dbo.utb_claim_aspect_involved cai  
                   LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT  Top 1 i.BusinessName  
                    FROM  dbo.utb_claim_aspect_involved cai  
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT sl.FaxAreaCode + FaxExchangeNumber + FaxUnitNumber  
      FROM utb_shop_location sl  
      LEFT JOIN utb_assignment a ON sl.ShopLocationID = a.ShopLocationID  
      LEFT JOIN utb_claim_aspect_service_channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
      LEFT JOIN utb_claim_aspect ca1 ON casc.ClaimAspectID = ca1.ClaimAspectID  
      WHERE a.assignmentsequencenumber = 1 AND a.CancellationDate IS NULL  
        AND casc.PrimaryFlag = 1  
        AND ca1.ClaimAspectID = ca.ClaimAspectID  
     ), ''),  
            ca.AnalystUserID,  
             inv.InvoiceID,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
           -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL, NULL
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID  
    Left JOIN utb_invoice inv on inv.ClaimAspectID =  ca.ClaimAspectID and  
    inv.enabledflag = 1   
    WHERE ca.LynxID = @LynxID  
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle  
      AND ca.EnabledFlag = 1  
  
  
    UNION ALL  
  
    SELECT  3,  
            2,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            ca.ClaimAspectID,  
            NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            casc.ClaimAspectServiceChannelID,  
            casc.ServiceChannelCD,   
            tmp.Name,  
            LTRIM(isNULL((SELECT TOP 1 sl.AddressState  
                            FROM utb_assignment a  
                            LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID  
                            WHERE a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
                              AND a.CancellationDate is null AND a.assignmentsequencenumber = 1  
                              AND a.ShopLocationID is not null), '')),  
            -- Form  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL, NULL
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID  
    LEFT JOIN (SELECT Code, Name  
                FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')) tmp ON casc.ServiceChannelCD = tmp.Code  
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1  
  
    
UNION ALL  
    -- Get all forms that are applicable to all insurance companies and the ones that are specific  
  SELECT 4,  
            1,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            FormID,   
            BundlingTaskID,   
            f.DocumentTypeID,   
            DocName,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            FormName,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Added dataset for Estimate and Supplement glsd451 
            NULL, NULL, NULL, NULL, NULL
    FROM @tmpReference f  
      
  
    UNION ALL  
  
    -- Get all supplement forms  
    SELECT  5,  
            4,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            fs.FormID,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            fs.FormSupplementID,   
            LTrim(isNull(fs.LossStateCode, '')),   
            LTrim(isNull(fs.ShopStateCode, '')),   
            fs.HTMLPath,   
            fs.Name,   
            fs.PDFPath,   
            LTrim(isNull(fs.ServiceChannelCD, '')),   
            fs.SQLProcedure,  
            -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL, NULL 
    FROM utb_form_supplement fs  
    LEFT JOIN utb_form f ON fs.FormID = f.FormID  
    WHERE (f.InsuranceCompanyID IS NULL   
       OR f.InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
      AND fs.EnabledFlag = 1  
  
       
              
    UNION ALL  
    -- Added dataset for Estimate and Supplement glsd451 
    SELECT  6 ,  
            1 ,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
			SupplementSeqNumber,Name,ClaimAspectID,ServiceChannelCD,duplicateflag 
			--SupplementSeqNumber,Name,ServiceChannelCD,duplicateflag 
        from (
			SELECT
			dt.Name,
			ca.ClaimAspectID,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,
			casc.ServiceChannelCD
			FROM dbo.utb_claim_aspect_Service_Channel_document cascd
			INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
			INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE ca.LynxID = @LynxID
			AND d.EnabledFlag = 1
			AND casc.EnabledFlag = 1
			AND casc.ServiceChannelCD = 'DA'
			AND dt.Name in('Estimate','Supplement')
			AND d.EstimateTypeCD = 'O'
			AND d.duplicateflag = 0
			group by dt.Name,
			ca.ClaimAspectID,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,casc.ServiceChannelCD,duplicateflag having count(d.EstimateTypeCD)=1
			UNION ALL

			SELECT
			dt.Name,
			ca.ClaimAspectID,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,
			casc.ServiceChannelCD
			FROM dbo.utb_claim_aspect_Service_Channel_document cascd
			INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
			INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE ca.LynxID = @LynxID
			AND d.EnabledFlag = 1
			AND casc.EnabledFlag = 1
			AND casc.ServiceChannelCD = 'DA'
			AND dt.Name in('Estimate','Supplement')
			AND d.EstimateTypeCD = 'A'
			AND d.duplicateflag = 0
			group by dt.Name,
			ca.ClaimAspectID,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,casc.ServiceChannelCD having count(d.EstimateTypeCD)=1
			)
			--as t1 group by SupplementSeqNumber,Name,ServiceChannelCD,duplicateflag having count(SupplementSeqNumber) =2  
			as t1 group by SupplementSeqNumber,Name,ClaimAspectID,ServiceChannelCD,duplicateflag having count(SupplementSeqNumber) =2  
            
            
            
    ORDER BY [Vehicle!2!ClaimAspectID], [Form!4!FormID], Tag  
  
    FOR XML EXPLICIT  
  
    IF @@ERROR <> 0  
    BEGIN  
       -- SQL Server Error  
      
        RAISERROR('99|%s', 16, 1, @ProcName)  
        RETURN(1)  
    END      
END  


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCustomFormsGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCustomFormsGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCustomFormsGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/