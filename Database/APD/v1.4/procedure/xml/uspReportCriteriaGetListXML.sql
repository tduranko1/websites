-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReportCriteriaGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspReportCriteriaGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspReportCriteriaGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     returns a XML formatted criteria list for the report
*
* PARAMETERS:  
* (I) @ReportID                Report ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspReportCriteriaGetListXML
    @ReportID           as udt_std_int,
    @OfficeID           as udt_std_int = null,
    @InsuranceCompanyID as udt_std_int = null
AS
BEGIN
    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @CriteriaID             AS int
    DECLARE @SQL                    AS nvarchar(1000)
    DECLARE @Debug                  AS bit
    DECLARE @CriteriaStateID        AS tinyint
    DECLARE @CriteriaOfficeID       AS tinyint
    DECLARE @CriteriaUserID         AS tinyint

    SET @ProcName = 'uspReportCriteriaGetListXML'
    
    SET @Debug = 0
    
    IF NOT EXISTS (SELECT ReportID
                    FROM utb_report
                    WHERE ReportID = @ReportID
                  )
    BEGIN
       -- Invalid User

        RAISERROR  ('1|Invalid Report', 16, 1, @ProcName)
        RETURN
    END
    
/*    IF @OfficeID IS NULL AND @InsuranceCompanyID IS NULL
    BEGIN
       -- Invalid User

        RAISERROR  ('1|Insurance Company ID is required when Office ID is NULL', 16, 1, @ProcName)
        RETURN
        
    END*/
    
    /*IF NOT EXISTS (SELECT OfficeID
                    FROM utb_Office
                    WHERE OfficeID = @OfficeID
                  )
    BEGIN
       -- Invalid User

        RAISERROR  ('1|Invalid Office id', 16, 1, @ProcName)
        RETURN
    END*/
    
    SELECT @CriteriaStateID = CriteriaID
    FROM utb_criteria
    WHERE DisplayText = 'State'

    SELECT @CriteriaOfficeID = CriteriaID
    FROM utb_criteria
    WHERE DisplayText = 'Office'

    SELECT @CriteriaUserID = CriteriaID
    FROM utb_criteria
    WHERE DisplayText = 'Claim Representative'
    
    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50)            NOT NULL,
        ReferenceCD     varchar(10)            NOT NULL,
        Name            varchar(500)           NOT NULL,
        DefaultFlag     bit                    NULL
    )
    
    -- Export format reference codes
    INSERT INTO @tmpReference
    SELECT 'ExportFormat',
           ExportFormatCD,
           t.Name,
           DefaultFlag
    FROM utb_report_export_format ref
    LEFT JOIN (SELECT * FROM ufnUtilityGetReferenceCodes('utb_report_export_format', 'ExportFormatCD')) t ON ref.ExportFormatCD = t.Code
    WHERE ReportID = @ReportID
    
    
    -- Load all the reference values for criteria that are of type reference and the source is list
    INSERT INTO @tmpReference
    SELECT c.CriteriaID, '', c.ReferenceSQL, NULL
    FROM utb_report_criteria rc, utb_criteria c
    WHERE rc.CriteriaID = c.CriteriaID
      AND rc.ReportID = @ReportID
      AND c.DataTypeCD = 'R'
      AND c.ReferenceTypeCD = 'L' -- User defined comma demilited list

    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = '#tmpReportReference' AND type = 'U'
               )
        drop table #tmpReportReference
    
    Create Table #tmpReportReference
    (
        code    varchar(50)     NOT NULL,
        value   varchar(100)    NOT NULL
    )
    
    -- Load all the reference values for criteria that are of type reference and the source is SQL
    DECLARE csrSQLReference CURSOR FOR
    SELECT c.CriteriaID, c.ReferenceSQL
    FROM utb_report_criteria rc, utb_criteria c
    WHERE rc.CriteriaID = c.CriteriaID
      AND rc.ReportID = @ReportID
      AND c.DataTypeCD = 'R'
      AND c.ReferenceTypeCD = 'S' -- SQL Source
      AND c.EnabledFlag = 1
    
    OPEN csrSQLReference

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrSQLReference
    INTO @CriteriaID, @SQL
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    WHILE @@Fetch_Status = 0
    BEGIN
    
        IF @Debug = 1
        BEGIN
            PRINT '@CriteriaID: ' + convert(varchar, @CriteriaID)
            PRINT '@SQL: ' + @SQL
            PRINT ''
        END
        
        IF @SQL IS NOT NULL AND LEN(@SQL) > 0
        BEGIN
            SET @SQL = N'INSERT INTO #tmpReportReference ' + @SQL

            EXEC sp_executeSQL @SQL

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END

            IF @Debug = 1
            BEGIN
                SELECT * FROM #tmpReportReference
            END

            INSERT INTO @tmpReference
            SELECT @CriteriaID, Code, Value, NULL
            FROM #tmpReportReference
            
            DELETE FROM #tmpReportReference
        END

        FETCH next
        FROM csrSQLReference
        INTO @CriteriaID, @SQL
    
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END

    CLOSE csrSQLReference
    DEALLOCATE csrSQLReference
    

    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = '#tmpReportReference' AND type = 'U'
               )
        DROP TABLE #tmpReportReference

    -- remove those states that are not in the Office's contracted state
    IF @OfficeID IS NOT NULL
    BEGIN
        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaStateID)
          AND ReferenceCD not in ( SELECT StateCode
                                   FROM utb_office_contract_state
                                   WHERE OfficeID = @OfficeID
                                 )
        
        IF @InsuranceCompanyID IS NOT NULL
        BEGIN
            DELETE 
            FROM @tmpReference 
            WHERE ListName = CONVERT(varchar, @CriteriaOfficeID)
              AND ReferenceCD NOT IN (SELECT OfficeID
                                      FROM utb_office
                                      WHERE InsuranceCompanyID = @InsuranceCompanyID
                                     )
        END
        ELSE
        BEGIN
            DELETE 
            FROM @tmpReference 
            WHERE ListName = CONVERT(varchar, @CriteriaOfficeID)
              AND ReferenceCD <> convert(varchar, @OfficeID)
        END
                
    END

    IF @OfficeID IS NULL and @InsuranceCompanyID IS NULL
    BEGIN
        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaStateID)

        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaOfficeID)
    END

    IF @OfficeID IS NULL and @InsuranceCompanyID IS NOT NULL
    BEGIN
        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaStateID)
          AND ReferenceCD not in ( SELECT distinct StateCode
                                    FROM utb_office o, utb_office_contract_state ocs
                                    WHERE ocs.OfficeID = o.OfficeID
                                      AND o.InsuranceCompanyID = @InsuranceCompanyID
                                 )

        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaOfficeID)
          AND ReferenceCD not in ( SELECT convert(varchar, o.OfficeID)
                                    FROM utb_office o
                                    WHERE o.InsuranceCompanyID = @InsuranceCompanyID
                                 )
    END
    
    -- Remove all users that don't belong to the Insurance company
    IF @InsuranceCompanyID is not NULL
    BEGIN
        DELETE 
        FROM @tmpReference 
        WHERE ListName = CONVERT(varchar, @CriteriaUserID)
          AND ReferenceCD not in ( SELECT convert(varchar, u.UserID)
                                    FROM utb_office o, utb_user u
                                    WHERE o.OfficeID = u.OfficeID
                                      AND o.InsuranceCompanyID = @InsuranceCompanyID
                                 )
    END
    
    SELECT
        1 as Tag,
        Null as Parent,
--        Root
        @ReportID                       as [Root!1!ReportID],
        IsNull(FileName, '')            as [Root!1!RptName],
        IsNull(SPName, '')              as [Root!1!ProcName],
        IsNull(StaticParameters, '')    as [Root!1!StaticParameters],
        IsNull(Description, '')         as [Root!1!ReportName],
--        Criteria
        Null as [Criteria!2!CriteriaID],
        Null as [Criteria!2!DataType],
        Null as [Criteria!2!DefaultValueScript],
        Null as [Criteria!2!DisplayOrder],
        Null as [Criteria!2!DisplayText],
        Null as [Criteria!2!MaxLength],
        Null as [Criteria!2!MaxValue],
        Null as [Criteria!2!RequiredField],
        Null as [Criteria!2!ReferenceType],
        Null as [Criteria!2!ParamName],
--        Reference Data
        Null as [Reference!3!List],
        Null as [Reference!3!ReferenceID],
        Null as [Reference!3!Name],
        Null as [Reference!3!DefaultFlag]
    FROM utb_report
    WHERE ReportID = @ReportID
    
    UNION ALL
    
    SELECT 2,
           1,
           -- Root
           NULL, NULL, NULL, NULL, NULL,
           -- Criteria
           c.CriteriaID,
           c.DataTypeCD,
           c.DefaultValueScript,
           c.DisplayOrder,
           c.DisplayText,
           c.MaxLength,
           isNull(c.MaxValue, ''),
           c.RequiredField,
           c.ReferenceTypeCD,
           c.ParamName,
           -- Reference
           NULL, NULL, NULL, NULL
    FROM utb_report_criteria rc, utb_criteria c
    WHERE rc.CriteriaID = c.CriteriaID
      AND rc.ReportID = @ReportID    
      AND c.EnabledFlag = 1
      
    UNION ALL
    
    SELECT 3,
           1,
           -- Root
           NULL, NULL, NULL,  NULL, NULL,
           -- Criteria
           NULL, NULL, NULL, NULL, NULL, 
           NULL, NULL, NULL, NULL, NULL,
           -- Reference
           ListName, 
           ReferenceCD, 
           Name,
           DefaultFlag
    FROM @tmpReference 
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReportCriteriaGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspReportCriteriaGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/