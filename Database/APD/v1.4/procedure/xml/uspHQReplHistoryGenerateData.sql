-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHQReplHistoryGenerateData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHQReplHistoryGenerateData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHQReplHistoryGenerateData
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Populates the replication table for HQ History.  HQ will replicate this table with their database.
* Date:			21Jul2016
*
* PARAMETERS:  @LastRunOverride = VARCHAR of override date for specific date processing
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	21Jul2016 - TVD 1.0 - Initial development
*				12Aug2016 - TVD 1.1 - Fine tunning this to not put dup records in replication history
*								- Modified he main clause to filter out already processed records.
*				20Aug2016 - TVD 1.2 - Increased the size of the Description in the tmpHistoryNotes table.
*								- Also cleaned up error handling so that the process doesn't stop on error.
*				24Aug2016 - TVD 1.3 - Adding TaskID to the details.
*				29Aug2016 - TVD 1.4 - Modified so that Tasks will have ClaimAspectNumber and ClaimAspectServiceChannel
*				07Sep2016 - TVD 1.5 - Modified to add Choice claim Notes to the Replication Table
*				12Oct2016 - TVD 1.6 - Corrected a Choice claim Notes add issue to the Replication Table
*				12Apr2017 - TVD 1.7 - Corrected a NULL value issue with ServiceChannel
*				11Nov2017 - TVD 1.8 - Redesigned to only replicate ONLY CS comments
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHQReplHistoryGenerateData]
	@LastRunOverride	VARCHAR(50) = NULL
AS
BEGIN
	-----------------------------------
	-- Declarations 
	-----------------------------------
	DECLARE @Debug              udt_std_flag
	SET @Debug = 0

	DECLARE @LastRunDateTime	udt_std_datetime
	DECLARE @LastAPDRunDateTime	udt_std_datetime
	DECLARE @CurrentRunDateTime	udt_std_datetime
	DECLARE @LynxID				INT
	DECLARE @ApplicationCD      udt_std_cd='APD'
	DECLARE @NumRowsProcessed   INT
	SET @NumRowsProcessed = 0

    IF @Debug = 1
    BEGIN
        PRINT 'Processing Started: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
		PRINT '---> Declaring tables '
    END

	-- Create temporary table to hold references to the claim's notes
	DECLARE @tmpNotes TABLE 
	(
		LynxID                      bigint          NOT NULL,
		DocumentID                  int             NOT NULL,
		PertainsTo                  varchar(50)     NOT NULL,
		ClaimAspectTypeName         varchar(50)     NOT NULL,
		ClaimAspectNumber           tinyint         NOT NULL,
		ClaimAspectID               bigint          NOT NULL,    
		ClaimAspectServiceChannelID bigint          NOT NULL,
		InsuranceCompanyID			int				NOT NULL
	)

	-- Create temporary table to hold references to the claim's history and notes
	DECLARE @tmpHistoryNotes TABLE 
	(
		LogID			            bigint			NULL,
		CompletedDate	            varchar(30)     NULL,
		Description                 varchar(8000)   NULL,
		EventName                   varchar(250)    NULL,
		PertainsTo                  varchar(50)     NULL,
		TaskName                    varchar(250)    NULL,
		ClaimAspectServiceChannelID bigint          NULL,
		VehicleNumber				bigint          NULL,
		InsuranceCompanyID			bigint          NULL,
		LynxID						bigint          NULL,
		OwnerUserID					bigint          NULL,
		OwnerNameFirst				varchar(250)    NULL,
		OwnerNameLast				varchar(250)    NULL,
		OwnerEmail		            varchar(250)    NULL,
		AnalystUserID				bigint          NULL,
		AnalystNameFirst			varchar(250)    NULL,
		AnalystNameLast				varchar(250)    NULL,
		AnalystEmail		        varchar(250)    NULL,
		SupportUserID				bigint          NULL,
		SupportNameFirst			varchar(250)    NULL,
		SupportNameLast				varchar(250)    NULL,
		SupportEmail		        varchar(250)    NULL,
		CreatedRoleName	            varchar(50)     NULL,
		CreatedUserID				bigint          NULL,
		CreatedByNameFirst			varchar(250)    NULL,
		CreatedByNameLast			varchar(250)    NULL,
		CreatedEmail                varchar(250)    NULL,
		CreatedDate                 varchar(250)    NULL,
		CompletedRoleName	        varchar(50)     NULL,
		CompletedUserID				bigint		    NULL,
		CompletedByNameFirst		varchar(50)     NULL,
		CompletedByNameLast			varchar(50)     NULL,
		CompletedEmail				varchar(50)     NULL,
		EventID			            bigint			NULL
	)

	DECLARE @DocumentID                 udt_std_int_big
	DECLARE @InsuranceCompanyIDClaim    udt_std_id
	DECLARE @ProcName                   AS varchar(30)   -- Used for raise error stmts 

    IF @Debug = 1
    BEGIN
		PRINT '---> Checking last run times: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
        PRINT '---> Processing Started: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
    END

	SET @ProcName = 'EventsToHQ'
	SET @LastRunDateTime = CURRENT_TIMESTAMP
	SET @CurrentRunDateTime = CURRENT_TIMESTAMP

	-----------------------------------
	-- Start by getting the last 
	-- SysLastUpdatedDate from the 
	-- utb_HQRepl_History table 
	-----------------------------------
	IF @LastRunOverride IS NULL
	BEGIN
		SELECT 'NO Override'

		SELECT TOP 1 
			@LastRunDateTime = SysLastUpdatedDate 
		FROM 
			utb_HQRepl_History WITH(NOLOCK)
		ORDER BY 
			LynxID DESC
			, SysLastUpdatedDate DESC

		IF @Debug = 1
		BEGIN
			PRINT '---> NO Override: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
			PRINT '-----> @LastRunDateTime : ' + CONVERT(VARCHAR(50), @LastRunDateTime)
		END

		--SELECT @LastRunDateTime
	END
	ELSE
	BEGIN
		SELECT 'Override'
		SET @LastRunDateTime = @LastRunOverride

		IF @Debug = 1
		BEGIN
			PRINT '---> Override: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
			PRINT '-----> @LastRunOverride : ' + CONVERT(VARCHAR(50), @LastRunDateTime)
		END
	END

	--------------------------------------
	-- Compile note list together from all 
	-- the various entities
	--------------------------------------
	INSERT INTO @tmpNotes
	SELECT DISTINCT 
		c.LynxID,
		cascd.DocumentID,
		dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 1) AS PertainsTo,
		cat.Code,
		ca.ClaimAspectNumber,
		ca.ClaimAspectID,
		casc.ClaimAspectServiceChannelID,
		c.InsuranceCompanyID
	FROM  
		dbo.utb_claim_aspect_Service_Channel_document cascd WITH(NOLOCK)
		INNER join utb_Claim_Aspect_Service_Channel casc WITH(NOLOCK) 
			ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
		LEFT JOIN dbo.utb_claim_aspect ca WITH(NOLOCK) 
			ON (casc.ClaimAspectID = ca.ClaimAspectID)
		INNER JOIN dbo.utb_Claim c WITH(NOLOCK) 
			ON (c.LynxID = ca.LynxID)
		INNER JOIN dbo.utb_document d WITH(NOLOCK) 
			ON (cascd.DocumentID = d.DocumentID)
		INNER JOIN dbo.utb_document_type dt WITH(NOLOCK) 
			ON (d.DocumentTypeID = dt.DocumentTypeID)
		INNER JOIN dbo.utb_claim_aspect_type cat WITH(NOLOCK) 
			ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
	WHERE 
		dt.Name = 'Note'
		AND d.EnabledFlag = 1
		AND casc.EnabledFlag = 1
		AND d.SysLastUpdatedDate > @LastRunDateTime 
		AND casc.ServiceChannelCD = 'CS'

	-- TVD DEBUG
	-- SELECT * FROM @tmpNotes

	--------------------------------------
	-- Let's insert the Notes gathered next 
	-- into the common table
	--------------------------------------
	INSERT INTO @tmpHistoryNotes
	SELECT DISTINCT 
			d.DocumentID,
			dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
			IsNull(LEFT(u.NameFirst, 1), '') + '. ' + IsNull(u.NameLast, '') + ' - ' + IsNull(d.Note, ''),
			nt.Name,
			IsNull(n.PertainsTo, ''),
			NULL,
			n.ClaimAspectServiceChannelID,
			n.ClaimAspectNumber,
			n.InsuranceCompanyID,
			n.LynxID,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			r.name,
			d.CreatedUserID,
			u.NameFirst,
			u.NameLast,
			u.EmailAddress,
			CONVERT(VARCHAR(50), d.CreatedDate,121),
			r1.Name,
			d.syslastuserid,
			u1.NameFirst,
			u1.NameLast,
			u1.EmailAddress,
			999
	FROM    
		@tmpNotes n
		LEFT JOIN dbo.utb_document d WITH(NOLOCK) 
			ON (n.DocumentID = d.DocumentID)
		LEFT JOIN dbo.utb_user u WITH(NOLOCK) 
			ON (d.CreatedUserID = u.UserID)
		LEFT JOIN dbo.utb_user u1 WITH(NOLOCK) 
			ON (d.SysLastUserID = u1.UserID)
		LEFT JOIN dbo.utb_role r WITH(NOLOCK) 
			ON (d.CreatedUserRoleID = r.RoleID)
		LEFT JOIN dbo.utb_role r1 WITH(NOLOCK) 
			ON (d.CreatedUserRoleID = r1.RoleID)
		LEFT JOIN dbo.utb_note_type nt WITH(NOLOCK) 
			ON (nt.NoteTypeID = d.NoteTypeID)
		-- 10Oct2016 TVD - This removes dup LogID's in Repl Table
		LEFT JOIN utb_HQRepl_History rh WITH(NOLOCK)
			ON rh.LogID = d.DocumentID
				AND rh.EventID = 999
	WHERE 
		d.Note <> ''
		AND rh.LogID IS NULL -- 10Oct2016 TVD - This removes dup LogID's in Repl Table

	-- TVD DEBUG
	-- SELECT * FROM @tmpHistoryNotes

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
    	IF @Debug = 1
		BEGIN
			PRINT '--------> ERROR: Updating the @tmpHistoryNotes table - FAILED...'
			INSERT INTO utb_apd_event_log VALUES ('HQRepl Process', 'ERROR', 'tmpHistoryNotes INSERT Phase 2', 'Error occured inserting data into the temp table.  Details in EventXML.', 'LynxID: ' + CONVERT(VARCHAR(50), @LynxID), 0, CURRENT_TIMESTAMP)
		END    
	END

    IF @Debug = 1
	BEGIN
		PRINT '--------> Updating the Main utb_HQRepl_History table'
	END

	------------------------------
	-- Main - Select
	------------------------------
	--SELECT * FROM utb_HQRepl_History
	INSERT INTO utb_HQRepl_History
	SELECT DISTINCT  
			n.LogID
			, n.InsuranceCompanyID
			, n.LynxID AS LynxID
			, CASE 
				WHEN vt.ServiceChannelCD = 'PS' THEN 'DRP'
				WHEN vt.ServiceChannelCD IS NULL THEN 'DRP'
				ELSE
					vt.ServiceChannelCD
				END
			, CASE 
				WHEN n.VehicleNumber > 0 THEN 
					CONVERT(VARCHAR(10), n.InsuranceCompanyID) + '-' + 
					CASE 
						WHEN vt.ServiceChannelCD = 'PS' THEN 'DRP'
						WHEN vt.ServiceChannelCD IS NULL THEN 'DRP'
						ELSE
							vt.ServiceChannelCD
					END
				ELSE CONVERT(VARCHAR(10), n.InsuranceCompanyID)
				END AS InscCompAndServiceChannel
			, vt.ClaimAspectServiceChannelID
			, n.VehicleNumber
			, n.CreatedRoleName
			, n.CreatedUserID
			, n.CreatedByNameFirst
			, n.CreatedByNameLast
			, n.CreatedEmail
			, n.CreatedDate
			, n.CompletedRoleName
			, n.CompletedUserID
			, n.CompletedByNameFirst
			, n.CompletedByNameLast
			, n.CompletedEmail
			, n.CompletedDate
			, n.EventID
			, n.EventName
			, SUBSTRING(REPLACE(Description,'''',''),0,500)
			, PertainsTo
			, isnull(n.TaskName,'') AS TaskName
			, 0
			, @CurrentRunDateTime
			, n.OwnerUserID
			, n.OwnerNameFirst
			, n.OwnerNameLast
			, n.OwnerEmail
			, n.AnalystUserID
			, n.AnalystNameFirst
			, n.AnalystNameLast
			, n.AnalystEmail
			, n.SupportUserID
			, n.SupportNameFirst
			, n.SupportNameLast
			, n.SupportEmail
	FROM 
		@tmpHistoryNotes n
		LEFT OUTER JOIN (
			Select      casc.ClaimAspectServiceChannelID,
						CASE
							WHEN fn.Code = 'PS' THEN 'DRP'
							WHEN fn.Code IS NULL THEN 'DRP'
							ELSE 
								fn.Code
							END as 'ServiceChannelCD',
						fn.Name as 'ServiceChannelCDName'
			from        utb_Claim_Aspect_Service_Channel casc WITH(NOLOCK)
			inner join  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') fn
			on          fn.Code = casc.ServiceChannelCD
		) vt
		ON vt.ClaimAspectServiceChannelID = n.ClaimAspectServiceChannelID
		LEFT JOIN utb_HQRepl_History rh WITH(NOLOCK)
			ON rh.LogID = n.LogID
		WHERE 
			rh.LogID IS NULL
	ORDER BY 
		CompletedDate 

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
    	IF @Debug = 1
		BEGIN
			PRINT '--------> ERROR: Updating the utb_HQRepl_History table - FAILED...'
			INSERT INTO utb_apd_event_log VALUES ('HQRepl Process', 'ERROR', 'utb_HQRepl_History INSERT', 'Error occured inserting data into the temp table.  Details in EventXML.', 'LynxID: ' + CONVERT(VARCHAR(50), @LynxID), 0, CURRENT_TIMESTAMP)
		END    
	END

	IF @Debug = 1
	BEGIN
		PRINT '---> All processing Complete: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
	END
END

GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHQReplHistoryGenerateData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHQReplHistoryGenerateData TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/