-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimMessageGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimMessageGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimMessageGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns detail of a given claim in XML format.
*
* PARAMETERS:   @LynxID                 The LynxID to get information for 
*
* VERSION:		1.0 - TVD - 30Jan2019 - XML Explicit Version
*			  		      - Add Rental Management if available
*
* RESULT SET:
* XML stream containing claim details
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimMessageGetDetailXML
    @LynxID                 udt_std_id_big,
    @UserID                 udt_std_id_big
AS
BEGIN
    DECLARE @now as datetime

    DECLARE @AdjusterFName as varchar(100)
    DECLARE @AdjusterLName as varchar(100)
    DECLARE @AdjusterPhone as varchar(100)
    DECLARE @AdjusterEmail as varchar(100)
    DECLARE @AdjusterOfficeName as varchar(100)
    DECLARE @InsuredName as varchar(100)
    DECLARE @ClaimantName as varchar(100)
    DECLARE @Party as varchar(2)
    DECLARE @ServiceChannelCD as varchar(2)
    DECLARE @VehicleDrivable as bit
    DECLARE @VehicleYear as varchar(100)
    DECLARE @VehicleMake as varchar(100)
    DECLARE @VehicleModel as varchar(100)
    DECLARE @CoverageName as varchar(100)
    DECLARE @Deductible as varchar(100)
    DECLARE @ShopName as varchar(100)
    DECLARE @ShopAddress as varchar(100)
    DECLARE @ShopCity as varchar(100)
    DECLARE @ShopState as varchar(100)
    DECLARE @ShopZip as varchar(100)
    DECLARE @ShopPhone as varchar(25)
    DECLARE @ShopFax as varchar(25)
    DECLARE @ShopEmail as varchar(100)
    DECLARE @ShopContact as varchar(100)
    DECLARE @ExpectedEstReceiveDt as varchar(10)
    DECLARE @RentalDailyAmt as varchar(100)
    DECLARE @RentalMax as varchar(100)
    DECLARE @ClaimOwnerName as varchar(100)
    DECLARE @ClaimOwnerPhone as varchar(100)
    DECLARE @ClaimOwnerFax as varchar(100)
    DECLARE @ClaimOwnerEmail as varchar(100)
    DECLARE @ClaimAnalystName as varchar(100)
    DECLARE @ClaimAnalystPhone as varchar(100)
    DECLARE @ClaimAnalystFax as varchar(100)
    DECLARE @ClaimAnalystEmail as varchar(100)
    DECLARE @RepairStartDt as varchar(100)
    DECLARE @RepairEndDt as varchar(100)
    DECLARE @DaysToRepair as varchar(100)
    DECLARE @RepairChangeReasonComment as varchar(250)
    DECLARE @RepairChangeReasonID as int
    DECLARE @InspectionDate as varchar(100)
    DECLARE @PreviousInspectionDate as varchar(100)
    DECLARE @GrossEstimateAmount as varchar(100)
    DECLARE @BettermentAmount as decimal(9, 2)
    DECLARE @OtherAJAmount as decimal(9, 2)
    DECLARE @NetEstimateAmount as varchar(100)
    DECLARE @NetDemandAmount as varchar(100)
    DECLARE @EstimateDocumentID as bigint
    DECLARE @AuditedEstimateDocumentID as bigint
    DECLARE @AuditedEstimateAgreedPriceMetCD as varchar(2)
    DECLARE @AuditedEstimateSequenceNumber as int
    DECLARE @OriginalEstimateDocumentID as bigint
    DECLARE @OriginalEstimateNetAmount as varchar(100)
    DECLARE @OriginalEstimateSequenceNumber as int
    DECLARE @TLPercentage as decimal(3,0)
    DECLARE @RequestorName as varchar(100)
    DECLARE @SourceApplicationID as int
    DECLARE @SourceApplicationName as varchar(50)
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @InsuranceCompanyID as int
    DECLARE @ClientClaimNumber as varchar(250)
    DECLARE @PolicyNumber as varchar(250)
    DECLARE @LossDate as varchar(15)
    DECLARE @IntakeFinishDate as varchar(15)
    DECLARE @BookValueAmt as decimal(9, 2)
    DECLARE @ApprovedNetEstimateAmount as decimal(9, 2)
    DECLARE @FinalNetEstimateAmount as decimal(9, 2)
    
    DECLARE @LetterOfGuaranteeAmount as decimal(9, 2)
    DECLARE @SettlementAmount as decimal(9, 2)
    DECLARE @AdvanceAmount as decimal(9, 2)
    DECLARE @PayoffAmount as decimal(9, 2)
    DECLARE @LienHolderName as varchar(100)
    DECLARE @LienHolderAddress1 as varchar(100)
    DECLARE @LienHolderAddress2 as varchar(100)
    DECLARE @LienHolderAddressCity as varchar(100)
    DECLARE @LienHolderAddressState as varchar(2)
    DECLARE @LienHolderAddressZip as varchar(5)
    
    DECLARE @SalvageName as varchar(100)
    DECLARE @SalvageAddress1 as varchar(100)
    DECLARE @SalvageAddress2 as varchar(100)
    DECLARE @SalvageAddressCity as varchar(100)
    DECLARE @SalvageAddressState as varchar(2)
    DECLARE @SalvageAddressZip as varchar(5)
    DECLARE @SalvageEmailAddress as varchar(100)
    DECLARE @SalvageFax as varchar(15)
    DECLARE @SalvageControlNumber as varchar(100)

    DECLARE @ClaimAspectIDWork  as bigint
    DECLARE @DocumentID as bigint
    DECLARE @EstSummaryTypeIDNetTotal as int
    DECLARE @SourceApplicationPassThruData as varchar(1000)
    DECLARE @InitialAssignmentTypeID as int
    DECLARE @delimiter as varchar(1)
    DECLARE @Business1PersonnelTypeID as udt_std_id
    
    DECLARE @debug as bit
    DECLARE @ProcName as varchar(50)

    DECLARE @PursuitAuditShopInfo TABLE
    (
        Position    int,
        Value       varchar(1000)
    )
    
    DECLARE @tmpData TABLE
    (
        ClaimAspectID               bigint          NOT NULL,
        ClaimAspectNumber           int             NULL,
        ServiceChannelCD            varchar(2)      NULL,
        Party                       varchar(2)      NULL,
	     ClaimantName                varchar(100)    NULL,
        VehicleDrivable             bit             NULL,
	     VehicleYear                 varchar(100)    NULL,
	     VehicleMake                 varchar(100)    NULL,
	     VehicleModel                varchar(100)    NULL,
	     CoverageName                varchar(100)    NULL,
	     Deductible                  varchar(100)    NULL,
	     ShopName                    varchar(100)    NULL,
	     ShopAddress                 varchar(100)    NULL,
	     ShopCity                    varchar(100)    NULL,
	     ShopState                   varchar(100)    NULL,
	     ShopZip                     varchar(100)    NULL,
        ShopPhone                   varchar(25)     NULL,
	     ShopFax                     varchar(25)     NULL,
        ShopEmail                   varchar(100)    NULL,
        ShopContact                 varchar(100)    NULL,
	     ExpectedEstReceiveDt        varchar(10)     NULL,
	     ClaimOwnerName              varchar(100)    NULL,
	     ClaimOwnerPhone             varchar(100)    NULL,
	     ClaimOwnerFax               varchar(100)    NULL,
	     ClaimOwnerEmail             varchar(100)    NULL,
	     ClaimAnalystName            varchar(100)    NULL,
	     ClaimAnalystPhone           varchar(100)    NULL,
	     ClaimAnalystFax             varchar(100)    NULL,
	     ClaimAnalystEmail           varchar(100)    NULL,
	     RepairStartDt               varchar(100)    NULL,
	     RepairEndDt                 varchar(100)    NULL,
	     DaysToRepair                varchar(100)    NULL,
	     RepairChangeReasonComment	varchar(250)    NULL,
	     RepairChangeReasonID        int             NULL,
	     InspectionDate              varchar(100)    NULL,
	     PreviousInspectionDate      varchar(100)    NULL,
	     GrossEstimateAmount         varchar(100)    NULL,
        OtherAJAmount               varchar(100)    NULL,
	     NetEstimateAmount           varchar(100)    NULL,
	     NetDemandAmount             varchar(100)    NULL,
        EstimateDocumentID          bigint          NULL,
        AuditedEstimateDocumentID   bigint          NULL,
        AuditedEstimateAgreedPriceMetCD varchar(2)  NULL,
        AuditedEstimateSequenceNumber   int         NULL,
        OriginalEstimateDocumentID  bigint          NULL,
        OriginalEstimateNetAmount   varchar(100)    NULL,
        OriginalEstimateSequenceNumber  int         NULL,
        SourceApplicationID         int             NULL,
        SourceApplicationName       varchar(50)     NULL,
        BookValueAmt                decimal(9, 2)   NULL,
        ApprovedNetEstimateAmount   decimal(9, 2)   NULL,
        FinalNetEstimateAmount      decimal(9, 2)   NULL,
        LetterOfGuaranteeAmount     decimal(9, 2)   NULL,
        SettlementAmount            decimal(9, 2)   NULL,
        AdvanceAmount               decimal(9, 2)   NULL,
        PayoffAmount                decimal(9, 2)   NULL,
        LienHolderName              varchar(100)    NULL,
        LienHolderAddress1          varchar(100)    NULL,
        LienHolderAddress2          varchar(100)    NULL,
        LienHolderAddressCity       varchar(100)    NULL,
        LienHolderAddressState      varchar(2)      NULL,
        LienHolderAddressZip        varchar(5)      NULL,
        SalvageControlNumber        varchar(100)    NULL,
        SalvageName                 varchar(100)    NULL,
        SalvageAddress1             varchar(100)    NULL,
        SalvageAddress2             varchar(100)    NULL,
        SalvageAddressCity          varchar(100)    NULL,
        SalvageAddressState         varchar(2)      NULL,
        SalvageAddressZip           varchar(5)      NULL,
        SalvageEmailAddress         varchar(100)    NULL,
        SalvageFax                  varchar(15)     NULL,

		RentalID					INT				  NULL,
		RentalAgency				VARCHAR(100)	  NULL,
		RentalAgencyAddress1		VARCHAR(100)	  NULL,
		RentalAgencyCity			VARCHAR(100)	  NULL,
		RentalAgencyState			VARCHAR(2)		  NULL,
		RentalAgencyZip				VARCHAR(10)		  NULL,
		RentalAgencyPhone			VARCHAR(12)		  NULL,
		ResConfNumber				VARCHAR(15)		  NULL,
		VehicleClass				VARCHAR(25)		  NULL,
		RentalStatus				VARCHAR(15)		  NULL,
		RateTypeCD					VARCHAR(15)		  NULL,
		Rate						MONEY			  NULL,
		TaxRate						MONEY			  NULL,
		TaxedRate					MONEY			  NULL,
		Rental						MONEY			  NULL,
		RentalNoOfDays				INT			      NULL,
		AuthPickupDate				DATETIME		  NULL,
		StartDate					DATETIME		  NULL,
		EndDate						DATETIME		  NULL,
		SysLastUserID				INT				  NULL,
		SysLastUpdatedDate			DATETIME		  NULL
    )

    SET @ProcName = 'uspClaimMessageGetDetailXML'
    SET @debug = 0

    SET @now = CURRENT_TIMESTAMP

    --SET @LynxID = 988712 --988712, 988712, 1005816

    SELECT @EstSummaryTypeIDNetTotal = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'Net Total'

    SELECT @RequestorName = NameFirst + ' ' + NameLast
    FROM utb_user
    WHERE UserID = @UserID

    INSERT INTO @tmpData
    (ClaimAspectID, ClaimAspectNumber)
    SELECT ClaimAspectID, ClaimAspectNumber
    FROM utb_claim_aspect
    WHERE LynxID = @LynxID
      AND ClaimAspectTypeID = 9
      AND EnabledFlag = 1
    ORDER BY ClaimAspectNumber

    -- Claim level data
    -- Adjuster Name
    SELECT @AdjusterFName = u.NameFirst,
           @AdjusterLName = u.NameLast,
           @AdjusterPhone = u.PhoneAreaCode + ' ' + u.PhoneExchangeNumber + ' ' + u.PhoneUnitNumber +
                            case
                              when u.PhoneExtensionNumber <> '' then ' x' + u.PhoneExtensionNumber
                              else ''
                            end,
           @AdjusterEmail = u.EmailAddress,
           @AdjusterOfficeName = o.Name,
           @TLPercentage = isNull(i.TotalLossWarningPercentage, 0) * 100,
           @InsuranceCompanyName = i.Name,
           @InsuranceCompanyID = c.InsuranceCompanyID,
           @ClientClaimNumber = ClientClaimNumber,
           @PolicyNumber = PolicyNumber,
           @LossDate = convert(varchar, LossDate, 101),
           @IntakeFinishDate = convert(varchar, IntakeFinishDate, 101)
    FROM utb_claim c
    left join utb_user u ON c.CarrierRepUserID = u.UserID
    left join utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    left join utb_office o ON u.OfficeID = o.OfficeID
    where c.LynxID = @LynxID
    
    -- InsuredName
    SELECT @InsuredName = CASE
                            WHEN i.BusinessName IS NOT NULL THEN i.BusinessName
                            ELSE i.NameFirst + ' ' + i.NameLast
                          END,
            @LynxID = ca.LynxID
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_aspect_involved cai on (ca.ClaimAspectID = cai.ClaimAspectID)
    LEFT JOIN dbo.utb_involved i on (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir on (i.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt on (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE ca.LynxID = @LynxID
      AND cai.EnabledFlag = 1 
      AND ca.ClaimAspectTypeID = 0
      AND (irt.Name = 'Insured' OR irt.Name IS NULL)

    -- Rental Coverage Information
    SELECT  @RentalDailyAmt = isNull(convert(varchar, LimitDailyAmt), ''),
            @RentalMax = isNull(convert(varchar, LimitAmt), '')
    FROM utb_claim_coverage cc 
    WHERE cc.LynxID = @LynxID
      AND cc.EnabledFlag = 1
      AND cc.CoverageTypeCD = 'RENT'

    IF @debug = 1
    BEGIN
        PRINT '@InsuredName = ' + @InsuredName
        PRINT '@RentalDailyAmt = ' + convert(varchar, @RentalDailyAmt)
        PRINT '@RentalMax = ' + convert(varchar, @RentalMax)
        PRINT ''
    END

    SELECT  @Business1PersonnelTypeID = PersonnelTypeID
      FROM  dbo.utb_personnel_type
      WHERE Name = 'Shop Manager'     -- personnel associated with utb_shop_location

    DECLARE csrClaimAspect CURSOR FOR
        SELECT ClaimAspectID FROM @tmpData

    OPEN csrClaimAspect

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrClaimAspect
    INTO @ClaimAspectIDWork

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        SET @ClaimantName = ''
        SET @ServiceChannelCD = ''
        SET @Party = '' 
        SET @VehicleDrivable = 0
        SET @VehicleYear = ''
        SET @VehicleMake = ''
        SET @VehicleModel = ''
        SET @CoverageName = ''
        SET @Deductible = ''
        SET @ShopName = ''
        SET @ShopAddress = ''
        SET @ShopCity = ''
        SET @ShopState = ''
        SET @ShopZip = ''
        SET @ShopPhone = ''
        SET @ShopFax = ''
        SET @ShopEmail = ''
        SET @ShopContact = ''
        SET @ExpectedEstReceiveDt = ''
        SET @ClaimOwnerName = ''
        SET @ClaimOwnerPhone = ''
        SET @ClaimOwnerFax = ''
        SET @ClaimOwnerEmail = ''
        SET @ClaimAnalystName = ''
        SET @ClaimAnalystPhone = ''
        SET @ClaimAnalystFax = ''
        SET @ClaimAnalystEmail = ''
        SET @RepairStartDt = ''
        SET @RepairEndDt = ''
        SET @DaysToRepair = ''
        SET @RepairChangeReasonComment = ''
        SET @RepairChangeReasonID = NULL
        SET @InspectionDate = ''
        SET @PreviousInspectionDate = ''
        SET @GrossEstimateAmount = ''
        SET @BettermentAmount = 0
        SET @OtherAJAmount = 0
        SET @NetEstimateAmount = ''
        SET @NetDemandAmount = ''
        SET @EstimateDocumentID = NULL
        SET @AuditedEstimateDocumentID = NULL
        SET @AuditedEstimateAgreedPriceMetCD = NULL
        SET @AuditedEstimateSequenceNumber = NULL
        SET @OriginalEstimateDocumentID = NULL
        SET @OriginalEstimateNetAmount = NULL
        SET @OriginalEstimateSequenceNumber  = NULL
        SET @SourceApplicationID = NULL
        SET @SourceApplicationName = NULL
        SET @BookValueAmt = NULL
        SET @ApprovedNetEstimateAmount = NULL
        SET @FinalNetEstimateAmount = NULL
        SET @LetterOfGuaranteeAmount = NULL
        SET @SettlementAmount = NULL
        SET @AdvanceAmount = NULL
        SET @PayoffAmount = NULL
        SET @LienHolderName = NULL
        SET @LienHolderAddress1 = NULL
        SET @LienHolderAddress2 = NULL
        SET @LienHolderAddressCity = NULL
        SET @LienHolderAddressState = NULL
        SET @LienHolderAddressZip = NULL
        SET @SalvageControlNumber = NULL
        SET @SalvageName = NULL
        SET @SalvageAddress1 = NULL
        SET @SalvageAddress2 = NULL
        SET @SalvageAddressCity = NULL
        SET @SalvageAddressState = NULL
        SET @SalvageAddressZip = NULL
        SET @SalvageEmailAddress = NULL
        SET @SalvageFax = NULL
        SET @InitialAssignmentTypeID = NULL
        SET @SourceApplicationPassThruData = NULL

        SELECT @ClaimantName = CASE
                                WHEN i.BusinessName IS NOT NULL THEN i.BusinessName
                                ELSE i.NameFirst + ' ' + i.NameLast
                              END
        FROM dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_involved cai on (ca.ClaimAspectID = cai.ClaimAspectID)
        LEFT JOIN dbo.utb_involved i on (cai.InvolvedID = i.InvolvedID)
        LEFT JOIN dbo.utb_involved_role ir on (i.InvolvedID = ir.InvolvedID)
        LEFT JOIN dbo.utb_involved_role_type irt on (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND cai.EnabledFlag = 1 
          AND ca.ClaimAspectTypeID = 9
          AND (irt.Name = 'Owner' OR irt.Name IS NULL)

        SELECT  @Party = ExposureCD,    
                @VehicleYear = VehicleYear,
                @VehicleMake = Make,
                @VehicleModel = Model,
                @VehicleDrivable = DriveableFlag,
                @SourceApplicationID = ca.SourceApplicationID,
                @SourceApplicationName = a.Name,
                @BookValueAmt = cv.BookValueAmt,
                @SourceApplicationPassThruData = ca.SourceApplicationPassThruData,
                @InitialAssignmentTypeID = ca.InitialAssignmentTypeID
        FROM utb_claim_aspect ca
        LEFT JOIN utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID
        LEFT JOIN utb_application a ON ca.SourceApplicationID = a.ApplicationID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND ca.EnabledFlag = 1

        -- Coverage Information
        SELECT  @CoverageName = isNull(tmpRef.Name, ''),
                @Deductible = isNull(convert(varchar, cc.DeductibleAmt), '')
        FROM utb_claim_aspect ca
        LEFT JOIN (select Code, Name 
                   from dbo.ufnUtilityGetReferenceCodes('utb_client_coverage_type', 'CoverageProfileCD')) tmpRef ON ca.CoverageProfileCD = tmpRef.Code
        LEFT JOIN utb_client_coverage_type cct ON ca.CoverageProfileCD = cct.CoverageProfileCD
        LEFT JOIN utb_claim_coverage cc ON cct.ClientCoverageTypeID = cc.ClientCoverageTypeID AND ca.LynxID = cc.LynxID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND ca.EnabledFlag = 1
          AND cc.EnabledFlag = 1

        -- Shop Information for the primary channel
        SELECT @ShopName = isNull(sl.Name, ''),
               @ShopAddress = isNull(sl.Address1, ''),
               @ShopCity = isNull(sl.AddressCity, ''), 
               @ShopState = isNull(sl.AddressState, ''),
               @ShopZip = isNull(sl.AddressZip, ''),
               @ShopPhone = isNull('(' + sl.PhoneAreaCode + ') ' + sl.PhoneExchangeNumber + ' ' + sl.PhoneUnitNumber, ''),
               @ShopFax = isNull('(' + sl.FaxAreaCode + ') ' + sl.FaxExchangeNumber + ' ' + sl.FaxUnitNumber, ''),
               @ShopEmail = sl.EmailAddress,
               @ShopContact = isNull((select top 1 p.Name
                                        from dbo.utb_shop_location_personnel slp
                                        LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
                                        LEFT JOIN dbo.utb_personnel_type pt ON (p.PersonnelTypeID = pt.PersonnelTypeID)
                                        where slp.ShopLocationID = sl.ShopLocationID
                                          and pt.PersonnelTypeID = @Business1PersonnelTypeID), ''),
               @ExpectedEstReceiveDt = isNull(
                                                CASE
                                                    WHEN a.AssignmentDate IS NOT NULL THEN CONVERT(varchar, DATEADD(day, 2, a.AssignmentDate), 101)
                                                    ELSE CONVERT(varchar, DATEADD(day, 2, @now), 101)
                                                END
                                        , ''),
               @ServiceChannelCD = casc.ServiceChannelCD
        FROM utb_assignment a 
        LEFT JOIN utb_claim_aspect_service_channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
        WHERE casc.ClaimAspectID = @ClaimAspectIDWork
          AND casc.EnabledFlag = 1
          AND casc.PrimaryFlag = 1 AND a.assignmentsequencenumber = 1
          AND a.CancellationDate IS NULL

        IF @InitialAssignmentTypeID = 15
        BEGIN
            DELETE FROM @PursuitAuditShopInfo
            
            SET @delimiter = char(255)
            
            INSERT INTO @PursuitAuditShopInfo
            SELECT strIndex, Value
            FROM dbo.ufnUtilityParseString(@SourceApplicationPassThruData, @delimiter, 1)
            
            IF (SELECT Count(*) FROM @PursuitAuditShopInfo) = 10
            BEGIN
                SELECT @ShopName = Value FROM @PursuitAuditShopInfo WHERE Position = 1
                SELECT @ShopAddress = Value FROM @PursuitAuditShopInfo WHERE Position = 2
                SELECT @ShopCity = Value FROM @PursuitAuditShopInfo WHERE Position = 4
                SELECT @ShopState = Value FROM @PursuitAuditShopInfo WHERE Position = 5
                SELECT @ShopZip = Value FROM @PursuitAuditShopInfo WHERE Position = 3
                SELECT @ShopPhone = Value FROM @PursuitAuditShopInfo WHERE Position = 7
                SELECT @ShopFax = Value FROM @PursuitAuditShopInfo WHERE Position = 8
                SELECT @ShopEmail = Value FROM @PursuitAuditShopInfo WHERE Position = 9
                SELECT @ShopContact = 'Shop Contact'
            END
            
        END
        
        
        -- Entity Information
        SELECT  @ClaimOwnerName = isNull(uo.NameFirst + ' ' + uo.NameLast, ''),
                @ClaimOwnerPhone = isNull(uo.PhoneAreaCode + uo.PhoneExchangeNumber + uo.PhoneUnitNumber, ''),
                @ClaimOwnerFax = isNull(uo.FaxAreaCode + uo.FaxExchangeNumber + uo.FaxUnitNumber, ''),
                @ClaimOwnerEmail = isNull(uo.EmailAddress, ''),
                @ClaimAnalystName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
                @ClaimAnalystPhone = isNull(ua.PhoneAreaCode + ua.PhoneExchangeNumber + ua.PhoneUnitNumber, ''),
                @ClaimAnalystFax = isNull(ua.FaxAreaCode + ua.FaxExchangeNumber + ua.FaxUnitNumber, ''),
                @ClaimAnalystEmail = isNull(ua.EmailAddress, '')
        FROM utb_claim_aspect ca
        LEFT JOIN utb_user uo ON ca.OwnerUserID = uo.UserID
        LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
        
        -- Repair information for the primary channel
        SELECT top 1   @RepairStartDt = isNull(convert(varchar, casc.WorkStartDate, 101), ''),
                       @RepairEndDt = isNull(convert(varchar, casc.WorkEndDate, 101), ''),
                       @RepairChangeReasonID = isNUll(scwh.ReasonID, ''),
                       @RepairChangeReasonComment = isNUll(scwh.ChangeComment, ''),
                       @InspectionDate = isNull(convert(varchar, casc.InspectionDate, 101), ''),
                       @PreviousInspectionDate = isNull(convert(varchar, casc.InspectionDateOriginal, 101), '')
        FROM utb_claim_aspect_service_channel casc 
        LEFT JOIN dbo.utb_service_channel_work_history scwh ON casc.ClaimAspectServiceChannelID = scwh.ClaimAspectServiceChannelID
        WHERE casc.ClaimAspectID = @ClaimAspectIDWork
          AND casc.EnabledFlag = 1
          AND casc.PrimaryFlag = 1  
        ORDER BY HistoryID DESC

        SELECT @DaysToRepair =  CASE
                                    WHEN @RepairStartDt <> '' AND @RepairEndDt <> '' THEN dbo.ufnUtilityBusinessDaysDiff(CONVERT(datetime, @RepairStartDt), dateadd(day, 1, convert(datetime, @RepairEndDt)))
                                    ELSE ''
                                END

        -- Get the latest estimate/supplement
        SELECT TOP 1 @DocumentID = d.DocumentID
        FROM utb_document d
        LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
        LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
        LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND dt.EstimateTypeFlag = 1 
          AND d.EnabledFlag = 1
          AND casc.PrimaryFlag = 1
        ORDER BY  d.CreatedDate DESC, ds.VANFlag DESC

        IF @DocumentID IS NOT NULL
        BEGIN
            SELECT @NetEstimateAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'NetTotal'

            SELECT @GrossEstimateAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'RepairTotal'

            SELECT @BettermentAmount = isNull(AgreedExtendedAmt, 0)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'AJ'
              AND est.Name = 'Betterment'

            SELECT @OtherAJAmount = isNull(AgreedExtendedAmt, 0)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'AJ'
              AND est.Name = 'Other'

            SET @EstimateDocumentID = @DocumentID
        END

        SET @DocumentID = NULL
        
        -- Get the latest audited estimate/supplement
        SELECT TOP 1 @DocumentID = d.DocumentID,
                     @AuditedEstimateAgreedPriceMetCD = d.AgreedPriceMetCD,
                     @AuditedEstimateSequenceNumber = d.SupplementSeqNumber
        FROM utb_document d
        LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
        LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
        LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND dt.EstimateTypeFlag = 1 
          AND d.EnabledFlag = 1
          AND casc.PrimaryFlag = 1
          AND d.EstimateTypeCD = 'A'
        ORDER BY  d.CreatedDate DESC, ds.VANFlag DESC

        IF @DocumentID IS NOT NULL
        BEGIN
            SELECT @NetDemandAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'NetTotal'

            SET @AuditedEstimateDocumentID = @DocumentID
        END
        
        SET @DocumentID = NULL
        
        -- Get the original estimate/supplement
        SELECT TOP 1 @DocumentID = d.DocumentID,
                     @OriginalEstimateSequenceNumber = d.SupplementSeqNumber
        FROM utb_document d
        LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
        LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
        LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND dt.EstimateTypeFlag = 1 
          AND d.EnabledFlag = 1
          AND casc.PrimaryFlag = 1
          AND d.EstimateTypeCD = 'O'
          /*AND ((@AuditedEstimateSequenceNumber is NULL AND d.SupplementSeqNumber = 0) 
            OR (@AuditedEstimateSequenceNumber IS NOT NULL AND d.SupplementSeqNumber = @AuditedEstimateSequenceNumber))*/
        ORDER BY  d.CreatedDate DESC, ds.VANFlag DESC

        IF @DocumentID IS NOT NULL
        BEGIN
            SELECT @OriginalEstimateNetAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'NetTotal'

            SET @OriginalEstimateDocumentID = @DocumentID
        END
        
        -- Get the approved estimate
        SET @DocumentID = NULL
        
        SELECT TOP 1 @DocumentID = d.DocumentID
        FROM utb_document d
        LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
        LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
        LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND dt.EstimateTypeFlag = 1 
          AND d.EnabledFlag = 1
          AND casc.PrimaryFlag = 1
          AND d.EstimateTypeCD = 'O'
          AND d.ApprovedFlag = 1
        ORDER BY  d.CreatedDate DESC, ds.VANFlag DESC

        IF @DocumentID IS NOT NULL
        BEGIN
            SELECT @ApprovedNetEstimateAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'NetTotal'
        END
        
        -- Get the FBE estimate
        SET @DocumentID = NULL
        
        SELECT TOP 1 @DocumentID = d.DocumentID
        FROM utb_document d
        LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
        LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
        LEFT JOIN utb_document_source ds ON d.DocumentSourceID = ds.DocumentSourceID
        WHERE ca.ClaimAspectID = @ClaimAspectIDWork
          AND dt.EstimateTypeFlag = 1 
          AND d.EnabledFlag = 1
          AND casc.PrimaryFlag = 1
          AND (d.FinalBillEstimateFlag = 1 OR d.FinalEstimateFlag = 1)
        ORDER BY  d.CreatedDate DESC, ds.VANFlag DESC

        IF @DocumentID IS NOT NULL
        BEGIN
            SELECT @FinalNetEstimateAmount = convert(varchar, AgreedExtendedAmt)
            FROM dbo.utb_estimate_summary es
            LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            WHERE DocumentID = @DocumentID
              AND est.CategoryCD = 'TT'
              AND est.Name = 'NetTotal'
        END
        
        -- Total Loss data
        SELECT @LetterOfGuaranteeAmount = casc.LetterOfGuaranteeAmount,
               @SettlementAmount = casc.SettlementAmount,
               @AdvanceAmount = casc.AdvanceAmount,
               @PayoffAmount = casc.PayoffAmount,
               @SalvageControlNumber = casc.SalvageControlNumber,
               @LienHolderName = lh.Name,
               @LienHolderAddress1 = lh.Address1,
               @LienHolderAddress2 = lh.Address2,
               @LienHolderAddressCity = lh.AddressCity,
               @LienHolderAddressState = lh.AddressState,
               @LienHolderAddressZip = lh.AddressZip
        FROM utb_claim_aspect_service_channel casc 
        LEFT OUTER JOIN utb_lien_holder lh ON casc.LienHolderID = lh.LienHolderID
        WHERE casc.ClaimAspectID = @ClaimAspectIDWork
          AND casc.EnabledFlag = 1
          AND casc.PrimaryFlag = 1

        SELECT @SalvageName = sv.Name,
               @SalvageAddress1 = sv.Address1,
               @SalvageAddress2 = sv.Address2,
               @SalvageAddressCity = sv.AddressCity,
               @SalvageAddressState = sv.AddressState,
               @SalvageAddressZip = sv.AddressZip,
               @SalvageEmailAddress = sv.EmailAddress,
               @SalvageFax = sv.FaxAreaCode + sv.FaxExchangeNumber + sv.FaxUnitNumber
        FROM utb_claim_aspect_service_channel casc 
        LEFT OUTER JOIN utb_salvage_vendor sv ON casc.SalvageVendorID = sv.SalvageVendorID
        WHERE casc.ClaimAspectID = @ClaimAspectIDWork
          AND casc.EnabledFlag = 1
          AND casc.PrimaryFlag = 1

        IF @debug = 1
        BEGIN
            PRINT '@ClaimAspectIDWork = ' + convert(varchar, @ClaimAspectIDWork)
            PRINT '@ClaimantName = ' + @ClaimantName
            PRINT '@Party = ' + @Party
            PRINT '@CoverageName = ' + @CoverageName
            PRINT '@Deductible = ' + convert(varchar, @Deductible)
            PRINT '@ShopName = ' + convert(varchar, @ShopName)
            PRINT '@ExpectedEstReceiveDt = ' + convert(varchar, @ExpectedEstReceiveDt)
            PRINT '@RentalMax = ' + convert(varchar, @RentalMax)
            PRINT '@RepairStartDt = ' + convert(varchar, @RepairStartDt)
            PRINT '@RepairEndDt = ' + convert(varchar, @RepairEndDt)
            PRINT '@RepairChangeReasonComment = ' + convert(varchar, @RepairChangeReasonComment)
            PRINT '@DocumentID = ' + convert(varchar, @DocumentID)
            PRINT '@NetEstimateAmount = ' + @NetEstimateAmount
            PRINT '@NetDemandAmount = ' + @NetDemandAmount
            PRINT ''
        END

        UPDATE @tmpData
        SET ClaimantName = @ClaimantName,
            ServiceChannelCD = @ServiceChannelCD,
            Party = @Party,
            VehicleDrivable = @VehicleDrivable,
            VehicleYear = @VehicleYear,
            VehicleMake = @VehicleMake,
            VehicleModel = @VehicleModel,
            CoverageName = @CoverageName,
            Deductible = @Deductible,
            ShopName = @ShopName,
            ShopAddress = @ShopAddress,
            ShopCity = @ShopCity,
            ShopState = @ShopState,
            ShopZip = @ShopZip,
            ShopPhone = @ShopPhone,
            ShopFax = @ShopFax,
            ShopEmail = @ShopEmail,
            ShopContact = @ShopContact,
            ExpectedEstReceiveDt = @ExpectedEstReceiveDt,
            ClaimOwnerName = @ClaimOwnerName,
            ClaimOwnerPhone = ltrim(rtrim(@ClaimOwnerPhone)),
            ClaimOwnerFax = ltrim(rtrim(@ClaimOwnerFax)),
            ClaimOwnerEmail = @ClaimOwnerEmail,
            ClaimAnalystName = @ClaimAnalystName,
            ClaimAnalystPhone = ltrim(rtrim(@ClaimAnalystPhone)),
            ClaimAnalystFax = ltrim(rtrim(@ClaimAnalystFax)),
            ClaimAnalystEmail = @ClaimAnalystEmail,
            RepairStartDt = @RepairStartDt,
            RepairEndDt = @RepairEndDt,
            DaysToRepair = @DaysToRepair,
            RepairChangeReasonComment = @RepairChangeReasonComment,
            RepairChangeReasonID = @RepairChangeReasonID,
            InspectionDate = @InspectionDate,
            PreviousInspectionDate = @PreviousInspectionDate,
            GrossEstimateAmount = @GrossEstimateAmount,
            NetEstimateAmount = @NetEstimateAmount,
            OtherAJAmount = @BettermentAmount + @OtherAJAmount,
            NetDemandAmount = @NetDemandAmount,
            EstimateDocumentID = @EstimateDocumentID,
            AuditedEstimateDocumentID = @AuditedEstimateDocumentID,
            AuditedEstimateAgreedPriceMetCD = @AuditedEstimateAgreedPriceMetCD,
            AuditedEstimateSequenceNumber = @AuditedEstimateSequenceNumber,
            OriginalEstimateDocumentID = @OriginalEstimateDocumentID,
            OriginalEstimateNetAmount = @OriginalEstimateNetAmount,
            OriginalEstimateSequenceNumber = @OriginalEstimateSequenceNumber,
            SourceApplicationID = @SourceApplicationID,
            SourceApplicationName = @SourceApplicationName,
            BookValueAmt = @BookValueAmt,
            ApprovedNetEstimateAmount = @ApprovedNetEstimateAmount,
            FinalNetEstimateAmount = @FinalNetEstimateAmount,
            LetterOfGuaranteeAmount = @LetterOfGuaranteeAmount,
            SettlementAmount = @SettlementAmount,
            AdvanceAmount = @AdvanceAmount,
            PayoffAmount = @PayoffAmount,
            LienHolderName = @LienHolderName,
            LienHolderAddress1 = @LienHolderAddress1,
            LienHolderAddress2 = @LienHolderAddress2,
            LienHolderAddressCity = @LienHolderAddressCity,
            LienHolderAddressState = @LienHolderAddressState,
            LienHolderAddressZip = @LienHolderAddressZip,
            SalvageControlNumber = @SalvageControlNumber,
            SalvageName = @SalvageName,
            SalvageAddress1 = @SalvageAddress1,
            SalvageAddress2 = @SalvageAddress2,
            SalvageAddressCity = @SalvageAddressCity,
            SalvageAddressState = @SalvageAddressState,
            SalvageAddressZip = @SalvageAddressZip,
            SalvageEmailAddress = @SalvageEmailAddress,
            SalvageFax = @SalvageFax
        WHERE ClaimAspectID = @ClaimAspectIDWork

		-- Add Rental Mgt
		UPDATE
			@tmpData
		SET
			td.RentalID = cvr.RentalID
			, td.RentalAgency = cvr.RentalAgency
			, td.RentalAgencyAddress1 = cvr.RentalAgencyAddress1
			, td.RentalAgencyCity = cvr.RentalAgencyCity
			, td.RentalAgencyState = cvr.RentalAgencyState
			, td.RentalAgencyZip = cvr.RentalAgencyZip
			, td.RentalAgencyPhone = cvr.RentalAgencyPhone
			, td.ResConfNumber = cvr.ResConfNumber
			, td.VehicleClass = cvr.VehicleClass
			, td.RentalStatus = cvr.RentalStatus
			, td.RateTypeCD = cvr.RateTypeCD
			, td.Rate = cvr.Rate
			, td.TaxRate = cvr.TaxRate
			, td.Rental = cvr.Rental
			, td.RentalNoOfDays = cvr.RentalNoOfDays
			, td.AuthPickupDate = cvr.AuthPickupDate
			, td.StartDate = cvr.StartDate
			, td.EndDate = cvr.EndDate
			, td.SysLastUserID = cvr.SysLastUserID
			, td.SysLastUpdatedDate = cvr.SysLastUpdatedDate
		FROM
			@tmpData td
			INNER JOIN utb_claim_vehicle_rental cvr
				ON cvr.ClaimAspectID = td.ClaimAspectID
		WHERE
			cvr.ClaimAspectID = @ClaimAspectIDWork
			AND cvr.RentalID = 143
			AND cvr.EnabledFlag = 1

        FETCH next
        FROM csrClaimAspect
        INTO @ClaimAspectIDWork

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END

    CLOSE csrClaimAspect
    DEALLOCATE csrClaimAspect
    

    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        @LynxID as [Root!1!LynxID],
        @RequestorName as [Root!1!RequestorName],
        -- Claim
        Null as [Claim!2!InsuredName],
        Null as [Claim!2!RentalDailyAmt],
        Null as [Claim!2!RentalMax],
        Null as [Claim!2!AdjusterFName],
        Null as [Claim!2!AdjusterLName],
        Null as [Claim!2!AdjusterPhone],
        Null as [Claim!2!AdjusterEmail],
        Null as [Claim!2!AdjusterOfficeName],
        Null as [Claim!2!TLPercentage],
        Null as [Claim!2!InsuranceCompanyName],
        Null as [Claim!2!InsuranceCompanyID],
        Null as [Claim!2!ClientClaimNumber],
        Null as [Claim!2!PolicyNumber],
        Null as [Claim!2!LossDate],
        Null as [Claim!2!IntakeFinishDate],
        -- Vehicle
        Null as [Vehicle!3!ClaimAspectID],
        Null as [Vehicle!3!ClaimAspectNumber],
        Null as [Vehicle!3!ServiceChannelCD],
        Null as [Vehicle!3!ClaimantName],
        Null as [Vehicle!3!PartyCD],
        Null as [Vehicle!3!VehicleDrivable],
        Null as [Vehicle!3!VehicleYear],
        Null as [Vehicle!3!VehicleMake],
        Null as [Vehicle!3!VehicleModel],
        Null as [Vehicle!3!CoverageName],
        Null as [Vehicle!3!Deductible],
        Null as [Vehicle!3!ShopName],
        Null as [Vehicle!3!ShopAddress],
        Null as [Vehicle!3!ShopCity],
        Null as [Vehicle!3!ShopState],
        Null as [Vehicle!3!ShopZip],
        Null as [Vehicle!3!ShopPhone],
        Null as [Vehicle!3!ShopFax],
        Null as [Vehicle!3!ShopEmail],
        Null as [Vehicle!3!ShopContact],
        Null as [Vehicle!3!ExpectedEstReceiveDt],
        Null as [Vehicle!3!ClaimOwnerName],
        Null as [Vehicle!3!ClaimOwnerPhone],
        Null as [Vehicle!3!ClaimOwnerFax],
        Null as [Vehicle!3!ClaimOwnerEmail],
        Null as [Vehicle!3!ClaimAnalystName],
        Null as [Vehicle!3!ClaimAnalystPhone],
        Null as [Vehicle!3!ClaimAnalystFax],
        Null as [Vehicle!3!ClaimAnalystEmail],
        Null as [Vehicle!3!RepairStartDt],
        Null as [Vehicle!3!RepairEndDt],
        Null as [Vehicle!3!DaysToRepair],
        Null as [Vehicle!3!RepairChangeReasonComment],
        Null as [Vehicle!3!RepairChangeReasonID],
        Null as [Vehicle!3!InspectionDate],
        Null as [Vehicle!3!PreviousInspectionDate],
        Null as [Vehicle!3!GrossEstimateAmount],
        Null as [Vehicle!3!NetEstimateAmount],
        Null as [Vehicle!3!OtherAdjustmentEstimateAmount],
        Null as [Vehicle!3!NetDemandAmount],
        Null as [Vehicle!3!EstimateDocumentID],
        Null as [Vehicle!3!AuditedEstimateDocumentID],
        Null as [Vehicle!3!AuditedEstimateAgreedPriceMetCD],
        Null as [Vehicle!3!AuditedEstimateSequenceNumber],
        Null as [Vehicle!3!OriginalEstimateDocumentID],
        Null as [Vehicle!3!OriginalEstimateNetAmount],
        Null as [Vehicle!3!OriginalEstimateSequenceNumber],
        Null as [Vehicle!3!SourceApplicationID],
        Null as [Vehicle!3!SourceApplicationName],
        Null as [Vehicle!3!BookValueAmt],
        Null as [Vehicle!3!ApprovedNetEstimateAmount],
        Null as [Vehicle!3!FinalNetEstimateAmount],
        Null as [Vehicle!3!LetterOfGuaranteeAmount],
        Null as [Vehicle!3!SettlementAmount],
        Null as [Vehicle!3!AdvanceAmount],
        Null as [Vehicle!3!PayoffAmount],
        Null as [Vehicle!3!LienHolderName],
        Null as [Vehicle!3!LienHolderAddress1],
        Null as [Vehicle!3!LienHolderAddress2],
        Null as [Vehicle!3!LienHolderAddressCity],
        Null as [Vehicle!3!LienHolderAddressState],
        Null as [Vehicle!3!LienHolderAddressZip],
        Null as [Vehicle!3!SalvageControlNumber],
        Null as [Vehicle!3!SalvageName],
        Null as [Vehicle!3!SalvageAddress1],
        Null as [Vehicle!3!SalvageAddress2],
        Null as [Vehicle!3!SalvageAddressCity],
        Null as [Vehicle!3!SalvageAddressState],
        Null as [Vehicle!3!SalvageAddressZip],
        Null as [Vehicle!3!SalvageEmailAddress],
        Null as [Vehicle!3!SalvageFax],
        -- Rental Mgt
		Null as [RentalMgt!4!RentalID],
        --Null as [Vehicle!3!RentalID]
		NULL as [RentalMgt!4!RentalAgency],
		NULL as [RentalMgt!4!RentalAgencyAddress1],
		NULL as [RentalMgt!4!RentalAgencyCity],
		NULL as [RentalMgt!4!RentalAgencyState],
		NULL as [RentalMgt!4!RentalAgencyZip],
		NULL as [RentalMgt!4!RentalAgencyPhone],
		NULL as [RentalMgt!4!ResConfNumber],
		NULL as [RentalMgt!4!VehicleClass],
		NULL as [RentalMgt!4!RentalStatus],
		NULL as [RentalMgt!4!RateTypeCD	],
		NULL as [RentalMgt!4!Rate],
		NULL as [RentalMgt!4!TaxRate],
		NULL as [RentalMgt!4!TaxedRate],
		NULL as [RentalMgt!4!Rental],
		NULL as [RentalMgt!4!RentalNoOfDays],
		NULL as [RentalMgt!4!AuthPickupDate],
		NULL as [RentalMgt!4!StartDate],
		NULL as [RentalMgt!4!EndDate],
		NULL as [RentalMgt!4!SysLastUserID],
		NULL as [RentalMgt!4!SysLastUpdatedDate]
    UNION ALL

    SELECT
        2 as Tag,
        1 as Parent,
        --  Root
        Null, NULL,
        -- Claim
        @InsuredName,
        @RentalDailyAmt,
        @RentalMax,
        @AdjusterFName,
        @AdjusterLName,
        @AdjusterPhone,
        @AdjusterEmail,
        @AdjusterOfficeName,
        @TLPercentage,
        @InsuranceCompanyName,
        @InsuranceCompanyID,
        @ClientClaimNumber,
        @PolicyNumber,
        @LossDate,
        @IntakeFinishDate,
        -- Vehicle
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL,
        -- Rental Mgt
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL
    UNION ALL

    SELECT
        3 as Tag,
        2 as Parent,
        --  Root
        Null, NULL,
        -- Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Vehicle
        ClaimAspectID,
        ClaimAspectNumber,
        ServiceChannelCD,
        ClaimantName,
        Party,
        VehicleDrivable,
        VehicleYear,
        VehicleMake,
        VehicleModel,
        CoverageName,
        Deductible,
        ShopName,
        ShopAddress,
        ShopCity,
        ShopState,
        ShopZip,
        ShopPhone,
        ShopFax,
        ShopEmail,
        ShopContact,
        ExpectedEstReceiveDt,
        ClaimOwnerName,
        ClaimOwnerPhone,
        ClaimOwnerFax,
        ClaimOwnerEmail,
        ClaimAnalystName,
        ClaimAnalystPhone,
        ClaimAnalystFax,
        ClaimAnalystEmail,
        RepairStartDt,
        RepairEndDt,
        DaysToRepair,
        RepairChangeReasonComment,
        RepairChangeReasonID,
        InspectionDate,
        PreviousInspectionDate,
        GrossEstimateAmount,
        NetEstimateAmount,
        OtherAJAmount,
        NetDemandAmount,
        EstimateDocumentID,
        AuditedEstimateDocumentID,
        AuditedEstimateAgreedPriceMetCD,
        AuditedEstimateSequenceNumber,
        OriginalEstimateDocumentID,
        OriginalEstimateNetAmount,
        OriginalEstimateSequenceNUmber,
        SourceApplicationID,
        SourceApplicationName,
        BookValueAmt,
        isNull(convert(varchar,ApprovedNetEstimateAmount), ''),
        isNull(convert(varchar,FinalNetEstimateAmount), ''),
        isNull(convert(varchar,LetterOfGuaranteeAmount), ''),
        isNull(convert(varchar,SettlementAmount), ''),
        isNull(convert(varchar,AdvanceAmount), ''),
        isNull(convert(varchar,PayoffAmount), ''),
        isNull(LienHolderName, ''),
        isNull(LienHolderAddress1, ''),
        isNull(LienHolderAddress2, ''),
        isNull(LienHolderAddressCity, ''),
        isNull(LienHolderAddressState, ''),
        isNull(LienHolderAddressZip, ''),
        isNull(SalvageControlNumber, ''),
        isNull(SalvageName, ''),
        isNull(SalvageAddress1, ''),
        isNull(SalvageAddress2, ''),
        isNull(SalvageAddressCity, ''),
        isNull(SalvageAddressState, ''),
        isNull(SalvageAddressZip, ''),
        isNull(SalvageEmailAddress, ''),
        isNull(SalvageFax, ''),
        -- Rental Mgt
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL
    FROM @tmpData

    UNION ALL

    SELECT
        4 as Tag,
        2 as Parent,
        --  Root
        Null, NULL,
        -- Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Vehicle
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL,
        -- Rental Mgt
		ISNULL(RentalID,''),
		ISNULL(RentalAgency, ''),    
		ISNULL(RentalAgencyAddress1, ''),    
		ISNULL(RentalAgencyCity, ''),    
		ISNULL(RentalAgencyState, ''),    
		ISNULL(RentalAgencyZip, ''),   
		ISNULL(RentalAgencyPhone, ''),   
		ISNULL(ResConfNumber, ''),   
		ISNULL(VehicleClass, ''),   
		ISNULL(RentalStatus, ''),   
		ISNULL(RateTypeCD, ''),   
		ISNULL(Rate, ''),   
		ISNULL(TaxRate, ''),   
		ISNULL(TaxedRate, ''),   
		ISNULL(Rental, ''),   
		ISNULL(RentalNoOfDays, ''),   
		ISNULL(AuthPickupDate, ''),   
		ISNULL(StartDate, ''),   
		ISNULL(EndDate, ''),   
		ISNULL(SysLastUserID, ''),   
		ISNULL(SysLastUpdatedDate, '')   
    FROM @tmpData
    ORDER BY tag
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimMessageGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimMessageGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO