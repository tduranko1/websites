-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetHQInvoiceCloseAssignmentsWSXML 
END

GO 

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceGetHQInvoiceCloseAssignmentsWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Get a list of waiting HQ Invoice and Close Choice Shop Assignments from the database.
* Date:			14Dev2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	14Dec2015 - TVD - Initial development
*				02Aug2016 - TVD - Fixed issue related to Choice Shop fee per client.
*				27Oct2016 - TVD - Adding additional events for Cancel Claim and ReOpen Claim
*				27Jan2017 - TVD - Corrected an issue with the 95 Event.
*				04Dec2017 - TVD - Corrected an issue with multiple fee amounts.  Added check for Effective date
*				29Nov2018 - TVD - Corrected invoice when claim is closed issue.  Adding claim reopen on 107 event.
*				24Apr2019 - TVD - Improve performance to reduce chance of timeout.
*				09May2019 - TVD - ReDesigning the ReOpen claim code
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceGetHQInvoiceCloseAssignmentsWSXML]
AS
BEGIN
    SET NOCOUNT ON
 	
	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag
	DECLARE @ChoiceProcessFee					udt_std_money
	DECLARE @ChoiceProcessFeeID					udt_std_id
	DECLARE @AndroidUserID						udt_std_id
	DECLARE @ActiveDate							VARCHAR(20)

	DECLARE @EventClaimAspectID INT
	DECLARE @EventLynxID INT
	DECLARE @EventVehicleID INT
	DECLARE @EventClaimAspectServiceChannelID INT
	DECLARE @EventDesc VARCHAR(100)

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML'
	SET @Now = CURRENT_TIMESTAMP
	SET @AndroidUserID = 0

	SET @ActiveDate = '2018-12-31'

	---------------------------------------
	-- Create Temp Tables to hold values
	---------------------------------------
	---------------------------------------
	-- Create Temp Tables to hold values
	---------------------------------------
	CREATE TABLE #InvoiceCloseCancelReopen (
			ClaimAspectIDClaim				INT
			, ClaimAspectIDVehicle			INT
			, ClaimAspectServiceChannelID	INT
			, JobID							VARCHAR(255)
			, LynxID						INT
			, LogID							INT
			, EventID						INT
			, EventDesc						VARCHAR(50)
			, EventCompleted				DATETIME
			, JobStatus						VARCHAR(25)
			, DocumentTypeID				INT
			, DocumentTypeName				VARCHAR(50)
			, AssignmentID					INT
			, ChoiceProcessFeeID			INT	
			, ChoiceProcessFee				MONEY
			, HQCompleteID					INT
			, VehicleID						INT
			, UserID						INT
	)

	---------------------------------------
	-- Get Android 
	-- UserID = APDAndroidUser@lynxservices.com
	---------------------------------------
	SELECT @AndroidUserID = UserID FROM utb_user WHERE EmailAddress = 'APDAndroidUser@lynxservices.com'

	---------------------------------------
	-- Main Code - Check if HQ Invoice/Close 
	-- assignments exist - Event 107
	---------------------------------------
	-- HQ Invoice Close 
	IF EXISTS (
		SELECT 
			MAX(hl.CompletedDate)
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			--AND cas.StatusID = 800 -- Active CS
			--AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID = 107 -- Transaction Review Complete (Invoice and Close)	
			AND hhp.LogID IS NULL
			AND hl.CompletedDate > @ActiveDate
	)
	BEGIN
		------------------------------------------------------
		-- Choice Invoice/Close Events waiting to be processed.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Event 107 - Choice Invoice/Close Events are waiting to be processed.'
		END

		---------------------------------------------------------
		-- Add the 107 - Choice Invoice/Close event to 
		-- the #InvoiceCloseCancelReopen table
		---------------------------------------------------------
		INSERT INTO #InvoiceCloseCancelReopen
		SELECT 
			(SELECT ClaimAspectID FROM utb_claim_aspect WHERE lynxid = hj.LynxID AND ClaimAspectTypeID = 0)
			, hj.ClaimAspectID      --hj.ClaimAspectIDVehicle
			, hj.ClaimAspectServiceChannelID
			, hj.JobID						
			, hj.LynxID		
			, hl.LogID	
			, 107	
			, 'Transaction Review Complete'	
			, hl.CompletedDate	
			, hj.JobStatus				
			, hj.DocumentTypeID		
			, hj.DocumentTypeName		
			, hj.AssignmentID			
			, ISNULL(cf.ClientFeeID,0)	
			, ISNULL(cf.FeeAmount,0)		
			, hj.SysLastUserID			
			, hj.VehicleID				
			, @AndroidUserID				
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_client_fee cf
			ON cf.InsuranceCompanyID = hj.InscCompID
				AND cf.[Description] LIKE 'Choice Shop%'
				AND cf.EnabledFlag = 1
				AND hj.JobFinishedDate BETWEEN cf.EffectiveStartDate AND ISNULL(cf.EffectiveEndDate,CURRENT_TIMESTAMP)
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			--AND cas.StatusID = 800 -- Active CS
			--AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID = 107 -- Transaction Review Complete (Invoice and Close)	
			AND hhp.LogID IS NULL
			AND hl.CompletedDate > @ActiveDate
	END

	---------------------------------------------------------
	-- Add the 109 - HQ Cancel Claim event to 
	-- the #InvoiceCloseCancelReopen table
	---------------------------------------------------------
	-- HQ Cancel Claim 
	IF EXISTS (
		SELECT DISTINCT
			hj.ClaimAspectID      --hj.ClaimAspectIDVehicle
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID IN (95, 109) -- 95 - Estimate Rejected by HQ, 109 - HQ Cancel Claim 
			AND hl.EventID NOT IN (107) -- Transaction Review Complete (Invoice and Close)	
			AND hhp.LogID IS NULL
	)
	BEGIN
		------------------------------------------------------
		-- Choice Cancel Events waiting to be processed.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Event 109 - Choice Cancel Events are waiting to be processed.'
		END

		---------------------------------------------------------
		-- Add the 109 - HQ Cancel Claim event to 
		-- the #InvoiceCloseCancelReopen table
		---------------------------------------------------------
		INSERT INTO #InvoiceCloseCancelReopen
		SELECT DISTINCT
			(SELECT ClaimAspectID FROM utb_claim_aspect WHERE lynxid = hj.LynxID AND ClaimAspectTypeID = 0)
			, hj.ClaimAspectID      --hj.ClaimAspectIDVehicle
			, hj.ClaimAspectServiceChannelID
			, hj.JobID						
			, hj.LynxID	
			, hl.LogID	
			, 109	
			, 'HQ Cancel Claim'	
			, hl.CompletedDate	
			, hj.JobStatus				
			, hj.DocumentTypeID		
			, hj.DocumentTypeName		
			, hj.AssignmentID			
			, NULL
			, NULL
			, NULL
			, hj.VehicleID				
			, 0  --@AndroidUserID				
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID IN (95, 109) -- 95 - Estimate Rejected by HQ, 109 - HQ Cancel Claim 
			AND hl.EventID NOT IN (107) -- Transaction Review Complete (Invoice and Close)	
			AND hhp.LogID IS NULL
	END

	---------------------------------------------------------
	-- Add the 110 - HQ ReOpen Claim event to 
	-- the #InvoiceCloseCancelReopen table
	---------------------------------------------------------
	-- HQ ReOpen Claim 
	IF EXISTS (
		SELECT DISTINCT
			hj.ClaimAspectID    
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			--AND cas.StatusID = 999 -- Claim Cancelled 
			AND cas.StatusID IN (898, 994) -- 998-CS Service Channel IS Cancelled, 994-Vehicle IS Cancelled
			AND hl.EventID NOT IN (107) -- Transaction Review Complete (Invoice and Close)	
			AND hl.EventID IN (110) -- HQ ReOpen Claim	
			AND hhp.LogID IS NULL
	)
	BEGIN
		INSERT INTO #InvoiceCloseCancelReopen
		SELECT DISTINCT
			(SELECT ClaimAspectID FROM utb_claim_aspect WHERE lynxid = hj.LynxID AND ClaimAspectTypeID = 0)
			, hj.ClaimAspectID      --hj.ClaimAspectIDVehicle
			, hj.ClaimAspectServiceChannelID
			, hj.JobID						
			, hj.LynxID	
			, hl.LogID	
			, 110	
			, 'HQ ReOpen Claim'			
			, hl.CompletedDate	
			, hj.JobStatus				
			, hj.DocumentTypeID		
			, hj.DocumentTypeName		
			, hj.AssignmentID			
			, NULL
			, NULL
			, NULL
			, hj.VehicleID				
			, 0  --@AndroidUserID				
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_hyperquest_history_processed hhp
			ON hhp.LogID = hl.LogID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			--AND cas.StatusID = 999 -- Claim Cancelled 
			AND cas.StatusID IN (898, 994) -- 998-CS Service Channel IS Cancelled, 994-Vehicle IS Cancelled
			AND hl.EventID NOT IN (107) -- Transaction Review Complete (Invoice and Close)	
			AND hl.EventID IN (110) -- HQ ReOpen Claim	
			AND hhp.LogID IS NULL
	END

	IF EXISTS(SELECT LynxID FROM #InvoiceCloseCancelReopen)
	BEGIN
		---------------------------------------
		-- Check Claim Status - Closed, Complete
		-- and NOT Cancelled
		---------------------------------------
		IF EXISTS(
			SELECT 
				MAX(icc.EventCompleted)
			FROM
				#InvoiceCloseCancelReopen icc
				INNER JOIN utb_claim_aspect_status cas
				ON cas.ClaimAspectID = icc.ClaimAspectIDVehicle
			WHERE
				cas.StatusID IN (998,899) -- Vehicle Closed / Service Channel Closed
				AND cas.StatusID NOT IN (994, 996) -- Vehicle Cancelled / Claim Cancelled
		)
		BEGIN
			BEGIN TRANSACTION ReOpenClaim
				DECLARE ChoiceEvent_Cursor CURSOR FAST_FORWARD FOR
					SELECT 
						icc.LynxID
						, icc.ClaimAspectIDVehicle
						, icc.VehicleID
						, icc.ClaimAspectServiceChannelID
					FROM
						#InvoiceCloseCancelReopen icc
						INNER JOIN utb_claim_aspect_status cas
						ON cas.ClaimAspectID = icc.ClaimAspectIDVehicle
					WHERE
						cas.StatusID IN (998,899) -- Vehicle Closed / Service Channel Closed
						AND cas.StatusID NOT IN (994, 996) -- Vehicle Cancelled / Claim Cancelled

				OPEN ChoiceEvent_Cursor 
				FETCH NEXT FROM ChoiceEvent_Cursor
					INTO @EventLynxID, @EventClaimAspectID, @EventVehicleID, @EventClaimAspectServiceChannelID

					WHILE @@FETCH_STATUS = 0
					BEGIN 
						--SELECT @EventLynxID,@EventClaimAspectID,@EventVehicleID,@EventClaimAspectServiceChannelID
						-- ReOpen Service Channel
						EXEC uspWorkflowUpdateStatus @ClaimAspectID = @EventClaimAspectID, @StatusID = 800, @UserID = @AndroidUserID -- ReOpen Service Channel (CS)
						EXEC uspChoiceClaimHistoryEventInsDetail @LynxID = @EventLynxID, @EventID = 110, @ClaimAspectTypeID = 9, @ClaimAspectNumber = @EventVehicleID, @ServiceChannelCD = "CS", @ClaimAspectServiceChannelID = @EventClaimAspectServiceChannelID, @Description = "Vehicle Service Channel ReOpened by HQ Invoice/Close Event", @UserID = 0, @UserRoleID = 6, @StatusID=100, @ClaimVehicleAspectFlag=0

						-- ReOpen Vehicle
						EXEC uspWorkflowUpdateStatus @ClaimAspectID = @EventClaimAspectID, @StatusID = 100, @UserID = @AndroidUserID -- ReOpen Vehicle
						SET @EventDesc = 'Vehicle ' + CONVERT(VARCHAR(1), @EventVehicleID) + ' ReOpened by HQ Invoice/Close Event.'
						EXEC uspChoiceClaimHistoryEventInsDetail @LynxID = @EventLynxID, @EventID = 33, @ClaimAspectTypeID = 9, @ClaimAspectNumber = @EventVehicleID, @ServiceChannelCD = "CS", @ClaimAspectServiceChannelID = @EventClaimAspectServiceChannelID, @Description = @EventDesc, @UserID = 0, @UserRoleID = 6, @StatusID=100, @ClaimVehicleAspectFlag=0

						FETCH NEXT FROM ChoiceEvent_Cursor
						INTO @EventLynxID, @EventClaimAspectID, @EventVehicleID, @EventClaimAspectServiceChannelID
					END
				CLOSE ChoiceEvent_Cursor
				DEALLOCATE ChoiceEvent_Cursor
	    
				IF @@ERROR <> 0
				BEGIN
					-- Processing Error
					ROLLBACK TRANSACTION
					RAISERROR('%s: (ReOpenClaim) Error ReOpening the claim for the 107 Event', 16, 1, @ProcName)
					RETURN
				END 

				COMMIT TRANSACTION ReOpenClaim
		END	

		------------------------------------------------------
		-- Create HQ Invoice/Close Assignment list XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!LogID]
			, NULL				AS [Claim!2!EventID]
			, NULL				AS [Claim!2!EventDesc]
			, NULL				AS [Claim!2!EventCompleted]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!ChoiceProcessFeeID]
			, NULL				AS [Claim!2!ChoiceProcessFee]
			, NULL				AS [Claim!2!HQCompleteID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!UserID]

		UNION ALL

		SELECT  2 AS Tag,  
				1 AS Parent
				, NULL
				, ClaimAspectIDClaim
				, ClaimAspectIDVehicle
				, ClaimAspectServiceChannelID
				, JobID						
				, LynxID
				, LogID
				, EventID	
				, EventDesc				
				, EventCompleted
				, JobStatus					
				, DocumentTypeID			
				, DocumentTypeName			
				, AssignmentID				
				, ISNULL(ChoiceProcessFeeID,'')		
				, ISNULL(ChoiceProcessFee,'')			
				, ISNULL(HQCompleteID,'')
				, VehicleID					
				, UserID					
		FROM 
			#InvoiceCloseCancelReopen
		ORDER BY
			[Claim!2!EventCompleted]
		FOR XML EXPLICIT	
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- NO Choice Events waiting
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'NO HQ Invoice/Close/Cancel/ReOpen Events waiting'
		END

		------------------------------------------------------
		-- Create NO JOBS XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!ChoiceProcessFeeID]
			, NULL				AS [Claim!2!ChoiceProcessFee]
			, NULL				AS [Claim!2!HQCompleteID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!UserID]

		FOR XML EXPLICIT	
	END

	DROP TABLE #InvoiceCloseCancelReopen
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceGetHQInvoiceCloseAssignmentsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/