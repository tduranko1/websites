-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspUtilEstimateListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspUtilEstimateListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspUtilEstimateListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo    
* FUNCTION:     Temporary procedure used with the Estimate Correction Utility
*
* PARAMETERS:  
* (I) @ClaimAspectID        The ClaimAspectID To Retrieve
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspUtilEstimateListXML
    @ClaimAspectID      udt_std_id_big
AS
BEGIN
    -- Validate parameters
    
    IF NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID)
    BEGIN
        RAISERROR('Invalid ClaimAspectID', 16, 1)
    END
    
        
    -- Run Query
    
    SELECT  1    AS Tag,
            NULL AS Parent,
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            -- Claim Information
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!InsuredNameFirst],
            NULL AS [Claim!2!InsuredNameLast],
            NULL AS [Claim!2!OwnerBusinessName],
            NULL AS [Claim!2!OwnerNameFirst],
            NULL AS [Claim!2!OwnerNameLast],
            NULL AS [Claim!2!Make],
            NULL AS [Claim!2!Model],
            NULL AS [Claim!2!VehicleYear],
            NULL AS [Claim!2!Type],
            NULL AS [Claim!2!AssignmentType],
            NULL AS [Claim!2!ClaimAspectNumber],
            -- Estmate List
            NULL AS [Estimate!3!DocumentID],
            NULL AS [Estimate!3!DocumentType],
            NULL AS [Estimate!3!SequenceNumber],            
            NULL AS [Estimate!3!CreatedDate],
            NULL AS [Estimate!3!Source],
            NULL AS [Estimate!3!EstimateTotal],            
            NULL AS [Estimate!3!ImageLocation],
            NULL AS [Estimate!3!ImageType],
            NULL AS [Estimate!3!ReviewedFlag],
            NULL AS [Estimate!3!DuplicateFlag],
            NULL AS [Estimate!3!EstimateTypeCD],            
            NULL AS [Estimate!3!VANSourceFlag]
                        
            
    UNION ALL

     
    SELECT  2,
            1,
            NULL,
            -- Claim Information
            (SELECT  IsNull(si.BusinessName, '')
               FROM  dbo.utb_claim_aspect sca
               LEFT JOIN dbo.utb_claim_aspect_involved scai ON (sca.ClaimAspectID = scai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved si ON (scai.InvolvedID = si.InvolvedID)
               LEFT JOIN dbo.utb_involved_role sir ON (scai.InvolvedID = sir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type sirt ON (sir.InvolvedRoleTypeID = sirt.InvolvedRoleTypeID)
               WHERE sca.LynxID = ca.LynxID
                 AND sca.ClaimAspectTypeID = 0
                 AND sirt.Name = 'Insured'),
            (SELECT IsNull(si.NameFirst, '')
               FROM  dbo.utb_claim_aspect sca
               LEFT JOIN dbo.utb_claim_aspect_involved scai ON (sca.ClaimAspectID = scai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved si ON (scai.InvolvedID = si.InvolvedID)
               LEFT JOIN dbo.utb_involved_role sir ON (scai.InvolvedID = sir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type sirt ON (sir.InvolvedRoleTypeID = sirt.InvolvedRoleTypeID)
               WHERE sca.LynxID = ca.LynxID
                 AND sca.ClaimAspectTypeID = 0
                 AND sirt.Name = 'Insured'),
            (SELECT IsNull(si.NameLast, '')
               FROM  dbo.utb_claim_aspect sca
               LEFT JOIN dbo.utb_claim_aspect_involved scai ON (sca.ClaimAspectID = scai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved si ON (scai.InvolvedID = si.InvolvedID)
               LEFT JOIN dbo.utb_involved_role sir ON (scai.InvolvedID = sir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type sirt ON (sir.InvolvedRoleTypeID = sirt.InvolvedRoleTypeID)
               WHERE sca.LynxID = ca.LynxID
                 AND sca.ClaimAspectTypeID = 0
                 AND sirt.Name = 'Insured'),
            IsNull(oi.BusinessName, ''),
            IsNull(oi.NameFirst, ''),
            IsNull(oi.NameLast, ''),
            IsNull(cv.Make, ''),
            IsNull(cv.Model, ''),
            IsNull(cv.VehicleYear, ''),
            (SELECT  IsNull(Name, '')
               FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'ServiceChannelCD')
               WHERE Code = casc.ServiceChannelCD),
            --IsNull(at.Name, ''),Remarked-off to support the schema change M.A.20061219
            (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD),
            ca.ClaimAspectNumber,
            -- Estimate List
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL
       FROM dbo.utb_claim_aspect ca
		/*********************************************************************************
		Project: 210474 APD - Enhancements to support multiple concurrent service channels
		Note:	Added reference to utb_Claim_Aspect_Service_Channel to provide the value
				for ServiceChannelCD above
				M.A. 20061201
		*********************************************************************************/
	   LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
       and PrimaryFlag = 1 --Project:210474 APD Added to support the schema change M.A.20061219
       --Project:210474 APD Remarked-off the following to support the schema change M.A.20061219
       --LEFT JOIN dbo.utb_assignment_type at ON (ca.CurrentAssignmentTypeID = at.AssignmentTypeID)
       LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
       LEFT JOIN dbo.utb_claim_aspect_involved caio ON (ca.ClaimAspectID = caio.ClaimAspectID)
       LEFT JOIN dbo.utb_involved oi ON (caio.InvolvedID = oi.InvolvedID)
       LEFT JOIN dbo.utb_involved_role oir ON (caio.InvolvedID = oir.InvolvedID)
       LEFT JOIN dbo.utb_involved_role_type oirt ON (oir.InvolvedRoleTypeID = oirt.InvolvedRoleTypeID)
       WHERE ca.ClaimAspectID = @ClaimAspectID
         AND oirt.Name = 'Owner'
                        
            
    UNION ALL

     
    SELECT  3,
            1,
            NULL,
            -- Claim Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Estimate List
            cascd.DocumentID,
            IsNull(dt.Name, ''),
            IsNull(d.SupplementSeqNumber, ''),
            IsNull(d.CreatedDate, ''),
            IsNull(ds.Name, ''),
            IsNull(Convert(varchar(12), es.OriginalExtendedAmt), ''),
            IsNull(d.ImageLocation, ''),
            IsNull(d.ImageType, ''),
            CASE
              WHEN d.DocumentID IS NOT NULL THEN 1
              ELSE 0
            END,
            IsNull(d.DuplicateFlag, ''),
            CASE 
              WHEN d.EstimateTypeCD IS NULL THEN ''
              ELSE (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD') WHERE Code = d.EstimateTypeCD)
            END,
            IsNull(ds.VANFlag, 0)
      FROM  dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
      LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
      LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
      LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 27 = es.EstimateSummaryTypeID)
      Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
      WHERE casc.ClaimAspectID = @ClaimAspectID
        And casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        AND d.EnabledFlag = 1
        AND dt.EstimateTypeFlag = 1
            
         
    ORDER BY Tag, [Estimate!3!CreatedDate]         
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspUtilEstimateListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspUtilEstimateListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/