-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientClaimGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClientClaimGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClientClaimGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh
* FUNCTION:     Retrieves claim information for client purpose
*
* PARAMETERS:  
* (I) @LogonID               Logon ID
* (I) @ClaimNumber           The client claim number
*
* RESULT SET:
*   XML Universal recordset detailing claim billing information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClientClaimGetDetailXML
   @LogonID      udt_sys_login,
   @ClaimNumber  udt_cov_claim_number
AS
BEGIN
   DECLARE @ProcName                         AS varchar(30)       -- Used for raise error stmts 
   DECLARE @now                              AS udt_std_datetime
   DECLARE @ErrorMessage                     AS varchar(100)
   DECLARE @InsuranceCompanyID               AS int
   DECLARE @AssignmentDate                   as datetime
   DECLARE @ShopName                         as varchar(100)
   
   DECLARE @HistoryDate                      as datetime
   DECLARE @Description                      as varchar(250)
   DECLARE @LynxID                           as bigint
   DECLARE @ClaimAspectID                    as bigint
   DECLARE @CurrentEstimateAmount            as decimal(9, 2)
   DECLARE @CurrentEstimateDocID             as bigint
   DECLARE @ClaimNumber_work                 as varchar(50)
   
   SET NOCOUNT ON

   --set @LogonID = 'pam.wilson@uticanational.com'
   --set @ClaimNumber = '1324349'
   SET @ProcName = 'uspClientClaimGetDetailXML'    
   SET @now = CURRENT_TIMESTAMP
   
   SET @ErrorMessage = ''

   -- validate the logon
   SELECT @InsuranceCompanyID = o.InsuranceCompanyID
   FROM utb_user_application ua
   LEFT JOIN utb_user u on ua.UserID = u.UserID
   LEFT JOIN utb_office o on u.OfficeID = o.OfficeID
   WHERE LogonId = @LogonID
     AND ApplicationID = 2
     AND AccessBeginDate <= @now
     and (AccessEndDate IS NULL OR AccessEndDate > @now)

   IF LEN(RTRIM(LTRIM(@LogonID))) = 0 or
      @InsuranceCompanyID IS NULL
   BEGIN
      SET @ErrorMessage = 'Invalid LogonID.'
   END
   
   -- validate the claim number
   SET @ClaimNumber_work = @ClaimNumber
   
   SELECT @LynxID = LynxID
   FROM utb_claim
   WHERE InsuranceCompanyID = @InsuranceCompanyID
     AND ClientClaimNumber = @ClaimNumber
   
   IF @LynxID IS NULL
   BEGIN
      SET @ClaimNumber_work = '00%' + @ClaimNumber
      
      SELECT TOP 1 @LynxID = LynxID
      FROM utb_claim
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND ClientClaimNumber like @ClaimNumber_work
      ORDER BY LynxID desc
   END
   
   IF @LynxID IS NULL
   BEGIN
      SET @ErrorMessage = 'Invalid Claim Number.'
   END
   
   DECLARE @tmpHistory TABLE 
   (
     ClaimAspectServiceChannelID   bigint NOT NULL,
     ContextDate                   varchar(10) NOT NULL,
     Description                   varchar(250) NOT NULL,
     UpdatedDate                   datetime NOT NULL
   )

   DECLARE @tmpHistory2 TABLE 
   (
     ClaimAspectServiceChannelID   bigint NOT NULL,
     ContextDate                   varchar(10) NOT NULL,
     Description                   varchar(250) NOT NULL,
     UpdatedDate                   datetime NOT NULL
   )

   DECLARE @tmpDocument TABLE 
   (
     DocumentPath    varchar(300) NOT NULL,
     DocumentTypeID  int NOT NULL,
     ImageType       varchar(5),
     CreatedDate     datetime,
     EstimateTotal   decimal(9, 2)
   )
   

   IF @ErrorMessage = ''
   BEGIN
      INSERT INTO @tmpHistory
      SELECT hl.ClaimAspectServiceChannelID,
             case
               when EventID = 25 then convert(varchar, hl.CreatedDate, 101)
               when EventID = 10 then substring(Description, PATINDEX('%Scheduled for:%', Description) + 14, 10)
               when EventID = 42 then substring(Description, PATINDEX('%Start Date:%', Description) + 11, 10)
               when EventID = 43 then convert(varchar, hl.CreatedDate, 101)
               when EventID = 45 then convert(varchar, casc.WorkStartDate, 101)
               when EventID = 46 then convert(varchar, casc.WorkEndDate, 101)
             end,
             case
               when EventID = 25 then 'Repair Assignment Sent to Repair Shop'
               when EventID = 10 then 'Vehicle Inspection Scheduled'
               when EventID = 42 then 'Repairs Scheduled to Start'
               when EventID = 43 then 'Repairs Start Delayed - ' + substring(Description, PATINDEX('%Reason: %', Description) + 8, LEN(Description))
               when EventID = 45 then 'Repairs Start Confirmed'
               when EventID = 46 then 'Repair Completion Confirmed'
             end,
             hl.SysLastUpdatedDate
      FROM utb_history_log hl
      LEFT JOIN utb_claim_aspect_service_channel casc on hl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      WHERE LynxID = @LynxID
        AND LogType = 'E'
        AND EventID in (10, 25, 42, 43, 45, 46)
      ORDER BY hl.SysLastUpdatedDate DESC      

      INSERT INTO @tmpHistory
      SELECT hl.ClaimAspectServiceChannelID,
             case
               when EventID = 42 AND PATINDEX('%End Date:%', Description) > 0 then substring(Description, PATINDEX('%End Date:%', Description) + 9, 10)
               when EventID = 43 then substring(Description, PATINDEX('%changed to %', Description) + 11, 10)
               else ''
             end,
             'Estimated Repair Complete Date',
             dateadd(second, 1, hl.SysLastUpdatedDate)
      FROM utb_history_log hl
      LEFT JOIN utb_claim_aspect_service_channel casc on hl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      WHERE LynxID = @LynxID
        AND LogType = 'E'
        AND EventID in (42, 43)
      ORDER BY hl.SysLastUpdatedDate DESC      
      
      INSERT INTO @tmpHistory2
      SELECT *
      FROM @tmpHistory
      ORDER BY UpdatedDate DESC, ContextDate DESC
      
      -- Get the current estimate data
      SELECT top 1 @ClaimAspectID = ClaimAspectID
      FROM utb_claim_aspect
      WHERE LynxID = @LynxID
        AND ClaimAspectTypeID = 9
        
      SELECT @CurrentEstimateAmount = AgreedExtendedAmt,
             @CurrentEstimateDocID = DocumentID
      FROM dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', 'RepairTotal', 'TT')
        
      INSERT INTO @tmpDocument
      SELECT replace(ImageLocation, '\\', '\'),
             DocumentTypeID,
             ImageType,
             CreatedDate,
             @CurrentEstimateAmount
      FROM utb_document
      WHERE DocumentID = @CurrentEstimateDocID
      
      -- Add the top 6 approved photos (non-private)
      INSERT INTO @tmpDocument
      SELECT top 6 replace(d.ImageLocation, '\\', '\'),
             d.DocumentTypeID,
             d.ImageType,
             d.CreatedDate,
             NULL
      FROM utb_document d
      LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      WHERE casc.ClaimAspectID = @ClaimAspectID
        AND d.DocumentTypeID = 8
        AND d.ApprovedFlag = 1
      ORDER BY d.CreatedDate desc
   END
   
    SELECT 	1              AS Tag,
            NULL           AS Parent,
            @LogonID       AS [Root!1!LogonID],
            @ClaimNumber   AS [Root!1!ClaimNumber],
            @ClaimNumber_work   AS [Root!1!ClaimNumberMatch],
            
            -- History
            NULL AS [History!2!HistoryID],
            
            -- History Item
            NULL AS [Entry!3!description],
            NULL AS [Entry!3!date],
            NULL AS [Entry!3!updatedDate],
            
            -- Documents
            NULL AS [Documents!4!documentID],
            
            -- Estimate
            NULL AS [Estimate!5!documentPath],
            NULL AS [Estimate!5!documentType],
            NULL AS [Estimate!5!amount],
            NULL AS [Estimate!5!description],
            NULL AS [Estimate!5!updatedDate],
            
            -- Photos
            NULL AS [Photo!6!documentPath],
            NULL AS [Photo!6!documentType],
            NULL AS [Photo!6!updatedDate],
            
            -- Shop Information
            NULL AS [Shop!7!name],
            NULL AS [Shop!7!address],
            NULL AS [Shop!7!city],
            NULL AS [Shop!7!state],
            NULL AS [Shop!7!zip],
            NULL AS [Shop!7!phone],
            NULL AS [Shop!7!fax],
            NULL AS [Shop!7!website],
            
            -- Error
            NULL AS [Error!8!code],
            NULL AS [Error!8!description]
   
   UNION ALL
   
    SELECT 	2,
            1,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            NULL, NULL
            
   UNION ALL
   
    SELECT 	3,
            2,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            Description, 
            case 
               when isDate(ContextDate) = 1 then ContextDate
               else ''
            end, 
            CONVERT(varchar, UpdatedDate, 101),
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            NULL, NULL
            
   FROM @tmpHistory2

   UNION ALL
   
   SELECT 	4,
            1,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            NULL, NULL
            

   UNION ALL
   
   SELECT 	5,
            4,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            DocumentPath, 
            ImageType, 
            EstimateTotal, 
            'Estimated Repair Cost',
            CONVERT(varchar, CreatedDate, 101),
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            NULL, NULL
   FROM @tmpDocument
   WHERE DocumentTypeID in (3, 10)


   UNION ALL
   
   SELECT 	6,
            4,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            DocumentPath, 
            ImageType, 
            CONVERT(varchar, CreatedDate, 101),
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            NULL, NULL
   FROM @tmpDocument
   WHERE DocumentTypeID = 8

   UNION ALL
   
   SELECT 	7,
            1,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            isNull(sl.Name, ''), 
            isNull(sl.Address1, ''), 
            isNull(sl.AddressCity, ''), 
            isNull(sl.AddressState, ''), 
            isNull(sl.AddressZip, ''), 
            isNull(sl.PhoneAreaCode + sl.PhoneExchangeNumber + sl.PhoneUnitNumber, ''), 
            isNull(sl.FaxAreaCode + sl.FaxExchangeNumber + sl.FaxUnitNumber, ''), 
            isNull(sl.WebSiteAddress, ''), 

            -- Error
            NULL, NULL
   FROM utb_assignment a
   LEFT JOIN dbo.utb_claim_aspect_service_channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
   WHERE ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
     AND a.CancellationDate IS NULL


   UNION ALL
   
   SELECT 	8,
            1,
            NULL, NULL, NULL,
            
            -- History
            NULL,
            
            -- History Item
            NULL, NULL, NULL,
            
            -- Documents
            NULL,
            
            -- Estimate
            NULL, NULL, NULL, NULL, NULL,
            
            -- Photos
            NULL, NULL, NULL,
            
            -- Shop Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- Error
            CASE
               WHEN @ErrorMessage <> '' THEN -1
               ELSE 0
            END, 
            @ErrorMessage
            
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientClaimGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClientClaimGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 
