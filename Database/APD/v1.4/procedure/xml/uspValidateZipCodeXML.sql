/****** Object:  StoredProcedure [dbo].[uspValidateZipCodeXML]    Script Date: 02/14/2011 14:12:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT name FROM sysobjects 
         WHERE name = 'uspValidateZipCodeXML' AND type = 'P')
   DROP PROCEDURE uspValidateZipCodeXML
GO

/************************************************************************************************************************
*
* PROCEDURE:    [uspValidateZipCodeXML]
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL details
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID                        ClaimAspectServiceChannelID
*
* RESULT SET:
* NONE

************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspValidateZipCodeXML]
    @ZipCode        varchar(5),
    @City			varchar(28),
    @State			varchar(2)
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    --DECLARE @LienHolderID as bigint
    

    SET @ProcName = 'uspValidateZipCodeXML'
    SET @now = CURRENT_TIMESTAMP


    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        @ZipCode    AS [Root!1!ZipCode],
        @City       AS [Root!1!City],
        @State		AS [Root!1!State],

        --Melissa Zip
        NULL AS [Melissa!2!MelZip],
        NULL AS [Melissa!2!MelState],
        NULL AS [Melissa!2!MelCity]
        
    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL, 
            NULL, 
            NULL, 
            
            --Melissa Zip Code Data
            melissa_zip.zip,
            melissa_zip.state,
            melissa_zip.city
            
    FROM dbo.utb_melissa_zip melissa_zip
    WHERE melissa_zip.zip = @ZipCode

    ORDER BY TAG

END


GO

--set execute permissions for APD stored procedure
EXECUTE ('GRANT EXECUTE ON dbo.uspValidateZipCodeXML TO ugr_lynxapd')



