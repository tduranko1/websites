-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCarrierRepGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCarrierRepGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCarrierRepGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves a list of carrier reps for the insurance company specified
*
* PARAMETERS:   
* (I)   InsuranceCompanyID      The insurance company to get the reps for
*
* RESULT SET:
*   An XML document detailing the reps
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimCarrierRepGetListWSXML
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimCarrierRepGetListWSXML'


    -- Check to make sure a valid Insurance Company id was passed in
    
    IF  (@InsuranceCompanyID IS NULL) OR
        (NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    
    
    -- Begin XML Select
    
    SELECT  1 AS tag,
            NULL AS parent,
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            -- Office Data Level
            NULL AS [Office!2!OfficeID],
            NULL AS [Office!2!ClientOfficeId],
            NULL AS [Office!2!OfficeName],
            -- Office Users
            NULL AS [OfficeUser!3!UserID],
            NULL AS [OfficeUser!3!NameFirst],
            NULL AS [OfficeUser!3!NameLast],
            -----------------------------------RC12
            NULL AS [OfficeUser!3!EmailAddress],
            NULL AS [OfficeUser!3!PhoneAreaCode],
            NULL AS [OfficeUser!3!PhoneExchangeNumber],
            NULL AS [OfficeUser!3!PhoneExtensionNumber],
            NULL AS [OfficeUser!3!PhoneUnitNumber],
            NULL AS [OfficeUser!3!FaxAreaCode],
            NULL AS [OfficeUser!3!FaxExchangeNumber],
            NULL AS [OfficeUser!3!FaxExtensionNumber],
            NULL AS [OfficeUser!3!FaxUnitNumber],
            NULL AS [OfficeUser!3!Enabled]
            

    UNION ALL


    -- Select Office Data Level

    SELECT  2,
            1,
            NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            IsNull(o.ClientOfficeId, ''),
            IsNull(o.Name, ''),
            -- Office Users
            NULL, NULL, NULL, NULL,
            
            ----------RC12
            NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL,NULL

      FROM  dbo.utb_office o
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID

    
    UNION ALL


    -- Select Office Users Level

    SELECT  3,
            2,
            NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            NULL, NULL,
            -- Office Users
            IsNull(u.UserID, 0),
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            ---------------------------RC12
            IsNull(u.EmailAddress, ''),
            RTRIM(LTRIM(IsNull(u.PhoneAreaCode, ''))),
            RTRIM(LTRIM(IsNull(u.PhoneExchangeNumber, ''))),
            RTRIM(LTRIM(IsNull(u.PhoneExtensionNumber, ''))),
            RTRIM(LTRIM(IsNull(u.PhoneUnitNumber, ''))),
            RTRIM(LTRIM(IsNull(u.FaxAreaCode, ''))),
            RTRIM(LTRIM(IsNull(u.FaxExchangeNumber, ''))),
            RTRIM(LTRIM(IsNull(u.FaxExtensionNumber, ''))),
            RTRIM(LTRIM(IsNull(u.FaxUnitNumber, ''))),
            IsNull(u.EnabledFlag, 0)

      FROM  dbo.utb_user u
      LEFT JOIN dbo.utb_office o ON (u.OfficeID = o.OfficeID)
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID

    
    ORDER BY [Office!2!OfficeID], Tag 
    FOR XML EXPLICIT      -- (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCarrierRepGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCarrierRepGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspClaimCarrierRepGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/