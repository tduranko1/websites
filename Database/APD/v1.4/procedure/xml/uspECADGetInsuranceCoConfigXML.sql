-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspECADGetInsuranceCoConfigXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspECADGetInsuranceCoConfigXML
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspECADGetInsuranceCoConfigXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves client specific reference information
*
* PARAMETERS:
* (I) @InsuranceCompanyID   The Insurance Company to retrieve data for
*
* RESULT SET:
*   XML detailing reference data specific to the insurance company
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspECADGetInsuranceCoConfigXML
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @LaborRateSetupFlag AS bit

    SET @ProcName = 'uspECADGetInsuranceCoConfigXML'


     -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        ServiceChannelName  varchar(50) NULL
    )

    DECLARE @tmpOfficeUsers TABLE    (
        UserID            int NOT NULL
    )

    -- Select reference information for all pertinent reference tables and store in the
    -- temporary table

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, ServiceChannelName)

    SELECT  'AssignmentType' AS ListName,
            at.DisplayOrder,
            convert(varchar, at.AssignmentTypeID),
            at.Name,
            --Project:210474 APD Modifed the column and added the case stmnt when we did the code merge M.A.20061114
            CASE
                WHEN sc.Name = 'Damage Reinspection' THEN 'Desk Review'
                ELSE sc.Name
            END
      FROM  dbo.utb_assignment_type at
      LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_Service_Channel', 'ServiceChannelCD') sc ON (at.ServiceChannelDefaultCD = sc.Code)
      WHERE EnabledFlag = 1
        AND DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'BestContactPhoneCD',
            NULL,
            Code,
            Name,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_involved', 'BestContactPhoneCD')

    UNION ALL

    SELECT  'CallerRelationToInsured',
            DisplayOrder,
            convert(varchar, RelationID),
            Name,
            NULL
    FROM    dbo.utb_Relation
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'ContractState',
            DisplayOrder,
            StateCode,
            StateValue,
            NULL
      FROM  dbo.utb_state_code
      WHERE EnabledFlag = 1
        AND DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'ImpactPoint',
            DisplayOrder,
            convert(varchar, ImpactID),
            Name,
            NULL
    FROM    dbo.utb_impact
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'ServiceChannel',
            0,
            convert(varchar, Code),
            Name,
            NULL
    FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')rc
    left join utb_client_service_channel csc on rc.Code = csc.ServiceChannelCD
    where csc.InsuranceCompanyID = @InsuranceCompanyID

	UNION ALL
	
    select distinct 'ClientAssignmentType',
        0, 
        convert(varchar, at.AssignmentTypeID), 
        at.Name,
        NULL
    FROM  dbo.utb_office_assignment_type oat
    LEFT JOIN dbo.utb_office o ON (oat.OfficeID = o.OfficeID)
    left join utb_assignment_type at ON oat.AssignmentTypeID = at.AssignmentTypeID
    WHERE o.InsuranceCompanyID = @InsuranceCompanyID

    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    -- Now get the office users
    -- Get all active users
    INSERT INTO @tmpOfficeUsers
    SELECT distinct u.userID
    FROM utb_user u
    LEFT JOIN utb_office o ON u.OfficeID = o.OfficeID
    LEFT JOIN utb_user_application ua ON u.UserID = ua.UserID
    WHERE o.InsuranceCompanyID = @InsuranceCompanyID
      AND u.EnabledFlag = 1
      AND ((ua.ApplicationID not in (1, 4)) AND (ua.AccessEndDate) is NULL OR (ua.AccessEndDate > current_timestamp))

    -- now add the users with open claim
    INSERT INTO @tmpOfficeUsers
    SELECT tc.CarrierRepUserID
    FROM dbo.utb_claim tc
    LEFT JOIN dbo.utb_claim_aspect tca on tc.LynxID = tca.LynxID
    LEFT JOIN dbo.utb_claim_aspect_status tcas on tca.ClaimAspectID = tcas.ClaimAspectID
    LEFT JOIN dbo.utb_status ts on tcas.StatusID = ts.StatusID
    WHERE tc.InsuranceCompanyID = @InsuranceCompanyID
      AND tca.ClaimAspectTypeID = 0 -- claim aspect type
      --AND tcas.StatusID < 900 -- use only open claims
      AND ts.Name = 'Open'
      AND tc.CarrierRepUserID NOT IN (SELECT UserID FROM @tmpOfficeUsers)
    GROUP BY tc.CarrierRepUserID HAVING COUNT(*) > 0
    
    -- Labor Rates for this client exists?
    SET @LaborRateSetupFlag = 0
    IF EXISTS(SELECT LaborRateID
               FROM dbo.utb_labor_rate
               WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
      SET @LaborRateSetupFlag = 1
    END

    -- Begin XML Select

    SELECT  1 AS tag,
            NULL AS parent,
            IsNull(i.InsuranceCompanyID, 0) AS [Root!1!InsuranceCompanyID],
            IsNull(i.Name, '') AS [Root!1!InsuranceCompanyName],
            IsNull(i.AssignmentAtSelectionFlag, 0) AS [Root!1!AssignmentAtSelectionFlag],
            IsNull(i.DemoFlag, 0) AS [Root!1!DemoFlag],
            IsNull(i.ClaimPointDocumentUploadFlag, 0) AS [Root!1!ClaimPointDocumentUploadFlag],
            IsNull(i.ClaimPointCarrierRepSelFlag, 0) AS [Root!1!AllowCarrierRepSelectionFlag],
            (SELECT  IsNull(Max(Convert(int, ClientAuthorizesPaymentFlag)), 0)
               --FROM  dbo.utb_client_assignment_type
                from utb_Client_Service_Channel
               WHERE InsuranceCompanyID = @InsuranceCompanyID) AS [Root!1!CarrierAuthorizesPaymentsFlag],
            IsNull(i.CarrierLynxContactPhone, '239-337-4300') AS [Root!1!LynxContactPhone],
            @LaborRateSetupFlag AS [Root!1!LaborRatesSetupFlag],
            -- Client Assignment Types
            NULL AS [AssignmentType!2!AssignmentTypeID],
            -- Client Coverage Types
            NULL AS [CoverageType!3!ClientCoverageTypeID],
            NULL AS [CoverageType!3!Name],
            NULL AS [CoverageType!3!CoverageProfileCD],
            NULL AS [CoverageType!3!DisplayOrder],
            NULL AS [CoverageType!3!AdditionalCoverageFlag],
            NULL AS [CoverageType!3!ClientCode],
            -- Client Contract States
            NULL AS [ContractStates!4!StateCode],
            -- Client Scripting
            NULL AS [CustomScripting!5!AssignmentTypeIDList],
            NULL AS [CustomScripting!5!ConditionOperator],
            NULL AS [CustomScripting!5!ConditionValue],
            NULL AS [CustomScripting!5!Event],
            NULL AS [CustomScripting!5!EventControl],
            NULL AS [CustomScripting!5!InputFormat],
            NULL AS [CustomScripting!5!InputRequestedFlag],
            NULL AS [CustomScripting!5!InputRequiredFlag],
            NULL AS [CustomScripting!5!InputResponseName],
            NULL AS [CustomScripting!5!InputTag],
            NULL AS [CustomScripting!5!InputTypeCD],
            NULL AS [CustomScripting!5!ScriptGroupName],
            NULL AS [CustomScripting!5!ScriptText],
            -- Office Data Level
            NULL AS [Office!6!OfficeID],
            NULL AS [Office!6!ClaimNumberFormatJS],
            NULL AS [Office!6!ClaimNumberValidJS],
            NULL AS [Office!6!ClaimNumberMsgText],
            NULL AS [Office!6!ClientOfficeId],
            NULL AS [Office!6!OfficeName],
            NULL AS [Office!6!EnabledFlag],  --Project:210474 APD Added the column when we did the code merge M.A.20061114
            -- Office Users
            NULL AS [OfficeUser!7!UserID],
            NULL AS [OfficeUser!7!NameFirst],
            NULL AS [OfficeUser!7!NameLast],
            NULL AS [OfficeUser!7!EnabledFlag],
            NULL AS [OfficeUser!7!ClaimsFlag],  --Project:210474 APD Added the column when we did the code merge M.A.20061114
            NULL AS [OfficeUser!7!EmailAddress],
            -- Office Assignment Types
            NULL AS [OfficeAssignmentType!8!AssignmentTypeID],
            -- Office Contract States
            NULL AS [OfficeContractStates!9!StateCode],
            -- Reference Data
            NULL AS [Reference!10!List],
            NULL AS [Reference!10!ReferenceID],
            NULL AS [Reference!10!Name],
            NULL AS [Reference!10!ServiceChannelName]

      FROM  (SELECT @InsuranceCompanyID AS InsuranceCompanyID) AS parms
      LEFT JOIN dbo.utb_insurance i ON (parms.InsuranceCompanyID = i.InsuranceCompanyID)


    UNION ALL


    -- Select Client Assignment Types Level

    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            IsNull(AssignmentTypeID, 0),
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_client_assignment_type
      WHERE InsuranceCompanyID = @InsuranceCompanyID


   UNION ALL

    -- Select Client Coverage Types
    SELECT  3,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            IsNull(ClientCoverageTypeID,0),
            IsNull(Name,''),
            IsNull(CoverageProfileCD,''),
            IsNull(DisplayOrder,''),
            IsNull(AdditionalCoverageFlag,0),
            IsNull(ClientCode,''),
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_client_coverage_type
      WHERE InsuranceCompanyID = @InsuranceCompanyID AND EnabledFlag = 1


      UNION ALL

    -- Select Client Contract States Level

    SELECT  4,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            IsNull(StateCode, ''),
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_client_contract_state
      WHERE InsuranceCompanyID = @InsuranceCompanyID


      UNION ALL

    -- Select Client Scripting Level

    SELECT  5,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            IsNull(AssignmentTypeIDList, ''),
            IsNull(ConditionOperator, ''),
            IsNull(ConditionValue, ''),
            IsNull(Event, ''),
            IsNull(EventControl, ''),
            IsNull(InputFormat, ''),
            IsNull(InputRequestedFlag, ''),
            IsNull(InputRequiredFlag, ''),
            IsNull(InputResponseName, ''),
            IsNull(InputTag, ''),
            IsNull(InputTypeCD, ''),
            IsNull(ScriptGroupName, ''),
            IsNull(ScriptText, ''),
            -- Office Data Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_client_scripting
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND EnabledFlag = 1


    UNION ALL


    -- Select Office Data Level

    SELECT  6,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            IsNull(o.ClaimNumberFormatJS, ''),
            IsNull(o.ClaimNumberValidJS, ''),
            IsNull(o.ClaimNumberMsgText, ''),
            IsNull(o.ClientOfficeId, ''),
            IsNull(o.Name, ''),
            o.EnabledFlag,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_office o
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID


    UNION ALL


    -- Select Office Users Level

    SELECT  7,
            6,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            IsNull(u.UserID, 0),
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            u.EnabledFlag,
            IsNull(c.ClaimsFlag, 0),
            IsNull(u.EmailAddress, ''),
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      /*FROM  dbo.utb_user u
      LEFT JOIN dbo.utb_office o ON (u.OfficeID = o.OfficeID)
      LEFT JOIN (SELECT CarrierRepUserID, 1 AS ClaimsFlag, COUNT(*) AS Count FROM dbo.utb_claim GROUP BY CarrierRepUserID HAVING COUNT(*) > 0 ) c
        ON (u.userid = c.CarrierRepUserID)*/
      FROM @tmpOfficeUsers tu
      LEFT JOIN utb_user u on tu.UserID = u.UserID
      LEFT JOIN utb_office o on u.OfficeID = o.OfficeID
      LEFT JOIN (SELECT CarrierRepUserID, 1 AS ClaimsFlag, COUNT(*) AS Count FROM dbo.utb_claim GROUP BY CarrierRepUserID HAVING COUNT(*) > 0 ) c
        ON (tu.userid = c.CarrierRepUserID)
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID


    UNION ALL


    -- Select Office Assignment Types Level

    SELECT  8,
            6,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            IsNull(oat.AssignmentTypeID, 0),
            -- Office Contract States
            NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_office_assignment_type oat
      LEFT JOIN dbo.utb_office o ON (oat.OfficeID = o.OfficeID)
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID


    UNION ALL


    -- Select Office Contract States Level

    SELECT  9,
            6,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            IsNull(o.OfficeID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            IsNull(ocs.StateCode, ''),
            -- Reference Data
            NULL, NULL, NULL, NULL

      FROM  dbo.utb_office_contract_state ocs
      LEFT JOIN dbo.utb_office o ON (ocs.OfficeID = o.OfficeID)
      WHERE o.InsuranceCompanyID = @InsuranceCompanyID


    UNION ALL


    -- Select Reference Data Level

    SELECT  10,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Assignment Types
            NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Client Contract States
            NULL,
            -- Client Scripting
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Office Data Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Users
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Office Assignment Types
            NULL,
            -- Office Contract States
            NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            ServiceChannelName

    FROM    @tmpReference


   ORDER BY [Office!6!OfficeID] , tag
-- FOR XML EXPLICIT      -- (Commented for client-side processing)


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspECADGetInsuranceCoConfigXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspECADGetInsuranceCoConfigXML TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/