-- Begin the transaction for dropping and creating the stored procedure 

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFAOIPHNotificationGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFAOIPHNotificationGetXML 
END
GO
/****** Object:  StoredProcedure [dbo].[uspCFAOIPHNotificationGetXML]    Script Date: 04/22/2014 12:43:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCFAOIPHNotificationGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspCFAOIPHNotificationGetXML]
    @ClaimAspectID      udt_std_id_big,
    @UserID				   udt_std_id_big
AS
BEGIN
    -- Declare internal variables
	 DECLARE @InsuredLName as varchar(50)
	 DECLARE @InsuredFName as varchar(50)
	 DECLARE @InsuredBName as varchar(100)
	 DECLARE @InsuredAddress1 as varchar(100)
	 DECLARE @InsuredAddress2 as varchar(100)
	 DECLARE @InsuredAddressCity as varchar(100)
	 DECLARE @InsuredAddressState as varchar(100)
	 DECLARE @InsuredAddressZip as varchar(100)
	 DECLARE @AgreedAuditDocumentID as bigint
	 DECLARE @AgreedAuditImageLocation as varchar(250)

    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFAOIPHNotificationGetXML'
    
    SELECT	@InsuredFName = LTrim(RTrim(IsNull(i.NameFirst, ''))),
			   @InsuredLName = LTrim(RTrim(IsNull(i.NameLast, ''))),
			   @InsuredBName = LTrim(RTrim(IsNull(i.BusinessName, ''))),
			   @InsuredAddress1 = LTrim(RTrim(IsNull(i.Address1, ''))),
			   @InsuredAddress2 = LTrim(RTrim(IsNull(i.Address2, ''))),
			   @InsuredAddressCity = LTrim(RTrim(IsNull(i.AddressCity, ''))),
			   @InsuredAddressState = LTrim(RTrim(IsNull(i.AddressState, ''))),
			   @InsuredAddressZip = LTrim(RTrim(IsNull(i.AddressZip, '')))
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Insured'
      
    SELECT @AgreedAuditDocumentID = d.DocumentID,
           @AgreedAuditImageLocation = d.ImageLocation
    FROM utb_document d
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    WHERE casc.ClaimAspectID = @ClaimAspectID
      AND d.DocumentTypeID in (3, 10)
      AND d.EstimateTypeCD = 'A'
      AND d.AgreedPriceMetCD = 'Y'
      AND d.EnabledFlag = 1
      
	IF @InsuredLName is NOT NULL AND @InsuredLName != ''
		 SET @InsuredLName = Left(UPPER(@InsuredLName),1)+Right(LOWER(@InsuredLName),len(@InsuredLName)-1)
		 
	IF @InsuredFName is NOT NULL AND @InsuredFName != ''
		 SET @InsuredFName = Left(UPPER(@InsuredFName),1)+Right(LOWER(@InsuredFName),len(@InsuredFName)-1)
		 
	IF @InsuredBName is NOT NULL AND @InsuredBName != ''
		SET @InsuredBName = Left(UPPER(@InsuredBName),1)+Right(LOWER(@InsuredBName),len(@InsuredBName)-1)
		
	IF @InsuredAddressCity is NOT NULL AND @InsuredAddressCity != ''
		SET @InsuredAddressCity = Left(UPPER(@InsuredAddressCity),1)+Right(LOWER(@InsuredAddressCity),len(@InsuredAddressCity)-1)
      
	DECLARE @StrInsuredAddress1 varchar(100)
	DECLARE @ResultInsuredAddress1 varchar(100)
	SET @StrInsuredAddress1 =@InsuredAddress1
	SET @StrInsuredAddress1 = LOWER(@StrInsuredAddress1) + ' '
	SET @ResultInsuredAddress1 = ''
  
		WHILE 1=1
		BEGIN
			IF PATINDEX('% %',@StrInsuredAddress1) = 0 BREAK
				SET @ResultInsuredAddress1 = @ResultInsuredAddress1 + UPPER(Left(@StrInsuredAddress1,1))+
				SubString  (@StrInsuredAddress1,2,CharIndex(' ',@StrInsuredAddress1)-1)
				SET @StrInsuredAddress1 = SubString(@StrInsuredAddress1,CharIndex(' ',@StrInsuredAddress1)+1,Len(@StrInsuredAddress1))
		END
		SET @InsuredAddress1 = Left(@ResultInsuredAddress1,Len(@ResultInsuredAddress1))
		
	DECLARE @StrInsuredAddress2 varchar(100)
	DECLARE @ResultInsuredAddress2 varchar(100)
	SET @StrInsuredAddress2 =@InsuredAddress2
	SET @StrInsuredAddress2 = LOWER(@StrInsuredAddress2) + ' '
	SET @ResultInsuredAddress2 = ''
	  
		WHILE 1=1
		BEGIN
			IF PATINDEX('% %',@StrInsuredAddress2) = 0 BREAK
				SET @ResultInsuredAddress2 = @ResultInsuredAddress2 + UPPER(Left(@StrInsuredAddress2,1))+
				SubString  (@StrInsuredAddress2,2,CharIndex(' ',@StrInsuredAddress2)-1)
				SET @StrInsuredAddress2 = SubString(@StrInsuredAddress2,CharIndex(' ',@StrInsuredAddress2)+1,Len(@StrInsuredAddress2))
		END
		SET @InsuredAddress2 = Left(@ResultInsuredAddress2,Len(@ResultInsuredAddress2))
       
      
    SELECT  1 as Tag,
            NULL as Parent,
            convert(varchar, current_timestamp, 101) as [Root!1!ReportDate],
			   isNull(@InsuredLName, '') as [Root!1!PHLName],
			   isNull(@InsuredFName, '') as [Root!1!PHFName],
			   isNull(@InsuredBName, '') as [Root!1!PHBName],
			   isNull(@InsuredAddress1, '') as [Root!1!PHAddress1],
			   isNull(@InsuredAddress2, '') as [Root!1!PHAddress2],
			   isNull(@InsuredAddressCity, '') as [Root!1!PHAddressCity],
			   isNull(UPPER(@InsuredAddressState), '') as [Root!1!PHAddressState],
			   isNull(UPPER(@InsuredAddressZip), '') as [Root!1!PHAddressZip],
			   isNull(UPPER(c.ClientClaimNumber), '') as [Root!1!ClaimNumber],
			   isNull(UPPER(c.PolicyNumber), '') as [Root!1!PolicyNumber],
			   isNull(UPPER(cv.VehicleYear), '') as [Root!1!VehicleYear],
			   Left(UPPER(isNull(UPPER(cv.Make), '')),1)+Right(LOWER(isNull(UPPER(cv.Make), '')),len(isNull(UPPER(cv.Make), ''))-1) as [Root!1!VehicleMake],
			   Left(UPPER(isNull(UPPER(cv.Model), '')),1)+Right(LOWER(isNull(UPPER(cv.Model), '')),len(isNull(UPPER(cv.Model), ''))-1) as [Root!1!VehicleModel],
			   isNull(ExposureCD, '') as [Root!1!PartyCD],
			   isNull(convert(varchar, @AgreedAuditDocumentID), '') as [Root!1!AgreedAuditDocumentID],
			   isNull(@AgreedAuditImageLocation, '') as [Root!1!AgreedAuditImageLocation]
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim c on ca.Lynxid = c.Lynxid
    LEFT JOIN utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID
    WHERE ca.ClaimAspectID = @ClaimAspectID

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFAOIPHNotificationGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFAOIPHNotificationGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO