-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDRAAUserGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAPDRAAUserGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspAPDRAAUserGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Return a list of APD users who have atleast one Receive/Analyze/Administrate profile set
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspAPDRAAUserGetListWSXML]
    @UserID         as udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(3)
    DECLARE @SupervisorFlag    AS bit
    DECLARE @RoleClaimsUnitManager AS int
    DECLARE @RoleClaimsSupervisor AS int
    DECLARE @DefaultUserID        AS int

    DECLARE @StatusIDVehicleOpen AS int
    DECLARE @ClaimAspectTypeIDVehicle AS int

    SET @ProcName = 'uspAPDRAAUserGetListWSXML'
    
    SET @ApplicationCD = 'APD'
    
    DECLARE @tmpUserList TABLE
    (
        UserID      int,
        Inactive    bit
    )
    
    SELECT @SupervisorFlag = SupervisorFlag
    FROM dbo.utb_user
    WHERE UserID = @UserID
    
    -- Get the Role IDs for ClaimsSupervisor and Claims Unit Manager
    SELECT @RoleClaimsUnitManager = RoleID
    FROM dbo.utb_role
    WHERE Name = 'Claims Unit Manager'
    

    SELECT @RoleClaimsSupervisor = RoleID
    FROM dbo.utb_role
    WHERE Name = 'Claims Supervisor'

    SELECT @StatusIDVehicleOpen = s.StatusID,
           @ClaimAspectTypeIDVehicle = cat.ClaimAspectTypeID
    FROM dbo.utb_status s
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (s.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    WHERE cat.Name = 'Vehicle'
      AND s.Name = 'Open'

    -- Get all active users for the application
    INSERT INTO @tmpUserList
    SELECT  UserID, 0
    FROM  dbo.utb_user u
    WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)
      AND u.UserID > 0
      AND u.EnabledFlag = 1


    -- Add owner users with open claims and not in the above list
    INSERT INTO @tmpUserList
    SELECT DISTINCT OwnerUserID, 1
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID --Project: 210474 APD - Enhancements to support multiple concurrent service channels  M.A.20061120
    WHERE cas.StatusID = @StatusIDVehicleOpen
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      AND c.DemoFlag = 0
      AND ca.OwnerUserID not in (SELECT UserID FROM @tmpUserList)

    -- Add support users with open claims and not in the above list
    INSERT INTO @tmpUserList
    SELECT DISTINCT SupportUserID, 1
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID --Project: 210474 APD - Enhancements to support multiple concurrent service channels  M.A.20061120
    WHERE cas.StatusID = @StatusIDVehicleOpen
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      AND c.DemoFlag = 0
      AND ca.SupportUserID not in (SELECT UserID FROM @tmpUserList)

    -- Add analyst users with open claims and not in the above list
    INSERT INTO @tmpUserList
    SELECT DISTINCT AnalystUserID, 1
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID --Project: 210474 APD - Enhancements to support multiple concurrent service channels  M.A.20061120
    WHERE cas.StatusID = @StatusIDVehicleOpen
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      AND c.DemoFlag = 0
      AND ca.AnalystUserID not in (SELECT UserID FROM @tmpUserList)

    -- Add users with tasks assigned to them and not in the above list
    INSERT INTO @tmpUserList
    SELECT DISTINCT AssignedUserID, 1
    FROM dbo.utb_checklist
    WHERE StatusID < 900
      AND AssignedUserID not in (SELECT UserID FROM @tmpUserList)
          
      
    -- Add the "default user" id
    INSERT INTO @tmpUserList
    SELECT Value as UserID, 1 as 'Inactive'
    FROM dbo.utb_app_variable
    WHERE Name = 'Default_Assignment_User'
    
    -- Begin XML Select
    
    SELECT  1 AS Tag,
            NULL AS Parent,
            @ApplicationCD AS [Root!1!ApplicationCD],
            @SupervisorFlag AS [Root!1!SupervisorFlag],
            -- Active Users
            NULL AS [User!2!UserID],
            NULL AS [User!2!NameFirst],
            NULL AS [User!2!NameLast],
            NULL AS [User!2!SupervisorFlag]

    UNION ALL
    
    SELECT  DISTINCT 2,
            1,
            NULL,
            NULL,
            -- Active Users
            u.UserID, 
            u.NameFirst, 
            u.NameLast, 
            u.SupervisorFlag
    FROM  @tmpUserList tmp
    LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
    LEFT JOIN dbo.utb_user_role ur ON (u.UserID = ur.UserID)
    WHERE ((ur.RoleID in (@RoleClaimsUnitManager, @RoleClaimsSupervisor)) OR
           (tmp.Inactive = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Receive Shop Program Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Receive Damage Reinspections', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Receive Desk Audits', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Receive Mobile Electronics Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Receive Total Loss Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Analyze Shop Program Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Analyze Damage Reinspections', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Analyze Desk Audits', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Analyze Mobile Electronics Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Administrate Shop Program Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Administrate Damage Reinspections', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Administrate Desk Audits', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Administrate Mobile Electronics Claims', DEFAULT, DEFAULT) = 1) OR
           (dbo.ufnUtilityGetProfileValue(u.UserID,'Administrate Total Loss Claims', DEFAULT, DEFAULT) = 1)
          )
    ORDER BY [User!2!NameLast], tag
	 FOR XML EXPLICIT
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDRAAUserGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAPDRAAUserGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAPDRAAUserGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/