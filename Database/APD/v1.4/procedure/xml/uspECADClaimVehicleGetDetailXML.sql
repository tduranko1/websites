-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADClaimVehicleGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspECADClaimVehicleGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspECADClaimVehicleGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Retrieves vehicle assignment information for all the Claim Aspect Service Channels for a given Claim Aspect
*
* PARAMETERS:  
* (I) @ClaimAspectId        The Claim Aspect ID
* (I) @InsuranceCompanyID   The insurance company the claim aspect belongs to
*
* RESULT SET:
* An XML Data stream containing vehicle assignment information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspECADClaimVehicleGetDetailXML]
    @ClaimAspectID      udt_std_id_big,
    @InsuranceCompanyID udt_std_id
AS
BEGIN
    -- Declare local variables
    DECLARE @EstimateSummaryTypeIDNetTotal  udt_std_id
    DECLARE @EstimateSummaryTypeIDRepairTotal udt_std_id
    DECLARE @ExposureCD                     udt_std_cd
    DECLARE @InsuranceCompanyIDClaim        udt_std_id
    DECLARE @ClaimAspectNumber              udt_std_id
    DECLARE @ClaimAspectTypeID              udt_std_id
    DECLARE @ClaimAspectIDCheck             udt_std_id_big
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @PertainsTo                     varchar(8)
    DECLARE @CountNotes                     udt_std_int
    DECLARE @CountTasks                     udt_std_int
    DECLARE @CountBilling                   udt_std_int
    DECLARE @ActiveReinspection             bit
    DECLARE @VehicleStatus                  varchar(10)
    DECLARE @VehicleOpenStatusID            udt_std_id
    DECLARE @DeskAuditAppraiserType         udt_std_cd
    DECLARE @DeskAuditID                    udt_std_id_big
    DECLARE @ClientClaimNumber              udt_cov_Claim_Number

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspECADClaimVehicleGetDetailXML'


    SET NOCOUNT ON
    
    -- Get Claim Aspect Type ID
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
    FROM    utb_claim_aspect_type
    WHERE   Name = 'Vehicle'

    IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

    IF @ClaimAspectTypeID IS NULL
        BEGIN
           -- Claim Aspect Not Found
        
            RAISERROR('100|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
            RETURN
        END


    -- Check to make sure a valid Claim Aspect ID was passed in, pull the information we'll need on the claim aspect

    IF not(@ClaimAspectID = 0)
        BEGIN
            SELECT  @ClaimAspectNumber = ClaimAspectNumber,
                    @LynxID = LynxID
            FROM    utb_claim_aspect 
            WHERE   ClaimAspectID = @ClaimAspectID 
            AND     ClaimAspectTypeID = @ClaimAspectTypeID
        
        
            IF (@LynxID IS NULL)
            BEGIN
                -- Invalid Claim Aspect ID
        
                RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
                RETURN
            END
        END


    -- Get information on the LYNX Desk audit unit so we can distinguish assignments to it versus regular assignments

    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('102|%s', 16, 1, @ProcName)
            RETURN
        END



    -- Get the Insurance Company Id for the claim

    SELECT      @InsuranceCompanyIDClaim = InsuranceCompanyID 
    FROM        utb_claim_aspect ca
    INNER JOIN  utb_claim c 
    ON          ca.LynxID = c.LynxID
    WHERE       ca.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('103|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('104|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


    -- Get the Insurance Company Id for the claim

    SELECT  @InsuranceCompanyIDClaim = InsuranceCompanyID 
      FROM  dbo.utb_claim_aspect ca
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      WHERE ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('105|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END

    -- Get the Client Claim Number from utb_Claim

    select @ClientClaimNumber = ClientClaimNumber
    from    utb_Claim_Aspect ca
    inner join    utb_Claim c
    on            c.LynxID = ca.LynxID
    
    where         ca.ClaimAspectID = @ClaimAspectID
    and           c.InsuranceCompanyID = @InsuranceCompanyID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('115|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Vehicle',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_claim_vehicle' AND Column_Name IN 
            ('BodyStyle',
             'BookValueAmt',
             'Color',
             'DriveableFlag',
             'ImpactSpeed',
             'InspectionDate',
             'LicensePlateNumber',
             'LicensePlateState',
             'LocationAreaCode',
             'LocationAddress1',
             'LocationAddress2',
             'LocationCity',
             'LocationExchangeNumber',
             'LocationExtensionNumber',
             'LocationName',
             'LocationState',
             'LocationUnitNumber',
             'LocationZip',
             'Make',
             'Mileage',
             'Model',
             'NADAId',
             'PermissionToDriveCD',
             'PostedSpeed',
             'Remarks',
             'RentalDaysAuthorized',
             'RentalInstructions',
             'RepairEndDate',
             'RepairStartDate',
             'ShopRemarks',
             'VehicleYear',
             'EstimateVIN',
             'VIN')) 
      OR    (Table_Name = 'utb_claim_aspect' AND Column_Name IN
            ('CoverageProfileCD',
             'CurrentAssignmentTypeID')) 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'SafetyDevice',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_vehicle_safety_device' AND Column_Name IN 
            ('SafetyDeviceID'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Impact',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_vehicle_impact' AND Column_Name IN 
            ('ImpactID',
             'CurrentImpactFlag',
             'PrimaryImpactFlag',
             'PriorImpactFlag'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Contact',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_involved' AND Column_Name IN 
            ('InsuredRelationID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressState',
             'AddressZip',
             'AlternateAreaCode',
             'AlternateExchangeNumber',
             'AlternateExtensionNumber',
             'AlternateUnitNumber',
             'BestContactTime',
             'BestContactPhoneCD',
             'DayAreaCode',
             'DayExchangeNumber',
             'DayExtensionNumber',
             'DayUnitNumber',
             'EmailAddress',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'NightAreaCode',
             'NightExchangeNumber',
             'NightExtensionNumber',
             'NightUnitNumber', 
		--Updated By glsd451
	         'PrefMethodUpd',
			 'CellPhoneCarrier',
			 'CellAreaCode',
			 'CellExchangeNumber',
			 'CellUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Document',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN 
            ('AgreedPriceMetCD'))
      AND   (Table_Name = 'utb_estimate_summary' AND Column_Name IN 
            ('OriginalExtendedAmt',
             'AgreedExtendedAmt'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

--select * from @tmpMetadata

    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,  
        ReferenceID     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )
    
    
    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table    

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name)
    
    SELECT  'AssignmentType' AS ListName,
            at.DisplayOrder,
            CAST(at.AssignmentTypeID AS Varchar),
            at.Name
    FROM    utb_client_assignment_type cat
    LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)    
    WHERE   cat.InsuranceCompanyID = @InsuranceCompanyID
      AND   at.enabledFlag = 1
    
    UNION ALL
    
    SELECT  'ContactRelationToInsured',
            DisplayOrder,
            CAST(RelationID AS Varchar),
            Name
    FROM    dbo.utb_Relation
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL
    AND     Name NOT LIKE 'Third%'

    UNION ALL
    
    SELECT  'CoverageProfile',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect', 'CoverageProfileCD' )

    UNION ALL
    
    SELECT  'EstimateType',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_document', 'EstimateTypeCD' )

    UNION ALL
    
    SELECT  'Impact',
            DisplayOrder,
            CAST(ImpactID AS Varchar), 
            Name 
    FROM    dbo.utb_impact
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'SafetyDevice',
            DisplayOrder,
            CAST(SafetyDeviceID AS Varchar), 
            Name 
    FROM    dbo.utb_safety_device
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'State',
            DisplayOrder,
            StateCode, 
            StateValue 
    FROM    dbo.utb_state_code

    UNION ALL

    SELECT  'Exposure',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect', 'ExposureCD' )

    UNION ALL
    
    SELECT  'PermissionToDrive',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_vehicle', 'PermissionToDriveCD' )

    UNION ALL
    
    SELECT  'BestContactPhone',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'BestContactPhoneCD' )
    
    UNION ALL
    
    SELECT  'PrefMethodUpd',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'PrefMethodUpd' )
    
    UNION ALL
    
    SELECT  'CellPhoneCarrier',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'CellPhoneCarrier' )
    
    UNION ALL   
    
    SELECT  'AgreedPriceMetCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_document', 'AgreedPriceMetCD' )    
    
    UNION ALL
    
    SELECT  'ServiceChannelCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect_service_channel', 'ServiceChannelCD' )    

    UNION ALL
    
    SELECT  'DispositionTypeCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect_service_channel', 'DispositionTypeCD' )    

    UNION ALL
    
    SELECT  Distinct 'ClientServiceChannels',
            NULL,
            a.ServiceChannelDefaultCD,
            urc.Name
    FROM    utb_client_assignment_type cat
	left join utb_assignment_type a on cat.AssignmentTypeID = a.AssignmentTypeID
	left join dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc on a.ServiceChannelDefaultCD = urc.Code
	where cat.InsuranceCompanyID = @InsuranceCompanyID

    ORDER BY ListName, DisplayOrder
    

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    -- Validate APD Data state
    
    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    -- Continuing to validate APD Data state
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WHERE CategoryCD = 'CP' AND Name = 'ContractPrice')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"ContractPrice"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'RepairTotal')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    
    -- We need to adjust the reference data for CoverageProfileCD.  For first party vehicle, only COLL, COMP and UIM are 
    -- valid, for third party on LIAB is valid, for Non-exposures, none is valid
    
    SELECT  @ExposureCD = ExposureCD 
      FROM  dbo.utb_claim_aspect
      WHERE ClaimAspectID = @ClaimAspectID
      
    IF @ExposureCD <> '1'
    BEGIN 
      DELETE FROM @tmpReference
        WHERE ListName = 'CoverageProfile'
          AND ReferenceID IN ('COLL', 'COMP', 'UIM')
    END
    
    IF @ExposureCD <> '3'
    BEGIN 
      DELETE FROM @tmpReference
        WHERE ListName = 'CoverageProfile'
          AND ReferenceID IN ('LIAB')
    END
    
      
    -- We now need to select estimates into a table variable.  We have to do this here instead of directly
    -- in the XML query because we need to guarantee a record returned.  Our standard way of doing this
    -- {(SELECT @LynxID AS LynxID) AS parms}  and then joining this back doesn't work because this table
    -- is shared by notes.  So a claim with notes but no documents won't retrieve the empty document
    -- correctly.  The following code is a workaround until a more elegant solution can be found. 

    DECLARE @tmpDocument TABLE
    (
        DocumentID  int
    )

    INSERT INTO @tmpDocument
      SELECT  cascd.DocumentID
        FROM  dbo.utb_claim_aspect_service_channel casc
        LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE casc.ClaimAspectID = @ClaimAspectID
          AND d.EnabledFlag = 1         -- Only return enabled
          --AND dt.EstimateTypeFlag = 1   -- Only interested in estimates
          AND dt.Name not in ('Note') -- Not interested in notes


    -- If no records were selected, add one manually

    IF @@rowcount = 0
    BEGIN
        INSERT INTO @tmpDocument
        VALUES (0)
    END
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpDocument', 16, 1, @ProcName)
        RETURN
    END
    

    -- Generate PertainsToCode
    
    SET @PertainsTo = LTrim(RTrim(dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, @ClaimAspectNumber, 0)))  -- 0=Get code


    -- Get Counts of Notes and Tasks
    SET @CountTasks = (SELECT COUNT(c.CheckListID) 
                        FROM utb_checklist c
                        LEFT JOIN utb_claim_aspect_service_channel casc ON (c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
                        WHERE casc.ClaimAspectID = @ClaimAspectID)
    
    SET @CountNotes = (SELECT COUNT(*) 
                       FROM utb_claim_aspect_service_channel casc
                       LEFT JOIN utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                       INNER JOIN utb_document d ON cascd.DocumentID = d.DocumentID
                       WHERE casc.ClaimAspectID = @ClaimAspectID
                         AND d.EnabledFlag = 1)


    --SET @CountBilling = (SELECT COUNT(*) FROM dbo.utb_client_billing_service WHERE ClaimAspectID = @ClaimAspectID AND EnabledFlag = 1)
    SELECT @CountBilling = Count(InvoiceID)
    FROM dbo.utb_invoice i
    WHERE i.ClaimAspectID = @ClaimAspectID
      AND i.EnabledFlag = 1
      AND i.ItemTypeCD = 'F'    -- Fee    
      
    SET @ActiveReinspection = 0

    -- See if there is a reinspection request out there for the claim aspect
    IF EXISTS(SELECT d.DocumentID
                FROM dbo.utb_claim_aspect_service_channel casc
                LEFT JOIN utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = @ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1) OR -- any active reinspection request
       EXISTS(SELECT ReinspectID
                FROM dbo.utb_reinspect
                WHERE ClaimAspectID = @ClaimAspectID
                  AND LockedFlag = 0
                  AND EnabledFlag = 1) -- reinspection requested and has not been completed
    BEGIN
        SET @ActiveReinspection = 1
    END
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    DECLARE @tmpCurEstimate TABLE
    (
        DocumentID                  bigint       NOT NULL,
        EstimateSummaryID           bigint       NOT NULL,        
        OriginalExtendedAmt         decimal(9,2) NULL,
        EstimateSummaryTypeIDDesc   varchar(50)  NULL,
        CategoryCD                  varchar(5)   NULL,
        ServiceChannelCD            varchar(5)   NOT NULL
    )
    
    -- Get the Current Estimate Details
    INSERT INTO @tmpCurEstimate
    (DocumentID, EstimateSummaryID, OriginalExtendedAmt, EstimateSummaryTypeIDDesc, CategoryCD, ServiceChannelCD)
    SELECT DocumentID, EstimateSummaryID, OriginalExtendedAmt, EstimateSummaryTypeIDDesc, CategoryCD, ServiceChannelCD
    FROM dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', null, null) ufnEst 

    

    -- Select Root Level

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            @PertainsTo AS [Root!1!Context],
            @CountNotes AS [Root!1!CountNotes],
            @CountTasks AS [Root!1!CountTasks],
            @CountBilling AS [Root!1!CountBilling], 
            CASE
                WHEN @PertainsTo IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList(@LynxID, 0, 0)) THEN  1
                ELSE 0
            END AS [Root!1!ContextSupportedFlag],
            @ClientClaimNumber AS [Root!1!ClientClaimNumber],
            @LynxID AS[Root!1!LynxID],
             -- Claim Vehicle
            NULL AS [Vehicle!2!ClaimAspectID],
            NULL AS [Vehicle!2!VehicleNumber],
            NULL AS [Vehicle!2!ActiveReinspection],            
            NULL AS [Vehicle!2!BodyStyle],
            NULL AS [Vehicle!2!BookValueAmt],
            NULL AS [Vehicle!2!Color],
            NULL AS [Vehicle!2!CoverageProfileCD],
            NULL AS [Vehicle!2!ClientCoverageTypeID],
            NULL AS [Vehicle!2!CurrentAssignmentTypeID],
            NULL AS [Vehicle!2!DispositionType],
            NULL AS [Vehicle!2!DriveableFlag],
            NULL AS [Vehicle!2!ExposureCD],
            NULL AS [Vehicle!2!InspectionDate],
            NULL AS [Vehicle!2!InitialAssignmentTypeID],
            NULL AS [Vehicle!2!LicensePlateNumber],
            NULL AS [Vehicle!2!LicensePlateState],
            NULL AS [Vehicle!2!LocationAreaCode],
            NULL AS [Vehicle!2!LocationAddress1],
            NULL AS [Vehicle!2!LocationAddress2],
            NULL AS [Vehicle!2!LocationCity],
            NULL AS [Vehicle!2!LocationExchangeNumber],
            NULL AS [Vehicle!2!LocationExtensionNumber],
            NULL AS [Vehicle!2!LocationName],
            NULL AS [Vehicle!2!LocationState],
            NULL AS [Vehicle!2!LocationUnitNumber],
            NULL AS [Vehicle!2!LocationZip],
            NULL AS [Vehicle!2!Make],
            NULL AS [Vehicle!2!Mileage],
            NULL AS [Vehicle!2!Model],
            NULL AS [Vehicle!2!NADAId],
            NULL AS [Vehicle!2!Remarks],
            NULL AS [Vehicle!2!RentalDaysAuthorized],
            NULL AS [Vehicle!2!RentalInstructions],
            NULL AS [Vehicle!2!RepairEndDate],
            NULL AS [Vehicle!2!RepairEndConfirmFlag],
            NULL AS [Vehicle!2!RepairStartDate],
            NULL AS [Vehicle!2!RepairStartConfirmFlag],
            NULL AS [Vehicle!2!VehicleYear],
            NULL AS [Vehicle!2!EstimateVIN],
            NULL AS [Vehicle!2!VIN],
            NULL AS [Vehicle!2!StatusID],
            NULL AS [Vehicle!2!Status],
            NULL AS [Vehicle!2!SysLastUpdatedDate],
            NULL AS [Vehicle!2!ClaimAspectSysLastUpdatedDate],
            -- Vehicle Safety Device
            NULL AS [SafetyDevice!3!SafetyDeviceID],
            -- Vehicle Impact
            NULL AS [Impact!4!ImpactID],
            NULL AS [Impact!4!CurrentImpactFlag],
            NULL AS [Impact!4!PrimaryImpactFlag],
            NULL AS [Impact!4!PriorImpactFlag],
            -- Client Coverage types
            NULL AS [CoverageType!5!ClientCoverageTypeID],
            NULL AS [CoverageType!5!Name],
            NULL AS [CoverageType!5!CoverageProfileCD],
            NULL AS [CoverageType!5!DisplayOrder],  
            -- Contact
            NULL as [Contact!6!InvolvedID],
            NULL as [Contact!6!NameFirst],
            NULL as [Contact!6!NameLast],
            NULL as [Contact!6!NameTitle],
            NULL as [Contact!6!InsuredRelationID],
            NULL as [Contact!6!Address1],
            NULL as [Contact!6!Address2],
            NULL as [Contact!6!AddressCity],
            NULL as [Contact!6!AddressState],
            NULL as [Contact!6!AddressZip],
            NULL as [Contact!6!DayAreaCode],
            NULL as [Contact!6!DayExchangeNumber],
            NULL as [Contact!6!DayExtensionNumber],
            NULL as [Contact!6!DayUnitNumber],
            NULL as [Contact!6!EmailAddress],
            NULL as [Contact!6!NightAreaCode],
            NULL as [Contact!6!NightExchangeNumber],
            NULL as [Contact!6!NightExtensionNumber],
            NULL as [Contact!6!NightUnitNumber],
            NULL as [Contact!6!AlternateAreaCode],
            NULL as [Contact!6!AlternateExchangeNumber],
            NULL as [Contact!6!AlternateExtensionNumber],
            NULL as [Contact!6!AlternateUnitNumber],
            NULL as [Contact!6!BestContactTime],
            NULL as [Contact!6!BestContactPhoneCD],
            NULL as [Contact!6!SysLastUpdatedDate],
            --Modified @ 13 June 2012
			--Updated  By glsd451          
            NULL as [Contact!6!PrefMethodUpd],
            NULL as [Contact!6!CellPhoneCarrier],
            NULL as [Contact!6!CellAreaCode],
            NULL as [Contact!6!CellExchangeNumber],
            NULL as [Contact!6!CellUnitNumber],
             -- Involved
            NULL AS [Involved!7!InvolvedID],
            NULL AS [Involved!7!NameFirst],
            NULL AS [Involved!7!NameLast],
            NULL AS [Involved!7!BusinessName],
            NULL AS [Involved!7!DayAreaCode],
            NULL AS [Involved!7!DayExchangeNumber],
            NULL AS [Involved!7!AddressCity],
            NULL AS [Involved!7!AddressState],
            NULL AS [Involved!7!AddressZip],
            -- Involved Type
            NULL AS [InvolvedType!8!InvolvedTypeName],
            -- Metadata Header
            NULL AS [Metadata!9!Entity],
            -- Columns
            NULL AS [Column!10!Name],
            NULL AS [Column!10!DataType],
            NULL AS [Column!10!MaxLength],
            NULL AS [Column!10!Precision],
            NULL AS [Column!10!Scale],
            NULL AS [Column!10!Nullable],
            -- Reference Data
            NULL AS [Reference!11!List],
            NULL AS [Reference!11!ReferenceID],
            NULL AS [Reference!11!Name],
			--Service Channel Data
			NULL as [ClaimAspectServiceChannel!12!ClaimAspectServiceChannelID],
			NULL as [ClaimAspectServiceChannel!12!CreatedUserFirstName],
			NULL as [ClaimAspectServiceChannel!12!CreatedUserLastName],
			NULL as [ClaimAspectServiceChannel!12!CreatedUserFullName],
			NULL as [ClaimAspectServiceChannel!12!CreatedUserID],
			NULL as [ClaimAspectServiceChannel!12!CreatedDate],
			NULL as [ClaimAspectServiceChannel!12!DispositionTypeCD],
			NULL as [ClaimAspectServiceChannel!12!StatusID],
			NULL as [ClaimAspectServiceChannel!12!StatusName],
			NULL as [ClaimAspectServiceChannel!12!EnabledFlag],
			NULL as [ClaimAspectServiceChannel!12!InspectionDate],
			NULL as [ClaimAspectServiceChannel!12!ClientInvoiceDate],
            NULL as [ClaimAspectServiceChannel!12!CashOutDate],
			NULL as [ClaimAspectServiceChannel!12!FinalEstDate],
			NULL as [ClaimAspectServiceChannel!12!OriginalCompleteDate],
			NULL as [ClaimAspectServiceChannel!12!OriginalEstimateDate],
			NULL as [ClaimAspectServiceChannel!12!PrimaryFlag],
			NULL as [ClaimAspectServiceChannel!12!RepairLocationCity],
			NULL as [ClaimAspectServiceChannel!12!RepairLocationCounty],
			NULL as [ClaimAspectServiceChannel!12!RepairLocationState],
			NULL as [ClaimAspectServiceChannel!12!ServiceChannelCD],
			NULL as [ClaimAspectServiceChannel!12!ServiceChannelName],
			NULL as [ClaimAspectServiceChannel!12!WorkEndConfirmFlag],
			NULL as [ClaimAspectServiceChannel!12!WorkEndDate],
			NULL as [ClaimAspectServiceChannel!12!WorkEndDateOriginal],
			NULL as [ClaimAspectServiceChannel!12!WorkStartConfirmFlag],
			NULL as [ClaimAspectServiceChannel!12!WorkStartDate],
			NULL as [ClaimAspectServiceChannel!12!CurEstGrossRepairTotal],
			NULL as [ClaimAspectServiceChannel!12!ReferenceID],
			NULL as [ClaimAspectServiceChannel!12!SysLastUpdatedDate],
			-- Claim Aspect Service Channel Coverage
			NULL as [Coverage!13!CoverageTypeCD],
			NULL as [Coverage!13!DeductibleAmt],
			NULL as [Coverage!13!LimitAmt],
			NULL as [Coverage!13!LimitDailyAmt],
			NULL as [Coverage!13!MaximumDays],
			NULL as [Coverage!13!DeductibleAppliedAmt],
			NULL as [Coverage!13!LimitAppliedAmt],
			NULL as [Coverage!13!ClientCode],
			NULL as [Coverage!13!PartialCoverageFlag],
            -- Assignment Data
            NULL AS [Assignment!14!AssignmentID],
            NULL AS [Assignment!14!AssignmentTypeCD],

            NULL AS [Assignment!14!CurEstGrossRepairTotal],
            NULL AS [Assignment!14!CurEstDeductiblesApplied],
            NULL AS [Assignment!14!CurEstLimitsEffect],
            NULL AS [Assignment!14!CurEstNetRepairTotal],
            NULL AS [Assignment!14!EffectiveDeductibleSentAmt],

            NULL AS [Assignment!14!VANAssignmentStatusID],
            NULL AS [Assignment!14!VANAssignmentStatusName],
            NULL AS [Assignment!14!FaxAssignmentStatusID],
            NULL AS [Assignment!14!FaxAssignmentStatusName],
            NULL AS [Assignment!14!AppraiserID],
            NULL AS [Assignment!14!ShopLocationID],
            NULL AS [Assignment!14!ShopLocationName],
            NULL AS [Assignment!14!ShopLocationContactName],
            NULL AS [Assignment!14!ShopLocationPhoneAreaCode],
            NULL AS [Assignment!14!ShopLocationPhoneExchangeNumber],
            NULL AS [Assignment!14!ShopLocationPhoneUnitNumber],
            NULL AS [Assignment!14!ShopLocationPhoneExtensionNumber],
            NULL AS [Assignment!14!ShopLocationFaxAreaCode],
            NULL AS [Assignment!14!ShopLocationFaxExchangeNumber],
            NULL AS [Assignment!14!ShopLocationFaxUnitNumber],
            NULL AS [Assignment!14!ShopLocationFaxExtensionNumber],
            NULL AS [Assignment!14!ShopLocationAddressLine1],
            NULL AS [Assignment!14!ShopLocationAddressLine2],
            NULL AS [Assignment!14!ShopLocationAddressCity],
            NULL AS [Assignment!14!ShopLocationAddressCounty],
            NULL AS [Assignment!14!ShopLocationAddressState],
            NULL AS [Assignment!14!ShopLocationAddressZip],
            NULL AS [Assignment!14!AssignmentDate],
            NULL AS [Assignment!14!PrevAssignmentDate],
            NULL AS [Assignment!14!CancellationDate],
            NULL AS [Assignment!14!SelectionDate],
            NULL AS [Assignment!14!CommunicationMethodName],
            NULL AS [Assignment!14!AssignmentRemarks],
            NULL AS [Assignment!14!SysLastUpdatedDate],
            -- Settlement
            NULL AS [Settlement!15!SettlementAmount],
            NULL AS [Settlement!15!SettlementDate],
            NULL AS [Settlement!15!AdvanceAmount],
            NULL AS [Settlement!15!LoGAmount],
            NULL AS [Settlement!15!LHName],
            NULL AS [Settlement!15!LHAddress1],
            NULL AS [Settlement!15!LHAddress2],
            NULL AS [Settlement!15!LHAddressCity],
            NULL AS [Settlement!15!LHAddressState],
            NULL AS [Settlement!15!LHAddressZip],
            NULL AS [Settlement!15!LHPhone],
            NULL AS [Settlement!15!LHFax],
            NULL AS [Settlement!15!LHEmailAddress],
            NULL AS [Settlement!15!LHContactName],
            NULL AS [Settlement!15!LHPayoffAmount],
            NULL AS [Settlement!15!LHPayoffExpirationDate],
            NULL AS [Settlement!15!LHAccountNumber],
            NULL AS [Settlement!15!SalvageName],
            NULL AS [Settlement!15!SalvageAddress1],
            NULL AS [Settlement!15!SalvageAddress2],
            NULL AS [Settlement!15!SalvageAddressCity],
            NULL AS [Settlement!15!SalvageAddressState],
            NULL AS [Settlement!15!SalvageAddressZip],
            NULL AS [Settlement!15!SalvagePhone],
            NULL AS [Settlement!15!SalvageFax],
            NULL AS [Settlement!15!SalvageContactName],
            NULL AS [Settlement!15!SalvageControlNumber],
            NULL AS [Settlement!15!TitleName],
            NULL AS [Settlement!15!TitleState],
            NULL AS [Settlement!15!TitleStatus]

    UNION ALL


    -- Select Vehicle Level

    SELECT  distinct 2,
            1,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            IsNull(ca.ClaimAspectID, 0),
            ISNULL(cast(ca.ClaimAspectNumber as varchar(10)),''),
            IsNull(@ActiveReinspection, 0),
            IsNull(cv.BodyStyle, ''),
            IsNull(Convert(varchar(20), cv.BookValueAmt), ''),
            IsNull(cv.Color, ''),
            IsNull(ca.CoverageProfileCD, ''),
            IsNull(ca.ClientCoverageTypeID, 0),
            '', --IsNull(ca.CurrentAssignmentTypeID, ''), -- column deprecated
            '', --IsNull((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'DispositionTypeCD') WHERE Code = ca.DispositionTypeCD), ''),
            IsNull(cv.DriveableFlag, ''),
            IsNull(ca.ExposureCD, ''),
            '', --IsNull(cv.InspectionDate, ''),
            IsNull(ca.InitialAssignmentTypeID, ''),
            IsNull(cv.LicensePlateNumber, ''),
            IsNull(cv.LicensePlateState, ''),
            IsNull(cv.LocationAreaCode, ''),
            IsNull(cv.LocationAddress1, ''),
            IsNull(cv.LocationAddress2, ''),
            IsNull(cv.LocationCity, ''),
            IsNull(cv.LocationExchangeNumber, ''),
            IsNull(cv.LocationExtensionNumber, ''),
            IsNull(cv.LocationName, ''),
            IsNull(cv.LocationState, ''),
            IsNull(cv.LocationUnitNumber, ''),
            IsNull(cv.LocationZip, ''),
            IsNull(cv.Make, ''),
            IsNull(Convert(varchar(10), cv.Mileage), ''),
            IsNull(cv.Model, ''),
            IsNull(cv.NADAId, ''),
            IsNull(cv.Remarks, ''),
            IsNull(cv.RentalDaysAuthorized, ''),
            IsNull(cv.RentalInstructions, ''),
            '', --IsNull(cv.RepairEndDate, ''),
            '', --IsNull(cv.RepairEndConfirmFlag, 0),
            '', --IsNull(cv.RepairStartDate, ''),
            '', --IsNull(cv.RepairStartConfirmFlag, 0),
            IsNull(convert(varchar(5), cv.VehicleYear), ''),
            IsNull(cv.EstimateVin, ''),
            IsNull(cv.Vin, ''),
            --@VehicleStatus,
            isnull(vs.StatusID, ''),
            isnull(vs.Name,''),
            dbo.ufnUtilityGetDateString( cv.SysLastUpdatedDate ),
            dbo.ufnUtilityGetDateString( ca.SysLastUpdatedDate ),            
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
        	--Service Channel data
        	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	-- Claim Aspect Service Channel Coverage
        	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    
    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_claim_aspect ca ON (parms.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (parms.ClaimAspectID = cv.ClaimAspectID)
    /*
    left outer join utb_Claim_Aspect_Status cas
    on cas.ClaimAspectID = ca.ClaimAspectID

    LEFT JOIN dbo.utb_status vs ON cas.StatusID = vs.StatusID
    where     cas.StatusTypeCD is null
    and       vs.StatusTypeCD is null
    and       vs.ClaimAspectTypeID = 9 --for vehicle
    */
    Left Outer Join    ( -- for Vehicle Status
                        select     cas.ClaimAspectID,
                                   s.StatusID,
                                   s.Name
                        
                        
                        from       utb_Claim_Aspect_Status cas
                        
                        inner join utb_Status s
                        on         s.StatusID = cas.StatusID

                        where   cas.ServiceChannelCD is null
                        and     cas.StatusTypeCD is null 
                       ) vs
    on                vs.ClaimAspectID = ca.ClaimAspectID


    UNION ALL


    -- Select the Safety Device Level

    SELECT  3,
            2,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            IsNull(Convert(varchar(3), vsd.SafetyDeviceID), ''), 
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,  
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_vehicle_safety_device vsd ON (parms.ClaimAspectID = vsd.ClaimAspectID)


    UNION ALL


    -- Select the Impact Level

    SELECT  4,
            2,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            IsNull(vi.ImpactID, 0),
            IsNull(vi.CurrentImpactFlag, ''),
            IsNull(vi.PrimaryImpactFlag, ''),
            IsNull(vi.PriorImpactFlag, ''),
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,  
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_vehicle_impact vi ON (parms.ClaimAspectID = vi.ClaimAspectID)


    UNION All

  -- Select client coverage types along with APD types

 SELECT  5,
            2,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage types
            IsNull(ClientCoverageTypeID,0),
            IsNull(Name,''),
            IsNull(CoverageProfileCD,''),  
            IsNull(DisplayOrder,0),            
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID and EnabledFlag = 1


    UNION ALL

    -- Select Vehicle Contact Level

    SELECT  6,
            2,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            IsNull(i.InvolvedID, 0),
            IsNull(i.NameFirst, ''),
            IsNull(i.NameLast, ''),
            IsNull(i.NameTitle, ''),
            IsNull(i.InsuredRelationID, 0),
            IsNull(i.Address1, ''),
            IsNull(i.Address2, ''),
            IsNull(i.AddressCity, ''),
            IsNull(i.AddressState, ''),
            IsNull(i.AddressZip, ''),
            IsNull(i.DayAreaCode, ''),
            IsNull(i.DayExchangeNumber, ''),
            IsNull(i.DayExtensionNumber, ''),
            IsNull(i.DayUnitNumber, ''),
            IsNull(i.EmailAddress, ''),
            IsNull(i.NightAreaCode, ''),
            IsNull(i.NightExchangeNumber, ''),
            IsNull(i.NightExtensionNumber, ''),
            IsNull(i.NightUnitNumber, ''),
            IsNull(i.AlternateAreaCode, ''),
            IsNull(i.AlternateExchangeNumber, ''),
            IsNull(i.AlternateExtensionNumber, ''),
            IsNull(i.AlternateUnitNumber, ''),
            IsNull(i.BestContactTime, ''),
            IsNull(i.BestContactPhoneCD, ''),
            dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate ),
			--Updated By glsd451
            IsNull(i.PrefMethodUpd, ''),
            IsNull(i.CellPhoneCarrier, ''),
            IsNull(i.CellAreaCode, ''),
            IsNull(i.CellExchangeNumber, ''),
            IsNull(i.CellUnitNumber, ''),
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_claim_vehicle cv ON (parms.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_involved i ON (cv.ContactInvolvedID = i.InvolvedID)


    UNION ALL


    -- Select the Involved Level

    SELECT  7,
            2,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            --Updated By glsd451 
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            IsNull(i.InvolvedID, 0),
            IsNull(i.NameFirst, ''),
            IsNull(i.NameLast, ''),
            IsNull(i.BusinessName, ''),
            IsNull(i.DayAreaCode, ''),
            IsNull(i.DayExchangeNumber, ''),
            IsNull(i.AddressCity, ''),
            IsNull(i.AddressState, ''),
            IsNull(i.AddressZip, ''),
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)


    UNION ALL


    -- Select the Involved Type Level

    SELECT  8,
            7,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            IsNull(cai.InvolvedID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            IsNull(irt.Name, ''),
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 9,
            1,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  10,
            9,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, 
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  11,
            1,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, 
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
			--Service Channel data
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        	NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, NULL, NULL,
			-- Claim Aspect Service Channel Coverage
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpReference

	UNION ALL

	select   distinct 
                12,
               2,
                   NULL, NULL, NULL, NULL, NULL, 
                   NULL, NULL, NULL, NULL,
               -- Claim Vehicle
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, 
               -- Vehicle Safety Device
               NULL,
               -- Vehicle Impact
               NULL, NULL, NULL, NULL,
               -- Client Coverage Types
               NULL, NULL, NULL, NULL,
               -- Contact
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
               NULL, NULL, NULL, NULL, NULL, NULL, 
               --Updated By glsd451
               NULL, NULL, NULL, NULL, NULL, 
               -- Involved
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
               -- Involved Type
               NULL,
               -- Metadata Header
               NULL,
               -- Columns
               NULL, NULL, NULL, NULL, NULL, NULL,
               -- Reference Data
               NULL,NULL,NULL,
               --Service Channel data
               casc.ClaimAspectServiceChannelID,
               ISNULL(u.NameFirst,''),
               ISNULL(u.NameLast,''),
               ltrim(rtrim(u.NameFirst)) + ' ' + ltrim(rtrim(u.NameLast)),
               ISNULL(cast(casc.CreatedUserID as varchar(20)),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.CreatedDate),''),
               ISNULL(casc.DispositionTypeCD,''),
               ISNULL(convert(varchar, s.StatusID), ''),
               ISNULL(s.Name, ''),
               ISNULL(cast(casc.EnabledFlag as varchar(01)),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.InspectionDate),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.ClientInvoiceDate),''),
                   isnull(dbo.ufnUtilityGetDateString(casc.CashOutDate),''),
                   isnull(dbo.ufnUtilityGetDateString(casc.AppraiserInvoiceDate),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.OriginalCompleteDate),''),
               ISNULL(dbo.ufnUtilityGetDateString(vt1.CreatedDate),''),  --Value for Original Estimate Date
               ISNULL(cast(casc.PrimaryFlag as varchar(01)),''),
               ISNULL(casc.RepairLocationCity, ''),
               ISNULL(casc.RepairLocationCounty, ''),
               ISNULL(casc.RepairLocationState, ''),
               ISNULL(casc.ServiceChannelCD,''),
               ISNULL(urc.Name,''),
               ISNULL(cast(casc.WorkEndConfirmFlag as varchar(01)),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.WorkEndDate),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.WorkEndDateOriginal),''),
               ISNULL(cast(casc.WorkStartConfirmFlag as varchar(01)),''),
               ISNULL(dbo.ufnUtilityGetDateString(casc.WorkStartDate),''),
               ISNULL(convert(varchar(10),ufnEst.OriginalExtendedAmt), ''),
               ISNULL((select a.ReferenceID
                       from utb_assignment a
                       where a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                         AND a.assignmentsequencenumber = 1 AND a.CancellationDate is null
                       ) --Only include the assignments that are not cancelled
                       , ''),
               dbo.ufnUtilityGetDateString(casc.SysLastUpdatedDate),
               -- Claim Aspect Service Channel Coverage
               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                -- Assignment Data
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	from 			utb_Claim_Aspect ca
	
	inner join 		utb_Claim_Aspect_Service_Channel casc on (ca.ClaimAspectID = casc.CLaimAspectID and ca.ClaimAspectID = @ClaimAspectID)
	Inner JOIN		utb_User u on casc.CreatedUserID = u.USERID	
	inner join     dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc on casc.ServiceChannelCD = urc.Code
	left outer join     dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', 'RepairTotal', null) ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
	
    left join      utb_claim_aspect_status cas on casc.ClaimAspectID = cas.ClaimAspectID --and casc.ServiceChannelCD = cas.ServiceChannelCD 20061228 M.A.
    and            cas.ServiceChannelCD = casc.ServiceChannelCD
    and            cas.StatusTypeCD='SC'
	left join      utb_status s on cas.StatusID = s.StatusID
    Left Outer join    (
                       select ClaimAspectID, ClaimAspectServiceChannelID, CreatedDate FROM dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'O', 'RepairTotal', 'TT')
                       ) vt1 --OriginalEstimateDate
     on                 vt1.ClaimAspectID = casc.ClaimAspectID
     and                vt1.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID

	WHERE casc.EnabledFlag = 1
    --AND   cas.StatusTypeCD is null

	UNION ALL

	select   13,
				12,
                NULL, NULL, NULL, NULL, NULL, 
                NULL, NULL, NULL, NULL,
				-- Claim Vehicle
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, 
				-- Vehicle Safety Device
				NULL,
				-- Vehicle Impact
				NULL, NULL, NULL, NULL,
				-- Client Coverage Types
				NULL, NULL, NULL, NULL,
				-- Contact
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, 
				--Updated By glsd451
				NULL, NULL, NULL, NULL, NULL, 
				-- Involved
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Involved Type
				NULL,
				-- Metadata Header
				NULL,
				-- Columns
				NULL, NULL, NULL, NULL, NULL, NULL,
				-- Reference Data
				NULL,NULL,NULL,
				--Service Channel data
				casc.ClaimAspectServiceChannelID, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				ISNULL(cc.CoverageTypeCD,''),
				ISNULL(cast(cc.DeductibleAmt as varchar(20)),''),
				isnull(cast(cc.LimitAmt as varchar(20)),''),
				isnull(cast(cc.LimitDailyAmt as varchar(20)),''),
				ISNULL(cast(cc.MaximumDays as varchar(20)),''),
				ISNULL(cast(cascc.DeductibleAppliedAmt as varchar(20)),''),
				ISNULL(cast(cascc.LimitAppliedAmt as varchar(20)),''),
                cast(PartialCoverageFlag as varchar(01)),
				IsNull(cct.ClientCode, ''),
                -- Assignment Data
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL, NULL, NULL, NULL, NULL,
                NULL,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	from utb_Claim_Aspect ca	
	left join utb_Claim_Aspect_Service_Channel casc on (ca.ClaimAspectID = casc.CLaimAspectID and ca.ClaimAspectID = @ClaimAspectID)
	LEFT join utb_Claim_Aspect_Service_Channel_Coverage cascc on (casc.ClaimAspectServiceChannelID = cascc.ClaimAspectServiceChannelID)
	Left JOIN utb_Claim_Coverage cc On  cascc.ClaimCoverageID = cc.ClaimCoverageID 	
	left join utb_client_coverage_type cct on cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
	WHERE casc.EnabledFlag = 1 
	  AND cc.EnabledFlag = 1


    UNION ALL

    SELECT  distinct 14,
           12,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, 
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
            --Service Channel data
            casc.ClaimAspectServiceChannelID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Coverage
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            isNull(a.AssignmentID, ''),
            CASE
               WHEN ((a.ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
                   ((a.AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A')) THEN 'LDAU'
               WHEN a.ShopLocationID IS NOT NULL THEN 'SHOP'
               WHEN a.AppraiserID IS NOT NULL THEN 'IA'
               ELSE ''
            END,
            isNull(convert(varchar, tmpEstRT.OriginalExtendedAmt), ''), --CurEstGrossRepairTotal
            isNull(convert(varchar, tmpEstDA.OriginalExtendedAmt), ''), --CurEstDeductiblesApplied
            isNull(convert(varchar, tmpEstLE.OriginalExtendedAmt), ''), --CurEstLimitsEffect
            isNull(convert(varchar, tmpEstNE.OriginalExtendedAmt), ''), --CurEstNetRepairTotal
            isNull(convert(varchar, a.EffectiveDeductibleSentAmt), ''),
            isNull(casv.StatusID, ''), --VANAssignmentStatusID
            isNull(sv.Name, ''), --VANAssignmentStatusName
            isNull(casf.StatusID, ''), --FaxAssignmentStatusID
            isNull(sf.Name, ''), --FaxAssignmentStatusName
            isNull(a.AppraiserID, ''),
            isNull(a.ShopLocationID, ''),
            isNull(tmp.Name, ''),
            isNull(tmp.ContactName, ''),
            isNull(tmp.PhoneAreaCode, ''),
            isNull(tmp.PhoneExchangeNumber, ''),
            isNull(tmp.PhoneUnitNumber, ''),
            isNull(tmp.PhoneExtensionNumber, ''),
            isNull(tmp.FaxAreaCode, ''),
            isNull(tmp.FaxExchangeNumber, ''),
            isNull(tmp.FaxUnitNumber, ''),
            isNull(tmp.FaxExtensionNumber, ''),
            isNull(tmp.Address1, ''),
            isNull(tmp.Address2, ''),
            isNull(tmp.AddressCity, ''),
            isNull(tmp.AddressCounty, ''),
            isNull(tmp.AddressState, ''),
            isNull(tmp.AddressZip, ''),
            isNull(dbo.ufnUtilityGetDateString (a.AssignmentDate), ''),
            isNull(dbo.ufnUtilityGetDateString (
               CASE
                  WHEN a.AppraiserID IS NOT NULL THEN
                     (SELECT MAX (AssignmentDate)
                      FROM utb_assignment al
                      WHERE al.AppraiserID = a.AppraiserID
                        AND al.AssignmentID <> a.AssignmentID)
                  ELSE
                     (SELECT MAX (AssignmentDate)
                      FROM utb_assignment al
                      WHERE al.ShopLocationID = a.ShopLocationID
                        AND al.AssignmentID <> a.AssignmentID)
               END), ''),
            isNull(dbo.ufnUtilityGetDateString (a.CancellationDate), ''),
            isNull(dbo.ufnUtilityGetDateString (a.SelectionDate), ''),
            isNull(cm.Name, ''),
            isNull(a.AssignmentRemarks, ''),
            a.SysLastUpdatedDate,
            -- Settlement
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL


      From utb_assignment a 
      left join utb_claim_aspect_service_channel casc on a.ClaimAspectServiceChannelID = casc.ClaimASpectServiceChannelID
      left join utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
      left join utb_claim_aspect_status casv ON ca.ClaimAspectID = casv.ClaimASpectID and casc.ServiceChannelCD = casv.ServiceCHannelCD and casv.StatusTypeCD = 'ELC'
      left join utb_status sv ON casv.StatusID = sv.StatusID
      left join utb_claim_aspect_status casf ON ca.ClaimAspectID = casf.ClaimASpectID and casc.ServiceChannelCD = casf.ServiceCHannelCD and casf.StatusTypeCD = 'FAX'
      left join utb_status sf ON casf.StatusID = sf.StatusID
      left join utb_communication_method cm ON a.CommunicationMethodID = cm.CommunicationMethodID
      left join @tmpCurEstimate tmpEstRT ON casc.ServiceChannelCD = tmpEstRT.ServiceChannelCD AND tmpEstRT.EstimateSummaryTypeIDDesc = 'RepairTotal' AND tmpEstRT.CategoryCD = 'TT'
      left join @tmpCurEstimate tmpEstDA ON casc.ServiceChannelCD = tmpEstDA.ServiceChannelCD AND tmpEstDA.EstimateSummaryTypeIDDesc = 'DeductiblesApplied' AND tmpEstDA.CategoryCD = 'OT'
      left join @tmpCurEstimate tmpEstLE ON casc.ServiceChannelCD = tmpEstLE.ServiceChannelCD AND tmpEstLE.EstimateSummaryTypeIDDesc = 'LimitEffect' AND tmpEstLE.CategoryCD = 'OT'
      left join @tmpCurEstimate tmpEstNE ON casc.ServiceChannelCD = tmpEstNE.ServiceChannelCD AND tmpEstNE.EstimateSummaryTypeIDDesc = 'NetTotalEffect' AND tmpEstNE.CategoryCD = 'OT'
      left join ( SELECT 
                     AppraiserID as tmpID,
                     Name, --NULL AS [Assignment!14!ShopLocationName],
                     NULL as ContactName, -- AS [Assignment!14!ShopLocationContactName],
                     PhoneAreaCode, --NULL AS [Assignment!14!ShopLocationPhoneAreaCode],
                     PhoneExchangeNumber, --NULL AS [Assignment!14!ShopLocationPhoneExchangeNumber],
                     PhoneUnitNumber, --NULL AS [Assignment!14!ShopLocationPhoneUnitNumber],
                     PhoneExtensionNumber, --NULL AS [Assignment!14!ShopLocationPhoneExtensionNumber],
                     FaxAreaCode, --NULL AS [Assignment!14!ShopLocationFaxAreaCode],
                     FaxExchangeNumber, --NULL AS [Assignment!14!ShopLocationFaxExchangeNumber],
                     FaxUnitNumber, --NULL AS [Assignment!14!ShopLocationFaxUnitNumber],
                     FaxExtensionNumber, --NULL AS [Assignment!14!ShopLocationFaxExtensionNumber],
                     Address1, --NULL AS [Assignment!14!ShopLocationAddressLine1],
                     Address2, --NULL AS [Assignment!14!ShopLocationAddressLine2],
                     AddressCity, --NULL AS [Assignment!14!ShopLocationAddressCity],
                     AddressCounty, --NULL AS [Assignment!14!ShopLocationAddressCounty],
                     AddressState, --NULL AS [Assignment!14!ShopLocationAddressState],
                     AddressZip --NULL AS [Assignment!14!ShopLocationAddressZip],
      
                from utb_appraiser
      
                UNION ALL
      
                SELECT 
                  sl.ShopLocationID,
                  sl.Name, --NULL AS [Assignment!14!ShopLocationName],
                  p.Name as ContactName, -- AS [Assignment!14!ShopLocationContactName],
                  sl.PhoneAreaCode, --NULL AS [Assignment!14!ShopLocationPhoneAreaCode],
                  sl.PhoneExchangeNumber, --NULL AS [Assignment!14!ShopLocationPhoneExchangeNumber],
                  sl.PhoneUnitNumber, --NULL AS [Assignment!14!ShopLocationPhoneUnitNumber],
                  sl.PhoneExtensionNumber, --NULL AS [Assignment!14!ShopLocationPhoneExtensionNumber],
                  sl.FaxAreaCode, --NULL AS [Assignment!14!ShopLocationFaxAreaCode],
                  sl.FaxExchangeNumber, --NULL AS [Assignment!14!ShopLocationFaxExchangeNumber],
                  sl.FaxUnitNumber, --NULL AS [Assignment!14!ShopLocationFaxUnitNumber],
                  sl.FaxExtensionNumber, --NULL AS [Assignment!14!ShopLocationFaxExtensionNumber],
                  sl.Address1, --NULL AS [Assignment!14!ShopLocationAddressLine1],
                  sl.Address2, --NULL AS [Assignment!14!ShopLocationAddressLine2],
                  sl.AddressCity, --NULL AS [Assignment!14!ShopLocationAddressCity],
                  sl.AddressCounty, --NULL AS [Assignment!14!ShopLocationAddressCounty],
                  sl.AddressState, --NULL AS [Assignment!14!ShopLocationAddressState],
                  sl.AddressZip --NULL AS [Assignment!14!ShopLocationAddressZip],
               from utb_shop_location sl
               LEFT JOIN dbo.utb_shop_location_personnel slp ON (sl.ShopLocationID = slp.ShopLocationID)
               LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
               LEFT JOIN dbo.utb_personnel_type pt ON (p.PersonnelTypeID = pt.PersonnelTypeID) 
               where pt.Name = 'Shop Manager') tmp ON (a.AppraiserID = tmp.tmpID or a.ShopLocationID = tmp.tmpID)
      where ca.ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
      and a.CancellationDate is null

    UNION ALL

    SELECT  distinct 15,
           12,
            NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, 
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, 
            --Updated By glsd451
            NULL, NULL, NULL, NULL, NULL, 
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
            --Service Channel data
            casc.ClaimAspectServiceChannelID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Coverage
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Data
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Settlement
			convert(varchar, SettlementAmount), 
			convert(varchar, SettlementDate, 101), 
			convert(varchar, AdvanceAmount), 
			convert(varchar, LetterOfGuaranteeAmount), 
			lh.Name, 
			lh.Address1, 
			lh.Address2, 
			lh.AddressCity,
			lh.AddressState, 
			lh.AddressZip, 
			lh.PhoneAreaCode + lh.PhoneExchangeNumber + lh.PhoneUnitNumber, 
			lh.FaxAreaCode + lh.FaxExchangeNumber + lh.FaxUnitNumber,
			lh.EmailAddress, 
			lh.ContactName, 
			convert(varchar, PayoffAmount),
			convert(varchar, PayoffExpirationDate, 101), 
			LeinHolderAccountNumber, 
			sv.Name, 
			sv.Address1, 
			sv.Address2, 
			sv.AddressCity, 
			sv.AddressState, 
			sv.AddressZip, 
			sv.PhoneAreaCode + sv.PhoneExchangeNumber + sv.PhoneUnitNumber, 
			sv.FaxAreaCode + sv.FaxExchangeNumber + sv.FaxExtensionNumber, 
			sv.ContactName, 
			SalvageControlNumber, 
			cv.TitleName, 
			cv.TitleState, 
			cv.TitleStatus

      From utb_claim_aspect_service_channel casc
      left join utb_lien_holder lh on casc.LienHolderID = lh.LienHolderID
      left join utb_salvage_vendor sv on casc.SalvageVendorID = sv.SalvageVendorID
      left join utb_claim_vehicle cv on casc.ClaimAspectID = cv.ClaimAspectID
      WHERE casc.ClaimAspectID = @ClaimAspectID

    ORDER BY [Metadata!9!Entity], [Involved!7!InvolvedID],[ClaimAspectServiceChannel!12!ClaimAspectServiceChannelID], Tag
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADClaimVehicleGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspECADClaimVehicleGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

