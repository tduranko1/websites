-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReportsGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspReportsGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspReportsGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     returns a list of reports available for the office the user belongs
*
* PARAMETERS:  
* (I) @UserID   The User ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspReportsGetListXML
    @UserID             as udt_std_int
AS
BEGIN
    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @InsuranceCompanyName   as varchar(50)
    DECLARE @OfficeName             as varchar(50)
    DECLARE @InsuranceCompanyIDWorking as int
    DECLARE @OfficeIDWorking        AS int
    DECLARE @ClaimViewLevel         AS tinyint
    DECLARE @AllClaimsID            AS tinyint
    DECLARE @OwnClaimsID            AS tinyint
    DECLARE @ViewReport             AS varchar(1)
    DECLARE @Debug                  AS bit
    DECLARE @ClaimViewLvlProfileID  AS tinyint

    SET @ProcName = 'uspReportsGetListXML'
    SET @Debug = 0
    
    IF @Debug = 1
    BEGIN
        PRINT '@UserID: ' + CONVERT(varchar, @UserID)
    END    
    
    IF NOT EXISTS (SELECT UserID
                    FROM utb_user
                    WHERE UserID = @UserID
                  )
    BEGIN
       -- Invalid User

        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the Insurance Company and Office to which the user belongs
    SELECT @InsuranceCompanyIDWorking = o.InsuranceCompanyID,
           @OfficeIDWorking = o.OfficeID,
           @OfficeName = isNull(Name, '')
    FROM utb_office o, utb_user u
    WHERE u.OfficeID = o.OfficeID
      AND u.UserID = @UserID
      
    -- Get the Insurance Company name the user belongs
    SELECT @InsuranceCompanyName = Name 
    FROM utb_insurance
    WHERE InsuranceCompanyID = @InsuranceCompanyIDWorking
    
    SELECT @ClaimViewLvlProfileID = ProfileID
    FROM dbo.utb_profile
    WHERE Name = 'ClaimPoint Claim View Level'
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @ClaimViewLvlProfileID IS NULL
    BEGIN
        -- Profile item not found

        RAISERROR('102|%s|"%s"|utb_profile', 16, 1, @ProcName, 'ClaimPoint Claim View Level')
        RETURN
    END
    
    

    -- Get the Users claim view level.
    SELECT @ClaimViewLevel = up.Value
    FROM utb_user_profile up, utb_user usr
    WHERE up.UserID = usr.UserID
      AND usr.UserID = @UserID
      AND up.ProfileID = @ClaimViewLvlProfileID
    
    -- Get the ID for All Claims view level
    SELECT @AllClaimsID = SelectionID
    FROM utb_profile_selection ps, utb_profile p
    WHERE p.ProfileID = ps.ProfileID
      AND p.Name = 'ClaimPoint Claim View Level'
      AND ps.Name like 'All Claims%' -- there is a space after the value in the data.

    -- Get the ID for Own Claims view level
    SELECT @OwnClaimsID = SelectionID
    FROM utb_profile_selection ps, utb_profile p
    WHERE p.ProfileID = ps.ProfileID
      AND p.Name = 'ClaimPoint Claim View Level'
      AND ps.Name like 'Own Claims' -- there is a space after the value in the data.
      
    -- Determine the selection criteria for the reports list
    IF @ClaimViewLevel = @AllClaimsID
        SET @ViewReport = '%'
    ELSE IF @ClaimViewLevel = @OwnClaimsID
        SET @ViewReport = ''
    ELSE
        SET @ViewReport = '1'
        
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyIDWorking: ' + CONVERT(varchar, @InsuranceCompanyIDWorking)
        PRINT '@InsuranceCompanyName: ' + CONVERT(varchar, @InsuranceCompanyName)
        PRINT '@OfficeIDWorking: ' + CONVERT(varchar, @OfficeIDWorking)
        PRINT '@OfficeName: ' + CONVERT(varchar, @OfficeName)
        PRINT '@ClaimViewLevel: ' + CONVERT(varchar, @ClaimViewLevel)
        PRINT '@ViewReport: ' + CONVERT(varchar, @ViewReport)
    END
    
    DECLARE @tmpReference TABLE 
    (
        ReportID        varchar(5)     NULL,
        Description     varchar(500)   NULL,
        DisplayOrder    int            NULL
    )
    
    IF @OfficeIDWorking IS NOT NULL AND @InsuranceCompanyIDWorking IS NOT NULL
    BEGIN
        --Non APD User
        INSERT INTO @tmpReference
        SELECT r.ReportID, r.Description, r.DisplayOrder
        FROM utb_report r, utb_client_report cr
        WHERE r.ReportID = cr.ReportID
          AND cr.InsuranceCompanyID = @InsuranceCompanyIDWorking
          AND r.ReportTypeCD = 'C' -- get the client reports only
          AND convert(varchar, r.OfficeLevelUserViewFlag) LIKE @ViewReport
          AND r.EnabledFlag = 1
    END
    ELSE
    BEGIN
        --APD User
        INSERT INTO @tmpReference
        SELECT r.ReportID, r.Description, r.DisplayOrder
        FROM utb_report r
        WHERE r.ReportTypeCD = 'C' -- get the client reports only
          AND r.EnabledFlag = 1
    END

    --Begin Select Statement
    SELECT
        1                           AS tag,
        Null                        AS Parent,
        @InsuranceCompanyName       AS [Root!1!InsuranceCompanyName],
        @InsuranceCompanyIDWorking  AS [Root!1!InsuranceCompanyID],
        @OfficeName                 AS [Root!1!OfficeName],
        @OfficeIDWorking            AS [Root!1!OfficeID],
        -- Available Report
        Null                        AS [Report!2!ReportID],
        Null                        AS [Report!2!ReportName],
        Null                        AS [Report!2!DisplayOrder]
    
    UNION ALL
    
    SELECT 2,
           1,
           NULL, NULL, NULL, NULL,
           -- Available report
           r.ReportID,
           r.Description,
           r.DisplayOrder
    FROM @tmpReference r
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReportsGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspReportsGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/