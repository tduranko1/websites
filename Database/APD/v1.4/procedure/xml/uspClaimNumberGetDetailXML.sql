-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimNumberGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimNumberGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will return the vehicle list for the client claim number
*
* PARAMETERS:  
* (I) @ClientClaimNumber    Client Claim Number
* (I) @InsuranceCompanyID   Insurance Company ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimNumberGetDetailXML
    @ClientClaimNumber      udt_cov_claim_number,
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Set database options

    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables

    DECLARE @LynxID                     udt_std_id_big
    DECLARE @InsuranceCompanyIDClaim    udt_std_id
    DECLARE @ClaimAspectTypeID          udt_std_id
    DECLARE @ClientClaimNumberSquished  udt_cov_claim_number

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimNumberGetDetailXML'
    
    SET @ClientClaimNumberSquished = dbo.ufnUtilitySquishString(@ClientClaimNumber, 1, 1, 0, NULL)

    SELECT @LynxID = LynxID
    FROM dbo.utb_claim WITH (NOLOCK)
    WHERE (@ClientClaimNumberSquished <> '' and ClientClaimNumberSquished = @ClientClaimNumberSquished)
      AND InsuranceCompanyID = @InsuranceCompanyID
    
    
    -- Get the aspect type id for property for use later
    
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type WITH (NOLOCK)
      WHERE Name = 'Vehicle'
      
    -- Begin XML Select

    SELECT
        1 as Tag,
        0 as Parent,
--        Root
        @LynxID as [Root!1!LynxID],
--        VehicleList
        Null as [Vehicle!2!VehicleNumber],
        Null as [Vehicle!2!ClaimAspectID],
        Null as [Vehicle!2!VehicleYear],
        Null as [Vehicle!2!Make],
        Null as [Vehicle!2!Model],
        Null as [Vehicle!2!NameFirst],
        Null as [Vehicle!2!NameLast],
        Null as [Vehicle!2!BusinessName],
        Null as [Vehicle!2!ClosedStatus],
        Null as [Vehicle!2!Status],
        Null as [Vehicle!2!ExposureCD],
        Null as [Vehicle!2!CoverageProfileCD],
        Null as [Vehicle!2!CurrentAssignmentType]
        

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null,
--        Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai WITH (NOLOCK)
           LEFT JOIN dbo.utb_involved i WITH (NOLOCK) ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir WITH (NOLOCK) ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt WITH (NOLOCK) ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai WITH (NOLOCK)
           LEFT JOIN dbo.utb_involved i WITH (NOLOCK) ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir WITH (NOLOCK) ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt WITH (NOLOCK) ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai WITH (NOLOCK)
           LEFT JOIN dbo.utb_involved i WITH (NOLOCK) ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir WITH (NOLOCK) ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt WITH (NOLOCK) ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.Name,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, ''),
        --Project:210474 APD Remarked-off the following to support the schema change M.A.20061219
        --(SELECT Name FROM dbo.utb_assignment_type cat WHERE cat.AssignmentTypeID = ca.CurrentAssignmentTypeID)  --Project:210474 APD The column was added in utb_Claim_Aspect so changed it back to the original state. M.A.20061201
        --Project:210474 APD Added the following to support the schema change M.A.20061219
        (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD)
    FROM
        (SELECT @LynxID AS LynxID, @ClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca WITH (NOLOCK) ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID AND 1 = ca.EnabledFlag)
        LEFT JOIN dbo.utb_claim_vehicle cv WITH (NOLOCK) ON (ca.ClaimAspectID = cv.ClaimAspectID)
        LEFT JOIN dbo.utb_claim_aspect_status cas WITH (NOLOCK) ON (ca.ClaimAspectID = cas.ClaimAspectID)
        LEFT JOIN dbo.utb_status vs WITH (NOLOCK) ON (cas.StatusID = vs.StatusID )
        Left Outer Join utb_Claim_Aspect_Service_Channel casc WITH (NOLOCK)
        On ca.ClaimAspectID = casc.ClaimAspectID
        and casc.PrimaryFlag = 1

        where cas.StatusTypeCD is null
        and cas.ServiceChannelCD is null

    Order by [Vehicle!2!VehicleNumber], tag
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimNumberGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/