-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestVehicleAssignGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestVehicleAssignGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestVehicleAssignGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves vehicle assignment information for Hyperquest Assignments
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  The Claim Aspect Service Channel ID
* (I) @InsuranceCompanyID           The insurance company the claim aspect service channel belongs to
*
* RESULT SET:
* An XML Data stream containing vehicle assignment information
*
* MODIFICATIONS:  24Jun2016 - TVD - Added owner email address to vehicle section
*		  		  15Sep2016 - TVD - Change SQL to add ClientClaimNumber to the XML
*				  12Jul2017 - TVD - Added current shop estimating platform
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspHyperquestVehicleAssignGetDetailWSXML]
    @ClaimAspectServiceChannelID    udt_std_id_big,
 --@AssignmentSequenceNumber       udt_std_int_tiny,  
    @InsuranceCompanyID             udt_std_id
AS
BEGIN
    DECLARE @tmpMultipleAssignments TABLE
    (
        AssignmentID            int         NOT NULL,
        AssignmentTypeCD        varchar(4)  NOT NULL,
        AssignmentTypeID        int         NOT NULL,
        AssignmentSeqNumber     tinyint     NOT NULL,
        AppraiserID             bigint      NULL,
        ShopLocationID          bigint      NULL,
        Name                    varchar(50) NULL,
        AssignmentDate          datetime    NULL,
        CancellationDate        datetime    NULL,
        SelectionDate           datetime    NULL,
        CommunicationMethodID   int         NULL,
        ProgramTypeCD           varchar(4)  NULL,     
        SysLastUpdatedDate      datetime    NOT NULL
    )


    -- Declare local variables

	-- Due to lack of time, two active assignments will be created to handle RRP.
	-- In the future, we should account for any number of assignments with any type of assignment
    DECLARE @ActiveShopAssignmentID         udt_std_id
    DECLARE @ActiveDAAssignmentID           udt_std_id
    DECLARE @ActiveAssignmentTypeCD         udt_std_cd
    DECLARE @AppraiserID                    udt_std_id_big                   
    DECLARE @AssignmentRemarks              udt_std_desc_xlong
    DECLARE @Business1PersonnelTypeID       udt_std_id
    DECLARE @ClaimAspectNumber              udt_std_int
    DECLARE @ClaimAspectTypeIDAssignment    udt_std_id
    DECLARE @ClaimAspectTypeIDFax           udt_std_id
    DECLARE @DeskAuditAppraiserType         udt_std_cd
    DECLARE @DeskAuditID                    udt_std_id_big
    DECLARE @InsuranceCompanyIDClaim        udt_std_int
    DECLARE @InsuranceCompanyName           varchar(100)
    DECLARE @LastAssignedDate               udt_std_datetime
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @ShopLocationID                 udt_std_id_big
    DECLARE @MobileElectronicsAppraiserType udt_std_cd
    DECLARE @MobileElectronicsID            udt_std_id_big
    DECLARE @ActiveReinspection             bit
    DECLARE @WorkStatus                     varchar(50)
    DECLARE @ClaimAspectID                  udt_std_id_big
    DECLARE @CurEstGrossAmt                 decimal(9,2)
    DECLARE @DeductiblesApplied             decimal(9,2)
    DECLARE @LimitsEffect                   decimal(9,2)
    DECLARE @NetTotalEffect                 decimal(9,2)
    DECLARE @ServiceChannelCD               varchar(5)
    DECLARE @CurEstDocumentID               bigint
    DECLARE @OriginalEstAmt                 decimal(9,2)
    DECLARE @OriginalEstDate                datetime
    DECLARE @GlassAppraiserID               udt_std_id_big
    DECLARE @LossDate                       datetime
    DECLARE @RentalCoverage                 bit
    DECLARE @InsuredClaimantName            varchar(100)
    DECLARE @InsuredName	                varchar(100)
    DECLARE @InsuredBusinessName            varchar(100)
    DECLARE @ClaimantName                   varchar(100)
    DECLARE @ClaimantBusinessName           varchar(100)
    DECLARE @ExposureCD                     varchar(5)
    DECLARE @ClaimOwnerFirstName            varchar(100)
    DECLARE @ClaimOwnerLastName             varchar(100)
    DECLARE @ClaimOwnerUserID               udt_std_id
	DECLARE @ClaimOwnerLogonID				varchar(50)
    DECLARE @ClaimOwnerPhone                varchar(15)
    DECLARE @ClaimOwnerEmail                varchar(50)
    DECLARE @RepairLocationCity             varchar(50)
    DECLARE @RepairLocationCounty           varchar(50)
    DECLARE @RepairLocationState            varchar(2)
    DECLARE @RepairLocationZip              varchar(10)
    DECLARE @RepairLocationName             varchar(250)
    DECLARE @RepairLocationAddress          varchar(250)
    DECLARE @RepairLocationPhone            varchar(20)
    DECLARE @RepairLocationFax              varchar(20)
    DECLARE @RepairLocationEmail            varchar(100)
    DECLARE @RepairInspectionDate           varchar(15)


    DECLARE @SourceApplicationPassThruData as varchar(1000)
    DECLARE @InitialAssignmentTypeID as int
    DECLARE @delimiter as varchar(1)

    DECLARE @ShopOpen                       varchar(8),
            @OperatingMondayStartTime       char(4),
            @OperatingMondayEndTime         char(4),
            @OperatingTuesdayStartTime      char(4),
            @OperatingTuesdayEndTime        char(4),
            @OperatingWednesdayStartTime    char(4),
            @OperatingWednesdayEndTime      char(4),
            @OperatingThursdayStartTime     char(4),
            @OperatingThursdayEndTime       char(4),
            @OperatingFridayStartTime       char(4),
            @OperatingFridayEndTime         char(4),
            @OperatingSaturdayStartTime     char(4),
            @OperatingSaturdayEndTime       char(4),
            @OperatingSundayStartTime       char(4),
            @OperatingSundayEndTime         char(4),
            @TodayStartTime                 char(5),
            @TodayEndTime                   char(5),
            @DOW                            int,
            @TimeAtShop                     datetime,
            @ShopTime                       datetime

	-- 16Dec2015 - TVD - Choice Process
    DECLARE @PriImpact						INT
    DECLARE @SecImpact						INT
    DECLARE @InsuredFirstName               VARCHAR(100)
    DECLARE @InsuredLastName                VARCHAR(100)
	DECLARE @InsuredAddress1				VARCHAR(250)
	DECLARE @InsuredAddress2				VARCHAR(250)
	DECLARE @InsuredCity					VARCHAR(50)
	DECLARE @InsuredState					VARCHAR(2)
	DECLARE @InsuredZip						VARCHAR(10)
	DECLARE @InsuredEmailAddress			VARCHAR(50)
	DECLARE @InsuredPhoneNumber				VARCHAR(15)
	DECLARE @InsuredCellPhone				VARCHAR(15)
	DECLARE @InsuredWorkPhone				VARCHAR(15)
	DECLARE @OwnerPhoneNumber				VARCHAR(15)
	DECLARE @ClaimRepComments				VARCHAR(500)
	DECLARE @LossDescription				VARCHAR(500)
	DECLARE @ShopComments					VARCHAR(500)

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspHyperquestVehicleAssignGetDetailWSXML'
    
    SET @RentalCoverage = 0


    -- Check to make sure a valid Claim Aspect Service Channel ID was passed in

    IF  (@ClaimAspectServiceChannelID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
    BEGIN
        -- Invalid Claim Aspect Service Channel ID
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END


    -- Get the Insurance Company Id for the claim
    
    SELECT  @InsuranceCompanyIDClaim = c.InsuranceCompanyID,
            @InsuranceCompanyName = i.Name,
            @ServiceChannelCD = casc.ServiceChannelCD,
            @LynxID = ca.LynxID,
            @ClaimAspectNumber = ca.ClaimAspectNumber,
            @ClaimAspectID = ca.ClaimAspectID,
            @ExposureCD = ca.ExposureCD,
            @RepairLocationCity = RepairLocationCity,
            @RepairLocationCounty = RepairLocationCounty,
            @RepairLocationState = RepairLocationState,
            @InitialAssignmentTypeID = ca.InitialAssignmentTypeID,
            @SourceApplicationPassThruData = ca.SourceApplicationPassThruData,
            @ClaimRepComments = ISNULL(c.remarks,''),
            @LossDescription = ISNULL(c.LossDescription ,'')

      FROM  dbo.utb_claim_aspect_service_channel casc
      LEFT JOIN dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    IF EXISTS(SELECT ClaimCoverageID
               FROM dbo.utb_claim_coverage
               WHERE LynxID = @LynxID
                 AND CoverageTypeCD = 'RENT'
                 AND EnabledFlag = 1)
    BEGIN
      SET @RentalCoverage = 1
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END
    
    /*-- Check if the claim aspect service channel has a status record
    IF NOT EXISTS(SELECT ClaimAspectStatusID
                   FROM dbo.utb_claim_aspect_service_channel casc
                   LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
                   LEFT JOIN dbo.utb_claim_aspect_status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                  WHERE cas.ServiceChannelCD = casc.ServiceChannelCD
                    AND casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                    AND cas.StatusTypeCD = 'SC')
    BEGIN
        -- Invalid data state. Claim aspect service channel does not have a status.
    
        RAISERROR('111|%s|ClaimAspectServiceChannelID %u does not have status information', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END*/


    -- Get Claim Aspect for Assignment

    SELECT  @ClaimAspectTypeIDAssignment = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Assignment'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDAssignment IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Assignment"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Get Claim Aspect for Fax

    SELECT  @ClaimAspectTypeIDFax = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Fax Assignment'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDFax IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Fax Assignment"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Get Shop Manager Personnel Type ID

    SELECT  @Business1PersonnelTypeID = PersonnelTypeID
      FROM  dbo.utb_personnel_type
      WHERE Name = 'Shop Manager'     -- personnel associated with utb_shop_location

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)

        RETURN
    END

    IF @Business1PersonnelTypeID IS NULL
    BEGIN
       -- Personnel Type Not Found
    
        RAISERROR('102|%s|"Shop Manager"|utb_personnel_type', 16, 1, @ProcName)
        RETURN
    END


    /*************************************************************************************
    *  Gather metadata for all entities updatable columns
    **************************************************************************************/

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    --    Assignment
    
    INSERT INTO @tmpMetadata (GroupName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Assignment',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  (Table_Name = 'utb_assignment' 
      AND Column_Name IN('AssignmentRemarks',
                         'EffectiveDeductibleSentAmt')
                        )

    UNION ALL
    
    SELECT  'ServiceChannel',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  (Table_Name = 'utb_claim_aspect_service_channel' 
      AND Column_Name IN('AppraiserInvoiceDate',
                         'AppraiserPaidDate',
                         'CashOutDate',
                         'ClientInvoiceDate',
                         'InspectionDate',
                         'WorkEndDate',
                         'WorkEndDateOriginal',
                         'WorkStartDate',
                         'DispositionTypeCD')
                        )

    UNION ALL
    
    SELECT  'Root',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  (Table_Name = 'utb_claim_aspect_service_channel' 
      AND Column_Name IN('RepairLocationCity',
                         'RepairLocationCounty',
                         'RepairLocationState')
                        )
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    /*-- Get the claim aspect information 
    
    SELECT  @LynxID = ca.LynxID,
            @ClaimAspectNumber = ca.ClaimAspectNumber,
            @ClaimAspectID = ca.ClaimAspectID
      FROM  dbo.utb_claim_aspect_service_channel casc
      LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END*/

   
    -- Get information on the LYNX Desk audit unit so we can distinguish assignments to it versus regular assignments

    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Get information on the LYNX Mobile Electronics so we can distinguish assignments to it versus regular assignments

    SELECT  @MobileElectronicsAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @MobileElectronicsID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'Mobile_Electronics_AutoID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Get information on the LYNX AGC so we can distinguish assignments to it versus regular assignments

    SELECT  @GlassAppraiserID = LTrim(RTrim(value))
      FROM  dbo.utb_app_variable
      WHERE Name = 'AGC_Appraiser_ID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    --   Get all assignments for this vehicle
    
    INSERT INTO @tmpMultipleAssignments   
      SELECT  a.AssignmentID,
              CASE
                WHEN ((a.ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
                        ((a.AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A')) THEN 'LDAU'
                WHEN a.ShopLocationID IS NOT NULL THEN 'SHOP'
                WHEN a.AppraiserID IS NOT NULL THEN 'IA'
                ELSE NULL
              END,
              ca.InitialAssignmentTypeID,
              a.AssignmentSequenceNumber,
              a.AppraiserID,
              a.ShopLocationID,
              CASE
                WHEN ((a.ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
                        ((a.AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A')) THEN 'LYNX Desk Audit Unit'
                WHEN a.ShopLocationID IS NOT NULL THEN sl.Name
                WHEN a.AppraiserID IS NOT NULL THEN ap.Name
                ELSE NULL
              END,                
              a.AssignmentDate,
              a.CancellationDate,
              a.SelectionDate,
              a.CommunicationMethodID,
              a.ProgramTypeCD,
              a.SysLastUpdatedDate
        FROM  dbo.utb_assignment a
        LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
        LEFT JOIN dbo.utb_appraiser ap ON (a.AppraiserID = ap.AppraiserID)
    	/*********************************************************************************
    	Project: 210474 APD - Enhancements to support multiple concurrent service channels
    	Note:	Added reference to utb_Claim_Aspect_Service_Channel to provide a join
                between utb_Assignment and utb_Claim_Aspect
    		    M.A. 20061120
    	*********************************************************************************/
        LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_claim_aspect ca ON (ca.ClaimAspectID = casc.ClaimAspectID)
        WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
         -- Insertion failure
    
         RAISERROR('105|%s|@tmpMultipleAssignments', 16, 1, @ProcName)
         RETURN
    END


    -- Get the active assignment (if there is one)
    -- Changed to get the active shop assignment and active DA assignment
    
    SELECT  @ActiveShopAssignmentID = AssignmentID
            -- @ActiveAssignmentTypeCD = AssignmentTypeCD
      FROM  @tmpMultipleAssignments
      WHERE -- AssignmentSeqNumber = @AssignmentSequenceNumber AND
         CancellationDate IS NULL AND AssignmentTypeCD = 'SHOP'
         
    SELECT  @ActiveDAAssignmentID = AssignmentID
            -- @ActiveAssignmentTypeCD = AssignmentTypeCD
      FROM  @tmpMultipleAssignments
      WHERE -- AssignmentSeqNumber = @AssignmentSequenceNumber AND
         CancellationDate IS NULL AND AssignmentTypeCD = 'LDAU'
    
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    IF @ActiveShopAssignmentID IS NOT NULL
    BEGIN
        --IF @ActiveAssignmentTypeCD = 'SHOP'
        --BEGIN
            -- Get the hours for the shop 

            SELECT  @ShopLocationID = sl.ShopLocationID,
                    @TimeAtShop = dbo.ufnUtilityGetLocalTime(GetUTCDate(), sl.AddressZip),
                    @DOW = datepart(dw, dbo.ufnUtilityGetLocalTime(GetUTCDate(), sl.AddressZip)),
                    @OperatingMondayStartTime = slh.OperatingMondayStartTime,
                    @OperatingMondayEndTime = slh.OperatingMondayEndTime,
                    @OperatingTuesdayStartTime = slh.OperatingTuesdayStartTime,
                    @OperatingTuesdayEndTime = slh.OperatingTuesdayEndTime,
                    @OperatingWednesdayStartTime = slh.OperatingWednesdayStartTime,
                    @OperatingWednesdayEndTime = slh.OperatingWednesdayEndTime,
                    @OperatingThursdayStartTime = slh.OperatingThursdayStartTime,
                    @OperatingThursdayEndTime = slh.OperatingThursdayEndTime,
                    @OperatingFridayStartTime = slh.OperatingFridayStartTime,
                    @OperatingFridayEndTime = slh.OperatingFridayEndTime,
                    @OperatingSaturdayStartTime = slh.OperatingSaturdayStartTime,
                    @OperatingSaturdayEndTime = slh.OperatingSaturdayEndTime,
                    @OperatingSundayStartTime = slh.OperatingSundayStartTime,
                    @OperatingSundayEndTime = slh.OperatingSundayEndTime,
                    @RepairLocationCity = sl.AddressCity,
                    @RepairLocationCounty = sl.AddressCounty,
                    @RepairLocationState = sl.AddressState
              FROM  dbo.utb_assignment a
              LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
              LEFT JOIN dbo.utb_shop_location_hours slh ON (a.ShopLocationID = slh.ShopLocationID)
              WHERE a.AssignmentID = @ActiveShopAssignmentID
        
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END


            -- Strip day off of @TimeAtShop

            SET @ShopTime = Convert(datetime, Convert(char(2), datepart(hour, @TimeAtShop)) + ':' +
                            Convert(char(2), datepart(minute, @TimeAtShop)))


            -- Find the start and end times for today
            SELECT  @TodayStartTime = 
                CASE @DOW
                    WHEN 1 THEN Left(@OperatingSundayStartTime, 2) + ':' + Right(@OperatingSundayStartTime, 2)
                    WHEN 2 THEN Left(@OperatingMondayStartTime, 2) + ':' + Right(@OperatingMondayStartTime, 2)
                    WHEN 3 THEN Left(@OperatingTuesdayStartTime, 2) + ':' + Right(@OperatingTuesdayStartTime, 2)
                    WHEN 4 THEN Left(@OperatingWednesdayStartTime, 2) + ':' + Right(@OperatingWednesdayStartTime, 2)
                    WHEN 5 THEN Left(@OperatingThursdayStartTime, 2) + ':' + Right(@OperatingThursdayStartTime, 2)
                    WHEN 6 THEN Left(@OperatingFridayStartTime, 2) + ':' + Right(@OperatingFridayStartTime, 2)
                    WHEN 7 THEN Left(@OperatingSaturdayStartTime, 2) + ':' + Right(@OperatingSaturdayStartTime, 2)
                END,
                    @TodayEndTime = 
                CASE @DOW
                    WHEN 1 THEN Left(@OperatingSundayEndTime, 2) + ':' + Right(@OperatingSundayEndTime, 2)
                    WHEN 2 THEN Left(@OperatingMondayEndTime, 2) + ':' + Right(@OperatingMondayEndTime, 2)
                    WHEN 3 THEN Left(@OperatingTuesdayEndTime, 2) + ':' + Right(@OperatingTuesdayEndTime, 2)
                    WHEN 4 THEN Left(@OperatingWednesdayEndTime, 2) + ':' + Right(@OperatingWednesdayEndTime, 2)
                    WHEN 5 THEN Left(@OperatingThursdayEndTime, 2) + ':' + Right(@OperatingThursdayEndTime, 2)
                    WHEN 6 THEN Left(@OperatingFridayEndTime, 2) + ':' + Right(@OperatingFridayEndTime, 2)
                    WHEN 7 THEN Left(@OperatingSaturdayEndTime, 2) + ':' + Right(@OperatingSaturdayEndTime, 2)
                END

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
        
                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END


            -- It is possible if the Start and End Times are Null, for @TodayStartTime and @TodayEndTime to contain an invalid
            -- value (basically, simply a ':'.)  If this is so, we need to treat these as NULL so that the processing of 
            -- of the next block is performed correctly.

            IF IsDate(@TodayStartTime) = 0
            BEGIN
                SET @TodayStartTime = NULL
            END

            IF IsDate(@TodayEndTime) = 0
            BEGIN
                SET @TodayEndTime = NULL
            END


            -- Now compare to the shops current time
        
            IF (@TodayStartTime IS NULL) OR (@TodayEndTime IS NULL)
            BEGIN
                -- Null values in times are unknown (unless @TodayFlag = 0 (closed), which is handled above) 
                SET @ShopOpen = 'Unknown'
       	    END
            ELSE
            BEGIN
                -- We're now guaranteed values in the start and end times.  It is necessary to make the "Between" check
                -- afterward because "Between" does not handle NULL values properly
                IF (@ShopTime BETWEEN Convert(datetime, @TodayStartTime) AND Convert(datetime, @TodayEndTime))
                BEGIN
                    SET @ShopOpen = 'Open'
                END
                ELSE    
                BEGIN
                    SET @ShopOpen = 'Closed'
                END
            END

        
            -- Get the last time previous to this that the shop was sent an assignment
        
            SELECT TOP 1 @LastAssignedDate = a.AssignmentDate
              FROM  dbo.utb_assignment a
            	/*********************************************************************************
            	Project: 210474 APD - Enhancements to support multiple concurrent service channels
            	Note:	Added reference to utb_Claim_Aspect_Service_Channel to get the value of
                        ClaimAspectID
            		    M.A. 20061120
            	*********************************************************************************/
              INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
              WHERE a.ShopLocationID = @ShopLocationID
                AND casc.ClaimAspectServiceChannelID <> @ClaimAspectServiceChannelID
                AND a.AssignmentDate IS NOT NULL
              ORDER BY a.AssignmentID Desc
        --END
	END
	IF @ActiveDAAssignmentID IS NOT NULL
    BEGIN
        --ELSE
        --BEGIN
            SELECT  @AppraiserID = AppraiserID                   
              FROM  @tmpMultipleAssignments
              WHERE AssignmentID = @ActiveDAAssignmentID
        
            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END


            -- Get the last time previous to this that the appraiser was sent an assignment
        
            SELECT TOP 1 @LastAssignedDate = a.AssignmentDate
              FROM  dbo.utb_assignment a
            	/*********************************************************************************
            	Project: 210474 APD - Enhancements to support multiple concurrent service channels
            	Note:	Added reference to utb_Claim_Aspect_Service_Channel to get the value of
                        ClaimAspectID
            		    M.A. 20061120
            	*********************************************************************************/
              INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
              WHERE a.AppraiserID = @AppraiserID
                AND casc.ClaimAspectServiceChannelID <> @ClaimAspectServiceChannelID
                AND a.AssignmentDate IS NOT NULL
              ORDER BY a.AssignmentID Desc
        --END
                
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END


    -- Get last assignment remarks
    
    SELECT TOP 1 @AssignmentRemarks = a.AssignmentRemarks
      FROM  dbo.utb_assignment a
    	/*********************************************************************************
    	Project: 210474 APD - Enhancements to support multiple concurrent service channels
    	Note:	Added reference to utb_Claim_Aspect_Service_Channel to get the value of
                ClaimAspectID
    		    M.A. 20061120
    	*********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        AND a.AssignmentSequenceNumber = 1
      ORDER BY a.SelectionDate DESC

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET @ActiveReinspection = 0

    /*
      TODO - Need to verify if the reinspection can be performed on a document on a specific service channel. if not, a request
             on one service channel can set the @ActiveReinspection flag incorrectly for another service channel of the same
             vehicle.
             01/04/07 - JP was going to verify with business about the scope of this.
    */
    -- See if there is a reinspection request out there for the claim aspect
    IF EXISTS(SELECT d.DocumentID
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                  AND casc.EnabledFlag = 1--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1) OR -- any active reinspection request
       EXISTS(SELECT ReinspectID
                FROM dbo.utb_reinspect r
                LEFT JOIN dbo.utb_claim_aspect_service_channel casc on (r.ClaimAspectID = casc.ClaimAspectID)
                WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                  AND r.LockedFlag = 0
                  AND r.EnabledFlag = 1) -- reinspection requested and has not been completed
    BEGIN
        SET @ActiveReinspection = 1
    END
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the insured/claimant name
    SELECT @ClaimantName =        IsNull((SELECT Top 1 i.NameFirst + ' ' + i.NameLast
                                           FROM dbo.utb_claim_aspect cas
                                           LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                                           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                           WHERE cas.ClaimAspectID = @ClaimAspectID
                                             AND cai.EnabledFlag = 1
                                             AND irt.Name = 'Claimant'), ''),
           @ClaimantBusinessName = IsNull((SELECT Top 1 i.BusinessName
                                           FROM dbo.utb_claim_aspect cas
                                           LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                                           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                           WHERE cas.ClaimAspectID = @ClaimAspectID
                                             AND cai.EnabledFlag = 1
                                             AND irt.Name = 'Claimant'), ''),
           @InsuredName =          IsNull((SELECT Top 1 i.NameFirst + ' ' + i.NameLast
                                           FROM dbo.utb_claim_aspect cas
                                           LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                                           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                           WHERE cas.ClaimAspectID = @ClaimAspectID
                                             AND cai.EnabledFlag = 1
                                             AND irt.Name = 'Insured'), ''),
           @InsuredBusinessName = IsNull((SELECT Top 1 i.BusinessName
                                           FROM dbo.utb_claim_aspect cas
                                           LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                                           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                           WHERE cas.ClaimAspectID = @ClaimAspectID
                                             AND cai.EnabledFlag = 1
                                             AND irt.Name = 'Insured'), '')
    IF @ExposureCD = '1' 
    BEGIN
      IF @InsuredBusinessName <> '' 
      BEGIN
         SET @InsuredClaimantName = @InsuredBusinessName
      END
      ELSE
      BEGIN
         SET @InsuredClaimantName = @InsuredName
      END
    END

    IF @ExposureCD = '3' 
    BEGIN
      IF @ClaimantBusinessName <> '' 
      BEGIN
         SET @InsuredClaimantName = @ClaimantBusinessName
      END
      ELSE
      BEGIN
         SET @InsuredClaimantName = @ClaimantName
      END
    END
    
    SELECT @ClaimOwnerFirstName = u.NameFirst,
		   @ClaimOwnerLastName = u.NameLast,
		   @ClaimOwnerUserID = u.UserID,
		   @ClaimOwnerLogonID = ua.LogonID,
		   @ClaimOwnerEmail = u.EmailAddress,
           @ClaimOwnerPhone = u.PhoneAreaCode + '-' + u.PhoneExchangeNumber + '-' + u.PhoneUnitNumber
    FROM utb_claim_aspect ca
    LEFT JOIN utb_user u ON ca.OwnerUserID = u.UserID
    LEFT JOIN utb_user_application ua ON ca.OwnerUserID = ua.UserID
    WHERE ca.ClaimAspectID = @ClaimAspectID
    
    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName              varchar(50) NOT NULL,
        DisplayOrder          int         NULL,  
        ReferenceID           varchar(10) NOT NULL,
        Name                  varchar(1000) NULL,
        SystemName            varchar(50) NULL,
        ClientCode            varchar(10) NULL,
        ClientCoverageTypeID  int         NULL,
        DeductibleAmt         decimal(9,2) NULL,
        LimitAmt              decimal(9,2) NULL
    )
    
    
    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table    

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, SystemName, ClientCoverageTypeID, DeductibleAmt, LimitAmt, ClientCode)
    
    SELECT  'ClaimCoverage' AS ListName,
            NULL,
            CAST(ClaimCoverageID AS Varchar),
            isNULL(Description, ''),
            (select Name 
               from dbo.ufnUtilityGetReferenceCodes('utb_claim_coverage', 'CoverageTypeCD') 
               where Code = CoverageTypeCD),
            ClientCoverageTypeID,
            DeductibleAmt,
            LimitAmt,
            NULL
            
    FROM    utb_claim_coverage
    WHERE   LynxID = 1600116
      AND   EnabledFlag = 1
    
    UNION ALL
    
    SELECT 'ClientCoverageType',
           DisplayOrder,
           CAST(ClientCoverageTypeID AS Varchar),
           isNull(Name, ''),
           NULL,
           NULL,
           NULL,
           NULL,
           isNull(ClientCode, '')
           
    FROM dbo.utb_client_coverage_type
    WHERE InsuranceCompanyID = @InsuranceCompanyID
    
    
    UNION ALL
    
    SELECT 'Appraisers',
           NULL,
           CAST(AppraiserID AS Varchar),
           Name,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL
    FROM dbo.utb_appraiser
    WHERE EnabledFlag = 1

    UNION ALL
    
    SELECT 'DispositionCD',
           NULL,
           Code,
           Name,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL
    FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'DispositionTypeCD') 

    UNION ALL
    
    SELECT 'ShopRemarkTemplates',
           NULL,
           CAST(MessageTemplateID AS Varchar),
           Description,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL
    FROM dbo.utb_message_template
    WHERE ServiceChannelCD = @ServiceChannelCD
      AND AppliesToCD = 'S'
      AND EnabledFlag = 1

    UNION ALL
    
    SELECT 'StateList',
           NULL,
           StateCode,
           StateValue,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL
    FROM dbo.utb_state_code
    WHERE EnabledFlag = 1
        
    DECLARE @tmpCurEstimate TABLE
    (
        DocumentID            bigint      NOT NULL,
        EstimateSummaryID     bigint      NOT NULL,        
        OriginalExtendedAmt   decimal(9,2) NULL,
        EstimateSummaryTypeIDDesc varchar(50) NULL,
        CategoryCD            varchar(5)  NULL
    )
    
    -- Get the Current Estimate Details
    INSERT INTO @tmpCurEstimate
    (DocumentID, EstimateSummaryID, OriginalExtendedAmt, EstimateSummaryTypeIDDesc, CategoryCD)
    SELECT DocumentID, EstimateSummaryID, OriginalExtendedAmt, EstimateSummaryTypeIDDesc, CategoryCD
    FROM dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', null, null) ufnEst 
    WHERE ufnEst.ServiceChannelCD = @ServiceChannelCD
    
    -- Get the current estimate gross amount
    SELECT @CurEstGrossAmt = OriginalExtendedAmt
    FROM @tmpCurEstimate
    WHERE EstimateSummaryTypeIDDesc = 'RepairTotal'
      AND CategoryCD = 'TT'
    
    -- Get the current estimate deductibles applied amount
    SELECT @DeductiblesApplied = OriginalExtendedAmt
    FROM @tmpCurEstimate
    WHERE EstimateSummaryTypeIDDesc = 'DeductiblesApplied'
      AND CategoryCD = 'OT'

    -- Get the current estimate Limits applied
    SELECT @LimitsEffect = OriginalExtendedAmt
    FROM @tmpCurEstimate
    WHERE EstimateSummaryTypeIDDesc = 'LimitEffect'
      AND CategoryCD = 'OT'

    -- Get the current estimate net amount
    SELECT @NetTotalEffect = OriginalExtendedAmt
    FROM @tmpCurEstimate
    WHERE EstimateSummaryTypeIDDesc = 'NetTotalEffect'
      AND CategoryCD = 'OT'

    -- Get the current estimate Document id
    SELECT top 1 @CurEstDocumentID = DocumentID
    FROM @tmpCurEstimate
    
    
    -- Get the Original Estimate amount
    SELECT @OriginalEstAmt = OriginalExtendedAmt,
           @OriginalEstDate = CreatedDate
    FROM dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'O', 'RepairTotal', 'TT') ufnEst 
    WHERE ufnEst.ServiceChannelCD = @ServiceChannelCD
    
    -- Get the loss date
    SELECT @LossDate = LossDate
    FROM dbo.utb_claim
    WHERE LynxID = @LynxID
    
    -- Get the Primary Impact Point
    SELECT TOP 1 @PriImpact = ImpactID
    FROM dbo.utb_vehicle_impact
    WHERE 
		PrimaryImpactFlag = 1
		AND ClaimAspectID = @ClaimAspectID
	ORDER BY
		SysLastUpdatedDate DESC

    -- Get the Secondary Impact Point
    SELECT TOP 1 @SecImpact = ImpactID
    FROM dbo.utb_vehicle_impact
    WHERE 
		PrimaryImpactFlag = 0
		AND ClaimAspectID = @ClaimAspectID
	ORDER BY
		SysLastUpdatedDate DESC
		
    -- Clear the temp table
    DELETE FROM @tmpCurEstimate
    
    IF @InitialAssignmentTypeID = 15
    BEGIN
        DECLARE @PursuitAuditShopInfo TABLE
        (
            Position    int,
            Value       varchar(1000)
        )
        
        SET @delimiter = char(255)
        
        INSERT INTO @PursuitAuditShopInfo
        SELECT strIndex, Value
        FROM dbo.ufnUtilityParseString(@SourceApplicationPassThruData, @delimiter, 1)
        
        IF (SELECT Count(*) FROM @PursuitAuditShopInfo) = 10
        BEGIN
            SELECT @RepairLocationName = Value FROM @PursuitAuditShopInfo WHERE Position = 1
            SELECT @RepairLocationAddress = Value FROM @PursuitAuditShopInfo WHERE Position = 2
            SELECT @RepairLocationCity = Value FROM @PursuitAuditShopInfo WHERE Position = 4
            SELECT @RepairLocationState = Value FROM @PursuitAuditShopInfo WHERE Position = 5
            SELECT @RepairLocationZip = Value FROM @PursuitAuditShopInfo WHERE Position = 3
            SELECT @RepairLocationPhone = Value FROM @PursuitAuditShopInfo WHERE Position = 7
            SELECT @RepairLocationFax = Value FROM @PursuitAuditShopInfo WHERE Position = 8
            SELECT @RepairLocationEmail = Value FROM @PursuitAuditShopInfo WHERE Position = 9
            SELECT @RepairInspectionDate = Value FROM @PursuitAuditShopInfo WHERE Position = 10

        END
        
    END

	-- 28Dec2015 - Choice Process - Insured Details
	SELECT TOP 1 
		@InsuredFirstName = ISNULL(i.NameFirst,'Unknown') 
		, @InsuredLastName = ISNULL(i.NameLast,'Unknown')
		, @InsuredAddress1 = ISNULL(i.Address1,'')
		, @InsuredAddress2 = ISNULL(i.Address2,'')
		, @InsuredCity = ISNULL(i.AddressCity,'')
		, @InsuredState = ISNULL(i.AddressState,'')
		, @InsuredZip = ISNULL(i.AddressZip,'')
		, @InsuredEmailAddress = ISNULL(i.EmailAddress,'')
		, @InsuredPhoneNumber = CASE i.BestContactPhoneCD
			WHEN 'D' THEN 
				i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber
			WHEN 'N' THEN 
				i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber
			ELSE
				i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber
		  END
		, @InsuredCellPhone = CASE i.BestContactPhoneCD
			WHEN 'C' THEN 
				i.CellAreaCode + i.CellExchangeNumber + i.CellUnitNumber
			ELSE ''
		  END
		, @InsuredWorkPhone = CASE i.BestContactPhoneCD
			WHEN 'A' THEN 
				i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber
			ELSE ''
		  END
	FROM 
		dbo.utb_claim_aspect cas
		LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
		LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
		LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
		LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
	WHERE 
		cas.ClaimAspectID = @ClaimAspectID
		AND cai.EnabledFlag = 1
		AND irt.Name = 'Insured'

	-- 28Dec2015 - Choice Process - Owner Details
	SELECT TOP 1 
		@OwnerPhoneNumber = CASE i.BestContactPhoneCD
			WHEN 'A' THEN 
				i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber
			WHEN 'D' THEN 
				i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber
			WHEN 'N' THEN 
				i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber
			WHEN 'C' THEN 
				i.CellAreaCode + i.CellExchangeNumber + i.CellUnitNumber
			ELSE
				i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber
		  END
	FROM 
		dbo.utb_claim_aspect cas
		LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
		LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
		LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
		LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
	WHERE 
		cas.ClaimAspectID = @ClaimAspectID
		AND cai.EnabledFlag = 1
		AND irt.Name = 'Owner'
                                     
    -- Select Root Level

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @LynxID                          AS [Root!1!LynxID],
            @ClaimAspectID                   AS [Root!1!ClaimAspectID],
            @InsuranceCompanyID              AS [Root!1!InsuranceCompanyID],
            @InsuranceCompanyName            AS [Root!1!InsuranceCompanyName],
            @DeskAuditAppraiserType          AS [Root!1!LDAUAppraiserTypeCD],
            @DeskAuditID                     AS [Root!1!LDAUID],
            @MobileElectronicsAppraiserType  AS [Root!1!MEAppraiserTypeCD],
            @MobileElectronicsID             AS [Root!1!MEID],
            @GlassAppraiserID                AS [Root!1!GLID],
            @ActiveReinspection              AS [Root!1!ActiveReinspection],
            isNull(dbo.ufnUtilityGetDateString(@LossDate), '') AS [Root!1!LossDate],
            @ClaimOwnerFirstName             AS [Root!1!ClaimOwnerFirstName],
            @ClaimOwnerLastName              AS [Root!1!ClaimOwnerLastName],
            @ClaimOwnerUserID                AS [Root!1!ClaimOwnerUserID],
            @ClaimOwnerLogonID               AS [Root!1!ClaimOwnerLogonID],
            @ClaimOwnerPhone                 AS [Root!1!ClaimOwnerPhone],
            @ClaimOwnerEmail                 AS [Root!1!ClaimOwnerEmail],
            @ClaimRepComments                AS [Root!1!ClaimRepComments],
			@LossDescription                 AS [Root!1!LossDescription],

            @InsuredFirstName		         AS [Root!1!InsuredFirstName],
            @InsuredLastName		         AS [Root!1!InsuredLastName],
			@InsuredAddress1		         AS [Root!1!InsuredAddress1],
			@InsuredAddress2		         AS [Root!1!InsuredAddress2],
			@InsuredCity					 AS [Root!1!InsuredCity],
			@InsuredState					 AS [Root!1!InsuredState],
			@InsuredZip						 AS [Root!1!InsuredZip],
			@InsuredEmailAddress		     AS [Root!1!InsuredEmailAddress],
			@InsuredPhoneNumber			     AS [Root!1!InsuredPhoneNumber],
			@InsuredCellPhone			     AS [Root!1!InsuredCellPhone],
			@InsuredWorkPhone			     AS [Root!1!InsuredWorkPhone],
			
            @RepairLocationCity              AS [Root!1!RepairLocationCity],
            @RepairLocationCounty            AS [Root!1!RepairLocationCounty],
            @RepairLocationState             AS [Root!1!RepairLocationState],
            @RepairLocationName              AS [Root!1!RepairLocationName],
            @RepairLocationAddress           AS [Root!1!RepairLocationAddress],
            @RepairLocationZip               AS [Root!1!RepairLocationZip],
            @RepairLocationPhone             AS [Root!1!RepairLocationPhone],
            @RepairLocationFax               AS [Root!1!RepairLocationFax],
            @RepairLocationEmail             AS [Root!1!RepairLocationEmail],
            @RepairInspectionDate            AS [Root!1!RepairLocationInspectionDate],
            -- Claim Aspect Service Channel  Data
            NULL as [ServiceChannel!2!ClaimAspectID],
            NULL as [ServiceChannel!2!ClaimAspectServiceChannelID],
            NULL as [ServiceChannel!2!ServiceChannelCD],
            NULL as [ServiceChannel!2!AppraiserInvoiceDate],
            NULL as [ServiceChannel!2!AppraiserPaidDate],
            NULL as [ServiceChannel!2!CashOutDate],
            NULL as [ServiceChannel!2!ClientInvoiceDate],
            NULL as [ServiceChannel!2!InspectionDate],
            NULL as [ServiceChannel!2!WorkEndConfirmFlag],
            NULL as [ServiceChannel!2!WorkEndDate],
            NULL as [ServiceChannel!2!WorkEndDateOriginal],
            NULL as [ServiceChannel!2!WorkStartConfirmFlag],
            NULL as [ServiceChannel!2!WorkStartDate],
            NULL as [ServiceChannel!2!OriginalEstAmt],
            NULL as [ServiceChannel!2!OriginalEstDate],
            NULL as [ServiceChannel!2!DispositionTypeCD],
            NULL AS [ServiceChannel!2!CurEstDocumentID],
            NULL AS [ServiceChannel!2!CurEstGrossRepairTotal],
            NULL AS [ServiceChannel!2!CurEstDeductiblesApplied],
            NULL AS [ServiceChannel!2!CurEstLimitsEffect],
            NULL AS [ServiceChannel!2!CurEstNetRepairTotal],
            NULL AS [ServiceChannel!2!FaxAssignmentStatus],
            -- Assignment Data
            NULL AS [Assignment!3!AssignmentID],
            NULL AS [Assignment!3!AssignmentTypeCD],
            NULL AS [Assignment!3!AssignmentTypeID],
            NULL AS [Assignment!3!VANAssignmentStatusID],
            NULL AS [Assignment!3!VANAssignmentStatusName],
            NULL AS [Assignment!3!FaxAssignmentStatusID],
            NULL AS [Assignment!3!FaxAssignmentStatusName],
            NULL AS [Assignment!3!AppraiserID],
            NULL AS [Assignment!3!ShopLocationID],
            NULL AS [Assignment!3!AssignmentDate],
            NULL AS [Assignment!3!CancellationDate],
            NULL AS [Assignment!3!SelectionDate],
            NULL AS [Assignment!3!CommunicationMethodName],
            NULL AS [Assignment!3!AssignmentRemarks],
            NULL AS [Assignment!3!ReferenceID],
            NULL AS [Assignment!3!WorkStatus],
            NULL AS [Assignment!3!EffectiveDeductibleSentAmt],
            NULL AS [Assignment!3!SysLastUpdatedDate],
            NULL AS [Assignment!3!AssignmentSequenceNumber],
            -- Shop
            NULL AS [Shop!4!ShopID],
            NULL AS [Shop!4!PreferredEstimatePackageID],
            NULL AS [Shop!4!BusinessID],
            NULL AS [Shop!4!Address1],
            NULL AS [Shop!4!Address2],
            NULL AS [Shop!4!AddressCity],
            NULL AS [Shop!4!AddressCounty],
            NULL AS [Shop!4!AddressState],
            NULL AS [Shop!4!AddressZip],
            NULL AS [Shop!4!EmailAddress],
            NULL AS [Shop!4!FaxAreaCode],
            NULL AS [Shop!4!FaxExchangeNumber],
            NULL AS [Shop!4!FaxExtensionNumber],
            NULL AS [Shop!4!FaxUnitNumber],
            NULL AS [Shop!4!LocalTime],
            NULL AS [Shop!4!Name],
            NULL AS [Shop!4!PhoneAreaCode],
            NULL AS [Shop!4!PhoneExchangeNumber],
            NULL AS [Shop!4!PhoneExtensionNumber],
            NULL AS [Shop!4!PhoneUnitNumber],
            NULL AS [Shop!4!ProgramType],
            NULL AS [Shop!4!WebSiteAddress],
            NULL AS [Shop!4!OperatingFridayEndTime],
            NULL AS [Shop!4!OperatingFridayStartTime],
            NULL AS [Shop!4!OperatingMondayEndTime],
            NULL AS [Shop!4!OperatingMondayStartTime],
            NULL AS [Shop!4!OperatingSaturdayEndTime],
            NULL AS [Shop!4!OperatingSaturdayStartTime],
            NULL AS [Shop!4!OperatingSundayEndTime],
            NULL AS [Shop!4!OperatingSundayStartTime],
            NULL AS [Shop!4!OperatingThursdayEndTime],
            NULL AS [Shop!4!OperatingThursdayStartTime],
            NULL AS [Shop!4!OperatingTuesdayEndTime],
            NULL AS [Shop!4!OperatingTuesdayStartTime],
            NULL AS [Shop!4!OperatingWednesdayEndTime],
            NULL AS [Shop!4!OperatingWednesdayStartTime],
            NULL AS [Shop!4!ShopOpen],
            NULL AS [Shop!4!LastAssignedDate],
            NULL AS [Shop!4!MSACode],
            -- Shop Contact
            NULL AS [ShopContact!5!Name],
            NULL AS [ShopContact!5!PhoneAreaCode],
            NULL AS [ShopContact!5!PhoneExchangeNumber],
            NULL AS [ShopContact!5!PhoneExtensionNumber],
            NULL AS [ShopContact!5!PhoneUnitNumber],
            -- Appraiser
            NULL AS [Appraiser!6!AppraiserID],
            NULL AS [Appraiser!6!Address1],
            NULL AS [Appraiser!6!Address2],
            NULL AS [Appraiser!6!AddressCity],
            NULL AS [Appraiser!6!AddressCounty],
            NULL AS [Appraiser!6!AddressState],
            NULL AS [Appraiser!6!AddressZip],
            NULL AS [Appraiser!6!EmailAddress],
            NULL AS [Appraiser!6!FaxAreaCode],
            NULL AS [Appraiser!6!FaxExchangeNumber],
            NULL AS [Appraiser!6!FaxExtensionNumber],
            NULL AS [Appraiser!6!FaxUnitNumber],
            NULL AS [Appraiser!6!Name],
            NULL AS [Appraiser!6!PhoneAreaCode],
            NULL AS [Appraiser!6!PhoneExchangeNumber],
            NULL AS [Appraiser!6!PhoneExtensionNumber],
            NULL AS [Appraiser!6!PhoneUnitNumber],
            NULL AS [Appraiser!6!WebSiteAddress],
            NULL AS [Appraiser!6!LastAssignedDate],
            -- Multiple Assignments
            NULL AS [Assignments!7!AssignmentID],
            NULL AS [Assignments!7!AssignmentTypeCD],
            NULL AS [Assignments!7!AppraiserName],
            NULL AS [Assignments!7!AssignmentDate],
            NULL AS [Assignments!7!CancellationDate],
            NULL AS [Assignments!7!SelectionDate],
            -- Coverages applied
            NULL AS [CoverageApplied!8!ClaimCoverageID],
            NULL AS [CoverageApplied!8!DeductibleAppliedAmt],
            NULL AS [CoverageApplied!8!LimitAppliedAmt],
            NULL AS [CoverageApplied!8!PartialCoverageFlag],
            NULL AS [CoverageApplied!8!SysLastUpdatedDate],
            -- Claim Coverages applied
            NULL AS [ClaimCoveragesApplied!9!ClaimAspectServiceChannelID],
            NULL AS [ClaimCoveragesApplied!9!ClaimCoverageID],
            NULL AS [ClaimCoveragesApplied!9!DeductibleAppliedAmt],
            NULL AS [ClaimCoveragesApplied!9!LimitAppliedAmt],
            NULL AS [ClaimCoveragesApplied!9!PartialCoverageFlag],
            NULL AS [ClaimCoveragesApplied!9!ServiceChannelName],
            NULL AS [ClaimCoveragesApplied!9!PertainsTo],
            -- Claim details
            NULL AS [Claim!10!CoverageClaimNumber],
            NULL AS [Claim!10!LossDate],
            NULL AS [Claim!10!LossDescription],
            NULL AS [Claim!10!DeductibleAmt],
            -- Vehicle details
            NULL AS [Vehicle!11!OwnerNameFirst],
            NULL AS [Vehicle!11!OwnerNameLast],
            NULL AS [Vehicle!11!VIN],
            NULL AS [Vehicle!11!VehicleNumber],
            NULL AS [Vehicle!11!VehicleYear],
            NULL AS [Vehicle!11!Make],
            NULL AS [Vehicle!11!Model],
            NULL AS [Vehicle!11!ContactBestPhoneCD],
            NULL AS [Vehicle!11!OwnerPhone],
            NULL AS [Vehicle!11!OwnerNightPhone],
            NULL AS [Vehicle!11!OwnerAltPhone],
            NULL AS [Vehicle!11!OwnerBestPhoneCD],
            NULL AS [Vehicle!11!Address1],
            NULL AS [Vehicle!11!AddressCity],
            NULL AS [Vehicle!11!AddressState],
            NULL AS [Vehicle!11!AddressZip],
            NULL AS [Vehicle!11!OwnerEmail],
            NULL AS [Vehicle!11!DriveableFlag],
            NULL AS [Vehicle!11!RentalCoverage],
            NULL AS [Vehicle!11!InsuredClaimantName],
            NULL AS [Vehicle!11!PriImpact],
            NULL AS [Vehicle!11!SecImpact],
            NULL AS [Vehicle!11!ShopComments],
            -- MetaData
            Null as [Metadata!12!Entity],
            -- Columns
            Null as [Column!13!Name],
            Null as [Column!13!DataType],
            Null as [Column!13!MaxLength],
            NULL AS [Column!13!Precision],
            NULL AS [Column!13!Scale],
            Null as [Column!13!Nullable],
            -- Reference Data
            NULL AS [Reference!14!List],
            NULL AS [Reference!14!ReferenceID],
            NULL AS [Reference!14!Name],
            NULL AS [Reference!14!SystemName],
            NULL AS [Reference!14!ClientCode],
            NULL AS [Reference!14!DisplayOrder],
            NULL AS [Reference!14!ClientCoverageTypeID],
            NULL AS [Reference!14!DeductibleAmt],
            NULL AS [Reference!14!LimitAmt]
       

    UNION ALL


    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            casc.ClaimAspectID,
            casc.ClaimAspectServiceChannelID,
            casc.ServiceChannelCD,
            isNull(dbo.ufnUtilityGetDateString(casc.AppraiserInvoiceDate), ''),
            isNull(dbo.ufnUtilityGetDateString(casc.AppraiserPaidDate), ''),
            isNull(dbo.ufnUtilityGetDateString(casc.CashOutDate), ''),
            isNull(dbo.ufnUtilityGetDateString(casc.ClientInvoiceDate), ''),
            isNull(dbo.ufnUtilityGetDateString(casc.InspectionDate), ''),
            casc.WorkEndConfirmFlag,
            isNull(dbo.ufnUtilityGetDateString(casc.WorkEndDate), ''),
            isNull(dbo.ufnUtilityGetDateString(casc.WorkEndDateOriginal), ''),
            casc.WorkStartConfirmFlag,
            isNull(dbo.ufnUtilityGetDateString(casc.WorkStartDate), ''),
            isNull(convert(varchar, @OriginalEstAmt), ''),
            isNull(dbo.ufnUtilityGetDateString(@OriginalEstDate), ''),
            isNull(casc.DispositionTypeCD, ''),
            IsNull(convert(varchar(10),@CurEstDocumentID), ''),
            ISNULL(convert(varchar(10),@CurEstGrossAmt), ''),
            ISNULL(convert(varchar(10),@DeductiblesApplied), ''),
            ISNULL(convert(varchar(10),@LimitsEffect), ''),
            ISNULL(convert(varchar(10),@NetTotalEffect), ''),
            NULL,/*(select s.Name
               from utb_claim_aspect_status cas
               left join utb_status s on cas.StatusID = s.StatusID
               where cas.ClaimAspectID = casc.ClaimAspectID
                 and cas.ServiceChannelCD = casc.ServiceChannelCD
                 AND cas.AssignmentSequenceNumber = @AssignmentSequenceNumber
                 and cas.StatusTypeCD = 'FAX'),*/
            -- Assignment
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Multiple Assignments
            NULL, NULL, NULL, NULL,NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM    utb_Claim_Aspect_Service_Channel casc 
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            

    UNION ALL


    -- Select the Assignment Level

    SELECT  3,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            IsNull(tmp.AssignmentID, ''),
            IsNull(tmp.AssignmentTypeCD, ''),
            tmp.AssignmentTypeID,
            IsNull((SELECT  st.StatusID 
                      FROM  dbo.utb_claim_aspect_service_channel casc
                      LEFT JOIN utb_claim_aspect ca on casc.claimaspectid = ca.claimaspectid
                      INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                      LEFT JOIN dbo.utb_status st ON (cas.StatusID = st.StatusID)
                      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND casc.ServiceChannelCD = cas.ServiceChannelCD
                        AND cas.AssignmentSequenceNumber = a.AssignmentSequenceNumber
                        AND cas.StatusTypeCD = 'ELC'), ''),
            IsNull((SELECT  st.Name 
                      FROM  dbo.utb_claim_aspect_service_channel casc
                      LEFT JOIN utb_claim_aspect ca on casc.claimaspectid = ca.claimaspectid
                      INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                      LEFT JOIN dbo.utb_status st ON (cas.StatusID = st.StatusID)
                      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND casc.ServiceChannelCD = cas.ServiceChannelCD
                        AND cas.AssignmentSequenceNumber = a.AssignmentSequenceNumber
                        AND cas.StatusTypeCD = 'ELC'), ''),
            IsNull((SELECT  st.StatusID 
                      FROM  dbo.utb_claim_aspect_service_channel casc
                      LEFT JOIN utb_claim_aspect ca on casc.claimaspectid = ca.claimaspectid
                      INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                      LEFT JOIN dbo.utb_status st ON (cas.StatusID = st.StatusID)
                      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND casc.ServiceChannelCD = cas.ServiceChannelCD
                        AND cas.AssignmentSequenceNumber = a.AssignmentSequenceNumber
                        AND cas.StatusTypeCD = 'FAX'), ''),
            IsNull((SELECT  st.Name 
                      FROM  dbo.utb_claim_aspect_service_channel casc
                      LEFT JOIN utb_claim_aspect ca on casc.claimaspectid = ca.claimaspectid
                      INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                      LEFT JOIN dbo.utb_status st ON (cas.StatusID = st.StatusID)
                      WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND casc.ServiceChannelCD = cas.ServiceChannelCD
                        AND cas.AssignmentSequenceNumber = a.AssignmentSequenceNumber
                        AND cas.StatusTypeCD = 'FAX'), ''),
            IsNull(Convert(varchar(10), tmp.AppraiserID), ''),
            IsNull(Convert(varchar(10), tmp.ShopLocationID), ''),
            IsNull(dbo.ufnUtilityGetDateString (tmp.AssignmentDate), ''),
            IsNull(dbo.ufnUtilityGetDateString (tmp.CancellationDate), ''),
            IsNull(dbo.ufnUtilityGetDateString (tmp.SelectionDate), ''),
            IsNull(cm.Name, ''),
            IsNull(@AssignmentRemarks, ''),
            IsNull(a.ReferenceID, ''),
            isNull((SELECT s.Name
                     FROM dbo.utb_claim_aspect_status cas
                     LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
                     WHERE cas.ClaimAspectID = ca.ClaimAspectID
                       AND cas.ServiceChannelCD = casc.ServiceChannelCD
                       AND cas.StatusTypeCD = 'SC'), ''),
            ISNULL(convert(varchar(10),a.EffectiveDeductibleSentAmt), ''),
            dbo.ufnUtilityGetDateString( tmp.SysLastUpdatedDate ),
            ISNULL(tmp.AssignmentSeqNumber, ''),
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Multiple Assignments
            NULL, NULL, NULL, NULL,NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM   -- (SELECT @ActiveAssignmentID AS AssignmentID) AS parms   LEFT JOIN
   @tmpMultipleAssignments tmp -- ON (parms.AssignmentID = tmp.AssignmentID)
    LEFT JOIN dbo.utb_communication_method cm ON (tmp.CommunicationMethodID = cm.CommunicationMethodID)
    LEFT JOIN utb_Assignment a on tmp.AssignmentID = a.AssignmentID  
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
    --LEFT OUTER JOIN dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', 'RepairTotal', null) ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID and tmp.CancellationDate IS NULL


    UNION ALL


    -- Select Shop Level

    SELECT  4,
            3,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            IsNull(tmp.AssignmentID, ''), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            sl.ShopLocationID,
			sl.PreferredEstimatePackageID,
            sl.ShopID,
            IsNull(sl.Address1, ''),
            IsNull(sl.Address2, ''),
            IsNull(sl.AddressCity, ''),
            IsNull(sl.AddressCounty, ''),
            IsNull(sl.AddressState, ''),
            IsNull(sl.AddressZip, ''),
            IsNull(sl.EmailAddress, ''),
            IsNull(sl.FaxAreaCode, ''),
            IsNull(sl.FaxExchangeNumber, ''),
            IsNull(sl.FaxExtensionNumber, ''),
            IsNull(sl.FaxUnitNumber, ''),
            IsNull(Convert(varchar(30), @TimeAtShop, 8), ''),   -- Convert to hh:mm:ss format 
            IsNull(sl.Name, ''),
            IsNull(sl.PhoneAreaCode, ''),
            IsNull(sl.PhoneExchangeNumber, ''),
            IsNull(sl.PhoneExtensionNumber, ''),
            IsNull(sl.PhoneUnitNumber, ''),
            IsNull(tmp.ProgramTypeCD,''),
            IsNull(sl.WebSiteAddress, ''),
            IsNull(@OperatingFridayEndTime, ''),
            IsNull(@OperatingFridayStartTime, ''),
            IsNull(@OperatingMondayEndTime, ''),
            IsNull(@OperatingMondayStartTime, ''),
            IsNull(@OperatingSaturdayEndTime, ''),
            IsNull(@OperatingSaturdayStartTime, ''),
            IsNull(@OperatingSundayEndTime, ''),
            IsNull(@OperatingSundayStartTime, ''),
            IsNull(@OperatingThursdayEndTime, ''),
            IsNull(@OperatingThursdayStartTime, ''),
            IsNull(@OperatingTuesdayEndTime, ''),
            IsNull(@OperatingTuesdayStartTime, ''),
            IsNull(@OperatingWednesdayEndTime, ''),
            IsNull(@OperatingWednesdayStartTime, ''),
            IsNull(@ShopOpen, ''),
            isNull(dbo.ufnUtilityGetDateString(@LastAssignedDate), ''),
            IsNull(z.CountyType, ''),
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    -- (SELECT @ActiveAssignmentID AS AssignmentID) AS parms     LEFT JOIN 
    @tmpMultipleAssignments tmp -- ON (parms.AssignmentID = tmp.AssignmentID)
    LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_zip_code z ON (sl.AddressZip = z.Zip)
    WHERE tmp.ShopLocationID IS NOT NULL
      AND tmp.AssignmentTypeCD = 'SHOP' 
      AND tmp.CancellationDate IS NULL


    UNION ALL


    -- Select the Shop Contact Level

    SELECT  5,
            4,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            IsNull(tmp.AssignmentID, ''), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            IsNull(p.Name, ''),
            IsNull(p.PhoneAreaCode, ''),
            IsNull(p.PhoneExchangeNumber, ''),
            IsNull(p.PhoneExtensionNumber, ''),
            IsNull(p.PhoneUnitNumber, ''),
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    --(SELECT @ActiveAssignmentID AS AssignmentID) AS parms     LEFT JOIN 
     @tmpMultipleAssignments tmp -- ON (parms.AssignmentID = tmp.AssignmentID)
    LEFT JOIN dbo.utb_shop_location_personnel slp ON (tmp.ShopLocationID = slp.ShopLocationID)
    LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
    LEFT JOIN dbo.utb_personnel_type pt ON (@Business1PersonnelTypeID = pt.PersonnelTypeID) 
    WHERE tmp.ShopLocationID IS NOT NULL
      AND tmp.AssignmentTypeCD = 'SHOP'
      AND tmp.CancellationDate IS NULL


    UNION ALL


    -- Select Appraiser Level

    SELECT  6,
            3,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            IsNull(tmp.AssignmentID, ''), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            ap.AppraiserID,
            IsNull(ap.Address1, ''),
            IsNull(ap.Address2, ''),
            IsNull(ap.AddressCity, ''),
            IsNull(ap.AddressCounty, ''),
            IsNull(ap.AddressState, ''),
            IsNull(ap.AddressZip, ''),
            IsNull(ap.EmailAddress, ''),
            IsNull(ap.FaxAreaCode, ''),
            IsNull(ap.FaxExchangeNumber, ''),
            IsNull(ap.FaxExtensionNumber, ''),
            IsNull(ap.FaxUnitNumber, ''),
            IsNull(ap.Name, ''),
            IsNull(ap.PhoneAreaCode, ''),
            IsNull(ap.PhoneExchangeNumber, ''),
            IsNull(ap.PhoneExtensionNumber, ''),
            IsNull(ap.PhoneUnitNumber, ''),
            IsNull(ap.WebSiteAddress, ''),
            isNull(dbo.ufnUtilityGetDateString(@LastAssignedDate), ''),
            --IsNull(@LastAssignedDate, ''),
             -- Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM   --  (SELECT @ActiveAssignmentID AS AssignmentID) AS parms     LEFT JOIN
     @tmpMultipleAssignments tmp -- ON (parms.AssignmentID = tmp.AssignmentID)
    LEFT JOIN dbo.utb_appraiser ap ON (tmp.AppraiserID = ap.AppraiserID)
    WHERE tmp.AppraiserID IS NOT NULL
		AND tmp.CancellationDate IS NULL
      --AND tmp.AssignmentTypeCD = 'IA'


    UNION ALL


    -- Select Multiple Assignments level
    
    SELECT  7,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            tmp.AssignmentID,
            tmp.AssignmentTypeCD,
            tmp.Name,
            IsNull(dbo.ufnUtilityGetDateString(tmp.AssignmentDate),''),
            IsNull(dbo.ufnUtilityGetDateString(tmp.CancellationDate),''),
            IsNull(dbo.ufnUtilityGetDateString(tmp.SelectionDate),''),
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMultipleAssignments tmp
    WHERE (tmp.AssignmentID <> @ActiveShopAssignmentID AND tmp.AssignmentID <> @ActiveDAAssignmentID)

    UNION ALL

    SELECT  8,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            casc.ClaimCoverageID,
            isNull(convert(varchar(20), casc.DeductibleAppliedAmt), ''),
            isNull(convert(varchar(20), casc.LimitAppliedAmt), ''),
            casc.PartialCoverageFlag,
            casc.SysLastUpdatedDate,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM dbo.utb_claim_aspect_service_channel_coverage casc
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    UNION ALL

    SELECT  9,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            cascc.ClaimAspectServiceChannelID,
            cascc.ClaimCoverageID,
            isNull(convert(varchar(20), cascc.DeductibleAppliedAmt), ''),
            isNull(convert(varchar(20), cascc.LimitAppliedAmt), ''),
            cascc.PartialCoverageFlag,
            casc.ServiceChannelCD,
            cat.Name + ' ' + convert(varchar, ca.ClaimAspectNumber),
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM dbo.utb_claim_aspect_service_channel casc
    LEFT JOIN utb_claim_aspect_service_channel_coverage cascc ON casc.ClaimAspectServiceChannelID = cascc.ClaimAspectServiceChannelID
    LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
    WHERE casc.ClaimAspectID = @ClaimAspectID

    UNION ALL

    SELECT  top 1 10,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            isNull(c.ClientClaimNumber, ''), 
            isNull(dbo.ufnUtilityGetDateString(c.LossDate),''), 
            isNull(c.LossDescription, ''),
            isNull(convert(varchar, cc.DeductibleAmt), ''),
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_coverage cc ON c.LynxID = cc.LynxID
    WHERE c.LynxID = @LynxID
      --AND cc.CoverageTypeCD = 'COMP'
      AND cc.EnabledFlag = 1

    UNION ALL

    SELECT  11,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            isNull(i.NameFirst, ''), 
            isNull(i.NameLast, ''), 
            isNull(cv.VIN, ''), 
            isNull(ca.ClaimAspectNumber, 1), 
            isNull(cv.VehicleYear, ''), 
            isNull(cv.Make, ''), 
            isNull(cv.Model, ''), 
            isNull(i.BestContactPhoneCD, ''), 
            isNull(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber, ''), 
            isNull(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber, ''), 
            isNull(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber, ''),
            isNull(i.BestContactPhoneCD, ''), 
            isNull(i.Address1, ''), 
            isNull(i.AddressCity, ''), 
            isNull(i.AddressState, ''), 
            isNull(i.AddressZip, ''), 
            isNull(i.EmailAddress, ''), 
            isNull(cv.DriveableFlag, 0),
            @RentalCoverage,
            isNull(@InsuredClaimantName, ''),
            isNull(@PriImpact, ''),
            isNull(@SecImpact, ''),
            isNull(@ShopComments, ''),
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON ca.ClaimAspectID = cai.ClaimAspectID
    LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
    LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
    LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
    WHERE ca.ClaimAspectID = @ClaimAspectID
      AND irt.Name = 'Owner'

    UNION ALL


    SELECT  DISTINCT 12,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM @tmpMetadata

    UNION ALL

    SELECT  13,
            12,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM @tmpMetadata
    
    UNION ALL
    
    SELECT  14,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Aspect Service Channel Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL,
            -- Assignment
            99999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL,
            -- Appraiser
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Multiple Assignments
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Coverages applied
            NULL, NULL, NULL, NULL, NULL,
            -- Claim Coverages applied
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim details
            NULL, NULL, NULL, NULL,
            -- Vehicle details
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            ListName,
            ReferenceID,
            Name,
            SystemName,
            ClientCode,
            DisplayOrder,
            ClientCoverageTypeID,
            isNull(convert(varchar, DeductibleAmt), ''),
            isNull(convert(varchar, LimitAmt), '')

    FROM @tmpReference
    


    ORDER BY [Assignment!3!AssignmentID], [Metadata!12!Entity], Tag , [Assignments!7!CancellationDate] desc
    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestVehicleAssignGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestVehicleAssignGetDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

