-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetHQInvoiceCloseAssignmentsWSXML 
END

GO 

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceGetHQInvoiceCloseAssignmentsWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Get a list of waiting HQ Invoice and Close Choice Shop Assignments from the database.
* Date:			14Dev2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	14Dec2015 - TVD - Initial development
*				02Aug2016 - TVD - Fixed issue related to Choice Shop fee per client.
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceGetHQInvoiceCloseAssignmentsWSXML]
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag
	DECLARE @ChoiceProcessFee					udt_std_money
	DECLARE @ChoiceProcessFeeID					udt_std_id
	DECLARE @AndroidUserID						udt_std_id

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML'
	SET @Now = CURRENT_TIMESTAMP
	SET @AndroidUserID = 0

	---------------------------------------
	-- Get Android 
	-- UserID = APDAndroidUser@lynxservices.com
	---------------------------------------
	SELECT @AndroidUserID = UserID FROM utb_user WHERE EmailAddress = 'APDAndroidUser@lynxservices.com'

	---------------------------------------
	-- Tests
	---------------------------------------
	
	---------------------------------------
	-- Main Code - Check if HQ Invoice/Close 
	-- assignments exist
	---------------------------------------
	-- HQ Invoice Close 
	IF EXISTS (
		SELECT TOP 1
			hj.ClaimAspectID
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID = 107 -- Transaction Review Complete (Invoice and Close)	
	)
	BEGIN
		------------------------------------------------------
		-- Choice Assignment waiting to be processed.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Choice Invoice/Close Assignments are waiting to be processed.'
		END
		
		------------------------------------------------------
		-- Create HQ Invoice/Close Assignment list XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!ChoiceProcessFeeID]
			, NULL				AS [Claim!2!ChoiceProcessFee]
			, NULL				AS [Claim!2!HQCompleteID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!UserID]

		UNION ALL

		SELECT  2 AS Tag,  
				1 AS Parent
				, NULL
				, hj.ClaimAspectID - 1
				, hj.ClaimAspectID
				, hj.ClaimAspectServiceChannelID
				, hj.JobID
				, hj.LynxID
				, hj.JobStatus
				, hj.DocumentTypeID
				, hj.DocumentTypeName
				, hj.AssignmentID
				, ISNULL(cf.ClientFeeID,0)
				, ISNULL(cf.FeeAmount,0)
				, hl.EventID
				, hj.VehicleID
				, @AndroidUserID
		FROM 
			utb_hyperquestsvc_jobs hj
			INNER JOIN utb_claim_aspect_status cas
			ON cas.ClaimAspectID = hj.ClaimAspectID
			INNER JOIN utb_history_log hl
			ON hl.ClaimAspectServiceChannelID = hj.ClaimAspectServiceChannelID
			LEFT OUTER JOIN utb_client_fee cf
			ON cf.InsuranceCompanyID = hj.InscCompID
				AND cf.[Description] = 'Choice Shop'
				AND cf.EnabledFlag = 1
		WHERE 
			hj.JobStatus='Processed'
			AND hj.DocumentTypeID = 22
			AND hj.DocumentTypeName = 'Choice Shop Assignment'
			AND hj.AssignmentID IS NOT NULL
			AND hj.EnabledFlag = 1
			AND cas.StatusID = 800 -- Active CS
			AND cas.StatusID NOT IN (899, 998, 999, 598, 994) -- Claim/Vehicle not closed/complete or cancelled
			AND hl.EventID = 107 -- Transaction Review Complete (Invoice and Close)	)

		FOR XML EXPLICIT	
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- NO Choice Assignment waiting
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'NO HQ Invoice/Close Assignments waiting'
		END

		------------------------------------------------------
		-- Create NO JOBS XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!ClaimAspectIDClaim]   
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]
			, NULL				AS [Claim!2!ClaimAspectServiceChannelID]
			, NULL				AS [Claim!2!JobID]
			, NULL				AS [Claim!2!LynxID]
			, NULL				AS [Claim!2!JobStatus]
			, NULL				AS [Claim!2!DocumentTypeID]
			, NULL				AS [Claim!2!DocumentTypeName]
			, NULL				AS [Claim!2!AssignmentID]
			, NULL				AS [Claim!2!ChoiceProcessFeeID]
			, NULL				AS [Claim!2!ChoiceProcessFee]
			, NULL				AS [Claim!2!HQCompleteID]
			, NULL				AS [Claim!2!VehicleID]
			, NULL				AS [Claim!2!UserID]

		FOR XML EXPLICIT	
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceGetHQInvoiceCloseAssignmentsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/