-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspUtilClaimListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspUtilClaimListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspUtilClaimListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo    
* FUNCTION:     Temporary procedure used with the Estimate Correction Utility
*
* PARAMETERS:  
* (I) @FromDate             Start date of claim list
* (I) @ToDate               To date of claim list
* (I) @NotReviewedOnlyFlag  Flag indicating user wishes to see only those claims not reviewed  
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspUtilClaimListXML
    @FromDate               varchar(30),
    @ToDate                 varchar(30),
    @NotReviewedOnlyFlag    udt_std_flag = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @tmpEstimate TABLE
    (
        ClaimAspectID       bigint      NOT NULL,
        EstimateCount       tinyint     NOT NULL,
        ReviewedFlag        bit         NULL
    )
    
    DECLARE @dtFromDate     datetime
    DECLARE @dtToDate       datetime
    
    
    -- Validate parameters
    
    IF @FromDate <> '0'
    BEGIN
        IF IsDate(@FromDate) <> 0
        BEGIN
            SET @dtFromDate = Convert(datetime, @FromDate + ' 0:00:00')
        END
        ELSE
        BEGIN 
            RAISERROR('Invalid From Date, Please reenter', 16, 1)
        END
    END
    ELSE
    BEGIN
        SET @dtFromDate = '1/1/1900'
    END
    
    
    IF @ToDate <> '0'
    BEGIN            
        IF IsDate(@ToDate) <> 0
        BEGIN
            SET @dtToDate = Convert(datetime, @ToDate + ' 23:59:59')
        END
        ELSE
        BEGIN 
            RAISERROR('Invalid To Date, Please reenter', 16, 1)
        END
    END
    ELSE
    BEGIN
        SET @dtToDate = '1/2/1900'
    END
        
    
    -- Compile the number of estimates for each claim aspect

    INSERT INTO @tmpEstimate (ClaimAspectID, EstimateCount)
      SELECT ca.ClaimAspectID, count(cascd.DocumentID)
        FROM dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
        Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectID = ca.ClaimAspectID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID) --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE c.IntakeFinishDate BETWEEN @dtFromDate AND @dtToDate
          AND casc.EnabledFlag = 1--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
          AND ca.ExposureCD IN ('1', '3')
          AND dt.EstimateTypeFlag = 1
        GROUP BY ca.ClaimAspectID
        ORDER BY ca.ClaimAspectID

    UPDATE @tmpEstimate
      SET ReviewedFlag = CASE
                   WHEN EXISTS (SELECT  scascd.DocumentID FROM  
                   dbo.utb_claim_aspect_service_channel_document scascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                   LEFT JOIN dbo.utb_document sd  ON scascd.DocumentID = sd.DocumentID 
                   LEFT JOIN dbo.utb_document_type sdt ON sd.DocumentTypeID = sdt.DocumentTypeID
                   Left outer join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = scascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                   WHERE  (casc.ClaimAspectID = tmp.ClaimAspectID and casc.EnabledFlag = 1 AND sdt.EstimateTypeFlag = 1 AND sd.EnabledFlag=1 AND                     sd.EstimateReviewDate IS  NULL )) THEN  0 ELSE 1

     END 
    FROM @tmpEstimate tmp


    -- If the user wants non-reviewed only, delete the records that have been reviewed
    
    IF @NotReviewedOnlyFlag = 1
    BEGIN
        DELETE FROM @tmpEstimate WHERE ReviewedFlag = 1
    END
    
    
    -- XML Select
    
    SELECT  1    AS Tag,
            NULL AS Parent,
            @FromDate AS [Root!1!FromDate],
            @ToDate AS [Root!1!ToDate],
            -- Claim Information
            NULL AS [Claim!2!ClaimAspectID],
            NULL AS [Claim!2!LynxID],
            NULL AS [Claim!2!ReceivedDate],
            NULL AS [Claim!2!Type],
            NULL AS [Claim!2!Status],
            NULL AS [Claim!2!ReviewedFlag]
            
            
    UNION ALL
     
    SELECT  2,
            1,
            NULL, NULL,
            -- Claim Information
            ca.ClaimAspectID,
            Convert(varchar(6), c.LynxId) + '-' + Convert(varchar(2), ca.ClaimAspectNumber),
            IsNull(c.IntakeFinishDate, ''),
            (SELECT  IsNull(Name, '')
               FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'ServiceChannelCD')
               WHERE Code = casc.ServiceChannelCD),
            (SELECT IsNull(ss.Name, '')
               FROM dbo.utb_claim_aspect sca
				LEFT OUTER JOIN utb_Claim_Aspect_Status cas1 ON sca.ClaimAspectID = cas1.ClaimAspectID
               LEFT JOIN dbo.utb_status ss ON (cas1.StatusID = ss.StatusID)
               WHERE sca.LynxID = ca.LynxID
                 AND sca.ClaimAspectTypeID = 0),
            tmp.ReviewedFlag
       FROM dbo.utb_claim_aspect ca
	   LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
       LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
       LEFT JOIN @tmpEstimate tmp ON (ca.ClaimAspectID = tmp.ClaimAspectID)
       WHERE c.IntakeFinishDate BETWEEN @dtFromDate AND @dtToDate
         AND ca.ExposureCD IN ('1', '3')
         AND tmp.EstimateCount > 0
         
    ORDER BY Tag, [Claim!2!LynxID]         
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspUtilClaimListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspUtilClaimListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/