-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVehicleGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVehicleGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves data necessary to populate the claim vehicle screen
*
* PARAMETERS:  
* (I) @ClaimAspectId        The Claim Aspect Id of the vehicle being retrieved
* (I) @InsuranceCompanyID   The Insurance company to validate against. 
*
* RESULT SET:
* An XML Data stream containing claim vehicle and involved person information
*
*
* VSS
* $Workfile: uspClaimVehicleGetDetailXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspClaimVehicleGetDetailXML.SQL $
* $Revision: 6 $
* $Author: Jonathan $
* $Date: 10/23/01 5:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClaimVehicleGetDetailWSXML]
    @ClaimAspectID          udt_std_int_big,
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @EstimateSummaryTypeIDNetTotal  udt_std_id
    DECLARE @EstimateSummaryTypeIDRepairTotal udt_std_id
    DECLARE @ExposureCD                     udt_std_cd
    DECLARE @InsuranceCompanyIDClaim        udt_std_id
    DECLARE @ClaimAspectNumber              udt_std_id
    DECLARE @ClaimAspectTypeID              udt_std_id
    DECLARE @ClaimAspectIDCheck             udt_std_id_big
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @PertainsTo                     varchar(8)
    DECLARE @CountNotes                     udt_std_int
    DECLARE @CountTasks                     udt_std_int
    DECLARE @CountBilling                   udt_std_int
    DECLARE @ActiveReinspection             bit
    DECLARE @VehicleStatus                  varchar(30)
    DECLARE @VehicleOpenStatusID            udt_std_id
    DECLARE @WarrantyExistsFlag             udt_std_flag

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimVehicleGetDetailWSXML'

    SET NOCOUNT ON
    
    -- Get Claim Aspect Type ID

    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeID IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Check to make sure a valid Claim Aspect ID was passed in, pull the information we'll need on the claim aspect

    IF  (@ClaimAspectID <> 0)
    BEGIN
        SELECT  @ClaimAspectIDCheck = ClaimAspectID,
                @ClaimAspectNumber = ClaimAspectNumber,
                @LynxID = LynxID
          FROM  dbo.utb_claim_aspect 
          WHERE ClaimAspectID = @ClaimAspectID 
            AND ClaimAspectTypeID = @ClaimAspectTypeID
    
    
        IF (@ClaimAspectIDCheck IS NULL)
        BEGIN
            -- Invalid Claim Aspect ID
    
            RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
    END


    -- Get the Insurance Company Id for the claim

    SELECT  @InsuranceCompanyIDClaim = InsuranceCompanyID 
      FROM  dbo.utb_claim_aspect ca
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      WHERE ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Vehicle',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_claim_vehicle' AND Column_Name IN 
            ('BodyStyle',
             'BookValueAmt',
             'Color',
             'DriveableFlag',
             'ImpactSpeed',
             'InspectionDate',
             'LicensePlateNumber',
             'LicensePlateState',
             'LocationAreaCode',
             'LocationAddress1',
             'LocationAddress2',
             'LocationCity',
             'LocationExchangeNumber',
             'LocationExtensionNumber',
             'LocationName',
             'LocationState',
             'LocationUnitNumber',
             'LocationZip',
             'Make',
             'Mileage',
             'Model',
             'NADAId',
             'PermissionToDriveCD',
             'PostedSpeed',
             'Remarks',
             'RentalDaysAuthorized',
             'RentalInstructions',
             'RepairEndDate',
             'RepairStartDate',
             'ShopRemarks',
             'VehicleYear',
             'VIN')) 
      OR    (Table_Name = 'utb_claim_aspect' AND Column_Name IN
            ('CoverageProfileCD',
             'CurrentAssignmentTypeID')) 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'SafetyDevice',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_vehicle_safety_device' AND Column_Name IN 
            ('SafetyDeviceID'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Impact',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_vehicle_impact' AND Column_Name IN 
            ('ImpactID',
             'CurrentImpactFlag',
             'PrimaryImpactFlag',
             'PriorImpactFlag'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Contact',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_involved' AND Column_Name IN 
            ('InsuredRelationID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressState',
             'AddressZip',
             'AlternateAreaCode',
             'AlternateExchangeNumber',
             'AlternateExtensionNumber',
             'AlternateUnitNumber',
             'BestContactTime',
             'BestContactPhoneCD',
             'DayAreaCode',
             'DayExchangeNumber',
             'DayExtensionNumber',
             'DayUnitNumber',
             'EmailAddress',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'NightAreaCode',
             'NightExchangeNumber',
             'NightExtensionNumber',
             'NightUnitNumber',
             'PrefMethodUpd',
             'CellPhoneCarrier',
             'CellAreaCode',
             'CellExchangeNumber',
             'CellUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Document',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN 
            ('AgreedPriceMetCD'))
      AND   (Table_Name = 'utb_estimate_summary' AND Column_Name IN 
            ('OriginalExtendedAmt',
             'AgreedExtendedAmt'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

--select * from @tmpMetadata

    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,  
        ReferenceID     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )
    
    
    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table    

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name)
    
    SELECT  'AssignmentType' AS ListName,
            at.DisplayOrder,
            convert(varchar, at.AssignmentTypeID),
            at.Name
    FROM    utb_client_assignment_type cat
    LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)    
    WHERE   cat.InsuranceCompanyID = @InsuranceCompanyID
      AND   at.enabledFlag = 1
    
    UNION ALL
    
    SELECT  'ContactRelationToInsured',
            DisplayOrder,
            convert(varchar, RelationID),
            Name
    FROM    dbo.utb_Relation
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL
    AND     Name NOT LIKE 'Third%'

    UNION ALL
    
    SELECT  'CoverageProfile',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect', 'CoverageProfileCD' )

    UNION ALL
    
    SELECT  'EstimateType',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_document', 'EstimateTypeCD' )

    UNION ALL
    
    SELECT  'Impact',
            DisplayOrder,
            convert(varchar, ImpactID), 
            Name 
    FROM    dbo.utb_impact
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'SafetyDevice',
            DisplayOrder,
            convert(varchar, SafetyDeviceID), 
            Name 
    FROM    dbo.utb_safety_device
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'State',
            DisplayOrder,
            StateCode, 
            StateValue 
    FROM    dbo.utb_state_code

    UNION ALL

    SELECT  'Exposure',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect', 'ExposureCD' )

    UNION ALL
    
    SELECT  'PermissionToDrive',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_vehicle', 'PermissionToDriveCD' )

    UNION ALL
    
    SELECT  'BestContactPhone',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'BestContactPhoneCD' )
    
    UNION ALL
    
    SELECT  'PrefMethodUpd',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'PrefMethodUpd' )
    
    UNION ALL
    
    SELECT  'CellPhoneCarrier',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'CellPhoneCarrier' )
    
    UNION ALL
    
    SELECT  'AgreedPriceMetCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_document', 'AgreedPriceMetCD' )    
    
    UNION ALL
    
    SELECT  'ServiceChannelCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect_service_channel', 'ServiceChannelCD' )    

    UNION ALL
    
    SELECT  'DispositionTypeCD',
            NULL,
            Code,
            Name
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_aspect_service_channel', 'DispositionTypeCD' )    

    UNION ALL
    
    SELECT  Distinct 'ClientServiceChannels',
            NULL,
            csc.ServiceChannelCD,
            urc.Name
    FROM    utb_client_service_channel csc
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc on csc.ServiceChannelCD = urc.Code
    WHERE csc.InsuranceCompanyID = @InsuranceCompanyID

    ORDER BY ListName, DisplayOrder
    

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    -- Validate APD Data state
    
    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    -- Continuing to validate APD Data state
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WHERE CategoryCD = 'CP' AND Name = 'ContractPrice')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"ContractPrice"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'RepairTotal')
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    
    -- We need to adjust the reference data for CoverageProfileCD.  For first party vehicle, only COLL, COMP and UIM are 
    -- valid, for third party on LIAB is valid, for Non-exposures, none is valid
    
    SELECT  @ExposureCD = ExposureCD 
      FROM  dbo.utb_claim_aspect
      WHERE ClaimAspectID = @ClaimAspectID
      
    IF @ExposureCD <> '1'
    BEGIN 
      DELETE FROM @tmpReference
        WHERE ListName = 'CoverageProfile'
          AND ReferenceID IN ('COLL', 'COMP', 'UIM')
    END
    
    IF @ExposureCD <> '3'
    BEGIN 
      DELETE FROM @tmpReference
        WHERE ListName = 'CoverageProfile'
          AND ReferenceID IN ('LIAB')
    END
    
      
    -- We now need to select estimates into a table variable.  We have to do this here instead of directly
    -- in the XML query because we need to guarantee a record returned.  Our standard way of doing this
    -- {(SELECT @LynxID AS LynxID) AS parms}  and then joining this back doesn't work because this table
    -- is shared by notes.  So a claim with notes but no documents won't retrieve the empty document
    -- correctly.  The following code is a workaround until a more elegant solution can be found. 

    DECLARE @tmpDocument TABLE
    (
        DocumentID  int
    )

    INSERT INTO @tmpDocument
      SELECT  cascd.DocumentID
        FROM  dbo.utb_claim_aspect_service_channel casc
        LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE casc.ClaimAspectID = @ClaimAspectID
          AND d.EnabledFlag = 1         -- Only return enabled
          --AND dt.EstimateTypeFlag = 1   -- Only interested in estimates
          AND dt.Name not in ('Note') -- Not interested in notes


    -- If no records were selected, add one manually

    IF @@rowcount = 0
    BEGIN
        INSERT INTO @tmpDocument
        VALUES (0)
    END
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpDocument', 16, 1, @ProcName)
        RETURN
    END
    

    -- Generate PertainsToCode
    
    SET @PertainsTo = LTrim(RTrim(dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, @ClaimAspectNumber, 0)))  -- 0=Get code


    -- Get Counts of Notes and Tasks
    SET @CountTasks = (SELECT COUNT(c.CheckListID) 
                        FROM utb_checklist c
                        LEFT JOIN utb_claim_aspect_service_channel casc ON (c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
                        WHERE casc.ClaimAspectID = @ClaimAspectID)
    
    SET @CountNotes = (SELECT COUNT(*) 
                       FROM utb_claim_aspect_service_channel casc
                       LEFT JOIN utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                       INNER JOIN utb_document d ON cascd.DocumentID = d.DocumentID
                       WHERE casc.ClaimAspectID = @ClaimAspectID
                         AND d.EnabledFlag = 1)


    --SET @CountBilling = (SELECT COUNT(*) FROM dbo.utb_client_billing_service WHERE ClaimAspectID = @ClaimAspectID AND EnabledFlag = 1)
    SELECT @CountBilling = Count(InvoiceID)
    FROM dbo.utb_invoice i
    WHERE i.ClaimAspectID = @ClaimAspectID
      AND i.EnabledFlag = 1
      AND i.ItemTypeCD = 'F'    -- Fee    
      
    SET @ActiveReinspection = 0

    -- See if there is a reinspection request out there for the claim aspect
    IF EXISTS(SELECT d.DocumentID
                FROM dbo.utb_claim_aspect_service_channel casc
                LEFT JOIN utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                WHERE casc.ClaimAspectID = @ClaimAspectID
                  AND d.ReinspectionRequestFlag = 1
                  AND d.EnabledFlag = 1) OR -- any active reinspection request
       EXISTS(SELECT ReinspectID
                FROM dbo.utb_reinspect
                WHERE ClaimAspectID = @ClaimAspectID
                  AND LockedFlag = 0
                  AND EnabledFlag = 1) -- reinspection requested and has not been completed
    BEGIN
        SET @ActiveReinspection = 1
    END
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    /*SELECT @VehicleOpenStatusID = StatusID
    FROM dbo.utb_status
    WHERE Name = 'Open'
      AND ClaimAspectTypeID = @ClaimAspectTypeID
    
    -- Get the Vehicle status
    IF EXISTS(SELECT ClaimAspectStatusID
                FROM dbo.utb_claim_aspect_status
                WHERE ClaimAspectID = @ClaimAspectID
                  AND StatusID = @VehicleOpenStatusID)
    BEGIN
        SET @VehicleStatus = 'Open'
    END*/
    
    SELECT @VehicleStatus = s.Name
    FROM dbo.utb_claim_aspect_status cas
    LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
    WHERE cas.ClaimAspectID = @ClaimAspectID
      AND cas.StatusTypeCD IS NULL

    SET @WarrantyExistsFlag = 0
    IF EXISTS(SELECT wa.AssignmentID
               FROM utb_claim_aspect_service_channel casc 
               LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
               LEFT JOIN utb_warranty_assignment wa ON casc.ClaimAspectServiceChannelID = wa.ClaimAspectServiceChannelID
               WHERE wa.CancellationDate IS NULL)
    BEGIN
      SET @WarrantyExistsFlag = 1
    END

    -- Select Root Level

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            @PertainsTo AS [Root!1!Context],
            @CountNotes AS [Root!1!CountNotes],
            @CountTasks AS [Root!1!CountTasks],
            @CountBilling AS [Root!1!CountBilling], 
            CASE
                WHEN @PertainsTo IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList(@LynxID, 0, 0)) THEN  1
                ELSE 0
            END AS [Root!1!ContextSupportedFlag],
             -- Claim Vehicle
            NULL AS [Vehicle!2!ClaimAspectID],
            NULL AS [Vehicle!2!ActiveReinspection],            
            NULL AS [Vehicle!2!BodyStyle],
            NULL AS [Vehicle!2!BookValueAmt],
            NULL AS [Vehicle!2!Color],
            NULL AS [Vehicle!2!CoverageProfileCD],
            NULL AS [Vehicle!2!ClientCoverageTypeID],
            NULL AS [Vehicle!2!CurrentAssignmentTypeID],
            NULL AS [Vehicle!2!DispositionType],
            NULL AS [Vehicle!2!DriveableFlag],
            NULL AS [Vehicle!2!ExposureCD],
            NULL AS [Vehicle!2!ImpactSpeed],
            NULL AS [Vehicle!2!InspectionDate],
            NULL AS [Vehicle!2!InitialAssignmentTypeID],
            NULL AS [Vehicle!2!LicensePlateNumber],
            NULL AS [Vehicle!2!LicensePlateState],
            NULL AS [Vehicle!2!LocationAreaCode],
            NULL AS [Vehicle!2!LocationAddress1],
            NULL AS [Vehicle!2!LocationAddress2],
            NULL AS [Vehicle!2!LocationCity],
            NULL AS [Vehicle!2!LocationExchangeNumber],
            NULL AS [Vehicle!2!LocationExtensionNumber],
            NULL AS [Vehicle!2!LocationName],
            NULL AS [Vehicle!2!LocationState],
            NULL AS [Vehicle!2!LocationUnitNumber],
            NULL AS [Vehicle!2!LocationZip],
            NULL AS [Vehicle!2!Make],
            NULL AS [Vehicle!2!Mileage],
            NULL AS [Vehicle!2!Model],
            NULL AS [Vehicle!2!NADAId],
            NULL AS [Vehicle!2!PermissionToDriveCD],
            NULL AS [Vehicle!2!PostedSpeed],
            NULL AS [Vehicle!2!Remarks],
            NULL AS [Vehicle!2!RentalDaysAuthorized],
            NULL AS [Vehicle!2!RentalStartDate],
            NULL AS [Vehicle!2!RentalEndDate],
            NULL AS [Vehicle!2!RentalInstructions],
            NULL AS [Vehicle!2!RepairEndDate],
            NULL AS [Vehicle!2!RepairEndConfirmFlag],
            NULL AS [Vehicle!2!RepairStartDate],
            NULL AS [Vehicle!2!RepairStartConfirmFlag],
            NULL AS [Vehicle!2!VehicleYear],
            NULL AS [Vehicle!2!EstimateVIN],
            NULL AS [Vehicle!2!VIN],
            NULL AS [Vehicle!2!Status],
            NULL AS [Vehicle!2!SysLastUpdatedDate],
            NULL AS [Vehicle!2!ClaimAspectSysLastUpdatedDate],
            NULL AS [Vehicle!2!WarrantyExistsFlag],
            -- Vehicle Safety Device
            NULL AS [SafetyDevice!3!SafetyDeviceID],
            -- Vehicle Impact
            NULL AS [Impact!4!ImpactID],
            NULL AS [Impact!4!CurrentImpactFlag],
            NULL AS [Impact!4!PrimaryImpactFlag],
            NULL AS [Impact!4!PriorImpactFlag],
            -- Client Coverage types
            NULL AS [CoverageType!5!ClientCoverageTypeID],
            NULL AS [CoverageType!5!Name],
            NULL AS [CoverageType!5!CoverageProfileCD],
            NULL AS [CoverageType!5!DisplayOrder],  
            -- Contact
            NULL as [Contact!6!InvolvedID],
            NULL as [Contact!6!NameFirst],
            NULL as [Contact!6!NameLast],
            NULL as [Contact!6!NameTitle],
            NULL as [Contact!6!InsuredRelationID],
            NULL as [Contact!6!Address1],
            NULL as [Contact!6!Address2],
            NULL as [Contact!6!AddressCity],
            NULL as [Contact!6!AddressState],
            NULL as [Contact!6!AddressZip],
            NULL as [Contact!6!DayAreaCode],
            NULL as [Contact!6!DayExchangeNumber],
            NULL as [Contact!6!DayExtensionNumber],
            NULL as [Contact!6!DayUnitNumber],
            NULL as [Contact!6!EmailAddress],
            NULL as [Contact!6!NightAreaCode],
            NULL as [Contact!6!NightExchangeNumber],
            NULL as [Contact!6!NightExtensionNumber],
            NULL as [Contact!6!NightUnitNumber],
            NULL as [Contact!6!AlternateAreaCode],
            NULL as [Contact!6!AlternateExchangeNumber],
            NULL as [Contact!6!AlternateExtensionNumber],
            NULL as [Contact!6!AlternateUnitNumber],
            NULL as [Contact!6!BestContactTime],
            NULL as [Contact!6!BestContactPhoneCD],
            NULL as [Contact!6!PrefMethodUpd],
            NULL as [Contact!6!CellPhoneCarrier],
            NULL as [Contact!6!CellAreaCode],
            NULL as [Contact!6!CellExchangeNumber],
            NULL as [Contact!6!CellUnitNumber],
            NULL as [Contact!6!SysLastUpdatedDate],
            -- Document
            NULL AS [Document!7!DocumentID],
            NULL AS [Document!7!DocumentSourceName],
            NULL AS [Document!7!VANFlag],
            NULL AS [Document!7!DocumentTypeName],
            NULL AS [Document!7!CreatedDate],
            NULL AS [Document!7!ReceivedDate],
            NULL AS [Document!7!ImageLocation],
            NULL AS [Document!7!ApprovedFlag],
            NULL AS [Document!7!SupplementSeqNumber],
            NULL AS [Document!7!FullSummaryExistsFlag],
            NULL AS [Document!7!AgreedPriceMetCD],
            NULL AS [Document!7!GrossEstimateAmt],
            NULL AS [Document!7!NetEstimateAmt],
            NULL AS [Document!7!EstimateTypeFlag],
            NULL AS [Document!7!DirectionToPayFlag],
            NULL AS [Document!7!FinalEstimateFlag],
            NULL AS [Document!7!DuplicateFlag],
            NULL AS [Document!7!EstimateTypeCD],
            NULL AS [Document!7!ServiceChannelCD],
            NULL AS [Document!7!SysLastUpdatedDateDocument],
            NULL AS [Document!7!SysLastUpdatedDateEstimate],
            -- Involved
            NULL AS [Involved!8!InvolvedID],
            NULL AS [Involved!8!NameFirst],
            NULL AS [Involved!8!NameLast],
            NULL AS [Involved!8!BusinessName],
            NULL AS [Involved!8!DayAreaCode],
            NULL AS [Involved!8!DayExchangeNumber],
            NULL AS [Involved!8!AddressCity],
            NULL AS [Involved!8!AddressState],
            NULL AS [Involved!8!AddressZip],
            -- Involved Type
            NULL AS [InvolvedType!9!InvolvedTypeName],
            -- Metadata Header
            NULL AS [Metadata!10!Entity],
            -- Columns
            NULL AS [Column!11!Name],
            NULL AS [Column!11!DataType],
            NULL AS [Column!11!MaxLength],
            NULL AS [Column!11!Precision],
            NULL AS [Column!11!Scale],
            NULL AS [Column!11!Nullable],
            -- Reference Data
            NULL AS [Reference!12!List],
            NULL AS [Reference!12!ReferenceID],
            NULL AS [Reference!12!Name],
            --Service Channel Data
            NULL as [ClaimAspectServiceChannel!13!ClaimAspectServiceChannelID],
            NULL as [ClaimAspectServiceChannel!13!CreatedUserFirstName],
            NULL as [ClaimAspectServiceChannel!13!CreatedUserLastName],
            NULL as [ClaimAspectServiceChannel!13!CreatedUserFullName],
            NULL as [ClaimAspectServiceChannel!13!CreatedUserID],
            NULL as [ClaimAspectServiceChannel!13!CreatedDate],
            NULL as [ClaimAspectServiceChannel!13!DispositionTypeCD],
            NULL as [ClaimAspectServiceChannel!13!StatusID],
            NULL as [ClaimAspectServiceChannel!13!StatusName],
            NULL as [ClaimAspectServiceChannel!13!EnabledFlag],
            NULL as [ClaimAspectServiceChannel!13!InspectionDate],
            NULL as [ClaimAspectServiceChannel!13!ClientInvoiceDate],
            NULL as [ClaimAspectServiceChannel!13!OriginalCompleteDate],
            NULL as [ClaimAspectServiceChannel!13!OriginalEstimateDate],
            NULL as [ClaimAspectServiceChannel!13!PrimaryFlag],
            NULL as [ClaimAspectServiceChannel!13!ServiceChannelCD],
            NULL as [ClaimAspectServiceChannel!13!ServiceChannelName],
            NULL as [ClaimAspectServiceChannel!13!WorkEndConfirmFlag],
            NULL as [ClaimAspectServiceChannel!13!WorkEndDate],
            NULL as [ClaimAspectServiceChannel!13!WorkEndDateOriginal],
            NULL as [ClaimAspectServiceChannel!13!WorkStartConfirmFlag],
            NULL as [ClaimAspectServiceChannel!13!WorkStartDate],
            NULL as [ClaimAspectServiceChannel!13!CurEstGrossRepairTotal],
            NULL as [ClaimAspectServiceChannel!13!ReferenceID],
            NULL as [ClaimAspectServiceChannel!13!SysLastUpdatedDate],
            -- Claim Aspect Service Channel Coverage
            NULL as [Coverage!14!ClaimCoverageID],
            NULL as [Coverage!14!ClientCoverageTypeID],
            NULL as [Coverage!14!CoverageTypeCD],
            NULL as [Coverage!14!DeductibleAmt],
            NULL as [Coverage!14!LimitAmt],
            NULL as [Coverage!14!LimitDailyAmt],
            NULL as [Coverage!14!MaximumDays],
            NULL as [Coverage!14!DeductibleAppliedAmt],
            NULL as [Coverage!14!LimitAppliedAmt],
            NULL as [Coverage!14!ClientCode],
            -- Concession
            NULL as [Concession!15!ClaimAspectServiceChannelConcessionID],
            NULL as [Concession!15!ClaimAspectServiceChannelID],
            NULL as [Concession!15!ConcessionReasonID],
            NULL as [Concession!15!TypeDescription],
            NULL as [Concession!15!ReasonDescription],
            NULL as [Concession!15!Amount],
            NULL as [Concession!15!CreatedDate],
            NULL as [Concession!15!Comments]



    UNION ALL


    -- Select Vehicle Level

    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            IsNull(ca.ClaimAspectID, 0),
            IsNull(@ActiveReinspection, 0),
            IsNull(cv.BodyStyle, ''),
            IsNull(Convert(varchar(20), cv.BookValueAmt), ''),
            IsNull(cv.Color, ''),
            IsNull(ca.CoverageProfileCD, ''),
            IsNull(ca.ClientCoverageTypeID, 0),
            '', --IsNull(ca.CurrentAssignmentTypeID, ''), -- column deprecated
            '', --IsNull((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'DispositionTypeCD') WHERE Code = ca.DispositionTypeCD), ''),
            IsNull(cv.DriveableFlag, ''),
            IsNull(ca.ExposureCD, ''),
            IsNull(Convert(varchar(10), cv.ImpactSpeed), ''),
            '', --IsNull(cv.InspectionDate, ''),
            IsNull(ca.InitialAssignmentTypeID, ''),
            IsNull(cv.LicensePlateNumber, ''),
            IsNull(cv.LicensePlateState, ''),
            IsNull(cv.LocationAreaCode, ''),
            IsNull(cv.LocationAddress1, ''),
            IsNull(cv.LocationAddress2, ''),
            IsNull(cv.LocationCity, ''),
            IsNull(cv.LocationExchangeNumber, ''),
            IsNull(cv.LocationExtensionNumber, ''),
            IsNull(cv.LocationName, ''),
            IsNull(cv.LocationState, ''),
            IsNull(cv.LocationUnitNumber, ''),
            IsNull(cv.LocationZip, ''),
            IsNull(cv.Make, ''),
            IsNull(Convert(varchar(10), cv.Mileage), ''),
            IsNull(cv.Model, ''),
            IsNull(cv.NADAId, ''),
            IsNull(cv.PermissionToDriveCD, ''),
            IsNull(Convert(varchar(10), cv.PostedSpeed), ''),
            IsNull(cv.Remarks, ''),
            IsNull(cv.RentalDaysAuthorized, ''),
            IsNull(cv.RentalStartDate, ''),
            IsNull(cv.RentalEndDate, ''),
            IsNull(cv.RentalInstructions, ''),
            '', --IsNull(cv.RepairEndDate, ''),
            '', --IsNull(cv.RepairEndConfirmFlag, 0),
            '', --IsNull(cv.RepairStartDate, ''),
            '', --IsNull(cv.RepairStartConfirmFlag, 0),
            IsNull(convert(varchar(5), cv.VehicleYear), ''),
            IsNull(cv.EstimateVin, ''),
            IsNull(cv.Vin, ''),
            @VehicleStatus,
            dbo.ufnUtilityGetDateString( cv.SysLastUpdatedDate ),
            dbo.ufnUtilityGetDateString( ca.SysLastUpdatedDate ),
            /*CASE 
               WHEN EXISTS(SELECT wa.AssignmentID
                           FROM utb_warranty_assignment wa
                           LEFT JOIN utb_claim_aspect_service_channel casc ON wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                           WHERE casc.ClaimAspectID = ca.ClaimAspectID
                             AND wa.CancellationDate IS NULL) THEN 1
               ELSE 0 
            END*/
            ca.WarrantyExistsFlag,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    
    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_claim_aspect ca ON (parms.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (parms.ClaimAspectID = cv.ClaimAspectID)
    --LEFT JOIN dbo.utb_status vs ON (ca.StatusID = vs.StatusID)


    UNION ALL


    -- Select the Safety Device Level

    SELECT  3,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            IsNull(Convert(varchar(3), vsd.SafetyDeviceID), ''), 
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_vehicle_safety_device vsd ON (parms.ClaimAspectID = vsd.ClaimAspectID)


    UNION ALL


    -- Select the Impact Level

    SELECT  4,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            IsNull(vi.ImpactID, 0),
            IsNull(vi.CurrentImpactFlag, ''),
            IsNull(vi.PrimaryImpactFlag, ''),
            IsNull(vi.PriorImpactFlag, ''),
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_vehicle_impact vi ON (parms.ClaimAspectID = vi.ClaimAspectID)


    UNION All

  -- Select client coverage types along with APD types

 SELECT  5,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage types
            IsNull(ClientCoverageTypeID,0),
            IsNull(Name,''),
            IsNull(CoverageProfileCD,''),  
            IsNull(DisplayOrder,0),            
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID and EnabledFlag = 1


    UNION ALL

    -- Select Vehicle Contact Level

    SELECT  6,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            IsNull(i.InvolvedID, 0),
            IsNull(i.NameFirst, ''),
            IsNull(i.NameLast, ''),
            IsNull(i.NameTitle, ''),
            IsNull(i.InsuredRelationID, 0),
            IsNull(i.Address1, ''),
            IsNull(i.Address2, ''),
            IsNull(i.AddressCity, ''),
            IsNull(i.AddressState, ''),
            IsNull(i.AddressZip, ''),
            IsNull(i.DayAreaCode, ''),
            IsNull(i.DayExchangeNumber, ''),
            IsNull(i.DayExtensionNumber, ''),
            IsNull(i.DayUnitNumber, ''),
            IsNull(i.EmailAddress, ''),
            IsNull(i.NightAreaCode, ''),
            IsNull(i.NightExchangeNumber, ''),
            IsNull(i.NightExtensionNumber, ''),
            IsNull(i.NightUnitNumber, ''),
            IsNull(i.AlternateAreaCode, ''),
            IsNull(i.AlternateExchangeNumber, ''),
            IsNull(i.AlternateExtensionNumber, ''),
            IsNull(i.AlternateUnitNumber, ''),
            IsNull(i.BestContactTime, ''),
            IsNull(i.BestContactPhoneCD, ''),
            IsNull(i.PrefMethodUpd, ''),
            IsNull(i.CellPhoneCarrier, ''),
            IsNull(i.CellAreaCode, ''),
            IsNull(i.CellExchangeNumber, ''),
            IsNull(i.CellUnitNumber, ''),
            dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate ),
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID) AS parms
    LEFT JOIN dbo.utb_claim_vehicle cv ON (parms.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_involved i ON (cv.ContactInvolvedID = i.InvolvedID)


    UNION ALL


    -- Select the Document Level

    SELECT  7,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            IsNull(d.DocumentID, 0),
            IsNull(ds.Name, ''),
            ds.VANFlag,
            IsNull(dt.Name, ''),
            IsNull(d.CreatedDate, ''),
            IsNull(d.ReceivedDate, ''),
            IsNull(d.ImageLocation, ''),
            d.ApprovedFlag,
            IsNull(d.SupplementSeqNumber, 0),
            IsNull(d.FullSummaryExistsFlag, 0),
            IsNull(d.AgreedPriceMetCD, ''),
            IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                        AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), ''),
            IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), ''),
            IsNull(dt.EstimateTypeFlag, 0),
            IsNull(d.DirectionToPayFlag, 0),
            IsNull(d.FinalEstimateFlag, 0),
            IsNull(d.DuplicateFlag, 0),
            IsNull(d.EstimateTypeCD, ''),          
            casc.ServiceChannelCD,
            dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
            IsNull((SELECT dbo.ufnUtilityGetDateString(SysLastUpdatedDate)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                        AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), ''),
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpDocument tmpD                -- Added to return blank claim even if no documents exist
    LEFT JOIN dbo.utb_Document d on tmpD.DocumentID = d.DocumentID
    LEFT JOIN dbo.utb_User u on d.CreatedUserID = u.UserID
    LEFT JOIN dbo.utb_role r on d.CreatedUserRoleID = r.RoleID
    LEFT JOIN dbo.utb_Document_Type dt on D.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN dbo.utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID


    UNION ALL


    -- Select the Involved Level

    SELECT  8,
            2,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            IsNull(i.InvolvedID, 0),
            IsNull(i.NameFirst, ''),
            IsNull(i.NameLast, ''),
            IsNull(i.BusinessName, ''),
            IsNull(i.DayAreaCode, ''),
            IsNull(i.DayExchangeNumber, ''),
            IsNull(i.AddressCity, ''),
            IsNull(i.AddressState, ''),
            IsNull(i.AddressZip, ''),
            -- Involved Type
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)


    UNION ALL


    -- Select the Involved Type Level

    SELECT  9,
            8,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            IsNull(cai.InvolvedID, 0),
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            IsNull(irt.Name, ''),
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @ClaimAspectID AS ClaimAspectID, 1 AS EnabledFlag) AS parms
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (parms.ClaimAspectID = cai.ClaimAspectID AND parms.EnabledFlag = cai.EnabledFlag)
    LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 10,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document           
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  11,
            10,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  12,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Claim Vehicle
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Vehicle Safety Device
            NULL,
            -- Vehicle Impact
            NULL, NULL, NULL, NULL,
            -- Client Coverage Types
            NULL, NULL, NULL, NULL,
            -- Contact
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Involved
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Involved Type
            NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
				--Service Channel and Coverage data
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpReference

	UNION ALL

	select   distinct 
                13,
				2,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Vehicle
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Vehicle Safety Device
				NULL,
				-- Vehicle Impact
				NULL, NULL, NULL, NULL,
				-- Client Coverage Types
				NULL, NULL, NULL, NULL,
				-- Contact
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
				-- Document
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
				-- Involved
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Involved Type
				NULL,
				-- Metadata Header
				NULL,
				-- Columns
				NULL, NULL, NULL, NULL, NULL, NULL,
				-- Reference Data
				NULL,NULL,NULL,
				--Service Channel and Coverage data
				casc.ClaimAspectServiceChannelID,
				ISNULL(u.NameFirst,''),
				ISNULL(u.NameLast,''),
				ltrim(rtrim(u.NameFirst)) + ' ' + ltrim(rtrim(u.NameLast)),
				ISNULL(cast(casc.CreatedUserID as varchar(20)),''),
				ISNULL(convert(varchar(10),casc.CreatedDate,110),''),
				ISNULL(casc.DispositionTypeCD,''),
				ISNULL((SELECT convert(varchar, cas.StatusID)
                     FROM dbo.utb_claim_aspect_status cas
                     WHERE cas.ClaimAspectID = ca.ClaimAspectID
                       AND cas.ServiceChannelCD = casc.ServiceChannelCD
                       AND cas.StatusTypeCD = 'SC'), ''),
				ISNULL((SELECT s.Name
                     FROM dbo.utb_claim_aspect_status cas
                     LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
                     WHERE cas.ClaimAspectID = ca.ClaimAspectID
                       AND cas.ServiceChannelCD = casc.ServiceChannelCD
                       AND cas.StatusTypeCD = 'SC'), ''),
				ISNULL(cast(casc.EnabledFlag as varchar(01)),''),
				ISNULL(convert(varchar(10),casc.InspectionDate,110),''),
				ISNULL(convert(varchar(10),casc.ClientInvoiceDate,110),''),
				ISNULL(convert(varchar(10),casc.OriginalCompleteDate,110),''),
				ISNULL(convert(varchar(10),casc.OriginalEstimateDate,110),''),
				ISNULL(cast(casc.PrimaryFlag as varchar(01)),''),
				ISNULL(casc.ServiceChannelCD,''),
				ISNULL(urc.Name,''),
				ISNULL(cast(casc.WorkEndConfirmFlag as varchar(01)),''),
				ISNULL(convert(varchar(10),casc.WorkEndDate,110),''),
				ISNULL(convert(varchar(10),casc.WorkEndDateOriginal,110),''),
				ISNULL(cast(casc.WorkStartConfirmFlag as varchar(01)),''),
				ISNULL(Convert(varchar(10),casc.WorkStartDate,110),''),
				ISNULL(convert(varchar(10),ufnEst.OriginalExtendedAmt), ''),
				ISNULL((SELECT a.ReferenceID
				       FROM dbo.utb_assignment a
				       WHERE a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
				         AND AssignmentSequenceNumber = 1
				         AND a.CancellationDate IS NULL), ''),
				casc.SysLastUpdatedDate,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	from 			utb_Claim_Aspect ca
	
	inner join 		utb_Claim_Aspect_Service_Channel casc on (ca.ClaimAspectID = casc.CLaimAspectID and ca.ClaimAspectID = @ClaimAspectID)
	Inner JOIN		utb_User u on casc.CreatedUserID = u.USERID	
	inner join     dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc on casc.ServiceChannelCD = urc.Code
	left outer join dbo.ufnUtilityGetEstimateInformation(@ClaimAspectID, 'C', 'RepairTotal', 'TT') ufnEst on casc.ServiceChannelCD = ufnEst.ServiceChannelCD
	--left outer join dbo.utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
	--left join      utb_status s on cas.StatusID = s.StatusID
	WHERE casc.EnabledFlag = 1


	UNION ALL

	select   14,
				13,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Vehicle
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Vehicle Safety Device
				NULL,
				-- Vehicle Impact
				NULL, NULL, NULL, NULL,
				-- Client Coverage Types
				NULL, NULL, NULL, NULL,
				-- Contact
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
				-- Document
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
				-- Involved
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Involved Type
				NULL,
				-- Metadata Header
				NULL,
				-- Columns
				NULL, NULL, NULL, NULL, NULL, NULL,
				-- Reference Data
				NULL,NULL,NULL,
				--Service Channel and Coverage data
				casc.ClaimAspectServiceChannelID, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				ISNULL(cc.ClaimCoverageID, ''),
				ISNULL(cc.ClientCoverageTypeID, ''),
				ISNULL(cc.CoverageTypeCD,''),
				ISNULL(cast(cc.DeductibleAmt as varchar(20)),''),
				isnull(cast(cc.LimitAmt as varchar(20)),''),
				isnull(cast(cc.LimitDailyAmt as varchar(20)),''),
				ISNULL(cast(cc.MaximumDays as varchar(20)),''),
				ISNULL(cast(cascc.DeductibleAppliedAmt as varchar(20)),''),
				ISNULL(cast(cascc.LimitAppliedAmt as varchar(20)),''),
				IsNull(cct.ClientCode, ''),
				-- Claim Aspect Service Channel Concession
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

	from utb_Claim_Aspect ca	
	left join utb_Claim_Aspect_Service_Channel casc on (ca.ClaimAspectID = casc.CLaimAspectID and ca.ClaimAspectID = @ClaimAspectID)
	LEFT join utb_Claim_Aspect_Service_Channel_Coverage cascc on (casc.ClaimAspectServiceChannelID = cascc.ClaimAspectServiceChannelID)
	Left JOIN utb_Claim_Coverage cc On  cascc.ClaimCoverageID = cc.ClaimCoverageID 	
	left join utb_client_coverage_type cct on cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
	WHERE casc.EnabledFlag = 1 
	  AND cc.EnabledFlag = 1


	UNION ALL

	select   15,
				13,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Vehicle
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Vehicle Safety Device
				NULL,
				-- Vehicle Impact
				NULL, NULL, NULL, NULL,
				-- Client Coverage Types
				NULL, NULL, NULL, NULL,
				-- Contact
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
				-- Document
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
				-- Involved
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Involved Type
				NULL,
				-- Metadata Header
				NULL,
				-- Columns
				NULL, NULL, NULL, NULL, NULL, NULL,
				-- Reference Data
				NULL,NULL,NULL,
				--Service Channel and Coverage data
				cascc.ClaimAspectServiceChannelID, 
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Coverage
				NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
				-- Claim Aspect Service Channel Concession
				cascc.ClaimAspectServiceChannelConcessionID, 
				cascc.ClaimAspectServiceChannelID, 
				cascc.ConcessionReasonID, 
				(select fn.Name
				 from dbo.ufnUtilityGetReferenceCodes('utb_concession_reason', 'ConcessionReasonTypeCD') fn
				 where fn.code = cr.ConcessionReasonTypeCD), 
				cr.Description, 
				convert(decimal(9, 2), cascc.Amount), 
				cascc.SysLastUpdatedDate,
				cascc.Comments

	from utb_Claim_Aspect ca
	left join utb_claim_aspect_service_channel casc on (ca.ClaimAspectID = casc.ClaimAspectID and ca.ClaimAspectID = @ClaimAspectID)
	left join utb_Claim_Aspect_Service_Channel_Concession cascc on casc.ClaimAspectServiceChannelID = cascc.ClaimAspectServiceChannelID
	left join utb_concession_reason cr on cascc.ConcessionReasonID = cr.ConcessionReasonID
	WHERE cascc.EnabledFlag = 1 
	  AND casc.EnabledFlag = 1

    ORDER BY [Metadata!10!Entity], [Involved!8!InvolvedID], [ClaimAspectServiceChannel!13!ClaimAspectServiceChannelID], Tag
    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVehicleGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspClaimVehicleGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/