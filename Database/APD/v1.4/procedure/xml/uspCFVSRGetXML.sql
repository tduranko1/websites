-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFVSRGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFVSRGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspCFVSRGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns an XML stream containing all data necessary to style the Salvage Bid Report
*
* PARAMETERS:  
* (I) @ClaimAspectID      The vehicle
* (I) @UserID             The user id accessing the APD system
*
* RESULT SET:   An XML stream containing claim summary information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspCFVSRGetXML]
    @ClaimAspectID      udt_std_id_big,
    @UserID				udt_std_id_big
AS
BEGIN
    -- Declare internal variables
	DECLARE @ClaimNumber as varchar(100)
	DECLARE @LynxID as varchar(100)
	DECLARE @CarrierRepLName as varchar(50)
	DECLARE @CarrierRepFName as varchar(50)
	DECLARE @CarrierRepPhone as varchar(15)
	DECLARE @CarrierRepEmail as varchar(100)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)    
	DECLARE @InsuredLName as varchar(50)
	DECLARE @InsuredFName as varchar(50)
	DECLARE @InsuredBName as varchar(100)
	DECLARE @OwnerLName as varchar(50)
	DECLARE @OwnerFName as varchar(50)
	DECLARE @OwnerBName as varchar(100)
	DECLARE @OwnerZip as varchar(5)
	DECLARE @OwnerState as varchar(2)
	DECLARE @OwnerDayPhone as varchar(15)
	DECLARE @OwnerDayPhoneExt as varchar(5)
	DECLARE @OwnerNightPhone as varchar(15)
	DECLARE @OwnerNightPhoneExt as varchar(5)
	DECLARE @OwnerAltPhone as varchar(15)
	DECLARE @OwnerAltPhoneExt as varchar(5)
	DECLARE @LossState as varchar(2)
	DECLARE @LossDate as varchar(10)
	DECLARE @VehicleYear as varchar(4)
	DECLARE @VehicleMake as varchar(50)
	DECLARE @VehicleModel as varchar(50)
	DECLARE @UserName as varchar(100)
	DECLARE @UserPhone as varchar(15)
	DECLARE @UserEmail as varchar(100)
	DECLARE @ShopFaxNumber as varchar(15)
	DECLARE @CommunicationAddress as varchar(100)

	DECLARE @ShopName as varchar(100)
	DECLARE @ShopEmail as varchar(100)
	DECLARE @ShopPhone as varchar(25)
	DECLARE @ShopFax as varchar(25)

	-- 22Jun2012 - TVD - New fields added
	DECLARE @VehicleVIN as varchar(50)
    DECLARE @VehicleTag as varchar(50)
    DECLARE @VehicleMileage as varchar(50)
    DECLARE @InsuredPhone as VARCHAR(50)
    DECLARE @OwnerCellPhone as Varchar(15)
    DECLARE @Carrier as Varchar(100)
    
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFVSRGetXML'
    
    SELECT  @UserName = NameFirst + ' ' + NameLast,
			@UserPhone = PhoneAreaCode + PhoneExchangeNumber + PhoneUnitNumber,
			@UserEmail = EmailAddress
    FROM utb_user
	WHERE UserID = @UserID

	-- 06Aug2012 - TVD - Null format changes
	SELECT	@VehicleYear = cv.VehicleYear,
			@VehicleModel = cv.Model,
			@VehicleMake = cv.Make,
			@ClaimNumber = c.ClientClaimNumber,
			@LynxID = c.LynxID,
			@CarrierRepLName = cu.NameLast,
			@CarrierRepFName = cu.NameFirst,
			@CarrierRepPhone = '(' + cu.PhoneAreaCode + ') ' + cu.PhoneExchangeNumber + ' ' + cu.PhoneUnitNumber,
			@CarrierRepEmail = cu.EmailAddress,
		    @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
            @AuditorTitle = 'Material Damage Specialist',
            @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
            @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
			@LossDate = CONVERT(varchar, c.LossDate, 110),
			@LossState = c.LossState,
			@VehicleVIN = LTrim(RTrim(IsNull(cv.vin,''))),
			@VehicleTag = isNull(LicensePlateNumber, ''),
            @VehicleMileage = isNull(CONVERT(VARCHAR(50),Mileage), ''),
            @Carrier = (SELECT [Name] FROM utb_insurance WHERE InsuranceCompanyID = c.InsuranceCompanyID)	
	FROM utb_claim_aspect ca
	LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID 
	LEFT JOIN utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID 
	LEFT JOIN utb_user cu ON c.CarrierRepUserID = cu.UserID 
	LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
	WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT	@OwnerFName = LTrim(RTrim(IsNull(i.NameFirst, ''))),
			@OwnerLName = LTrim(RTrim(IsNull(i.NameLast, ''))),
			@OwnerBName = LTrim(RTrim(IsNull(i.BusinessName, ''))),
			@OwnerState = LTrim(RTrim(IsNull(i.AddressState, ''))),
			@OwnerZip = LTrim(RTrim(IsNull(i.AddressZip, ''))),
            @OwnerDayPhone = IsNull(LTRIM(RTRIM(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber)), ''),
            @OwnerDayPhoneExt = LTrim(RTrim(IsNull(i.DayExtensionNumber, ''))),
            @OwnerNightPhone = IsNull(LTRIM(RTRIM(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber)), ''), 
            @OwnerNightPhoneExt = LTrim(RTrim(IsNull(i.NightExtensionNumber, ''))),
            @OwnerAltPhone = IsNull(LTRIM(RTRIM(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber)), ''),
            @OwnerAltPhoneExt = LTrim(RTrim(IsNull(i.AlternateExtensionNumber, ''))),
            @OwnerCellPhone = IsNull(LTRIM(RTRIM(i.CellAreaCode + i.CellExchangeNumber + i.CellUnitNumber)), '')
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Owner'


    SELECT	@InsuredFName = LTrim(RTrim(IsNull(i.NameFirst, ''))),
			@InsuredLName = LTrim(RTrim(IsNull(i.NameLast, ''))),
			@InsuredBName = LTrim(RTrim(IsNull(i.BusinessName, ''))),
            @InsuredPhone = '(' + LTrim(RTrim(IsNull(i.DayAreaCode, ''))) + ') ' + LTrim(RTrim(IsNull(i.DayExchangeNumber, ''))) + ' ' + LTrim(RTrim(IsNull(i.DayUnitNumber, '')))
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Insured'
      
      
    SELECT	@ShopFaxNumber = sl.FaxAreaCode + sl.FaxExchangeNumber + sl.FaxUnitNumber
			, @CommunicationAddress = sl.EmailAddress
			, @ShopName = sl.Name 
			, @ShopEmail = sl.EmailAddress
			, @ShopPhone = sl.PhoneAreaCode + '-' + sl.PhoneExchangeNumber + '-' + sl.PhoneUnitNumber
			, @ShopFax = sl.FaxAreaCode + '-' + sl.FaxExchangeNumber + '-' + sl.FaxUnitNumber
    FROM utb_assignment a
    LEFT JOIN utb_claim_aspect_service_channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID 
    --LEFT JOIN utb_claim_vehicle cv on casc.ClaimAspectID = cv.ClaimAspectID 
    LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID 
    WHERE ca.ClaimAspectID = @ClaimAspectID 
      AND casc.ServiceChannelCD IN ('PS', 'RRP')
      AND casc.EnabledFlag = 1 
      AND a.assignmentsequencenumber = 1
      AND a.CancellationDate IS NULL

    SELECT  1 as Tag,
            NULL as Parent,
            isNull(@ClaimNumber, '') as [Root!1!ClaimNumber],
            isNull(@LynxID, '') as [Root!1!LynxID],
			isNull(@Carrier, '') as [Root!1!Carrier],
			isNull(@CarrierRepLName, '') as [Root!1!CarrierRepLName],
			isNull(@CarrierRepFName, '') as [Root!1!CarrierRepFName],
			isNull(@CarrierRepPhone, '') as [Root!1!CarrierRepPhone],
			isNull(@CarrierRepEmail, '') as [Root!1!CarrierRepEmail],
			isNull(@AuditorName,'') as[Root!1!AuditorName],
            isNull(@AuditorTitle,'') as [Root!1!AuditorTitle],
            isNull(@AuditorPhone,'') as [Root!1!AuditorPhone],
            isNull(@AuditorEmail,'') as [Root!1!AuditorEmail],
			isNull(@InsuredLName, '') as [Root!1!InsuredLName],
			isNull(@InsuredFName, '') as [Root!1!InsuredFName],
			isNull(@InsuredBName, '') as [Root!1!InsuredBName],
			isNull(@OwnerLName, '') as [Root!1!OwnerLName],
			isNull(@OwnerFName, '') as [Root!1!OwnerFName],
			isNull(@OwnerBName, '') as [Root!1!OwnerBName],
			isNull(@OwnerZip, '') as [Root!1!OwnerZip],
			isNull(@OwnerState, '') as [Root!1!OwnerState],
			isNull(@OwnerDayPhone, '') as [Root!1!OwnerDayPhone],
			isNull(@OwnerDayPhoneExt, '') as [Root!1!OwnerDayPhoneExt],
			isNull(@OwnerNightPhone, '') as [Root!1!OwnerNightPhone],
			isNull(@OwnerNightPhoneExt, '') as [Root!1!OwnerNightPhoneExt],
			isNull(@OwnerAltPhone, '') as [Root!1!OwnerAltPhone],
			isNull(@OwnerAltPhoneExt, '') as [Root!1!OwnerAltPhoneExt],
			isNull(@LossState, '') as [Root!1!LossState],
			isNull(@LossDate, '') as [Root!1!LossDate],
			isNull(@VehicleYear, '') as [Root!1!VehicleYear],
			isNull(@VehicleMake, '') as [Root!1!VehicleMake],
			isNull(@VehicleModel, '') as [Root!1!VehicleModel],
			isNull(@UserName, '') as [Root!1!UserName],
			isNull(@UserPhone, '') as [Root!1!UserPhone],
			isNull(@UserEmail, '') as [Root!1!UserEmail],
			ISNULL(@ShopName, '') as [Root!1!ShopName],
			ISNULL(@ShopFaxNumber, '') as [Root!1!ShopFaxNumber],
			ISNULL(@ShopPhone, '') as [Root!1!ShopPhone],
			ISNULL(@ShopFax, '') as [Root!1!ShopFax],
			ISNULL(@ShopEmail, '') as [Root!1!ShopEmail],
            ISNULL(@CommunicationAddress, '') as [Root!1!CommunicationAddress],
			ISNULL(@VehicleVIN, '') as [Root!1!VehicleVIN],
			ISNULL(@VehicleTag, '') as [Root!1!VehicleTag],
            ISNULL(@VehicleMileage, '') AS [Root!1!VehicleMileage],
            ISNULL(@InsuredPhone, '') AS [Root!1!InsuredPhone],
            isNull(@OwnerCellPhone, '') as [Root!1!OwnerCellPhone]

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFVSRGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFVSRGetXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCFVSRGetXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/