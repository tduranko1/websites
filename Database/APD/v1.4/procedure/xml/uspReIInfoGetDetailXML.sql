-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReIInfoGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspReIInfoGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspReIInfoGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspReIInfoGetDetailXML
    @ClaimAspectID      bigint
AS
BEGIN
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error      AS INT
    DECLARE @rowcount   AS INT
    DECLARE @Now        AS datetime
    DECLARE @ProcName   AS varchar(30)       -- Used for raise error stmts 
    DECLARE @ReIRequestedBy AS varchar(150)
    DECLARE @ShopName   AS udt_std_name
    DECLARE @ShopCity   AS udt_addr_city
    DECLARE @ProgramManagerEmailAddress AS udt_web_email

    SET @ProcName = 'uspReIInfoGetDetailXML'
    SET @Now = CURRENT_TIMESTAMP
    
    DECLARE @tmpReIRequestData TABLE 
    (
        LynxID              bigint  NOT NULL,
        ClaimAspectNumber   tinyInt NOT NULL,
        ReIRequestedBy      varchar(100) NULL,
        ShopName            varchar(100) NULL,
        ShopCity            varchar(50)  NULL,
        ProgramManagerEmailAddress varchar(100) NULL
    )
    
    INSERT INTO @tmpReIRequestData
    (LynxID, ClaimAspectNumber)
    SELECT LynxID, ClaimAspectNumber
    FROM dbo.utb_claim_aspect
    WHERE ClaimAspectID = @ClaimAspectID
    
    -- check if there was any reinspection request made for the claimAspect
    IF EXISTS(SELECT d.DocumentID
                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                Left Outer join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                WHERE casc.ClaimAspectID = @ClaimAspectID
                  AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                  AND d.ReinspectionRequestFlag = 1)
    BEGIN
        -- Now grab the assignment information
        
        -- Get the shop location id for the last active assignment
        SELECT TOP 1 @ShopName = sl.Name,
                     @ShopCity = sl.AddressCity,
                     @ProgramManagerEmailAddress = u.EmailAddress 
        FROM dbo.utb_assignment a
    	/*********************************************************************************
    	Project: 210474 APD - Enhancements to support multiple concurrent service channels
    	Note:	Added utb_Claim_Aspect_Service_Channel to get the column "CLaimAspectID"
    		used in the WHERE clause
    		M.A. 20061110
    	*********************************************************************************/
    	INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
        LEFT JOIN dbo.utb_user u ON (sl.ProgramManagerUserID = u.UserID)
        WHERE casc.ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
          AND a.CancellationDate IS NULL
        ORDER BY AssignmentDate DESC
        
        SELECT TOP 1 @ReIRequestedBy = isNull(u.NameLast, '') + ', ' + isNull(u.NameFirst, '')
        FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_user u ON (d.ReinspectionRequestingUserID = u.UserID)
        Left Outer join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        WHERE casc.ClaimAspectID = @ClaimAspectID
          AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
          AND d.ReinspectionRequestFlag = 1
        ORDER BY d.ReinspectionRequestDate DESC
        
        -- Now update the temp table
        UPDATE @tmpReIRequestData
        SET ReIRequestedBy = @ReIRequestedBy,
            ShopName = @ShopName,
            ShopCity = @ShopCity,
            ProgramManagerEmailAddress = @ProgramManagerEmailAddress
        
    END
    
    -- Final Select
    SELECT
        1                                       AS Tag,
        NULL                                    AS Parent,
        --Root
        @ClaimAspectID                          AS [Root!1!ClaimAspectID],
        --Reinspection request information
        NULL                                    AS [ReIRequest!2!LynxID],
        NULL                                    AS [ReIRequest!2!ClaimAspectNumber],
        NULL                                    AS [ReIRequest!2!RequestedBy],
        NULL                                    AS [ReIRequest!2!ShopName],
        NULL                                    AS [ReIRequest!2!ShopCity],
        NULL                                    AS [ReIRequest!2!ProgramManagerEmailAddress]

    UNION ALL
    
    SELECT 
        2,
        1,
        -- Root
        NULL,
        -- Reinspection request information
        LynxID,
        ClaimAspectNumber,
        ReIRequestedBy,
        ShopName,
        ShopCity,
        ProgramManagerEmailAddress
    FROM @tmpReIRequestData
    
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReIInfoGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspReIInfoGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/