-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcGetJobDetailsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPartnerSvcGetJobDetailsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPartnerSvcGetJobDetailsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns the job status details for a specific file waiting to be processed by the 
*				PartnerService.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspPartnerSvcGetJobDetailsWSXML]
    @vFileName as VARCHAR(255)
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspPartnerSvcGetJobDetailsWSXML'
    
    SET @ApplicationCD = 'APDPartner'

    -- Validation
    -- None
    
    -- Begin XML Select
    SELECT  
			1 AS Tag
            , NULL AS Parent
            , JobID AS [Root!1!JobID]
            , InscCompID AS [Root!1!InscCompID]

            , NULL AS [Job!2!LynxID]
            , NULL AS [Job!2!ClaimNumber]
            , NULL AS [Job!2!FromPartner]
            , NULL AS [Job!2!ToPartner]
            , NULL AS [Job!2!IODirection]
            , NULL AS [Job!2!JobXSLFile]
            , NULL AS [Job!2!ResponseXSLFile]
            , NULL AS [Job!2!JobStatus]
            , NULL AS [Job!2!JobStatusDetails]
            , NULL AS [Job!2!JobPriority]
            , NULL AS [Job!2!JobCreatedDate]
            , NULL AS [Job!2!JobProcessedDate]
            , NULL AS [Job!2!JobTransformedDate]
            , NULL AS [Job!2!JobTransmittedDate]
            , NULL AS [Job!2!JobArchivedDate]
            , NULL AS [Job!2!JobFinishedDate]
			, NULL AS [Job!2!ResponseRequired]
			, NULL AS [Job!2!Method]
			, NULL AS [Job!2!Enabled]
    FROM  
		utb_partnersvc_jobs
    WHERE 
		EnabledFlag = 1
		AND JobID = @vFileName
    
    UNION ALL
    
    SELECT  
			2 AS Tag
            , 1 AS Parent
            , NULL
            , NULL
            , NULL
            , ClaimNumber
            , FromPartnerID
            , ToPartnerID
            , IODirection
            , JobXSLFile
            , ResponseXSLFile
            , JobStatus
            , JobStatusDetails
            , JobPriority
            , JobCreatedDate
            , JobProcessedDate
            , JobTransformedDate
            , JobTransmittedDate
            , JobArchivedDate
            , JobFinishedDate
            , ResponseRequired
            , Method
            , EnabledFlag
    FROM  
		utb_partnersvc_jobs 
    WHERE 
		EnabledFlag = 1
		AND JobID = @vFileName
    ORDER BY 
		[Job!2!Method], tag
	 FOR XML EXPLICIT
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcGetJobDetailsWSXML' AND type = 'P')
BEGIN
--    GRANT EXECUTE ON dbo.uspPartnerSvcGetJobDetailsWSXML TO 
--        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspPartnerSvcGetJobDetailsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/