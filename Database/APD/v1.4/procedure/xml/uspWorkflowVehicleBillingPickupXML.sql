 -- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspWorkflowVehicleBillingPickupXML' AND type = 'P') 
BEGIN
    DROP PROCEDURE dbo.uspWorkflowVehicleBillingPickupXML
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowVehicleBillingPickupXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves billing items for LynxID entered
*
* PARAMETERS:  
* (I) @ClaimAspectID        LynxID to pick up billing for 
* (I) @ToDate               The date up to which include billing records for
*
* RESULT SET:   XML document detailing all information needed to generate an invoice
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspWorkflowVehicleBillingPickupXML
    @ClaimAspectID      udt_std_id_big,
    @ToDate             varchar(30),
    @UserID             udt_std_id
AS
BEGIN
    -- This procedure finds all the records that haven't been picked up yetfor @LynxID and returns them in an XML stream.
    -- Declare internal variables

    DECLARE @ClaimAspectTypeIDClaim         udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       udt_std_id
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @VehicleNumber                  udt_std_int
    DECLARE @InvoiceSeqNumber               udt_std_int_tiny
    DECLARE @TotalFeeAmount                 udt_std_money
    DECLARE @TotalInvoiceAmount             udt_std_money
    DECLARE @DefaultLynxShopID              udt_std_id_big
    DECLARE @DefaultCEIShopID               udt_std_id_big    
    
    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 
    DECLARE @now                            udt_std_datetime

    SET @ProcName = 'uspWorkflowBillingPickUp'


    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
    
    
    -- Validate UserID
    IF ((SELECT dbo.ufnUtilityIsUserActive(@UserID, 'APD', NULL)) = 0)
    BEGIN
        -- Invalid User ID
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate @ClaimAspectID if it was passed in    
    IF (@ClaimAspectID IS NULL) OR (NOT EXISTS (SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName,@ClaimAspectID)
        RETURN
    END
    
    -- Validate and prepare @ToDate
    IF @ToDate IS NOT NULL
    BEGIN
        IF IsDate(@ToDate) = 0
        BEGIN
            -- Invalid ToDate
            RAISERROR('101|%s|@ToDate|%s', 16, 1, @ProcName, @ToDate)
            RETURN
        END
        ELSE
        BEGIN
            -- Ignore any time from the date passed in and set to midnight
            SET @ToDate = Convert(varchar(30), Convert(datetime, @ToDate), 101) + ' 11:59:59 PM'
        END
    END
    ELSE
    BEGIN
        -- @ToDate wasn't passed in
        RAISERROR('101|%s|@ToDate|%s', 16, 1, @ProcName, @ToDate)
        RETURN
    END
        
    
     -- Get Claim Aspect Type for use later
    SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

       
    -- Resolve LynxID and Vehicle number from Claim Aspect ID
    SELECT @LynxID = LynxID, @VehicleNumber = ClaimAspectNumber
    FROM dbo.utb_claim_aspect
    WHERE ClaimAspectID = @ClaimAspectID
      
	
	
    -- Get the next available sequence number for the invoice.
	  SET @InvoiceSeqNumber = (SELECT ISNULL(MAX(SupplementSeqNumber), 0) + 1
                             FROM dbo.utb_document d INNER JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
                                                     INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                                                     INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
                             WHERE casc.ClaimAspectID = @ClaimAspectID
                               AND casc.EnabledFlag = 1--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
                               AND dt.Name = 'Invoice')

    
    
    -- Retrieve the default Shop ID to be used if a PayeeID is 0.
    SELECT @DefaultLynxShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_Lynx_ShopLocationID'
    
    
    -- Retrieve the defalt Shop ID to be used if a Payee is a CEI shop and the carrier does not use CEI shops.
    SELECT @DefaultCEIShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_CEI_ShopLocationID'
        
    
    DECLARE @tmpIdentity TABLE (ID          smallint          IDENTITY(1,1),
                                PayeeID     bigint            NULL)
    
    
    DECLARE @tmpInvoice TABLE (TransactionID     smallint            NULL,
                               InvoiceID         bigint          NOT NULL,
                               PayeeID           bigint              NULL,
                               Amount            money           NOT NULL,
                               ItemTypeCD        varchar(4)      NOT NULL)
                              
    
    
    DECLARE @tmpIngresTransaction TABLE (TransactionID    smallint          NULL,       
                                         ItemTypeCD       varchar(4)    NOT NULL,
                                         PayeeID          bigint            NULL, 
                                         InvoiceDesc      varchar(250)      NULL,
                                         Amount           money         NOT NULL,
                                         DispatchNumber   varchar(20)       NULL,
                                         ItemizeFlag      bit               NULL)
                                         
    
    -- Gather all items to be included on this invoice.
    INSERT INTO @tmpInvoice (InvoiceID, PayeeID, Amount, ItemTypeCD)
    SELECT InvoiceID, 
           ISNULL(PayeeID, 0),
           Amount, 
           ItemTypeCD
    FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                           INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                           INNER JOIN dbo.utb_insurance ins ON c.InsuranceCompanyID = ins.InsuranceCompanyID
                           --INNER JOIN dbo.utb_client_assignment_type cat ON ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
                           --                                             AND c.InsuranceCompanyID = cat.InsuranceCompanyID
                           INNER JOIN     (
                                            select      c.LynxID,
                                                        c.InsuranceCompanyID,
                                                        ca.ClaimAspectID,
                                                        ca.ClaimAspectTypeID,
                                                        casc.ClaimAspectServiceChannelID,
                                                        casc.ServiceChannelCD,
                                                        csc.ClientAuthorizesPaymentFlag,
                                                        csc.InvoicingModelBillingCD
                                            
                                            
                                            from       utb_Claim_Aspect_Service_Channel casc
                                            
                                            inner join  utb_Claim_Aspect ca
                                            on          ca.ClaimAspectID = casc.ClaimAspectID
                                            and         casc.PrimaryFlag = 1 -- pick only the primary service channel for the claim
                                            and         casc.EnabledFlag = 1 -- Only pick the enabled service channel
                                            
                                            inner join  utb_Claim c
                                            on          c.LynxID = ca.LynxID
                                            
                                            inner join  utb_client_service_channel csc
                                            on          c.InsuranceCompanyID = csc.InsuranceCompanyID
                                            and         casc.ServiceChannelCD = csc.ServiceChannelCD
                                          ) cat
                        on                cat.LynxID = ca.LynxID
                        and               cat.ClaimAspectID = ca.ClaimAspectID

    WHERE i.ClaimAspectID = @ClaimAspectID
      AND i.EnabledFlag = 1
      AND ((i.ItemTypeCD <> 'F' AND ins.InvoicingModelPaymentCD = 'C') OR (i.ItemTypeCD = 'F' AND cat.InvoicingModelBillingCD = 'C'))
      AND i.EntryDate < @ToDate
      AND i.StatusCD = 'APD'  
      

    -- Insert payments totaled by Payee
    INSERT INTO @tmpIngresTransaction
    SELECT NULL,
           'P',
           ISNULL(PayeeID, 0),
           NULL,
           SUM(Amount),
           NULL,
           NULL
    FROM  @tmpInvoice
    WHERE ItemTypeCD <> 'F'     
    GROUP BY PayeeID  
    
    
    -- Create a unique identifier for each payee id.
    INSERT INTO @tmpIdentity (PayeeID)
    SELECT PayeeID FROM @tmpIngresTransaction           
                 
                 
    -- Add most recent dispatch number for this ClaimAspectID/PayeeID.
    UPDATE @tmpIngresTransaction
    SET TransactionID = t2.ID,
        DispatchNumber = i.DispatchNumber,
        InvoiceDesc = LEFT((ISNULL(i.PayeeName, '') + ' - ' + ISNULL(i.Description, '')), 250) + ' Claim',  
        ItemizeFlag = i.ItemizeFlag                           
    FROM @tmpIngresTransaction t INNER JOIN dbo.utb_invoice i ON t.PayeeID = i.PayeeID
                                 INNER JOIN @tmpIdentity t2 ON t.PayeeID = t2.PayeeID
    WHERE i.InvoiceID = (SELECT MAX(InvoiceID) FROM dbo.utb_invoice WHERE ClaimAspectID = @ClaimAspectID
                                                                     AND PayeeID = t.PayeeID)
                                                             
        
    -- Determine the Final PayeeID to be sent to Ingres.   	
    UPDATE @tmpIngresTransaction
    SET PayeeID = (SELECT CASE
                            WHEN t.PayeeID <> 0 AND ProgramTypeCD = 'CEI' THEN @DefaultCEIShopID
                            WHEN t.PayeeID = 0 THEN @DefaultLynxShopID
                            ELSE t.PayeeID
                          END)
    FROM @tmpIngresTransaction t INNER JOIN dbo.utb_assignment a ON t.PayeeID = a.ShopLocationID
    WHERE a.AssignmentID = (SELECT MAX(a1.AssignmentID) 
                            FROM dbo.utb_assignment a1
							/*********************************************************************************
							Project: 210474 APD - Enhancements to support multiple concurrent service channels
							Note:	Added reference to utb_Claim_Aspect_Service Channel to provide the value
									of "ClaimAspectID" used in the WHERE clause
									M.A. 20061124
							*********************************************************************************/
							INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a1.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                            WHERE casc.ClaimAspectID = @ClaimAspectID
                              AND a1.ShopLocationID = t.PayeeID)
    
    
    -- Insert fees.
    INSERT INTO @tmpIngresTransaction
    SELECT (SELECT TOP 1 TransactionID FROM @tmpIngresTransaction),
           'F',
           0,
           CASE
             WHEN ClientFeeCode IS NOT NULL THEN ClientFeeCode
             WHEN InvoiceDescription IS NOT NULL THEN InvoiceDescription
             ELSE ISNULL(Description, '')
           END,
           t.Amount,
           DispatchNumber,
           i.ItemizeFlag
    FROM @tmpInvoice t INNER JOIN dbo.utb_invoice i ON t.InvoiceID = i.InvoiceID                            
    WHERE t.ItemTypeCD = 'F'
      
    UPDATE @tmpInvoice
    SET TransactionID = t2.TransactionID
    FROM @tmpInvoice t1 INNER JOIN @tmpIngresTransaction t2 ON t1.PayeeID = t2.PayeeID    
     
--     select * from @tmpIngresTransaction
--     select * from @tmpInvoice
--     return
          
    
    DECLARE @NTUserID   varchar(10)
    
    SELECT @NTUserID = LogonID 
    FROM dbo.utb_user_application ua INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
    WHERE UserID = @UserID
      AND a.Name = 'APD'
    
      
    -- Begin XML Select        
    SELECT  1 AS Tag,
            NULL AS Parent,            
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @ToDate AS [Root!1!ToDate],
            @UserID AS [Root!1!UserID],
            @NTUserID AS [Root!1!NTUserID],
            
            -- Claim Information level
            NULL AS [Claim!2!LynxID],
            NULL AS [Claim!2!ClaimAspectID],
            NULL AS [Claim!2!CarrierName],
            NULL AS [Claim!2!CarrierOfficeName],
            NULL AS [Claim!2!CarrierAddress1],
            NULL AS [Claim!2!CarrierAddress2],
            NULL AS [Claim!2!CarrierCity],
            NULL AS [Claim!2!CarrierState],
            NULL AS [Claim!2!CarrierZip],
            NULL AS [Claim!2!CarrierClaimNumber],
            NULL AS [Claim!2!CarrierRepNameFirst],
            NULL AS [Claim!2!CarrierRepNameLast],
            NULL AS [Claim!2!CarrierInvoicingModelPaymentCD],
            NULL AS [Claim!2!InsuredNameFirst],
            NULL AS [Claim!2!InsuredNameLast],
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!LossDate],
            NULL AS [Claim!2!PolicyNumber],
            
            -- Exposure Information Level
            NULL AS [Exposure!3!VehicleNumber],
            NULL AS [Exposure!3!ClaimAspectID],
            NULL AS [Exposure!3!Claimant],
            NULL AS [Exposure!3!Vehicle],
            NULL AS [Exposure!3!InvoicingModelBillingCD],
            
            -- Invoice Level
            NULL AS [Invoice!4!LynxID],                 -- LynxID-VehicleNumber       
            NULL AS [Invoice!4!IngresAccountingID],
            NULL AS [Invoice!4!TotalFeeAmount],
            NULL AS [Invoice!4!InvoiceNumber],          -- LynxID-VehicleNumber-SequenceNumber
            NULL AS [Invoice!4!InvoiceSeqNumber],
            NULL AS [Invoice!4!InvoiceDate],
            NULL AS [Invoice!4!Timestamp],
            NULL AS [Invoice!4!TotalInvoiceAmount],
            NULL AS [Invoice!4!PertainsTo],
            
            -- Invoice Line Item Level
            NULL AS [LineItem!5!TransactionID],
            NULL AS [LineItem!5!PayeeID],
            NULL AS [LineItem!5!Amount],                -- Total Payment
            NULL AS [LineItem!5!DispatchNumber],
            NULL AS [LineItem!5!InvoiceDescription],
            NULL AS [LineItem!5!ItemTypeCD],
            NULL AS [LineItem!5!ItemizeFlag],
            
            -- Raw Invoice
            NULL AS [RawInvoice!6!TransactionID],
            NULL AS [RawInvoice!6!InvoiceID]
            
             

    UNION ALL            

    -- Select Claim Level                
    SELECT  2,
            1,
            NULL, NULL, NULL, NULL,
            
            -- Claim level
            IsNull(c.LynxID, ''),
            IsNull(ca.ClaimAspectID, ''),
            IsNull(ic.Name, ''),
            IsNull(o.Name, ''),
            IsNull(ic.Address1, ''),
            IsNull(ic.Address2, ''),
            IsNull(ic.AddressCity, ''),
            IsNull(ic.AddressState, ''),
            IsNull(ic.AddressZip, ''),
            IsNull(c.ClientClaimNumber, ''),  --Project:210474 APD Modified when we did the code merge M.A.20061124
            IsNull(cu.NameFirst, ''),
            IsNull(cu.NameLast, ''),
            IsNull(ic.InvoicingModelPaymentCD, ''),
            IsNull((SELECT IsNull(i.NameFirst, '')
               FROM dbo.utb_claim_aspect cas
               LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
               LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
               WHERE cas.LynxID = c.LynxID
                  AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                  AND cai.EnabledFlag = 1
                  AND irt.Name = 'Insured'), ''),
            IsNull((SELECT IsNull(i.NameLast, '')
               FROM dbo.utb_claim_aspect cas
               LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
               LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
               WHERE cas.LynxID = c.LynxID
                  AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                  AND cai.EnabledFlag = 1
                  AND irt.Name = 'Insured'), ''),
            IsNull((SELECT IsNull(i.BusinessName, '')
               FROM dbo.utb_claim_aspect cas
               LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
               LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
               WHERE cas.LynxID = c.LynxID
                  AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                  AND cai.EnabledFlag = 1
                  AND irt.Name = 'Insured'), ''),
            IsNull(CONVERT(varchar(30), c.LossDate, 101), ''),
            IsNull(c.PolicyNumber, ''),
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Line Item Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Raw Invoice
            NULL, NULL

      FROM  (SELECT @LynxID AS LynxID) AS parms 
      LEFT JOIN dbo.utb_claim c ON (parms.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID AND @ClaimAspectTypeIDClaim = ca.ClaimAspectTypeID AND 0 = ca.ClaimAspectNumber)
      LEFT JOIN dbo.utb_insurance ic ON (c.InsuranceCompanyID = ic.InsuranceCompanyID)
      LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID)
      LEFT JOIN dbo.utb_user cu ON (c.CarrierRepUserID = cu.UserID)
      LEFT JOIN dbo.utb_office o ON (cu.OfficeID = o.OfficeID)            

    UNION ALL            

    -- Select Exposure Level                
    SELECT  3,
            2,
            NULL, NULL, NULL, NULL,
            
            -- Claim level
            ca.LynxID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            IsNull(ca.ClaimAspectNumber, ''),
            IsNull(ca.ClaimAspectID, ''),
            IsNull((SELECT Top 1 CASE
                                    WHEN i.BusinessName IS NOT NULL THEN i.BusinessName
                                    ELSE IsNull(i.NameFirst, '') + ' ' + ISNULL(i.NameLast, '')
                                 END
               FROM dbo.utb_claim_aspect cas
               LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
               LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
               LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
               WHERE cas.ClaimAspectID = ca.ClaimAspectID
                 AND cai.EnabledFlag = 1
                 AND irt.Name IN ('Insured', 'Claimant')), ''),           
            LTRIM(IsNull(CONVERT(varchar(4), cv.VehicleYear), '') + ' ' + IsNull(cv.Make, '') + ' ' + IsNull(cv.Model, '') + ' ' + 
                 IsNull(cv.BodyStyle, '') + ' ' + CASE WHEN cv.VIN IS NOT NULL THEN 'VIN: ' + cv.VIN ELSE '' END),
            ISNULL(cat.InvoicingModelBillingCD, ''),
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Line Item Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Raw Invoice
            NULL, NULL
            
      FROM  (SELECT @ClaimAspectID AS ClaimAspectID) AS parms 
      LEFT JOIN dbo.utb_claim_aspect ca ON (parms.ClaimAspectID = ca.ClaimAspectID AND @ClaimAspectTypeIDVehicle = ca.ClaimAspectTypeID)
      LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
      --LEFT JOIN dbo.utb_client_assignment_type cat ON ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
      --                                            AND c.InsuranceCompanyID = cat.InsuranceCompanyID
      LEFT OUTER JOIN (
                        select      c.LynxID,
                                    c.InsuranceCompanyID,
                                    ca.ClaimAspectID,
                                    ca.ClaimAspectTypeID,
                                    casc.ClaimAspectServiceChannelID,
                                    casc.ServiceChannelCD,
                                    csc.ClientAuthorizesPaymentFlag,
                                    csc.InvoicingModelBillingCD
                        
                        
                        from       utb_Claim_Aspect_Service_Channel casc
                        
                        inner join  utb_Claim_Aspect ca
                        on          ca.ClaimAspectID = casc.ClaimAspectID
                        and         casc.PrimaryFlag = 1 -- pick only the primary service channel for the claim
                        and         casc.EnabledFlag = 1 -- Only pick the enabled service channel
                        
                        inner join  utb_Claim c
                        on          c.LynxID = ca.LynxID
                        
                        inner join  utb_client_service_channel csc
                        on          c.InsuranceCompanyID = csc.InsuranceCompanyID
                        and         casc.ServiceChannelCD = csc.ServiceChannelCD
                      )cat
      on              cat.LynxID = ca.LynxID
      and             cat.ClaimAspectID = ca.ClaimAspectID

      WHERE ca.EnabledFLag = 1
        AND ca.ExposureCD <> 'N'    -- Exclude non-exposure vehicles
            

    UNION ALL            

    -- Select Invoice Level                
    SELECT  4,
            1,
            NULL, NULL, NULL, NULL,
            
            -- Claim level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            CONVERT(varchar(20), @LynxID) + '-' + CONVERT(varchar(20), @VehicleNumber),
            ISNULL(i.IngresAccountingID, 0),
            CONVERT(varchar(20), ISNULL((SELECT SUM(Amount) FROM @tmpIngresTransaction WHERE ItemTypeCD = 'F'), 0.00)),    -- Total fee amount.
            CONVERT(varchar(20), @LynxID) + '-' + CONVERT(varchar(3), @VehicleNumber) + '-' + CONVERT(varchar(3), @InvoiceSeqNumber), 
			      @InvoiceSeqNumber,
			      CONVERT(varchar(20), @now, 101),
			      (SELECT dbo.ufnUtilityGetDateString(@now)),
			      CONVERT(varchar(20), ISNULL((SELECT SUM(Amount) FROM @tmpIngresTransaction), 0.00)),     -- Total invoice amount.
			      cat.Code + CONVERT(varchar(4), ca.ClaimAspectNumber),
            
            -- Invoice Line Item Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Raw Invoice
            NULL, NULL
            
     FROM dbo.utb_claim_aspect ca INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                  INNER JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
                                  INNER JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
     WHERE ca.ClaimAspectID = @ClaimAspectID
     
     
    UNION ALL            

    -- Select Invoice Line Item Level                
    SELECT  5,
            4,
            NULL, NULL, NULL, NULL,
            
            -- Claim level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Line Item Level
            TransactionID,
            ISNULL(CONVERT(varchar(20), PayeeID), ''), 
            CONVERT(varchar(20), ISNULL(Amount, 0.00)), 
            ISNULL(DispatchNumber, '0'),
            ISNULL(InvoiceDesc, ''),
            ItemTypeCD,
            ItemizeFlag,
            
            -- Raw Invoice
            NULL, NULL
                                    
            
	  FROM @tmpIngresTransaction
            
    UNION ALL            

    -- Select Raw Invoice Level                
    SELECT  6,
            1,
            NULL, NULL, NULL, NULL,
            
            -- Claim level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Line Item Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Raw Invoice
            TransactionID, 
            InvoiceID                                    
            
	  FROM @tmpInvoice

    ORDER BY [Claim!2!LynxID], Tag
    
    --FOR XML EXPLICIT          -- (Commented for client-side)
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error Selecting XML
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    
   
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowVehicleBillingPickupXML' AND type = 'P') BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowVehicleBillingPickupXML TO ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

