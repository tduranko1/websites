-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTDealerGetDetailXML 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTDealerGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Prce
* FUNCTION:     Retrieve a specific Dealer
*
* PARAMETERS:  
* (I) @DealerID          The Dealer's identity
*
* RESULT SET:
* DealerID                           The Dealer's identity
* Address1                           The Dealer's address line 1
* Address2                           The Dealer's address line 2
* AddressCity                        The Dealer's address city
* AddressCounty                      The Dealer's address county
* AddressState                       The Dealer's address state
* AddressZip                         The Dealer's address zip code
* FaxAreaCode                        The Dealer's fax area code
* FaxExchangeNumber                  The Dealer's fax exchange number
* FaxExtensionNumber                 The Dealer's fax extension number
* FaxUnitNumber                      The Dealer's fax unit number
* Name                               The Dealer's name
* PhoneAreaCode                      The Dealer's phone area code
* PhoneExchangeNumber                The Dealer's phone exchange number
* PhoneExtensionNumber               The Dealer's phone extension number
* PhoneUnitNumber                    The Dealer's phone unit number
* SysLastUserID                      The Dealer's updating user identity
* SysLastUpdatedDate                 The Dealer's last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTDealerGetDetailXML
(
	@DealerID                    	udt_std_int_big
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTDealerGetDetailXML'


     /*************************************************************************************
    *  Gather Meta Data
    **************************************************************************************/  
    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'ShopInfo',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_dealer' AND Column_Name IN 
            ('DealerID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressCounty',
             'AddressState',
             'AddressZip',
             'FaxAreaCode',
             'FaxExchangeNumber',
             'FaxExtensionNumber',
             'FaxUnitNumber',
             'Name',
             'PhoneAreaCode',
             'PhoneExchangeNumber',
             'PhoneExtensionNumber',
             'PhoneUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    /*************************************************************************************
    *  Gather Reference Data
    **************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )

    -- States
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name)
        Select Distinct 'State', DisplayOrder, StateCode, StateValue
        From dbo.utb_state_code
        Where EnabledFlag = 1
        Order By DisplayOrder

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

  

    SELECT  
        1                                       AS Tag,
        Null                                    AS Parent,
        --Root
        @DealerID                               AS [Root!1!DealerID],
        --Dealer
        Null                                    AS [Dealer!2!DealerID],                                                            
        Null                                    AS [Dealer!2!Address1],                                                            
        Null                                    AS [Dealer!2!Address2],                                                            
        Null                                    AS [Dealer!2!AddressCity],                                                         
        Null                                    AS [Dealer!2!AddressCounty],                                                       
        Null                                    AS [Dealer!2!AddressState],                                                        
        Null                                    AS [Dealer!2!AddressZip],                                                          
        Null                                    AS [Dealer!2!FaxAreaCode],                                                         
        Null                                    AS [Dealer!2!FaxExchangeNumber],                                                   
        Null                                    AS [Dealer!2!FaxExtensionNumber],                                                  
        Null                                    AS [Dealer!2!FaxUnitNumber],                                                       
        Null                                    AS [Dealer!2!Name],                                                                
        Null                                    AS [Dealer!2!PhoneAreaCode],                                                       
        Null                                    AS [Dealer!2!PhoneExchangeNumber],                                                 
        Null                                    AS [Dealer!2!PhoneExtensionNumber],                                                
        Null                                    AS [Dealer!2!PhoneUnitNumber],                                                     
        Null                                    AS [Dealer!2!SysLastUpdatedDate],    
    
        -- Metadata Header
        NULL                                    AS [Metadata!3!Entity],
        -- Metadata
        NULL                                    AS [Column!4!Name],
        NULL                                    AS [Column!4!DataType],
        NULL                                    AS [Column!4!MaxLength],
        NULL                                    AS [Column!4!Precision],
        NULL                                    AS [Column!4!Scale],
        NULL                                    AS [Column!4!Nullable],
        --Reference
        Null                                    AS [Reference!5!ListName],
        Null                                    AS [Reference!5!ReferenceID],
        Null                                    AS [Reference!5!Name]
    
    UNION ALL

    --Dealer Level
    SELECT
        2,                                      
        1,                                      
        
        --Root
        NULL,   
                        
        -- Dealer
        D.DealerID,
        D.Address1,
        D.Address2,
        D.AddressCity,
        D.AddressCounty,
        D.AddressState,
        D.AddressZip,
        D.FaxAreaCode,
        D.FaxExchangeNumber,
        D.FaxExtensionNumber,
        D.FaxUnitNumber,
        D.Name,
        D.PhoneAreaCode,
        D.PhoneExchangeNumber,
        D.PhoneExtensionNumber,
        D.PhoneUnitNumber,
        dbo.ufnUtilityGetDateString(D.SysLastUpdatedDate) AS SysLastUpdatedDate,

        --Metadata Header
        NULL,
            
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM (SELECT @DealerID as DealerID) as Parms left join dbo.utb_dealer D on Parms.DealerID = D.DealerID
      
    
    UNION ALL 
  
    --Metadata Header Level
    SELECT DISTINCT
        3,
        1,

        --Root
        Null,

        --Dealer
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null,
            
        --Metadata Header
        GroupName,
            
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM    @tmpMetadata

    UNION ALL

    --Metadata Level
    SELECT 
        4,
        3,

        --Root
        Null,

        --Dealer
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null,
            
        --Metadata Header
        GroupName,

        -- Metadata
        ColumnName,
        DataType,
        MaxLength,
        NumericPrecision,
        Scale,
        Nullable,

        --Reference
        Null, Null, Null

    FROM @tmpMetadata

    UNION ALL

    --Reference Level
    SELECT
        5,
        1,

        --Root
        Null,

        --Dealer
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null,

        --Metadata Header
        'ZZ-Reference',
            
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        ListName,
        ReferenceID,
        Name

    FROM @tmpReference
        
    ORDER BY Tag, [Metadata!3!Entity]
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTDealerGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

