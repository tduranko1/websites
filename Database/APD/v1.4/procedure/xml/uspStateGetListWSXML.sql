-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspStateGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspStateGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspStateGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns list of states
*
* MODIFIED:     Jon Leatherwood, 6/9/2006 - Modified to split LicenseRequiredFlag to LicenseRequiredOwnerFlag
*                                           and LicenseRequiredAnalystFlag                    
*
* PARAMETERS:  
* None
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspStateGetListWSXML]
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspStateGetListWSXML'


    SELECT
        1 as tag,
        Null as Parent,
--        Root
        Null as [Root!1!Root],
--        State
        Null as [State!2!DisplayOrder!Hide],
        Null as [State!2!StateCode],
        Null as [State!2!StateValue],
        Null as [State!2!LicenseRequiredAnalystFlag],
        Null as [State!2!LicenseRequiredOwnerFlag]

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null,
--        State
        SC.DisplayOrder,
        SC.StateCode,
        SC.StateValue,
        SC.LicenseRequiredAnalystFlag,
        SC.LicenseRequiredOwnerFlag

    FROM
        utb_State_Code SC

    WHERE
        SC.EnabledFlag = 1

    ORDER BY tag, [State!2!DisplayOrder!Hide]
    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspStateGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspStateGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspStateGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/