-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditEstimateValuesGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAuditEstimateValuesGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




/************************************************************************************************************************
*
* PROCEDURE:    uspAuditEstimateValuesGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves Shop Audit stuff
*
* PARAMETERS:  
* (I) ClaimAspectID        
*
* RESULT SET:
* An XML Data stream containing payment information
*
*
* VSS
* $Workfile: uspAuditEstimateValuesGetDetailXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspAuditEstimateValuesGetDetailXML.SQL $
* $Revision: 1 $
* $Author: Dan Price $
* $Date: 10/23/01 5:08p $
*
* Updates:  10Jul2012 - TVD - Added additional XML fields
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAuditEstimateValuesGetDetailXML
    @DocumentID			udt_std_id_big,
    @UserID				udt_std_id
AS
BEGIN
    
    SET NOCOUNT ON
    
    -- Declare local variables

    DECLARE @ProcName						AS varchar(50)       -- Used for raise error stmts
    DECLARE @error							AS int
    DECLARE @ClaimAspectID					AS int
	DECLARE @ClaimAspectServiceChannelID	AS int
	DECLARE @RepairTotal					AS money
	DECLARE @Disposition					AS varchar(5)

    SET @ProcName = 'uspAuditEstimateValuesGetDetailXML'
    
    -- Validate Parameters
    --IF  ((SELECT DocumentID 
    --     FROM dbo.utb_document d INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    --     WHERE DocumentID = @DocumentID
    --       AND EstimateTypeFlag = 1) IS NULL)
    --BEGIN
    --    -- Invalid ID
    --    RAISERROR('101|%s|@DocumentID|%s', 16, 1, @ProcName, @DocumentID)
    --    RETURN
    --END
    

    --Get claim aspect and claim aspect service channel IDs
    SELECT @ClaimAspectID = ClaimAspectID, 
		@ClaimAspectServiceChannelID = utb_claim_aspect_service_channel.ClaimAspectServiceChannelID,
		@Disposition = isNull(utb_claim_aspect_service_channel.DispositionTypeCD,'')
	FROM utb_claim_aspect_service_channel_document 
	INNER JOIN utb_claim_aspect_service_channel
		ON utb_claim_aspect_service_channel_document.ClaimAspectServiceChannelID = utb_claim_aspect_service_channel.ClaimAspectServiceChannelID
    WHERE DocumentID = @DocumentID

	-- 10Jul2012 - TVD - Added to get Orignal Gross Amount
	--Get repair total for original estimate
	DECLARE @OriginalGrossAmount AS money
	
	SELECT @OriginalGrossAmount = AgreedExtendedAmt
	FROM utb_estimate_summary
	INNER JOIN utb_document
		ON utb_estimate_summary.DocumentID = utb_document.DocumentID
	WHERE utb_document.DocumentTypeID = 3
	AND EstimateSummaryTypeID = 27
	AND utb_document.DocumentID in
		(SELECT DocumentID
		FROM utb_claim_aspect_service_channel_document
		WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
		    
    --Get repair total
    IF EXISTS (
		SELECT DocumentID
		FROM utb_document
		WHERE DocumentTypeID = 10 and SupplementSeqNumber = 1
		AND DocumentID in
			(SELECT DocumentID
			FROM utb_claim_aspect_service_channel_document
			WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
	BEGIN
		--Get repair total for supplement 1
		SELECT @RepairTotal = AgreedExtendedAmt
		FROM utb_estimate_summary
		INNER JOIN utb_document
			ON utb_estimate_summary.DocumentID = utb_document.DocumentID
		WHERE utb_document.DocumentTypeID = 10 and SupplementSeqNumber = 1
		AND EstimateSummaryTypeID = 27
		AND utb_document.DocumentID in
			(SELECT DocumentID
			FROM utb_claim_aspect_service_channel_document
			WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
	END
	ELSE
	BEGIN
		--Get repair total for original estimate
		SELECT @RepairTotal = AgreedExtendedAmt
		FROM utb_estimate_summary
		INNER JOIN utb_document
			ON utb_estimate_summary.DocumentID = utb_document.DocumentID
		WHERE utb_document.DocumentTypeID = 3
		AND EstimateSummaryTypeID = 27
		AND utb_document.DocumentID in
			(SELECT DocumentID
			FROM utb_claim_aspect_service_channel_document
			WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
	END
    
    

    -- Select Root Level
    SELECT 	1								AS Tag,
            NULL							AS Parent,
            @ClaimAspectID					AS [Root!1!ClaimAspectID],
            @ClaimAspectServiceChannelID	AS [Root!1!ClaimAspectServiceChannelID],
            @Disposition					AS [Root!1!Disposition],
            @UserID							AS [Root!1!UserID],
            
            --Values
            NULL					AS [Values!2!RepairTotal],
            NULL					AS [Values!2!OriginalGrossAmount],
            NULL					AS [Values!2!AuditedOriginalGrossAmount],
            NULL					AS [Values!2!AdditionalLineItemValues],
            NULL					AS [Values!2!MissingLineItemValues],
            NULL					AS [Values!2!LineItemValueCorrection]
                  
    UNION ALL

    SELECT 2,
           1,
           @ClaimAspectID,
           NULL,
           NULL,
           NULL,
           
           -- Values Level
           @RepairTotal,
           @OriginalGrossAmount,
           NULL,
           NULL,
           NULL,
           NULL
	
	UNION ALL
	
	SELECT	2,
			1,
			@ClaimAspectID,
			NULL,
			NULL,
			NULL,
			NULL,
			IsNull(@OriginalGrossAmount,0),
			IsNull(AuditedOriginalGrossAmount,0),
			IsNull(AdditionalLineItemValues,0),
			IsNull(MissingLineItemValues,0),
			IsNull(LineItemValueCorrection,0)
	FROM utb_claim_aspect_audit_values
	WHERE ClaimAspectID = @ClaimAspectID
           
                    
    ORDER BY Tag

    --FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditEstimateValuesGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAuditEstimateValuesGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

