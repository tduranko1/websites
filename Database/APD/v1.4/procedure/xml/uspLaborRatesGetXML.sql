-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspLaborRatesGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspLaborRatesGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspLaborRatesGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the Labor Rates
*
* PARAMETERS:  
* (I) @StateCode  The State Code
* (I) @City       The City Name
* (I) @County     The County
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspLaborRatesGetXML
    @InsuranceCompanyID int,
    @StateCode          varchar(2),
    @City               varchar(50) = NULL,
    @County             varchar(50) = NULL
AS
BEGIN

   DECLARE @LaborRateID as bigint
   DECLARE @w_StateCode as varchar(2)
   DECLARE @w_City as varchar(50)
   DECLARE @w_County as varchar(50)

   SET @LaborRateID = null
   SET @w_StateCode = NULL
   SET @w_City = NULL
   SET @w_County = NULL
   
   IF LEN(LTRIM(RTRIM(@City))) = 0 SET @City = NULL
   IF LEN(LTRIM(RTRIM(@County))) = 0 SET @County = NULL

   IF @StateCode IS NOT NULL AND
      @City IS NOT NULL AND
      @County IS NOT NULL
   BEGIN
      SELECT @LaborRateID = LaborRateID
      FROM utb_labor_rate
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND StateCode = @StateCode 
        AND City = @City
        AND County = @County

      IF @LaborRateID IS NOT NULL
      BEGIN
         SET @w_StateCode = @StateCode
         SET @w_City = @City
         SET @w_County = @County
      END
   END

   IF @LaborRateID IS NULL
   BEGIN
      -- NO match with all criteria. Let us see if we match with state and city
      SELECT @LaborRateID = LaborRateID
      FROM utb_labor_rate
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND StateCode = @StateCode 
        AND City = @City
        AND (County IS NULL OR County = '')

      IF @LaborRateID IS NOT NULL
      BEGIN
         SET @w_StateCode = @StateCode
         SET @w_City = @City
         SET @w_County = NULL
      END
   END

   IF @LaborRateID IS NULL
   BEGIN
      -- NO match with all criteria. Let us see if we match with state and county
      SELECT @LaborRateID = LaborRateID
      FROM utb_labor_rate
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND StateCode = @StateCode 
        AND County = @County
        AND (City IS NULL OR City = '')

      IF @LaborRateID IS NOT NULL
      BEGIN
         SET @w_StateCode = @StateCode
         SET @w_City = NULL
         SET @w_County = @County
      END
   END

   IF @LaborRateID IS NULL
   BEGIN
      -- NO match with all criteria. Let us see if we match with state
      SELECT @LaborRateID = LaborRateID
      FROM utb_labor_rate
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND StateCode = @StateCode 
        AND (City IS NULL OR City = '')
        AND (County IS NULL OR County = '')

      IF @LaborRateID IS NOT NULL
      BEGIN
         SET @w_StateCode = @StateCode
         SET @w_City = NULL
         SET @w_County = NULL
      END
   END

   IF @LaborRateID IS NULL
   BEGIN
      -- NO match with all criteria. Let us see if we match with state
      SELECT @LaborRateID = LaborRateID
      FROM utb_labor_rate
      WHERE InsuranceCompanyID = @InsuranceCompanyID
        AND (StateCode IS NULL OR StateCode = '')
        AND (City IS NULL OR City = '')
        AND (County IS NULL OR County = '')

      IF @LaborRateID IS NOT NULL
      BEGIN
         SET @w_StateCode = NULL
         SET @w_City = NULL
         SET @w_County = NULL
      END
   END

   SELECT   1 AS Tag,
            NULL AS Parent,
            -- Root
            isNull(@InsuranceCompanyID, '')  as [Root!1!InsuranceCompanyID],
            isNull(@w_StateCode, '')           as [Root!1!MatchStateCode],
            isNull(@w_City, '')                as [Root!1!MatchCity],
            isNull(@w_County, '')              as [Root!1!MatchCounty],
            -- Labor Rate Details
            NULL AS [LaborRate!2!LaborRateID],
            NULL AS [LaborRate!2!AgreedPriceVariance],
            NULL AS [LaborRate!2!AuthorizedBy],
            NULL AS [LaborRate!2!AuthorizationDate],
            NULL AS [LaborRate!2!BodyRateMin],
            NULL AS [LaborRate!2!BodyRateMax],
            NULL AS [LaborRate!2!FrameRateMin],
            NULL AS [LaborRate!2!FrameRateMax],
            NULL AS [LaborRate!2!MaterialRateMin],
            NULL AS [LaborRate!2!MaterialRateMax],
            NULL AS [LaborRate!2!MechRateMin],
            NULL AS [LaborRate!2!MechRateMax],
            NULL AS [LaborRate!2!RefinishRateMin],
            NULL AS [LaborRate!2!RefinishRateMax],
            NULL AS [LaborRate!2!LaborTax],
            NULL AS [LaborRate!2!PartsTax],
            NULL AS [LaborRate!2!MaterialsTax]

   UNION ALL

   SELECT   2,
            1,
            -- Root
            NULL, NULL, NULL, NULL,
            -- Labor Rate Details
            LaborRateID,
            AgreedPriceVariance,
            AuthorizedBy,
            AuthorizationDate,
            BodyRateMin,
            BodyRateMax,
            FrameRateMin,
            FrameRateMax,
            MaterialRateMin,
            MaterialRateMax,
            MechRateMin,
            MechRateMax,
            RefinishRateMin,
            RefinishRateMax,
            LaborTax,
            PartsTax,
            MaterialsTax
   FROM utb_labor_rate
   Where LaborRateID = @LaborRateID         

   ORDER BY [LaborRate!2!LaborRateID], Tag        

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspLaborRatesGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspLaborRatesGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
