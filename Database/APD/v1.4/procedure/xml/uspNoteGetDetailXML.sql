                                                                                                                         -- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspNoteGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNoteGetDetailXML
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspNoteGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves data necessary to populate the notes screen
*
* PARAMETERS:
* (I) @LynxID               The Lynx ID
*
* RESULT SET:
* An XML Data stream containing notes for the claim
*
*
* VSS
* $Workfile: uspNoteGetDetailXML.sql $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspNoteGetDetailXML.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/29/01 2:49p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspNoteGetDetailXML
    @LynxID         udt_std_int_big,
    @DocumentID     udt_std_int_big=NULL
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspNoteGetDetailXML'


    -- Check to make sure a valid Lynx ID was passed in

    IF  (@LynxID IS NULL) OR
        ((NOT @LynxID = 0) AND NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID

        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    IF (@DocumentID = 0) SET @DocumentID = NULL

    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all entities and store in the temporary table

    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Note',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN
            ('NoteTypeID',
             'StatusID',
             'Note'))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        LinkedReferenceID   varchar(10) NULL,        -- This field is special to support linking Status IDs to Pertains To
        ServiceChannelCD    varchar(4)  NULL,
        EnabledFlag         bit         NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
        StatusID            varchar(10) NULL
    )


    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, LinkedReferenceID,
                                ServiceChannelCD, EnabledFlag, StatusID)

    SELECT  'NoteType' AS ListName,
            DisplayOrder AS DisplayOrder,
            NoteTypeID,
            Name,
            NULL,
            NULL,
            EnabledFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            NULL
    FROM    utb_note_type
    WHERE   /*EnabledFlag = 1  --Project:210474 APD Modified the WHERE clause when we did the code merge M.A.20061117
    AND     */DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'PertainsTo',
            NULL,
            --EntityCode,
            ClaimAspectID,
            Name,
            EntityCode,
            NULL,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            NULL
    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0)  -- (0, 0) means all aspects, open or closed
    WHERE   EntityCode <> 'cov'

    UNION ALL

    SELECT  'Status',
            s.DisplayOrder,
            s.StatusID,
            s.Name,
            cat.Code,
            s.ServiceChannelCD,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            NULL
    FROM    utb_status s
    LEFT JOIN utb_claim_aspect_type cat ON (s.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    WHERE   s.EnabledFlag = 1

    UNION ALL

    -- Add the service channel reference items

    SELECT       'ServiceChannel',
                 NULL,
                 casc.ClaimAspectServiceChannelID,
                 urc.Name,
				 CAST (ca.ClaimAspectID As Varchar),
                 --ca.ClaimAspectID,
                 casc.ServiceChannelCD,
                 NULL,
                 cas.StatusID
    FROM         utb_claim_aspect_service_channel casc
    INNER JOIN   utb_claim_aspect ca
    ON           ca.ClaimAspectID = casc.ClaimAspectID

    INNER JOIN   utb_claim_aspect_status cas
    ON           (casc.ClaimAspectID = cas.ClaimAspectID AND casc.ServiceChannelCD = cas.ServiceChannelCD)
    and          cas.StatusTypeCD = 'SC'

    INNER JOIN   ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc
    ON           casc.ServiceChannelCD = urc.Code
    WHERE        ca.LynxID = @LynxID

    UNION ALL

    SELECT  'MessageTemplate',
            NULL,
            MessageTemplateID,
            Description,
            NULL,
            isNull(ServiceChannelCD, ''),
            NULL,
            NULL
    FROM    dbo.utb_message_template
    WHERE   AppliesToCD = 'N'
      AND   EnabledFlag = 1

    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary table to hold references to the claim's notes

    DECLARE @tmpNotes TABLE
    (
        LynxID                      bigint          NOT NULL,
        DocumentID                  int             NOT NULL,
        --PertainsTo                varchar(5)      NOT NULL,
        ClaimAspectTypeName         varchar(50)     NOT NULL,
        ClaimAspectNumber           tinyint         NOT NULL,
        ClaimAspectID               bigint          NOT NULL,
        ClaimAspectServiceChannelID bigint          NOT NULL,
        ServiceChannelCD            varchar(5)      NOT NULL
    )


    IF NOT EXISTS(SELECT Name FROM dbo.utb_document_type WHERE Name = 'Note')
    BEGIN
       -- Note Document Type Not Found

        RAISERROR('102|%s|"Note"|utb_document_type', 16, 1, @ProcName)
        RETURN
    END


    -- Compile note list together from all the various entities


    IF (@DocumentID IS NULL)
    BEGIN
      INSERT INTO @tmpNotes

      SELECT  @LynxID,
              cascd.DocumentID,
              --dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 0) AS PertainsTo
              cat.Code,
              ca.ClaimAspectNumber,
              ca.ClaimAspectID,
              casc.ClaimAspectServiceChannelID,
              casc.ServiceChannelCD
        FROM  dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
        INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        INNER JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE ca.LynxID = @LynxID
          AND dt.Name = 'Note'
          AND d.EnabledFlag = 1
          AND casc.EnabledFlag = 1--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    END
    ELSE
    BEGIN
      INSERT INTO @tmpNotes

      SELECT  @LynxID,
              cascd.DocumentID,
              --dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 0) AS PertainsTo
              cat.Code,
              ca.ClaimAspectNumber,
              ca.ClaimAspectID,
              casc.ClaimAspectServiceChannelID,
              casc.ServiceChannelCD
        FROM  dbo.utb_claim_aspect_service_channel_document cascd--Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
        INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        INNER JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE ca.LynxID = @LynxID
          AND dt.Name = 'Note'
          AND d.EnabledFlag = 1
          AND d.DocumentID = @DocumentID
          AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    END

    IF @@ERROR <> 0
    BEGIN
       -- Insertion failure

        RAISERROR('105|%s|@tmpNotes', 16, 1, @ProcName)
        RETURN
    END

    --select * from @tmpNotes
    --return


    -- Begin XML Select

    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            @LynxID AS [Root!1!LynxID],
            -- Note
            NULL AS [Note!2!DocumentID],
            NULL AS [Note!2!ClaimAspectID],
            NULL AS [Note!2!ClaimAspectNumber],
            NULL AS [Note!2!ClaimAspectCode],
            NULL AS [Note!2!ClaimAspectServiceChannelID],
            NULL AS [Note!2!ServiceChannelCD],
            NULL AS [Note!2!CreatedUserID],
            NULL AS [Note!2!CreatedUserNameFirst],
            NULL AS [Note!2!CreatedUserNameLast],
            NULL AS [Note!2!CreatedUserRoleID],
            NULL AS [Note!2!CreatedUserRoleName],
            NULL AS [Note!2!CreatedUserSupervisorFlag],
            NULL AS [Note!2!DocumentSourceID],
            NULL AS [Note!2!DocumentTypeID],
            NULL AS [Note!2!NoteTypeID],
            NULL AS [Note!2!ParentDocumentID],
            NULL AS [Note!2!PrivateFlag], --Project:210474 APD Added the column when we did the code merge M.A.20061117
            NULL AS [Note!2!StatusID],
            --NULL AS [Note!2!PertainsTo],
            NULL AS [Note!2!CurrentDate],
            NULL AS [Note!2!CreatedDate],
            NULL AS [Note!2!Note],
            NULL AS [Note!2!SysLastUpdatedDate],
            -- Metadata Header
            NULL AS [Metadata!3!Entity],
            -- Columns
            NULL AS [Column!4!Name],
            NULL AS [Column!4!DataType],
            NULL AS [Column!4!MaxLength],
            NULL AS [Column!4!Precision],
            NULL AS [Column!4!Scale],
            NULL AS [Column!4!Nullable],
            -- Reference Data
            NULL AS [Reference!5!List],
            NULL AS [Reference!5!ReferenceID],
            NULL AS [Reference!5!Name],
            NULL AS [Reference!5!ClaimAspectCode],  -- This field is special to support linking Status IDs to Aspect Types
            NULL AS [Reference!5!ServiceChannelCD],
            NULL AS [Reference!5!EnabledFlag],  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            NULL AS [Reference!5!StatusID]

    UNION ALL


    -- Select Note Level

    SELECT  2,
            1,
            NULL,
            -- Note
            IsNull(n.DocumentID, ''),
            IsNull(n.ClaimAspectID, ''),
            IsNull(n.ClaimAspectNumber, ''),
            IsNull(n.ClaimAspectTypeName, ''),
            IsNull(n.ClaimAspectServiceChannelID, ''),
            IsNull(n.ServiceChannelCD, ''),
            d.CreatedUserID,
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            d.CreatedUserRoleID,
            IsNull(r.Name, ''),
            d.CreatedUserSupervisorFlag,
            d.DocumentSourceID,
            d.DocumentTypeID,
            d.NoteTypeID,
            IsNull(d.ParentDocumentID, ''),
            d.PrivateFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            d.StatusID,
            --IsNull(n.PertainsTo, ''),
            GetDate(),
            IsNull(d.CreatedDate, ''),
            IsNull(d.Note, ''),
            dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL  --Project:210474 APD Added the column when we did the code merge M.A.20061117

    FROM    (SELECT @LynxID AS LynxID) AS parms
    LEFT JOIN @tmpNotes n ON (parms.LynxID = n.LynxID)
    LEFT JOIN dbo.utb_document d ON (n.DocumentID = d.DocumentID)
    LEFT JOIN dbo.utb_user u ON (d.CreatedUserID = u.UserID)
    LEFT JOIN dbo.utb_role r ON (d.CreatedUserRoleID = r.RoleID)


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 3,
            1,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL  --Project:210474 APD Added the column when we did the code merge M.A.20061117

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  4,
            3,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  5,
            1,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, --Project:210474 APD Added the column when we did the code merge M.A.20061117
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            LinkedReferenceID,
            ServiceChannelCD,
            EnabledFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
            StatusID

    FROM    @tmpReference


    ORDER BY [Metadata!3!Entity], Tag
--    FOR XML EXPLICIT                  -- (Comment out for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspNoteGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNoteGetDetailXML TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: uspNoteGetDetailXML.sql $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspNoteGetDetailXML.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/29/01 2:49p $
* $History: uspNoteGetDetailXML.sql $
 *
 * *****************  Version 2  *****************
 * User: Jim          Date: 10/29/01   Time: 2:49p
 * Updated in $/Database/APD/v1.0.0/procedure/xml
 * Bug fix
 *
 * *****************  Version 1  *****************
 * User: Jim          Date: 10/04/01   Time: 1:56p
 * Created in $/Database/APD/v1.0.0/procedure/xml
 * Initial development complete
*
************************************************************************************************************************/

