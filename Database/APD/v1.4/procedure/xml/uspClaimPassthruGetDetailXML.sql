-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimPassthruGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimPassthruGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimPassthruGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns Source Application Pass-thru data
*
* PARAMETERS:  
* (I) @LynxID               The LynxID to retrieve passthru data for
*
* RESULT SET:
*   LynxID                          The LynxID requested
*   ApplicattionCode                The code of the application the data was submitted from
*   ApplicationPassthruData         The passthru data
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE dbo.uspClaimPassthruGetDetailXML
    @LynxID     udt_std_id_big
AS
BEGIN
    -- Declare local variables
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimPassthruGetDetailXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    
    -- Begin Select
    SELECT 1                    AS Tag,
           NULL                 AS Parent,
            
           -- Root
           @LynxID              AS [Root!1!LynxID],
            
           -- ClaimAspect
           NULL                 AS [ClaimAspect!2!ClaimAspectID],
           NULL                 AS [ClaimAspect!2!Code],
           NULL                 AS [ClaimAspect!2!SourceApplicationPassThruData]
            
    UNION ALL
    
    SELECT 2,
           1,
           
           -- Root
           NULL,
           
           -- ClaimAspect
           ca.ClaimAspectID,
           a.Code,
           ca.SourceApplicationPassThruData
      FROM dbo.utb_claim c INNER JOIN dbo.utb_claim_aspect ca ON c.LynxID = ca.LynxID
                            LEFT JOIN dbo.utb_application a ON (ca.SourceApplicationID = a.ApplicationID)
      WHERE c.LynxID = @LynxID

      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

END


GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimPassthruGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimPassthruGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/