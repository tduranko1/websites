-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyAssignmentGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWarrantyAssignmentGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWarrantyAssignmentGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Retrieves the Warranty Assignments
*
* PARAMETERS:  
*   None
*
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWarrantyAssignmentGetDetailXML
    @ClaimAspectID     udt_std_id_big
AS
BEGIN
   DECLARE @Business1PersonnelTypeID AS int
   DECLARE @ClaimAspectServiceChannelID as bigint
   DECLARE @EstimateSummaryTypeIDRepairTotal AS int
   DECLARE @EstimateSummaryTypeIDNetTotal AS int
   
   DECLARE @ProcName as varchar(50)

   --set @ClaimAspectID = 625252
   set @ProcName = 'uspWarrantyAssignmentGetDetailXML'


   SELECT @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
   FROM utb_claim_aspect_service_channel
   WHERE ClaimAspectID = @ClaimAspectID
     AND ServiceChannelCD = 'PS'

   -- Get Shop Manager Personnel Type ID

   SELECT  @Business1PersonnelTypeID = PersonnelTypeID
   FROM  dbo.utb_personnel_type
   WHERE Name = 'Shop Manager'     -- personnel associated with utb_shop_location

   IF @@ERROR <> 0
   BEGIN
    -- SQL Server Error

     RAISERROR('99|%s', 16, 1, @ProcName)

     RETURN
   END

   IF @Business1PersonnelTypeID IS NULL
   BEGIN
    -- Personnel Type Not Found

     RAISERROR('102|%s|"Shop Manager"|utb_personnel_type', 16, 1, @ProcName)
     RETURN
   END
   
    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END   

    DECLARE @tmpDocument TABLE
    (
        DocumentID  int
    )

    INSERT INTO @tmpDocument
      SELECT  cascd.DocumentID
        FROM  dbo.utb_claim_aspect_service_channel casc
        LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE casc.ClaimAspectID = @ClaimAspectID
          AND d.EnabledFlag = 1         -- Only return enabled
          --AND dt.EstimateTypeFlag = 1   -- Only interested in estimates
          AND dt.Name not in ('Note') -- Not interested in notes
          AND d.WarrantyFlag = 1


    -- If no records were selected, add one manually

    IF @@rowcount = 0
    BEGIN
        INSERT INTO @tmpDocument
        VALUES (0)
    END
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpDocument', 16, 1, @ProcName)
        RETURN
    END


    SELECT
        1    as Tag,
        Null as Parent,
        Null as [Root!1!Root],
        -- Warranty Assignments
        Null as [Assignment!2!AssignmentID],
        Null as [Assignment!2!ShopName],
        Null as [Assignment!2!ShopContact],
        Null as [Assignment!2!ShopPhone],
        Null as [Assignment!2!ShopFax],
        Null as [Assignment!2!AssignmentStatus],
        Null as [Assignment!2!AssignmentDate],
        Null as [Assignment!2!PreviousAssignmentDate],
        Null as [Assignment!2!Remarks],
        Null as [Assignment!2!WarrantyStartDate],
        Null as [Assignment!2!WarrantyStartConfirmFlag],
        Null as [Assignment!2!WarrantyEndDate],
        Null as [Assignment!2!WarrantyEndConfirmFlag],
         -- Document
         NULL AS [Document!3!DocumentID],
         NULL AS [Document!3!DocumentSourceName],
         NULL AS [Document!3!VANFlag],
         NULL AS [Document!3!DocumentTypeName],
         NULL AS [Document!3!CreatedDate],
         NULL AS [Document!3!ReceivedDate],
         NULL AS [Document!3!ImageLocation],
         NULL AS [Document!3!PrivateFlag],
         NULL AS [Document!3!SupplementSeqNumber],
         NULL AS [Document!3!FullSummaryExistsFlag],
         NULL AS [Document!3!AgreedPriceMetCD],
         NULL AS [Document!3!GrossEstimateAmt],
         NULL AS [Document!3!NetEstimateAmt],
         NULL AS [Document!3!EstimateTypeFlag],
         NULL AS [Document!3!DirectionToPayFlag],
         NULL AS [Document!3!FinalEstimateFlag],
         NULL AS [Document!3!DuplicateFlag],
         NULL AS [Document!3!EstimateTypeCD],
         NULL AS [Document!3!ServiceChannelCD],
         NULL AS [Document!3!SysLastUpdatedDateDocument],
         NULL AS [Document!3!SysLastUpdatedDateEstimate]
        
    UNION ALL

   SELECT   2,
            1,
            NULL,
            -- Warranty Assignments
            AssignmentID,
            sl.Name,
            (SELECT top 1 p.Name
             FROM dbo.utb_shop_location_personnel slp
             LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
             LEFT JOIN dbo.utb_personnel_type pt ON (@Business1PersonnelTypeID = pt.PersonnelTypeID)
             WHERE slp.ShopLocationID = sl.ShopLocationID),
            sl.PhoneAreaCode + sl.PhoneExchangeNumber + sl.PhoneUnitNumber + 
            case
               when sl.PhoneExtensionNumber is not null then 'x' + sl.PhoneExtensionNumber
               else ''
            end,
            sl.FaxAreaCode + sl.FaxExchangeNumber + sl.FaxUnitNumber,
            wa.CommunicationAddress,
            isNull(convert(varchar, AssignmentDate, 101), ''),
            NULL,
            isNULL(AssignmentRemarks, ''),
            isNull(convert(varchar, WarrantyStartDate, 101), ''),
            WarrantyStartConfirmFlag,
            isNull(convert(varchar, WarrantyEndDate, 101), ''),
            WarrantyEndConfirmFlag,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL

   FROM dbo.utb_warranty_assignment wa
   LEFT JOIN dbo.utb_shop_location sl ON wa.ShopLocationID = sl.ShopLocationID
   WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
     AND CancellationDate IS NULL

    UNION ALL

   SELECT   3,
            1,
            NULL,
            -- Warranty Assignments
            Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
            Null, Null, Null,
            -- Document
            IsNull(d.DocumentID, 0),
            IsNull(ds.Name, ''),
            ds.VANFlag,
            IsNull(dt.Name, ''),
            IsNull(d.CreatedDate, ''),
            IsNull(d.ReceivedDate, ''),
            IsNull(d.ImageLocation, ''),
            d.PrivateFlag,
            IsNull(d.SupplementSeqNumber, 0),
            IsNull(d.FullSummaryExistsFlag, 0),
            IsNull(d.AgreedPriceMetCD, ''),
            IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                        AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), ''),
            IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), ''),
            IsNull(dt.EstimateTypeFlag, 0),
            IsNull(d.DirectionToPayFlag, 0),
            IsNull(d.FinalEstimateFlag, 0),
            IsNull(d.DuplicateFlag, 0),
            IsNull(d.EstimateTypeCD, ''),          
            casc.ServiceChannelCD,
            dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
            IsNull((SELECT dbo.ufnUtilityGetDateString(SysLastUpdatedDate)
                      FROM  dbo.utb_estimate_summary 
                      WHERE DocumentID = D.DocumentID 
                        AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), '')

    FROM    @tmpDocument tmpD                -- Added to return blank claim even if no documents exist
    LEFT JOIN dbo.utb_Document d on tmpD.DocumentID = d.DocumentID
    LEFT JOIN dbo.utb_User u on d.CreatedUserID = u.UserID
    LEFT JOIN dbo.utb_role r on d.CreatedUserRoleID = r.RoleID
    LEFT JOIN dbo.utb_Document_Type dt on D.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN dbo.utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
     
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyAssignmentGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWarrantyAssignmentGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/