-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCPGetAllVehicleListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCPGetAllVehicleListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCPGetAllVehicleListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @LynxID   Lynx ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCPGetAllVehicleListXML
    @LynxID                 udt_std_id_big
AS
BEGIN
    -- Set database options

    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables
    DECLARE @ClaimAspectTypeID          udt_std_id
    DECLARE @InsuranceCompanyID         udt_std_id

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCPGetAllVehicleListXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END
    
    -- Get the aspect type id for property for use later
    
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    SELECT @InsuranceCompanyID = InsuranceCompanyID
    FROM utb_claim
    WHERE LynxID = @LynxID
    
    -- Begin XML Select

    SELECT
        1 as Tag,
        0 as Parent,
--        Root
        @LynxID as [Root!1!LynxID],
        @InsuranceCompanyID as [Root!1!InsuranceCompanyID],
--        VehicleList
        Null as [Vehicle!2!VehicleNumber],
        Null as [Vehicle!2!ClaimAspectID],
        Null as [Vehicle!2!VehicleYear],
        Null as [Vehicle!2!Make],
        Null as [Vehicle!2!Model],
        Null as [Vehicle!2!NameFirst],
        Null as [Vehicle!2!NameLast],
        Null as [Vehicle!2!BusinessName],
        Null as [Vehicle!2!ClosedStatus],
        Null as [Vehicle!2!Status],
        Null as [Vehicle!2!ExposureCD],
        Null as [Vehicle!2!CoverageProfileCD]
        

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null, Null,
--        Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.Name,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, '')

    FROM
        (SELECT @LynxID AS LynxID, @ClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID)
        LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	"StatusID" is not part of utb_Claim_Aspect instead the column exists in a
		table utb_Claim_Aspect_Status.
		Added the table as an intermediate table to join utb_Claim_Aspect to 
		utb_Status
		M.A. 20061108
	*********************************************************************************/
	LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
        LEFT JOIN dbo.utb_status vs ON (cas.StatusID = vs.StatusID )
      where cas.StatusTypeCD = 'SC'

    Order by [Vehicle!2!VehicleNumber], tag
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCPGetAllVehicleListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCPGetAllVehicleListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/