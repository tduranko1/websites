-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientCoverageTypesWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientCoverageTypesWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientCoverageTypesWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns Assignment Types by InsuranceCompanyID
*
* PARAMETERS:  
* (I) @input                InsuranceCompanyID
* (O) @output               XML
* (I) @ModifiedDateTime     2014-01-15
*
* RESULT SET:
*	XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspGetClientCoverageTypesWSXML]
    -- (@parameter_name datatype [OUTPUT],...)
AS
BEGIN
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspGetClientCoverageTypesWSXML'

    -- Set Database options
    
    SET NOCOUNT ON

    SELECT
        1 as Tag,
        0 as Parent,
        -- Root
        NULL as [Root!1!Root],
        -- Coverage Types
        NULL as [Coverage!2!ClientCoverageTypeID],
        NULL as [Coverage!2!Name],
        NULL as [Coverage!2!ClientCode],
        NULL as [Coverage!2!CoverageProfileCD]
   
   UNION ALL
   
   SELECT 
        2,
        1,
        -- Root
        NULL,
        -- Coverage Types
        ClientCoverageTypeID,
        Name,
        ClientCode,
        CoverageProfileCD
    FROM dbo.utb_client_coverage_type
	WHERE EnabledFlag = 1
	
    FOR XML EXPLICIT      
       
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientCoverageTypesWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientCoverageTypesWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientCoverageTypesWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/