-- Begin the transaction for dropping and creating the stored procedure
 
BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDAJobDocuments' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetDAJobDocuments 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetDAJobDocuments
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			2014Oct22
* FUNCTION:     Builds the job details database for new DeskAudit documents received to process to HyperquestService.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* Version:  1.0 - Initial Build
*        :  1.1 - 15Apr2015 - Added new fields for DRP
*        :  1.2 - 16Apr2016 - Modified to send ALL documents to Hyperquest automated.
*		 :  1.3 - 30Nov2016 - Allow the following CHOICE documents to be sent to HQ: (2,3,8,10,21,23,29,30,39,49,50,52,76,77,79,82)
*		 :  1.4 - 20Dec2016 - Correcting an issue with automated estimates to HQ
*		 :  1.5 - 06Feb2017 - Allow the following DRP documents to be sent to HQ: (2,3,8,10,21,23,29,30,39,49,50,52,76,77,79,82)
*		 :  1.6 - 29Feb2017 - Removed ADP Documents from this process
*		 :  1.7 - 15Aug2017 - ReAdded ADP Documents since we are now sending ADP assignment to HQ
*		 :  1.8 - 17Aug2017 - Modified DRP section to check if Assignment EXISTS before copying files to HQ Job table
*		 :  1.9 - 13Oct2017 - Added new NULL value to the insert due to new ErrorMessage field in utb_hyperquestsvr_job table
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetDAJobDocuments]
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @dtNow DATETIME
	DECLARE @DRPInitStartDate VARCHAR(50)

    SET @ProcName = 'uspHyperquestGetDAJobDocuments'
    SET @ApplicationCD = 'APDHyperquestPartner'
	SET @dtNow = CURRENT_TIMESTAMP
	SET @DRPInitStartDate = '2017-08-22'			-- Initial Start Date so NO OLD Data

    -- Validation

	-- Create Temp Table to hold unprocessed
	CREATE TABLE #NewDocuments (
		InsuranceCompanyID				INT
		, LynxID						INT
		, ServiceChannelCD				VARCHAR(10)
		, ClientClaimNumber				VARCHAR(30)
		, ClaimAspectNumber				INT
		, ClaimAspectID					INT
		, ClaimAspectServiceChannelID	INT
		, DocIDinAPD					INT
		, DocIDtoHQ						INT
		, AssignmentID					INT
		, CreatedDate					DATETIME
		, DocumentTypeID				INT
		, DocumentTypeName				VARCHAR(50)
		, DocumentSource				VARCHAR(100)
		, ImageType						VARCHAR(10)
		, ImageLocation					VARCHAR(255)
		, SysLastUpdatedDate			DATETIME
		, CurrentAssignmentStatus		BIT
	)

	----------------------------------------
	-- Populate Automated DA ONLY Estimates
	----------------------------------------
	INSERT INTO #NewDocuments
	SELECT 
		c.InsuranceCompanyID
		, ca.LynxID
		, casc.ServiceChannelCD
		, c.ClientClaimNumber
		, ca.ClaimAspectNumber
		, ca.ClaimAspectID
		, cascd.ClaimAspectServiceChannelID
		, cascd.DocumentID  'DocInAPD'
		, hj.DocumentID  'SentToHQ'
		, d.AssignmentID
		, d.CreatedDate
		, d.DocumentTypeID
		, dt.Name
		, ds.Name
		, d.ImageType
		, d.ImageLocation
		, cascd.SysLastupdateddate
		, 1
	FROM 
		utb_document AS d
		INNER JOIN utb_document_type AS dt
			ON d.DocumentTypeID = dt.DocumentTypeID
		INNER JOIN utb_document_source AS ds
			ON d.DocumentSourceID = ds.DocumentSourceID
		INNER JOIN utb_claim_aspect_service_channel_document AS cascd
			ON cascd.DocumentID = d.DocumentID
		INNER JOIN utb_claim_aspect_service_channel AS casc
			ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
		INNER JOIN utb_claim_aspect AS ca
			ON ca.ClaimAspectID = casc.ClaimAspectID
		INNER JOIN utb_claim AS c
			ON c.LynxID = ca.LynxID
		INNER JOIN utb_hyperquest_insurance AS hi
			ON hi.InsuranceCompanyID = c.InsuranceCompanyID
		LEFT JOIN utb_hyperquestsvc_jobs hj
			ON hj.DocumentID = d.DocumentID
	WHERE
		hi.EnabledFlag = 1
		AND d.EnabledFlag = 1	
		AND d.DocumentTypeID IN (3)
		AND casc.ServiceChannelCD='DA' -- Desk Audits only
		AND ds.Name NOT IN ('CCC') -- Not from CCC (Old Code)
		AND hj.DocumentID IS NULL -- DocumentID not already sent to HQ
		AND UPPER(ds.Name) <> 'HYPERQUEST'
		AND d.syslastupdateddate > '2016-10-20' -- Just don't want a bunch of old stuff
	ORDER BY
		d.DocumentID

	----------------------------------------
	-- Populate unprocessed CHOICE documents
	----------------------------------------
	INSERT INTO #NewDocuments
	SELECT --TOP 50 
		c.InsuranceCompanyID
		, ca.LynxID
		, casc.ServiceChannelCD
		, c.ClientClaimNumber
		, ca.ClaimAspectNumber
		, ca.ClaimAspectID
		, cascd.ClaimAspectServiceChannelID
		, cascd.DocumentID  'DocInAPD'
		, hj.DocumentID  'SentToHQ'
		, d.AssignmentID
		, d.CreatedDate
		, d.DocumentTypeID
		, dt.Name
		, ds.Name
		, d.ImageType
		, d.ImageLocation
		, cascd.SysLastupdateddate
		, 0
	FROM 
		utb_claim_aspect ca
		INNER JOIN utb_claim c
			ON c.LynxID = ca.LynxID
		INNER JOIN utb_claim_aspect_service_channel casc
			ON casc.claimaspectid = ca.claimaspectid
		INNER JOIN utb_claim_aspect_service_channel_document cascd
			ON cascd.claimaspectserviceChannelid = casc.claimaspectserviceChannelid
		INNER JOIN utb_document d
			ON d.DocumentID = cascd.DocumentID
		INNER JOIN utb_document_type dt
			ON dt.DocumentTypeID = d.DocumentTypeID
		INNER JOIN utb_document_source ds
			ON ds.DocumentSourceID = d.DocumentSourceID
		LEFT JOIN utb_hyperquestsvc_jobs hj
			ON hj.DocumentID = cascd.DocumentID
	WHERE 
		ca.InitialAssignmentTypeID = 17						-- Choice Claims
		AND hj.DocumentID IS NULL							-- Documents not sent to HQ
		AND hj.JobTransmittedDate IS NULL					-- Documents not transmitted to HQ
		AND d.DirectionalCD = 'I'							-- Incoming only
		AND d.DocumentSourceID NOT IN (16) 					-- No HQ Incoming Docs
		AND d.DocumentTypeID IN (2,3,8,10,21,23,29,30,39,49,50,52,76,77,79,82)		-- Send these documents to HQ
		AND ca.ClaimAspectID NOT IN (						-- Remove Cancelled Assignment
			SELECT 
				ClaimAspectID 
			FROM 
				utb_claim_aspect_status
			WHERE
				StatusID = 898
				AND ServiceChannelCD = 'CS'
		)
	ORDER BY
		cascd.SysLastupdateddate DESC

	----------------------------------------
	-- Populate unprocessed DRP(PS) documents
	----------------------------------------
	INSERT INTO #NewDocuments
	SELECT --TOP 50 
		c.InsuranceCompanyID
		, ca.LynxID
		, casc.ServiceChannelCD
		, c.ClientClaimNumber
		, ca.ClaimAspectNumber
		, ca.ClaimAspectID
		, cascd.ClaimAspectServiceChannelID
		, cascd.DocumentID  'DocInAPD'
		, hj.DocumentID  'SentToHQ'
		, d.AssignmentID
		, d.CreatedDate
		, d.DocumentTypeID
		, dt.Name
		, ds.Name
		, d.ImageType
		, d.ImageLocation
		, cascd.SysLastupdateddate
		, 0
	FROM 
		utb_claim_aspect ca
		INNER JOIN utb_claim c
			ON c.LynxID = ca.LynxID
		INNER JOIN utb_claim_aspect_service_channel casc
			ON casc.claimaspectid = ca.claimaspectid
		INNER JOIN utb_claim_aspect_service_channel_document cascd
			ON cascd.claimaspectserviceChannelid = casc.claimaspectserviceChannelid
		INNER JOIN utb_document d
			ON d.DocumentID = cascd.DocumentID
		INNER JOIN utb_document_type dt
			ON dt.DocumentTypeID = d.DocumentTypeID
		INNER JOIN utb_document_source ds
			ON ds.DocumentSourceID = d.DocumentSourceID
		LEFT JOIN utb_hyperquestsvc_jobs hj
			ON hj.DocumentID = cascd.DocumentID
	WHERE 
		ca.InitialAssignmentTypeID = 4						-- DRP Claims
		AND hj.DocumentID IS NULL							-- Documents not sent to HQ
		AND hj.JobTransmittedDate IS NULL					-- Documents not transmitted to HQ
		AND d.DirectionalCD = 'I'							-- Incoming only
		AND d.DocumentSourceID NOT IN (16) 					-- No HQ Incoming Docs
		AND d.DocumentTypeID IN (2,3,8,10,21,23,29,30,39,49,50,52,76,77,79,82)		-- Send these documents to HQ
		AND ca.ClaimAspectID NOT IN (						-- Remove Cancelled Assignment
			SELECT 
				ClaimAspectID 
			FROM 
				utb_claim_aspect_status
			WHERE
				StatusID = 598								-- PS Service Channel Cancelled
				AND ServiceChannelCD = 'PS'					-- DRP ONLY
		)
		--AND ds.Name NOT IN ('ADP')						-- NO ADP Files  -- TVD - 10Aug2017 - Removed to allow ADP to HQ
		AND d.CreatedDate > @DRPInitStartDate				-- Initial Start Date because we don't what OLD data
	ORDER BY
		cascd.SysLastupdateddate DESC

	----------------------------------------
	-- Check if AssignmentAdd was Successful
	----------------------------------------
	UPDATE 
		#NewDocuments
	SET CurrentAssignmentStatus = 1
	WHERE
		LynxID IN (
			SELECT  
				hj.LynxID
			FROM
				utb_hyperquestsvc_jobs hj WITH(NOLOCK)
				INNER JOIN #NewDocuments nd WITH(NOLOCK)
					ON nd.LynxID = hj.LynxID
			WHERE
				hj.DocumentTypeID = 22
				AND hj.JobStatus = 'Processed'
				AND hj.JobTransmittedDate IS NOT NULL
		)

	--===================================--
	-- MAIN CODE
	--===================================--
	-- Create new HQ Job Records for these claims
	INSERT INTO
		utb_hyperquestsvc_jobs
		SELECT 
		(SELECT NEWID()) JobID
		, nd.InsuranceCompanyID
		, nd.LynxID
		, nd.ClientClaimNumber
		, nd.DocIDinAPD
		, nd.AssignmentID
		, nd.CreatedDate
		, nd.DocumentTypeID
		, nd.DocumentTypeName
		, nd.DocumentSource
--		, nd.ImageType
		, CASE 
	 		WHEN UPPER(nd.DocumentSource) = 'ADP' AND UPPER(nd.ImageType) = 'PDF' AND UPPER(SUBSTRING(nd.ImageLocation, (LEN(nd.ImageLocation) - 2), 3)) = 'JPG' THEN 'JPG'
			ELSE
				nd.ImageType
		  END AS ImageType
		, nd.ImageLocation
		, nd.ClaimAspectServiceChannelID
		, nd.ClaimAspectID
		, CASE
			WHEN UPPER(nd.ServiceChannelCD) = 'DA' THEN 'AttachmentUpload.xsl' 
			WHEN UPPER(nd.ServiceChannelCD) = 'CS' THEN 'ChoiceAttachmentUpload.xsl' 
			WHEN UPPER(nd.ServiceChannelCD) = 'PS' THEN 'LynxAttachmentUpload.xsl' 
		  END JobXSLFile
		, 'Unprocessed' JobStatus
		, CASE
			WHEN UPPER(nd.ServiceChannelCD) = 'DA' THEN 'APD DeskAudit to Hyperquest (AUTO)' 
			WHEN UPPER(nd.ServiceChannelCD) = 'CS' THEN 'ALL CS Documents to Hyperquest' 
			WHEN UPPER(nd.ServiceChannelCD) = 'PS' THEN 'ALL PS Documents to Hyperquest' 
		  END JobStatusDetails
		, 1 JobPriority
		, @dtNow
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 1 EnabledFlag
		, 'NONE' JobXML
		, 0
		, @dtNow
		, 0
		, 0 
		, 0
		, NULL
	FROM 
		#NewDocuments nd
	WHERE
		CurrentAssignmentStatus = 1
	ORDER BY
		syslastupdateddate

SELECT * FROM #NewDocuments

DROP TABLE #NewDocuments
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDAJobDocuments' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetDAJobDocuments TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/