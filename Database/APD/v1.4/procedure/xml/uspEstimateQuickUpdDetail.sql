-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateQuickUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateQuickUpdDetail 
END

GO
/****** Object:  StoredProcedure [dbo].[uspEstimateQuickUpdDetail]    Script Date: 04/24/2014 11:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateQuickUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates the estimate data in utb_document and utb_estimate_summary
*
* PARAMETERS:
* (I) @DocumentID           The DocumentID to update
* (I) @ClaimAspectID        The claim aspect id
* (I) @AdjustmentTotalAmt   The total adjustment amount
* (I) @DeductibleAmt        The amount of the deductible
* (I) @DocumentTypeID       The document type id
* (I) @DuplicateFlag        Indicates whether the estimate is a duplicate
* (I) @EstimateTypeCD       Indicates whether the estimate is (O)riginal or (A)udited
* (I) @NetTotalAmt          The Net total of the estimate (counting adjustments)
* (I) @OtherAdjustmentAmt   The amount of other adjustments
* (I) @RepairTotalAmt       The repair total of the estimate without applied adjustments
* (l) @BettermentTotalAmt   The betterment amount
* (I) @SupplementSeqNumber  The supplement sequence number
* (I) @AgreedPriceMetCD     Was the price agreed to by the shop
* (I) @UserID               UserID performing the update
*
* New Parameters are Added By Glsd451
* (I) @ShopContacted		Indicates Whether the shopcontacted is Yes or No
* (I) @ShopContactName			Name of the ShopContacted
* (I) @PrimaryReasonCode	Values of PrimaryReasonCode Checkbox
* (I) @PrimaryReasonCodeText Values for PrimaryReasonCode Textbox
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspEstimateQuickUpdDetail]
    @DocumentID             udt_std_id_big,
    @ClaimAspectID          udt_std_id_big,
    @AdjustmentTotalAmt     decimal(9,2),
    @DeductibleAmt          decimal(9,2),
    @DocumentTypeID         udt_std_id,
    @DuplicateFlag          udt_std_flag,
    @EstimateTypeCD         udt_std_cd,
    @NetTotalAmt            decimal(9,2),
    @OtherAdjustmentAmt     decimal(9,2),
    @RepairTotalAmt         decimal(9,2),
    @BettermentTotalAmt     decimal(9,2),
    @SupplementSeqNumber    udt_std_int_tiny,
    @AgreedPriceMetCD       udt_std_cd,
    @TaxTotalAmt            decimal(9, 2),
    @ApprovedFlag           udt_std_flag,
    @WarrantyFlag           udt_std_flag,
    @DocumentEditFlag       udt_std_flag,
    @UserID                 udt_std_id,

-- New Parameters added by Glsd451
	@ShopContacted			udt_std_flag,
	@ShopContactName		udt_std_desc_mid,
	@PrimaryReasonCode		udt_std_note,
	@PrimaryReasonCodeText  udt_std_note,
	@SecondaryReasonCode	udt_std_note,
	@SecondaryReasonCodeText udt_std_note
AS	
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@EstimateTypeCD))) = 0 SET @EstimateTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@AgreedPriceMetCD))) = 0 SET @AgreedPriceMetCD = NULL


    -- Declare internal variables

    DECLARE @DocumentVANSourceFlag                  udt_std_flag
    DECLARE @DateLastUpdatedAdjustmentTotal         varchar(30)
    DECLARE @DateLastUpdatedContractPrice           varchar(30)
    DECLARE @DateLastUpdatedDeductible              varchar(30)
    DECLARE @DateLastUpdatedNetTotal                varchar(30)
    DECLARE @DateLastUpdatedOtherAdjustment         varchar(30)
    DECLARE @DateLastUpdatedRepairTotal             varchar(30)
    DECLARE @DateLastUpdatedBettermentTotal         varchar(30)
    DECLARE @DateLastUpdatedDeductiblesAppliedTotal varchar(30)
    DECLARE @DateLastUpdatedLimitEffectTotal        varchar(30)
    DECLARE @DateLastUpdatedNetTotalEffectTotal     varchar(30)
    DECLARE @DateLastUpdatedTaxTotal                varchar(30)
    DECLARE @AOINameFirst							varchar(30)
    DECLARE @AOINameLast							varchar(30)
    
    DECLARE @EstimateSummaryIDAdjustmentTotal       udt_std_id
    DECLARE @EstimateSummaryIDContractPrice         udt_std_id
    DECLARE @EstimateSummaryIDDeductible            udt_std_id
    DECLARE @EstimateSummaryIDNetTotal              udt_std_id
    DECLARE @EstimateSummaryIDOtherAdjustment       udt_std_id
    DECLARE @EstimateSummaryIDRepairTotal           udt_std_id
    DECLARE @EstimateSummaryIDBetterment            udt_std_id
    DECLARE @EstimateSummaryIDDeductiblesApplied    udt_std_id
    DECLARE @EstimateSummaryIDLimitEffect           udt_std_id
    DECLARE @EstimateSummaryIDNetTotalEffect        udt_std_id
    DECLARE @EstimateSummaryIDTaxTotal              udt_std_id
    DECLARE @LynxID									udt_std_id_big
    
    DECLARE @LimitEffectAmt                         udt_std_money
    DECLARE @NetTotalEffectAmt                      udt_std_money
    
    DECLARE @ClaimAspectServiceChannelID            bigint
    DECLARE @ClaimCoverageID                        bigint
    DECLARE @ExposureCD                             varchar(4)
    DECLARE @EventID                                int
    DECLARE @EventName                              varchar(50)
    DECLARE @PHNotificationDocumentTypeID           int

    DECLARE @error AS int
    DECLARE @rowcount AS int

    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspEstimateQuickUpdDetail'


    -- Set Database options

    SET NOCOUNT ON


    -- Check to make sure a valid Document id was passed in

    IF  (@DocumentID IS NULL) OR
        (NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID))
    BEGIN
        -- Invalid Document ID

        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
     RETURN
    END


    -- Check to make sure a valid Claim Aspect id was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspect ID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure the document id belongs to the claim aspect id
    --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    IF  (NOT EXISTS(
                    SELECT ClaimAspectID 
                    FROM dbo.utb_claim_aspect_service_channel_document  cascd
                    INNER JOIN    utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                    WHERE DocumentID = @DocumentID 
                    AND ClaimAspectID = @ClaimAspectID
                   )
        )
    --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    BEGIN
        -- Invalid ClaimAspect ID for the Document id

        RAISERROR('%s|Claim Aspect ID invalid for Document', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure EstimateTypeCD is valid

    IF (@EstimateTypeCD IS NOT NULL) AND
       (@EstimateTypeCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')))
    BEGIN
        -- Invalid Estimate Type CD

        RAISERROR('101|%s|@EstimateTypeCD|%s', 16, 1, @ProcName, @EstimateTypeCD)
        RETURN
    END


    -- Check to make sure AgreedPriceMetCD is valid

    IF (@AgreedPriceMetCD IS NOT NULL) AND
       (@AgreedPriceMetCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'AgreedPriceMetCD')))
    BEGIN
        -- Invalid Agreed Price Met CD

        RAISERROR('101|%s|@AgreedPriceMetCD|%s', 16, 1, @ProcName, @AgreedPriceMetCD)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Validate APD Date State

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'AdjustmentTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"AdjustmentTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'CP' AND Name = 'ContractPrice')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"ContractPrice"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Deductible')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Deductible"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'NetTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Other')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Other (Other Adjustment)"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'RepairTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Betterment')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Betterment"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'DeductiblesApplied')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"DeductiblesApplied"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'LimitEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"LimitEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'NetTotalEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotalEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'TaxTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"TaxTotal"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF @ApprovedFlag = 1 AND @AgreedPriceMetCD = 'N'
    BEGIN
      -- Cannot set Approved Flag to Yes for an estimate that has Agreed Price Met as No
      SET @ApprovedFlag = 0
    END
    
    
   
    

    -- Check to see if we're updating a VAN sourced document.  If so, we'll ignore the amounts
    -- (the user shouldn't have edited those anyway)

    SELECT @DocumentVANSourceFlag = ds.VANFlag
      FROM dbo.utb_document d
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
      WHERE DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    FROM utb_document d
    LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
    WHERE d.DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the claim coverage based on the vehicle coverage.
    SELECT @ClaimCoverageID = cc.ClaimCoverageID,
           @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID and ca.CoverageProfileCD = cc.CoverageTypeCD
    WHERE ca.ClaimAspectID = @ClaimAspectID
      AND cc.EnabledFlag = 1
      AND cc.AddtlCoverageFlag = 0

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
   
    SELECT @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    WHERE ca.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Initialize all the estimate summary ids to 0

        SET @EstimateSummaryIDAdjustmentTotal = 0
        SET @EstimateSummaryIDContractPrice = 0
        SET @EstimateSummaryIDDeductible = 0
        SET @EstimateSummaryIDNetTotal = 0
        SET @EstimateSummaryIDOtherAdjustment = 0
        SET @EstimateSummaryIDRepairTotal = 0
        SET @EstimateSummaryIDBetterment = 0
        SET @EstimateSummaryIDTaxTotal = 0
        
        SET @EstimateSummaryIDDeductiblesApplied = 0
        SET @EstimateSummaryIDLimitEffect = 0
        SET @EstimateSummaryIDNetTotalEffect = 0
        -- Get the ids for any existing estimate summary records

        SELECT  @EstimateSummaryIDAdjustmentTotal = EstimateSummaryID,
                @DateLastUpdatedAdjustmentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'AdjustmentTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDContractPrice = EstimateSummaryID,
                @DateLastUpdatedContractPrice = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'CP'
            AND est.Name = 'ContractPrice'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDDeductible = EstimateSummaryID,
                @DateLastUpdatedDeductible = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Deductible'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDNetTotal = EstimateSummaryID,
                @DateLastUpdatedNetTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'NetTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDOtherAdjustment = EstimateSummaryID,
                @DateLastUpdatedOtherAdjustment = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Other'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDRepairTotal = EstimateSummaryID,
                @DateLastUpdatedRepairTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'RepairTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        SELECT  @EstimateSummaryIDTaxTotal = EstimateSummaryID,
                @DateLastUpdatedTaxTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'TaxTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        
        SELECT  @EstimateSummaryIDBetterment = EstimateSummaryID,
                @DateLastUpdatedBettermentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Betterment'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDDeductiblesApplied = EstimateSummaryID,
                @DateLastUpdatedDeductiblesAppliedTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'DeductiblesApplied'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDLimitEffect = EstimateSummaryID,
                @LimitEffectAmt = OriginalExtendedAmt,
                @DateLastUpdatedLimitEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'LimitEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDNetTotalEffect = EstimateSummaryID,
                @DateLastUpdatedNetTotalEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'NetTotalEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SET @NetTotalEffectAmt = isNull(@RepairTotalAmt, 0) - isNull(@DeductibleAmt, 0) - isNull(@LimitEffectAmt, 0)
        
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

        -- Perform an update

        UPDATE  dbo.utb_document
           SET  DocumentTypeID          = @DocumentTypeID,
                DuplicateFlag           = @DuplicateFlag,
                EstimateTypeCD          = @EstimateTypeCD,
                EstimateReviewDate      = @now,
                AgreedPriceMetCD        = @AgreedPriceMetCD,
                SupplementSeqNumber     = @SupplementSeqNumber,
                WarrantyFlag            = @WarrantyFlag,
                SysLastUserID           = @UserID,
                SysLastUpdatedDate      = @now,
				ShopContacted			= @ShopContacted,
				ShopContactName			= @ShopContactName,			
				PrimaryReasonCode		= @PrimaryReasonCode,
				PrimaryReasonCodeText   = @PrimaryReasonCodeText,
				SecondaryReasonCode		= @SecondaryReasonCode,
				SecondaryReasonCodeText = @SecondaryReasonCodeText 
        WHERE DocumentID = @DocumentID

        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

        -- Check error value

        IF @error <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END



    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Let's update the estimate summary

        -- First, delete all existing estimate summary records that we're not going to change

        DELETE FROM dbo.utb_estimate_summary
          WHERE DocumentID = @DocumentID
            AND EstimateSummaryID NOT IN (@EstimateSummaryIDAdjustmentTotal,
                                          @EstimateSummaryIDContractPrice,
                                          @EstimateSummaryIDDeductible,
                                          @EstimateSummaryIDNetTotal,
                                          @EstimateSummaryIDOtherAdjustment,
                                          @EstimateSummaryIDRepairTotal,
                                          @EstimateSummaryIDBetterment,
                                          @EstimateSummaryIDDeductiblesApplied,
                                          @EstimateSummaryIDLimitEffect,
                                          @EstimateSummaryIDNetTotalEffect,
                                          @EstimateSummaryIDTaxTotal)

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('106|%s|utb_estimate_summary', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDContractPrice,
                                              @EstimateSummaryType      = 'ContractPrice',
                                              @EstimateSummaryTypeCD    = 'CP',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedContractPrice,
                                              @ReturnSysLastUpdate      = 0

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Contract Price)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDRepairTotal,
                                              @EstimateSummaryType      = 'RepairTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedRepairTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Repair Total)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductible,
                                              @EstimateSummaryType      = 'Deductible',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductible,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Deductible)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDOtherAdjustment,
                                              @EstimateSummaryType      = 'Other',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @OtherAdjustmentAmt,
                                              @AgreedUnitAmt            = @OtherAdjustmentAmt,
                                              @OriginalExtendedAmt      = @OtherAdjustmentAmt,
                                              @OriginalUnitAmt          = @OtherAdjustmentAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedOtherAdjustment,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDAdjustmentTotal,
                                              @EstimateSummaryType      = 'AdjustmentTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @AdjustmentTotalAmt,
                                              @AgreedUnitAmt            = @AdjustmentTotalAmt,
                                              @OriginalExtendedAmt      = @AdjustmentTotalAmt,
                                              @OriginalUnitAmt          = @AdjustmentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedAdjustmentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDTaxTotal,
                                              @EstimateSummaryType      = 'TaxTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @TaxTotalAmt,
                                              @AgreedUnitAmt            = @TaxTotalAmt,
                                              @OriginalExtendedAmt      = @TaxTotalAmt,
                                              @OriginalUnitAmt          = @TaxTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedTaxTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotal,
                                              @EstimateSummaryType      = 'NetTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @NetTotalAmt,
                                              @AgreedUnitAmt            = @NetTotalAmt,
                                              @OriginalExtendedAmt      = @NetTotalAmt,
                                              @OriginalUnitAmt          = @NetTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDBetterment,
                                              @EstimateSummaryType      = 'Betterment',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @BettermentTotalAmt,
                                              @AgreedUnitAmt            = @BettermentTotalAmt,
                                              @OriginalExtendedAmt      = @BettermentTotalAmt,
                                              @OriginalUnitAmt          = @BettermentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedBettermentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductiblesApplied,
                                              @EstimateSummaryType      = 'DeductiblesApplied',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductiblesAppliedTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (DeductiblesApplied)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDLimitEffect,
                                              @EstimateSummaryType      = 'LimitEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @LimitEffectAmt,
                                              @AgreedUnitAmt            = @LimitEffectAmt,
                                              @OriginalExtendedAmt      = @LimitEffectAmt,
                                              @OriginalUnitAmt          = @LimitEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedLimitEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (LimitEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotalEffect,
                                              @EstimateSummaryType      = 'NetTotalEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @NetTotalEffectAmt,
                                              @AgreedUnitAmt            = @NetTotalEffectAmt,
                                              @OriginalExtendedAmt      = @NetTotalEffectAmt,
                                              @OriginalUnitAmt          = @NetTotalEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotalEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (NetTotalEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        IF @ClaimCoverageID IS NOT NULL AND @ClaimAspectServiceChannelID IS NOT NULL
        BEGIN
           -- Now check if the deductible applied record exist for the claim aspect service channel.
           IF EXISTS (SELECT ClaimAspectServiceChannelID
                      FROM dbo.utb_claim_aspect_service_channel_coverage
                      WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND ClaimCoverageID = @ClaimCoverageID)
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               UPDATE dbo.utb_claim_aspect_service_channel_coverage
               SET DeductibleAppliedAmt = @DeductibleAmt
               WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                 AND ClaimCoverageID = @ClaimCoverageID
   
               IF @error <> 0
               BEGIN
                   -- Update failed
       
                   RAISERROR('104|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
           ELSE
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               INSERT INTO dbo.utb_claim_aspect_service_channel_coverage
                  (ClaimAspectServiceChannelID, 
                   ClaimCoverageID, 
                   DeductibleAppliedAmt, 
                   SysLastUserID, 
                   SysLastUpdatedDate)
               VALUES
                  (@ClaimAspectServiceChannelID, 
                   @ClaimCoverageID, 
                   @DeductibleAmt, 
                   @UserID, 
                   @now)
   
               IF @error <> 0
               BEGIN
                   -- Insert failed
       
                   RAISERROR('105|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
        END

	Select @LynxID = LynxID from
			 utb_claim_aspect where ClaimAspectID = @ClaimAspectID	
			 
	IF @LynxID is Not Null
	BEGIN			 
		Select @AOINameFirst = i.NameFirst,
			   @AOINameLast = i.NameLast
			From utb_involved i
		INNER JOIN dbo.utb_claim c on i.InvolvedID = c.CallerInvolvedID
		WHERE c.LynxID = @LynxID	
	END


        IF @ExposureCD = '1' AND @EstimateTypeCD = 'A' AND @AgreedPriceMetCD = 'Y' AND @AOINameFirst = 'AOI' AND @AOINameLast = 'Quick Claims'
        BEGIN
            -- Adding an price agreed audited estimate for a 1st party vehicle.
            
            -- Check if the PH Notifcation document exists
            SELECT @PHNotificationDocumentTypeID = DocumentTypeID
            FROM utb_document_type
            WHERE Name = 'PH Notification'

            IF NOT EXISTS(SELECT d.DocumentID
                           FROM utb_document d
                           LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
                           LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                           WHERE d.DocumentTypeID = @PHNotificationDocumentTypeID 
                             AND casc.ClaimAspectID = @ClaimAspectID
                             AND d.EnabledFlag = 1)
            BEGIN
               -- Raise the PH Notofication event
               SET @EventName = 'First Party Audited Estimate'

               SELECT  @EventID = EventID 
               FROM  utb_event 
               WHERE Name = @EventName

               IF @@ERROR <> 0
               BEGIN
                  -- SQL Server Error

                  RAISERROR('99|%s', 16, 1, @ProcName)
                  RETURN
               END              

               IF @EventID IS NULL
               BEGIN
                  -- Event Name not found

                  RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
                  ROLLBACK TRANSACTION
                  RETURN
               END


               -- Make the workflow call

               EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                           @ClaimAspectID = @ClaimAspectID,
                                           @Description = 'Audited Estimate with agreed price added to a First Party vehicle',
                                           @Key = NULL,
                                           @UserID = @UserID,
                                           @ConditionValue = 'DA',
                                           @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

            END
        END
    END


    COMMIT TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    SELECT  1                   AS tag,
            NULL                AS parent,
            @DocumentID         AS [Root!1!DocumentID],
            @Now                AS [Root!1!SysLastUpdatedDate]

--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateQuickUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateQuickUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO