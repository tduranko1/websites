-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateChangesGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateChangesGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateChangesGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspEstimateChangesGetDetailXML
(
    @DocumentID         udt_std_id_big,
--    @ShopLocationID     udt_std_id_big,
    @UserID             udt_std_id_big,
    @EstimateChanges    varchar(8000)
)
AS
BEGIN
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                AS datetime
    DECLARE @LineItem           AS varchar(8000)
    DECLARE @Lynxid             AS udt_std_id_big
    DECLARE @ShopName           AS varchar(200)
    DECLARE @ShopFaxNumber      AS varchar(10)
    DECLARE @ClaimAnalyst       AS varchar(100)
    DECLARE @ClaimAnalystPhone  AS varchar(15)
    DECLARE @VehicleOwner       AS varchar(100)
    DECLARE @VehicleDescription AS varchar(150)
    DECLARE @PertainsTo         AS varchar(10)
    DECLARE @LogonID            AS udt_sys_login
    DECLARE @DocumentDesc       AS varchar(100)
    DECLARE @DocumentCreatedOn  AS datetime

    SET @ProcName = 'uspEstimateChangesGetDetailXML'


    -- Set Database options
    
    SET NOCOUNT ON
    
    -- Validate the inputs
    IF @DocumentID is NULL OR NOT EXISTS(SELECT DocumentID
                                            FROM dbo.utb_document
                                            WHERE DocumentID = @DocumentID)
    BEGIN
        -- Invalid Document ID
    
        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
        RETURN        
    END

    /*IF @ShopLocationID is NULL OR NOT EXISTS(SELECT ShopLocationID
                                                FROM dbo.utb_shop_location
                                                WHERE ShopLocationID = @ShopLocationID)
    BEGIN
        -- Invalid Shop Location ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN        
    END*/
    
    IF LEN(LTRIM(RTRIM(@EstimateChanges))) = 0
    BEGIN
        -- Invalid Estimate Changes
    
        RAISERROR('101|%s|@EstimateChanges|%u', 16, 1, @ProcName, @EstimateChanges)
        RETURN        
    END
    
    IF @UserID is NULL OR NOT EXISTS(SELECT UserID
                                        FROM dbo.utb_user
                                        WHERE UserID = @UserID)
    BEGIN
        -- Invalid UserID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN        
    END

    SET @now = CURRENT_TIMESTAMP
    
    DECLARE @tblEstimateChanges TABLE (
        LineItem  varchar(8000)  NOT NULL
    )
    
    DECLARE @tblEstimateChangesLineItems TABLE (
        LineNumber      varchar(50)     NULL,  --Project:210474 APD Modified the datatype definition when we did the code merge M.A.20061117
        ChangesText     varchar(7950)   NULL  --Project:210474 APD Modified the datatype definition when we did the code merge M.A.20061117
    )
    
    -- The Estimate changes will be of the following format
    -- Line1~Description1||Line2~Description2
    -- Each line will be delimited by double pipe and within each line
    -- the line number and the text will be delimited by tilde ~.
    
    -- Extract each line of changes
    INSERT INTO @tblEstimateChanges
    SELECT value FROM dbo.ufnUtilityParseString(@EstimateChanges, '|', 1)
    
    -- Extract the individual columns for each line
    DECLARE curLineItems CURSOR FOR
    SELECT * FROM @tblEstimateChanges
    
    OPEN curLineItems

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM curLineItems
    INTO @LineItem

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
    
        INSERT INTO @tblEstimateChangesLineItems
        SELECT (SELECT value FROM dbo.ufnUtilityParseString(@LineItem, '~', 1) WHERE strIndex = 1),
               (SELECT value FROM dbo.ufnUtilityParseString(@LineItem, '~', 1) WHERE strIndex = 2)
        
        FETCH next
        FROM curLineItems
        INTO @LineItem

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE curLineItems
    DEALLOCATE curLineItems
    
    -- Get the claim information
    SELECT @LynxID = ca.LynxID,
           @PertainsTo = cat.Code + convert(varchar, ca.ClaimAspectNumber),
           @ClaimAnalyst = isNull(u.NameFirst, '') + ' ' + isNull(u.NameLast, ''),
           @ClaimAnalystPhone = '(' + isNull(u.PhoneAreaCode, '') + ') ' + isNull(u.PhoneExchangeNumber, '') + '-' + isNull(u.PhoneUnitNumber, '') + isNull('x' + u.PhoneExtensionNumber, ''),
           @VehicleDescription = isNull(convert(varchar, cv.VehicleYear), '') + ' ' + isNull(cv.Make, '') + ' ' + isNull(cv.Model, ''),
           @VehicleOwner = IsNull((SELECT  Top 1 CASE 
                                                    WHEN i.BusinessName IS NOT NULL THEN i.BusinessName
                                                    ELSE isNull(i.NameFirst, '')  + ' ' + isNull(i.NameLast, '')
                                                 END 
                                       FROM  dbo.utb_claim_aspect_involved cai
                                       LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                       LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                       LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                       WHERE cai.ClaimAspectID = cv.ClaimAspectID
                                         AND cai.EnabledFlag = 1
                                         AND irt.Name = 'Owner'), ''),
            @DocumentDesc = (CASE
                                WHEN dt.Name = 'Supplement' THEN dt.Name + ' ' + convert(varchar, d.SupplementSeqNumber)
                                ELSE dt.Name
                               END),
            @DocumentCreatedOn = d.CreatedDate,
            @ShopName = sl.Name,
            @ShopFaxNumber = sl.FaxAreaCode + sl.FaxExchangeNumber + sl.FaxUnitNumber
    FROM dbo.utb_document d 
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (d.DocumentID = cascd.DocumentID)--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    Left OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)--Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (casc.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_assignment a ON (d.AssignmentID = a.AssignmentID)
    LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_user u ON (ca.AnalystUserID = u.UserID)  --Project:210474 APD Modified the joins when we did the code merge M.A.20061117
    LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
    WHERE d.DocumentID = @DocumentID
    
/*    SELECT @ShopName = Name,
           @ShopFaxNumber = FaxAreaCode + FaxExchangeNumber + FaxUnitNumber
    FROM dbo.utb_shop_location
    WHERE ShopLocationID = @ShopLocationID*/
    
    SELECT @LogonID = LogonID
    FROM dbo.utb_user_application ua
    LEFT JOIN dbo.utb_application a ON (ua.ApplicationID = a.ApplicationID)
    WHERE ua.UserID = @UserID
      AND a.Name = 'APD'
    
    
    -- Now do the final select
    SELECT
        1                                       AS Tag,
        NULL                                    AS Parent,
        --Root
        @DocumentID                             AS [Root!1!DocumentID],
        --@ShopLocationID                         AS [Root!1!ShopLocationID],
        @UserID                                 AS [Root!1!UserID],
        @LogonID                                AS [Root!1!NTUserID],
        --Estimate Details
        NULL                                    AS [Estimate!2!ReportDateTime],
        NULL                                    AS [Estimate!2!LynxID],
        NULL                                    AS [Estimate!2!PertainsTo],
        NULL                                    AS [Estimate!2!EstimatorName],
        NULL                                    AS [Estimate!2!ShopFaxNumber],
        NULL                                    AS [Estimate!2!ClaimAnalystName],
        NULL                                    AS [Estimate!2!ClaimAnalystPhone],
        NULL                                    AS [Estimate!2!VehicleOwner],
        NULL                                    AS [Estimate!2!VehicleDescription],
        NULL                                    AS [Estimate!2!DocumentDescription],
        NULL                                    AS [Estimate!2!DocumentReceivedOn],
        -- Estimate Change Line Items
        NULL                                    AS [EstimateChanges!3!LineNumber],
        NULL                                    AS [EstimateChanges!3!Description]
        
    UNION ALL
    
    SELECT 
        2,
        1,
        -- Root
        NULL, NULL, NULL, --NULL,
        -- Estimate Details
        convert(varchar, @now, 101),
        @LynxID,
        @PertainsTo,
        @ShopName,
        @ShopFaxNumber,
        @ClaimAnalyst,
        @ClaimAnalystPhone,
        @VehicleOwner,
        @VehicleDescription,
        @DocumentDesc,
        convert(varchar, @DocumentCreatedOn, 101),
        -- Estimate Change Line Items
        NULL, NULL
   
   UNION ALL
   
   SELECT
        3,
        2,
        -- Root
        NULL, NULL, NULL, --NULL,
        -- Estimate Details
        NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
        NULL,
        -- Estimate Change Line Items
        LineNumber,
        ChangesText
    FROM @tblEstimateChangesLineItems
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateChangesGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateChangesGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/