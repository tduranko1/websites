-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQCloseClaimsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetHQCloseClaimsWSXML 
END

GO 

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceGetHQCloseClaimsWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       James Kennedy
* FUNCTION:     Get a list of claims where all vehicles are closed and closes the claim
* Date:			06Dec2016
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	06Jan2016 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceGetHQCloseClaimsWSXML]
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceGetHQCloseClaimsWSXML'
	SET @Now = CURRENT_TIMESTAMP

	------------------------------------------------------
	-- Processing Begins
	------------------------------------------------------
	IF @Debug = 1
	BEGIN
		PRINT 'Choice Close Claim processing began at: ' + CONVERT(VARCHAR(50),@Now)
	END

	---------------------------------------
	-- Create Temp table to hold the details
	-- as we build them
	---------------------------------------
	------------------------------------------------------
	-- Processing
	------------------------------------------------------
	IF @Debug = 1
	BEGIN
		PRINT 'Processing: ' + CONVERT(VARCHAR(50),@Now)
	END

	CREATE TABLE #ChoiceClaims (
		LynxID INT
		, ClaimAspectIDClaim INT
		, ClaimAspectIDVehicle INT
		, AssignmentTypeID INT
		, VehicleCloseStatus BIT
	)

	-------------------------------------------
	-- Gets unique Choice LynxIDs that have at 
	-- least 1 vehicle closed or cancelled
	-------------------------------------------
	INSERT INTO #ChoiceClaims 
	SELECT DISTINCT
		ca.LynxID
		, 0 
		, 0 
		, 0
		, 0
	FROM 
		utb_claim_aspect ca 
		LEFT JOIN dbo.utb_claim_aspect_status cas 
			ON (cas.ClaimAspectID = ca.ClaimAspectID)
	WHERE 
		ca.InitialAssignmentTypeID IN (17)  -- Choice Claims
		AND cas.StatusID IN (998,994) -- Vehicle Close/Cancelled

	---------------------------------------------
	-- Gets unique Vehicles and Assignment Type 
	-- for the above LynxID's
	---------------------------------------------
	INSERT INTO #ChoiceClaims 
	SELECT 
		ca.LynxID
		, (SELECT ClaimAspectID FROM utb_claim_aspect WHERE ClaimAspectTypeID = 0 AND LynxID = ca.LynxID)
		, ca.ClaimAspectID
		, ca.InitialAssignmentTypeID
		, 0
	FROM 
		utb_claim_aspect ca 
		INNER JOIN #ChoiceClaims cc
		ON cc.LynxID = ca.LynxID
	WHERE 
		ca.ClaimAspectTypeID = 9
	GROUP BY
		ca.LynxID
		, ca.ClaimAspectID
		, ca.InitialAssignmentTypeID

	------------------------------
	-- Gets rid of the seed rows
	------------------------------
	DELETE FROM #ChoiceClaims WHERE ClaimAspectIDVehicle = 0

	------------------------------
	-- Deletes all lynxid that have
	-- any non Choice Vehicles
	------------------------------
	DELETE FROM 
		#ChoiceClaims 
	WHERE 
		LynxID IN (
			SELECT	
				LynxID	
			FROM 
				#ChoiceClaims
			WHERE
				AssignmentTypeID <> 17
		)


	------------------------------
	-- Delete any LynxID's that are
	-- all ready closed
	------------------------------
	DELETE FROM 
		#ChoiceClaims 
	WHERE 
		LynxID IN (
			SELECT	
				LynxID	
			FROM 
				#ChoiceClaims cc
				LEFT JOIN dbo.utb_claim_aspect_status cas 
				ON (cas.ClaimAspectID = cc.ClaimAspectIDClaim)
			WHERE
				cas.StatusID IN (999)
		)
			
	-----------------------------------------
	-- Sets the status on each Vehicles that 
	-- are closed
	-----------------------------------------
	UPDATE 
		#ChoiceClaims
	SET VehicleCloseStatus = 1 
	FROM (
		SELECT 
			ClaimAspectID
		FROM
			utb_claim_aspect_status cas
		WHERE
			cas.StatusID IN (998,994) -- Vehicle Close/Cancelled
	) cas
	WHERE			
		cas.ClaimAspectID = #ChoiceClaims.ClaimAspectIDVehicle

	IF EXISTS (SELECT * FROM #ChoiceClaims)
	BEGIN
		------------------------------------------------------
		-- Create HQ Invoice/Close Assignment list XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!LynxID]   
			, NULL				AS [Claim!2!ClaimAspectIDClaim]
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]

		UNION ALL

		---------------------------------------
		-- List of unique LynxID's
		-- ready to close (No unclosed Vehicles)
		----------------------------------------
		SELECT DISTINCT 2 AS Tag,  
				1 AS Parent
				, NULL
				, cc.LynxID
				, cc.ClaimAspectIDClaim
				, cc.ClaimAspectIDVehicle
		FROM 
			#ChoiceClaims cc
		WHERE 
			cc.VehicleCloseStatus = 1 
			AND cc.lynxid NOT IN (
				SELECT	
					c2.LynxID 
				FROM
					#ChoiceClaims c2
				WHERE
					c2.LynxID = cc.LynxID
					AND c2.VehicleCloseStatus = 0
			) 

		FOR XML EXPLICIT	
		DROP TABLE #ChoiceClaims
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- NO Choice Claims ready to close
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'NO Choice Claims ready to close'
		END

		------------------------------------------------------
		-- Create NO JOBS XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Claim!2!LynxID]   
			, NULL				AS [Claim!2!ClaimAspectIDClaim]
			, NULL				AS [Claim!2!ClaimAspectIDVehicle]

		FOR XML EXPLICIT	
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQCloseClaimsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceGetHQCloseClaimsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/