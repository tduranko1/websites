-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageGetDetailsXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCoverageGetDetailsXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCoverageGetDetailsXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Retrieves the claim and the coverage information for the provided LynxID
*
* PARAMETERS:  
* (I) @LynxID        		The LynxID for the claim
* (I) @InsuranceCompanyID	The Insurance Company ID to go with the claim
*
* RESULT SET:
* An XML Data stream containing claim and the coverage information
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE dbo.uspClaimCoverageGetDetailsXML	
						@LynxID udt_std_int_big, 
						@InsuranceCompanyID udt_std_int_small
AS

BEGIN

    SET nocount on
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspClaimCoverageGetDetailsXML'

 	-- If the Lynx id that was passed by the caller was invalid 
    IF  NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
	-- then raise error and return/exit
    BEGIN
        -- Invalid Lynx ID
            RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

 	-- Now see if the Lynx id has the valid Insurance Company ID that was provided by the caller 
    IF  NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID and InsuranceCompanyID = @InsuranceCompanyID)
	-- then raise error and return/exit
    BEGIN
        -- Invalid Insurance Company ID
            RAISERROR('102|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

	/*************************************************************************************
	*  Gather Reference Data
	**************************************************************************************/
    DECLARE @tmpReference TABLE 
    	(
        ListName        varchar(50)             NOT NULL,
        DisplayOrder    int                     NULL,
        ReferenceId     varchar(10)             NOT NULL,
        Name            varchar(50)             NOT NULL,
        CatStateCode    char(2)                 NULL,
        CatLossNumber   varchar(30)             NULL,
        LossParentID    int                     NULL
    	)

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name, CatStateCode, CatLossNumber, LossParentID)
    SELECT  'CoverageTypeCD',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_coverage', 'CoverageTypeCD' )

	UNION ALL

	SELECT 'ClientCoverageType',
			DisplayOrder,
			CoverageProfileCD,
			Name,
			NULL,
			NULL,
			ClientCoverageTypeID

	from	dbo.utb_client_coverage_type
	where	InsuranceCompanyID = @InsuranceCompanyID
	and		EnabledFlag = 1

	order by 1,2
			


    IF @@ERROR <> 0
	    BEGIN
	        -- Insertion failure
	    
	        RAISERROR('103|%s|@tmpReference', 16, 1, @ProcName)
	        RETURN
	    END

	-- XML SELECT

	-- Select Root Level

	SELECT 		1 as Tag,
				NULL as Parent,
				@LynxID as [Root!1!LynxID],
				-- Coverage information
				NULL as [Coverage!2!ClaimCoverageID],
				NULL as [Coverage!2!ClientCoverageTypeID],
				NULL as [Coverage!2!LynxID],
				NULL as [Coverage!2!AddtlCoverageFlag],
				NULL as [Coverage!2!CoverageTypeCD],
				NULL as [Coverage!2!Description],
				NULL as [Coverage!2!ClientCoverageDesc],
				NULL as [Coverage!2!DeductibleAmt],
				NULL as [Coverage!2!LimitAmt],
				NULL as [Coverage!2!LimitDailyAmt],
				NULL as [Coverage!2!MaximumDays],
				NULL as [Coverage!2!SysLastUserID],
				NULL as [Coverage!2!SysLastUpdatedDate],
				--Reference Data
		        Null as [Reference!3!List],
		        Null as [Reference!3!ReferenceID],
		        Null as [Reference!3!Name],
		        NULL AS [Reference!3!StateCode],
		        NULL AS [Reference!3!CatastrophicLossNumber],
		        Null as [Reference!3!ParentID]

	UNION ALL

	--	Coverage data for the LynxID
	SELECT		2,
				1,
				NULL,
				-- Coverage Information
				cc.ClaimCoverageID,
				ISNULL(cast(cc.ClientCoverageTypeID as varchar(20)),''),
				cc.LynxID,
				cc.AddtlCoverageFlag,
				cc.CoverageTypeCD,
				ISNULL(cc.Description,''),
				ISNULL(cct.Name,''),
				cc.DeductibleAmt,
				ISNULL(cast(cc.LimitAmt as varchar(20)),''),
				ISNULL(cast(cc.LimitDailyAmt as varchar(20)),''),
				ISNULL(cast(cc.MaximumDays as varchar(20)),''),
				cc.SysLastUserID,
				dbo.ufnUtilityGetDateString(cc.SysLastUpdatedDate),
				-- Reference data
				NULL, NULL, NULL, NULL, NULL,
				NULL

	FROM		dbo.utb_Claim c

	LEFT OUTER JOIN	dbo.utb_Claim_Coverage cc
	on			c.LynxID = cc.LynxID

	LEFT OUTER JOIN	dbo.utb_Client_Coverage_Type cct
	on			cc.ClientCoverageTypeID = cct.ClientCoverageTypeID

	WHERE 		c.LynxID = @LynxID


	UNION All

	-- Reference data
	SELECT		3,
				1,
				NULL,
				-- Coverage Information
				NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL, NULL, NULL,
				NULL, NULL, NULL,
				--Reference data
		        ListName,
		        ReferenceID,
		        Name,
		        CatStateCode,
		        CatLossNumber,
		        LossParentID

    FROM        @tmpReference

	ORDER BY	1,2

	-- FOR XML EXPLICIT  (Commented for client-side processing)
	IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
		
		    RAISERROR('99|%s', 16, 1, @ProcName)
		    RETURN
		END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageGetDetailsXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCoverageGetDetailsXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


