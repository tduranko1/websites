-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCheckIFExistsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCheckIFExistsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCheckIFExistsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns XML Representing an existing claim if it exists
* DATE:			10May2017
*
* PARAMETERS:  
*
* REVISIONS:	10May2017 - TVD - Initial Development
*				08Aug2017 - TVD - Added code to exclude by Insc Company
*
* RESULT SET:
*				Claim XML Details
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspClaimCheckIFExistsWSXML]
	@vClientClaimNumber				udt_cov_claim_number
	, @iExposureCD					INT
	, @iClaimAspectNumber			INT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspClaimCheckIFExistsWSXML'
    SET @ApplicationCD = 'APD'

    -- Validation
	DECLARE @ClientClaimNumberSquished	udt_cov_claim_number
	SET @ClientClaimNumberSquished = dbo.ufnUtilitySquishString(@vClientClaimNumber, 1, 1, 0, NULL)
    
    -- Begin main code
	IF EXISTS 
	(
		SELECT 
			ca.*
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.lynxid = c.lynxid
		WHERE
			(ClientClaimNumber = @vClientClaimNumber OR ClientClaimNumberSquished = @ClientClaimNumberSquished)
			AND ca.ExposureCD = @iExposureCD
			AND ca.ClaimAspectNumber = @iClaimAspectNumber
			AND c.InsuranceCompanyID NOT IN (
				SELECT 
					Value
				FROM 
					utb_app_variable 
				WHERE 
					Name = 'Duplicate_Claim_Exclude' 
					AND SubName = 'WebService' 
			)
	)
	BEGIN
		SELECT TOP 1
			1 AS Tag
			, NULL AS Parent
			, c.LynxID AS [Root!1!LynxID]
			, c.InsuranceCompanyID AS [Root!1!InsuranceCompanyID]
			, c.ClientClaimNumber AS [Root!1!ClientClaimNumber]
			, u.UserID AS [Root!1!CarrierRepUserID]
			, u.NameFirst AS [Root!1!CarrierRepNameFirst]
			, u.NameLast AS [Root!1!CarrierRepNameLast]
			, u.PhoneAreaCode + '-' + PhoneExchangeNumber + '-' + PhoneUnitNumber AS [Root!1!CarrierRepPhoneDay]
			, u.EmailAddress AS [Root!1!CarrierRepEmailAddress]
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.lynxid = c.lynxid
			INNER JOIN utb_user u
				ON u.UserID = c.CarrierRepUserID
		WHERE
			(ClientClaimNumber = @vClientClaimNumber OR ClientClaimNumberSquished = @ClientClaimNumberSquished)
			AND ca.ExposureCD = @iExposureCD
			AND ca.ClaimAspectNumber = @iClaimAspectNumber

		FOR XML EXPLICIT
	END
	ELSE
	BEGIN
		SELECT TOP 1
			1 AS Tag
			, NULL AS Parent
			, '' AS [Root!1!LynxID]
			, '' AS [Root!1!InsuranceCompanyID]
			, '' AS [Root!1!ClientClaimNumber]
			, '' AS [Root!1!CarrierRepUserID]
			, '' AS [Root!1!CarrierRepNameFirst]
			, '' AS [Root!1!CarrierRepNameLast]
			, '' AS [Root!1!CarrierRepPhoneDay]
			, '' AS [Root!1!CarrierRepEmailAddress]

		FOR XML EXPLICIT
	END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCheckIFExistsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCheckIFExistsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/