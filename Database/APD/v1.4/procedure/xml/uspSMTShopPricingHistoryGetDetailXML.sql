-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopPricingHistoryGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopPricingHistoryGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopPricingHistoryGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Given a DocumentID determine the ShopLocationID and AssignmentDate.  Retrieve the record from 
*               pricing_history for that ShopLocationID that has the greatest SysLastUpdatedDate that is less than 
*               the AssignmentDate.  If no pricing_history record exists for the ShopLocationID, return the record from 
*               the live pricing table for that ShopLocationID.  If that does not exist indicate that in a flag in the root 
*               of the returned XML.

*
* PARAMETERS:  
* (I) @DocumentID          The document id of the estimate for which static pricing data is needed
*
* RESULT SET:
* All shop_location_pricing Fields                  The Shop Location's identity
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopPricingHistoryGetDetailXML
(
	@DocumentID                    	udt_std_id_big = NULL,
	@ShopLocationID                 	udt_std_id_big = NULL
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              int
    DECLARE @rowcount           int
    --DECLARE @ShopLocationID     bigint
    DECLARE @AssignmentDate     datetime
    DECLARE @PricingAvailable   char(1)
    DECLARE @ShopName           udt_std_name
    DECLARE @ShopAddress        udt_std_desc_mid
    DECLARE @ShopCity           udt_addr_city
    DECLARE @ShopPhone          udt_std_desc_short
    
    DECLARE @ProcName           varchar(50)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTShopPricingHistoryGetDetailXML'
    
    IF @DocumentID IS NULL AND @ShopLocationID IS NULL
    BEGIN
         RAISERROR  ('1|Invalid DocumentID/ShopLocationID. Both cannot be NULL', 16, 1, @ProcName)
         RETURN
    END
    
    IF @DocumentID IS NOT NULL
    BEGIN
       SELECT @ShopLocationID = ShopLocationID,
              @AssignmentDate = AssignmentDate
	   /*********************************************************************************
	   Project: 210474 APD - Enhancements to support multiple concurrent service channels
	   Note:	Added reference to utb_Claim_Aspect_Service_Channel table below to link
               utb_Assignment and utb_Claim_Aspect_Document
		   M.A. 20061120
	   *********************************************************************************/
       FROM dbo.utb_assignment a INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
                                 INNER JOIN dbo.utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                 INNER JOIN dbo.utb_document d ON cascd.DocumentID = d.DocumentID
       WHERE d.DocumentID = @DocumentID AND ShopLocationID IS NOT NULL  AND a.assignmentdate is not null   AND a.CancellationDate IS NULL
    END
          
        
    DECLARE @tmpPricing TABLE  (ShopLocationID                     bigint     NOT NULL,
                                AlignmentFourWheel                 money          NULL,
                                AlignmentTwoWheel                  money          NULL,
                                CaulkingSeamSealer                 money          NULL,
                                ChipGuard                          money          NULL,
                                CoolantGreen                       money          NULL,
                                CoolantRed                         money          NULL,
                                CorrosionProtection                money          NULL,
                                CountyTaxPct                       decimal        NULL,
                                CoverCar                           money          NULL,
                                DailyStorage                       money          NULL,
                                DiscountPctSideBackGlass           decimal        NULL,
                                DiscountPctWindshield              decimal        NULL,
                                FlexAdditive                       money          NULL,
                                GlassOutsourceServiceFee           money          NULL,
                                GlassReplacementChargeTypeCD       varchar        NULL,
                                GlassSubletServiceFee              money          NULL,
                                GlassSubletServicePct              decimal        NULL,
                                HazardousWaste                     money          NULL,
                                HourlyRateMechanical               money          NULL,
                                HourlyRateRefinishing              money          NULL,
                                HourlyRateSheetMetal               money          NULL,
                                HourlyRateUnibodyFrame             money          NULL,
                                MunicipalTaxPct                    decimal        NULL,
                                OtherTaxPct                        decimal        NULL,
                                PartsRecycledMarkupPct             decimal        NULL,
                                PullSetUp                          decimal        NULL,
                                PullSetUpMeasure                   decimal        NULL,
                                RefinishSingleStageHourly          money          NULL,
                                RefinishSingleStageMax             money          NULL,
                                RefinishThreeStageCCMaxHrs         decimal        NULL,
                                RefinishThreeStageHourly           money          NULL,
                                RefinishThreeStageMax              money          NULL,
                                RefinishTwoStageCCMaxHrs           decimal        NULL,
                                RefinishTwoStageHourly             money          NULL,
                                RefinishTwoStageMax                money          NULL,
                                R12EvacuateRecharge                money          NULL,
                                R12EvacuateRechargeFreon           money          NULL,
                                R134EvacuateRecharge               money          NULL,
                                R134EvacuateRechargeFreon          money          NULL,
                                SalesTaxPct                        decimal        NULL,
                                StripePaintPerPanel                money          NULL,
                                StripePaintPerSide                 money          NULL,
                                StripeTapePerPanel                 money          NULL,
                                StripeTapePerSide                  money          NULL,
                                TireMountBalance                   money          NULL,
                                TowInChargeTypeCD                  varchar        NULL,
                                TowInFlatFee                       money          NULL,
                                Undercoat                          money          NULL,
                                SysLastUserID                      int        NOT NULL,
                                SysLastUpdatedDate                 datetime   NOT NULL)
    
    
    IF EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location_pricing_history WHERE ShopLocationID = @ShopLocationID
                                                                                 AND SysLastUpdatedDate < @AssignmentDate)
    BEGIN
      INSERT INTO @tmpPricing
      SELECT TOP 1 * 
      FROM dbo.utb_shop_location_pricing_history
      WHERE ShopLocationID = @ShopLocationID
        AND SysLastUpdatedDate < @AssignmentDate
      ORDER BY SysLastUpdatedDate desc    
    END
    ELSE
    BEGIN
      INSERT INTO @tmpPricing
      SELECT ShopLocationID, AlignmentFourWheel, AlignmentTwoWheel, 
             CaulkingSeamSealer, ChipGuard, CoolantGreen, CoolantRed, 
             CorrosionProtection, CountyTaxPct, CoverCar, DailyStorage, 
             DiscountPctSideBackGlass, DiscountPctWindshield, FlexAdditive, 
             GlassOutsourceServiceFee, GlassReplacementChargeTypeCD, 
             GlassSubletServiceFee, GlassSubletServicePct, HazardousWaste, 
             HourlyRateMechanical, HourlyRateRefinishing, HourlyRateSheetMetal, 
             HourlyRateUnibodyFrame, MunicipalTaxPct, OtherTaxPct, 
             PartsRecycledMarkupPct, PullSetUp, PullSetUpMeasure, 
             NULL, NULL, NULL, NULL, NULL, 
             RefinishTwoStageCCMaxHrs, RefinishTwoStageHourly, 
             RefinishTwoStageMax, R12EvacuateRecharge, R12EvacuateRechargeFreon, 
             R134EvacuateRecharge, R134EvacuateRechargeFreon, SalesTaxPct, 
             StripePaintPerPanel, StripePaintPerSide, StripeTapePerPanel, 
             StripeTapePerSide, TireMountBalance, TowInChargeTypeCD, 
             TowInFlatFee, Undercoat, SysLastUserID, SysLastUpdatedDate
      FROM dbo.utb_shop_location_pricing
      WHERE ShopLocationID = @ShopLocationID
    END     
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpPricing', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Flag to explicitly indicate whether or not pricing is available.
    SET @PricingAvailable = (SELECT COUNT(*) FROM @tmpPricing)                               
    
        
    -- Grab some info about the shop
    SELECT @ShopName = ISNULL(Name, ''),
           @ShopAddress = ISNULL(Address1, '') + ' ' + ISNULL(Address2, ''),
           @ShopCity = ISNULL(AddressCity, '') + ', ' + ISNULL(AddressState, '') + '  ' + ISNULL(AddressZip, ''),
           @ShopPhone = ISNULL(PhoneAreaCode, '') + '-' + ISNULL(PhoneExchangeNumber, '') + '-' + ISNULL(PhoneUnitNumber, '')
    FROM dbo.utb_shop_location 
    WHERE ShopLocationID = @ShopLocationID

    -- Begin Final Select

     -- Root Level
    SELECT 1                                        AS Tag,
           Null                                     AS Parent,
           -- Root
           @DocumentID                              AS [Root!1!DocumentID],
           @PricingAvailable                        AS [Root!1!PricingAvailable],
           @ShopLocationID                          AS [Root!1!ShopID],
                      
           --Pricing
           Null                                     AS [Pricing!2!ShopID],   
           Null                                     AS [Pricing!2!Name],
           Null                                     AS [Pricing!2!Address], 
           Null                                     AS [Pricing!2!City],
           Null                                     AS [Pricing!2!Phone],                                                                                             
           Null                                     AS [Pricing!2!AlignmentFourWheel],                                                   
           Null                                     AS [Pricing!2!AlignmentTwoWheel],                                                    
           Null                                     AS [Pricing!2!CaulkingSeamSealer],                                                   
           Null                                     AS [Pricing!2!ChipGuard],                                                            
           Null                                     AS [Pricing!2!CoolantGreen],                                                         
           Null                                     AS [Pricing!2!CoolantRed],                                                           
           Null                                     AS [Pricing!2!CorrosionProtection],                                                  
           Null                                     AS [Pricing!2!CountyTaxPct],                                                         
           Null                                     AS [Pricing!2!CoverCar],    
           Null                                     AS [Pricing!2!DailyStorage],                                                         
           Null                                     AS [Pricing!2!DiscountPctSideBackGlass],                                             
           Null                                     AS [Pricing!2!DiscountPctWindshield],                                                
           Null                                     AS [Pricing!2!FlexAdditive],    
           Null                                     AS [Pricing!2!GlassOutsourceServiceFee],                                          
           Null                                     AS [Pricing!2!GlassReplacementChargeTypeCD],                                         
           Null                                     AS [Pricing!2!GlassSubletServiceFee],                                                
           Null                                     AS [Pricing!2!GlassSubletServicePct],                                                
           Null                                     AS [Pricing!2!HazardousWaste],                                                       
           Null                                     AS [Pricing!2!HourlyRateMechanical],                                                 
           Null                                     AS [Pricing!2!HourlyRateRefinishing],                                                
           Null                                     AS [Pricing!2!HourlyRateSheetMetal],                                                 
           Null                                     AS [Pricing!2!HourlyRateUnibodyFrame],                                               
           Null                                     AS [Pricing!2!MunicipalTaxPct],                                                      
           Null                                     AS [Pricing!2!OtherTaxPct],                                                          
           Null                                     AS [Pricing!2!PartsRecycledMarkupPct],                                               
           Null                                     AS [Pricing!2!PullSetup],                                                            
           Null                                     AS [Pricing!2!PullSetupMeasure],                                                     
           Null                                     AS [Pricing!2!RefinishSingleStageHourly],                                            
           Null                                     AS [Pricing!2!RefinishSingleStageMax],                                               
           Null                                     AS [Pricing!2!RefinishThreeStageHourly],                                             
           Null                                     AS [Pricing!2!RefinishThreeStageMax],                                                
           Null                                     AS [Pricing!2!RefinishThreeStageCCMaxHrs],                                           
           Null                                     AS [Pricing!2!RefinishTwoStageHourly],                                               
           Null                                     AS [Pricing!2!RefinishTwoStageMax],                                                  
           Null                                     AS [Pricing!2!RefinishTwoStageCCMaxHrs],                                             
           Null                                     AS [Pricing!2!R12EvacuateRecharge],                                                  
           Null                                     AS [Pricing!2!R12EvacuateRechargeFreon],                                             
           Null                                     AS [Pricing!2!R134EvacuateRecharge],                                                 
           Null                                     AS [Pricing!2!R134EvacuateRechargeFreon],                                            
           Null                                     AS [Pricing!2!SalesTaxPct],                                                          
           Null                                     AS [Pricing!2!StripePaintPerPanel],                                                  
           Null                                     AS [Pricing!2!StripePaintPerSide],                                                   
           Null                                     AS [Pricing!2!StripeTapePerPanel],                                                   
           Null                                     AS [Pricing!2!StripeTapePerSide],                                                    
           Null                                     AS [Pricing!2!TireMountBalance],                                                     
           Null                                     AS [Pricing!2!TowInChargeTypeCD],                                                    
           Null                                     AS [Pricing!2!TowInFlatFee],                                                         
           Null                                     AS [Pricing!2!Undercoat],                                                            
           Null                                     AS [Pricing!2!SysLastUserID],                                                        
           Null                                     AS [Pricing!2!SysLastUpdatedDate]
               
    
    UNION ALL

    -- Pricing Level
    SELECT 2 AS Tag,
           1 AS Parent,
           --Root
           Null, NULL, NULL,
           --Pricing
           ShopLocationID,
           @ShopName,
           @ShopAddress,
           @ShopCity,
           @ShopPhone,
           convert(varchar(20), AlignmentFourWheel),       
           convert(varchar(20), AlignmentTwoWheel),         
           convert(varchar(20), CaulkingSeamSealer),          
           convert(varchar(20), ChipGuard),                   
           convert(varchar(20), CoolantGreen),                
           convert(varchar(20), CoolantRed),                  
           convert(varchar(20), CorrosionProtection), 
           convert(decimal(9, 1), CountyTaxPct),
           convert(varchar(20), CoverCar),
           convert(varchar(20), DailyStorage),
           convert(decimal(9, 1), DiscountPctSideBackGlass),    
           convert(decimal(9, 1), DiscountPctWindshield),      
           convert(varchar(20), FlexAdditive), 
           convert(varchar(20), GlassOutsourceServiceFee),    
           ISNULL((SELECT Name 
                   FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location_pricing', 'GlassReplacementChargeTypeCD')
                   WHERE Code = GlassReplacementChargeTypeCD), ''),
           convert(varchar(20), GlassSubletServiceFee),       
           convert(decimal(9, 1), GlassSubletServicePct),       
           convert(varchar(20), HazardousWaste),             
           convert(varchar(20), HourlyRateMechanical),
           convert(varchar(20), HourlyRateRefinishing),       
           convert(varchar(20), HourlyRateSheetMetal),        
           convert(varchar(20), HourlyRateUnibodyFrame),  
           convert(decimal(9, 1), MunicipalTaxPct),
           convert(decimal(9, 1), OtherTaxPct),
           convert(decimal(9, 1), PartsRecycledMarkupPct),      
           convert(varchar(20), PullSetUp),                
           convert(varchar(20), PullSetUpMeasure),     
           convert(varchar(20), RefinishSingleStageHourly),   
           convert(varchar(20), RefinishSingleStageMax),      
           convert(varchar(20), RefinishThreeStageHourly),    
           convert(varchar(20), RefinishThreeStageMax),
           convert(varchar(20), RefinishThreeStageCCMaxHrs),
           convert(varchar(20), RefinishTwoStageHourly),      
           convert(varchar(20), RefinishTwoStageMax), 
           convert(varchar(20), RefinishTwoStageCCMaxHrs),
           convert(varchar(20), R12EvacuateRecharge),         
           convert(varchar(20), R12EvacuateRechargeFreon),    
           convert(varchar(20), R134EvacuateRecharge),        
           convert(varchar(20), R134EvacuateRechargeFreon),  
           convert(decimal(9, 3), SalesTaxPct), 
           convert(varchar(20), StripePaintPerPanel),         
           convert(varchar(20), StripePaintPerSide),          
           convert(varchar(20), StripeTapePerPanel),          
           convert(varchar(20), StripeTapePerSide),           
           convert(varchar(20), TireMountBalance),            
           ISNULL((SELECT Name 
                   FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location_pricing', 'TowInChargeTypeCD')
                   WHERE Code = TowInChargeTypeCD), ''),
           convert(varchar(20), TowInFlatFee),  
           convert(varchar(20), Undercoat),                   
           SysLastUserID,
           dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
          

       FROM @tmpPricing t
            
    
        
    ORDER BY Tag 
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)


    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopPricingHistoryGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopPricingHistoryGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/