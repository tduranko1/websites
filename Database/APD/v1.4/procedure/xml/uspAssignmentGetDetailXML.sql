-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAssignmentGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAssignmentGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This procedure will return assignment information for use in estimate audit
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAssignmentGetDetailXML
(
    @AssignmentID   as bigint
)
AS
BEGIN
    DECLARE @ShopLocationID     as int
    DECLARE @InsuranceCompanyID as int
    DECLARE @LossState          as varchar(2)
    DECLARE @ClaimAspectID      as int
    DECLARE @LastEstRepairTotal as money
    DECLARE @RepairTotalID      as udt_std_id

    DECLARE @ProcName           as varchar(50)

    SET @ProcName = 'uspAssignmentGetDetailXML'
    
    IF NOT EXISTS(SELECT AssignmentID
                    FROM dbo.utb_assignment
                    WHERE AssignmentID = @AssignmentID)
    BEGIN
       -- Invalid Assignment ID

        RAISERROR('102|%s|"AssignmentID"|Invalid Assignment ID', 16, 1, @ProcName)
        RETURN
    END

    -- Get the id for the repair total from the estimate summary type table
    SELECT @RepairTotalID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @RepairTotalID IS NULL
    BEGIN
       -- Repair Total ID not found

        RAISERROR('102|%s|"RepairTotalID"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

/*
Changed the code below to get the ClaimAspectID from
utb_claim_aspect_service_channel
M.A. 2006-11-06

Note:
There is no R.I. constraint between the two tables.  Ask Jim about the RI/FK.
Based on the information, change the relationship to either an OUTER or an
INNER JOIN between utb_assignment and utb_claim_aspect_service_channel
*/

    SELECT @ShopLocationID = a.ShopLocationID,
           @ClaimAspectID = casc.ClaimAspectID
    FROM dbo.utb_assignment a
    INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelId = casc.ClaimAspectServiceChannelId
    WHERE a.AssignmentID = @AssignmentID


    -- The assignment must be a shop assignment
    IF @ShopLocationID IS NULL
    BEGIN
--        RAISERROR('1|No Shop associated with AssignmentID %s ', 16, 1, @ShopLocationID)
        RAISERROR('1|No Shop associated with Assignment', 16, 1)
        RETURN
    END
    

/*
Changed the code below to get the ClaimAspectID from
utb_claim_aspect_service_channel
M.A. 2006-11-06

Note:
There is no R.I. constraint between the two tables.  Ask Jim about the RI/FK.
Based on the information, change the relationship to either an OUTER or an
INNER JOIN between utb_assignment and utb_claim_aspect_service_channel
*/

    SELECT @InsuranceCompanyID = c.InsuranceCompanyID,
           @LossState = c.LossState
    FROM dbo.utb_assignment a
    INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelId = casc.ClaimAspectServiceChannelId
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    WHERE a.AssignmentID = @AssignmentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get the last electronic estimate repair total
    SELECT TOP 1 @LastEstRepairTotal = es.OriginalExtendedAmt
    --FROM dbo.utb_claim_aspect_document cad --Project:210474 APD Remarked-off to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
    --Project:210474 APD Added the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
    FROM utb_Claim_Aspect_Service_Channel_Document cascd
    inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    --Project:210474 APD Added the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
    LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
    LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
    LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
    LEFT JOIN dbo.utb_estimate_summary es ON (cascd.DocumentID = es.DocumentID)
    WHERE es.EstimateSummaryTypeID = @RepairTotalID
      AND casc.claimAspectID = @ClaimAspectID
      AND casc.Enabledflag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
      AND dt.EstimateTypeFlag = 1
      AND ds.VANFlag = 1
      AND d.EnabledFlag = 1
    ORDER BY d.CreatedDate DESC

    -- Begin final select 
    SELECT
        1                      AS Tag,
        NULL                   AS Parent,
        --Root
        @InsuranceCompanyID    AS [Root!1!InsuranceCompanyID],
        @LossState             AS [Root!1!LossState],
        --ShopInfo
        NULL                   AS [ShopInfo!2!ShopID],
        NULL                   AS [ShopInfo!2!BillingID],
        NULL                   AS [ShopInfo!2!DataReviewUserID],
        NULL                   AS [ShopInfo!2!PreferredCommunicationMethodID],
        NULL                   AS [ShopInfo!2!PreferredEstimatePackageID],
        NULL                   AS [ShopInfo!2!ProgramManagerUserID],
        NULL                   AS [ShopInfo!2!BusinessInfoID],
        NULL                   AS [ShopInfo!2!TerritoryID],
        NULL                   AS [ShopInfo!2!Territory],  
        NULL                   AS [ShopInfo!2!TerritoryManagerUserID],
        NULL                   AS [ShopInfo!2!TerritoryManager],                                  
        NULL                   AS [ShopInfo!2!EIN],
        NULL                   AS [ShopInfo!2!Address1],
        NULL                   AS [ShopInfo!2!Address2],
        NULL                   AS [ShopInfo!2!AddressCity],
        NULL                   AS [ShopInfo!2!AddressCounty],
        NULL                   AS [ShopInfo!2!AddressState],
        NULL                   AS [ShopInfo!2!AddressZip],
        NULL                   AS [ShopInfo!2!AvailableForSelectionFlag],
        NULL                   AS [ShopInfo!2!CEIProgramFlag],
        NULL                   AS [ShopInfo!2!CEIProgramID],
        NULL                   AS [ShopInfo!2!CertifiedFirstFlag],
        NULL                   AS [ShopInfo!2!CertifiedFirstID],
        NULL                   AS [ShopInfo!2!DataReviewDate],
        NULL                   AS [ShopInfo!2!DataReviewStatusCD],
        NULL                   AS [ShopInfo!2!DrivingDirections],
        NULL                   AS [ShopInfo!2!EmailAddress],
        NULL                   AS [ShopInfo!2!FaxAreaCode],
        NULL                   AS [ShopInfo!2!FaxExchangeNumber],
        NULL                   AS [ShopInfo!2!FaxExtensionNumber],
        NULL                   AS [ShopInfo!2!FaxUnitNumber],
        NULL                   AS [ShopInfo!2!LastAssignmentDate],
        NULL                   AS [ShopInfo!2!LastReIDate],
        NULL                   AS [ShopInfo!2!Latitude],
        NULL                   AS [ShopInfo!2!Longitude],
        NULL                   AS [ShopInfo!2!MaxWeeklyAssignments],
        NULL                   AS [ShopInfo!2!Name],
        NULL                   AS [ShopInfo!2!PhoneAreaCode],
        NULL                   AS [ShopInfo!2!PhoneExchangeNumber],
        NULL                   AS [ShopInfo!2!PhoneExtensionNumber],
        NULL                   AS [ShopInfo!2!PhoneUnitNumber],
        NULL                   AS [ShopInfo!2!PPGCTSCustomerFlag],
        NULL                   AS [ShopInfo!2!PPGCTSFlag],
        NULL                   AS [ShopInfo!2!PPGCTSId],
        NULL                   AS [ShopInfo!2!PPGCTSLevelCD],
        NULL                   AS [ShopInfo!2!PreferredCommunicationAddress],
        NULL                   AS [ShopInfo!2!ProgramFlag],
        NULL                   AS [ShopInfo!2!ProgramScore],
        NULL                   AS [ShopInfo!2!WebSiteAddress],
        NULL                   AS [ShopInfo!2!WarrantyPeriodRefinishCD],
        NULL                   AS [ShopInfo!2!WarrantyPeriodWorkmanshipCD],

        --Shop Hours
        NULL                   AS [ShopHours!3!OperatingMondayStartTime],
        NULL                   AS [ShopHours!3!OperatingMondayEndTime],
        NULL                   AS [ShopHours!3!OperatingTuesdayStartTime],
        NULL                   AS [ShopHours!3!OperatingTuesdayEndTime],
        NULL                   AS [ShopHours!3!OperatingWednesdayStartTime],
        NULL                   AS [ShopHours!3!OperatingWednesdayEndTime],
        NULL                   AS [ShopHours!3!OperatingThursdayStartTime],
        NULL                   AS [ShopHours!3!OperatingThursdayEndTime],
        NULL                   AS [ShopHours!3!OperatingFridayStartTime],
        NULL                   AS [ShopHours!3!OperatingFridayEndTime],
        NULL                   AS [ShopHours!3!OperatingSaturdayStartTime],
        NULL                   AS [ShopHours!3!OperatingSaturdayEndTime],
        NULL                   AS [ShopHours!3!OperatingSundayStartTime],
        NULL                   AS [ShopHours!3!OperatingSundayEndTime],

        --Shop Specialties
        NULL                   AS [ShopSpecialty!4!SpecialtyID],
        NULL                   AS [ShopSpecialty!4!Name],
    
        --Shop OEM Discounts
        NULL                   AS [ShopOEMDiscount!5!VehicleMake],
        NULL                   AS [ShopOEMDiscount!5!DomesticFlag],
        NULL                   AS [ShopOEMDiscount!5!DiscountPct],
    
        --Shop Pricing
        NULL                   AS [ShopPricing!6!AlignmentFourWheel],
        NULL                   AS [ShopPricing!6!AlignmentTwoWheel],
        NULL                   AS [ShopPricing!6!CaulkingSeamSealer],
        NULL                   AS [ShopPricing!6!ChipGuard],
        NULL                   AS [ShopPricing!6!CoolantGreen],
        NULL                   AS [ShopPricing!6!CoolantRed],
        NULL                   AS [ShopPricing!6!CorrosionProtection],
        NULL                   AS [ShopPricing!6!CountyTaxPct],
        NULL                   AS [ShopPricing!6!CoverCar],
        NULL                   AS [ShopPricing!6!DailyStorage],
        NULL                   AS [ShopPricing!6!DiscountPctSideBackGlass],
        NULL                   AS [ShopPricing!6!DiscountPctWindshield],
        NULL                   AS [ShopPricing!6!FlexAdditive],
        NULL                   AS [ShopPricing!6!GlassOutsourceServiceFee],
        NULL                   AS [ShopPricing!6!GlassReplacementChargeTypeCD],
        NULL                   AS [ShopPricing!6!GlassSubletServiceFee],
        NULL                   AS [ShopPricing!6!GlassSubletServicePct],
        NULL                   AS [ShopPricing!6!HazardousWaste],
        NULL                   AS [ShopPricing!6!HourlyRateMechanical],
        NULL                   AS [ShopPricing!6!HourlyRateRefinishing],
        NULL                   AS [ShopPricing!6!HourlyRateSheetMetal],
        NULL                   AS [ShopPricing!6!HourlyRateUnibodyFrame],
        NULL                   AS [ShopPricing!6!MunicipalTaxPct],
        NULL                   AS [ShopPricing!6!OtherTaxPct],
        NULL                   AS [ShopPricing!6!PartsRecycledMarkupPct],
        NULL                   AS [ShopPricing!6!PullSetUp],
        NULL                   AS [ShopPricing!6!PullSetUpMeasure],
        NULL                   AS [ShopPricing!6!RefinishTwoStageCCMaxHrs],
        NULL                   AS [ShopPricing!6!RefinishTwoStageHourly],
        NULL                   AS [ShopPricing!6!RefinishTwoStageMax],
        NULL                   AS [ShopPricing!6!R12EvacuateRecharge],
        NULL                   AS [ShopPricing!6!R12EvacuateRechargeFreon],
        NULL                   AS [ShopPricing!6!R134EvacuateRecharge],
        NULL                   AS [ShopPricing!6!R134EvacuateRechargeFreon],
        NULL                   AS [ShopPricing!6!SalesTaxPct],
        NULL                   AS [ShopPricing!6!StripePaintPerPanel],
        NULL                   AS [ShopPricing!6!StripePaintPerSide],
        NULL                   AS [ShopPricing!6!StripeTapePerPanel],
        NULL                   AS [ShopPricing!6!StripeTapePerSide],
        NULL                   AS [ShopPricing!6!TireMountBalance],
        NULL                   AS [ShopPricing!6!TowInChargeTypeCD],
        NULL                   AS [ShopPricing!6!TowInFlatFee],
        NULL                   AS [ShopPricing!6!Undercoat],
    
        -- Vehicle Data
        NULL                   AS [Vehicle!7!TotalLossWarningPercentage],
        NULL                   AS [Vehicle!7!BookValue],
        NULL                   AS [Vehicle!7!VIN],
        NULL                   AS [Vehicle!7!VehicleYear],
        NULL                   AS [Vehicle!7!Make],
        NULL                   AS [Vehicle!7!Model],
        NULL                   AS [Vehicle!7!LastRepairTotal],
    
        -- Industry data
        NULL                   AS [Industry!8!StateCode],
        NULL                   AS [Industry!8!MSACode],
        NULL                   AS [Industry!8!BodyLaborRate],
        NULL                   AS [Industry!8!FrameLaborRate],
        NULL                   AS [Industry!8!MechanicalLaborRate],
        NULL                   AS [Industry!8!RefinishLaborRate],
        NULL                   AS [Industry!8!SalesTaxRate],
        NULL                   AS [Industry!8!EstimateCount],
    
        -- Reference Data
        NULL                   AS [Reference!9!ListName],
        NULL                   AS [Reference!9!VehicleID],
        NULL                   AS [Reference!9!Name],
        NULL                   AS [Reference!9!DomesticFlag]

    UNION ALL

    --ShopInfo Level
    SELECT
        2,
        1,
        --Root
        NULL, NULL,
    
        --ShopInfo
        ISNULL(S.ShopLocationID, ''),
        ISNULL(S.BillingID, ''),
        ISNULL(S.DataReviewUserID, ''),
        ISNULL(S.PreferredCommunicationMethodID, ''),
        ISNULL(S.PreferredEstimatePackageID, ''),
        ISNULL(S.ProgramManagerUserID, ''),
        ISNULL(S.ShopID, ''),
        ISNULL(t.TerritoryID, ''),
        ISNULL(t.Name, '') Territory,
        ISNULL(t.ProgramManagerUserID, '') TerritoryManagerUserID,
        ISNULL(u.NameFirst, '') + ' ' + ISNULL(u.NameLast, '') TerritoryManager,
        ISNULL((Select FedTaxID From dbo.utb_shop Where ShopID = S.ShopID), ''),
        ISNULL(S.Address1, ''),
        ISNULL(S.Address2, ''),
        ISNULL(S.AddressCity, ''),
        ISNULL(S.AddressCounty, ''),
        ISNULL(S.AddressState, ''),
        ISNULL(S.AddressZip, ''),
        ISNULL(S.AvailableForSelectionFlag,''),
        ISNULL(S.CEIProgramFlag, 0),
        ISNULL(S.CEIProgramID, ''),
        ISNULL(S.CertifiedFirstFlag, ''),
        ISNULL(S.CertifiedFirstId, ''),
        dbo.ufnUtilityGetDateString(DataReviewDate),
        ISNULL(S.DataReviewStatusCD, ''),
        ISNULL(S.DrivingDirections, ''),
        ISNULL(S.EmailAddress, ''),
        S.FaxAreaCode,
        S.FaxExchangeNumber,
        S.FaxExtensionNumber,
        S.FaxUnitNumber,
        ISNULL(dbo.ufnUtilityGetDateString(s.LastAssignmentDate), ''),
        '',
        ISNULL(convert(varchar(20), S.Latitude), ''),
        ISNULL(convert(varchar(20), S.Longitude), ''),
        ISNULL(S.MaxWeeklyAssignments, ''),
        S.Name,
        S.PhoneAreaCode,
        S.PhoneExchangeNumber,
        S.PhoneExtensionNumber,
        S.PhoneUnitNumber,
        ISNULL(S.PPGCTSCustomerFlag, '0'),
        ISNULL(S.PPGCTSFlag, '0'),
        ISNULL(S.PPGCTSId, ''),
        ISNULL(S.PPGCTSLevelCD, ''),
        ISNULL(S.PreferredCommunicationAddress, ''),
        S.ProgramFlag,
        ISNULL(S.ProgramScore, ''),
        ISNULL(S.WebSiteAddress, ''),
        ISNULL(WarrantyPeriodRefinishCD, ''),
        ISNULL(WarrantyPeriodWorkmanshipCD, ''),
    
        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  (Select @ShopLocationID as ShopLocationID, 1 as EnabledFlag) as parms 
    LEFT JOIN utb_shop_location S ON (parms.ShopLocationID = S.ShopLocationID and parms.EnabledFlag = S.EnabledFlag)
    LEFT JOIN utb_state_code sc ON s.AddressState = sc.StateCode
    LEFT JOIN utb_territory t ON sc.TerritoryID = t.TerritoryID
    LEFT JOIN utb_user u ON t.ProgramManagerUserID = u.UserID

    UNION ALL

    SELECT
        3,
        2,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        isNull(slh.OperatingFridayEndTime, ''),
        isNull(slh.OperatingFridayStartTime, ''),
        isNull(slh.OperatingMondayEndTime, ''),
        isNull(slh.OperatingMondayStartTime, ''),
        isNull(slh.OperatingSaturdayEndTime, ''),
        isNull(slh.OperatingSaturdayStartTime, ''),
        isNull(slh.OperatingSundayEndTime, ''),
        isNull(slh.OperatingSundayStartTime, ''),
        isNull(slh.OperatingThursdayEndTime, ''),
        isNull(slh.OperatingThursdayStartTime, ''),
        isNull(slh.OperatingTuesdayEndTime, ''),
        isNull(slh.OperatingTuesdayStartTime, ''),
        isNull(slh.OperatingWednesdayEndTime, ''),
        isNull(slh.OperatingWednesdayStartTime, ''),

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  (Select @ShopLocationID as ShopLocationID, 1 as EnabledFlag) as parms 
    LEFT JOIN utb_shop_location_hours slh ON (parms.ShopLocationID = slh.ShopLocationID)

    UNION ALL

    SELECT
        4,
        2,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        ISNULL(S.SpecialtyID, ''), 
        ISNULL(SP.Name, ''), 

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  (Select @ShopLocationID as ShopLocationID, 1 as EnabledFlag) as parms 
    LEFT JOIN dbo.utb_shop_location_specialty S ON parms.ShopLocationID = S.ShopLocationID
    INNER JOIN dbo.utb_specialty SP ON (S.SpecialtyID = SP.SpecialtyID AND parms.EnabledFlag = SP.EnabledFlag)

    UNION ALL

    SELECT
        5,
        2,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        UPPER(LTrim(RTrim(isNull(vm.Name, '')))), 
        isNull(vm.DomesticFlag, 0),
        isNull( (SELECT DiscountPct
        FROM dbo.utb_shop_location_oem_discount sod
        WHERE sod.VehicleMakeID = vm.VehicleMakeID
          AND sod.ShopLocationID = @ShopLocationID), 0),
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  dbo.utb_vehicle_make vm 

    UNION ALL

    SELECT
        6,
        2,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        isNull(AlignmentFourWheel, 0),
        isNull(AlignmentTwoWheel, 0),
        isNull(CaulkingSeamSealer, 0),
        isNull(ChipGuard, 0),
        isNull(CoolantGreen, 0),
        isNull(CoolantRed, 0),
        isNull(CorrosionProtection, 0),
        isNull(CountyTaxPct, 0),
        isNull(CoverCar, 0),
        isNull(DailyStorage, 0),
        isNull(DiscountPctSideBackGlass, 0),
        isNull(DiscountPctWindshield, 0),
        isNull(FlexAdditive, 0),
        isNull(GlassOutsourceServiceFee, 0),
        isNull(GlassReplacementChargeTypeCD, 0),
        isNull(GlassSubletServiceFee, 0),
        isNull(GlassSubletServicePct, 0),
        isNull(HazardousWaste, 0),
        isNull(HourlyRateMechanical, 0),
        isNull(HourlyRateRefinishing, 0),
        isNull(HourlyRateSheetMetal, 0),
        isNull(HourlyRateUnibodyFrame, 0),
        isNull(MunicipalTaxPct, 0),
        isNull(OtherTaxPct, 0),
        isNull(PartsRecycledMarkupPct, 0),
        isNull(PullSetUp, 0),
        isNull(PullSetUpMeasure, 0),
        isNull(RefinishTwoStageCCMaxHrs, 0),
        isNull(RefinishTwoStageHourly, 0),
        isNull(RefinishTwoStageMax, 0),
        isNull(R12EvacuateRecharge, 0),
        isNull(R12EvacuateRechargeFreon, 0),
        isNull(R134EvacuateRecharge, 0),
        isNull(R134EvacuateRechargeFreon, 0),
        isNull(SalesTaxPct, 0),
        isNull(StripePaintPerPanel, 0),
        isNull(StripePaintPerSide, 0),
        isNull(StripeTapePerPanel, 0),
        isNull(StripeTapePerSide, 0),
        isNull(TireMountBalance, 0),
        isNull(TowInChargeTypeCD, 0),
        isNull(TowInFlatFee, 0),
        isNull(Undercoat, 0),

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  (Select @ShopLocationID as ShopLocationID, 1 as EnabledFlag) as parms 
    LEFT JOIN dbo.utb_shop_location_pricing slp ON (parms.ShopLocationID = slp.ShopLocationID)

    UNION ALL

    SELECT
        7,
        1,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        i.TotalLossWarningPercentage, 
        cv.BookValueAmt, 
        cv.VIN, 
        cv.VehicleYear, 
        cv.Make, 
        cv.Model, 
        IsNull(@LastEstRepairTotal, 0),
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    FROM  (Select @ClaimAspectID as ClaimAspectID, 1 as EnabledFlag) as parms 
    LEFT JOIN dbo.utb_claim_aspect ca ON (parms.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)

    UNION ALL

    SELECT
        8,
        1,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        NULL, NULL, NULL, NULL

    UNION ALL

    SELECT
        9,
        1,
        --Root
        NULL, NULL,
    
        --ShopInfo
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,

        --Shop Hours
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,

        --Shop Specialties
        NULL, NULL,

        --Shop OEM Discounts
        NULL, NULL, NULL,
    
        --Shop Pricing
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,

        -- Vehicle Data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    
        -- Industry data
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

        -- Reference Data
        'VehicleMake', 
        VehicleMakeID, 
        LTrim(RTrim(Name)), 
        DomesticFlag

    FROM  dbo.utb_vehicle_make
    WHERE EnabledFlag = 1

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAssignmentGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
