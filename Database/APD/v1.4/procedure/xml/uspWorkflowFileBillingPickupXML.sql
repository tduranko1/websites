  -- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspWorkflowFileBillingPickupXML' AND type = 'P') 
BEGIN
    DROP PROCEDURE dbo.uspWorkflowFileBillingPickupXML
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowFileBillingPickupXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves billing items for LynxID entered
*
* PARAMETERS:  
* (I) @ClaimAspectID        LynxID to pick up billing for 
* (I) @ToDate               The date up to which include billing records for
*
* RESULT SET:   XML document detailing all information needed to generate an invoice
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspWorkflowFileBillingPickupXML
    @InsuranceCompanyID     udt_std_id,
    @ToDate                 varchar(30),
    @UserID                 udt_std_id
AS
BEGIN
    -- This procedure finds all the records that haven't been picked up yetfor @LynxID and returns them in an XML stream.
    -- Declare internal variables

    DECLARE @ClaimAspectTypeIDClaim         udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       udt_std_id
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @VehicleNumber                  udt_std_int
    DECLARE @InvoiceSeqNumber               udt_std_int_tiny
    DECLARE @TotalFeeAmount                 udt_std_money
    DECLARE @TotalInvoiceAmount             udt_std_money
    DECLARE @DefaultLynxShopID              udt_std_id_big
    DECLARE @DefaultCEIShopID               udt_std_id_big    
    DECLARE @UseCEIShopsFlag                udt_std_flag
    DECLARE @IngresAccountingID             udt_std_int
    DECLARE @NTUserID                       varchar(10)

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 
    DECLARE @now                            udt_std_datetime


    SET NOCOUNT ON
    
    SET @ProcName = 'uspWorkflowBillingPickUp'


    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
    
    
    -- Validate UserID
    IF ((SELECT dbo.ufnUtilityIsUserActive(@UserID, 'APD', NULL)) = 0)
    BEGIN
        -- Invalid User ID
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate @InsuranceCompanyID    
    IF (@InsuranceCompanyID IS NULL) OR (NOT EXISTS (SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName,@InsuranceCompanyID)
        RETURN
    END
    
    -- Validate and prepare @ToDate
    IF @ToDate IS NOT NULL
    BEGIN
        IF IsDate(@ToDate) = 0
        BEGIN
            -- Invalid ToDate
            RAISERROR('101|%s|@ToDate|%s', 16, 1, @ProcName, @ToDate)
            RETURN
        END
        ELSE
        BEGIN
            -- Ignore any time from the date passed in and set to midnight
            SET @ToDate = Convert(varchar(30), Convert(datetime, @ToDate), 101) + ' 11:59:59 PM'
        END
    END
    ELSE
    BEGIN
        -- @ToDate wasn't passed in
        RAISERROR('101|%s|@ToDate|%s', 16, 1, @ProcName, @ToDate)
        RETURN
    END
        
    
    -- Retrieve the default Shop ID to be used if a PayeeID is 0.
    SELECT @DefaultLynxShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_Lynx_ShopLocationID'
    
    
    -- Retrieve the defalt Shop ID to be used if a Payee is a CEI shop and the carrier does not use CEI shops.
    SELECT @DefaultCEIShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_CEI_ShopLocationID'
    
    
    DECLARE @tmpItemType TABLE (Code      varchar(4)      NOT NULL,
                                Name      varchar(50)     NOT NULL)
                          
    INSERT INTO @tmpItemType
    SELECT Code, Name FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD')
    
    
    -- These are the individual items from the APD database.
    DECLARE @tmpInvoice TABLE (InvoiceID                    bigint          NOT NULL,
                               AuthorizingClientUserID      varchar(50)         NULL,
                               ClaimAspectID                bigint          NOT NULL,
                               LynxID                       bigint          NOT NULL,
                               Amount                       money           NOT NULL,
                               ClientFeeCode                varchar(50)         NULL,
                               DispatchNumber               varchar(50)         NULL,
                               EntryDate                    datetime        NOT NULL,
                               FeeCategoryCD                varchar(4)          NULL,
                               InvoiceDescription           varchar(250)        NULL,
                               ItemType                     varchar(50)     NOT NULL,
                               PayeeID                      bigint              NULL,
                               PayeeName                    varchar(50)         NULL,
                               PayeeCity                    varchar(30)         NULL,
                               PayeeState                   char(2)             NULL)                             
                                                       
    
    
    -- Populate tmpInvoice.  
    INSERT INTO @tmpInvoice
    SELECT i.InvoiceID, 
           u.ClientUserID,
           i.ClaimAspectID,
           ca.LynxID,
           i.Amount, 
           i.ClientFeeCode,
           i.DispatchNumber,
           i.EntryDate,
           i.FeeCategoryCD,
           CASE 
              WHEN i.ItemTypeCD = 'F' THEN i.InvoiceDescription
              ELSE i.Description
           END InvoiceDescription,
           t.Name,
           CASE
              WHEN i.ItemTypeCD = 'F' THEN @DefaultLynxShopID
              ELSE i.PayeeID
           END PayeeID,
           i.PayeeName,
           i.PayeeAddressCity,
           i.PayeeAddressState
           
    FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                           INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                           INNER JOIN dbo.utb_insurance ins ON c.InsuranceCompanyID = ins.InsuranceCompanyID
                           LEFT JOIN dbo.utb_user u ON i.AuthorizingUserID = u.UserID
                           INNER JOIN @tmpItemType t ON i.ItemTypeCD = t.Code
                           INNER JOIN dbo.utb_claim_aspect_service_channel casc on i.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                           INNER JOIN dbo.utb_client_service_channel csc ON csc.InsuranceCompanyID = ins.InsuranceCompanyID
                                                                         AND csc.ServiceChannelCD = casc.ServiceChannelCD
--                           INNER JOIN dbo.utb_client_assignment_type cat ON ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
--                                                                        AND c.InsuranceCompanyID = cat.InsuranceCompanyID
    WHERE c.InsuranceCompanyID = @InsuranceCompanyID
      AND ins.InvoiceMethodCD = 'F'
      AND i.EnabledFlag = 1
      AND i.ItemizeFlag = 1
      AND i.EntryDate < @ToDate
      AND ((csc.ClientAuthorizesPaymentFlag = 0 and i.StatusCD = 'APD') 
            OR (csc.ClientAuthorizesPaymentFlag = 1 and i.StatusCD = 'AUTH'))
    ORDER BY ca.ClaimAspectID, PayeeID
        
        
    -- Get the ShopLocationID associated with the most recent shop assignment    
    DECLARE @tmpLastShopLocationIDs TABLE (ClaimAspectID    bigint    NOT NULL,
                                           AssignmentID     bigint        NULL,
                                           ShopLocationID   bigint        NULL)
    
    INSERT INTO @tmpLastShopLocationIDs
    SELECT casc.ClaimAspectID, MAX(AssignmentID), NULL
    FROM dbo.utb_assignment a 
								/*********************************************************************************
								Project: 210474 APD - Enhancements to support multiple concurrent service channels
								Note:	Added utb_Claim_Aspect_Service_Channel to join utb_CLaim_Aspect to
										utb_Invoice table
									M.A. 20061124
								*********************************************************************************/
							  INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
							  INNER JOIN dbo.utb_invoice i ON casc.ClaimAspectID = i.ClaimAspectID
                                                          AND a.ShopLocationID = i.PayeeID
                              INNER JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
                                                                 
    WHERE casc.ClaimAspectID IN (SELECT DISTINCT ClaimAspectID FROM @tmpInvoice)
      AND i.EnabledFlag = 1
    GROUP BY casc.ClaimAspectID
    
    
    -- Determine the Final PayeeID to be sent to Ingres.   	
    UPDATE @tmpInvoice
    SET PayeeID = (SELECT CASE
                            WHEN t.PayeeID <> 0 AND ProgramTypeCD = 'CEI' THEN @DefaultCEIShopID
                            WHEN t.PayeeID = 0 THEN @DefaultLynxShopID
                            ELSE t.PayeeID
                          END)
    FROM @tmpInvoice t INNER JOIN dbo.utb_assignment a ON t.PayeeID = a.ShopLocationID
    WHERE a.AssignmentID = (SELECT MAX(AssignmentID) 
                            FROM dbo.utb_assignment 
                            WHERE ClaimAspectID = t.ClaimAspectID
                              AND ShopLocationID = t.PayeeID)
                              
                              
    
    UPDATE @tmpLastShopLocationIDs
    SET ShopLocationID = a.ShopLocationID
    FROM @tmpLastShopLocationIDs t LEFT JOIN dbo.utb_assignment a ON t.AssignmentID = a.AssignmentID
    

    
    
     -- Get Claim Aspect Type for use later
    SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

          
    -- Retrieve the Ingres Accounting ID for this insurance company.  This is required when sending items to Ingres.
    SELECT @IngresAccountingID = IngresAccountingID FROM dbo.utb_insurance i WHERE InsuranceCompanyID = @InsuranceCompanyID
      
      
    SET @NTUserID = 'lynxdba'   -- standard user to be passed to Ingres for batch jobs per Bruce Miller's email dated 10/20/2004
    
    -- select * from @tmpInvoice
    -- return

    -- Begin XML Select        
    SELECT  1 AS Tag,
            NULL AS Parent,        
            
            -- Root    
            @InsuranceCompanyID   AS [Root!1!InsuranceCompanyID],
            @ToDate               AS [Root!1!ToDate],
            @UserID               AS [Root!1!UserID],
            @NTUserID             AS [Root!1!NTUserID],
            @IngresAccountingID   AS [Root!1!IngresAccountingID],
            
            
            -- Claim Information level
            NULL AS [Claim!2!LynxID],
            NULL AS [Claim!2!CarrierClaimNumber],
            NULL AS [Claim!2!CarrierRepClientUserID],
            NULL AS [Claim!2!CarrierRepNameFirst],
            NULL AS [Claim!2!CarrierRepNameLast],
            NULL AS [Claim!2!InsuredNameFirst],
            NULL AS [Claim!2!InsuredNameLast],
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!IntakeFinishDate],
            NULL AS [Claim!2!LossDate],
            NULL AS [Claim!2!LossState],
            NULL AS [Claim!2!PolicyNumber],
            
            -- Exposure Information Level
            NULL AS [Exposure!3!ClaimAspectID],
            NULL AS [Exposure!3!ClaimantBusinessName],
            NULL AS [Exposure!3!ClaimantNameFirst],
            NULL AS [Exposure!3!ClaimantNameLast],
            NULL AS [Exposure!3!CoverageType],
            NULL AS [Exposure!3!SourceApplicationPassThruData],
            NULL AS [Exposure!3!Vehicle],
            NULL AS [Exposure!3!VehicleNumber],
            NULL AS [Exposure!3!LastShopLocationID],
            
            -- Invoice Level
            NULL AS [Invoice!4!InvoiceID],
            NULL AS [Invoice!4!AuthorizingClientUserID],
            NULL AS [Invoice!4!Amount],     
            NULL AS [Invoice!4!ClientFeeCode],           
            NULL AS [Invoice!4!DispatchNumber],
            NULL AS [Invoice!4!EntryDate],
            NULL AS [Invoice!4!FeeCategoryCD],
            NULL AS [Invoice!4!InvoiceDate],
            NULL AS [Invoice!4!InvoiceDescription],
            NULL AS [Invoice!4!ItemType],
            NULL AS [Invoice!4!PayeeID],
            NULL AS [Invoice!4!PayeeName],
            NULL AS [Invoice!4!PayeeCity],
            NULL AS [Invoice!4!PayeeState]

    UNION ALL            

    -- Select Claim Level                
    SELECT  DISTINCT 2,
            1,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), c.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber),
            ISNULL(c.ClientClaimNumber, ''), --Project:210474 APD Modified when we did the code merge M.A.20061124
            ISNULL(cu.ClientUserID, ''),
            ISNULL(cu.NameFirst, ''),
            ISNULL(cu.NameLast, ''), 
                     
            ISNULL((SELECT TOP 1 i.NameFirst
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
              
            ISNULL((SELECT TOP 1 i.NameLast
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
              
            ISNULL((SELECT TOP 1 i.BusinessName
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
                
            ISNULL(c.IntakeFinishDate,''),
            ISNULL(CONVERT(varchar(30), c.LossDate, 101), ''),
            ISNULL(c.LossState, ''),
            ISNULL(c.PolicyNumber, ''),                    
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL

      FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
                         INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID  
                         INNER JOIN dbo.utb_invoice i ON ca.ClaimAspectID = i.ClaimAspectID  
                         LEFT JOIN dbo.utb_claim_coverage cc ON c.LynxID = cc.LynxID
                         LEFT JOIN dbo.utb_user cu ON c.CarrierRepUserID = cu.UserID           
      
    UNION ALL            

    -- Select Exposure Level                
    SELECT DISTINCT
            3,
            2,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), ca.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            
            -- Exposure Level
            ca.ClaimAspectID,
            ISNULL((SELECT Top 1 i.BusinessName
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''),    
                      
            ISNULL((SELECT Top 1 i.NameFirst
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''), 
                   
            ISNULL((SELECT Top 1 i.NameLast
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''),
            
            ISNULL(cct.Name, ''),
            ISNULL(ca.SourceApplicationPassThruData, ''),
            LTRIM(ISNULL(CONVERT(varchar(4), cv.VehicleYear), '') + ' ' + ISNULL(cv.Make, '') + ' ' + ISNULL(cv.Model, '') + ' ' + 
                 ISNULL(cv.BodyStyle, '') + ' ' + CASE WHEN cv.VIN IS NOT NULL THEN 'VIN: ' + cv.VIN ELSE '' END),
            ISNULL(ca.ClaimAspectNumber, ''),
            ISNULL(CONVERT(varchar(10), t2.ShopLocationID), ''),
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL
            
      FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID 
                         INNER JOIN dbo.utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID
                         LEFT JOIN dbo.utb_client_coverage_type cct ON ca.ClientCoverageTypeID = cct.ClientCoverageTypeID  
                         LEFT JOIN @tmpLastShopLocationIDs t2 ON ca.ClaimAspectID = t2.ClaimAspectID
             
    
      
    UNION ALL            

    -- Select Invoice Level                
    SELECT  4,
            3,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), ca.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            
            -- Exposure Level
            ISNULL(CONVERT(varchar(20), t.ClaimAspectID), ''), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
           InvoiceID,
           ISNULL(AuthorizingClientUserID, ''), 
           ISNULL(CONVERT(varchar(20), Amount), ''),
           ISNULL(ClientFeeCode, ''),
           ISNULL(DispatchNumber, ''),
           ISNULL(EntryDate, ''),
           ISNULL(FeeCategoryCD, ''),
           @now,
           ISNULL(InvoiceDescription, ''),
           ISNULL(ItemType, ''),
           ISNULL(PayeeID, 0), 
           ISNULL(PayeeName, ''),
           ISNULL(PayeeCity, ''),
           ISNULL(PayeeState, '')
            
    FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
       
    

    ORDER BY [Claim!2!LynxID], [Exposure!3!ClaimAspectID], Tag
    
    --FOR XML EXPLICIT          -- (Commented for client-side)
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error Selecting XML
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    
   
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowFileBillingPickupXML' AND type = 'P') BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowFileBillingPickupXML TO ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

