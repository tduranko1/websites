-- Begin the transaction for dropping and creating the stored procedure
BEGIN TRANSACTION

-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSimilarMatchWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopSimilarMatchWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspShopSimilarMatchWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     This SP check a new shop request to see if it matches an existing APD Shop.  It does this by matching
*				ZipCode, Phone and Address 1 numerics.
*
* Date:			19Oct2016
*
* PARAMETERS:  
*
* RESULT SET:
* An XML Stream containing shop summary data that matches the search criteria
*
* REVISIONS:	19Oct2016 - TVD - 1.0 - Initial development
*				09Nov2016 - TVD - 1.1 - Adding Choice shops to this search
*				09Nov2016 - TVD - 1.2 - Adding ShopTypeCode Param
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure  
CREATE PROCEDURE [dbo].[uspShopSimilarMatchWSXML]  
 @ShopName     udt_std_name  
 , @ShopAddress1    udt_addr_line_1  
 , @ShopAddress2    udt_addr_line_2  
 , @ShopCity     udt_addr_city  
 , @ShopState    udt_addr_state  
 , @ShopZip     udt_addr_zip_code  
 , @PhoneAreaCode   udt_ph_area_code  
 , @PhoneExchangeNumber  udt_ph_exchange_number  
 , @PhoneUnitNumber   udt_ph_unit_number  
 , @ShopTypeCode    VARCHAR(1) = 'B'  
 , @InsuranceCompanyID	udt_std_id
AS  
BEGIN  
 --=======================================================  
 -- Declarations and Sets  
 --=======================================================  
 DECLARE @ProcName    VARCHAR(50)    
 DECLARE @Now     DATETIME  
   
 -- Debug/Error Handling  
 DECLARE @Debug       BIT  
 DECLARE @DebugEventType     VARCHAR(50)  
 DECLARE @DebugEventStatus    VARCHAR(50)  
 DECLARE @DebugEventDescription   VARCHAR(100)  
 DECLARE @DebugEventDetailedDescription VARCHAR(1000)  
 DECLARE @DebugEventXML     VARCHAR(1000)  
 DECLARE @DebugSeq      INT  
  
 -- Sets  
 SET @Now = Current_Timestamp  
 SET @ProcName = 'uspShopSimilarMatchWSXML'  
 SET @Debug = 0  
 SET @DebugSeq = 0  
  
 -----------------------------------  
 -- Debug - Show Params  
 -----------------------------------  
 IF @Debug = 1  
 BEGIN  
  SET @DebugEventType = 'HQ - New Shop Match Check'  
  SET @DebugEventStatus = 'Debugging'       
  SET @DebugEventDescription = @ProcName + ' - Checking APD Shops for existing match.'     
  SET @DebugEventDetailedDescription = 'Parameters are in EventXML'    
  SET @DebugEventXML = 'Parameters: @ShopName = ''' + @ShopName + ''',  @ShopAddress1 = ''' + @ShopAddress1 + ''', @ShopAddress2 = ''' + @ShopAddress2 + ''', @ShopCity = ''' + @ShopCity + ''', @ShopState = ''' + @ShopState + ''', @ShopZip = ''' + CONVERT(
VARCHAR(10), @ShopZip) + ''', @PhoneAreaCode = ''' + CONVERT(VARCHAR(3), @PhoneAreaCode) + ''', @PhoneExchangeNumber = ''' + CONVERT(VARCHAR(3), @PhoneExchangeNumber) + ''', @PhoneUnitNumber = ''' + CONVERT(VARCHAR(4), @PhoneUnitNumber) + ''''  
  SET @DebugSeq = @DebugSeq + 1  
  
  PRINT '-------------------------------------------------------------------'  
  PRINT @DebugEventDescription  
  PRINT @DebugEventDetailedDescription  
  PRINT @DebugEventXML  
  PRINT '-------------------------------------------------------------------'  
  --INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)  
 END  
  
 -----------------------------------  
 -- Validation  
 -----------------------------------  
    -- Validate the ShopAddress1  
 IF (LEN(@ShopAddress1) < 1)   
  OR (@ShopAddress1 IS NULL)   
    BEGIN  
  SET @DebugEventType = 'HQ - New Shop Match Check'  
  SET @DebugEventStatus = 'ERROR'       
  SET @DebugEventDescription = @ProcName + ' - Validation of @ShopAddress failed.'     
  SET @DebugEventDetailedDescription = 'The @ShopAddress is a required field. '    
  SET @DebugEventXML = ''  
  SET @DebugSeq = @DebugSeq + 1  
  
  PRINT '-------------------------------------------------------------------'  
  PRINT @DebugEventDescription  
  PRINT @DebugEventDetailedDescription  
  PRINT @DebugEventXML  
  PRINT '-------------------------------------------------------------------'  
  --INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)  
  
        RAISERROR('%s: Invalid @ShopAddress.  @ShopAddress is a required field.', 16, 1, @ProcName)  
        RETURN  
    END  
  
    -- Validate the Shop Phone  
 IF ((LEN(@PhoneAreaCode) < 1) OR (@PhoneAreaCode IS NULL))   
    OR ((LEN(@PhoneExchangeNumber) < 1) OR (@PhoneExchangeNumber IS NULL))  
    OR ((LEN(@PhoneUnitNumber) < 1) OR (@PhoneUnitNumber IS NULL))  
 BEGIN  
  SET @DebugEventType = 'HQ - New Shop Match Check'  
  SET @DebugEventStatus = 'ERROR'       
  SET @DebugEventDescription = @ProcName + ' - Validation of Phone failed.'     
  SET @DebugEventDetailedDescription = 'The @PhoneAreaCode, @PhoneExchangeNumber and @PhoneUnitNumber is a required field. '    
  SET @DebugEventXML = ''  
  SET @DebugSeq = @DebugSeq + 1  
  
  PRINT '-------------------------------------------------------------------'  
  PRINT @DebugEventDescription  
  PRINT @DebugEventDetailedDescription  
  PRINT @DebugEventXML  
  PRINT '-------------------------------------------------------------------'  
  --INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)  
  
        RAISERROR('%s: Invalid Phone Number.  @PhoneAreaCode, @PhoneExchangeNumber and @PhoneUnitNumber is a required field.', 16, 1, @ProcName)  
        RETURN  
    END  
  
    -- Validate the ShopZip  
 IF (LEN(@ShopZip) < 1)   
  OR (@ShopZip IS NULL)   
    BEGIN  
  SET @DebugEventType = 'HQ - New Shop Match Check'  
  SET @DebugEventStatus = 'ERROR'       
  SET @DebugEventDescription = @ProcName + ' - Validation of @ShopZip failed.'     
  SET @DebugEventDetailedDescription = 'The @ShopZip is a required field. '    
  SET @DebugEventXML = ''  
  SET @DebugSeq = @DebugSeq + 1  
  
  PRINT '-------------------------------------------------------------------'  
  PRINT @DebugEventDescription  
  PRINT @DebugEventDetailedDescription  
  PRINT @DebugEventXML  
  PRINT '-------------------------------------------------------------------'  
  --INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)  
  
        RAISERROR('%s: Invalid @ShopZip.  @ShopZip is a required field.', 16, 1, @ProcName)  
        RETURN  
    END  
  
 -----------------------------------  
 -- Debug - Shop Address 1 numeric  
 -----------------------------------  
 IF @Debug = 1  
 BEGIN  
  SET @DebugEventType = 'HQ - Shop Address1 Numeric'  
  SET @DebugEventStatus = 'Debugging'       
  SET @DebugEventDescription = @ProcName + ' - Checking dbo.ufnGetNumeric()'     
  SET @DebugEventDetailedDescription = 'Parameters are in EventXML'    
  SET @DebugEventXML = 'Parameters: @ShopAddress1 = ''' + @ShopAddress1 + ''', dbo.ufnGetNumeric(@ShopAddress1) = ''' + dbo.ufnGetNumeric(@ShopAddress1)  + ''''  
  SET @DebugSeq = @DebugSeq + 1  
  
  PRINT '-------------------------------------------------------------------'  
  PRINT @DebugEventDescription  
  PRINT @DebugEventDetailedDescription  
  PRINT @DebugEventXML  
  PRINT '-------------------------------------------------------------------'  
  --INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)  
 END  
  
 -------------------------------------  
 -- Create temp table to hold results  
 -------------------------------------  
 CREATE TABLE #SimilarShop (  
  ShopSource     VARCHAR(15)  
  , ShopLocationID   INT  
  , ShopGUID     VARCHAR(50)  
  , ShopName     VARCHAR(50)  
  , ShopAddress1    VARCHAR(50)  
  , ShopAddress2    VARCHAR(50)  
  , ShopCity     VARCHAR(30)  
  , ShopState     VARCHAR(2)  
  , ShopZip     VARCHAR(10)  
  , ShopPhoneAreaCode         CHAR(3)        
  , ShopPhoneExchangeNumber   CHAR(3)        
  , ShopPhoneUnitNumber       VARCHAR(5)        
  , ShopPhoneExtensionNumber  CHAR(4)    
  , SelectedShopRank   INT  
  , SelectedShopScore   INT  
  , ShopSearchLogID   INT  
  , FedralTaxID    VARCHAR(30)  
  , Distance     FLOAT  
 )  
  
 -------------------------------------  
 -- Add Lynx Shops to temp table  
 -------------------------------------  
    IF UPPER(@ShopTypeCode) IN ('P', 'B')  
    BEGIN  
  INSERT INTO #SimilarShop  
  SELECT    
   CASE
    WHEN (((sl.ShopLocationID IN (SELECT csl.ShopLocationID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID))
           AND ((sl.ProgramFlag = 1 OR sl.ReferralFlag = 1) AND sl.AvailableForSelectionFlag = 0))
           OR (sl.ShopLocationID IN (SELECT csl.ShopLocationID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID))) THEN 'LYNXDISABLE' -- LYNX SELECTSHOP but Disable one 
    WHEN ((sl.ShopLocationID IN (SELECT csl.ShopLocationID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID))
           AND ((sl.ProgramFlag = 1 OR sl.ReferralFlag = 1) AND sl.AvailableForSelectionFlag = 1)) THEN 'LYNX' -- Lynx Select Shop  
    WHEN ((sl.ProgramFlag = 1 OR sl.ReferralFlag = 1) AND sl.AvailableForSelectionFlag = 1) THEN 'LYNXHIDE' -- Lynx Select Shop but its not included 
    WHEN sl.PPGCTSid LIKE REPLACE('00000000-0000-0000-0000-000000000000', '0', '[0-9a-fA-F]') THEN 'HQ' -- HQ Matched Shop  
    WHEN (sl.ProgramFlag = 0 AND sl.AvailableForSelectionFlag = 0) THEN 'HQNew' -- HQ New NOT Provisioned in HQ Shop  
    --WHEN (sl.AutoVerseId IS NOT NULL AND sl.AvailableForSelectionFlag = 1) THEN 'ADP' -- ADP Shop  
    ELSE 'HQNew'  
   END  
   , sl.ShopLocationID  
   , ISNULL(sl.PPGCTSID, '')  
   , sl.Name      
   , sl.Address1     
   , ISNULL(sl.Address2, '')  
   , sl.AddressCity    
   , sl.AddressState    
   , sl.AddressZip     
   , ISNULL(sl.PhoneAreaCode, '')    
   , ISNULL(sl.PhoneExchangeNumber, '')  
   , ISNULL(sl.PhoneUnitNumber, '')    
   , ISNULL(sl.PhoneExtensionNumber, '')  
   , 0        
   , ISNULL(sl.ProgramScore,'')  
   , 0  
   , ISNULL(s.FedTaxId,'')  
   , 0  
  FROM  
   utb_shop_location sl  
   INNER JOIN utb_shop s  
    ON s.ShopID = sl.ShopID  
  WHERE  
   sl.AddressZip = @ShopZip  
   AND sl.PhoneAreaCode = @PhoneAreaCode  
   AND sl.PhoneExchangeNumber = @PhoneExchangeNumber  
   AND sl.PhoneUnitNumber = @PhoneUnitNumber  
   AND dbo.ufnGetNumeric(sl.Address1) = dbo.ufnGetNumeric(@ShopAddress1)  
 END  

 -------------------------------------  
 -- Add HQ Shops to temp table  
 -------------------------------------  
    IF UPPER(@ShopTypeCode) IN ('C', 'B') 
    BEGIN  
    IF NOT EXISTS (SELECT * FROM #SimilarShop WHERE ShopSource = 'LYNXHIDE')
    BEGIN
    PRINT 'IN IF'
	 -- INSERT INTO #SimilarShop  
	  SELECT    
		'HQ' ShopSource 
		, 0  ShopLocationID
		, GUID ShopGUID
		, p.Name ShopName
		, p.Address1 ShopAddress1
		, ISNULL(p.Address2,'') ShopAddress2
		, City ShopCity
		, State ShopState 
		, Zip ShopZip
		, SUBSTRING(ISNULL(p.Phone, ''),1,3) ShopPhoneAreaCode
		, SUBSTRING(ISNULL(p.Phone, ''),4,3) ShopPhoneExchangeNumber
		, SUBSTRING(ISNULL(p.Phone, ''),7,4) ShopPhoneUnitNumber
		, 0 ShopPhoneExtensionNumber
		, 0 SelectedShopRank      
		, '' SelectedShopScore  
		, 0 ShopSearchLogID
		, ISNULL(p.FederalTaxId,'') FedralTaxID
		, 0 Distance
		INTO #SimilarHQShop 
	  FROM  
	   provider p  
	  WHERE  
	   p.Zip = @ShopZip  
	   AND p.Phone = @PhoneAreaCode + @PhoneExchangeNumber + @PhoneUnitNumber  
	   AND dbo.ufnGetNumeric(p.Address1) = dbo.ufnGetNumeric(@ShopAddress1) 
	   END
	   ELSE
		   BEGIN
		   PRINT 'IN ELSE'
		  INSERT INTO #SimilarShop  
		  SELECT    
			'HQ' ShopSource 
			, 0  ShopLocationID
			, GUID ShopGUID
			, p.Name ShopName
			, p.Address1 ShopAddress1
			, ISNULL(p.Address2,'') ShopAddress2
			, City ShopCity
			, State ShopState 
			, Zip ShopZip
			, SUBSTRING(ISNULL(p.Phone, ''),1,3) ShopPhoneAreaCode
			, SUBSTRING(ISNULL(p.Phone, ''),4,3) ShopPhoneExchangeNumber
			, SUBSTRING(ISNULL(p.Phone, ''),7,4) ShopPhoneUnitNumber
			, 0 ShopPhoneExtensionNumber
			, 0 SelectedShopRank      
			, '' SelectedShopScore  
			, 0 ShopSearchLogID
			, ISNULL(p.FederalTaxId,'') FedralTaxID
			, 0 Distance
			--INTO #SimilarHQShop 
		  FROM  
		   provider p  
		  WHERE  
		   p.Zip = @ShopZip  
		   AND p.Phone = @PhoneAreaCode + @PhoneExchangeNumber + @PhoneUnitNumber  
		   AND dbo.ufnGetNumeric(p.Address1) = dbo.ufnGetNumeric(@ShopAddress1) 
		   END
 END  

 --==========================================  
-- Merge Processing  
--==========================================  
--SELECT * FROM #SimilarHQShop
IF NOT EXISTS (SELECT * FROM #SimilarShop WHERE ShopSource = 'LYNXHIDE')
    BEGIN
	MERGE INTO #SimilarShop WITH (HOLDLOCK) AS target
	USING #SimilarHQShop AS source
		ON target.ShopName = source.ShopName
		AND dbo.ufnGetNumeric(target.ShopAddress1) = dbo.ufnGetNumeric(source.ShopAddress1)
		AND target.ShopCity = source.ShopCity
		AND target.ShopState = source.ShopState
		AND target.ShopZip = source.ShopZip
		AND target.ShopPhoneAreaCode = source.ShopPhoneAreaCode
		AND target.ShopPhoneExchangeNumber = source.ShopPhoneExchangeNumber
		AND target.ShopPhoneUnitNumber = source.ShopPhoneUnitNumber
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (ShopSource, ShopLocationID, ShopGUID, ShopName, ShopAddress1, ShopAddress2, ShopCity, ShopState, ShopZip, ShopPhoneAreaCode,
		        ShopPhoneExchangeNumber, ShopPhoneUnitNumber, ShopPhoneExtensionNumber, SelectedShopRank, SelectedShopScore, ShopSearchLogID, 
		        FedralTaxID, Distance)
		VALUES (source.ShopSource, source.ShopLocationID, source.ShopGUID, source.ShopName, source.ShopAddress1, source.ShopAddress2, source.ShopCity,
		        source.ShopState, source.ShopZip, source.ShopPhoneAreaCode ,source.ShopPhoneExchangeNumber, source.ShopPhoneUnitNumber, 
		        source.ShopPhoneExtensionNumber, source.SelectedShopRank, source.SelectedShopScore, source.ShopSearchLogID, 
		        source.FedralTaxID, source.Distance);
END


 --==============================================================================
 -- Identifying duplicate records
 --=============================================================================
  
   ALTER TABLE #SimilarShop  
   ADD IsDuplicateShop BIT   
     
   DECLARE @ShpSource VARCHAR(10)  
   DECLARE @ShpAddress1 VARCHAR(50)  
   DECLARE @ShpCity VARCHAR(30)  
   DECLARE @ShpState VARCHAR(2)  
   DECLARE @ShpZip VARCHAR(10)  
   DECLARE @ShpPhoneAreaCode CHAR(3)  
   DECLARE @ShpPhoneExchangeNumber CHAR(3)  
   DECLARE @ShpPhoneUnitNumber VARCHAR(5)  
   declare @ShpGUID     VARCHAR(50)  
   DECLARE @ShpLocationID INT  
   DECLARE @IsDuplicateShop BIT  
   DECLARE @CSCount INT
   DECLARE @PSCount INT
   DECLARE @PSHIDECount INT
     
   DECLARE curDuplicateShops CURSOR FOR  
    SELECT ShopSource,  
          ShopAddress1,  
          ShopCity,  
          ShopState,  
          ShopZip,  
          ShopPhoneAreaCode,  
          ShopPhoneExchangeNumber,  
          ShopPhoneUnitNumber,  
          ShopGUID,   
          ShopLocationID,  
          IsDuplicateShop  
           from #SimilarShop   
             
   OPEN curDuplicateShops  
    FETCH NEXT FROM  curDuplicateShops INTO @ShpSource, @ShpAddress1, @ShpCity, @ShpState, @ShpZip, @ShpPhoneAreaCode,  
          @ShpPhoneExchangeNumber, @ShpPhoneUnitNumber, @ShpGUID,@ShpLocationID, @IsDuplicateShop  
      
    WHILE @@FETCH_STATUS=0   
    BEGIN 
    
    IF @IsDuplicateShop IS NULL 
    BEGIN
		SELECT @CSCount= count(ShopLocationID) from #SimilarShop where  ShopCity = @ShpCity  
																	 AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)  
																	 AND ShopState = @ShpState AND ShopZip = @ShpZip   
																	 AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
																	 AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
																	 AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber 
																	 AND ShopSource IN ('HQ', 'HQNew')
	    
		SELECT @PSCount= count(ShopLocationID) from #SimilarShop where  ShopCity = @ShpCity  
																	 AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)  
																	 AND ShopState = @ShpState AND ShopZip = @ShpZip   
																	 AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
																	 AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
																	 AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber 
																	 AND ShopSource IN ('LYNX','LYNXDISABLE','LYNXHIDE')	
																	 	
	   SELECT @PSHIDECount= count(ShopLocationID) from #SimilarShop where  ShopCity = @ShpCity  
																	 AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)  
																	 AND ShopState = @ShpState AND ShopZip = @ShpZip   
																	 AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
																	 AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
																	 AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber 
																	 AND ShopSource IN ('LYNXHIDE')														 
      
	                                                                 
		   IF @PSCount=1
			BEGIN
			   IF @PSHIDECount > 0
				 BEGIN
				   UPDATE  #SimilarShop   
						SET IsDuplicateShop = 1
					 WHERE ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
							AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   
							AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
							AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
							AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('HQ', 'HQNew') 
						
					  UPDATE  #SimilarShop   
						SET IsDuplicateShop = 0
					  WHERE ShopGUID in (SELECT TOP 1 ShopGUID FROM #SimilarShop WHERE IsDuplicateShop  = 1 AND ShopSource = 'HQ') 
					  				 
					  IF NOT EXISTS(SELECT * FROM #SimilarShop WHERE IsDuplicateShop  = 0)
						 BEGIN
						  UPDATE  #SimilarShop   
						  SET IsDuplicateShop = 0
						  WHERE  ShopLocationID in (SELECT TOP 1 ShopLocationID FROM #SimilarShop WHERE IsDuplicateShop  = 1 AND ShopSource = 'HQNew') 
						 END
				 END
			   ELSE
				 BEGIN
					UPDATE  #SimilarShop   
					SET IsDuplicateShop = 1
				    WHERE ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
						AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   
						AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
						AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
						AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('HQ', 'HQNew') 
				 END	
		                
				 UPDATE  #SimilarShop   
					SET IsDuplicateShop = 0
				 WHERE ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
						AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   
						AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
						AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
						AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('LYNX','LYNXDISABLE','LYNXHIDE')  
			END
		   ELSE
			BEGIN
				UPDATE  #SimilarShop   
					SET IsDuplicateShop = 1
				 WHERE 
						ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
						AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   
						AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
						AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
						AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('HQ', 'HQNew')
					    
			
				UPDATE #SimilarShop   
					SET IsDuplicateShop = 0
				 WHERE  ShopLocationID= (Select TOP 1 ShopLocationID FROM #SimilarShop where ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
										AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   
										AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
										AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
										AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('HQ', 'HQNew'))
						
						and ShopGUID = (Select TOP 1 ShopGUID FROM #SimilarShop where ShopCity = @ShpCity AND ShopState = @ShpState AND ShopZip = @ShpZip  
										AND dbo.ufnGetNumeric(ShopAddress1) = dbo.ufnGetNumeric(@ShpAddress1)   						
										AND ShopPhoneAreaCode = @ShpPhoneAreaCode   
										AND ShopPhoneExchangeNumber = @ShpPhoneExchangeNumber  
										AND ShopPhoneUnitNumber = @ShpPhoneUnitNumber AND ShopSource IN ('HQ', 'HQNew'))
					
						
			END
     
	END
    FETCH NEXT FROM  curDuplicateShops into @ShpSource, @ShpAddress1, @ShpCity, @ShpState, @ShpZip, @ShpPhoneAreaCode,  
          @ShpPhoneExchangeNumber, @ShpPhoneUnitNumber, @ShpGUID,@ShpLocationID, @IsDuplicateShop  
  END  
  
     
  CLOSE curDuplicateShops  
  DEALLOCATE curDuplicateShops
  
  
--==========================================  
-- Main XML Processing  
--==========================================  

    SELECT  1 AS Tag  
            , NULL AS Parent  
   -- Root  
   , @ShopName    AS [Root!1!NewShopName]  
   , @ShopAddress1   AS [Root!1!NewShopAddress1]  
   , @ShopAddress2   AS [Root!1!NewShopAddress2]  
   , @ShopCity    AS [Root!1!NewShopCity]  
   , @ShopState   AS [Root!1!NewShopState]  
   , @ShopZip    AS [Root!1!NewShopZip]  
   , @PhoneAreaCode  AS [Root!1!NewShopPhoneAreaCode]  
   , @PhoneExchangeNumber AS [Root!1!NewShopPhoneExchangeNumber]  
   , @PhoneUnitNumber  AS [Root!1!NewShopPhoneUnitNumber]  
   -- Shop  
   , NULL     AS [Shop!2!Source]  
   , NULL     AS [Shop!2!ShopLocationID]  
   , NULL     AS [Shop!2!ShopGUID]  
   , NULL     AS [Shop!2!ShopName]  
   , NULL     AS [Shop!2!ShopAddress1]  
   , NULL     AS [Shop!2!ShopAddress2]  
   , NULL     AS [Shop!2!ShopCity]  
   , NULL     AS [Shop!2!ShopState]  
   , NULL     AS [Shop!2!ShopZip]  
      , NULL     AS [Shop!2!ShopPhoneAreaCode]    
      , NULL     AS [Shop!2!ShopPhoneExchangeNumber]    
      , NULL     AS [Shop!2!ShopPhoneUnitNumber]    
   , NULL     AS [Shop!2!SelectedShopRank]  
   , NULL     AS [Shop!2!SelectedShopScore]  
   , NULL     AS [Shop!2!ShopSearchLogID]  
   , NULL     AS [Shop!2!FedralTaxID]  
   , NULL     AS [Shop!2!Distance]  
UNION ALL  
  
    SELECT  2 AS Tag  
            , 1 AS Parent  
   -- Root  
   , NULL, NULL, NULL, NULL, NULL  
   , NULL, NULL, NULL, NULL  
   -- Shop  
   , ShopSource  
   , ShopLocationID  
   , ISNULL(ShopGUID, '')   
   , ShopName  
   , ShopAddress1  
   , ISNULL(ShopAddress2, '')  
   , ShopCity   
   , ShopState   
   , ShopZip   
      , ShopPhoneAreaCode    
      , ShopPhoneExchangeNumber    
      , ShopPhoneUnitNumber    
   , 0     
   , ISNULL(SelectedShopScore,'')  
   , 0  
   , ISNULL(FedralTaxID,'')  
   , 0  
 FROM  
  #SimilarShop  WHERE IsDuplicateShop = 0  AND ShopSource <> 'LYNXHIDE'
  
FOR XML EXPLICIT  
  
DROP TABLE #SimilarShop
  
END  
GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSimilarMatchWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopSimilarMatchWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
************************************************************************************************************************/
