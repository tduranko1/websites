-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientReportsGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClientReportsGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClientReportsGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Procedure to return the list of available Client reports
*
* PARAMETERS:  
*
* RESULT SET:
* returns the list of Client reports and the supporting reference values as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClientReportsGetListXML]
    @ReportTypeCD     varchar(1) = NULL
AS
BEGIN

        -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now               AS DateTime          -- Timestamp for Procedure

    SET @ProcName = 'uspClientReportsGetListXML'
    SET @Now = Current_TimeStamp


    DECLARE @tmpReference TABLE 
    (
        ListName            varchar(50)  NOT NULL,
        ListCode            varchar(10)  NOT NULL,
        ListValue           varchar(50)  NOT NULL,
        InsuranceCompanyID  smallint     NULL
    )

    DECLARE @tmpInsuranceContractedStates TABLE 
    (
        InsuranceCompanyID  int          NOT NULL,
        StateCode           varchar(2)   NOT NULL,
        StateValue          varchar(50)  NOT NULL,
        DisplayOrder        int          NOT NULL
    )
    
    -- Check the report Type CD
    IF @ReportTypeCD NOT IN ('C', 'M', 'O', 'P', 'U')
    BEGIN
        RAISERROR  ('101|%s|@ReportTypeCD|%s', 16, 1, @ProcName, @ReportTypeCD)
        RETURN
    END
    
    INSERT INTO @tmpInsuranceContractedStates
    SELECT DISTINCT o.InsuranceCompanyID, sc.StateCode, sc.StateValue, sc.DisplayOrder
    FROM utb_office o, utb_office_contract_state ocs, utb_state_code sc
    WHERE o.OfficeID = ocs.OfficeID
      AND ocs.StateCode = sc.StateCode
      AND sc.EnabledFlag = 1
    GROUP BY o.InsuranceCompanyID, sc.StateCode, sc.StateValue, sc.DisplayOrder
    ORDER BY o.InsuranceCompanyID, sc.DisplayOrder    

    INSERT INTO @tmpReference (ListName, ListCode, ListValue, InsuranceCompanyID)
      SELECT  'Coverage Type' AS ListName, 
              CAST(Code AS Varchar),
              Name,
              NULL
        FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'CoverageProfileCD')
        
      UNION ALL

      SELECT  'Office', 
              CAST(OfficeID AS Varchar),
              CASE 
                WHEN Len(LTrim(RTrim(ClientOfficeId))) > 0 THEN ClientOfficeID
                ELSE Name
              END,
              InsuranceCompanyID
        FROM  dbo.utb_office
        WHERE  EnabledFlag = 1
      
      UNION ALL

      SELECT  'Service Channel', 
              CAST(Code AS Varchar),
              Name,
              NULL
        FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'ServiceChannelCD')

      UNION ALL

      SELECT  'State',
              CAST(StateCode AS Varchar),
              StateValue,
              InsuranceCompanyID
        FROM  @tmpInsuranceContractedStates
              
      UNION ALL
      
      SELECT  'InsuranceCompany', 
               CAST(InsuranceCompanyID AS Varchar),
               Name,
               NULL
        FROM   utb_insurance
        WHERE  EnabledFlag = 1

    
    SELECT
        1 as Tag,
        Null as Parent,
        --Root
        'Root' as [Root!1!Root],
        --Client Reports
        NULL as [Report!2!ClientReportID],
        NULL as [Report!2!Description],
        NULL as [Report!2!FileName],
        NULL as [Report!2!ShowCoverageTypeCD],
        NULL as [Report!2!ShowOfficeID],
        NULL as [Report!2!ShowServiceChannelCD],
        NULL as [Report!2!ShowShowAllClientsFlag],
        NULL as [Report!2!ShowShowUserDetailsFlag],
        NULL as [Report!2!ShowStateCD],
        NULL as [Report!2!StoredProcName],
        NULL as [Report!2!StaticParameters],
        NULL as [Report!2!DisplayOrder],
        -- Reference Data
        Null as [Reference!3!ListName],
        Null as [Reference!3!ListCode],
        Null as [Reference!3!ListValue],
        Null as [Reference!3!InsuranceCompanyID]

    UNION ALL

    SELECT
        2 as Tag,
        1 as Parent,
        --Root
        NULL,
        --Insurance Company
        R.ReportID,
        IsNull(R.Description, ''),
        IsNull(R.FileName, ''),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'Coverage Type'),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'Office'),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'Service Channel'),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'View "All Clients" Data'),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'Show User Details'),
        (SELECT count(c.CriteriaID)
           FROM utb_criteria c, utb_report_criteria rc
           WHERE c.CriteriaID = rc.CriteriaID
             AND rc.ReportID = R.ReportID
             AND DisplayText = 'State'),
        IsNull(R.SPName, ''),
        IsNull(R.StaticParameters, ''),
        IsNull(R.DisplayOrder, ''),
        -- Reference Data
        NULL, NULL, NULL, NULL
    FROM
        utb_report R
    WHERE ReportTypeCD = @ReportTypeCD
      AND r.EnabledFlag = 1

--****************************************************************************

    UNION ALL

    -- Reference Data

    SELECT
        3 as tag,
        1 as parent,
        NULL,
        -- Insurance
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
        -- Reference Data
        ListName, ListCode, ListValue, InsuranceCompanyID
    FROM
        @tmpReference


END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientReportsGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClientReportsGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/