-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveInfoFromAutoverseIdXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopRetrieveInfoFromAutoverseIdXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopRetrieveInfoFromAutoverseIdXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


create procedure dbo.uspShopRetrieveInfoFromAutoverseIdXML
    @AutoverseID        udt_std_desc_mid,
    @InsuranceCompanyID udt_std_id
as
begin

    declare @ShopLocID      udt_std_id
    declare @AllowSelection udt_std_flag

    declare @WarrantyRefinishMinYrs     udt_std_int_tiny
    declare @WarrantyWorkmanshipMinYrs  udt_std_int_tiny    
    
    
    select @ShopLocID = ShopLocationID
    from utb_shop_location
    where AutoverseID = @AutoverseID
    
    if @ShopLocID is not null
    begin
      
      select @WarrantyRefinishMinYrs = convert(tinyint, WarrantyPeriodRefinishMinCD),
             @WarrantyWorkmanshipMinYrs = convert(tinyint, WarrantyPeriodWorkmanshipMinCD)
      from dbo.utb_insurance
      where InsuranceCompanyID = @InsuranceCompanyID    

      select @AllowSelection = count(sl.ShopLocationID)
      from utb_shop_location sl 
      inner join utb_client_contract_state ccs on (@InsuranceCompanyID = ccs.InsuranceCompanyID and sl.AddressState = ccs.StateCode)
      left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
      left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csli on (sl.ShopLocationID = csli.ShopLocationID)
      where ((ccs.UseCEIShopsFlag = 1 and sl.CEIProgramFlag = 1) or (ccs.UseCEIShopsFlag = 0 and sl.ProgramFlag = 1)) 
        and sl.EnabledFlag = 1
--        and sl.AvailableForSelectionFlag = 1
        and ((@InsuranceCompanyID not in (145, 209) and sl.AvailableForSelectionFlag = 1) 
          OR (@InsuranceCompanyID in (145, 209)))
        and ((@InsuranceCompanyID not in (145, 209) and sl.AddressState <> 'MA') 
          OR (@InsuranceCompanyID in (145, 209)))
        and isnull(convert(tinyint,sl.WarrantyPeriodRefinishCD),0) >= @WarrantyRefinishMinYrs
        and isnull(convert(tinyint,sl.WarrantyPeriodWorkmanshipCD),0) >= @WarrantyWorkmanshipMinYrs
        and csle.InsuranceCompanyID is null
        and (csli.InsuranceCompanyID is null or csli.InsuranceCompanyID = @InsuranceCompanyID)
        and sl.ShopLocationID = @ShopLocID
    end
    
      
    select 1 as Tag,
           0 as Parent,
           @AutoverseID as [Root!1!AutoverseId],
           --
           null as [Shop!2!ShopLocationID],
           null as [Shop!2!Name],
           null as [Shop!2!AddressState],
           null as [Shop!2!AllowSelection]           
    union all
    select 2 as Tag,
           1 as Parent,
           null,
           --           
           ShopLocationID as [Shop!1!ShopLocationID],
           Name,
           AddressState as [Shop!1!AddressState],
           @AllowSelection
    from utb_shop_location
    where ShopLocationID = @ShopLocID

        
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveInfoFromAutoverseIdXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopRetrieveInfoFromAutoverseIdXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/