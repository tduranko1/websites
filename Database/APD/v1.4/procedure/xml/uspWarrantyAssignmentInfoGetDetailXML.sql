-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyAssignmentInfoGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWarrantyAssignmentInfoGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWarrantyAssignmentInfoGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This procedure will return assignment information
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWarrantyAssignmentInfoGetDetailXML
(
    @AssignmentID   as bigint
)
AS
BEGIN

    DECLARE @ProcName           as varchar(50)

    -- Set Database options
    
    SET NOCOUNT ON

    SET @ProcName = 'uspWarrantyAssignmentInfoGetDetailXML'
    
    IF NOT EXISTS(SELECT AssignmentID
                    FROM dbo.utb_warranty_assignment
                    WHERE AssignmentID = @AssignmentID)
    BEGIN
       -- Invalid Assignment ID

        RAISERROR('102|%s|"AssignmentID"|Invalid Assignment ID', 16, 1, @ProcName)
        RETURN
    END

    -- Begin final select 
    SELECT
        1                      AS Tag,
        NULL                   AS Parent,
        --Root
        @AssignmentID    AS [Root!1!AssignmentID],
        --  Assignment Information
        --NULL                   AS [Assignment!2!AssignmentID],
        NULL                   AS [Assignment!2!AppraiserID],
        NULL                   AS [Assignment!2!ClaimAspectServiceChannelID],
        NULL                   AS [Assignment!2!CommunicationMethodID],
        NULL                   AS [Assignment!2!ShopLocationID],
        NULL                   AS [Assignment!2!AssignmentDate],
        NULL                   AS [Assignment!2!AssignmentReceivedDate],
        NULL                   AS [Assignment!2!AssignmentRemarks],
        NULL                   AS [Assignment!2!AssignmentSuffix],
        NULL                   AS [Assignment!2!CancellationDate],
        --NULL                   AS [Assignment!2!CertifiedFirstFlag],
        NULL                   AS [Assignment!2!CommunicationAddress],
        NULL                   AS [Assignment!2!EffectiveDeductibleSentAmt],
        NULL                   AS [Assignment!2!ProgramTypeCD],
        NULL                   AS [Assignment!2!ReferenceId],
        NULL                   AS [Assignment!2!SearchTypeCD],
        NULL                   AS [Assignment!2!SelectionDate],
        NULL                   AS [Assignment!2!SysLastUserID],
        NULL                   AS [Assignment!2!SysLastUpdatedDate]

    UNION ALL

    --ShopInfo Level
    SELECT  2,
            1,
            --Root
            NULL,
            
            --ShopInfo
            AssignmentID,
            --isNull(convert(varchar, AppraiserID), ''),
            isNull(convert(varchar, ClaimAspectServiceChannelID), ''),
            isNull(convert(varchar, CommunicationMethodID), ''),
            isNull(convert(varchar, ShopLocationID), ''),
            isNull(dbo.ufnUtilityGetDateString(AssignmentDate), ''),
            isNull(dbo.ufnUtilityGetDateString(AssignmentReceivedDate), ''),
            isNull(AssignmentRemarks, ''),
            isNull(AssignmentSuffix, ''),
            isNull(dbo.ufnUtilityGetDateString(CancellationDate), ''),
            --isNull(convert(varchar, CertifiedFirstFlag), ''),
            isNull(CommunicationAddress, ''),
            isNull(convert(varchar, EffectiveDeductibleSentAmt), ''),
            isNull(ProgramTypeCD, ''),
            isNull(ReferenceId, ''),
            isNull(SearchTypeCD, ''),
            isNull(dbo.ufnUtilityGetDateString(SelectionDate), ''),
            SysLastUserID,
            SysLastUpdatedDate

   FROM dbo.utb_warranty_assignment
   WHERE AssignmentID = @AssignmentID
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyAssignmentInfoGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWarrantyAssignmentInfoGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
