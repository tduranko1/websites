-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRepairSchReasonGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRepairSchReasonGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRepairSchReasonGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This proc will return the reasons for repair date change
*
* PARAMETERS:  
* (I) @RepairStartConfirmFlag     bit
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRepairSchReasonGetListXML
    @ClaimAspectServiceChannelID  udt_std_id_big
AS
BEGIN
    DECLARE @ProcName                   AS varchar(30)       -- Used for raise error stmts 
    DECLARE @WorkStartConfirmFlag     AS bit
    DECLARE @WorkStartNotConfirmFlag  AS bit

    SET @ProcName = 'uspRepairSchReasonGetListXML'
    
    SET NOCOUNT ON

    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectServiceChannelID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
    BEGIN
        -- Invalid Claim Aspect Service Channel ID
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END
    
    SELECT @WorkStartConfirmFlag = WorkStartConfirmFlag  
    FROM utb_Claim_Aspect_Service_Channel casc 
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    IF @WorkStartConfirmFlag = 0
        SET @WorkStartNotConfirmFlag = 1
    ELSE
        SET @WorkStartNotConfirmFlag = 0

    
    
    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Reasons
            NULL AS [Reason!2!ReasonID],
            NULL AS [Reason!2!DisplayOrder],
            NULL AS [Reason!2!Name]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Reasons
            ReasonID,
            DisplayOrder,
            Name
    FROM dbo.utb_service_channel_schedule_change_reason cr
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cr.ServiceChannelCD = casc.ServiceChannelCD
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      AND (cr.WorkStartNotConfirmFlag = @WorkStartNotConfirmFlag
        OR cr.WorkStartConfirmFlag = @WorkStartConfirmFlag)

    ORDER BY tag, [Reason!2!DisplayOrder]
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRepairSchReasonGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRepairSchReasonGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
