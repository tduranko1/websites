-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL details
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID                        ClaimAspectServiceChannelID
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLGetDetailXML
    @ClaimAspectServiceChannelID udt_std_id
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    DECLARE @LienHolderID as bigint
    DECLARE @ClaimAspectID as bigint
    DECLARE @ServiceChannelStatus as varchar(50)
    

    SET @ProcName = 'uspTLGetDetailXML'
    SET @now = CURRENT_TIMESTAMP

    
    IF NOT EXISTS (SELECT ClaimAspectServiceChannelID 
                   FROM utb_claim_aspect_service_channel 
                   WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                     AND ServiceChannelCD = 'TL')
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID.', 16, 1, @ProcName)
        RETURN
    END


    
    SELECT @LienHolderID = LienHolderID,
           @ServiceChannelStatus = s.Name,
           @ClaimAspectID = casc.ClaimAspectID
    
    FROM dbo.utb_claim_aspect_service_channel casc
    LEFT JOIN dbo.utb_claim_aspect_status cas ON casc.ClaimAspectID = cas.ClaimAspectID
    LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      AND cas.StatusTypeCD = 'SC'

    
    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        @ClaimAspectID AS [Root!1!ClaimAspectID],
        @ClaimAspectServiceChannelID AS [Root!1!ClaimAspectServiceChannelID],
        @ServiceChannelStatus AS [Root!1!ServiceChannelStatus],

        --Lien Holder
        NULL AS [LienHolder!2!LienHolderID],
        NULL AS [LienHolder!2!ParentLienHolderID],
        NULL AS [LienHolder!2!Name],
        NULL AS [LienHolder!2!Address1],
        NULL AS [LienHolder!2!Address2],
        NULL AS [LienHolder!2!AddressCity],
        NULL AS [LienHolder!2!AddressState],
        NULL AS [LienHolder!2!AddressZip],
        NULL AS [LienHolder!2!Phone],
        NULL AS [LienHolder!2!Fax],
        NULL AS [LienHolder!2!OfficeName],
        NULL AS [LienHolder!2!ContactName],
        NULL AS [LienHolder!2!EmailAddress],
        NULL AS [LienHolder!2!FedTaxID],
        NULL AS [LienHolder!2!BusinessTypeCD],
        NULL AS [LienHolder!2!EFTAccountNumber],
        NULL AS [LienHolder!2!EFTAccountTypeCD],
        NULL AS [LienHolder!2!EFTContractSignedFlag],
        NULL AS [LienHolder!2!EFTEffectiveDate],
        NULL AS [LienHolder!2!EFTRoutingNumber],
        NULL AS [LienHolder!2!BillingName],
        NULL AS [LienHolder!2!BillingAddress1],
        NULL AS [LienHolder!2!BillingAddress2],
        NULL AS [LienHolder!2!BillingAddressCity],
        NULL AS [LienHolder!2!BillingAddressState],
        NULL AS [LienHolder!2!BillingAddressZip],
        NULL AS [LienHolder!2!BillingPhone],
        NULL AS [LienHolder!2!BillingFax],
        NULL AS [LienHolder!2!ParentLienHolderIDFound],

        -- TL information
        NULL AS [TL!3!PayoffAmount],
        NULL AS [TL!3!SettlementAmount],
        NULL AS [TL!3!AdvanceAmount],
        NULL AS [TL!3!LetterofGauranteeAmount],
        NULL AS [TL!3!PayoffAmountConfirmedFlag],
        NULL AS [TL!3!PayoffAmountConfirmedDate],
        NULL AS [TL!3!PayoffExpirationDate],
        NULL AS [TL!3!SalvageControlNumber],
        NULL AS [TL!3!LienHolderAccountNumber],
        
        -- Salvage Vendor
        NULL AS [Salvage!4!SalvageVendorID],
        NULL AS [Salvage!4!Address1],
        NULL AS [Salvage!4!Address2],
        NULL AS [Salvage!4!AddressCity],
        NULL AS [Salvage!4!AddressState],
        NULL AS [Salvage!4!AddressZip],
        NULL AS [Salvage!4!ContactName],
        NULL AS [Salvage!4!EmailAddress],
        NULL AS [Salvage!4!Fax],
        NULL AS [Salvage!4!Phone],
        NULL AS [Salvage!4!MailingAddress1],
        NULL AS [Salvage!4!MailingAddress2],
        NULL AS [Salvage!4!MailingAddressCity],
        NULL AS [Salvage!4!MailingAddressState],
        NULL AS [Salvage!4!MailingAddressZip],
        NULL AS [Salvage!4!Name],
        NULL AS [Salvage!4!OfficeName],
        NULL AS [Salvage!4!WebSiteAddress],
        
        -- Title
        NULL AS [Title!5!TitleName],
        NULL AS [Title!5!TitleNameConfirmDate],
        NULL AS [Title!5!TitleNameConfirmFlag],
        NULL AS [Title!5!TitleState],
        NULL AS [Title!5!TitleStatus],
        
        -- Documents
        NULL AS [Document!6!DocumentID],
        NULL AS [Document!6!DocumentTypeID],
        NULL AS [Document!6!Name],
        NULL AS [Document!6!CreatedDate]

        
    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL, NULL, NULL,
            
            --Lien Holder
            lh.LienHolderID,
            isNull(convert(varchar, lh.ParentLienHolderID), ''),
            lh.Name, 
            isNull(lh.Address1,  ''),
            isNull(lh.Address2,  ''),
            isNull(lh.AddressCity,  ''),
            isNull(lh.AddressState,  ''),
            isNull(lh.AddressZip,  ''),
            isNull(lh.PhoneAreaCode + lh.PhoneExchangeNumber + lh.PhoneUnitNumber, ''),
            isNull(lh.FaxAreaCode + lh.FaxExchangeNumber + lh.FaxUnitNumber, ''),
            isNull(lh.OfficeName, ''),
            isNull(lh.ContactName, ''),
            isNull(lh.EmailAddress, ''),
            isNull(lh.FedTaxId, ''),
            isNull(lh.BusinessTypeCD, ''),
            isNull(b.EFTAccountNumber, ''),
            isNull(b.EFTAccountTypeCD, ''),
            isNull(b.EFTContractSignedFlag,0),
            isNull(convert(varchar, b.EFTEffectiveDate, 101), ''),
            isNull(b.EFTRoutingNumber, ''),
            isNull(b.Name, ''),
            isNull(b.Address1, ''),
            isNull(b.Address2, ''),
            isNull(b.AddressCity, ''),
            isNull(b.AddressState, ''),
            isNull(b.AddressZip, ''),
            isNull(b.PhoneAreaCode + b.PhoneExchangeNumber + b.PhoneUnitNumber, ''),
            isNull(b.FaxAreaCode + b.FaxExchangeNumber + b.FaxUnitNumber, ''),
            isNull(case
                     when FedTaxID is not null then (SELECT TOP 1 convert(varchar, LienHolderID)
                                                       FROM dbo.utb_lien_holder lhp
                                                       WHERE lhp.FedTaxId = lh.FedTaxId
                                                         AND ParentLienHolderID IS NULL
                                                         AND lhp.LienHolderID <> lh.LienHolderID)
                     else ''
                   end, ''),

            -- TL information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Salvage Vendor
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Title
            NULL, NULL, NULL, NULL, NULL,
        
            -- Documents
            NULL, NULL, NULL, NULL
            
    FROM dbo.utb_lien_holder lh
    LEFT OUTER JOIN dbo.utb_billing b on lh.BillingID = b.BillingID
    WHERE LienHolderID = @LienHolderID

    UNION ALL
    
    SELECT  3,
            1,
            -- Root
            NULL, NULL, NULL,

            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- TL information
            isNull(convert(varchar, convert(money, PayoffAmount)), ''), 
            isNull(convert(varchar, convert(money, SettlementAmount)), ''), 
            isNull(convert(varchar, convert(money, AdvanceAmount)), ''), 
            isNull(convert(varchar, convert(money, LetterofGuaranteeAmount)), ''),
            isNull(PayoffAmountConfirmedFlag, 0),
            isNull(convert(varchar, PayoffAmountConfirmedDate, 101) + ' ' + convert(varchar, PayoffAmountConfirmedDate, 108), ''),
            convert(varchar, PayoffExpirationDate, 101),
            isNull(SalvageControlNumber, ''),
            isNull(LeinHolderAccountNumber, ''),
            
            -- Salvage Vendor
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Title
            NULL, NULL, NULL, NULL, NULL,
        
            -- Documents
            NULL, NULL, NULL, NULL
            
    FROM dbo.utb_claim_aspect_service_channel
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID


    UNION ALL
    
    SELECT  4,
            1,
            -- Root
            NULL, NULL, NULL,

            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- TL information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Salvage Vendor
            sv.SalvageVendorID,
            isNull(Address1, ''),
            isNull(Address2, ''),
            isNull(AddressCity, ''),
            isNull(AddressState, ''),
            isNull(AddressZip, ''),
            isNull(ContactName, ''),
            isNull(EmailAddress, ''),
            isNull(FaxAreaCode + FaxExchangeNumber + FaxUnitNumber, ''),
            isNull(PhoneAreaCode + PhoneExchangeNumber + PhoneUnitNumber + PhoneExtensionNumber, ''),
            isNull(MailingAddress1, ''),
            isNull(MailingAddress2, ''),
            isNull(MailingAddressCity, ''),
            isNull(MailingAddressState, ''),
            isNull(MailingAddressZip, ''),
            isNull(Name, ''),
            isNull(OfficeName, ''),
            isNull(WebSiteAddress, ''),
            
            -- Title
            NULL, NULL, NULL, NULL, NULL,
        
            -- Documents
            NULL, NULL, NULL, NULL
            
    FROM dbo.utb_salvage_vendor sv
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON sv.SalvageVendorID = casc.SalvageVendorID
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID


    UNION ALL
    
    SELECT  5,
            1,
            -- Root
            NULL, NULL, NULL,

            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- TL information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Salvage Vendor
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Title
            TitleName, 
            convert(varchar, TitleNameConfirmDate, 101) + ' ' + convert(varchar, TitleNameConfirmDate, 108), 
            TitleNameConfirmFlag, 
            TitleState, 
            TitleStatus,
        
            -- Documents
            NULL, NULL, NULL, NULL
            
    FROM dbo.utb_claim_vehicle
    WHERE ClaimAspectID = @ClaimAspectID
    
    UNION ALL
    
    SELECT  6,
            1,
            -- Root
            NULL, NULL, NULL,

            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,

            -- TL information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Salvage Vendor
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Title
            NULL, NULL, NULL, NULL, NULL,
        
            -- Documents
            d.DocumentID, 
            d.DocumentTypeID, 
            dt.Name, 
            convert(varchar, d.CreatedDate, 101) --+ ' ' + convert(varchar, d.CreatedDate, 108)
            
    FROM dbo.utb_claim_aspect_service_channel_document cascd
    LEFT JOIN dbo.utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      AND d.EnabledFlag = 1
      AND dt.Name in ('Title',
                      'Vehicle Owner POA',
                      'Title Copy',
                      'Letter of Guarantee',
                      'Lien Holder POA')
    
    
    ORDER BY TAG

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLGetDetailXML TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/