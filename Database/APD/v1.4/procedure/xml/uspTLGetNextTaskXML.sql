-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLGetNextTaskXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLGetNextTaskXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLGetNextTaskXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the next available TL Task
*
* PARAMETERS:  
* (I) @UserID                        User id
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLGetNextTaskXML
    @UserID                 udt_std_id,
    @ChecklistID            udt_std_id_big = NULL
AS
BEGIN

    Set NOCOUNT ON
    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    DECLARE @TaskCreatedDate as datetime
    --DECLARE @CheckListID as bigint
    DECLARE @LockExpirationMinutes as int
    DECLARE @ClaimAspectServiceChannelID as bigint
    DECLARE @LienHolderID as bigint
    DECLARE @InvolvedIDVehOwner as bigint
    DECLARE @ClaimAspectID as bigint
    DECLARE @LynxRepName as varchar(100)
    DECLARE @LastCalledOn as datetime
    DECLARE @TaskID as int
    DECLARE @LockExpirationDt as datetime

    DECLARE @TaskIDLHInitialContact as int
    DECLARE @TaskIDLHLoGFollowup as int
    DECLARE @TaskIDLHPOFollowup as int
    DECLARE @TaskIDVOInitialContact as int
    DECLARE @TaskIDVOFollowup as int
    
    DECLARE @tmpTLQueue TABLE (
       id                   bigint          NOT NULL,
       LynxID               bigint          NOT NULL,
       ClaimAspectNumber    int             NOT NULL,
       TaskID               int             NOT NULL,
       TaskName             varchar(100)    NULL,
       LHName               varchar(100)    NULL,
       VOName               varchar(100)    NULL,
       DueDate              varchar(25)     NULL,
       Locked               varchar(1)      NOT NULL,
       LockedUserID			bigint			NULL)

     
    SET @ProcName = 'uspTLGetNextTaskXML'
    SET @now = CURRENT_TIMESTAMP
    SET @LockExpirationMinutes = 30
    SET @TaskIDLHInitialContact = 94
    SET @TaskIDLHPOFollowup = 98
    SET @TaskIDLHLoGFollowup = 97
    SET @TaskIDVOInitialContact = 93
    SET @TaskIDVOFollowup = 96
    
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END

    SELECT @LynxRepName = NameFirst + ' ' + NameLast
    FROM dbo.utb_user
    WHERE UserID = @UserID
    
    -- Delete Expired Tasks - all users
    DELETE FROM dbo.utb_checklist_lock
    WHERE DATEDIFF(minute, ExpirationDate, @now ) > 0
    
    INSERT INTO @tmpTLQueue
    SELECT id,
           LynxID,
           ClaimAspectNumber,
           TaskID,
           TaskName,
           LHName,
           VOName,
           DueDate,
           Locked,
           LockedUserID
    FROM ufnTLQueueTasksGet(@now)


    -- delete all tasks that are locked by this user
    DELETE FROM dbo.utb_checklist_lock
    WHERE UserID = @UserID
      AND DATEDIFF(minute, ExpirationDate, @now ) >= (-1 * @LockExpirationMinutes)
      
    IF @ChecklistID IS NOT NULL AND
       EXISTS(SELECT CheckListID
               FROM utb_checklist_lock
               Where ChecklistID = @ChecklistID
                 AND UserID <> @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid Task or Task has been locked.', 16, 1, @ProcName)
        RETURN

    END
    
    -- Lock the first available task
    BEGIN TRANSACTION
    
    SET @LockExpirationDt = DATEADD(minute, @LockExpirationMinutes, @now )
    
    IF @ChecklistID IS NULL OR
       NOT EXISTS(SELECT ChecklistID
                  FROM utb_checklist
                  WHERE ChecklistID = @ChecklistID)
    BEGIN
    
       INSERT INTO dbo.utb_checklist_lock
       SELECT TOP 1 id,
                    @UserID,
                    @now,
                    @LockExpirationDt
       FROM @tmpTLQueue
       WHERE Locked = 0
         AND id NOT IN (SELECT CheckListID
                        FROM dbo.utb_checklist_lock)
    
    END
    ELSE
    BEGIN
      DELETE FROM dbo.utb_checklist_lock
      WHERE ChecklistID = @ChecklistID
      
      INSERT INTO dbo.utb_checklist_lock
      SELECT @ChecklistID,
             @UserID,
             @now,
             @LockExpirationDt      
    END
    
    COMMIT TRANSACTION
    
    SELECT @TaskCreatedDate = cl.CreatedDate,
           @CheckListID = cl.CheckListID,
           @ClaimAspectServiceChannelID = cl.ClaimAspectServiceChannelID,
           @TaskID = cl.TaskID
    FROM dbo.utb_checklist cl
    LEFT JOIN dbo.utb_checklist_lock cll ON cl.CheckListID = cll.CheckListID
    WHERE cll.UserID = @UserID
      AND cll.ExpirationDate = @LockExpirationDt
--    ORDER BY ExpirationDate ASC
    
    UPDATE dbo.utb_checklist_lock
    SET CreatedDate = @TaskCreatedDate
    WHERE UserID = @UserID
      AND CheckListID = @CheckListID
    

    UPDATE @tmpTLQueue
    SET Locked = 1
    WHERE id = @CheckListID
    
    IF @CheckListID > 0
    BEGIN
    
       SELECT @LienHolderID = LienHolderID,
              @ClaimAspectID = ClaimAspectID
       FROM dbo.utb_claim_aspect_service_channel
       WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       
       SELECT TOP 1 @InvolvedIDVehOwner = i.InvolvedID
       FROM  dbo.utb_claim_aspect_involved cai
       LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
       LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
       LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
       WHERE cai.ClaimAspectID = @ClaimAspectID
         AND cai.EnabledFlag = 1
         AND irt.Name = 'Owner' 
        
       SELECT @LastCalledOn = CASE 
							     WHEN @TaskID = @TaskIDVOFollowup AND VORecentContactDate IS NULL THEN VOInitialContactDate
							     WHEN @TaskID = @TaskIDVOFollowup AND VORecentContactDate IS NOT NULL THEN VORecentContactDate
							     WHEN @TaskID = @TaskIDLHPOFollowup AND LHRecentContactDate IS NULL THEN LHInitialContactDate
							     WHEN @TaskID = @TaskIDLHPOFollowup AND LHRecentContactDate IS NOT NULL THEN LHRecentContactDate
							     WHEN @TaskID = @TaskIDLHLoGFollowup AND LHRecentContactDate IS NULL THEN LHInitialContactDate
							     WHEN @TaskID = @TaskIDLHLoGFollowup AND LHRecentContactDate IS NOT NULL THEN LHRecentContactDate
						      END
       FROM dbo.utb_total_loss_metrics
       WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    END
    
    -- Get data fresh so we can verify if the current user is the one locked the current task
    DELETE FROM @tmpTLQueue
    
    INSERT INTO @tmpTLQueue
    SELECT id,
           LynxID,
           ClaimAspectNumber,
           TaskID,
           TaskName,
           LHName,
           VOName,
           DueDate,
           Locked,
           LockedUserID
    FROM ufnTLQueueTasksGet(@now)
    
    
    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        @UserID AS [Root!1!UserID],
        @LynxRepName AS [Root!1!LynxRepName],
        -- Checklist
        NULL AS [Checklist!2!ChecklistID],
        NULL AS [Checklist!2!LynxID],
        NULL AS [Checklist!2!ClaimAspectNumber],
        NULL AS [Checklist!2!TaskID],
        NULL AS [Checklist!2!TaskName],
        NULL AS [Checklist!2!lastCalledOn],
        NULL AS [Checklist!2!ClaimOwnerName],
        NULL AS [Checklist!2!ClaimOwnerPhone],
        NULL AS [Checklist!2!ClaimOwnerEmail],
        --Lien Holder
        NULL AS [LienHolder!3!Name],
        NULL AS [LienHolder!3!Address1],
        NULL AS [LienHolder!3!Address2],
        NULL AS [LienHolder!3!AddressCity],
        NULL AS [LienHolder!3!AddressState],
        NULL AS [LienHolder!3!AddressZip],
        NULL AS [LienHolder!3!Phone],
        NULL AS [LienHolder!3!FedTaxID],
        NULL AS [LienHolder!3!EmailAddress],
        NULL AS [LienHolder!3!EFTAccountNumber],
        NULL AS [LienHolder!3!EFTAccountTypeCD],
        NULL AS [LienHolder!3!EFTContractSignedFlag],
        NULL AS [LienHolder!3!EFTEffectiveDate],
        NULL AS [LienHolder!3!EFTRoutingNumber],
        NULL AS [LienHolder!3!BillingName],
        NULL AS [LienHolder!3!BillingAddress1],
        NULL AS [LienHolder!3!BillingAddress2],
        NULL AS [LienHolder!3!BillingAddressCity],
        NULL AS [LienHolder!3!BillingAddressState],
        NULL AS [LienHolder!3!BillingAddressZip],
        -- Vehicle Owner
        NULL AS [VehicleOwner!4!NameFirst],
        NULL AS [VehicleOwner!4!NameLast],
        NULL AS [VehicleOwner!4!NameBusiness],
        NULL AS [VehicleOwner!4!Address1],
        NULL AS [VehicleOwner!4!Address2],
        NULL AS [VehicleOwner!4!AddressCity],
        NULL AS [VehicleOwner!4!AddressState],
        NULL AS [VehicleOwner!4!AddressZip],
        NULL AS [VehicleOwner!4!Phone],
        NULL AS [VehicleOwner!4!AltPhone],
        NULL AS [VehicleOwner!4!EmailAddress],
        -- Vehicle
        NULL AS [Vehicle!5!Year],
        NULL AS [Vehicle!5!Make],
        NULL AS [Vehicle!5!Model],
        NULL AS [Vehicle!5!Vin],
        -- TL information
        NULL AS [TL!6!PayoffAmount],
        NULL AS [TL!6!SettlementAmount],
        NULL AS [TL!6!AdvanceAmount],
        NULL AS [TL!6!LetterofGauranteeAmount],
        NULL AS [TL!6!LienHolderAccountNumber],
        -- Insurance Information
        NULL AS [Insurance!7!ID],
        NULL AS [Insurance!7!Name],
        -- Document
        NULL AS [Document!8!DocumentID],
        NULL AS [Document!8!DocumentTypeID],
        NULL AS [Document!8!DocumentName],
        NULL AS [Document!8!CreatedDate],
        -- TL Queue
        NULL AS [TLQueue!9!ID],
        NULL AS [TLQueue!9!LynxID],
        NULL AS [TLQueue!9!TaskID],
        NULL AS [TLQueue!9!TaskName],
        NULL AS [TLQueue!9!LHName],
        NULL AS [TLQueue!9!VOName],
        NULL AS [TLQueue!9!DueDate],
        NULL AS [TLQueue!9!Locked],
        NULL AS [TLQueue!9!LockedUserID]
        
    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL, NULL,
            -- Checklist
            tmp.id,
            tmp.LynxID,
            tmp.ClaimAspectNumber,
            tmp.TaskID, 
            tmp.TaskName,
            isNull(convert(varchar, @LastCalledOn, 101), ''),
            isNull(u.NameFirst + ' ' + u.NameLast, ''),
            isNull(u.PhoneAreaCode + u.PhoneExchangeNumber + u.PhoneUnitNumber, ''),
            isNull(u.EmailAddress, ''),
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM @tmpTLQueue tmp
    LEFT JOIN utb_claim_aspect ca ON (tmp.LynxID = ca.LynxID and tmp.ClaimAspectNumber = ca.ClaimAspectNumber)
    LEFT JOIN utb_user u on ca.OwnerUserID = u.UserID
    WHERE id = @CheckListID
        
    UNION ALL
    
    SELECT  3,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            lh.Name, 
            isNull(lh.Address1,  ''),
            isNull(lh.Address2,  ''),
            isNull(lh.AddressCity,  ''),
            isNull(lh.AddressState,  ''),
            isNull(lh.AddressZip,  ''),
            isNull(lh.PhoneAreaCode + lh.PhoneExchangeNumber + lh.PhoneUnitNumber, ''),
            isNull(lh.FedTaxId, ''),
            isNull(lh.EmailAddress, ''),
            isNull(b.EFTAccountNumber, ''),
            isNull(b.EFTAccountTypeCD, ''),
            b.EFTContractSignedFlag,
            b.EFTEffectiveDate,
            isNull(b.EFTRoutingNumber, ''),
            isNull(b.Name, ''),
            isNull(b.Address1, ''),
            isNull(b.Address2, ''),
            isNull(b.AddressCity, ''),
            isNull(b.AddressState, ''),
            isNull(b.AddressZip, ''),
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM dbo.utb_lien_holder lh
    LEFT OUTER JOIN dbo.utb_billing b on lh.BillingID = b.BillingID
    WHERE LienHolderID = @LienHolderID
        
    UNION ALL
    
    SELECT  4,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            isNull(NameFirst,'') ,
            isNull(NameLast, ''),
            isNull(BusinessName, ''), 
            isNull(Address1, ''),
            isNull(Address2, ''),
            isNull(AddressCity, ''),
            isNull(AddressState, ''),
            isNull(AddressZip, ''),
            CASE 
				   WHEN BestContactPhoneCD = 'N' then NightAreaCode + NightExchangeNumber + NightUnitNumber
				   WHEN BestContactPhoneCD = 'A' then AlternateAreaCode + AlternateExchangeNumber + AlternateUnitNumber
				   ELSE DayAreaCode + DayExchangeNumber + DayUnitNumber
            END,
            CASE 
				   WHEN BestContactPhoneCD = 'N' AND DayAreaCode IS NOT NULL then DayAreaCode + DayExchangeNumber + DayUnitNumber
				   WHEN BestContactPhoneCD = 'N' AND AlternateAreaCode IS NOT NULL then AlternateAreaCode + AlternateExchangeNumber + AlternateUnitNumber
				   WHEN BestContactPhoneCD = 'A' AND DayAreaCode IS NOT NULL then DayAreaCode + DayExchangeNumber + DayUnitNumber
				   WHEN BestContactPhoneCD = 'A' AND NightAreaCode IS NOT NULL then NightAreaCode + NightExchangeNumber + NightUnitNumber
				   WHEN BestContactPhoneCD = 'D' AND AlternateAreaCode IS NOT NULL then AlternateAreaCode + AlternateExchangeNumber + AlternateUnitNumber
				   WHEN BestContactPhoneCD = 'D' AND NightAreaCode IS NOT NULL then NightAreaCode + NightExchangeNumber + NightUnitNumber
				   ELSE ''
            END,
            isNull(EmailAddress, ''),
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM dbo.utb_involved
    WHERE InvolvedID = @InvolvedIDVehOwner
    
    UNION ALL
    
    SELECT  5,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            VehicleYear, 
            Make, 
            Model, 
            VIN,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM dbo.utb_claim_vehicle
    WHERE ClaimAspectID = @ClaimAspectID

    UNION ALL
    
    SELECT  6,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            isNull(convert(varchar, convert(money, PayoffAmount)), ''), 
            isNull(convert(varchar, convert(money, SettlementAmount)), ''), 
            isNull(convert(varchar, convert(money, AdvanceAmount)), ''), 
            isNull(convert(varchar, convert(money, LetterofGuaranteeAmount)), ''),
            isNull(LeinHolderAccountNumber, ''),
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM dbo.utb_claim_aspect_service_channel
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    UNION ALL
    
    SELECT  7,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            i.InsuranceCompanyID, 
            i.Name,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM @tmpTLQueue tmp
    LEFT JOIN utb_claim c ON tmp.LynxID = c.LynxID
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE id = @CheckListID

    UNION ALL
    
    SELECT  8,
            2,
            -- Root
            NULL, NULL,
            -- Checklist
            @CheckListID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            d.DocumentID, 
            d.DocumentTypeID, 
            dt.Name, 
            convert(varchar, d.CreatedDate, 101),
            -- TL Queue
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM @tmpTLQueue tmp
    LEFT JOIN dbo.utb_checklist cl ON tmp.id = cl.checklistID
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON cl.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE tmp.id = @CheckListID
      AND dt.Name in ('Vehicle Owner Title',
                      'Vehicle Owner POA',
                      'Lien Title Copy',
                      'Letter of Guarantee',
                      'Lien Holder POA',
                      'Lien Holder Clear Title')

    UNION ALL
    
    SELECT  9,
            1,
            -- Root
            NULL, NULL,
            -- Checklist
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            --Lien Holder
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
            -- Vehicle Owner
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Vehicle
            NULL, NULL, NULL, NULL,
            -- TL information
            NULL, NULL, NULL, NULL, NULL,
            -- Insurance Information
            NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL,
            -- TL Queue
            id,
            tmp.LynxID,
            TaskID,
            TaskName,
            LHName,
            VOName,
            DueDate,
            Locked,
            LockedUserID
    FROM @tmpTLQueue tmp
    
    ORDER BY TAG

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLGetNextTaskXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLGetNextTaskXML TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/