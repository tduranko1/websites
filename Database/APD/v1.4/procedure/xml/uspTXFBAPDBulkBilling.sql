USE [udb_apd]
GO

BEGIN TRANSACTION
-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTXFBAPDBulkBilling' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTXFBAPDBulkBilling 
END

GO 
/****** Object:  StoredProcedure [dbo].[uspTXFBAPDBulkBilling]    Script Date: 01/09/2019 20:52:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
/************************************************************************************************************************  
*  
* PROCEDURE:    [uspTXFBAPDBulkBilling]  
* SYSTEM:       Lynx Services APD  
* AUTHOR:       [your name here]  
* FUNCTION:     [procedure description here]  
*  
* PARAMETERS:    
* (I) @input                description  
* (O) @output               description  
* (I) @ModifiedDateTime     The "previous" updated date  
*  
* RESULT SET:  
* [result set details here]  
*  
*  
* VSS  
* $Workfile: $  
* $Archive:  $  
* $Revision: $  
* $Author:   $  
* $Date:     $  
*  
************************************************************************************************************************/  
  
-- Create the stored procedure  
  
  
CREATE PROCEDURE [dbo].[uspTXFBAPDBulkBilling]  
    @ClientID       udt_std_id,  
    @StatementEndDate     udt_std_datetime,    
    @StatementStartDate     udt_std_datetime       
AS  
BEGIN  
  
 declare @CreateDate   udt_std_datetime  
 declare @BillingSessionID udt_std_name  
   
 set @BillingSessionID = newid()  
 set @CreateDate = current_timestamp  
   
 declare @tmpRecords table   
 (  
  InvoiceServiceID int identity(1,1),  
  InvoiceID   bigint,  
  ClaimantName  varchar(50),  
  LynxID    varchar(20)  
 )  
   
 declare @error int  
 declare @rowcount int  
   
 declare @procname varchar(30)  
 set @procname = 'uspTXFBAPDBulkBilling'  
   
   
 -- validate @ClientID  
 if not exists (select InsuranceCompanyID from dbo.utb_insurance where InsuranceCompanyID = @ClientID)  
 begin  
  return -1  
 end  
   
 -- validate @StatementEndDate  
 if @StatementEndDate is not null  
 begin  
  if IsDate(@StatementEndDate) = 0  
  begin  
   return -1  
  end  
  else  
  begin     
   set @StatementEndDate = convert(varchar(30),convert(datetime,@StatementEndDate),101) + ' 11:59:59 PM'     
  end  
 end  
 else  
 begin  
  return -1  
 end  
   
 begin transaction  
   
 insert into @tmpRecords  
 select i.InvoiceID, invs.ClaimantName, invs.UId  
 from dbo.utb_invoice i left join utb_invoice_service invs on (i.InvoiceID = invs.InvoiceID)  
                        inner join utb_claim_aspect ca on (i.claimAspectID = ca.ClaimAspectID)  
                        inner join utb_claim c on (ca.LynxID = c.LynxID)  
                        inner join utb_claim_aspect_service_channel casc on i.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
                        inner join utb_client_service_channel csc on ((csc.InsuranceCompanyID = c.InsuranceCompanyID) and  
                                                                      (csc.ServiceChannelCD = casc.ServiceChannelCD))  
 where i.EnabledFlag = 1  
   and i.ItemTypeCD = 'F'  
   --and i.StatusCD not in ('FS','NB')   
   and i.EntryDate between @StatementStartDate and @StatementEndDate  
   and c.InsuranceCompanyID = @ClientID  
   and csc.InvoicingModelBillingCD = 'B'      
     
 if @@error <> 0  
 begin    
  rollback transaction  
  return -1  
 end                                                                                           
   
 insert into utb_temp_invoice_prep  
 select @BillingSessionID,  
     tmp.InvoiceID,  
     i.Amount,  
     i.EntryDate,  
     i.CarrierRepName,  
     tmp.ClaimantName,  
     c.ClientClaimNumber ClaimNumber,  
     ins.InsuranceCompanyID ClientID,  
     ins.Name ClientName,  
     @CreateDate CreateDate,  
     CASE WHEN ca.InitialAssignmentTypeID = 17 then 
		(i.InvoiceDescription + ' - ' + convert(varchar,i.InvoiceID)) 
	 ELSE 
		i.InvoiceDescription
	 END [Description], 
     i.InsuredName,  
     c.LossDate,  
     sc.StateValue LossState,  
     c.LossState LossStateCD,  
     tmp.LynxID,  
     c.PolicyNumber,  
     o.OfficeID,  
     case when ins.InsuranceCompanyID = 515 then  
     case when o.ClientOfficeID = 'Zipcar' then 'ZipCar'  
          when c.ClientClaimNumber is null then 'Avis Budget'  
          when len(c.ClientClaimNumber) < 4 then 'Avis Budget'  
          when upper(substring(c.ClientClaimNumber,3,1)) = 'P' then 'Payless'  
          else 'Avis Budget' end       
    else o.Name end OfficeName       
 from (select min(InvoiceServiceID) as MinInvoiceServiceID from @tmpRecords group by InvoiceID) mtmp  
   inner join @tmpRecords tmp on (mtmp.MinInvoiceServiceID = tmp.InvoiceServiceID)  
   inner join dbo.utb_invoice i on (tmp.InvoiceID = i.InvoiceID)  
   left join dbo.utb_claim_aspect ca on (i.ClaimAspectID = ca.ClaimAspectID)  
   left join dbo.utb_claim c on (ca.LynxID = c.LynxID)  
   left join dbo.utb_insurance ins on (c.InsuranceCompanyID = ins.InsuranceCompanyID)  
   left join dbo.utb_state_code sc on (c.LossState = sc.StateCode)  
   left join dbo.utb_user u on (c.CarrierRepUserID = u.UserID)  
   left join dbo.utb_office o on (u.OfficeID = o.OfficeID)  
  
  
 if @@error <> 0  
 begin  
  print 'temp insert'  
  rollback transaction  
  return -1  
 end  
  
 commit  
  
 -- Now return overall info  
 select BillingSessionID,  
     sum(Amount) BillAmount  
 from utb_temp_invoice_prep  
 where BillingSessionID = @BillingSessionID  
 group by BillingSessionID  
  
end  
  
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTXFBAPDBulkBilling' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTXFBAPDBulkBilling TO 
        wsAPDUser,ugr_lynxapd
         

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/




