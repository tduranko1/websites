-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDiaryGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDiaryGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDiaryGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves claim diary information on a specific checklist id
*
* PARAMETERS:
* (I) @ChecklistID          The ChecklistID to retrieve
* (I) @LynxID               The LynxID the checklist item belongs to
* (I) @UserID               To compile CRUD for each task
*
* RESULT SET:
* A universal recordset containing diary task information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspDiaryGetDetailWSXML]
    @ChecklistID    udt_std_int_big,
    @LynxID         udt_std_int_big,
    @UserID         udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @now               AS datetime
    --Project:210474 APD Added the declare below when we did the code merge M.A.20061116
    DECLARE @hasSupervisor     AS bit
    DECLARE @InsuranceCompanyID as int

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    --Project:210474 APD Added the declare below when we did the code merge M.A.20061215
    DECLARE @NoteTypeDefaultID AS udt_std_int_tiny    -- Used to determine default note type, if any

    SET @ProcName = 'uspDiaryGetDetailWSXML'


    -- Check to make sure a valid Checklist ID, LynxID, and User ID was passed in

    IF  (@ChecklistID IS NULL) OR
        ((NOT @ChecklistID = 0) AND NOT EXISTS(SELECT ChecklistID FROM dbo.utb_checklist WHERE ChecklistID = @ChecklistID))
    BEGIN
        -- Invalid Checklist ID

        RAISERROR('101|%s|@ChecklistID|%u', 16, 1, @ProcName, @ChecklistID)
        RETURN
    END

    IF  (@LynxID IS NULL) OR
        ((NOT @LynxID = 0) AND NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID

        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    SELECT @InsuranceCompanyID = InsuranceCompanyID
    FROM utb_claim
    WHERE LynxID = @LynxID


    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all entities and store in the temporary table

    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'DiaryTask',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_checklist' AND Column_Name IN
            ('AlarmDate',
             'UserTaskDescription'))
    --Project:210474 APD Added the code below when we did the code merge M.A.20061116
    UNION ALL

    SELECT  'DiaryTask',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN
            ('Note'))
    --Project:210474 APD Added the code above when we did the code merge M.A.20061116

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Declare temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName                        varchar(50) NOT NULL,
        DisplayOrder                    int         NULL,
        ReferenceID                     varchar(10) NOT NULL,
        Name                            varchar(50) NOT NULL,
        ClaimAspectID                   bigint      NULL,       -- Used to connect task to claim item
        ClaimAspectServiceChannelID     bigint      NULL,       -- Used to connect task to service channel
        EscalationDate                  varchar(30) NULL,
        AlarmModificationAllowedFlag    tinyint     NULL,
        PertainsTo                      varchar(8)  NULL,
        --Project:210474 APD Added the additional columns below when we did the code merge M.A.20061116
        DefaultAssignmentPoolID         int         NULL,
        DefaultAssignmentPoolFunctionCD varchar(5)  NULL,
        FOhasSupervisor                 bit         NULL,
        FAhasSupervisor                 bit         NULL,
        FShasSupervisor                 bit         NULL
    )


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    -- Get the default note type ID that is associated with this task.  Can be NULL.

    SELECT @NoteTypeDefaultID = t.NoteTypeDefaultID FROM dbo.utb_checklist c
        INNER JOIN dbo.utb_task t ON t.TaskID = c.TaskID
        WHERE c.CheckListID = @CheckListID

    -- Select All reference information for all pertinent reference tables and store in the
    -- temporary table

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, ClaimAspectID,
        ClaimAspectServiceChannelID, EscalationDate, AlarmModificationAllowedFlag, PertainsTo,
        DefaultAssignmentPoolID, DefaultAssignmentPoolFunctionCD, FOhasSupervisor, FAhasSupervisor, FShasSupervisor)

    SELECT  'TaskList' AS ListName,
            t.DisplayOrder AS DisplayOrder,
            t.TaskID,
            t.Name,
            ca.ClaimAspectID,     --Project:210474 APD Added the column when we did the code merge M.A.20061116
            casc.ClaimAspectServiceChannelID, -- Used to connect task to service channel
            --Project:210474 APD Remarked-off the code below when we did the code merge M.A.20061116
            /*
            (SELECT  ClaimAspectID
               FROM  dbo.utb_claim_aspect
               WHERE LynxID = @LynxID
                 AND ClaimAspectTypeID = ca.ClaimAspectTypeID
                 AND ClaimAspectNumber = ca.ClaimAspectNumber),
        */
            --Project:210474 APD Remarked-off the code above when we did the code merge M.A.20061116
            CASE
                --WHEN t.EscalationMinutes IS NOT NULL THEN IsNull(dbo.ufnUtilityGetDateString( DateAdd (minute, t.EscalationMinutes, @now) ), '')
                WHEN dbo.ufnTaskGetEscalationMinutes(t.taskID, @InsuranceCompanyID, casc.ServiceChannelCD) IS NOT NULL THEN IsNull(dbo.ufnUtilityGetDateString( dbo.ufnTaskGetEscalationDate(t.taskID, @InsuranceCompanyID, casc.ServiceChannelCD, @now)), '')
                ELSE ''
            END,
            IsNull((SELECT TOP 1 tr.AlarmModificationAllowedFlag
                    FROM   dbo.utb_task_role tr
                    WHERE  tr.TaskID = st.TaskID
                      AND    tr.RoleID IN  (SELECT RoleID FROM   dbo.utb_user_role WHERE  UserID = @UserID)
                    ORDER BY tr.AlarmModificationAllowedFlag DESC)  , 0),
            NULL,
            --Project:210474 APD Added the code below when we did the code merge M.A.20061116
            CASE
                WHEN (SELECT COUNT(*) 
                        FROM dbo.utb_task_assignment_pool tap
                        WHERE tap.taskID = t.TaskID
                          AND tap.ServiceChannelCD = casc.ServiceChannelCD
                          AND tap.InsuranceCompanyID = @InsuranceCompanyID) > 0 THEN isNull((SELECT convert(varchar, AssignmentPoolID)
                                                                                               FROM dbo.utb_task_assignment_pool tap
                                                                                               WHERE tap.taskID = t.TaskID
                                                                                                 AND tap.ServiceChannelCD = casc.ServiceChannelCD
                                                                                                 AND tap.InsuranceCompanyID = @InsuranceCompanyID), '')
                ELSE isNull((SELECT convert(varchar, AssignmentPoolID)
                               FROM dbo.utb_task_assignment_pool tap
                               WHERE tap.taskID = t.TaskID
                                 AND tap.ServiceChannelCD = casc.ServiceChannelCD
                                 AND tap.InsuranceCompanyID = 0), '')
            END,                                                                                                   
            CASE
                WHEN (SELECT COUNT(*) 
                        FROM dbo.utb_task_assignment_pool tap
                        WHERE tap.taskID = t.TaskID
                          AND tap.ServiceChannelCD = casc.ServiceChannelCD
                          AND tap.InsuranceCompanyID = @InsuranceCompanyID) > 0 THEN isNull((SELECT ap.FunctionCD
                                                                                               FROM dbo.utb_task_assignment_pool tap
                                                                                               LEFT JOIN dbo.utb_assignment_pool ap ON (tap.AssignmentPoolID = ap.AssignmentPoolID)
                                                                                               WHERE tap.taskID = t.TaskID
                                                                                                 AND tap.ServiceChannelCD = casc.ServiceChannelCD
                                                                                                 AND tap.InsuranceCompanyID = @InsuranceCompanyID), '')
                ELSE isNull((SELECT ap.FunctionCD
                               FROM dbo.utb_task_assignment_pool tap
                               LEFT JOIN dbo.utb_assignment_pool ap ON (tap.AssignmentPoolID = ap.AssignmentPoolID)
                               WHERE tap.taskID = t.TaskID
                                 AND tap.ServiceChannelCD = casc.ServiceChannelCD
                                 AND tap.InsuranceCompanyID = 0), '')
            END,
            NULL,
            NULL,
            --Project:210474 APD Added the code above when we did the code merge M.A.20061116
            NULL

    FROM    dbo.utb_claim_aspect ca
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:   "StatusID" is no longer part of utb_Claim_Aspect table.  Added reference
        to utb_Claim_Aspect_Status to get to "StatusID" column for an ClaimAspectID
        M.A. 20061108
    *********************************************************************************/
    INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
    --Project:210474 APD Remarked-off the following to support the schema change M.A.20061218
    --LEFT JOIN dbo.utb_assignment_type uat ON (ca.CurrentAssignmentTypeID = uat.AssignmentTypeID)
    --Project:210474 APD Added the following to support the schema change M.A.20061218
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
    on              cas.ClaimAspectID = casc.ClaimAspectID
    and             cas.ServiceChannelCD = casc.ServiceChannelCD
    and             cas.StatusTypeCD = 'SC'
    LEFT JOIN dbo.utb_status_task st ON (cas.StatusID = st.StatusID)
    LEFT JOIN dbo.utb_task t ON (st.TaskID = t.TaskID)
    WHERE   ca.LynxID = @LynxID AND
            t.EnabledFlag = 1 AND
            t.UserTaskFlag = 1 AND
            (SELECT TOP 1 tr.CreationAllowedFlag
                   FROM   dbo.utb_task_role tr
                   WHERE  tr.TaskID = st.TaskID AND
                          tr.RoleID IN
                                (SELECT RoleID
                                 FROM   dbo.utb_user_role
                                 WHERE  UserID = @UserID)
                   ORDER BY tr.CreationAllowedFlag DESC) = 1
    and     casc.ClaimAspectServiceChannelID is not null


    UNION ALL

    -- Add the pertains to reference items

    SELECT  'PertainsTo',
            NULL,
            ClaimAspectID,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            EntityCode,
            --Project:210474 APD Added the code below when we did the code merge M.A.20061116
            NULL,
            NULL,
            CASE
                WHEN (SELECT u.SupervisorUserID
                            FROM dbo.utb_claim_aspect ca
                            LEFT JOIN dbo.utb_user u ON (ca.OwnerUserID = u.UserID)
                            WHERE ca.ClaimAspectID = ufn.ClaimAspectID) > 0 THEN 1
                ELSE 0
            END,
            CASE
                WHEN (SELECT u.SupervisorUserID
                            FROM dbo.utb_claim_aspect ca
                            LEFT JOIN dbo.utb_user u ON (ca.AnalystUserID = u.UserID)
                            WHERE ca.ClaimAspectID = ufn.ClaimAspectID) > 0 THEN 1
                ELSE 0
            END,
            CASE
                WHEN (SELECT u.SupervisorUserID
                            FROM dbo.utb_claim_aspect ca
                            LEFT JOIN dbo.utb_user u ON (ca.SupportUserID = u.UserID)
                            WHERE ca.ClaimAspectID = ufn.ClaimAspectID) > 0 THEN 1
                ELSE 0
            END
            --Project:210474 APD Added the code above when we did the code merge M.A.20061116

    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0) ufn -- (0, 0) means all aspects, open or closed

    --Project:210474 APD Added the code below when we did the code merge M.A.20061116
    UNION ALL

    -- Add the service channel reference items

    SELECT  'ServiceChannel',
            NULL,
            casc.ClaimAspectServiceChannelID,
            urc.Name,
            ca.ClaimAspectID,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM utb_claim_aspect_service_channel casc
        INNER JOIN dbo.utb_claim_aspect ca
            ON ca.ClaimAspectID = casc.ClaimAspectID
        INNER JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc
            ON casc.ServiceChannelCD = urc.Code
        WHERE ca.LynxID = @LynxID

    UNION ALL

    -- Add the user pool reference items

    SELECT  'UserPool',
            NULL,
            ap.AssignmentPoolID,
            ap.FunctionCD,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM    dbo.utb_assignment_pool ap
    LEFT JOIN dbo.utb_assignment_pool_user apu ON (ap.AssignmentPoolID = apu.AssignmentPoolID)
    WHERE apu.UserID = @UserID

    UNION ALL

    --Project:210474 APD Added the following SELECT Stmnt when we did the code merge M.A.20061215
    -- Add the note type reference items
    -- If we had a default note type, then only add that type.  Otherwise add them all.

    SELECT  'NoteType', NULL,
            NoteTypeID,
            Name,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
    FROM    dbo.utb_note_type
    WHERE   ( ( ( @NoteTypeDefaultID IS NULL ) AND ( EnabledFlag = 1 ) ) OR
            ( ( @NoteTypeDefaultID IS NOT NULL ) AND ( NoteTypeID = @NoteTypeDefaultID ) ) )

    -- Set the results sort order

    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

--Project:210474 APD Added the code below when we did the code merge M.A.20061116
    SELECT @hasSupervisor = CASE
                                WHEN SuperVisorUserID > 0 THEN 1
                                ELSE 0
                            END
    FROM dbo.utb_user
    WHERE UserID = @UserID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
--Project:210474 APD Added the code above when we did the code merge M.A.20061116

    -- Begin XML Select

    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            @ChecklistID AS [Root!1!CheckListID],
            @LynxID AS [Root!1!LynxID],
            @UserID AS [Root!1!UserID],
            @hasSupervisor AS [Root!1!hasSupervisor], --Project:210474 APD Added the column when we did the code merge M.A.20061116
            -- Checklist Information
            NULL AS [DiaryTask!2!CheckListID],
            NULL AS [DiaryTask!2!AssignedUserID], --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL AS [DiaryTask!2!AssignedUserNameFirst],
            NULL AS [DiaryTask!2!AssignedUserNameLast],
            NULL AS [DiaryTask!2!CreatedUserNameFirst],
            NULL AS [DiaryTask!2!CreatedUserNameLast],
            NULL AS [DiaryTask!2!CreatedUserRoleName],
            NULL AS [DiaryTask!2!LynxID],
            NULL AS [DiaryTask!2!ClaimAspectCode],
            NULL AS [DiaryTask!2!ClaimAspectServiceChannelID],
            NULL AS [DiaryTask!2!ClaimAspectTypeName],
            NULL AS [DiaryTask!2!ClaimAspectNumber],
            NULL AS [DiaryTask!2!StatusName],
            NULL AS [DiaryTask!2!TaskName],
            NULL AS [DiaryTask!2!AlarmDate],
            NULL AS [DiaryTask!2!CreatedDate],
            NULL AS [DiaryTask!2!UserTaskDescription],
            NULL AS [DiaryTask!2!CommentCompleteRequiredFlag],
            NULL AS [DiaryTask!2!CommentNARequiredFlag],
            NULL AS [DiaryTask!2!SysLastUpdatedDate],
            -- Metadata Header
            NULL AS [Metadata!3!Entity],
            -- Columns
            NULL AS [Column!4!Name],
            NULL AS [Column!4!DataType],
            NULL AS [Column!4!MaxLength],
            NULL AS [Column!4!Precision],
            NULL AS [Column!4!Scale],
            NULL AS [Column!4!Nullable],
            -- Reference Data
            NULL AS [Reference!5!List],
            NULL AS [Reference!5!ReferenceID],
            NULL AS [Reference!5!Name],
            NULL AS [Reference!5!ClaimAspectID],
            NULL AS [Reference!5!ClaimAspectServiceChannelID],
            NULL AS [Reference!5!EscalationDate],
            NULL AS [Reference!5!AlarmModificationAllowedFlag],
            NULL AS [Reference!5!PertainsTo],
            --Project:210474 APD Added the 5 columns below when we did the code merge M.A.20061116
            NULL AS [Reference!5!DefaultAssignmentPoolID],
            NULL AS [Reference!5!DefaultAssignmentPoolFunctionCD],
            NULL AS [Reference!5!FOHasSupervisor],
            NULL AS [Reference!5!FAHasSupervisor],
            NULL AS [Reference!5!FSHasSupervisor]


    UNION ALL


    -- Select Checklist Information Level

    SELECT  2,
            1,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added a column when we did the code merge M.A.20061116
            -- Checklist Information
            IsNull(c.ChecklistID, 0),
            IsNull(au.UserID, 0),  --Project:210474 APD Added a column when we did the code merge M.A.20061116
            IsNull(au.NameFirst, ''),
            IsNull(au.NameLast, ''),
            IsNull(cu.NameFirst, ''),
            IsNull(cu.NameLast, ''),
            IsNull(cr.Name, ''),
            IsNull(ca.LynxID, 0),
            IsNull(cat.Code, ''),
            IsNull(casc.ClaimAspectServiceChannelID, ''),
            IsNull(cat.Name, ''),
            IsNull(ca.ClaimAspectNumber, 0),
            IsNull(s.Name, ''),
            IsNull(t.Name, ''),
            IsNull(c.AlarmDate, ''),
            IsNull(c.CreatedDate, ''),
            IsNull(c.UserTaskDescription, ''),
            IsNull(t.CommentCompleteRequiredFlag, ''),
            IsNull(t.CommentNARequiredFlag, ''),
            dbo.ufnUtilityGetDateString( c.SysLastUpdatedDate ),
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added columns when we did the code merge M.A.20061116
            NULL, NULL, NULL, NULL  --Project:210474 APD Added columns when we did the code merge M.A.20061116

    FROM (SELECT @ChecklistID AS ChecklistID) AS Parms
    LEFT JOIN dbo.utb_checklist c ON (parms.ChecklistID = c.ChecklistID)
    LEFT JOIN dbo.utb_user au ON (c.AssignedUserID = au.UserID)
    LEFT JOIN dbo.utb_user cu ON (c.CreatedUserID = cu.UserID)
    LEFT JOIN dbo.utb_role cr ON (c.CreatedRoleID = cr.RoleID)
    /*********************************************************************************
    Project: 210474 APD - Enhancements to support multiple concurrent service channels
    Note:   "ClaimAspectID" is not part of utb_checklist.  Added a table
        utb_Claim_Aspect_Service_Channel between utb_claim_aspect and
        utb_checklist to join them.
        M.A. 20061108
    *********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    LEFT JOIN dbo.utb_status s ON (c.StatusID = s.StatusID)
    LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 3,
            1,
            NULL, NULL, NULL,NULL,  --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Checklist Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added columns when we did the code merge M.A.20061116
            NULL, NULL, NULL, NULL   --Project:210474 APD Added columns when we did the code merge M.A.20061116

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  4,
            3,
            NULL, NULL, NULL, NULL,   --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Checklist Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   --Project:210474 APD Added columns when we did the code merge M.A.20061116
            NULL, NULL, NULL, NULL   --Project:210474 APD Added columns when we did the code merge M.A.20061116


    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Level

    SELECT  5,
            1,
            NULL, NULL, NULL, NULL,   --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Checklist Information
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added column when we did the code merge M.A.20061116
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            ClaimAspectID,
            ClaimAspectServiceChannelID,
            EscalationDate,
            AlarmModificationAllowedFlag,
            PertainsTo,
           --Project:210474 APD Added 5 columns below when we did the code merge M.A.20061116
            DefaultAssignmentPoolID,
            DefaultAssignmentPoolFunctionCD,
            FOHasSupervisor,
            FAHasSupervisor,
            FSHasSupervisor


    FROM    @tmpReference

    ORDER BY [Metadata!3!Entity], Tag
    FOR XML EXPLICIT                  -- (Comment out for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDiaryGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDiaryGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspDiaryGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/