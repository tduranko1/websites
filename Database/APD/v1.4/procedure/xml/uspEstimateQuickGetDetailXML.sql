-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspEstimateQuickGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateQuickGetDetailXML
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateQuickGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retreives estimate data for the estimate utility program
*
* PARAMETERS:
* (I) @DocumentID                   The Document ID to retrieve
* (I) @ClaimAspectID                The Claim Aspect ID of the document
* (I) @LynxID                       The Lynx ID of the document
* (I) @ClaimAspectServiceChannelID  The Claim Aspect Service Channel ID of the document
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspEstimateQuickGetDetailXML
    @DocumentID                     udt_std_id_big,
    @ClaimAspectID                  udt_std_id_big,
    @LynxID                         udt_std_id = NULL,
    @ClaimAspectServiceChannelID    udt_std_id_big = NULL,
    @UserID							      udt_std_id = NULL
AS
BEGIN
    -- Declare internal variables

    SET NOCOUNT ON
    DECLARE @ProcName               varchar(30)

    SET @ProcName = 'uspEstimateQuickGetDetailXML'

    DECLARE @CoverageProfileCD      udt_std_cd
    DECLARE @DeductibleAmt          udt_std_money
    DECLARE @ShopLocationID         udt_std_id_big
    DECLARE @AssignmentDate         udt_std_datetime
    DECLARE @PricingAvailable       char(1)
    DECLARE @ActiveAssignment       bit
    DECLARE @ProgramShopAssignment  bit
    DECLARE @ClaimAspectStatus      int
    DECLARE @ReinspectionCompleted  bit
    DECLARE @ReinspectionPerformedBy varchar(100)
    DECLARE @ReinspectionDate       datetime
    DECLARE @UserIsSupervisor		   bit
    DECLARE @ServiceChannelCD       udt_std_cd
    DECLARE @EarlyBillFlag          bit
    DECLARE @SupervisorFlag         bit
    DECLARE @ApprovedDocumentCount  bit
    DECLARE @WarrantyExistsFlag     bit

    SET @ShopLocationID = 0

    -- Validate parameters

    IF @DocumentID > 0
    BEGIN
        IF NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID)
        BEGIN
            -- Invalid Document ID

            RAISERROR  ('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
            RETURN
        END

        --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        IF NOT EXISTS(
                        SELECT DocumentID
                        FROM dbo.utb_claim_aspect_service_channel_document cascd
                        Inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                        WHERE casc.ClaimAspectID = @ClaimAspectID
                        AND cascd.DocumentID = @DocumentID
                     )
        --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        BEGIN
            RAISERROR('%s: Invalid ClaimAspectID for this DocumentID', 16, 1, @ProcName)
        END
        ELSE
        BEGIN
            SELECT @ServiceChannelCD = casc.ServiceChannelCD,
                   @WarrantyExistsFlag = ca.WarrantyExistsFlag
            FROM dbo.utb_claim_aspect_service_channel_document cascd
            Inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
            Inner join utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
            WHERE casc.ClaimAspectID = @ClaimAspectID
            AND cascd.DocumentID = @DocumentID
        END
    END
    ELSE
    BEGIN
        -- IF DocumentID = 0, we need a LYNX ID

        IF @LynxID IS NULL OR
           NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        BEGIN
            RAISERROR  ('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
            RETURN
        END
    END
    
    SELECT @UserIsSupervisor = SupervisorFlag
    FROM utb_user
    WHERE UserID = @UserID


    -- Gather metadata for all entities updatable columns

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )

    INSERT INTO @tmpMetadata (GroupName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Estimate',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  ((Table_Name = 'utb_document' AND Column_Name IN
            ('DocumentTypeID',
             'SupplementSeqNumber',
             'AgreedPriceMetCD',
             'DuplicateFlag',
             'EstimateTypeCD',
             'SysLastUserID')))
      OR  ((Table_Name = 'utb_estimate_summary' AND Column_Name IN
            ('ExtendedAmt')))
        --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
      OR  ((Table_Name = 'utb_claim_aspect_service_channel_document' AND Column_Name IN
            ('ClaimAspectServiceChannelID')))
        --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Get LYNX ID from ClaimAspectID if LynxID was not provided

    IF @LynxID IS NULL
    BEGIN
        SELECT  @LynxID = LynxID
          FROM  dbo.utb_claim_aspect
          WHERE ClaimAspectID = @ClaimAspectID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    -- Create temporary Table to hold Approved estimates

    DECLARE @tmpApprovedEst TABLE
    (
        DocumentID          bigint NOT NULL,
        ClaimAspectID       bigint NULL
    )
    
    INSERT INTO @tmpApprovedEst
    SELECT d.DocumentID, ca.ClaimAspectID
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE c.LynxID = @LynxID
      AND d.EnabledFlag = 1
      AND dt.EstimateTypeFlag = 1
      AND d.ApprovedFlag = 1 

    -- Gather Reference Data

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50)     NOT NULL,
        DisplayOrder        int             NULL,
        ReferenceId         varchar(10)     NOT NULL,
        Name                varchar(50)     NOT NULL,
        EstimateTypeFlag    bit             NULL,
        ClaimAspectID       int             NULL,
        DeductibleAmt       money           NULL,
        ShowAudited         bit             NULL,
        ServiceChannelCD    varchar(4)      NULL,
        ApprovedDocumentCount int           NULL
    )

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name, EstimateTypeFlag, ClaimAspectID, DeductibleAmt, ShowAudited, ServiceChannelCD, ApprovedDocumentCount)

    SELECT  'DocumentType' AS ListName,
            DisplayOrder,
            convert(varchar, DocumentTypeID),
            Name,
            EstimateTypeFlag,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_document_type
    WHERE   EnabledFlag = 1
      AND   DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'AgreedPriceMetCD' AS ListName,
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_document', 'AgreedPriceMetCD')

    UNION ALL

    SELECT  'EstimateTypeCD' AS ListName,
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')

    UNION ALL

    SELECT  DISTINCT 'PertainsTo',
            NULL,
            EntityCode,
            cel.Name,
            NULL,
            cel.ClaimAspectID,
            CASE
              WHEN ca.CoverageProfileCD IS NULL THEN 0
		--*********************************************************************************
		--Project: 210474 APD - Enhancements to support multiple concurrent service channels
		--Note:	Verify the logic below with JR/JP
		--	M.A. 20061109
		--*********************************************************************************

	      else ISNULL(cc.DeductibleAmt,0.00)
		
        --      WHEN ca.CoverageProfileCD = 'COLL' THEN IsNull(cc.CollisionDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'COMP' THEN IsNull(cc.ComprehensiveDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'LIAB' THEN IsNull(cc.LiabilityDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'UIM' THEN IsNull(cc.UnderinsuredDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'UM' THEN IsNull(cc.UninsuredDeductibleAmt, 0.00)
		
            END,
            (SELECT CASE
                        WHEN casc.ServiceChannelCD = 'DA' or casc.ServiceChannelCD = 'DR' or casc.ServiceChannelCD = 'RRP' THEN 1
                        ELSE 0
                    END
             FROM dbo.utb_claim_aspect ca
             --Project:210474 APD Remarked-off the following to support the schema change M.A.20061218
             --LEFT JOIN dbo.utb_assignment_type cat ON (ca.CurrentAssignmentTypeID = cat.AssignmentTypeID)
             --Project:210474 APD Added the following to support the schema change M.A.20061218
             LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
             on ca.ClaimAspectID = casc.ClaimAspectID
             and casc.Primaryflag = 1
             WHERE ca.ClaimAspectID = cel.ClaimAspectID),
            NULL,
            (SELECT COUNT(DocumentID)
             FROM @tmpApprovedEst t
             WHERE t.ClaimAspectID = ca.ClaimAspectID)
      FROM  dbo.ufnUtilityGetClaimEntityList(@LynxID, 1, 0) cel   -- Get All exposures
      LEFT JOIN dbo.utb_claim_aspect ca ON (cel.ClaimAspectID = ca.ClaimAspectID)
      Left Outer join   utb_Claim c
      on           c.lynxid = ca.Lynxid
      Left Outer join   utb_Client_Coverage_Type cct
      on           c.InsuranceCompanyID = cct.InsuranceCompanyID
      and           cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
      and          cct.CoverageProfileCD = ca.CoverageProfileCD
      Left Outer join   utb_Claim_Coverage cc
      on           cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
      and          c.LynxID = cc.LynxID
      and          cc.CoverageTypeCD = cct.CoverageProfileCD
      and          cc.AddtlCoverageFlag = 0 -- Exclude the additional coverages


    UNION ALL

   SELECT  'ServiceChannelCD' AS ListName,
            NULL,
            code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel', 'ServiceChannelCD')

    UNION ALL

    SELECT  DISTINCT 'ServiceChannel',
            NULL,
            convert(varchar(10),casc.ClaimAspectServiceChannelID),
            fn.Name,
            NULL,
            ca.ClaimAspectID,
            NULL,
            NULL,
            casc.ServiceChannelCD,
            NULL

      FROM  dbo.ufnUtilityGetClaimEntityList(@LynxID, 1, 0) cel   -- Get All exposures
      LEFT JOIN dbo.utb_claim_aspect ca ON (cel.ClaimAspectID = ca.ClaimAspectID)
      Left Outer join   utb_Claim c
      on           c.lynxid = ca.Lynxid
      Left Outer join   utb_Client_Coverage_Type cct
      on           c.InsuranceCompanyID = cct.InsuranceCompanyID
      and           cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
      and          cct.CoverageProfileCD = ca.CoverageProfileCD
      Left Outer join   utb_Claim_Coverage cc
      on           cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
      and          c.LynxID = cc.LynxID
      and          cc.CoverageTypeCD = cct.CoverageProfileCD
      and          cc.AddtlCoverageFlag = 0 -- Exclude the additional coverages
      left outer join utb_Claim_Aspect_Service_Channel casc
      on    casc.ClaimAspectID = ca.ClaimAspectID
      left outer join dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel', 'ServiceChannelCD') fn
      on fn.code = casc.ServiceChannelCD

      where ca.ClaimAspectTypeID = 9 --for vehicle


    ORDER BY ListName,  EstimateTypeFlag DESC, DisplayOrder, Name

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    -- Shop pricing data as of assignmentdate
    DECLARE @tmpShopPricing TABLE (HourlyRateMechanical     money   NULL,
                                   HourlyRateRefinishing    money   NULL,
                                   HourlyRateSheetMetal     money   NULL,
                                   HourlyRateUnibodyFrame   money   NULL)


    -- Grab some info needed to locate the shop pricing data
    -- Use the document's AssignmentID if we have it.  Which we should.

    IF @DocumentID > 0
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        INNER JOIN utb_document d
            ON d.AssignmentID = a.AssignmentID
        WHERE d.DocumentID = @DocumentID
        AND a.ShopLocationID IS NOT NULL -- changed by Venu
          AND  a.AssignmentDate IS NOT NULL           
            AND a.CancellationDate IS NULL
    END

    -- If we dont have the ShopLocationID but we have a ClaimAspectServiceChannelID,
    -- then attempt to use the latter to get the shop pricing.  This would
    -- be applicable for when a shop is selected but no assignment has been sent.

    IF ( @ShopLocationID IS NULL OR @ShopLocationID = 0 )
        AND ( NOT ( @ClaimAspectServiceChannelID IS NULL ) )
        AND ( @ClaimAspectServiceChannelID > 0 )
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        WHERE a.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            AND a.CancellationDate IS NULL
              AND a.ShopLocationID IS NOT NULL -- changed by Venu
    END

    -- Now fall back to the original method - usign the ClaimAspectID.

    IF ( @ShopLocationID IS NULL OR @ShopLocationID = 0 )
        AND ( ( @ClaimAspectServiceChannelID IS NULL )
        OR ( @ClaimAspectServiceChannelID = 0 ) )
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        INNER JOIN utb_Claim_Aspect_Service_Channel casc
            ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        WHERE casc.ClaimAspectID = @ClaimAspectID
            AND a.CancellationDate IS NULL
              AND a.ShopLocationID IS NOT NULL -- changed by Venu
    END


    IF @@ERROR <> 0
    BEGIN

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Grap the shop pricing data
    IF EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location_pricing_history WHERE ShopLocationID = @ShopLocationID)
    BEGIN
      INSERT INTO @tmpShopPricing
      SELECT TOP 1 HourlyRateMechanical,
                   HourlyRateRefinishing,
                   HourlyRateSheetMetal,
                   HourlyRateUnibodyFrame
      FROM dbo.utb_shop_location_pricing_history
      WHERE ShopLocationID = @ShopLocationID
        AND SysLastUpdatedDate < @AssignmentDate
      ORDER BY SysLastUpdatedDate desc
    END
    ELSE
    BEGIN
      INSERT INTO @tmpShopPricing
      SELECT HourlyRateMechanical,
             HourlyRateRefinishing,
             HourlyRateSheetMetal,
             HourlyRateUnibodyFrame
      FROM dbo.utb_shop_location_pricing
      WHERE ShopLocationID = @ShopLocationID
    END

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpShopPricing', 16, 1, @ProcName)
        RETURN
    END

    SET @PricingAvailable = (SELECT COUNT(*) FROM @tmpShopPricing)

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimAspectStatus = cas.StatusID
    FROM dbo.utb_claim_aspect ca
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Status to get to a ClaimAspectID row
		M.A. 20061109
	*********************************************************************************/
	INNER JOIN utb_Claim_Aspect_Status cas ON cas.ClaimAspectID = ca.ClaimAspectID
    WHERE ca.ClaimAspectID = @ClaimAspectID

    IF NOT EXISTS(SELECT *
                    FROM utb_status
                    WHERE StatusID = @ClaimAspectStatus
                      AND Name IN ('Vehicle Closed', 'Vehicle Cancelled', 'Vehicle Voided'))
    BEGIN
        -- Claim Aspect is active. Now see if there is an active assignment.

        SET @ActiveAssignment = (SELECT count(*)
                                    FROM dbo.utb_assignment a
					/*********************************************************************************
					Project: 210474 APD - Enhancements to support multiple concurrent service channels
					Note:	Added utb_Claim_Aspect_Service_Channel to get to a ClaimAspectID row
						M.A. 20061109
					*********************************************************************************/
					INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                    WHERE casc.ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
                                      AND a.CancellationDate IS NULL)

        -- Now get the program shop status of the most recent assignment
        SELECT @ProgramShopAssignment = ProgramFlag
        FROM dbo.utb_shop_location
        WHERE ShopLocationID = (SELECT top 1 ShopLocationID
                                FROM utb_assignment a
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel to get to a ClaimAspectID row
					M.A. 20061109
				*********************************************************************************/
				INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                WHERE casc.ClaimAspectID = @ClaimAspectID
                                  AND a.CancellationDate IS NULL
                                ORDER BY a.AssignmentDate DESC)
    END

    -- Check if a reinspection was completed
    SET @ReinspectionCompleted = 0
    SELECT @ReinspectionCompleted = isNull(LockedFlag, 0),
           @ReinspectionPerformedBy = isNull(u.NameLast, '') + ', ' + isNull(u.NameFirst, ''),
           @ReinspectionDate = ReinspectionDate
    FROM dbo.utb_reinspect r
    LEFT JOIN dbo.utb_user u ON (r.ReinspectorUserID = u.UserID)
    WHERE r.ClaimAspectID = @ClaimAspectID
      AND r.EstimateDocumentID = @DocumentID

    SELECT @EarlyBillFlag = i.EarlyBillFlag
    FROM utb_claim c 
    LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID

    IF @UserID IS NOT NULL
    BEGIN
      SELECT @SupervisorFlag = SupervisorFlag
      FROM dbo.utb_user
      WHERE UserID = @UserID
    END
    
    -- Count the number of approved estimates besides this one
    IF @DocumentID > 0
    BEGIN
       SELECT @ApprovedDocumentCount = COUNT(d.DocumentID)
       FROM utb_document d 
       LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
       LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
       WHERE cascd.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
         AND d.EnabledFlag = 1
         AND dt.EstimateTypeFlag = 1
         AND d.ApprovedFlag = 1
         AND d.DocumentID <> @DocumentID
    END

    SET @WarrantyExistsFlag = 0
    IF EXISTS(SELECT wa.AssignmentID
               FROM utb_warranty_assignment wa
               LEFT JOIN utb_claim_aspect_service_channel casc  ON wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
               WHERE (ca.ClaimAspectID = @ClaimAspectID OR ca.LynxID = @LynxID)
                AND wa.CancellationDate IS NULL)
    BEGIN
      SET @WarrantyExistsFlag = 1
    END
    
    -- Final Select

    SELECT  1    AS Tag,
            NULL AS Parent,
            @DocumentID AS [Root!1!DocumentID],
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @ClaimAspectServiceChannelID AS [Root!1!ClaimAspectServiceChannelID],
            @PricingAvailable AS [Root!1!PricingAvailable],
            @UserID AS [Root!1!UserID],
            @UserIsSupervisor AS [Root!1!UserIsSupervisor],
            @ServiceChannelCD AS [Root!1!ServiceChannelCD],
            @EarlyBillFlag as [Root!1!EarlyBillFlag],
            @SupervisorFlag as [Root!1!SupervisorFlag],
            @ApprovedDocumentCount as [Root!1!ApprovedDocumentCount],
            @WarrantyExistsFlag as [Root!1!WarrantyExistsFlag],
            -- Estimate Detail
            NULL AS [Estimate!2!DocumentID],
            NULL AS [Estimate!2!DocumentTypeID],
            NULL AS [Estimate!2!SupplementSeqNumber],
            NULL AS [Estimate!2!DocumentVANSourceFlag],
            NULL AS [Estimate!2!DuplicateFlag],
            NULL AS [Estimate!2!EstimateTypeCD],
            NULL AS [Estimate!2!RepairTotalAmt],
            NULL AS [Estimate!2!DeductibleAmt],
            NULL AS [Estimate!2!BettermentAmt],
            NULL AS [Estimate!2!OtherAdjustmentAmt],
            NULL AS [Estimate!2!NetTotalAmt],
            NULL AS [Estimate!2!TaxTotalAmt],
            NULL AS [Estimate!2!AgreedPriceMetCD],
            NULL AS [Estimate!2!FullSummaryExistsFlag],
            NULL AS [Estimate!2!ActiveAssignment],
            NULL AS [Estimate!2!ProgramShopAssignment],
            NULL AS [Estimate!2!ReinspectionRequestFlag],
            NULL AS [Estimate!2!ReinspectionCompleted],
            NULL AS [Estimate!2!ReinspectionPerformedBy],
            NULL AS [Estimate!2!ReinspectionDate],
            NULL AS [Estimate!2!EstimateLockedFlag],
            NULL AS [Estimate!2!ApprovedFlag],
            NULL AS [Estimate!2!ApprovedDate],
            NULL AS [Estimate!2!WarrantyFlag],
            NULL AS [Estimate!2!BilledFlag],
            -- Shop Pricing
            NULL AS [ShopPricing!3!Mechanical],
            NULL AS [ShopPricing!3!Refinishing],
            NULL AS [ShopPricing!3!SheetMetal],
            NULL AS [ShopPricing!3!UnibodyFrame],
            -- Metadata Header
            NULL AS [Metadata!4!Entity],
            -- Columns
            NULL AS [Column!5!Name],
            NULL AS [Column!5!DataType],
            NULL AS [Column!5!MaxLength],
            NULL AS [Column!5!Precision],
            NULL AS [Column!5!Scale],
            NULL AS [Column!5!Nullable],
            -- Reference Data
            NULL AS [Reference!6!List],
            NULL AS [Reference!6!ReferenceID],
            NULL AS [Reference!6!Name],
            NULL AS [Reference!6!EstimateTypeFlag],
            NULL AS [Reference!6!ClaimAspectID],
            NULL AS [Reference!6!DeductibleAmt],
            NULL AS [Reference!6!ShowAudited],
            NULL AS [Reference!6!ServiceChannelCD],
            NULL AS [Reference!6!ApprovedDocumentCount]


    UNION ALL


    SELECT  2,
            1,
            NULL, NULL, NULL, NULL,
            @UserID,
            isNull(@UserIsSupervisor, 0),
            NULL, NULL, NULL, NULL, NULL,
            -- Estimate Detail
            d.DocumentID,
            IsNull(d.DocumentTypeID, 0),
            IsNull(d.SupplementSeqNumber, ''),
            IsNull(ds.VANFlag, 0),
            IsNull(d.DuplicateFlag, ''),
            IsNull(d.EstimateTypeCD, ''),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 27 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 28 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 30 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 39 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 32 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 25 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull(d.AgreedPriceMetCD, ''),
            IsNull(d.FullSummaryExistsFlag, 0),
            IsNull(@ActiveAssignment, 0),
            IsNull(@ProgramShopAssignment, 0),
            d.ReinspectionRequestFlag,
            @ReinspectionCompleted,
            @ReinspectionPerformedBy,
            @ReinspectionDate,
            IsNull(d.EstimateLockedFlag, 0),
            d.ApprovedFlag, 
            d.ApprovedDate, 
            d.WarrantyFlag,
            isNull((SELECT CASE 
                              WHEN i.StatusCD = 'APD' OR 
                                   i.StatusCD = 'AC' OR
                                   i.StatusCD = 'NB' THEN 0
                              ELSE 1
                          END
                     FROM utb_invoice i
                     WHERE i.DocumentID = d.DocumentID
                       AND i.EnabledFlag = 1), 0),
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
      FROM  (SELECT @DocumentID AS DocumentID) AS parms
      LEFT JOIN dbo.utb_document d ON (parms.DocumentID = d.DocumentID)
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)


    UNION ALL

    -- Select Shop Pricing Level

    SELECT DISTINCT 3,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            ISNULL(convert(varchar(15), HourlyRateMechanical), ''),
            ISNULL(convert(varchar(15), HourlyRateRefinishing), ''),
            ISNULL(convert(varchar(15), HourlyRateSheetMetal), ''),
            ISNULL(convert(varchar(15), HourlyRateUnibodyFrame), ''),

            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpShopPricing


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 4,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  5,
            4,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  6,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            EstimateTypeFlag,
            ClaimAspectID,
            DeductibleAmt,
            ShowAudited,
            ServiceChannelCD,
            ApprovedDocumentCount

    FROM    @tmpReference


    ORDER BY [Metadata!4!Entity], Tag
--    FOR XML EXPLICIT                  -- (Comment out for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspEstimateQuickGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateQuickGetDetailXML TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
