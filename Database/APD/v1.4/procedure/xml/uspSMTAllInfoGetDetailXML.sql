-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTAllInfoGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTAllInfoGetDetailXML 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTAllInfoGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieve a specific Shop Location
*
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSMTAllInfoGetDetailXML]
(
	@CertifiedFirstID       udt_std_desc_short,
  @AddressZip             udt_addr_zip_code
  
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error          AS INT
    DECLARE @rowcount       AS INT
    DECLARE @Now            AS datetime
    DECLARE @ProcName       AS varchar(30)       -- Used for raise error stmts 
    DECLARE @BusinessInfoID AS udt_std_id_big
    DECLARE @ShopID         AS udt_std_id_big
    DECLARE @ShopLoadID     AS bigint
    DECLARE @MergeDate      AS udt_std_datetime
    

    SET @ProcName = 'uspSMTAllInfoGetDetailXML'
    SET @Now = CURRENT_TIMESTAMP
    
    
    -- Initialize Local Variables and validate Input Parameters
    IF (@CertifiedFirstID IS NULL)
    BEGIN
      RAISERROR ('Invalid Parameter. CertifiedFirstID=%s', 16, 1, @CertifiedFirstID)
      RETURN
    END
      
        
    SET @ShopID = ISNULL((SELECT TOP 1 ShopLocationID FROM dbo.utb_shop_load WHERE CertifiedFirstID = @CertifiedFirstID), 0)
    
        
    SET @ShopLoadID = ISNULL((SELECT TOP 1 ShopLoadID FROM dbo.utb_shop_load WHERE CertifiedFirstID = @CertifiedFirstID), 0)
    
    SET @MergeDate = (SELECT MergeDate FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID)
    IF (@MergeDate IS NOT NULL) SET @ShopLoadID = -1 -- prevents web access to records that have been merged
    
    
    IF (@ShopLoadID > 0)
    BEGIN
      IF ((SELECT AddressZip FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID AND AddressZip = @AddressZip) IS NULL)
      BEGIN
        SET @ShopID = 0
        SET @ShopLoadID = 0
      END
    END
    ELSE IF (@ShopLoadID IS NULL) OR (@ShopLoadID = 0)
    BEGIN
      IF ((SELECT TOP 1 AddressZip FROM dbo.utb_shop_location WHERE CertifiedFirstID = @CertifiedFirstID AND AddressZip = @AddressZip) IS NULL)
      BEGIN
        SET @ShopID = 0
        SET @ShopLoadID = 0
      END
    END
                                                                                   
    SET @BusinessInfoID = ISNULL((Select ShopID FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID), 0)
    
        
        
    
     /*************************************************************************************
    *  Gather Meta Data
    **************************************************************************************/  
    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    
    SELECT  'BusinessInfo',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop' AND Column_Name IN 
            ('ShopID',
             'BusinessTypeCD',
             'FedTaxID'))
    
    UNION ALL
    
    SELECT  'ShopInfo',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop_location' AND Column_Name IN 
            ('ShopLocationID',
             'PreferredCommunicationMethodID',
             'PreferredEstimatePackageID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressCounty',
             'AddressState',
             'AddressZip',
             'CertifiedFirstId',
             'DrivingDirections',
             'EmailAddress',
             'FaxAreaCode',
             'FaxExchangeNumber',
             'FaxExtensionNumber',
             'FaxUnitNumber',
             'MaxWeeklyAssignments',
             'Name',
             'PhoneAreaCode',
             'PhoneExchangeNumber',
             'PhoneExtensionNumber',
             'PhoneUnitNumber',
             'WarrantyPeriodRefinishCD',
             'WarrantyPeriodWorkmanshipCD',
             'WebSiteAddress'))
             
    UNION ALL
    
    SELECT  'ShopLoad',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop_load' AND Column_Name IN 
            ('FinalSubmitFlag',
             'RepairFacilityTypeCD'))         

    UNION ALL
    
    SELECT  'ShopHours',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop_location_hours' AND Column_Name IN 
            ('OperatingMondayStartTime',
             'OperatingMondayEndTime',
             'OperatingTuesdayStartTime',
             'OperatingTuesdayEndTime',
             'OperatingWednesdayStartTime',
             'OperatingWednesdayEndTime',
             'OperatingThursdayStartTime',
             'OperatingThursdayEndTime',
             'OperatingFridayStartTime',
             'OperatingFridayEndTime',
             'OperatingSaturdayStartTime',
             'OperatingSaturdayEndTime',
             'OperatingSundayStartTime',
             'OperatingSundayEndTime'))
             
    UNION ALL
    
    SELECT  'Personnel',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_personnel' AND Column_Name IN 
            ('PersonnelID',
             'PersonnelTypeID',
             'PreferredContactMethodID',
             'CellAreaCode',
             'CellExchangeNumber',
             'CellExtensionNumber',
             'CellUnitNumber',
             'EmailAddress',
             'FaxAreaCode',
             'FaxExchangeNumber',
             'FaxExtensionNumber',
             'FaxUnitNumber',
             'GenderCD',
             'Name',
             'PagerAreaCode',
             'PagerExchangeNumber',
             'PagerExtensionNumber',
             'PagerUnitNumber',
             'PhoneAreaCode',
             'PhoneExchangeNumber',
             'PhoneExtensionNumber',
             'PhoneUnitNumber',
             'ShopManagerFlag'))
             
    UNION ALL
    
    SELECT  'Pricing',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_shop_location_pricing' AND Column_Name IN 
            ('ShopLocationID',
             'AlignmentFourWheel',
             'AlignmentTwoWheel',              
             'CaulkingSeamSealer',             
             'ChipGuard',                      
             'CoolantGreen',                   
             'CoolantRed',                     
             'CorrosionProtection',            
             'CountyTaxPct',                   
             'CoverCar',     
             'DailyStorage',                  
             'DiscountPctSideBackGlass',       
             'DiscountPctWindshield',          
             'FlexAdditive',                   
             'GlassReplacementChargeTypeCD',   
             'GlassSubletServiceFee',          
             'GlassSubletServicePct',          
             'HazardousWaste',                 
             'HourlyRateMechanical',           
             'HourlyRateRefinishing',          
             'HourlyRateSheetMetal',           
             'HourlyRateUnibodyFrame',         
             'MunicipalTaxPct',                
             'OtherTaxPct',                    
             'PartsRecycledMarkupPct',         
             'PullSetUp',                      
             'PullSetUpMeasure',               
             'RefinishSingleStageHourly',      
             'RefinishSingleStageMax',         
             'RefinishThreeStageHourly',       
             'RefinishThreeStageMax',          
             'RefinishThreeStageCCMaxHrs',     
             'RefinishTwoStageHourly',         
             'RefinishTwoStageMax',            
             'RefinishTwoStageCCMaxHrs',       
             'R12EvacuateRecharge',            
             'R12EvacuateRechargeFreon',       
             'R134EvacuateRecharge',           
             'R134EvacuateRechargeFreon',      
             'SalesTaxPct',                    
             'StripePaintPerPanel',            
             'StripePaintPerSide',             
             'StripeTapePerPanel',             
             'StripeTapePerSide',              
             'TireMountBalance',               
             'TowInChargeTypeCD',              
             'TowInFlatFee',                   
             'Undercoat'))


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    /*************************************************************************************
    *  Gather Reference Data
    **************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )

    -- States
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name)
        
    Select 'State' AS ListName, DisplayOrder, StateCode, StateValue
    From dbo.utb_state_code
    Where EnabledFlag = 1
    
    UNION ALL
    
    -- Estimate Packages
    Select 'EstimatePackage', DisplayOrder, CAST(EstimatePackageID AS Varchar), Name
    From dbo.utb_estimate_package
    Where EnabledFlag = 1
    
    UNION ALL
    
    
    -- Repair Facility Types
    SELECT  'RepairFacilityType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'RepairFacilityTypeCD' ) 
    
    UNION ALL
    
    -- Warranty Periods
    SELECT 'WarrantyPeriod',
           NULL,
           CAST(Code AS Varchar),
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'WarrantyPeriodRefinishCD' )
    
    UNION ALL
    
    -- Business Types
    SELECT 'BusinessType',
           NULL,
           Code,
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'BusinessTypeCD' )
     
    
    UNION ALL
    
    SELECT  'ContactMethod' AS ListName,DisplayOrder, CAST(PersonnelContactMethodID AS Varchar), Name 
    FROM    dbo.utb_personnel_contact_method
    WHERE   EnabledFlag = 1
      AND   DisplayOrder IS NOT NULL
      
    UNION ALL
    
    SELECT  'GlassReplacementChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'GlassReplacementChargeTypeCD' ) 
      
    UNION ALL
    
    SELECT  'TowInChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'TowInChargeTypeCD' )  
    
    UNION ALL
    
    SELECT 'PersonnelType' AS ListName, DisplayOrder, CAST(PersonnelTypeID AS Varchar), Name
    FROM dbo.utb_personnel_type
    WHERE EnabledFlag = 1
      AND AppliesToCD = 'L'
      
    ORDER BY ListName, DisplayOrder
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
           

   -- Begin final select 
    SELECT
        1                                       AS Tag,
        Null                                    AS Parent,
        --Root
        @ShopID                                 AS [Root!1!ShopID],
        @BusinessInfoID                         AS [Root!1!BusinessInfoID],
        @ShopLoadID                             AS [Root!1!ShopLoadID],
        
        --BusinessInfo
        Null                                    AS [BusinessInfo!2!FedTaxID],
        Null                                    AS [BusinessInfo!2!BusinessTypeCD],
        
        --Shop
        Null                                    AS [ShopInfo!3!PreferredEstimatePackageID],
        Null                                    AS [ShopInfo!3!Address1],
        Null                                    AS [ShopInfo!3!Address2],
        Null                                    AS [ShopInfo!3!AddressCity],
        Null                                    AS [ShopInfo!3!AddressCounty],
        Null                                    AS [ShopInfo!3!AddressState],
        Null                                    AS [ShopInfo!3!AddressZip],
        Null                                    AS [ShopInfo!3!CertifiedFirstID],
        Null                                    AS [ShopInfo!3!DrivingDirections],
        Null                                    AS [ShopInfo!3!EmailAddress],
        Null                                    AS [ShopInfo!3!FaxAreaCode],
        Null                                    AS [ShopInfo!3!FaxExchangeNumber],
        Null                                    AS [ShopInfo!3!FaxExtensionNumber],
        Null                                    AS [ShopInfo!3!FaxUnitNumber],
        Null                                    AS [ShopInfo!3!FinalSubmitFlag],
        Null                                    AS [ShopInfo!3!MaxWeeklyAssignments],
        Null                                    AS [ShopInfo!3!Name],
        Null                                    AS [ShopInfo!3!PhoneAreaCode],
        Null                                    AS [ShopInfo!3!PhoneExchangeNumber],
        Null                                    AS [ShopInfo!3!PhoneExtensionNumber],
        Null                                    AS [ShopInfo!3!PhoneUnitNumber],
        Null                                    AS [ShopInfo!3!RepairFacilityTypeCD],
        Null                                    AS [ShopInfo!3!WebSiteAddress],
        Null                                    AS [ShopInfo!3!WarrantyPeriodRefinishCD],
        Null                                    AS [ShopInfo!3!WarrantyPeriodWorkmanshipCD],
        
        --Shop Hours
        Null                                    AS [ShopHours!4!OperatingMondayStartTime],
        Null                                    AS [ShopHours!4!OperatingMondayEndTime],
        Null                                    AS [ShopHours!4!OperatingTuesdayStartTime],
        Null                                    AS [ShopHours!4!OperatingTuesdayEndTime],
        Null                                    AS [ShopHours!4!OperatingWednesdayStartTime],
        Null                                    AS [ShopHours!4!OperatingWednesdayEndTime],
        Null                                    AS [ShopHours!4!OperatingThursdayStartTime],
        Null                                    AS [ShopHours!4!OperatingThursdayEndTime],
        Null                                    AS [ShopHours!4!OperatingFridayStartTime],
        Null                                    AS [ShopHours!4!OperatingFridayEndTime],
        Null                                    AS [ShopHours!4!OperatingSaturdayStartTime],
        Null                                    AS [ShopHours!4!OperatingSaturdayEndTime],
        Null                                    AS [ShopHours!4!OperatingSundayStartTime],
        Null                                    AS [ShopHours!4!OperatingSundayEndTime],
        
        -- Shop Personnel
        NULL                                    AS [ShopPersonnel!5!PersonnelID],
        NULL                                    AS [ShopPersonnel!5!PersonnelTypeID],
        NULL                                    AS [ShopPersonnel!5!PreferredContactMethodID],
        NULL                                    AS [ShopPersonnel!5!CellAreaCode],
        NULL                                    AS [ShopPersonnel!5!CellExchangeNumber],
        NULL                                    AS [ShopPersonnel!5!CellExtensionNumber],
        NULL                                    AS [ShopPersonnel!5!CellUnitNumber],
        NULL                                    AS [ShopPersonnel!5!EmailAddress],
        NULL                                    AS [ShopPersonnel!5!FaxAreaCode],
        NULL                                    AS [ShopPersonnel!5!FaxExchangeNumber],
        NULL                                    AS [ShopPersonnel!5!FaxExtensionNumber],
        NULL                                    AS [ShopPersonnel!5!FaxUnitNumber],
        NULL                                    AS [ShopPersonnel!5!GenderCD],
        NULL                                    AS [ShopPersonnel!5!Name],
        NULL                                    AS [ShopPersonnel!5!PagerAreaCode],
        NULL                                    AS [ShopPersonnel!5!PagerExchangeNumber],
        NULL                                    AS [ShopPersonnel!5!PagerExtensionNumber],
        NULL                                    AS [ShopPersonnel!5!PagerUnitNumber],
        NULL                                    AS [ShopPersonnel!5!PhoneAreaCode],
        NULL                                    AS [ShopPersonnel!5!PhoneExchangeNumber],
        NULL                                    AS [ShopPersonnel!5!PhoneExtensionNumber],
        NULL                                    AS [ShopPersonnel!5!PhoneUnitNumber],
        NULL                                    AS [ShopPersonnel!5!ShopManagerFlag],
        
        -- Business Personnel
        NULL                                    AS [BusinessPersonnel!6!PersonnelID],
        NULL                                    AS [BusinessPersonnel!6!PersonnelTypeID],
        NULL                                    AS [BusinessPersonnel!6!PreferredContactMethodID],
        NULL                                    AS [BusinessPersonnel!6!CellAreaCode],
        NULL                                    AS [BusinessPersonnel!6!CellExchangeNumber],
        NULL                                    AS [BusinessPersonnel!6!CellExtensionNumber],
        NULL                                    AS [BusinessPersonnel!6!CellUnitNumber],
        NULL                                    AS [BusinessPersonnel!6!EmailAddress],
        NULL                                    AS [BusinessPersonnel!6!FaxAreaCode],
        NULL                                    AS [BusinessPersonnel!6!FaxExchangeNumber],
        NULL                                    AS [BusinessPersonnel!6!FaxExtensionNumber],
        NULL                                    AS [BusinessPersonnel!6!FaxUnitNumber],
        NULL                                    AS [BusinessPersonnel!6!GenderCD],
        NULL                                    AS [BusinessPersonnel!6!Name],
        NULL                                    AS [BusinessPersonnel!6!PagerAreaCode],
        NULL                                    AS [BusinessPersonnel!6!PagerExchangeNumber],
        NULL                                    AS [BusinessPersonnel!6!PagerExtensionNumber],
        NULL                                    AS [BusinessPersonnel!6!PagerUnitNumber],
        NULL                                    AS [BusinessPersonnel!6!PhoneAreaCode],
        NULL                                    AS [BusinessPersonnel!6!PhoneExchangeNumber],
        NULL                                    AS [BusinessPersonnel!6!PhoneExtensionNumber],
        NULL                                    AS [BusinessPersonnel!6!PhoneUnitNumber],
        NULL                                    AS [BusinessPersonnel!6!ShopManagerFlag],
        
        --Pricing
        Null                                    AS [Pricing!7!AlignmentFourWheel],                                                   
        Null                                    AS [Pricing!7!AlignmentTwoWheel],                                                    
        Null                                    AS [Pricing!7!CaulkingSeamSealer],                                                   
        Null                                    AS [Pricing!7!ChipGuard],                                                            
        Null                                    AS [Pricing!7!CoolantGreen],                                                         
        Null                                    AS [Pricing!7!CoolantRed],                                                           
        Null                                    AS [Pricing!7!CorrosionProtection],                                                  
        Null                                    AS [Pricing!7!CountyTaxPct],                                                         
        Null                                    AS [Pricing!7!CoverCar],
        Null                                    AS [Pricing!7!DailyStorage],
        Null                                    AS [Pricing!7!DiscountDomestic],
        Null                                    AS [Pricing!7!DiscountImport],                                                             
        Null                                    AS [Pricing!7!DiscountPctSideBackGlass],                                             
        Null                                    AS [Pricing!7!DiscountPctWindshield],                                                
        Null                                    AS [Pricing!7!FlexAdditive],    
        Null                                    AS [Pricing!7!GlassOutsourceServiceFee],                                          
        Null                                    AS [Pricing!7!GlassReplacementChargeTypeCD],                                         
        Null                                    AS [Pricing!7!GlassSubletServiceFee],                                                
        Null                                    AS [Pricing!7!GlassSubletServicePct],                                                
        Null                                    AS [Pricing!7!HazardousWaste],                                                       
        Null                                    AS [Pricing!7!HourlyRateMechanical],                                                 
        Null                                    AS [Pricing!7!HourlyRateRefinishing],                                                
        Null                                    AS [Pricing!7!HourlyRateSheetMetal],                                                 
        Null                                    AS [Pricing!7!HourlyRateUnibodyFrame],                                               
        Null                                    AS [Pricing!7!MunicipalTaxPct], 
        Null                                    AS [Pricing!7!OEMDiscounts],                                                     
        Null                                    AS [Pricing!7!OtherTaxPct],                                                          
        Null                                    AS [Pricing!7!PartsRecycledMarkupPct],                                               
        Null                                    AS [Pricing!7!PullSetup],                                                            
        Null                                    AS [Pricing!7!PullSetupMeasure],                                                     
        Null                                    AS [Pricing!7!RefinishSingleStageHourly],                                            
        Null                                    AS [Pricing!7!RefinishSingleStageMax],                                               
        Null                                    AS [Pricing!7!RefinishThreeStageHourly],                                             
        Null                                    AS [Pricing!7!RefinishThreeStageMax],                                                
        Null                                    AS [Pricing!7!RefinishThreeStageCCMaxHrs],                                           
        Null                                    AS [Pricing!7!RefinishTwoStageHourly],                                               
        Null                                    AS [Pricing!7!RefinishTwoStageMax],                                                  
        Null                                    AS [Pricing!7!RefinishTwoStageCCMaxHrs],                                             
        Null                                    AS [Pricing!7!R12EvacuateRecharge],                                                  
        Null                                    AS [Pricing!7!R12EvacuateRechargeFreon],                                             
        Null                                    AS [Pricing!7!R134EvacuateRecharge],                                                 
        Null                                    AS [Pricing!7!R134EvacuateRechargeFreon],                                            
        Null                                    AS [Pricing!7!SalesTaxPct],                                                          
        Null                                    AS [Pricing!7!StripePaintPerPanel],                                                  
        Null                                    AS [Pricing!7!StripePaintPerSide],                                                   
        Null                                    AS [Pricing!7!StripeTapePerPanel],                                                   
        Null                                    AS [Pricing!7!StripeTapePerSide],                                                    
        Null                                    AS [Pricing!7!TireMountBalance],                                                     
        Null                                    AS [Pricing!7!TowInChargeTypeCD],                                                    
        Null                                    AS [Pricing!7!TowInFlatFee],                                                         
        Null                                    AS [Pricing!7!Undercoat],                                                            
            
        -- Metadata Header
        NULL                                    AS [Metadata!8!Entity],
        
        -- Metadata
        NULL                                    AS [Column!9!Name],
        NULL                                    AS [Column!9!DataType],
        NULL                                    AS [Column!9!MaxLength],
        NULL                                    AS [Column!9!Precision],
        NULL                                    AS [Column!9!Scale],
        NULL                                    AS [Column!9!Nullable],
        
        --Reference
        Null                                    AS [Reference!10!ListName],
        Null                                    AS [Reference!10!ReferenceID],
        Null                                    AS [Reference!10!Name]

   UNION ALL

    --BusinessInfo Level
   SELECT
        2,
        1,
        --Root
        Null, Null, Null,
        
        --Business
        IsNull(FedTaxID, ''),
        IsNull(BusinessTypeCD, ''),
        
        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
        
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        Null,

        --Metadata
        Null, Null, Null, Null, Null, Null, 

        --Reference
        Null, Null, Null
        
    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip
    
    UNION ALL


    --ShopInfo Level
    SELECT
        3,
        1,
        
        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,
        
        --Shop
        IsNull(PreferredEstimatePackageID, ''),
        IsNull(Address1, ''),
        IsNull(Address2, ''),
        IsNull(AddressCity, ''),
        IsNull(AddressCounty, ''),
        IsNull(AddressState, ''),
        IsNull(l.AddressZip, ''),
        IsNull(l.CertifiedFirstId, ''),
        IsNull(l.DrivingDirections, ''),
        IsNull(EmailAddress, ''),
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        IsNull(FinalSubmitFlag, '0'),
        IsNull(convert(varchar(10), MaxWeeklyAssignments), ''),
        Name,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        IsNull(RepairFacilityTypeCD, ''),
        IsNull(WebSiteAddress, ''),
        IsNull(WarrantyPeriodRefinishCD, ''),
        IsNull(WarrantyPeriodWorkmanshipCD, ''),
        
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        Null,

        --Metadata
        Null, Null, Null, Null, Null, Null, 

        --Reference
        Null, Null, Null
        
    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip
    UNION ALL

    --Shop Hours Level
    SELECT 
        4,
        1,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,

        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        OperatingMondayStartTime, 
        OperatingMondayEndTime, 
        OperatingTuesdayStartTime, 
        OperatingTuesdayEndTime, 
        OperatingWednesdayStartTime, 
        OperatingWednesdayEndTime, 
        OperatingThursdayStartTime, 
        OperatingThursdayEndTime, 
        OperatingFridayStartTime,
        OperatingFridayEndTime, 
        OperatingSaturdayStartTime, 
        OperatingSaturdayEndTime, 
        OperatingSundayStartTime, 
        OperatingSundayEndTime, 
        
        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        Null,

        --Metadata
        Null, Null, Null, Null, Null, Null, 

        --Reference
        Null, Null, Null

    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip
    
    UNION ALL

    --Shop Personnel Level
    SELECT Top 1
        5,
        1,

        --Root
        Null, Null, Null,

        --Business
        Null, Null,
        
        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        -- Shop Personnel
        IsNULL(convert(varchar(5), SLPPersonnelID), ''),
        SLPPersonnelTypeID,
        IsNull(SLPPreferredContactMethodID, 0),
        SLPCellAreaCode,
        SLPCellExchangeNumber,
        SLPCellExtensionNumber,
        SLPCellUnitNumber,
        IsNull(SLPEmailAddress, ''),
        SLPFaxAreaCode,
        SLPFaxExchangeNumber,
        SLPFaxExtensionNumber,
        SLPFaxUnitNumber,
        IsNull(SLPGenderCD, ''),
        SLPName,
        SLPPagerAreaCode,
        SLPPagerExchangeNumber,
        SLPPagerExtensionNumber,
        SLPPagerUnitNumber,
        SLPPhoneAreaCode,
        SLPPhoneExchangeNumber,
        SLPPhoneExtensionNumber,
        SLPPhoneUnitNumber,
        IsNull(SLPShopManagerFlag, ''),
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        Null,
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip
            
    UNION ALL 
    
    --Business Personnel Level
    SELECT Top 1
        6,
        1,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,
        
        --ShopInfo
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        -- Business Personnel
        IsNULL(convert(varchar(5), SPPersonnelID), ''),
        SPPersonnelTypeID,
        IsNull(SPPreferredContactMethodID, 0),
        SPCellAreaCode,
        SPCellExchangeNumber,
        SPCellExtensionNumber,
        SPCellUnitNumber,
        IsNull(SPEmailAddress, ''),
        SPFaxAreaCode,
        SPFaxExchangeNumber,
        SPFaxExtensionNumber,
        SPFaxUnitNumber,
        IsNull(SPGenderCD, ''),
        SPName,
        SPPagerAreaCode,
        SPPagerExchangeNumber,
        SPPagerExtensionNumber,
        SPPagerUnitNumber,
        SPPhoneAreaCode,
        SPPhoneExchangeNumber,
        SPPhoneExtensionNumber,
        SPPhoneUnitNumber,
        IsNull(SPShopManagerFlag, ''),
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        Null,
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip 
    
  
    UNION ALL
  
    --Shop Pricing Level
    SELECT Top 1
        7,
        1,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,

        --ShopInfo
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        IsNull(convert(varchar(20), AlignmentFourWheel), ''),       
        IsNull(convert(varchar(20), AlignmentTwoWheel), ''),         
        IsNull(convert(varchar(20), CaulkingSeamSealer), ''),          
        IsNull(convert(varchar(20), ChipGuard), ''),                    
        IsNull(convert(varchar(20), CoolantGreen), ''),                 
        IsNull(convert(varchar(20), CoolantRed), ''),                   
        IsNull(convert(varchar(20), CorrosionProtection), ''),  
        IsNull(convert(varchar(20), convert(decimal(9, 1), CountyTaxPct)), ''), 
        IsNull(convert(varchar(20), CoverCar), ''), 
        IsNull(convert(varchar(20), DailyStorage), ''),     
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountDomestic)), ''),     
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountImport)), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountPctSideBackGlass)), ''),     
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountPctWindshield)), ''),       
        IsNull(convert(varchar(20), FlexAdditive),  ''), 
        IsNull(convert(varchar(20), GlassOutsourceServiceFee), ''),     
        IsNull(GlassReplacementChargeTypeCD, ''), 
        IsNull(convert(varchar(20), GlassSubletServiceFee), ''),        
        IsNull(convert(varchar(20), convert(decimal(9, 1), GlassSubletServicePct)), ''),        
        IsNull(convert(varchar(20), HazardousWaste), ''),              
        IsNull(convert(varchar(20), HourlyRateMechanical), ''), 
        IsNull(convert(varchar(20), HourlyRateRefinishing), ''),        
        IsNull(convert(varchar(20), HourlyRateSheetMetal), ''),         
        IsNull(convert(varchar(20), HourlyRateUnibodyFrame), ''),   
        IsNull(convert(varchar(20), convert(decimal(9, 1), MunicipalTaxPct)), ''),
        IsNull(OEMDiscounts, ''), 
        IsNull(convert(varchar(20), convert(decimal(9, 1), OtherTaxPct)), ''), 
        IsNull(convert(varchar(20), convert(decimal(9, 1), PartsRecycledMarkupPct)), ''),       
        IsNull(convert(varchar(20), PullSetUp), ''),                 
        IsNull(convert(varchar(20), PullSetUpMeasure), ''),      
        IsNull(convert(varchar(20), RefinishSingleStageHourly), ''),    
        IsNull(convert(varchar(20), RefinishSingleStageMax), ''),       
        IsNull(convert(varchar(20), RefinishThreeStageHourly), ''),     
        IsNull(convert(varchar(20), RefinishThreeStageMax), ''), 
        IsNull(convert(varchar(20), RefinishThreeStageCCMaxHrs), ''), 
        IsNull(convert(varchar(20), RefinishTwoStageHourly), ''),       
        IsNull(convert(varchar(20), RefinishTwoStageMax), ''),  
        IsNull(convert(varchar(20), RefinishTwoStageCCMaxHrs), ''), 
        IsNull(convert(varchar(20), R12EvacuateRecharge), ''),          
        IsNull(convert(varchar(20), R12EvacuateRechargeFreon), ''),     
        IsNull(convert(varchar(20), R134EvacuateRecharge), ''),         
        IsNull(convert(varchar(20), R134EvacuateRechargeFreon), ''),   
        IsNull(convert(varchar(20), convert(decimal(9, 3), SalesTaxPct)), ''),  
        IsNull(convert(varchar(20), StripePaintPerPanel), ''),          
        IsNull(convert(varchar(20), StripePaintPerSide), ''),           
        IsNull(convert(varchar(20), StripeTapePerPanel), ''),           
        IsNull(convert(varchar(20), StripeTapePerSide), ''),            
        IsNull(convert(varchar(20), TireMountBalance), ''),             
        IsNull(TowInChargeTypeCD, ''), 
        IsNull(convert(varchar(20), TowInFlatFee), ''),   
        IsNull(convert(varchar(20), Undercoat), ''),                   
           

        --Metadata Header
        Null,
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM (SELECT @CertifiedFirstID AS CertifiedFirstID, @AddressZip AS AddressZip) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.CertifiedFirstID = l.CertifiedFirstID
                                       AND parms.AddressZip = l.AddressZip    
  
    UNION ALL
    
    --Metadata Header Level
    SELECT DISTINCT
        8,
        1,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,

        --ShopInfo
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,
        
        --Metadata Header
        GroupName,
            
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM    @tmpMetadata

    UNION ALL

    --Metadata Level
    SELECT 
        9,
        8,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,

        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,  
        Null,

        --Metadata Header
        GroupName,

        -- Metadata
        ColumnName,
        DataType,
        MaxLength,
        NumericPrecision,
        Scale,
        Nullable,

        --Reference
        Null, Null, Null

    FROM    @tmpMetadata

    UNION ALL

    --Reference Level
    SELECT
        10,
        1,

        --Root
        Null, Null, Null,
        
        --Business
        Null, Null,

        --Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null,
            
        --Shop Hours
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null,

        --Shop Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Business Personnel
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL,
        
        --Pricing
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null,

        --Metadata Header
        'ZZ-Reference',
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        ListName,
        ReferenceID,
        Name

    FROM    @tmpReference
        
    ORDER BY [Metadata!8!Entity], Tag 
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)        
           
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount

GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTAllInfoGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTAllInfoGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
