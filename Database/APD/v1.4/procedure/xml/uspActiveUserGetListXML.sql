-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspActiveUserGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspActiveUserGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspActiveUserGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a list of Active users along with the assignment pool information
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspActiveUserGetListXML
    @ApplicationCD  udt_std_cd = 'APD'
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspActiveUserGetListXML'
    
/*************************************************************************************
*  Gather Reference Data
**************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50)             NOT NULL,
        ReferenceId     varchar(10)             NOT NULL,
        Name            varchar(50)             NOT NULL,
        FunctionCD      varchar(5)              NULL
    )

    INSERT INTO @tmpReference (ListName, ReferenceId, Name, FunctionCD)
    
    SELECT 'AssignmentPool' as ListName,
           AssignmentPoolID as ReferenceID,
           Name,
           FunctionCD
    FROM dbo.utb_assignment_pool
    WHERE EnabledFlag = 1
    ORDER BY DisplayOrder 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpUserList', 16, 1, @ProcName)
        RETURN
    END    


    DECLARE @tmpUserList TABLE
    (
        UserID      int
    )
    
    -- Get all active users for the application
    INSERT INTO @tmpUserList
      SELECT  UserID
        FROM  dbo.utb_user u
        WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
          AND u.UserID > 0
          AND u.EnabledFlag = 1
    
    
    -- Begin XML Select
    
    SELECT  1 AS Tag,
            NULL AS Parent,
            @ApplicationCD AS [Root!1!ApplicationCD],
            -- Active Users
            NULL AS [User!2!UserID],
            NULL AS [User!2!NameFirst],
            NULL AS [User!2!NameLast],
            NULL AS [User!2!SupervisorFlag],
            -- User Assignment Pools
            NULL AS [UserPool!3!AssignmentPoolID],
            -- Reference Data
            NULL as [Reference!4!List],
            NULL as [Reference!4!ReferenceID],
            NULL as [Reference!4!Name],
            NULL as [Reference!4!FunctionCD]

    UNION ALL
    
    SELECT  2,
            1,
            NULL,
            -- Active Users
            u.UserID, 
            u.NameFirst, 
            u.NameLast, 
            u.SupervisorFlag,
            -- User Assignment Pools
            NULL, 
            -- Reference Data
            NULL, NULL, NULL, NULL
    FROM  @tmpUserList tmp
    LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)

    UNION ALL
    
    SELECT  3,
            2,
            NULL,
            -- Active Users
            tmp.UserID, 
            NULL, NULL, NULL,
            -- User Assignment Pools
            apu.AssignmentPoolID,
            -- Reference Data
            NULL, NULL, NULL, NULL
    FROM  @tmpUserList tmp
    INNER JOIN dbo.utb_assignment_pool_user apu ON (tmp.UserID = apu.UserID)
    
    UNION ALL
    
    SELECT  4,
            1,
            NULL,
            -- Active Users
            NULL, NULL, NULL, NULL,
            -- User Assignment Pools
            NULL,
            -- Reference Data
            ListName, 
            ReferenceID, 
            Name, 
            FunctionCD
    FROM  @tmpReference
    
    ORDER BY [User!2!UserID], tag

/*
    SELECT  2,
            1,
            NULL,
            -- Active Users
            NULL, NULL, NULL, NULL,
            -- User Assignment Pools
            NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL
*/    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspActiveUserGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspActiveUserGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/