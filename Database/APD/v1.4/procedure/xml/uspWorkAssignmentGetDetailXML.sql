-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkAssignmentGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkAssignmentGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspWorkAssignmentGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Madhavi Kotipalli]
* FUNCTION:     [This proc is used to return data related to user work assignment window]
*
* PARAMETERS:  
* (I) @UserID              
* (I) @ApplicationCD
* (I) @InsuranceCompanyID
*
* RESULT SET:
* User Detail Information as a XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspWorkAssignmentGetDetailXML]
(
    @UserID             udt_std_id      = NULL,
    @ApplicationCD      udt_std_cd      = 'APD',
    @InsuranceCompanyID udt_std_id      = NULL
)
AS
BEGIN
    DECLARE @ApplicationID      udt_std_id   
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspWorkAssignmentGetDetailXML'


    -- Check to make sure a valid User id was passed in
    IF  @UserID IS NOT NULL
    BEGIN
        IF (NOT @UserID = -1)    AND 
           (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
        BEGIN
            -- Invalid User ID

            RAISERROR('101|%s|@UserID|%s', 16, 1, @ProcName, @UserID)
            RETURN
        END
    END
    ELSE
    BEGIN
   
       SET @UserID = -1
    
    END    


    SELECT  @ApplicationID = ApplicationID 
      FROM  dbo.utb_application 
      WHERE Code = @ApplicationCD
      
    IF @ApplicationID IS NULL
    BEGIN
        -- Invalid ApplicationCD
   
        RAISERROR  ('101|%s|@ApplicationCD|%s', 16, 1, @ProcName, @ApplicationCD)
        RETURN
    END


    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    --User table
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'User',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_user' AND Column_Name IN 
            ('UserID',
             'AssignmentBeginDate',
             'AssignmentEndDate',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'OfficeID',
             'OperatingMondayStartTime',
             'OperatingMondayEndTime',
             'OperatingTuesdayStartTime',
             'OperatingTuesdayEndTime',
             'OperatingWednesdayStartTime',
             'OperatingWednesdayEndTime',
             'OperatingThursdayStartTime',
             'OperatingThursdayEndTime',
             'OperatingFridayStartTime',
             'OperatingFridayEndTime',
             'OperatingSaturdayStartTime',
             'OperatingSaturdayEndTime',
             'OperatingSundayStartTime',
             'OperatingSundayEndTime',
             'SysLastUserID',
             'SysLastUpdatedDate'
            ))

    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'User Application',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_user_application' AND Column_Name IN 
            ('UserID',
             'ApplicationID',
             'AccessBeginDate',
             'AccessEndDate',
             'LogonId',
             'SysLastUserID',
             'SysLastUpdatedDate'
            ))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,  
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        ProfileID           int         NULL,
        BusinessFunctionCD  varchar(4)  NULL,
        InsuranceCompanyID  int         NULL,
        ClientOfficeId      varchar(50) NULL,
        FunctionCD			varchar(6)  NULL
    )
    
    
    -- Select All reference information for all pertinent reference tables and store in the temporary table
    
   
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, ProfileID, BusinessFunctionCD, InsuranceCompanyId, ClientOfficeId, FunctionCD)

    SELECT  'Application'  AS ListName,
            DisplayOrder,
            CAST(ApplicationID AS Varchar),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_application
    WHERE   EnabledFlag = 1

    UNION ALL
    
    SELECT  'BusinessFunction',
            NULL,
            Code ,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_role', 'BusinessFunctionCD' )

    UNION ALL

    SELECT  'InsuranceCompany' AS ListName,
            NULL,
            CAST(InsuranceCompanyID AS Varchar),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_insurance
    WHERE   EnabledFlag = 1

    UNION ALL

    SELECT  'Office' AS ListName,
            NULL,
            CAST(OfficeID AS Varchar),
            Name,
            NULL,
            NULL,
            InsuranceCompanyID,
            ClientOfficeId,
            NULL
    FROM    dbo.utb_office
    WHERE   EnabledFlag = 1

    UNION ALL
    
    SELECT  'ProfileSelection',
            ps.DisplayOrder,
            CAST(ps.SelectionID AS Varchar),
            ps.Name,
            ps.ProfileID,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_profile_selection ps
    INNER JOIN dbo.utb_application_profile ap ON (ps.ProfileID = ap.ProfileID)
    LEFT JOIN dbo.utb_application a ON (ap.ApplicationID = a.ApplicationID)
    WHERE   ps.EnabledFlag = 1
      AND   a.Code = @ApplicationCD
  
    UNION ALL

    SELECT  'AssignmentPool' AS ListName,
            DisplayOrder,
            CAST(AssignmentPoolID AS Varchar),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            FunctionCD
    FROM    dbo.utb_assignment_pool
    WHERE   EnabledFlag = 1

    ORDER BY ListName, DisplayOrder



    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
   

    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            @UserID AS [Root!1!UserID],
            @ApplicationCD AS [Root!1!ApplicationCD],
            -- User Info
            NULL AS [User!2!UserID],
            NULL AS [User!2!NameTitle],
            NULL AS [User!2!NameFirst],
            NULL AS [User!2!NameLast],
            NULL AS [User!2!OfficeID],
            NULL AS [User!2!InsuranceCompanyID],
            NULL AS [User!2!OperatingMondayStartTime],
            NULL AS [User!2!OperatingMondayEndTime],
            NULL AS [User!2!OperatingTuesdayStartTime],
            NULL AS [User!2!OperatingTuesdayEndTime],
            NULL AS [User!2!OperatingWednesdayStartTime],
            NULL AS [User!2!OperatingWednesdayEndTime],
            NULL AS [User!2!OperatingThursdayStartTime],
            NULL AS [User!2!OperatingThursdayEndTime],
            NULL AS [User!2!OperatingFridayStartTime],
            NULL AS [User!2!OperatingFridayEndTime],
            NULL AS [User!2!OperatingSaturdayStartTime],
            NULL AS [User!2!OperatingSaturdayEndTime],
            NULL AS [User!2!OperatingSundayStartTime],
            NULL AS [User!2!OperatingSundayEndTime],
            NULL AS [User!2!AssignmentBeginDate],
            NULL AS [User!2!AssignmentEndDate],
            NULL AS [User!2!LastAssignmentDate],
            NULL AS [User!2!ReceiveCCCOneAssignmentFlag],
            NULL AS [User!2!SysLastUpdatedDate],
            -- User Application Info
            NULL AS [Application!3!ApplicationID],
            NULL AS [Application!3!AccessBeginDate],
            NULL AS [Application!3!AccessEndDate],
            NULL AS [Application!3!LogonId],
            NULL AS [Application!3!PasswordHint],
            NULL AS [Application!3!SysLastUpdatedDate],
            -- User Insurance Info
            NULL AS [Insurance!4!InsuranceCompanyID],    
            --States
            NULL AS [States!5!StateCode],
            NULL AS [States!5!StateValue],
            NULL AS [States!5!AnalystLicenseRequired],
            NULL AS [States!5!OwnerLicenseRequired],
            NULL AS [States!5!LicenseFlag],
            NULL AS [States!5!AssignWorkFlag],
            --User Profile
            NULL AS [Profile!6!ProfileID],
            NULL AS [Profile!6!DisplayOrder!hide],
            NULL AS [Profile!6!Show],
            NULL AS [Profile!6!BusinessFunctionCD],
            NULL AS [Profile!6!DataTypeCD],
            NULL AS [Profile!6!OverriddenFlag],
            NULL AS [Profile!6!ProfileName],
            NULL AS [Profile!6!ProfileValue],
            NULL AS [Profile!6!DefaultProfileValue],
            -- Assignment Pool
            NULL AS [AssignmentPool!7!AssignmentPoolID],
            -- Metadata Header
            NULL AS [Metadata!8!Entity],
            -- Columns
            NULL AS [Column!9!Name],
            NULL AS [Column!9!DataType],
            NULL AS [Column!9!MaxLength],
            NULL AS [Column!9!Precision],
            NULL AS [Column!9!Scale],
            NULL AS [Column!9!Nullable],
            -- Reference Data
            NULL AS [Reference!10!List],
            NULL AS [Reference!10!ReferenceID],
            NULL AS [Reference!10!Name],
            NULL AS [Reference!10!ProfileID],
            NULL AS [Reference!10!BusinessFunctionCD],
            NULL AS [Reference!10!InsuranceCompanyID],
            NULL AS [Reference!10!ClientOfficeId],
            NULL AS [Reference!10!FunctionCD]


    UNION ALL


    --User Info
    SELECT  2,
            1,
            NULL, NULL,
            -- User Info
            u.UserID,
            LTrim(RTrim(isNull(u.NameTitle, ''))),
            LTrim(RTrim(isNull(u.NameFirst, ''))),
            LTrim(RTrim(isNull(u.NameLast, ''))),
            IsNull(u.OfficeID, 0),
            IsNull(o.InsuranceCompanyID, 0),
            IsNull(u.OperatingMondayStartTime,''),
            IsNull(u.OperatingMondayEndTime,''),
            IsNull(u.OperatingTuesdayStartTime,''),
            IsNull(u.OperatingTuesdayEndTime,''),
            IsNull(u.OperatingWednesdayStartTime,''),
            IsNull(u.OperatingWednesdayEndTime,''),
            IsNull(u.OperatingThursdayStartTime,''),
            IsNull(u.OperatingThursdayEndTime,''),
            IsNull(u.OperatingFridayStartTime,''),
            IsNull(u.OperatingFridayEndTime,''),
            IsNull(u.OperatingSaturdayStartTime,''),
            IsNull(u.OperatingSaturdayEndTime,''),
            IsNull(u.OperatingSundayStartTime,''),
            IsNull(u.OperatingSundayEndTime,''),
            IsNull(dbo.ufnUtilityGetDateString(u.AssignmentBeginDate), ''),
            IsNull(dbo.ufnUtilityGetDateString(u.AssignmentEndDate), ''),
            dbo.ufnUtilityGetDateString(u.LastAssignmentDate),
            IsNull(u.ReceiveCCCOneAssignmentFlag, 0),
            dbo.ufnUtilityGetDateString(u.SysLastUpdatedDate),
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM (select @UserID as UserID) AS parms
      LEFT JOIN dbo.utb_user u ON (parms.UserID = u.userid)
      LEFT JOIN dbo.utb_office o ON (u.OfficeID = o.OfficeID)


    UNION ALL


    -- User Applications
    SELECT  3,
            2,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            ua.ApplicationID,
            IsNull(dbo.ufnUtilityGetDateString(ua.AccessBeginDate), ''),
            IsNull(dbo.ufnUtilityGetDateString(ua.AccessEndDate), ''),
            IsNull(ua.LogonId, ''),
            IsNull(ua.PasswordHint, ''),
            dbo.ufnUtilityGetDateString(ua.SysLastUpdatedDate),
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM (select @UserID as UserID) AS parms
      LEFT JOIN dbo.utb_user_application ua ON (parms.UserID = ua.userid)


    UNION ALL

    -- User Insurance
    SELECT  4,
            2,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            InsuranceCompanyID,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM (select @UserID as UserID) AS parms
      LEFT JOIN dbo.utb_user_insurance ui ON (parms.UserID = ui.userid)
    

    UNION ALL


    -- User License States
    SELECT  5,
            2,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            sc.StateCode, 
            StateValue, 
            LicenseRequiredAnalystFlag, 
            LicenseRequiredOwnerFlag, 
            isNull(us.LicenseFlag, 0), 
            isNull(us.AssignWorkFlag, 0),
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM utb_state_code sc
         LEFT JOIN dbo.utb_user_state us ON (us.StateCode = sc.StateCode and UserID = @UserID)
      WHERE EnabledFlag = 1


    UNION ALL


    -- User Profile
    SELECT  6,
            2,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            p.ProfileID,
            p.DisplayOrder,
            CASE
                WHEN EXISTS (SELECT ap.ProfileID 
                             FROM   dbo.utb_application_profile ap
                             WHERE  ap.ProfileID = p.ProfileID 
                             AND    ap.ApplicationID = @ApplicationID)     THEN 1
                ELSE 0
            END,             
            IsNull(LTrim(RTrim(p.BusinessFunctionCD)), ''),
            p.DataTypeCD,
            CASE
                WHEN EXISTS (SELECT sup.ProfileID 
                             FROM   dbo.utb_user_profile sup
                             WHERE  sup.UserID = @UserID 
                             AND    sup.ProfileID = p.ProfileID)     THEN 1
                ELSE 0
            END, 
            p.Name, 
            dbo.ufnUtilityGetProfileValue( @UserID, DEFAULT, p.ProfileID, DEFAULT),
            dbo.ufnUtilityGetProfileValue( @UserID, DEFAULT, p.ProfileID, 1),   -- 1 = ignore overrides for default
            -- Assignment Pool
            NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    dbo.utb_profile p 
    WHERE p.EnabledFlag = 1


   UNION ALL


    -- Select Work Assignment Pool
    SELECT DISTINCT 7,
            2,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            apu.AssignmentPoolID,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM (select @UserID as UserID) AS parms
      LEFT JOIN dbo.utb_assignment_pool_user apu ON (parms.UserID = apu.UserID)


   UNION ALL


    -- Select Metadata Header Level
    SELECT DISTINCT 8,
            1,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  9,
            8,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  10,
            1,
            NULL, NULL,
            -- User Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- User Application Info
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Insurance Info
            NULL,
            --States with User License info
            NULL, NULL, NULL, NULL, NULL, NULL,
            --User Profile
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Assignment Pool
            NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            ProfileID,
            BusinessFunctionCD,
            InsuranceCompanyID,
            ClientOfficeId,
			FunctionCD

    FROM    @tmpReference


    ORDER BY [Metadata!8!Entity], tag, [Profile!6!DisplayOrder!hide]
--    FOR XML EXPLICIT  (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkAssignmentGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkAssignmentGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
