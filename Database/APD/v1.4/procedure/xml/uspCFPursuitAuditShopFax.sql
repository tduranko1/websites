-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFPursuitAuditShopFax' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFPursuitAuditShopFax 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCFPursuitAuditShopFax
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCFPursuitAuditShopFax
    @ClaimAspectID      udt_std_id_big,
    @UserID             udt_std_id_big
AS
BEGIN
    -- Declare internal variables
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @InsuranceCompanyID as int
    DECLARE @ClaimNumber as varchar(50)
    DECLARE @ClaimRepName as varchar(100)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)
    
    DECLARE @ClaimRepTitle as varchar(100)
    DECLARE @ClaimRepPhone as varchar(15)
    DECLARE @ClaimRepEmail as varchar(75)
    DECLARE @ClaimAspectNumber as tinyint
    DECLARE @VehicleYear as smallint
    DECLARE @VehicleMake as varchar(50)
    DECLARE @VehicleModel as varchar(50)
    DECLARE @VehicleVIN as varchar(50)
    DECLARE @VehicleTag as varchar(50)
    DECLARE @VehicleMileage as varchar(50)
    DECLARE @ServerDateTime as varchar(25)
    DECLARE @SourceApplicationPassThruData as varchar(1000)
    DECLARE @LossDate as varchar(20)
    DECLARE @Party as varchar(10)
    DECLARE @Deductible as varchar(10)
    DECLARE @VehicleOwnerName as varchar(1000)
    DECLARE @VehicleOwnerAddress as varchar(100)
    DECLARE @VehicleOwnerAddressCity as varchar(100)
    DECLARE @VehicleOwnerAddressState as varchar(100)
    DECLARE @VehicleOwnerAddressZip as varchar(100)
    DECLARE @VehicleOwnerPhone as varchar(100)
    DECLARE @VehicleOwnerEmail as varchar(100)
    DECLARE @UploadLink as varchar(1000)
    
    DECLARE @LynxID as bigint

    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFPursuitAuditShopFax'

    SET @ServerDateTime = dbo.ufnUtilityFormatDate(current_timestamp, 'MM/dd/yyy hh:nn tt')

    -- Check to make sure a valid ClaimAspectID was passed in
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspectID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    -- Check to make sure a valid user id was passed in
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    SELECT @ClaimRepName = isNull(uc.NameFirst + ' ' + uc.NameLast, ''),
           @ClaimRepTitle = 'LYNX APD Audit Services Representative',
           @ClaimRepPhone = '(' + uc.PhoneAreaCode + ') ' + uc.PhoneExchangeNumber + ' ' + uc.PhoneUnitNumber,
           @ClaimRepEmail = LTRIM(isNull(uc.EmailAddress, '')),
           @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
           @AuditorTitle = 'Quality Control Representative',
           @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
           @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
           @ClaimAspectNumber = ca.ClaimAspectNumber,
           @SourceApplicationPassThruData = ca.SourceApplicationPassThruData,
           @LossDate = isNull(convert(varchar, c.LossDate, 101), ''),
           @Party = CASE 
                        WHEN ca.ExposureCD = 1 THEN '1st Party'
                        WHEN ca.ExposureCD = 3 THEN '3rd Party'
                        ELSE 'NA'
                    END,
            @LynxID = ca.LynxID
    FROM utb_claim_aspect ca 
    LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
    LEFT JOIN utb_user uc ON ca.OwnerUserID = uc.UserID
    INNER JOIN utb_claim c ON ca.LynxID = c.LynxID
    WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT  @Deductible = isNull(convert(varchar, cc.DeductibleAmt), '')
    FROM utb_claim_aspect ca
    LEFT JOIN (select Code, Name 
               from dbo.ufnUtilityGetReferenceCodes('utb_client_coverage_type', 'CoverageProfileCD')) tmpRef ON ca.CoverageProfileCD = tmpRef.Code
    LEFT JOIN utb_client_coverage_type cct ON ca.CoverageProfileCD = cct.CoverageProfileCD
    LEFT JOIN utb_claim_coverage cc ON cct.ClientCoverageTypeID = cc.ClientCoverageTypeID AND ca.LynxID = cc.LynxID
    WHERE ca.ClaimAspectID = @ClaimAspectID
      AND ca.EnabledFlag = 1
      AND cc.EnabledFlag = 1

    SELECT	@VehicleOwnerName = CASE
                                    WHEN i.BusinessName IS NOT NULL OR
                                         LEN(i.BusinessName) > 0 THEN i.BusinessName
                                    ELSE LTrim(RTrim(IsNull(i.NameFirst, ''))) + ' ' + LTrim(RTrim(IsNull(i.NameLast, '')))
                                END,
            @VehicleOwnerAddress = LTrim(RTrim(IsNull(i.Address1, ''))),
            @VehicleOwnerAddressCity = LTrim(RTrim(IsNull(i.AddressCity, ''))),
            @VehicleOwnerAddressState = LTrim(RTrim(IsNull(i.AddressState, ''))),
			   @VehicleOwnerAddressZip = LTrim(RTrim(IsNull(i.AddressZip, ''))),
            @VehicleOwnerPhone = IsNull(LTRIM(RTRIM('(' + i.DayAreaCode + ') ' + i.DayExchangeNumber + '-' + i.DayUnitNumber)), '')
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Owner'
    
    SELECT @UploadLink = ''
    
    DECLARE @PursuitAuditShopInfo TABLE
    (
        Position    int,
        Value       varchar(1000)
    )
    
    DECLARE @delimiter as varchar(1)
    DECLARE @RepairLocationName as varchar(1000)
    DECLARE @RepairLocationAddress as varchar(1000)
    DECLARE @RepairLocationCity as varchar(1000)
    DECLARE @RepairLocationState as varchar(1000)
    DECLARE @RepairLocationZip as varchar(1000)
    DECLARE @RepairLocationPhone as varchar(1000)
    DECLARE @RepairLocationFax as varchar(1000)
    DECLARE @RepairLocationEmail as varchar(1000)
    DECLARE @RepairInspectionDate as varchar(1000)

    SET @delimiter = char(255)
    
    INSERT INTO @PursuitAuditShopInfo
    SELECT strIndex, Value
    FROM dbo.ufnUtilityParseString(@SourceApplicationPassThruData, @delimiter, 1)
    
    IF (SELECT Count(*) FROM @PursuitAuditShopInfo) = 10
    BEGIN
        SELECT @RepairLocationName = Value FROM @PursuitAuditShopInfo WHERE Position = 1
        SELECT @RepairLocationAddress = Value FROM @PursuitAuditShopInfo WHERE Position = 2
        SELECT @RepairLocationCity = Value FROM @PursuitAuditShopInfo WHERE Position = 4
        SELECT @RepairLocationState = Value FROM @PursuitAuditShopInfo WHERE Position = 5
        SELECT @RepairLocationZip = Value FROM @PursuitAuditShopInfo WHERE Position = 3
        SELECT @RepairLocationPhone = Value FROM @PursuitAuditShopInfo WHERE Position = 7
        SELECT @RepairLocationFax = Value FROM @PursuitAuditShopInfo WHERE Position = 8
        SELECT @RepairLocationEmail = Value FROM @PursuitAuditShopInfo WHERE Position = 9
        SELECT @RepairInspectionDate = Value FROM @PursuitAuditShopInfo WHERE Position = 10
        
        --IF LEN(@RepairLocationPhone) > 0
        --BEGIN
        --    SET @RepairLocationPhone = '(' + substring(@RepairLocationPhone, 1, 3) + ') ' + 
        --                                substring(@RepairLocationPhone, 4, 3) + ' ' + 
        --                                substring(@RepairLocationPhone, 7, 4)
        --END
        --IF LEN(@RepairLocationFax) > 0
        --BEGIN
        --    SET @RepairLocationFax = '(' + substring(@RepairLocationFax, 1, 3) + ') ' + 
        --                                substring(@RepairLocationFax, 4, 3) + ' ' + 
        --                                substring(@RepairLocationFax, 7, 4)
        --END
    END

    SELECT @InsuranceCompanyName = i.Name,
           @InsuranceCompanyID = i.InsuranceCompanyID,
           @ClaimNumber = c.ClientClaimNumber
    FROM utb_claim c 
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID


    SELECT @VehicleYear = isNull(cv.VehicleYear, ''),
           @VehicleMake = isNull(cv.Make, ''),
           @VehicleModel = isNull(cv.Model, ''),
           @VehicleVIN = isNull(VIN, ''),
           @VehicleTag = isNull(LicensePlateNumber, ''),
           @VehicleMileage = isNull(convert(bigint, Mileage), '')
    FROM utb_claim_vehicle cv
    WHERE cv.ClaimAspectID = @ClaimAspectID
    
    IF @VehicleMileage = '0' SET @VehicleMileage = ''

    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            @LynxID AS [Root!1!LynxID],
            @ClaimAspectNumber AS [Root!1!ClaimAspectNumber],
            @ClaimNumber AS [Root!1!ClaimNumber],
            @LossDate AS [Root!1!DateOfLoss],
            @Party AS [Root!1!Party],
            @Deductible AS [Root!1!Deductible],
            @ClaimRepName AS [Root!1!ClaimRepresentative],
            @AuditorName AS [Root!1!AuditorName],
            @AuditorTitle AS [Root!1!AuditorTitle],
            @AuditorPhone AS [Root!1!AuditorPhone],
            @AuditorEmail AS [Root!1!AuditorEmail],
            @ClaimRepName AS [Root!1!ClaimRepName],
            @ClaimRepTitle AS [Root!1!ClaimRepTitle],
            @ClaimRepPhone AS [Root!1!ClaimRepPhone],
            @ClaimRepEmail AS [Root!1!ClaimRepEmail],
            @VehicleYear AS [Root!1!VehicleYear],
            @VehicleMake AS [Root!1!VehicleMake],
            @VehicleModel AS [Root!1!VehicleModel],
            @VehicleVIN AS [Root!1!VehicleVIN],
            @VehicleTag AS [Root!1!VehicleTag],
            @VehicleMileage AS [Root!1!VehicleMileage],
            @ServerDateTime AS [Root!1!ReportDate],
            @RepairLocationName as [Root!1!RepairLocationName],
            @RepairLocationAddress as [Root!1!RepairLocationAddress],
            @RepairLocationCity as [Root!1!RepairLocationCity],
            @RepairLocationState as [Root!1!RepairLocationState],
            @RepairLocationZip as [Root!1!RepairLocationZip],
            @RepairLocationPhone as [Root!1!RepairLocationPhone],
            @RepairLocationFax as [Root!1!RepairLocationFax],
            @RepairLocationEmail as [Root!1!RepairLocationEmail],
            @RepairInspectionDate as [Root!1!RepairInspectionDate],
            @VehicleOwnerName as [Root!1!VehicleOwnerName],
            @VehicleOwnerAddress as [Root!1!VehicleOwnerAddress],
            @VehicleOwnerAddressCity as [Root!1!VehicleOwnerAddressCity],
            @VehicleOwnerAddressState as [Root!1!VehicleOwnerAddressState],
            @VehicleOwnerAddressZip as [Root!1!VehicleOwnerAddressZip],
            @VehicleOwnerPhone as [Root!1!VehicleOwnerPhone]


    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFPursuitAuditShopFax' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFPursuitAuditShopFax TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/