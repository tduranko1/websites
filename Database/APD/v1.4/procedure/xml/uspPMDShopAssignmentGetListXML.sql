-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDShopAssignmentGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPMDShopAssignmentGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspPMDShopAssignmentGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspPMDShopAssignmentGetListXML
    @ShopLocationID     udt_std_id,
    @BeginDate          udt_std_DateTime = NULL,
    @EndDate            udt_std_DateTime = NULL,
    @AssignmentCode     udt_std_cd = 'B',
    @UserID             udt_std_id         
AS
BEGIN
    DECLARE @ProcName                           udt_std_name 
    DECLARE @Now                                udt_std_datetime
    DECLARE @StatRangeBeginDate                 udt_std_datetime
    DECLARE @WorkingHours                       Float
    DECLARE @VehicleClosedStatusID              udt_std_int
    DECLARE @ViewCRD                            udt_std_flag
    DECLARE @ViewSMD                            udt_std_flag
    DECLARE @ShopBusinessID                     udt_std_id_big
    DECLARE @ShopName                           udt_std_name
    DECLARE @EstimateDocumentTypeID             udt_std_int
    DECLARE @LaborTotalTypeID                   udt_std_int
    DECLARE @RepairTotalID                      udt_std_int
    DECLARE @BettermentID                       udt_std_int
    DECLARE @PartsTotalTypeID                   udt_std_int
    DECLARE @OEMPartsTypeID                     udt_std_int
    DECLARE @LKQPartsTypeID                     udt_std_int
    DECLARE @AFMKPartsTypeID                    udt_std_int
    DECLARE @RecycledPartsTypeID                udt_std_int
    DECLARE @LaborBodyID                        udt_std_int
    DECLARE @LaborRefinishID                    udt_std_int
    DECLARE @LaborMechanicalID                  udt_std_int
    DECLARE @LaborFrameID                       udt_std_int
    DECLARE @RefinishMaterialsID                udt_std_int
    DECLARE @TaxTotalID                         udt_std_int
    DECLARE @ShopProfileBodyRate                money
    DECLARE @ShopProfileRefinishRate            money
    DECLARE @ShopProfileMechanicalRate          money
    DECLARE @ShopProfileFrameRate               money
    DECLARE @ShopProfileRefinishMaterials       money
    DECLARE @ShopProfileTaxRate                 money
    DECLARE @MktBodyRate                        money
    DECLARE @MktRefinishRate                    money
    DECLARE @MktMechanicalRate                  money
    DECLARE @MktFrameRate                       money
    DECLARE @MktRefinishMaterials               money   
    DECLARE @MktTaxRate                         money
    DECLARE @LastReIDate                        datetime
    
        
    --Vars 
    DECLARE @MaxDateRange   as int
    DECLARE @DayAdded       as Bit
    DECLARE @AtoE_90        as Float      --Average Assignment to Estimate w/in 90 days
    DECLARE @AtoE_180       as Float      --Average Assignment to Estimate w/in 180 days

    DECLARE @RepairDays_90    as int      --Average Repair days w/in 90 days
    DECLARE @RepairDays_180   as int      --Average Repair days w/in 180 days

    DECLARE @EstimateHrs_90   as Float    --Average Estimate Hrs w/in 90 days
    DECLARE @EstimateHrs_180  as Float    --Average Estimate Hrs w/in 180 days

    DECLARE @Assignments90    as int      --Count Assignments w/in 90 days
    DECLARE @Assignments180   as Float    --Count Assignments w/in 180 days
    
    DECLARE @Date90Ago        as datetime
    DECLARE @Date180Ago       as datetime

    --Shop working hours.  Right now we're assuming 10 hours per shop, but this can
    --easily be changed by including a field in utb_shop_location, or making this a var in
    --utb_app_variable
    
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF
    
    IF (@ShopLocationID IS NULL) OR (NOT EXISTS (SELECT ShopLocationID FROM dbo.utb_shop_location 
                                                                    WHERE ShopLocationID = @ShopLocationID))
    BEGIN
      -- Invalid Shop Location ID 
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END
    
        
    IF (@UserID IS NULL) OR (NOT EXISTS (SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
      -- Invalid Shop Location ID 
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END    
    

    SET @WorkingHours = 10.0
    
    SET @ProcName = 'uspPMDShopAssignmentGetListXML'
    SET @Now = current_timestamp
    SET @VehicleClosedStatusID = (SELECT StatusID FROM dbo.utb_status WHERE Name = 'Vehicle Closed')
    IF @AssignmentCode IS NULL SET @AssignmentCode = 'B'
    
    -- Get the shop name
    SET @AssignmentCode = UPPER(@AssignmentCode)
    SELECT @ShopName = Name
    FROM dbo.utb_shop_location
    WHERE ShopLocationID = @ShopLocationID
    
    IF @AssignmentCode <> 'O' AND @AssignmentCode <> 'C' AND @AssignmentCode <> 'B'
    BEGIN
       -- Missing Assignment Code
        RAISERROR('101|%s|@AssignmentCode|%s', 16, 1, @ProcName, @AssignmentCode)
        RETURN
    END
    
    SET @Date90Ago = DateAdd(day, -90, @Now)
    SET @Date180Ago = DateAdd(day, -180, @Now)
    
    IF @BeginDate IS NULL 
    BEGIN
        SET @BeginDate = DATEADD(month, -6, @Now)
    END

    IF @EndDate IS NULL 
    BEGIN
        SET @EndDate = @Now
    END
    
    
    SET @BeginDate = CONVERT(varchar, @BeginDate, 101) + ' 00:00:00'
    SET @EndDate = CONVERT(varchar, @EndDate, 101) + ' 23:59:59'
    
    SELECT @EstimateDocumentTypeID = DocumentTypeID
    FROM dbo.utb_document_type
    WHERE Name = 'Estimate'
    
    SELECT @LaborTotalTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'LaborTotal'
      AND CategoryCD = 'TT'
    
    SELECT @PartsTotalTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'PartsTotal'
      AND CategoryCD = 'TT'

    SELECT @RepairTotalID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'RepairTotal'
      AND CategoryCD = 'TT'

    SELECT @BettermentID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Betterment'
      AND CategoryCD = 'AJ'

    SELECT @OEMPartsTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'OEMParts'

    SELECT @LKQPartsTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'LKQParts'

    SELECT @AFMKPartsTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'AFMKParts'

    SELECT @RecycledPartsTypeID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'RemanParts'

    SELECT @LaborBodyID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Body'
      AND CategoryCD = 'LC'

    SELECT @LaborMechanicalID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Mechanical'
      AND CategoryCD = 'LC'

    SELECT @LaborFrameID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Frame'
      AND CategoryCD = 'LC'

    SELECT @RefinishMaterialsID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Paint'
      AND CategoryCD = 'MC'

    SELECT @LaborRefinishID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'Refinish'
      AND CategoryCD = 'LC'

    SELECT @TaxTotalID = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'TaxTotal'
      AND CategoryCD = 'TT'

    SELECT @ViewCRD = ReadFlag 
    FROM dbo.ufnUtilityGetCRUD(@UserID, 'Action:View Claim Rep Desktop', 0, 0)
    
    SELECT @ViewSMD = ReadFlag 
    FROM dbo.ufnUtilityGetCRUD(@UserID, 'Action:View Shop Maintenance Desktop', 0, 0)
    
    SELECT @ShopBusinessID = ShopID 
    FROM dbo.utb_shop_location 
    WHERE ShopLocationID = @ShopLocationID
    
    SELECT @LastReIDate = isNull(max(ReinspectionDate), '')
    FROM dbo.utb_reinspect
    WHERE ShopLocationID = @ShopLocationID

    
--    print convert(varchar, @RepairTotalID)
--    print convert(varchar, @BettermentID)
    
    DECLARE @tmpAssignment TABLE
    (
        ShopLocationID      bigint,
        ClaimAspectID       bigint,
        StatusID            int,
        LynxID              bigint,
        ClaimAspectNumber   tinyInt,
        LastAssignmentDate  datetime    NULL,
        AssignmentDate      datetime    NULL,
        DaysSinceAssignment int         NULL,
        AssignmentID        bigint      NULL,
        ReinspectionFlag    bit         NULL,
        VehicleYear         int         NULL,
        VehicleMake         varchar(50) NULL,
        VehicleModel        varchar(50) NULL,
        ExposureCD          varchar(4)  NULL,
        DriveableFlag       bit         NULL,
        OwnerNameFirst      varchar(50) NULL,
        OwnerNameLast       varchar(50) NULL,
        OwnerNameBusiness   varchar(100) NULL,
        InsuranceCoName     varchar(100) NULL,
        LastSuppDocID       bigint      NULL,
        OriginalEstDocID    bigint      NULL,
        OriginalEstDate     datetime    NULL,
        RepairStartDate     datetime    NULL,
        RepairEndDate       datetime    NULL,
        EstimateHours       decimal(5,2) NULL,
        OriginalEstTotal    decimal(9,2) NULL,
        LastSuppTotal       decimal(9,2) NULL,
        OriginalEstGross    decimal(9,2) NULL,
        TotalLaborCost      decimal(9,2) NULL,
        OEMPartsCost        decimal(9,2) NULL,
        LKQPartsCost        decimal(9,2) NULL,
        AFMKPartsCost       decimal(9,2) NULL,
        RecycledPartsCost   decimal(9,2) NULL,        
        LaborPercentRepair  decimal(5,2) NULL,
        OEMPartsPercent     decimal(5,2) NULL,
        LKQPartsPercent     decimal(5,2) NULL,
        AFMKPartsPercent    decimal(5,2) NULL,
        RecycledPartsPercent decimal(5,2) NULL,
        --ReinspectDocID      bigint       NULL,
        ReinspectionDate    datetime     NULL,
        ReinspectionType    varchar(50)  NULL,
        SatisfactionPercent decimal(5,4) NULL
    )

    DECLARE @tmpAssignmentHistoric TABLE
    (
        ShopLocationID      bigint,
        ClaimAspectID       bigint,
        StatusID            int,
        LynxID              bigint,
        ClaimAspectNumber   tinyInt,
        AssignmentDate      datetime    NULL,
        DaysSinceAssignment int         NULL,
        AssignmentID        bigint      NULL,
        LastSuppDocID       bigint      NULL,
        OriginalEstDocID    bigint      NULL,
        OriginalEstDate     datetime    NULL,
        RepairStartDate     datetime    NULL,
        RepairEndDate       datetime    NULL,
        Assignment2Estimate int         NULL,
        RepairDays          int         NULL,
        EstimateHours       decimal(5,2) NULL,
        ReinspectionFlag    bit          NULL,
        ReISatisfactionBR   decimal(5,4) NULL, -- Before Repair
        ReISatisfactionDR   decimal(5,4) NULL, -- During Repair
        ReISatisfactionAR   decimal(5,4) NULL -- After Repair
    )
    
    DECLARE @tmpIndemnityRecords TABLE
    (
        ShopLocationID      bigint,
        EstimateDocID       bigint,
        ShopProfileRate     decimal(5,2)    NULL,
        MarketRate          decimal(5,2)    NULL,
        BodyLabor           money           NULL,
        RefinishLabor       money           NULL,
        MechanicalLabor     money           NULL,
        FrameLabor          money           NULL,
        RefinishMaterials   money           NULL,
        TaxRate             decimal(5,2)    NULL
    )
    

    -- Need to get active assignments for the selected shops, ignore assignments on cancelled and voided claims
    
    INSERT INTO @tmpAssignment (ShopLocationID, ClaimAspectID, StatusID, LynxID, ClaimAspectNumber, ExposureCD)
      SELECT  a.ShopLocationID,
              casc.ClaimAspectID,
              cas.StatusID,
              ca.LynxID,
              ca.ClaimAspectNumber,
              ca.ExposureCD
        FROM  dbo.utb_assignment a
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment to
		utb_Claim_Aspect
		M.A. 20061110
	*********************************************************************************/
	LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Status to join utb_Claim_Aspect and get the "STATUSID"
		M.A. 20061110
	*********************************************************************************/
	LEFT OUTER JOIN utb_Claim_Aspect_Status cas 
    ON     cas.ClaimAspectID = ca.ClaimAspectID
    --and    cas.ServiceChannelCD = casc.ServiceChannelCD
        WHERE a.ShopLocationID = @ShopLocationID
          AND a.AssignmentDate BETWEEN @BeginDate AND @EndDate AND a.assignmentsequencenumber = 1
          AND a.CancellationDate IS NULL
          AND (casc.DispositionTypeCD NOT IN ('CA', 'VD') 
                OR casc.DispositionTypeCD IS NULL)
          and cas.ServiceChannelCD is null
          and cas.StatusTypeCD is null
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot insert into @tmpAssignment

        RAISERROR('105|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     

    --Now let's filter it.  <INSERT EVIL GRIN HERE>
    --(It's more efficient to do the initial calculations and just kill the records that don't fit)

    IF @AssignmentCode <> 'B'       --if we don't want everything
    BEGIN
        IF @AssignmentCode = 'O'    --Return Open Claims Only
        BEGIN
            DELETE @tmpAssignment WHERE StatusID = @VehicleClosedStatusID
        END
        ELSE                        --Return Closed Claims Only
        BEGIN
            DELETE @tmpAssignment WHERE StatusID <> @VehicleClosedStatusID
        END
    END

    IF @@Error <> 0
    BEGIN

        RAISERROR  ('%s: Error filtering data.', 16, 1, @ProcName)
        RETURN
    END


    -- For each of these active assignments, we need to get the FIRST assignment date
    
    UPDATE @tmpAssignment
      SET  AssignmentDate = (SELECT  Min(a.AssignmentDate)
                               FROM  dbo.utb_assignment a
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
					"ClaimAspectID" in the WHERE clause
					M.A. 20061110
				*********************************************************************************/
			       INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                               WHERE a. ShopLocationID = tmp.ShopLocationID
                                 AND casc.ClaimAspectID = tmp.ClaimAspectID),
           LastAssignmentDate = (SELECT  Max(a.AssignmentDate)
                                   FROM  dbo.utb_assignment a
				   /*********************************************************************************
				   Project: 210474 APD - Enhancements to support multiple concurrent service channels
				   Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
						"ClaimAspectID" in the WHERE clause
						M.A. 20061110
				   *********************************************************************************/
				   INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                                   WHERE a. ShopLocationID = tmp.ShopLocationID
                                     AND casc.ClaimAspectID = tmp.ClaimAspectID)
      FROM @tmpAssignment tmp
      
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the Assignment ID and calculate days since assignment

    UPDATE @tmpAssignment
      SET  DaysSinceAssignment = DateDiff(day, AssignmentDate, @Now),
           AssignmentID = (SELECT  Min(a.AssignmentID)
                             FROM  dbo.utb_assignment a
			   /*********************************************************************************
			   Project: 210474 APD - Enhancements to support multiple concurrent service channels
			   Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
					"ClaimAspectID" in the WHERE clause
					M.A. 20061110
			   *********************************************************************************/
			   INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                             WHERE a. ShopLocationID = tmp.ShopLocationID
                               AND casc.ClaimAspectID = tmp.ClaimAspectID
                               AND a.AssignmentDate = tmp.AssignmentDate)           
      FROM @tmpAssignment tmp
      
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the vehicle information
    
    UPDATE @tmpAssignment
      SET VehicleYear = cv.VehicleYear,
          VehicleMake = cv.Make,
          VehicleModel = cv.Model,
          DriveableFlag = cv.DriveableFlag,
          RepairStartDate = CASE
                                WHEN casc.WorkStartConfirmFlag = 1 THEN casc.WorkStartDate
                                ELSE ''
                            END,
          RepairEndDate =   CASE
                                WHEN casc.WorkEndConfirmFlag = 1 THEN casc.WorkEndDate
                                ELSE ''
                            END
    FROM @tmpAssignment tmp
    LEFT JOIN dbo.utb_claim_vehicle cv ON (tmp.ClaimAspectID = cv.ClaimAspectID)
   /*********************************************************************************
   Project: 210474 APD - Enhancements to support multiple concurrent service channels
   Note:	Added utb_Claim_Aspect_Service_Channel to join utb_claim_vehicle to get
		some values specified in the CASE stmnts above
		M.A. 20061110
   *********************************************************************************/
   LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectID = cv.ClaimAspectID
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the vehicle owner information
    UPDATE @tmpAssignment
      SET OwnerNameFirst = isNull(i.NameFirst, ''),
          OwnerNameLast = isNull(i.NameLast, ''),
          OwnerNameBusiness = isNull(i.BusinessName, '')
    FROM @tmpAssignment tmp
    LEFT JOIN  dbo.utb_claim_aspect_involved cai ON (tmp.ClaimAspectID = cai.ClaimAspectID)
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE cai.EnabledFlag = 1
      AND irt.Name = 'Owner'
      
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the insurance company name
    UPDATE @tmpAssignment
      SET InsuranceCoName = i.Name
    FROM @tmpAssignment tmp
    LEFT JOIN  dbo.utb_claim_aspect ca ON (tmp.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the reinspection flag for the claim aspect
    UPDATE @tmpAssignment
    SET ReinspectionFlag = CASE
                                WHEN EXISTS(SELECT d.ReinspectionRequestFlag
                                            FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                            LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                                            Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                            WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                                              AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                              AND d.ReinspectionRequestFlag = 1) THEN 1
                                ELSE 0
                           END
    FROM @tmpAssignment tmp

    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     

    -- Now grab the Original Estimate and Last Supplement Document IDs for calculation later
    UPDATE @tmpAssignment
      SET OriginalEstDocID = (SELECT TOP 1 d.DocumentID
                                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                                LEFT JOIN utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
                                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                                  AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                  AND d.DocumentTypeID = @EstimateDocumentTypeID
                                  AND d.EnabledFlag = 1
                                ORDER BY d.SupplementSeqNumber ASC,
                                         ds.VANFlag DESC,
                                         d.CreatedDate ASC),
          LastSuppDocID = (SELECT top 1 d.DocumentID
                            FROM utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                            LEFT JOIN utb_document d ON (cascd.DocumentID = d.DocumentID)
                            LEFT JOIN utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
                            left outer join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                            WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                              AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                              AND d.DocumentTypeID IN (3, 4, 10) -- Estimate, Supplement, Final Estimate
                              AND d.EnabledFlag = 1
                            ORDER BY d.SupplementSeqNumber DESC,
                                     ds.VANFlag DESC,
                                     d.CreatedDate DESC)
    FROM @tmpAssignment tmp
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the Original Estimate Date
    UPDATE @tmpAssignment
    SET OriginalEstDate = d.CreatedDate
    FROM @tmpAssignment tmp
    LEFT JOIN dbo.utb_document d ON (tmp.OriginalEstDocID = d.DocumentID)
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the estimate hours
    UPDATE @tmpAssignment
    SET EstimateHours = (SELECT sum(OriginalHrs)
                            FROM dbo.utb_estimate_summary es 
                            WHERE es.DocumentID = tmp.LastSuppDocID
                            AND es.EstimateSummaryTypeID IN (SELECT EstimateSummaryTypeID 
                                                                FROM utb_estimate_summary_type 
                                                                WHERE CategoryCD = 'LC')),
        OriginalEstTotal =  (SELECT OriginalExtendedAmt
                                FROM utb_estimate_summary es
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @RepairTotalID) - 
                            (SELECT OriginalExtendedAmt
                                FROM utb_estimate_summary es
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @BettermentID),
        LastSuppTotal =     (SELECT OriginalExtendedAmt
                                FROM utb_estimate_summary es
                                WHERE es.DocumentID = tmp.LastSuppDocID
                                AND es.EstimateSummaryTypeID = @RepairTotalID) - 
                            (SELECT OriginalExtendedAmt
                                FROM utb_estimate_summary es
                                WHERE es.DocumentID = tmp.LastSuppDocID
                                AND es.EstimateSummaryTypeID = @BettermentID),
        OriginalEstGross =  (SELECT OriginalExtendedAmt
                                FROM utb_estimate_summary es
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @RepairTotalID),
        TotalLaborCost =    (SELECT OriginalExtendedAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @LaborTotalTypeID),
        OEMPartsCost =      isNull((SELECT sum(OriginalExtendedAmt)
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @OEMPartsTypeID), 0),
        LKQPartsCost =      isNull((SELECT sum(OriginalExtendedAmt)
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @LKQPartsTypeID), 0),
        AFMKPartsCost =     isNull((SELECT sum(OriginalExtendedAmt)
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @AFMKPartsTypeID), 0),
        RecycledPartsCost = isNull((SELECT sum(OriginalExtendedAmt)
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.OriginalEstDocID
                                AND es.EstimateSummaryTypeID = @RecycledPartsTypeID), 0)
    FROM @tmpAssignment tmp
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    
   -- Now compute the stuff
   
   UPDATE @tmpAssignment
   SET LaborPercentRepair = (isNull(TotalLaborCost, 0) / (CASE
                                                            WHEN OriginalEstGross > 0 then OriginalEstGross
                                                            ELSE 1
                                                          END)),
       OEMPartsPercent = (isNull(OEMPartsCost, 0) / (CASE
                                                        WHEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost) > 0 THEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost)
                                                        ELSE 1
                                                     END)),
       LKQPartsPercent = (isNull(LKQPartsCost, 0) / (CASE
                                                        WHEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost) > 0 THEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost)
                                                        ELSE 1
                                                     END)),
       AFMKPartsPercent = (isNull(AFMKPartsCost, 0) / (CASE
                                                        WHEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost) > 0 THEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost)
                                                        ELSE 1
                                                     END)),
       RecycledPartsPercent = (isNull(RecycledPartsCost, 0) / (CASE
                                                                    WHEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost) > 0 THEN (OEMPartsCost + LKQPartsCost + AFMKPartsCost + RecycledPartsCost)
                                                                    ELSE 1
                                                               END))

    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END  

    -- Now grab the reinspection information
    
    UPDATE @tmpAssignment
    SET ReinspectionDate = isNull((SELECT TOP 1 r.ReinspectionDate
                            FROM dbo.utb_reinspect r 
                            WHERE r.ClaimAspectID = tmp.ClaimAspectID
                            ORDER BY ReinspectionDate DESC), ''),
        ReinspectionType = isNull((SELECT rc.Name
                            FROM dbo.ufnUtilityGetReferenceCodes('utb_reinspect', 'ReinspectTypeCD') rc
                            WHERE rc.Code = (SELECT TOP 1 r.ReinspectTypeCD
                                                FROM dbo.utb_reinspect r 
                                                WHERE r.ClaimAspectID = tmp.ClaimAspectID
                                                ORDER BY ReinspectionDate DESC)), ''),
        SatisfactionPercent = (SELECT TOP 1 r.ReinspectSatisfactionPercent
                                FROM dbo.utb_reinspect r 
                                WHERE r.ClaimAspectID = tmp.ClaimAspectID
                                ORDER BY ReinspectionDate DESC)
    FROM @tmpAssignment tmp

    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignment', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Summary information
    
    -- Now grab all the assignments with in 180 days
    INSERT INTO @tmpAssignmentHistoric (ShopLocationID, ClaimAspectID, StatusID, LynxID, ClaimAspectNumber)
      SELECT  a.ShopLocationID,
              casc.ClaimAspectID,
              cas.StatusID,
              ca.LynxID,
              ca.ClaimAspectNumber
        FROM  dbo.utb_assignment a
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
		"ClaimAspectID" in the WHERE clause
		M.A. 20061110
	*********************************************************************************/
	INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Status to join utb_claim_aspect and get
		"StatusID" in the SELECT Stmnt
		M.A. 20061110
	*********************************************************************************/
	LEFT OUTER JOIN  utb_Claim_Aspect_Status cas 
    on cas.ClaimAspectID = ca.ClaimAspectID
    --and cas.ServiceChannelCD = casc.ServiceChannelCD
        WHERE a.ShopLocationID = @ShopLocationID
          AND a.AssignmentDate BETWEEN @Date180Ago AND @EndDate
          AND a.CancellationDate IS NULL
          AND (casc.DispositionTypeCD NOT IN ('CA', 'VD')
            OR casc.DispositionTypeCD IS NULL)
          and cas.ServiceChannelCD is null
          and cas.StatusTypeCD is null

    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot insert into @tmpAssignment

        RAISERROR('105|%s|@tmpAssignmentHistoric', 16, 1, @ProcName)
        RETURN
    END     

    --Now let's filter it.  <INSERT EVIL GRIN HERE>
    --(It's more efficient to do the initial calculations and just kill the records that don't fit)

    IF @AssignmentCode <> 'B'       --if we don't want everything
    BEGIN
        IF @AssignmentCode = 'O'    --Return Open Claims Only
        BEGIN
            DELETE @tmpAssignmentHistoric WHERE StatusID = @VehicleClosedStatusID
        END
        ELSE                        --Return Closed Claims Only
        BEGIN
            DELETE @tmpAssignmentHistoric WHERE StatusID <> @VehicleClosedStatusID
        END
    END

    IF @@Error <> 0
    BEGIN

        RAISERROR  ('%s: Error filtering data.', 16, 1, @ProcName)
        RETURN
    END


    -- For each of these active assignments, we need to get the FIRST assignment date
    
    UPDATE @tmpAssignmentHistoric
      SET  AssignmentDate = (SELECT  Min(a.AssignmentDate)
                               FROM  dbo.utb_assignment a
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
					"ClaimAspectID" in the WHERE clause
					M.A. 20061110
				*********************************************************************************/
				INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                               WHERE a. ShopLocationID = tmp.ShopLocationID
                                 AND casc.ClaimAspectID = tmp.ClaimAspectID)
      FROM @tmpAssignmentHistoric tmp
      
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignmentHistoric

        RAISERROR('104|%s|@tmpAssignmentHistoric', 16, 1, @ProcName)
        RETURN
    END     
    
    -- Now grab the original estimate and the last supplement document id
    UPDATE @tmpAssignmentHistoric
    SET OriginalEstDocID = (SELECT TOP 1 d.DocumentID
                                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                                LEFT JOIN utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
                                left outer join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                                  AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                  AND d.DocumentTypeID = @EstimateDocumentTypeID
                                  AND d.EnabledFlag = 1
                                ORDER BY d.SupplementSeqNumber ASC,
                                         ds.VANFlag DESC,
                                         d.CreatedDate ASC),
        LastSuppDocID =    (SELECT TOP 1 d.DocumentID
                                FROM utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                LEFT JOIN utb_document d ON (cascd.DocumentID = d.DocumentID)
                                LEFT JOIN utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
                                Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                                  AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                  AND d.DocumentTypeID IN (3, 4, 10) -- Estimate, Supplement, Final Estimate
                                  AND d.EnabledFlag = 1
                                ORDER BY d.SupplementSeqNumber DESC,
                                         ds.VANFlag DESC,
                                         d.CreatedDate DESC)
    FROM @tmpAssignmentHistoric tmp
    
    -- Now grab the Assignment ID and calculate days since assignment

    UPDATE @tmpAssignmentHistoric
      SET  DaysSinceAssignment = DateDiff(day, AssignmentDate, @Now),
           AssignmentID = (SELECT  Min(a.AssignmentID)
                             FROM  dbo.utb_assignment a
			     /*********************************************************************************
			     Project: 210474 APD - Enhancements to support multiple concurrent service channels
			     Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment and get
					"ClaimAspectID" in the WHERE clause
					M.A. 20061110
			     *********************************************************************************/
			     INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
                             WHERE a. ShopLocationID = tmp.ShopLocationID
                               AND casc.ClaimAspectID = tmp.ClaimAspectID
                               AND a.AssignmentDate = tmp.AssignmentDate),
           OriginalEstDate = (SELECT TOP 1 d.CreatedDate
                                FROM dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                                LEFT JOIN utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
                                Left Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                WHERE casc.ClaimAspectID = tmp.ClaimAspectID
                                  AND casc.EnabledFlag = 1--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                  AND d.DocumentTypeID = @EstimateDocumentTypeID
                                  AND d.EnabledFlag = 1
                                ORDER BY d.SupplementSeqNumber ASC,
                                         ds.VANFlag DESC,
                                         d.CreatedDate ASC),
           RepairStartDate = (SELECT top 1 WorkStartDate
                                FROM utb_claim_vehicle cav
			     	/*********************************************************************************
			     	Project: 210474 APD - Enhancements to support multiple concurrent service channels
			     	Note:	Added utb_Claim_Aspect_Service_Channel to join to utb_claim_vehicle and get
					"WorkStartDate" value
					M.A. 20061110
			     	*********************************************************************************/
				INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectID = cav.ClaimAspectID
                                WHERE cav.ClaimAspectID = tmp.ClaimAspectID
                                  AND WorkStartConfirmFlag = 1),
           RepairEndDate = (SELECT top 1 WorkEndDate
                                FROM utb_claim_vehicle cav
			     	/*********************************************************************************
			     	Project: 210474 APD - Enhancements to support multiple concurrent service channels
			     	Note:	Added utb_Claim_Aspect_Service_Channel to join to utb_claim_vehicle and get
					"WorkEndDate" value
					M.A. 20061110
			     	*********************************************************************************/
				INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectID = cav.ClaimAspectID
                                WHERE cav.ClaimAspectID = tmp.ClaimAspectID
                                  AND casc.WorkEndConfirmFlag = 1)
      FROM @tmpAssignmentHistoric tmp
      
    IF (@@ERROR <> 0)
    BEGIN
        -- Cannot update @tmpAssignment

        RAISERROR('104|%s|@tmpAssignmentHistoric', 16, 1, @ProcName)
        RETURN
    END
    
    -- Now calculate the Assignment to Estimate days
    UPDATE @tmpAssignmentHistoric
    SET Assignment2Estimate = (SELECT dbo.ufnUtilityCalcShopCycleTime(AssignmentDate, OriginalEstDate, @WorkingHours)),--DATEDIFF(day, Assignmentdate, OriginalEstDate)
        RepairDays = (SELECT isNull(dbo.ufnUtilityCalcShopCycleTime(RepairStartDate, RepairEndDate, @WorkingHours), 0)),
        EstimateHours = (SELECT sum(OriginalHrs)
                            FROM dbo.utb_estimate_summary es 
                            WHERE es.DocumentID = tmp.LastSuppDocID
                            AND es.EstimateSummaryTypeID IN (SELECT EstimateSummaryTypeID 
                                                                FROM utb_estimate_summary_type 
                                                                WHERE CategoryCD = 'LC'))
    FROM @tmpAssignmentHistoric tmp
    
    -- Now get the reinspection info for the claim aspect
    UPDATE @tmpAssignmentHistoric
    SET ReinspectionFlag = CASE
                                WHEN EXISTS(SELECT ReinspectID
                                            FROM dbo.utb_reinspect r
                                            WHERE r.ClaimAspectID = tmp.ClaimAspectID) THEN 1
                                ELSE 0
                           END,
        ReISatisfactionBR = (SELECT top 1 ReinspectSatisfactionPercent
                             FROM dbo.utb_reinspect r
                             WHERE r.ClaimAspectID = tmp.ClaimAspectID
                               AND r.RepairStatusCD = 'B'
                             ORDER BY ReinspectionDate DESC),
        ReISatisfactionDR = (SELECT top 1 ReinspectSatisfactionPercent
                             FROM dbo.utb_reinspect r
                             WHERE r.ClaimAspectID = tmp.ClaimAspectID
                               AND r.RepairStatusCD = 'D'
                             ORDER BY ReinspectionDate DESC),
        ReISatisfactionAR = (SELECT top 1 ReinspectSatisfactionPercent
                             FROM dbo.utb_reinspect r
                             WHERE r.ClaimAspectID = tmp.ClaimAspectID
                               AND r.RepairStatusCD = 'A'
                             ORDER BY ReinspectionDate DESC)
        
    FROM @tmpAssignmentHistoric tmp
    
    --SELECT * FROM @tmpAssignmentHistoric
    
    -- Indemnity Summary information
    
    -- Now grab the last 3 estimates for the shop
    
    INSERT INTO @tmpIndemnityRecords (ShopLocationID, EstimateDocID)
    SELECT top 3 @ShopLocationID,
            d.DocumentID
    FROM dbo.utb_assignment a
   /*********************************************************************************
   Project: 210474 APD - Enhancements to support multiple concurrent service channels
   Note:	Added utb_Claim_Aspect_Service_Channel to join utb_assignment to
		utb_claim_aspect_document
		M.A. 20061110
   *********************************************************************************/
   INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID)--Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    LEFT JOIN utb_document d ON (cascd.DocumentID = d.DocumentID)
    WHERE a.ShopLocationID = @ShopLocationID
      AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
      AND d.DocumentTypeID IN (3, 4, 10) -- Estimate, Supplement, Final Estimate
      AND d.EnabledFlag = 1
    ORDER BY d.CreatedDate DESC

    -- Now grab the metrics for the assignments
    UPDATE @tmpIndemnityRecords
    SET BodyLabor =         (SELECT OriginalUnitAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @LaborBodyID),
        RefinishLabor =     (SELECT OriginalUnitAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @LaborRefinishID),
        MechanicalLabor =   (SELECT OriginalUnitAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @LaborMechanicalID),
        FrameLabor =        (SELECT OriginalUnitAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @LaborFrameID),
        TaxRate =           ((SELECT OriginalExtendedAmt
                                FROM dbo.utb_estimate_summary es
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @TaxTotalID) /
                            isNull((SELECT CASE 
                                                WHEN OriginalExtendedAmt > 0 THEN OriginalExtendedAmt 
                                                ELSE 1 
                                           END
                                FROM dbo.utb_estimate_summary es
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @RepairTotalID), 1) * 100.00),
        RefinishMaterials = (SELECT OriginalUnitAmt
                                FROM dbo.utb_estimate_summary es 
                                WHERE es.DocumentID = tmp.EstimateDocID
                                AND es.EstimateSummaryTypeID = @RefinishMaterialsID)
    FROM @tmpIndemnityRecords tmp
    
    -- Now grab the Indemnity Summary information from the shop pricing table
    SELECT  @ShopProfileBodyRate = HourlyRateSheetMetal,
            @ShopProfileRefinishRate = HourlyRateRefinishing,
            @ShopProfileMechanicalRate = HourlyRateMechanical,
            @ShopProfileFrameRate = HourlyRateUnibodyFrame,
            @ShopProfileRefinishMaterials = RefinishTwoStageHourly,
            @ShopProfileTaxRate = isNull(CountyTaxPct, 0) + isNull(MunicipalTaxPct, 0) + 
                                  isNull(OtherTaxPct, 0) + isNull(SalesTaxPct, 0)
    FROM dbo.utb_shop_location_pricing
    WHERE ShopLocationID = @ShopLocationID
    
    -- TODO - Market analysis table is not ready yet
    -- Now grab the Indemnity summary information from the market pricing table
    SELECT  @MktBodyRate = NULL,
            @MktRefinishRate = NULL,
            @MktMechanicalRate = NULL,
            @MktFrameRate = NULL,
            @MktRefinishMaterials = NULL,
            @MktTaxRate = NULL
    
    -- Now we have all the data.
    
    -- Generate the final XML
    SELECT
        1                     AS Tag,
        NULL                  AS Parent,
        --Root
        @ShopLocationID       AS [Root!1!ShopLocationID],
        @ShopBusinessID       AS [Root!1!ShopBusinessID],
        @ShopName             AS [Root!1!ShopName],
        @LastReIDate          AS [Root!1!LastReIDate],
        @BeginDate            AS [Root!1!BeginDate],
        @EndDate              AS [Root!1!EndDate],
        @AssignmentCode       AS [Root!1!AssignmentCode],
        @ViewCRD              AS [Root!1!ViewCRD],
        @ViewSMD              AS [Root!1!ViewSMD],
        -- Shop Data
        NULL                  AS [Shop!2!Name],
        NULL                  AS [Shop!2!Address1],
        NULL                  AS [Shop!2!AddressCity],
        NULL                  AS [Shop!2!AddressState],
        NULL                  AS [Shop!2!AddressZip],
        NULL                  AS [Shop!2!AddressCounty],
        NULL                  AS [Shop!2!ProgramManagerName],
        NULL                  AS [Shop!2!ContactName],
        NULL                  AS [Shop!2!Phone],
        NULL                  AS [Shop!2!Fax],
        NULL                  AS [Shop!2!EmailAddress],
        NULL                  AS [Shop!2!ProgramScore],
        --Assignment Data
        NULL                  AS [Assignment!3!AssignmentID],
        NULL                  AS [Assignment!3!LynxID],
        NULL                  AS [Assignment!3!ClaimAspectNumber],
        NULL                  AS [Assignment!3!ClaimAspectID],
        NULL                  AS [Assignment!3!ReinspectionFlag],
        NULL                  AS [Assignment!3!VehicleYear],
        NULL                  AS [Assignment!3!VehicleMake],
        NULL                  AS [Assignment!3!VehicleModel],
        NULL                  AS [Assignment!3!DrivableFlag],
        NULL                  AS [Assignment!3!OwnerNameFirst],
        NULL                  AS [Assignment!3!OwnerNameLast],
        NULL                  AS [Assignment!3!OwnerNameBusiness],
        NULL                  AS [Assignment!3!ExposureCD],
        NULL                  AS [Assignment!3!InsuranceCoName],
        NULL                  AS [Assignment!3!LastAssignmentDate],
        NULL                  AS [Assignment!3!AssignmentDate],
        NULL                  AS [Assignment!3!OriginalEstDate],
        NULL                  AS [Assignment!3!RepairStartDate],
        NULL                  AS [Assignment!3!RepairEndDate],
        NULL                  AS [Assignment!3!EstimateHours],        
        NULL                  AS [Assignment!3!OriginalEstTotal],
        NULL                  AS [Assignment!3!LastSuppTotal],
        NULL                  AS [Assignment!3!LaborPercentRepair],
        NULL                  AS [Assignment!3!OEMPartsPercent],
        NULL                  AS [Assignment!3!LKQPartsPercent],
        NULL                  AS [Assignment!3!AFMKPartsPercent],
        NULL                  AS [Assignment!3!RecycledPartsPercent],
        NULL                  AS [Assignment!3!ReinspectionDate],
        NULL                  AS [Assignment!3!ReinspectionType],
        NULL                  AS [Assignment!3!ReinspectionSatisfactionPercent],
        -- Cycle Time Summary
        NULL                  AS [CycleTimeSummary!4!Assignnment90],
        NULL                  AS [CycleTimeSummary!4!Assignnment180],
        NULL                  AS [CycleTimeSummary!4!Assignnment2Estimate90],
        NULL                  AS [CycleTimeSummary!4!Assignnment2Estimate180],
        NULL                  AS [CycleTimeSummary!4!RepairDays90],
        NULL                  AS [CycleTimeSummary!4!RepairDays180],
        NULL                  AS [CycleTimeSummary!4!EstimateHours90],
        NULL                  AS [CycleTimeSummary!4!EstimateHours180],
        -- Indemnity Summary
        NULL                  AS [IndemnitySummary!5!Last3EstBodyRate],
        NULL                  AS [IndemnitySummary!5!Last3EstRefinishRate],
        NULL                  AS [IndemnitySummary!5!Last3EstMechanicalRate],
        NULL                  AS [IndemnitySummary!5!Last3EstFrameRate],
        NULL                  AS [IndemnitySummary!5!Last3EstRefinishMaterials],
        NULL                  AS [IndemnitySummary!5!Last3EstTaxRate],
        NULL                  AS [IndemnitySummary!5!ShopProfileBodyRate],
        NULL                  AS [IndemnitySummary!5!ShopProfileRefinishRate],
        NULL                  AS [IndemnitySummary!5!ShopProfileMechanicalRate],
        NULL                  AS [IndemnitySummary!5!ShopProfileFrameRate],
        NULL                  AS [IndemnitySummary!5!ShopProfileRefinishMaterials],
        NULL                  AS [IndemnitySummary!5!ShopProfileTaxRate],
        NULL                  AS [IndemnitySummary!5!MktBodyRate],
        NULL                  AS [IndemnitySummary!5!MktRefinishRate],
        NULL                  AS [IndemnitySummary!5!MktMechanicalRate],
        NULL                  AS [IndemnitySummary!5!MktFrameRate],
        NULL                  AS [IndemnitySummary!5!MktRefinishMaterials],
        NULL                  AS [IndemnitySummary!5!MktTaxRate],
        -- Reinspection summary
        NULL                  AS [ReinspectionSummary!6!Reinspection90],
        NULL                  AS [ReinspectionSummary!6!Reinspection180],
        NULL                  AS [ReinspectionSummary!6!BeforeRepair90],
        NULL                  AS [ReinspectionSummary!6!BeforeRepair180],
        NULL                  AS [ReinspectionSummary!6!DuringRepair90],
        NULL                  AS [ReinspectionSummary!6!DuringRepair180],
        NULL                  AS [ReinspectionSummary!6!AfterRepair90],
        NULL                  AS [ReinspectionSummary!6!AfterRepair180]
        
    UNION ALL
    
    SELECT 2,
           1,
        -- Root
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Shop Data
        Name,
        isNull(Address1, ''), 
        isNull(AddressCity, ''), 
        isNull(AddressState, ''), 
        isNull(AddressZip, ''),
        isNull(AddressCounty, ''), 
        IsNull((SELECT NameLast + ', ' + NameFirst
          FROM dbo.utb_user u
          WHERE u.UserID = sl.ProgramManagerUserID), ''), 
        ISNULL((SELECT P.Name 
                FROM dbo.utb_shop_location_personnel LP 
                     INNER JOIN dbo.utb_personnel P ON LP.PersonnelID = P.PersonnelID
                     INNER JOIN dbo.utb_personnel_type PT ON P.PersonnelTypeID = PT.PersonnelTypeID                     
                WHERE LP.ShopLocationID = @ShopLocationID
                  AND PT.Name = 'Shop Manager'), ''), 
        isNull(sl.PhoneAreaCode, '') + '-' + isNull(sl.PhoneExchangeNumber, '') + '-' + isNull(sl.PhoneUnitNumber, '') + 'x' + isNull(sl.PhoneExtensionNumber, ''), 
        isNull(sl.FaxAreaCode, '') + '-' + isNull(sl.FaxExchangeNumber, '') + '-' + isNull(sl.FaxUnitNumber, '') + 'x' + isNull(sl.FaxExtensionNumber, ''),
        isNull(sl.EmailAddress, ''),
        isNull(sl.ProgramScore, ''),
        --Assignment Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Cycle Time Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Indemnity Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Reinspection summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL
    FROM dbo.utb_shop_location sl
    WHERE sl.ShopLocationID = @ShopLocationID
    
    UNION ALL
    
    SELECT 3,
           1,
        -- Root
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Shop Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
        --Assignment Data
        AssignmentID,
        LynxID,
        ClaimAspectNumber,
        ClaimAspectID,
        ReinspectionFlag,
        VehicleYear,
        VehicleMake,
        VehicleModel,
        DriveableFlag,
        OwnerNameFirst,
        OwnerNameLast,
        OwnerNameBusiness,
        ExposureCD,
        InsuranceCoName,
        LastAssignmentDate,
        AssignmentDate,
        OriginalEstDate,
        RepairStartDate,
        RepairEndDate,
        EstimateHours,
        OriginalEstTotal,
        LastSuppTotal,
        LaborPercentRepair,
        OEMPartsPercent,
        LKQPartsPercent,
        AFMKPartsPercent,
        RecycledPartsPercent,
        ReinspectionDate,
        ReinspectionType,
        SatisfactionPercent,
        -- Cycle Time Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Indemnity Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Reinspection summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL
        
    FROM @tmpAssignment
           
    UNION ALL
    
    SELECT 4,
           1,
        -- Root
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Shop Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
        --Assignment Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Cycle Time Summary
        (SELECT count(*) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90),
        (SELECT count(*) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180),
        (SELECT AVG(Assignment2Estimate) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90),
        (SELECT AVG(Assignment2Estimate) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180),
        (SELECT AVG(RepairDays) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90),
        (SELECT AVG(RepairDays) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180),
        (SELECT AVG(EstimateHours) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90),
        (SELECT AVG(EstimateHours) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180),
        -- Indemnity Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Reinspection summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL

    UNION ALL
    
    SELECT 5,
           1,
        -- Root
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Shop Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
        --Assignment Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Cycle Time Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Indemnity Summary
        Avg(BodyLabor),
        Avg(RefinishLabor),
        Avg(MechanicalLabor),
        Avg(FrameLabor),
        Avg(RefinishMaterials),
        Avg(TaxRate),
        @ShopProfileBodyRate,
        @ShopProfileRefinishRate,
        @ShopProfileMechanicalRate,
        @ShopProfileFrameRate,
        @ShopProfileRefinishMaterials,
        @ShopProfileTaxRate,
        @MktBodyRate,
        @MktRefinishRate,
        @MktMechanicalRate,
        @MktFrameRate,
        @MktRefinishMaterials,
        @MktTaxRate,
        -- Reinspection summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL
    FROM @tmpIndemnityRecords
               
    UNION ALL
    
    SELECT 6,
           1,
        -- Root
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Shop Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
        --Assignment Data
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        -- Cycle Time Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Indemnity Summary
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL,
        -- Reinspection summary
        (SELECT count(*) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90
            AND ReinspectionFlag = 1),
        (SELECT count(*) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180
            AND ReinspectionFlag = 1),
        (SELECT AVG(ReISatisfactionBR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90
              AND ReISatisfactionBR > 0),
        (SELECT AVG(ReISatisfactionBR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180
              AND ReISatisfactionBR > 0),
        (SELECT AVG(ReISatisfactionDR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90
              AND ReISatisfactionDR > 0),
        (SELECT AVG(ReISatisfactionDR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180
              AND ReISatisfactionDR > 0),
        (SELECT AVG(ReISatisfactionAR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 90
              AND ReISatisfactionAR > 0),
        (SELECT AVG(ReISatisfactionAR) 
            FROM @tmpAssignmentHistoric
            WHERE DaysSinceAssignment <= 180
              AND ReISatisfactionAR > 0)

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDShopAssignmentGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPMDShopAssignmentGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
