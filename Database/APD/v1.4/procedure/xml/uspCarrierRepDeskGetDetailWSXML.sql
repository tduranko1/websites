-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCarrierRepDeskGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCarrierRepDeskGetDetailWSXML 
END

GO

/****** Object:  StoredProcedure [dbo].[uspCarrierRepDeskGetDetailWSXML]    Script Date: 03/23/2015 16:25:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCarrierRepDeskGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company id
* (I) @UserID               The carrier representative to get the claims for
* (I) @OfficeID             The OfficeID to get claims for
* (I) @SelectionCode        Selection of claims to retrieve 'D' -Default View, 'A' - All, 'O' - Open Only, 'CL' - Closed Only, 'CA' - Cancelled Only, 'V' - Voided only
* (I) @Days                 Number of days in the past to retrieve
*
* RESULT SET:
* XML document detailing the claims returned
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspCarrierRepDeskGetDetailWSXML]
    @InsuranceCompanyID udt_std_id,
    @UserID             udt_std_id  = 0,
    @OfficeID           udt_std_id  = 0,
    @SelectionCode      udt_std_cd  = 'D',
    @Days               udt_std_int = 0,
    @ServiceChannelCD   udt_std_cd  = '',
    @AssignmentTypeID	udt_std_cd  = ''
AS
BEGIN
    SET NOCOUNT ON
    -- Define temporary table variables

    DECLARE @tmpUsers TABLE
    (
        UserID                  bigint      NOT NULL
    )

    DECLARE @tmpStatuses TABLE
    (
        StatusID                int         NOT NULL,
        OpenStatusFlag          bit         NOT NULL,
        ClosedStatusFlag        bit         NOT NULL,
        CancelledStatusFlag     bit         NOT NULL,
        VoidedStatusFlag        bit         NOT NULL
    )
        
    DECLARE @tmpClaimSummary TABLE
    (
        LynxID                  bigint      NOT NULL,
        InsuredNameFirst        varchar(50) NULL,
        InsuredNameLast         varchar(50) NULL,
        InsuredBusinessName     varchar(50) NULL        
    )

    DECLARE @tmpVehicleSummary TABLE
    (
        LynxID                  bigint      NOT NULL,
        VehicleNumber           int         NOT NULL,
        ClaimAspectID           bigint      NOT NULL,
        VehicleStatus           varchar(25) NULL
    )

    
    -- Declare Internal Variables

    DECLARE @ClaimAspectTypeIDAssElec   AS udt_std_id
    DECLARE @ClaimAspectTypeIDAssFax    AS udt_std_id
    DECLARE @ClaimAspectTypeIDClaim     AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle   AS udt_std_id
    DECLARE @DaysWork                   AS udt_std_int
    
    DECLARE @ClaimAspectID              AS udt_std_id
    DECLARE @LynxID                     AS udt_std_id
    DECLARE @VehicleNumber              AS udt_std_int
    DECLARE @VehicleStatus              AS varchar(25)
    
    DECLARE @now                AS datetime
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts

    SET @now = CURRENT_TIMESTAMP
    SET @ProcName = 'uspCarrierRepDeskGetDetailXML'

    IF @ServiceChannelCD = ''
    BEGIN
        SET @ServiceChannelCD = '%'
    END
    
    IF @AssignmentTypeID = ''
    BEGIN
		SET @AssignmentTypeID = '%'
    END

    -- Check to make sure a valid Insurance Company id was passed in

    IF (NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
    
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END


    -- Check to make sure a valid user id was passed in

    IF (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Check to make sure a valid office id was passed in

    IF  (@OfficeID IS NULL) OR
        ((@OfficeID <> 0) AND 
        (NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID)))
    BEGIN
        -- Invalid Office ID
    
        RAISERROR('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
        RETURN
    END


    -- Check to make sure a valid selection code was passed in

    IF  (@SelectionCode IS NULL) OR 
        (@SelectionCode NOT IN ('A', 'CA', 'CL', 'D', 'O', 'V'))
    BEGIN
        -- Invalid Selection
    
        RAISERROR('101|%s|@SelectionCode|%s', 16, 1, @ProcName, @SelectionCode)
        RETURN
    END
    

    -- Set @DaysWork based on selection code and @Days passed in
    
    IF @SelectionCode = 'D'
    BEGIN
        -- This is the default view, set to 30 days
        
        SET @DaysWork = 30
    END
    ELSE
    BEGIN
        -- Look at @Days and set appropriately
        
        IF @Days = 0
        BEGIN
            -- The user wants no limitation on the view, set to arbitrary value high enough to handle this
            
            SET @DaysWork = 9999
        END
        ELSE
        BEGIN
            -- Just copy @Days over to @DaysWork
            
            SET @DaysWork = @Days
        END
    END
    
        
    -- Populate the tmpUser table.  This will hold the list of users we need to return claim records for.  In most cases,
    -- this will be just one user.  However, if a request is to see an entire office or entire carrier, then this
    -- table will be populated with more than 1 record.
    
    IF @UserID = 0
    BEGIN
        IF @OfficeID = 0
        BEGIN
            -- User is looking for claims for all users of the insurance company
            
            INSERT INTO @tmpUsers
              SELECT  UserID
                FROM  dbo.utb_user u
                LEFT JOIN dbo.utb_office o ON (u.OfficeID = o.OfficeID)
                WHERE o.InsuranceCompanyID = @InsuranceCompanyID
        END
        ELSE
        BEGIN
            -- User is looking all users of @OfficeID
                            
            INSERT INTO @tmpUsers
              SELECT  UserID
                FROM  dbo.utb_user
                WHERE OfficeID = @OfficeID
        END
    END
    ELSE
    BEGIN
        -- Just looking for a single user
        
        INSERT INTO @tmpUsers
          SELECT  UserID
            FROM  dbo.utb_user
            WHERE UserID = @UserID
    END
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpUsers', 16, 1, @ProcName)
        RETURN
    END


    -- Get Claim Aspect Type IDs for use below

   SELECT @ClaimAspectTypeIDAssElec = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Assignment'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDAssElec IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Assignment"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


   SELECT @ClaimAspectTypeIDAssFax = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Fax Assignment'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDAssFax IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Fax Assignment"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


   SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Populate the tmpStatuses table
    -- This table will contain all the valid claim statuses to include when pulling claims and vehicles
    
    IF (@SelectionCode IN ('A', 'D', 'O'))
    BEGIN
        INSERT INTO @tmpStatuses
          SELECT  StatusID,
                  1, 0, 0, 0
            FROM  dbo.utb_status 
            WHERE (ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND Name = 'Open')
              OR  (ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Open')

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@tmpStatuses', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF (@SelectionCode IN ('A', 'D', 'CL'))
    BEGIN
        INSERT INTO @tmpStatuses
          SELECT  StatusID,
                  0, 1, 0, 0
            FROM  dbo.utb_status 
            WHERE (ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND Name IN ('Claim Closed', 'Closed - Subro Pending'))
              OR  (ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Vehicle Closed')

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@tmpStatuses', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF (@SelectionCode IN ('A', 'D', 'CA'))
    BEGIN
        INSERT INTO @tmpStatuses
          SELECT  StatusID,
                  0, 0, 1, 0
            FROM  dbo.utb_status 
            WHERE (ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND Name = 'Claim Cancelled')
			  OR  (ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Vehicle Cancelled')

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@tmpStatuses', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF (@SelectionCode IN ('A', 'D', 'V'))
    BEGIN
        INSERT INTO @tmpStatuses
          SELECT  StatusID,
                  0, 0, 0, 1
            FROM  dbo.utb_status 
            WHERE (ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND Name = 'Claim Voided')
			  OR  (ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND Name = 'Vehicle Voided')

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@tmpStatuses', 16, 1, @ProcName)
            RETURN
        END
    END

       
    -- Continue to Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Claimant')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Claimant"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END


    -- Initial select for claim
    INSERT INTO @tmpClaimSummary
      SELECT  distinct c.LynxID,
              (SELECT top 1 i.NameFirst
                 FROM dbo.utb_claim_aspect cas
                 LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                 LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                 WHERE cas.LynxID = c.LynxID
                   AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                   AND cai.EnabledFlag = 1
                   AND irt.Name = 'Insured'),
              (SELECT top 1 i.NameLast
                 FROM dbo.utb_claim_aspect cas
                 LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                 LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                 WHERE cas.LynxID = c.LynxID
                   AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                   AND cai.EnabledFlag = 1
                   AND irt.Name = 'Insured'),
              (SELECT top 1 i.BusinessName
                 FROM dbo.utb_claim_aspect cas
                 LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                 LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                 LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                 WHERE cas.LynxID = c.LynxID
                   AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                   AND cai.EnabledFlag = 1
                   AND irt.Name = 'Insured')
        FROM  dbo.utb_claim c
        LEFT JOIN dbo.utb_claim_aspect ca ON c.LynxID = ca.LynxID
	/***********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
	***********************************************************************************/
	LEFT OUTER JOIN dbo.utb_claim_aspect_status cas ON ca.ClaimAspectID = cas.ClaimAspectID  	
        LEFT JOIN dbo.utb_status s on cas.StatusID = s.StatusID
        WHERE c.CarrierRepUserID IN (SELECT UserID FROM @tmpUsers)
          AND c.InsuranceCompanyID = @InsuranceCompanyID
          AND (ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim OR ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle)
          AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses)
          AND Datediff(d, c.IntakeFinishDate, @now) <= @DaysWork

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpClaimSummary', 16, 1, @ProcName)
        RETURN
    END


    -- If this for the default selection (Selection Code = 'D'), we need to add to this list all open claims from previous to the 
    -- default days specified
    
    IF @SelectionCode = 'D'
    BEGIN
        INSERT INTO @tmpClaimSummary
          SELECT  distinct c.LynxID,
                  (SELECT i.NameFirst
                     FROM dbo.utb_claim_aspect cas
                     LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                     LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                     WHERE cas.LynxID = c.LynxID
                       AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                       AND cai.EnabledFlag = 1
                       AND irt.Name = 'Insured'),
                  (SELECT i.NameLast
                     FROM dbo.utb_claim_aspect cas
                     LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                     LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                     WHERE cas.LynxID = c.LynxID
                       AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                       AND cai.EnabledFlag = 1
                       AND irt.Name = 'Insured'),
                  (SELECT i.BusinessName
                     FROM dbo.utb_claim_aspect cas
                     LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                     LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                     LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                     WHERE cas.LynxID = c.LynxID
                       AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                       AND cai.EnabledFlag = 1
                       AND irt.Name = 'Insured')
            FROM  dbo.utb_claim c
            LEFT JOIN dbo.utb_claim_aspect ca ON c.LynxID = ca.LynxID
            /****************************************************************************************
            Project: 210474 APD - Enhancements to support multiple concurrent service channels
            Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
            ****************************************************************************************/
            LEFT OUTER JOIN dbo.utb_claim_aspect_status cas ON ca.ClaimAspectID = cas.ClaimAspectID
  	
            LEFT JOIN dbo.utb_status s on cas.StatusID = s.StatusID
            WHERE c.CarrierRepUserID IN (SELECT UserID FROM @tmpUsers)
              AND c.InsuranceCompanyID = @InsuranceCompanyID
              AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
              AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses WHERE OpenStatusFlag = 1)
              AND c.LynxID NOT IN (SELECT LynxID FROM @tmpClaimSummary)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@tmpClaimSummary', 16, 1, @ProcName)
            RETURN
        END
    END

    -- delete all claims that don't have the service channel specified
    IF @ServiceChannelCD <> '%'
    BEGIN
        DELETE FROM @tmpClaimSummary
        WHERE LynxID not in (SELECT t.LynxID
                             FROM @tmpClaimSummary t
                             LEFT JOIN dbo.utb_claim_aspect ca on t.LynxID = ca.LynxID
                             LEFT JOIN dbo.utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
                             WHERE casc.ServiceChannelCD = @ServiceChannelCD)
    END
    
    -- delete all claims that don't have the assignment type specified
    IF @AssignmentTypeID <> '%'
    BEGIN
        DELETE FROM @tmpClaimSummary
        WHERE LynxID not in (SELECT t.LynxID
                             FROM @tmpClaimSummary t
                             LEFT JOIN dbo.utb_claim_aspect ca on t.LynxID = ca.LynxID
                             WHERE ca.InitialAssignmentTypeID = @AssignmentTypeID)
    END
     
    -- Now do the select for vehicles, including the status for each vehicle.  Note these aren't the simple statuses 
    -- defined in the APD system.  Instead, we will need to peck around the database, interpreting the status by the
    -- setting of different flags and the presence or absence of records.
    
    -- Best thing to do is work backward in the processing of a claim to determine what's the best status to 
    -- assign.  The statuses defined and their triggers are listed below:
    --     Pending - Claim not yet been reviewed by LYNX Rep (claim.NewAssignmentFlag set)
    --     Reviewed - Claim being reviewed (claim.NewAssignmentFlag not set, Shop Assignment not sent)
    --     Shop Contacted - Shop Assignment has been sent (Fax/Electronic Assignment Status "Enroute" or "Sent"
    --     Awaiting Estimate - The shop has downloaded the assignment (Electronic assignment set to "Sent" Note: This is CCC Only)
    --     Estimate Received - Estimate has been received (An E01 exists for the vehicle)
    --     Supplement Received - Supplement has been received (An S01 or greater exists for the vehicle)
    --     Closed - Vehicle Repair closed (Vehicle Status set to "Vehicle Closed")
    --     Cancelled - Claim was cancelled (Claim status set to "Claim Cancelled")
    --     Voided - Claim was voided (Claim status set to "Claim Voided")

    INSERT INTO @tmpVehicleSummary
      SELECT  ca.LynxID,
              ca.ClaimAspectNumber,
              ca.ClaimAspectID,
              CASE 
                  WHEN EXISTS(SELECT  cas.StatusID 
                                FROM  dbo.utb_claim_aspect ca1
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
				INNER JOIN dbo.utb_claim_aspect_status cas ON ca1.ClaimAspectID = cas.ClaimAspectID  	
                                WHERE ca1.LynxID = ca.LynxID 
                                  AND ca1.ClaimAspectTypeID = @ClaimAspectTypeIDClaim 
                                  AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses WHERE CancelledStatusFlag = 1)) THEN 'Cancelled'        
                  
                  WHEN EXISTS(SELECT  cas.StatusID 
                                FROM  dbo.utb_claim_aspect ca1
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
				INNER JOIN dbo.utb_claim_aspect_status cas ON ca1.ClaimAspectID = cas.ClaimAspectID 
                                WHERE ca1.LynxID = ca.LynxID 
                                  AND ca1.ClaimAspectTypeID = @ClaimAspectTypeIDClaim 
                                  AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses WHERE VoidedStatusFlag = 1)) THEN 'Voided'
                  
                  WHEN EXISTS(SELECT  cas.StatusID 
                                FROM  dbo.utb_claim_aspect ca1
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
				INNER JOIN dbo.utb_claim_aspect_status cas ON ca1.ClaimAspectID = cas.ClaimAspectID 
                                WHERE ca1.ClaimAspectID = ca.ClaimAspectID 
                                  AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses WHERE ClosedStatusFlag = 1)) THEN 'Closed'
                  
                  WHEN (SELECT Max(d.SupplementSeqNumber) 
                          --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
                          FROM  dbo.utb_claim_aspect_Service_Channel_document cascd
                          inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID 
                          LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                          LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
                          WHERE casc.ClaimAspectID = ca.ClaimAspectID
                            AND dt.EstimateTypeFlag = 1
                            and casc.EnabledFlag = 1) > 0 THEN 'Supplement Received'
                          --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226

                  
                  WHEN (SELECT Max(d.SupplementSeqNumber) 
                          --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
                          FROM  dbo.utb_claim_aspect_Service_Channel_document cascd
                          inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID 
                          LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
                          LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
                          WHERE casc.ClaimAspectID = ca.ClaimAspectID
                            AND dt.EstimateTypeFlag = 1
                            and casc.EnabledFlag = 1) = 0 THEN 'Estimate Received'
                          --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226

                  
                  WHEN (SELECT  s.Name
                          FROM  dbo.utb_claim_aspect sca 
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
			  LEFT OUTER JOIN dbo.utb_claim_aspect_status cas1 ON cas1.ClaimAspectID = sca.ClaimAspectID
                          --LEFT JOIN dbo.utb_status s ON (sca.StatusID = s.StatusID) --Project: 210474 APD - Enhancements to support multiple concurrent service channels
			  LEFT JOIN dbo.utb_status s ON (cas1.StatusID = s.StatusID)
                          WHERE sca.LynxID = ca.LynxID
                            AND sca.ClaimAspectTypeID = @ClaimAspectTypeIDAssElec
                            AND sca.ClaimAspectNumber = ca.ClaimAspectNumber) = 'Received By Shop' THEN 'Awaiting Estimate'
                  
                  WHEN ((SELECT  s.Name
                          FROM  dbo.utb_claim_aspect sca 
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
			  LEFT OUTER JOIN dbo.utb_claim_aspect_status cas1 ON cas1.ClaimAspectID = sca.ClaimAspectID
                          --LEFT JOIN dbo.utb_status s ON (sca.StatusID = s.StatusID) --Project: 210474 APD - Enhancements to support multiple concurrent service channels
                          LEFT JOIN dbo.utb_status s ON (cas1.StatusID = s.StatusID)
                          WHERE sca.LynxID = ca.LynxID
                            AND sca.ClaimAspectTypeID = @ClaimAspectTypeIDAssElec
                            AND sca.ClaimAspectNumber = ca.ClaimAspectNumber) = 'Sent')   OR
                       ((SELECT  s.Name
                          FROM  dbo.utb_claim_aspect sca 
				/***********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
				***********************************************************************************/
			  LEFT OUTER JOIN dbo.utb_claim_aspect_status cas1 ON cas1.ClaimAspectID = sca.ClaimAspectID
                          --LEFT JOIN dbo.utb_status s ON (ca.StatusID = s.StatusID) --Project: 210474 APD - Enhancements to support multiple concurrent service channels
                          LEFT JOIN dbo.utb_status s ON (cas1.StatusID = s.StatusID)
                          WHERE sca.LynxID = ca.LynxID
                            AND sca.ClaimAspectTypeID = @ClaimAspectTypeIDAssFax
                            AND sca.ClaimAspectNumber = ca.ClaimAspectNumber) = 'Sent')   THEN 'Shop Contacted'
			/*********************************************************************************
			Project: 210474 APD - Enhancements to support multiple concurrent service channels
			Note:	Renamed the column below from OwnerNewAssignmentFlag to NewAssignmentOwnerFlag
				M.A. 20061113
			*********************************************************************************/
                  WHEN (SELECT  ca.NewAssignmentOwnerFlag --OwnerNewAssignmentFlag 
                          FROM  dbo.utb_claim_aspect ca1 
			
                          WHERE ca1.ClaimAspectID = ca.ClaimAspectID 
			    and ca1.NewAssignmentOwnerFlag = 0) > 0  THEN 'Reviewed'
                          
                  ELSE 'Pending'
              END
        FROM  @tmpClaimSummary tmp
        INNER JOIN dbo.utb_claim_aspect ca ON (tmp.LynxID = ca.LynxID)
        /***********************************************************************************
        Project: 210474 APD - Enhancements to support multiple concurrent service channels
        Note:	Added reference to utb_claim_aspect_status to get to "StatusID" M.A.20061107
        ***********************************************************************************/
        INNER JOIN dbo.utb_claim_aspect_status cas ON cas.ClaimAspectID = ca.ClaimAspectID
        WHERE (ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
				AND ca.EnabledFlag = 1
				AND Isnumeric(@AssignmentTypeID) = 0
				AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses))
				OR
				(ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
				AND ca.EnabledFlag = 1
				AND Isnumeric(@AssignmentTypeID) = 1
				AND convert(varchar(4),ca.initialAssignmentTypeID) = @AssignmentTypeID
				AND cas.StatusID IN (SELECT StatusID FROM @tmpStatuses))


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpVehicleSummary', 16, 1, @ProcName)
        RETURN
    END
    
        
    -- XML select

    -- Select Root Level

   SELECT  1 AS Tag,
            NULL AS Parent,
            ISNULL (@UserID, '') AS [Root!1!UserID],
            ISNULL (@OfficeID, '') AS [Root!1!OfficeID],
            ISNULL (@SelectionCode, '') AS [Root!1!SelectionCode],
            ISNULL (@Days, '') AS [Root!1!Days],
            -- Search Results Record
            999999999 AS [Claim!2!LynxId],
            NULL AS [Claim!2!ClaimNumber],
            NULL AS [Claim!2!InsuredLastName],
            NULL AS [Claim!2!InsuredFirstName],
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!LynxRepUserID],
            NULL AS [Claim!2!LynxRepNameFirst],
            NULL AS [Claim!2!LynxRepNameLast],
            NULL AS [Claim!2!CarrierRepUserID],
            NULL AS [Claim!2!CarrierRepNameFirst],
            NULL AS [Claim!2!CarrierRepNameLast],
            NULL AS [Claim!2!CarrierRepOfficeCd],
            NULL AS [Claim!2!CarrierRepOfficeName],
            NULL AS [Claim!2!IntakeFinishDate],
            -- Claim Vehicle
            NULL AS [Vehicle!3!ClaimAspectID],
            NULL AS [Vehicle!3!VehicleNumber],
            NULL AS [Vehicle!3!VehicleYear],
            NULL AS [Vehicle!3!Make],
            NULL AS [Vehicle!3!Model],
            NULL AS [Vehicle!3!OwnerNameLast],
            NULL AS [Vehicle!3!OwnerNameFirst],
            NULL AS [Vehicle!3!OwnerBusinessName],
            NULL AS [Vehicle!3!Status],
            NULL AS [Vehicle!3!AssignmentType],
            NULL AS [Vehicle!3!SourceApplicationID],
            NULL AS [Vehicle!3!SourceApplicationName],
            NULL AS [Vehicle!3!InitialAssignmentTypeName],
            /*********************************************************************************
            Project: 210474 APD - Enhancements to support multiple concurrent service channels
            Note:	Added the following to provide the data on Service Channels
                   	M.A. 20061215
            *********************************************************************************/
            -- Claim Aspect Service Channel
            NULL as [ServiceChannel!4!ClaimAspectID],
            NULL as [ServiceChannel!4!ClaimAspectStatusID],
            NULL as [ServiceChannel!4!ClaimAspectStatusDesc],
            NULL as [ServiceChannel!4!ClaimAspectTypeCD],
            NULL as [ServiceChannel!4!ClaimAspectTypeCDDesc],
            NULL as [ServiceChannel!4!ClaimAspectServiceChannelID],
            NULL as [ServiceChannel!4!CreatedUserID],
            NULL as [ServiceChannel!4!CreatedDate],
            NULL as [ServiceChannel!4!DispositionTypeCD],
            NULL as [ServiceChannel!4!EnabledFlag],
            NULL as [ServiceChannel!4!InspectionDate],
            NULL as [ServiceChannel!4!InvoiceDate],
            NULL as [ServiceChannel!4!OriginalCompleteDate],
            NULL as [ServiceChannel!4!OriginalEstimateDate],
            NULL as [ServiceChannel!4!PrimaryFlag],
            NULL as [ServiceChannel!4!ServiceChannelCD],
            NULL as [ServiceChannel!4!ServiceChannelCDDesc],
            NULL as [ServiceChannel!4!WorkEndConfirmFlag],
            NULL as [ServiceChannel!4!WorkEndDate],
            NULL as [ServiceChannel!4!WorkEndDateOriginal],
            NULL as [ServiceChannel!4!WorkStartConfirmFlag],
            NULL as [ServiceChannel!4!WorkStartDate],
            NULL as [ServiceChannel!4!SysLastUserID],
            NULL as [ServiceChannel!4!SysLastUpdatedDate]
            /*********************************************************************************
            Project: 210474 APD - Enhancements to support multiple concurrent service channels
            Note:	Added the above to provide the data on Service Channels
                   	M.A. 20061215
            *********************************************************************************/            

    UNION ALL


    -- Select Search Results Header level

    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, 
            --Search Results Record
            IsNull(tmp.LynxId, ''),
            --IsNull(cc.ClientClaimNumber, ''),
            IsNull(c.ClientClaimNumber, ''),
            IsNull(tmp.InsuredNameLast, ''),
            IsNull(tmp.InsuredNameFirst, ''),
            IsNull(tmp.InsuredBusinessName, ''),
            IsNull(ca.OwnerUserID, 0),
            IsNull(ou.NameFirst, ''),
            IsNull(ou.NameLast, ''),
            IsNull(c.CarrierRepUserID, 0),
            IsNull(cru.NameFirst, ''),
            IsNull(cru.NameLast, ''),
            IsNull(cro.ClientOfficeId, ''),            
            IsNull(cro.Name, ''),
            IsNull(c.IntakeFinishDate, ''),            
            --ClaimVehicle
            Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
            Null, Null, Null,
            --Service Channel
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL

    FROM    @tmpClaimSummary tmp
    LEFT JOIN dbo.utb_claim c ON (tmp.LynxID = c.LynxID)
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels

	Notes:	Removed the reference to the table utb_claim_coverage to get the value 
		from "ClientClaimNumber" column.  The value is available in "Claim" table.
		M.A. 20061107
	*********************************************************************************/
    --LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID)
    LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
    LEFT JOIN dbo.utb_user cru ON (c.CarrierRepUserID = cru.UserID)
    LEFT JOIN dbo.utb_office cro ON (cru.OfficeID = cro.OfficeID)
    WHERE ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
    
    UNION ALL


    --  Select list of vehicles belonging to claim.

    SELECT  3, -- Selecting TOP allows return of only 1 record, even if there are multiple owners
            2,
            NULL, NULL, NULL, NULL, 
            --Claim
            IsNull(tmp.LynxId, ''), 
            IsNull(c.ClientClaimNumber, ''), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            --ClaimVehicle
            tmp.ClaimAspectID,
            IsNull(tmp.VehicleNumber, ''),
            IsNull(cv.VehicleYear, ''),
            IsNull(cv.Make, ''),
            IsNull(cv.Model, ''),
            IsNull((SELECT Top 1 i.NameLast 
                FROM dbo.utb_claim_aspect_involved cai
                LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                LEFT JOIN dbo.utb_involved_role ir ON (I.InvolvedID = ir.InvolvedID)
                LEFT JOIN dbo.utb_involved_Role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                WHERE cai.ClaimAspectID = tmp.ClaimAspectID
                  AND cai.EnabledFlag = 1) , ''),
                  --AND (irt.Name = 'Claimant' OR irt.Name='Insured')), ''), -- To Show Owner Last Name for all claim
            IsNull((SELECT Top 1 i.NameFirst 
                FROM dbo.utb_claim_aspect_involved cai
                LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                LEFT JOIN dbo.utb_involved_role ir ON (I.InvolvedID = ir.InvolvedID)
                LEFT JOIN dbo.utb_involved_Role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                WHERE cai.ClaimAspectID = tmp.ClaimAspectID
                  AND cai.EnabledFlag = 1) , ''),
                 -- AND (irt.Name = 'Claimant' OR irt.Name='Insured')), ''),-- To Show Owner First Name for all claim
            IsNull((SELECT Top 1 i.BusinessName 
                FROM dbo.utb_claim_aspect_involved cai
                LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                LEFT JOIN dbo.utb_involved_role ir ON (I.InvolvedID = ir.InvolvedID)
                LEFT JOIN dbo.utb_involved_Role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                WHERE cai.ClaimAspectID = tmp.ClaimAspectID
                  AND cai.EnabledFlag = 1
                  AND (irt.Name = 'Claimant' OR irt.Name='Insured')), ''),
            IsNull(tmp.VehicleStatus, ''),
            --Project:210474 APD Remarked-off the following to support the schema change M.A.20061218
            --IsNull(at.Name, '')
            --Project:210474 APD Added the following to support the schema change M.A.20061219
            (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD),
            a.ApplicationID, 
            a.Name,
            at.Name,
            --Service Channel
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL

    FROM  @tmpVehicleSummary tmp
    LEFT JOIN dbo.utb_claim c ON (tmp.LynxID = c.LynxID)
    LEFT JOIN dbo.utb_claim_aspect ca on (tmp.ClaimAspectID = ca.ClaimAspectID)
    --Project:210474 APD Remarked-off the following to support the schema change M.A.20061218
    --LEFT JOIN dbo.utb_assignment_type at on (ca.CurrentAssignmentTypeID = at.AssignmentTypeID)--Project:210474 APD The column was added in utb_Claim_Aspect so changed it back to the original state. M.A.20061201
    --Project:210474 APD Added the following to support the schema change M.A.20061218
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
    on ca.ClaimAspectID = casc.ClaimAspectID
    and casc.Primaryflag=1
    LEFT JOIN dbo.utb_claim_vehicle cv on (tmp.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_application a on ca.SourceApplicationID = a.ApplicationID
    LEFT JOIN dbo.utb_assignment_type at on ca.InitialAssignmentTypeID = at.AssignmentTypeID 


UNION ALL

--*********************************************************
--SERVICE CHANNEL DATA
--*********************************************************

    SELECT
            4,
            3,
            NULL, NULL, NULL, NULL,
--          Claim
            IsNull(tmp.LynxId, ''), 
            IsNull(c.ClientClaimNumber, ''), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL,
--          ClaimVehicle
            tmp.ClaimAspectID,
            IsNull(ca.ClaimAspectNumber, ''),
            Null, Null, Null, Null, Null, Null, Null, NULL,
            Null, Null, Null,
--          Service Channel Information
            ISNULL(cast(casc.ClaimAspectID as varchar(20)),''),
            isnull(cast(s.StatusID as varchar(05)),''),
            isnull(cast(s.name as varchar(20)),''),
            isnull(cast(cas.StatusTypeCD as varchar(20)),''),
            isnull(cast(fn2.Name as varchar(20)),''),
            isnull(cast(casc.ClaimAspectServiceChannelID as varchar(20)),''),
            isNull(cast(casc.CreatedUserID as varchar(20)),''),
            isnull(casc.CreatedDate,''),
            isnull(cast(casc.DispositionTypeCD as varchar(04)),''),
            isnull(cast(casc.EnabledFlag as varchar(01)),''),
            isnull(casc.InspectionDate,''),
            isnull(casc.ClientInvoiceDate ,''),
            isnull(casc.OriginalCompleteDate ,''),
            isnull(casc.OriginalEstimateDate ,''),
            isnull(cast(casc.PrimaryFlag as varchar(01)),''),
            isnull(cast(casc.ServiceChannelCD as varchar(02)),''),
            isnull(cast(fn.Name as varchar(20)),''),
            isnull(cast(casc.WorkEndConfirmFlag as varchar(01)),''),
            isnull(casc.WorkEndDate ,''),
            isnull(casc.WorkEndDateOriginal ,''),
            isnull(cast(casc.WorkStartConfirmFlag as varchar(20)),''),
            isnull(casc.WorkStartDate ,''),
            isnull(cast(casc.SysLastUserID as varchar(20)),''),
            isnull(casc.SysLastUpdatedDate, '')
                
            
    FROM  @tmpVehicleSummary tmp
    LEFT JOIN dbo.utb_claim c ON (tmp.LynxID = c.LynxID)

    LEFT JOIN dbo.utb_claim_aspect ca on (tmp.ClaimAspectID = ca.ClaimAspectID)

    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
    on  tmp.ClaimAspectID = casc.ClaimAspectID

    Left outer join dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel','ServiceChannelCD') fn
    on fn.Code = casc.ServiceChannelCD

    LEFT OUTER JOIN utb_Claim_Aspect_Status cas
    on ca.ClaimAspectID = cas.ClaimAspectID
    and casc.ServiceChannelCD = cas.ServiceChannelCD
    and cas.StatusTypeCD = 'SC'

    left outer join utb_Status s
    on s.StatusID = cas.StatusID

    left outer join dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Status','StatusTypeCD') fn2
    on    cas.StatusTypeCD = fn2.code
    --where casc.ServiceChannelCD like @ServiceChannelCD

--   ORDER BY [Claim!2!LynxID], Tag
     order by   [Claim!2!LynxID] DESC,[Claim!2!ClaimNumber] , [Vehicle!3!ClaimAspectID],[Vehicle!3!VehicleNumber],[ServiceChannel!4!ClaimAspectServiceChannelID], tag
  FOR XML EXPLICIT


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCarrierRepDeskGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCarrierRepDeskGetDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/