-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDADocumentDetailsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetDADocumentDetailsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetDADocumentDetailsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			2014Oct22
* FUNCTION:     Gets the details for a document and returns as XML
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetDADocumentDetailsWSXML]
	@iDocumentID INT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @iLastDocumentID   AS INT
	DECLARE @dtNow			   AS DATETIME
	DECLARE @iError			   AS INT

    SET @ProcName = 'uspHyperquestGetDADocumentDetailsWSXML'
    SET @ApplicationCD = 'APDHyperquestPartner'
	SET @dtNow = CURRENT_TIMESTAMP
	SET @iError = 0
	
    -- Validation
    IF NOT EXISTS (	SELECT DocumentID FROM utb_document WHERE DocumentID=@iDocumentID AND EnabledFlag = 1)
    BEGIN
		RAISERROR('%s: DocumentID does not exist.', 16, 1, @ProcName)
		SET @iError = 100
    END

    -- PreSQL
    
    -- Main SQL
		SELECT 
		1 AS Tag
		, NULL AS Parent
		, (SELECT NEWID()) AS [Root!1!JobID]
		, c.InsuranceCompanyID AS [Root!1!InsuranceCompanyID]
		, ca.LynxID AS [Root!1!LynxID]
		, c.ClientClaimNumber AS [Root!1!ClientClaimNumber]
		, d.DocumentID AS [Root!1!DocumentID]
		, d.AssignmentID AS [Root!1!AssignmentID]
		, d.CreatedDate AS [Root!1!ReceivedDate]
		, d.DocumentTypeID AS [Root!1!DocumentTypeID]
		, dt.Name AS [Root!1!DocumentTypeName]
		, ds.Name AS [Root!1!DocumentSource]
		, d.ImageType AS [Root!1!DocumentImageType]
		, d.ImageLocation AS [Root!1!DocumentImageLocation]
		, cascd.ClaimAspectServiceChannelID AS [Root!1!ClaimAspectServiceChannelID]
		, casc.ClaimAspectID AS [Root!1!ClaimAspectID]
		, NULL AS [Root!1!JobXSLFile]
		, 'Unprocessed' AS [Root!1!JobStatus]
		, 'APD DeskAudit to Hyperquest (MANUAL)' AS [Root!1!JobStatusDetails]
		, 1  AS [Root!1!JobPriority]
		, @dtNow AS [Root!1!JobCreatedDate]
		, NULL AS [Root!1!JobProcessedDate]
		, NULL AS [Root!1!JobTransformedDate]
		, NULL AS [Root!1!JobTransmittedDate]
		, NULL AS [Root!1!JobArchivedDate]
		, NULL AS [Root!1!JobFinishedDate]
		, 1  AS [Root!1!EnabledFlag]
		, 'NONE' AS [Root!1!JobXML]
		, 0 AS [Root!1!SysLastUserID]
		, d.CreatedUserID AS [Root!1!CreatedUserID]
		, @dtNow AS [Root!1!SysLastUpdatedDate]
	FROM 
		utb_document AS d
		INNER JOIN utb_document_type AS dt
			ON d.DocumentTypeID = dt.DocumentTypeID
		INNER JOIN utb_document_source AS ds
			ON d.DocumentSourceID = ds.DocumentSourceID
		INNER JOIN utb_claim_aspect_service_channel_document AS cascd
			ON cascd.DocumentID = d.DocumentID
		INNER JOIN utb_claim_aspect_service_channel AS casc
			ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
		INNER JOIN utb_claim_aspect AS ca
			ON ca.ClaimAspectID = casc.ClaimAspectID
		INNER JOIN utb_claim AS c
			ON c.LynxID = ca.LynxID
		INNER JOIN utb_hyperquest_insurance AS hi
			ON hi.InsuranceCompanyID = c.InsuranceCompanyID
		LEFT JOIN utb_hyperquestsvc_jobs hj
			ON hj.DocumentID = d.DocumentID
	WHERE
		hi.EnabledFlag = 1
		AND d.EnabledFlag = 1	
		AND d.DocumentTypeID IN (3,10)
		AND casc.ServiceChannelCD='DA'
		AND d.DocumentID = @iDocumentID
		AND hj.DocumentID IS NULL
		AND d.DocumentID NOT IN
		(
			SELECT DocumentID FROM dbo.utb_hyperquest_processed
		)

	FOR XML EXPLICIT	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetDADocumentDetailsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetDADocumentDetailsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/