-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimReopenAssignGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimReopenAssignGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimReopenAssignGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Procedure that returns a list of assignable users for Claim reopen
*
* PARAMETERS:  
* (I) @LynxID                LynxID that is being reopened
* (I) @UserID                User ID who is performing this action
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimReopenAssignGetListXML
    @LynxID         udt_std_id_big,
    @UserID         udt_std_id,
    @ApplicationCD  udt_std_cd='APD'
AS
BEGIN

    -- Set database Options

    SET NOCOUNT ON


    -- Declare internal variables
    DECLARE @now                            udt_std_datetime
    DECLARE @error                          int
    DECLARE @rowcount                       int
    DECLARE @ClaimAspectTypeIDClaim         udt_std_id

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimReopenAssignGetListXML'

    DECLARE @debug                          udt_std_flag

    SET @debug = 0

    -- Check to make sure a valid Lynx id was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (dbo.ufnUtilityIsUserActive(@UserID, @ApplicationCD, NULL) = 0)
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    IF @Debug = 1
    BEGIN
        Print ''
        Print 'Input Parameters:'
        Print '@LynxID=' + convert(varchar, @LynxID)
        Print '@UserID=' + convert(varchar, @UserID)
    END

   SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    IF @Debug = 1
    BEGIN
        Print ''
        Print 'ClaimAspectTypeID=' + convert(varchar, @ClaimAspectTypeIDClaim)
    END
    
    -- Select Root Level

    SELECT  1 AS tag,
            NULL AS parent,
            @LynxID AS [Root!1!LynxID],
            @UserID AS [Root!1!UserID],
            s.Name  AS [Root!1!ClaimStatus],
            -- Claims Original Owner Active Status
            NULL AS [Owner!2!OwnerUserID],
            NULL AS [Owner!2!OwnerUserNameFirst],
            NULL AS [Owner!2!OwnerUserNameLast],
            NULL AS [Owner!2!OwnerUserActive],
            -- Current User
            NULL AS [Supervisor!3!UserID],
            -- Subordinates for the current user
            NULL AS [Subordinates!4!UserID],
            NULL AS [Subordinates!4!NameFirst],
            NULL AS [Subordinates!4!NameLast]
    FROM dbo.utb_claim_aspect ca
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	"StatusID" is no longer part of utb_claim_aspect.  Had to add the table 
		utb_Claim_Aspect_Status to join utb_claim_aspect to utb_status
		M.A. 20061108
	*********************************************************************************/
	inner join utb_Claim_Aspect_Status cas on ca.ClaimAspectID = cas.ClaimAspectID

        LEFT JOIN dbo.utb_status s on cas.StatusID = s.StatusID
        WHERE ca.LynxID = @LynxID
          and ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim

    UNION ALL
    
    SELECT  2,
            1,
            NULL, NULL, NULL,
            -- Claims Original Owner Active Status
            ca.OwnerUserID,
            u.NameFirst,
            u.NameLast,
            dbo.ufnUtilityIsUserActive(ca.OwnerUserID, @ApplicationCD, NULL),
            -- Current User
            NULL,
            -- Subordinates for the current user
            NULL, NULL, NULL
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT JOIN dbo.utb_user u ON (ca.OwnerUserID = u.UserID)
    WHERE c.LynxID = @LynxID
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
      
    UNION ALL
    
    SELECT  3,
            1,
            NULL, NULL, NULL,
            -- Claims Original Owner Active Status
            NULL, NULL, NULL, NULL,
            -- Current User
            @UserID,
            -- Subordinates for the current user
            NULL, NULL, NULL

    UNION ALL

    SELECT  4,
            3,
            NULL, NULL, NULL,
            -- Claims Original Owner Active Status
            NULL, NULL, NULL, NULL,
            -- Current User
            NULL,
            -- Subordinates for the current user
            NewUserID,
            NameFirst,
            NameLast
    FROM dbo.ufnUtilityOrgChartGetList(@UserID)
    WHERE NewUserID <> @UserID
               
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimReopenAssignGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimReopenAssignGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/