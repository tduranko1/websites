-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDiaryGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDiaryGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDiaryGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets Diary TODO list data
*
* PARAMETERS:  
*				@LynxID - Key ID for the claim
*				@UserID - Logged in User
*				@SubordinateUserID
*				@SubordinatesFlag
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDiaryGetListWSXML
    @LynxID               udt_std_int_big,
    @UserID               udt_std_id,
    @SubordinateUserID    udt_std_id=NULL,
    @SubordinatesFlag     tinyint=0
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspDiaryGetListWSXML'


    -- Check to make sure a valid Lynx ID and User ID was passed in

    IF  (@LynxID IS NULL) OR
        ((NOT @LynxID = 0) AND NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID

        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    IF  (@SubordinateUserID IS NOT NULL) AND
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @SubordinateUserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@SubordinateUserID|%u', 16, 1, @ProcName, @SubordinateUserID)
        RETURN
    END


    -- Declare Local Table variable

    DECLARE @tmpDiary TABLE
    (
        LynxID      bigint     NOT NULL
    )


    DECLARE @tmpUser TABLE
    (
        UserID      int        NOT NULL
    )


    -- Gather Users for whom tasks will be pulled.  ------------------------------------

    IF (@SubordinateUserID IS NULL) AND (@SubordinatesFlag = 0)
    BEGIN
      INSERT INTO @tmpUser
      SELECT @UserID
    END


    IF (@SubordinateUserID IS NULL) AND (@SubordinatesFlag = 1)
    BEGIN
      INSERT INTO @tmpUser
      SELECT NewUserID
      FROM dbo.ufnUtilityOrgChartGetList(@UserID)
      WHERE SupervisorID = @UserID
         OR SupervisorID IS NULL
    END

    IF (@SubordinateUserID IS NULL) AND (@SubordinatesFlag = 2)
    BEGIN
      INSERT INTO @tmpUser
      SELECT NewUserID
      FROM dbo.ufnUtilityOrgChartGetList(@UserID)
    END


    IF (@SubordinateUserID IS NOT NULL) AND (@SubordinatesFlag = 0)
    BEGIN
      INSERT INTO @tmpUser
      SELECT @SubordinateUserID
    END


    IF (@SubordinateUserID IS NOT NULL) AND (@SubordinatesFlag = 1)
    BEGIN
      INSERT INTO @tmpUser
      SELECT NewUserID
      FROM dbo.ufnUtilityOrgChartGetList(@SubordinateUserID)
      WHERE SupervisorID = @SubordinateUserID
         OR SupervisorID IS NULL

    END

    IF (@SubordinateUserID IS NOT NULL) AND (@SubordinatesFlag = 2)
    BEGIN
      INSERT INTO @tmpUser
      SELECT NewUserID
      FROM dbo.ufnUtilityOrgChartGetList(@SubordinateUserID)
    END

--    select * from @tmpUser
    ------------------------------------------------------------------------------------



    -- Check the LynxID passed in.  If 0 was passed, the request is for diary entries for ALL diary entries of all claims in
    -- which the user has at least 1 task assigned to them

    IF @LynxID = 0
    BEGIN
        -- Populate with the LynxIDs of all claims with diary entries assigned to the user

        INSERT INTO @tmpDiary
          SELECT DISTINCT ca.LynxID
            FROM dbo.utb_checklist c
    		/*********************************************************************************
    		Project: 210474 APD - Enhancements to support multiple concurrent service channels
    		Note:	"ClaimAspectID" is not part of utb_checklist.  Added a table
			utb_Claim_Aspect_Service_Channel between utb_claim_aspect and
			utb_checklist to join them.
			M.A. 20061108
    		*********************************************************************************/
	    INNER JOIN utb_Claim_Aspect_Service_Channel casc on c.ClaimAspectServiceChannelID = c.ClaimAspectServiceChannelID
            LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
            WHERE c.AssignedUserID IN (SELECT UserID FROM @tmpUser)

          UNION

          SELECT  LynxID
            FROM  dbo.utb_claim_aspect
            WHERE OwnerUserID IN (SELECT UserID FROM @tmpUser)
              AND ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = 'Claim')
    END
    ELSE
    BEGIN
        -- We want only the one claim's tasks
        INSERT INTO @tmpDiary
          SELECT DISTINCT ca.LynxID
            FROM dbo.utb_checklist c
    		/*********************************************************************************
    		Project: 210474 APD - Enhancements to support multiple concurrent service channels
    		Note:	"ClaimAspectID" is not part of utb_checklist.  Added a table
			utb_Claim_Aspect_Service_Channel between utb_claim_aspect and
			utb_checklist to join them.
			M.A. 20061108
    		*********************************************************************************/
	    INNER JOIN utb_Claim_Aspect_Service_Channel casc on c.ClaimAspectServiceChannelID = c.ClaimAspectServiceChannelID
            LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
            WHERE ca.LynxID = @LynxID
    END

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpDiary', 16, 1, @ProcName)
        RETURN
    END


    -- Begin XML Select

    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            @LynxID AS [Root!1!LynxID],
            @UserID AS [Root!1!UserID],
            -- Checklist Information
            NULL AS [DiaryTask!2!CheckListID],
            NULL AS [DiaryTask!2!AssignedUserNameFirst],
            NULL AS [DiaryTask!2!AssignedUserNameLast],
            NULL AS [DiaryTask!2!AssignedUserID],
            NULL AS [DiaryTask!2!CreatedUserNameFirst],
            NULL AS [DiaryTask!2!CreatedUserNameLast],
            NULL AS [DiaryTask!2!CreatedUserRoleName],
            NULL AS [DiaryTask!2!LynxID],
            NULL AS [DiaryTask!2!InsuranceCompanyName],
            NULL AS [DiaryTask!2!ClaimAspectID],
            NULL AS [DiaryTask!2!ClaimAspectTypeName],
            NULL AS [DiaryTask!2!ClaimAspectNumber],
            NULL AS [DiaryTask!2!StatusName],
            NULL AS [DiaryTask!2!TaskName],
            NULL AS [DiaryTask!2!AlarmDate],
            NULL AS [DiaryTask!2!CreatedDate],
            NULL AS [DiaryTask!2!UserTaskDescription],
            NULL AS [DiaryTask!2!AlarmModificationAllowedFlag],
            NULL AS [DiaryTask!2!CompletionAllowedFlag],
            NULL AS [DiaryTask!2!ManagerialTaskFlag],  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL AS [DiaryTask!2!ClaimAspectServiceChannelID],
            NULL AS [DiaryTask!2!ServiceChannelCD],
            NULL AS [DiaryTask!2!SysLastUpdatedDate]


    UNION ALL


    -- Select Checklist Information Level

    SELECT  2,
            1,
            NULL, NULL,
            -- Checklist Information
            IsNull(c.ChecklistID, 0),
            IsNull(au.NameFirst, ''),
            IsNull(au.NameLast, ''),
            IsNull(au.UserID, ''),
            IsNull(cu.NameFirst, ''),
            IsNull(cu.NameLast, ''),
            IsNull(cr.Name, ''),
            IsNull(ca.LynxID, 0),
            IsNull(ins.Name, ''),
            IsNull(casc.ClaimAspectID, ''),--Project: 210474 APD - Enhancements to support multiple concurrent service channels.  M.A. 20061108 The column comes from utb_Claim_Aspect_Service_Channel
            IsNull(cat.Name, ''),
            IsNull(ca.ClaimAspectNumber, 0),
            IsNull(s.Name, ''),
            IsNull(t.Name, ''),
            IsNull(c.AlarmDate, ''),
            IsNull(c.CreatedDate, ''),
            IsNull(c.UserTaskDescription, ''),
            -- The next 3 fields are permission fields for the task.  Select TOP record from sorted value
            -- list to resolve multiple role conflicts.
            IsNull((SELECT TOP 1 tr.AlarmModificationAllowedFlag
                          FROM   dbo.utb_task_role tr
                          WHERE  tr.TaskID = c.TaskID
                          AND    tr.RoleID IN
                                (SELECT RoleID
                                 FROM   dbo.utb_user_role
                                 WHERE  UserID = @UserID)
                          ORDER BY tr.AlarmModificationAllowedFlag DESC)  , 0),
            IsNull((SELECT TOP 1 tr.CompletionAllowedFlag
                          FROM   dbo.utb_task_role tr
                          WHERE  tr.TaskID = c.TaskID
                          AND    tr.RoleID IN
                                (SELECT RoleID
                                 FROM   dbo.utb_user_role
                                 WHERE  UserID = @UserID)
                          ORDER BY tr.CompletionAllowedFlag DESC)  , 0),
            cu.SupervisorFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061114
            casc.ClaimAspectServiceChannelID,
            casc.ServiceChannelCD,
            dbo.ufnUtilityGetDateString( c.SysLastUpdatedDate )

    FROM    dbo.utb_checklist c
    LEFT JOIN dbo.utb_user au ON (c.AssignedUserID = au.UserID)
    LEFT JOIN dbo.utb_user cu ON (c.CreatedUserID = cu.UserID)
    LEFT JOIN dbo.utb_role cr ON (c.CreatedRoleID = cr.RoleID)
    /*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	"ClaimAspectID" is not part of utb_checklist.  Added a table
	utb_Claim_Aspect_Service_Channel between utb_claim_aspect and
	utb_checklist to join them.
	M.A. 20061108
    *********************************************************************************/
    INNER JOIN utb_Claim_Aspect_Service_Channel casc on c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    LEFT JOIN dbo.utb_status s ON (c.StatusID = s.StatusID)
    LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)
    LEFT JOIN dbo.utb_claim clm ON (clm.LynxID = ca.LynxID)
    LEFT JOIN dbo.utb_insurance ins ON (ins.InsuranceCompanyID = clm.InsuranceCompanyID)

    WHERE   ca.LynxID IN (SELECT LynxID
                            FROM @tmpDiary)


    ORDER BY Tag
    FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDiaryGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDiaryGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspDiaryGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/