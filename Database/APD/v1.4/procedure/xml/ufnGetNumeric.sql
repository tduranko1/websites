-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnGetNumeric' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnGetNumeric 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnGetNumeric
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets numeric values from a string
*
* PARAMETERS:  
*		@strAlphaNumeric - VARCHAR mixed numeric and alphnumeric
*
* RETURNS:
*       Numeric values from Varchar
*
* REVISIONS:	20Oct2016 - TVD - 1.0 - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnGetNumeric (@strAlphaNumeric VARCHAR(256))
RETURNS VARCHAR(256)
AS
BEGIN
    -- Declare internal variables
      BEGIN
            DECLARE @intAlpha INT
            
            SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric)
            BEGIN
                  WHILE @intAlpha > 0
                  BEGIN
                        SET @strAlphaNumeric = STUFF(@strAlphaNumeric, @intAlpha, 1, '' )
                        SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric )
                  END
            END
            
            RETURN ISNULL(@strAlphaNumeric,0)
      END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnGetNumeric' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnGetNumeric TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.ufnGetNumeric TO 
        wsAPDUser


    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnGetNumeric' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnGetNumeric TO 
            ugr_lynxapd
        GRANT SELECT ON dbo.ufnGetNumeric TO 
            wsAPDUser

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/