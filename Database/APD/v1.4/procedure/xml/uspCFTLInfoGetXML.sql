-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFTLInfoGetXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCFTLInfoGetXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCFTLInfoGetXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCFTLInfoGetXML
    @ClaimAspectID      udt_std_id_big,
    @UserID				udt_std_id_big
AS
BEGIN
    -- Declare internal variables
	DECLARE @ClaimNumber as varchar(100)
	DECLARE @CarrierRepLName as varchar(50)
	DECLARE @CarrierRepFName as varchar(50)
	DECLARE @InsuredLName as varchar(50)
	DECLARE @InsuredFName as varchar(50)
	DECLARE @InsuredBName as varchar(100)
	DECLARE @OwnerLName as varchar(50)
	DECLARE @OwnerFName as varchar(50)
	DECLARE @OwnerBName as varchar(100)
	DECLARE @OwnerAddress1 as varchar(100)
	DECLARE @OwnerAddress2 as varchar(100)
	DECLARE @OwnerCity as varchar(100)
	DECLARE @OwnerZip as varchar(5)
	DECLARE @OwnerState as varchar(2)
	DECLARE @OwnerDayPhone as varchar(15)
	DECLARE @OwnerDayPhoneExt as varchar(5)
	DECLARE @OwnerNightPhone as varchar(15)
	DECLARE @OwnerNightPhoneExt as varchar(5)
	DECLARE @OwnerAltPhone as varchar(15)
	DECLARE @OwnerAltPhoneExt as varchar(5)
	DECLARE @OwnerEmailAddress as varchar(100)
	DECLARE @LossState as varchar(2)
	DECLARE @LossDate as varchar(10)
	DECLARE @VehicleYear as varchar(4)
	DECLARE @VehicleMake as varchar(50)
	DECLARE @VehicleModel as varchar(50)
	DECLARE @VehicleBody as varchar(50)
	DECLARE @VehicleVIN as varchar(20)
	DECLARE @VehicleLicensePlate as varchar(10)
	DECLARE @VehicleLicenseState as varchar(50)
	DECLARE @VehicleMileage as varchar(12)
	DECLARE @UserName as varchar(100)
	DECLARE @UserPhone as varchar(15)
	DECLARE @UserEmail as varchar(100)
	DECLARE @LHName as varchar(100)
	DECLARE @LHAddress1 as varchar(100)
	DECLARE @LHAddress2 as varchar(100)
	DECLARE @LHAddressCity as varchar(100)
	DECLARE @LHAddressState as varchar(100)
	DECLARE @LHAddressZip as varchar(100)
	DECLARE @LHFaxNumber as varchar(15)
	DECLARE @LHPhoneNumber as varchar(15)
	DECLARE @LHEmailAddress as varchar(100)
	DECLARE @InsuranceCompanyName as varchar(100)
	DECLARE @TitleNames as varchar(100)
	DECLARE @TitleState as varchar(100)
	DECLARE @TitleNameConfirmedFlag AS bit
	

    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFTLInfoGetXML'
    
    SELECT @InsuranceCompanyName = Name
    FROM utb_insurance i
    LEFT JOIN utb_claim c ON i.InsuranceCompanyID = c.InsuranceCompanyID
    LEFT JOIN utb_claim_aspect ca ON c.LynxID = ca.LynxID
    
    SELECT  @UserName = NameFirst + ' ' + NameLast,
			   @UserPhone = PhoneAreaCode + PhoneExchangeNumber + PhoneUnitNumber,
			   @UserEmail = EmailAddress
    FROM utb_user
	 WHERE UserID = @UserID

	 SELECT	@VehicleYear = cv.VehicleYear,
			   @VehicleModel = cv.Model,
			   @VehicleMake = cv.Make,
			   @VehicleBody = cv.BodyStyle,
			   @VehicleVIN = cv.VIN,
			   @VehicleLicensePlate = cv.LicensePlateNumber,
			   @VehicleLicenseState = cv.LicensePlateState,
			   @VehicleMileage = cv.Mileage,
			   @ClaimNumber = c.ClientClaimNumber,
			   @CarrierRepLName = cu.NameLast,
			   @CarrierRepFName = cu.NameFirst,
			   @LossDate = CONVERT(varchar, c.LossDate, 110),
			   @LossState = c.LossState,
			   @TitleNames = cv.TitleName,
			   @TitleState = cv.TitleState,
			   @TitleNameConfirmedFlag = cv.TitleNameConfirmFlag
	FROM utb_claim_aspect ca
	LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID 
	LEFT JOIN utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID 
	LEFT JOIN utb_user cu ON c.CarrierRepUserID = cu.UserID 
	WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT	@OwnerFName = LTrim(RTrim(IsNull(i.NameFirst, ''))),
			   @OwnerLName = LTrim(RTrim(IsNull(i.NameLast, ''))),
			   @OwnerBName = LTrim(RTrim(IsNull(i.BusinessName, ''))),
			   @OwnerAddress1 = LTrim(RTrim(IsNull(i.Address1, ''))),
			   @OwnerAddress2 = LTrim(RTrim(IsNull(i.Address2, ''))),
			   @OwnerCity = LTrim(RTrim(IsNull(i.AddressCity, ''))),
			   @OwnerState = LTrim(RTrim(IsNull(i.AddressState, ''))),
			   @OwnerZip = LTrim(RTrim(IsNull(i.AddressZip, ''))),
            @OwnerDayPhone = IsNull(LTRIM(RTRIM(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber)), ''),
            @OwnerDayPhoneExt = LTrim(RTrim(IsNull(i.DayExtensionNumber, ''))),
            @OwnerNightPhone = IsNull(LTRIM(RTRIM(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber)), ''), 
            @OwnerNightPhoneExt = LTrim(RTrim(IsNull(i.NightExtensionNumber, ''))),
            @OwnerAltPhone = IsNull(LTRIM(RTRIM(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber)), ''),
            @OwnerAltPhoneExt = LTrim(RTrim(IsNull(i.AlternateExtensionNumber, ''))),
            @OwnerEmailAddress = LTrim(RTrim(IsNull(i.EmailAddress, '')))
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Owner'


    SELECT	@InsuredFName = LTrim(RTrim(IsNull(i.NameFirst, ''))),
			   @InsuredLName = LTrim(RTrim(IsNull(i.NameLast, ''))),
			   @InsuredBName = LTrim(RTrim(IsNull(i.BusinessName, '')))
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Insured'
      
      
    SELECT @LHName = lh.Name,
           @LHAddress1 = lh.Address1,
           @LHAddress2 = lh.Address2,
           @LHAddressCity = lh.AddressCity,
           @LHAddressState = lh.AddressState,
           @LHAddressZip = lh.AddressZip,
           @LHPhoneNumber = lh.PhoneAreaCode + lh.PhoneExchangeNumber + lh.PhoneUnitNumber,
           @LHFaxNumber = lh.FaxAreaCode + lh.FaxExchangeNumber + lh.FaxUnitNumber,
           @LHEmailAddress =lh.EmailAddress 
    FROM utb_lien_holder lh
    LEFT JOIN utb_claim_aspect_service_channel casc ON lh.LienHolderID = casc.LienHolderID
    LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID 
    WHERE ca.ClaimAspectID = @ClaimAspectID 
      AND casc.ServiceChannelCD = 'TL'
      AND casc.EnabledFlag = 1
      
    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName as [Root!1!InsuranceCompanyName],
            isNull(@ClaimNumber, '') as [Root!1!ClaimNumber],
			   isNull(@CarrierRepLName, '') as [Root!1!CarrierRepLName],
			   isNull(@CarrierRepFName, '') as [Root!1!CarrierRepFName],
			   isNull(@InsuredLName, '') as [Root!1!InsuredLName],
			   isNull(@InsuredFName, '') as [Root!1!InsuredFName],
			   isNull(@InsuredBName, '') as [Root!1!InsuredBName],
			   isNull(@OwnerLName, '') as [Root!1!OwnerLName],
			   isNull(@OwnerFName, '') as [Root!1!OwnerFName],
			   isNull(@OwnerBName, '') as [Root!1!OwnerBName],
			   isNull(@OwnerAddress1, '') as [Root!1!OwnerAddress1],
			   isNull(@OwnerAddress2, '') as [Root!1!OwnerAddress2],
			   isNull(@OwnerCity, '') as [Root!1!OwnerCity],
			   isNull(@OwnerState, '') as [Root!1!OwnerState],
			   isNull(@OwnerZip, '') as [Root!1!OwnerZip],
			   isNull(@OwnerDayPhone, '') as [Root!1!OwnerDayPhone],
			   isNull(@OwnerDayPhoneExt, '') as [Root!1!OwnerDayPhoneExt],
			   isNull(@OwnerNightPhone, '') as [Root!1!OwnerNightPhone],
			   isNull(@OwnerNightPhoneExt, '') as [Root!1!OwnerNightPhoneExt],
			   isNull(@OwnerAltPhone, '') as [Root!1!OwnerAltPhone],
			   isNull(@OwnerAltPhoneExt, '') as [Root!1!OwnerAltPhoneExt],
			   isNull(@OwnerEmailAddress, '') as [Root!1!OwnerEmailAddress],
			   isNull(@LossState,   '') as [Root!1!LossState],
			   isNull(@LossDate, '') as [Root!1!LossDate],
			   isNull(@VehicleYear, '') as [Root!1!VehicleYear],
			   isNull(@VehicleMake, '') as [Root!1!VehicleMake],
			   isNull(@VehicleModel, '') as [Root!1!VehicleModel],
			   isNull(@VehicleBody, '') as [Root!1!VehicleBody],
			   isNull(@VehicleVIN, '') as [Root!1!VehicleVIN],
			   isNull(@VehicleLicensePlate, '') as [Root!1!VehicleLicensePlate],
			   isNull(@VehicleLicenseState, '') as [Root!1!VehicleLicenseState],
			   isNull(@VehicleMileage, '') as [Root!1!VehicleMileage],
			   isNull(@UserName, '') as [Root!1!UserName],
			   isNull(@UserPhone, '') as [Root!1!UserPhone],
			   isNull(@UserEmail, '') as [Root!1!UserEmail],
			   ISNULL(@LHName, '') as [Root!1!LienHolderName],
			   ISNULL(@LHAddress1, '') as [Root!1!LienHolderAddress1],
			   ISNULL(@LHAddress2, '') as [Root!1!LienHolderAddress2],
			   ISNULL(@LHAddressCity, '') as [Root!1!LienHolderAddressCity],
			   ISNULL(@LHAddressState, '') as [Root!1!LienHolderAddressState],
			   ISNULL(@LHAddressZip, '') as [Root!1!LienHolderAddressZip],
			   ISNULL(@LHPhoneNumber, '') as [Root!1!LienHolderPhoneNumber],
			   ISNULL(@LHFaxNumber, '') as [Root!1!LienHolderFaxNumber],
			   ISNULL(@LHEmailAddress, '') as [Root!1!LienHolderEmailAddress],
			   ISNULL(@TitleNames, '') as [Root!1!TitleNames],
			   ISNULL(@TitleState, '') as [Root!1!TitleState],
			   @TitleNameConfirmedFlag as [Root!1!TitleNameConfirmedFlag]



    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFTLInfoGetXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFTLInfoGetXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/