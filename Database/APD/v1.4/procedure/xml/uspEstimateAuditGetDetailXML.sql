-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateAuditGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateAuditGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateAuditGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves Shop Audit stuff
*
* PARAMETERS:  
* (I) ClaimAspectID        
*
* RESULT SET:
* An XML Data stream containing payment information
*
*
* VSS
* $Workfile: uspEstimateAuditGetDetailXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspEstimateAuditGetDetailXML.SQL $
* $Revision: 1 $
* $Author: Dan Price $
* $Date: 10/23/01 5:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspEstimateAuditGetDetailXML
    @DocumentID           udt_std_id_big
AS
BEGIN
    
    SET NOCOUNT ON
    
    -- Declare local variables

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts
    DECLARE @error                  AS int
    DECLARE @DocumentAuditWeight    AS int
   

    SET @ProcName = 'uspEstimateAuditGetDetailXML'
    
    -- Validate Parameters
    IF  ((SELECT DocumentID 
         FROM dbo.utb_document d INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
         WHERE DocumentID = @DocumentID
           AND EstimateTypeFlag = 1) IS NULL)
    BEGIN
        -- Invalid Location ID
        RAISERROR('101|%s|@DocumentID|%s', 16, 1, @ProcName, @DocumentID)
        RETURN
    END
    
    


    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )
    

    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'EstimateAuditResults',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_estimate_audit_results' AND Column_Name IN 
            ('EstimateAuditResultsID',
             'EstimateDocumentID',
             'EstimateDetailNumber',
             'AuditRuleName',
             'AuditDescription',
             'AppliedWeight'))
             
             
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|tmpMetadata', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @DocumentAuditWeight = AuditWeight
    FROM dbo.utb_document
    WHERE DocumentID = @DocumentID
    
    
/*    SET EstimateAuditResultsID = IsNull((SELECT TOP 1 EstimateAuditID 
                                   FROM dbo.utb_estimate_audit_results 
                                   WHERE EstimateDocumentID = @DocumentID), 0) 
*/   
    -- Select Root Level
    
    SELECT 	1                             AS Tag,
            NULL                          AS Parent,
            
             --Root
            @DocumentID                   AS [Root!1!DocumentID],
            @DocumentAuditWeight          AS [Root!1!AuditWeight],
                  
            -- Header
            NULL                          AS [Header!2!AssignmentDate],
            NULL                          AS [Header!2!ClaimNumber],
            NULL                          AS [Header!2!ClaimRepName],
            NULL                          AS [Header!2!EstimateReceivedDate],
            NULL                          AS [Header!2!EstimateType],
            NULL                          AS [Header!2!InsuranceCompanyName],
            NULL                          AS [Header!2!InsuredName],
            NULL                          AS [Header!2!LynxID],
            NULL                          AS [Header!2!VehicleNum],
            NULL                          AS [Header!2!ShopContact],
            NULL                          AS [Header!2!ShopID],
            NULL                          AS [Header!2!ShopName],
            NULL                          AS [Header!2!ShopPhone],
            NULL                          AS [Header!2!SupplementSequenceNumber],
            NULL                          AS [Header!2!VehicleYMM],
                  
            -- Discrepancies
            NULL                          AS [Discrepancy!3!EstimateAuditResultsID],
            NULL                          AS [Discrepancy!3!EstimateDocumentID],
            NULL                          AS [Discrepancy!3!EstimateDetailNumber],
            NULL                          AS [Discrepancy!3!AuditRuleName],
            NULL                          AS [Discrepancy!3!AuditDescription],
            NULL                          AS [Discrepancy!3!AppliedWeight],
            NULL                          AS [Discrepancy!3!SysLastUpdatedDate],
            
            -- Metadata Header
            NULL                          AS [Metadata!4!Entity],
            
            -- Columns
            NULL                          AS [Column!5!Name],
            NULL                          AS [Column!5!DataType],
            NULL                          AS [Column!5!MaxLength],
            NULL                          AS [Column!5!Precision],
            NULL                          AS [Column!5!Scale],
            NULL                          AS [Column!5!Nullable]


    UNION ALL
    
    -- Header Level
    
    SELECT 2,
           1,
           
           -- Root
           Null, NULL,
           
           -- Header
           CONVERT(varchar(2), MONTH(a.AssignmentDate)) + '/' + CONVERT(varchar(2), Day(a.AssignmentDate)) + '/' + CONVERT(varchar(4), YEAR(a.AssignmentDate)),
           IsNull(c.ClientClaimNumber, ''), --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  Get the column from Claim table M.A. 20061109
           --Project:210474 APD Remarked-off the column below when we did the code merge M.A.20061116          
            /*(SELECT u.NameFirst + ' ' + u.NameLast
            FROM dbo.utb_user u
            WHERE u.UserID = ca.OwnerUserID*/
              /*AND ca.ClaimAspectTypeID = (SELECT ClaimAspectTypeID 
                                            FROM dbo.utb_claim_aspect_type 
                                            WHERE Name = 'Claim' AND EnabledFlag = 1)*/
           --),
           '', --Project:210474 APD Added the column when we did the code merge M.A.20061116
           CONVERT(varchar(2), MONTH(d.CreatedDate)) + '/' + CONVERT(varchar(2), Day(d.CreatedDate)) + '/' + CONVERT(varchar(4), YEAR(d.CreatedDate)),
           IsNull(dt.Name, ''),
           i.Name,
           (SELECT i.NameFirst + ' ' + i.NameLast
            FROM dbo.utb_claim_aspect_involved cai 
                 INNER JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                 INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                 INNER JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
            WHERE cai.ClaimAspectID = ca.ClaimAspectID
              AND irt.Name = 'Insured'),
           ca.LynxID,
           ca.ClaimAspectNumber,
           IsNull((SELECT p.Name 
                   FROM dbo.utb_shop_location_personnel slp 
                        INNER JOIN dbo.utb_personnel p ON slp.PersonnelID = p.PersonnelID
                        INNER JOIN dbo.utb_personnel_type pt ON p.PersonnelTypeID = pt.PersonnelTypeID
                   WHERE pt.Name = 'Shop Manager'
                     AND slp.ShopLocationID = sl.ShopLocationID), ''),
           IsNull(sl.ShopLocationID, ''),
           IsNull(sl.Name, ''),
           sl.PhoneAreaCode + '-' + sl.PhoneExchangeNumber + '-' + sl.PhoneUnitNumber,
           IsNull(d.SupplementSeqNumber, ''),
           convert(varchar(4), cv.VehicleYear) + ' ' + cv.Make + ' ' + cv.Model,
           
           -- Discrepancies
           NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           
           -- Metadata Header
           NULL,
            
           -- Columns
           NULL, NULL, NULL, NULL, NULL, NULL
    
    FROM  dbo.utb_document d INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                             INNER JOIN dbo.utb_assignment a ON d.AssignmentID = a.AssignmentID
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel below, to provide a join betwwen 
					utb_Claim_Aspect and utb_Assignment
					M.A. 20061109
				*********************************************************************************/
			     INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                             INNER JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
                             INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                              --LEFT JOIN dbo.utb_user u ON ca.OwnerUserID = u.UserID
                              LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID
                              LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
                              LEFT JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
                              LEFT JOIN dbo.utb_claim_vehicle cv ON casc.ClaimAspectID = cv.ClaimAspectID --Project: 210474 APD - Enhancements to support multiple concurrent service channels. Changed the join to utb_Claim_Aspect_Service_Channel from utb_Assignment.  M.A. 20061109
    WHERE d.DocumentID = @DocumentID
      --AND ca.ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = 'Claim' AND EnabledFlag = 1)
                    
                    
    UNION ALL
    

    -- Discrepancies Level
    
    SELECT 3,
           1,
           
           -- Root
           Null, NULL,
           
           -- Header
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
           NULL, NULL, NULL, NULL, NULL,

           -- Discrepancies
           IsNull(r.EstimateAuditResultsID, ''),
           IsNull(r.EstimateDocumentID, ''),
           IsNull(r.EstimateDetailNumber, ''),
           IsNull(r.AuditRuleName, ''),
           IsNull(r.AuditDescription, ''),
           IsNull(r.AppliedWeight, ''),
           dbo.ufnUtilityGetDateString(r.SysLastUpdatedDate),
    
           -- Metadata Header
           NULL,
            
           -- Columns
           NULL, NULL, NULL, NULL, NULL, NULL
    
    FROM  dbo.utb_estimate_audit_results r
    WHERE EstimateDocumentID = @DocumentID
                    
                    
    UNION ALL
    
    -- Select Metadata Header Level

    SELECT DISTINCT 
            4,
            1,
            -- Root
            NULL, NULL,
            
            -- Header
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Discrepancies
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  5,
            4,
            -- Root
            NULL, NULL,
            
            -- Header
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Discrepancies
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Metadata Header
            GroupName,
            
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable

    FROM  @tmpMetadata


    
            
    
    ORDER BY Tag
    --FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateAuditGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateAuditGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
