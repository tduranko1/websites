-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDMainGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPMDMainGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspPMDMainGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns a list of active APD District Managers and above
*
* PARAMETERS:
* None  
*
* RESULT SET:
* UserID                            The Manager's user identity
* CsrNo                             The Manager's CSR Number
* NameFirst                         The Manager's first name
* NameLast                          The Manager's last name
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspPMDMainGetListXML
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON


    -- Declare internal variables

    DECLARE @error      AS INT
    DECLARE @rowcount   AS INT
    DECLARE @now        AS datetime
    DECLARE @ProcName   AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspPMDMainGetListXML'
    SET @now = CURRENT_TIMESTAMP

    
    SELECT 
        1 as tag,
        Null as Parent,
        -- Root
        Null as [Root!1!Root],
        -- District Manager
        Null as [DistrictManager!2!UserID],
        Null as [DistrictManager!2!CsrNo],
        Null as [DistrictManager!2!NameFirst],
        Null as [DistrictManager!2!NameLast],
        -- Program Manager
        Null as [ProgramManager!3!UserID],
        Null as [ProgramManager!3!CsrNo],
        Null as [ProgramManager!3!NameFirst],
        Null as [ProgramManager!3!NameLast],
        Null as [ProgramManager!3!SupervisorUserID],
        -- State List
        Null as [State!4!StateCode],
        Null as [State!4!StateName],
        Null as [State!4!DisplayOrder]
        
    UNION ALL
    
    SELECT DISTINCT
        2 as tag,
        1 as parent,
        
        -- Root
        Null,
        
        -- District Manager
        ISNULL(CONVERT(varchar(10), U.UserID), ''),
        ISNULL(U.CsrNo, ''),
        ISNULL(U.NameFirst, ''),
        ISNULL(U.NameLast, ''),
        -- Program Manager
        Null, Null, Null, Null, Null,
        -- State Codes
        Null, Null, Null
        
        FROM dbo.ufnUtilityPMDSupervisorsGetList() U
        
    UNION ALL
    
    SELECT DISTINCT
        3 AS tag,
        1 AS parent,
        
        -- Root
        Null,
        
        -- District Manager
        Null, Null, Null, Null, 
        
        -- Program Manager
        ISNULL(CONVERT(varchar(10), U.UserID), ''),
        ISNULL(U.CsrNo, ''),
        ISNULL(U.NameFirst, ''),
        ISNULL(U.NameLast, ''),
        ISNULL(CONVERT(varchar(10), U.SupervisorUserID), ''),
        -- State Codes
        Null, Null, Null
    
        
    FROM dbo.ufnUtilityProgramManagersGetList() U
          
    UNION ALL
    
    SELECT DISTINCT
        4 AS tag,
        1 AS parent,
        
        -- Root
        Null,
        
        -- District Manager
        Null, Null, Null, Null, 
        
        -- Program Manager
        Null, Null, Null, Null, Null,
        -- State Codes
        StateCode, StateValue, DisplayOrder
    
        
    FROM dbo.utb_state_code
    WHERE EnabledFlag = 1
      
    ORDER BY tag, 
             [DistrictManager!2!NameLast], 
             [DistrictManager!2!NameFirst], 
             [ProgramManager!3!NameLast], 
             [ProgramManager!3!NameFirst]
             
    -- FOR XML EXPLICIT      -- (Commented for Client-side processing)
    

SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPMDMainGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPMDMainGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
