-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDeskAuditSummaryGenerateXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDeskAuditSummaryGenerateXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDeskAuditSummaryGenerateXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns data necessary to populate he desk audit summary cover sheets
*
* PARAMETERS:  
* (I) @LynxID               LynxID the summary cover is being generated for
*
* RESULT SET:
*       XML stram containing claim and Estimate Information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDeskAuditSummaryGenerateXML
    @LynxID           AS udt_std_id_big,
    @DocumentTypeCD   AS udt_std_cd=NULL
AS
BEGIN
    -- Set database options

    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables

    DECLARE @tmpEstimate TABLE
    (
        ClaimAspectID           bigint,
        SupplementSeqNumber     tinyint,
        OriginalGrossAmt        money,
        AuditedGrossAmt         money,
        AuditedBettermentAmt    money,
        AuditedDeductibleAmt    money,
        AuditedNetAmt           money,
        AgreedPriceMetCD        varchar(4)
    )
    

    DECLARE @AgreedPriceMetCD                   udt_std_cd
    DECLARE @AgreedPriceMetCDOriginal           udt_std_cd
    DECLARE @BettermentAmt                      udt_std_money
    DECLARE @ClaimAspectID                      udt_std_id_big
    DECLARE @ClaimAspectTypeIDVehicle           udt_std_id
    DECLARE @DeductibleAmt                      udt_std_money
    DECLARE @DocumentID                         udt_std_id_big
    DECLARE @EstimateSummaryTypeIDBetterment    udt_std_id
    DECLARE @EstimateSummaryTypeIDDeductible    udt_std_id
    DECLARE @EstimateSummaryTypeIDNetTotal      udt_std_id
    DECLARE @EstimateSummaryTypeIDRepairTotal   udt_std_id
    DECLARE @EstimateTypeCD                     udt_std_cd
    DECLARE @EstimateTypeCDPrevious             udt_std_cd
    DECLARE @NetTotalAmt                        udt_std_money
    DECLARE @RepairTotalAmt                     udt_std_money
    DECLARE @SupplementSeqNumber                udt_std_int_tiny
            
    DECLARE @Debug              AS udt_std_flag
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspDeskAuditSummaryGenerateXML'
    SET @Debug = 0
    

    IF @Debug = 1
    BEGIN
        PRINT 'Parameters...'
        PRINT '    @LynxID = ' + Convert(varchar(20), @LynxID)
    END
    
    
    -- Check to make sure a valid Lynx id was passed in
    
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Validate APD data state
    
    SELECT  @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'TT' 
        AND Name = 'RepairTotal'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
        -- Estimate Summary Type not found
        
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
        
        
    SELECT  @EstimateSummaryTypeIDBetterment = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'AJ' 
        AND Name = 'Betterment'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    IF @EstimateSummaryTypeIDBetterment IS NULL
    BEGIN
        -- Estimate Summary Type not found
        
        RAISERROR('102|%s|"Betterment"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
        
        
    SELECT  @EstimateSummaryTypeIDDeductible = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'AJ' 
        AND Name = 'Deductible'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    IF @EstimateSummaryTypeIDDeductible IS NULL
    BEGIN
        -- Estimate Summary Type not found
        
        RAISERROR('102|%s|"Deductible"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
        
        
    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID
      FROM  dbo.utb_estimate_summary_type
      WHERE CategoryCD = 'TT' 
        AND Name = 'NetTotal'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
        -- Estimate Summary Type not found
        
        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
        
        
    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        -- Claim Aspect Type not found
        
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Get estimate information

    IF @Debug = 1
    BEGIN
        PRINT 'Estimate Records attached to the claim...'
        SELECT  casc.ClaimAspectID,
                d.DocumentID,
                IsNull(d.SupplementSeqNumber, 0) AS SupplementSeqNumber,
                IsNull(d.EstimateTypeCD, 'O') AS EstimateTypeCD,
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal) AS RepairTotal,
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment) AS Betterment,
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible) AS Deductible,
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal) AS NetTotal,
                d.AgreedPriceMetCD
          FROM  dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
          LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
          LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
          WHERE ca.LynxID = @LynxID
            AND d.EnabledFlag = 1         -- Only want enabled
            AND d.DuplicateFLag = 0       -- non-duplicate
            AND dt.EstimateTypeFlag = 1   -- estimates
            AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          ORDER BY d.SupplementSeqNumber, d.EstimateTypeCD DESC
    END
    
    
    IF  @DocumentTypeCD NOT IN ('DASS', 'EAS') -- skip estimate checks for these types -- DASS = Desk Audit Status Supplement
                                                                                       -- EAS  = Estimate Audit Summary
    BEGIN
      DECLARE csrEstimate CURSOR FOR
        SELECT  casc.ClaimAspectID,
                d.DocumentID,
                IsNull(d.SupplementSeqNumber, 0),
                IsNull(d.EstimateTypeCD, 'O'),
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal),
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment),
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible),
                (SELECT ses.OriginalExtendedAmt FROM dbo.utb_estimate_summary ses WHERE ses.DocumentID = d.DocumentID AND ses.EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal),
                d.AgreedPriceMetCD
          FROM  dbo.utb_claim_aspect_service_channel_document cascd --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          Left Outer Join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
          LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
          LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
          WHERE ca.LynxID = @LynxID
            AND d.EnabledFlag = 1         -- Only want enabled
            AND d.DuplicateFLag = 0       -- non-duplicate
            AND dt.EstimateTypeFlag = 1   -- estimates
            AND casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
          ORDER BY d.SupplementSeqNumber, d.EstimateTypeCD DESC

    
      OPEN csrEstimate
    
      FETCH NEXT FROM csrEstimate
        INTO  @ClaimAspectID,
              @DocumentID,
              @SupplementSeqNumber,
              @EstimateTypeCD,
              @RepairTotalAmt,
              @BettermentAmt,
              @DeductibleAmt,
              @NetTotalAmt,
              @AgreedPriceMetCD
            
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
    
          RAISERROR('99|%s', 16, 1, @ProcName)
          RETURN
      END    



      WHILE @@FETCH_STATUS = 0
      BEGIN
          IF @EstimateTypeCD = 'O'    -- Original estimate record
          BEGIN
              -- Start building the audit summary information record
            
              -- Make sure no previous record has been started
            
              IF NOT EXISTS(SELECT * FROM @tmpEstimate WHERE ClaimAspectID = @ClaimAspectID AND SupplementSeqNumber = @SupplementSeqNumber)
              BEGIN
                  IF @EstimateTypeCDPrevious = 'O'
                  BEGIN 
                      -- We have missing audited estimate data 
                         
                      RAISERROR('1|Missing audited estimate data for at least one sequence.  Please correct by entering summary data for all estimates and try again.', 16, 1)
                      RETURN
                  END                
                

                  INSERT INTO @tmpEstimate (ClaimAspectID, SupplementSeqNumber, OriginalGrossAmt, AgreedPriceMetCD)
                    SELECT  @ClaimAspectID,
                            @SupplementSeqNumber,
                            @RepairTotalAmt,
                            @AgreedPriceMetCD
                          
                  IF @@ERROR <> 0
                  BEGIN
                      -- Insertion failure
    
                      RAISERROR('105|%s|@tmpEstimate', 16, 1, @ProcName)
                      RETURN
                  END

                
                  SET @AgreedPriceMetCDOriginal = @AgreedPriceMetCD
              END
              ELSE
              BEGIN
                  -- Invalid data state.  An original record for the sequence already exists.
                     
                  RAISERROR('1|There is more than 1 original estimate for at least one sequence.  Please correct by flagging all duplicates and try again.', 16, 1)
                  RETURN
              END                
          END
          ELSE
          BEGIN
              -- Audited Estimate Record
            
              -- Verify the original record information exists
            
              -- Make sure an original exists...
            
              IF EXISTS(SELECT * FROM @tmpEstimate WHERE ClaimAspectID = @ClaimAspectID AND SupplementSeqNumber = @SupplementSeqNumber AND OriginalGrossAmt IS NOT NULL)
              BEGIN
                  -- ...and that an audited estimate has not been already been processed
                
                  IF EXISTS(SELECT * FROM @tmpEstimate WHERE ClaimAspectID = @ClaimAspectID AND SupplementSeqNumber = @SupplementSeqNumber AND AuditedGrossAmt IS NOT NULL)
                  BEGIN
                      -- Invalid data state.  An audited record for the sequence already exists.
                         
                      RAISERROR('1|There is more than 1 audited estimate for at least one sequence.  Please correct by flagging all duplicates and try again.', 16, 1)
                      RETURN
                  END                
                  ELSE
                  BEGIN
                      -- Update the audit summary information record
                    
                      UPDATE  @tmpEstimate
                        SET   AuditedGrossAmt = CASE 
                                                  WHEN @RepairTotalAmt > tmp.OriginalGrossAmt THEN tmp.OriginalGrossAmt
                                                  ELSE @RepairTotalAmt
                                                END,      -- If audited is more than the original, use the original
                              AuditedBettermentAmt = IsNull(@BettermentAmt, 0),
                              AuditedDeductibleAmt = IsNull(@DeductibleAmt, 0),
                              AuditedNetAmt = CASE 
                                                  WHEN @RepairTotalAmt > tmp.OriginalGrossAmt THEN tmp.OriginalGrossAmt - @BettermentAmt - @DeductibleAmt
                                                  ELSE @NetTotalAmt
                                                END,
                              AgreedPriceMetCD = CASE
                                                 WHEN @AgreedPriceMetCD IS NULL THEN @AgreedPriceMetCDOriginal
                                                 ELSE @AgreedPriceMetCD
                                               END
                        FROM  @tmpEstimate tmp
                        WHERE tmp.ClaimAspectID = @ClaimAspectID
                          AND tmp.SupplementSeqNumber = @SupplementSeqNumber                          
        
                      IF @@ERROR <> 0
                      BEGIN
                          -- Update failure

                          RAISERROR('104|%s|@tmpEstimate', 16, 1, @ProcName)
                          ROLLBACK TRANSACTION 
                          RETURN
                      END
        
        
                      -- Make sure we have an AgreedPriceMetCD
        
                      IF (SELECT AgreedPriceMetCD FROM @tmpEstimate WHERE ClaimAspectID = @ClaimAspectID AND SupplementSeqNumber = @SupplementSeqNumber) IS NULL
                      BEGIN
                          -- Agreed Price Met was never indicated
                                 
                          RAISERROR('1|Indication of whether an agreed price was reached is missing on at least one sequence.  Please correct by entering this data for each audited estimate and try again.', 16, 1)
                          RETURN
                      END                
            
        
                      SET @AgreedPriceMetCDOriginal = NULL
                  END
              END
              ELSE
              BEGIN
                  -- Invalid data state.  An audited record has been found with no matching original.
                     
                  RAISERROR('1|Missing original estimate data for at least one sequence.  Please correct by entering summary data for all estimates and try again.', 16, 1)
                  RETURN
              END                
          END                
        
        
          SET @EstimateTypeCDPrevious = @EstimateTypeCD       
        
        
          -- Reinitialize the cursor variables for the next pass
        
          SET @ClaimAspectID = NULL
          SET @DocumentID = NULL
          SET @SupplementSeqNumber = NULL
          SET @EstimateTypeCD = NULL
          SET @RepairTotalAmt = NULL
          SET @BettermentAmt = NULL
          SET @DeductibleAmt = NULL
          SET @NetTotalAmt = NULL
          SET @AgreedPriceMetCD = NULL
                
        
          -- Fetch next record
        
          FETCH NEXT FROM csrEstimate
            INTO  @ClaimAspectID,
                  @DocumentID,
                  @SupplementSeqNumber,
                  @EstimateTypeCD,
                  @RepairTotalAmt,
                  @BettermentAmt,
                  @DeductibleAmt,
                  @NetTotalAmt,
                  @AgreedPriceMetCD
            
          IF @@ERROR <> 0
          BEGIN
             -- SQL Server Error
    
              RAISERROR('99|%s', 16, 1, @ProcName)
              RETURN
          END    
      END                
         
         
      CLOSE csrEstimate
      DEALLOCATE csrEstimate
    

      -- Final checks.  Make sure we didn't have an Original estimate at the end with no matching audited
    
      IF EXISTS(SELECT * FROM @tmpEstimate WHERE AuditedGrossAmt IS NULL)
      BEGIN
          -- Invalid data state.  An audited data is missing.
             
          RAISERROR('1|Missing audited estimate data for at least one sequence.  Please correct by entering summary data for all estimates and try again.', 16, 1)
          RETURN
      END      
    END -- end 'if' for checking document type to determine whether to run checks against estimates.
              

    IF @Debug = 1
    BEGIN
        PRINT 'Resulting Estimate audit summary records...'
        SELECT * FROM @tmpEstimate
    END

                          
    -- Begin XML Select

    SELECT
        1 as Tag,
        Null as Parent,
        @LynxID as [Root!1!LynxID],
        -- Claim Information
        NULL AS [Claim!2!LynxID],
        NULL AS [Claim!2!InsuranceCompanyID],
        NULL AS [Claim!2!InsuranceCompanyName],
        NULL AS [Claim!2!OwnerUserFirstName],
        NULL AS [Claim!2!OwnerUserLastName],
        NULL AS [Claim!2!OwnerUserEmail],
        NULL AS [Claim!2!OwnerUserAreaCode],
        NULL AS [Claim!2!OwnerUserExchangeNumber],
        NULL AS [Claim!2!OwnerUserExtensionNumber],
        NULL AS [Claim!2!OwnerUserUnitNumber],
        NULL AS [Claim!2!CarrierNameFirst],
        NULL AS [Claim!2!CarrierNameLast],
        NULL AS [Claim!2!CarrierOfficeName],        
        NULL AS [Claim!2!ClientClaimNumber],
        -- Vehicle Information
        NULL AS [Vehicle!3!ClaimAspectID],
        NULL AS [Vehicle!3!VehicleNumber],
        NULL AS [Vehicle!3!ExposureCD],
        NULL AS [Vehicle!3!CoverageProfileCD],
        NULL AS [Vehicle!3!VehicleYear],
        NULL AS [Vehicle!3!Make],
        NULL AS [Vehicle!3!Model],
        -- Estimate Information
        NULL AS [Estimate!4!SupplementSeqNumber],
        NULL AS [Estimate!4!OriginalGrossAmt],
        NULL AS [Estimate!4!AuditedGrossAmt],
        NULL AS [Estimate!4!AuditedBettermentAmt],
        NULL AS [Estimate!4!AuditedDeductibleAmt],
        NULL AS [Estimate!4!AuditedNetAmt],
        NULL AS [Estimate!4!AgreedPriceMetCD]       
        

    UNION ALL

    -- Claim Information

    SELECT
        2 as Tag,
        1 as Parent,
        NULL,
        -- Claim Information
        c.LynxID,
        c.InsuranceCompanyID,
        IsNull(i.Name, ''),
        IsNull(ou.NameFirst, ''),
        IsNull(ou.NameLast, ''),
        IsNull(ou.EmailAddress, ''),
        IsNull(ou.PhoneAreaCode, ''),
        IsNull(ou.PhoneExchangeNumber, ''),
        IsNull(ou.PhoneExtensionNumber, ''),
        IsNull(ou.PhoneUnitNumber, ''),
        IsNull(cu.NameFirst, ''),
        IsNull(cu.NameLast, ''),
        IsNull(cuo.Name, ''),
        IsNull(c.ClientClaimNumber, ''),--Project: 210474 APD - Enhancements to support multiple concurrent service channels. The column is part of utb_Claim Table instead of utb_Claim_Coverage.  M.A. 20061108
        -- Vehicle Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        -- Estimate Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL
       
    FROM  dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
    LEFT JOIN dbo.utb_user ou on (ca.OwnerUserID = ou.UserID)
    LEFT JOIN dbo.utb_user cu on (c.CarrierRepUserID = cu.UserID)
    LEFT JOIN dbo.utb_office cuo ON (cu.OfficeID = cuo.OfficeID)
    LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID)    
    WHERE c.LynxID = @LynxID
      AND ca.ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = 'Claim' AND EnabledFlag = 1)
    
    
    UNION ALL
    
    
    -- Select vehicle information

    SELECT
        3 as tag,
        2 as parent,
        NULL,
        -- Claim Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Vehicle Information
        ca.ClaimAspectID,
        ca.ClaimAspectNumber,
        ca.ExposureCD,
        ca.CoverageProfileCD,
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        -- Estimate Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL
    
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
    WHERE ca.LynxID = @LynxID
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      
          
    UNION ALL
    
    
    -- Select Estimate information

    SELECT
        4 as tag,
        3 as parent,
        NULL,
        -- Claim Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL,
        -- Vehicle Information
        ClaimAspectID,
        NULL, NULL, NULL, NULL, NULL, NULL, 
        -- Estimate Information
        SupplementSeqNumber,
        OriginalGrossAmt,
        AuditedGrossAmt,
        AuditedBettermentAmt,
        AuditedDeductibleAmt,
        AuditedNetAmt,
        AgreedPriceMetCD
        
    FROM @tmpEstimate    

    ORDER BY [Vehicle!3!ClaimAspectID], tag
--    FOR XML EXPLICIT  (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDeskAuditSummaryGenerateXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDeskAuditSummaryGenerateXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/