-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspZipCodeSearchWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspZipCodeSearchWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspZipCodeSearchWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Given a zip code, returns city and state information
*
* PARAMETERS:  
* (I) @ZipCode              Zip code (Varchar(8))
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspZipCodeSearchWSXML]
    
    @ZipCode udt_addr_zip_code

AS

BEGIN
    -- Set default values for area code and exchange if blank

    IF LEN(RTRIM(LTRIM(@ZipCode))) = 0 SET @ZipCode = NULL


    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspZipCodeSearchWSXML'


    -- Check to make sure a valid ZipCode was passed in

    IF  (@ZipCode IS NULL)
    BEGIN
        -- Invalid Zip Code
    
        RAISERROR('101|%s|@ZipCode|%s', 16, 1, @ProcName, @ZipCode)
        RETURN
    END


    -- Begin XML Select

    SELECT 
        1 as Tag,
        Null as Parent,
--        Root
        @ZipCode as [Root!1!ZipCode],
--        ZipInfo
        Null as [ZipInfo!2!City],
        Null as [ZipInfo!2!County],
        Null as [ZipInfo!2!State],
        Null as [ZipInfo!2!TimeZone],
        Null as [ZipInfo!2!DaylightSavingsTimeFlag],
        Null as [ZipInfo!2!Latitude],
        Null as [ZipInfo!2!Longitude]

    UNION ALL

    SELECT
        2 as Tag,
        1 as Parent, 
--        Root
        Null,
--        ZipInfo
        RTRIM(LTRIM(ZC.City)),
        RTRIM(LTRIM(ZC.County)),
        ZC.State,
        ZC.TimeZone,
        ZC.DaylightSavingsTimeFlag,
        ZC.Latitude,
        ZC.Longitude

    FROM
        utb_Zip_Code ZC

    WHERE
        ZC.Zip = @ZipCode

    ORDER BY tag
    FOR XML EXPLICIT      -- (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspZipCodeSearchWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspZipCodeSearchWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspZipCodeSearchWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/