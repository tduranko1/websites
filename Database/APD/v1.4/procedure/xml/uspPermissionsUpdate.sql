IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataGet' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDataGet TO 
        wsAPDUser
END

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataUpd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDataUpd TO 
        wsAPDUser
END