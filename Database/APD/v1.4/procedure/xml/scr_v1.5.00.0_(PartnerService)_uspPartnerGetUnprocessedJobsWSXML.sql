-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerGetUnprocessedJobsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPartnerGetUnprocessedJobsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPartnerGetUnprocessedJobsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns a list of Unprocessed jobs waiting in the queues as XML
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspPartnerGetUnprocessedJobsWSXML]
	@vIODirection VARCHAR(1)
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspPartnerGetUnprocessedJobsWSXML'
    
    SET @ApplicationCD = 'APDPartner'

    -- Validation
	IF NOT @vIODirection IN ('I','O')
	BEGIN
		RAISERROR('%s: (Get unprocessed jobs) Error with the IODirection, must be I or O.', 16, 1, @ProcName)
	END

    -- Begin XML Select
    SELECT  
		1 AS Tag
        , NULL AS Parent
        , NULL AS [Root!1!Job]
        , NULL AS [Job!2!JobID]
        , NULL AS [Job!2!IODirection]
        , NULL AS [Job!2!JobXSLFile]
        , NULL AS [Job!2!ResponseXSLFile]
        , NULL AS [Job!2!ResponseRequired]
        , NULL AS [Job!2!JobCreatedDate]
        , NULL AS [Job!2!JobPriority]
			
	UNION ALL

    SELECT  
		2 AS Tag
        , 1 AS Parent
        , NULL AS [Root!1!Job]
        , JobID AS [Root!1!JobID]
        , IODirection AS [Job!2!IODirection]
        , JobXSLFile AS [Job!2!JobXSLFile]
        , ResponseXSLFile AS [Job!2!ResponseXSLFile]
        , ResponseRequired AS [Root!1!ResponseRequired]
        , JobCreatedDate AS [Job!2!JobCreatedDate]
        , JobPriority AS [Job!2!JobPriority]
    FROM  
		utb_partnersvc_jobs
    WHERE 
		EnabledFlag = 1
		AND IODirection = 'I'
		AND UPPER(JobStatus) = 'UNPROCESSED'
	ORDER BY
		[Job!2!JobCreatedDate]
		, [Job!2!JobPriority]	
	
	FOR XML EXPLICIT	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerGetUnprocessedJobsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPartnerGetUnprocessedJobsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/