-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTNoteGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTNoteGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves data necessary to populate the notes screen
*
* PARAMETERS:  
* (I) @ShopLocationID               The Shop Location ID
*
* RESULT SET:
* An XML Data stream containing notes for the claim
*
*
* VSS
* $Workfile: uspSMTNoteGetDetailXML.sql $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspSMTNoteGetDetailXML.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/29/01 2:49p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTNoteGetDetailXML
    @ShopLocationID     udt_std_int_big,
    @DocumentID         udt_std_int_big=NULL
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTNoteGetDetailXML'


    -- Check to make sure a valid Shop Location ID was passed in

    IF  (@ShopLocationID IS NULL) OR
        ((NOT @ShopLocationID = 0) AND NOT EXISTS(SELECT @ShopLocationID FROM dbo.utb_shop_location 
                                                                         WHERE @ShopLocationID = @ShopLocationID))
    BEGIN
        -- Invalid Shop Location ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END

    IF (@DocumentID = 0) SET @DocumentID = NULL

    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all entities and store in the temporary table    

    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Note',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN 
            ('NoteTypeID',
             'StatusID',
             'Note'))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE 
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,  
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        LinkedReferenceID   varchar(10) NULL        -- This field is special to support linking Status IDs to Pertains To
    )
    
    
    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table    

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, LinkedReferenceID)
    
    SELECT  'NoteType' AS ListName,
            DisplayOrder AS DisplayOrder,
            NoteTypeID, 
            Name, 
            NULL
    FROM    utb_note_type
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    /*
    UNION ALL

    SELECT  'PertainsTo',
            NULL,
            --EntityCode, 
            ClaimAspectID,
            Name,
            EntityCode
    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0)  -- (0, 0) means all aspects, open or closed
    WHERE   EntityCode <> 'cov'

    UNION ALL

    SELECT  'Status',
            s.DisplayOrder,
            s.StatusID, 
            s.Name, 
            cat.Code
    FROM    utb_status s
    LEFT JOIN utb_claim_aspect_type cat ON (s.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    WHERE   s.EnabledFlag = 1
    */
    
    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary table to hold references to the claim's notes

    DECLARE @tmpNotes TABLE 
    (
        ShopLocationID          bigint          NOT NULL,
        DocumentID              int             NOT NULL
        --PertainsTo              varchar(5)      NOT NULL,
        --ClaimAspectTypeName     varchar(50)     NOT NULL,
        --ClaimAspectNumber       tinyint         NOT NULL,
        --ClaimAspectID           bigint          NOT NULL
    )
    

    IF NOT EXISTS(SELECT Name FROM dbo.utb_document_type WHERE Name = 'Note')
    BEGIN
       -- Note Document Type Not Found
    
        RAISERROR('102|%s|"Note"|utb_document_type', 16, 1, @ProcName)
        RETURN
    END


    -- Compile note list together from all the various entities

    
    IF (@DocumentID IS NULL)
    BEGIN
      INSERT INTO @tmpNotes

      SELECT  @ShopLocationID,
              sld.DocumentID    
              
              
        FROM  dbo.utb_shop_location_document sld
        LEFT JOIN dbo.utb_shop_location sl ON (sld.ShopLocationID = sl.ShopLocationID)
        INNER JOIN dbo.utb_document d ON (sld.DocumentID = d.DocumentID)
        INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        --INNER JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE sl.ShopLocationID = @ShopLocationID
          AND dt.Name = 'Note'
          AND d.EnabledFlag = 1
    END
    ELSE
    BEGIN
      INSERT INTO @tmpNotes

      SELECT  @ShopLocationID,
              sld.DocumentID
              --dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 0) AS PertainsTo
              
        FROM  dbo.utb_shop_location_document sld
        LEFT JOIN dbo.utb_shop_location sl ON (sld.ShopLocationID = sl.ShopLocationID)
        INNER JOIN dbo.utb_document d ON (sld.DocumentID = d.DocumentID)
        INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        --INNER JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE sl.ShopLocationID = @ShopLocationID
          AND dt.Name = 'Note'
          AND d.EnabledFlag = 1
          AND d.DocumentID = @DocumentID
    END

    IF @@ERROR <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|@tmpNotes', 16, 1, @ProcName)
        RETURN
    END

    --select * from @tmpNotes
    --return


    -- Begin XML Select

    -- Select Root Level

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @ShopLocationID AS [Root!1!ShopLocationID],
            -- Note
            NULL AS [Note!2!DocumentID],
            --NULL AS [Note!2!ClaimAspectID],
            --NULL AS [Note!2!ClaimAspectNumber],
            --NULL AS [Note!2!ClaimAspectCode],
            NULL AS [Note!2!CreatedUserID],
            NULL AS [Note!2!CreatedUserNameFirst],
            NULL AS [Note!2!CreatedUserNameLast],
            NULL AS [Note!2!CreatedUserRoleID],
            NULL AS [Note!2!CreatedUserRoleName],
            NULL AS [Note!2!CreatedUserSupervisorFlag],           
            NULL AS [Note!2!DocumentSourceID],
            NULL AS [Note!2!DocumentTypeID],
            NULL AS [Note!2!NoteTypeID],
            NULL AS [Note!2!ParentDocumentID],
            NULL AS [Note!2!StatusID],
            --NULL AS [Note!2!PertainsTo],
            NULL AS [Note!2!CurrentDate],  
            NULL AS [Note!2!CreatedDate],
            NULL AS [Note!2!Note],
            NULL AS [Note!2!SysLastUpdatedDate],
            -- Metadata Header
            NULL AS [Metadata!3!Entity],
            -- Columns
            NULL AS [Column!4!Name],
            NULL AS [Column!4!DataType],
            NULL AS [Column!4!MaxLength],
            NULL AS [Column!4!Precision],
            NULL AS [Column!4!Scale],
            NULL AS [Column!4!Nullable],
            -- Reference Data
            NULL AS [Reference!5!List],
            NULL AS [Reference!5!ReferenceID],
            NULL AS [Reference!5!Name]
            --NULL AS [Reference!5!ClaimAspectCode]  -- This field is special to support linking Status IDs to Aspect Types


    UNION ALL


    -- Select Note Level

    SELECT 	2,
            1,
            NULL,
            -- Note
            IsNull(n.DocumentID, ''), 
            --IsNull(n.ClaimAspectID, ''),
            --IsNull(n.ClaimAspectNumber, ''),
            --IsNull(n.ClaimAspectTypeName, ''),
            d.CreatedUserID, 
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            d.CreatedUserRoleID,
            IsNull(r.Name, ''),
            d.CreatedUserSupervisorFlag,
            d.DocumentSourceID,
            d.DocumentTypeID,
            d.NoteTypeID,
            IsNull(d.ParentDocumentID, ''),
            d.StatusID,
            --IsNull(n.PertainsTo, ''),
            GetDate(),  
            IsNull(d.CreatedDate, ''),
            IsNull(d.Note, ''),
            dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL

    FROM    (SELECT @ShopLocationID AS ShopLocationID) AS parms
    LEFT JOIN @tmpNotes n ON (parms.ShopLocationID = n.ShopLocationID)
    LEFT JOIN dbo.utb_document d ON (n.DocumentID = d.DocumentID)
    LEFT JOIN dbo.utb_user u ON (d.CreatedUserID = u.UserID)
    LEFT JOIN dbo.utb_role r ON (d.CreatedUserRoleID = r.RoleID)


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 3,
            1,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, --NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL --, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT 	4,
            3,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, --NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL --, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT 	5,
            1,
            NULL,
            -- Note
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, --NULL, NULL, NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name
            --LinkedReferenceID

    FROM    @tmpReference


    ORDER BY [Metadata!3!Entity], Tag
--    FOR XML EXPLICIT                  -- (Comment out for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTNoteGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTNoteGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: uspSMTNoteGetDetailXML.sql $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspSMTNoteGetDetailXML.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/29/01 2:49p $
* $History: uspSMTNoteGetDetailXML.sql $
 * 
 * *****************  Version 2  *****************
 * User: Jim          Date: 10/29/01   Time: 2:49p
 * Updated in $/Database/APD/v1.0.0/procedure/xml
 * Bug fix
 * 
 * *****************  Version 1  *****************
 * User: Jim          Date: 10/04/01   Time: 1:56p
 * Created in $/Database/APD/v1.0.0/procedure/xml
 * Initial development complete
*
************************************************************************************************************************/