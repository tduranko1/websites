-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADGetAuthorizationListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspECADGetAuthorizationListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspECADGetAuthorizationListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns a list of indemnity items to populate the ClaimPoint authorization screen
*
* PARAMETERS:   
* (I) @InsuranceCompanyID   The insurance company to obtain the list for
*
* RESULT SET:
* LynxID, ClaimAspectID, ClientClaimNumber, CoverageTypeID, LossDate, LossState, ClaimantNameFirst, ClaimantNameLast, FeeName, FeeCode, IndemnityAmount
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspECADGetAuthorizationListXML
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 
    DECLARE @AwaitingAuthStatus     udt_std_cd
    DECLARE @FeeItemType            udt_std_cd
    DECLARE @OwnerRoleType          udt_std_int_tiny         
        
        
    SET @ProcName = 'uspECADGetAuthorizationListXML'

    SELECT @AwaitingAuthStatus = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD') 
                                          WHERE Name = 'Awaiting Client Authorization'
    
    SELECT @FeeItemType = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') 
                                          WHERE Name = 'Fee'
    
    SELECT @OwnerRoleType = InvolvedRoleTypeID FROM dbo.utb_involved_role_type WHERE Name = 'Owner'                                      
                                          
                                          
     -- Create temporary Table to hold Reference information
    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,  
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        CoverageProfileCD   varchar(4)  NULL
    )
    
    
    -- Select reference information for all pertinent reference tables and store in the
    -- temporary table    
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, CoverageProfileCD)
    
    SELECT  'ClientCoverageType' AS ListName,
            DisplayOrder,
            clientCoverageTypeID,
            Name,
            CoverageProfileCD
      FROM  dbo.utb_client_coverage_type
      WHERE InsuranceCompanyID = @InsuranceCompanyID AND EnabledFlag = 1

    UNION ALL
        
    SELECT  'ContractState',
            DisplayOrder,
            StateCode,
            StateValue,
            NULL
    FROM  dbo.utb_state_code
    WHERE EnabledFlag = 1
      AND DisplayOrder IS NOT NULL
        
    ORDER BY ListName, DisplayOrder
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    DECLARE @tmpPayments TABLE (ClaimAspectID           bigint          NOT NULL,
                                Amount                  money           NOT NULL,
                                StatusDate              datetime            NULL,
                                FeeName                 varchar(50)         NULL,
                                FeeCode                 varchar(4)          NULL,
                                RentalFlag              bit                 NULL,
                                LynxID                  bigint              NULL,
                                VehicleNumber           tinyint             NULL,
                                ClientClaimNumber       varchar(30)         NULL, 
                                ClientCoverageTypeID    int                 NULL,
                                DataChangedFlag         bit                 NULL,
                                ExposureCD              varchar(4)          NULL,
                                LossDate                datetime            NULL,
                                LossState               char(2)             NULL,
                                ClaimantNameFirst       varchar(50)         NULL,
                                ClaimantNameLast        varchar(50)         NULL)
                                
                                
                                    
    -- Get Total indemnity amount for each Claim Aspect for the passed InsuranceCompanyID where the status of the invoice
    -- item is 'Awaiting Client Authorization'.                            
    INSERT INTO @tmpPayments (ClaimAspectID, Amount, RentalFlag)
    SELECT i.ClaimAspectID,
           SUM(i.Amount),
           0    -- initialize RentalFlag'
           
    FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                           INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    WHERE c.InsuranceCompanyID = @InsuranceCompanyID
      AND i.ItemTypeCD <> @FeeItemtype
      AND i.StatusCD = @AwaitingAuthStatus
      AND i.EnabledFlag = 1
    GROUP BY i.ClaimAspectID
    
    IF (@@Error <> 0)
    BEGIN      
        -- Insertion failure    
        RAISERROR('105|%s|@tmpPayments', 16, 1, @ProcName)
        RETURN    
    END
    
    
    -- Set the status date for each claim aspect to the earliest status date for the Claim Aspect where the status of the
    -- invoice item is 'Awaiting Client Authorization'.
    UPDATE @tmpPayments
    SET StatusDate = (SELECT TOP 1 StatusDate
                      FROM dbo.utb_invoice
                      WHERE ClaimAspectID = t.ClaimAspectID
                        AND StatusCD = @AwaitingAuthStatus
                        AND ItemTypeCD <> @FeeItemtype
                        AND EnabledFlag = 1
                      ORDER BY StatusDate)
    FROM @tmpPayments t
    
    IF (@@Error <> 0)
    BEGIN      
        -- Update failure    
        RAISERROR('104|%s|@tmpPayments', 16, 1, @ProcName)
        RETURN    
    END
    
    
    
    -- Get the description and client code for the most recent handling fee billed to each claim aspect in the temp table.
    UPDATE @tmpPayments
    SET FeeName = i.Description,
        FeeCode = i.ClientFeeCode,
        DataChangedFlag = (SELECT CASE WHEN i.EntryDate = i.SysLastUpdatedDate THEN 0 ELSE 1 END)        
    FROM @tmpPayments t INNER JOIN dbo.utb_invoice i ON t.ClaimAspectID = i.ClaimAspectID
    WHERE t.ClaimAspectID = (SELECT TOP 1 ClaimAspectID
                             FROM dbo.utb_invoice
                             WHERE ClaimAspectID = t.ClaimAspectID
                               AND ItemTypeCD = @FeeItemType
                               AND FeeCategoryCD = 'H'
                               AND EnabledFlag = 1   
                             ORDER BY InvoiceID DESC)                  
    
    IF (@@Error <> 0)
    BEGIN      
        -- Update failure    
        RAISERROR('104|%s|@tmpPayments', 16, 1, @ProcName)
        RETURN    
    END
        
            
    -- Set RentalFlag if any rental services have been billed to this Claim Aspect 
    UPDATE @tmpPayments
    SET RentalFlag = 1
    FROM @tmpPayments t
    WHERE 0 < (SELECT COUNT(invs.ServiceID) 
               FROM dbo.utb_invoice_service invs INNER JOIN dbo.utb_service s ON invs.ServiceID = s.ServiceID
               WHERE invs.ClaimAspectID = t.ClaimAspectID
                 AND invs.EnabledFlag = 1
                 AND s.Name IN ('First Party Rental', 'Third Party Loss of Use'))
        
    IF (@@Error <> 0)
    BEGIN      
        -- Update failure    
        RAISERROR('104|%s|@tmpPayments', 16, 1, @ProcName)
        RETURN    
    END
    
        
    -- Populate the claim data fields in the temp table.
    UPDATE @tmpPayments
    SET LynxID = ca.LynxID, 
        VehicleNumber = ca.ClaimAspectNumber, 
        ClientClaimNumber = c.ClientClaimNumber, --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  ClientClaimNumber is now part of utb_Claim table. So changed the reference to utb_Claim. M.A.20061109
        ClientCoverageTypeID = ca.ClientCoverageTypeID,
        ExposureCD = ca.ExposureCD,
        LossDate = c.LossDate,
        LossState = c.LossState,
        ClaimantNameFirst = i.NameFirst,
        ClaimantNameLast = i.NameLast        
    FROM @tmpPayments t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
                        INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                        LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID
                        --LEFT JOIN dbo.utb_assignment_type at ON ca.CurrentAssignmentTypeID = at.AssignmentTypeID  --Project:210474 APD The column CurrentAssignmentTypeID was removed again from utb_Claim_Aspect.  So removed the reference to the table because it is not needed M.A. 20070108
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON ca.ClaimAspectID = cai.ClaimAspectID
                        LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                        LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID 
                                                           AND ir.InvolvedRoleTypeID = @OwnerRoleType     
                 
    IF (@@Error <> 0)
    BEGIN      
        -- Update failure    
        RAISERROR('104|%s|@tmpPayments', 16, 1, @ProcName)
        RETURN    
    END
        
    
    -- Begin final select.
    SELECT  1 AS Tag,
            NULL AS Parent,
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            -- Claim Listing
            NULL AS [Claim!2!LynxID], 
            NULL AS [Claim!2!VehicleNumber],
            NULL AS [Claim!2!ClaimAspectID], 
            NULL AS [Claim!2!ClientClaimNumber], 
            NULL AS [Claim!2!ClientCoverageTypeID], 
            NULL AS [Claim!2!DataChanged],
            NULL AS [Claim!2!ExposureCD], 
            NULL AS [Claim!2!LossDate], 
            NULL AS [Claim!2!LossState], 
            NULL AS [Claim!2!ClaimantNameFirst], 
            NULL AS [Claim!2!ClaimantNameLast], 
            NULL AS [Claim!2!FeeName], 
            NULL AS [Claim!2!FeeCode], 
            NULL AS [Claim!2!IndemnityAmount],
            NULL AS [Claim!2!AuthorizationRequestDate],
            NULL AS [Claim!2!RentalFlag],
            -- Reference Data
            NULL AS [Reference!3!List],
            NULL AS [Reference!3!ReferenceID],
            NULL AS [Reference!3!Name],
            NULL AS [Reference!3!CoverageProfileCD]
            
            
    UNION ALL
    
    SELECT  2,
            1,
            NULL,
            -- Claim Listing
            LynxID,
            VehicleNumber,
            ClaimAspectID,
            ISNULL(ClientClaimNumber, ''), 
            ISNULL(ClientCoverageTypeID, ''),
            ISNULL(DataChangedFlag, '0'),
            ISNULL(ExposureCD, ''),
            ISNULL(LossDate, ''),
            ISNULL(LossState, ''),
            ISNULL(ClaimantNameFirst, ''),
            ISNULL(ClaimantNameLast, ''), 
            ISNULL(FeeName, ''),
            ISNULL(FeeCode, ''),
            ISNULL(CONVERT(varchar(30), Amount), ''),
            ISNULL(StatusDate, ''),
            ISNULL(RentalFlag, 0),
            
            -- Reference Data
            NULL, NULL, NULL, NULL
            
    FROM  @tmpPayments          
      
    UNION ALL            
      
    SELECT  3, 
            1, 
            NULL,
            
            -- Claim Listing
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Reference Data
            ISNULL(ListName, ''),
            ISNULL(ReferenceID, ''),
            ISNULL(Name, ''),
            ISNULL(CoverageProfileCD, '')

    FROM    @tmpReference 
            
    ORDER BY tag
    -- FOR XML EXPLICIT      -- (Commented for client-side processing)


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END      
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADGetAuthorizationListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspECADGetAuthorizationListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/