-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspJournalGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspJournalGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspJournalGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves the Journal Details
*
* PARAMETERS:  
*   None
*
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

Create PROCEDURE [dbo].[uspJournalGetDetailWSXML]
    @LynxID     udt_std_id_big
AS
BEGIN
    DECLARE @OldDispatchNumber as varchar(50)
    DECLARE @NewDispatchNumber as varchar(50)
    DECLARE @ClaimAspectID as bigint
    DECLARE @LoopCountMax as int
    
    DECLARE @tmpClaimAspects TABLE (
      ClaimAspectID                 bigint NOT NULL,
      ClaimAspectServiceChannelID   bigint NOT NULL,
      ClaimAspectNumber             int NOT NULL,
      ServiceChannelDesc            varchar(50) NOT NULL,
      DispositionDesc               varchar(50) NULL
    )
    
    INSERT INTO @tmpClaimAspects
    SELECT casc.ClaimAspectID,
           ClaimAspectServiceChannelID,
           ca.ClaimAspectNumber,
           ufn1.Name,
           ufn.Name
    FROM utb_claim_aspect_service_channel casc 
    LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'DispositionTypeCD') ufn ON casc.DispositionTypeCD = ufn.Code
    LEFT JOIN ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') ufn1 ON casc.ServiceChannelCD = ufn1.Code
    WHERE ca.LynxID = @LynxID
    
    --select * from @tmpClaimAspects
    
    DECLARE @tmpDispatches TABLE (
      DispatchNumber                varchar(50) NOT NULL,
      ClaimAspectID                 bigint NOT NULL,
      ClaimAspectServiceChannelID   bigint NOT NULL
    )
    
    INSERT INTO @tmpDispatches
    SELECT DISTINCT DispatchNumber,
           tc.ClaimAspectID,
           tc.ClaimAspectServiceChannelID
    FROM dbo.utb_invoice i
    INNER JOIN @tmpClaimAspects tc ON i.ClaimAspectID = tc.ClaimAspectID OR i.ClaimAspectServiceChannelID = tc.ClaimAspectServiceChannelID
    WHERE i.DispatchNumber IS NOT NULL
    
    --select * from @tmpDispatches
    
    DECLARE @tmpDispatchInfo TABLE (
       ClaimAspectID          bigint         NOT NULL,
       TransactionID          bigint         NOT NULL,
       Amount                 decimal(9, 2)  NOT NULL,
       Description            varchar(250)   NOT NULL,
       DisbursementAmount     decimal(9, 2)  NULL,
       DisbursementDate       datetime       NULL,
       DisbursementMethodCD   varchar(4)     NULL,
       DisbursementMethodDesc varchar(50)    NULL,
       DispatchNumber         varchar(50)    NOT NULL,
       DisptachNumberNew      varchar(50)    NULL,
       TransactionCD          varchar(4)     NOT NULL,
       TransactionDesc        varchar(50)    NOT NULL,
       TransactionDate        datetime       NOT NULL)
       
    INSERT INTO @tmpDispatchInfo 
    SELECT  td.ClaimAspectID,
            TransactionID,
            Amount,
            Description,
            DisbursementAmount,
            DisbursementDate,
            DisbursementMethodCD,
            dc.Name,
            id.DispatchNumber,
            DisptachNumberNew,
            TransactionCD,
            tc.Name,
            TransactionDate
    FROM dbo.utb_invoice_dispatch id
    INNER JOIN @tmpDispatches td ON id.DispatchNumber = td.DispatchNumber OR id.DisptachNumberNew = td.DispatchNumber
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
    ORDER BY TransactionID DESC
    

    --SELECT * FROM @tmpDispatchInfo
    
    SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                 @NewDispatchNumber = DisptachNumberNew,
                 @ClaimAspectID     = ClaimAspectID
    FROM @tmpDispatchInfo
    WHERE DisptachNumberNew IS NOT NULL
    
    SET @LoopCountMax = 1000 -- Prevent endless loop
    
    WHILE @OldDispatchNumber IS NOT NULL AND @LoopCountMax > 0
    BEGIN
    
       --PRINT '@OldDispatchNumber=' + @OldDispatchNumber
       INSERT INTO @tmpDispatchInfo 
       SELECT  @ClaimAspectID,
               TransactionID,
               Amount,
               Description,
               DisbursementAmount,
               DisbursementDate,
               DisbursementMethodCD,
               dc.Name,
               DispatchNumber,
               DisptachNumberNew,
               TransactionCD,
               tc.Name,
               TransactionDate
       FROM dbo.utb_invoice_dispatch id
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
       WHERE DispatchNumber = @OldDispatchNumber
       ORDER BY TransactionID DESC
       
       UPDATE @tmpDispatchInfo
       SET DisptachNumberNew = NULL
       WHERE DispatchNumber = @OldDispatchNumber
         AND DisptachNumberNew = @NewDispatchNumber
       
       SET @OldDispatchNumber = NULL
       SET @NewDispatchNumber = NULL
       SET @ClaimAspectID = NULL
      
       SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                    @NewDispatchNumber = DisptachNumberNew,
                    @ClaimAspectID = ClaimAspectID
       FROM @tmpDispatchInfo
       WHERE DisptachNumberNew IS NOT NULL
       
       SET @LoopCountMax = @LoopCountMax - 1
      
    END    


    SELECT
        1    as Tag,
        Null as Parent,
        Null as [Root!1!Root],
        -- Claim Aspects
        Null as [ClaimAspect!2!ClaimAspectID],
        Null as [ClaimAspect!2!ClaimAspectNumber],
        Null as [ClaimAspect!2!ServiceChannel],
        Null as [ClaimAspect!2!Disposition],
        -- Dispatch Information
        Null as [Dispatch!3!ClaimAspectID],
        Null as [Dispatch!3!TransactionID],
        Null as [Dispatch!3!Amount],
        Null as [Dispatch!3!Description],
        Null as [Dispatch!3!DisbursementAmount],
        Null as [Dispatch!3!DisbursementDate],
        Null as [Dispatch!3!DisbursementMethodCD],
        Null as [Dispatch!3!DisbursementMethodDesc],
        Null as [Dispatch!3!DispatchNumber],
        Null as [Dispatch!3!DisptachNumberNew],
        Null as [Dispatch!3!TransactionCD],
        Null as [Dispatch!3!TransactionDesc],
        Null as [Dispatch!3!TransactionDate],
        Null as [Dispatch!3!TransactionDateFormatted]
        
    UNION ALL

    SELECT  
        2,
        1,
        NULL,
        -- Claim Aspects
        ClaimAspectID,
        ClaimAspectNumber,
        ServiceChannelDesc,
        DispositionDesc,
        -- Dispatch Information
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL

    FROM
        @tmpClaimAspects

    UNION ALL

    SELECT  
        3,
        1,
        NULL,
        -- Claim Aspects
        NULL, NULL, NULL, NULL,
        -- Dispatch Information
        ClaimAspectID,
        TransactionID,
        Amount,
        Description,
        DisbursementAmount,
        convert(varchar, DisbursementDate, 101),
        DisbursementMethodCD,
        DisbursementMethodDesc,
        DispatchNumber,
        DisptachNumberNew,
        TransactionCD,
        TransactionDesc,
        TransactionDate,
        convert(varchar, TransactionDate, 101)

    FROM
        @tmpDispatchInfo

    FOR XML EXPLICIT      -- (Commented for Client-side processing)
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspJournalGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspJournalGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspJournalGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/