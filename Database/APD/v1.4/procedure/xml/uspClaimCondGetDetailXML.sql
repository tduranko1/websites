-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCondGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCondGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCondGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This procedure returns the data for the Condensed claim view
*
* PARAMETERS:  
*   @LynxID                 The LynxID to get information for 
*   @InsuranceCompanyID     The Insurance company to validate the claim against. 
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClaimCondGetDetailXML]
    @LynxID                 udt_std_id_big,
    @InsuranceCompanyID     udt_std_id
AS
BEGIN
    -- Set database options

    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables

    DECLARE @InsuranceCompanyIDClaim    udt_std_id
    DECLARE @LossTypeParentID           udt_std_id
    DECLARE @LossTypeGrandParentID      udt_std_id
    DECLARE @ClaimAspectCode            varchar(3)
    DECLARE @VehicleClaimAspectTypeID   udt_std_id
    DECLARE @Claim_ClaimAspectTypeID AS udt_std_int_tiny

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimGetDetailXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Get the Insurance Company Id for the claim
    SELECT  @InsuranceCompanyIDClaim = InsuranceCompanyID 
      FROM  dbo.utb_claim 
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


/*************************************************************************************
*  Get @LossTypeParentID and @LossTypeGrandParentID
**************************************************************************************/

    SELECT  @LossTypeParentID = IsNull(ParentLossTypeID, 0)
      FROM  dbo.utb_Claim C
      LEFT JOIN dbo.utb_Loss_Type LT on C.LossTypeID = LT.LossTypeID
      WHERE C.LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @LossTypeGrandParentID = IsNull(ParentLossTypeID, 0)
      FROM  dbo.utb_Loss_Type LT 
      WHERE LT.LossTypeID = @LossTypeParentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


/*************************************************************************************
*  Get Claim Aspect Code
**************************************************************************************/

    SELECT  @ClaimAspectCode = Code
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectCode IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get the Claim's ClaimAspectTypeID
    SELECT @Claim_ClaimAspectTypeID = ClaimAspectTypeID 
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @Claim_ClaimAspectTypeID IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    
/*************************************************************************************
*  Get Claim Aspect Code for Vehicle
**************************************************************************************/
    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Owner')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Owner"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get the aspect type id for property for use later
    
    SELECT  @VehicleClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'


/*************************************************************************************
*  Gather metadata for all entities updatable columns
**************************************************************************************/

    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )

--    Claim
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Claim',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_Claim' AND Column_Name IN
            ('CarrierRepUserID',
             'CatastrophicLossID',
             'ClaimantContactMethodID',
             'InsuranceCompanyID',
             'LossTypeID',
             'RoadLocationID',
             'RoadTypeID',
             'WeatherConditionID',
             'AgentAreaCode',
             'AgentExchangeNumber',
             'AgentExtensionNumber',
             'AgentName',
             'AgentUnitNumber',
             'LossCity',
             'LossCounty',
             'LossDate',
             'LossDescription',
             'LossLocation',
             'LossState',
             'LossZip',
             'PoliceDepartmentName',
             'PolicyNumber',
             'ClientClaimNumber',
             'ClientClaimNumberSquished',
             'Remarks',
             'RestrictedFlag',
             'TripPurposeCD'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


--    Caller
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Caller',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_Involved' AND Column_Name IN
            ('InsuredRelationID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressState',
             'AddressZip',
             'AlternateAreaCode',
             'AlternateExchangeNumber',
             'AlternateExtensionNumber',
             'AlternateUnitNumber',
             'BestContactTime',
             'BestContactPhoneCD',
             'DayAreaCode',
             'DayExchangeNumber',
             'DayExtensionNumber',
             'DayUnitNumber',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'NightAreaCode',
             'NightExchangeNumber',
             'NightExtensionNumber',
             'NightUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


--    Insured
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Insured',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_Involved' AND Column_Name IN
            ('Address1',
             'Address2',
             'AddressCity',
             'AddressState',
             'AddressZip',
             'AlternateAreaCode',
             'AlternateExchangeNumber',
             'AlternateExtensionNumber',
             'AlternateUnitNumber',
             'BestContactTime',
             'BestContactPhoneCD',
             'BusinessName',
             'DayAreaCode',
             'DayExchangeNumber',
             'DayExtensionNumber',
             'DayUnitNumber',
             'EmailAddress',
             'FedTaxID',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'NightAreaCode',
             'NightExchangeNumber',
             'NightExtensionNumber',
             'NightUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


--    Contact
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Contact',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_Involved' AND Column_Name IN
            ('InsuredRelationID',
             'Address1',
             'Address2',
             'AddressCity',
             'AddressState',
             'AddressZip',
             'AlternateAreaCode',
             'AlternateExchangeNumber',
             'AlternateExtensionNumber',
             'AlternateUnitNumber',
             'BestContactTime',
             'BestContactPhoneCD',
             'DayAreaCode',
             'DayExchangeNumber',
             'DayExtensionNumber',
             'DayUnitNumber',
             'EmailAddress',
             'NameFirst',
             'NameLast',
             'NameTitle',
             'NightAreaCode',
             'NightExchangeNumber',
             'NightExtensionNumber',
             'NightUnitNumber',
             'PrefMethodUpd',
             'CellPhoneCarrier',
             'CellAreaCode',
             'CellExchangeNumber',
             'CellUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END
    

--    Coverage
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Coverage',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_Claim_Coverage' AND Column_Name IN
            ('ClaimCoverageID',
             'ClientCoverageTypeID',
             'LynxID',
             'AddtlCoverageFlag',
             'CoverageTypeCD',
             'Description',
             'DeductibleAmt',
             'LimitAmt',
             'LimitDailyAmt',
             'MaximumDays',
             'SysLastUserID',
             'SysLastUpdatedDate'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

--    Carrier - user data
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Carrier',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_user' AND Column_Name IN
            ('EmailAddress',
             'FaxAreaCode',
             'FaxExchangeNumber',
             'FaxExtensionNumber',
             'FaxUnitNumber',
             'NameLast',
             'NameFirst',
             'NameTitle',
             'PhoneAreaCode',
             'PhoneExchangeNumber',
             'PhoneExtensionNumber',
             'PhoneUnitNumber'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

--    Carrier - office data
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Carrier',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_office' AND Column_Name IN
            ('Name'))

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    
    update @tmpMetadata
    set ColumnName = 'OfficeName'
    where GroupName = 'Carrier'
      and ColumnName = 'Name'

/*************************************************************************************
*  Gather Reference Data
**************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName              varchar(50)             NOT NULL,
        DisplayOrder          int                     NULL,
        ReferenceId           varchar(10)             NOT NULL,
        Name                  varchar(50)             NOT NULL,
        CatStateCode          char(2)                 NULL,
        CatLossNumber         varchar(30)             NULL,
        LossParentID          int                     NULL,
        ClientCoverageTypeID  int                     null,
	     AddtlCoverageFlag     bit			            null,
	     ClientCode            varchar(20)             null
    )

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name, CatStateCode, CatLossNumber, LossParentID, ClientCoverageTypeID, AddtlCoverageFlag, ClientCode)

    SELECT  'CatastrophicLoss' AS ListName,
            NULL AS DisplayOrder,
            convert(varchar, CatastrophicLossID),
            Description,
            StateCode,
            CatastrophicLossNumber,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_catastrophic_loss

    UNION ALL

    SELECT  'NoticeMethod' AS ListName,
            DisplayOrder,
            convert(varchar, ClaimantContactMethodID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_claimant_contact_method
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL
    
    SELECT  'LossType',
            DisplayOrder,
            convert(varchar, LossTypeID),
            Name,
            NULL,
            NULL,
            ParentLossTypeID,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Loss_Type
    WHERE   EnabledFlag = 1  

    UNION ALL

    SELECT  'RoadLocation',
            DisplayOrder,
            convert(varchar, RoadLocationID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Road_Location
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'RoadType' AS ListName,
            DisplayOrder,
            convert(varchar, RoadTypeID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Road_Type
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL
    
    SELECT  'WeatherCondition',
            DisplayOrder,
            convert(varchar, WeatherConditionID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Weather_Condition
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL
    
    SELECT  'CallerRelationToInsured',
            DisplayOrder,
            convert(varchar, RelationID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Relation
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL
    
    SELECT  'ContactRelationToInsured',
            DisplayOrder,
            convert(varchar, RelationID),
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_Relation
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL
    AND     Name NOT LIKE 'Third%'

    UNION ALL
    
    SELECT  'TripPurpose',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim', 'TripPurposeCD' )

    UNION ALL
    
    SELECT  'BestContactPhone',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'BestContactPhoneCD' )

    UNION ALL
    
    SELECT  'PrefMethodUpd',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'PrefMethodUpd' )

    UNION ALL
   
   SELECT  'CellPhoneCarrier',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_involved', 'CellPhoneCarrier' )

    UNION ALL
    
    SELECT  'State',
            NULL,
            StateCode,
            StateValue,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_state_code

	UNION ALL
    
    SELECT  'CoverageTypeCD',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_claim_coverage', 'CoverageTypeCD' )

	UNION ALL

	SELECT 'ClientCoverageType',
		DisplayOrder,
		convert(varchar, CoverageProfileCD),
		Name,
		NULL,
		NULL,
      NULL,
		ClientCoverageTypeID,
      AdditionalCoverageFlag,
      ClientCode
	from	dbo.utb_client_coverage_type
	where	InsuranceCompanyID = @InsuranceCompanyID
	and		EnabledFlag = 1

    ORDER BY ListName, DisplayOrder

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    -- Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Insured Involved Role Type Not Found
    
        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Witness')
    BEGIN
       -- Witness Involved Role Type Not Found
    
        RAISERROR('102|%s|"Witness"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END


/*************************************************************************************
*  Ok.  Deep breath.  Let's start the XML select
**************************************************************************************/

/*************************************************************************************
*  Root (and column structure)
**************************************************************************************/


    SELECT
        1 as Tag,
        Null as Parent,
--        Root
        @LynxID as [Root!1!LynxID],
        @InsuranceCompanyID as [Root!1!InsuranceCompanyID],
        @ClaimAspectCode as [Root!1!Context],
--        Claim
        Null as [Claim!2!LynxID],
        Null as [Claim!2!CatastrophicLossID],
        Null as [Claim!2!ClaimantContactMethodID],
/*
Project:210474 APD - Enhancements to support multiple concurrent service channels
Note:	Remarked off the following when we did the merge of VSS1.3 & VSS1.4 of APD
	M.A. 20061114
--      Null as [Claim!2!ClientClaimNumber],
*/
        Null as [Claim!2!InsuranceCompanyID],
        Null as [Claim!2!IntakeUserFirstName],
        Null as [Claim!2!IntakeUserLastName],
        Null as [Claim!2!IntakeUserCompany],
        Null as [Claim!2!LossTypeID],
        Null as [Claim!2!LossTypeParentID],
        Null as [Claim!2!LossTypeGrandParentID],
        Null as [Claim!2!OwnerUserFirstName],
        Null as [Claim!2!OwnerUserLastName],
        Null as [Claim!2!OwnerUserEmail],
        Null as [Claim!2!OwnerUserAreaCode],
        Null as [Claim!2!OwnerUserExchangeNumber],
        Null as [Claim!2!OwnerUserExtensionNumber],
        Null as [Claim!2!OwnerUserUnitNumber],
        Null as [Claim!2!RoadLocationID],
        Null as [Claim!2!RoadTypeID],
        Null as [Claim!2!WeatherConditionID],
        Null as [Claim!2!AgentAreaCode],
        Null as [Claim!2!AgentExchangeNumber],
        Null as [Claim!2!AgentExtensionNumber],
        Null as [Claim!2!AgentUnitNumber],
        Null as [Claim!2!AgentName],
        Null as [Claim!2!RestrictedFlag],
        Null as [Claim!2!IntakeStartDate],
        Null as [Claim!2!IntakeFinishDate],
        Null as [Claim!2!LossCity],
        Null as [Claim!2!LossCounty],
        Null as [Claim!2!LossDate],
        Null as [Claim!2!LossDescription],
        Null as [Claim!2!LossLocation],
        Null as [Claim!2!LossState],
        Null as [Claim!2!LossZip],
        Null as [Claim!2!PoliceDepartmentName],
        Null as [Claim!2!PolicyNumber],
        Null as [Claim!2!ClientClaimNumber],
        Null as [Claim!2!ClientClaimNumberSquished],
        Null as [Claim!2!Remarks],
        Null as [Claim!2!TripPurposeCD],
        Null as [Claim!2!SysLastUpdatedDate],
--        Caller
        Null as [Caller!3!InvolvedID],
        Null as [Caller!3!NameFirst],
        Null as [Caller!3!NameLast],
        Null as [Caller!3!NameTitle],
        Null as [Caller!3!InsuredRelationID],
        Null as [Caller!3!Address1],
        Null as [Caller!3!Address2],
        Null as [Caller!3!AddressCity],
        Null as [Caller!3!AddressState],
        Null as [Caller!3!AddressZip],
        Null as [Caller!3!DayAreaCode],
        Null as [Caller!3!DayExchangeNumber],
        Null as [Caller!3!DayExtensionNumber],
        Null as [Caller!3!DayUnitNumber],
        Null as [Caller!3!NightAreaCode],
        Null as [Caller!3!NightExchangeNumber],
        Null as [Caller!3!NightExtensionNumber],
        Null as [Caller!3!NightUnitNumber],
        Null as [Caller!3!AlternateAreaCode],
        Null as [Caller!3!AlternateExchangeNumber],
        Null as [Caller!3!AlternateExtensionNumber],
        Null as [Caller!3!AlternateUnitNumber],
        Null as [Caller!3!BestContactTime],
        Null as [Caller!3!BestContactPhoneCD],
        Null as [Caller!3!SysLastUpdatedDate],
--        Insured
        Null as [Insured!4!InvolvedID],
        Null as [Insured!4!NameFirst],
        Null as [Insured!4!NameLast],
        Null as [Insured!4!NameTitle],
        Null as [Insured!4!BusinessName],
        Null as [Insured!4!Address1],
        Null as [Insured!4!Address2],
        Null as [Insured!4!AddressCity],
        Null as [Insured!4!AddressState],
        Null as [Insured!4!AddressZip],
        Null as [Insured!4!DayAreaCode],
        Null as [Insured!4!DayExchangeNumber],
        Null as [Insured!4!DayExtensionNumber],
        Null as [Insured!4!DayUnitNumber],
        Null as [Insured!4!EmailAddress],
        Null as [Insured!4!FedTaxId],
        Null as [Insured!4!NightAreaCode],
        Null as [Insured!4!NightExchangeNumber],
        Null as [Insured!4!NightExtensionNumber],
        Null as [Insured!4!NightUnitNumber],
        Null as [Insured!4!AlternateAreaCode],
        Null as [Insured!4!AlternateExchangeNumber],
        Null as [Insured!4!AlternateExtensionNumber],
        Null as [Insured!4!AlternateUnitNumber],
        Null as [Insured!4!BestContactTime],
        Null as [Insured!4!BestContactPhoneCD],
        Null as [Insured!4!SysLastUpdatedDate],
--        Contact
        Null as [Contact!5!InvolvedID],
        Null as [Contact!5!NameFirst],
        Null as [Contact!5!NameLast],
        Null as [Contact!5!NameTitle],
        Null as [Contact!5!InsuredRelationID],
        Null as [Contact!5!Address1],
        Null as [Contact!5!Address2],
        Null as [Contact!5!AddressCity],
        Null as [Contact!5!AddressState],
        Null as [Contact!5!AddressZip],
        Null as [Contact!5!DayAreaCode],
        Null as [Contact!5!DayExchangeNumber],
        Null as [Contact!5!DayExtensionNumber],
        Null as [Contact!5!DayUnitNumber],
        Null as [Contact!5!EmailAddress],
        Null as [Contact!5!NightAreaCode],
        Null as [Contact!5!NightExchangeNumber],
        Null as [Contact!5!NightExtensionNumber],
        Null as [Contact!5!NightUnitNumber],
        Null as [Contact!5!AlternateAreaCode],
        Null as [Contact!5!AlternateExchangeNumber],
        Null as [Contact!5!AlternateExtensionNumber],
        Null as [Contact!5!AlternateUnitNumber],
        Null as [Contact!5!BestContactTime],
        Null as [Contact!5!BestContactPhoneCD],
        NULL as [Contact!5!PrefMethodUpd],
        NULL as [Contact!5!CellPhoneCarrier],
        NULL as [Contact!5!CellAreaCode],
        NULL as [Contact!5!CellExchangeNumber],
        NULL as [Contact!5!CellUnitNumber],
        Null as [Contact!5!SysLastUpdatedDate],
--        Carrier
        Null as [Carrier!6!UserID],
        Null as [Carrier!6!EmailAddress],
        Null as [Carrier!6!FaxAreaCode],
        Null as [Carrier!6!FaxExchangeNumber],
        Null as [Carrier!6!FaxExtensionNumber],
        Null as [Carrier!6!FaxUnitNumber],
        Null as [Carrier!6!NameFirst],
        Null as [Carrier!6!NameLast],
        Null as [Carrier!6!NameTitle],
        Null as [Carrier!6!OfficeName],        
        Null as [Carrier!6!OfficeEmailAddress], --Project:210474 APD Added the column when we did the merge M.A.20061114
        Null as [Carrier!6!PhoneAreaCode],
        Null as [Carrier!6!PhoneExchangeNumber],
        Null as [Carrier!6!PhoneExtensionNumber],
        Null as [Carrier!6!PhoneUnitNumber],
        Null as [Carrier!6!ReturnDocDestinationCD], --Project:210474 APD Added the column when we did the merge M.A.20061114
        Null as [Carrier!6!OfficeID], --Project:210474 APD Added the column for ClaimPoint use M.A.20070109
        Null as [Carrier!6!ActiveFlag],
--        Witness
        Null as [Witness!7!InvolvedID],
        Null as [Witness!7!NameFirst],
        Null as [Witness!7!NameLast],
--        Coverage
        NULL as [Coverage!8!ClaimCoverageID],
        NULL as [Coverage!8!ClientCoverageTypeID],
        NULL as [Coverage!8!LynxID],
        NULL as [Coverage!8!AddtlCoverageFlag],
        NULL as [Coverage!8!CoverageTypeCD],
        NULL as [Coverage!8!Description],
        NULL as [Coverage!8!ClientCoverageDesc],
        NULL as [Coverage!8!SystemDesc],
        NULL as [Coverage!8!CoverageProfileCD],
        NULL as [Coverage!8!DeductibleAmt],
        NULL as [Coverage!8!LimitAmt],
        NULL as [Coverage!8!LimitDailyAmt],
        NULL as [Coverage!8!MaximumDays],
        NULL as [Coverage!8!CoverageApplied],
        NULL as [Coverage!8!SysLastUserID],
        NULL as [Coverage!8!SysLastUpdatedDate],
--        Coverage Applied
        NULL as [CoverageApplied!9!ClaimCoverageID],
        NULL as [CoverageApplied!9!ClaimAspectServiceChannelID],
        NULL as [CoverageApplied!9!DeductibleAppliedAmt],
        NULL as [CoverageApplied!9!LimitAppliedAmt],
--        Vehicle List - Summary of the Vehicle information
        Null as [Vehicle!10!VehicleNumber],
        Null as [Vehicle!10!ClaimAspectID],
        Null as [Vehicle!10!VehicleYear],
        Null as [Vehicle!10!Make],
        Null as [Vehicle!10!Model],
        Null as [Vehicle!10!VIN],
        Null as [Vehicle!10!NameFirst],
        Null as [Vehicle!10!NameLast],
        Null as [Vehicle!10!BusinessName],
        Null as [Vehicle!10!ClosedStatus],
        Null as [Vehicle!10!StatusID],
        Null as [Vehicle!10!Status],
        Null as [Vehicle!10!ExposureCD],
        Null as [Vehicle!10!CoverageProfileCD],
        Null as [Vehicle!10!OwnerUserFirstName],
        Null as [Vehicle!10!OwnerUserLastName],
        Null as [Vehicle!10!OwnerUserEmail],
        Null as [Vehicle!10!OwnerUserAreaCode],
        Null as [Vehicle!10!OwnerUserExchangeNumber],
        Null as [Vehicle!10!OwnerUserExtensionNumber],
        Null as [Vehicle!10!OwnerUserUnitNumber],
--        MetaData
        Null as [Metadata!11!Entity],
--        Columns
        Null as [Column!12!Name],
        Null as [Column!12!DataType],
        Null as [Column!12!MaxLength],
        NULL AS [Column!12!Precision],
        NULL AS [Column!12!Scale],
        Null as [Column!12!Nullable],
--        Reference Data
        Null as [Reference!13!List],
        Null as [Reference!13!DisplayOrder],
        Null as [Reference!13!ReferenceID],
        Null as [Reference!13!Name],
        NULL AS [Reference!13!StateCode],
        NULL AS [Reference!13!CatastrophicLossNumber],
        Null as [Reference!13!ParentID],
        NULL as [Reference!13!ClientCoverageTypeID],
        NULL as [Reference!13!AddtlCoverageFlag],
        NULL as [Reference!13!ClientCode]

    UNION ALL

--*************************************************************************************
--*  Claim
--*************************************************************************************

    SELECT
        2 as Tag,
        1 as Parent,
--        root
        NULL, NULL, NULL,
--        Claim
        IsNull(c.LynxID, 0),
        IsNull(c.CatastrophicLossID, ''),
        IsNull(c.ClaimantContactMethodID, ''),
--        IsNull(c.ClientClaimNumber, ''), --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
        IsNull(c.InsuranceCompanyID, ''),
        IsNull(iu.NameFirst, ''),
        IsNull(iu.NameLast, ''),
        CASE 
          WHEN iu.OfficeID IS NULL THEN 'Lynx Services'
          ELSE IsNull(iui.Name, '')
        END,
        IsNull(c.LossTypeID, ''),
        IsNull(@LossTypeParentID,0),
        IsNull(@LossTypeGrandParentID,0),
        IsNull(ou.NameFirst, ''),
        IsNull(ou.NameLast, ''),
        IsNull(ou.EmailAddress, ''),
        IsNull(ou.PhoneAreaCode, ''),
        IsNull(ou.PhoneExchangeNumber, ''),
        IsNull(ou.PhoneExtensionNumber, ''),
        IsNull(ou.PhoneUnitNumber, ''),
        IsNull(c.RoadLocationID, ''),
        IsNull(c.RoadTypeID, ''),
        IsNull(c.WeatherConditionID, ''),
        IsNull(c.AgentAreaCode, ''),
        IsNull(c.AgentExchangeNumber, ''),
        IsNull(c.AgentExtensionNumber, ''),
        IsNull(c.AgentUnitNumber, ''),
        IsNull(c.AgentName, ''),
        IsNull(c.RestrictedFlag, ''),
        IsNull(c.IntakeStartDate, ''),
        IsNull(c.IntakeFinishDate, ''),
        IsNull(c.LossCity, ''),
        IsNull(c.LossCounty, ''),
        IsNull(c.LossDate, ''),
        IsNull(c.LossDescription, ''),
        IsNull(c.LossLocation, ''),
        IsNull(c.LossState, ''),
        IsNull(c.LossZip, ''),
        IsNull(c.PoliceDepartmentName, ''),
        IsNull(c.PolicyNumber, ''),
        IsNull(c.ClientClaimNumber, ''),
        IsNull(c.ClientClaimNumberSquished, ''),
        IsNull(c.Remarks, ''),
        IsNull(c.TripPurposeCD, ''),
        dbo.ufnUtilityGetDateString( c.SysLastUpdatedDate ),
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
       
    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_Claim c on parms.LynxID = c.LynxID
        LEFT JOIN dbo.utb_Claim_Aspect ca on parms.LynxID = ca.LynxID
        LEFT JOIN dbo.utb_user iu on c.IntakeUserID = iu.UserID
        LEFT JOIN dbo.utb_office iuo ON (iu.OfficeID = iuo.OfficeID)
        LEFT JOIN dbo.utb_insurance iui ON (iuo.InsuranceCompanyID = iui.InsuranceCompanyID)
        LEFT JOIN dbo.utb_user ou on (ca.OwnerUserID = ou.UserID)
    WHERE ca.ClaimAspectTypeID = @Claim_ClaimAspectTypeID


    UNION ALL
    
--*************************************************************************************
--*  Caller
--**************************************************************************************

    SELECT
        3 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        IsNull(i.InvolvedID, 0),
        IsNull(i.NameFirst, ''),
        IsNull(i.NameLast, ''),
        IsNull(i.NameTitle, ''),
        IsNull(i.InsuredRelationID, 0),
        IsNull(i.Address1, ''),
        IsNull(i.Address2, ''),
        IsNull(i.AddressCity, ''),
        IsNull(i.AddressState, ''),
        IsNull(i.AddressZip, ''),
        IsNull(i.DayAreaCode, ''),
        IsNull(i.DayExchangeNumber, ''),
        IsNull(i.DayExtensionNumber, ''),
        IsNull(i.DayUnitNumber, ''),
        IsNull(i.NightAreaCode, ''),
        IsNull(i.NightExchangeNumber, ''),
        IsNull(i.NightExtensionNumber, ''),
        IsNull(i.NightUnitNumber, ''),
        IsNull(i.AlternateAreaCode, ''),
        IsNull(i.AlternateExchangeNumber, ''),
        IsNull(i.AlternateExtensionNumber, ''),
        IsNull(i.AlternateUnitNumber, ''),
        IsNull(i.BestContactTime, ''),
        IsNull(i.BestContactPhoneCD, ''),
        dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate ),
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, Null, NULL, --Project:210474 APD - Added the column when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_Claim c on parms.LynxID = c.LynxID
        Left Join dbo.utb_Involved i on c.CallerInvolvedID = i.InvolvedID


    UNION ALL
    
--*************************************************************************************
--*  Insured
--**************************************************************************************

    SELECT
        4 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--        NULL,  --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        IsNull(i.InvolvedID, 0),
        IsNull(i.NameFirst, ''),
        IsNull(i.NameLast, ''),
        IsNull(i.NameTitle, ''),
        IsNull(i.BusinessName, ''),
        IsNull(i.Address1, ''),
        IsNull(i.Address2, ''),
        IsNull(i.AddressCity, ''),
        IsNull(i.AddressState, ''),
        IsNull(i.AddressZip, ''),
        IsNull(i.DayAreaCode, ''),
        IsNull(i.DayExchangeNumber, ''),
        IsNull(i.DayExtensionNumber, ''),
        IsNull(i.DayUnitNumber, ''),
        IsNull(i.EmailAddress, ''),
        IsNull(i.FedTaxID, ''),
        IsNull(i.NightAreaCode, ''),
        IsNull(i.NightExchangeNumber, ''),
        IsNull(i.NightExtensionNumber, ''),
        IsNull(i.NightUnitNumber, ''),
        IsNull(i.AlternateAreaCode, ''),
        IsNull(i.AlternateExchangeNumber, ''),
        IsNull(i.AlternateExtensionNumber, ''),
        IsNull(i.AlternateUnitNumber, ''),
        IsNull(i.BestContactTime, ''),
        IsNull(i.BestContactPhoneCD, ''),              
        dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate ),
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID, 0 AS ClaimAspectTypeID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        Left Join dbo.utb_claim_aspect ca on (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID)
        Left Join dbo.utb_claim_aspect_involved cai on (ca.ClaimAspectID = cai.ClaimAspectID)
        Left Join dbo.utb_involved i on (cai.InvolvedID = i.InvolvedID)
        Left Join dbo.utb_involved_role ir on (i.InvolvedID = ir.InvolvedID)
        Left Join dbo.utb_involved_role_type irt on (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)

    WHERE cai.EnabledFlag = 1 
      AND (irt.Name = 'Insured' OR irt.Name IS NULL)


    UNION ALL

--*************************************************************************************
--*  Contact
--*************************************************************************************

    SELECT
        5 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        IsNull(i.InvolvedID, 0),
        IsNull(i.NameFirst, ''),
        IsNull(i.NameLast, ''),
        IsNull(i.NameTitle, ''),
        IsNull(i.InsuredRelationID, 0),
        IsNull(i.Address1, ''),
        IsNull(i.Address2, ''),
        IsNull(i.AddressCity, ''),
        IsNull(i.AddressState, ''),
        IsNull(i.AddressZip, ''),
        IsNull(i.DayAreaCode, ''),
        IsNull(i.DayExchangeNumber, ''),
        IsNull(i.DayExtensionNumber, ''),
        IsNull(i.DayUnitNumber, ''),
        IsNull(i.EmailAddress, ''),
        IsNull(i.NightAreaCode, ''),
        IsNull(i.NightExchangeNumber, ''),
        IsNull(i.NightExtensionNumber, ''),
        IsNull(i.NightUnitNumber, ''),
        IsNull(i.AlternateAreaCode, ''),
        IsNull(i.AlternateExchangeNumber, ''),
        IsNull(i.AlternateExtensionNumber, ''),
        IsNull(i.AlternateUnitNumber, ''),
        IsNull(i.BestContactTime, ''),
        IsNull(i.BestContactPhoneCD, ''),
        IsNull(i.PrefMethodUpd, ''),
        IsNull(i.CellPhoneCarrier, ''),
        IsNull(i.CellAreaCode, ''),
        IsNull(i.CellExchangeNumber, ''),
        IsNull(i.CellUnitNumber, ''),  
        dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate ),
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_Claim c on parms.LynxID = c.LynxID
        Left Join dbo.utb_Involved i on c.ContactInvolvedID = i.InvolvedID


    UNION ALL
   
-- *************************************************************************************
-- *  Carrier
-- **************************************************************************************

    SELECT
        6 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        IsNull(u.UserID, 0),
        LTrim(RTrim(IsNull(u.EmailAddress, ''))),
        LTrim(RTrim(IsNull(u.FaxAreaCode, ''))),
        LTrim(RTrim(IsNull(u.FaxExchangeNumber, ''))),
        LTrim(RTrim(IsNull(u.FaxExtensionNumber, ''))),
        LTrim(RTrim(IsNull(u.FaxUnitNumber, ''))),
        LTrim(RTrim(IsNull(u.NameFirst, ''))),
        LTrim(RTrim(IsNull(u.NameLast, ''))),
        LTrim(RTrim(IsNull(u.NameTitle, ''))),
        LTrim(RTrim(IsNull(o.Name, ''))),
        LTrim(RTrim(IsNull(o.CCEmailAddress, ''))), --Project:210474 APD - Added the column when we did the code merge M.A.20061114
        LTrim(RTrim(IsNull(u.PhoneAreaCode, ''))),
        LTrim(RTrim(IsNull(u.PhoneExchangeNumber, ''))),
        LTrim(RTrim(IsNull(u.PhoneExtensionNumber, ''))),
        LTrim(RTrim(IsNull(u.PhoneUnitNumber, ''))),
        IsNull(i.ReturnDocDestinationCD, ''),  --Project:210474 APD - Added the column when we did the code merge M.A.20061114,
        isnull(o.OfficeID,''),
        dbo.ufnUtilityIsUserActive(u.UserID, NULL, NULL), -- Carrier rep active in ClaimPoint
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_Claim c on (parms.LynxID = c.LynxID)
        Left Join dbo.utb_user u on (c.CarrierRepUserID = u.UserID)
        Left Join dbo.utb_office o on (u.OfficeID = o.OfficeID)
	--Project:210474 APD - Added the table below when we did the code merge M.A.20061114
        LEFT JOIN dbo.utb_insurance i on (c.InsuranceCompanyID = i.InsuranceCompanyID)

    UNION ALL

-- *************************************************************************************
-- *  Witness
-- **************************************************************************************

    SELECT
        7 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, 	--Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, --Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        IsNull(i.InvolvedID, 0),
        IsNull(i.NameFirst, ''),
        IsNull(i.NameLast, ''),
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID, 0 AS ClaimAspectTypeID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        Left Join dbo.utb_claim_aspect ca on (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID)
        Left Join dbo.utb_claim_aspect_involved cai on (ca.ClaimAspectID = cai.ClaimAspectID)
        Left Join dbo.utb_involved i on (cai.InvolvedID = i.InvolvedID)
        Left Join dbo.utb_involved_role ir on (i.InvolvedID = ir.InvolvedID)
        Left Join dbo.utb_involved_role_type irt on (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)

    WHERE cai.EnabledFlag = 1 
      AND (irt.Name = 'Witness' OR irt.Name IS NULL)


    UNION ALL

-- *************************************************************************************
-- *  Coverage
-- **************************************************************************************

    SELECT
        8 as tag,
        2 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        cc.ClaimCoverageID,
        ISNULL(cast(cc.ClientCoverageTypeID as varchar(20)),''),
        cc.LynxID,
        cc.AddtlCoverageFlag,
        cc.CoverageTypeCD,
        ISNULL(cc.Description,''),
        ISNULL(cct.Name,''),
        (SELECT Name
         FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_coverage', 'CoverageTypeCD') 
         WHERE Code = cc.CoverageTypeCD
        ),
        ISNULL(cct.CoverageProfileCD,''),
        ISNULL(cast(cc.DeductibleAmt as varchar(20)),''),
        ISNULL(cast(cc.LimitAmt as varchar(20)),''),
        ISNULL(cast(cc.LimitDailyAmt as varchar(20)),''),
        ISNULL(cast(cc.MaximumDays as varchar(20)),''),
        CASE
         WHEN EXISTS(SELECT ClaimAspectServiceChannelID
                     FROM dbo.utb_claim_aspect_service_channel_coverage cascc
                     WHERE cascc.ClaimCoverageID = cc.ClaimCoverageID
                       AND (cascc.DeductibleAppliedAmt > 0
                         OR cascc.LimitAppliedAmt > 0))
              THEN 1
         ELSE 0
        END,
        cc.SysLastUserID,
        dbo.ufnUtilityGetDateString(cc.SysLastUpdatedDate),
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_claim_coverage cc on parms.LynxID = cc.LynxID
    	  LEFT OUTER JOIN	dbo.utb_client_coverage_type cct on	cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
    WHERE cc.EnabledFlag = 1
 
    UNION ALL

-- *************************************************************************************
-- *  Coverage Applied
-- **************************************************************************************

    SELECT
        9 as tag,
        1 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        cascc.ClaimCoverageID,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        cascc.ClaimCoverageID, 
        cascc.ClaimAspectServiceChannelID, 
        isNull(cascc.DeductibleAppliedAmt, 0), 
        isNull(cascc.LimitAppliedAmt, 0),
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID) AS parms                 -- Added to return blank claim even if LynxID doesn't exist
        LEFT JOIN dbo.utb_claim_coverage cc on parms.LynxID = cc.LynxID
        LEFT JOIN dbo.utb_claim_aspect_service_channel_coverage cascc ON cc.ClaimCoverageID = cascc.ClaimCoverageID
    WHERE cc.EnabledFlag = 1
 
 
    UNION ALL

-- *************************************************************************************
-- *  Vehicle List
-- **************************************************************************************

    SELECT
        10 as tag,
        1 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull(cv.VIN, ''),
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@VehicleClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.StatusID,
        vs.Name,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, ''),
/*      --Project:210474 APD Remarked-off the following to support ClaimPoint M.A.20070109
        IsNull(ou.NameFirst, ''),
        IsNull(ou.NameLast, ''),
        IsNull(ou.EmailAddress, ''),
        IsNull(ou.PhoneAreaCode, ''),
        IsNull(ou.PhoneExchangeNumber, ''),
        IsNull(ou.PhoneExtensionNumber, ''),
        IsNull(ou.PhoneUnitNumber, ''),
*/
        --Project:210474 APD Added the following to support ClaimPoint M.A.20070109
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.NameFirst
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.NameFirst
            ELSE su.NameFirst
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.NameLast
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.NameLast
            ELSE su.NameLast
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.EmailAddress
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.EmailAddress
            ELSE su.EmailAddress
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.PhoneAreaCode
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.PhoneAreaCode
            ELSE su.PhoneAreaCode
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.PhoneExchangeNumber
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.PhoneExchangeNumber
            ELSE su.PhoneExchangeNumber
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.PhoneExtensionNumber
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.PhoneExtensionNumber
            ELSE su.PhoneExtensionNumber
        END,
        CASE
            WHEN casc.ServiceChannelCD in ('PS', 'ME', 'RRP') AND ca.OwnerUserID IS NOT NULL THEN ou.PhoneUnitNumber
            WHEN casc.ServiceChannelCD in ('DA', 'DR') AND ca.AnalystUserID IS NOT NULL THEN au.PhoneUnitNumber
            ELSE su.PhoneUnitNumber
        END,
        --Project:210474 APD Added the above to support ClaimPoint M.A.20070109
--        MetaData
        NULL, 
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        (SELECT @LynxID AS LynxID, @VehicleClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID AND 1 = ca.EnabledFlag)
        LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
        --Project:210474 APD Added the following to support ClaimPoint M.A.20070109
        Left Outer Join utb_Claim_Aspect_Service_Channel casc
        on casc.ClaimAspectID = cv.ClaimAspectID
        and casc.PrimaryFlag = 1
        --Project:210474 APD Added the above to support ClaimPoint M.A.20070109
        LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
        LEFT JOIN dbo.utb_status vs ON (cas.StatusID = vs.StatusID)
        LEFT JOIN dbo.utb_user ou on (ca.OwnerUserID = ou.UserID)
        --Project:210474 APD Added the following to support ClaimPoint M.A.20070109
        LEFT JOIN dbo.utb_user au on (ca.AnalystUserID = au.UserID)
        LEFT JOIN dbo.utb_user su on (ca.SupportUserID = su.UserID)
        --Project:210474 APD Added the above to support ClaimPoint M.A.20070109
    Where cas.StatusTypeCD is NULL

 
    UNION ALL

-- *************************************************************************************
-- *  MetaData
-- **************************************************************************************

    SELECT DISTINCT
        11 as tag,
        1 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        MetaData
        GroupName,
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        @tmpMetadata


    UNION ALL
   
-- *************************************************************************************
-- *  Columns
-- **************************************************************************************

    SELECT
        12 as tag,
        11 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL, --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        Metadata
        GroupName,
--        Columns
        ColumnName,
        DataType,
        MaxLength,
        NumericPrecision,
        Scale,
        Nullable, 
--        Reference
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM
        @tmpMetadata


    UNION ALL
   
-- *************************************************************************************
-- *  Reference
-- **************************************************************************************

    SELECT
        13 as tag,
        1 as parent,
--        Root
        NULL, NULL, NULL,
--        Claim
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,
--      NULL,  --Project:210474 APD - Removed the column when we did the code merge M.A.20061114
--        Caller
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
--        Insured
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--        Contact
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL,
--        Carrier
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,--Project:210474 APD - Added the columns when we did the code merge M.A.20061114
--        Witness
        NULL, NULL, NULL,
--        Coverage
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Coverage Applied
        NULL, NULL, NULL, NULL,
--        Vehicle List
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
--        Metadata
        'ZZ-Reference',
--        Columns
        NULL, NULL, NULL, NULL, NULL, NULL,
--        Reference
        ListName,
        DisplayOrder,
        ReferenceID,
        Name,
        CatStateCode,
        CatLossNumber,
        LossParentID,
        ClientCoverageTypeID,
        AddtlCoverageFlag,
        ClientCode

    FROM
        @tmpReference

    ORDER BY [Metadata!11!Entity], tag
--    FOR XML EXPLICIT  (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCondGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCondGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
