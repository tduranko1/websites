-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopLoadGetDetailXML 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopLoadGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieve a specific Shop Location
*
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSMTShopLoadGetDetailXML]
(
	@ShopLoadID       udt_std_id_big
  
  
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error            AS INT
    DECLARE @rowcount         AS INT
    DECLARE @Now              AS datetime
    DECLARE @ProcName         AS varchar(30)       -- Used for raise error stmts 
    
    DECLARE @FedTaxID         AS varchar(15)
    DECLARE @ShopID           AS bigint
    DECLARE @ShopLocationID   AS bigint
    DECLARE @tmpShop             TABLE (ShopID          bigint    not null)
    DECLARE @tmpShopLocation     TABLE (ShopLocationID  bigint    not null,
                                        ShopID          bigint    not null)
    
    
    
    SET @ProcName = 'uspSMTShopLoadGetDetailXML'
    SET @Now = CURRENT_TIMESTAMP
    
               
    
    -- insert the is of all Shops that match the Load Record's zip code and 10-digit phone number
    INSERT INTO @tmpShopLocation
    SELECT s.ShopLocationID, s.ShopID
    FROM dbo.utb_shop_location s INNER JOIN dbo.utb_shop_load l ON s.AddressZip = l.AddressZip
                                                               AND s.PhoneAreaCode = l.PhoneAreaCode
                                                               AND s.PhoneExchangeNumber = l.PhoneExchangeNumber
                                                               AND s.PhoneUnitNumber = l.PhoneUnitNumber
    WHERE s.EnabledFlag = 1
      AND l.ShopLoadID = @ShopLoadID
    
                  
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpShopLocation', 16, 1, @ProcName)
        RETURN
    END                     
   
    -- insert the ShopLocationID from the load record if it is not already in @tmpShopLocation
    INSERT INTO @tmpShopLocation
    SELECT ShopLocationID, ShopID
    FROM dbo.utb_shop_location
    WHERE ShopLocationID = (SELECT ShopLocationID FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID)
      AND ShopLocationID NOT IN (SELECT ShopLocationID FROM @tmpShopLocation)
      AND EnabledFlag = 1
      
      
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpShopLocation', 16, 1, @ProcName)
        RETURN
    END      
                       
    
    ----------------------------------------  need for speed ------------------------
    -- this was included as a subquery below but was a performance problem               
    DECLARE   @tmpFedTaxID  varchar(15)
    
    SET @tmpFedTaxID = (SELECT dbo.ufnUtilitySquishString(FedTaxID, 0, 1, 0, NULL)  
                      FROM dbo.utb_shop_load
                      WHERE ShopLoadID = @ShopLoadID)
    ---------------------------------------------------------------------------------
        
                         
    -- get the id of all Businesses that have a FedTaxID matching the one in shop_load
    -- exclude any Businesses that own one or more of the Shops in @tmpShopLocation                                              
    INSERT INTO @tmpShop
    SELECT ShopID
    FROM dbo.utb_shop 
    WHERE EnabledFlag = 1
      AND ShopID NOT IN (SELECT ShopID FROM @tmpShopLocation)
      AND FedTaxID = @tmpFedTaxID
                      
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpShop', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- insert the ids of all Businesses that match the Load Record's zip code and 10-digit phone number
    -- exclude businesses that would be duplicates
    -- exclude businesses that own one or more of the Shops in @tmpShopLocation
    INSERT INTO @tmpShop
    SELECT s.ShopID
    FROM dbo.utb_shop s INNER JOIN dbo.utb_shop_load l ON s.AddressZip = l.AddressZip
                                                      AND s.PhoneAreaCode = l.PhoneAreaCode
                                                      AND s.PhoneExchangeNumber = l.PhoneExchangeNumber
                                                      AND s.PhoneUnitNumber = l.PhoneUnitNumber
    WHERE s.EnabledFlag = 1
      AND l.ShopLoadID = @ShopLoadID
      AND s.ShopID NOT IN (SELECT ShopID FROM @tmpShop)
      AND s.ShopID NOT IN (SELECT ShopID FROM @tmpShopLocation)
      
      
      
                      
    -- insert the shop id from the shop load record if it is not already in @tmpShop   
    -- exclude any Businesses that own one or more of the Shops in @tmpShopLocation                   
    INSERT INTO @tmpShop
    SELECT ShopID
    FROM dbo.utb_shop
    WHERE ShopID = (SELECT ShopID FROM dbo.utb_shop_load WHERE ShopLoadID = @ShopLoadID)
      AND ShopID NOT IN (SELECT ShopID FROM @tmpShop)
      AND ShopID NOT IN (SELECT ShopID FROM @tmpShopLocation)
      AND EnabledFlag = 1
       
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpShop', 16, 1, @ProcName)
        RETURN
    END
    
    
    
       
    
    /*************************************************************************************
    *  Gather Meta Data
    **************************************************************************************/  
    -- Create temporary table to hold metadata information
    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all tables and store in the temporary table    
    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    
    SELECT  'ShopLoad',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  (Table_Name = 'utb_shop_load' AND Column_Name IN 
           ('Address1', 
            'Address2', 
            'AddressCity', 
            'AddressCounty', 
            'AddressState', 
            'AddressZip', 
            'Age', 
            'AlignmentFourWheel', 
            'AlignmentTwoWheel', 
            'ApprovedFlag', 
            'ASEFlag', 
            'BusinessRegsFlag', 
            'BusinessStabilityFlag', 
            'BusinessTypeCD', 
            'CaulkingSeamSealer', 
            'CertifiedFirstId', 
            'ChipGuard', 
            'CommercialLiabilityFlag', 
            'CompetentEstimatorsFlag', 
            'CoolantGreen', 
            'CoolantRed', 
            'CorrosionProtection', 
            'CountyTaxPct', 
            'CoverCar', 
            'CustomaryHoursFlag', 
            'DailyStorage',
            'DigitalPhotosFlag', 
            'DiscountDomestic', 
            'DiscountImport', 
            'DiscountPctSideBackGlass', 
            'DiscountPctWindshield', 
            'DrivingDirections', 
            'EFTFlag', 
            'ElectronicEstimatesFlag', 
            'EmailAddress', 
            'EmployeeEducationFlag', 
            'EmployerLiabilityFlag', 
            'FaxAreaCode', 
            'FaxExchangeNumber', 
            'FaxExtensionNumber', 
            'FaxUnitNumber', 
            'FedTaxId', 
            'FinalSubmitFlag', 
            'FlexAdditive', 
            'FrameAnchoringPullingEquipFlag', 
            'FrameDiagnosticEquipFlag', 
            'GlassOutsourceServiceFee', 
            'GlassReplacementChargeTypeCD', 
            'GlassSubletServiceFee', 
            'GlassSubletServicePct', 
            'HazardousWaste', 
            'HazMatFlag', 
            'HourlyRateMechanical', 
            'HourlyRateRefinishing', 
            'HourlyRateSheetMetal', 
            'HourlyRateUnibodyFrame', 
            'HydraulicLiftFlag', 
            'ICARFlag', 
            'MaxWeeklyAssignments', 
            'MechanicalRepairFlag', 
            'MergeActionBillingCD', 
            'MergeActionPersonnelCD', 
            'MergeActionShopCD', 
            'MergeActionShopLocationCD', 
            'MergeActionShopLocationHoursCD', 
            'MergeActionShopLocationOEMDiscountCD', 
            'MergeActionShopLocationPersonnelCD', 
            'MergeActionShopLocationPricingCD', 
            'MergeActionShopPersonnelCD', 
            'MergeDate', 
            'MergeUserID', 
            'MIGWeldersFlag', 
            'MinimumWarrantyFlag', 
            'MunicipalTaxPct', 
            'Name', 
            'NoFeloniesFlag', 
            'OEMDiscounts', 
            'OperatingFridayEndTime', 
            'OperatingFridayStartTime', 
            'OperatingMondayEndTime', 
            'OperatingMondayStartTime', 
            'OperatingSaturdayEndTime', 
            'OperatingSaturdayStartTime', 
            'OperatingSundayEndTime', 
            'OperatingSundayStartTime', 
            'OperatingThursdayEndTime', 
            'OperatingThursdayStartTime', 
            'OperatingTuesdayEndTime', 
            'OperatingTuesdayStartTime', 
            'OperatingWednesdayEndTime', 
            'OperatingWednesdayStartTime', 
            'OtherTaxPct', 
            'PartsRecycledMarkupPct', 
            'PhoneAreaCode', 
            'PhoneExchangeNumber', 
            'PhoneExtensionNumber', 
            'PhoneUnitNumber', 
            'PreferredCommunicationMethodID', 
            'PreferredEstimatePackageID', 
            'PullSetUp', 
            'PullSetUpMeasure', 
            'QualificationsSubmitedDate', 
            'R12EvacuateRecharge', 
            'R12EvacuateRechargeFreon', 
            'R134EvacuateRecharge', 
            'R134EvacuateRechargeFreon', 
            'ReceptionistFlag', 
            'RefinishCapabilityFlag', 
            'RefinishSingleStageHourly', 
            'RefinishSingleStageMax', 
            'RefinishThreeStageCCMaxHrs', 
            'RefinishThreeStageHourly', 
            'RefinishThreeStageMax', 
            'RefinishTrainedFlag', 
            'RefinishTwoStageCCMaxHrs', 
            'RefinishTwoStageHourly', 
            'RefinishTwoStageMax', 
            'RepairFacilityTypeCD', 
            'SalesTaxPct', 
            'SecureStorageFlag', 
            'ShopID', 
            'ShopLoadID', 
            'ShopLocationID', 
            'SiteFirstVisitDate', 
            'SLPCellAreaCode', 
            'SLPCellExchangeNumber', 
            'SLPCellExtensionNumber', 
            'SLPCellUnitNumber', 
            'SLPEmailAddress', 
            'SLPFaxAreaCode', 
            'SLPFaxExchangeNumber', 
            'SLPFaxExtensionNumber', 
            'SLPFaxUnitNumber', 
            'SLPGenderCD', 
            'SLPName', 
            'SLPPagerAreaCode', 
            'SLPPagerExchangeNumber', 
            'SLPPagerExtensionNumber', 
            'SLPPagerUnitNumber', 
            'SLPPersonnelID', 
            'SLPPersonnelTypeID', 
            'SLPPhoneAreaCode', 
            'SLPPhoneExchangeNumber', 
            'SLPPhoneExtensionNumber', 
            'SLPPhoneUnitNumber', 
            'SLPPreferredContactMethodID', 
            'SLPShopManagerFlag', 
            'SPCellAreaCode', 
            'SPCellExchangeNumber', 
            'SPCellExtensionNumber', 
            'SPCellUnitNumber', 
            'SPEmailAddress', 
            'SPFaxAreaCode', 
            'SPFaxExchangeNumber', 
            'SPFaxExtensionNumber', 
            'SPFaxUnitNumber', 
            'SPGenderCD', 
            'SPName', 
            'SPPagerAreaCode', 
            'SPPagerExchangeNumber', 
            'SPPagerExtensionNumber', 
            'SPPagerUnitNumber', 
            'SPPersonnelID', 
            'SPPersonnelTypeID', 
            'SPPhoneAreaCode', 
            'SPPhoneExchangeNumber', 
            'SPPhoneExtensionNumber', 
            'SPPhoneUnitNumber', 
            'SPPreferredContactMethodID', 
            'SPShopManagerFlag', 
            'StripePaintPerPanel', 
            'StripePaintPerSide', 
            'StripeTapePerPanel', 
            'StripeTapePerSide', 
            'SysLastUpdatedDate', 
            'TireMountBalance', 
            'TowInChargeTypeCD', 
            'TowInFlatFee', 
            'TransferFlag', 
            'Undercoat', 
            'ValidEPALicenseFlag', 
            'VANInetFlag', 
            'WarrantyPeriodRefinishCD', 
            'WarrantyPeriodWorkmanshipCD', 
            'WebSiteAddress', 
            'WorkersCompFlag',
            'ProgramFlag',
            'ReferralFlag'))
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    /*************************************************************************************
    *  Gather Reference Data
    **************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )

    -- States
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name)
        
    Select 'State' AS ListName, DisplayOrder, StateCode, StateValue
    From dbo.utb_state_code
    Where EnabledFlag = 1
    
    UNION ALL
    
    -- Estimate Packages
    Select 'EstimatePackage', DisplayOrder, CAST(EstimatePackageID AS Varchar), Name
    From dbo.utb_estimate_package
    Where EnabledFlag = 1
    
    UNION ALL
    
    -- Communication Methods
    Select 'CommunicationMethod', DisplayOrder, CAST(CommunicationMethodID AS Varchar), Name
    From dbo.utb_communication_method
    Where EnabledFlag = 1
    
    UNION ALL
    
    
    -- Repair Facility Types
    SELECT  'RepairFacilityType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'RepairFacilityTypeCD' ) 
    
    UNION ALL
    
    -- Warranty Periods
    SELECT 'WarrantyPeriod',
           NULL,
           Code,
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'WarrantyPeriodRefinishCD' )
    
    UNION ALL
    
    -- Business Types
    SELECT 'BusinessType',
           NULL,
           Code,
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'BusinessTypeCD' )
     
    
    UNION ALL
    
    SELECT  'ContactMethod' AS ListName,DisplayOrder, CAST(PersonnelContactMethodID AS Varchar), Name 
    FROM    dbo.utb_personnel_contact_method
    WHERE   EnabledFlag = 1
      AND   DisplayOrder IS NOT NULL
      
    UNION ALL
    
    SELECT  'GlassReplacementChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'GlassReplacementChargeTypeCD' ) 
      
    UNION ALL
    
    SELECT  'TowInChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'TowInChargeTypeCD' )  
    
    UNION ALL
    
    SELECT 'PersonnelType' AS ListName, DisplayOrder, CAST(PersonnelTypeID AS Varchar), Name
    FROM dbo.utb_personnel_type
    WHERE EnabledFlag = 1
      AND AppliesToCD = 'L'
      
    ORDER BY ListName, DisplayOrder
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
           

   -- Begin final select 
    SELECT
        1                                      AS Tag,
        Null                                   AS Parent,
        --Root
        @ShopLoadID                            AS [Root!1!ShopLoadID],
        
        --BusinessInfo
        Null                                   AS [ShopLoad!2!Address1],
        Null                                   AS [ShopLoad!2!Address2],
        Null                                   AS [ShopLoad!2!AddressCity],
        Null                                   AS [ShopLoad!2!AddressCounty],
        Null                                   AS [ShopLoad!2!AddressState],
        Null                                   AS [ShopLoad!2!AddressZip],
        Null                                   AS [ShopLoad!2!Age],
        Null                                   AS [ShopLoad!2!AlignmentFourWheel],
        Null                                   AS [ShopLoad!2!AlignmentTwoWheel],
        Null                                   AS [ShopLoad!2!ApprovedFlag],
        Null                                   AS [ShopLoad!2!ASEFlag],
        Null                                   AS [ShopLoad!2!BusinessRegsFlag],
        Null                                   AS [ShopLoad!2!BusinessStabilityFlag],
        Null                                   AS [ShopLoad!2!BusinessTypeCD],
        Null                                   AS [ShopLoad!2!CaulkingSeamSealer],
        Null                                   AS [ShopLoad!2!CertifiedFirstId],
        Null                                   AS [ShopLoad!2!ChipGuard],
        Null                                   AS [ShopLoad!2!CommercialLiabilityFlag],
        Null                                   AS [ShopLoad!2!CompetentEstimatorsFlag],
        Null                                   AS [ShopLoad!2!CoolantGreen],
        Null                                   AS [ShopLoad!2!CoolantRed],
        Null                                   AS [ShopLoad!2!CorrosionProtection],
        Null                                   AS [ShopLoad!2!CountyTaxPct],
        Null                                   AS [ShopLoad!2!CoverCar],
        Null                                   AS [ShopLoad!2!CustomaryHoursFlag],
        Null                                   AS [ShopLoad!2!DailyStorage],
        Null                                   AS [ShopLoad!2!DataEntryDate],
        Null                                   AS [ShopLoad!2!DigitalPhotosFlag],
        Null                                   AS [ShopLoad!2!DiscountDomestic],
        Null                                   AS [ShopLoad!2!DiscountImport],
        Null                                   AS [ShopLoad!2!DiscountPctSideBackGlass],
        Null                                   AS [ShopLoad!2!DiscountPctWindshield],
        Null                                   AS [ShopLoad!2!DrivingDirections],
        Null                                   AS [ShopLoad!2!EFTFlag],
        Null                                   AS [ShopLoad!2!ElectronicEstimatesFlag],
        Null                                   AS [ShopLoad!2!EmailAddress],
        Null                                   AS [ShopLoad!2!EmployeeEducationFlag],
        Null                                   AS [ShopLoad!2!EmployerLiabilityFlag],
        Null                                   AS [ShopLoad!2!FaxAreaCode],
        Null                                   AS [ShopLoad!2!FaxExchangeNumber],
        Null                                   AS [ShopLoad!2!FaxExtensionNumber],
        Null                                   AS [ShopLoad!2!FaxUnitNumber],
        Null                                   AS [ShopLoad!2!FedTaxId],
        Null                                   AS [ShopLoad!2!FinalSubmitDate],
        Null                                   AS [ShopLoad!2!FinalSubmitFlag],
        Null                                   AS [ShopLoad!2!FlexAdditive],
        Null                                   AS [ShopLoad!2!FrameAnchoringPullingEquipFlag],
        Null                                   AS [ShopLoad!2!FrameDiagnosticEquipFlag],
        Null                                   AS [ShopLoad!2!GlassOutsourceServiceFee],
        Null                                   AS [ShopLoad!2!GlassReplacementChargeTypeCD],
        Null                                   AS [ShopLoad!2!GlassSubletServiceFee],
        Null                                   AS [ShopLoad!2!GlassSubletServicePct],
        Null                                   AS [ShopLoad!2!HazardousWaste],
        Null                                   AS [ShopLoad!2!HazMatFlag],
        Null                                   AS [ShopLoad!2!HourlyRateMechanical],
        Null                                   AS [ShopLoad!2!HourlyRateRefinishing],
        Null                                   AS [ShopLoad!2!HourlyRateSheetMetal],
        Null                                   AS [ShopLoad!2!HourlyRateUnibodyFrame],
        Null                                   AS [ShopLoad!2!HydraulicLiftFlag],
        Null                                   AS [ShopLoad!2!ICARFlag],
        Null                                   AS [ShopLoad!2!MaxWeeklyAssignments],
        Null                                   AS [ShopLoad!2!MechanicalRepairFlag],
        Null                                   AS [ShopLoad!2!MergeActionBillingCD],
        Null                                   AS [ShopLoad!2!MergeActionPersonnelCD],
        Null                                   AS [ShopLoad!2!MergeActionShopCD],
        Null                                   AS [ShopLoad!2!MergeActionShopLocationCD],
        Null                                   AS [ShopLoad!2!MergeActionShopLocationHoursCD],
        Null                                   AS [ShopLoad!2!MergeActionShopLocationOEMDiscountCD],
        Null                                   AS [ShopLoad!2!MergeActionShopLocationPersonnelCD],
        Null                                   AS [ShopLoad!2!MergeActionShopLocationPricingCD],
        Null                                   AS [ShopLoad!2!MergeActionShopPersonnelCD],
        Null                                   AS [ShopLoad!2!MergeDate],
        Null                                   AS [ShopLoad!2!MergeUserID],
        Null                                   AS [ShopLoad!2!MIGWeldersFlag],
        Null                                   AS [ShopLoad!2!MinimumWarrantyFlag],
        Null                                   AS [ShopLoad!2!MunicipalTaxPct],
        Null                                   AS [ShopLoad!2!Name],
        Null                                   AS [ShopLoad!2!NoFeloniesFlag],
        Null                                   AS [ShopLoad!2!OEMDiscounts],
        Null                                   AS [ShopLoad!2!OperatingFridayEndTime],
        Null                                   AS [ShopLoad!2!OperatingFridayStartTime],
        Null                                   AS [ShopLoad!2!OperatingMondayEndTime],
        Null                                   AS [ShopLoad!2!OperatingMondayStartTime],
        Null                                   AS [ShopLoad!2!OperatingSaturdayEndTime],
        Null                                   AS [ShopLoad!2!OperatingSaturdayStartTime],
        Null                                   AS [ShopLoad!2!OperatingSundayEndTime],
        Null                                   AS [ShopLoad!2!OperatingSundayStartTime],
        Null                                   AS [ShopLoad!2!OperatingThursdayEndTime],
        Null                                   AS [ShopLoad!2!OperatingThursdayStartTime],
        Null                                   AS [ShopLoad!2!OperatingTuesdayEndTime],
        Null                                   AS [ShopLoad!2!OperatingTuesdayStartTime],
        Null                                   AS [ShopLoad!2!OperatingWednesdayEndTime],
        Null                                   AS [ShopLoad!2!OperatingWednesdayStartTime],
        Null                                   AS [ShopLoad!2!OtherTaxPct],
        Null                                   AS [ShopLoad!2!PartsRecycledMarkupPct],
        Null                                   AS [ShopLoad!2!PhoneAreaCode],
        Null                                   AS [ShopLoad!2!PhoneExchangeNumber],
        Null                                   AS [ShopLoad!2!PhoneExtensionNumber],
        Null                                   AS [ShopLoad!2!PhoneUnitNumber],
        Null                                   AS [ShopLoad!2!PreferredCommunicationMethodID],
        Null                                   AS [ShopLoad!2!PreferredEstimatePackageID],
        Null                                   AS [ShopLoad!2!PullSetUp],
        Null                                   AS [ShopLoad!2!PullSetUpMeasure],
        Null                                   AS [ShopLoad!2!QualificationsSubmitedDate],
        Null                                   AS [ShopLoad!2!R12EvacuateRecharge],
        Null                                   AS [ShopLoad!2!R12EvacuateRechargeFreon],
        Null                                   AS [ShopLoad!2!R134EvacuateRecharge],
        Null                                   AS [ShopLoad!2!R134EvacuateRechargeFreon],
        Null                                   AS [ShopLoad!2!ReceptionistFlag],
        Null                                   AS [ShopLoad!2!RefinishCapabilityFlag],
        Null                                   AS [ShopLoad!2!RefinishSingleStageHourly],
        Null                                   AS [ShopLoad!2!RefinishSingleStageMax],
        Null                                   AS [ShopLoad!2!RefinishThreeStageCCMaxHrs],
        Null                                   AS [ShopLoad!2!RefinishThreeStageHourly],
        Null                                   AS [ShopLoad!2!RefinishThreeStageMax],
        Null                                   AS [ShopLoad!2!RefinishTrainedFlag],
        Null                                   AS [ShopLoad!2!RefinishTwoStageCCMaxHrs],
        Null                                   AS [ShopLoad!2!RefinishTwoStageHourly],
        Null                                   AS [ShopLoad!2!RefinishTwoStageMax],
        Null                                   AS [ShopLoad!2!RepairFacilityTypeCD],
        Null                                   AS [ShopLoad!2!SalesTaxPct],
        Null                                   AS [ShopLoad!2!SecureStorageFlag],
        Null                                   AS [ShopLoad!2!ShopID],
        Null                                   AS [ShopLoad!2!ShopLoadID],
        Null                                   AS [ShopLoad!2!ShopLocationID],
        Null                                   AS [ShopLoad!2!SiteFirstVisitDate],
        Null                                   AS [ShopLoad!2!SLPCellAreaCode],
        Null                                   AS [ShopLoad!2!SLPCellExchangeNumber],
        Null                                   AS [ShopLoad!2!SLPCellExtensionNumber],
        Null                                   AS [ShopLoad!2!SLPCellUnitNumber],
        Null                                   AS [ShopLoad!2!SLPEmailAddress],
        Null                                   AS [ShopLoad!2!SLPFaxAreaCode],
        Null                                   AS [ShopLoad!2!SLPFaxExchangeNumber],
        Null                                   AS [ShopLoad!2!SLPFaxExtensionNumber],
        Null                                   AS [ShopLoad!2!SLPFaxUnitNumber],
        Null                                   AS [ShopLoad!2!SLPGenderCD],
        Null                                   AS [ShopLoad!2!SLPName],
        Null                                   AS [ShopLoad!2!SLPPagerAreaCode],
        Null                                   AS [ShopLoad!2!SLPPagerExchangeNumber],
        Null                                   AS [ShopLoad!2!SLPPagerExtensionNumber],
        Null                                   AS [ShopLoad!2!SLPPagerUnitNumber],
        Null                                   AS [ShopLoad!2!SLPPersonnelID],
        Null                                   AS [ShopLoad!2!SLPPersonnelTypeID],
        Null                                   AS [ShopLoad!2!SLPPhoneAreaCode],
        Null                                   AS [ShopLoad!2!SLPPhoneExchangeNumber],
        Null                                   AS [ShopLoad!2!SLPPhoneExtensionNumber],
        Null                                   AS [ShopLoad!2!SLPPhoneUnitNumber],
        Null                                   AS [ShopLoad!2!SLPPreferredContactMethodID],
        Null                                   AS [ShopLoad!2!SLPShopManagerFlag],
        Null                                   AS [ShopLoad!2!SPCellAreaCode],
        Null                                   AS [ShopLoad!2!SPCellExchangeNumber],
        Null                                   AS [ShopLoad!2!SPCellExtensionNumber],
        Null                                   AS [ShopLoad!2!SPCellUnitNumber],
        Null                                   AS [ShopLoad!2!SPEmailAddress],
        Null                                   AS [ShopLoad!2!SPFaxAreaCode],
        Null                                   AS [ShopLoad!2!SPFaxExchangeNumber],
        Null                                   AS [ShopLoad!2!SPFaxExtensionNumber],
        Null                                   AS [ShopLoad!2!SPFaxUnitNumber],
        Null                                   AS [ShopLoad!2!SPGenderCD],
        Null                                   AS [ShopLoad!2!SPName],
        Null                                   AS [ShopLoad!2!SPPagerAreaCode],
        Null                                   AS [ShopLoad!2!SPPagerExchangeNumber],
        Null                                   AS [ShopLoad!2!SPPagerExtensionNumber],
        Null                                   AS [ShopLoad!2!SPPagerUnitNumber],
        Null                                   AS [ShopLoad!2!SPPersonnelID],
        Null                                   AS [ShopLoad!2!SPPersonnelTypeID],
        Null                                   AS [ShopLoad!2!SPPhoneAreaCode],
        Null                                   AS [ShopLoad!2!SPPhoneExchangeNumber],
        Null                                   AS [ShopLoad!2!SPPhoneExtensionNumber],
        Null                                   AS [ShopLoad!2!SPPhoneUnitNumber],
        Null                                   AS [ShopLoad!2!SPPreferredContactMethodID],
        Null                                   AS [ShopLoad!2!SPShopManagerFlag],
        Null                                   AS [ShopLoad!2!StripePaintPerPanel],
        Null                                   AS [ShopLoad!2!StripePaintPerSide],
        Null                                   AS [ShopLoad!2!StripeTapePerPanel],
        Null                                   AS [ShopLoad!2!StripeTapePerSide],
        Null                                   AS [ShopLoad!2!TireMountBalance],
        Null                                   AS [ShopLoad!2!TowInChargeTypeCD],
        Null                                   AS [ShopLoad!2!TowInFlatFee],
        Null                                   AS [ShopLoad!2!TransferFlag],
        Null                                   AS [ShopLoad!2!Undercoat],
        Null                                   AS [ShopLoad!2!ValidEPALicenseFlag],
        Null                                   AS [ShopLoad!2!VANInetFlag],
        Null                                   AS [ShopLoad!2!WarrantyPeriodRefinishCD],
        Null                                   AS [ShopLoad!2!WarrantyPeriodWorkmanshipCD],
        Null                                   AS [ShopLoad!2!WebSiteAddress],
        Null                                   AS [ShopLoad!2!WorkersCompFlag], 
        Null                                   AS [ShopLoad!2!SLProgramFlag],		-- new flags for RRP - 4/5/2011 - WJC
        Null                                   AS [ShopLoad!2!SLReferralFlag],		--
        Null                                   AS [ShopLoad!2!SysLastUpdatedDate],
        
        -- Merge To Shop
        Null                                   AS [Shop!3!ShopID],
        Null                                   AS [Shop!3!Address1],
        Null                                   AS [Shop!3!Address2], 
        Null                                   AS [Shop!3!AddressCity],
        Null                                   AS [Shop!3!AddressState],
        Null                                   AS [Shop!3!AddressZip],
        Null                                   AS [Shop!3!Name],
        Null                                   AS [Shop!3!PhoneAreaCode],
        Null                                   AS [Shop!3!PhoneExchangeNumber],
        Null                                   AS [Shop!3!PhoneUnitNumber],
                
        -- Merge To ShopLocation 
        Null                                   AS [ShopLocation!4!ShopLocationID],
        Null                                   AS [ShopLocation!4!ShopID],
        Null                                   AS [ShopLocation!4!Address1],
        Null                                   AS [ShopLocation!4!Address2], 
        Null                                   AS [ShopLocation!4!AddressCity],
        Null                                   AS [ShopLocation!4!AddressState],
        Null                                   AS [ShopLocation!4!AddressZip],
        Null                                   AS [ShopLocation!4!CEIProgramFlag],
        Null                                   AS [ShopLocation!4!Name],
        Null                                   AS [ShopLocation!4!PhoneAreaCode],
        Null                                   AS [ShopLocation!4!PhoneExchangeNumber],
        Null                                   AS [ShopLocation!4!PhoneUnitNumber],
        Null                                   AS [ShopLocation!4!ProgramFlag],
                
        -- Metadata Header
        NULL                                   AS [Metadata!5!Entity],
        
        -- Metadata
        NULL                                   AS [Column!6!Name],
        NULL                                   AS [Column!6!DataType],
        NULL                                   AS [Column!6!MaxLength],
        NULL                                   AS [Column!6!Precision],
        NULL                                   AS [Column!6!Scale],
        NULL                                   AS [Column!6!Nullable],
        
        --Reference
        Null                                   AS [Reference!7!ListName],
        Null                                   AS [Reference!7!ReferenceID],
        Null                                   AS [Reference!7!Name]

   UNION ALL

    --ShopLoad Level
   SELECT
        2,
        1,
        --Root
        Null,
        
        --ShopLoad
        IsNull(Address1, ''),
        IsNull(Address2, ''),
        IsNull(AddressCity, ''),
        IsNull(AddressCounty, ''),
        IsNull(AddressState, ''),
        IsNull(AddressZip, ''),
        IsNull(Age, ''),
        IsNull(convert(varchar(20), AlignmentFourWheel), ''),
        IsNull(convert(varchar(20), AlignmentTwoWheel), ''),
        IsNull(ApprovedFlag, ''),
        IsNull(convert(varchar(5), ASEFlag), ''),
        IsNull(BusinessRegsFlag, ''),
        IsNull(BusinessStabilityFlag, ''),
        IsNull(BusinessTypeCD, ''),
        IsNull(convert(varchar(20), CaulkingSeamSealer), ''),
        IsNull(CertifiedFirstId, ''),
        IsNull(convert(varchar(20), ChipGuard), ''),
        IsNull(CommercialLiabilityFlag, ''),
        IsNull(CompetentEstimatorsFlag, ''),
        IsNull(convert(varchar(20), CoolantGreen), ''),
        IsNull(convert(varchar(20), CoolantRed), ''),
        IsNull(convert(varchar(20), CorrosionProtection), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), CountyTaxPct)), ''),
        IsNull(convert(varchar(20), CoverCar), ''),
        IsNull(CustomaryHoursFlag, ''),
        IsNull(convert(varchar(20), DailyStorage), ''),
        IsNull(DataEntryDate, ''),
        IsNull(DigitalPhotosFlag, ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountDomestic)), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountImport)), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountPctSideBackGlass)), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), DiscountPctWindshield)), ''),
        IsNull(DrivingDirections, ''),
        IsNull(EFTFlag, ''),
        IsNull(ElectronicEstimatesFlag, ''),
        IsNull(EmailAddress, ''),
        IsNull(EmployeeEducationFlag, ''),
        IsNull(EmployerLiabilityFlag, ''),
        IsNull(FaxAreaCode, ''),
        IsNull(FaxExchangeNumber, ''),
        IsNull(FaxExtensionNumber, ''),
        IsNull(FaxUnitNumber, ''),
        IsNull(dbo.ufnUtilitySquishString(FedTaxId, 0, 1, 0, null), ''),
        IsNull(FinalSubmitDate, ''),
        IsNull(FinalSubmitFlag, ''),
        IsNull(convert(varchar(20), FlexAdditive), ''),
        IsNull(FrameAnchoringPullingEquipFlag, ''),
        IsNull(FrameDiagnosticEquipFlag, ''),
        IsNull(convert(varchar(20), GlassOutsourceServiceFee), ''),
        IsNull(GlassReplacementChargeTypeCD, ''),
        IsNull(convert(varchar(20), GlassSubletServiceFee), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), GlassSubletServicePct)), ''),
        IsNull(convert(varchar(20), HazardousWaste), ''),
        IsNull(HazMatFlag, ''),
        IsNull(convert(varchar(20), HourlyRateMechanical), ''),
        IsNull(convert(varchar(20), HourlyRateRefinishing), ''),
        IsNull(convert(varchar(20), HourlyRateSheetMetal), ''),
        IsNull(convert(varchar(20), HourlyRateUnibodyFrame), ''),
        IsNull(convert(varchar(5), HydraulicLiftFlag), ''),
        IsNull(convert(varchar(5), ICARFlag), ''),
        IsNull(MaxWeeklyAssignments, ''),
        IsNull(MechanicalRepairFlag, ''),
        IsNull(MergeActionBillingCD, ''),
        IsNull(MergeActionPersonnelCD, ''),
        IsNull(MergeActionShopCD, ''),
        IsNull(MergeActionShopLocationCD, ''),
        IsNull(MergeActionShopLocationHoursCD, ''),
        IsNull(MergeActionShopLocationOEMDiscountCD, ''),
        IsNull(MergeActionShopLocationPersonnelCD, ''),
        IsNull(MergeActionShopLocationPricingCD, ''),
        IsNull(MergeActionShopPersonnelCD, ''),
        IsNull(MergeDate, ''),
        IsNull(MergeUserID, ''),
        IsNull(MIGWeldersFlag, ''),
        IsNull(MinimumWarrantyFlag, ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), MunicipalTaxPct)), ''),
        IsNull(Name, ''),
        IsNull(NoFeloniesFlag, ''),
        IsNull(OEMDiscounts, ''),
        CASE
          WHEN OperatingFridayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingFridayEndTime, '')
        END,
        CASE
          WHEN OperatingFridayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingFridayStartTime, '')
        END,
        CASE
          WHEN OperatingMondayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingMondayEndTime, '')
        END,
        CASE
          WHEN OperatingMondayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingMondayStartTime, '')
        END,
        CASE
          WHEN OperatingSaturdayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingSaturdayEndTime, '')
        END,
        CASE
          WHEN OperatingSaturdayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingSaturdayStartTime, '')
        END,
        CASE
          WHEN OperatingSundayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingSundayEndTime, '')
        END,
        CASE
          WHEN OperatingSundayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingSundayStartTime, '')
        END,
        CASE
          WHEN OperatingThursdayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingThursdayEndTime, '')
        END,
        CASE
          WHEN OperatingThursdayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingThursdayStartTime, '')
        END,
        CASE
          WHEN OperatingTuesdayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingTuesdayEndTime, '')
        END,
        CASE
          WHEN OperatingTuesdayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingTuesdayStartTime, '')
        END,
        CASE
          WHEN OperatingWednesdayEndTime = '0000' THEN ''
          ELSE IsNull(OperatingWednesdayEndTime, '')
        END,
        CASE
          WHEN OperatingWednesdayStartTime = '0000' THEN ''
          ELSE IsNull(OperatingWednesdayStartTime, '')
        END,
        IsNull(convert(varchar(20), convert(decimal(9, 1), OtherTaxPct)), ''),
        IsNull(convert(varchar(20), convert(decimal(9, 1), PartsRecycledMarkupPct)), ''),
        IsNull(PhoneAreaCode, ''),
        IsNull(PhoneExchangeNumber, ''),
        IsNull(PhoneExtensionNumber, ''),
        IsNull(PhoneUnitNumber, ''),
        IsNull(PreferredCommunicationMethodID, ''),
        IsNull(PreferredEstimatePackageID, ''),
        IsNull(convert(varchar(20), PullSetUp), ''),
        IsNull(convert(varchar(20), PullSetUpMeasure), ''),
        IsNull(QualificationsSubmitedDate, ''),
        IsNull(convert(varchar(20), R12EvacuateRecharge), ''),
        IsNull(convert(varchar(20), R12EvacuateRechargeFreon), ''),
        IsNull(convert(varchar(20), R134EvacuateRecharge), ''),
        IsNull(convert(varchar(20), R134EvacuateRechargeFreon), ''),
        IsNull(ReceptionistFlag, ''),
        IsNull(RefinishCapabilityFlag, ''),
        IsNull(convert(varchar(20), RefinishSingleStageHourly), ''),
        IsNull(convert(varchar(20), RefinishSingleStageMax), ''),
        IsNull(convert(varchar(20), RefinishThreeStageCCMaxHrs), ''),
        IsNull(convert(varchar(20), RefinishThreeStageHourly), ''),
        IsNull(convert(varchar(20), RefinishThreeStageMax), ''),
        IsNull(convert(varchar(5), RefinishTrainedFlag), ''),
        IsNull(convert(varchar(20), RefinishTwoStageCCMaxHrs), ''), 
        IsNull(convert(varchar(20), RefinishTwoStageHourly), ''),
        IsNull(convert(varchar(20), RefinishTwoStageMax), ''),
        IsNull(RepairFacilityTypeCD, ''),
        IsNull(convert(varchar(20), convert(decimal(9, 3), SalesTaxPct)), ''),
        IsNull(convert(varchar(5), SecureStorageFlag), ''),
        IsNull(ShopID, ''),
        IsNull(l.ShopLoadID, ''),
        IsNull(ShopLocationID, ''),
        IsNull(SiteFirstVisitDate, ''),
        IsNull(SLPCellAreaCode, ''),
        IsNull(SLPCellExchangeNumber, ''),
        IsNull(SLPCellExtensionNumber, ''),
        IsNull(SLPCellUnitNumber, ''),
        IsNull(SLPEmailAddress, ''),
        IsNull(SLPFaxAreaCode, ''),
        IsNull(SLPFaxExchangeNumber, ''),
        IsNull(SLPFaxExtensionNumber, ''),
        IsNull(SLPFaxUnitNumber, ''),
        IsNull(SLPGenderCD, ''),
        IsNull(SLPName, ''),
        IsNull(SLPPagerAreaCode, ''),
        IsNull(SLPPagerExchangeNumber, ''),
        IsNull(SLPPagerExtensionNumber, ''),
        IsNull(SLPPagerUnitNumber, ''),
        IsNull(SLPPersonnelID, ''),
        IsNull(SLPPersonnelTypeID, ''),
        IsNull(SLPPhoneAreaCode, ''),
        IsNull(SLPPhoneExchangeNumber, ''),
        IsNull(SLPPhoneExtensionNumber, ''),
        IsNull(SLPPhoneUnitNumber, ''),
        IsNull(SLPPreferredContactMethodID, ''),
        IsNull(SLPShopManagerFlag, ''),
        IsNull(SPCellAreaCode, ''),
        IsNull(SPCellExchangeNumber, ''),
        IsNull(SPCellExtensionNumber, ''),
        IsNull(SPCellUnitNumber, ''),
        IsNull(SPEmailAddress, ''),
        IsNull(SPFaxAreaCode, ''),
        IsNull(SPFaxExchangeNumber, ''),
        IsNull(SPFaxExtensionNumber, ''),
        IsNull(SPFaxUnitNumber, ''),
        IsNull(SPGenderCD, ''),
        IsNull(SPName, ''),
        IsNull(SPPagerAreaCode, ''),
        IsNull(SPPagerExchangeNumber, ''),
        IsNull(SPPagerExtensionNumber, ''),
        IsNull(SPPagerUnitNumber, ''),
        IsNull(SPPersonnelID, ''),
        IsNull(SPPersonnelTypeID, ''),
        IsNull(SPPhoneAreaCode, ''),
        IsNull(SPPhoneExchangeNumber, ''),
        IsNull(SPPhoneExtensionNumber, ''),
        IsNull(SPPhoneUnitNumber, ''),
        IsNull(SPPreferredContactMethodID, ''),
        IsNull(SPShopManagerFlag, ''),
        IsNull(convert(varchar(20), StripePaintPerPanel), ''),
        IsNull(convert(varchar(20), StripePaintPerSide), ''),
        IsNull(convert(varchar(20), StripeTapePerPanel), ''),
        IsNull(convert(varchar(20), StripeTapePerSide), ''),
        IsNull(convert(varchar(20), TireMountBalance), ''),
        IsNull(TowInChargeTypeCD, ''),
        IsNull(convert(varchar(20), TowInFlatFee), ''),
        IsNull(TransferFlag, ''),
        IsNull(convert(varchar(20), Undercoat), ''),
        IsNull(convert(varchar(5), ValidEPALicenseFlag), ''),
        IsNull(VANInetFlag, ''),
        IsNull(WarrantyPeriodRefinishCD, ''),
        IsNull(WarrantyPeriodWorkmanshipCD, ''),
        IsNull(WebSiteAddress, ''),
        IsNull(WorkersCompFlag, ''),
        IsNull(SLProgramFlag, '0'),			--New Flags for RRP - WJC
        IsNull(SLReferralFlag, '0'),		--
        dbo.ufnUtilityGetDateString(SysLastUpdatedDate),

        -- Business
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        
        -- Shop 
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        Null, Null, Null,

        --Metadata Header
        Null,

        --Metadata
        Null, Null, Null, Null, Null, Null, 

        --Reference
        Null, Null, Null
        
    FROM (SELECT @ShopLoadID AS ShopLoadID) AS parms
          LEFT JOIN dbo.utb_shop_load l ON parms.ShopLoadID = l.ShopLoadID
    
    UNION ALL

    --Business Level
    SELECT
        3,
        1,

        --Root
        Null,
        
        --ShopLoad
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,	
        NULL, NULL,	--SLProgramShop, SLReferralShop (new RRP flags)
        
        -- Shop
        IsNull(s.ShopID, ''),
        IsNull(Address1, ''),
        IsNull(Address2, ''),
        IsNull(AddressCity, ''),
        IsNull(AddressState, ''),
        IsNull(AddressZip, ''),
        IsNull(Name, ''),
        IsNull(PhoneAreaCode, ''), 
        IsNull(PhoneExchangeNumber, ''), 
        IsNull(PhoneUnitNumber, ''), 
        
        
        -- Shop Location
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        Null, Null, Null,		

        --Metadata Header
        Null,
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM    @tmpShop t LEFT JOIN dbo.utb_shop s ON t.ShopID = s.ShopID
    
    UNION ALL

    --Shop Level
    SELECT
        4,
        1,

        --Root
        Null,
        
        --ShopLoad
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL,	--SLProgramShop, SLReferralShop (new RRP flags)
        
        -- Shop
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        
        -- Shop Location
        IsNull(l.ShopLocationID, ''),
        IsNull(l.ShopID, ''),
        IsNull(Address1, ''),
        IsNull(Address2, ''), 
        IsNull(AddressCity, ''),
        IsNull(AddressState, ''),
        IsNull(AddressZip, ''),
        IsNull(CEIProgramFlag, ''),
        IsNull(Name, ''),
        IsNull(PhoneAreaCode, ''),
        IsNull(PhoneExchangeNumber, ''),
        IsNull(PhoneUnitNumber, ''),
        IsNull(ProgramFlag, ''),
       
        --Metadata Header
        Null,
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM    @tmpShopLocation t LEFT JOIN dbo.utb_shop_location l ON t.ShopLocationID = l.ShopLocationID
    
    UNION ALL


    --Metadata Header Level
    SELECT DISTINCT
        5,
        1,

        --Root
        Null,
        
        --ShopLoad
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,	--SLProgramShop, SLReferralShop (new RRP flags)
        
        -- Business
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        
        -- Shop 
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        Null, Null, Null,

        --Metadata Header
        GroupName,
            
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        Null, Null, Null

    FROM    @tmpMetadata

    UNION ALL

    --Metadata Level
    SELECT 
        6,
        5,

        --Root
        Null,
        
        --ShopLoad
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL,	--SLProgramShop, SLReferralShop (new RRP flags)
        
        -- Business
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        
        -- Shop 
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        Null, Null, Null,

        --Metadata Header
        GroupName,

        -- Metadata
        ColumnName,
        DataType,
        MaxLength,
        NumericPrecision,
        Scale,
        Nullable,

        --Reference
        Null, Null, Null

    FROM    @tmpMetadata

    UNION ALL

    --Reference Level
    SELECT
        7,
        1,

        --Root
        Null,
        
        --ShopLoad
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL,	--SLProgramShop, SLReferralShop (new RRP flags)
        
        -- Business
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        
        -- Shop 
        Null, Null, Null, Null, Null, Null,Null, Null, Null, Null,
        Null, Null, Null,

        --Metadata Header
        'ZZ-Reference',
           
        --Metadata
        Null, Null, Null, Null, Null, Null,

        --Reference
        ListName,
        ReferenceID,
        Name

    FROM    @tmpReference
        
    ORDER BY [Metadata!5!Entity], Tag 
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)        
           
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopLoadGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
