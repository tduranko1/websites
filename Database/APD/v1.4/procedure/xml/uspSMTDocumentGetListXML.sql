-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDocumentGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTDocumentGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopDocumentGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTDocumentGetListXML
(
    @ShopLocationID     bigint
)
AS
BEGIN
    DECLARE @ProcName                           varchar(30) 
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF
    
    
    SET @ProcName = 'uspSMTDocumentGetListXML'
    
    --Verify Parameter List
    IF  (@ShopLocationID IS NULL) OR
        (NOT EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID))
    BEGIN
        -- Invalid Shop Location ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN
    END
    
    -- Now grab the document data
    SELECT
        1                                       AS Tag,
        Null                                    AS Parent,
        --Root
        @ShopLocationID                         AS [Root!1!ShopLocationID],
        --Document
        Null                                    AS [Document!2!DocumentID],
        Null                                    AS [Document!2!Comments],
        Null                                    AS [Document!2!DocumentType],
        Null                                    AS [Document!2!CreatedDate],
        Null                                    AS [Document!2!EffectiveDate],
        Null                                    AS [Document!2!ExpirationDate],
        Null                                    AS [Document!2!ImageLocation],
        Null                                    AS [Document!2!ImageType],
        -- Reference
        Null                                    AS [Reference!3!ListName],
        Null                                    AS [Reference!3!DisplayOrder],
        Null                                    AS [Reference!3!ReferenceID],
        Null                                    AS [Reference!3!Value]
        
    UNION ALL
    
    SELECT
        2,
        1,
        -- Root
        NULL,
        -- Document
        d.DocumentID,
        d.Note,
        dt.Name,
        d.CreatedDate,
        convert(varchar, sld.EffectiveDate, 101),
        convert(varchar, sld.ExpirationDate, 101),
        d.ImageLocation,
        isNull(LOWER(d.ImageType), ''),
        -- Reference
        NULL, NULL, NULL, NULL
    FROM utb_shop_location_document sld 
    LEFT JOIN utb_document d ON (sld.DocumentID = d.DocumentID)
    LEFT JOIN utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
    WHERE sld.ShopLocationID = @ShopLocationID
      AND dt.DocumentClassCD = 'S'
      AND d.EnabledFlag = 1
      
    UNION ALL
    
    SELECT
        3,
        1,
        -- Root
        NULL,
        -- Document
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        -- Reference
        'DocumentType',
        DisplayOrder,
        DocumentTypeID,
        Name
    FROM dbo.utb_document_type
    WHERE DocumentClassCD = 'S'

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDocumentGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTDocumentGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/