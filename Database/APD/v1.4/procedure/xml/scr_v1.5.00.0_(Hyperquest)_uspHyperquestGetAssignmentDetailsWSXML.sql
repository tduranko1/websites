-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetAssignmentDetailsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetAssignmentDetailsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetAssignmentDetailsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns a assignment data from the utb_assignment table as XML
* DATE:			29Apr2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* VERSION:		1.0 - Initial Development
*				1.1 - 04Aug2015 - TVD - Added more HQ fields
*				1.2 - 22Oct2015 - TVD - Added to the WHERE clause to get only cancels.
*				1.3 - 10Aug2017 - TVD - Added code to allow ADP transactions to be closed in HQ also
*				
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetAssignmentDetailsWSXML]
	@iClaimAspectServiceChannelID BIGINT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspHyperquestGetAssignmentDetailsWSXML'
    
    SET @ApplicationCD = 'HyperquestAssignment'

    -- Validation

    -- Create Temp table to provide ORDER BY
    
	SELECT TOP 1
		a.AssignmentID
		, hj.ShopLocationID
		, a.AssignmentDate
		, a.CancellationDate
		, a.AssignmentSequenceNumber
		, hj.JobID
		, hj.LynxID
		, hj.DocumentTypeID
		, hj.JobStatus
		, hj.VehicleID
		, hj.SeqID
		, hj.SysLastUpdatedDate
	INTO #HQJob
	FROM 
		utb_assignment a
		INNER JOIN utb_hyperquestsvc_jobs hj
		ON hj.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID 
	WHERE 
		a.ClaimAspectServiceChannelID = @iClaimAspectServiceChannelID   
		AND hj.EnabledFlag = 1
		AND a.CommunicationMethodID IN (2,17)  -- 2 = ADPShoplink, 17 = HQLink
		AND hj.DocumentTypeID = 81
		AND a.CancellationDate IS NOT NULL
	ORDER BY 
		--a.AssignmentID DESC
		hj.syslastupdatedDate DESC
    
    -- Begin main code
	SELECT
		1 AS Tag
		, NULL AS Parent
		, @iClaimAspectServiceChannelID AS [Root!1!ClaimAspectServiceChannelID]
		, NULL AS [Assignment!2!AssignmentID]
		, NULL AS [Assignment!2!ShopLocationID]
		, NULL AS [Assignment!2!AssignmentDate]
		, NULL AS [Assignment!2!CancellationDate]
		, NULL AS [Assignment!2!AssignmentSequenceNumber]
		, NULL AS [Assignment!2!JobID]
		, NULL AS [Assignment!2!LynxID]
		, NULL AS [Assignment!2!DocumentTypeID]
		, NULL AS [Assignment!2!JobStatus]
		, NULL AS [Assignment!2!VehicleID]
		, NULL AS [Assignment!2!SeqID]
		, NULL AS [Assignment!2!SysLastUpdatedDate]
	FROM 
		#HQJob 

	UNION ALL

	SELECT
		2 AS Tag
		, 1 AS Parent
		, NULL
		, AssignmentID
		, ShopLocationID
		, AssignmentDate
		, CancellationDate
		, AssignmentSequenceNumber
		, JobID
		, LynxID
		, DocumentTypeID
		, JobStatus
		, VehicleID
		, SeqID
		, SysLastUpdatedDate
	FROM 
		#HQJob 

	FOR XML EXPLICIT	

DROP TABLE #HQJob

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetAssignmentDetailsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetAssignmentDetailsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/