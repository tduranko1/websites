-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleDeactivatedServiceChannelGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspVehicleDeactivatedServiceChannelGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspVehicleDeactivatedServiceChannelGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a list of deactivate service channel for a claim (LynxID)
*
* PARAMETERS:
* (I) @LynxID               LynxID of the claim to operate on  
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

create procedure dbo.uspVehicleDeactivatedServiceChannelGetListXML
  @LynxID                  udt_std_id_big,
  @InsuranceCompanyID      udt_std_id
as
begin
  
  declare @now                          udt_std_datetime
  declare @ProcName                     varchar(50)
  DECLARE @InsuranceCompanyIDClaim      udt_std_id
  
  set @ProcName = 'uspVehicleDeactivatedServiceChannelGetListXML'
  set @now = getdate()
  

  -- Validate the Lynx id
  if (@LynxID IS NULL) OR
     (not exists (select LynxID
                  from dbo.utb_claim
                  where LynxID = @LynxID))
  begin
    raiserror('101|%s|@LynxID passed is null or invalid',16,1,@ProcName)
    return
  end 
  
  SELECT @InsuranceCompanyIDClaim = InsuranceCompanyID
  FROM dbo.utb_claim
  WHERE LynxID = @LynxID
  
  
  -- Validate the Insurance Company for the passed Lynx ID
  IF @InsuranceCompanyIDClaim <> @InsuranceCompanyID
  BEGIN
    raiserror('101|%s|@LynxID %u does not belong to the passed @InsuranceCompanyID %u passed is null or invalid',16,1,@ProcName, @LynxID, @InsuranceCompanyID)
    return
  END
  
    -- Create XML Document to return
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Vehicle Level
            NULL AS [Vehicle!2!ClaimAspectID],
            NULL AS [Vehicle!2!EntityName],
            NULL AS [Vehicle!2!ClaimAspectNumber],
            -- Deactivated service channels
            NULL AS [InactiveServiceChannel!3!ClaimAspectServiceChannelID],
            NULL AS [InactiveServiceChannel!3!ClaimAspectID],
            NULL AS [InactiveServiceChannel!3!ServiceChannelCD],
            NULL AS [InactiveServiceChannel!3!ServiceChannelName],
            NULL AS [InactiveServiceChannel!3!PrimaryFlag]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Vehicle Level
            ca.ClaimAspectID,
            cat.Name,
            ca.ClaimAspectNumber,
            -- Deactivate service channel
            NULL, NULL, NULL, NULL, NULL
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
    WHERE LynxID = @LynxID
      AND cat.Name in ('Vehicle', 'Property')
    
    
    UNION ALL
    
    SELECT 3,
           2,
           NULL,
           -- Vehicle Level
           ca.ClaimAspectID,
           NULL, NULL,
           -- Deactivated service channels
           casc.ClaimAspectServiceChannelID,
           casc.ClaimAspectID,
           casc.ServiceChannelCD,
           ref.Name,
           casc.PrimaryFlag
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID)
    LEFT JOIN dbo.utb_claim_aspect_status cas ON ca.ClaimAspectID = cas.ClaimAspectID AND casc.ServiceChannelCD = cas.ServiceChannelCD
    LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') ref ON (casc.ServiceChannelCD = ref.Code)
    WHERE ca.LynxID = @LynxID
      AND cas.StatusTypeCD = 'SC'
      AND s.Name in ('Cancelled', 'Complete')


    ORDER BY [Vehicle!2!ClaimAspectID], tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


end                              
-- permissions
go 

if exists(select name from sysobjects
          where name = 'uspVehicleDeactivatedServiceChannelGetListXML' and type='P')
begin
  grant execute on dbo.uspVehicleDeactivatedServiceChannelGetListXML to ugr_lynxapd
  commit
end
else
begin
  raiserror ('Stored proc creation failure.',16,1)
  rollback
end

go            
                                  
                              
                 
  
 
  
  
  
  
  
  
  
  
  
  
  
      
         
  
  
 

    



  
