-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInsuranceGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopInsuranceGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopInsuranceGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns list of insurance companyies with an indication of whether the indicated shop is on each
*               company's inclusion or exclusion list
*
* PARAMETERS:  
* (I) @ShopID   The shop (location) for which inclusion/exclusion indicators will be retrieved.
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopInsuranceGetListXML
    @ShopID udt_std_id_big
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now               AS DateTime          -- Timestamp for Procedure

    SET @ProcName = 'uspSMTShopInsuranceGetListXML'
    SET @Now = Current_TimeStamp

    --Verify that a valid Insurance Company was passed in.
    IF (@ShopID is Null) OR
    NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID)
    BEGIN
        -- Invalid Insurance CompanyID
    
        RAISERROR  ('101|%s|@ShopID|%n', 16, 1, @ProcName, @ShopID)
        RETURN
    END
        

    --Start Select. 
    SELECT
        1                   AS tag,
        Null                AS Parent,
        @ShopID             AS [Root!1!ShopID],
        --Insurance
        Null                AS [Insurance!2!InsuranceCompanyID],
        Null                AS [Insurance!2!Name],
        Null                AS [Insurance!2!Address1],
        Null                AS [Insurance!2!Address2],
        Null                AS [Insurance!2!AddressCity],
        Null                AS [Insurance!2!AddressState],
        Null                AS [Insurance!2!ExcludeFlag],
        Null                AS [Insurance!2!IncludeFlag]
        
    UNION ALL

    SELECT
        2,
        1,
        Null,
        --Exclusion
        i.InsuranceCompanyID,
        IsNull(i.Name, ''),
        IsNull(i.Address1, ''),
        IsNull(i.Address2, ''),
        IsNull(i.AddressCity, ''),
        IsNull(i.AddressState, ''),
        ISNULL(csl.ExcludeFlag, 0),
        ISNULL(csl.IncludeFlag, 0)                

    FROM dbo.utb_insurance i LEFT JOIN dbo.utb_client_shop_location csl ON i.InsuranceCompanyID = csl.InsuranceCompanyID
                                                                       AND @ShopID = csl.ShopLocationID
    
            
    ORDER BY Tag, [Insurance!2!Name]

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInsuranceGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopInsuranceGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/