-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFindLienHolderByZipPhone' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFindLienHolderByZipPhone 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFindLienHolderByZipPhone
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL details
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID                        ClaimAspectServiceChannelID
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFindLienHolderByZipPhone
    @ZipCode               varchar(5),
    @PhoneAreaCode         varchar(3),
    @PhoneExchangeNumber   varchar(3),
    @PhoneUnitNumber       varchar(4)
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    DECLARE @LienHolderID as bigint
    

    SET @ProcName = 'uspFindLienHolderByZipPhone'
    SET @now = CURRENT_TIMESTAMP

    SELECT
        1 as Tag,
        Null as Parent,
        --  Root
        @ZipCode              AS [Root!1!ZipCode],
        @PhoneAreaCode        AS [Root!1!PhoneAreaCode],
        @PhoneExchangeNumber  AS [Root!1!PhoneExchangeNumber],
        @PhoneUnitNumber      AS [Root!1!PhoneUnitNumber],

        --Lien Holder
        NULL AS [LienHolder!2!LienHolderID],
        NULL AS [LienHolder!2!ParentLienHolderID],
        NULL AS [LienHolder!2!Name],
        NULL AS [LienHolder!2!Address1],
        NULL AS [LienHolder!2!Address2],
        NULL AS [LienHolder!2!AddressCity],
        NULL AS [LienHolder!2!AddressState],
        NULL AS [LienHolder!2!AddressZip],
        NULL AS [LienHolder!2!Phone],
        NULL AS [LienHolder!2!Fax],
        NULL AS [LienHolder!2!OfficeName],
        NULL AS [LienHolder!2!ContactName],
        NULL AS [LienHolder!2!EmailAddress],
        NULL AS [LienHolder!2!FedTaxID],
        NULL AS [LienHolder!2!BusinessTypeCD],
        NULL AS [LienHolder!2!EFTAccountNumber],
        NULL AS [LienHolder!2!EFTAccountTypeCD],
        NULL AS [LienHolder!2!EFTContractSignedFlag],
        NULL AS [LienHolder!2!EFTEffectiveDate],
        NULL AS [LienHolder!2!EFTRoutingNumber],
        NULL AS [LienHolder!2!BillingName],
        NULL AS [LienHolder!2!BillingAddress1],
        NULL AS [LienHolder!2!BillingAddress2],
        NULL AS [LienHolder!2!BillingAddressCity],
        NULL AS [LienHolder!2!BillingAddressState],
        NULL AS [LienHolder!2!BillingAddressZip],
        NULL AS [LienHolder!2!BillingPhone],
        NULL AS [LienHolder!2!BillingFax],
        NULL AS [LienHolder!2!ParentLienHolderIDFound]
        
    UNION ALL
    
    SELECT  2,
            1,
            -- Root
            NULL, NULL, NULL, NULL,
            
            --Lien Holder
            lh.LienHolderID,
            isNull(convert(varchar, lh.ParentLienHolderID), ''),
            lh.Name, 
            isNull(lh.Address1,  ''),
            isNull(lh.Address2,  ''),
            isNull(lh.AddressCity,  ''),
            isNull(lh.AddressState,  ''),
            isNull(lh.AddressZip,  ''),
            isNull(lh.PhoneAreaCode + lh.PhoneExchangeNumber + lh.PhoneUnitNumber, ''),
            isNull(lh.FaxAreaCode + lh.FaxExchangeNumber + lh.FaxUnitNumber, ''),
            isNull(lh.OfficeName, ''),
            isNull(lh.ContactName, ''),
            isNull(lh.EmailAddress, ''),
            isNull(lh.FedTaxId, ''),
            isNull(lh.BusinessTypeCD, ''),
            isNull(b.EFTAccountNumber, ''),
            isNull(b.EFTAccountTypeCD, ''),
            isNull(b.EFTContractSignedFlag,0),
            isNull(convert(varchar, b.EFTEffectiveDate, 101), ''),
            isNull(b.EFTRoutingNumber, ''),
            isNull(b.Name, ''),
            isNull(b.Address1, ''),
            isNull(b.Address2, ''),
            isNull(b.AddressCity, ''),
            isNull(b.AddressState, ''),
            isNull(b.AddressZip, ''),
            isNull(b.PhoneAreaCode + b.PhoneExchangeNumber + b.PhoneUnitNumber, ''),
            isNull(b.FaxAreaCode + b.FaxExchangeNumber + b.FaxUnitNumber, ''),
            isNull((SELECT convert(varchar, LienHolderID)
                      FROM dbo.utb_lien_holder lhp
                      WHERE lhp.FedTaxId = lh.FedTaxId
                        AND ParentLienHolderID IS NULL
                        AND lhp.LienHolderID <> lh.LienHolderID), '')
            
    FROM dbo.utb_lien_holder lh
    LEFT OUTER JOIN dbo.utb_billing b on lh.BillingID = b.BillingID
    WHERE lh.AddressZip = @ZipCode
      AND lh.PhoneAreaCode = @PhoneAreaCode
      AND lh.PhoneExchangeNumber = @PhoneExchangeNumber
      AND lh.PhoneUnitNumber = @PhoneUnitNumber

    
    ORDER BY TAG

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFindLienHolderByZipPhone' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFindLienHolderByZipPhone TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/