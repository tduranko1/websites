-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimHistoryGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimHistoryGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspClaimHistoryGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves claim history
*
* PARAMETERS:  
* (I) @LynxID               LynxID
* (I) @InsuranceCompanyID   The Insurance company to validate the claim against. 
*
* RESULT SET:
*   Universal recordset containg claim history information
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClaimHistoryGetDetailWSXML]
    @LynxID                 udt_std_id_big,
    @InsuranceCompanyID     udt_std_id,
    @ApplicationCD          udt_std_cd='APD'
AS
BEGIN
    -- Declare local variables

    DECLARE @tmpHistory TABLE
    (
        LogID               bigint      NOT NULL,
        ClaimAspectTypeID   int         NOT NULL,
        ClaimAspectNumber   int         NOT NULL,
        PertainsTo          varchar(50) NULL
    )


    DECLARE @InsuranceCompanyIDClaim    udt_std_id

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimHistoryGetDetailWSXML'


    -- Check to make sure a valid Lynx ID was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Get the Insurance Company Id for the claim
    SELECT  @InsuranceCompanyIDClaim = InsuranceCompanyID 
      FROM  dbo.utb_claim 
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


    -- Select the claim aspect and claim aspect numbers for all pertinent log records into a temporary table.  This will be used
    -- to generate the "Pertains To" string.  We have to do this because SQL has a bug which causes an Access Violation if we
    -- try to run the function which generates PertainsTo as a correlated part of the query.  We'll then join this data back in
    -- when we do the XML Select.

    INSERT INTO @tmpHistory (LogID, ClaimAspectTypeID, ClaimAspectNumber)
      SELECT  LogID,
              ClaimAspectTypeID,
              ClaimAspectNumber
        FROM  dbo.utb_history_log
        WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update Pertains To

    UPDATE @tmpHistory
      SET  PertainsTo = dbo.ufnUtilityGetPertainsTo(tmpH.ClaimAspectTypeID, tmpH.ClaimAspectNumber, 1)
      FROM @tmpHistory tmpH

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            @LynxID AS [Root!1!LynxID],
            @InsuranceCompanyID as [Root!1!InsuranceCompanyID],
            -- History Level
            NULL AS [History!2!LogID],
            NULL AS [History!2!CompletedDate],
            NULL AS [History!2!CompletedLogonId],
            NULL AS [History!2!CompletedUserNameFirst],
            NULL AS [History!2!CompletedUserNameLast],
            NULL AS [History!2!CompletedRoleName],
            NULL AS [History!2!CreatedDate],
            NULL AS [History!2!CreatedLogonId],
            NULL AS [History!2!CreatedUserNameFirst],
            NULL AS [History!2!CreatedUserNameLast],
            NULL AS [History!2!CreatedRoleName],
            NULL AS [History!2!Description],
            NULL AS [History!2!EntryType],
            NULL AS [History!2!EventName],
            NULL AS [History!2!PertainsTo],
            NULL AS [History!2!TaskName],
            NULL AS [History!2!StatusName],
            NULL AS [History!2!ServiceChannelCD], --Project:210474 APD Added to support multiple concurrent service channels enhancements  M.A.20070105
            NULL AS [History!2!ServiceChannelCDName] --Project:210474 APD Added to support multiple concurrent service channels enhancements  M.A.20070105


    UNION ALL


    SELECT  2,
            1,
            NULL, NULL,
            -- History Level
            hl.LogID,
            IsNull(hl.CompletedDate, ''),
            IsNull(coua.LogonId, ''),
            IsNull(cou.NameFirst, ''),
            IsNull(cou.NameLast, ''),
            IsNull(cor.Name, ''),
            IsNull(hl.CreatedDate, ''),
            IsNull(crua.LogonId, ''),
            IsNull(cru.NameFirst, ''),
            IsNull(cru.NameLast, ''),
            IsNull(crr.Name, ''),
            CASE 
                WHEN hl.LogType = 'E' THEN IsNull(hl.Description, '')
                WHEN hl.LogType = 'S' THEN IsNull(hl.Description, '')
                WHEN hl.LogType = 'T' THEN 'Task ' +
                        CASE 
                            WHEN LTRIM(RTRIM(hl.Description)) IS NOT NULL AND LTRIM(RTRIM(hl.Description)) <> '[NULL]' THEN '(Description: ' + IsNull(LTRIM(RTRIM(hl.Description)), '') + ') '
                            ELSE ''
                        END +
                        CASE
                            WHEN hl.NotApplicableFlag = 0 THEN 'Completed ' 
                            ELSE 'Marked N/A '
                        END +
                        CASE
                            WHEN (LTRIM(RTRIM(hl.ReferenceID)) IS NOT NULL) AND
                                 ((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID) IS NOT NULL) AND
                                 ((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID) <> '[NULL]') 
                                        THEN 'with Note: ' + IsNull((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID), '')
                            ELSE ''
                        END
                ELSE ''
            END,
            CASE hl.LogType
                WHEN 'E' THEN 'Event'
                WHEN 'T' THEN 'Task'
                WHEN 'S' THEN 'Status'
                ELSE ''
            END,
            IsNull(e.Name, ''),
            tmpH.PertainsTo,
            IsNull(t.Name, ''),
            IsNull(s.Name, ''),
            isnull(vt.ServiceChannelCD,''), --Project:210474 APD Added to support multiple concurrent service channels enhancements  M.A.20070105
            isnull(vt.ServiceChannelCDName,'') --Project:210474 APD Added to support multiple concurrent service channels enhancements  M.A.20070105
    FROM (SELECT @LynxID AS LynxID, @ApplicationCD AS ApplicationCD) AS parms
    LEFT JOIN dbo.utb_history_log hl ON (parms.LynxID = hl.LynxID)
    LEFT JOIN @tmpHistory tmpH ON (hl.LogID = tmpH.LogID)
    LEFT JOIN dbo.utb_user cou ON (hl.CompletedUserID = cou.UserID)
    LEFT JOIN dbo.utb_application a ON (parms.ApplicationCD = a.Code)
    LEFT JOIN dbo.utb_user_application coua ON (cou.UserID = coua.UserID AND a.ApplicationID = coua.ApplicationID)
    LEFT JOIN dbo.utb_role cor ON (hl.CompletedRoleID = cor.RoleID)
    LEFT JOIN dbo.utb_user cru ON (hl.CreatedUserID = cru.UserID)
    LEFT JOIN dbo.utb_user_application crua ON (cru.UserID = crua.UserID AND a.ApplicationID = crua.ApplicationID)
    LEFT JOIN dbo.utb_role crr ON (hl.CreatedRoleID = crr.RoleID)
    LEFT JOIN dbo.utb_event e ON (hl.EventID = e.EventID)
    LEFT JOIN dbo.utb_task t ON (hl.TaskID = t.TaskID)
    LEFT JOIN dbo.utb_status s ON (hl.StatusID = s.StatusID)
    --Project:210474 APD Added the following join to support multiple concurrent service channels enhancements  M.A.20070105
    LEFT OUTER JOIN (
                    Select      casc.ClaimAspectServiceChannelID,
                                fn.Code as 'ServiceChannelCD',
                                fn.Name as 'ServiceChannelCDName'
                    from        utb_Claim_Aspect_Service_Channel casc
                    inner join  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') fn
                    on          fn.Code = casc.ServiceChannelCD
                    ) vt
    on  vt.ClaimAspectServiceChannelID = hl.ClaimAspectServiceChannelID
    --Project:210474 APD Added the above join to support multiple concurrent service channels enhancements  M.A.20070105
    ORDER BY Tag, [History!2!CompletedDate] DESC
    FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimHistoryGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimHistoryGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspClaimHistoryGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/