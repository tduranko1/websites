-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetAssignmentsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetAssignmentsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceGetAssignmentsWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Get a list of unprocessed Choice Shop Assignments from the database.
* Date:			16Nov2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	16Nov2015 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceGetAssignmentsWSXML]
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @iChoiceAssignmentsWaiting			INT
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceGetAssignmentsWSXML'
	SET @Now = CURRENT_TIMESTAMP
	SET @iChoiceAssignmentsWaiting = 0

	---------------------------------------
	-- Validations
	---------------------------------------
	-- NONE
/*
    IF @Debug = 1
    BEGIN
        PRINT 'Passed in Parameters:'
		PRINT '    @NewShopFlag = ' + CONVERT(VARCHAR(1), @NewShopFlag)
		PRINT '    @HQBusinessTypeCD = ' + @HQBusinessTypeCD
		PRINT '    @HQAddress1 = ' + @HQAddress1
		PRINT '    @HQAddress2 = ' + @HQAddress2
		PRINT '    @HQAddressCity = ' + @HQAddressCity
		PRINT '    @HQAddressState = ' + @HQAddressState
		PRINT '    @HQAddressZip = ' + CONVERT(VARCHAR(12), @HQAddressZip)
		PRINT '    @HQEmailAddress = ' + @HQEmailAddress
		PRINT '    @HQEnabledFlag = ' + CONVERT(VARCHAR(1), @HQEnabledFlag)
		PRINT '    @HQFaxAreaCode = ' + CONVERT(VARCHAR(3), @HQFaxAreaCode)
		PRINT '    @HQFaxExchangeNumber = ' + CONVERT(VARCHAR(3), @HQFaxExchangeNumber)
		PRINT '    @HQFaxUnitNumber = ' + CONVERT(VARCHAR(4), @HQFaxUnitNumber)
		PRINT '    @HQFedTaxId = ' + CONVERT(VARCHAR(15), @HQFedTaxId)
		PRINT '    @HQName = ' + @HQName
		PRINT '    @HQPhoneAreaCode = ' + CONVERT(VARCHAR(3), @HQPhoneAreaCode)
		PRINT '    @HQPhoneExchangeNumber = ' + CONVERT(VARCHAR(2), @HQPhoneExchangeNumber)
		PRINT '    @HQPhoneUnitNumber = ' + CONVERT(VARCHAR(4), @HQPhoneUnitNumber)
		PRINT '    @HQSysLastUserID = ' + CONVERT(VARCHAR(10), @HQSysLastUserID)
		PRINT '    @HQApplicationCD = ' + @HQApplicationCD
		PRINT '    @HQShopPartnerID = ' + @HQShopPartnerID
    END
*/
	---------------------------------------
	-- Tests
	---------------------------------------
	
	---------------------------------------------
	-- Main Code - Check if new assignment exist
	---------------------------------------------
	IF EXISTS (
		SELECT TOP 1
			a.AssignmentID
		FROM
			utb_claim c
			INNER JOIN utb_claim_aspect ca
				ON ca.LynxID = c.LynxID
			INNER JOIN utb_claim_aspect_service_channel casc
				ON casc.ClaimAspectID = ca.ClaimAspectID
			INNER JOIN utb_assignment a
				ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID		
			INNER JOIN utb_shop_location sl
				ON sl.ShopLocationID = a.ShopLocationID
			INNER JOIN utb_application ap
				ON ap.ApplicationID = ca.SourceApplicationID
		WHERE 
			casc.ServiceChannelCD = 'CS'
			--AND sl.PPGCTSId LIKE REPLACE('00000000-0000-0000-0000-000000000000', '0', '[0-9a-fA-F]') -- Remove to all new shops
			AND a.AssignmentSuffix IS NULL
			AND a.CancellationDate IS NULL
			AND a.AssignmentDate IS NULL
		ORDER BY
			c.SysLastUpdatedDate DESC
	)
	BEGIN
		------------------------------------------------------
		-- Choice Assignment waiting to be processed.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Choice Assignment are waiting to be processed.'
		END
		
		------------------------------------------------------
		-- Create assignment list XML for return
		------------------------------------------------------
		SELECT
			1 AS Tag
			, NULL AS Parent
			, NULL				AS [Root!1!Root]
			, NULL				AS [Assignment!2!InsuranceCompanyID]   
			, NULL				AS [Assignment!2!ClientClaimNumber]   
			, NULL				AS [Assignment!2!ApplicationSource]   
			, NULL				AS [Assignment!2!AssignmentID]   
			, NULL				AS [Assignment!2!ClaimAspectServiceChannelID]
			, NULL				AS [Assignment!2!CommunicationMethodID]
			, NULL				AS [Assignment!2!ShopLocationID]   
			, NULL				AS [Assignment!2!AssignmentDate]   
			, NULL				AS [Assignment!2!AssignmentReceivedDate]   
			, NULL				AS [Assignment!2!AssignmentRemarks]   
			, NULL				AS [Assignment!2!AssignmentSequenceNumber]   
			, NULL				AS [Assignment!2!AssignmentSuffix]   
			, NULL				AS [Assignment!2!CancellationDate]   
			, NULL				AS [Assignment!2!CommunicationAddress]   
			, NULL				AS [Assignment!2!EffectiveDeductibleSentAmt]   
			, NULL				AS [Assignment!2!ProgramTypeCD]   
			, NULL				AS [Assignment!2!SelectionDate]   
			, NULL				AS [Assignment!2!SysLastUserID]   
			, NULL				AS [Assignment!2!SysLastUpdatedDate]   

		UNION ALL

		SELECT  2 AS Tag,  
				1 AS Parent
				, NULL
				, CONVERT(VARCHAR(25), c.InsuranceCompanyID)
				, ISNULL(c.ClientClaimNumber,'')
				, ap.Name
				, CONVERT(VARCHAR(25), a.AssignmentID)
				, CONVERT(VARCHAR(25), a.ClaimAspectServiceChannelID)
				, CONVERT(VARCHAR(10), a.CommunicationMethodID)		 
				, CONVERT(VARCHAR(10), a.ShopLocationID)             
				, CONVERT(VARCHAR(26), a.AssignmentDate,121)         
				, CONVERT(VARCHAR(26), a.AssignmentReceivedDate,121) 
				, a.AssignmentRemarks								 
				, CONVERT(VARCHAR(10), a.AssignmentSequenceNumber)   
				, a.AssignmentSuffix								 
				, CONVERT(VARCHAR(26), a.CancellationDate,121)       
				, a.CommunicationAddress							 
				, CONVERT(VARCHAR(10), a.EffectiveDeductibleSentAmt) 
				, a.ProgramTypeCD									 
				, CONVERT(VARCHAR(26), a.SelectionDate,121)          
				, CONVERT(VARCHAR(10), a.SysLastUserID)              
				, CONVERT(VARCHAR(26), a.SysLastUpdatedDate,121)     
		FROM
				utb_claim c
				INNER JOIN utb_claim_aspect ca
					ON ca.LynxID = c.LynxID
				INNER JOIN utb_claim_aspect_service_channel casc
					ON casc.ClaimAspectID = ca.ClaimAspectID
				INNER JOIN utb_assignment a
					ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID		
				INNER JOIN utb_shop_location sl
					ON sl.ShopLocationID = a.ShopLocationID
				INNER JOIN utb_application ap
					ON ap.ApplicationID = ca.SourceApplicationID
		WHERE 
			casc.ServiceChannelCD = 'CS'
			-- AND sl.PPGCTSId LIKE REPLACE('00000000-0000-0000-0000-000000000000', '0', '[0-9a-fA-F]')
			AND a.AssignmentSuffix IS NULL
			AND a.CancellationDate IS NULL
			AND a.AssignmentDate IS NULL
			AND a.AssignmentID NOT IN (
				SELECT AssignmentID FROM utb_hyperquestsvc_jobs WHERE AssignmentID > 0
			)
			--AND c.LynxID NOT IN (
			--	SELECT LynxID FROM utb_hyperquestsvc_jobs
			--)
			
		FOR XML EXPLICIT	
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- NO Choice Assignment waiting
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'NO Choice Assignment waiting'
		END

		------------------------------------------------------
		-- Create NO JOBS XML for return
		------------------------------------------------------
		SELECT  1 AS Tag,  
				NULL AS Parent
				, '0'	                AS [Assignment!1!AssignmentID]   
		FOR XML EXPLICIT	
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetAssignmentsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceGetAssignmentsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/