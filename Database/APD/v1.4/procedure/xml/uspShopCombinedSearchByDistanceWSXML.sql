-- Begin the transaction for dropping and creating the stored procedure
BEGIN TRANSACTION

-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopCombinedSearchByDistanceWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopCombinedSearchByDistanceWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspShopCombinedSearchByDistanceWSXML
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     This SP combines shops from HQ and APD based on ZipCode.  Once this combined table exists, we remove 
*				shops from the list where we already have a matching GUID in APD.  We then remove all Shops where the
*				ZipCode, Phone and Address 1 numerics EXIST This leaves us with just LYNX Shops and Choice Shops that match.
*
* Date:			12Oct2016
*
* PARAMETERS:  
* (I) @Zip                  The Zip code that forms the basis of the search  
* (I) @InsuranceCompanyID   The Insurance Company ID (To process carrier exclusions)
* (I) @ShopTypeCode         (P)rogram, (C)hoice, (B)Both, (N)on-program, (A)ll
* (I) @MaxShops             Limits the number of shops returned in the search.  If omitted, no limit is set.
* (I) @UserID               The ID of the user performing the search
* (I) @LogSearchFlag        Flag allowing caller to control whether the search is logged or not.  Used for "internal"
*                               searches fromthe application (such as Shop Maintenance) 
*
* RESULT SET:
* An XML Stream containing shop summary data that matches the search criteria
*
* REVISIONS:	12Oct2016 - TVD - 1.0 - Initial development
*				11Nov2016 - TVD - 1.1 - Added code to test distance for 0.  If 0 make it a 1
*				30Nov2016 - TVD - 1.2 - Added distance parameter
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE [dbo].[uspShopCombinedSearchByDistanceWSXML]
	@Zip						VARCHAR(11) = Null
	, @InsuranceCompanyID		udt_std_id
	, @ShopTypeCode				VARCHAR(1)
	, @ShopName                 udt_std_name = NULL
	, @MaxShops					udt_std_int = 0
	, @UserID					udt_std_id = 0
	, @LogSearchFlag			udt_std_flag = 1
	, @TargetDistance			INT = 50
AS
BEGIN
	--=======================================================
	-- Declarations and Sets
	--=======================================================
	DECLARE @ProcName				VARCHAR(50)  
	DECLARE @Now					DATETIME
	
	-- Debug/Error Handling
	DECLARE @Debug							BIT
	DECLARE @DebugEventType					VARCHAR(50)
	DECLARE @DebugEventStatus				VARCHAR(50)
	DECLARE @DebugEventDescription			VARCHAR(100)
	DECLARE @DebugEventDetailedDescription	VARCHAR(1000)
	DECLARE @DebugEventXML					VARCHAR(1000)
	DECLARE @DebugSeq						INT
	

	-- TEST Params
	-- SET @ZipCode = 15201 --15301
	--SET @Zip = @ZipCode
	--SET @InsuranceCompanyID = 184
	--SET @ShopTypeCode = 'P'
	--SET @MaxShops = 0
	--SET @UserID = 0
	--SET @LogSearchFlag = 1

	SET @Now = Current_Timestamp
	SET @ProcName = 'uspShopCombinedSearchByDistanceWSXML'
	SET @Debug = 0
	SET @DebugSeq = 0

	--=======================================================
	-- Temp table creates to hold the processing data
	--=======================================================
	-- Create table to hold combined shops
	CREATE TABLE #CombinedShops (
		Source								VARCHAR(10)
		, ShopSearchLogID					INT
		, InsuranceCompanyID				INT
		, Zip								VARCHAR(8)
		, ShopTypeCode						VARCHAR(1)
		, AssignmentAtSelectionFlag			BIT
		, SelectedShopRank					INT
		, Distance							INT
		, ShopLocationID					INT
		, SelectedShopScore					INT
		, Address1							VARCHAR(50)
		, Address2							VARCHAR(50)
		, AddressCity						VARCHAR(30)
		, AddressState						VARCHAR(2)
		, AddressZip						VARCHAR(10)
		, DrivingDirections					VARCHAR(500)
		, ShopType							VARCHAR(50)
		, Name								VARCHAR(50)
		, OperatingFridayEndTime			CHAR(4)
		, OperatingFridayStartTime			CHAR(4)
		, OperatingMondayEndTime			CHAR(4)
		, OperatingMondayStartTime			CHAR(4)
		, OperatingSaturdayEndTime			CHAR(4)
		, OperatingSaturdayStartTime		CHAR(4)	
		, OperatingSundayEndTime			CHAR(4)
		, OperatingSundayStartTime			CHAR(4)
		, OperatingThursdayEndTime			CHAR(4)
		, OperatingThursdayStartTime		CHAR(4)
		, OperatingTuesdayEndTime			CHAR(4)
		, OperatingTuesdayStartTime			CHAR(4)
		, OperatingWednesdayEndTime			CHAR(4)
		, OperatingWednesdayStartTime		CHAR(4)
		, PhoneAreaCode						CHAR(3)
		, PhoneExchangeNumber				CHAR(3)
		, PhoneUnitNumber					VARCHAR(5)
		, PhoneExtensionNumber				CHAR(4)	
		, FaxAreaCode						CHAR(3)
		, FaxExchangeNumber					CHAR(3)
		, FaxUnitNumber						VARCHAR(5)
		, FaxExtensionNumber				CHAR(4)
		, ShopOpen							VARCHAR(8)
		, TimeAtShop						DATETIME
		, TimeZone							VARCHAR(50)
		, WarrantyPeriodRefinishCD			VARCHAR(4)
		, WarrantyPeriodWorkmanshipCD		VARCHAR(4)
		, CertifiedFirstFlag				BIT
		, ShopContactName					VARCHAR(100)
		, ShopContactPhoneAreaCode			CHAR(3)
		, ShopContactPhoneExchangeNumber	CHAR(3)
		, ShopContactPhoneExtensionNumber	CHAR(4)
		, ShopContactPhoneUnitNumber		VARCHAR(5)
		, ShopGUID							VARCHAR(50)
		, FederalTaxId						VARCHAR(30)
		, Match								BIT
		, Sort								INT
	)

	-- Create ShopGUID Duplicate Removal Temp Table
	CREATE TABLE #DupShops (
		ShopGUID					VARCHAR(50)
		, Duplicates				INT
	)

	-- Create HQ/APD Duplicate Removal Temp Table
	CREATE TABLE #DupHQAPDShops (
			Zip						VARCHAR(11)
			, PhoneAreaCode			CHAR(3)
			, PhoneExchangeNumber	CHAR(3)
			, PhoneUnitNumber		VARCHAR(5)
			, Address1				VARCHAR(50)
			, Match					INT
	)
	
	-- Create Lynx Diabled shops Temp Table
	CREATE TABLE #LynxDisabledShops (
		ShopLocationID				BIGINT
		, Zip                       VARCHAR(11)
		, PhoneNumber				VARCHAR(50)
		, Address1					VARCHAR(50)
		, HQGUID					VARCHAR(50)
		, AvailableForSelection		BIT
		, IsDisabled           BIT
	)

	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		SET @DebugEventType = 'HQ/APD Combined Shop - APD'
		SET @DebugEventStatus = 'Debugging'					
		SET @DebugEventDescription = @ProcName + ' - APD Shop Processing Started.  Parameters'			
		SET @DebugEventDetailedDescription = 'Passed in Parameters are in EventXML' 	
		SET @DebugEventXML = 'Parameters: @Zip = ' + @Zip + ',  @InsuranceCompanyID = ' + CONVERT(VARCHAR(10), @InsuranceCompanyID) + ', @ShopTypeCode = ''' + @ShopTypeCode + ''', @MaxShops = ' + ISNULL(CONVERT(VARCHAR(10), @MaxShops), '') + ', @UserID = ' + CONVERT(VARCHAR(10), @UserID) + ', @LogSearchFlag = ' + CONVERT(VARCHAR(1), @LogSearchFlag) + ', @Distance = ' + CONVERT(VARCHAR(5), @TargetDistance)
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	-----------------------------------
	-- Validation
	-----------------------------------
    -- Validate the ShopTypeCode
    IF UPPER(@ShopTypeCode) NOT IN ('P', 'N', 'A', 'B', 'C')
    BEGIN
		SET @DebugEventType = 'HQ/APD Combined Shop - APD'
		SET @DebugEventStatus = 'ERROR'					
		SET @DebugEventDescription = @ProcName + ' - Validation of @ShopTypeCode.'			
		SET @DebugEventDetailedDescription = 'The @ShopTypeCode was not valid.  Must be P, C, N, A, B.' 	
		SET @DebugEventXML = 'Parameters: @ShopTypeCode = ' + @ShopTypeCode
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)

        RAISERROR('%s: Invalid @ShopTypeCode.  Must be P, N, A, B.', 16, 1, @ProcName)
        RETURN
    END


IF UPPER(@ShopTypeCode) IN ('P', 'N', 'A', 'B')
BEGIN
	--=======================================================
	-- Add Lynx Shops 
	--=======================================================
	-- Declarations and Sets
	DECLARE @MaxDistance				INT
	DECLARE @MinDistance				INT
	DECLARE @MinShops					INT
	DECLARE @SearchIncrement			INT
	DECLARE @Distance					INT
	DECLARE @DefaultShopScore			INT
	DECLARE @CertifiedFirstScore		INT
	DECLARE @CEIScore					INT
	DECLARE @Latitude					float
	DECLARE @Longitude					float
	DECLARE @UseCEIShops                bit
	DECLARE @ShopSelector               varchar(1)
	DECLARE @WarrantyRefinishMinYrs     tinyint
	DECLARE @WarrantyWorkmanshipMinYrs  tinyint    
	DECLARE @CFLogoDisplayFlag          udt_std_flag
	DECLARE @SearchID                   udt_std_id_big
	DECLARE @Business1PersonnelTypeID   udt_std_id
	DECLARE @Sub_ShopName				VARCHAR(50)

	-- Create temporary table to hold output records as they are collected
	DECLARE @tmpShopSearch TABLE
	(
		ShopLocationID              bigint      NOT NULL,
		Distance                    int         NOT NULL,
		SelectionScore              int         NULL,
		OperatingFridayEndTime      varchar(4)  NULL,
		OperatingFridayStartTime    varchar(4)  NULL,
		OperatingMondayEndTime      varchar(4)  NULL,
		OperatingMondayStartTime    varchar(4)  NULL,
		OperatingSaturdayEndTime    varchar(4)  NULL,
		OperatingSaturdayStartTime  varchar(4)  NULL,
		OperatingSundayEndTime      varchar(4)  NULL,
		OperatingSundayStartTime    varchar(4)  NULL,
		OperatingThursdayEndTime    varchar(4)  NULL,
		OperatingThursdayStartTime  varchar(4)  NULL,
		OperatingTuesdayEndTime     varchar(4)  NULL,
		OperatingTuesdayStartTime   varchar(4)  NULL,
		OperatingWednesdayEndTime   varchar(4)  NULL,
		OperatingWednesdayStartTime varchar(4)  NULL,
		TimeAtShop                  datetime    NULL,
		TodayStartTime              varchar(20) NULL,
		TodayEndTime                varchar(20) NULL,
		ShopOpen                    varchar(8)  NOT NULL
	)

	DECLARE @tmpShopSearch2 TABLE
	(
		Rank                        int         IDENTITY(1,1), 
		ShopLocationID              bigint      NOT NULL,
		Distance                    int         NOT NULL,
		SelectionScore              int         NULL,
		OperatingFridayEndTime      varchar(4)  NULL,
		OperatingFridayStartTime    varchar(4)  NULL,
		OperatingMondayEndTime      varchar(4)  NULL,
		OperatingMondayStartTime    varchar(4)  NULL,
		OperatingSaturdayEndTime    varchar(4)  NULL,
		OperatingSaturdayStartTime  varchar(4)  NULL,
		OperatingSundayEndTime      varchar(4)  NULL,
		OperatingSundayStartTime    varchar(4)  NULL,
		OperatingThursdayEndTime    varchar(4)  NULL,
		OperatingThursdayStartTime  varchar(4)  NULL,
		OperatingTuesdayEndTime     varchar(4)  NULL,
		OperatingTuesdayStartTime   varchar(4)  NULL,
		OperatingWednesdayEndTime   varchar(4)  NULL,
		OperatingWednesdayStartTime varchar(4)  NULL,
		TimeAtShop                  datetime    NULL,
		ShopOpen                    varchar(8)  NOT NULL
	)

	DECLARE @tmpPPGScores TABLE
	(
		PPGCTSLevelCD               varchar(4)  NOT NULL,
		Score                       int         NOT NULL
	)
    
	DECLARE @tmpDistanceScores TABLE
	(
		MinDistance                 int         NOT NULL,
		MaxDistance                 int         NOT NULL,
		Score                       int         NOT NULL
	)

	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		SET @DebugEventDescription = @ProcName + ' - Temp Table Create'			
		SET @DebugEventDetailedDescription = 'All temp tables created' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	-------------------------------------
	-- Validations
	-------------------------------------
	IF LEN(RTRIM(LTRIM(@Zip))) = 0 SET @Zip = NULL

	-- Using the MSA for the zip code, get max distance to search within.  If no MaxDistance is returned, set to 50
	SELECT  
		@MaxDistance = value
	FROM    
		dbo.utb_app_variable WITH(NOLOCK)
	WHERE   
		Name = 'MAX_SHOP_SEARCH_RADIUS'
		AND SubName = (SELECT CountyType FROM utb_zip_code WHERE Zip = @Zip)

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - MAX_SHOP_SEARCH_RADIUS'			
		SET @DebugEventDetailedDescription = 'Failed to get the MAX_SHOP_SEARCH_RADIUS from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @MaxDistance = IsNull(@MaxDistance, 50) 

	-- Get minimum distance of search (starting point of search), if no Minimum distance is returned, set to 5
	SELECT  
		@MinDistance = value 
	FROM    
		dbo.utb_app_variable WITH(NOLOCK)
	WHERE   
		name = 'MIN_SHOP_SEARCH_RADIUS'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - MIN_SHOP_SEARCH_RADIUS'			
		SET @DebugEventDetailedDescription = 'Failed to get the MIN_SHOP_SEARCH_RADIUS from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @MinDistance = IsNull(@MinDistance, 5)

	-- Get minimum number of shops to return, if no MinShops is returned, set to 3
	SELECT  
		@MinShops = value 
	FROM    
		dbo.utb_app_variable
	WHERE   
		name = 'MIN_SHOPS_FOR_SEARCH_COMPLETE'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - MIN_SHOPS_FOR_SEARCH_COMPLETE'			
		SET @DebugEventDetailedDescription = 'Failed to get the MIN_SHOPS_FOR_SEARCH_COMPLETE from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @MinShops = IsNull(@MinShops, 3)

	-- Get search distance increment, if no SearchIncrement is returned, set to 5
	SELECT  
		@SearchIncrement = value
	FROM    
		dbo.utb_app_variable WITH(NOLOCK)
	WHERE   
		name = 'SHOP_SEARCH_INCREMENT'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - SHOP_SEARCH_INCREMENT'			
		SET @DebugEventDetailedDescription = 'Failed to get the SHOP_SEARCH_INCREMENT from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @SearchIncrement = IsNull(@SearchIncrement, 5) 

	-- Set distance start to Min Distance.  If MinDistance is larger than MaxDistance, set distance equal to
	-- Max Distance.  This makes sure the loop is executed at least once.
	IF @MinDistance >= @MaxDistance
	BEGIN
		SET @Distance = @MaxDistance
	END
	ELSE
	BEGIN
		SET @Distance = @MinDistance
	END

	-- Get default shop score, if no default score is returned, set to 75
	SELECT  
		@DefaultShopScore = value
	FROM    
		dbo.utb_app_variable
	WHERE   
		name = 'DEFAULT_POINTS_SCORELESS_SHOP'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - DEFAULT_POINTS_SCORELESS_SHOP'			
		SET @DebugEventDetailedDescription = 'Failed to get the DEFAULT_POINTS_SCORELESS_SHOP from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @DefaultShopScore = IsNull(@DefaultShopScore, 75) 

	-- Get Certified First point score, if no default score is returned, set to 30
	SELECT  
		@CertifiedFirstScore = value
	FROM    
		dbo.utb_app_variable
	WHERE   
		name = 'POINTS_CF_SHOP'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - POINTS_CF_SHOP'			
		SET @DebugEventDetailedDescription = 'Failed to get the POINTS_CF_SHOP from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @CertifiedFirstScore = IsNull(@CertifiedFirstScore, 30) 

	-- Get CEI point score, if no default score is returned, set to 50
	SELECT  
		@CEIScore = value
	FROM    
		dbo.utb_app_variable
	WHERE   
		name = 'POINTS_CEI_SHOP'

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - POINTS_CEI_SHOP'			
		SET @DebugEventDetailedDescription = 'Failed to get the POINTS_CEI_SHOP from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	SET  @CEIScore = IsNull(@CEIScore, 50) 

	-- Get Point values for PPG affiliation.  Default any levels not populated to 10
	INSERT INTO 
		@tmpPPGScores
	SELECT  
		SubName
		, IsNull(Value, 10)
	FROM  
		dbo.utb_app_variable
	WHERE 
		Name = 'POINTS_PPG_SHOP'

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
		SET @DebugEventDescription = @ProcName + ' - POINTS_PPG_SHOP'			
		SET @DebugEventDetailedDescription = 'Failed to get the POINTS_PPG_SHOP from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('105|%s|@tmpPPGScores', 16, 1, @ProcName)
		RETURN
	END

	-- Get Point values for distance.  Default any levels not populated to 0
	INSERT INTO 
		@tmpDistanceScores
	SELECT  
		IsNull(MinRange, 0), 
		IsNull(MaxRange, 99999), 
		IsNull(Value, 0)
	FROM  
		dbo.utb_app_variable
	WHERE 
		Name = 'POINTS_SHOP_DISTANCE'

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
		SET @DebugEventDescription = @ProcName + ' - POINTS_SHOP_DISTANCE'			
		SET @DebugEventDetailedDescription = 'Failed to get the POINTS_SHOP_DISTANCE from utb_app_variable' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('105|%s|@tmpDistanceScores', 16, 1, @ProcName)
		RETURN
	END

	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		SET @DebugEventDescription = @ProcName + ' - Values from utb_app_variables:'			
		SET @DebugEventDetailedDescription = 'Lookup values are in EventXML' 	
		SET @DebugEventXML = 'Values from utb_app_variables: Max Distance = ' + CONVERT(VARCHAR(9), @MaxDistance) + 
			', Min Distance = ' + CONVERT(VARCHAR(9), @MinDistance) + 
			', Min Shops = ' + CONVERT(VARCHAR(9), @MinShops) +
			', Search Increment = ' + CONVERT(VARCHAR(9), @SearchIncrement) + 
			', Start Distance = ' + CONVERT(VARCHAR(9), @Distance) + 
			', Default Shop Score = ' + CONVERT(VARCHAR(9), @DefaultShopScore) +
			', Certified First Score = ' + CONVERT(VARCHAR(9), @CertifiedFirstScore) +
			', CEI Shop Score = ' + CONVERT(VARCHAR(9), @CEIScore) 
			--', PPG Refinish Scores = ' SELECT * FROM @tmpPPGScores 
			--', Distance Scores = ' SELECT * FROM @tmpDistanceScores 
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	-- Get Latitude and Longitude for the entered Zip, as well as whether CEI shops will be considered for the state
	SELECT	
		@Latitude = zc.latitude,
		@Longitude = zc.longitude,
		@UseCEIShops = IsNull
		(
			(
				SELECT  
					ccs.UseCEIShopsFlag 
				FROM  
					dbo.utb_client_contract_state ccs 
				WHERE 
					ccs.InsuranceCompanyID = @InsuranceCompanyID 
					AND ccs.StateCode = zc.State
			) , 0
		)
		FROM	dbo.utb_zip_Code zc
		WHERE	zc.Zip = @Zip

	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - Latitude and Longitude'			
		SET @DebugEventDetailedDescription = 'Failed to get the Latitude and Longitude by ZipCode' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	IF @UseCEIShops IS NULL
	BEGIN
		SET @UseCEIShops=0
	END
    
	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		SET @DebugEventType = 'HQ/APD Combined Shop - APD'
		SET @DebugEventStatus = 'Debugging'					
		SET @DebugEventDescription = @ProcName + ' - APD Shop Latitude/Longitude'			
		SET @DebugEventDetailedDescription = 'Parameters are in EventXML' 	
		SET @DebugEventXML = 'Parameters: Latitude = ' + Convert(varchar(9), @Latitude) + ',  Longitude = ' + Convert(varchar(9), @Longitude)
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	IF Upper(@ShopTypeCode) = ('P')
	BEGIN
		SET @ShopSelector = '1'
	END
	ELSE IF upper(@ShopTypeCode) = 'B'
	BEGIN
		SET @ShopSelector = '1'
	END
	ELSE IF upper(@ShopTypeCode) = 'N'
	BEGIN
		SET @ShopSelector = '0'
	END
	ELSE IF Upper(@ShopTypeCode) = 'A'
	BEGIN
		SET @ShopSelector = '%'
	END

	IF @Zip is Null SET @Zip = '%'

	-- Get the minimum warranty for the insurance company.  The shop's warranties must meet or exceed the
	-- insurance company's minimums.  If @UseCEIShops is set, the shops of the CEI network will be considered in the shop search
	SELECT  
		@WarrantyRefinishMinYrs = Convert(tinyint, WarrantyPeriodRefinishMinCD),
		@WarrantyWorkmanshipMinYrs = Convert(tinyint, WarrantyPeriodWorkmanshipMinCD),
		@CFLogoDisplayFlag = CFLogoDisplayFlag
	FROM  
		dbo.utb_insurance
	WHERE 
		InsuranceCompanyID = @InsuranceCompanyID
      
	IF @@ERROR <> 0
	BEGIN
		-- SQL Server Error
		SET @DebugEventDescription = @ProcName + ' - Minimum Warranty'			
		SET @DebugEventDetailedDescription = 'Failed to get the Warranty details by InsuranceCompanyID' 	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	IF @WarrantyRefinishMinYrs IS NULL
	BEGIN
		-- The carrier had no configuration for a minimum refinish warranty, set minimum to 0
        
		SET @WarrantyRefinishMinYrs = 0
	END
    
	IF @WarrantyWorkmanshipMinYrs IS NULL
	BEGIN
		-- The carrier had no configuration for a minimum workmanship warranty, set minimum to 0
        
		SET @WarrantyWorkmanshipMinYrs = 0
	END

	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		SET @DebugEventDescription = @ProcName + ' - From Carrier:'			
		SET @DebugEventDetailedDescription = 'Values are in EventXML' 	
		SET @DebugEventXML = 'WarrantyRefinishMinYrs = ' + Convert(varchar(2), @WarrantyRefinishMinYrs) + 
			', WarrantyWorkmanshipMinYrs = ' + Convert(varchar(9), @WarrantyWorkmanshipMinYrs) 
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	-- In order to qualify, a shop must meet the following criteria:
	--      1)  Distance to the shop from the caller must be within @Distance.  This is determined by a trigonometric formula
	--          which compares the shops and callers longitutude and latitude.
	--      2)  The program/non-program status of the shop must match that requested
	--      3)  If this carrier can use the CEI shop network, a shop may be a CEI Program Shop
	--      4)  If the shop is specifically on a carrier's inclusion list, they will be considered
	--      4)  The shop must be enabled
	--      5)  The shop must be available for selection (for various reasons, an enabled program shop could be temporarily 
	--          turned off from receiving assignments eg. Local disaster, not accepting any more work, etc.)
	--      5)  The shop's workmanship and refinish warranties must meet or exceed the requirements of the insurance company
	--      6)  The shop must not be on the exclusion list given to us by the Insurance Company
	--      7)  The shop must NOT be located in Massachusetts (LYNX is unable to assign work to shops in this state)

	-- If less then @MinShops qualify, @Distance is increased by @SearchIncrement and the test repeated until at least @MinShops qualifying
	-- shops are found or @MaxDistance is reached.

	IF @Debug = 1
	BEGIN
		PRINT ''
		PRINT 'Shops Considered ='
		SELECT  sl.ShopLocationID,
				CASE
					WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN 'CEI'
					WHEN sl.ProgramFlag = 1 THEN 'LS'
					ELSE 'NON'
				END AS ProgramCD,
				IsNull(sl.Latitude, zc.Latitude) AS LatitudeUsed,
				IsNull(sl.Longitude, zc.Longitude) AS LongitudeUsed,
				sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
									square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) AS Distance
			FROM  dbo.utb_shop_location sl
			INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
			LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
			LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
			WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
							square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @MaxDistance 
			--AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
			AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
					(Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
					(Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
					(Upper(@ShopTypeCode) IN ('P','B') AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))            
			AND sl.EnabledFlag = 1
			AND sl.AvailableForSelectionFlag = 1
			AND sl.AddressState <> 'MA'
			AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
			AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
			AND csle.InsuranceCompanyID IS NULL
			AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL)
	END

	WHILE @Distance <= @MaxDistance
	BEGIN
		IF 	((SELECT  COUNT(*)
			FROM  dbo.utb_shop_location sl
			INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
			LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
			LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
			WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
							square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @distance 
			--AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
			AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
					(Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
					(Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
					(Upper(@ShopTypeCode) IN ('P','B') AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))            
			AND sl.EnabledFlag = 1
			AND sl.AvailableForSelectionFlag = 1
			AND sl.AddressState <> 'MA'
			AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
			AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
			AND csle.InsuranceCompanyID IS NULL
			AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL))
																	>= @MinShops)  -- If @MinShops or more shops are within distance
			OR
			(@Distance + @SearchIncrement > @MaxDistance)     --If we are on our last loop
   		BEGIN
			-- Perform shop selection, with an initial selection score that takes into account everything but
			-- distance.  Insert the results into the temporary table.
			INSERT INTO	@tmpShopSearch
				SELECT	sl.ShopLocationId,
			     		Convert(int,
									sqrt(square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) + 
            			     			square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @Longitude) * cos(IsNull(sl.Latitude, zc.Latitude) / 57.3)))  ) AS Distance,
						-- Begin selection score logic
							CASE  -- Shop Performance score
							WHEN IsNull(sl.ProgramScore, 0) > 0 THEN sl.ProgramScore
							ELSE @DefaultShopScore
							END + 
							CASE  -- PPG Affiliation
							WHEN sl.CertifiedFirstFlag = 1 THEN @CertifiedFirstScore
							WHEN sl.PPGCTSFlag = 1 THEN IsNull((SELECT Score FROM @tmpPPGScores WHERE PPGCTSLevelCD = sl.PPGCTSLevelCD), 10)    -- Default unknown levels to 10
							ELSE 0
							END + 
							CASE  -- CEI Shop
							WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN @CEIScore
							ELSE 0
							END AS SelectionScore,
						-- End selection score logic
						slh.OperatingFridayEndTime,
						slh.OperatingFridayStartTime,
						slh.OperatingMondayEndTime,
						slh.OperatingMondayStartTime,
						slh.OperatingSaturdayEndTime,
						slh.OperatingSaturdayStartTime,
						slh.OperatingSundayEndTime,
						slh.OperatingSundayStartTime,
						slh.OperatingThursdayEndTime,
						slh.OperatingThursdayStartTime,
						slh.OperatingTuesdayEndTime,
						slh.OperatingTuesdayStartTime,
						slh.OperatingWednesdayEndTime,
						slh.OperatingWednesdayStartTime,
						dbo.ufnUtilityGetLocalTime( GetUTCDate(), sl.AddressZip ) as TimeAtShop,
						NULL,
						NULL,
						'Unknown' as ShopOpen
				FROM   dbo.utb_shop_location sl
				INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
				LEFT JOIN dbo.utb_shop_location_hours slh ON (sl.ShopLocationID = slh.ShopLocationID)
				LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
				LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
				WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
								square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @distance 
					--AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
				AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
						(Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
						(Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
						(Upper(@ShopTypeCode) IN ('P','B') AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))                  
					AND sl.EnabledFlag = 1
					AND sl.AvailableForSelectionFlag = 1
					AND sl.AddressState <> 'MA'
					AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
					AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
					AND csle.InsuranceCompanyID IS NULL
					AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL)

			IF @@ERROR <> 0
			BEGIN
				-- Insertion failure
				SET @DebugEventDescription = @ProcName + ' - insert into @tmpShopSearch'			
				SET @DebugEventDetailedDescription = 'Failed to insert data into the @tmpShopSearch table' 	
				SET @DebugEventXML = ''
				SET @DebugSeq = @DebugSeq + 1

				PRINT @DebugEventDescription
				PRINT @DebugEventDetailedDescription
				PRINT @DebugEventXML
				INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
            
				RAISERROR('105|%s|@tmpShopSearch', 16, 1, @ProcName)
				RETURN
			END

			BREAK
		END
		ELSE
		BEGIN
			-- There were less than @MinShops shops within the distance, increase distance by @SearchIncrement until it
			-- reaches @MaxDistance miles
			SET @Distance = @Distance + @SearchIncrement
		END
	END

	-- Update the selection score for distance, and additional updates for shop open/close determination
	UPDATE  @tmpShopSearch
		SET   SelectionScore =    SelectionScore + IsNull((SELECT TOP 1 Score FROM @tmpDistanceScores WHERE Distance >= MinDistance AND Distance < MaxDistance ORDER BY Score DESC), 0),  -- Default to 0 if record isn't found
			TodayStartTime =    CASE datepart(dw, TimeAtShop)
									WHEN 1 THEN CASE  
												WHEN IsDate(Left(OperatingSundayStartTime, 2) + ':' + Right(OperatingSundayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSundayStartTime, 2) + ':' + Right(OperatingSundayStartTime, 2)
												END
									WHEN 2 THEN CASE
												WHEN IsDate(Left(OperatingMondayStartTime, 2) + ':' + Right(OperatingMondayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingMondayStartTime, 2) + ':' + Right(OperatingMondayStartTime, 2)
												END
									WHEN 3 THEN CASE
												WHEN IsDate(Left(OperatingTuesdayStartTime, 2) + ':' + Right(OperatingTuesdayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingTuesdayStartTime, 2) + ':' + Right(OperatingTuesdayStartTime, 2)
												END
									WHEN 4 THEN CASE
												WHEN IsDate(Left(OperatingWednesdayStartTime, 2) + ':' + Right(OperatingWednesdayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingWednesdayStartTime, 2) + ':' + Right(OperatingWednesdayStartTime, 2)
												END
									WHEN 5 THEN CASE
												WHEN IsDate(Left(OperatingThursdayStartTime, 2) + ':' + Right(OperatingThursdayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingThursdayStartTime, 2) + ':' + Right(OperatingThursdayStartTime, 2)
												END
									WHEN 6 THEN CASE
												WHEN IsDate(Left(OperatingFridayStartTime, 2) + ':' + Right(OperatingFridayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingFridayStartTime, 2) + ':' + Right(OperatingFridayStartTime, 2)
												END
									WHEN 7 THEN CASE
												WHEN IsDate(Left(OperatingSaturdayStartTime, 2) + ':' + Right(OperatingSaturdayStartTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSaturdayStartTime, 2) + ':' + Right(OperatingSaturdayStartTime, 2)
												END
								END,
			TodayEndTime =      CASE datepart(dw, TimeAtShop)
									WHEN 1 THEN CASE
												WHEN IsDate(Left(OperatingSundayEndTime, 2) + ':' + Right(OperatingSundayEndTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSundayEndTime, 2) + ':' + Right(OperatingSundayEndTime, 2)
												END
									WHEN 2 THEN CASE
												WHEN IsDate(Left(OperatingMondayEndTime, 2) + ':' + Right(OperatingMondayEndTime, 2)) = 0 THEN NULL 
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingMondayEndTime, 2) + ':' + Right(OperatingMondayEndTime, 2)
												END
									WHEN 3 THEN CASE
												WHEN IsDate(Left(OperatingTuesdayEndTime, 2) + ':' + Right(OperatingTuesdayEndTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingTuesdayEndTime, 2) + ':' + Right(OperatingTuesdayEndTime, 2)
												END
									WHEN 4 THEN CASE
												WHEN IsDate(Left(OperatingWednesdayEndTime, 2) + ':' + Right(OperatingWednesdayEndTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingWednesdayEndTime, 2) + ':' + Right(OperatingWednesdayEndTime, 2)
												END
									WHEN 5 THEN CASE
												WHEN IsDate(Left(OperatingThursdayEndTime, 2) + ':' + Right(OperatingThursdayEndTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingThursdayEndTime, 2) + ':' + Right(OperatingThursdayEndTime, 2)
												END
									WHEN 6 THEN CASE
												WHEN IsDate(Left(OperatingFridayEndTime, 2) + ':' + Right(OperatingFridayEndTime, 2)) = 0 THEN NULL
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingFridayEndTime, 2) + ':' + Right(OperatingFridayEndTime, 2)
												END
									WHEN 7 THEN CASE
												WHEN IsDate(Left(OperatingSaturdayEndTime, 2) + ':' + Right(OperatingSaturdayEndTime, 2)) = 0 THEN NULL 
												ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSaturdayEndTime, 2) + ':' + Right(OperatingSaturdayEndTime, 2)
												END
								END

	IF @@ERROR <> 0
	BEGIN
		-- Update failure
		SET @DebugEventDescription = @ProcName + ' - Distance and Shop Open/Close'			
		SET @DebugEventDetailedDescription = 'Failed to update the selection score for distance, and shop open/close determination'  	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)

		RAISERROR('104|%s|@tmpShopSearch', 16, 1, @ProcName)
		RETURN
	END

	-- Final update for shop open/close determination
	UPDATE  @tmpShopSearch
		SET   ShopOpen = CASE
							WHEN TodayStartTime IS NULL OR TodayEndTime IS NULL THEN 'Unknown'
							WHEN TimeAtShop BETWEEN Convert(datetime, TodayStartTime) AND Convert(datetime, TodayEndTime) THEN 'Open'
							ELSE 'Closed'
						END

	IF @@ERROR <> 0
	BEGIN
		-- Update failure
		SET @DebugEventDescription = @ProcName + ' - Final Open/Close Determination'			
		SET @DebugEventDetailedDescription = 'Failed to update the final shop open/close determination'  	
		SET @DebugEventXML = ''
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)

		RAISERROR('104|%s|@tmpShopSearch', 16, 1, @ProcName)
		RETURN
	END

	-- Compile with identity for final select.  If @MaxShops is specified, tell SQL to only return @MaxShops rows
	IF @MaxShops IS NOT NULL
	BEGIN
		SET ROWCOUNT @MaxShops
	END
	ELSE
	BEGIN
		SET ROWCOUNT 0
	END

	INSERT INTO @tmpShopSearch2
	(
		ShopLocationID,
		Distance,
		SelectionScore,
		OperatingFridayEndTime,
		OperatingFridayStartTime,
		OperatingMondayEndTime,
		OperatingMondayStartTime,
		OperatingSaturdayEndTime,
		OperatingSaturdayStartTime,
		OperatingSundayEndTime,
		OperatingSundayStartTime,
		OperatingThursdayEndTime,
		OperatingThursdayStartTime,
		OperatingTuesdayEndTime,
		OperatingTuesdayStartTime,
		OperatingWednesdayEndTime,
		OperatingWednesdayStartTime,
		TimeAtShop,
		ShopOpen
	)
	SELECT ShopLocationID,
			Distance,
			SelectionScore,
			OperatingFridayEndTime,
			OperatingFridayStartTime,
			OperatingMondayEndTime,
			OperatingMondayStartTime,
			OperatingSaturdayEndTime,
			OperatingSaturdayStartTime,
			OperatingSundayEndTime,
			OperatingSundayStartTime,
			OperatingThursdayEndTime,
			OperatingThursdayStartTime,
			OperatingTuesdayEndTime,
			OperatingTuesdayStartTime,
			OperatingWednesdayEndTime,
			OperatingWednesdayStartTime,
			TimeAtShop,
			ShopOpen
		FROM @tmpShopSearch
		ORDER BY SelectionScore DESC, Distance, ShopLocationID

	-- Cancel any previous SET ROWCOUNT
	IF @MaxShops IS NOT NULL
	BEGIN
		SET ROWCOUNT 0
	END

	-----------------------------------
	-- Debug - Show Params
	-----------------------------------
	IF @Debug = 1
	BEGIN
		PRINT ''

		SET @DebugEventDescription = @ProcName + ' - Final Distance:'			
		SET @DebugEventDetailedDescription = 'Values are in EventXML' 	
		SET @DebugEventXML = 'Final Distance = ' + Convert(varchar(9), @Distance) 
		SET @DebugSeq = @DebugSeq + 1

		PRINT @DebugEventDescription
		PRINT @DebugEventDetailedDescription
		PRINT @DebugEventXML
		PRINT 'Shops Selected ='
		SELECT * FROM @tmpShopSearch2
		INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
	END

	-- Log the search attempt if the search was conducted for a non-demo company and @LogSearchFlag = 1
	IF ((SELECT DemoFlag FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID) = 0) AND 
		(@LogSearchFlag = 1)
	BEGIN    
		INSERT INTO dbo.utb_shop_search_log 
		(
			CEIShopsIncludedFlag, 
			InsuranceCompanyID, 
			ResultCount,
			SearchUserID, 
			SearchCriteria,
			SearchDate, 
			SearchTypeCD,
			SysLastUserID, 
			SysLastUpdatedDate
		)
			SELECT @UseCEIShops,
					@InsuranceCompanyID,
					Count(*),
					@UserID,
					@Zip + '; ' + @ShopTypeCode,
					@Now,
					'D',
					@UserID,
					@Now
			FROM @tmpShopSearch2
        
		IF @@ERROR <> 0
		BEGIN
			-- Insertion failure
			SET @DebugEventDescription = @ProcName + ' - Log the search attempt '			
			SET @DebugEventDetailedDescription = 'Failed to insert the Log search attempt'  	
			SET @DebugEventXML = ''
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
			RAISERROR('105|%s|@utb_shop_search_log', 16, 1, @ProcName)
			RETURN
		END
		ELSE
		BEGIN
			-- Get identity of search
			SET @SearchID = SCOPE_IDENTITY()
		END
    
		-- Log the search results (if turned on)
		IF (SELECT value FROM dbo.utb_app_variable WHERE Name = 'LOG_SHOP_SEARCH_RESULTS') = 'ON'
		BEGIN 
			INSERT INTO dbo.utb_shop_search_results_log
				SELECT  @SearchID,
						tmp.ShopLocationID,
						tmp.Distance,
						CASE  -- CEI Shop
						WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN @CEIScore
						ELSE 0
						END,
						IsNull((SELECT TOP 1 Score FROM @tmpDistanceScores WHERE Distance >= MinDistance AND tmp.Distance < MaxDistance ORDER BY Score DESC), 0), -- Defaulted to 0 for not found records
						CASE  -- Shop Performance score
						WHEN IsNull(sl.ProgramScore, 0) > 0 THEN sl.ProgramScore
						ELSE @DefaultShopScore
						END, 
						CASE  -- PPG Affiliation
						WHEN sl.CertifiedFirstFlag = 1 THEN @CertifiedFirstScore
						WHEN sl.PPGCTSFlag = 1 THEN IsNull((SELECT Score FROM @tmpPPGScores WHERE PPGCTSLevelCD = sl.PPGCTSLevelCD), 10)    -- Default unknown levels to 10
						ELSE 0
						END,
						SelectionScore,
						CASE  -- PPG Affiliation
						WHEN sl.CertifiedFirstFlag = 1 THEN 'CF'
						WHEN sl.PPGCTSFlag = 1 THEN IsNull(sl.PPGCTSLevelCD, '')
						ELSE ''
						END,
						tmp.Rank,
						@UserID,
						@Now
				FROM  @tmpShopSearch2 tmp
				LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
                       
			IF @@ERROR <> 0
			BEGIN
				-- Insertion failure
				SET @DebugEventDescription = @ProcName + ' - Log the search results '			
				SET @DebugEventDetailedDescription = 'Failed to insert the Log search results'  	
				SET @DebugEventXML = ''
				SET @DebugSeq = @DebugSeq + 1

				PRINT @DebugEventDescription
				PRINT @DebugEventDetailedDescription
				PRINT @DebugEventXML
				INSERT INTO utb_apd_event_log VALUES (@DebugEventType, 'ERROR -' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
    
				RAISERROR('105|%s|@utb_shop_search_results_log', 16, 1, @ProcName)
				RETURN
			END                 
		END
	END
	ELSE
	BEGIN    
		-- This is a demo company, return a dummy Search ID
		SET @SearchID = 0
	END

	-------------------------------------------
	-- Add Lynx Select shops to combined list
	-------------------------------------------
	IF LEN(RTRIM(LTRIM(@ShopName))) = 0 SET @ShopName = NULL

    -- Create search values for name and city
    IF @ShopName IS NULL
    BEGIN 
        SET @Sub_ShopName = '%'
    END
    ELSE 
    BEGIN
        SET @Sub_ShopName = '%' + @ShopName + '%'
    END

	INSERT INTO #CombinedShops
	SELECT 
		'LYNX'
		, @SearchID
		, @InsuranceCompanyID
		, @Zip
		, @ShopTypeCode
		, (SELECT AssignmentAtSelectionFlag FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
		, tmp.Rank
		, CASE tmp.Distance
			WHEN 0 THEN 1
			ELSE tmp.Distance
		  END
		, tmp.ShopLocationID
		, tmp.SelectionScore
		, IsNull(LTrim(RTrim(sl.Address1)), '')
		, IsNull(LTrim(RTrim(sl.Address2)), '')
		, IsNull(LTrim(RTrim(sl.AddressCity)), '')
		, IsNull(LTrim(RTrim(sl.AddressState)), '')
		, IsNull(LTrim(RTrim(sl.AddressZip)), '')
		, IsNull(LTrim(RTrim(sl.DrivingDirections)), '')
		, CASE WHEN 
			(Replace(Ltrim(Rtrim((IsNull((CASE WHEN (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1)  THEN 'C ' END), '') + 
				IsNull((CASE WHEN (sl.ProgramFlag = 1)  THEN 'D ' END), '') + 
				IsNull((CASE WHEN (sl.ReferralFlag = 1)  THEN 'R ' END), '')))), ' ', ', ')) = '' THEN 'N/A' 
		   ELSE
			(Replace(Ltrim(Rtrim((IsNull((CASE WHEN (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1)  THEN 'C ' END), '') + 
				IsNull((CASE WHEN (sl.ProgramFlag = 1)  THEN 'D ' END), '') + 
				IsNull((CASE WHEN (sl.ReferralFlag = 1)  THEN 'R ' END), '')))), ' ', ', '))
		   END
		 , IsNull(LTrim(RTrim(sl.Name)), '')
		 , IsNull(tmp.OperatingFridayEndTime, '')
		 , IsNull(tmp.OperatingFridayStartTime, '')
		 , IsNull(tmp.OperatingMondayEndTime, '')
		 , IsNull(tmp.OperatingMondayStartTime, '')
		 , IsNull(tmp.OperatingSaturdayEndTime, '')
		 , IsNull(tmp.OperatingSaturdayStartTime, '')
		 , IsNull(tmp.OperatingSundayEndTime, '')
		 , IsNull(tmp.OperatingSundayStartTime, '')
		 , IsNull(tmp.OperatingThursdayEndTime, '')
		 , IsNull(tmp.OperatingThursdayStartTime, '')
		 , IsNull(tmp.OperatingTuesdayEndTime, '')
		 , IsNull(tmp.OperatingTuesdayStartTime, '')
		 , IsNull(tmp.OperatingWednesdayEndTime, '')
		 , IsNull(tmp.OperatingWednesdayStartTime, '')
		 , IsNull(LTrim(RTrim(sl.PhoneAreaCode)), '')
		 , IsNull(LTrim(RTrim(sl.PhoneExchangeNumber)), '')
		 , IsNull(LTrim(RTrim(sl.PhoneUnitNumber)), '')
		 , IsNull(LTrim(RTrim(SUBSTRING(sl.PhoneExtensionNumber,0,2))), '')
		 , IsNull(LTrim(RTrim(sl.FaxAreaCode)), '')
		 , IsNull(LTrim(RTrim(sl.FaxExchangeNumber)), '')
		 , IsNull(LTrim(RTrim(sl.FaxUnitNumber)), '')
		 , IsNull(LTrim(RTrim(sl.FaxExtensionNumber)), '')
		 , IsNull(tmp.ShopOpen, '')
		 , IsNull(tmp.TimeAtShop, '')
		 , IsNull(tz.Name, '')
		 , IsNull(sl.WarrantyPeriodRefinishCD, '')
		 , IsNull(sl.WarrantyPeriodWorkmanshipCD, '')
		 , CASE @CFLogoDisplayFlag
			  WHEN 1 THEN IsNull(sl.CertifiedFirstFlag, 0)
		   ELSE 0
		 END
		 , IsNull(LTrim(RTrim(p.Name)), '')
		 , IsNull(LTrim(RTrim(p.PhoneAreaCode)), '')
		 , IsNull(LTrim(RTrim(p.PhoneExchangeNumber)), '')
		 , IsNull(LTrim(RTrim(SUBSTRING(p.PhoneExtensionNumber,0,2))), '')
		 , IsNull(LTrim(RTrim(p.PhoneUnitNumber)), '')
		 , sl.ppgctsid
		 , sl.SalesTaxNumber
		 , 0
		 , -1
	FROM
		@tmpShopSearch2 tmp
		LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
		LEFT JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.Zip)
		LEFT JOIN dbo.utb_time_zone tz ON (zc.TimeZone = tz.TimeZone)

		LEFT JOIN dbo.utb_shop_location_personnel slp ON (tmp.ShopLocationID = slp.ShopLocationID)
		LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
		LEFT JOIN dbo.utb_personnel_type pt ON (p.PersonnelTypeID = pt.PersonnelTypeID AND pt.PersonnelTypeID = @Business1PersonnelTypeID)
	WHERE
		sl.Name LIKE @Sub_ShopName
END

IF UPPER(@ShopTypeCode) IN ('C', 'B')
BEGIN
	--=======================================================
	-- Add HQ Shops
	--=======================================================
	-- Check if this is a Choice client
	IF EXISTS(SELECT * FROM utb_hyperquest_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
	BEGIN
		DECLARE @ZipRadius		AS INT
		DECLARE @CompanyName	AS VARCHAR(100)
		DECLARE @PhoneNum		AS VARCHAR(25)
		DECLARE @EmailAddress	AS VARCHAR(100)
		DECLARE @Threshold		AS INT
		DECLARE @MaxRows		AS INT
		DECLARE @PartnerKey		AS VARCHAR(50)
		DECLARE @SortCd			AS varchar(10)
		DECLARE @X float, @Y float, @Z float

		CREATE TABLE #zip_temp (
			ZipCode NVARCHAR(11) NOT NULL
			, X		float NULL
			, Y		float NULL
			, Z		float NULL
		)

		IF @MaxRows IS NULL
		BEGIN
			SET @MaxRows = 200
		END

		SET @CompanyName	= CASE ISNULL(@CompanyName, '')		WHEN '' THEN '*' WHEN '*' THEN '*' ELSE @CompanyName + '%' END
		SET @PhoneNum		= CASE ISNULL(@PhoneNum, '')		WHEN '' THEN '*' WHEN '*' THEN '*' ELSE @PhoneNum + '%' END
		SET @EmailAddress	= CASE ISNULL(@EmailAddress, '')	WHEN '' THEN '*' WHEN '*' THEN '*' ELSE @EmailAddress + '%' END
		SET @SortCd			= CASE LTRIM(RTRIM(ISNULL(@SortCd, 'DS')))	WHEN '' THEN 'DS' WHEN 'SN' THEN 'SN' WHEN 'CT' THEN 'CT' WHEN 'ZIP' THEN 'ZIP' ELSE 'DS' END

		SELECT 
			@X =  zc.X
			, @Y = zc.Y
			, @Z = zc.Z
		FROM 
			ZipCodes zc WITH (NOLOCK)
		WHERE 
			ZipCode = @Zip

		--SELECT @ZipRadius

		-----------------------------------
		-- Debug - Show Params
		-----------------------------------
		IF @Debug = 1
		BEGIN
			SET @DebugEventType = 'HQ/APD Combined Shop - HQ'
			SET @DebugEventStatus = 'Debugging'					
			SET @DebugEventDescription = @ProcName + ' - HQ Shop Process Starting.  Parameters:'			
			SET @DebugEventDetailedDescription = 'Passed in Parameters are in EventXML' 	
			SET @DebugEventXML = 'Parameters: @CompanyName = ''' + @CompanyName + ''',  @PhoneNum = ''' + @PhoneNum + ''', @EmailAddress = ''' + @EmailAddress + ''', @SortCd = ' + @SortCd + ', @MaxRows = ' + CONVERT(VARCHAR(20), @MaxRows)
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
		END

		IF (@ZipRadius > 0)
		BEGIN
			INSERT INTO #zip_temp 
			(
				ZipCode
				, X
				, Y
				, Z
			)
			SELECT 
				ZipCode
				, X
				, Y
				, Z
			FROM 
				ZipCodes WITH (NOLOCK)
			WHERE 
				(@X - X) <= @ZipRadius
				AND (@Y - Y) <= @ZipRadius
				AND (@Z - Z) <= @ZipRadius
				AND SQRT(SQUARE(@X - X) + SQUARE(@Y - Y) + SQUARE(@Z - Z)) <= @ZipRadius
		END
		ELSE
		BEGIN
			INSERT INTO #zip_temp 
			(
				ZipCode
				, X
				, Y
				, Z
			)
			VALUES 
			(
				@Zip
				, @X
				, @Y
				, @Z
			)
		END

		-----------------------------------
		-- Debug - Show Params
		-----------------------------------
		IF @Debug = 1
		BEGIN
			SET @DebugEventDescription = @ProcName + ' - Adding HQ Shops'			
			SET @DebugEventDetailedDescription = 'Adding HQ Shops to the #CombinedShops table' 	
			SET @DebugEventXML = ''
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
		END

		-----------------------------------
		-- Distance Calculation
		-----------------------------------
		-- Get Lat/Lon or Search Zip
		DECLARE @SearchLatitude		FLOAT
		DECLARE @SearchLongitude	FLOAT

		SELECT @SearchLatitude = Latitude, @SearchLongitude = Longitude FROM utb_zip_code where Zip = @Zip

		IF LEN(RTRIM(LTRIM(@ShopName))) = 0 SET @ShopName = NULL

		-- Create search values for name and city
		IF @ShopName IS NULL
		BEGIN 
			SET @Sub_ShopName = '%'
		END
		ELSE 
		BEGIN
			SET @Sub_ShopName = '%' + @ShopName + '%'
		END
		
		-- Fiter the Disabled shops from Lynx table and filter shops from provider(CHIOCE)
		If @Sub_ShopName IS NOT NULL
		BEGIN 
		    INSERT INTO #LynxDisabledShops
			SELECT
				 sl.ShopLocationID 
				 ,sl.AddressZip
				 ,sl.PhoneAreaCode+sl.PhoneExchangeNumber+sl.PhoneUnitNumber
				 ,sl.Address1
				 ,p.[GUID]
				 ,sl.AvailableForSelectionFlag
				 ,''
				FROM 
						utb_shop_location sl  
						LEFT JOIN Provider p WITH (NOLOCK)
							ON sl.PhoneAreaCode+sl.PhoneExchangeNumber+sl.PhoneUnitNumber = SUBSTRING(ISNULL(p.Phone, ''),1,3) + SUBSTRING(ISNULL(p.Phone, ''),4,3) + SUBSTRING(ISNULL(p.Phone, ''),7,4)
							AND dbo.ufnGetNumeric(sl.Address1) = dbo.ufnGetNumeric(p.Address1)
							AND sl.AddressZip = p.zip
						INNER JOIN #zip_temp z 
							ON p.Zip = z.ZipCode
						LEFT JOIN CompanyProviderLookup cpl
							ON cpl.ProviderGuid = p.GUID
							AND cpl.PartnerKey = 'LYNX'
					WHERE ( @CompanyName = '*' OR p.NAME LIKE @CompanyName )
						AND ( @PhoneNum = '*' OR p.Phone like @PhoneNum )
						AND p.IsDeleted = 0
						AND p.[Disabled] = 0
						AND p.NAME LIKE @Sub_ShopName
						
						
			IF  ( 1 <= (SELECT Count(*) FROM #LynxDisabledShops))
			BEGIN
	            BEGIN          
					Update #LynxDisabledShops  SET IsDisabled = '1' WHERE 
					HQGUID IN (SELECT HQGUID FROM #LynxDisabledShops 
									WHERE PhoneNumber IN (
									SELECT		PhoneNumber 
									FROM		#LynxDisabledShops
									WHERE       AvailableForSelection = 0       
									GROUP BY	PhoneNumber
									HAVING		COUNT(1) >= 1)
									AND PhoneNumber Not IN (SELECT PhoneNumber FROM #LynxDisabledShops WHERE AvailableForSelection = 1))
				END
				BEGIN 
					Update #LynxDisabledShops  SET IsDisabled = '0' WHERE IsDisabled <> '1'
				END
			END
	     END
		
		--SELECT * FROM #zip_temp 
		INSERT INTO #CombinedShops (
			Source
			, ShopSearchLogID
			, InsuranceCompanyID
			, Zip				
			, ShopTypeCode		
			, AssignmentAtSelectionFlag	
			, SelectedShopRank			
			, Distance					
			, ShopLocationID			
			, SelectedShopScore			
			, Address1					
			, Address2					
			, AddressCity				
			, AddressState				
			, AddressZip				
			, DrivingDirections			
			, ShopType					
			, Name						
			, OperatingFridayEndTime	
			, OperatingFridayStartTime	
			, OperatingMondayEndTime	
			, OperatingMondayStartTime	
			, OperatingSaturdayEndTime	
			, OperatingSaturdayStartTime
			, OperatingSundayEndTime	
			, OperatingSundayStartTime	
			, OperatingThursdayEndTime	
			, OperatingThursdayStartTime
			, OperatingTuesdayEndTime	
			, OperatingTuesdayStartTime	
			, OperatingWednesdayEndTime	
			, OperatingWednesdayStartTime
			, PhoneAreaCode				
			, PhoneExchangeNumber		
			, PhoneUnitNumber			
			, PhoneExtensionNumber		
			, FaxAreaCode				
			, FaxExchangeNumber			
			, FaxUnitNumber				
			, FaxExtensionNumber			
			, ShopOpen						
			, TimeAtShop					
			, TimeZone						
			, WarrantyPeriodRefinishCD		
			, WarrantyPeriodWorkmanshipCD	
			, CertifiedFirstFlag			
			, ShopContactName				
			, ShopContactPhoneAreaCode		
			, ShopContactPhoneExchangeNumber	
			, ShopContactPhoneExtensionNumber	
			, ShopContactPhoneUnitNumber
			, ShopGUID					
			, FederalTaxId				
			, Match						
		)
		SELECT TOP (@MaxRows) 
			'HQ'
			, @SearchID
			, @InsuranceCompanyID
			, ISNULL(p.Zip, '')
			, 'C'
			, 0
			, 0
			, CASE Convert(int,	sqrt(square(69.1 * (IsNull(p.Latitude, @SearchLatitude) - @SearchLatitude)) + 
            		square(69.1 * (IsNull(p.Longitude, @SearchLongitude) - @SearchLongitude) * cos(IsNull(p.Latitude, @SearchLatitude) / 57.3)))  ) 
				WHEN 0 THEN 1
				ELSE Convert(int,	sqrt(square(69.1 * (IsNull(p.Latitude, @SearchLatitude) - @SearchLatitude)) + 
            		square(69.1 * (IsNull(p.Longitude, @SearchLongitude) - @SearchLongitude) * cos(IsNull(p.Latitude, @SearchLatitude) / 57.3)))  )
			  END
			, ISNULL(cpl.CompanyProviderID, '')
			, 0
			, ISNULL(p.Address1, '')
			, ISNULL(p.Address2, '')
			, ISNULL(p.City, '')
			, ISNULL(p.[State], '')
			, ISNULL(p.Zip, '')
			, NULL
			, NULL
			, p.NAME
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, SUBSTRING(ISNULL(p.Phone, ''),1,3)
			, SUBSTRING(ISNULL(p.Phone, ''),4,3)
			, SUBSTRING(ISNULL(p.Phone, ''),7,4)
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, 0
			, NULL
			, NULL
			, NULL
			, NULL
			, NULL
			, CONVERT(VARCHAR(64), p.[GUID])
			, ISNULL(p.FederalTaxID, '')
			, 0
		FROM 
			Provider p WITH (NOLOCK)
			INNER JOIN #zip_temp z 
				ON p.Zip = z.ZipCode
			LEFT JOIN CompanyProviderLookup cpl
				ON cpl.ProviderGuid = p.GUID
				AND cpl.PartnerKey = 'LYNX'
		WHERE ( @CompanyName = '*' OR p.NAME LIKE @CompanyName )
			AND ( @PhoneNum = '*' OR p.Phone like @PhoneNum )
			AND p.[GUID] NOT In(SELECT HQGUID FROM #LynxDisabledShops WHERE IsDisabled = '1')              
			AND p.IsDeleted = 0
			AND p.[Disabled] = 0
			AND p.NAME LIKE (CASE WHEN @Sub_ShopName = '%' THEN '%'
									ELSE @Sub_ShopName
							  END )

		ORDER BY CASE WHEN @SortCd = 'DS'	THEN (CAST(SQRT(SQUARE(@X - z.X) + SQUARE(@Y - z.Y) + SQUARE(@Z - z.Z)) as Decimal(10,2))) END ASC
				, CASE WHEN @SortCd = 'SN'	THEN p.NAME END ASC
				, CASE WHEN @SortCd = 'CT'	THEN p.City END ASC
				, CASE WHEN @SortCd = 'ZIP'	THEN p.Zip END ASC
				, CASE WHEN @SortCd != 'SN' THEN p.NAME END ASC

		--SELECT * FROM #CombinedShops ORDER BY SelectedShopRank

		-----------------------------------
		-- Debug - Show Params
		-----------------------------------
		IF @Debug = 1
		BEGIN
			SET @DebugEventDescription = @ProcName + ' - Matching Logic - ShopGUID'			
			SET @DebugEventDetailedDescription = 'Removing HQ shops from @CombinedShops table that are already in APD' 	
			SET @DebugEventXML = ''
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
		END

		--=====================================
		-- Shop Matching and Clean-up
		--=====================================
		----------------------------------
		-- Remove HQ Shops that already
		-- exist in the APD ShopLocation
		-- database
		----------------------------------
		INSERT INTO #DupShops
		SELECT
			ShopGUID
			, Count(ShopGUID) as 'Duplicate' 
		FROM 
			#CombinedShops 
		GROUP BY
			ShopGUID
		HAVING
			COUNT(*) > 1
			AND LEN(ShopGUID) > 10

		IF EXISTS(SELECT * FROM #DupShops)
		BEGIN
			-----------------------------------
			-- Debug - Show Params
			-----------------------------------
			IF @Debug = 1
			BEGIN
				SET @DebugEventType = 'HQ/APD Combined Shop - APD'
				SET @DebugEventStatus = 'Debugging'					
				SET @DebugEventDescription = @ProcName + ' - HQ Duplicates Shops Exist in APD'			
				SET @DebugEventDetailedDescription = 'HQ Duplicate shop exist in APD and will need removed.' 	
				SET @DebugEventXML = ''
				SET @DebugSeq = @DebugSeq + 1

				PRINT @DebugEventDescription
				PRINT @DebugEventDetailedDescription
				PRINT @DebugEventXML
				INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
			END

			DELETE FROM #CombinedShops WHERE ShopGUID IN
			(
				SELECT DISTINCT
					cs1.ShopGUID 
				FROM 
					#DupHQAPDShops hs
					INNER JOIN #CombinedShops cs1
					ON
						cs1.zip = hs.zip
						AND cs1.PhoneAreaCode = hs.PhoneAreaCode
						AND cs1.PhoneExchangeNumber = hs.PhoneExchangeNumber
						AND cs1.PhoneUnitNumber = hs.PhoneUnitNumber
						AND cs1.source = 'HQ'
					LEFT JOIN #CombinedShops cs
					ON 
						cs.zip = hs.zip
						AND cs.PhoneAreaCode = hs.PhoneAreaCode
						AND cs.PhoneExchangeNumber = hs.PhoneExchangeNumber
						AND cs.PhoneUnitNumber = hs.PhoneUnitNumber
						AND cs.source = 'LYNX'
				WHERE
					cs.Source IS NOT NULL
			)	
			AND Source = 'HQ'

		END
		ELSE
		BEGIN
			-----------------------------------
			-- Debug - Show Params
			-----------------------------------
			IF @Debug = 1
			BEGIN
				SET @DebugEventType = 'HQ/APD Combined Shop - APD'
				SET @DebugEventStatus = 'Debugging'					
				SET @DebugEventDescription = @ProcName + ' - NO HQ Duplicates Shops Exist in APD'			
				SET @DebugEventDetailedDescription = 'No HQ Duplicate shop exist in APD.  Nothing to remove.' 	
				SET @DebugEventXML = ''
				SET @DebugSeq = @DebugSeq + 1

				PRINT @DebugEventDescription
				PRINT @DebugEventDetailedDescription
				PRINT @DebugEventXML
				INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
			END
		END

		-----------------------------------
		-- Debug - Show Params
		-----------------------------------
		IF @Debug = 1
		BEGIN
			SET @DebugEventDescription = @ProcName + ' - Matching Logic - Zip/Phone/Address'			
			SET @DebugEventDetailedDescription = 'Removing HQ shops from @CombinedShops table that match Zip/Phone/Address in APD' 	
			SET @DebugEventXML = ''
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
		END

		----------------------------------
		-- Remove HQ Shops MATCH APD Shops
		-- based on Zipcode, Phone number
		-- Numeric Address 1
		----------------------------------
		-- Clean-up table so I can reuse it
		-- Find Matches
		INSERT INTO #DupHQAPDShops
		SELECT
			Zip
			, PhoneAreaCode
			, PhoneExchangeNumber
			, PhoneUnitNumber 
			, Address1
			, COUNT(*) 'Match'
		FROM 
			#CombinedShops 
		GROUP BY
			Zip
			, PhoneAreaCode
			, PhoneExchangeNumber
			, PhoneUnitNumber
			, Address1
		HAVING
			COUNT(*) > 1

		DELETE FROM #CombinedShops WHERE ShopGUID IN
		(
			SELECT DISTINCT
				cs1.ShopGUID 
			FROM 
				#DupHQAPDShops hs
				INNER JOIN #CombinedShops cs1
				ON
					cs1.zip = hs.zip
					AND cs1.PhoneAreaCode = hs.PhoneAreaCode
					AND cs1.PhoneExchangeNumber = hs.PhoneExchangeNumber
					AND cs1.PhoneUnitNumber = hs.PhoneUnitNumber
					AND dbo.ufnGetNumeric(cs1.Address1) = dbo.ufnGetNumeric(hs.Address1)
					AND cs1.source = 'HQ'
				LEFT JOIN #CombinedShops cs
				ON 
					cs.zip = hs.zip
					AND cs.PhoneAreaCode = hs.PhoneAreaCode
					AND cs.PhoneExchangeNumber = hs.PhoneExchangeNumber
					AND cs.PhoneUnitNumber = hs.PhoneUnitNumber
					AND dbo.ufnGetNumeric(cs.Address1) = dbo.ufnGetNumeric(hs.Address1)
					AND cs.source = 'LYNX'
			WHERE
				cs.Source IS NOT NULL
		)	
		AND Source = 'HQ'

		DROP TABLE #Zip_Temp
	END -- Choice Client
	ELSE
	BEGIN
		-----------------------------------
		-- Debug - Show Params
		-----------------------------------
		IF @Debug = 1
		BEGIN
			SET @DebugEventType = 'HQ/APD Combined Shop - APD'
			SET @DebugEventStatus = 'Debugging'					
			SET @DebugEventDescription = @ProcName + ' - Not a Choice Client'			
			SET @DebugEventDetailedDescription = 'Client @InsuranceCompanyID: ' + CONVERT(VARCHAR(5), @InsuranceCompanyID) + ' is not a Choice Client.'
			SET @DebugEventXML = ''
			SET @DebugSeq = @DebugSeq + 1

			PRINT @DebugEventDescription
			PRINT @DebugEventDetailedDescription
			PRINT @DebugEventXML
			INSERT INTO utb_apd_event_log VALUES (@DebugEventType, @DebugEventStatus + '-' + CONVERT(VARCHAR(10), @DebugSeq), @DebugEventDescription, @DebugEventDetailedDescription, @DebugEventXML, 0, CURRENT_TIMESTAMP)
		END
	END
END

--==========================================
-- Main XML Processing
--==========================================
    SELECT  1 AS Tag
            , NULL AS Parent
			-- Root
			, @SearchID				AS [Root!1!ShopSearchLogID]
			, @InsuranceCompanyID	AS [Root!1!InsuranceCompanyID]
			, @Zip					AS [Root!1!Zip]
			, @ShopTypeCode			AS [Root!1!ShopTypeCode]
			--, @AssignmentAtSelectionFlag AS [Root!1!AssignmentAtSelection]
			, NULL AS [Root!1!AssignmentAtSelection]
			-- Shop
			, NULL					AS [Shop!2!Source]
			, NULL					AS [Shop!2!SelectedShopRank]
			, NULL					AS [Shop!2!Distance]
			, NULL					AS [Shop!2!ShopLocationID]
			, NULL					AS [Shop!2!SelectedShopScore]
			, NULL					AS [Shop!2!Address1]
			, NULL					AS [Shop!2!Address2]
			, NULL					AS [Shop!2!AddressCity]
			, NULL					AS [Shop!2!AddressState]
			, NULL					AS [Shop!2!AddressZip]
			, NULL					AS [Shop!2!DrivingDirections]
	        , NULL					AS [Shop!2!Name]
			, NULL					AS [Shop!2!OperatingMondayEndTime]
			, NULL					AS [Shop!2!OperatingMondayStartTime]
			, NULL					AS [Shop!2!OperatingTuesdayEndTime]
			, NULL					AS [Shop!2!OperatingTuesdayStartTime]
			, NULL					AS [Shop!2!OperatingWednesdayEndTime]
			, NULL					AS [Shop!2!OperatingWednesdayStartTime]
			, NULL					AS [Shop!2!OperatingThursdayEndTime]
			, NULL					AS [Shop!2!OperatingThursdayStartTime]
			, NULL					AS [Shop!2!OperatingFridayEndTime]
			, NULL					AS [Shop!2!OperatingFridayStartTime]
			, NULL					AS [Shop!2!OperatingSaturdayEndTime]
			, NULL					AS [Shop!2!OperatingSaturdayStartTime]
			, NULL					AS [Shop!2!OperatingSundayEndTime]
			, NULL					AS [Shop!2!OperatingSundayStartTime]
			, NULL					AS [Shop!2!PhoneAreaCode]	
			, NULL					AS [Shop!2!PhoneExchangeNumber]	
			, NULL					AS [Shop!2!PhoneUnitNumber]	
			, NULL					AS [Shop!2!PhoneExtensionNumber]	
			, NULL					AS [Shop!2!FaxAreaCode]	
			, NULL					AS [Shop!2!FaxExchangeNumber]	
			, NULL					AS [Shop!2!FaxUnitNumber]	
			, NULL					AS [Shop!2!FaxExtensionNumber]
			, NULL					AS [Shop!2!ShopOpen]	
			, NULL					AS [Shop!2!TimeAtShop]	
			, NULL					AS [Shop!2!TimeZone]	
			, NULL					AS [Shop!2!WarrantyPeriodRefinishCD]	
			, NULL					AS [Shop!2!WarrantyPeriodWorkmanshipCD]	
			, NULL					AS [Shop!2!CertifiedFirstFlag]
			, NULL					AS [Shop!2!ShopGUID]
			, NULL					AS [Shop!2!Sort]
			--, NULL					AS [Shop!2!ContactName]	
			--, NULL					AS [Shop!2!ContactPhoneAreaCode]	
			--, NULL					AS [Shop!2!ContactPhoneExchangeNumber]
			--, NULL					AS [Shop!2!ContactPhoneExtensionNumber]	
			--, NULL					AS [Shop!2!ContactPhoneUnitNumber]

UNION ALL

    SELECT DISTINCT 2
            , 1
            , NULL, NULL, NULL, NULL, NULL
			, Source
			, SelectedShopRank
			, Distance
			, ShopLocationID
			, SelectedShopScore
			, Address1
			, Address2
			, AddressCity
			, AddressState
			, AddressZip
			, DrivingDirections
	        , Name
			, OperatingMondayEndTime
			, OperatingMondayStartTime
			, OperatingTuesdayEndTime
			, OperatingTuesdayStartTime
			, OperatingWednesdayEndTime
			, OperatingWednesdayStartTime
			, OperatingThursdayEndTime
			, OperatingThursdayStartTime
			, OperatingFridayEndTime
			, OperatingFridayStartTime
			, OperatingSaturdayEndTime
			, OperatingSaturdayStartTime
			, OperatingSundayEndTime
			, OperatingSundayStartTime
			, PhoneAreaCode	
			, PhoneExchangeNumber	
			, PhoneUnitNumber	
			, PhoneExtensionNumber	
			, FaxAreaCode	
			, FaxExchangeNumber	
			, FaxUnitNumber	
			, FaxExtensionNumber	
			, ShopOpen	
			, TimeAtShop	
			, TimeZone	
			, WarrantyPeriodRefinishCD	
			, WarrantyPeriodWorkmanshipCD	
			, CertifiedFirstFlag
			, ShopGUID	
			, CASE  WHEN Source = 'LYNX' AND Distance <= 5 THEN -1 ELSE Distance END Sort	
			--, ShopContactName	
			--, ShopContactPhoneAreaCode	
			--, ShopContactPhoneExchangeNumber	
			--, ShopContactPhoneExtensionNumber	
			--, ShopContactPhoneUnitNumber
	FROM
		#CombinedShops 
	WHERE
		Distance <= @TargetDistance
	ORDER BY
		[Shop!2!Sort],[Shop!2!Distance]

FOR XML EXPLICIT

	--SELECT * FROM #CombinedShops ORDER BY SelectedShopRank

	DROP TABLE #CombinedShops
	DROP TABLE #DupShops
	DROP TABLE #DupHQAPDShops
	DROP TABLE #LynxDisabledShops
END
GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopCombinedSearchByDistanceWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopCombinedSearchByDistanceWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
************************************************************************************************************************/
