-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCRUDGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCRUDGetDetailWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCRUDGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns CRUD information for all entities passed in @EntityList
*
* PARAMETERS:  
* (I) @EntityList           A comma delimited list of entities
* (I) @UserID               User to get CRUD for
*
* RESULT SET:
* XML Stream containg CRUD for all the entities requested
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCRUDGetDetailWSXML
    @EntityList     udt_std_desc_long,
    @UserID         udt_std_id
AS
BEGIN
    IF LEN(RTRIM(LTRIM(@EntityList))) = 0 SET @EntityList = NULL


    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCRUDGetDetailWSXML'


    -- Check to make sure a valid Entity List and User ID

    IF  (@EntityList IS NULL)
    BEGIN
        -- Invalid Entity List
    
        RAISERROR('101|%s|@EntityList|%s', 16, 1, @ProcName, @EntityList)
        RETURN
    END

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- In order to be able to use the GetCRUD function, it will be necessary to build the output as a 2-step process.
    -- This entails first gathering the entities into a temporary table.  We will then step through the table
    -- using a CURSOR, calling the GetCRUD function for each row to populate the CRUD flags.


    -- Define local variables
    DECLARE @Entity         udt_std_name,
            @CreateFlag     udt_std_flag,
            @DeleteFlag     udt_std_flag,
            @ReadFlag       udt_std_flag,
            @UpdateFlag     udt_std_flag


    -- Declare table variable

    DECLARE @tmpPermission TABLE
    (
        Entity      varchar(50),
        CreateFlag  bit,
        DeleteFlag  bit,
        ReadFlag    bit,
        UpdateFlag  bit
    )


    -- Select into the temporary table

    INSERT INTO @tmpPermission
      SELECT  value AS Entity,
              NULL AS CreateFlag,
              NULL AS DeleteFlag,
              NULL AS ReadFlag,
              NULL AS UpdateFlag
        FROM  dbo.ufnUtilityParseString( @EntityList, ',', 1 )    -- 1 denotes to trim leading, training spaces


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpPermission', 16, 1, @ProcName)
        RETURN
    END
    

    -- Define the cursor        

    DECLARE Permission_Cursor CURSOR FOR
        SELECT  Entity
        FROM    @tmpPermission
    FOR UPDATE OF CreateFlag, DeleteFlag, ReadFlag, UpdateFlag

    OPEN Permission_Cursor

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Start walking through the records

    FETCH NEXT FROM Permission_Cursor
    INTO @Entity

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
 

    WHILE @@FETCH_STATUS = 0
    BEGIN

        -- Call CRUD for this permission and store values in local variables

        SELECT  @CreateFlag = CreateFlag,
                @DeleteFlag = DeleteFlag,
                @ReadFlag = ReadFlag,
                @UpdateFlag = UpdateFlag

        FROM    ufnUtilityGetCRUD( @UserID, @Entity, DEFAULT, DEFAULT )


        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        -- Update the cursor

        UPDATE  @tmpPermission
        SET     CreateFlag = @CreateFlag,
                DeleteFlag = @DeleteFlag,
                ReadFlag = @ReadFlag,
                UpdateFlag = @UpdateFlag
        WHERE CURRENT OF Permission_Cursor


        IF @@ERROR <> 0
        BEGIN
            -- Update failure
    
            RAISERROR('104|%s|@tmpPermission', 16, 1, @ProcName)
            RETURN
        END


        -- Move on to the next cursor record

        FETCH NEXT FROM Permission_Cursor
        INTO @Entity

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END


    -- Close and deallocate the cursor

    CLOSE Permission_Cursor
    DEALLOCATE Permission_Cursor


    -- Final XML Select
    
    SELECT  1 AS tag,
            NULL AS parent,
            @EntityList AS [Root!1!EntityList],
            @UserID AS [Root!1!UserID],
            -- Permission List
            NULL AS [Crud!2!Entity],
            NULL AS [Crud!2!Insert],
            NULL AS [Crud!2!Select],
            NULL AS [Crud!2!Update],
            NULL AS [Crud!2!Delete]


    UNION ALL


    -- Select Permission List Level

    SELECT  2,
            1,
            NULL, NULL,
            -- Permission List
            Entity,
            CreateFlag,
            ReadFlag,
            UpdateFlag,
            DeleteFlag

    FROM    @tmpPermission

    ORDER BY [Crud!2!Entity]

    FOR XML EXPLICIT      -- Comment for Client-side processing

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCRUDGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCRUDGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCRUDGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/