-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimReassignmentGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimReassignmentGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimReassignmentGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves the list of users for reassignment selection
*
* PARAMETERS:  
* (I) @LynxID               LynxID of claim being reassigned
* (I) @UserID               User ID of the person doing the reassignment (For use with ufnUtiltyOrgChartGetList)
*
* RESULT SET:
* XML Document containing list of names
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimReassignmentGetListXML
    @LynxID         udt_std_id_big,
    @ClaimAspectID  udt_std_id_big,
    @Entity         udt_std_desc_short,
    @UserID         udt_std_id,
    @ApplicationCD  udt_std_cd = 'APD'
AS
BEGIN
    -- Declare internal variables

    DECLARE @tmpUserList TABLE
    (
        UserID      int
    )
    
    --Project:210474 APD Added the table below when we did the code merge M.A.20061114
    DECLARE @tmpAssignmentPool TABLE
    (
        AssignmentPoolID      int,
        Name                  varchar(100),
        FunctionCD            varchar(10)
    )
    
    DECLARE @ClaimAspectTypeIDVehicle       udt_std_id
    DECLARE @ClaimAspectTypeIDProperty      udt_std_id
    DECLARE @ServiceChannelCD               udt_std_cd

    DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimReassignmentGetListXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate APD Data state
    
    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('%s: SQL Server Error.', 16, 1, @ProcName)
        RETURN
    END    

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN    
        -- Claim Aspect Type Not Found
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    
    SELECT  @ClaimAspectTypeIDProperty = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Property'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('%s: SQL Server Error.', 16, 1, @ProcName)
        RETURN
    END    

    IF @ClaimAspectTypeIDProperty IS NULL
    BEGIN    
        -- Claim Aspect Type Not Found
    
        RAISERROR('102|%s|"Property"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Continue to validate APD Data State

    --Project:210474 APD Added the following when we did the code merge M.A.20061114
    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Receive Shop Program Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Receive Shop Program Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Receive Damage Reinspections')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Receive Damage Reinspections"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Receive Desk Audits')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Receive Desk Audits"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Receive Mobile Electronics Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Receive Mobile Electronics Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Receive Repair Referral Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Receive Repair Referral Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Analyze Shop Program Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Analyze Shop Program Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Analyze Desk Audits')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Analyze Desk Audits"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Analyze Damage Reinspections')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Analyze Damage Reinspections"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Analyze Mobile Electronics Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Analyze Mobile Electronics Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Analyze Repair Referral Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Analyze Repair Referral Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Administrate Shop Program Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Administrate Shop Program Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Administrate Desk Audits')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Administrate Desk Audits"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Administrate Damage Reinspections')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Administrate Damage Reinspections"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Administrate Mobile Electronics Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Administrate Mobile Electronics Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT ProfileID FROM dbo.utb_profile WHERE Name = 'Administrate Repair Referral Claims')
    BEGIN
        -- Profile Attribute Not Found
    
        RAISERROR('102|%s|"Administrate Repair Referral Claims"|utb_profile', 16, 1, @ProcName)
        RETURN
    END
    --Project:210474 APD Added the above when we did the code merge M.A.20061114

    -- First check to see if this claim is all desk reviews.  If so, retrieve all users for possible reassignment.  If not,
    -- use the "classic" mechanism and retrieve just users who are subordinates of the current user.

    SELECT  @ServiceChannelCD = CASE
                                  WHEN ClaimAspectTypeID = @ClaimAspectTypeIDProperty THEN 'PS'  -- Properties are treated like program shop vehicles for assignment
                                  WHEN ClaimAspectTypeID = @ClaimAspectTypeIDVehicle THEN IsNull(casc.ServiceChannelCD, 'PS')
                                  ELSE 'PS' -- If undetermined service channel, treat as a program shop vehicle
                                END
      FROM  dbo.utb_claim_aspect ca
      --Project:210474 APD Remarked-off to support the schema change M.A.20061219
      --LEFT JOIN dbo.utb_assignment_type at ON (ca.CurrentAssignmentTypeID = at.AssignmentTypeID) --Project:210474 APD The column was added in utb_Claim_Aspect so changed it back to the original state. M.A.20061201
      --Project:210474 APD Added the following to support the schema change M.A.20061219
      Left Outer Join utb_Claim_Aspect_Service_Channel casc
      on ca.ClaimAspectID = casc.ClaimAspectID
      and casc.PrimaryFlag = 1
      WHERE ca.ClaimAspectID = @ClaimAspectID
              
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('%s: SQL Server Error.', 16, 1, @ProcName)
        RETURN
    END    

--Project:210474 APD Added the following when we did the code merge M.A.20061114
    IF @Entity = 'Claim'
    BEGIN
        -- This is a non-desk review or mixed claim.  Get the list of subordinates for reassignment.
        
        INSERT INTO @tmpUserList
          SELECT  NewUserID
            FROM  dbo.ufnUtilityOrgChartGetList(@UserID)
            WHERE NewUserID <> @UserID
              AND dbo.ufnUtilityIsUserActive(NewUserID, @ApplicationCD, NULL) = 1
    END
    ELSE IF @Entity = 'Task'
    BEGIN
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Shop Program Claims', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Shop Program Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Shop Program Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Desk Audits', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Desk Audits', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Desk Audits', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Damage Reinspections', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Damage Reinspections', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Damage Reinspections', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Repair Referral Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Repair Referral Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Repair Referral Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'PS' 
    BEGIN
        -- This entity is shop program, retrieve all users who have "Receive Shop Program Claims" or 
        -- "Analyze Program Shop Claims" or "Administrate Program Shop Claims" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Shop Program Claims', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Shop Program Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Shop Program Claims', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'DR'
    BEGIN
        -- This entity is Damage Reinspections, retrieve all users who have "Receive Damage Reinspections" or 
        -- "Analyze Damage Reinspections" or "Administrate Damage Reinspections" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Damage Reinspections', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Damage Reinspections', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Damage Reinspections', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'DA'
    BEGIN
        -- This entity is Damage Reinspections, retrieve all users who have "Receive Damage Reinspections" or 
        -- "Analyze Damage Reinspections" or "Administrate Damage Reinspections" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Desk Audits', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Desk Audits', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Desk Audits', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'ME'
    BEGIN
        -- This entity is Damage Reinspections, retrieve all users who have "Receive Damage Reinspections" or 
        -- "Analyze Damage Reinspections" or "Administrate Damage Reinspections" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Mobile Electronics Claims', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'RRP'
    BEGIN
        -- This entity is Repair Referral, retrieve all users who have "Receive Repair Referral Claims" or 
        -- "Analyze Repair Referral Claims" or "Administrate Repair Referral Claims" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  
              AND ((dbo.ufnUtilityGetProfileValue ( u.UserID, 'Receive Repair Referral Claims', DEFAULT, DEFAULT ) = 1)  OR
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Analyze Repair Referral Claims', DEFAULT, DEFAULT ) = 1)  OR 
                   (dbo.ufnUtilityGetProfileValue ( u.UserID, 'Administrate Repair Referral Claims', DEFAULT, DEFAULT ) = 1)) 
    END
    ELSE IF @ServiceChannelCD = 'IA' -- TODO: Verify the profiles used
    BEGIN
        -- This entity is Damage Reinspections, retrieve all users who have "Receive Damage Reinspections" or 
        -- "Analyze Damage Reinspections" or "Administrate Damage Reinspections" set to 1
        
        INSERT INTO @tmpUserList
          SELECT  UserID
            FROM  dbo.utb_user u
            WHERE (dbo.ufnUtilityIsUserActive ( u.UserID, @ApplicationCD, NULL ) = 1)  

    END
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpUserList', 16, 1, @ProcName)
        RETURN
    END    
    
    INSERT INTO @tmpAssignmentPool
    SELECT AssignmentPoolID,
           Name,
           FunctionCD
    FROM dbo.utb_assignment_pool
    WHERE EnabledFlag = 1
    ORDER BY DisplayOrder
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpAssignmentPool', 16, 1, @ProcName)
        RETURN
    END    


    -- Begin XML Select
    
    SELECT  1 AS Tag,
            NULL AS Parent,
            @LynxID AS [Root!1!LynxID],
            @UserID AS [Root!1!UserID],
            SupervisorFlag AS [Root!1!SupervisorFlag],
            -- ClaimAspect info
            NULL AS [ClaimAspect!2!ClaimAspectID],
            NULL AS [ClaimAspect!2!OwnerUserID],
            NULL AS [ClaimAspect!2!AnalystUserID],
            NULL AS [ClaimAspect!2!SupportUserID],
            NULL AS [ClaimAspect!2!FOhasSupervisor],
            NULL AS [ClaimAspect!2!FAhasSupervisor],
            NULL AS [ClaimAspect!2!FShasSupervisor],
            -- User Level
            NULL AS [User!3!UserID],
            NULL AS [User!3!NameFirst],
            NULL AS [User!3!NameLast],
            -- User Assignment Pool
            NULL AS [Pool!4!AssignmentPoolID],
            NULL AS [Pool!4!FunctionCD],
            --Assignment Pools
            NULL AS [AssignmentPool!5!AssignmentPoolID],
            NULL AS [AssignmentPool!5!Name],
            NULL AS [AssignmentPool!5!FunctionCD]
    FROM dbo.utb_user
    WHERE userID = @UserID

    UNION ALL
    
    SELECT  2,
            1,
            NULL, NULL, NULL,
            -- ClaimAspect Info
            @ClaimAspectID,
            isNull(ca.OwnerUserID, 0),
            isNull(ca.AnalystUserID, 0),
            isNull(ca.SupportUserID, 0),
            CASE
                WHEN uo.SupervisorUserID > 0 THEN 1
                ELSE 0 
            END,
            CASE
                WHEN ua.SupervisorUserID > 0 THEN 1
                ELSE 0 
            END,
            CASE
                WHEN us.SupervisorUserID > 0 THEN 1
                ELSE 0 
            END,
            -- User Level
            NULL, NULL, NULL,
            -- User assignment pool
            NULL, NULL,
            --AssignmentPool
            NULL, NULL, NULL

    FROM utb_claim_aspect ca
    LEFT JOIN dbo.utb_user uo ON (ca.OwnerUserID = uo.UserID)
    LEFT JOIN dbo.utb_user ua ON (ca.AnalystUserID = ua.UserID)
    LEFT JOIN dbo.utb_user us ON (ca.SupportUserID = us.UserID)
    WHERE ca.ClaimAspectID = @ClaimAspectID 
            
    
    UNION ALL
    
    
    SELECT  3,
            1,
            NULL, NULL, NULL,
            -- ClaimAspect Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Level
            u.UserID,
            u.NameFirst,
            u.NameLast,
            -- User assignment pool
            NULL, NULL,
            --AssignmentPool
            NULL, NULL, NULL
      
      FROM  @tmpUserList tmp
      LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
      
    UNION ALL
    
    
    SELECT  4,
            3,
            NULL, NULL, NULL,
            -- ClaimAspect Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Level
            u.UserID,
            NULL,
            NULL,
            -- User assignment pool
            ap.AssignmentPoolID, 
            ap.FunctionCD,
            --AssignmentPool
            NULL, NULL, NULL
      
      FROM  @tmpUserList tmp
      LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
      LEFT JOIN dbo.utb_assignment_pool_user apu ON (tmp.UserID = apu.UserID)
      LEFT JOIN dbo.utb_assignment_pool ap ON (apu.AssignmentPoolID = ap.AssignmentPoolID)
      
    UNION ALL
    
    SELECT  5,
            1,
            NULL, NULL, NULL,
            -- ClaimAspect Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- User Level
            NULL, NULL, NULL,
            -- User assignment pool
            NULL, NULL,
            --AssignmentPool
            AssignmentPoolID, 
            Name,
            FunctionCD
      
      FROM  @tmpAssignmentPool
      
    ORDER BY [User!3!UserID], tag --, [User!3!NameLast], [User!3!NameFirst]
--Project:210474 APD Added the above when we did the code merge M.A.20061114
--    FOR XML EXPLICIT  (Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimReassignmentGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimReassignmentGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/