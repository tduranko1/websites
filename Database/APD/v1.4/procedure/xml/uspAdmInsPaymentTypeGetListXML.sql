-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsPaymentTypeGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsPaymentTypeGetListXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsPaymentTypeGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieves Selected Payment Types for a Given Insurance Company
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   ID of the insurance company
*
* RESULT SET:
* XML Stream containg Claim Aspects and reference data.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmInsPaymentTypeGetListXML
    @InsuranceCompanyID udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now               AS DateTime          -- Timestamp for Procedure

    SET @ProcName = 'uspAdmInsPaymentTypeGetListXML'
    SET @Now = Current_TimeStamp

    --Verify that a valid Insurance Company was passed in.
    IF (@InsuranceCompanyID is Null) OR
    NOT EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance CompanyID
    
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    -- Gather metadata for all entities updatable columns

    DECLARE @tmpMetadata TABLE 
    (
        GroupName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )

    -- MetaData
    INSERT INTO @tmpMetadata (GroupName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'PaymentType',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_client_payment_type' AND Column_Name IN
            ('InsuranceCompanyID',
             'PaymentTypeCD'))

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    -- Gather Reference Data

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50)             NOT NULL,
        ReferenceID     varchar(10)             NOT NULL,
        Name            varchar(50)             NOT NULL
    )

   INSERT INTO @tmpReference (ListName, ReferenceID, Name)

    SELECT  'PaymentType', Code, Name
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_client_payment_type', 'PaymentTypeCD')
    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    --Begin Select Statement
    SELECT
        1                       AS tag,
        Null                    AS Parent,
        @InsuranceCompanyID     AS [Root!1!InsuranceCompanyID],
        
        --AspectType
        Null                    AS [ClientPaymentType!2!PaymentTypeCD],
        Null                    AS [ClientPaymentType!2!SysLastUpdatedDate],
        
        -- MetaData
        Null                    AS [Metadata!3!Entity],
        
        -- Columns
        Null                    AS [Column!4!Name],
        Null                    AS [Column!4!DataType],
        Null                    AS [Column!4!MaxLength],
        NULL                    AS [Column!4!Precision],
        NULL                    AS [Column!4!Scale],
        Null                    AS [Column!4!Nullable],
        
        -- Reference Data
        Null                    AS [Reference!5!List],
        Null                    AS [Reference!5!ReferenceID],
        Null                    AS [Reference!5!Name]

    UNION ALL

    SELECT
        2,
        1,
        Null,
        
        --Aspect
        cpt.PaymentTypeCD,
        dbo.ufnUtilityGetDateString(cpt.SysLastUpdatedDate),
        
        --MetaData
        Null,
        
        --Columns
        Null, Null, Null, Null, Null, Null,
        
        --Reference
        Null, Null, Null

    FROM
        dbo.utb_client_payment_type cpt
        
    WHERE
        InsuranceCompanyID = @InsuranceCompanyID

--****************************************************************************

    UNION ALL

    --  MetaData Level

    SELECT DISTINCT
        3 AS tag,
        1 AS parent,
        Null,
        -- Aspect
        Null, Null,
         -- MetaData
        GroupName,
        -- Columns
        Null, Null, Null, Null, Null, Null,
        -- Reference Data
        Null, Null, Null

    FROM
        @tmpMetadata


    UNION ALL
   

    -- Column Level

    SELECT
        4 as tag,
        3 as parent,
        Null,
        -- Aspect
        Null, Null,
        -- MetaData
        GroupName,
        -- Columns
        ColumnName,
        DataType,
        MaxLength,
        NumericPrecision,
        Scale,
        Nullable, 
        -- Reference Data
        Null, Null, Null

    FROM
        @tmpMetadata


    UNION ALL
   
--****************************************************************************

    -- Reference Level

    SELECT
        5 as tag,
        1 as parent,
        Null,
        -- Aspect
        Null, Null,
        -- MetaData
        'ZZ-Reference',
        -- Columns
        Null, Null, Null, Null, Null, Null,
        -- Reference Data
        ListName,
        ReferenceID,
        Name

    FROM
        @tmpReference

    ORDER BY [Metadata!3!Entity], tag
--    FOR XML EXPLICIT  --(Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
       

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsPaymentTypeGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsPaymentTypeGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/