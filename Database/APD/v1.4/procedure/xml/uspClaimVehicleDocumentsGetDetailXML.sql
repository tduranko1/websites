-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleDocumentsGetDetailXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVehicleDocumentsGetDetailXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVehicleDocumentsGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Retrieves documents for a claim aspect
*
* PARAMETERS:  
* (I) @ClaimAspectId        The Claim Aspect Id of the vehicle being retrieved
* (I) @InsuranceCompanyID   The Insurance company to validate against. 
* (I) @DocumentCd			'A' = All Documents; 'E' = Estimates only, 'N' = Non Estimates only
*
* RESULT SET:
* An XML Data stream containing claim vehicle and involved person information
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimVehicleDocumentsGetDetailXML
	@ClaimAspectID		udt_std_int_big,
	@InsuranceCompanyID udt_std_id,
	@DocumentCD			varchar(1) = 'A'
AS
BEGIN

	DECLARE @EstimateSummaryTypeIDNetTotal    udt_std_id
	DECLARE @EstimateSummaryTypeIDRepairTotal udt_std_id
	DECLARE @EstimateSummaryTypeIDBetterment  udt_std_id
	DECLARE @EstimateSummaryTypeIDDeductible  udt_std_id
	DECLARE @EstimateSummaryTypeIDOthAdj      udt_std_id
	DECLARE @ExposureCD                     udt_std_cd
	DECLARE @InsuranceCompanyIDClaim        udt_std_id
	DECLARE @ClaimAspectNumber              udt_std_id
	DECLARE @ClaimAspectTypeID              udt_std_id
	DECLARE @ClaimAspectIDCheck             udt_std_id_big
	DECLARE @LynxID                         udt_std_id_big
	DECLARE @PertainsTo                     varchar(8)
	DECLARE @CountNotes                     udt_std_int
	DECLARE @CountTasks                     udt_std_int
	DECLARE @CountBilling                   udt_std_int
	DECLARE @ActiveReinspection             bit
	DECLARE @VehicleStatus                  varchar(30)
	DECLARE @VehicleOpenStatusID            udt_std_id


	DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

	SET @ProcName = 'uspClaimVehicleDocumentsGetDetailXML'

	SET NOCOUNT ON

	-- Get Claim Aspect Type ID

	SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
	  FROM  dbo.utb_claim_aspect_type
	  WHERE Name = 'Vehicle'

	IF @@ERROR <> 0
	BEGIN
	   -- SQL Server Error

		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END

	IF @ClaimAspectTypeID IS NULL
	BEGIN
	   -- Claim Aspect Not Found

		RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
		RETURN
	END


	-- Check to make sure a valid Claim Aspect ID was passed in, pull the information we'll need on the claim aspect

	IF  (@ClaimAspectID <> 0)
	BEGIN
		SELECT  @ClaimAspectIDCheck = ClaimAspectID,
				@ClaimAspectNumber = ClaimAspectNumber,
				@LynxID = LynxID
		  FROM  dbo.utb_claim_aspect 
		  WHERE ClaimAspectID = @ClaimAspectID 
			AND ClaimAspectTypeID = @ClaimAspectTypeID


		IF (@ClaimAspectIDCheck IS NULL)
		BEGIN
			-- Invalid Claim Aspect ID

			RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
			RETURN
		END
	END
	
	IF @DocumentCD NOT IN ('A', 'E', 'N')
	BEGIN
		SET @DocumentCD = 'A'
	END


	-- Get the Insurance Company Id for the claim

	SELECT  @InsuranceCompanyIDClaim = InsuranceCompanyID 
	  FROM  dbo.utb_claim_aspect ca
	  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
	  WHERE ClaimAspectID = @ClaimAspectID

	IF @@ERROR <> 0
	BEGIN
	   -- SQL Server Error

		RAISERROR('99|%s', 16, 1, @ProcName)
		RETURN
	END


	-- Validate it against what has been passed in

	IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
	BEGIN
		-- Insurance Company ID does not match

		RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
		RETURN
	END

    -- Validate APD Data state
    
    SELECT  @EstimateSummaryTypeIDNetTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'NetTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDNetTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'TT' 
        AND Name = 'RepairTotal'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @EstimateSummaryTypeIDBetterment = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'AJ' 
        AND Name = 'Betterment'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDBetterment IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT  @EstimateSummaryTypeIDDeductible = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'AJ' 
        AND Name = 'Deductible'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDDeductible IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT  @EstimateSummaryTypeIDOthAdj = EstimateSummaryTypeID 
      FROM  dbo.utb_estimate_summary_type 
      WHERE CategoryCD = 'AJ' 
        AND Name = 'Other'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EstimateSummaryTypeIDOthAdj IS NULL
    BEGIN
       -- Estimate Summary Type Not Found
    
        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
            
	DECLARE @tmpDocument TABLE
	(
		DocumentID  bigint
	)

	IF @DocumentCD = 'E'
	BEGIN
		INSERT INTO @tmpDocument
		  SELECT  cascd.DocumentID
			FROM  dbo.utb_claim_aspect_service_channel casc
			LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE casc.ClaimAspectID = @ClaimAspectID
			  AND d.EnabledFlag = 1         -- Only return enabled
			  AND dt.EstimateTypeFlag = 1   -- Only interested in estimates
			  AND dt.Name not in ('Note') -- Not interested in notes
	END

	IF @DocumentCD = 'N'
	BEGIN
		INSERT INTO @tmpDocument
		  SELECT  cascd.DocumentID
			FROM  dbo.utb_claim_aspect_service_channel casc
			LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE casc.ClaimAspectID = @ClaimAspectID
			  AND d.EnabledFlag = 1         -- Only return enabled
			  AND dt.EstimateTypeFlag = 0   -- Only interested in estimates
			  AND dt.Name not in ('Note') -- Not interested in notes
	END

	IF @DocumentCD = 'A'
	BEGIN
		INSERT INTO @tmpDocument
		  SELECT  cascd.DocumentID
			FROM  dbo.utb_claim_aspect_service_channel casc
			LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE casc.ClaimAspectID = @ClaimAspectID
			  AND d.EnabledFlag = 1         -- Only return enabled
			  AND dt.Name not in ('Note') -- Not interested in notes
	END

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            -- Document
            NULL AS [Document!2!DocumentID],
            NULL AS [Document!2!DocumentSourceName],
            NULL AS [Document!2!VANFlag],
            NULL AS [Document!2!DocumentTypeID],
            NULL AS [Document!2!DocumentTypeName],
            NULL AS [Document!2!CreatedDate],
            NULL AS [Document!2!ReceivedDate],
            NULL AS [Document!2!ImageLocation],
            NULL AS [Document!2!SupplementSeqNumber],
            NULL AS [Document!2!FullSummaryExistsFlag],
            NULL AS [Document!2!AgreedPriceMetCD],
            NULL AS [Document!2!GrossEstimateAmt],
            NULL AS [Document!2!NetEstimateAmt],
            NULL AS [Document!2!BettermentAmt],
            NULL AS [Document!2!DeductibleAmt],
            NULL AS [Document!2!OtherAdjustmentsAmt],
            NULL AS [Document!2!EstimateTypeFlag],
            NULL AS [Document!2!DirectionToPayFlag],
            NULL AS [Document!2!FinalEstimateFlag],
            NULL AS [Document!2!DuplicateFlag],
            NULL AS [Document!2!EstimateTypeCD],
            NULL AS [Document!2!ServiceChannelCD],
            NULL AS [Document!2!SysLastUpdatedDateDocument],
            NULL AS [Document!2!SysLastUpdatedDateEstimate]

	UNION ALL
	
	SELECT  2,
			1,
			NULL, NULL,
			-- Document
			IsNull(d.DocumentID, 0),
			IsNull(ds.Name, ''),
			ds.VANFlag,
			dt.DocumentTypeID,
			IsNull(dt.Name, ''),
			IsNull(d.CreatedDate, ''),
			IsNull(d.ReceivedDate, ''),
			IsNull(d.ImageLocation, ''),
			IsNull(d.SupplementSeqNumber, 0),
			IsNull(d.FullSummaryExistsFlag, 0),
			IsNull(d.AgreedPriceMetCD, ''),
			IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
						AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), ''),
			IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
					  AND EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotal), ''),
			IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
					  AND EstimateSummaryTypeID = @EstimateSummaryTypeIDBetterment), ''),
			IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
					  AND EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductible), ''),
			IsNull((SELECT Convert(varchar(15), OriginalExtendedAmt)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
					  AND EstimateSummaryTypeID = @EstimateSummaryTypeIDOthAdj), ''),
			IsNull(dt.EstimateTypeFlag, 0),
			IsNull(d.DirectionToPayFlag, 0),
			IsNull(d.FinalEstimateFlag, 0),
			IsNull(d.DuplicateFlag, 0),
			IsNull(d.EstimateTypeCD, ''),          
			casc.ServiceChannelCD,
			dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
			IsNull((SELECT dbo.ufnUtilityGetDateString(SysLastUpdatedDate)
					  FROM  dbo.utb_estimate_summary 
					  WHERE DocumentID = D.DocumentID 
						AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal), '')

	FROM    @tmpDocument tmpD                -- Added to return blank claim even if no documents exist
	LEFT JOIN dbo.utb_Document d on tmpD.DocumentID = d.DocumentID
	LEFT JOIN dbo.utb_User u on d.CreatedUserID = u.UserID
	LEFT JOIN dbo.utb_role r on d.CreatedUserRoleID = r.RoleID
	LEFT JOIN dbo.utb_Document_Type dt on D.DocumentTypeID = dt.DocumentTypeID
	LEFT JOIN dbo.utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
	LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
	LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID

    ORDER BY Tag
--    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVehicleDocumentsGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVehicleDocumentsGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO