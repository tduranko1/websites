-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRelationshipGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRelationshipGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspRelationshipGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns list of relationships
*
* PARAMETERS:  
* None
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspRelationshipGetListWSXML]
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRelationshipGetListWSXML'


    SELECT
        1 as tag,
        Null as Parent,
--        Root
        Null as [Root!1!Root],
--        State
        Null as [State!2!RelationID],
        Null as [State!2!DisplayOrder!Hide],
        Null as [State!2!Name]

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null,
--        State
        RC.RelationID,
        RC.DisplayOrder,
        RC.Name

    FROM
        utb_relation RC

    WHERE
        RC.EnabledFlag = 1

    ORDER BY tag, [State!2!DisplayOrder!Hide]
    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRelationshipGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRelationshipGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspRelationshipGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/