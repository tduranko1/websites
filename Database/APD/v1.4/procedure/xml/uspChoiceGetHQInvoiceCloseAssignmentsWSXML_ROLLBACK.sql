-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceGetHQInvoiceCloseAssignmentsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceGetHQInvoiceCloseAssignmentsWSXML 
END

GO 
