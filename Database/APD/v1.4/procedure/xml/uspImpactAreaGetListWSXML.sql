-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspImpactAreaGetListWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspImpactAreaGetListWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspImpactAreaGetListWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Return all the impact points
*
* PARAMETERS:  
* (I) @input                None
* (O) @output               XML
* (I) @ModifiedDateTime     2014-01-15
*
* RESULT SET:
*	XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspImpactAreaGetListWSXML]
    -- (@parameter_name datatype [OUTPUT],...)
AS
BEGIN
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspImpactAreaGetListWSXML'


    -- Set Database options
    
    SET NOCOUNT ON
    
    SELECT
        1 as Tag,
        0 as Parent,
        -- Root
        NULL as [Root!1!Root],
        -- Impact Area
        NULL as [Impact!2!ImpactID],
        NULL as [Impact!2!DisplayOrder],
        NULL as [Impact!2!Name]
   
   UNION ALL
   
   SELECT 
        2,
        1,
        -- Root
        NULL,
        -- Impact Area
        ImpactID,
        DisplayOrder,
        Name
    FROM dbo.utb_impact
    WHERE EnabledFlag = 1

    FOR XML EXPLICIT      
       
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspImpactAreaGetListWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspImpactAreaGetListWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspImpactAreaGetListWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/