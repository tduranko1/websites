-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetJobDetailsWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetJobDetailsWSXML 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetJobDetailsWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns a list of Hyperquest job details in queue as XML
* DATE:			23Oct2014
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* VERSION:		1.0 - Initial Development
*				1.1 - 27Jul2015 - TVD - Added HQ Assignments
*				1.2 - 18Aug2015 - TVD - Added temp table so XML output could be ORDERED
*				1.3 - 11Jan2016 - TVD - Corrected and issue with a null JobXslFile
*				1.4 - 14Feb2017 - TVD - Modified to look for Unprocessed or Waiting jobs
*				1.5 - 14Aug2017 - TVD - Faking the ImageType for ADP Photos as per Richard
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetJobDetailsWSXML]
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspHyperquestGetJobDetailsWSXML'
    
    SET @ApplicationCD = 'HyperquestPartner'

    -- Validation
    
    -- Create temp table so XML output can be ordered
	SELECT 
		[JobID]
		, [InscCompID]
		, [LynxID]
		, [ClaimNumber]
		, [DocumentID]
		, [AssignmentID]
		, [ReceivedDate]
		, [DocumentTypeID]
		, [DocumentTypeName]
		, [DocumentSource]
		, CASE 
	 		WHEN UPPER(DocumentSource) = 'ADP' AND UPPER(DocumentImageType) = 'PDF' AND UPPER(SUBSTRING(DocumentImageLocation, (LEN(DocumentImageLocation) - 2), 3)) = 'JPG' THEN 'JPG'
			ELSE
				[DocumentImageType]
		  END AS DocumentImageType
		, [DocumentImageLocation]
		, [ClaimAspectServiceChannelID]
		, [ClaimAspectID]
		, ISNULL([JobXSLFile],'') [JobXSLFile]
		, [JobStatus]
		, [JobStatusDetails]
		, [JobPriority]
		, [JobCreatedDate]
		, [JobProcessedDate]
		, [JobTransformedDate]
		, [JobTransmittedDate]
		, [JobArchivedDate]
		, [JobFinishedDate]
		, [EnabledFlag]
		, [JobXML]
		, [SysLastUserID]
		, [SysLastUpdatedDate]
		, [VehicleID]
		, [ShopLocationID]
		, [SeqID]
	INTO #HQJobs 
	FROM
		utb_hyperquestsvc_jobs
	WHERE 
		UPPER(jobstatus) IN ('UNPROCESSED','WAITING')
		AND EnabledFlag = 1
	ORDER BY 
		Receiveddate
    
    -- Begin main code
	SELECT
		1 AS Tag
		, NULL AS Parent
		, NULL AS [Root!1!Job]
		, NULL AS [Job!2!JobID]
		, NULL AS [Job!2!InscCompID]
		, NULL AS [Job!2!LynxID]
		, NULL AS [Job!2!ClaimNumber]
		, NULL AS [Job!2!DocumentID]
		, NULL AS [Job!2!AssignmentID]
		, NULL AS [Job!2!VehicleID]
		, NULL AS [Job!2!ShopLocationID]
		, NULL AS [Job!2!SeqID]
		, NULL AS [Job!2!ReceivedDate]
		, NULL AS [Job!2!DocumentTypeID]
		, NULL AS [Job!2!DocumentTypeName]
		, NULL AS [Job!2!DocumentSource]
		, NULL AS [Job!2!DocumentImageType]
		, NULL AS [Job!2!DocumentImageLocation]
		, NULL AS [Job!2!ClaimAspectServiceChannelID]
		, NULL AS [Job!2!ClaimAspectID]
		, NULL AS [Job!2!JobXSLFile]
		, NULL AS [Job!2!JobStatus]
		, NULL AS [Job!2!JobStatusDetails]
		, NULL AS [Job!2!JobPriority]
		, NULL AS [Job!2!JobProcessedDate]
		, NULL AS [Job!2!JobTransformedDate]
		, NULL AS [Job!2!JobTransmittedDate]
		, NULL AS [Job!2!JobArchivedDate]
		, NULL AS [Job!2!JobFinishedDate]
		, NULL AS [Job!2!EnabledFlag]
		, NULL AS [Job!2!JobXML]
		, NULL AS [Job!2!SysLastUserID]
		, NULL AS [Job!2!SysLastUpdatedDate]

	UNION ALL

	SELECT  
		2 AS Tag
		, 1 AS Parent
		, NULL
		, JobID
		, InscCompID
		, LynxID
		, ClaimNumber
		, DocumentID
		, AssignmentID
		, VehicleID
		, ShopLocationID
		, SeqID
		, ReceivedDate
		, DocumentTypeID
		, DocumentTypeName
		, DocumentSource
		, DocumentImageType
		, DocumentImageLocation
		, ClaimAspectServiceChannelID
		, ClaimAspectID
		, JobXSLFile
		, JobStatus
		, JobStatusDetails
		, JobPriority
		, JobProcessedDate
		, JobTransformedDate
		, JobTransmittedDate
		, JobArchivedDate
		, JobFinishedDate
		, EnabledFlag
		, JobXML
		, SysLastUserID
		, SysLastUpdatedDate
	FROM
		#HQJobs

	FOR XML EXPLICIT	

	--DROP #HQJobs
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetJobDetailsWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetJobDetailsWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/