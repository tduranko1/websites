-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientVerifyAssignmentTypeXml' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClientVerifyAssignmentTypeXml 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClientVerifyAssignmentTypeXml
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClientVerifyAssignmentTypeXml
    -- (@parameter_name datatype [OUTPUT],...)
    @InsuranceCompanyID         udt_std_id_small,
    @AssignmentTypeID           udt_std_id
AS
BEGIN
    -- TODO: Add SQL statements here
    DECLARE @Msg udt_std_desc_mid
    DECLARE @AssTypeName udt_std_desc_short
    DECLARE @ClientName  udt_std_desc_short
    
	-- 18Feb2013 - TVD - Added join to utb_insurance table
	-- and checks for EnabledFlag.
	IF NOT EXISTS (
		SELECT 
			* 
		FROM 
			utb_client_assignment_type cat
			INNER JOIN utb_insurance i
				ON i.InsuranceCompanyID = cat.InsuranceCompanyID
		WHERE 
			cat.InsuranceCompanyID = @InsuranceCompanyID
            AND cat.AssignmentTypeID = @AssignmentTypeID
            AND EnabledFlag = 1
    )
    BEGIN
        SELECT @ClientName =  Name
        FROM utb_insurance
        WHERE InsuranceCompanyID = @InsuranceCompanyID
        
		-- 25Mar2013 - TVD - Added RRP to the code
        -- For Now we'll hard code Assignment Type Names here.  Eventually we'll tie
        -- it into database name.
        IF @AssignmentTypeID = 4 
        BEGIN 
            SET @AssTypeName = 'Shop Program'
        END
        IF @AssignmentTypeID = 6
        BEGIN
            SET @AssTypeName = 'Desk Audit'
        END
        IF @AssignmentTypeID = 16
        BEGIN
            SET @AssTypeName = 'Repair Referral'
        END
        
        Set @Msg = 'LYNX Services''s ' + @AssTypeName + ' not activated for ' + @ClientName
    END
    ELSE
    BEGIN
        SET @Msg = 'OK'
    END
    
    SELECT 1 as TAG, 0 as Parent, @Msg as [Verify!1!result]
        
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientVerifyAssignmentTypeXml' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClientVerifyAssignmentTypeXml TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/