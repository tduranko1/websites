ALTER TABLE dbo.utb_audit_log DROP CONSTRAINT uck_audit_log_audittypecd

ALTER TABLE dbo.utb_audit_log 
    ADD CONSTRAINT uck_audit_log_audittypecd
    CHECK ([AuditTypeCD] = 'C' or [AuditTypeCD] = 'F' or [AuditTypeCD] = 'P' or [AuditTypeCD] = 'S')
go
