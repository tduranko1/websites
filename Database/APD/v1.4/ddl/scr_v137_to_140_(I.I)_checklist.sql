--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_checklist'                    --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Change columns                                            !!!!
--!!!!!             ClaimAspectID to ClaimAspectServiceChannelID          !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_checklist_history DROP CONSTRAINT ufk_checklist_history_checklistid_in_checklist
    COMMIT

    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT upk_checklist

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_createdroleid_in_role

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_claimaspectid_in_claim_aspect

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_assigeduserid_in_user

    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_createduserid_in_user

    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_syslastuserid_in_user

    COMMIT

    BEGIN TRANSACTION
    EXECUTE sp_rename N'dbo.utb_checklist', N'Tmp_utb_checklist', 'OBJECT'
    COMMIT

    BEGIN TRANSACTION
    CREATE TABLE dbo.utb_checklist
	    (
	    CheckListID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AssignedUserID udt_std_id NULL,
	    ClaimAspectServiceChannelID udt_std_id_big NOT NULL,
	    CreatedRoleID udt_std_int_small NOT NULL,
	    CreatedUserID udt_std_id NOT NULL,
	    StatusID udt_std_int_small NULL,
	    TaskID udt_std_int_small NULL,
	    AlarmDate udt_std_datetime NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    UserTaskDescription udt_std_desc_mid NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

--     SET IDENTITY_INSERT dbo.utb_checklist ON
-- 
--     IF EXISTS(SELECT * FROM dbo.Tmp_utb_checklist)
-- 	     EXEC('INSERT INTO dbo.utb_checklist (CheckListID, AssignedUserID, ClaimAspectServiceChannelID, CreatedRoleID, CreatedUserID, StatusID, TaskID, AlarmDate, CreatedDate, UserTaskDescription, SysLastUserID, SysLastUpdatedDate)
-- 		    SELECT CheckListID, AssignedUserID, 0, CreatedRoleID, CreatedUserID, StatusID, TaskID, AlarmDate, CreatedDate, UserTaskDescription, SysLastUserID, SysLastUpdatedDate FROM dbo.Tmp_utb_checklist (HOLDLOCK TABLOCKX)')
-- 
--     SET IDENTITY_INSERT dbo.utb_checklist OFF

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    upk_checklist PRIMARY KEY CLUSTERED 
	    (
	    CheckListID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow


    CREATE NONCLUSTERED INDEX uix_ie_checklist_assigneduserid ON dbo.utb_checklist
	    (
	    AssignedUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_createdroleid ON dbo.utb_checklist
	    (
	    CreatedRoleID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_createduserid ON dbo.utb_checklist
	    (
	    CreatedUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_statusid ON dbo.utb_checklist
	    (
	    StatusID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_taskid ON dbo.utb_checklist
	    (
	    TaskID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_alarmdate ON dbo.utb_checklist
	    (
	    AlarmDate
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_checklist_status_task ON dbo.utb_checklist
	    (
	    StatusID,
	    TaskID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_assigeduserid_in_user FOREIGN KEY
	    (
	    AssignedUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_createduserid_in_user FOREIGN KEY
	    (
	    CreatedUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_createdroleid_in_role FOREIGN KEY
	    (
	    CreatedRoleID
	    ) REFERENCES dbo.utb_role
	    (
	    RoleID
	    )

    COMMIT
 
    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

