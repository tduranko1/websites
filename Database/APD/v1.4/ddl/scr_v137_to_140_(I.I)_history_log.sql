--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_history_log'                  --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             ClaimAspectServiceChannelID                           !!!!
--!!!!!             ServiceChannelCD                                      !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_history_log
	    DROP CONSTRAINT ufk_history_log_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp_utb_history_log
	    (
	    LogID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    ClaimAspectNumber udt_std_int NOT NULL,
            ClaimAspectServiceChannelID udt_std_id_big NULL,
	    ClaimAspectTypeID udt_std_int_tiny NOT NULL,
	    CompletedDate udt_std_datetime NOT NULL,
	    CompletedRoleID udt_std_int_small NOT NULL,
	    CompletedUserID udt_std_int NOT NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    CreatedRoleID udt_std_int_small NOT NULL,
	    CreatedUserID udt_std_int NOT NULL,
	    Description udt_std_desc_long NULL,
	    EventID udt_std_int_small NULL,
	    LogType udt_std_cd NOT NULL,
	    LynxID udt_std_int_big NOT NULL,
	    NotApplicableFlag udt_std_flag NULL,
	    ReferenceID udt_std_desc_mid NULL,
	    ServiceChannelCD udt_std_cd NULL,
	    StatusID udt_std_int_small NOT NULL,
	    TaskID udt_std_int_small NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

    SET IDENTITY_INSERT dbo.Tmp_utb_history_log ON

    IF EXISTS(SELECT * FROM dbo.utb_history_log)
	     EXEC('INSERT INTO dbo.Tmp_utb_history_log (LogID, ClaimAspectNumber, ClaimAspectTypeID, CompletedDate, CompletedRoleID, CompletedUserID, CreatedDate, CreatedRoleID, CreatedUserID, Description, EventID, LogType, LynxID, NotApplicableFlag, ReferenceID, StatusID, TaskID, SysLastUserID, SysLastUpdatedDate)
		    SELECT LogID, ClaimAspectNumber, ClaimAspectTypeID, CompletedDate, CompletedRoleID, CompletedUserID, CreatedDate, CreatedRoleID, CreatedUserID, Description, EventID, LogType, LynxID, NotApplicableFlag, ReferenceID, StatusID, TaskID, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_history_log (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_history_log OFF

    DROP TABLE dbo.utb_history_log

    EXECUTE sp_rename N'dbo.Tmp_utb_history_log', N'utb_history_log', 'OBJECT'

    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    upk_history_log PRIMARY KEY CLUSTERED 
	    (
	    LogID
	    ) WITH FILLFACTOR = 100 ON ufg_workflow


    CREATE NONCLUSTERED INDEX uix_ie_history_log_lynxid ON dbo.utb_history_log
	    (
	    LynxID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    uck_history_log_logtype CHECK (([LogType] = 'T' or [LogType] = 'S' or [LogType] = 'E'))

    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    ufk_history_log_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

