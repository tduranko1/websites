--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_form_supplement'               --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_form_supplement
    (
        FormSupplementID    udt_std_id_big IDENTITY(1,1)    NOT NULL,
        FormID              udt_std_id_big                  NOT NULL,
        LossStateCode       udt_addr_state                      NULL,
        ShopStateCode       udt_addr_state                      NULL,
        EnabledFlag         udt_enabled_flag                NOT NULL,
        HTMLPath            udt_std_desc_mid                    NULL,
        Name                udt_std_name                    NOT NULL,
        PDFPath             udt_std_desc_mid                    NULL,
        ServiceChannelCD    udt_std_cd                          NULL,
        SQLProcedure        udt_std_desc_mid                NOT NULL,
	    SysLastUserID       udt_std_id                      NOT NULL,
	    SysLastUpdatedDate  udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_form_supplement
        PRIMARY KEY CLUSTERED (FormSupplementID) WITH FILLFACTOR=90
                                                                              ON ufg_workflow
    )
    ON ufg_workflow
     
    ALTER TABLE dbo.utb_form_supplement ADD CONSTRAINT
	    ufk_form_supplement_formid_in_form FOREIGN KEY
	    (
	    FormID
	    ) REFERENCES dbo.utb_form
	    (
	    FormID
	    )
    
    ALTER TABLE dbo.utb_form_supplement ADD CONSTRAINT
	    ufk_form_supplement_lossstatecode_in_state_code FOREIGN KEY
	    (
	    LossStateCode
	    ) REFERENCES dbo.utb_state_code
	    (
	    StateCode
	    )
    
    ALTER TABLE dbo.utb_form_supplement ADD CONSTRAINT
	    ufk_form_supplement_shopstatecode_in_state_code FOREIGN KEY
	    (
	    ShopStateCode
	    ) REFERENCES dbo.utb_state_code
	    (
	    StateCode
	    )
    
    ALTER TABLE dbo.utb_form_supplement ADD CONSTRAINT
	    ufk_form_supplement_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    CREATE NONCLUSTERED INDEX uix_ie_form_supplement_formid ON dbo.utb_form_supplement
	    (
	    FormID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_form_supplement_lossstatecode ON dbo.utb_form_supplement
	    (
	    LossStateCode
	    ) WITH FILLFACTOR = 85 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_form_supplement_shopstatecode ON dbo.utb_form_supplement
	    (
	    ShopStateCode
	    ) WITH FILLFACTOR = 85 ON ufg_workflow_index
    
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

