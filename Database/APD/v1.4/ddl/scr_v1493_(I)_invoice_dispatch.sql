--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_invoice_dispatch' --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_invoice_dispatch 
    (
        TransactionID   					udt_std_id             IDENTITY NOT NULL,
        Amount							    udt_std_money                   NOT NULL,
        Description						    udt_std_desc_mid                NULL,
        DisbursementAmount					udt_std_money					NULL,
        DisbursementDate					udt_std_datetime				NULL,
        DisbursementMethodCD				udt_std_cd		    			NULL,
        DispatchNumber					    udt_std_desc_short              NOT NULL,
        DisptachNumberNew					udt_std_desc_short  			NULL,
        TransactionCD						udt_std_cd                      NOT NULL,
        TransactionDate						udt_std_datetime				NOT NULL,
        SysLastUserID						udt_std_id                      NOT NULL,
        SysLastUpdatedDate					udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_invoice_dispatch
        PRIMARY KEY CLUSTERED (TransactionID) WITH FILLFACTOR=85
                             ON ufg_claim
    )
    ON ufg_claim
     
    ALTER TABLE dbo.utb_invoice_dispatch ADD CONSTRAINT
        uck_invoice_dispatch_transactioncd
        CHECK  ((TransactionCD = 'A' or TransactionCD = 'Q' or TransactionCD = 'R'))

    ALTER TABLE dbo.utb_invoice_dispatch ADD CONSTRAINT
        uck_invoice_dispatch_disbursementmethodcd
        CHECK  ((DisbursementMethodCD = 'C' or DisbursementMethodCD = 'E'))

    ALTER TABLE dbo.utb_invoice_dispatch ADD CONSTRAINT
        ufk_invoice_dispatch_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

