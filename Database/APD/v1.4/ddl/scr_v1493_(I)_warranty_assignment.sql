--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_warranty_assignment' --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_warranty_assignment 
    (
        AssignmentID    					udt_std_id_big        IDENTITY  NOT NULL,
        ClaimAspectServiceChannelID         udt_std_id_big                  NULL,
        CommunicationMethodID				udt_std_int_tiny                NULL,
        ShopLocationID						udt_std_id_big                  NULL,
        AssignmentDate						udt_std_datetime				NULL,
        AssignmentReceivedDate				udt_std_datetime                NULL,
	    AssignmentRemarks					udt_std_desc_xlong              NULL,
	    AssignmentSuffix					udt_std_cd					    NULL,
	    CancellationDate					udt_std_datetime				NULL,
	    CommunicationAddress				udt_std_file_location			NULL,
	    EffectiveDeductibleSentAmt			udt_std_money					NULL,
	    ProgramTypeCD						udt_std_cd					    NULL,
	    ReferenceID						    udt_std_desc_short				NULL,
	    SearchTypeCD						udt_std_cd					    NULL,
	    SelectionDate						udt_std_datetime				NULL,
	    WarrantyEndDate						udt_std_datetime				NULL,
	    WarrantyEndConfirmFlag				udt_std_flag					NULL,
	    WarrantyStartDate					udt_std_datetime				NULL,
	    WarrantyStartConfirmFlag			udt_std_flag					NULL,
        SysLastUserID						udt_std_id                      NOT NULL,
        SysLastUpdatedDate					udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_warranty_assignment
        PRIMARY KEY CLUSTERED (AssignmentID) WITH FILLFACTOR=85
                             ON ufg_claim
    )
    ON ufg_claim
     
    ALTER TABLE dbo.utb_warranty_assignment  WITH CHECK ADD  CONSTRAINT ufk_warranty_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY(ClaimAspectServiceChannelID)
    REFERENCES dbo.utb_claim_aspect_service_channel (ClaimAspectServiceChannelID)

    ALTER TABLE dbo.utb_warranty_assignment  WITH CHECK ADD  CONSTRAINT ufk_warranty_assignment_communicationmethodid_in_communication_method FOREIGN KEY(CommunicationMethodID)
    REFERENCES dbo.utb_communication_method (CommunicationMethodID)

    ALTER TABLE dbo.utb_warranty_assignment  WITH CHECK ADD  CONSTRAINT ufk_warranty_assignment_shoplocationid_in_shop_location FOREIGN KEY(ShopLocationID)
    REFERENCES dbo.utb_shop_location (ShopLocationID)

    ALTER TABLE dbo.utb_warranty_assignment  WITH CHECK ADD  CONSTRAINT uck_warranty_assignment_programtypecd CHECK  ((ProgramTypeCD = 'CEI' or ProgramTypeCD = 'LS' or ProgramTypeCD = 'NON' or ProgramTypeCD = 'OOP'))

    ALTER TABLE dbo.utb_warranty_assignment  WITH CHECK ADD  CONSTRAINT uck_warranty_assignment_searchtypecd CHECK  ((SearchTypeCD = 'D' or SearchTypeCD = 'N'))

    CREATE NONCLUSTERED INDEX uix_ie_warranty_assignment_claimaspectservicechannelid ON dbo.utb_warranty_assignment
	    (
	    ClaimAspectServiceChannelID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index
    
    CREATE NONCLUSTERED INDEX uix_ie_warranty_assignment_communicationmethodid ON dbo.utb_warranty_assignment
	    (
	    CommunicationMethodID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index
    
    CREATE NONCLUSTERED INDEX uix_ie_warranty_assignment_shoplocationid ON dbo.utb_warranty_assignment
	    (
	    ShopLocationID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index
    
    ALTER TABLE dbo.utb_warranty_assignment ADD CONSTRAINT
        ufk_warranty_assignment_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

