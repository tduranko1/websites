--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_billing'                      --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_billing
	    DROP CONSTRAINT ufk_billing_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_billing
	    (
	    BillingID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    Address1 udt_addr_line_1 NULL,
	    Address2 udt_addr_line_2 NULL,
	    AddressCity udt_addr_city NULL,
	    AddressCounty udt_addr_county NULL,
	    AddressState udt_addr_state NULL,
	    AddressZip udt_addr_zip_code NULL,
	    EFTAccountNumber udt_std_desc_short NULL,
	    EFTAccountTypeCD udt_std_cd NULL,
	    EFTContractSignedFlag udt_std_flag NOT NULL,
	    EFTEffectiveDate udt_std_datetime NULL,
	    EFTRoutingNumber udt_std_desc_short NULL,
	    FaxAreaCode udt_ph_area_code NULL,
	    FaxExchangeNumber udt_ph_exchange_number NULL,
	    FaxExtensionNumber udt_ph_extension_number NULL,
	    FaxUnitNumber udt_ph_unit_number NULL,
	    Name udt_std_name NOT NULL,
	    NotificationAddress udt_std_desc_long NULL,
	    NotificationMethodCD udt_std_cd NULL,
	    PhoneAreaCode udt_ph_area_code NULL,
	    PhoneExchangeNumber udt_ph_exchange_number NULL,
	    PhoneExtensionNumber udt_ph_extension_number NULL,
	    PhoneUnitNumber udt_ph_unit_number NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_shop

    SET IDENTITY_INSERT dbo.Tmp_utb_billing ON

    IF EXISTS(SELECT * FROM dbo.utb_billing)
	     EXEC('INSERT INTO dbo.Tmp_utb_billing (BillingID, Address1, Address2, AddressCity, AddressCounty, AddressState, AddressZip, EFTAccountNumber, EFTAccountTypeCD, EFTContractSignedFlag, EFTEffectiveDate, EFTRoutingNumber, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, SysLastUserID, SysLastUpdatedDate)
		    SELECT BillingID, Address1, Address2, AddressCity, AddressCounty, AddressState, AddressZip, EFTAccountNumber, EFTAccountTypeCD, EFTContractSignedFlag, EFTEffectiveDate, EFTRoutingNumber, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_billing WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_billing OFF

    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_billingid_in_billing

    DROP TABLE dbo.utb_billing

    EXECUTE sp_rename N'dbo.Tmp_utb_billing', N'utb_billing', 'OBJECT' 

    ALTER TABLE dbo.utb_billing ADD CONSTRAINT
	    upk_billing PRIMARY KEY CLUSTERED 
	    (
	    BillingID
	    ) WITH FILLFACTOR = 90 ON ufg_shop


    CREATE NONCLUSTERED INDEX uix_ie_billing_name ON dbo.utb_billing
	    (
	    Name
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index

    ALTER TABLE dbo.utb_billing ADD CONSTRAINT
	    uck_billing_eftaccounttypecd CHECK (([EFTAccountTypeCD] = 'C' or [EFTAccountTypeCD] = 'S'))

    ALTER TABLE dbo.utb_billing ADD CONSTRAINT
	    ufk_billing_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_billingid_in_billing FOREIGN KEY
	    (
	    BillingID   
	    ) REFERENCES dbo.utb_billing
	    (
	    BillingID
	    )

    COMMIT

IF 0 = @error
BEGIN

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

