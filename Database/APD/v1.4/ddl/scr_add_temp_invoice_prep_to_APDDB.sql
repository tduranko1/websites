-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


CREATE TABLE dbo.utb_temp_invoice_prep
(
	BillingSessionID		varchar(50)			not null,
	InvoiceID				udt_std_id_big		not null,
	Amount					udt_std_money		null,
	BillingEntryDate		udt_std_datetime	null,
	CarrierClaimRep			udt_std_name		null,
	ClaimantName			udt_std_name		null,
	ClaimNumber				udt_std_desc_short	null,
	ClientID				udt_std_id			null,
	ClientName				udt_std_name		null,
	CreateDate				udt_std_datetime	null,
	Description				udt_std_desc_short	null,
	InsuredName				udt_std_name		null,
	LossDate				udt_std_datetime	null,
	LossState				varchar(30)			null,
	LossStateCD				udt_addr_state		null,	
	LynxID					udt_std_desc_short	null,
	PolicyNumber			udt_cov_policy_number null,
	OfficeID				udt_std_id			null,
	OfficeName				udt_std_desc_short	null,	
	constraint upk_temp_invoice_prep 
		primary key (BillingSessionID, InvoiceID) 
	
)


