--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_claim_aspect_service_channel_concession' -- !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_claim_aspect_service_channel_concession 
    (
        ClaimAspectServiceChannelConcessionID   udt_std_id_big    IDENTITY(1,1) NOT NULL,
        ClaimAspectServiceChannelID             udt_std_id_big                  NOT NULL,
        ConcessionReasonID                      udt_std_int_small               NOT NULL,
        Amount                                  udt_std_money                   NOT NULL,
        Comments                                udt_std_desc_mid                NOT NULL,
        CreatedDate                             udt_std_datetime                NOT NULL,
        EnabledFlag                             udt_enabled_flag                NOT NULL,
        RentalDays                              udt_std_int_small                   NULL,
        SysLastUserID                           udt_std_id                      NOT NULL,
        SysLastUpdatedDate                      udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_claim_aspect_service_channel_concession
        PRIMARY KEY CLUSTERED (ClaimAspectServiceChannelConcessionID) WITH FILLFACTOR=85
                                                                              ON ufg_claim
    )
    ON ufg_claim
     
    ALTER TABLE dbo.utb_claim_aspect_service_channel_concession ADD CONSTRAINT
        ufk_claim_aspect_service_channel_concession_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
        (
        ClaimAspectServiceChannelID
        ) REFERENCES dbo.utb_claim_aspect_service_channel
        (
        ClaimAspectServiceChannelID
        )
        
    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_concession_claimaspectservicechannelid ON dbo.utb_claim_aspect_service_channel_concession
        (
        ClaimAspectServiceChannelID
        ) WITH FILLFACTOR = 80 ON ufg_claim_index
    
    ALTER TABLE dbo.utb_claim_aspect_service_channel_concession ADD CONSTRAINT
        ufk_claim_aspect_service_channel_concession_concessionreasonid_in_concession_reason FOREIGN KEY
        (
        ConcessionReasonID
        ) REFERENCES dbo.utb_concession_reason
        (
        ConcessionReasonID
        )
        
    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_concession_concessionreasonid ON dbo.utb_claim_aspect_service_channel_concession
        (
        ConcessionReasonID
        ) WITH FILLFACTOR = 80 ON ufg_claim_index
    
    
    ALTER TABLE dbo.utb_claim_aspect_service_channel_concession ADD CONSTRAINT
        ufk_claim_aspect_service_channel_concession_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

