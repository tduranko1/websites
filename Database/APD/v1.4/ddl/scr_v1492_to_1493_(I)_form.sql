--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_form'       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_eventid_in_event

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_documenttypeid_in_document_type

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_lossstatecode_in_state_code

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_shopstatecode_in_state_code

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_bundlingtaskid_in_bundling_task

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_insurancecompanyid_in_insurance

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_form
	    (
	    FormID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    BundlingTaskID udt_std_int NULL,
	    DocumentTypeID udt_std_int_tiny NULL,
	    EventID udt_std_int_small NULL,
	    InsuranceCompanyID udt_std_int_small NULL,
	    LossStateCode udt_addr_state NULL,
	    ShopStateCode udt_addr_state NULL,
	    AutoBundlingFlag udt_std_flag NOT NULL,
	    ConditionValue udt_std_desc_mid NULL,
	    DataMiningFlag udt_std_flag NOT NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    FormSupplementID udt_std_id_big NULL,
	    HTMLPath udt_std_desc_mid NULL,
	    Name udt_std_name NOT NULL,
	    PDFPath udt_std_desc_mid NULL,
	    PertainsToCD udt_std_cd NULL,
	    ServiceChannelCD udt_std_cd NULL,
	    SQLProcedure udt_std_desc_mid NOT NULL,
	    SystemFlag udt_std_flag NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

    SET IDENTITY_INSERT dbo.Tmp_utb_form ON

    IF EXISTS(SELECT * FROM dbo.utb_form)
	     EXEC('INSERT INTO dbo.Tmp_utb_form (FormID, BundlingTaskID, DocumentTypeID, EventID, InsuranceCompanyID, LossStateCode, ShopStateCode, AutoBundlingFlag, DataMiningFlag, EnabledFlag, FormSupplementID, HTMLPath, Name, PDFPath, PertainsToCD, ServiceChannelCD, SQLProcedure, SystemFlag, SysLastUserID, SysLastUpdatedDate)
		    SELECT FormID, BundlingTaskID, DocumentTypeID, EventID, InsuranceCompanyID, LossStateCode, ShopStateCode, AutoBundlingFlag, DataMiningFlag, EnabledFlag, FormSupplementID, HTMLPath, Name, PDFPath, PertainsToCD, ServiceChannelCD, SQLProcedure, SystemFlag, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_form WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_form OFF

    ALTER TABLE dbo.utb_form_field
	    DROP CONSTRAINT ufk_form_field_formid_in_form

    ALTER TABLE dbo.utb_form_supplement
	    DROP CONSTRAINT ufk_form_supplement_formid_in_form

    ALTER TABLE dbo.utb_form_data
	    DROP CONSTRAINT ufk_form_data_formid_in_form

    DROP TABLE dbo.utb_form

    EXECUTE sp_rename N'dbo.Tmp_utb_form', N'utb_form', 'OBJECT' 

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    upk_form PRIMARY KEY CLUSTERED 
	    (
	    FormID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow


    CREATE NONCLUSTERED INDEX uix_ie_form_bundlingtaskid ON dbo.utb_form
	    (
	    BundlingTaskID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_form_documenttypeid ON dbo.utb_form
	    (
	    DocumentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_form_eventid ON dbo.utb_form
	    (
	    EventID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_form_insurancecompanyid ON dbo.utb_form
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_form_lossstatecode ON dbo.utb_form
	    (
	    LossStateCode
	    ) WITH FILLFACTOR = 85 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_form_shopstatecode ON dbo.utb_form
	    (
	    ShopStateCode
	    ) WITH FILLFACTOR = 85 ON ufg_workflow_index

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_bundlingtaskid_in_bundling_task FOREIGN KEY
	    (
	    BundlingTaskID
	    ) REFERENCES dbo.utb_bundling_task
	    (
	    BundlingTaskID
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_lossstatecode_in_state_code FOREIGN KEY
	    (
	    LossStateCode
	    ) REFERENCES dbo.utb_state_code
	    (
	    StateCode
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_shopstatecode_in_state_code FOREIGN KEY
	    (
	    ShopStateCode
	    ) REFERENCES dbo.utb_state_code
	    (
	    StateCode
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_documenttypeid_in_document_type FOREIGN KEY
	    (
	    DocumentTypeID
	    ) REFERENCES dbo.utb_document_type
	    (
	    DocumentTypeID
	    )

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_eventid_in_event FOREIGN KEY
	    (
	    EventID
	    ) REFERENCES dbo.utb_event
	    (
	    EventID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form_data ADD CONSTRAINT
	    ufk_form_data_formid_in_form FOREIGN KEY
	    (
	    FormID
	    ) REFERENCES dbo.utb_form
	    (
	    FormID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form_supplement ADD CONSTRAINT
	    ufk_form_supplement_formid_in_form FOREIGN KEY
	    (
	    FormID
	    ) REFERENCES dbo.utb_form
	    (
	    FormID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form_field ADD CONSTRAINT
	    ufk_form_field_formid_in_form FOREIGN KEY
	    (
	    FormID
	    ) REFERENCES dbo.utb_form
	    (
	    FormID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

