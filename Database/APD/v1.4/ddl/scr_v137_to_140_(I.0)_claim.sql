--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim'                        --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             ClientClaimNumber                                     !!!!
--!!!!!             ClientClaimNumberSquished                             !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_insurancecompanyid_in_insurance

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_weatherconditionid_in_weather_condition

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_roadtypeid_in_road_type

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_roadlocationid_in_road_location

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_losstypeid_in_loss_type

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_claimantcontactmethodid_in_claimant_contact_method

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_catastrophiclossid_in_catastrophic_loss

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_callerinvolvedid_in_involved

    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_contactinvolvedid_in_involved

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_syslastuserid_in_user

    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_carrierrepuserid_in_user

    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_intakeuserid_in_user

    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp_utb_claim
	    (
	    LynxID udt_std_int_big NOT NULL,
	    CallerInvolvedID udt_std_id_big NULL,
	    CarrierRepUserID udt_std_id NULL,
	    CatastrophicLossID udt_std_int_big NULL,
	    ClaimantContactMethodID udt_std_int_tiny NULL,
	    ContactInvolvedID udt_std_id_big NULL,
	    InsuranceCompanyID udt_std_int_small NOT NULL,
	    IntakeUserID udt_std_id NOT NULL,
	    LossTypeID udt_std_int_tiny NULL,
	    RoadLocationID udt_std_int_tiny NULL,
	    RoadtypeID udt_std_int_tiny NULL,
	    WeatherConditionID udt_std_int_tiny NULL,
	    AgentAreaCode udt_ph_area_code NULL,
	    AgentExchangeNumber udt_ph_exchange_number NULL,
	    AgentExtensionNumber udt_ph_extension_number NULL,
	    AgentName udt_std_name NULL,
	    AgentUnitNumber udt_ph_unit_number NULL,
	    ClientClaimNumber udt_cov_claim_number NULL,
	    ClientClaimNumberSquished udt_cov_claim_number NULL,
	    ComplexityFinalScore udt_std_int_small NULL,
	    ComplexityInitialScore udt_std_int_small NULL,
	    DemoFlag udt_std_flag NOT NULL,
	    IntakeFinishDate udt_std_datetime NULL,
	    IntakeStartDate udt_std_datetime NOT NULL,
	    LossCity udt_addr_city NULL,
	    LossCounty udt_addr_county NULL,
	    LossDate udt_std_datetime NULL,
	    LossDescription udt_std_desc_xlong NULL,
	    LossLocation udt_std_desc_short NULL,
	    LossState udt_addr_state NULL,
	    LossZip udt_addr_zip_code NULL,
	    PoliceDepartmentName udt_std_name NULL,
	    PolicyNumber udt_cov_policy_number NULL,
	    Remarks udt_std_desc_xlong NULL,
	    RestrictedFlag udt_std_flag NOT NULL,
	    TripPurposeCD udt_std_cd NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    IF EXISTS(SELECT * FROM dbo.utb_claim)
	     EXEC('INSERT INTO dbo.Tmp_utb_claim (LynxID, CallerInvolvedID, CarrierRepUserID, CatastrophicLossID, ClaimantContactMethodID, ContactInvolvedID, InsuranceCompanyID, IntakeUserID, LossTypeID, RoadLocationID, RoadtypeID, WeatherConditionID, AgentAreaCode, AgentExchangeNumber, AgentExtensionNumber, AgentName, AgentUnitNumber, ComplexityFinalScore, ComplexityInitialScore, DemoFlag, IntakeFinishDate, IntakeStartDate, LossCity, LossCounty, LossDate, LossDescription, LossLocation, LossState, LossZip, PoliceDepartmentName, PolicyNumber, Remarks, RestrictedFlag, TripPurposeCD, SysLastUserID, SysLastUpdatedDate)
		    SELECT LynxID, CallerInvolvedID, CarrierRepUserID, CatastrophicLossID, ClaimantContactMethodID, ContactInvolvedID, InsuranceCompanyID, IntakeUserID, LossTypeID, RoadLocationID, RoadtypeID, WeatherConditionID, AgentAreaCode, AgentExchangeNumber, AgentExtensionNumber, AgentName, AgentUnitNumber, ComplexityFinalScore, ComplexityInitialScore, DemoFlag, IntakeFinishDate, IntakeStartDate, LossCity, LossCounty, LossDate, LossDescription, LossLocation, LossState, LossZip, PoliceDepartmentName, PolicyNumber, Remarks, RestrictedFlag, TripPurposeCD, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_claim (HOLDLOCK TABLOCKX)')

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_lynxid_in_claim

    DROP TABLE dbo.utb_claim

    EXECUTE sp_rename N'dbo.Tmp_utb_claim', N'utb_claim', 'OBJECT'

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    upk_claim PRIMARY KEY CLUSTERED 
	    (
	    LynxID
	    ) WITH FILLFACTOR = 90 ON ufg_claim


    CREATE NONCLUSTERED INDEX uix_ie_claim_callerinvolvedid ON dbo.utb_claim
	    (
	    CallerInvolvedID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_catastrophiclossid ON dbo.utb_claim
	    (
	    CatastrophicLossID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_claimantcontactmethodid ON dbo.utb_claim
	    (
	    ClaimantContactMethodID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_contactinvolvedid ON dbo.utb_claim
	    (
	    ContactInvolvedID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_insurancecompanyid ON dbo.utb_claim
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_intakeuserid ON dbo.utb_claim
	    (
	    IntakeUserID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_losstypeid ON dbo.utb_claim
	    (
	    LossTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_roadlocationid ON dbo.utb_claim
	    (
	    RoadLocationID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_roadtypeid ON dbo.utb_claim
	    (
	    RoadtypeID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_weatherconditionid ON dbo.utb_claim
	    (
	    WeatherConditionID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_carrierrepuserid ON dbo.utb_claim
	    (
	    CarrierRepUserID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    uck_claim_trippurposecd CHECK (([TripPurposeCD] = 'P' or [TripPurposeCD] = 'B'))

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_callerinvolvedid_in_involved FOREIGN KEY
	    (
	    CallerInvolvedID
	    ) REFERENCES dbo.utb_involved
	    (
	    InvolvedID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_catastrophiclossid_in_catastrophic_loss FOREIGN KEY
	    (
	    CatastrophicLossID
	    ) REFERENCES dbo.utb_catastrophic_loss
	    (
	    CatastrophicLossID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_claimantcontactmethodid_in_claimant_contact_method FOREIGN KEY
	    (
	    ClaimantContactMethodID
	    ) REFERENCES dbo.utb_claimant_contact_method
	    (
	    ClaimantContactMethodID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_contactinvolvedid_in_involved FOREIGN KEY
	    (
	    ContactInvolvedID
	    ) REFERENCES dbo.utb_involved
	    (
	    InvolvedID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_losstypeid_in_loss_type FOREIGN KEY
	    (
	    LossTypeID
	    ) REFERENCES dbo.utb_loss_type
	    (
	    LossTypeID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_roadlocationid_in_road_location FOREIGN KEY
	    (
	    RoadLocationID
	    ) REFERENCES dbo.utb_road_location
	    (
	    RoadLocationID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_roadtypeid_in_road_type FOREIGN KEY
	    (
	    RoadtypeID
	    ) REFERENCES dbo.utb_road_type
	    (
	    RoadtypeID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_weatherconditionid_in_weather_condition FOREIGN KEY
	    (
	    WeatherConditionID
	    ) REFERENCES dbo.utb_weather_condition
	    (
	    WeatherConditionID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_carrierrepuserid_in_user FOREIGN KEY
	    (
	    CarrierRepUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_intakeuserid_in_user FOREIGN KEY
	    (
	    IntakeUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_lynxid_in_claim FOREIGN KEY
	    (
	    LynxID
	    ) REFERENCES dbo.utb_claim
	    (
	    LynxID
	    )

    COMMIT


    -- Capture any error

    SELECT @error = @@ERROR
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

