--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_document'                     --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_createduserid_in_user

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_syslastuserid_in_user

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_reinspectionrequestinguserid_in_user

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_documentsourceid_in_document_source

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_notetypeid_in_note_type

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_createduserroleid_in_role

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_statusid_in_status

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_documenttypeid_in_document_type

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_document
	    (
	    DocumentID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AssignmentID udt_std_id_big NULL,
	    CreatedUserID udt_std_id NOT NULL,
	    CreatedUserRoleID udt_std_int_small NOT NULL,
	    DocumentSourceID udt_std_int_tiny NOT NULL,
	    DocumentTypeID udt_std_int_tiny NOT NULL,
	    NoteTypeID udt_std_int_tiny NULL,
	    ParentDocumentID udt_std_id_big NULL,
	    ReinspectionRequestingUserID udt_std_id NULL,
	    StatusID udt_std_int_small NULL,
	    AgreedPriceMetCD udt_std_cd NULL,
	    AuditWeight udt_std_int NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    CreatedUserSupervisorFlag udt_std_flag NOT NULL,
	    DirectionalCD udt_std_cd NULL,
	    DirectionToPayFlag udt_std_flag NOT NULL,
	    DuplicateFlag udt_std_flag NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    EstimateLockedFlag udt_std_flag NOT NULL,
	    EstimateReviewDate udt_std_datetime NULL,
	    EstimateTypeCD udt_std_cd NULL,
	    ExternalReferenceDocumentID udt_std_desc_short NULL,
	    FinalEstimateFlag udt_std_flag NOT NULL,
	    FullSummaryExistsFlag udt_std_flag NOT NULL,
	    ImageLocation udt_std_file_location NULL,
	    ImageType udt_std_file_extension NULL,
	    Note udt_std_desc_huge NULL,
	    PrivateFlag udt_std_flag NOT NULL,
	    ReceivedDate udt_std_datetime NULL,
	    ReinspectionRequestComment udt_std_note NULL,
	    ReinspectionRequestDate udt_std_datetime NULL,
	    ReinspectionRequestFlag udt_std_flag NOT NULL,
	    SendToCarrierFlag udt_std_flag NOT NULL,
	    SendToCarrierStatusCD udt_std_cd NULL,
	    SendToCarrierTransactionID udt_std_desc_mid NULL,
	    SupplementSeqNumber udt_std_int_tiny NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    SET IDENTITY_INSERT dbo.Tmp_utb_document ON

    IF EXISTS(SELECT * FROM dbo.utb_document)
	     EXEC('INSERT INTO dbo.Tmp_utb_document (DocumentID, AssignmentID, CreatedUserID, CreatedUserRoleID, DocumentSourceID, DocumentTypeID, NoteTypeID, ParentDocumentID, ReinspectionRequestingUserID, StatusID, AgreedPriceMetCD, AuditWeight, CreatedDate, CreatedUserSupervisorFlag, DirectionalCD, DirectionToPayFlag, DuplicateFlag, EnabledFlag, EstimateReviewDate, EstimateTypeCD, ExternalReferenceDocumentID, FinalEstimateFlag, FullSummaryExistsFlag, ImageLocation, ImageType, Note, PrivateFlag, ReceivedDate, ReinspectionRequestComment, ReinspectionRequestDate, ReinspectionRequestFlag, SendToCarrierFlag, SendToCarrierStatusCD, SendToCarrierTransactionID, SupplementSeqNumber, SysLastUserID, SysLastUpdatedDate)
		    SELECT DocumentID, AssignmentID, CreatedUserID, CreatedUserRoleID, DocumentSourceID, DocumentTypeID, NoteTypeID, ParentDocumentID, ReinspectionRequestingUserID, StatusID, AgreedPriceMetCD, AuditWeight, CreatedDate, CreatedUserSupervisorFlag, DirectionalCD, DirectionToPayFlag, DuplicateFlag, EnabledFlag, EstimateReviewDate, EstimateTypeCD, ExternalReferenceDocumentID, FinalEstimateFlag, FullSummaryExistsFlag, ImageLocation, ImageType, Note, PrivateFlag, ReceivedDate, ReinspectionRequestComment, ReinspectionRequestDate, ReinspectionRequestFlag, SendToCarrierFlag, SendToCarrierStatusCD, SendToCarrierTransactionID, SupplementSeqNumber, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_document WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_document OFF

    ALTER TABLE dbo.utb_estimate
	    DROP CONSTRAINT ufk_estimate_documentid_in_document

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_parentdocumentid_in_document

    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_documentid_in_document

    ALTER TABLE dbo.utb_estimate_audit_results
	    DROP CONSTRAINT ufk_estimate_audit_results_estimatedocumentid_in_document

    ALTER TABLE dbo.utb_shop_location_document
	    DROP CONSTRAINT ufk_shop_location_document_documentid_in_document

    ALTER TABLE dbo.utb_estimate_detail
	    DROP CONSTRAINT ufk_estimate_detail_documentid_in_document

    ALTER TABLE dbo.utb_reinspect
	    DROP CONSTRAINT ufk_reinspect_estimatedocumentid_in_document

    ALTER TABLE dbo.utb_estimate_summary
	    DROP CONSTRAINT ufk_estimate_summary_documentid_in_document

    ALTER TABLE dbo.utb_claim_aspect_service_channel_document
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_document_documentid_in_document

    DROP TABLE dbo.utb_document

    EXECUTE sp_rename N'dbo.Tmp_utb_document', N'utb_document', 'OBJECT' 

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    upk_document PRIMARY KEY CLUSTERED 
	    (
	    DocumentID
	    ) WITH FILLFACTOR = 90 ON ufg_claim


    CREATE NONCLUSTERED INDEX uix_ie_document_documenttypeid ON dbo.utb_document
	    (
	    DocumentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_createduserid ON dbo.utb_document
	    (
	    CreatedUserID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_createduserroleid ON dbo.utb_document
	    (
	    CreatedUserRoleID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_documentsourceid ON dbo.utb_document
	    (
	    DocumentSourceID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_notetypeid ON dbo.utb_document
	    (
	    NoteTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_parentdocumentid ON dbo.utb_document
	    (
	    ParentDocumentID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_statusid ON dbo.utb_document
	    (
	    StatusID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_assignmentid ON dbo.utb_document
	    (
	    AssignmentID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_reinspectionrequestinguserid ON dbo.utb_document
	    (
	    ReinspectionRequestingUserID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_privateflag ON dbo.utb_document
	    (
	    PrivateFlag
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_document_externalreferencedocumentid ON dbo.utb_document
	    (
	    ExternalReferenceDocumentID
	    ) WITH FILLFACTOR = 95 ON ufg_claim_index

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    uck_document_agreedpricemetcd CHECK (([AgreedPriceMetCD] = 'N' or [AgreedPriceMetCD] = 'Y'))

    DECLARE @v sql_variant 
    SET @v = cast(N'N|No|Y|Yes' as varchar(10))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'USER', N'dbo', N'TABLE', N'utb_document', N'CONSTRAINT', N'uck_document_agreedpricemetcd'

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    uck_document_directionalcd CHECK (([DirectionalCD] = 'I' or [DirectionalCD] = 'O'))

    SET @v = cast(N'I|Received|O|Sent' as varchar(17))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'USER', N'dbo', N'TABLE', N'utb_document', N'CONSTRAINT', N'uck_document_directionalcd'

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    uck_document_estimatetypecd CHECK (([EstimateTypeCD] = 'A' or [EstimateTypeCD] = 'O'))

    SET @v = cast(N'A|Audited|O|Original' as varchar(20))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'USER', N'dbo', N'TABLE', N'utb_document', N'CONSTRAINT', N'uck_document_estimatetypecd'

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    uck_document_sendtocarrierstatuscd CHECK (([SendToCarrierStatusCD] = 'F' or [SendToCarrierStatusCD] = 'NS' or [SendToCarrierStatusCD] = 'S' or [SendToCarrierStatusCD] = 'UNK'))

    SET @v = cast(N'F|Failure|NS|Not Sent|S|Sent|UNK|Uknown' as varchar(39))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'USER', N'dbo', N'TABLE', N'utb_document', N'CONSTRAINT', N'uck_document_sendtocarrierstatuscd'

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_documenttypeid_in_document_type FOREIGN KEY
	    (
	    DocumentTypeID
	    ) REFERENCES dbo.utb_document_type
	    (
	    DocumentTypeID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_statusid_in_status FOREIGN KEY
	    (
	    StatusID
	    ) REFERENCES dbo.utb_status
	    (
	    StatusID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_createduserroleid_in_role FOREIGN KEY
	    (
	    CreatedUserRoleID
	    ) REFERENCES dbo.utb_role
	    (
	    RoleID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_notetypeid_in_note_type FOREIGN KEY
	    (
	    NoteTypeID
	    ) REFERENCES dbo.utb_note_type
	    (
	    NoteTypeID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_parentdocumentid_in_document FOREIGN KEY
	    (
	    ParentDocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_documentsourceid_in_document_source FOREIGN KEY
	    (
	    DocumentSourceID
	    ) REFERENCES dbo.utb_document_source
	    (
	    DocumentSourceID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_createduserid_in_user FOREIGN KEY
	    (
	    CreatedUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_reinspectionrequestinguserid_in_user FOREIGN KEY
	    (
	    ReinspectionRequestingUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel_document ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_document_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_estimate_summary ADD CONSTRAINT
	    ufk_estimate_summary_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_reinspect ADD CONSTRAINT
	    ufk_reinspect_estimatedocumentid_in_document FOREIGN KEY
	    (
	    EstimateDocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_estimate_detail ADD CONSTRAINT
	    ufk_estimate_detail_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_shop_location_document ADD CONSTRAINT
	    ufk_shop_location_document_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_estimate_audit_results ADD CONSTRAINT
	    ufk_estimate_audit_results_estimatedocumentid_in_document FOREIGN KEY
	    (
	    EstimateDocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_estimate ADD CONSTRAINT
	    ufk_estimate_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

