--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_client_assignment_type'       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Remove columns                                            !!!!
--!!!!!             ClientAuthorizesPaymentFlag
--!!!!!             InvoicingModelBillingCD
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_assignment_type
	    DROP CONSTRAINT ufk_client_assignment_type_syslastuserid_in_user
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_assignment_type
	    DROP CONSTRAINT ufk_client_assignment_type_insurancecompanyid_in_insurance
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_assignment_type
	    DROP CONSTRAINT ufk_client_assignment_type_assignmenttypeid_in_assignment_type
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_assignment_type
	    DROP CONSTRAINT upk_client_assignment_type
    
    COMMIT
    BEGIN TRANSACTION
    
    EXECUTE sp_rename N'dbo.utb_client_assignment_type', N'Tmp_utb_client_assignment_type', 'OBJECT' 
    
    CREATE TABLE dbo.utb_client_assignment_type
	    (
	    InsuranceCompanyID udt_std_int_small NOT NULL,
	    AssignmentTypeID udt_std_int NOT NULL,
	    SysLastUserID udt_std_int NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_reference
    
    IF EXISTS(SELECT * FROM dbo.Tmp_utb_client_assignment_type)
	     EXEC('INSERT INTO dbo.utb_client_assignment_type (InsuranceCompanyID, AssignmentTypeID, SysLastUserID, SysLastUpdatedDate)
		    SELECT InsuranceCompanyID, AssignmentTypeID, SysLastUserID, SysLastUpdatedDate FROM dbo.Tmp_utb_client_assignment_type WITH (HOLDLOCK TABLOCKX)')
    
    ALTER TABLE dbo.utb_client_assignment_type ADD CONSTRAINT
	    upk_client_assignment_type PRIMARY KEY CLUSTERED 
	    (
	    InsuranceCompanyID,
	    AssignmentTypeID
	    ) WITH FILLFACTOR = 100 ON ufg_reference

    
    CREATE NONCLUSTERED INDEX uix_ie_client_assignment_type_insurancecompanyid ON dbo.utb_client_assignment_type
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index
    
    CREATE NONCLUSTERED INDEX uix_ie_client_assignment_type_assignmenttypeid ON dbo.utb_client_assignment_type
	    (
	    AssignmentTypeID
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index
    
    ALTER TABLE dbo.utb_client_assignment_type ADD CONSTRAINT
	    ufk_client_assignment_type_assignmenttypeid_in_assignment_type FOREIGN KEY
	    (
	    AssignmentTypeID
	    ) REFERENCES dbo.utb_assignment_type
	    (
	    AssignmentTypeID
	    )
    
    ALTER TABLE dbo.utb_client_assignment_type ADD CONSTRAINT
	    ufk_client_assignment_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )
    
    ALTER TABLE dbo.utb_client_assignment_type ADD CONSTRAINT
	    ufk_client_assignment_type_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    COMMIT


    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

