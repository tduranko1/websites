--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_assignment'                   --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    -- Create new table schema in holding area
    CREATE TABLE dbo.Tmp_utb_assignment
	    (
	    AssignmentID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AppraiserID udt_std_id_big NULL,
	    ClaimAspectServiceChannelID udt_std_id_big NOT NULL,
	    CommunicationMethodID udt_std_int_tiny NULL,
	    ShopLocationID udt_std_id_big NULL,
	    AssignmentDate udt_std_datetime NULL,
	    AssignmentReceivedDate udt_std_datetime NULL,
	    AssignmentRemarks udt_std_desc_xlong NULL,
	    AssignmentSequenceNumber udt_std_int_small NULL,
	    AssignmentSuffix udt_std_cd NULL,
	    CancellationDate udt_std_datetime NULL,
	    CertifiedFirstFlag udt_std_flag NOT NULL,
	    CommunicationAddress udt_web_address NULL,
        EffectiveDeductibleSentAmt udt_std_money NULL,
	    ProgramTypeCD udt_std_cd NOT NULL,
	    ReferenceId udt_std_desc_short NULL,
	    SearchTypeCD udt_std_cd NULL,
	    SelectionDate udt_std_datetime NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    SET IDENTITY_INSERT dbo.Tmp_utb_assignment ON
	    
    IF EXISTS(SELECT * FROM dbo.utb_assignment)
	     EXEC('INSERT INTO dbo.Tmp_utb_assignment (AssignmentID, AppraiserID, ClaimAspectServiceChannelID, CommunicationMethodID, ShopLocationID, AssignmentDate, AssignmentReceivedDate, AssignmentRemarks, AssignmentSequenceNumber, AssignmentSuffix, CancellationDate, CertifiedFirstFlag, CommunicationAddress, EffectiveDeductibleSentAmt, ProgramTypeCD, ReferenceId, SearchTypeCD, SelectionDate, SysLastUserID, SysLastUpdatedDate) 
		    SELECT AssignmentID, AppraiserID, ClaimAspectServiceChannelID, CommunicationMethodID, ShopLocationID, AssignmentDate, AssignmentReceivedDate, AssignmentRemarks, 1, AssignmentSuffix, CancellationDate, CertifiedFirstFlag, CommunicationAddress, EffectiveDeductibleSentAmt, ProgramTypeCD, ReferenceId, SearchTypeCD, SelectionDate, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_assignment WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_assignment OFF

    IF (SELECT Count(*) FROM dbo.Tmp_utb_assignment) = (SELECT Count(*) FROM dbo.utb_assignment)
    BEGIN
        -- Remove FKs
        -- No FKs to remove    


        -- Drop current table and replace with new table schema
        DROP TABLE dbo.utb_assignment

        EXECUTE sp_rename N'dbo.Tmp_utb_assignment', N'utb_assignment', 'OBJECT' 

        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        upk_assignment PRIMARY KEY CLUSTERED 
	        (
	        AssignmentID
	        ) WITH FILLFACTOR = 90 ON ufg_claim


        -- Recreate indexes
        CREATE NONCLUSTERED INDEX uix_ie_assignment_communicationmethodid ON dbo.utb_assignment
	        (
	        CommunicationMethodID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_assignment_shoplocationid ON dbo.utb_assignment
	        (
	        ShopLocationID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_assignment_appraiserid ON dbo.utb_assignment
	        (
	        AppraiserID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index


        -- Add check costraints and reference codes
        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        uck_assignment_programtypecd CHECK (([ProgramTypeCD] = 'CEI' or [ProgramTypeCD] = 'LS' or [ProgramTypeCD] = 'NON' or [ProgramTypeCD] = 'OOP' or [ProgramTypeCD] = 'RRP'))

        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        uck_assignment_searchtypecd CHECK (([SearchTypeCD] = 'D' or [SearchTypeCD] = 'N'))

        EXEC sp_addextendedproperty 'Codes', 'CEI|CEI|LS|Lynx Select|NON|Non-Program|OOP|Out of Program|RRP|Referral Program', 
            'user', dbo, 'table','utb_assignment', 'CONSTRAINT','uck_assignment_programtypecd'

        EXEC sp_addextendedproperty 'Codes', 'D|Distance|N|Name', 
            'user', dbo, 'table','utb_assignment', 'CONSTRAINT','uck_assignment_searchtypecd'


        -- Reimplement FKs
        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        ufk_assignment_appraiserid_in_appraiser FOREIGN KEY
	        (
	        AppraiserID
	        ) REFERENCES dbo.utb_appraiser
	        (
	        AppraiserID
	        )

        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        ufk_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	        (
	        ClaimAspectServiceChannelID
	        ) REFERENCES dbo.utb_claim_aspect_service_channel
	        (
	        ClaimAspectServiceChannelID
	        )

        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        ufk_assignment_communicationmethodid_in_communication_method FOREIGN KEY
	        (
	        CommunicationMethodID
	        ) REFERENCES dbo.utb_communication_method
	        (
	        CommunicationMethodID
	        )

        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        ufk_assignment_shoplocationid_in_shop_location FOREIGN KEY
	        (
	        ShopLocationID
	        ) REFERENCES dbo.utb_shop_location
	        (
	        ShopLocationID
	        )
        
        ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	        ufk_assignment_syslastuserid_in_user FOREIGN KEY
	        (
	        SysLastUserID
	        ) REFERENCES dbo.utb_user
	        (
	        UserID
	        )
    END
    ELSE
    BEGIN
        RAISERROR('ERROR COPYING TABLE, ORIGINAL TABLE NOT REMOVED', 16,1)
    END
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 


