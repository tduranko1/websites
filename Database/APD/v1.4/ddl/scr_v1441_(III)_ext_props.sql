--
--
-- Script for updating APD Database to version 1.3
--

SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


-- Define Codes extended properties

-- Add Codes: utb_admin_fee Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_admin_fee', 'constraint', 'uck_admin_fee_appliestocd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_admin_fee, 'constraint', uck_admin_fee_appliestocd
EXEC sp_addextendedproperty 'Codes', 'F|Fixed|M|MSA|S|State|X|Mixed',
                            'user', dbo, 'table', utb_admin_fee, 'constraint', uck_admin_fee_appliestocd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_admin_fee', 'constraint', 'uck_admin_fee_adminfeecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_admin_fee, 'constraint', uck_admin_fee_adminfeecd
EXEC sp_addextendedproperty 'Codes', 'I|Insurance Paid|M|Mixed|S|Shop Paid',
                            'user', dbo, 'table', utb_admin_fee, 'constraint', uck_admin_fee_adminfeecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_admin_fee', 'constraint', default)


-- Add Codes: utb_admin_fee_msa Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_admin_fee_msa', 'constraint', 'uck_admin_fee_msa_adminfeecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_admin_fee_msa, 'constraint', uck_admin_fee_msa_adminfeecd
EXEC sp_addextendedproperty 'Codes', 'I|Insurance Paid|M|Mixed|S|Shop Paid',
                            'user', dbo, 'table', utb_admin_fee_msa, 'constraint', uck_admin_fee_msa_adminfeecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_admin_fee_msa', 'constraint', default)


-- Add Codes: utb_admin_fee_state Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_admin_fee_state', 'constraint', 'uck_admin_fee_state_adminfeecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_admin_fee_state, 'constraint', uck_admin_fee_state_adminfeecd
EXEC sp_addextendedproperty 'Codes', 'I|Insurance Paid|M|Mixed|S|Shop Paid',
                            'user', dbo, 'table', utb_admin_fee_state, 'constraint', uck_admin_fee_state_adminfeecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_admin_fee_state', 'constraint', default)


-- Add Codes: utb_assignment Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_assignment', 'constraint', 'uck_assignment_programtypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_assignment, 'constraint', uck_assignment_programtypecd
EXEC sp_addextendedproperty 'Codes', 'CEI|CEI|LS|Lynx Select|NON|Non-Program|OOP|Out of Program',
                            'user', dbo, 'table', utb_assignment, 'constraint', uck_assignment_programtypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_assignment', 'constraint', 'uck_assignment_searchtypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_assignment, 'constraint', uck_assignment_searchtypecd
EXEC sp_addextendedproperty 'Codes', 'D|Distance|N|Name',
                            'user', dbo, 'table', utb_assignment, 'constraint', uck_assignment_searchtypecd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_assignment', 'constraint', default)


-- Add Codes: utb_assignment_pool Table
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_assignment_pool', 'constraint', 'uck_assignment_pool_functioncd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_assignment_pool, 'constraint', uck_assignment_pool_functioncd
EXEC sp_addextendedproperty 'Codes', 'OWN|Owner|ALST|Analyst|SPRT|Support',
                                'user', dbo, 'table', utb_assignment_pool, 'constraint', uck_assignment_pool_functioncd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_assignment_pool', 'constraint', default)


-- Add Codes: utb_assignment_type Table
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_assignment_type', 'constraint', 'uck_assignment_type_servicechanneldefaultcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_assignment_type, 'constraint', uck_assignment_type_servicechanneldefaultcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_assignment_type, 'constraint', uck_assignment_type_servicechanneldefaultcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_assignment_type', 'constraint', default)

-- Add Codes: utb_audit_log Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_audit_log, 'constraint', uck_audit_log_audittypecd
EXEC sp_addextendedproperty 'Codes', 'C|Claims|F|Fee|P|Shop|S|Security',
                            'user', dbo, 'table', utb_audit_log, 'constraint', uck_audit_log_audittypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_audit_log', 'constraint', default)


-- Add Codes: utb_billing Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_billing', 'constraint', 'uck_billing_eftaccounttypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_billing, 'constraint', uck_billing_eftaccounttypecd
EXEC sp_addextendedproperty 'Codes', 'C|Checking Account|S|Savings Account',
                            'user', dbo, 'table', utb_billing, 'constraint', uck_billing_eftaccounttypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_billing', 'constraint', default)


-- Add Codes: utb_bundling_document_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_bundling_document_type', 'constraint', 'uck_bundling_document_type_directionalcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_bundling_document_type, 'constraint', uck_bundling_document_type_directionalcd
EXEC sp_addextendedproperty 'Codes', 'I|Received|O|Sent',
                            'user', dbo, 'table', utb_bundling_document_type, 'constraint', uck_bundling_document_type_directionalcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_bundling_document_type', 'constraint', 'uck_bundling_document_type_estimatetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_bundling_document_type, 'constraint', uck_bundling_document_type_estimatetypecd
EXEC sp_addextendedproperty 'Codes', 'A|Audited|O|Original',
                            'user', dbo, 'table', utb_bundling_document_type, 'constraint', uck_bundling_document_type_estimatetypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_bundling_document_type', 'constraint', default)


-- Add Codes: utb_claim Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim', 'constraint', 'uck_claim_trippurposecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim, 'constraint', uck_claim_trippurposecd
EXEC sp_addextendedproperty 'Codes', 'B|Business|P|Pleasure',
                            'user', dbo, 'table', utb_claim, 'constraint', uck_claim_trippurposecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim', 'constraint', default)


-- Add Codes: utb_claim_aspect Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect', 'constraint', 'uck_claim_aspect_coverageprofilecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect, 'constraint', uck_claim_aspect_coverageprofilecd
EXEC sp_addextendedproperty 'Codes', 'COLL|Collision|COMP|Comprehensive|LIAB|Liability|UIM|Underinsured|UM|Uninsured',
                            'user', dbo, 'table', utb_claim_aspect, 'constraint', uck_claim_aspect_coverageprofilecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect', 'constraint', 'uck_claim_aspect_exposurecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect, 'constraint', uck_claim_aspect_exposurecd
EXEC sp_addextendedproperty 'Codes', '1|1st Party|3|3rd Party|N|Not an Exposure',
                            'user', dbo, 'table', utb_claim_aspect, 'constraint', uck_claim_aspect_exposurecd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim_aspect', 'constraint', default)


-- Add Codes: utb_claim_aspect_service_channel Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_service_channel', 'constraint', 'uck_claim_aspect_service_channel_dispositiontypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_dispositiontypecd
EXEC sp_addextendedproperty 'Codes', 'CA|Cancelled|CO|Cash-Out|IC|Insurance Co Managing|IME|Included with ME Estimate|IPD|Included with Body Estimate|NA|Not Authorized|NOCV|Not Covered|PCO|Partial Cash-Out|RC|Repair Complete|RNC|Repair Not Completed|ST|Stale|TL|Total Loss|VD|Voided',
                            'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_dispositiontypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_service_channel', 'constraint', 'uck_claim_aspect_service_channel_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_servicechannelcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim_aspect_service_channel', 'constraint', default)


-- Add Codes: utb_claim_aspect_status Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_status', 'constraint', 'uck_claim_aspect_status_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_servicechannelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_status', 'constraint', 'uck_claim_aspect_status_statustypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_statustypecd
EXEC sp_addextendedproperty 'Codes', 'ELC|Electronic|FAX|Fax|SC|Service Channel',
                            'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_statustypecd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim_aspect_status', 'constraint', default)


-- Add Codes: utb_claim_coverage Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_coverage', 'constraint', 'uck_claim_coverage_coveragetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_coverage, 'constraint', uck_claim_coverage_coveragetypecd
EXEC sp_addextendedproperty 'Codes', 'COLL|Collision|COMP|Comprehensive|GLAS|Glass|LIAB|Liability|RENT|Rental|TOW|Towing|UIM|Underinsured|UM|Uninsured',
                            'user', dbo, 'table', utb_claim_coverage, 'constraint', uck_claim_coverage_coveragetypecd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim_coverage', 'constraint', default)


-- Add Codes: utb_claim_vehicle Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_vehicle', 'constraint', 'uck_claim_vehicle_permissiontodrivecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_vehicle, 'constraint', uck_claim_vehicle_permissiontodrivecd
EXEC sp_addextendedproperty 'Codes', 'N|No|Y|Yes|U|Unknown',
                            'user', dbo, 'table', utb_claim_vehicle, 'constraint', uck_claim_vehicle_permissiontodrivecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_claim_vehicle', 'constraint', default)


-- Add Codes: utb_client_bundling Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_bundling', 'constraint', 'uck_client_bundling_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_bundling, 'constraint', uck_client_bundling_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_client_bundling, 'constraint', uck_client_bundling_servicechannelcd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_client_bundling', 'constraint', default)


-- Add Codes: utb_client_coverage_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_coverage_type', 'constraint', 'uck_client_coverage_type_coverageprofilecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_coverage_type, 'constraint', uck_client_coverage_type_coverageprofilecd
EXEC sp_addextendedproperty 'Codes', 'COLL|Collision|COMP|Comprehensive|GLAS|Glass|LIAB|Liability|RENT|Rental|TOW|Towing|UIM|Underinsured|UM|Uninsured',
                            'user', dbo, 'table', utb_client_coverage_type, 'constraint', uck_client_coverage_type_coverageprofilecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_client_coverage_type', 'constraint', default)


-- Add Codes: utb_client_fee Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_fee', 'constraint', 'uck_client_fee_appliestocd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_fee, 'constraint', uck_client_fee_appliestocd
EXEC sp_addextendedproperty 'Codes', 'B|Billing|C|Claim',
                            'user', dbo, 'table', utb_client_fee, 'constraint', uck_client_fee_appliestocd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_fee', 'constraint', 'uck_client_fee_categorycd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_fee, 'constraint', uck_client_fee_categorycd
EXEC sp_addextendedproperty 'Codes', 'A|Additional Fee|H|Handling Fee',
                            'user', dbo, 'table', utb_client_fee, 'constraint', uck_client_fee_categorycd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_client_fee', 'constraint', default)


-- Add Codes: utb_client_payment_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_payment_type', 'constraint', 'uck_client_payment_type_paymenttypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_payment_type, 'constraint', uck_client_payment_type_paymenttypecd
EXEC sp_addextendedproperty 'Codes', 'E|Expense|I|Indemnity|L|Loss of Use',
                            'user', dbo, 'table', utb_client_payment_type, 'constraint', uck_client_payment_type_paymenttypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_client_payment_type', 'constraint', default)


-- Add Codes: utb_client_service_channel Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_service_channel', 'constraint', 'uck_client_service_channel_invoicingmodelbillingcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_service_channel, 'constraint', uck_client_service_channel_invoicingmodelbillingcd
EXEC sp_addextendedproperty 'Codes', 'B|Combination|C|Claim|E|Exposure',
                            'user', dbo, 'table', utb_client_service_channel, 'constraint', uck_client_service_channel_invoicingmodelbillingcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_client_service_channel', 'constraint', default)


-- Add Codes: utb_criteria Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_criteria', 'constraint', 'uck_criteria_datatypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_criteria, 'constraint', uck_criteria_datatypecd
EXEC sp_addextendedproperty 'Codes', 'B|Boolean|D|Date|N|Numeric|R|Reference',
                            'user', dbo, 'table', utb_criteria, 'constraint', uck_criteria_datatypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_criteria', 'constraint', 'uck_criteria_referencetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_criteria, 'constraint', uck_criteria_referencetypecd
EXEC sp_addextendedproperty 'Codes', 'L|List|S|SQL',
                            'user', dbo, 'table', utb_criteria, 'constraint', uck_criteria_referencetypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_criteria', 'constraint', default)


-- Add Codes: utb_document Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_document', 'constraint', 'uck_document_agreedpricemetcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_document, 'constraint', uck_document_agreedpricemetcd
EXEC sp_addextendedproperty 'Codes', 'N|No|Y|Yes',
                            'user', dbo, 'table', utb_document, 'constraint', uck_document_agreedpricemetcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_document', 'constraint', 'uck_document_directionalcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_document, 'constraint', uck_document_directionalcd
EXEC sp_addextendedproperty 'Codes', 'I|Received|O|Sent',
                            'user', dbo, 'table', utb_document, 'constraint', uck_document_directionalcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_document', 'constraint', 'uck_document_estimatetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_document, 'constraint', uck_document_estimatetypecd
EXEC sp_addextendedproperty 'Codes', 'A|Audited|O|Original',
                            'user', dbo, 'table', utb_document, 'constraint', uck_document_estimatetypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_document', 'constraint', 'uck_document_sendtocarrierstatuscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_document, 'constraint', uck_document_sendtocarrierstatuscd
EXEC sp_addextendedproperty 'Codes', 'F|Failure|NS|Not Sent|S|Sent|UNK|Uknown',
                            'user', dbo, 'table', utb_document, 'constraint', uck_document_sendtocarrierstatuscd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_document', 'constraint', default)


-- Add Codes: utb_document_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_document_type', 'constraint', 'uck_document_type_documentclasscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_document_type, 'constraint', uck_document_type_documentclasscd
EXEC sp_addextendedproperty 'Codes', 'C|Claim|S|Shop',
                            'user', dbo, 'table', utb_document_type, 'constraint', uck_document_type_documentclasscd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_document_type', 'constraint', default)


-- Add Codes: utb_estimate_detail Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_apdpartcategoryagreedcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_apdpartcategoryagreedcd
EXEC sp_addextendedproperty 'Codes', 'PAT|Parts Taxable|PAN|Parts NonTaxable|POT|Parts Other Taxable|PON|Parts Other NonTaxable|STG|Storage|SUB|Sublet|TOW|Towing|OTH|Other',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_apdpartcategoryagreedcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_apdpartcategoryoriginalcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_apdpartcategoryoriginalcd
EXEC sp_addextendedproperty 'Codes', 'PAT|Parts Taxable|PAN|Parts NonTaxable|POT|Parts Other Taxable|PON|Parts Other NonTaxable|STG|Storage|SUB|Sublet|TOW|Towing|OTH|Other',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_apdpartcategoryoriginalcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_labortypeagreedcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_labortypeagreedcd
EXEC sp_addextendedproperty 'Codes', 'B|Body|F|Frame|M|Mechanical',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_labortypeagreedcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_labortypeoriginalcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_labortypeoriginalcd
EXEC sp_addextendedproperty 'Codes', 'B|Body|F|Frame|M|Mechanical',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_labortypeoriginalcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_operationcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_operationcd
EXEC sp_addextendedproperty 'Codes', 'ALGN|Alignment|BLND|Blend|COM|Comment|R&I|Remove/Install|REFN|Refinish|REPR|Repair|REPL|Replace|SUB|Sublet|OTH|Other|!ND!|Not Defined',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_operationcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_parttypeagreedcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_parttypeagreedcd
EXEC sp_addextendedproperty 'Codes', 'AFMK|Aftermarket|GLS|Glass|LKQ|LKQ|NEW|New|REC|Reconditioned|SUB|Sublet|OTH|Other',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_parttypeagreedcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_detail', 'constraint', 'uck_estimate_detail_parttypeoriginalcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_parttypeoriginalcd
EXEC sp_addextendedproperty 'Codes', 'AFMK|Aftermarket|GLS|Glass|LKQ|LKQ|NEW|New|REC|Reconditioned|SUB|Sublet|OTH|Other',
                            'user', dbo, 'table', utb_estimate_detail, 'constraint', uck_estimate_detail_parttypeoriginalcd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_estimate_detail', 'constraint', default)


-- Add Codes: utb_estimate_summary Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_summary', 'constraint', 'uck_estimate_summary_taxtypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_summary, 'constraint', uck_estimate_summary_taxtypecd
EXEC sp_addextendedproperty 'Codes', 'AC|Add Chrg|LB|Labor|MT|P&M|PT|Parts|SP|Supplies|ST|Subtotal|OT|Other',
                            'user', dbo, 'table', utb_estimate_summary, 'constraint', uck_estimate_summary_taxtypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_estimate_summary', 'constraint', default)


-- Add Codes: utb_estimate_summary_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_estimate_summary_type', 'constraint', 'uck_estimate_summary_type_categorycd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_estimate_summary_type, 'constraint', uck_estimate_summary_type_categorycd
EXEC sp_addextendedproperty 'Codes', 'AC|Add Chrg|AJ|Adjustments|CP|Contract Price|LC|Labor Cost|MC|Material Parts|PC|Part Category|TT|Total|TX|Tax|VA|Variance|OT|Other',
                            'user', dbo, 'table', utb_estimate_summary_type, 'constraint', uck_estimate_summary_type_categorycd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_estimate_summary_type', 'constraint', default)


-- Add Codes: utb_insurance Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_billingmodelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_billingmodelcd
EXEC sp_addextendedproperty 'Codes', 'C|Claim|E|Exposure',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_billingmodelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_businesstypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_businesstypecd
EXEC sp_addextendedproperty 'Codes', 'C|Corporation|P|Partnership|S|Sole Proprietor',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_businesstypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_invoicemethodcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_invoicemethodcd
EXEC sp_addextendedproperty 'Codes', 'F|File|P|Paper',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_invoicemethodcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_invoicingmodelpaymentcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_invoicingmodelpaymentcd
EXEC sp_addextendedproperty 'Codes', 'B|Bulk|C|Per Claim',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_invoicingmodelpaymentcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_licensedeterminationcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_licensedeterminationcd
EXEC sp_addextendedproperty 'Codes', 'INSD|Insured''s Residence|LOSS|Loss State|VLOC|Vehicle Location',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_licensedeterminationcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_returndocdestinationcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_returndocdestinationcd
EXEC sp_addextendedproperty 'Codes', 'REP|Claim Representative|OFC|Office|BOTH|Claim Rep and Office',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_returndocdestinationcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_returndocroutingcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_returndocroutingcd
EXEC sp_addextendedproperty 'Codes', 'CCAV|CCC Autoverse|EML|Email|FAX|Fax|SG|Scene Genesis',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_returndocroutingcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_warrantyperiodrefinishmincd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_warrantyperiodrefinishmincd
EXEC sp_addextendedproperty 'Codes', '0|None|1|1 Year|3|3 Year|5|5 Year|99|Lifetime',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_warrantyperiodrefinishmincd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_insurance', 'constraint', 'uck_insurance_warrantyperiodworkmanshipmincd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_warrantyperiodworkmanshipmincd
EXEC sp_addextendedproperty 'Codes', '0|None|1|1 Year|3|3 Year|5|5 Year|99|Lifetime',
                            'user', dbo, 'table', utb_insurance, 'constraint', uck_insurance_warrantyperiodworkmanshipmincd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_insurance', 'constraint', default)


-- Add Codes: utb_invoice Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice', 'constraint', 'uck_invoice_itemtypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_itemtypecd
EXEC sp_addextendedproperty 'Codes', 'E|Expense|F|Fee|I|Indemnity|L|Loss of Use',
                            'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_itemtypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice', 'constraint', 'uck_invoice_feecategorycd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_feecategorycd
EXEC sp_addextendedproperty 'Codes', 'A|Additional Fee|H|Handling Fee',
                            'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_feecategorycd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice', 'constraint', 'uck_invoice_payeetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_payeetypecd
EXEC sp_addextendedproperty 'Codes', 'S|Shop|A|Appraiser|I|Involved|V|Vendor|O|Other',
                            'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_payeetypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice', 'constraint', 'uck_invoice_paymentchannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_paymentchannelcd
EXEC sp_addextendedproperty 'Codes', 'BOTH|Both|CCC|CCC|PCSO|P-CCSO|STD|Standard',
                            'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_paymentchannelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice', 'constraint', 'uck_invoice_statuscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_statuscd
EXEC sp_addextendedproperty 'Codes', 'APD|APD|AC|Awaiting Client Authorization|AUTH|Authorized|FS|Financial Services|NB|Non-billable|PU|Picked Up',
                            'user', dbo, 'table', utb_invoice, 'constraint', uck_invoice_statuscd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_invoice', 'constraint', default)


-- Add Codes: utb_invoice_service Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice_service', 'constraint', 'uck_invoice_service_dispositiontypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_dispositiontypecd
EXEC sp_addextendedproperty 'Codes', 'CA|Cancelled|CO|Cash-Out|NA|Not Authorized|RC|Repair Complete|ST|Stale|TL|Total Loss|VD|Voided',
                            'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_dispositiontypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice_service', 'constraint', 'uck_invoice_service_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_servicechannelcd
EXEC sp_addextendedproperty 'Codes', '1P|1st Party|3P|3rd Party|DA|Desk Audit|DR|Damage Reinspection|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_servicechannelcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_invoice_service', 'constraint', default)


-- Add Codes: utb_involved Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved, 'constraint', uck_involved_bestcontactphonecd
EXEC sp_addextendedproperty 'Codes', 'A|Alternate|D|Day|N|Night',
                            'user', dbo, 'table', utb_involved, 'constraint', uck_involved_bestcontactphonecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved, 'constraint', uck_involved_businesstypecd
EXEC sp_addextendedproperty 'Codes', 'C|Corporation|P|Partnership|S|Sole Proprietor',
                            'user', dbo, 'table', utb_involved, 'constraint', uck_involved_businesstypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved, 'constraint', uck_involved_gendercd
EXEC sp_addextendedproperty 'Codes', 'M|Male|F|Female|U|Unknown',
                            'user', dbo, 'table', utb_involved, 'constraint', uck_involved_gendercd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved, 'constraint', uck_involved_injuredcd
EXEC sp_addextendedproperty 'Codes', 'N|No|Y|Yes|U|Unknown',
                            'user', dbo, 'table', utb_involved, 'constraint', uck_involved_injuredcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_audit_log', 'constraint', 'uck_audit_log_audittypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved, 'constraint', uck_involved_seatbeltcd
EXEC sp_addextendedproperty 'Codes', 'N|No|Y|Yes|U|Unknown',
                            'user', dbo, 'table', utb_involved, 'constraint', uck_involved_seatbeltcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_involved', 'constraint', default)


-- Add Codes: utb_involved_role_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_involved_role_type', 'constraint', 'uck_involved_role_type_exposurecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_involved_role_type, 'constraint', uck_involved_role_type_exposurecd
EXEC sp_addextendedproperty 'Codes', '1|1st Party|3|3rd Party',
                            'user', dbo, 'table', utb_involved_role_type, 'constraint', uck_involved_role_type_exposurecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_involved_role_type', 'constraint', default)


-- Add Codes: utb_message_template Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_message_template', 'constraint', 'uck_message_template_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_message_template, 'constraint', uck_message_template_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_message_template, 'constraint', uck_message_template_servicechannelcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_message_template', 'constraint', default)


-- Add Codes: utb_permission Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_permission', 'constraint', 'uck_permission_businessfunctioncd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_permission, 'constraint', uck_permission_businessfunctioncd
EXEC sp_addextendedproperty 'Codes', '|None|C|Claim|S|Shop',
                            'user', dbo, 'table', utb_permission, 'constraint', uck_permission_businessfunctioncd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_permission', 'constraint', 'uck_permission_groupcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_permission, 'constraint', uck_permission_groupcd
EXEC sp_addextendedproperty 'Codes', 'A|Action|D|Data|R|Report',
                            'user', dbo, 'table', utb_permission, 'constraint', uck_permission_groupcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_permission', 'constraint', default)


-- Add Codes: utb_personnel Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_personnel', 'constraint', 'uck_personnel_gendercd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_personnel, 'constraint', uck_personnel_gendercd
EXEC sp_addextendedproperty 'Codes', 'M|Male|F|Female|U|Unknown',
                            'user', dbo, 'table', utb_personnel, 'constraint', uck_personnel_gendercd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_personnel', 'constraint', default)


-- Add Codes: utb_personnel_type Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_personnel_type', 'constraint', 'uck_personnel_type_appliestocd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_personnel_type, 'constraint', uck_personnel_type_appliestocd
EXEC sp_addextendedproperty 'Codes', 'L|Location|S|Shop',
                            'user', dbo, 'table', utb_personnel_type, 'constraint', uck_personnel_type_appliestocd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_personnel_type', 'constraint', default)


-- Add Codes: utb_profile Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_profile', 'constraint', 'uck_profile_businessfunctioncd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_profile, 'constraint', uck_profile_businessfunctioncd
EXEC sp_addextendedproperty 'Codes', '|None|C|Claim|S|Shop',
                            'user', dbo, 'table', utb_profile, 'constraint', uck_profile_businessfunctioncd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_profile', 'constraint', 'uck_profile_datatypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_profile, 'constraint', uck_profile_datatypecd
EXEC sp_addextendedproperty 'Codes', 'B|Boolean|N|Number|S|Selection',
                            'user', dbo, 'table', utb_profile, 'constraint', uck_profile_datatypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_profile', 'constraint', 'uck_profile_reconciliationcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_profile, 'constraint', uck_profile_reconciliationcd
EXEC sp_addextendedproperty 'Codes', 'P|Primary Role|W|Weight',
                            'user', dbo, 'table', utb_profile, 'constraint', uck_profile_reconciliationcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_profile', 'constraint', default)


-- Add Codes: utb_reinspect Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_reinspect', 'constraint', 'uck_reinspect_reinspectclasscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_reinspectclasscd
EXEC sp_addextendedproperty 'Codes', 'DR|Desk Review|PI|Physical Inspection',
                            'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_reinspectclasscd
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_reinspecttypecd
EXEC sp_addextendedproperty 'Codes', 'IND|Independent|LS|LYNX Select|SUP|Supervisor',
                            'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_reinspecttypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_reinspect', 'constraint', 'uck_reinspect_repairstatuscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_repairstatuscd
EXEC sp_addextendedproperty 'Codes', 'A|After|B|Before|D|During|S|Supplement|T|Total Loss',
                            'user', dbo, 'table', utb_reinspect, 'constraint', uck_reinspect_repairstatuscd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_reinspect', 'constraint', default)


-- Add Codes: utb_report Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_report', 'constraint', 'uck_report_reporttypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_report, 'constraint', uck_report_reporttypecd
EXEC sp_addextendedproperty 'Codes', 'C|Client Report|M|Management Report|O|Operational Report|P|Program Shop|U|User Management',
                            'user', dbo, 'table', utb_report, 'constraint', uck_report_reporttypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_report', 'constraint', default)

-- Add Codes: utb_report_export_format Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_report_export_format', 'constraint', 'uck_report_export_format_exportformatcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_report_export_format, 'constraint', uck_report_export_format_exportformatcd
EXEC sp_addextendedproperty 'Codes', 'DOC|MS Word|PDF|Adobe Acrobat PDF|RTF|Rich Text|XLS|MS Excel',
                            'user', dbo, 'table', utb_report_export_format, 'constraint', uck_report_export_format_exportformatcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_report', 'constraint', default)


-- Add Codes: utb_role Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_role', 'constraint', 'uck_role_businessfunctioncd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_role, 'constraint', uck_role_businessfunctioncd
EXEC sp_addextendedproperty 'Codes', '|None|C|Claim|S|Shop',
                            'user', dbo, 'table', utb_role, 'constraint', uck_role_businessfunctioncd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_role', 'constraint', default)


-- Add Codes: utb_service Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service', 'constraint', 'uck_service_billingmodelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service, 'constraint', uck_service_billingmodelcd
EXEC sp_addextendedproperty 'Codes', 'C|Claim|E|Exposure',
                            'user', dbo, 'table', utb_service, 'constraint', uck_service_billingmodelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service', 'constraint', 'uck_service_dispositiontypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service, 'constraint', uck_service_dispositiontypecd
EXEC sp_addextendedproperty 'Codes', 'CA|Cancelled|CO|Cash-Out|NA|Not Authorized|RC|Repair Complete|ST|Stale|TL|Total Loss|VD|Voided|?|User Selected',
                            'user', dbo, 'table', utb_service, 'constraint', uck_service_dispositiontypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service', 'constraint', 'uck_service_partycd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service, 'constraint', uck_service_partycd
EXEC sp_addextendedproperty 'Codes', '1|1st Party|3|3rd Party',
                            'user', dbo, 'table', utb_service, 'constraint', uck_service_partycd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service', 'constraint', 'uck_service_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service, 'constraint', uck_service_servicechannelcd
EXEC sp_addextendedproperty 'Codes', '1P|1st Party|3P|3rd Party|DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|?|User Selected',
                            'user', dbo, 'table', utb_service, 'constraint', uck_service_servicechannelcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_service', 'constraint', default)

-- Add Codes: utb_service_channel_schedule_change_reason Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service_channel_schedule_change_reason', 'constraint', 'uck_service_channel_schedule_change_reason_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service_channel_schedule_change_reason, 'constraint', uck_service_channel_schedule_change_reason_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_service_channel_schedule_change_reason, 'constraint', uck_service_channel_schedule_change_reason_servicechannelcd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_service_channel_schedule_change_reason', 'constraint', default)


-- Add Codes: utb_shop Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop', 'constraint', 'uck_shop_businesstypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop, 'constraint', uck_shop_businesstypecd
EXEC sp_addextendedproperty 'Codes', 'C|Corporation|P|Partnership|S|Sole Proprietor',
                            'user', dbo, 'table', utb_shop, 'constraint', uck_shop_businesstypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_shop', 'constraint', default)


-- Add Codes: utb_shop_load Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_businesstypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_businesstypecd
EXEC sp_addextendedproperty 'Codes', 'C|Corporation|P|Partnership|S|Sole Proprietor',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_businesstypecd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_warrantyperiodrefinishcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_warrantyperiodrefinishcd
EXEC sp_addextendedproperty 'Codes', '3|3 Year|5|5 Year|7|7 Year|10|10 Year|15|15 Year|20|20 Year|25|25 Year|99|Lifetime',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_warrantyperiodrefinishcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_warrantyperiodworkmanshipcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_warrantyperiodworkmanshipcd
EXEC sp_addextendedproperty 'Codes', '3|3 Year|5|5 Year|7|7 Year|10|10 Year|15|15 Year|20|20 Year|25|25 Year|99|Lifetime',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_warrantyperiodworkmanshipcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_slpgendercd'))
    EXEC sp_dropextendedproperty 'Codes', 
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_slpgendercd
EXEC sp_addextendedproperty 'Codes', 'M|Male|F|Female|U|Unknown',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_slpgendercd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_spgendercd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_spgendercd
EXEC sp_addextendedproperty 'Codes', 'M|Male|F|Female|U|Unknown',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_spgendercd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_load', 'constraint', 'uck_shop_load_repairfacilitytypecd'))
    EXEC sp_dropextendedproperty 'Codes', 
                                'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_repairfacilitytypecd
EXEC sp_addextendedproperty 'Codes', 'C|Consolidator|D|Dealership|F|Franchise|I|Independent',
                            'user', dbo, 'table', utb_shop_load, 'constraint', uck_shop_load_repairfacilitytypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_shop_load', 'constraint', default)


-- Add Codes: utb_shop_location Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location', 'constraint', 'uck_shop_location_datareviewstatuscd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_datareviewstatuscd
EXEC sp_addextendedproperty 'Codes', 'A|Approved|I|Approval In Process|N|Not Approved',
                            'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_datareviewstatuscd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location', 'constraint', 'uck_shop_location_ppgctslevelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_ppgctslevelcd
EXEC sp_addextendedproperty 'Codes', 'BRZ|Bronze|SLV|Silver|GLD|Gold|PLT|Platinum|PRF|Preferred',
                            'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_ppgctslevelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location', 'constraint', 'uck_shop_location_warrantyperiodrefinishcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_warrantyperiodrefinishcd
EXEC sp_addextendedproperty 'Codes', '3|3 Year|5|5 Year|7|7 Year|10|10 Year|15|15 Year|20|20 Year|25|25 Year|99|Lifetime',
                            'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_warrantyperiodrefinishcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location', 'constraint', 'uck_shop_location_warrantyperiodworkmanshipcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_warrantyperiodworkmanshipcd
EXEC sp_addextendedproperty 'Codes', '3|3 Year|5|5 Year|7|7 Year|10|10 Year|15|15 Year|20|20 Year|25|25 Year|99|Lifetime',
                            'user', dbo, 'table', utb_shop_location, 'constraint', uck_shop_location_warrantyperiodworkmanshipcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_shop_location', 'constraint', default)


-- Add Codes: utb_shop_location_pricing Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location_pricing', 'constraint', 'uck_shop_location_pricing_glassreplacementchargetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location_pricing, 'constraint', uck_shop_location_pricing_glassreplacementchargetypecd
EXEC sp_addextendedproperty 'Codes', 'N|NAGS|O|Outsource|S|Sublet',
                            'user', dbo, 'table', utb_shop_location_pricing, 'constraint', uck_shop_location_pricing_glassreplacementchargetypecd

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location_pricing', 'constraint', 'uck_shop_location_pricing_towinchargetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location_pricing, 'constraint', uck_shop_location_pricing_towinchargetypecd
EXEC sp_addextendedproperty 'Codes', 'A|Actual|F|Flat Fee|N|No Charge',
                            'user', dbo, 'table', utb_shop_location_pricing, 'constraint', uck_shop_location_pricing_towinchargetypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_shop_location_pricing', 'constraint', default)


-- Add Codes: utb_shop_location_pricing_history Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location_pricing_history', 'constraint', 'uck_shop_location_pricing_history_glassreplacementchargetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location_pricing_history, 'constraint', uck_shop_location_pricing_history_glassreplacementchargetypecd
EXEC sp_addextendedproperty 'Codes', 'N|NAGS|O|Outsource|S|Sublet',
                            'user', dbo, 'table', utb_shop_location_pricing_history, 'constraint', uck_shop_location_pricing_history_glassreplacementchargetypecd

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_shop_location_pricing_history', 'constraint', 'uck_shop_location_pricing_history_towinchargetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_shop_location_pricing_history, 'constraint', uck_shop_location_pricing_history_towinchargetypecd
EXEC sp_addextendedproperty 'Codes', 'A|Actual|F|Flat Fee|N|No Charge',
                            'user', dbo, 'table', utb_shop_location_pricing_history, 'constraint', uck_shop_location_pricing_history_towinchargetypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_shop_location_pricing_history', 'constraint', default)


-- Add Codes: utb_spawn Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_spawn', 'constraint', 'uck_spawn_spawningtypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_spawn, 'constraint', uck_spawn_spawningtypecd
EXEC sp_addextendedproperty 'Codes', 'C|Custom|E|Event|S|State|T|Task',
                            'user', dbo, 'table', utb_spawn, 'constraint', uck_spawn_spawningtypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_spawn', 'constraint', default)


-- Add Codes: utb_state_code Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_state_code', 'constraint', 'uck_state_code_negligencetypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_state_code, 'constraint', uck_state_code_negligencetypecd
EXEC sp_addextendedproperty 'Codes', 'C|Contributory|M|Modified Comparative|P|Pure Comparative',
                            'user', dbo, 'table', utb_state_code, 'constraint', uck_state_code_negligencetypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_state_code', 'constraint', default)


-- Add Codes: utb_status Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_status', 'constraint', 'uck_status_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_status, 'constraint', uck_status_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_status, 'constraint', uck_status_servicechannelcd
IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_status', 'constraint', 'uck_status_statustypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_status, 'constraint', uck_status_statustypecd
EXEC sp_addextendedproperty 'Codes', 'ELC|Electronic|FAX|Fax|SC|Service Channel',
                            'user', dbo, 'table', utb_status, 'constraint', uck_status_statustypecd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_status', 'constraint', default)


-- Add Codes: utb_task_assignment_pool Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_task_assignment_pool', 'constraint', 'uck_task_assignment_pool_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_task_assignment_pool, 'constraint', uck_task_assignment_pool_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_task_assignment_pool, 'constraint', uck_task_assignment_pool_servicechannelcd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_task_assignment_pool', 'constraint', default)


-- Add Codes: utb_task_escalation Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_task_escalation', 'constraint', 'uck_task_escalation_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_task_escalation, 'constraint', uck_task_escalation_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser',
                            'user', dbo, 'table', utb_task_escalation, 'constraint', uck_task_escalation_servicechannelcd


SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_task_escalation', 'constraint', default)


-- Add Codes: utb_vendor Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_vendor', 'constraint', 'uck_vendor_vendortypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_vendor, 'constraint', uck_vendor_vendortypecd
EXEC sp_addextendedproperty 'Codes', 'ME|Mobile Electronics|R|Rental',
                            'user', dbo, 'table', utb_vendor, 'constraint', uck_vendor_vendortypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_vendor', 'constraint', default)


-- Add Codes: utb_workflow Table

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_workflow', 'constraint', 'uck_workflow_originatortypecd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_workflow, 'constraint', uck_workflow_originatortypecd
EXEC sp_addextendedproperty 'Codes', 'E|Event|S|State|T|Task',
                            'user', dbo, 'table', utb_workflow, 'constraint', uck_workflow_originatortypecd

SELECT *
FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user','dbo','table',
                               'utb_workflow', 'constraint', default)


GO

