--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_spawn'                --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                              !!!!
--!!!!!             SpawningServiceChannelCD
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    DROP TABLE dbo.utb_spawn

    CREATE TABLE dbo.utb_spawn 
    (
        SpawnID            [udt_std_id]                IDENTITY,
        WorkflowID         [udt_std_int_small]         NOT NULL,
        ConditionalValue   [udt_std_desc_mid]          NULL,
        ContextNote        [udt_std_desc_mid]          NULL,
        CustomProcName     [udt_std_desc_short]        NULL,
        EnabledFlag        [udt_enabled_flag]          NOT NULL,
        SpawningID         [udt_std_int]               NOT NULL,
        SpawningServiceChannelCD     [udt_std_cd]      NULL,
        SpawningTypeCD     [udt_std_cd]                NOT NULL,
        SysLastUserID      [udt_std_id]                NOT NULL,
        SysLastUpdatedDate [udt_sys_last_updated_date] NOT NULL,
        CONSTRAINT upk_spawn
        PRIMARY KEY CLUSTERED (SpawnID) WITH FILLFACTOR=90 
                                                        ON ufg_workflow,
    CONSTRAINT uck_spawn_spawningtypecd
        CHECK ([SpawningTypeCD] = 'C' or [SpawningTypeCD] = 'S' or [SpawningTypeCD] = 'E' or [SpawningTypeCD] = 'T'),
    CONSTRAINT uck_claim_aspect_service_channel_spawningservicechannelcd
        CHECK (SpawningServiceChannelCD = 'DA' or SpawningServiceChannelCD = 'DR' or SpawningServiceChannelCD = 'GL' or SpawningServiceChannelCD = 'IA' or SpawningServiceChannelCD = 'ME' or SpawningServiceChannelCD = 'PS' or SpawningServiceChannelCD = 'SA'),
    CONSTRAINT ufk_spawn_syslastuserid_in_user
        FOREIGN KEY (SysLastUserID)
        REFERENCES dbo.utb_user (UserID),
    CONSTRAINT ufk_spawn_workflowid_in_workflow
        FOREIGN KEY (WorkflowID)
        REFERENCES dbo.utb_workflow (WorkflowID)
    )
    ON ufg_workflow
    

    CREATE INDEX uix_ie_spawn_workflowid
        ON dbo.utb_spawn(WorkflowID)
      WITH FILLFACTOR = 90
        ON ufg_workflow_index
    


    -- Capture any error

    SELECT @error = @@ERROR
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

