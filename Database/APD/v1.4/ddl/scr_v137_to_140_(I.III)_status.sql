--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_status'                       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             ClaimAspectServiceChannelID, StatusTypeCD             !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_status
	    DROP CONSTRAINT ufk_status_claimaspecttypeid_in_claim_aspect_type

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_status
	    DROP CONSTRAINT ufk_status_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp_utb_status
	    (
	    StatusID udt_std_int_small NOT NULL,
	    ClaimAspectTypeID udt_std_int_tiny NOT NULL,
	    DisplayOrder udt_std_int_tiny NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    LogToHistoryFlag udt_std_flag NOT NULL,
	    Name udt_std_name NOT NULL,
	    ServiceChannelCD udt_std_cd NULL,
        StatusTypeCD udt_std_cd NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_reference

    IF EXISTS(SELECT * FROM dbo.utb_status)
	     EXEC('INSERT INTO dbo.Tmp_utb_status (StatusID, ClaimAspectTypeID, DisplayOrder, EnabledFlag, LogToHistoryFlag, Name, SysLastUserID, SysLastUpdatedDate)
		    SELECT StatusID, ClaimAspectTypeID, DisplayOrder, EnabledFlag, LogToHistoryFlag, Name, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_status (HOLDLOCK TABLOCKX)')

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_statusid_in_status

    ALTER TABLE dbo.utb_document
	    DROP CONSTRAINT ufk_document_statusid_in_status

    ALTER TABLE dbo.utb_status_task
	    DROP CONSTRAINT ufk_status_task_statusid_in_status

    DROP TABLE dbo.utb_status

    EXECUTE sp_rename N'dbo.Tmp_utb_status', N'utb_status', 'OBJECT'

    ALTER TABLE dbo.utb_status ADD CONSTRAINT
	    upk_status PRIMARY KEY CLUSTERED 
	    (
	    StatusID
	    ) WITH FILLFACTOR = 100 ON ufg_reference


    CREATE NONCLUSTERED INDEX uix_ie_status_name ON dbo.utb_status
	    (
	    Name
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    CREATE NONCLUSTERED INDEX uix_ie_status_claimaspecttypeid_name ON dbo.utb_status
	    (
	    ClaimAspectTypeID,
	    Name
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    CREATE NONCLUSTERED INDEX uix_ie_status_claimaspecttypeid ON dbo.utb_status
	    (
	    ClaimAspectTypeID
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    ALTER TABLE dbo.utb_status ADD CONSTRAINT
	    ufk_status_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_status ADD CONSTRAINT
	    ufk_status_claimaspecttypeid_in_claim_aspect_type FOREIGN KEY
	    (
	    ClaimAspectTypeID
	    ) REFERENCES dbo.utb_claim_aspect_type
	    (
	    ClaimAspectTypeID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_status_task ADD CONSTRAINT
	    ufk_status_task_statusid_in_status FOREIGN KEY
	    (
	    StatusID
	    ) REFERENCES dbo.utb_status
	    (
	    StatusID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_document ADD CONSTRAINT
	    ufk_document_statusid_in_status FOREIGN KEY
	    (
	    StatusID
	    ) REFERENCES dbo.utb_status
	    (
	    StatusID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_statusid_in_status FOREIGN KEY
	    (
	    StatusID
	    ) REFERENCES dbo.utb_status
	    (
	    StatusID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

