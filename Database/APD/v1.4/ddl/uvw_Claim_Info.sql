-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the data view

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uvw_Claim_Info' AND type = 'V')
BEGIN
    DROP view dbo.uvw_Claim_Info 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* VIEW:         uvw_Claim_Info
* SYSTEM:       LYNX Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Claim information data view
*
* DESCRIPTION:  The view displays the claim, service channels, statuses for vehicles and service channels and assignments
*               and coverage information
*               
* RESULT SET:
*
*
*
************************************************************************************************************************/

-- Create the data view

Create view uvw_Claim_Info
as


    select            --************************************
                      --Columns from utb_Claim table
                      --************************************
                      c.LynxID,
                      c.InsuranceCompanyID,
                      c.ClientClaimNumber,
                      c.ClientClaimNumberSquished,
                        
                      --***********************************************
                      --Columns from utb_Claim_Aspect table for Claim
                      --***********************************************
                      cac.ClaimAspectID as 'ClaimAspectID_Claim',
                      cs.StatusID as 'ClaimStatusID',
                      cs.Name as 'ClaimStatusDesc',
    
                      --***********************************************
                      --Columns from utb_Claim_Aspect table for Vehicle
                      --***********************************************
                      cav.ClaimAspectID as 'ClaimAspectID_Vehicle',
                      vs.StatusID as 'VehicleStatusID',
                      vs.Name as 'VehicleStatusDesc',
                      cav.ClaimAspectNumber,
                      cav.ClientCoverageTypeID,

                      --*********************************************
                      --Columns from utb_Claim_Aspect_Service_Channel
                      --*********************************************
                      casc.ClaimAspectServiceChannelID,
                      ref1.Code as 'ServiceChannelCD',
                      ref1.Name as 'ServiceChannelCDDesc',
                      scs.StatusID as 'ServiceChannelStatusID',
                      scs.Name as 'ServiceChannelStatusIDDesc',
                      es.StatusID as 'ElectronicAssignmentStatusID',
                      es.Name as 'ElectronicAssignmentStatusIDDesc',
                      fs.StatusID as 'FaxAssignmentStatusID',
                      fs.Name as 'FaxAssignmentStatusIDDesc',
                      casc.DispositionTypeCD,
                      casc.EnabledFlag as 'ServiceChannelEnabledFlag',
                      casc.PrimaryFlag,
                      casc.CashOutDate,
    
                      --**************************************
                      --Columns from utb_Claim_Coverage table
                      --**************************************
                      cc.ClaimCoverageID,
                      cc.AddtlCoverageFlag,
                      cc.CoverageTypeCD,
                      cc.DeductibleAmt,
                      cc.EnabledFlag as 'ClaimCoverageEnabledFlag',
                      cc.LimitAmt,
                      cc.LimitDailyAmt,
                      cc.MaximumDays,
    
                      --******************************************************
                      --Columns from utb_Claim_Aspect_Service_Channel_Coverage
                      --******************************************************
                      cascc.DeductibleAppliedAmt,
                      cascc.LimitAppliedAmt,
                      cascc.PartialCoverageFlag
     
    
    from              utb_Claim c

    inner join        utb_Claim_Aspect cac
    on                cac.LynxID = c.LynxID

    inner join        utb_claim_aspect_type cact
    on                cac.ClaimAspectTypeID = cact.ClaimAspectTypeID

    left outer join   utb_Claim_Aspect_Status cacs -- For Claim Status
    on                cacs.ClaimAspectID = cac.ClaimAspectID
    
    left outer join   utb_Status cs
    on                cs.StatusID = cacs.StatusID

    inner join        utb_Claim_Aspect cav
    on                cav.LynxID = c.LynxID
  
    inner join        utb_claim_aspect_type cavt
    on                cav.ClaimAspectTypeID = cavt.ClaimAspectTypeID
    
    inner join        utb_Claim_Aspect_Service_Channel casc
    on                cav.ClaimAspectID = casc.ClaimAspectID
    
    Inner join        dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel','ServiceChannelCD') ref1
    on                casc.ServiceChannelCD = ref1.Code
    
    
    left outer join   utb_Claim_Aspect_Status vcas -- For Vehicle Status
    on                vcas.ClaimAspectID = cav.ClaimAspectID
    
    left outer join   utb_Status vs
    on                vs.StatusID = vcas.StatusID
    
    
    left outer join   utb_Claim_Aspect_Status sccas -- For Service Channel Status
    on                sccas.ClaimAspectID = cav.ClaimAspectID
    and               sccas.ServiceChannelCD = casc.ServiceChannelCD
    and               sccas.StatusTypeCD = 'SC'
    
    left outer join   utb_Status scs
    on                scs.StatusID = sccas.StatusID
    
    
    
    left outer join   utb_Claim_Aspect_Status ecas -- For "ELC" Assignment Status
    on                ecas.ClaimAspectID = cav.ClaimAspectID
    and               ecas.ServiceChannelCD = casc.ServiceChannelCD
    and               ecas.StatusTypeCD = 'ELC'
    
    
    left outer join   utb_Status es
    on                es.StatusID = ecas.StatusID
    
    left outer join   utb_Claim_Aspect_Status fcas -- For "Fax" Assignment Status
    on                fcas.ClaimAspectID = cav.ClaimAspectID
    and               fcas.ServiceChannelCD = casc.ServiceChannelCD
    and               fcas.StatusTypeCD = 'FAX'
    
    left outer join   utb_Status fs
    on                fs.StatusID = fcas.StatusID
    
    Left Outer join   utb_Client_Coverage_Type cct
    on                c.InsuranceCompanyID = cct.InsuranceCompanyID
    and               cct.ClientCoverageTypeID = cav.ClientCoverageTypeID
    and               cct.CoverageProfileCD = cav.CoverageProfileCD
    
    Left Outer join   utb_Claim_Coverage cc
    on                cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
    and               c.LynxID = cc.LynxID
    and               cc.CoverageTypeCD = cct.CoverageProfileCD
    
    left outer join   utb_claim_aspect_service_channel_coverage cascc
    on                cascc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    and               cascc.ClaimCoverageID = cc.ClaimCoverageID
    
    
    
    
    where             cacs.ServiceChannelCD is null
    and               cacs.StatusTypeCD is null
    and               cact.Name = 'Claim'

    and               vcas.ServiceChannelCD is null
    and               vcas.StatusTypeCD is null
    and               cavt.Name = 'Vehicle'

     

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uvw_Claim_Info' AND type = 'V')
BEGIN
    GRANT SELECT ON dbo.uvw_Claim_Info TO ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the data view

    RAISERROR ('Data view creation failure.', 16, 1)
    ROLLBACK
END

GO


     

