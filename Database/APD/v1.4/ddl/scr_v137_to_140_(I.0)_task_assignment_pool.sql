ALTER TABLE dbo.utb_task_assignment_pool DROP CONSTRAINT uck_task_assignment_pool_servicechannelcd
GO
ALTER TABLE dbo.utb_task_assignment_pool 
    ADD CONSTRAINT uck_task_assignment_pool_servicechannelcd
    CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'GL')
GO
