--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_dtwh_fact_claim'              --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_dtwh_fact_claim
	    (
	    FactID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AssignmentTypeClosingID udt_std_id NULL,
	    AssignmentTypeID udt_std_id NULL,
	    ClaimAspectID udt_std_id_big NOT NULL,
	    ClaimLocationID udt_std_id NOT NULL,
	    ClaimSourceID udt_std_id NOT NULL,
	    CoverageTypeID udt_std_id NULL,
	    CustomerID udt_std_id_big NOT NULL,
	    DispositionTypeID udt_std_id NULL,
	    LynxHandlerAnalystID udt_std_id_big NULL,
	    LynxHandlerOwnerID udt_std_id_big NULL,
	    LynxHandlerSupportID udt_std_id_big NULL,
	    RepairLocationID udt_std_id_big NULL,
	    ServiceChannelID udt_std_id NULL,
	    TimeIDAssignDownload udt_std_id_big NULL,
	    TimeIDAssignSent udt_std_id_big NULL,
	    TimeIDCancelled udt_std_id_big NULL,
	    TimeIDClosed udt_std_id_big NULL,
	    TimeIDEstimate udt_std_id_big NULL,
	    TimeIDInspection udt_std_id_big NULL,
	    TimeIDNew udt_std_id_big NOT NULL,
	    TimeIDReclosed udt_std_id_big NULL,
	    TimeIDRepairComplete udt_std_id_big NULL,
	    TimeIDRepairCompletePromise udt_std_id_big NULL,
	    TimeIDRepairStarted udt_std_id_big NULL,
	    TimeIDReopened udt_std_id_big NULL,
	    TimeIDVoided udt_std_id_big NULL,
	    VehicleNumber udt_std_id NOT NULL,
	    VehicleLicensePlateStateID udt_std_id NULL,
	    AuditedEstimateAgreedFlag udt_std_flag NULL,
	    AuditedEstimateBettermentAmt udt_std_money NULL,
	    AuditedEstimateDeductibleAmt udt_std_money NULL,
	    AuditedEstimateGrossAmt udt_std_money NULL,
	    AuditedEstimateNetAmt udt_std_money NULL,
	    AuditedEstimateOtherAdjustmentAmt udt_std_money NULL,
	    AuditedEstimatewoBettAmt udt_std_money NULL,
	    CFProgramFlag udt_std_flag NULL,
	    ClaimStatusCD udt_std_cd NOT NULL,
	    ClientClaimNumber udt_cov_claim_number NULL,
	    CycleTimeAssignToEstBusDay udt_std_int_small NULL,
	    CycleTimeAssignToEstCalDay udt_std_int_small NULL,
	    CycleTimeAssignToEstHrs udt_std_int NULL,
	    CycleTimeEstToCloseBusDay udt_std_int_small NULL,
	    CycleTimeEstToCloseCalDay udt_std_int_small NULL,
	    CycleTimeEstToCloseHrs udt_std_int NULL,
	    CycleTimeInspectionToEstBusDay udt_std_int_small NULL,
	    CycleTimeInspectionToEstCalDay udt_std_int_small NULL,
	    CycleTimeInspectionToEstHrs udt_std_int NULL,
	    CycleTimeNewToCloseBusDay udt_std_int_small NULL,
	    CycleTimeNewToCloseCalDay udt_std_int_small NULL,
	    CycleTimeNewToCloseHrs udt_std_int NULL,
	    CycleTimeNewToEstBusDay udt_std_int_small NULL,
	    CycleTimeNewToEstCalDay udt_std_int_small NULL,
	    CycleTimeNewToEstHrs udt_std_int NULL,
	    CycleTimeNewToInspectionBusDay udt_std_int_small NULL,
	    CycleTimeNewToInspectionCalDay udt_std_int_small NULL,
	    CycleTimeNewToInspectionHrs udt_std_int NULL,
	    CycleTimeNewToRecloseBusDay udt_std_int_small NULL,
	    CycleTimeNewToRecloseCalDay udt_std_int_small NULL,
	    CycleTimeNewToRecloseHrs udt_std_int NULL,
	    CycleTimeRepairPromiseStartToEndBusDay udt_std_int_small NULL,
	    CycleTimeRepairPromiseStartToEndCalDay udt_std_int_small NULL,
	    CycleTimeRepairPromiseStartToEndHrs udt_std_int NULL,
	    CycleTimeRepairStartToEndBusDay udt_std_int_small NULL,
	    CycleTimeRepairStartToEndCalDay udt_std_int_small NULL,
	    CycleTimeRepairStartToEndHrs udt_std_int NULL,
	    DemoFlag udt_std_flag NULL,
	    EarlyBillAuditedAddtlLineItemAmt udt_std_money NULL,
	    EarlyBillAuditedGrossAmt udt_std_money NULL,
	    EarlyBillAuditedLineItemCorrectionAmt udt_std_money NULL,	    
	    EarlyBillAuditedMissingLineItemAmt udt_std_money NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    ExposureCD udt_std_cd NULL,
	    FeeRevenueAmt udt_std_money NULL,
	    FinalAuditedSuppAgreedFlag udt_std_flag NULL,
	    FinalAuditedSuppBettermentAmt udt_std_money NULL,
	    FinalAuditedSuppDeductibleAmt udt_std_money NULL,
	    FinalAuditedSuppGrossAmt udt_std_money NULL,
	    FinalAuditedSuppNetAmt udt_std_money NULL,
	    FinalAuditedSuppOtherAdjustmentAmt udt_std_money NULL,
	    FinalAuditedSuppwoBettAmt udt_std_money NULL,
	    FinalEstimateBettermentAmt udt_std_money NULL,
	    FinalEstimateDeductibleAmt udt_std_money NULL,
	    FinalEstimateGrossAmt udt_std_money NULL,
	    FinalEstimateNetAmt udt_std_money NULL,
	    FinalEstimateOtherAdjustmentAmt udt_std_money NULL,
	    FinalEstimatewoBettAmt udt_std_money NULL,
	    FinalSupplementBettermentAmt udt_std_money NULL,
	    FinalSupplementDeductibleAmt udt_std_money NULL,
	    FinalSupplementGrossAmt udt_std_money NULL,
	    FinalSupplementNetAmt udt_std_money NULL,
	    FinalSupplementOtherAdjustmentAmt udt_std_money NULL,
	    FinalSupplementwoBettAmt udt_std_money NULL,
	    IndemnityAmount udt_std_money NULL,
	    LaborRateBodyAmt udt_std_money NULL,
	    LaborRateFrameAmt udt_std_money NULL,
	    LaborRateMechAmt udt_std_money NULL,
	    LaborRateRefinishAmt udt_std_money NULL,
	    LaborRepairAmt udt_std_money NULL,
	    LaborReplaceAmt udt_std_money NULL,
	    LaborTotalAmt udt_std_money NULL,
	    LossDate udt_std_datetime NULL,
	    LynxID udt_std_id_big NOT NULL,
	    MaxEstSuppSequenceNumber udt_std_int_tiny NULL,
	    OriginalAuditedEstimateAgreedFlag udt_std_flag NULL,
	    OriginalAuditedEstimateBettermentAmt udt_std_money NULL,
	    OriginalAuditedEstimateDeductibleAmt udt_std_money NULL,
	    OriginalAuditedEstimateGrossAmt udt_std_money NULL,
	    OriginalAuditedEstimateNetAmt udt_std_money NULL,
	    OriginalAuditedEstimateOtherAdjustmentAmt udt_std_money NULL,
	    OriginalAuditedEstimatewoBettAmt udt_std_money NULL,
	    OriginalEstimateBettermentAmt udt_std_money NULL,
	    OriginalEstimateDeductibleAmt udt_std_money NULL,
	    OriginalEstimateGrossAmt udt_std_money NULL,
	    OriginalEstimateNetAmt udt_std_money NULL,
	    OriginalEstimateOtherAdjustmentAmt udt_std_money NULL,
	    OriginalEstimatewoBettAmt udt_std_money NULL,
	    PartsAFMKReplacedAmt udt_std_money NULL,
	    PartsLKQReplacedAmt udt_std_money NULL,
	    PartsOEMDiscountAmt udt_std_money NULL,
	    PartsOEMReplacedAmt udt_std_money NULL,
	    PartsRemanReplacedAmt udt_std_money NULL,
	    PartsTotalReplacedAmt udt_std_money NULL,
	    PhotosAttachedFlag udt_std_flag NULL,
	    PolicyNumber udt_cov_policy_number NULL,
	    PolicyDeductibleAmt udt_std_money NULL,
	    PolicyLimitAmt udt_std_money NULL,
	    PolicyRentalDeductibleAmt udt_std_money NULL,
	    PolicyRentalLimitAmt udt_std_money NULL,
	    PolicyRentalMaxDays udt_std_int_small NULL,
	    ProgramCD udt_std_cd NOT NULL,
	    ReinspectionCount udt_std_int_tiny NULL,
	    ReinspectionDeviationAmt udt_std_money NULL,
	    ReinspectionDeviationCount udt_std_int_tiny NULL,
	    RentalAmount udt_std_money NULL,
	    RentalCostPerDay udt_std_money NULL,
	    RentalDays udt_std_id_small NULL,
	    ServiceLossOfUseFlag udt_std_flag NOT NULL,
	    ServiceSubrogationFlag udt_std_flag NOT NULL,
	    ServiceTotalLossFlag udt_std_flag NOT NULL,
	    ServiceTotalTheftFlag udt_std_flag NOT NULL,
	    SupplementTotalAmt udt_std_money NULL,
        TotalConcessionLaborRate udt_std_money NULL,
        TotalConcessionLaborRateComment varchar(2500) NULL,
        TotalConcessionPartsUsage udt_std_money null,
        TotalConcessionPartsUsageComment varchar(2500) NULL,
        TotalConcessionTotalLoss udt_std_money NULL,
        TotalConcessionTotalLossComment varchar(2500) NULL,
        TotalConcessionUnrelatedDamage udt_std_money null,
        TotalConcessionUnrelatedDamageComment varchar(2500) NULL,
	    VehicleDriveableFlag udt_std_flag NOT NULL,
	    VehicleLicensePlateNumber udt_auto_plate_number NULL,
	    VehicleMake udt_auto_make NULL,
	    VehicleModel udt_auto_model NULL,
	    VehicleYear udt_dt_year NULL,
	    VehicleVIN udt_auto_vin NULL
	    )  ON ufg_dtwh
    
    SET IDENTITY_INSERT dbo.Tmp_utb_dtwh_fact_claim ON
    
    IF EXISTS(SELECT * FROM dbo.utb_dtwh_fact_claim)
	     EXEC('INSERT INTO dbo.Tmp_utb_dtwh_fact_claim 
	            (
	                FactID, AssignmentTypeClosingID, AssignmentTypeID, ClaimAspectID, ClaimLocationID, ClaimSourceID, CoverageTypeID, CustomerID, 
	                DispositionTypeID, LynxHandlerAnalystID, LynxHandlerOwnerID, LynxHandlerSupportID, RepairLocationID, ServiceChannelID,
	                TimeIDAssignDownload, TimeIDAssignSent, TimeIDCancelled, TimeIDClosed, TimeIDEstimate, TimeIDInspection, TimeIDNew, TimeIDReclosed, TimeIDRepairComplete, TimeIDRepairCompletePromise, TimeIDRepairStarted, TimeIDReopened, TimeIDVoided,
	                VehicleNumber, VehicleLicensePlateStateID, AuditedEstimateAgreedFlag, AuditedEstimateBettermentAmt, AuditedEstimateDeductibleAmt, AuditedEstimateGrossAmt, AuditedEstimateNetAmt, AuditedEstimateOtherAdjustmentAmt, AuditedEstimatewoBettAmt, CFProgramFlag, ClaimStatusCD, ClientClaimNumber,
	                CycleTimeAssignToEstBusDay, CycleTimeAssignToEstCalDay, CycleTimeAssignToEstHrs,
	                CycleTimeEstToCloseBusDay, CycleTimeEstToCloseCalDay, CycleTimeEstToCloseHrs,
	                CycleTimeInspectionToEstBusDay, CycleTimeInspectionToEstCalDay, CycleTimeInspectionToEstHrs,
	                CycleTimeNewToCloseBusDay, CycleTimeNewToCloseCalDay, CycleTimeNewToCloseHrs,
	                CycleTimeNewToEstBusDay, CycleTimeNewToEstCalDay, CycleTimeNewToEstHrs,
	                CycleTimeNewToInspectionBusDay, CycleTimeNewToInspectionCalDay, CycleTimeNewToInspectionHrs,
            	    CycleTimeNewToRecloseBusDay, CycleTimeNewToRecloseCalDay, CycleTimeNewToRecloseHrs,
	                CycleTimeRepairPromiseStartToEndBusDay, CycleTimeRepairPromiseStartToEndCalDay, CycleTimeRepairPromiseStartToEndHrs,
            	    CycleTimeRepairStartToEndBusDay, CycleTimeRepairStartToEndCalDay, CycleTimeRepairStartToEndHrs,
            	    DemoFlag, EarlyBillAuditedAddtlLineItemAmt, EarlyBillAuditedGrossAmt, EarlyBillAuditedLineItemCorrectionAmt, EarlyBillAuditedMissingLineItemAmt, EnabledFlag, ExposureCD, FeeRevenueAmt,
	                FinalAuditedSuppAgreedFlag, FinalAuditedSuppBettermentAmt, FinalAuditedSuppDeductibleAmt, FinalAuditedSuppGrossAmt, FinalAuditedSuppNetAmt, FinalAuditedSuppOtherAdjustmentAmt, FinalAuditedSuppwoBettAmt,
	                FinalEstimateBettermentAmt, FinalEstimateDeductibleAmt, FinalEstimateGrossAmt, FinalEstimateNetAmt, FinalEstimateOtherAdjustmentAmt, FinalEstimatewoBettAmt, 
	                FinalSupplementBettermentAmt, FinalSupplementDeductibleAmt, FinalSupplementGrossAmt, FinalSupplementNetAmt, FinalSupplementOtherAdjustmentAmt, FinalSupplementwoBettAmt,
	                IndemnityAmount, LaborRateBodyAmt, LaborRateFrameAmt, LaborRateMechAmt, LaborRateRefinishAmt, LaborRepairAmt, LaborReplaceAmt, LaborTotalAmt, LossDate, LynxID, MaxEstSuppSequenceNumber,
	                OriginalAuditedEstimateAgreedFlag, OriginalAuditedEstimateBettermentAmt, OriginalAuditedEstimateDeductibleAmt, OriginalAuditedEstimateGrossAmt, OriginalAuditedEstimateNetAmt, OriginalAuditedEstimateOtherAdjustmentAmt, OriginalAuditedEstimatewoBettAmt,
	                OriginalEstimateBettermentAmt, OriginalEstimateDeductibleAmt, OriginalEstimateGrossAmt, OriginalEstimateNetAmt, OriginalEstimateOtherAdjustmentAmt, OriginalEstimatewoBettAmt, 
	                PartsAFMKReplacedAmt, PartsLKQReplacedAmt, PartsOEMDiscountAmt, PartsOEMReplacedAmt, PartsRemanReplacedAmt, PartsTotalReplacedAmt, 
	                PhotosAttachedFlag, PolicyNumber, PolicyDeductibleAmt, PolicyLimitAmt, PolicyRentalDeductibleAmt, PolicyRentalLimitAmt, PolicyRentalMaxDays, ProgramCD, 
	                ReinspectionCount, ReinspectionDeviationAmt, ReinspectionDeviationCount, RentalAmount, RentalCostPerDay, RentalDays, 
	                ServiceLossOfUseFlag, ServiceSubrogationFlag, ServiceTotalLossFlag, ServiceTotalTheftFlag, SupplementTotalAmt, 
	                TotalConcessionLaborRate, TotalConcessionLaborRateComment, TotalConcessionPartsUsage, TotalConcessionPartsUsageComment, TotalConcessionTotalLoss, TotalConcessionTotalLossComment, TotalConcessionUnrelatedDamage, TotalConcessionUnrelatedDamageComment, 
	                VehicleDriveableFlag, VehicleLicensePlateNumber, VehicleMake, VehicleModel, VehicleYear, VehicleVIN
	            )
		    SELECT  FactID, AssignmentTypeClosingID, AssignmentTypeID, ClaimAspectID, ClaimLocationID, ClaimSourceID, CoverageTypeID, CustomerID, 
	                DispositionTypeID, LynxHandlerAnalystID, LynxHandlerOwnerID, LynxHandlerSupportID, RepairLocationID, ServiceChannelID,
	                TimeIDAssignDownload, TimeIDAssignSent, TimeIDCancelled, TimeIDClosed, TimeIDEstimate, NULL, TimeIDNew, TimeIDReclosed, TimeIDRepairComplete, TimeIDRepairCompletePromise, TimeIDRepairStarted, TimeIDReopened, TimeIDVoided,
	                VehicleNumber, VehicleLicensePlateStateID, AuditedEstimateAgreedFlag, AuditedEstimateBettermentAmt, AuditedEstimateDeductibleAmt, AuditedEstimateGrossAmt, AuditedEstimateNetAmt, AuditedEstimateOtherAdjustmentAmt, AuditedEstimatewoBettAmt, CFProgramFlag, ClaimStatusCD, ClientClaimNumber,
	                CycleTimeAssignToEstBusDay, CycleTimeAssignToEstCalDay, CycleTimeAssignToEstHrs,
	                CycleTimeEstToCloseBusDay, CycleTimeEstToCloseCalDay, CycleTimeEstToCloseHrs,
	                NULL, NULL, NULL,
	                CycleTimeNewToCloseBusDay, CycleTimeNewToCloseCalDay, CycleTimeNewToCloseHrs,
	                CycleTimeNewToEstBusDay, CycleTimeNewToEstCalDay, CycleTimeNewToEstHrs,
	                NULL, NULL, NULL,
            	    CycleTimeNewToRecloseBusDay, CycleTimeNewToRecloseCalDay, CycleTimeNewToRecloseHrs,
	                CycleTimeRepairPromiseStartToEndBusDay, CycleTimeRepairPromiseStartToEndCalDay, CycleTimeRepairPromiseStartToEndHrs,
            	    CycleTimeRepairStartToEndBusDay, CycleTimeRepairStartToEndCalDay, CycleTimeRepairStartToEndHrs,
            	    DemoFlag, NULL, NULL, NULL, NULL, EnabledFlag, ExposureCD, FeeRevenueAmt,
	                FinalAuditedSuppAgreedFlag, FinalAuditedSuppBettermentAmt, FinalAuditedSuppDeductibleAmt, FinalAuditedSuppGrossAmt, FinalAuditedSuppNetAmt, FinalAuditedSuppOtherAdjustmentAmt, FinalAuditedSuppwoBettAmt,
	                FinalEstimateBettermentAmt, FinalEstimateDeductibleAmt, FinalEstimateGrossAmt, FinalEstimateNetAmt, FinalEstimateOtherAdjustmentAmt, FinalEstimatewoBettAmt, 
	                FinalSupplementBettermentAmt, FinalSupplementDeductibleAmt, FinalSupplementGrossAmt, FinalSupplementNetAmt, FinalSupplementOtherAdjustmentAmt, FinalSupplementwoBettAmt,
	                IndemnityAmount, LaborRateBodyAmt, LaborRateFrameAmt, LaborRateMechAmt, LaborRateRefinishAmt, LaborRepairAmt, LaborReplaceAmt, LaborTotalAmt, LossDate, LynxID, MaxEstSuppSequenceNumber,
	                OriginalAuditedEstimateAgreedFlag, OriginalAuditedEstimateBettermentAmt, OriginalAuditedEstimateDeductibleAmt, OriginalAuditedEstimateGrossAmt, OriginalAuditedEstimateNetAmt, OriginalAuditedEstimateOtherAdjustmentAmt, OriginalAuditedEstimatewoBettAmt,
	                OriginalEstimateBettermentAmt, OriginalEstimateDeductibleAmt, OriginalEstimateGrossAmt, OriginalEstimateNetAmt, OriginalEstimateOtherAdjustmentAmt, OriginalEstimatewoBettAmt, 
	                PartsAFMKReplacedAmt, PartsLKQReplacedAmt, PartsOEMDiscountAmt, PartsOEMReplacedAmt, PartsRemanReplacedAmt, PartsTotalReplacedAmt, 
	                NULL, PolicyNumber, PolicyDeductibleAmt, PolicyLimitAmt, PolicyRentalDeductibleAmt, PolicyRentalLimitAmt, PolicyRentalMaxDays, ProgramCD, 
	                ReinspectionCount, ReinspectionDeviationAmt, ReinspectionDeviationCount, RentalAmount, RentalCostPerDay, RentalDays, 
	                ServiceLossOfUseFlag, ServiceSubrogationFlag, ServiceTotalLossFlag, ServiceTotalTheftFlag, SupplementTotalAmt, 
	                TotalConcessionLaborRate, TotalConcessionLaborRateComment, TotalConcessionPartsUsage, TotalConcessionPartsUsageComment, TotalConcessionTotalLoss, TotalConcessionTotalLossComment, TotalConcessionUnrelatedDamage, TotalConcessionUnrelatedDamageComment, 
	                VehicleDriveableFlag, VehicleLicensePlateNumber, VehicleMake, VehicleModel, VehicleYear, VehicleVIN 
		      FROM dbo.utb_dtwh_fact_claim WITH (HOLDLOCK TABLOCKX)')
    
    SET IDENTITY_INSERT dbo.Tmp_utb_dtwh_fact_claim OFF
    
    DROP TABLE dbo.utb_dtwh_fact_claim
    
    EXECUTE sp_rename N'dbo.Tmp_utb_dtwh_fact_claim', N'utb_dtwh_fact_claim', 'OBJECT' 
    
    CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_claimaspectid_assignmenttypeid ON dbo.utb_dtwh_fact_claim
	    (
	    ClaimAspectID,
	    AssignmentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_dtwh_index
    
    ALTER TABLE dbo.utb_dtwh_fact_claim ADD CONSTRAINT
	    uck_dtwh_fact_claim_claimstatuscd CHECK (([ClaimStatusCD] = 'CANC' or [ClaimStatusCD] = 'CLSD' or [ClaimStatusCD] = 'OPEN' or [ClaimStatusCD] = 'VOID'))
    
    ALTER TABLE dbo.utb_dtwh_fact_claim ADD CONSTRAINT
	    uck_dtwh_fact_claim_exposurecd CHECK (([ExposureCD] = '1' or [ExposureCD] = '3'))
    
    ALTER TABLE dbo.utb_dtwh_fact_claim ADD CONSTRAINT
	    uck_dtwh_fact_claim_programcd CHECK (([ProgramCD] = 'CEI' or [ProgramCD] = 'LS' or [ProgramCD] = 'NON'))
    
    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

