--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_client_required_document_type' --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_client_required_document_type 
    (
        InsuranceCompanyID					udt_std_int_small               NOT NULL,
        ServiceChannelCD					udt_std_cd                      NOT NULL,
        DocumentTypeID						udt_std_int_tiny                NOT NULL,
        ConditionValue						udt_std_desc_mid				NOT NULL,
        EnabledFlag							udt_enabled_flag                NOT NULL,
	    MandatoryFlag						udt_std_flag                    NOT NULL,
	    WarrantyFlag						udt_std_flag					NOT NULL,
        SysLastUserID						udt_std_id                      NOT NULL,
        SysLastUpdatedDate					udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_client_required_document_type
        PRIMARY KEY CLUSTERED (InsuranceCompanyID, ServiceChannelCD, DocumentTypeID) WITH FILLFACTOR=100
                             ON ufg_reference
    )
    ON ufg_reference
     
    ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
        uck_client_required_document_type_servicechannelcd
        CHECK  ((ServiceChannelCD = 'DA' or ServiceChannelCD = 'DR' or ServiceChannelCD = 'GL' or ServiceChannelCD = 'IA' or ServiceChannelCD = 'ME' or ServiceChannelCD = 'PS' or ServiceChannelCD = 'SA'))

    CREATE NONCLUSTERED INDEX uix_ie_client_required_document_type_insurancecompanyid ON dbo.utb_client_required_document_type
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_reference_index
    
    CREATE NONCLUSTERED INDEX uix_ie_client_required_document_type_documenttypeid ON dbo.utb_client_required_document_type
	    (
	    DocumentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_reference_index
    
    ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
        ufk_client_required_document_type_insurancecompanyid_in_insurance FOREIGN KEY
        (
        InsuranceCompanyID
        ) REFERENCES dbo.utb_insurance
        (
        InsuranceCompanyID
        )
        
    ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
        ufk_client_required_document_type_documenttypeid_in_document_type FOREIGN KEY
        (
        documenttypeid
        ) REFERENCES dbo.utb_document_type
        (
        documenttypeid
        )
        
    ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
        ufk_client_required_document_type_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

