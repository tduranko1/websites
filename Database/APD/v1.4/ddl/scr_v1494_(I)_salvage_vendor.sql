--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_salvage_vendor' --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_salvage_vendor 
    (
        SalvageVendorID					    udt_std_id_big IDENTITY         NOT NULL,
        ParentSalvageVendorID				udt_std_id_big                      NULL,
        Address1						    udt_addr_line_1  				    NULL,
        Address2							udt_addr_line_2                     NULL,
        AddressCity  						udt_addr_city                       NULL,
        AddressState						udt_addr_state					    NULL,
        AddressZip  						udt_addr_zip_code				    NULL,
        ContactName 						udt_std_name					    NULL,
        EmailAddress						udt_web_email					    NULL,
        EnabledFlag 						udt_enabled_flag					NOT NULL,
        FaxAreaCode  						udt_ph_area_code				    NULL,
        FaxExchangeNumber					udt_ph_exchange_number			    NULL,
        FaxExtensionNumber					udt_ph_extension_number			    NULL,
        FaxUnitNumber						udt_ph_unit_number				    NULL,
        MailingAddress1						udt_addr_line_1  				    NULL,
        MailingAddress2						udt_addr_line_2                     NULL,
        MailingAddressCity  				udt_addr_city                       NULL,
        MailingAddressState					udt_addr_state					    NULL,
        MailingAddressZip  					udt_addr_zip_code				    NULL,
        Name                                udt_std_name                    NOT NULL,
        OfficeName                          udt_std_name                    NOT NULL,
        PhoneAreaCode  						udt_ph_area_code				    NULL,
        PhoneExchangeNumber					udt_ph_exchange_number			    NULL,
        PhoneExtensionNumber				udt_ph_extension_number				NULL,
        PhoneUnitNumber						udt_ph_unit_number				    NULL,
        WebSiteAddress                      udt_web_address                     NULL,                             
        SysLastUserID						udt_std_id                      NOT NULL,
        SysLastUpdatedDate					udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_salvage_vendor
        PRIMARY KEY CLUSTERED (SalvageVendorID) WITH FILLFACTOR=85
                             ON ufg_shop
    )
    ON ufg_shop
     
    CREATE NONCLUSTERED INDEX uix_ie_salvage_vendor_parentsalvagevendorid ON dbo.utb_salvage_vendor
        (
        ParentSalvageVendorID
        ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    ALTER TABLE dbo.utb_salvage_vendor ADD CONSTRAINT
        ufk_salvage_vendor_parentsalvagevendorid_in_salvage_vendor FOREIGN KEY
        (
        ParentSalvageVendorID
        ) REFERENCES dbo.utb_salvage_vendor
        (
        SalvageVendorID
        )
        
    ALTER TABLE dbo.utb_salvage_vendor ADD CONSTRAINT
        ufk_salvage_vendor_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

