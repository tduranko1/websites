--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_aspect_service_channel' --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_createduserid_in_user

    ALTER TABLE dbo.utb_claim_aspect_service_channel
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_claimaspectid_in_claim_aspect

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    ClaimAspectID udt_std_id_big NOT NULL,
	    CreatedUserID udt_std_id NOT NULL,
	    LienHolderID udt_std_id_big NULL,
	    SalvageVendorID udt_std_id_big NULL,
	    AdvanceAmount udt_std_money NULL,
	    AppraiserInvoiceDate udt_std_datetime NULL,
	    AppraiserPaidDate udt_std_datetime NULL,
	    CashOutDate udt_std_datetime NULL,
	    ClientInvoiceDate udt_std_datetime NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    DispositionTypeCD udt_std_cd NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    InspectionDate udt_std_datetime NULL,
	    LetterOfGuaranteeAmount udt_std_money NULL,
	    LeinHolderAccountNumber udt_std_desc_mid NULL,
	    OriginalCompleteDate udt_std_datetime NULL,
	    OriginalEstimateDate udt_std_datetime NULL,
	    PayoffAmount udt_std_money NULL,
	    PayoffAmountConfirmedDate udt_std_datetime NULL,
	    PayoffAmountConfirmedFlag udt_std_flag NOT NULL,
	    PayoffExpirationDate udt_std_datetime NULL,
	    PrimaryFlag udt_std_flag NOT NULL,
	    RepairLocationCity udt_addr_city NULL,
	    RepairLocationCounty udt_addr_county NULL,
	    RepairLocationState udt_addr_state NULL,
	    SalvageControlNumber udt_std_desc_mid NULL,
	    ServiceChannelCD udt_std_cd NULL,
	    SettlementAmount udt_std_money NULL,
	    SettlementDate udt_std_datetime NULL,
	    WorkEndConfirmFlag udt_std_flag NOT NULL,
	    WorkEndDate udt_std_datetime NULL,
	    WorkEndDateOriginal udt_std_datetime NULL,
	    WorkStartConfirmFlag udt_std_flag NOT NULL,
	    WorkStartDate udt_std_datetime NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_service_channel ON

    IF EXISTS(SELECT * FROM dbo.utb_claim_aspect_service_channel)
	     EXEC('INSERT INTO dbo.Tmp_utb_claim_aspect_service_channel (ClaimAspectServiceChannelID, ClaimAspectID, CreatedUserID, AppraiserInvoiceDate, AppraiserPaidDate, CashOutDate, ClientInvoiceDate, CreatedDate, DispositionTypeCD, EnabledFlag, InspectionDate, OriginalCompleteDate, OriginalEstimateDate, PrimaryFlag, RepairLocationCity, RepairLocationCounty, RepairLocationState, ServiceChannelCD, WorkEndConfirmFlag, WorkEndDate, WorkEndDateOriginal, WorkStartConfirmFlag, WorkStartDate, SysLastUserID, SysLastUpdatedDate)
		    SELECT ClaimAspectServiceChannelID, ClaimAspectID, CreatedUserID, AppraiserInvoiceDate, AppraiserPaidDate, CashOutDate, ClientInvoiceDate, CreatedDate, DispositionTypeCD, EnabledFlag, InspectionDate, OriginalCompleteDate, OriginalEstimateDate, PrimaryFlag, RepairLocationCity, RepairLocationCounty, RepairLocationState, ServiceChannelCD, WorkEndConfirmFlag, WorkEndDate, WorkEndDateOriginal, WorkStartConfirmFlag, WorkStartDate, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_claim_aspect_service_channel WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_service_channel OFF

    ALTER TABLE dbo.utb_warranty_assignment
	    DROP CONSTRAINT ufk_warranty_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_total_loss_metrics
	    DROP CONSTRAINT ufk_total_loss_metrics_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_claim_aspect_service_channel_deliverable
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_deliverable_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_claim_aspect_service_channel_concession
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_concession_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_checklist
	    DROP CONSTRAINT ufk_checklist_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_assignment
	    DROP CONSTRAINT ufk_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_service_channel_work_history
	    DROP CONSTRAINT ufk_service_channel_work_history_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_claim_aspect_service_channel_coverage
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_coverage_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_claimaspectservicechannelid_in_claim_aspect_service_channel

    ALTER TABLE dbo.utb_claim_aspect_service_channel_document
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_document_claimaspectservicechannelid_in_claim_aspect_service_channel

    DROP TABLE dbo.utb_claim_aspect_service_channel

    EXECUTE sp_rename N'dbo.Tmp_utb_claim_aspect_service_channel', N'utb_claim_aspect_service_channel', 'OBJECT' 

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    upk_claim_aspect_service_channel PRIMARY KEY CLUSTERED 
	    (
	    ClaimAspectServiceChannelID
	    ) WITH FILLFACTOR = 90 ON ufg_claim


    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_claimaspectid ON dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_createduserid ON dbo.utb_claim_aspect_service_channel
	    (
	    CreatedUserID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_lienholderid ON dbo.utb_claim_aspect_service_channel
	    (
	    LienHolderID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_salvagevendorrid ON dbo.utb_claim_aspect_service_channel
	    (
	    SalvageVendorID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_dispositiontypecd ON dbo.utb_claim_aspect_service_channel
	    (
	    DispositionTypeCD
	    ) ON ufg_claim_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_claimaspectservicechannelid_claimaspectid_servicechannelcd ON dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID,
	    ClaimAspectID,
	    ServiceChannelCD
	    ) ON ufg_claim

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_x ON dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectID,
	    EnabledFlag,
	    PrimaryFlag,
	    ClaimAspectServiceChannelID,
	    ServiceChannelCD
	    ) ON ufg_claim

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    uck_claim_aspect_service_channel_dispositiontypecd CHECK (([DispositionTypeCD] = 'CA' or [DispositionTypeCD] = 'CO' or [DispositionTypeCD] = 'IC' or [DispositionTypeCD] = 'IME' or [DispositionTypeCD] = 'IPD' or [DispositionTypeCD] = 'NA' or [DispositionTypeCD] = 'NOCV' or [DispositionTypeCD] = 'PCO' or [DispositionTypeCD] = 'RC' or [DispositionTypeCD] = 'RNC' or [DispositionTypeCD] = 'ST' or [DispositionTypeCD] = 'TL' or [DispositionTypeCD] = 'VD'))

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    uck_claim_aspect_service_channel_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA'))

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_createduserid_in_user FOREIGN KEY
	    (
	    CreatedUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_lienholderid_in_lien_holder FOREIGN KEY
	    (
	    LienHolderID
	    ) REFERENCES dbo.utb_lien_holder
	    (
	    LienHolderID
	    )

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_salvagevendorid_in_salvage_vendor FOREIGN KEY
	    (
	    SalvageVendorID
	    ) REFERENCES dbo.utb_salvage_vendor
	    (
	    SalvageVendorID
	    )

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel_document ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_document_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel_coverage ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_coverage_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_service_channel_work_history ADD CONSTRAINT
	    ufk_service_channel_work_history_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	    ufk_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel_concession ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_concession_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_aspect_service_channel_deliverable ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_deliverable_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_total_loss_metrics ADD CONSTRAINT
	    ufk_total_loss_metrics_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_warranty_assignment ADD CONSTRAINT
	    ufk_warranty_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

