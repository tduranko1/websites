--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_shop_location_program_delta'    --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_shop_location_program_delta
	    (
	    ShopLocationID udt_std_id_big NOT NULL,
	    InsuranceCompanyID udt_std_int_small NOT NULL,
	    ActionCD udt_std_cd NOT NULL,
	    DeltaDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_load
    
    IF EXISTS(SELECT * FROM dbo.utb_shop_location_program_delta)
	     EXEC('INSERT INTO dbo.Tmp_utb_shop_location_program_delta (ShopLocationID, InsuranceCompanyID, ActionCD)
		    SELECT ShopLocationID, InsuranceCompanyID, ''A'' FROM dbo.utb_shop_location_program_delta WITH (HOLDLOCK TABLOCKX)')
    
    DROP TABLE dbo.utb_shop_location_program_delta
    
    EXECUTE sp_rename N'dbo.Tmp_utb_shop_location_program_delta', N'utb_shop_location_program_delta', 'OBJECT' 
    
    ALTER TABLE dbo.utb_shop_location_program_delta ADD CONSTRAINT
	    upk_shop_location_program_delta PRIMARY KEY CLUSTERED 
	    (
	    ShopLocationID,
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_load

    
    ALTER TABLE dbo.utb_shop_location_program_delta ADD CONSTRAINT
	    ufk_shop_location_program_delta_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )
        
    ALTER TABLE dbo.utb_shop_location_program_delta ADD CONSTRAINT
	    ufk_shop_location_program_delta_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
        
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_program_delta_shoplocationid ON dbo.utb_shop_location_program_delta
	    (
	    ShopLocationID
	    ) WITH FILLFACTOR = 90 ON ufg_load_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_program_delta_insurancecompanyid ON dbo.utb_shop_location_program_delta
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_load_index
    
    ALTER TABLE dbo.utb_shop_location_program_delta 
        ADD CONSTRAINT uck_shop_location_program_delta_actioncd
        CHECK ([ActionCD] = 'A' or [ActionCD] = 'D' or [ActionCD] = 'R' or [ActionCD] = 'S')

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

