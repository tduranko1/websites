--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_lien_holder' --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_lien_holder 
    (
        LienHolderID					    udt_std_id_big IDENTITY(510000,1) NOT NULL,
        BillingID   						udt_std_id_big                  NOT NULL,
        ParentLienHolderID					udt_std_id_big                      NULL,
        Address1						    udt_addr_line_1  				    NULL,
        Address2							udt_addr_line_2                     NULL,
        AddressCity  						udt_addr_city                       NULL,
        AddressState						udt_addr_state					    NULL,
        AddressZip  						udt_addr_zip_code				    NULL,
        BusinessTypeCD                      udt_std_cd                          NULL,
        ContactName 						udt_std_name					    NULL,
        EmailAddress						udt_web_email					    NULL,
        EnabledFlag 						udt_enabled_flag					NOT NULL,
        FaxAreaCode  						udt_ph_area_code				    NULL,
        FaxExchangeNumber					udt_ph_exchange_number			    NULL,
        FaxExtensionNumber					udt_ph_extension_number			    NULL,
        FaxUnitNumber						udt_ph_unit_number				    NULL,
        FedTaxId                            udt_fed_tax_id                      NULL,
        MailingAddress1						udt_addr_line_1  				    NULL,
        MailingAddress2						udt_addr_line_2                     NULL,
        MailingAddressCity  				udt_addr_city                       NULL,
        MailingAddressState					udt_addr_state					    NULL,
        MailingAddressZip  					udt_addr_zip_code				    NULL,
        Name                                udt_std_name                    NOT NULL,
        OfficeName                          udt_std_name                    NOT NULL,
        PhoneAreaCode  						udt_ph_area_code				    NULL,
        PhoneExchangeNumber					udt_ph_exchange_number			    NULL,
        PhoneExtensionNumber				udt_ph_extension_number				NULL,
        PhoneUnitNumber						udt_ph_unit_number				    NULL,
        WebSiteAddress                      udt_web_address                     NULL,                             
        SysLastUserID						udt_std_id                      NOT NULL,
        SysLastUpdatedDate					udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_lien_holder
        PRIMARY KEY CLUSTERED (LienHolderID) WITH FILLFACTOR=85
                             ON ufg_claim
    )
    ON ufg_claim
     
    CREATE NONCLUSTERED INDEX uix_ie_lien_holder_billingid ON dbo.utb_lien_holder
        (
        BillingID
        ) WITH FILLFACTOR = 90 ON ufg_claim_index
    
    CREATE NONCLUSTERED INDEX uix_ie_lien_holder_parentlienholderid ON dbo.utb_lien_holder
        (
        ParentLienHolderID
        ) WITH FILLFACTOR = 90 ON ufg_claim_index
    
    ALTER TABLE dbo.utb_lien_holder ADD CONSTRAINT
        ufk_lien_holder_parentlienholderid_in_lien_holder FOREIGN KEY
        (
        ParentLienHolderID
        ) REFERENCES dbo.utb_lien_holder
        (
        LienHolderID
        )
        
    ALTER TABLE dbo.utb_lien_holder ADD CONSTRAINT
        ufk_lien_holder_billingid_in_billing FOREIGN KEY
        (
        billingid
        ) REFERENCES dbo.utb_billing
        (
        billingid
        )
        
    ALTER TABLE dbo.utb_lien_holder ADD CONSTRAINT
        ufk_lien_holder_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    ALTER TABLE dbo.utb_lien_holder ADD CONSTRAINT
        uck_lien_holder_businesstypecd CHECK  (([BusinessTypeCD] = 'S' or [BusinessTypeCD] = 'P' or [BusinessTypeCD] = 'C'))
    
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

