--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_client_fee'                   --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             AdminFeeFlag                                          !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_fee
	    DROP CONSTRAINT ufk_client_fee_insurancecompanyid_in_insurance
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_fee
	    DROP CONSTRAINT ufk_client_fee_claimaspecttypeid_in_claim_aspect_type
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_fee
	    DROP CONSTRAINT ufk_client_fee_syslastuserid_in_user
    
    COMMIT
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_client_fee
	    (
	    ClientFeeID udt_std_id NOT NULL IDENTITY (1, 1),
	    ClaimAspectTypeID udt_std_int_tiny NOT NULL,
	    InsuranceCompanyID udt_std_int_small NULL,
	    AdminFeeFlag udt_std_flag NOT NULL,
	    AppliesToCD udt_std_cd NOT NULL,
	    CategoryCD udt_std_cd NOT NULL,
	    ClientCode udt_std_desc_short NULL,
	    Description udt_std_name NOT NULL,
	    DisplayOrder udt_std_int NULL,
	    EffectiveEndDate udt_std_datetime NULL,
	    EffectiveStartDate udt_std_datetime NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    FeeAmount udt_std_money NULL,
	    FeeInstructions udt_std_desc_mid NULL,
	    HideFromInvoiceFlag udt_std_flag NOT NULL,
	    ItemizeFlag udt_std_flag NOT NULL,
	    InvoiceDescription udt_std_name NOT NULL,
	    PaymentRequiredFlag udt_std_flag NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow
    
    SET IDENTITY_INSERT dbo.Tmp_utb_client_fee ON
    
    IF EXISTS(SELECT * FROM dbo.utb_client_fee)
	     EXEC('INSERT INTO dbo.Tmp_utb_client_fee (ClientFeeID, ClaimAspectTypeID, InsuranceCompanyID, AppliesToCD, CategoryCD, ClientCode, Description, DisplayOrder, EffectiveEndDate, EffectiveStartDate, EnabledFlag, FeeAmount, FeeInstructions, HideFromInvoiceFlag, ItemizeFlag, InvoiceDescription, PaymentRequiredFlag, SysLastUserID, SysLastUpdatedDate)
		    SELECT ClientFeeID, ClaimAspectTypeID, InsuranceCompanyID, AppliesToCD, CategoryCD, ClientCode, Description, DisplayOrder, EffectiveEndDate, EffectiveStartDate, EnabledFlag, FeeAmount, FeeInstructions, HideFromInvoiceFlag, ItemizeFlag, InvoiceDescription, PaymentRequiredFlag, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_client_fee WITH (HOLDLOCK TABLOCKX)')
    
    SET IDENTITY_INSERT dbo.Tmp_utb_client_fee OFF
    
    ALTER TABLE dbo.utb_client_fee_definition
	    DROP CONSTRAINT ufk_client_fee_definition_clientfeeid_in_client_fee
    
    DROP TABLE dbo.utb_client_fee
    
    EXECUTE sp_rename N'dbo.Tmp_utb_client_fee', N'utb_client_fee', 'OBJECT' 
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    upk_client_fee PRIMARY KEY CLUSTERED 
	    (
	    ClientFeeID
	    ) ON ufg_workflow

    
    CREATE NONCLUSTERED INDEX uix_ie_client_fee_claimaspecttypeid ON dbo.utb_client_fee
	    (
	    ClaimAspectTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_client_fee_insurancecompanyid ON dbo.utb_client_fee
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    uck_client_fee_appliestocd CHECK (([AppliesToCD] = 'B' or [AppliesToCD] = 'C'))
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    uck_client_fee_categorycd CHECK (([CategoryCD] = 'A' or [CategoryCD] = 'H'))
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    ufk_client_fee_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    ufk_client_fee_claimaspecttypeid_in_claim_aspect_type FOREIGN KEY
	    (
	    ClaimAspectTypeID
	    ) REFERENCES dbo.utb_claim_aspect_type
	    (
	    ClaimAspectTypeID
	    )
    
    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    ufk_client_fee_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_fee_definition ADD CONSTRAINT
	    ufk_client_fee_definition_clientfeeid_in_client_fee FOREIGN KEY
	    (
	    ClientFeeID
	    ) REFERENCES dbo.utb_client_fee
	    (
	    ClientFeeID
	    )
    
    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

