--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_aspect_status'          --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    -- Create new table schema in holding area
    CREATE TABLE dbo.Tmp_utb_claim_aspect_status
	    (
	        ClaimAspectStatusID udt_std_id_big IDENTITY(1,1) NOT NULL,
	        ClaimAspectID udt_std_id_big NOT NULL,
	        StatusID udt_std_int_small NULL,
	        AssignmentSequenceNumber udt_std_int_small NULL,
	        ServiceChannelCD udt_std_cd NULL,
	        StatusStartDate udt_std_datetime NULL,
	        StatusTypeCD udt_std_cd NULL,
	        SysLastUserID udt_std_id NOT NULL,
	        SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_status ON
    	    
    IF EXISTS(SELECT * FROM dbo.utb_claim_aspect_status)
	     EXEC('INSERT INTO dbo.Tmp_utb_claim_aspect_status (ClaimAspectStatusID, ClaimAspectID, StatusID, AssignmentSequenceNumber, ServiceChannelCD, StatusStartDate, StatusTypeCD, SysLastUserID, SysLastUpdatedDate) 
		    SELECT ClaimAspectStatusID, ClaimAspectID, StatusID, CASE WHEN StatusTypeCD IN (''ELC'', ''FAX'') THEN 1 ELSE NULL END, ServiceChannelCD, StatusStartDate, StatusTypeCD, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_claim_aspect_status WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_status OFF

    IF (SELECT Count(*) FROM dbo.Tmp_utb_claim_aspect_status) = (SELECT Count(*) FROM dbo.utb_claim_aspect_status)
    BEGIN
        -- Remove FKs
        -- No FKs to remove    


        -- Drop current table and replace with new table schema
        DROP TABLE dbo.utb_claim_aspect_status

        EXECUTE sp_rename N'dbo.Tmp_utb_claim_aspect_status', N'utb_claim_aspect_status', 'OBJECT' 

        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        upk_claim_aspect_status PRIMARY KEY CLUSTERED 
	        (
	        ClaimAspectStatusID
	        ) WITH FILLFACTOR = 90 ON ufg_claim


        -- Recreate indexes
        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_claimaspectid ON dbo.utb_claim_aspect_status
	        (
	        ClaimAspectID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_claimaspectid_statustypecd_servicechannelcd_statusid ON dbo.utb_claim_aspect_status
	        (
	        ClaimAspectID,
	        StatusTypeCD,
	        ServiceChannelCD,
	        StatusID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_servicechannelcd ON dbo.utb_claim_aspect_status
	        (
	        ServiceChannelCD
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_statusid ON dbo.utb_claim_aspect_status
	        (
	        StatusID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_statusid_claimaspectid_servicechannelcd_statustypecd ON dbo.utb_claim_aspect_status
	        (
	        StatusID,
	        ClaimAspectID,
	        ServiceChannelCD,
	        StatusTypeCD
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_statustypecd_claimaspectid_statusid ON dbo.utb_claim_aspect_status
	        (
	        StatusTypeCD,
	        ClaimAspectID,
	        StatusID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index

        CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_status_x ON dbo.utb_claim_aspect_status
	        (
	        StatusTypeCD,
	        ServiceChannelCD,
	        ClaimAspectID,
	        StatusID
	        ) WITH FILLFACTOR = 90 ON ufg_claim_index


        -- Add check costraints and reference codes
        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        uck_claim_aspect_status_servicechannelcd CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        uck_claim_aspect_status_statustypecd CHECK  (([StatusTypeCD] = 'ELC' or [StatusTypeCD] = 'FAX' or [StatusTypeCD] = 'SC'))

        EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral' , 
            'user', dbo, 'table','utb_claim_aspect_status', 'CONSTRAINT','uck_claim_aspect_status_servicechannelcd'

        EXEC sp_addextendedproperty 'Codes', 'ELC|Electronic|FAX|Fax|SC|Service Channel' , 
            'user', dbo, 'table','utb_claim_aspect_status', 'CONSTRAINT','uck_claim_aspect_status_statustypecd'


        -- Reimplement FKs
        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        ufk_claim_aspect_status_claimaspectid_in_claim_aspect FOREIGN KEY
	        (
	        ClaimAspectID
	        ) REFERENCES dbo.utb_claim_aspect
	        (
	        ClaimAspectID
	        )

        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        ufk_claim_aspect_status_statusid_in_status FOREIGN KEY
	        (
	        StatusID
	        ) REFERENCES dbo.utb_status
	        (
	        StatusID
	        )

        ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	        ufk_claim_aspect_status_syslastuserid_in_user FOREIGN KEY
	        (
	        SysLastUserID
	        ) REFERENCES dbo.utb_user
	        (
	        UserID
	        )

    END
    ELSE
    BEGIN
        RAISERROR('ERROR COPYING TABLE, ORIGINAL TABLE NOT REMOVED', 16,1)
    END
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

