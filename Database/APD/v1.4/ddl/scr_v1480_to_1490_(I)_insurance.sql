ALTER TABLE dbo.utb_insurance DROP CONSTRAINT uck_insurance_returndocroutingcd
GO
ALTER TABLE dbo.utb_insurance 
    ADD CONSTRAINT uck_insurance_returndocroutingcd
    CHECK ([ReturnDocRoutingCD] = 'CCAV' or [ReturnDocRoutingCD] = 'EML' or [ReturnDocRoutingCD] = 'FAX' or [ReturnDocRoutingCD] = 'FTP' or [ReturnDocRoutingCD] = 'SG')
GO
