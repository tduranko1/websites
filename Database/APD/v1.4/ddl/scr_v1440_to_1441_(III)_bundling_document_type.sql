    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT
        uck_bundling_document_type_directionalcd CHECK
        (DirectionalCD = 'I' or DirectionalCD = 'O')

    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT 
        uck_bundling_document_type_estimatetypecd CHECK
        (EstimateTypeCD = 'A' or EstimateTypeCD = 'O')

