--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_service_channel_work_history'  --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_service_channel_work_history(
        HistoryID             udt_std_id_big               IDENTITY(1,1),
        ClaimAspectServiceChannelID         udt_std_id_big               NOT NULL,
        CreatedUserID         udt_std_id                   NOT NULL,
        ReasonID              udt_std_int                  NOT NULL,
        ChangeComment         udt_std_desc_mid             NULL,
        ChangeDate            udt_std_datetime             NOT NULL,
        WorkEndNewDate      udt_std_datetime             NULL,
        WorkEndOldDate      udt_std_datetime             NULL,
        WorkStartNewDate    udt_std_datetime             NULL,
        WorkStartOldDate    udt_std_datetime             NULL,
        SysLastUserID         udt_std_id                   NOT NULL,
        SysLastUpdatedDate    udt_sys_last_updated_date    NOT NULL,
        CONSTRAINT upk_service_channel_work_history PRIMARY KEY CLUSTERED (HistoryID)
        WITH FILLFACTOR = 90
        ON ufg_claim, 
        CONSTRAINT ufk_service_channel_work_history_reasonid_in_service_channel_schedule_change_reason FOREIGN KEY (ReasonID)
        REFERENCES dbo.utb_service_channel_schedule_change_reason(ReasonID),
        CONSTRAINT ufk_service_channel_work_history_syslastuserid_in_user FOREIGN KEY (SysLastUserID)
        REFERENCES dbo.utb_user(UserID),
        CONSTRAINT ufk_service_channel_work_history_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY (ClaimAspectServiceChannelID)
        REFERENCES dbo.utb_claim_aspect_service_channel(ClaimAspectServiceChannelID),
        CONSTRAINT ufk_service_channel_work_history_createduserid_in_user FOREIGN KEY (CreatedUserID)
        REFERENCES dbo.utb_user(UserID)
    ) ON ufg_claim



    CREATE INDEX uix_ie_service_channel_work_history_claimaspectservicechannelid ON dbo.utb_service_channel_work_history(ClaimAspectServiceChannelID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    CREATE INDEX uix_ie_service_channel_work_history_createduserid ON dbo.utb_service_channel_work_history(CreatedUserID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    CREATE INDEX uix_ie_service_channel_work_history_reasonid ON dbo.utb_service_channel_work_history(ReasonID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    
 
    EXEC sp_rename 'dbo.utb_repair_schedule_history','Tmp_utb_repair_schedule_history',OBJECT

    -- Capture any error

    SELECT @error = @@ERROR

END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

