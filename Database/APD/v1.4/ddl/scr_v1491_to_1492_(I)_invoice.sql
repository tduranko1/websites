--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_invoice'                      --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_documentid_in_document
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_claimaspectservicechannelid_in_claim_aspect_service_channel
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_authorizinguserid_in_user
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_recordinguserid_in_user
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_syslastuserid_in_user
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_claimaspectid_in_claim_aspect
    
    COMMIT
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_invoice
	    (
	    InvoiceID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AuthorizingUserID udt_std_id NULL,
	    ClaimAspectID udt_std_id_big NOT NULL,
	    ClaimAspectServiceChannelID udt_std_id_big NULL,
	    DocumentID udt_std_id_big NULL,
	    RecordingUserID udt_std_id NOT NULL,
	    AdminFeeAmount udt_std_money NULL,
	    AdminFeeCD udt_std_cd NULL,
	    Amount udt_std_money NOT NULL,
	    AuthorizedDate udt_std_datetime NULL,
	    CarrierOfficeCode udt_std_desc_short NULL,
	    CarrierRepName udt_std_name NULL,
	    ClientFeeCode udt_std_name NULL,
	    DeductibleAmt udt_std_money NULL,
	    Description udt_std_desc_mid NOT NULL,
	    DispatchNumber udt_std_desc_short NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    EntryDate udt_std_datetime NOT NULL,
	    FeeCategoryCD udt_std_cd NULL,
	    InsuredName udt_per_name NULL,
	    InvoiceDescription udt_std_desc_mid NULL,
	    ItemizeFlag udt_std_flag NOT NULL,
	    ItemTypeCD udt_std_cd NOT NULL,
	    PayeeAddress1 udt_addr_line_1 NULL,
	    PayeeAddress2 udt_addr_line_2 NULL,
	    PayeeAddressCity udt_addr_city NULL,
	    PayeeAddressState udt_addr_state NULL,
	    PayeeAddressZip udt_addr_zip_code NULL,
	    PayeeID udt_std_id_big NULL,
	    PayeeName udt_std_name NULL,
	    PayeeTypeCD udt_std_cd NULL,
	    PaymentChannelCD udt_std_cd NULL,
	    SentToIngresDate udt_std_datetime NULL,
	    StatusCD udt_std_cd NOT NULL,
	    StatusDate udt_std_datetime NOT NULL,
	    SystemAmount udt_std_money NULL,
	    TaxTotalAmt udt_std_money NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow
    
    SET IDENTITY_INSERT dbo.Tmp_utb_invoice ON
    
    IF EXISTS(SELECT * FROM dbo.utb_invoice)
	     EXEC('INSERT INTO dbo.Tmp_utb_invoice (InvoiceID, AuthorizingUserID, ClaimAspectID, ClaimAspectServiceChannelID, DocumentID, RecordingUserID, AdminFeeAmount, AdminFeeCD, Amount, AuthorizedDate, CarrierOfficeCode, CarrierRepName, ClientFeeCode, Description, DispatchNumber, EnabledFlag, EntryDate, FeeCategoryCD, InsuredName, InvoiceDescription, ItemizeFlag, ItemTypeCD, PayeeAddress1, PayeeAddress2, PayeeAddressCity, PayeeAddressState, PayeeAddressZip, PayeeID, PayeeName, PayeeTypeCD, PaymentChannelCD, SentToIngresDate, StatusCD, StatusDate, SystemAmount, SysLastUserID, SysLastUpdatedDate)
		    SELECT InvoiceID, AuthorizingUserID, ClaimAspectID, ClaimAspectServiceChannelID, DocumentID, RecordingUserID, AdminFeeAmount, AdminFeeCD, Amount, AuthorizedDate, CarrierOfficeCode, CarrierRepName, ClientFeeCode, Description, DispatchNumber, EnabledFlag, EntryDate, FeeCategoryCD, InsuredName, InvoiceDescription, ItemizeFlag, ItemTypeCD, PayeeAddress1, PayeeAddress2, PayeeAddressCity, PayeeAddressState, PayeeAddressZip, PayeeID, PayeeName, PayeeTypeCD, PaymentChannelCD, SentToIngresDate, StatusCD, StatusDate, SystemAmount, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_invoice WITH (HOLDLOCK TABLOCKX)')
    
    SET IDENTITY_INSERT dbo.Tmp_utb_invoice OFF
    
    ALTER TABLE dbo.utb_invoice_service
	    DROP CONSTRAINT ufk_invoice_service_invoiceid_in_invoice
    
    DROP TABLE dbo.utb_invoice
    
    EXECUTE sp_rename N'dbo.Tmp_utb_invoice', N'utb_invoice', 'OBJECT' 
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    upk_invoice PRIMARY KEY CLUSTERED 
	    (
	    InvoiceID
	    ) WITH FILLFACTOR = 80 ON ufg_workflow

    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_claimaspectservicechannelid ON dbo.utb_invoice
	    (
	    ClaimAspectServiceChannelID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_claimaspectid ON dbo.utb_invoice
	    (
	    ClaimAspectID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_documentid ON dbo.utb_invoice
	    (
	    DocumentID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_authorizinguserid ON dbo.utb_invoice
	    (
	    AuthorizingUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_recordinguserid ON dbo.utb_invoice
	    (
	    RecordingUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_invoice_statuscd ON dbo.utb_invoice
	    (
	    StatusCD
	    ) ON ufg_workflow_index
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_adminfeecd CHECK (([AdminFeeCD] = 'I' or [AdminFeeCD] = 'S'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_feecategorycd CHECK (([FeeCategoryCD] = 'A' or [FeeCategoryCD] = 'H'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_itemtypecd CHECK (([ItemTypeCD] = 'E' or [ItemTypeCD] = 'F' or [ItemTypeCD] = 'I' or [ItemTypeCD] = 'L'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_payeetypecd CHECK (([PayeeTypeCD] = 'A' or [PayeeTypeCD] = 'I' or [PayeeTypeCD] = 'O' or [PayeeTypeCD] = 'S' or [PayeeTypeCD] = 'V'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_paymentchannelcd CHECK (([PaymentChannelCD] = 'BOTH' or [PaymentChannelCD] = 'CCC' or [PaymentChannelCD] = 'PCSO' or [PaymentChannelCD] = 'STD'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    uck_invoice_statuscd CHECK (([StatusCD] = 'AC' or [StatusCD] = 'APD' or [StatusCD] = 'AUTH' or [StatusCD] = 'FS' or [StatusCD] = 'NB' or [StatusCD] = 'PU'))
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_authorizinguserid_in_user FOREIGN KEY
	    (
	    AuthorizingUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_recordinguserid_in_user FOREIGN KEY
	    (
	    RecordingUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )
    
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_documentid_in_document FOREIGN KEY
	    (
	    DocumentID
	    ) REFERENCES dbo.utb_document
	    (
	    DocumentID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_invoice_service ADD CONSTRAINT
	    ufk_invoice_service_invoiceid_in_invoice FOREIGN KEY
	    (
	    InvoiceID
	    ) REFERENCES dbo.utb_invoice
	    (
	    InvoiceID
	    )
    
    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

