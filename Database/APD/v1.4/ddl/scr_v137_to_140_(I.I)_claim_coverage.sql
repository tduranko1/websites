--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_coverage'               --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             EnabledFlag                                           !!!!
--!!!!!         Change columns                                            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_claim_coverage DROP CONSTRAINT upk_claim_coverage
    
    EXECUTE sp_rename N'dbo.utb_claim_coverage', N'Tmp_utb_claim_coverage', 'OBJECT'
  
    COMMIT
 

    BEGIN TRANSACTION

    CREATE TABLE dbo.utb_claim_coverage(
        ClaimCoverageID                  udt_std_id_big               IDENTITY(1,1),
        ClientCoverageTypeID             udt_std_id                   NULL,
        LynxID                           udt_std_int_big              NOT NULL,
        AddtlCoverageFlag                udt_std_flag                 NOT NULL,
        CoverageTypeCD                   udt_std_cd                   NOT NULL,
        Description                      udt_std_desc_mid             NULL,
        DeductibleAmt                    udt_std_money                NULL,
        EnabledFlag                      udt_enabled_flag             NOT NULL,
        LimitAmt                         udt_std_money                NULL,
        LimitDailyAmt                    udt_std_money                NULL,
        MaximumDays                      udt_std_int_small            NULL,
        SysLastUserID                    udt_std_id                   NOT NULL,
        SysLastUpdatedDate               udt_sys_last_updated_date    NOT NULL,
        CONSTRAINT upk_claim_coverage PRIMARY KEY CLUSTERED (ClaimCoverageID)
        WITH FILLFACTOR = 90
        ON ufg_claim, 
        CONSTRAINT ufk_claim_coverage_clientcoveragetypeid_in_client_coverage_type FOREIGN KEY (ClientCoverageTypeID)
        REFERENCES dbo.utb_client_coverage_type(ClientCoverageTypeID),
        CONSTRAINT ufk_claim_coverage_lynxid_in_claim FOREIGN KEY (LynxID)
        REFERENCES dbo.utb_claim(LynxID),
        CONSTRAINT ufk_claim_coverage_syslastuserid_in_user FOREIGN KEY (SysLastUserID)
        REFERENCES dbo.utb_user(UserID),
        CONSTRAINT uck_claim_coverage_coveragetypecd
        CHECK ([CoverageTypeCD] = 'COLL' or [CoverageTypeCD] = 'COMP' or [CoverageTypeCD] = 'GLAS' or [CoverageTypeCD] = 'LIAB' or [CoverageTypeCD] = 'RENT' or [CoverageTypeCD] = 'TOW' or [CoverageTypeCD] = 'UIM' or [CoverageTypeCD] = 'UM')
    ) ON ufg_claim


    CREATE INDEX uix_ie_claim_coverage_clientcoveragetypeid ON dbo.utb_claim_coverage(ClientCoverageTypeID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    CREATE INDEX uix_ie_claim_coverage_lynxid ON dbo.utb_claim_coverage(LynxID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    


    COMMIT
    
    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

