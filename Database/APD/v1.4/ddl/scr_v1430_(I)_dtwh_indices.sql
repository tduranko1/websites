IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_assignment_type') AND name='uix_dtwh_dim_assignment_type_servicechannelid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_assignment_type.uix_dtwh_dim_assignment_type_servicechannelid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_assignment_type') AND name='uix_dtwh_dim_assignment_type_servicechannelid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_assignment_type.uix_dtwh_dim_assignment_type_servicechannelid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_assignment_type.uix_dtwh_dim_assignment_type_servicechannelid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_customerid_in_dtwh_dim_customer
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_insurancecompanyid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_insurancecompanyid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_insurancecompanyid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_insurancecompanyid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_insurancecompanyid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_customerid_in_dtwh_dim_customer
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_officeid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_officeid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_officeid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_officeid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_officeid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_customerid_in_dtwh_dim_customer
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_userid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_userid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_customer') AND name='uix_ie_dtwh_dim_customer_userid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_userid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_userid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_disposition_type') AND name='uix_dtwh_dim_disposition_type_dispositiontypecd')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_disposition_type.uix_dtwh_dim_disposition_type_dispositiontypecd
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_disposition_type') AND name='uix_dtwh_dim_disposition_type_dispositiontypecd')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_disposition_type.uix_dtwh_dim_disposition_type_dispositiontypecd >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_disposition_type.uix_dtwh_dim_disposition_type_dispositiontypecd >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_disposition_type') AND name='uix_ie_dtwh_dim_disposition_type_description')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_disposition_type.uix_ie_dtwh_dim_disposition_type_description
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_disposition_type') AND name='uix_ie_dtwh_dim_disposition_type_description')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_disposition_type.uix_ie_dtwh_dim_disposition_type_description >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_disposition_type.uix_ie_dtwh_dim_disposition_type_description >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_documentsourceid_in_dtwh_dim_document_source
go
ALTER TABLE dbo.utb_dtwh_dim_repair_location DROP CONSTRAINT ufk_dtwh_dim_repair_location_standarddocumentsourceid_in_dtwh_dim_document_source
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_document_source') AND name='uix_dtwh_dim_document_source_documentsourcename')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_documentsourcename
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_document_source') AND name='uix_dtwh_dim_document_source_documentsourcename')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_documentsourcename >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_documentsourcename >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_documentsourceid_in_dtwh_dim_document_source
go
ALTER TABLE dbo.utb_dtwh_dim_repair_location DROP CONSTRAINT ufk_dtwh_dim_repair_location_standarddocumentsourceid_in_dtwh_dim_document_source
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_document_source') AND name='uix_dtwh_dim_document_source_electronicsourceflag')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_electronicsourceflag
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_document_source') AND name='uix_dtwh_dim_document_source_electronicsourceflag')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_electronicsourceflag >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_electronicsourceflag >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_lynx_handler') AND name='uix_dtwh_dim_lynx_handler_userid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_lynx_handler.uix_dtwh_dim_lynx_handler_userid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_lynx_handler') AND name='uix_dtwh_dim_lynx_handler_userid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_lynx_handler.uix_dtwh_dim_lynx_handler_userid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_lynx_handler.uix_dtwh_dim_lynx_handler_userid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_city')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_city
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_city')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_city >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_city >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_county')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_county
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_county')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_county >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_county >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_shoplocationid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_shoplocationid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_shoplocationid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_shoplocationid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_shoplocationid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_stateid')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_stateid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_stateid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_stateid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_stateid >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_zipcode')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_zipcode
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_repair_location') AND name='uix_ie_dtwh_dim_repair_location_zipcode')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_zipcode >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_zipcode >>>'
END
go
ALTER TABLE dbo.utb_dtwh_dim_assignment_type DROP CONSTRAINT ufk_dtwh_dim_assignment_type_servicechannelid_in_dtwh_dim_service_channel
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_service_channel') AND name='uix_dtwh_dim_service_channel_servicechannelcd')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_service_channel.uix_dtwh_dim_service_channel_servicechannelcd
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_service_channel') AND name='uix_dtwh_dim_service_channel_servicechannelcd')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_service_channel.uix_dtwh_dim_service_channel_servicechannelcd >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_service_channel.uix_dtwh_dim_service_channel_servicechannelcd >>>'
END
go
ALTER TABLE dbo.utb_dtwh_dim_repair_location DROP CONSTRAINT ufk_dtwh_dim_repair_location_stateid_in_dtwh_dim_state
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_countrycode')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_countrycode
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_countrycode')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_countrycode >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_countrycode >>>'
END
go
ALTER TABLE dbo.utb_dtwh_dim_repair_location DROP CONSTRAINT ufk_dtwh_dim_repair_location_stateid_in_dtwh_dim_state
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_regioncd')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_regioncd
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_regioncd')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_regioncd >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_regioncd >>>'
END
go
ALTER TABLE dbo.utb_dtwh_dim_repair_location DROP CONSTRAINT ufk_dtwh_dim_repair_location_stateid_in_dtwh_dim_state
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_statecode')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_statecode
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_state') AND name='uix_dtwh_dim_state_statecode')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_statecode >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_statecode >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_datevalue')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_datevalue
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_datevalue')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_datevalue >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_datevalue >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_dayofmonth')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_dayofmonth
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_dayofmonth')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_dayofmonth >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_dayofmonth >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_hourvalue')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_hourvalue
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_hourvalue')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_hourvalue >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_hourvalue >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_julianday')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_julianday
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_julianday')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_julianday >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_julianday >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_monthofyear')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_monthofyear
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_monthofyear')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_monthofyear >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_monthofyear >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_quarter')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_quarter
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_quarter')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_quarter >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_quarter >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_weekofyear')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_weekofyear
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_weekofyear')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_weekofyear >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_weekofyear >>>'
END
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidassignment_in_dtwh_dim_time
go
ALTER TABLE dbo.utb_dtwh_fact_estimate DROP CONSTRAINT ufk_dtwh_fact_estimate_timeidreceived_in_dtwh_dim_time
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_yearvalue')
BEGIN
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_yearvalue
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_dim_time') AND name='uix_dtwh_dim_time_yearvalue')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_yearvalue >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_yearvalue >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ak_dtwh_fact_estimate_documentid')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ak_dtwh_fact_estimate_documentid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ak_dtwh_fact_estimate_documentid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ak_dtwh_fact_estimate_documentid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ak_dtwh_fact_estimate_documentid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_claimaspectid')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_claimaspectid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_claimaspectid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_claimaspectid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_claimaspectid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_customerid')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_customerid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_customerid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_customerid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_customerid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_documentsourceid')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_documentsourceid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_documentsourceid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_documentsourceid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_documentsourceid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_repairlocationid')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_repairlocationid
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_repairlocationid')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_repairlocationid >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_repairlocationid >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_timeidassignment')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidassignment
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_timeidassignment')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidassignment >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidassignment >>>'
END
go
IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_timeidreceived')
BEGIN
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidreceived
    IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_dtwh_fact_estimate') AND name='uix_ie_dtwh_fact_estimate_timeidreceived')
        PRINT '<<< FAILED DROPPING INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidreceived >>>'
    ELSE
        PRINT '<<< DROPPED INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidreceived >>>'
END
go

CREATE NONCLUSTERED INDEX uix_ie_estimate_summary_documentid_estimatesummarytypeid_agreedextendedamt
    ON dbo.utb_estimate_summary(DocumentID, EstimateSummaryTypeID, AgreedExtendedAmt)
  WITH FILLFACTOR = 90
    ON ufg_estimate_index
go
CREATE NONCLUSTERED INDEX uix_ie_estimate_summary_documentid_estimatesummarytypeid_originalextendedamt
    ON dbo.utb_estimate_summary(DocumentID, EstimateSummaryTypeID, OriginalExtendedAmt)
  WITH FILLFACTOR = 90
    ON ufg_estimate_index
go
CREATE NONCLUSTERED INDEX uix_ie_estimate_summary_documentid_estimatesummarytypeid_originalunitamt
    ON dbo.utb_estimate_summary(DocumentID, EstimateSummaryTypeID, OriginalUnitAmt)
  WITH FILLFACTOR = 90
    ON ufg_estimate_index
go

CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_claimaspectid_assignmenttypeid
    ON dbo.utb_dtwh_fact_claim(ClaimAspectID, AssignmentTypeID)
  WITH FILLFACTOR = 90
    ON ufg_dtwh_index
go

CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_documentid
    ON dbo.utb_dtwh_fact_estimate(DocumentID)
  WITH FILLFACTOR = 90
    ON ufg_dtwh_index
go

CREATE NONCLUSTERED INDEX uix_ie_user_officeid
    ON dbo.utb_user(OfficeID)
  WITH FILLFACTOR = 90
    ON ufg_security_index
go

CREATE NONCLUSTERED INDEX uix_ie_history_log_syslastupdateddate_lynxid
    ON dbo.utb_history_log(SysLastUpdatedDate DESC, LynxID)
  WITH FILLFACTOR = 90
    ON ufg_workflow_index
go

CREATE NONCLUSTERED INDEX uix_ie_history_log_LynxID_ClaimAspectTypeID_ClaimAspectNumber_EventID_CompletedDate
    ON dbo.utb_history_log(LynxID DESC, ClaimAspectTypeID, ClaimAspectNumber, EventID, CompletedDate)
  WITH FILLFACTOR = 90
    ON ufg_workflow_index
go

