--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CLEANUP SCRIPT FOR UPDATING APD DATABASE          !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_checklist_history 
        ADD CONSTRAINT ufk_checklist_history_checklistid_in_checklist
        FOREIGN KEY (CheckListID)
        REFERENCES dbo.utb_checklist (CheckListID)


    ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	    ufk_assignment_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT 
       ufk_claim_aspect_service_channel_claimaspectid_in_claim_aspect FOREIGN KEY
       (
       ClaimAspectID
       ) REFERENCES dbo.utb_claim_aspect 
       (
       ClaimAspectID
       )


    CREATE NONCLUSTERED INDEX uix_ie_assignment_claimaspectservicechannelid ON dbo.utb_assignment
	    (
	    ClaimAspectServiceChannelID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    ALTER TABLE dbo.utb_checklist ADD CONSTRAINT
	    ufk_checklist_claimaspectservicechannelid_in_claim_aspect_service_channel FOREIGN KEY
	    (
	    ClaimAspectServiceChannelID
	    ) REFERENCES dbo.utb_claim_aspect_service_channel
	    (
	    ClaimAspectServiceChannelID
	    )

    CREATE NONCLUSTERED INDEX uix_ie_checklist_claimaspectservicechannelid ON dbo.utb_checklist
	    (
	    ClaimAspectServiceChannelID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

	ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
		uck_invoice_paymentchannelcd CHECK (([PaymentChannelCD] = 'BOTH' or [PaymentChannelCD] = 'CCC' or [PaymentChannelCD] = 'PCSO' or [PaymentChannelCD] = 'STD'))

    ALTER TABLE dbo.utb_status 
        ADD CONSTRAINT uck_status_servicechannelcd
        CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA')

     ALTER TABLE dbo.utb_status 
        ADD CONSTRAINT uck_status_statustypecd
        CHECK ([StatusTypeCD] = 'ELC' or [StatusTypeCD] = 'FAX' or [StatusTypeCD] = 'SC')

    ALTER TABLE dbo.utb_service DROP CONSTRAINT uck_service_servicechannelcd

    ALTER TABLE dbo.utb_service 
        ADD CONSTRAINT uck_service_servicechannelcd
        CHECK ([ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = '?')

    ALTER TABLE dbo.utb_client_coverage_type DROP CONSTRAINT uck_client_coverage_type_coverageprofilecd

    ALTER TABLE dbo.utb_client_coverage_type 
        ADD CONSTRAINT uck_client_coverage_type_coverageprofilecd
        CHECK ([CoverageProfileCD] = 'COLL' or [CoverageProfileCD] = 'COMP' or [CoverageProfileCD] = 'LIAB' or [CoverageProfileCD] = 'RENT' or [CoverageProfileCD] = 'UIM' or [CoverageProfileCD] = 'UM')

   COMMIT
 
    -- Capture any error

    SELECT @error = @@ERROR

END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

