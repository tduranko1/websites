--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_bundling_document_type'    --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_bundling_document_type
	    DROP CONSTRAINT ufk_bundling_document_type_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_bundling_document_type
	    DROP CONSTRAINT ufk_bundling_document_type_documenttypeid_in_document_type

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_bundling_document_type
	    DROP CONSTRAINT ufk_bundling_document_type_bundlingid_in_bundling

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_bundling_document_type
	    (
	    BundlingID udt_std_id_big NOT NULL,
	    DocumentTypeID udt_std_int_tiny NOT NULL,
	    DirectionalCD udt_std_cd NULL,
	    DirectionToPayFlag udt_std_flag NOT NULL,
	    DuplicateFlag udt_std_flag NULL,
	    EstimateDuplicateFlag udt_std_flag NOT NULL,
	    EstimateTypeCD udt_std_cd NULL,
	    FinalEstimateFlag udt_std_flag NOT NULL,
        LossState udt_addr_state NULL, 
	    MandatoryFlag udt_std_flag NOT NULL,
	    SelectionOrder udt_std_int_tiny NOT NULL,
        ShopState udt_addr_state NULL, 
	    VANFlag udt_std_flag NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

    IF EXISTS(SELECT * FROM dbo.utb_bundling_document_type)
	     EXEC('INSERT INTO dbo.Tmp_utb_bundling_document_type (BundlingID, DocumentTypeID, MandatoryFlag, DuplicateFlag, SysLastUserID, SysLastUpdatedDate)
		    SELECT BundlingID, DocumentTypeID, MandatoryFlag, DuplicateFlag, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_bundling_document_type WITH (HOLDLOCK TABLOCKX)')

    DROP TABLE dbo.utb_bundling_document_type

    EXECUTE sp_rename N'dbo.Tmp_utb_bundling_document_type', N'utb_bundling_document_type', 'OBJECT' 

    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT
	    upk_bundling_document_type PRIMARY KEY CLUSTERED 
	    (
	    BundlingID,
	    DocumentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow


    CREATE NONCLUSTERED INDEX uix_ie_bundling_document_type_bundlingid ON dbo.utb_bundling_document_type
	    (
	    BundlingID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_bundling_document_type_documenttypeid ON dbo.utb_bundling_document_type
	    (
	    DocumentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT
	    ufk_bundling_document_type_bundlingid_in_bundling FOREIGN KEY
	    (
	    BundlingID
	    ) REFERENCES dbo.utb_bundling
	    (
	    BundlingID
	    )

    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT
	    ufk_bundling_document_type_documenttypeid_in_document_type FOREIGN KEY
	    (
	    DocumentTypeID
	    ) REFERENCES dbo.utb_document_type
	    (
	    DocumentTypeID
	    )

    ALTER TABLE dbo.utb_bundling_document_type ADD CONSTRAINT
	    ufk_bundling_document_type_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

