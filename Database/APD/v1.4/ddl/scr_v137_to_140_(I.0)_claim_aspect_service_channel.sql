--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_claim_aspect_service_channel'  --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_claim_aspect_service_channel(
        ClaimAspectServiceChannelID      udt_std_id_big               IDENTITY(1,1),
        ClaimAspectID                    udt_std_id_big               NOT NULL,
        CreatedUserID                    udt_std_id                   NOT NULL,
        AppraiserInvoiceDate             udt_std_datetime             NULL,
        AppraiserPaidDate                udt_std_datetime             NULL,
        CashOutDate                      udt_std_datetime             NULL,
        ClientInvoiceDate                udt_std_datetime             NULL,
        CreatedDate                      udt_std_datetime             NOT NULL,
        DispositionTypeCD                udt_std_cd                   NULL,
        EnabledFlag                      udt_enabled_flag             NOT NULL,
        InspectionDate                   udt_std_datetime             NULL,
        OriginalCompleteDate             udt_std_datetime             NULL,
        OriginalEstimateDate             udt_std_datetime             NULL,
        PrimaryFlag                      udt_std_flag                 NOT NULL,
        ServiceChannelCD                 udt_std_cd                   NULL,
        WorkEndConfirmFlag               udt_std_flag                 NOT NULL,
        WorkEndDate                      udt_std_datetime             NULL,
        WorkEndDateOriginal              udt_std_datetime             NULL,
        WorkStartConfirmFlag             udt_std_flag                 NOT NULL,
        WorkStartDate                    udt_std_datetime             NULL,
        SysLastUserID                    udt_std_id                   NOT NULL,
        SysLastUpdatedDate               udt_sys_last_updated_date    NOT NULL,
        CONSTRAINT upk_claim_aspect_service_channel PRIMARY KEY CLUSTERED (ClaimAspectServiceChannelID)
        WITH FILLFACTOR = 90
        ON ufg_claim, 
        CONSTRAINT ufk_claim_aspect_service_channel_createduserid_in_user FOREIGN KEY (CreatedUserID)
        REFERENCES dbo.utb_user(UserID),
        CONSTRAINT ufk_claim_aspect_service_channel_syslastuserid_in_user FOREIGN KEY (SysLastUserID)
        REFERENCES dbo.utb_user(UserID)
    ) ON ufg_claim


    CREATE INDEX uix_ie_claim_aspect_service_channel_claimaspectid ON dbo.utb_claim_aspect_service_channel(ClaimAspectID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    CREATE INDEX uix_ie_claim_aspect_service_channel_createduserid ON dbo.utb_claim_aspect_service_channel(CreatedUserID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    ALTER TABLE dbo.utb_claim_aspect_service_channel 
        ADD CONSTRAINT uck_claim_aspect_service_channel_dispositiontypecd
        CHECK ([DispositionTypeCD] = 'CA' or [DispositionTypeCD] = 'CO' or [DispositionTypeCD] = 'IC' or [DispositionTypeCD] = 'IME' or [DispositionTypeCD] = 'IPD' or [DispositionTypeCD] = 'NA' or [DispositionTypeCD] = 'NOCV' or [DispositionTypeCD] = 'PCO' or [DispositionTypeCD] = 'RC' or [DispositionTypeCD] = 'RNC' or [DispositionTypeCD] = 'ST' or [DispositionTypeCD] = 'TL' or [DispositionTypeCD] = 'VD')

    ALTER TABLE dbo.utb_claim_aspect_service_channel 
        ADD CONSTRAINT uck_claim_aspect_service_channel_servicechannelcd
        CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA')

    EXEC sp_rename 'dbo.utb_mobile_electronics_results','tmp_utb_mobile_electronics_results',OBJECT


    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

