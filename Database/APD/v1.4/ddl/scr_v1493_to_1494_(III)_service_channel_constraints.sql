ALTER TABLE dbo.utb_assignment_type
	DROP CONSTRAINT uck_assignment_type_servicechanneldefaultcd
GO
ALTER TABLE dbo.utb_assignment_type ADD CONSTRAINT
	uck_assignment_type_servicechanneldefaultcd CHECK (([ServiceChannelDefaultCD] = 'DA' or [ServiceChannelDefaultCD] = 'DR' or [ServiceChannelDefaultCD] = 'IA' or [ServiceChannelDefaultCD] = 'ME' or [ServiceChannelDefaultCD] = 'PS' or [ServiceChannelDefaultCD] = 'SA' or [ServiceChannelDefaultCD] = 'TL'))
GO
ALTER TABLE dbo.utb_claim_aspect_service_channel
	DROP CONSTRAINT uck_claim_aspect_service_channel_servicechannelcd
GO
ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	uck_claim_aspect_service_channel_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_claim_aspect_status
	DROP CONSTRAINT uck_claim_aspect_status_servicechannelcd
GO
ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	uck_claim_aspect_status_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_client_bundling
	DROP CONSTRAINT uck_client_bundling_servicechannelcd
GO
ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	uck_client_bundling_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_client_required_document_type
	DROP CONSTRAINT uck_client_required_document_type_servicechannelcd
GO
ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
	uck_client_required_document_type_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_client_service_channel
	DROP CONSTRAINT uck_client_service_channel_servicechannelcd
GO
ALTER TABLE dbo.utb_client_service_channel ADD CONSTRAINT
	uck_client_service_channel_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_concession_reason
	DROP CONSTRAINT uck_concession_reason_servicechannelcd
GO
ALTER TABLE dbo.utb_concession_reason ADD CONSTRAINT
	uck_concession_reason_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_message_template
	DROP CONSTRAINT uck_message_template_servicechannelcd
GO
ALTER TABLE dbo.utb_message_template ADD CONSTRAINT
	uck_message_template_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_service_channel_schedule_change_reason
	DROP CONSTRAINT uck_service_channel_schedule_change_reason_servicechannelcd
GO
ALTER TABLE dbo.utb_service_channel_schedule_change_reason ADD CONSTRAINT
	uck_service_channel_schedule_change_reason_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_status
	DROP CONSTRAINT uck_status_servicechannelcd
GO
ALTER TABLE dbo.utb_status ADD CONSTRAINT
	uck_status_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_task_assignment_pool
	DROP CONSTRAINT uck_task_assignment_pool_servicechannelcd
GO
ALTER TABLE dbo.utb_task_assignment_pool ADD CONSTRAINT
	uck_task_assignment_pool_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_task_escalation
	DROP CONSTRAINT uck_task_escalation_servicechannelcd
GO
ALTER TABLE dbo.utb_task_escalation ADD CONSTRAINT
	uck_task_escalation_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_invoice_service
	DROP CONSTRAINT uck_invoice_service_servicechannelcd
GO
ALTER TABLE dbo.utb_invoice_service ADD CONSTRAINT
	uck_invoice_service_servicechannelcd CHECK (([ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))
GO
ALTER TABLE dbo.utb_service
	DROP CONSTRAINT uck_service_servicechannelcd
GO
ALTER TABLE dbo.utb_service ADD CONSTRAINT
	uck_service_servicechannelcd CHECK (([ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = '?'))
GO
