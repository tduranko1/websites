SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

--DROP TABLE dbo.utb_dtwh_dim_shop

CREATE TABLE dbo.utb_dtwh_dim_shop
   (ShopID bigint NOT NULL,
	ShopParentID bigint NULL,
	Address1 varchar(50) NULL,
	Address2 varchar(50) NULL,
	AddressCity varchar(30) NULL,
	AddressCounty varchar(30) NULL,
	AddressState varchar(2) NULL,
	AddressZip varchar(8) NULL,
	BusinessOwner1EmailAddr varchar(50) NULL,
	BusinessOwner1Name varchar(50) NULL,
	BusinessOwner1PhoneNumber varchar(12) NULL,
	BusinessOwner1PhoneExtNumber varchar(5) NULL,
	BusinessOwner2EmailAddr varchar(50) NULL,
	BusinessOwner2Name varchar(50) NULL,
	BusinessOwner2PhoneNumber varchar(12) NULL,
	BusinessOwner2PhoneExtNumber varchar(5) NULL,
	BusinessTypeCD varchar(4) NULL,
	BusinessTypeDesc varchar(20) NULL,
	FaxNumber varchar(12) NULL,
	FaxNumberExtension varchar(5) NULL,
	FedTaxId varchar(15) NULL,
	LastUserID int NOT NULL,
	LastUpdatedByName varchar(50) NULL,
	LastUpdatedDate datetime NOT NULL,
	PhoneNumber varchar(12) NULL,
	PhoneNumberExtension varchar(5) NULL,
	ShopName varchar(50) NOT NULL
) ON ufg_dtwh

GO