--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_admin_fee'                     --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_admin_fee 
    (
        AdminFeeID   udt_std_id_big IDENTITY(1,1) NOT NULL,
	    ClientFeeID  udt_std_id NOT NULL,
        AdminFeeCD   udt_std_cd NOT NULL,
        AppliesToCD  udt_std_cd NOT NULL,
        Amount       udt_std_money NULL,
        EnabledFlag  udt_enabled_flag NOT NULL,
        SplitFlag    udt_std_flag NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL,
        CONSTRAINT upk_admin_fee
        PRIMARY KEY CLUSTERED (AdminFeeID) WITH FILLFACTOR=100 
                                                                              ON ufg_shop
    )
    ON ufg_shop
     
    ALTER TABLE dbo.utb_admin_fee ADD CONSTRAINT
	    uck_admin_fee_appliestocd CHECK (([AppliesToCD] = 'F' or [AppliesToCD] = 'M' or [AppliesToCD] = 'S' or [AppliesToCD] = 'X'))
    
    ALTER TABLE dbo.utb_admin_fee ADD CONSTRAINT
	    uck_admin_fee_adminfeecd CHECK ((AdminFeeCD = 'I' or AdminFeeCD = 'M' or AdminFeeCD = 'S'))
    
    ALTER TABLE dbo.utb_admin_fee ADD CONSTRAINT
	    ufk_admin_fee_clientfeeid_in_client_fee FOREIGN KEY
	    (
	    ClientFeeID
	    ) REFERENCES dbo.utb_client_fee
	    (
	    ClientFeeID
	    )
    
    ALTER TABLE dbo.utb_admin_fee ADD CONSTRAINT
	    ufk_admin_fee_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    CREATE NONCLUSTERED INDEX uix_ie_admin_fee_clientfeeid ON dbo.utb_admin_fee
	    (
	    ClientFeeID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

