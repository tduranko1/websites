    ALTER TABLE dbo.utb_shop_location_program_delta 
        ADD CONSTRAINT uck_shop_location_program_delta_actioncd
        CHECK ([ActionCD] = 'A' or [ActionCD] = 'D' or [ActionCD] = 'R' or [ActionCD] = 'S' or [ActionCD] = 'U')
