--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_insurance'       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_insurance
	    DROP CONSTRAINT ufk_insurance_deskauditpreferredcommunicationmethodid_in_communication_method

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_insurance
	    DROP CONSTRAINT ufk_insurance_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_insurance
	    (
	    InsuranceCompanyID udt_std_int_small NOT NULL,
	    DeskAuditPreferredCommunicationMethodID udt_std_int_tiny NULL,
	    Address1 udt_addr_line_1 NULL,
	    Address2 udt_addr_line_2 NULL,
	    AddressCity udt_addr_city NULL,
	    AddressState udt_addr_state NULL,
	    AddressZip udt_addr_zip_code NULL,
	    AssignmentAtSelectionFlag udt_std_flag NULL,
	    AuthorizeLaborRatesFlag udt_std_flag NOT NULL,
	    BillingModelCD udt_std_cd NOT NULL,
	    BusinessTypeCD udt_std_cd NULL,
	    CarrierLynxContactPhone udt_std_desc_short NULL,
	    CFLogoDisplayFlag udt_std_flag NOT NULL,
	    ClaimPointCarrierRepSelFlag udt_std_flag NOT NULL,
	    ClaimPointDocumentUploadFlag udt_std_flag NOT NULL,
	    ClientAccessFlag udt_std_flag NOT NULL,
	    DemoFlag udt_std_flag NOT NULL,
	    DeskAuditCompanyCD udt_std_cd NULL,
	    DeskReviewLicenseReqFlag udt_std_flag NOT NULL,
	    EarlyBillFlag udt_std_flag NOT NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    FaxAreaCode udt_ph_area_code NULL,
	    FaxExchangeNumber udt_ph_exchange_number NULL,
	    FaxExtensionNumber udt_ph_extension_number NULL,
	    FaxUnitNumber udt_ph_unit_number NULL,
	    FedTaxId udt_fed_tax_id NULL,
	    IngresAccountingId udt_std_int NULL,
	    InvoicingModelPaymentCD udt_std_cd NOT NULL,
	    InvoiceMethodCD udt_std_cd NOT NULL,
	    LicenseDeterminationCD udt_std_cd NULL,
	    Name udt_std_name NOT NULL,
	    PhoneAreaCode udt_ph_area_code NULL,
	    PhoneExchangeNumber udt_ph_exchange_number NULL,
	    PhoneExtensionNumber udt_ph_extension_number NULL,
	    PhoneUnitNumber udt_ph_unit_number NULL,
	    ReturnDocDestinationCD udt_std_cd NULL,
	    ReturnDocPackageTypeCD udt_std_cd NULL,
	    ReturnDocRoutingCD udt_std_cd NULL,
	    TotalLossValuationWarningPercentage udt_std_pct NULL,
	    TotalLossWarningPercentage udt_std_pct NULL,
	    WarrantyPeriodRefinishMinCD udt_std_cd NULL,
	    WarrantyPeriodWorkmanshipMinCD udt_std_cd NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_reference

    IF EXISTS(SELECT * FROM dbo.utb_insurance)
	     EXEC('INSERT INTO dbo.Tmp_utb_insurance (InsuranceCompanyID, DeskAuditPreferredCommunicationMethodID, Address1, Address2, AddressCity, AddressState, AddressZip, AssignmentAtSelectionFlag, AuthorizeLaborRatesFlag, BillingModelCD, BusinessTypeCD, CarrierLynxContactPhone, CFLogoDisplayFlag, ClaimPointCarrierRepSelFlag, ClaimPointDocumentUploadFlag, ClientAccessFlag, DemoFlag, DeskAuditCompanyCD, DeskReviewLicenseReqFlag, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, FedTaxId, IngresAccountingId, InvoicingModelPaymentCD, InvoiceMethodCD, LicenseDeterminationCD, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, ReturnDocDestinationCD, ReturnDocPackageTypeCD, ReturnDocRoutingCD, TotalLossValuationWarningPercentage, TotalLossWarningPercentage, WarrantyPeriodRefinishMinCD, WarrantyPeriodWorkmanshipMinCD, SysLastUserID, SysLastUpdatedDate)
		    SELECT InsuranceCompanyID, DeskAuditPreferredCommunicationMethodID, Address1, Address2, AddressCity, AddressState, AddressZip, AssignmentAtSelectionFlag, AuthorizeLaborRatesFlag, BillingModelCD, BusinessTypeCD, CarrierLynxContactPhone, CFLogoDisplayFlag, ClaimPointCarrierRepSelFlag, ClaimPointDocumentUploadFlag, ClientAccessFlag, DemoFlag, DeskAuditCompanyCD, DeskReviewLicenseReqFlag, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, FedTaxId, IngresAccountingId, InvoicingModelPaymentCD, InvoiceMethodCD, LicenseDeterminationCD, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, ReturnDocDestinationCD, ReturnDocPackageTypeCD, ReturnDocRoutingCD, TotalLossValuationWarningPercentage, TotalLossWarningPercentage, WarrantyPeriodRefinishMinCD, WarrantyPeriodWorkmanshipMinCD, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_insurance WITH (HOLDLOCK TABLOCKX)')

    ALTER TABLE dbo.utb_client_required_document_type
	    DROP CONSTRAINT ufk_client_required_document_type_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_labor_rate
	    DROP CONSTRAINT ufk_labor_rate_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_task_escalation
	    DROP CONSTRAINT ufk_task_escalation_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_bundling
	    DROP CONSTRAINT ufk_client_bundling_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_service_channel
	    DROP CONSTRAINT ufk_client_service_channel_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_coverage_type
	    DROP CONSTRAINT ufk_client_coverage_type_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_assignment_type
	    DROP CONSTRAINT ufk_client_assignment_type_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_office
	    DROP CONSTRAINT ufk_office_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_document_exception
	    DROP CONSTRAINT ufk_client_document_exception_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_contract_state
	    DROP CONSTRAINT ufk_client_contract_state_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_user_insurance
	    DROP CONSTRAINT ufk_user_insurance_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_shop_location_program_delta
	    DROP CONSTRAINT ufk_shop_location_program_delta_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_report
	    DROP CONSTRAINT ufk_client_report_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_workflow
	    DROP CONSTRAINT ufk_workflow_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_payment_type
	    DROP CONSTRAINT ufk_client_payment_type_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_fnol_parameter
	    DROP CONSTRAINT ufk_fnol_parameter_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_shop_location
	    DROP CONSTRAINT ufk_client_shop_location_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_form
	    DROP CONSTRAINT ufk_form_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_business_state
	    DROP CONSTRAINT ufk_client_business_state_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_claim_aspect_type
	    DROP CONSTRAINT ufk_client_claim_aspect_type_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_audit_log
	    DROP CONSTRAINT ufk_audit_log_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_claim
	    DROP CONSTRAINT ufk_claim_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_vendor
	    DROP CONSTRAINT ufk_client_vendor_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_scripting
	    DROP CONSTRAINT ufk_client_scripting_insurancecompanyid_in_insurance

    ALTER TABLE dbo.utb_client_fee
	    DROP CONSTRAINT ufk_client_fee_insurancecompanyid_in_insurance

    DROP TABLE dbo.utb_insurance

    EXECUTE sp_rename N'dbo.Tmp_utb_insurance', N'utb_insurance', 'OBJECT' 

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    upk_insurance PRIMARY KEY CLUSTERED 
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 100 ON ufg_reference


    CREATE UNIQUE NONCLUSTERED INDEX uix_ak_insurance_name ON dbo.utb_insurance
	    (
	    Name
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    CREATE NONCLUSTERED INDEX uix_ie_insurance_deskauditpreferredcommunicationmethodid ON dbo.utb_insurance
	    (
	    DeskAuditPreferredCommunicationMethodID
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_billingmodelcd CHECK (([BillingModelCD] = 'B' or [BillingModelCD] = 'C' or [BillingModelCD] = 'E'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_businesstypecd CHECK (([BusinessTypeCD] = 'S' or [BusinessTypeCD] = 'P' or [BusinessTypeCD] = 'C'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_invoicemethodcd CHECK (([InvoiceMethodCD] = 'P' or [InvoiceMethodCD] = 'F'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_invoicingmodelpaymentcd CHECK (([InvoicingModelPaymentCD] = 'B' or [InvoicingModelPaymentCD] = 'C'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_licensedeterminationcd CHECK (([LicenseDeterminationCD] = 'LOSS' or [LicenseDeterminationCD] = 'VLOC' or [LicenseDeterminationCD] = 'INSD'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_returndocdestinationcd CHECK (([ReturnDocDestinationCD] = 'REP' or [ReturnDocDestinationCD] = 'OFC' or [ReturnDocDestinationCD] = 'BOTH'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_returndocpackagetypecd CHECK (([ReturnDocPackageTypeCD] = 'PDF' or [ReturnDocPackageTypeCD] = 'PDFZ' or [ReturnDocPackageTypeCD] = 'TIF' or [ReturnDocPackageTypeCD] = 'TIFZ' or [ReturnDocPackageTypeCD] = 'ZIP'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_returndocroutingcd CHECK (([ReturnDocRoutingCD] = 'CCAV' or [ReturnDocRoutingCD] = 'EML' or [ReturnDocRoutingCD] = 'FAX' or [ReturnDocRoutingCD] = 'FTP' or [ReturnDocRoutingCD] = 'SG'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_warrantyperiodrefinishmincd CHECK (([WarrantyPeriodRefinishMinCD] = '0' or [WarrantyPeriodRefinishMinCD] = '1' or [WarrantyPeriodRefinishMinCD] = '3' or [WarrantyPeriodRefinishMinCD] = '5' or [WarrantyPeriodRefinishMinCD] = '99'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    uck_insurance_warrantyperiodworkmanshipmincd CHECK (([WarrantyPeriodWorkmanshipMinCD] = '0' or [WarrantyPeriodWorkmanshipMinCD] = '1' or [WarrantyPeriodWorkmanshipMinCD] = '3' or [WarrantyPeriodWorkmanshipMinCD] = '5' or [WarrantyPeriodWorkmanshipMinCD] = '99'))

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    ufk_insurance_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_insurance ADD CONSTRAINT
	    ufk_insurance_deskauditpreferredcommunicationmethodid_in_communication_method FOREIGN KEY
	    (
	    DeskAuditPreferredCommunicationMethodID
	    ) REFERENCES dbo.utb_communication_method
	    (
	    CommunicationMethodID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_fee ADD CONSTRAINT
	    ufk_client_fee_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_scripting ADD CONSTRAINT
	    ufk_client_scripting_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_vendor ADD CONSTRAINT
	    ufk_client_vendor_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim ADD CONSTRAINT
	    ufk_claim_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_audit_log ADD CONSTRAINT
	    ufk_audit_log_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_claim_aspect_type ADD CONSTRAINT
	    ufk_client_claim_aspect_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_business_state ADD CONSTRAINT
	    ufk_client_business_state_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_form ADD CONSTRAINT
	    ufk_form_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_shop_location ADD CONSTRAINT
	    ufk_client_shop_location_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_fnol_parameter ADD CONSTRAINT
	    ufk_fnol_parameter_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_payment_type ADD CONSTRAINT
	    ufk_client_payment_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_workflow ADD CONSTRAINT
	    ufk_workflow_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_report ADD CONSTRAINT
	    ufk_client_report_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_shop_location_program_delta ADD CONSTRAINT
	    ufk_shop_location_program_delta_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_user_insurance ADD CONSTRAINT
	    ufk_user_insurance_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_contract_state ADD CONSTRAINT
	    ufk_client_contract_state_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_document_exception ADD CONSTRAINT
	    ufk_client_document_exception_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_office ADD CONSTRAINT
	    ufk_office_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_assignment_type ADD CONSTRAINT
	    ufk_client_assignment_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_coverage_type ADD CONSTRAINT
	    ufk_client_coverage_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_service_channel ADD CONSTRAINT
	    ufk_client_service_channel_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_escalation ADD CONSTRAINT
	    ufk_task_escalation_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_labor_rate ADD CONSTRAINT
	    ufk_labor_rate_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_required_document_type ADD CONSTRAINT
	    ufk_client_required_document_type_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT

IF 0 = @error
BEGIN

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

