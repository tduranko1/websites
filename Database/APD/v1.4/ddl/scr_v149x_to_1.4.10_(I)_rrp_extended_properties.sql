--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          CHANGE CONSTRAINTS AND EXTENDED PROPERTIES               !!!!
--!!!!!                     FOR RRP SERVICE CHANNEL                       !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new service channel code to all pertinent constraints
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


--utb_task_escalation
ALTER TABLE dbo.[utb_task_escalation]
	DROP CONSTRAINT [uck_task_escalation_servicechannelcd]

ALTER TABLE [dbo].[utb_task_escalation]  WITH CHECK 
	ADD  CONSTRAINT [uck_task_escalation_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_task_escalation] CHECK CONSTRAINT [uck_task_escalation_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_task_escalation', 'constraint', 'uck_task_escalation_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_task_escalation, 'constraint', uck_task_escalation_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_task_escalation, 'constraint', uck_task_escalation_servicechannelcd


--utb_task_assignment_pool
ALTER TABLE dbo.[utb_task_assignment_pool]
	DROP CONSTRAINT [uck_task_assignment_pool_servicechannelcd]
	
ALTER TABLE [dbo].[utb_task_assignment_pool]  WITH CHECK 
	ADD  CONSTRAINT [uck_task_assignment_pool_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_task_assignment_pool] CHECK CONSTRAINT [uck_task_assignment_pool_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_task_assignment_pool', 'constraint', 'uck_task_assignment_pool_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_task_assignment_pool, 'constraint', uck_task_assignment_pool_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_task_assignment_pool, 'constraint', uck_task_assignment_pool_servicechannelcd
                            

--utb_assignment_type
ALTER TABLE dbo.[utb_assignment_type]
	DROP CONSTRAINT [uck_assignment_type_servicechanneldefaultcd]
	
ALTER TABLE [dbo].[utb_assignment_type]  WITH CHECK 
	ADD  CONSTRAINT [uck_assignment_type_servicechanneldefaultcd] CHECK  (([ServiceChannelDefaultCD] = 'DA' or [ServiceChannelDefaultCD] = 'DR' or [ServiceChannelDefaultCD] = 'GL' or [ServiceChannelDefaultCD] = 'IA' or [ServiceChannelDefaultCD] = 'ME' or [ServiceChannelDefaultCD] = 'PS' or [ServiceChannelDefaultCD] = 'SA' or [ServiceChannelDefaultCD] = 'TL' or [ServiceChannelDefaultCD] = 'RRP'))

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_assignment_type', 'constraint', 'uck_assignment_type_servicechanneldefaultcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_assignment_type, 'constraint', uck_assignment_type_servicechanneldefaultcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_assignment_type, 'constraint', uck_assignment_type_servicechanneldefaultcd


--utb_status
ALTER TABLE dbo.[utb_status]
	DROP CONSTRAINT [uck_status_servicechannelcd]
	
ALTER TABLE [dbo].[utb_status]  WITH NOCHECK 
	ADD  CONSTRAINT [uck_status_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_status] CHECK CONSTRAINT [uck_status_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_status', 'constraint', 'uck_status_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_status, 'constraint', uck_status_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_status, 'constraint', uck_status_servicechannelcd


--utb_service_channel_schedule_change_reason
ALTER TABLE dbo.[utb_service_channel_schedule_change_reason]
	DROP CONSTRAINT [uck_service_channel_schedule_change_reason_servicechannelcd]
	
ALTER TABLE [dbo].[utb_service_channel_schedule_change_reason]  WITH CHECK 
	ADD  CONSTRAINT [uck_service_channel_schedule_change_reason_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_service_channel_schedule_change_reason] CHECK CONSTRAINT [uck_service_channel_schedule_change_reason_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service_channel_schedule_change_reason', 'constraint', 'uck_service_channel_schedule_change_reason_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service_channel_schedule_change_reason, 'constraint', uck_service_channel_schedule_change_reason_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_service_channel_schedule_change_reason, 'constraint', uck_service_channel_schedule_change_reason_servicechannelcd


--utb_service
ALTER TABLE dbo.[utb_service]
	DROP CONSTRAINT [uck_service_servicechannelcd]
	
ALTER TABLE [dbo].[utb_service]  WITH CHECK 
	ADD  CONSTRAINT [uck_service_servicechannelcd] CHECK  (([ServiceChannelCD] = '?' or [ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_service] CHECK CONSTRAINT [uck_service_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_service', 'constraint', 'uck_service_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_service, 'constraint', uck_service_servicechannelcd
EXEC sp_addextendedproperty 'Codes', '1P|1st Party|3P|3rd Party|DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_service, 'constraint', uck_service_servicechannelcd


--utb_message_template
ALTER TABLE dbo.[utb_message_template]
	DROP CONSTRAINT [uck_message_template_servicechannelcd]

ALTER TABLE [dbo].[utb_message_template]  WITH CHECK 
	ADD  CONSTRAINT [uck_message_template_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_message_template] CHECK CONSTRAINT [uck_message_template_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_message_template', 'constraint', 'uck_message_template_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_message_template, 'constraint', uck_message_template_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_message_template, 'constraint', uck_message_template_servicechannelcd


--utb_invoice_service
ALTER TABLE dbo.[utb_invoice_service]
	DROP CONSTRAINT [uck_invoice_service_servicechannelcd]
	
ALTER TABLE [dbo].[utb_invoice_service]  WITH CHECK 
	ADD  CONSTRAINT [uck_invoice_service_servicechannelcd] CHECK  (([ServiceChannelCD] = '?' or [ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_invoice_service] CHECK CONSTRAINT [uck_invoice_service_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_invoice_service', 'constraint', 'uck_invoice_service_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_servicechannelcd
EXEC sp_addextendedproperty 'Codes', '1P|1st Party|3P|3rd Party|DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_invoice_service, 'constraint', uck_invoice_service_servicechannelcd
                            

--utb_concession_reason
ALTER TABLE dbo.[utb_concession_reason]
	DROP CONSTRAINT [uck_concession_reason_servicechannelcd]
	
ALTER TABLE [dbo].[utb_concession_reason]  WITH CHECK 
	ADD  CONSTRAINT [uck_concession_reason_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_concession_reason] CHECK CONSTRAINT [uck_concession_reason_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_concession_reason', 'constraint', 'uck_concession_reason_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_concession_reason, 'constraint', uck_concession_reason_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_concession_reason, 'constraint', uck_concession_reason_servicechannelcd


--utb_client_required_document_type
ALTER TABLE dbo.[utb_client_required_document_type]
	DROP CONSTRAINT [uck_client_required_document_type_servicechannelcd]

ALTER TABLE [dbo].[utb_client_required_document_type]  WITH CHECK 
	ADD  CONSTRAINT [uck_client_required_document_type_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_client_required_document_type] CHECK CONSTRAINT [uck_client_required_document_type_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_required_document_type', 'constraint', 'uck_client_required_document_type_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_required_document_type, 'constraint', uck_client_required_document_type_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_client_required_document_type, 'constraint', uck_client_required_document_type_servicechannelcd
                            

--utb_client_deliverable
ALTER TABLE dbo.[utb_client_deliverable]
	DROP CONSTRAINT [uck_client_deliverable_servicechannelcd]

ALTER TABLE [dbo].[utb_client_deliverable]  WITH CHECK 
	ADD  CONSTRAINT [uck_client_deliverable_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_client_deliverable] CHECK CONSTRAINT [uck_client_deliverable_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_deliverable', 'constraint', 'uck_client_deliverable_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_deliverable, 'constraint', uck_client_deliverable_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_client_deliverable, 'constraint', uck_client_deliverable_servicechannelcd
                            

--utb_client_bundling
ALTER TABLE dbo.[utb_client_bundling]
	DROP CONSTRAINT [uck_client_bundling_servicechannelcd]
	
ALTER TABLE [dbo].[utb_client_bundling]  WITH CHECK 
	ADD  CONSTRAINT [uck_client_bundling_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_client_bundling] CHECK CONSTRAINT [uck_client_bundling_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_bundling', 'constraint', 'uck_client_bundling_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_bundling, 'constraint', uck_client_bundling_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_client_bundling, 'constraint', uck_client_bundling_servicechannelcd


--utb_client_service_channel
ALTER TABLE dbo.[utb_client_service_channel]
	DROP CONSTRAINT [uck_client_service_channel_servicechannelcd]
	
ALTER TABLE [dbo].[utb_client_service_channel]  WITH CHECK 
	ADD  CONSTRAINT [uck_client_service_channel_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_client_service_channel] CHECK CONSTRAINT [uck_client_service_channel_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_client_service_channel', 'constraint', 'uck_client_service_channel_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_client_service_channel, 'constraint', uck_client_service_channel_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_client_service_channel, 'constraint', uck_client_service_channel_servicechannelcd


--utb_claim_aspect_status
ALTER TABLE dbo.[utb_claim_aspect_status]
	DROP CONSTRAINT [uck_claim_aspect_status_servicechannelcd]

ALTER TABLE [dbo].[utb_claim_aspect_status]  WITH NOCHECK 
	ADD  CONSTRAINT [uck_claim_aspect_status_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_claim_aspect_status] CHECK CONSTRAINT [uck_claim_aspect_status_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_status', 'constraint', 'uck_claim_aspect_status_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_claim_aspect_status, 'constraint', uck_claim_aspect_status_servicechannelcd


--utb_claim_aspect_service_channel
ALTER TABLE dbo.utb_claim_aspect_service_channel
	DROP CONSTRAINT [uck_claim_aspect_service_channel_servicechannelcd]

ALTER TABLE [dbo].[utb_claim_aspect_service_channel]  WITH NOCHECK 
	ADD  CONSTRAINT [uck_claim_aspect_service_channel_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_claim_aspect_service_channel] CHECK CONSTRAINT [uck_claim_aspect_service_channel_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_claim_aspect_service_channel', 'constraint', 'uck_claim_aspect_service_channel_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_claim_aspect_service_channel, 'constraint', uck_claim_aspect_service_channel_servicechannelcd


--utb_form
ALTER TABLE dbo.utb_form
	DROP CONSTRAINT [uck_form_servicechannelcd]

ALTER TABLE [dbo].[utb_form]  WITH NOCHECK 
	ADD  CONSTRAINT [uck_form_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_form] CHECK CONSTRAINT [uck_form_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_form', 'constraint', 'uck_form_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_form, 'constraint', uck_form_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_form, 'constraint', uck_form_servicechannelcd


--utb_form_supplement
ALTER TABLE dbo.utb_form_supplement
	DROP CONSTRAINT [uck_form_supplement_servicechannelcd]

ALTER TABLE [dbo].[utb_form_supplement]  WITH NOCHECK 
	ADD  CONSTRAINT [uck_form_supplement_servicechannelcd] CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

ALTER TABLE [dbo].[utb_form_supplement] CHECK CONSTRAINT [uck_form_supplement_servicechannelcd]

IF EXISTS (SELECT * FROM ::FN_LISTEXTENDEDPROPERTY('Codes', 'user', 'dbo','table', 'utb_form_supplement', 'constraint', 'uck_form_supplement_servicechannelcd'))
    EXEC sp_dropextendedproperty 'Codes',
                                'user', dbo, 'table', utb_form_supplement, 'constraint', uck_form_supplement_servicechannelcd
EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral',
                            'user', dbo, 'table', utb_form_supplement, 'constraint', uck_form_supplement_servicechannelcd

