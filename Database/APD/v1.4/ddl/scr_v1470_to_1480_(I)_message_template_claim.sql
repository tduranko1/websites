    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_message_template
	    DROP CONSTRAINT ufk_message_template_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_message_template
	    (
	    MessageTemplateID udt_std_id NOT NULL IDENTITY (1, 1),
	    AppliesToCD udt_std_cd NOT NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    Description udt_std_desc_huge NOT NULL,
	    ServiceChannelCD udt_std_cd NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_reference

    SET IDENTITY_INSERT dbo.Tmp_utb_message_template ON

    IF EXISTS(SELECT * FROM dbo.utb_message_template)
	     EXEC('INSERT INTO dbo.Tmp_utb_message_template (MessageTemplateID, AppliesToCD, EnabledFlag, Description, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
		    SELECT MessageTemplateID, ''C'', EnabledFlag, Description, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_message_template WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_message_template OFF

    ALTER TABLE dbo.utb_message_template_macro
	    DROP CONSTRAINT ufk_message_template_macro_messagetemplateid_in_message_template

    ALTER TABLE dbo.utb_bundling
	    DROP CONSTRAINT ufk_bundling_messagetemplateid_in_message_template

    DROP TABLE dbo.utb_message_template

    EXECUTE sp_rename N'dbo.Tmp_utb_message_template', N'utb_message_template', 'OBJECT' 

    ALTER TABLE dbo.utb_message_template ADD CONSTRAINT
	    upk_message_template PRIMARY KEY CLUSTERED 
	    (
	    MessageTemplateID
	    ) WITH FILLFACTOR = 100 ON ufg_reference


    ALTER TABLE dbo.utb_message_template ADD CONSTRAINT
	    uck_message_template_appliestocd CHECK ((AppliesToCD = 'C' or AppliesToCD = 'N' or AppliesToCD = 'S'))

    ALTER TABLE dbo.utb_message_template ADD CONSTRAINT
	    uck_message_template_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA'))

    ALTER TABLE dbo.utb_message_template ADD CONSTRAINT
	    ufk_message_template_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_bundling ADD CONSTRAINT
	    ufk_bundling_messagetemplateid_in_message_template FOREIGN KEY
	    (
	    MessageTemplateID
	    ) REFERENCES dbo.utb_message_template
	    (
	    MessageTemplateID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_message_template_macro ADD CONSTRAINT
	    ufk_message_template_macro_messagetemplateid_in_message_template FOREIGN KEY
	    (
	    MessageTemplateID
	    ) REFERENCES dbo.utb_message_template
	    (
	    MessageTemplateID
	    )

    COMMIT
