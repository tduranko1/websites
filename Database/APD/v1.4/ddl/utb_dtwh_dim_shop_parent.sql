SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

--DROP TABLE dbo.utb_dtwh_dim_shop_parent

CREATE TABLE dbo.utb_dtwh_dim_shop_parent
   (ShopParentID bigint NULL,
	Address1 varchar(50) NULL,
	Address2 varchar(50) NULL,
	AddressCity varchar(30) NULL,
	AddressCounty varchar(30) NULL,
	AddressState varchar(2) NULL,
	AddressZip varchar(8) NULL,
	FaxNumber varchar(12) NULL,
	FaxNumberExtension varchar(5) NULL,
	LastUserID int NOT NULL,
	LastUpdatedByName varchar(50) NOT NULL,
	LastUpdatedDate datetime NOT NULL,
	PhoneNumber varchar(12) NULL,
	PhoneNumberExtension varchar(5) NULL,
	ShopParentName varchar(50) NOT NULL
) ON ufg_dtwh

GO