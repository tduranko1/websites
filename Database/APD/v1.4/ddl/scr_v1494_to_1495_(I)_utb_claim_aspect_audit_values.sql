

IF EXISTS (SELECT name,* FROM sysobjects 
            WHERE name = 'utb_claim_aspect_audit_values' AND type = 'U')
BEGIN
    DROP TABLE dbo.utb_claim_aspect_audit_values 
END



/****** Object:  Table [dbo].[utb_claim_aspect_audit_values]    Script Date: 08/25/2011 15:58:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[utb_claim_aspect_audit_values](
	[ClaimAspectID] [udt_std_id_big] NOT NULL,
	[AuditedOriginalGrossAmount] [udt_std_money] NULL,
	[AdditionalLineItemValues] [udt_std_money] NULL,
	[MissingLineItemValues] [udt_std_money] NULL,
	[LineItemValueCorrection] [udt_std_money] NULL,
	[SysLastUserID] [udt_std_id] NOT NULL,
	[SysLastUpdatedDate] [udt_sys_last_updated_date] NOT NULL,
 CONSTRAINT [upk_utb_claim_aspect_audit_values] PRIMARY KEY CLUSTERED 
(
	[ClaimAspectID] ASC
)
--WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
ON [ufg_claim]
) ON [ufg_claim]

GO


ALTER TABLE [dbo].[utb_claim_aspect_audit_values]  WITH CHECK ADD  CONSTRAINT [ufk_claim_aspect_audit_values_claimaspectid_in_claim_aspect] FOREIGN KEY([ClaimAspectID])
REFERENCES [dbo].[utb_claim_aspect] ([ClaimAspectID])
GO

ALTER TABLE [dbo].[utb_claim_aspect_audit_values] CHECK CONSTRAINT [ufk_claim_aspect_audit_values_claimaspectid_in_claim_aspect]
GO

ALTER TABLE [dbo].[utb_claim_aspect_audit_values]  WITH CHECK ADD  CONSTRAINT [ufk_claim_aspect_audit_values_syslastuserid_in_user] FOREIGN KEY([SysLastUserID])
REFERENCES [dbo].[utb_user] ([UserID])
GO

ALTER TABLE [dbo].[utb_claim_aspect_audit_values] CHECK CONSTRAINT [ufk_claim_aspect_audit_values_syslastuserid_in_user]
GO
