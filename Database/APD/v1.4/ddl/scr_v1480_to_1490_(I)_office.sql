--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_office'                     --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_office
	    DROP CONSTRAINT ufk_office_insurancecompanyid_in_insurance

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_office
	    (
	    OfficeID udt_std_id NOT NULL IDENTITY (1, 1),
	    InsuranceCompanyID udt_std_int_small NOT NULL,
	    Address1 udt_addr_line_1 NULL,
	    Address2 udt_addr_line_2 NULL,
	    AddressCity udt_addr_city NULL,
	    AddressState udt_addr_state NULL,
	    AddressZip udt_addr_zip_code NULL,
	    CCEmailAddress udt_web_email NULL,
	    ClaimNumberFormatJS udt_std_desc_mid NULL,
	    ClaimNumberValidJS udt_std_desc_mid NULL,
	    ClaimNumberMsgText udt_std_desc_long NULL,
	    ClientOfficeId udt_std_desc_short NOT NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    FaxAreaCode udt_ph_area_code NULL,
	    FaxExchangeNumber udt_ph_exchange_number NULL,
	    FaxExtensionNumber udt_ph_extension_number NULL,
	    FaxUnitNumber udt_ph_unit_number NULL,
	    MailingAddress1 udt_addr_line_1 NULL,
	    MailingAddress2 udt_addr_line_2 NULL,
	    MailingAddressCity udt_addr_city NULL,
	    MailingAddressState udt_addr_state NULL,
	    MailingAddressZip udt_addr_zip_code NULL,
	    Name udt_std_name NOT NULL,
	    PhoneAreaCode udt_ph_area_code NULL,
	    PhoneExchangeNumber udt_ph_exchange_number NULL,
	    PhoneExtensionNumber udt_ph_extension_number NULL,
	    PhoneUnitNumber udt_ph_unit_number NULL,
	    ReturnDocDestinationValue udt_std_desc_mid NULL,
	    ReturnDocEmailAddress udt_web_email NULL,
	    ReturnDocFaxAreaCode udt_ph_area_code NULL,
	    ReturnDocFaxExchangeNumber udt_ph_exchange_number NULL,
	    ReturnDocFaxExtensionNumber udt_ph_extension_number NULL,
	    ReturnDocFaxUnitNumber udt_ph_unit_number NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_client

    SET IDENTITY_INSERT dbo.Tmp_utb_office ON

    IF EXISTS(SELECT * FROM dbo.utb_office)
	     EXEC('INSERT INTO dbo.Tmp_utb_office (OfficeID, InsuranceCompanyID, Address1, Address2, AddressCity, AddressState, AddressZip, CCEmailAddress, ClaimNumberFormatJS, ClaimNumberValidJS, ClaimNumberMsgText, ClientOfficeId, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, MailingAddress1, MailingAddress2, MailingAddressCity, MailingAddressState, MailingAddressZip, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, ReturnDocEmailAddress, ReturnDocFaxAreaCode, ReturnDocFaxExchangeNumber, ReturnDocFaxExtensionNumber, ReturnDocFaxUnitNumber, SysLastUserID, SysLastUpdatedDate)
		    SELECT OfficeID, InsuranceCompanyID, Address1, Address2, AddressCity, AddressState, AddressZip, CCEmailAddress, ClaimNumberFormatJS, ClaimNumberValidJS, ClaimNumberMsgText, ClientOfficeId, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, MailingAddress1, MailingAddress2, MailingAddressCity, MailingAddressState, MailingAddressZip, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, ReturnDocEmailAddress, ReturnDocFaxAreaCode, ReturnDocFaxExchangeNumber, ReturnDocFaxExtensionNumber, ReturnDocFaxUnitNumber, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_office WITH (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_office OFF

    ALTER TABLE dbo.utb_office_assignment_type
	    DROP CONSTRAINT ufk_office_assignment_type_officeid_in_office

    ALTER TABLE dbo.utb_user
	    DROP CONSTRAINT ufk_user_officeid_in_office

    ALTER TABLE dbo.utb_office
	    DROP CONSTRAINT ufk_office_syslastuserid_in_user

    ALTER TABLE dbo.utb_client_report
	    DROP CONSTRAINT ufk_client_report_officeid_in_office

    ALTER TABLE dbo.utb_office_contract_state
	    DROP CONSTRAINT ufk_office_contract_state_officeid_in_office

    ALTER TABLE dbo.utb_client_bundling
	    DROP CONSTRAINT ufk_client_bundling_officeid_in_office

    DROP TABLE dbo.utb_office

    EXECUTE sp_rename N'dbo.Tmp_utb_office', N'utb_office', 'OBJECT' 

    ALTER TABLE dbo.utb_office ADD CONSTRAINT
	    upk_office PRIMARY KEY CLUSTERED 
	    (
	    OfficeID
	    ) WITH FILLFACTOR = 90 ON ufg_client


    CREATE NONCLUSTERED INDEX uix_ie_office_insurancecompanyid ON dbo.utb_office
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_client_index

    ALTER TABLE dbo.utb_office ADD CONSTRAINT
	    ufk_office_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_office_contract_state ADD CONSTRAINT
	    ufk_office_contract_state_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_client_report ADD CONSTRAINT
	    ufk_client_report_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_user ADD CONSTRAINT
	    ufk_user_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )

    ALTER TABLE dbo.utb_office ADD CONSTRAINT
	    ufk_office_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_office_assignment_type ADD CONSTRAINT
	    ufk_office_assignment_type_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

