--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_fnol_claim_vehicle_load'      --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_fnol_claim_vehicle_load
	    (
	    LynxID udt_std_int_big NOT NULL,
	    VehicleNumber udt_std_id NOT NULL,
	    AirBagDriverFront udt_std_flag NULL,
	    AirBagDriverSide udt_std_flag NULL,
	    AirBagHeadliner udt_std_flag NULL,
	    AirBagPassengerFront udt_std_flag NULL,
	    AirBagPassengerSide udt_std_flag NULL,
	    AssignmentTypeID udt_std_id NULL,
	    BodyStyle udt_auto_body NULL,
	    ClientCoverageTypeID udt_std_int NULL,
	    Color udt_auto_color NULL,
	    ContactAddress1 udt_addr_line_1 NULL,
	    ContactAddress2 udt_addr_line_2 NULL,
	    ContactAddressCity udt_addr_city NULL,
	    ContactAddressState udt_addr_state NULL,
	    ContactAddressZip udt_addr_zip_code NULL,
	    ContactAltPhone varchar(15) NULL,
	    ContactAltPhoneExt udt_ph_extension_number NULL,
	    ContactBestPhoneCD udt_std_cd NULL,
	    ContactNameFirst udt_per_name NULL,
	    ContactNameLast udt_per_name NULL,
	    ContactNameTitle udt_per_title NULL,
	    ContactNightPhone varchar(15) NULL,
	    ContactNightPhoneExt udt_ph_extension_number NULL,
	    ContactPhone varchar(15) NULL,
	    ContactPhoneExt udt_ph_extension_number NULL,
	    CoverageProfileCD udt_std_cd NULL,
	    Drivable udt_std_flag NULL,
	    ExposureCD udt_std_cd NULL,
	    GlassDamageFlag udt_std_flag NULL,
	    GlassDispatchedFlag udt_std_flag NULL,
	    GlassDispatchNumber udt_std_desc_short NULL,
	    GlassNotDispatchedReason udt_std_desc_long NULL,
	    GlassReferenceNumber udt_std_desc_short NULL,
	    GlassShopAppointmentDate udt_std_datetime NULL,
	    GlassShopName udt_std_name NULL,
	    GlassShopPhoneNumber udt_std_desc_short NULL,
	    ImpactLocations varchar(100) NULL,
	    ImpactSpeed varchar(10) NULL,
	    LicensePlateNumber udt_auto_plate_number NULL,
	    LicensePlateState udt_addr_state NULL,
	    LocationAddress1 udt_addr_line_1 NULL,
	    LocationAddress2 udt_addr_line_2 NULL,
	    LocationCity udt_addr_city NULL,
	    LocationName udt_std_name NULL,
	    LocationPhone varchar(15) NULL,
	    LocationState udt_addr_state NULL,
	    LocationZip udt_addr_zip_code NULL,
	    Make udt_auto_make NULL,
	    Mileage varchar(10) NULL,
	    Model udt_auto_model NULL,
	    NADAId udt_auto_nada_id NULL,
	    PermissionToDrive udt_std_cd NULL,
	    PhysicalDamageFlag udt_std_flag NULL,
	    PostedSpeed varchar(10) NULL,
	    PriorDamage varchar(100) NULL,
	    PriorityFlag udt_std_flag NULL,
	    Remarks udt_std_desc_xlong NULL,
	    RentalDaysAuthorized udt_dt_day NULL,
	    RentalInstructions udt_std_desc_mid NULL,
	    RepairLocationCity udt_addr_city NULL,
	    RepairLocationCounty udt_addr_county NULL,
	    RepairLocationState udt_addr_state NULL,
	    SelectedShopRank udt_std_int_tiny NULL,
	    SelectedShopScore udt_std_int NULL,
	    ShopLocationID udt_std_id_big NULL,
	    ShopRemarks udt_std_desc_xlong NULL,
	    ShopSearchLogID udt_std_id_big NULL,
	    VehicleYear varchar(5) NULL,
	    Vin udt_auto_vin NULL
	    )  ON ufg_load

    IF EXISTS(SELECT * FROM dbo.utb_fnol_claim_vehicle_load)
	     EXEC('INSERT INTO dbo.Tmp_utb_fnol_claim_vehicle_load (LynxID, VehicleNumber, AirBagDriverFront, AirBagDriverSide, AirBagHeadliner, AirBagPassengerFront, AirBagPassengerSide, AssignmentTypeID, BodyStyle, ClientCoverageTypeID, Color, ContactAddress1, ContactAddress2, ContactAddressCity, ContactAddressState, ContactAddressZip, ContactAltPhone, ContactAltPhoneExt, ContactBestPhoneCD, ContactNameFirst, ContactNameLast, ContactNameTitle, ContactNightPhone, ContactNightPhoneExt, ContactPhone, ContactPhoneExt, CoverageProfileCD, Drivable, ExposureCD, GlassDamageFlag, GlassDispatchedFlag, GlassDispatchNumber, GlassNotDispatchedReason, GlassReferenceNumber, GlassShopAppointmentDate, GlassShopName, GlassShopPhoneNumber, ImpactLocations, ImpactSpeed, LicensePlateNumber, LicensePlateState, LocationAddress1, LocationAddress2, LocationCity, LocationName, LocationPhone, LocationState, LocationZip, Make, Mileage, Model, NADAId, PermissionToDrive, PhysicalDamageFlag, PostedSpeed, PriorDamage, PriorityFlag, Remarks, RentalDaysAuthorized, RentalInstructions, SelectedShopRank, SelectedShopScore, ShopLocationID, ShopRemarks, ShopSearchLogID, VehicleYear, Vin)
		    SELECT LynxID, VehicleNumber, AirBagDriverFront, AirBagDriverSide, AirBagHeadliner, AirBagPassengerFront, AirBagPassengerSide, AssignmentTypeID, BodyStyle, ClientCoverageTypeID, Color, ContactAddress1, ContactAddress2, ContactAddressCity, ContactAddressState, ContactAddressZip, ContactAltPhone, ContactAltPhoneExt, ContactBestPhoneCD, ContactNameFirst, ContactNameLast, ContactNameTitle, ContactNightPhone, ContactNightPhoneExt, ContactPhone, ContactPhoneExt, CoverageProfileCD, Drivable, ExposureCD, GlassDamageFlag, GlassDispatchedFlag, GlassDispatchNumber, GlassNotDispatchedReason, GlassReferenceNumber, GlassShopAppointmentDate, GlassShopName, GlassShopPhoneNumber, ImpactLocations, ImpactSpeed, LicensePlateNumber, LicensePlateState, LocationAddress1, LocationAddress2, LocationCity, LocationName, LocationPhone, LocationState, LocationZip, Make, Mileage, Model, NADAId, PermissionToDrive, PhysicalDamageFlag, PostedSpeed, PriorDamage, PriorityFlag, Remarks, RentalDaysAuthorized, RentalInstructions, SelectedShopRank, SelectedShopScore, ShopLocationID, ShopRemarks, ShopSearchLogID, VehicleYear, Vin FROM dbo.utb_fnol_claim_vehicle_load WITH (HOLDLOCK TABLOCKX)')

    DROP TABLE dbo.utb_fnol_claim_vehicle_load

    EXECUTE sp_rename N'dbo.Tmp_utb_fnol_claim_vehicle_load', N'utb_fnol_claim_vehicle_load', 'OBJECT' 

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

