--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_task_completion_criteria'     --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                              !!!!
--!!!!!             SpawningServiceChannelCD
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_task_completion_criteria
	    DROP CONSTRAINT ufk_task_completion_criteria_syslastuserid_in_user
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_task_completion_criteria
	    DROP CONSTRAINT ufk_task_completion_criteria_taskid_in_task
    
    COMMIT
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_task_completion_criteria
	    (
	    TaskCompletionCriteriaID udt_std_int NOT NULL,
	    TaskID udt_std_int_small NOT NULL,
	    CriteriaSQL udt_std_desc_long NOT NULL,
	    FailureMessage udt_std_desc_long NOT NULL,
	    ValidResultSetSQL udt_std_desc_long NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow
    
    IF EXISTS(SELECT * FROM dbo.utb_task_completion_criteria)
	     EXEC('INSERT INTO dbo.Tmp_utb_task_completion_criteria (TaskCompletionCriteriaID, TaskID, CriteriaSQL, FailureMessage, ValidResultSetSQL, SysLastUserID, SysLastUpdatedDate)
		    SELECT TaskCompletionCriteriaID, TaskID, CriteriaSQL, FailureMessage, ValidResultSetSQL, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_task_completion_criteria WITH (HOLDLOCK TABLOCKX)')
    
    DROP TABLE dbo.utb_task_completion_criteria
    
    EXECUTE sp_rename N'dbo.Tmp_utb_task_completion_criteria', N'utb_task_completion_criteria', 'OBJECT' 
    
    ALTER TABLE dbo.utb_task_completion_criteria ADD CONSTRAINT
	    upk_task_completion_criteria PRIMARY KEY CLUSTERED 
	    (
	    TaskCompletionCriteriaID
	    ) WITH FILLFACTOR = 80 ON ufg_workflow

    
    CREATE NONCLUSTERED INDEX uix_ie_task_completion_criteria_taskid ON dbo.utb_task_completion_criteria
	    (
	    TaskID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    ALTER TABLE dbo.utb_task_completion_criteria ADD CONSTRAINT
	    ufk_task_completion_criteria_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )
    
    ALTER TABLE dbo.utb_task_completion_criteria ADD CONSTRAINT
	    ufk_task_completion_criteria_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    COMMIT


    -- Capture any error

    SELECT @error = @@ERROR
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

