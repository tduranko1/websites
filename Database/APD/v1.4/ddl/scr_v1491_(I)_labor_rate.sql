--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_labor_rate'         --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    CREATE TABLE dbo.utb_labor_rate 
    (
        LaborRateID                    udt_std_id_big     IDENTITY     NOT NULL,
        InsuranceCompanyID             udt_std_int_small               NOT NULL,
        City                           udt_addr_city                       NULL,
        County                         udt_addr_county                     NULL,
	    StateCode                      udt_addr_state                      NULL,
        AgreedPriceVariance            udt_std_int_tiny                    NULL,
        ActivityCD                     udt_std_flag                    NOT NULL,
        AuthorizedBy                   udt_std_id                          NULL,
        AuthorizationDate              udt_std_datetime                    NULL,
        BodyRateMin                    udt_std_money                       NULL,
        BodyRateMax                    udt_std_money                       NULL,
        FrameRateMin                   udt_std_money                       NULL,
        FrameRateMax                   udt_std_money                       NULL,
        MaterialRateMin                udt_std_money                       NULL,
        MaterialRateMax                udt_std_money                       NULL,
        MechRateMin                    udt_std_money                       NULL,
        MechRateMax                    udt_std_money                       NULL,
        RefinishRateMin                udt_std_money                       NULL,
        RefinishRateMax                udt_std_money                       NULL,
        LaborTax                       udt_std_money                       NULL,
        PartsTax                       udt_std_money                       NULL,
        MaterialsTax                   udt_std_money                       NULL,
        SysLastUserID                  udt_std_id                      NOT NULL,
        SysLastUpdatedDate             udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_labor_rate
        PRIMARY KEY CLUSTERED (LaborRateID) WITH FILLFACTOR=75
                                                                              ON ufg_shop
    )
    ON ufg_shop
     
    ALTER TABLE dbo.utb_labor_rate ADD CONSTRAINT
        ufk_labor_rate_insurancecompanyid_in_insurance FOREIGN KEY
        (
        InsuranceCompanyID
        ) REFERENCES dbo.utb_insurance
        (
        InsuranceCompanyID
        )
        
    ALTER TABLE dbo.utb_labor_rate ADD CONSTRAINT
        ufk_labor_rate_syslastuserid_in_user FOREIGN KEY
        (
        SysLastUserID
        ) REFERENCES dbo.utb_user
        (
        UserID
        )
        
    CREATE NONCLUSTERED INDEX uix_ie_lobor_rate_insurancecompanyid ON dbo.utb_labor_rate
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index


    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

