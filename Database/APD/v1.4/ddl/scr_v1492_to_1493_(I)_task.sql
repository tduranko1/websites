--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_task'       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task
	    DROP CONSTRAINT ufk_task_notetypedefaultid_in_note_type

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task
	    DROP CONSTRAINT ufk_task_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_task
	    (
	    TaskID udt_std_int_small NOT NULL,
	    NoteTypeDefaultID udt_std_int_tiny NULL,
	    CommentCompleteRequiredFlag udt_std_flag NOT NULL,
	    CommentNARequiredFlag udt_std_flag NOT NULL,
	    CRDSortOrder udt_std_int NULL,
	    DisplayOrder udt_std_int_tiny NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    EscalationMinutes udt_std_int_small NULL,
	    Name udt_std_name NOT NULL,
	    UserTaskFlag udt_std_flag NOT NULL,
	    SysMaintainedFlag udt_sys_maintained_flag NOT NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_reference

    IF EXISTS(SELECT * FROM dbo.utb_task)
	     EXEC('INSERT INTO dbo.Tmp_utb_task (TaskID, NoteTypeDefaultID, CommentCompleteRequiredFlag, CommentNARequiredFlag, DisplayOrder, EnabledFlag, EscalationMinutes, Name, UserTaskFlag, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate)
		    SELECT TaskID, NoteTypeDefaultID, CommentCompleteRequiredFlag, CommentNARequiredFlag, DisplayOrder, EnabledFlag, EscalationMinutes, Name, UserTaskFlag, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_task WITH (HOLDLOCK TABLOCKX)')

    ALTER TABLE dbo.utb_status_task
	    DROP CONSTRAINT ufk_status_task_taskid_in_task

    ALTER TABLE dbo.utb_task_assignment_pool
	    DROP CONSTRAINT ufk_task_assignment_pool_taskid_in_task

    ALTER TABLE dbo.utb_task_completion_criteria
	    DROP CONSTRAINT ufk_task_completion_criteria_taskid_in_task

    ALTER TABLE dbo.utb_task_delay_reason
	    DROP CONSTRAINT ufk_task_delay_reason_taskid_in_task

    ALTER TABLE dbo.utb_task_role
	    DROP CONSTRAINT ufk_task_role_taskid_in_task

    ALTER TABLE dbo.utb_task_escalation
	    DROP CONSTRAINT ufk_task_escalation_taskid_in_task

    DROP TABLE dbo.utb_task

    EXECUTE sp_rename N'dbo.Tmp_utb_task', N'utb_task', 'OBJECT' 

    ALTER TABLE dbo.utb_task ADD CONSTRAINT
	    upk_task PRIMARY KEY CLUSTERED 
	    (
	    TaskID
	    ) WITH FILLFACTOR = 100 ON ufg_reference


    CREATE UNIQUE NONCLUSTERED INDEX uix_ak_task_name ON dbo.utb_task
	    (
	    Name
	    ) WITH FILLFACTOR = 100 ON ufg_reference_index

    CREATE NONCLUSTERED INDEX uix_ie_task_notetypedefaultid ON dbo.utb_task
	    (
	    NoteTypeDefaultID
	    ) WITH FILLFACTOR = 75 ON ufg_reference_index

    ALTER TABLE dbo.utb_task ADD CONSTRAINT
	    ufk_task_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_task ADD CONSTRAINT
	    ufk_task_notetypedefaultid_in_note_type FOREIGN KEY
	    (
	    NoteTypeDefaultID
	    ) REFERENCES dbo.utb_note_type
	    (
	    NoteTypeID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_escalation ADD CONSTRAINT
	    ufk_task_escalation_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_role ADD CONSTRAINT
	    ufk_task_role_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_delay_reason ADD CONSTRAINT
	    ufk_task_delay_reason_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_completion_criteria ADD CONSTRAINT
	    ufk_task_completion_criteria_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_task_assignment_pool ADD CONSTRAINT
	    ufk_task_assignment_pool_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_status_task ADD CONSTRAINT
	    ufk_status_task_taskid_in_task FOREIGN KEY
	    (
	    TaskID
	    ) REFERENCES dbo.utb_task
	    (
	    TaskID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

