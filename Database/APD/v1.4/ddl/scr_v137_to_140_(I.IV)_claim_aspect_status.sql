--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_aspect_status'          --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
     CREATE TABLE dbo.utb_claim_aspect_status(
        ClaimAspectStatusID              udt_std_id_big               IDENTITY(1,1),
        ClaimAspectID                    udt_std_id_big               NOT NULL,
        StatusID                         udt_std_int_small            NULL,
        ServiceChannelCD                 udt_std_cd                   NULL,
        StatusStartDate                  udt_std_datetime             NULL,
        StatusTypeCD                     udt_std_cd                   NULL,
        SysLastUserID                    udt_std_id                   NOT NULL,
        SysLastUpdatedDate               udt_sys_last_updated_date    NOT NULL,
        CONSTRAINT upk_claim_aspect_status PRIMARY KEY CLUSTERED (ClaimAspectStatusID)
        WITH FILLFACTOR = 90
        ON ufg_claim, 
        CONSTRAINT ufk_claim_aspect_status_claimaspectid_in_claim_aspect FOREIGN KEY (ClaimAspectID)
        REFERENCES dbo.utb_claim_aspect(ClaimAspectID),
        CONSTRAINT ufk_claim_aspect_status_statusid_in_status FOREIGN KEY (StatusID)
        REFERENCES dbo.utb_status(StatusID),
        CONSTRAINT ufk_claim_aspect_status_syslastuserid_in_user FOREIGN KEY (SysLastUserID)
        REFERENCES dbo.utb_user(UserID)
    ) ON ufg_claim


    CREATE INDEX uix_ie_claim_aspect_status_claimaspectid ON dbo.utb_claim_aspect_status(ClaimAspectID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    CREATE INDEX uix_ie_claim_aspect_status_statusid ON dbo.utb_claim_aspect_status(StatusID)
    WITH FILLFACTOR = 90
    ON ufg_claim_index    

    ALTER TABLE dbo.utb_claim_aspect_status 
        ADD CONSTRAINT uck_claim_aspect_status_servicechannelcd
        CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA')

    ALTER TABLE dbo.utb_claim_aspect_status 
        ADD CONSTRAINT uck_claim_aspect_status_statustypecd
        CHECK ([StatusTypeCD] = 'ELC' or [StatusTypeCD] = 'FAX' or [StatusTypeCD] = 'SC')


    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

