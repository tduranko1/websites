--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_aspect_service_channel' --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_vehicle
	    DROP CONSTRAINT ufk_claim_vehicle_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_vehicle
	    DROP CONSTRAINT ufk_claim_vehicle_contactinvolvedid_in_involved

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_claim_vehicle
	    DROP CONSTRAINT ufk_claim_vehicle_claimaspectid_in_claim_aspect

    COMMIT
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_claim_vehicle
	    (
	    ClaimAspectID udt_std_id_big NOT NULL,
	    ContactInvolvedID udt_std_id_big NULL,
	    BodyStyle udt_auto_body NULL,
	    BookValueAmt udt_std_money NULL,
	    Color udt_auto_color NULL,
	    DriveableFlag udt_std_flag NOT NULL,
	    EstimateVIN udt_auto_vin NULL,
	    ImpactSpeed udt_auto_speed NULL,
	    LicensePlateNumber udt_auto_plate_number NULL,
	    LicensePlateState udt_addr_state NULL,
	    LocationAddress1 udt_addr_line_1 NULL,
	    LocationAddress2 udt_addr_line_2 NULL,
	    LocationAreaCode udt_ph_area_code NULL,
	    LocationCity udt_addr_city NULL,
	    LocationExchangeNumber udt_ph_exchange_number NULL,
	    LocationExtensionNumber udt_ph_extension_number NULL,
	    LocationName udt_std_name NULL,
	    LocationState udt_addr_state NULL,
	    LocationUnitNumber udt_ph_unit_number NULL,
	    LocationZip udt_addr_zip_code NULL,
	    Make udt_auto_make NULL,
	    Mileage udt_auto_mileage NULL,
	    Model udt_auto_model NULL,
	    NADAId udt_auto_nada_id NULL,
	    PermissionToDriveCD udt_std_cd NULL,
	    PostedSpeed udt_auto_speed NULL,
	    Remarks udt_std_desc_xlong NULL,
	    RentalDaysAuthorized udt_dt_day NULL,
	    RentalEndDate udt_std_datetime NULL,
	    RentalInstructions udt_std_desc_mid NULL,
	    RentalStartDate udt_std_datetime NULL,
	    RentalStartFlag udt_std_flag NOT NULL,
	    TitleName udt_std_desc_mid NULL,
	    TitleNameConfirmDate udt_std_datetime NULL,
	    TitleNameConfirmFlag udt_std_flag NOT NULL,
	    TitleState udt_addr_state NULL,
	    TitleStatus udt_std_name NULL,
	    VehicleYear udt_dt_year NULL,
	    VIN udt_auto_vin NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_claim

    IF EXISTS(SELECT * FROM dbo.utb_claim_vehicle)
	     EXEC('INSERT INTO dbo.Tmp_utb_claim_vehicle (ClaimAspectID, ContactInvolvedID, BodyStyle, BookValueAmt, Color, DriveableFlag, EstimateVIN, ImpactSpeed, LicensePlateNumber, LicensePlateState, LocationAddress1, LocationAddress2, LocationAreaCode, LocationCity, LocationExchangeNumber, LocationExtensionNumber, LocationName, LocationState, LocationUnitNumber, LocationZip, Make, Mileage, Model, NADAId, PermissionToDriveCD, PostedSpeed, Remarks, RentalDaysAuthorized, RentalEndDate, RentalInstructions, RentalStartDate, RentalStartFlag, VehicleYear, VIN, SysLastUserID, SysLastUpdatedDate)
		    SELECT ClaimAspectID, ContactInvolvedID, BodyStyle, BookValueAmt, Color, DriveableFlag, EstimateVIN, ImpactSpeed, LicensePlateNumber, LicensePlateState, LocationAddress1, LocationAddress2, LocationAreaCode, LocationCity, LocationExchangeNumber, LocationExtensionNumber, LocationName, LocationState, LocationUnitNumber, LocationZip, Make, Mileage, Model, NADAId, PermissionToDriveCD, PostedSpeed, Remarks, RentalDaysAuthorized, RentalEndDate, RentalInstructions, RentalStartDate, RentalStartFlag, VehicleYear, VIN, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_claim_vehicle WITH (HOLDLOCK TABLOCKX)')

    ALTER TABLE dbo.utb_vehicle_impact
	    DROP CONSTRAINT ufk_vehicle_impact_claimaspectid_in_claim_vehicle

    ALTER TABLE dbo.utb_reinspect
	    DROP CONSTRAINT ufk_reinspect_x_in_claim_vehicle

    ALTER TABLE dbo.utb_vehicle_safety_device
	    DROP CONSTRAINT ufk_vehicle_safety_device_claimaspectid_in_claim_vehicle

    DROP TABLE dbo.utb_claim_vehicle

    EXECUTE sp_rename N'dbo.Tmp_utb_claim_vehicle', N'utb_claim_vehicle', 'OBJECT' 

    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    upk_claim_vehicle PRIMARY KEY CLUSTERED 
	    (
	    ClaimAspectID
	    ) WITH FILLFACTOR = 90 ON ufg_claim


    CREATE NONCLUSTERED INDEX uix_ie_claim_vehicle_contactinvolvedid ON dbo.utb_claim_vehicle
	    (
	    ContactInvolvedID
	    ) WITH FILLFACTOR = 90 ON ufg_claim_index

    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    uck_claim_vehicle_permissiontodrivecd CHECK (([PermissionToDriveCD] = 'Y' or [PermissionToDriveCD] = 'N' or [PermissionToDriveCD] = 'U'))

    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    ufk_claim_vehicle_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    ufk_claim_vehicle_contactinvolvedid_in_involved FOREIGN KEY
	    (
	    ContactInvolvedID
	    ) REFERENCES dbo.utb_involved
	    (
	    InvolvedID
	    )

    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    ufk_claim_vehicle_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_vehicle_safety_device ADD CONSTRAINT
	    ufk_vehicle_safety_device_claimaspectid_in_claim_vehicle FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_vehicle
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_reinspect ADD CONSTRAINT
	    ufk_reinspect_x_in_claim_vehicle FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_vehicle
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION

    ALTER TABLE dbo.utb_vehicle_impact ADD CONSTRAINT
	    ufk_vehicle_impact_claimaspectid_in_claim_vehicle FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_vehicle
	    (
	    ClaimAspectID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

