--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_shop_location'                --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_shopid_in_shop
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_preferredestimatepackageid_in_estimate_package
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_billingid_in_billing
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_datareviewuserid_in_user
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_mergeuserid_in_user
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_programmanageruserid_in_user
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_syslastuserid_in_user
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location
	    DROP CONSTRAINT ufk_shop_location_preferredcommunicationmethodid_in_communication_method
    
    COMMIT
    BEGIN TRANSACTION
    
    CREATE TABLE dbo.Tmp_utb_shop_location
	    (
	    ShopLocationID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    BillingID udt_std_id_big NOT NULL,
	    DataReviewUserID udt_std_id NULL,
	    MergeUserID udt_std_id NULL,
	    PreferredCommunicationMethodID udt_std_int_tiny NULL,
	    PreferredEstimatePackageID udt_std_int_tiny NULL,
	    ProgramManagerUserID udt_std_id NULL,
	    ShopID udt_std_id_big NOT NULL,
	    Address1 udt_addr_line_1 NULL,
	    Address2 udt_addr_line_2 NULL,
	    AddressCity udt_addr_city NULL,
	    AddressCounty udt_addr_county NULL,
	    AddressState udt_addr_state NULL,
	    AddressZip udt_addr_zip_code NULL,
	    AutoVerseId udt_std_desc_mid NULL,
	    AvailableForSelectionFlag udt_std_flag NOT NULL,
	    CEIProgramFlag udt_std_flag NOT NULL,
	    CEIProgramId udt_std_desc_short NULL,
	    CertifiedFirstFlag udt_std_flag NOT NULL,
	    CertifiedFirstId udt_std_desc_short NULL,
	    DataReviewDate udt_std_datetime NULL,
	    DataReviewStatusCD udt_std_cd NOT NULL,
	    DMVFacilityNumber udt_std_desc_short NULL,
	    DrivingDirections udt_std_desc_long NULL,
	    EmailAddress udt_web_email NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    FaxAreaCode udt_ph_area_code NULL,
	    FaxExchangeNumber udt_ph_exchange_number NULL,
	    FaxExtensionNumber udt_ph_extension_number NULL,
	    FaxUnitNumber udt_ph_unit_number NULL,
	    LastAssignmentDate udt_std_datetime NULL,
	    LastReinspectionDate udt_std_datetime NULL,
	    Latitude udt_addr_latitude NULL,
	    Longitude udt_addr_longitude NULL,
	    MaxWeeklyAssignments udt_std_int_tiny NULL,
	    MergeDate udt_std_datetime NULL,
	    Name udt_std_name NOT NULL,
	    PhoneAreaCode udt_ph_area_code NULL,
	    PhoneExchangeNumber udt_ph_exchange_number NULL,
	    PhoneExtensionNumber udt_ph_extension_number NULL,
	    PhoneUnitNumber udt_ph_unit_number NULL,
	    PPGCTSCustomerFlag udt_std_flag NOT NULL,
	    PPGCTSFlag udt_std_flag NOT NULL,
	    PPGCTSId udt_std_desc_short NULL,
	    PPGCTSLevelCD udt_std_cd NULL,
	    PPGRegion udt_std_cd NULL,
	    PPGTerritory udt_std_cd NULL,
	    PPGZone udt_std_cd NULL,
	    PreferredCommunicationAddress udt_web_address NULL,
	    ProgramFlag udt_std_flag NOT NULL,
	    ProgramScore udt_std_int_tiny NULL,
	    RegistrationExpDate udt_std_datetime NULL,
	    RegistrationNumber udt_std_desc_short NULL,
	    SalesTaxNumber udt_std_desc_short NULL,
	    WarrantyPeriodRefinishCD udt_std_cd NULL,
	    WarrantyPeriodWorkmanshipCD udt_std_cd NULL,
	    WebSiteAddress udt_web_address NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_shop
    
    SET IDENTITY_INSERT dbo.Tmp_utb_shop_location ON
    
    IF EXISTS(SELECT * FROM dbo.utb_shop_location)
	     EXEC('INSERT INTO dbo.Tmp_utb_shop_location (ShopLocationID, BillingID, DataReviewUserID, MergeUserID, PreferredCommunicationMethodID, PreferredEstimatePackageID, ProgramManagerUserID, ShopID, Address1, Address2, AddressCity, AddressCounty, AddressState, AddressZip, AutoVerseId, AvailableForSelectionFlag, CEIProgramFlag, CEIProgramId, CertifiedFirstFlag, CertifiedFirstId, DataReviewDate, DataReviewStatusCD, DrivingDirections, EmailAddress, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, LastAssignmentDate, LastReinspectionDate, Latitude, Longitude, MaxWeeklyAssignments, MergeDate, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, PPGCTSCustomerFlag, PPGCTSFlag, PPGCTSId, PPGCTSLevelCD, PPGRegion, PPGTerritory, PPGZone, PreferredCommunicationAddress, ProgramFlag, ProgramScore, RegistrationExpDate, RegistrationNumber, WarrantyPeriodRefinishCD, WarrantyPeriodWorkmanshipCD, WebSiteAddress, SysLastUserID, SysLastUpdatedDate)
		    SELECT ShopLocationID, BillingID, DataReviewUserID, MergeUserID, PreferredCommunicationMethodID, PreferredEstimatePackageID, ProgramManagerUserID, ShopID, Address1, Address2, AddressCity, AddressCounty, AddressState, AddressZip, AutoVerseId, AvailableForSelectionFlag, CEIProgramFlag, CEIProgramId, CertifiedFirstFlag, CertifiedFirstId, DataReviewDate, DataReviewStatusCD, DrivingDirections, EmailAddress, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, LastAssignmentDate, LastReinspectionDate, Latitude, Longitude, MaxWeeklyAssignments, MergeDate, Name, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, PPGCTSCustomerFlag, PPGCTSFlag, PPGCTSId, PPGCTSLevelCD, PPGRegion, PPGTerritory, PPGZone, PreferredCommunicationAddress, ProgramFlag, ProgramScore, RegistrationExpDate, RegistrationNumber, WarrantyPeriodRefinishCD, WarrantyPeriodWorkmanshipCD, WebSiteAddress, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_shop_location WITH (HOLDLOCK TABLOCKX)')
    
    SET IDENTITY_INSERT dbo.Tmp_utb_shop_location OFF
    
    ALTER TABLE dbo.utb_client_shop_location
	    DROP CONSTRAINT ufk_client_shop_location_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_survey
	    DROP CONSTRAINT ufk_shop_location_survey_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_ingres
	    DROP CONSTRAINT ufk_shop_ingres_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_specialty
	    DROP CONSTRAINT ufk_shop_location_specialty_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_personnel
	    DROP CONSTRAINT ufk_shop_location_personnel_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_oem_discount
	    DROP CONSTRAINT ufk_shop_location_oem_discount_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_hours
	    DROP CONSTRAINT ufk_shop_location_hours_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_pricing_history
	    DROP CONSTRAINT ufk_shop_location_pricing_history_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_pricing
	    DROP CONSTRAINT ufk_shop_location_pricing_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_reinspect
	    DROP CONSTRAINT ufk_reinspect_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_document
	    DROP CONSTRAINT ufk_shop_location_document_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_load
	    DROP CONSTRAINT ufk_shop_load_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_search_results_log
	    DROP CONSTRAINT ufk_shop_search_results_log_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_assignment
	    DROP CONSTRAINT ufk_assignment_shoplocationid_in_shop_location
    
    ALTER TABLE dbo.utb_shop_location_program_delta
	    DROP CONSTRAINT ufk_shop_location_program_delta_shoplocationid_in_shop_location
    
    DROP TABLE dbo.utb_shop_location
    
    EXECUTE sp_rename N'dbo.Tmp_utb_shop_location', N'utb_shop_location', 'OBJECT' 
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    upk_shop_location PRIMARY KEY CLUSTERED 
	    (
	    ShopLocationID
	    ) WITH FILLFACTOR = 90 ON ufg_shop

    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_shopid ON dbo.utb_shop_location
	    (
	    ShopID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_lastassignmentdate ON dbo.utb_shop_location
	    (
	    LastAssignmentDate
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_name ON dbo.utb_shop_location
	    (
	    Name
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_phone ON dbo.utb_shop_location
	    (
	    PhoneAreaCode,
	    PhoneExchangeNumber,
	    PhoneUnitNumber,
	    PhoneExtensionNumber
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_datareviewuserid ON dbo.utb_shop_location
	    (
	    DataReviewUserID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_mergeuserid ON dbo.utb_shop_location
	    (
	    MergeUserID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_programmanageruserid ON dbo.utb_shop_location
	    (
	    ProgramManagerUserID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_billingid ON dbo.utb_shop_location
	    (
	    BillingID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_preferredcommunicationmethodid ON dbo.utb_shop_location
	    (
	    PreferredCommunicationMethodID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_preferredestimatepackageid ON dbo.utb_shop_location
	    (
	    PreferredEstimatePackageID
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_addresszip ON dbo.utb_shop_location
	    (
	    AddressZip
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    CREATE NONCLUSTERED INDEX uix_ie_shop_location_fax ON dbo.utb_shop_location
	    (
	    FaxAreaCode,
	    FaxExchangeNumber,
	    FaxUnitNumber,
	    FaxExtensionNumber
	    ) WITH FILLFACTOR = 90 ON ufg_shop_index
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_datareviewdate CHECK (([DataReviewDate] is not null and [DataReviewStatusCD] <> 'N' or [DataReviewDate] is null))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_datareviewstatuscd CHECK (([DataReviewStatusCD] = 'A' or [DataReviewStatusCD] = 'I' or [DataReviewStatusCD] = 'N'))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_datareviewuserid CHECK (([DataReviewUserID] is not null and [DataReviewStatusCD] <> 'N' or [DataReviewUserID] is null))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_ppgctslevelcd CHECK (([PPGCTSLevelCD] = 'BRZ' or [PPGCTSLevelCD] = 'GLD' or [PPGCTSLevelCD] = 'PLT' or [PPGCTSLevelCD] = 'PRF' or [PPGCTSLevelCD] = 'SLV'))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_programflag CHECK (([ProgramFlag] = 1 and [ProgramManagerUserID] is not null or [ProgramFlag] = 0))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_programscore CHECK (([ProgramScore] < 101 and [ProgramScore] > (-1)))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_warrantyperiodrefinishcd CHECK (([WarrantyPeriodRefinishCD] = '0' or [WarrantyPeriodRefinishCD] = '1' or [WarrantyPeriodRefinishCD] = '3' or [WarrantyPeriodRefinishCD] = '5' or [WarrantyPeriodRefinishCD] = '7' or [WarrantyPeriodRefinishCD] = '10' or [WarrantyPeriodRefinishCD] = '15' or [WarrantyPeriodRefinishCD] = '20' or [WarrantyPeriodRefinishCD] = '25' or [WarrantyPeriodRefinishCD] = '99'))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    uck_shop_location_warrantyperiodworkmanshipcd CHECK (([WarrantyPeriodWorkmanshipCD] = '0' or [WarrantyPeriodWorkmanshipCD] = '1' or [WarrantyPeriodWorkmanshipCD] = '3' or [WarrantyPeriodWorkmanshipCD] = '5' or [WarrantyPeriodWorkmanshipCD] = '7' or [WarrantyPeriodWorkmanshipCD] = '10' or [WarrantyPeriodWorkmanshipCD] = '15' or [WarrantyPeriodWorkmanshipCD] = '20' or [WarrantyPeriodWorkmanshipCD] = '25' or [WarrantyPeriodWorkmanshipCD] = '99'))
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_preferredcommunicationmethodid_in_communication_method FOREIGN KEY
	    (
	    PreferredCommunicationMethodID
	    ) REFERENCES dbo.utb_communication_method
	    (
	    CommunicationMethodID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_datareviewuserid_in_user FOREIGN KEY
	    (
	    DataReviewUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_mergeuserid_in_user FOREIGN KEY
	    (
	    MergeUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_programmanageruserid_in_user FOREIGN KEY
	    (
	    ProgramManagerUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_billingid_in_billing FOREIGN KEY
	    (
	    BillingID
	    ) REFERENCES dbo.utb_billing
	    (
	    BillingID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_preferredestimatepackageid_in_estimate_package FOREIGN KEY
	    (
	    PreferredEstimatePackageID
	    ) REFERENCES dbo.utb_estimate_package
	    (
	    EstimatePackageID
	    )
    
    ALTER TABLE dbo.utb_shop_location ADD CONSTRAINT
	    ufk_shop_location_shopid_in_shop FOREIGN KEY
	    (
	    ShopID
	    ) REFERENCES dbo.utb_shop
	    (
	    ShopID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_program_delta ADD CONSTRAINT
	    ufk_shop_location_program_delta_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_assignment ADD CONSTRAINT
	    ufk_assignment_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_search_results_log ADD CONSTRAINT
	    ufk_shop_search_results_log_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_load ADD CONSTRAINT
	    ufk_shop_load_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_document ADD CONSTRAINT
	    ufk_shop_location_document_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_reinspect ADD CONSTRAINT
	    ufk_reinspect_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_pricing ADD CONSTRAINT
	    ufk_shop_location_pricing_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_pricing_history ADD CONSTRAINT
	    ufk_shop_location_pricing_history_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_hours ADD CONSTRAINT
	    ufk_shop_location_hours_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_oem_discount ADD CONSTRAINT
	    ufk_shop_location_oem_discount_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_personnel ADD CONSTRAINT
	    ufk_shop_location_personnel_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_specialty ADD CONSTRAINT
	    ufk_shop_location_specialty_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_ingres ADD CONSTRAINT
	    ufk_shop_ingres_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_shop_location_survey ADD CONSTRAINT
	    ufk_shop_location_survey_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT
    BEGIN TRANSACTION
    
    ALTER TABLE dbo.utb_client_shop_location ADD CONSTRAINT
	    ufk_client_shop_location_shoplocationid_in_shop_location FOREIGN KEY
	    (
	    ShopLocationID
	    ) REFERENCES dbo.utb_shop_location
	    (
	    ShopLocationID
	    )
    
    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

