CREATE NONCLUSTERED INDEX uix_ie_invoice_statuscd
    ON dbo.utb_invoice(StatusCD)
    ON ufg_workflow_index
go

CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_dispositiontypecd
    ON dbo.utb_claim_aspect_service_channel(DispositionTypeCD)
    ON ufg_claim_index
go

