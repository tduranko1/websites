--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_client_coverage_type'         --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             AdditionalCoverageFlag                                !!!!
--!!!!!             ClientCode                                            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_client_coverage_type
    	DROP CONSTRAINT ufk_client_coverage_type_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_client_coverage_type
    	DROP CONSTRAINT ufk_client_coverage_type_insurancecompanyid_in_insurance

    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp_utb_client_coverage_type
    	(
    	ClientCoverageTypeID udt_std_id NOT NULL IDENTITY (1, 1),
    	InsuranceCompanyID udt_std_int_small NOT NULL,
    	AdditionalCoverageFlag udt_std_flag NOT NULL,
    	ClientCode udt_std_desc_short NULL,
    	CoverageProfileCD udt_std_cd NOT NULL,
    	DisplayOrder udt_std_int NULL,
    	EnabledFlag udt_std_flag NOT NULL,
    	Name udt_std_name NOT NULL,
    	SysLastUserID udt_std_id NOT NULL,
    	SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
    	)  ON ufg_client

    SET IDENTITY_INSERT dbo.Tmp_utb_client_coverage_type ON

    IF EXISTS(SELECT * FROM dbo.utb_client_coverage_type)
    	 EXEC('INSERT INTO dbo.Tmp_utb_client_coverage_type (ClientCoverageTypeID, InsuranceCompanyID, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
    		SELECT ClientCoverageTypeID, InsuranceCompanyID, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_client_coverage_type TABLOCKX')

    SET IDENTITY_INSERT dbo.Tmp_utb_client_coverage_type OFF

    ALTER TABLE dbo.utb_claim_aspect
    	DROP CONSTRAINT ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type

    DROP TABLE dbo.utb_client_coverage_type

    EXECUTE sp_rename N'dbo.Tmp_utb_client_coverage_type', N'utb_client_coverage_type', 'OBJECT'

    ALTER TABLE dbo.utb_client_coverage_type ADD CONSTRAINT
    	upk_client_coverage_type PRIMARY KEY CLUSTERED 
    	(
    	ClientCoverageTypeID
    	) WITH FILLFACTOR = 85 ON ufg_client


    CREATE NONCLUSTERED INDEX uix_ie_client_coverage_type_insurancecompanyid ON dbo.utb_client_coverage_type
    	(
    	InsuranceCompanyID
    	) WITH FILLFACTOR = 85 ON ufg_client_index

    ALTER TABLE dbo.utb_client_coverage_type 
        ADD CONSTRAINT uck_client_coverage_type_coverageprofilecd
        CHECK ([CoverageProfileCD] = 'COLL' or [CoverageProfileCD] = 'COMP' or [CoverageProfileCD] = 'LIAB' or [CoverageProfileCD] = 'RENT' or [CoverageProfileCD] = 'UIM' or [CoverageProfileCD] = 'UM')

    ALTER TABLE dbo.utb_client_coverage_type ADD CONSTRAINT
    	ufk_client_coverage_type_insurancecompanyid_in_insurance FOREIGN KEY
    	(
    	InsuranceCompanyID
    	) REFERENCES dbo.utb_insurance
    	(
    	InsuranceCompanyID
    	)

    ALTER TABLE dbo.utb_client_coverage_type ADD CONSTRAINT
    	ufk_client_coverage_type_syslastuserid_in_user FOREIGN KEY
    	(
    	SysLastUserID
    	) REFERENCES dbo.utb_user
    	(
    	UserID
    	)

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
    	ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type FOREIGN KEY
    	(
    	ClientCoverageTypeID
    	) REFERENCES dbo.utb_client_coverage_type
    	(
    	ClientCoverageTypeID
    	)

    COMMIT
 
    -- Capture any error

    SELECT @error = @@ERROR

END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

