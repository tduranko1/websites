--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_claim_aspect'                 --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add columns                                               !!!!
--!!!!!             OriginalCompleteDate                                  !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_lynxid_in_claim

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_owneruserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_analystuserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_supportuserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_completedowneruserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_completedanalystuserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_completedsupportuserid_in_user

    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_syslastuserid_in_user

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_claimaspecttypeid_in_claim_aspect_type

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_sourceapplicationid_in_application

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect
	    DROP CONSTRAINT ufk_claim_aspect_initialassignmenttypeid_in_assignment_type

    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp_utb_claim_aspect_1
	    (
	    ClaimAspectID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    AnalystUserID udt_std_id NULL,
	    ClaimAspectTypeID udt_std_int_tiny NOT NULL,
	    ClientCoverageTypeID udt_std_id NULL,
	    CompletedAnalystUserID udt_std_id NULL,
	    CompletedOwnerUserID udt_std_id NULL,
	    CompletedSupportUserID udt_std_id NULL,
	    InitialAssignmentTypeID udt_std_int NULL,
	    LynxID udt_std_int_big NOT NULL,
	    OwnerUserID udt_std_id NULL,
	    SourceApplicationID udt_std_int_tiny NULL,
	    SupportUserID udt_std_id NULL,
	    ClaimAspectNumber udt_std_int_tiny NOT NULL,
	    CoverageProfileCD udt_std_cd NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    EnabledFlag udt_enabled_flag NOT NULL,
	    ExposureCD udt_std_cd NULL,
	    NewAssignmentAnalystFlag udt_std_flag NULL,
	    NewAssignmentOwnerFlag udt_std_flag NULL,
	    NewAssignmentSupportFlag udt_std_flag NULL,
	    OriginalCompleteDate udt_std_datetime NULL,
	    PriorityFlag udt_std_flag NOT NULL,
	    SourceApplicationPassThruData udt_std_desc_huge NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_1 ON

    IF EXISTS(SELECT * FROM dbo.utb_claim_aspect)
	     EXEC('INSERT INTO dbo.Tmp_utb_claim_aspect_1 (ClaimAspectID, AnalystUserID, ClaimAspectTypeID, ClientCoverageTypeID, CompletedAnalystUserID, CompletedOwnerUserID, CompletedSupportUserID, InitialAssignmentTypeID, LynxID, OwnerUserID, SourceApplicationID, SupportUserID, ClaimAspectNumber, CoverageProfileCD, CreatedDate, EnabledFlag, ExposureCD, NewAssignmentAnalystFlag, NewAssignmentOwnerFlag, NewAssignmentSupportFlag, PriorityFlag, SourceApplicationPassThruData, SysLastUserID, SysLastUpdatedDate)
		    SELECT ClaimAspectID, AnalystUserID, ClaimAspectTypeID, ClientCoverageTypeID, CompletedAnalystUserID, CompletedOwnerUserID, CompletedSupportUserID, InitialAssignmentTypeID, LynxID, OwnerUserID, SourceApplicationID, SupportUserID, ClaimAspectNumber, CoverageProfileCD, CreatedDate, EnabledFlag, ExposureCD, NewAssignmentAnalystFlag, NewAssignmentOwnerFlag, NewAssignmentSupportFlag, PriorityFlag, SourceApplicationPassThruData, SysLastUserID, SysLastUpdatedDate FROM dbo.utb_claim_aspect (HOLDLOCK TABLOCKX)')

    SET IDENTITY_INSERT dbo.Tmp_utb_claim_aspect_1 OFF

    ALTER TABLE dbo.utb_claim_aspect_service_channel
	    DROP CONSTRAINT ufk_claim_aspect_service_channel_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_claim_aspect_involved
	    DROP CONSTRAINT ufk_claim_aspect_involved_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_invoice_service
	    DROP CONSTRAINT ufk_invoice_service_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_invoice
	    DROP CONSTRAINT ufk_invoice_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_claim_vehicle
	    DROP CONSTRAINT ufk_claim_vehicle_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_claim_property
	    DROP CONSTRAINT ufk_claim_property_claimaspectid_in_claim_aspect

    ALTER TABLE dbo.utb_claim_aspect_status
	    DROP CONSTRAINT ufk_claim_aspect_status_claimaspectid_in_claim_aspect

    DROP TABLE dbo.utb_claim_aspect

    EXECUTE sp_rename N'dbo.Tmp_utb_claim_aspect_1', N'utb_claim_aspect', 'OBJECT'

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    upk_claim_aspect PRIMARY KEY CLUSTERED 
	    (
	    ClaimAspectID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow


    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_claimaspecttypeid ON dbo.utb_claim_aspect
	    (
	    ClaimAspectTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_clientcoveragetypeid ON dbo.utb_claim_aspect
	    (
	    ClientCoverageTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_initialassignmenttypeid ON dbo.utb_claim_aspect
	    (
	    InitialAssignmentTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_lynxid ON dbo.utb_claim_aspect
	    (
	    LynxID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_owneruserid ON dbo.utb_claim_aspect
	    (
	    OwnerUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_analystuserid ON dbo.utb_claim_aspect
	    (
	    AnalystUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_supportuserid ON dbo.utb_claim_aspect
	    (
	    SupportUserID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_sourceapplicationid ON dbo.utb_claim_aspect
	    (
	    SourceApplicationID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_lynxid_claimaspecttypeid ON dbo.utb_claim_aspect
	    (
	    LynxID,
	    ClaimAspectTypeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    uck_claim_aspect_coverageprofilecd CHECK (([CoverageProfileCD] = 'COLL' or [CoverageProfileCD] = 'COMP' or [CoverageProfileCD] = 'LIAB' or [CoverageProfileCD] = 'UIM' or [CoverageProfileCD] = 'UM'))

    DECLARE @v sql_variant 
    SET @v = cast(N'COLL|Collision|COMP|Comprehensive|LIAB|Liability|UIM|Underinsured|UM|Uninsured' as varchar(78))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'user', N'dbo', N'table', N'utb_claim_aspect', N'constraint', N'uck_claim_aspect_coverageprofilecd'

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    uck_claim_aspect_exposurecd CHECK (([ExposureCD] = '1' or [ExposureCD] = '3' or [ExposureCD] = 'N'))

    SET @v = cast(N'1|1st Party|3|3rd Party|N|Not an Exposure' as varchar(41))
    EXECUTE sp_addextendedproperty N'Codes', @v, N'user', N'dbo', N'table', N'utb_claim_aspect', N'constraint', N'uck_claim_aspect_exposurecd'

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_initialassignmenttypeid_in_assignment_type FOREIGN KEY
	    (
	    InitialAssignmentTypeID
	    ) REFERENCES dbo.utb_assignment_type
	    (
	    AssignmentTypeID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_sourceapplicationid_in_application FOREIGN KEY
	    (
	    SourceApplicationID
	    ) REFERENCES dbo.utb_application
	    (
	    ApplicationID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_claimaspecttypeid_in_claim_aspect_type FOREIGN KEY
	    (
	    ClaimAspectTypeID
	    ) REFERENCES dbo.utb_claim_aspect_type
	    (
	    ClaimAspectTypeID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type FOREIGN KEY
	    (
	    ClientCoverageTypeID
	    ) REFERENCES dbo.utb_client_coverage_type
	    (
	    ClientCoverageTypeID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_owneruserid_in_user FOREIGN KEY
	    (
	    OwnerUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_analystuserid_in_user FOREIGN KEY
	    (
	    AnalystUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_supportuserid_in_user FOREIGN KEY
	    (
	    SupportUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_completedowneruserid_in_user FOREIGN KEY
	    (
	    CompletedOwnerUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_completedanalystuserid_in_user FOREIGN KEY
	    (
	    CompletedAnalystUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_completedsupportuserid_in_user FOREIGN KEY
	    (
	    CompletedSupportUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )

    ALTER TABLE dbo.utb_claim_aspect ADD CONSTRAINT
	    ufk_claim_aspect_lynxid_in_claim FOREIGN KEY
	    (
	    LynxID
	    ) REFERENCES dbo.utb_claim
	    (
	    LynxID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect_status ADD CONSTRAINT
	    ufk_claim_aspect_status_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_property ADD CONSTRAINT
	    ufk_claim_property_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_vehicle ADD CONSTRAINT
	    ufk_claim_vehicle_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_invoice ADD CONSTRAINT
	    ufk_invoice_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_invoice_service ADD CONSTRAINT
	    ufk_invoice_service_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect_involved ADD CONSTRAINT
	    ufk_claim_aspect_involved_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT
    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_claim_aspect_service_channel ADD CONSTRAINT
	    ufk_claim_aspect_service_channel_claimaspectid_in_claim_aspect FOREIGN KEY
	    (
	    ClaimAspectID
	    ) REFERENCES dbo.utb_claim_aspect
	    (
	    ClaimAspectID
	    )

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

