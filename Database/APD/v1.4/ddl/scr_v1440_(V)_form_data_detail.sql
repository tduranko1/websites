--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_form_data_detail'              --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_form_data_detail
    (
        FormDataID          udt_std_id_big                  NOT NULL,
        FormFieldID         udt_std_id_big                  NOT NULL,
	    FieldData           udt_std_desc_huge               NOT NULL,
        CONSTRAINT upk_form_data_detail
        PRIMARY KEY CLUSTERED (FormDataID, FormFieldID) WITH FILLFACTOR=90
                                                                              ON ufg_workflow
    )
    ON ufg_workflow
     
    ALTER TABLE dbo.utb_form_data_detail ADD CONSTRAINT
	    ufk_form_data_detail_formdataid_in_form_data FOREIGN KEY
	    (
	    FormDataID
	    ) REFERENCES dbo.utb_form_data
	    (
	    FormDataID
	    )
    
    ALTER TABLE dbo.utb_form_data_detail ADD CONSTRAINT
	    ufk_form_data_detail_FormFieldID_in_form_field FOREIGN KEY
	    (
	    FormFieldID
	    ) REFERENCES dbo.utb_form_field
	    (
	    FormFieldID
	    )
    
    CREATE NONCLUSTERED INDEX uix_ie_form_data_detail_formdataid ON dbo.utb_form_data_detail
	    (
	    FormDataID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_form_data_detail_formfieldid ON dbo.utb_form_data_detail
	    (
	    FormFieldID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
   
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

