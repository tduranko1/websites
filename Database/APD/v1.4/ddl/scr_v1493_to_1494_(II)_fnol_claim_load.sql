--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_fnol_claim_load' --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    CREATE TABLE dbo.Tmp_utb_fnol_claim_load
	    (
	    LynxID udt_std_int_big NOT NULL,
	    AdvanceAmount udt_std_money null,
	    AgentName udt_std_name NULL,
	    AgentPhone varchar(15) NULL,
	    AgentExt udt_ph_extension_number NULL,
	    CallerAddress1 udt_addr_line_1 NULL,
	    CallerAddress2 udt_addr_line_2 NULL,
	    CallerAddressCity udt_addr_city NULL,
	    CallerAddressState udt_addr_state NULL,
	    CallerAddressZip udt_addr_zip_code NULL,
	    CallerBestPhoneCode udt_std_cd NULL,
	    CallerBestTimeToCall udt_std_desc_short NULL,
	    CallerNameFirst udt_per_name NULL,
	    CallerNameLast udt_per_name NULL,
	    CallerNameTitle udt_per_title NULL,
	    CallerPhoneAlternate varchar(15) NULL,
	    CallerPhoneAlternateExt udt_ph_extension_number NULL,
	    CallerPhoneDay varchar(15) NULL,
	    CallerPhoneDayExt udt_ph_extension_number NULL,
	    CallerPhoneNight varchar(15) NULL,
	    CallerPhoneNightExt udt_ph_extension_number NULL,
	    CallersRelationToInsuredID udt_std_id NOT NULL,
	    CarrierRepUserID udt_std_id NOT NULL,
	    ContactAddress1 udt_addr_line_1 NULL,
	    ContactAddress2 udt_addr_line_2 NULL,
	    ContactAddressCity udt_addr_city NULL,
	    ContactAddressState udt_addr_state NULL,
	    ContactAddressZip udt_addr_zip_code NULL,
	    ContactBestPhoneCode udt_std_cd NULL,
	    ContactBestTimeToCall udt_std_desc_short NULL,
	    ContactEmailAddress udt_web_email NULL,
	    ContactNameFirst udt_per_name NULL,
	    ContactNameLast udt_per_name NULL,
	    ContactNameTitle udt_per_title NULL,
	    ContactPhoneAlternate varchar(15) NULL,
	    ContactPhoneAlternateExt udt_ph_extension_number NULL,
	    ContactPhoneDay varchar(15) NULL,
	    ContactPhoneDayExt udt_ph_extension_number NULL,
	    ContactPhoneNight varchar(15) NULL,
	    ContactPhoneNightExt udt_ph_extension_number NULL,
	    ContactsRelationToInsuredID udt_std_id NOT NULL,
	    CoverageClaimNumber udt_cov_claim_number NULL,
	    DemoFlag udt_std_flag NOT NULL,
	    FNOLUserID udt_std_id NOT NULL,
	    InsuranceCompanyID udt_std_id NOT NULL,
	    IntakeSeconds udt_std_int NOT NULL,
        LetterOfGuaranteeAmount udt_std_money null,
        LienHolderName		udt_std_name null,
        LienHolderAccountNumber	varchar(75) null,
        LienHolderAddress1	udt_addr_line_1 null,
        LienHolderAddress2	udt_addr_line_2 null,
        LienHolderAddressCity	udt_addr_city null,
        LienHolderAddressState	udt_addr_state null,
        LienHolderAddressZip	udt_addr_zip_code null,
        LienHolderContactName		udt_std_name null,
        LienHolderEmailAddress		udt_web_email null,
        LienHolderExpirationDate	udt_std_datetime null,
        LienHolderFaxAreaCode		udt_ph_area_code null,
        LienHolderFaxExchange		udt_ph_exchange_number null,
        LienHolderFaxUnitNumber	udt_ph_unit_number null,
        LienHolderPayoffAmount		udt_std_money null,
        LienHolderPhoneAreaCode	udt_ph_area_code null,
        LienHolderPhoneExchange	udt_ph_exchange_number null,
        LienHolderPhoneExtension	udt_ph_extension_number null,
        LienHolderPhoneUnitNumber	udt_ph_unit_number null,
	    LossDate varchar(30) NULL,
	    LossAddressCity udt_addr_city NULL,
	    LossAddressCounty udt_addr_county NULL,
	    LossAddressState udt_addr_state NULL,
	    LossAddressStreet udt_addr_line_1 NULL,
	    LossAddressZip udt_addr_zip_code NULL,
	    LossDescription udt_std_desc_xlong NULL,
	    LossTime varchar(30) NULL,
	    LossTypeLevel1ID udt_std_id NULL,
	    LossTypeLevel2ID udt_std_id NULL,
	    LossTypeLevel3ID udt_std_id NULL,
	    NoticeDate varchar(30) NULL,
	    NoticeMethodID udt_std_id NULL,
	    PayoffAmount udt_std_money NULL,
	    PayoffExpirationDate udt_std_datetime NULL,
	    PoliceDepartmentName udt_std_name NULL,
	    PolicyNumber udt_cov_policy_number NULL,
	    Remarks udt_std_desc_xlong NULL,
	    RoadLocationID udt_std_id NULL,
	    RoadTypeID udt_std_id NULL,
        SalvageAddress1	udt_addr_line_1 null,
        SalvageAddress2	udt_addr_line_2 null,
        SalvageAddressCity	udt_addr_city null,
        SalvageAddressState	udt_addr_state null,
        SalvageAddressZip	udt_addr_zip_code null,
        SalvageContactName		udt_std_name null,
        SalvageControlNumber	varchar(75) null,
        SalvageEmailAddress		udt_web_email null,
        SalvageFaxAreaCode		udt_ph_area_code null,
        SalvageFaxExchange		udt_ph_exchange_number null,
        SalvageFaxUnitNumber	udt_ph_unit_number null,
        SalvageName		udt_std_name null,
        SalvagePhoneAreaCode	udt_ph_area_code null,
        SalvagePhoneExchange	udt_ph_exchange_number null,
        SalvagePhoneExtension	udt_ph_extension_number null,
        SalvagePhoneUnitNumber	udt_ph_unit_number null,
        SettlementAmount	udt_std_money null,
	    SettlementDate udt_std_datetime NULL,
	    SourceApplicationCD udt_std_cd NOT NULL,
	    SourceApplicationPassThruData udt_std_desc_huge NULL,
	    TripPurpose udt_std_cd NULL,
	    WeatherConditionID udt_std_id NULL
	    )  ON ufg_load

    IF EXISTS(SELECT * FROM dbo.utb_fnol_claim_load)
	     EXEC('INSERT INTO dbo.Tmp_utb_fnol_claim_load (LynxID, AgentName, AgentPhone, AgentExt, CallerAddress1, CallerAddress2, CallerAddressCity, CallerAddressState, CallerAddressZip, CallerBestPhoneCode, CallerBestTimeToCall, CallerNameFirst, CallerNameLast, CallerNameTitle, CallerPhoneAlternate, CallerPhoneAlternateExt, CallerPhoneDay, CallerPhoneDayExt, CallerPhoneNight, CallerPhoneNightExt, CallersRelationToInsuredID, CarrierRepUserID, ContactAddress1, ContactAddress2, ContactAddressCity, ContactAddressState, ContactAddressZip, ContactBestPhoneCode, ContactBestTimeToCall, ContactEmailAddress, ContactNameFirst, ContactNameLast, ContactNameTitle, ContactPhoneAlternate, ContactPhoneAlternateExt, ContactPhoneDay, ContactPhoneDayExt, ContactPhoneNight, ContactPhoneNightExt, ContactsRelationToInsuredID, CoverageClaimNumber, DemoFlag, FNOLUserID, InsuranceCompanyID, IntakeSeconds, LossDate, LossAddressCity, LossAddressCounty, LossAddressState, LossAddressStreet, LossAddressZip, LossDescription, LossTime, LossTypeLevel1ID, LossTypeLevel2ID, LossTypeLevel3ID, NoticeDate, NoticeMethodID, PoliceDepartmentName, PolicyNumber, Remarks, RoadLocationID, RoadTypeID, SourceApplicationCD, SourceApplicationPassThruData, TripPurpose, WeatherConditionID)
		    SELECT LynxID, AgentName, AgentPhone, AgentExt, CallerAddress1, CallerAddress2, CallerAddressCity, CallerAddressState, CallerAddressZip, CallerBestPhoneCode, CallerBestTimeToCall, CallerNameFirst, CallerNameLast, CallerNameTitle, CallerPhoneAlternate, CallerPhoneAlternateExt, CallerPhoneDay, CallerPhoneDayExt, CallerPhoneNight, CallerPhoneNightExt, CallersRelationToInsuredID, CarrierRepUserID, ContactAddress1, ContactAddress2, ContactAddressCity, ContactAddressState, ContactAddressZip, ContactBestPhoneCode, ContactBestTimeToCall, ContactEmailAddress, ContactNameFirst, ContactNameLast, ContactNameTitle, ContactPhoneAlternate, ContactPhoneAlternateExt, ContactPhoneDay, ContactPhoneDayExt, ContactPhoneNight, ContactPhoneNightExt, ContactsRelationToInsuredID, CoverageClaimNumber, DemoFlag, FNOLUserID, InsuranceCompanyID, IntakeSeconds, LossDate, LossAddressCity, LossAddressCounty, LossAddressState, LossAddressStreet, LossAddressZip, LossDescription, LossTime, LossTypeLevel1ID, LossTypeLevel2ID, LossTypeLevel3ID, NoticeDate, NoticeMethodID, PoliceDepartmentName, PolicyNumber, Remarks, RoadLocationID, RoadTypeID, SourceApplicationCD, SourceApplicationPassThruData, TripPurpose, WeatherConditionID FROM dbo.utb_fnol_claim_load WITH (HOLDLOCK TABLOCKX)')

    DROP TABLE dbo.utb_fnol_claim_load

    EXECUTE sp_rename N'dbo.Tmp_utb_fnol_claim_load', N'utb_fnol_claim_load', 'OBJECT' 

    COMMIT

    -- Capture any error

    SELECT @error = @@ERROR
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

