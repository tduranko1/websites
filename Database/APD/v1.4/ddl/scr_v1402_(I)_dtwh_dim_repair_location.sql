BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.utb_dtwh_dim_repair_location
	DROP CONSTRAINT ufk_dtwh_dim_repair_location_stateid_in_dtwh_dim_state

COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.utb_dtwh_dim_repair_location
	DROP CONSTRAINT ufk_dtwh_dim_repair_location_standarddocumentsourceid_in_dtwh_dim_document_source

COMMIT
BEGIN TRANSACTION
CREATE TABLE dbo.Tmp_utb_dtwh_dim_repair_location
	(
	RepairLocationID udt_std_id_big NOT NULL IDENTITY (1, 1),
	StandardDocumentSourceID udt_std_id NULL,
	StateID udt_std_id NOT NULL,
	CEIProgramFlag udt_std_flag NOT NULL,
	CFProgramFlag udt_std_flag NOT NULL,
	City udt_addr_city NOT NULL,
	County udt_addr_county NOT NULL,
	LYNXSelectProgramFlag udt_std_flag NOT NULL,
	PPGPaintBuyerFlag udt_std_flag NOT NULL,
	ShopLocationID udt_std_id_big NULL,
	ShopLocationName udt_std_name NOT NULL,
	StandardEstimatePackage udt_std_name NULL,
	ZipCode udt_addr_zip_code NOT NULL
	)  ON ufg_dtwh

SET IDENTITY_INSERT dbo.Tmp_utb_dtwh_dim_repair_location ON

IF EXISTS(SELECT * FROM dbo.utb_dtwh_dim_repair_location)
	 EXEC('INSERT INTO dbo.Tmp_utb_dtwh_dim_repair_location (RepairLocationID, StandardDocumentSourceID, StateID, CEIProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationID, ShopLocationName, StandardEstimatePackage, ZipCode)
		SELECT RepairLocationID, StandardDocumentSourceID, StateID, CEIProgramFlag, CFProgramFlag, City, County, LYNXSelectProgramFlag, PPGPaintBuyerFlag, ShopLocationID, ShopLocationName, StandardEstimatePackage, ZipCode FROM dbo.utb_dtwh_dim_repair_location TABLOCKX')

SET IDENTITY_INSERT dbo.Tmp_utb_dtwh_dim_repair_location OFF

ALTER TABLE dbo.utb_dtwh_fact_estimate
	DROP CONSTRAINT ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location

DROP TABLE dbo.utb_dtwh_dim_repair_location

EXECUTE sp_rename N'dbo.Tmp_utb_dtwh_dim_repair_location', N'utb_dtwh_dim_repair_location', 'OBJECT'

ALTER TABLE dbo.utb_dtwh_dim_repair_location ADD CONSTRAINT
	upk_dtwh_dim_repair_location PRIMARY KEY CLUSTERED 
	(
	RepairLocationID
	) WITH FILLFACTOR = 90 ON ufg_dtwh


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_shoplocationid ON dbo.utb_dtwh_dim_repair_location
	(
	ShopLocationID
	) WITH FILLFACTOR = 90 ON ufg_dtwh_index

CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_city ON dbo.utb_dtwh_dim_repair_location
	(
	City
	) WITH FILLFACTOR = 90 ON ufg_dtwh_index

CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_county ON dbo.utb_dtwh_dim_repair_location
	(
	County
	) WITH FILLFACTOR = 90 ON ufg_dtwh_index

CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_stateid ON dbo.utb_dtwh_dim_repair_location
	(
	StateID
	) WITH FILLFACTOR = 85 ON ufg_dtwh_index

CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_zipcode ON dbo.utb_dtwh_dim_repair_location
	(
	ZipCode
	) WITH FILLFACTOR = 90 ON ufg_dtwh_index

ALTER TABLE dbo.utb_dtwh_dim_repair_location ADD CONSTRAINT
	ufk_dtwh_dim_repair_location_standarddocumentsourceid_in_dtwh_dim_document_source FOREIGN KEY
	(
	StandardDocumentSourceID
	) REFERENCES dbo.utb_dtwh_dim_document_source
	(
	DocumentSourceID
	)

ALTER TABLE dbo.utb_dtwh_dim_repair_location ADD CONSTRAINT
	ufk_dtwh_dim_repair_location_stateid_in_dtwh_dim_state FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.utb_dtwh_dim_state
	(
	StateID
	)

COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.utb_dtwh_fact_estimate ADD CONSTRAINT
	ufk_dtwh_fact_estimate_repairlocationid_in_dtwh_dim_repair_location FOREIGN KEY
	(
	RepairLocationID
	) REFERENCES dbo.utb_dtwh_dim_repair_location
	(
	RepairLocationID
	)

COMMIT
