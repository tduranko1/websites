--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME           --           !!!!
            SET @table = 'utb_client_bundling'               --           !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Create table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    CREATE TABLE dbo.utb_client_bundling
    (
        ClientBundlingID    udt_std_id_big IDENTITY(1,1)    NOT NULL,
        BundlingID          udt_std_id_big                  NOT NULL,
	    InsuranceCompanyID  udt_std_int_small               NOT NULL,
        OfficeID            udt_std_id                      NOT NULL,
	    ReturnDocPackageTypeCD udt_std_cd                       NULL,
	    ReturnDocRoutingCD     udt_std_cd                       NULL,
	    ReturnDocRoutingValue  udt_std_desc_mid                 NULL,
	    SysLastUserID       udt_std_id                      NOT NULL,
	    SysLastUpdatedDate  udt_sys_last_updated_date       NOT NULL,
        CONSTRAINT upk_client_bundling
        PRIMARY KEY CLUSTERED (ClientBundlingID) WITH FILLFACTOR=90
                                                                              ON ufg_workflow
    )
    ON ufg_workflow
     
    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_bundlingid_in_bundling FOREIGN KEY
	    (
	    BundlingID
	    ) REFERENCES dbo.utb_bundling
	    (
	    BundlingID
	    )
    
    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_insurancecompanyid_in_insurance FOREIGN KEY
	    (
	    InsuranceCompanyID
	    ) REFERENCES dbo.utb_insurance
	    (
	    InsuranceCompanyID
	    )
    
    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_officeid_in_office FOREIGN KEY
	    (
	    OfficeID
	    ) REFERENCES dbo.utb_office
	    (
	    OfficeID
	    )
    
    ALTER TABLE dbo.utb_client_bundling ADD CONSTRAINT
	    ufk_client_bundling_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    CREATE NONCLUSTERED INDEX uix_ie_client_bundling_bundlingid ON dbo.utb_client_bundling
	    (
	    BundlingID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_client_bundling_insurancecompanyid ON dbo.utb_client_bundling
	    (
	    InsuranceCompanyID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    CREATE NONCLUSTERED INDEX uix_ie_client_bundling_officeid ON dbo.utb_client_bundling
	    (
	    OfficeID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
   
    -- Capture any error

    SELECT @error = @@ERROR
END
    
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING New table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK New table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - New table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'New table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

