--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!          OBJECT CREATION SCRIPT FOR UPDATING APD DATABASE         !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_task_assignment_pool'         --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     Changes:                                                      !!!!
--!!!!!         Add new columns
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP

PRINT '.'
PRINT '.'
PRINT 'Modify table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END


PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN

    -- Create new table schema in holding area
    CREATE TABLE dbo.Tmp_utb_task_assignment_pool
	    (
	        TaskID udt_std_id_small NOT NULL,
	        ServiceChannelCD udt_std_cd NOT NULL,
	        InsuranceCompanyID udt_std_id_small NOT NULL,
	        AssignmentPoolID udt_std_id NOT NULL,
	        SysLastUserID udt_std_id NOT NULL,
	        SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow

    IF EXISTS(SELECT * FROM dbo.utb_task_assignment_pool)
	     EXEC('INSERT INTO dbo.Tmp_utb_task_assignment_pool (TaskID,ServiceChannelCD,InsuranceCompanyID,AssignmentPoolID,SysLastUserID,SysLastUpdatedDate) 
		    SELECT TaskID,ServiceChannelCD,0,AssignmentPoolID,SysLastUserID,SysLastUpdatedDate FROM dbo.utb_task_assignment_pool WITH (HOLDLOCK TABLOCKX)')


    -- Remove FKs
    -- No FKs on this table    


    -- Drop current table and replace with new table schema
    DROP TABLE dbo.utb_task_assignment_pool

    EXECUTE sp_rename N'dbo.Tmp_utb_task_assignment_pool', N'utb_task_assignment_pool', 'OBJECT' 

    ALTER TABLE dbo.utb_task_assignment_pool ADD CONSTRAINT
	    upk_task_assignment_pool PRIMARY KEY CLUSTERED 
	    (
	        TaskID ASC, 
	        ServiceChannelCD ASC, 
	        InsuranceCompanyID ASC
	    ) WITH FILLFACTOR = 90 ON ufg_workflow


    -- Recreate indexes
    -- No secondary indexes on this table


    -- Add check costraints and reference codes
    ALTER TABLE dbo.utb_task_assignment_pool ADD CONSTRAINT
	    uck_task_assignment_pool_servicechannelcd CHECK (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL'))

    EXEC sp_addextendedproperty 'Codes', 'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|SA|Staff Appraiser|TL|Total Loss' , 
        'user', dbo, 'table','utb_task_assignment_pool', 'CONSTRAINT','uck_task_assignment_pool_servicechannelcd'


    -- Reimplement FKs
    -- No FKs on this table    
    
END
    
-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END

PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
     WHERE Table_Name = ''' + @table + '''
     ORDER BY Ordinal_Position
'

EXEC @return = sp_executesql @cmd


SELECT @cmd = N'
    SELECT ''' + @table + ''', COUNT(*) FROM ' + @table

EXEC @return = sp_executesql @cmd
 

