ALTER TABLE [dbo].[utb_client_coverage_type] DROP CONSTRAINT [uck_client_coverage_type_coverageprofilecd]
GO

ALTER TABLE [dbo].[utb_client_coverage_type]  WITH CHECK ADD  CONSTRAINT [uck_client_coverage_type_coverageprofilecd] CHECK  (([CoverageProfileCD] = 'COLL' or [CoverageProfileCD] = 'COMP' or [CoverageProfileCD] = 'GLASS' or [CoverageProfileCD] = 'LIAB' or [CoverageProfileCD] = 'RENT' or [CoverageProfileCD] = 'TOW' or [CoverageProfileCD] = 'UIM' or [CoverageProfileCD] = 'UM'))
GO

ALTER TABLE [dbo].[utb_client_coverage_type] CHECK CONSTRAINT [uck_client_coverage_type_coverageprofilecd]
GO
