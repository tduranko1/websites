-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @monitored_server AS NVARCHAR(100)
DECLARE @monitored_db AS NVARCHAR(100)
DECLARE @remote_db_flag AS BIT

DECLARE @query1         AS NVARCHAR(500)   -- The application query/procedure to execute
DECLARE @subject1       AS NVARCHAR(255)   -- The email subject
DECLARE @recipients1    AS NVARCHAR(256)   -- The list of mail recepients
DECLARE @query2         AS NVARCHAR(500)   -- The application query/procedure to execute
DECLARE @subject2       AS NVARCHAR(255)   -- The email subject
DECLARE @recipients2    AS NVARCHAR(256)   -- The list of mail recepients
DECLARE @query3         AS NVARCHAR(500)   -- The application query/procedure to execute
DECLARE @subject3       AS NVARCHAR(255)   -- The email subject
DECLARE @recipients3    AS NVARCHAR(256)   -- The list of mail recepients


-- Set monitor server and database, job name

SET @remote_db_flag = 1
SET @monitored_server = N'SFTMAPDPRDSQL'
SET @monitored_db = N'udb_apd'

SET @job_name = N'uj_app_dbs_monthly_notices_' + @monitored_server + N'_' + @monitored_db


SET @query1 = N'SELECT ''''Check for reference data updates (ftp://ftp.melissadata.com/updates)''''  + CHAR(13) + CHAR(13) + '''' TO DO: Load Production Admin database'''''
SET @subject1 = N'NOTICE: Production LynxSelect - Verify ZIP/FONE data updates'
SET @recipients1 = N'dl-lynxapdprddbadmin@lynxservices.com'

SET @query2 = N'SELECT ''''Download NADA monthly data updates (http://www.nada.com/b2b/evaldownload/evalmain.asp)''''  + CHAR(13) + CHAR(13) + '''' UserID: 23384''''  + CHAR(13) + '''' Password: 123456''''  + CHAR(13) + CHAR(13) + '''' TO DO: Load DEV/STG/PRD APD Database Servers'''''
SET @subject2 = N'NOTICE: Production LynxSelect - Retrieve NADA monthly data updates'
SET @recipients2 = N'dl-lynxapdprddbadmin@lynxservices.com;danprice@lynxservices.com'

SET @query3 = N'SELECT ''''Check for Area Code Split updates (http://frodo.bruderhof.com/areacode/#list)''''  + CHAR(13) + CHAR(13) + '''' TO DO: Upate APD Databases '''' + CHAR(13) + '''' Step 1: Update Area Code Split tables with new split(s)'''' + CHAR(13) + '''' Step 2: Execute uspAreaCodeSplitConversion (after Permissive End Date)'''''
SET @subject3 = N'NOTICE: Production LynxSelect - Verify Area Code Splits'
SET @recipients3 = N'dl-lynxapdprddbadmin@lynxservices.com'


-- Add linked server

IF @remote_db_flag = 1
BEGIN
    -- The @monitored_server may need to be given an Alias on the monitoring server

    EXECUTE @rc = sp_helpserver @monitored_server
    IF @rc = 1 EXECUTE sp_addlinkedserver @monitored_server
END


-- Add APP monitoring job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_app_monitoring')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_app_monitoring'
END


-- Delete previous DBA monitor job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name 
END

-- Add job to report notices

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name,
    @description = N'Report monthly notices',
    @category_name = N'ujc_app_monitoring',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @notify_level_email = 2,
    @notify_email_operator_name = N'uop_dba_monitor',
    @notify_level_netsend = 2,
    @notify_netsend_operator_name = N'uop_dba_monitor',
    @job_id = @job_id OUTPUT

-- Add the job steps

SET @command = N'EXECUTE master..xp_sendmail @width=8000, @no_header=''TRUE'', @query=''' + @query1 + ''', @subject=''' + @subject1 + ''', @recipients=''' + @recipients1 + ''''

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_sendmail_xp notifications1',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 3,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0


-- Add the job steps

SET @command = N'EXECUTE master..xp_sendmail @width=8000, @no_header=''TRUE'', @query=''' + @query2 + ''', @subject=''' + @subject2 + ''', @recipients=''' + @recipients2 + ''''

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 2,
    @step_name = N'ujs_execute_sendmail_xp notifications2',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 3,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0


-- Add the job steps

SET @command = N'EXECUTE master..xp_sendmail @width=8000, @no_header=''TRUE'', @query=''' + @query3 + ''', @subject=''' + @subject3 + ''', @recipients=''' + @recipients3 + ''''

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 3,
    @step_name = N'ujs_execute_sendmail_xp notifications3',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0


-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_monthly_notices',
    @freq_type = 16,  -- Monthly
    @freq_interval = 16,  -- Day of month
    @freq_subday_type = 1,  
    @freq_recurrence_factor = 1,
    @active_start_time = 070000  -- Starting at 7:00 am

  
-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 