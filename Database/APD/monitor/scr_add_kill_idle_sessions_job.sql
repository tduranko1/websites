-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @monitored_server AS NVARCHAR(100)
DECLARE @monitored_db AS NVARCHAR(100)
DECLARE @remote_db_flag AS BIT


-- Set monitor server and database, job name

SET @remote_db_flag = 1
SET @monitored_server = N'SFTMAPDPRDSQL'
SET @monitored_db = N'udb_apd'

SET @job_name = N'uj_app_dbs_kill_idle_sessions_' + @monitored_server + N'_' + @monitored_db
SET @command = N'EXECUTE ' + @monitored_server + N'.' + @monitored_db + '.dbo.uspSessionDelExpired @minutes=20160' -- 2 weeks


-- Add linked server

IF @remote_db_flag = 1
BEGIN
    -- The @monitored_server may need to be given an Alias on the monitoring server

    EXECUTE @rc = sp_helpserver @monitored_server
    IF @rc = 1 EXECUTE sp_addlinkedserver @monitored_server
END


-- Add DBA monitoring job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_app_monitoring')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_app_monitoring'
END


-- Delete previous DBA monitor job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name 
END

-- Add job to terminate idle processes

SET @job_id = NULL

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name,
    @description = N'Terminate idle sessions',
    @category_name = N'ujc_app_monitoring',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @job_id = @job_id OUTPUT

-- Add the job steps

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_monitor_usp',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @database_name = N'master'

-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_monitor_kill_idle_sessions',
    @freq_type = 4,  -- Daily
    @freq_interval = 1,  -- Every day
    @active_start_time = 00000, -- 12:00 AM (immediately)
    @freq_subday_type = 4,  
    @freq_subday_interval = 30  -- Every 30 minutes

-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 


GO


