-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @monitored_server AS NVARCHAR(100)
DECLARE @monitored_db AS NVARCHAR(100)
DECLARE @remote_db_flag AS BIT


-- Set monitor server and database, job name

SET @remote_db_flag = 1
-- SET @monitored_server = N'SFTMAPDDEVDB1'
-- SET @monitored_db = N'udb_apd_dev'
-- SET @monitored_db = N'udb_apd_test'
-- SET @monitored_server = N'SPADAPDSTGSQL'
-- SET @monitored_db = N'udb_apd_stg'
-- SET @monitored_server = N'SFTMAPDPRDSQL'
-- SET @monitored_db = N'udb_apd'

SET @job_name = N'uj_app_dbs_daily_rpt_load_' + @monitored_server + N'_' + @monitored_db
SET @command = N'EXECUTE ' + @monitored_server + N'.' + @monitored_db + '.dbo.uspDTWHGenerateData '

-- Add linked server

IF @remote_db_flag = 1
BEGIN
    -- The @monitored_server may need to be given an Alias on the monitoring server

    EXECUTE @rc = sp_helpserver @monitored_server
    IF @rc = 1 EXECUTE sp_addlinkedserver @monitored_server
END


-- Add APP monitoring job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_app_monitoring')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_app_monitoring'
END


-- Delete previous DBA monitor job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name 
END

-- Add job to report new claims

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name,
    @description = N'Reporting data load',
    @category_name = N'ujc_app_monitoring',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @notify_level_email = 2,
    @notify_email_operator_name = N'uop_dba_monitor',
    @notify_level_netsend = 2,
    @notify_netsend_operator_name = N'uop_dba_monitor',
    @job_id = @job_id OUTPUT

-- Add the job steps

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_notification_usp daily claims',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0


-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_daily_new_claim_report',
    @freq_type = 8,  -- Weekly
    @freq_interval = 62,  -- Every business day
    @freq_subday_type = 1,  
    @freq_recurrence_factor = 1,
    @active_start_time = 230000  -- Starting at 11:00 pm

  
-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 