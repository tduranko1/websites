-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @monitored_server AS NVARCHAR(100)
DECLARE @monitored_db AS NVARCHAR(100)
DECLARE @remote_db_flag AS BIT

DECLARE @query         AS NVARCHAR(500)   -- The application query/procedure to execute
DECLARE @subject       AS NVARCHAR(255)   -- The email subject
DECLARE @recipients    AS NVARCHAR(256)   -- The list of mail recepients


-- Set monitor server and database, job name

SET @remote_db_flag = 1
SET @monitored_server = N'SFTMAPDPRDSQL'
SET @monitored_db = N'udb_apd'

SET @job_name = N'uj_app_dbs_estimate_orphans_' + @monitored_server + N'_' + @monitored_db

SET @query = N'EXECUTE SFTMAPDPRDSQL.udb_apd.dbo.uspEstimateOrphanGetList udb_partner'
SET @subject = N'ALERT: Production Partner/LynxSelect - Estimate Orphans'
SET @recipients = N'lynxapdprdmon@lynxservices.com;dl-lynxapdprddbadmin@lynxservices.com'


-- Add linked server

IF @remote_db_flag = 1
BEGIN
    -- The @monitored_server may need to be given an Alias on the monitoring server

    EXECUTE @rc = sp_helpserver @monitored_server
    IF @rc = 1 EXECUTE sp_addlinkedserver @monitored_server
END


-- Add APP monitoring job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_app_monitoring')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_app_monitoring'
END


-- Delete previous DBA monitor job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name 
END

-- Add job to monitor database file groups

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name,
    @description = N'Monitor estimate orphans',
    @category_name = N'ujc_app_monitoring',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @notify_level_email = 2,
    @notify_email_operator_name = N'uop_dba_monitor',
    @notify_level_netsend = 2,
    @notify_netsend_operator_name = N'uop_dba_monitor',
    @job_id = @job_id OUTPUT

-- Add the job steps

SET @command = N'EXECUTE udb_admin.dbo.usp_app_db_notification @query=''' + @query + ''', @subject=''' + @subject + ''', @recipients=''' + @recipients + ''''

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_notification_usp',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0


-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_estimate_orphans_business_days',
    @freq_type = 8,  -- Weekly
    @freq_interval = 62,  -- Every business day
    @freq_subday_type = 4,  
    @freq_subday_interval = 60,  -- Every 60 minutes
    @freq_recurrence_factor = 1,
    @active_start_time = 090000,  -- Starting at 9:00 am
    @active_end_time = 175959  -- till 6:00 pm

  
-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 
