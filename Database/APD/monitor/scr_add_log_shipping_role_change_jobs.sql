-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @database_name AS VARCHAR(32)
DECLARE @primary_server_name AS NVARCHAR(32)
DECLARE @secondary_server_name AS NVARCHAR(32)
DECLARE @monitor_server_name AS NVARCHAR(32)


-- Set monitor server and database, job name

SET @database_name = 'udb_apd_dba'
SET @primary_server_name = 'SFTMAPDDEVDB2'
SET @secondary_server_name = 'SFTMAPDDEVDB'
SET @monitor_server_name = 'SFTMAPDDB'

SET @job_name = N'uj_app_log_shipping_role_change_' + @primary_server_name + N'_' + @database_name


-- Add linked servers

EXECUTE @rc = sp_helpserver @primary_server_name
IF @rc = 1 EXECUTE sp_addlinkedserver @primary_server_name

EXECUTE @rc = sp_helpserver @secondary_server_name
IF @rc = 1 EXECUTE sp_addlinkedserver @secondary_server_name


-- Add APP monitoring job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_app_monitoring')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_app_monitoring'
END


-- Delete previous DBA monitor job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name 
END

-- Add job to execute a log shipping role change

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name,
    @description = N'Execute a log shipping role change',
    @category_name = N'ujc_app_monitoring',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @notify_level_email = 3,
    @notify_email_operator_name = N'uop_dba_monitor',
    @notify_level_netsend = 2,
    @notify_netsend_operator_name = N'uop_dba_monitor',
    @job_id = @job_id OUTPUT

-- Add the job steps

SET @command = N'EXECUTE udb_admin.dbo.usp_app_log_shipping_role_change @database_name=''' + @database_name +
                         ''', @primary_server_name=''' + @primary_server_name + 
                         ''', @secondary_server_name=''' + @secondary_server_name + 
                         ''', @monitor_server_name=''' + @monitor_server_name + ''''

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_log_shipping_role_change_usp',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master'


-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_log_shipping_role_change',
    @freq_type = 1,  -- Once
    @freq_interval = 1  -- Every day


-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 
