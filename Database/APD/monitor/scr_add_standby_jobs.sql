-- Set destination database

USE msdb


-- SET NOCOUNT to ON and no longer display the count message or warnings

SET NOCOUNT ON
SET ANSI_WARNINGS OFF


-- Declare internal variables

DECLARE @command AS NVARCHAR(500)
DECLARE @job_name_copy AS NVARCHAR(500)
DECLARE @job_name_restore AS NVARCHAR(500)
DECLARE @job_id AS BINARY(16)  

DECLARE @rc AS BIT

DECLARE @standby_server AS NVARCHAR(100)
DECLARE @primary_server AS NVARCHAR(100)
DECLARE @standby_db AS NVARCHAR(100)
DECLARE @remote_db_flag AS BIT


-- Set monitor server and database, job name

SET @remote_db_flag = 1
SET @standby_server = N'SPADAPDSTGSQL'
SET @primary_server = N'SFTMAPDPRDSQL'
SET @standby_db = N'udb_standby'

SET @job_name_copy = N'uj_standby_dbs_copy_backups_' + @primary_server
SET @job_name_restore = N'uj_standby_dbs_restore_backups_' + @primary_server


-- Add linked server

IF @remote_db_flag = 1
BEGIN
    -- The Primary Server may need to be given an Alias on the Standby server

    EXECUTE @rc = sp_helpserver @primary_server
    IF @rc = 1 EXECUTE sp_addlinkedserver @primary_server
END


-- Add Standby job category

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'ujc_standby')
BEGIN 
    EXECUTE msdb.dbo.sp_add_category
        @class = N'JOB', 
        @type = N'LOCAL',
        @name = N'ujc_standby'
END


-- Delete previous Server Standby job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name_copy)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name_copy 
END


-- Add job to copy backups

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name_copy,
    @description = N'Copy application database backup files',
    @category_name = N'ujc_standby',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @job_id = @job_id OUTPUT

-- Add the job steps

SET @command = N'EXECUTE '  + @standby_server + N'.' + @standby_db + N'.dbo.spCopy ' + @primary_server

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_standby_copy_usp',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0

-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_standby_copy_startup',
    @freq_type = 64,  -- When SQL Server Agent starts
    @freq_interval = 64  -- When SQL Server Agent starts

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_standby_copy_retry',
    @freq_type = 4,  -- Daily
    @freq_interval = 1,  -- Every day
    @active_start_time = 00000, -- 12:00 AM (immediately)
    @freq_subday_type = 4,  
    @freq_subday_interval = 1  -- Every 1 minutes

-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 

-- Delete previous Server Standby job

IF EXISTS (SELECT name FROM msdb.dbo.sysjobs WHERE name = @job_name_restore)
BEGIN 
    -- Delete the job with the same name

    EXECUTE msdb.dbo.sp_delete_job
        @job_name = @job_name_restore 
END


SET @job_id = NULL

-- Add job to restore backups

EXECUTE msdb.dbo.sp_add_job 
    @job_name = @job_name_restore,
    @description = N'Restore application database backup files',
    @category_name = N'ujc_standby',
    @owner_login_name = N'sa',
    @notify_level_eventlog = 2,
    @job_id = @job_id OUTPUT

-- Add the job steps

SET @command = N'EXECUTE '  + @standby_server + N'.' + @standby_db + N'.dbo.spRestore ' + @primary_server

EXECUTE msdb.dbo.sp_add_jobstep 
    @job_id = @job_id,
    @step_id = 1,
    @step_name = N'ujs_execute_standby_restore_usp',
    @subsystem = N'TSQL',
    @command = @command,
    @on_success_action = 1,
    @on_success_step_id = 0,
    @on_fail_action = 2,
    @on_fail_step_id = 0,
    @database_name = N'master',
    @retry_attempts = 5,
    @retry_interval = 0

-- Add the job schedules

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_standby_restore_startup',
    @freq_type = 64,  -- When SQL Server Agent starts
    @freq_interval = 64  -- When SQL Server Agent starts

EXECUTE msdb.dbo.sp_add_jobschedule
    @job_id = @job_id,
    @name = N'ujsch_app_dbs_standby_restore_retry',
    @freq_type = 4,  -- Daily
    @freq_interval = 1,  -- Every day
    @active_start_time = 00000, -- 12:00 AM (immediately)
    @freq_subday_type = 4,  
    @freq_subday_interval = 1  -- Every 1 minutes

-- Add the Target Servers

EXECUTE msdb.dbo.sp_add_jobserver
    @job_id = @job_id,
    @server_name = @@SERVERNAME 


