<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="vDrivable">
      <xsl:choose>
        <xsl:when test="Root/Claim/Vehicle/@Drivable = '1'">
          <xsl:value-of select="'Y'"/>
        </xsl:when>
        <xsl:when test="Root/Claim/Vehicle/@Drivable = '0'">
          <xsl:value-of select="'N'"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="LynxIDVeh" select="concat(substring((Root/@LynxID),1,string-length(Root/@LynxID)-1), '-', (Root/Vehicle/@VehicleNumber), '-', (/Root/ServiceChannel/Assignment/Shop/@ShopID))" />
    <xsl:variable name="ClaimNumber" select="concat('LYNXID', concat((Root/@LynxID), '-', (Root/Vehicle/@VehicleNumber)))" />
    <xsl:variable name="InsuranceCompanyID" select="concat((Root/@InsuranceCompanyID),'-CS')" />

    <AssignmentAddRq xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <RqUID xmlns="http://www.cieca.com/BMS"></RqUID>
      <AsyncRqUID xmlns="http://www.cieca.com/BMS"></AsyncRqUID>
      <PartnerKey xmlns="http://www.cieca.com/BMS"></PartnerKey>
      <SvcProviderName xmlns="http://www.cieca.com/BMS">
        <xsl:value-of select="'LYNX'"/>
      </SvcProviderName>
      <DocumentInfo xmlns="http://www.cieca.com/BMS">
        <BMSVer>
          <xsl:value-of select="'4.6.0'"/>
        </BMSVer>
        <DocumentType>A</DocumentType>
        <DocumentID>
          <xsl:value-of select="$LynxIDVeh"/>
        </DocumentID>
        <DocumentStatus>O</DocumentStatus>
        <CreateDateTime>
          <xsl:value-of select="/Root/ServiceChannel/Assignment/@SysLastUpdatedDate"/>
        </CreateDateTime>
        <TransmitDateTime>
          <xsl:value-of select="/Root/ServiceChannel/Assignment/@SysLastUpdatedDate"/>
        </TransmitDateTime>
        <ReferenceInfo>
          <PassThroughInfo>
            <xsl:value-of select="$LynxIDVeh"/>
          </PassThroughInfo>
          <OtherReferenceInfo>
            <OtherReferenceName>
              <xsl:value-of select="'HQREVIEWTYPECD'"/>
            </OtherReferenceName>
            <OtherRefNum>
              <xsl:value-of select="'CHOICE'"/>
            </OtherRefNum>
          </OtherReferenceInfo>
        </ReferenceInfo>
      </DocumentInfo>
      <EventInfo xmlns="http://www.cieca.com/BMS">
        <AssignmentEvent>
          <CreateDateTime>2014-10-24T00:00:00.00000-00:00</CreateDateTime>
        </AssignmentEvent>
      </EventInfo>
      <AdminInfo xmlns="http://www.cieca.com/BMS">
        <InsuranceCompany>
          <Party>
            <OrgInfo>
              <IDInfo>
                <IDQualifierCode>
                  <xsl:value-of select="'CompanyID'"/>
                </IDQualifierCode>
                <IDNum>
                  <xsl:value-of select="$InsuranceCompanyID"/>
                </IDNum>
              </IDInfo>
              <IDInfo>
                <IDQualifierCode>
                  <xsl:value-of select="'UserID'"/>
                </IDQualifierCode>
                <IDNum>
                  <xsl:value-of select="Root/@ClaimOwnerUserID"/>
                </IDNum>
              </IDInfo>
              <IDInfo>
                <IDQualifierCode>
                  <xsl:value-of select="'WAGroupID'"/>
                </IDQualifierCode>
                <IDNum>NA</IDNum>
              </IDInfo>
            </OrgInfo>
          </Party>
        </InsuranceCompany>
        <ClaimOffice>
          <Party>
            <OrgInfo>
              <IDInfo>
                <IDQualifierCode>
                  <!-- xsl:value-of select="'Claim Office'"/ -->
                </IDQualifierCode>
                <IDNum>
                  <!-- xsl:value-of select="Root/Claim/@OfficeID"/ -->
                </IDNum>
              </IDInfo>
            </OrgInfo>
            <PartyType>
              <xsl:value-of select="'Insurance Carrier'"/>
            </PartyType>
          </Party>
          <Affiliation>
            <xsl:value-of select="'IN'"/>
          </Affiliation>
        </ClaimOffice>
        <Owner>
          <Party>
            <PersonInfo>
              <PersonName>
                <FirstName>
                  <xsl:value-of select="/Root/Vehicle/@OwnerNameFirst" />
                </FirstName>
                <LastName>
                  <xsl:value-of select="/Root/Vehicle/@OwnerNameLast" />
                </LastName>
              </PersonName>
            </PersonInfo>
            <ContactInfo>
              <Communications>
                <CommQualifier>
                  <xsl:value-of select="'CP'"/>
                </CommQualifier>
                <CommPhone>
                  <xsl:if test="string-length(/Root/@ClaimOwnerPhone)=0">
                    <xsl:value-of select="'111-111-1111'"/>
                  </xsl:if>
                </CommPhone>
              </Communications>
              <Communications>
                <CommQualifier>
                  <xsl:value-of select="'EM'"/>
                </CommQualifier>
                <CommEmail>
                  <xsl:value-of select="/Root/@ClaimOwnerEmail"/>
                </CommEmail>
              </Communications>
            </ContactInfo>
          </Party>
        </Owner>
        <InspectionSite>
          <Party>
            <OrgInfo>
              <CompanyName>
              </CompanyName>
            </OrgInfo>
            <ContactInfo>
              <Communications>
                <CommQualifier>
                </CommQualifier>
                <Address>
                  <Address1>
                  </Address1>
                  <City>
                  </City>
                  <StateProvince>
                  </StateProvince>
                  <PostalCode>
                  </PostalCode>
                  <CountryCode/>
                </Address>
              </Communications>
            </ContactInfo>
          </Party>
        </InspectionSite>
        <RepairFacility>
          <Party>
            <OrgInfo>
              <CompanyName>
                <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@Name"/>
              </CompanyName>
              <IDInfo>
                <IDQualifierCode>Repair Facility ID</IDQualifierCode>
                <IDNum>
                  <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@ShopID"/>
                </IDNum>
              </IDInfo>
            </OrgInfo>
            <ContactInfo>
              <Communications>
                <CommQualifier>
                  <xsl:value-of select="'SA'"/>
                </CommQualifier>
                <Address>
                  <Address1>
                    <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@Address1"/>
                  </Address1>
                  <City>
                    <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@AddressCity"/>
                  </City>
                  <StateProvince>
                    <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@AddressState"/>
                  </StateProvince>
                  <PostalCode>
                    <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@AddressZip"/>
                  </PostalCode>
                  <CountryCode>USA</CountryCode>
                </Address>
              </Communications>
              <Communications>
                <CommQualifier>WP</CommQualifier>
                <CommPhone>
                  <xsl:value-of select="concat(/Root/ServiceChannel/Assignment/Shop/@PhoneAreaCode, '-',/Root/ServiceChannel/Assignment/Shop/@PhoneExchangeNumber,'-',/Root/ServiceChannel/Assignment/Shop/@PhoneUnitNumber)" />
                </CommPhone>
              </Communications>
              <Communications>
                <CommQualifier>EM</CommQualifier>
                <CommEmail>
                  <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@EmailAddress"/>
                </CommEmail>
              </Communications>
            </ContactInfo>
          </Party>
        </RepairFacility>
        <Adjuster>
          <Party>
            <PersonInfo>
              <PersonName>
                <FirstName>
                  <xsl:value-of select="/Root/@ClaimOwnerFirstName" />
                </FirstName>
                <LastName>
                  <xsl:value-of select="/Root/@ClaimOwnerLastName" />
                </LastName>
              </PersonName>
              <IDInfo>
                <IDQualifierCode>AdjusterCode</IDQualifierCode>
                <IDNum>
                  <xsl:value-of select="/Root/@ClaimOwnerLogonID" />
                </IDNum>
              </IDInfo>
            </PersonInfo>
            <ContactInfo>
              <Communications>
                <CommQualifier>
                  <xsl:value-of select="'WP'"/>
                </CommQualifier>
                <CommPhone>
                  <xsl:value-of select="/Root/@ClaimOwnerPhone"/>
                </CommPhone>
              </Communications>
              <Communications>
                <CommQualifier>EM</CommQualifier>
                <CommEmail>
                  <xsl:value-of select="/Root/@ClaimOwnerEmail"/>
                </CommEmail>
              </Communications>
            </ContactInfo>
          </Party>
        </Adjuster>
      </AdminInfo>

      <ClaimInfo xmlns="http://www.cieca.com/BMS">
        <ClaimNum>
          <xsl:value-of select="$ClaimNumber"/>
        </ClaimNum>
        <PolicyInfo>
          <PolicyNumber></PolicyNumber>
          <PolicyStateProvince></PolicyStateProvince>
        </PolicyInfo>
        <LossInfo>
          <Facts>
            <LossDateTime>
              <xsl:value-of select="Root/@LossDate"/>
            </LossDateTime>
            <ReportedDateTime>
              <xsl:value-of select="Root/@LossDate"/>
            </ReportedDateTime>
            <AirbagDeployedInd>false</AirbagDeployedInd>
            <DamageMemo></DamageMemo>
            <LossMemo></LossMemo>
            <LossLocationAddress>
              <StateProvince></StateProvince>
            </LossLocationAddress>
          </Facts>
          <TotalLossInd></TotalLossInd>
        </LossInfo>
        <AdditionalIDInfo>
          <IDQualifierCode></IDQualifierCode>
          <IDNum></IDNum>
        </AdditionalIDInfo>
      </ClaimInfo>

      <VehicleDamageAssignment xmlns="http://www.cieca.com/BMS">
        <VehicleInfo>
          <VINInfo>
            <VIN>
              <xsl:value-of select="/Root/Vehicle/@VIN"/>
            </VIN>
          </VINInfo>
          <VehicleDesc>
            <ModelYear>
              <xsl:value-of select="/Root/Vehicle/@VehicleYear"/>
            </ModelYear>
            <MakeCode>
              <xsl:value-of select="/Root/Vehicle/@Make"/>
            </MakeCode>
            <ModelName>
              <xsl:value-of select="/Root/Vehicle/@Model"/>
            </ModelName>
            <OdometerInfo/>
          </VehicleDesc>
          <Condition>
            <DrivableInd>
              <xsl:choose>
                <xsl:when test="/Root/Vehicle/@DrivableFlag &gt; 0">
                  <xsl:value-of select="'N'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'Y'"/>
                </xsl:otherwise>
              </xsl:choose>
            </DrivableInd>
          </Condition>
          <VehicleMemo/>
        </VehicleInfo>
        <AdjusterMemo/>
      </VehicleDamageAssignment>
    </AssignmentAddRq>
  </xsl:template>
</xsl:stylesheet>
