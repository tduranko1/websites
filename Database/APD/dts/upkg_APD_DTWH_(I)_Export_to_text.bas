'****************************************************************
'Microsoft SQL Server 2000
'Visual Basic file generated for DTS Package
'File Name: G:\work\Database\APD\dts\upkg_APD_DTWH_(I)_Export_to_text.bas
'Package Name: upkg_APD_DTWH_(I)_Export_to_text
'Package Description: DTS package to export APD Datawarehouse to a text file for import into PPG
'Generated Date: 1/3/2007
'Generated Time: 5:36:15 PM
'****************************************************************

Option Explicit
Public goPackageOld As New DTS.Package
Public goPackage As DTS.Package2
Private Sub Main()
	set goPackage = goPackageOld

	goPackage.Name = "upkg_APD_DTWH_(I)_Export_to_text"
	goPackage.Description = "DTS package to export APD Datawarehouse to a text file for import into PPG"
	goPackage.WriteCompletionStatusToNTEventLog = True
	goPackage.FailOnError = True
	goPackage.PackagePriorityClass = 2
	goPackage.MaxConcurrentSteps = 4
	goPackage.LineageOptions = 0
	goPackage.UseTransaction = True
	goPackage.TransactionIsolationLevel = 4096
	goPackage.AutoCommitTransaction = True
	goPackage.RepositoryMetadataOptions = 0
	goPackage.UseOLEDBServiceComponents = True
	goPackage.LogToSQLServer = True
	goPackage.LogServerName = "(local)"
	goPackage.LogServerFlags = 256
	goPackage.FailPackageOnLogFailure = True
	goPackage.ExplicitGlobalVariables = False
	goPackage.PackageType = 0
	


'---------------------------------------------------------------------------
' create package connection information
'---------------------------------------------------------------------------

Dim oConnection as DTS.Connection2

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("SQLOLEDB")

	oConnection.ConnectionProperties("Integrated Security") = "SSPI"
	oConnection.ConnectionProperties("Persist Security Info") = True
	oConnection.ConnectionProperties("Initial Catalog") = "udb_apd"
	oConnection.ConnectionProperties("Data Source") = "SFTMAPDPRDDB1"
	oConnection.ConnectionProperties("Connect Timeout") = 60
	oConnection.ConnectionProperties("Application Name") = "DTS Designer"
	
	oConnection.Name = "Production APD Database Source"
	oConnection.ID = 5
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "SFTMAPDPRDDB1"
	oConnection.ConnectionTimeout = 60
	oConnection.Catalog = "udb_apd"
	oConnection.UseTrustedConnection = True
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHFCLAIM.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,8,4,4,8,4,4,8,4,8,8,8,8,4,8,8,8,8,8,8,8,8,8,8,8,8,4,4,5,8,8,8,8,8,8,5,4,30,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,5,5,4,8,5,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,16,8,1,8,8,8,8,8,8,8,8,8,8,8,8,20,8,8,8,8,2,4,1,8,1,2,8,8,5,5,5,5,8,5,10,50,50,2,17,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,FactID,AssignmentTypeClosingID,AssignmentTypeID,ClaimAspectID,ClaimLocationID,CoverageTypeID,CustomerID,DispositionTypeID,LynxHandlerAnalystID,LynxHandlerOwnerID,LynxHandlerSupportID,RepairLocationID,ServiceChannelID,TimeIDAssignDownload,TimeIDAssignSent,TimeIDCancelled,TimeIDClosed,TimeIDEstimate,TimeIDNew,TimeIDReclosed,TimeIDRepairComplete,TimeIDRepairCompletePromise,TimeIDRepairStarted,TimeIDReopened,TimeIDVoided,VehicleNumber,VehicleLicensePlateStateID,AuditedEstimateAgreedFlag,AuditedEstimateBettermentAmt,AuditedEstimateDeductibleAmt,AuditedEstimateGrossAmt,AuditedEstimateNetAmt,AuditedEstimateOtherAdjustmentAmt,AuditedEstimatewoBettAmt,CFProgramFlag,ClaimStatusCD,ClientClaimNumber,CycleTimeAssignToEstBusDay,CycleTimeAssignToEstCalDay,CycleTimeAssignToEstHrs,CycleTimeEstToCloseBusDay,CycleTimeEstToCloseCalDay,CycleTimeEstToCloseHrs,CycleTimeNewToCloseBusDay,CycleTimeNewToCloseCalDay,CycleTimeNewToCloseHrs,CycleTimeNewToEstBusDay,CycleTimeNewToEstCalDay,CycleTimeNewToEstHrs,CycleTimeNewToRecloseBusDay,CycleTimeNewToRecloseCalDay,CycleTimeNewToRecloseHrs,CycleTimeRepairPromiseStartToEndBusDay,CycleTimeRepairPromiseStartToEndCalDay,CycleTimeRepairPromiseStartToEndHrs,CycleTimeRepairStartToEndBusDay,CycleTimeRepairStartToEndCalDay,CycleTimeRepairStartToEndHrs,DemoFlag,EnabledFlag,ExposureCD,FeeRevenueAmt,FinalAuditedSuppAgreedFlag,FinalAuditedSuppBettermentAmt,FinalAuditedSuppDeductibleAmt,FinalAuditedSuppGrossAmt,FinalAuditedSuppNetAmt,FinalAuditedSuppOtherAdjustmentAmt,FinalAuditedSuppwoBettAmt,FinalEstimateBettermentAmt,FinalEstimateDeductibleAmt,FinalEstimateGrossAmt,FinalEstimateNetAmt,FinalEstimateOtherAdjustmentAmt,FinalEstimatewoBettAmt,FinalSupplementBettermentAmt,FinalSupplementDeductibleAmt,FinalSupplementGrossAmt,FinalSupplementNetAmt,FinalSupplementOtherAdjustmentAmt,FinalSupplementwoBettAmt,IndemnityAmount,LaborRateBodyAmt,LaborRateFrameAmt,LaborRateMechAmt,LaborRateRefinishAmt,LaborRepairAmt,LaborReplaceAmt,LaborTotalAmt,LossDate,LynxID,MaxEstSuppSequenceNumber,OriginalEstimateBettermentAmt,OriginalEstimateDeductibleAmt,OriginalEstimateGrossAmt,OriginalEstimateNetAmt,OriginalEstimateOtherAdjustmentAmt,OriginalEstimatewoBettAmt,PartsAFMKReplacedAmt,PartsLKQReplacedAmt,PartsOEMDiscountAmt,PartsOEMReplacedAmt,PartsRemanReplacedAmt,PartsTotalReplacedAmt,PolicyNumber,PolicyDeductibleAmt,PolicyLimitAmt,PolicyRentalDeductibleAmt,PolicyRentalLimitAmt,PolicyRentalMaxDays,ProgramCD,ReinspectionCount,ReinspectionDeviationAmt,ReinspectionDeviationCount,RentalDays,RentalAmount,RentalCostPerDay,ServiceLossOfUseFlag,ServiceSubrogationFlag,ServiceTotalLossFlag,ServiceTotalTheftFlag,SupplementTotalAmt,VehicleDriveableFlag,VehicleLicensePlateNumber,VehicleMake,VehicleModel,VehicleYear,VehicleVIN,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 132
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "110000000000000000000000000000000000011000000000000000000000001000000000000000000000000000000000000000000010000010000000000001110111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	
	oConnection.Name = "APD_DTWH_Fact_Claims"
	oConnection.ID = 7
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHFCLAIM.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCUST.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,9,8,50,4,50,50,4,50,50,50,8,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,CustomerID,ClientUserId,InsuranceCompanyID,InsuranceCompanyName,OfficeClientId,OfficeID,OfficeName,RepNameFirst,RepNameLast,UserID,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 14
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "11010110111011"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "00000000000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Customer"
	oConnection.ID = 9
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCUST.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCOVTY.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,4,50,4,50,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,CoverageTypeID,CoverageTypeCD,CoverageTypeDescription,PartyCD,PartyDescription,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 9
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "110111111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "000000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Coverage_Type"
	oConnection.ID = 10
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCOVTY.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDISPT.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,4,50,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,DispositionTypeID,DispositionTypeCD,DispositionTypeDescription,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 7
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "1101111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "0000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Disposition_Type"
	oConnection.ID = 11
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDISPT.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDLHAND.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,8,8,50,50,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,LynxHandlerID,UserID,UserNameFirst,UserNameLast,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 8
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "11001111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "00000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Lynx_Handler"
	oConnection.ID = 12
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDLHAND.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDOCSR.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,50,5,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,DocumentSourceID,DocumentSourceName,ElectronicSourceFlag,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 7
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "1101011"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "0000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Document_Source"
	oConnection.ID = 13
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDOCSR.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDREPLC.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,8,4,4,5,5,30,30,5,5,8,50,50,8,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,RepairLocationID,StandardDocumentSourceID,StateID,CEIProgramFlag,CFProgramFlag,City,County,LYNXSelectProgramFlag,PPGPaintBuyerFlag,ShopLocationID,ShopLocationName,StandardEstimatePackage,ZipCode,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 17
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "11000001100011111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "00000000000000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Repair_Location"
	oConnection.ID = 14
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDREPLC.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSCHNL.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,4,50,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,ServiceChannelID,ServiceChannelCD,ServiceChannelDescription,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 7
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "1101111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "0000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Service_Channel"
	oConnection.ID = 15
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSCHNL.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSTATE.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,2,50,4,50,4,50,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,StateID,StateCode,StateName,CountryCode,CountryName,RegionCD,RegionName,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 11
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "11011111111"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "00000000000"
	
	oConnection.Name = "APD_DTWH_Dimension_State"
	oConnection.ID = 16
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSTATE.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDTIME.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,9,8,16,1,1,2,1,1,1,2,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,TimeID,DateValue,HourValue,DayOfMonth,JulianDay,MonthOfYear,Quarter,WeekOfYear,YearValue,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 13
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "1100000000011"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "0000000000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Time"
	oConnection.ID = 17
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDTIME.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHFEST.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,8,8,8,8,4,8,8,8,8,8,8,8,8,8,19,19,4,8,5,1,19,19,19,8,8,19,8,8,19,8,8,19,8,8,19,8,19,8,8,19,19,8,8,8,8,8,8,8,8,8,19,19,8,8,8,8,8,8,8,8,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,FactID,ClaimAspectID,CustomerID,DocumentSourceID,RepairLocationID,TimeIDAssignment,TimeIDReceived,AdjAppearanceAllowanceAmount,AdjBettermentAmount,AdjDeductibleAmount,AdjOtherAmount,AdjRelatedPriorDamAmount,AdjUnrelatedPriorDamAmount,BodyRepairHrs,BodyReplaceHrs,DaysSinceAssignment,DocumentID,EstimateBalancedFlag,EstSuppSequenceNumber,FrameRepairHrs,FrameReplaceHrs,LaborBodyHours,LaborBodyRate,LaborBodyAmount,LaborFrameHours,LaborFrameRate,LaborFrameAmount,LaborMechHours,LaborMechRate,LaborMechAmount,LaborRefinishHours,LaborRefinishRate,LaborRefinishAmount,LaborTotalHours,LaborTotalAmount,MaterialsHours,MaterialsRate,MaterialsAmount,MechanicalRepairHrs,MechanicalReplaceHrs,NetAmount,OtherAmount,PartDiscountAmount,PartMarkupAmount,PartsNewAmount,PartsLKQAmount,PartsAFMKAmount,PartsRemanAmount,PartsTotalAmount,RefinishRepairHrs,RefinishReplaceHrs,StorageAmount,SubletAmount,TaxLaborAmount,TaxPartsAmount,TaxMaterialsAmount,TaxTotalAmount,TotalAmount,TowingAmount,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 63
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "110000000000000000000000000000000000000000000000000000000000011"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "000000000000000000000000000000000000000000000000000000000000000"
	
	oConnection.Name = "APD_DTWH_Fact_Estimate"
	oConnection.ID = 18
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHFEST.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("DTSFlatFile")

	oConnection.ConnectionProperties("Data Source") = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDATYPE.download"
	oConnection.ConnectionProperties("Mode") = 3
	oConnection.ConnectionProperties("Row Delimiter") = vbCrLf
	oConnection.ConnectionProperties("File Format") = 1
	oConnection.ConnectionProperties("Column Lengths") = "3,10,4,50,4,18,30"
	oConnection.ConnectionProperties("Column Delimiter") = "|"
	oConnection.ConnectionProperties("File Type") = 1
	oConnection.ConnectionProperties("Skip Rows") = 0
	oConnection.ConnectionProperties("Text Qualifier") = """"
	oConnection.ConnectionProperties("First Row Column Name") = False
	oConnection.ConnectionProperties("Column Names") = "data_strip_system,data_strip_file,AssignmentTypeID,AssignmentTypeDescription,ServiceChannelID,data_strip_application,data_strip_date"
	oConnection.ConnectionProperties("Number of Column") = 7
	oConnection.ConnectionProperties("Text Qualifier Col Mask: 0=no, 1=yes, e.g. 0101") = "1101011"
	oConnection.ConnectionProperties("Max characters per delimited column") = 8000
	oConnection.ConnectionProperties("Blob Col Mask: 0=no, 1=yes, e.g. 0101") = "0000000"
	
	oConnection.Name = "APD_DTWH_Dimension_Assignment_Type"
	oConnection.ID = 8
	oConnection.Reusable = True
	oConnection.ConnectImmediate = False
	oConnection.DataSource = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDATYPE.download"
	oConnection.ConnectionTimeout = 60
	oConnection.UseTrustedConnection = False
	oConnection.UseDSL = False
	
	'If you have a password for this connection, please uncomment and add your password below.
	'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'---------------------------------------------------------------------------
' create package steps information
'---------------------------------------------------------------------------

Dim oStep as DTS.Step2
Dim oPrecConstraint as DTS.PrecedenceConstraint

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "Copy Data from Results to Text File Step"
	oStep.Description = "Transform DTWHFCLAIM"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "Copied data in table D:\APD_LynxSelect_shops.txt"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = False
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSExecuteSQLTask_1"
	oStep.Description = "Execute SQL Task: DTWH Meta Data"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSExecuteSQLTask_1"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = False
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = True
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_1"
	oStep.Description = "Transform DTWHDATYPE"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_1"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_2"
	oStep.Description = "Transform DTWHDCUST"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_2"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_3"
	oStep.Description = "Transform DTWHDCOVTY"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_3"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_4"
	oStep.Description = "Transform DTWHDDISPT"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_4"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_5"
	oStep.Description = "Transform DTWHDLHAND"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_5"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_6"
	oStep.Description = "Transform DTWHDDOCSR"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_6"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_7"
	oStep.Description = "Transform DTWHDREPLC"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_7"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_8"
	oStep.Description = "Transform DTWHDSCHNL"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_8"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_9"
	oStep.Description = "Transform DTWHDSTATE"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_9"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_10"
	oStep.Description = "Transform DTWHDTIME"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_10"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSDataPumpTask_11"
	oStep.Description = "Transform DTWHFEST"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSDataPumpTask_11"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = True
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSCreateProcessTask_1"
	oStep.Description = "Execute Process Task: PUT DTWH files"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSCreateProcessTask_1"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = False
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSSendMailTask_1"
	oStep.Description = "Send Mail Task: Completion Notification"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSSendMailTask_1"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = False
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

	oStep.Name = "DTSStep_DTSSendMailTask_2"
	oStep.Description = "Send Mail Task: Completion Notification"
	oStep.ExecutionStatus = 1
	oStep.TaskName = "DTSTask_DTSSendMailTask_2"
	oStep.CommitSuccess = False
	oStep.RollbackFailure = False
	oStep.ScriptLanguage = "VBScript"
	oStep.AddGlobalVariables = True
	oStep.RelativePriority = 3
	oStep.CloseConnection = False
	oStep.ExecuteInMainThread = False
	oStep.IsPackageDSORowset = False
	oStep.JoinTransactionIfPresent = False
	oStep.DisableStep = False
	oStep.FailPackageOnError = False
	
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_11")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_11"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_10")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_10"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_9")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_9"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_8")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_8"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_7")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_7"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_6")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_6"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_5")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_5"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_4")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_4"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_3")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_3"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_2")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_2"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDataPumpTask_1")
	oPrecConstraint.StepName = "DTSStep_DTSDataPumpTask_1"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSCreateProcessTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("Copy Data from Results to Text File Step")
	oPrecConstraint.StepName = "Copy Data from Results to Text File Step"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSSendMailTask_1")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSCreateProcessTask_1")
	oPrecConstraint.StepName = "DTSStep_DTSCreateProcessTask_1"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 0
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSSendMailTask_2")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSCreateProcessTask_1")
	oPrecConstraint.StepName = "DTSStep_DTSCreateProcessTask_1"
	oPrecConstraint.PrecedenceBasis = 1
	oPrecConstraint.Value = 1
	
oStep.precedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'---------------------------------------------------------------------------
' create package tasks information
'---------------------------------------------------------------------------

'------------- call Task_Sub1 for task Copied data in table D:\APD_LynxSelect_shops.txt (Transform DTWHFCLAIM)
Call Task_Sub1( goPackage	)

'------------- call Task_Sub2 for task DTSTask_DTSExecuteSQLTask_1 (Execute SQL Task: DTWH Meta Data)
Call Task_Sub2( goPackage	)

'------------- call Task_Sub3 for task DTSTask_DTSDataPumpTask_1 (Transform DTWHDATYPE)
Call Task_Sub3( goPackage	)

'------------- call Task_Sub4 for task DTSTask_DTSDataPumpTask_2 (Transform DTWHDCUST)
Call Task_Sub4( goPackage	)

'------------- call Task_Sub5 for task DTSTask_DTSDataPumpTask_3 (Transform DTWHDCOVTY)
Call Task_Sub5( goPackage	)

'------------- call Task_Sub6 for task DTSTask_DTSDataPumpTask_4 (Transform DTWHDDISPT)
Call Task_Sub6( goPackage	)

'------------- call Task_Sub7 for task DTSTask_DTSDataPumpTask_5 (Transform DTWHDLHAND)
Call Task_Sub7( goPackage	)

'------------- call Task_Sub8 for task DTSTask_DTSDataPumpTask_6 (Transform DTWHDDOCSR)
Call Task_Sub8( goPackage	)

'------------- call Task_Sub9 for task DTSTask_DTSDataPumpTask_7 (Transform DTWHDREPLC)
Call Task_Sub9( goPackage	)

'------------- call Task_Sub10 for task DTSTask_DTSDataPumpTask_8 (Transform DTWHDSCHNL)
Call Task_Sub10( goPackage	)

'------------- call Task_Sub11 for task DTSTask_DTSDataPumpTask_9 (Transform DTWHDSTATE)
Call Task_Sub11( goPackage	)

'------------- call Task_Sub12 for task DTSTask_DTSDataPumpTask_10 (Transform DTWHDTIME)
Call Task_Sub12( goPackage	)

'------------- call Task_Sub13 for task DTSTask_DTSDataPumpTask_11 (Transform DTWHFEST)
Call Task_Sub13( goPackage	)

'------------- call Task_Sub14 for task DTSTask_DTSCreateProcessTask_1 (Execute Process Task: PUT DTWH files)
Call Task_Sub14( goPackage	)

'------------- call Task_Sub15 for task DTSTask_DTSSendMailTask_1 (Send Mail Task: Completion Notification)
Call Task_Sub15( goPackage	)

'------------- call Task_Sub16 for task DTSTask_DTSSendMailTask_2 (Send Mail Task: Completion Notification)
Call Task_Sub16( goPackage	)

'---------------------------------------------------------------------------
' Save or execute package
'---------------------------------------------------------------------------

'goPackage.SaveToSQLServer "(local)", "sa", ""
goPackage.Execute
tracePackageError goPackage
goPackage.Uninitialize
'to save a package instead of executing it, comment out the executing package lines above and uncomment the saving package line
set goPackage = Nothing

set goPackageOld = Nothing

End Sub


'-----------------------------------------------------------------------------
' error reporting using step.GetExecutionErrorInfo after execution
'-----------------------------------------------------------------------------
Public Sub tracePackageError(oPackage As DTS.Package)
Dim ErrorCode As Long
Dim ErrorSource As String
Dim ErrorDescription As String
Dim ErrorHelpFile As String
Dim ErrorHelpContext As Long
Dim ErrorIDofInterfaceWithError As String
Dim i As Integer

	For i = 1 To oPackage.Steps.Count
		If oPackage.Steps(i).ExecutionResult = DTSStepExecResult_Failure Then
			oPackage.Steps(i).GetExecutionErrorInfo ErrorCode, ErrorSource, ErrorDescription, _
					ErrorHelpFile, ErrorHelpContext, ErrorIDofInterfaceWithError
			MsgBox oPackage.Steps(i).Name & " failed" & vbCrLf & ErrorSource & vbCrLf & ErrorDescription
		End If
	Next i

End Sub

'------------- define Task_Sub1 for task Copied data in table D:\APD_LynxSelect_shops.txt (Transform DTWHFCLAIM)
Public Sub Task_Sub1(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask1 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "Copied data in table D:\APD_LynxSelect_shops.txt"
Set oCustomTask1 = oTask.CustomTask

	oCustomTask1.Name = "Copied data in table D:\APD_LynxSelect_shops.txt"
	oCustomTask1.Description = "Transform DTWHFCLAIM"
	oCustomTask1.SourceConnectionID = 5
	oCustomTask1.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "       'DTWHFCLAIM' AS data_strip_file," & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "  FROM utb_dtwh_fact_claim" & vbCrLf
	oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & " "
	oCustomTask1.DestinationConnectionID = 7
	oCustomTask1.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\APD_DTWH_Fact_Claims.txt"
	oCustomTask1.ProgressRowCount = 1000
	oCustomTask1.MaximumErrorCount = 0
	oCustomTask1.FetchBufferSize = 1
	oCustomTask1.UseFastLoad = True
	oCustomTask1.InsertCommitSize = 0
	oCustomTask1.ExceptionFileColumnDelimiter = "|"
	oCustomTask1.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask1.AllowIdentityInserts = False
	oCustomTask1.FirstRow = "0"
	oCustomTask1.LastRow = "0"
	oCustomTask1.FastLoadOptions = 2
	oCustomTask1.ExceptionFileOptions = 1
	oCustomTask1.DataPumpOptions = 0
	
Call oCustomTask1_Trans_Sub1( oCustomTask1 )
Call oCustomTask1_Trans_Sub2( oCustomTask1 )
Call oCustomTask1_Trans_Sub3( oCustomTask1 )
Call oCustomTask1_Trans_Sub4( oCustomTask1 )
Call oCustomTask1_Trans_Sub5( oCustomTask1 )
Call oCustomTask1_Trans_Sub6( oCustomTask1 )
Call oCustomTask1_Trans_Sub7( oCustomTask1 )
Call oCustomTask1_Trans_Sub8( oCustomTask1 )
Call oCustomTask1_Trans_Sub9( oCustomTask1 )
Call oCustomTask1_Trans_Sub10( oCustomTask1 )
Call oCustomTask1_Trans_Sub11( oCustomTask1 )
Call oCustomTask1_Trans_Sub12( oCustomTask1 )
Call oCustomTask1_Trans_Sub13( oCustomTask1 )
Call oCustomTask1_Trans_Sub14( oCustomTask1 )
Call oCustomTask1_Trans_Sub15( oCustomTask1 )
Call oCustomTask1_Trans_Sub16( oCustomTask1 )
Call oCustomTask1_Trans_Sub17( oCustomTask1 )
Call oCustomTask1_Trans_Sub18( oCustomTask1 )
Call oCustomTask1_Trans_Sub19( oCustomTask1 )
Call oCustomTask1_Trans_Sub20( oCustomTask1 )
Call oCustomTask1_Trans_Sub21( oCustomTask1 )
Call oCustomTask1_Trans_Sub22( oCustomTask1 )
Call oCustomTask1_Trans_Sub23( oCustomTask1 )
Call oCustomTask1_Trans_Sub24( oCustomTask1 )
Call oCustomTask1_Trans_Sub25( oCustomTask1 )
Call oCustomTask1_Trans_Sub26( oCustomTask1 )
Call oCustomTask1_Trans_Sub27( oCustomTask1 )
Call oCustomTask1_Trans_Sub28( oCustomTask1 )
Call oCustomTask1_Trans_Sub29( oCustomTask1 )
Call oCustomTask1_Trans_Sub30( oCustomTask1 )
Call oCustomTask1_Trans_Sub31( oCustomTask1 )
Call oCustomTask1_Trans_Sub32( oCustomTask1 )
Call oCustomTask1_Trans_Sub33( oCustomTask1 )
Call oCustomTask1_Trans_Sub34( oCustomTask1 )
Call oCustomTask1_Trans_Sub35( oCustomTask1 )
Call oCustomTask1_Trans_Sub36( oCustomTask1 )
Call oCustomTask1_Trans_Sub37( oCustomTask1 )
Call oCustomTask1_Trans_Sub38( oCustomTask1 )
Call oCustomTask1_Trans_Sub39( oCustomTask1 )
Call oCustomTask1_Trans_Sub40( oCustomTask1 )
Call oCustomTask1_Trans_Sub41( oCustomTask1 )
Call oCustomTask1_Trans_Sub42( oCustomTask1 )
Call oCustomTask1_Trans_Sub43( oCustomTask1 )
Call oCustomTask1_Trans_Sub44( oCustomTask1 )
Call oCustomTask1_Trans_Sub45( oCustomTask1 )
Call oCustomTask1_Trans_Sub46( oCustomTask1 )
Call oCustomTask1_Trans_Sub47( oCustomTask1 )
Call oCustomTask1_Trans_Sub48( oCustomTask1 )
Call oCustomTask1_Trans_Sub49( oCustomTask1 )
Call oCustomTask1_Trans_Sub50( oCustomTask1 )
Call oCustomTask1_Trans_Sub51( oCustomTask1 )
Call oCustomTask1_Trans_Sub52( oCustomTask1 )
Call oCustomTask1_Trans_Sub53( oCustomTask1 )
Call oCustomTask1_Trans_Sub54( oCustomTask1 )
Call oCustomTask1_Trans_Sub55( oCustomTask1 )
Call oCustomTask1_Trans_Sub56( oCustomTask1 )
Call oCustomTask1_Trans_Sub57( oCustomTask1 )
Call oCustomTask1_Trans_Sub58( oCustomTask1 )
Call oCustomTask1_Trans_Sub59( oCustomTask1 )
Call oCustomTask1_Trans_Sub60( oCustomTask1 )
Call oCustomTask1_Trans_Sub61( oCustomTask1 )
Call oCustomTask1_Trans_Sub62( oCustomTask1 )
Call oCustomTask1_Trans_Sub63( oCustomTask1 )
Call oCustomTask1_Trans_Sub64( oCustomTask1 )
Call oCustomTask1_Trans_Sub65( oCustomTask1 )
Call oCustomTask1_Trans_Sub66( oCustomTask1 )
Call oCustomTask1_Trans_Sub67( oCustomTask1 )
Call oCustomTask1_Trans_Sub68( oCustomTask1 )
Call oCustomTask1_Trans_Sub69( oCustomTask1 )
Call oCustomTask1_Trans_Sub70( oCustomTask1 )
Call oCustomTask1_Trans_Sub71( oCustomTask1 )
Call oCustomTask1_Trans_Sub72( oCustomTask1 )
Call oCustomTask1_Trans_Sub73( oCustomTask1 )
Call oCustomTask1_Trans_Sub74( oCustomTask1 )
Call oCustomTask1_Trans_Sub75( oCustomTask1 )
Call oCustomTask1_Trans_Sub76( oCustomTask1 )
Call oCustomTask1_Trans_Sub77( oCustomTask1 )
Call oCustomTask1_Trans_Sub78( oCustomTask1 )
Call oCustomTask1_Trans_Sub79( oCustomTask1 )
Call oCustomTask1_Trans_Sub80( oCustomTask1 )
Call oCustomTask1_Trans_Sub81( oCustomTask1 )
Call oCustomTask1_Trans_Sub82( oCustomTask1 )
Call oCustomTask1_Trans_Sub83( oCustomTask1 )
Call oCustomTask1_Trans_Sub84( oCustomTask1 )
Call oCustomTask1_Trans_Sub85( oCustomTask1 )
Call oCustomTask1_Trans_Sub86( oCustomTask1 )
Call oCustomTask1_Trans_Sub87( oCustomTask1 )
Call oCustomTask1_Trans_Sub88( oCustomTask1 )
Call oCustomTask1_Trans_Sub89( oCustomTask1 )
Call oCustomTask1_Trans_Sub90( oCustomTask1 )
Call oCustomTask1_Trans_Sub91( oCustomTask1 )
Call oCustomTask1_Trans_Sub92( oCustomTask1 )
Call oCustomTask1_Trans_Sub93( oCustomTask1 )
Call oCustomTask1_Trans_Sub94( oCustomTask1 )
Call oCustomTask1_Trans_Sub95( oCustomTask1 )
Call oCustomTask1_Trans_Sub96( oCustomTask1 )
Call oCustomTask1_Trans_Sub97( oCustomTask1 )
Call oCustomTask1_Trans_Sub98( oCustomTask1 )
Call oCustomTask1_Trans_Sub99( oCustomTask1 )
Call oCustomTask1_Trans_Sub100( oCustomTask1 )
Call oCustomTask1_Trans_Sub101( oCustomTask1 )
Call oCustomTask1_Trans_Sub102( oCustomTask1 )
Call oCustomTask1_Trans_Sub103( oCustomTask1 )
Call oCustomTask1_Trans_Sub104( oCustomTask1 )
Call oCustomTask1_Trans_Sub105( oCustomTask1 )
Call oCustomTask1_Trans_Sub106( oCustomTask1 )
Call oCustomTask1_Trans_Sub107( oCustomTask1 )
Call oCustomTask1_Trans_Sub108( oCustomTask1 )
Call oCustomTask1_Trans_Sub109( oCustomTask1 )
Call oCustomTask1_Trans_Sub110( oCustomTask1 )
Call oCustomTask1_Trans_Sub111( oCustomTask1 )
Call oCustomTask1_Trans_Sub112( oCustomTask1 )
Call oCustomTask1_Trans_Sub113( oCustomTask1 )
Call oCustomTask1_Trans_Sub114( oCustomTask1 )
Call oCustomTask1_Trans_Sub115( oCustomTask1 )
Call oCustomTask1_Trans_Sub116( oCustomTask1 )
Call oCustomTask1_Trans_Sub117( oCustomTask1 )
Call oCustomTask1_Trans_Sub118( oCustomTask1 )
Call oCustomTask1_Trans_Sub119( oCustomTask1 )
Call oCustomTask1_Trans_Sub120( oCustomTask1 )
Call oCustomTask1_Trans_Sub121( oCustomTask1 )
Call oCustomTask1_Trans_Sub122( oCustomTask1 )
Call oCustomTask1_Trans_Sub123( oCustomTask1 )
Call oCustomTask1_Trans_Sub124( oCustomTask1 )
Call oCustomTask1_Trans_Sub125( oCustomTask1 )
Call oCustomTask1_Trans_Sub126( oCustomTask1 )
Call oCustomTask1_Trans_Sub127( oCustomTask1 )
Call oCustomTask1_Trans_Sub128( oCustomTask1 )
Call oCustomTask1_Trans_Sub129( oCustomTask1 )
Call oCustomTask1_Trans_Sub130( oCustomTask1 )
Call oCustomTask1_Trans_Sub131( oCustomTask1 )
Call oCustomTask1_Trans_Sub132( oCustomTask1 )
goPackage.Tasks.Add oTask
Set oCustomTask1 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub1(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub2(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub3(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FactID" , 1)
			oColumn.Name = "FactID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FactID" , 1)
			oColumn.Name = "FactID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub4(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AssignmentTypeClosingID" , 1)
			oColumn.Name = "AssignmentTypeClosingID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AssignmentTypeClosingID" , 1)
			oColumn.Name = "AssignmentTypeClosingID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub5(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AssignmentTypeID" , 1)
			oColumn.Name = "AssignmentTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AssignmentTypeID" , 1)
			oColumn.Name = "AssignmentTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub6(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClaimAspectID" , 1)
			oColumn.Name = "ClaimAspectID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClaimAspectID" , 1)
			oColumn.Name = "ClaimAspectID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub7(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClaimLocationID" , 1)
			oColumn.Name = "ClaimLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClaimLocationID" , 1)
			oColumn.Name = "ClaimLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub8(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CoverageTypeID" , 1)
			oColumn.Name = "CoverageTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CoverageTypeID" , 1)
			oColumn.Name = "CoverageTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub9(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub10(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DispositionTypeID" , 1)
			oColumn.Name = "DispositionTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DispositionTypeID" , 1)
			oColumn.Name = "DispositionTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub11(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LynxHandlerAnalystID" , 1)
			oColumn.Name = "LynxHandlerAnalystID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LynxHandlerAnalystID" , 1)
			oColumn.Name = "LynxHandlerAnalystID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub12(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__12"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LynxHandlerOwnerID" , 1)
			oColumn.Name = "LynxHandlerOwnerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LynxHandlerOwnerID" , 1)
			oColumn.Name = "LynxHandlerOwnerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub13(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__13"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LynxHandlerSupportID" , 1)
			oColumn.Name = "LynxHandlerSupportID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LynxHandlerSupportID" , 1)
			oColumn.Name = "LynxHandlerSupportID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub14(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__14"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub15(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__15"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub16(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__16"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDAssignDownload" , 1)
			oColumn.Name = "TimeIDAssignDownload"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDAssignDownload" , 1)
			oColumn.Name = "TimeIDAssignDownload"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub17(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__17"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDAssignSent" , 1)
			oColumn.Name = "TimeIDAssignSent"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDAssignSent" , 1)
			oColumn.Name = "TimeIDAssignSent"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub18(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__18"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDCancelled" , 1)
			oColumn.Name = "TimeIDCancelled"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDCancelled" , 1)
			oColumn.Name = "TimeIDCancelled"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub19(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__19"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDClosed" , 1)
			oColumn.Name = "TimeIDClosed"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDClosed" , 1)
			oColumn.Name = "TimeIDClosed"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub20(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__20"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDEstimate" , 1)
			oColumn.Name = "TimeIDEstimate"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDEstimate" , 1)
			oColumn.Name = "TimeIDEstimate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub21(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__21"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDNew" , 1)
			oColumn.Name = "TimeIDNew"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDNew" , 1)
			oColumn.Name = "TimeIDNew"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub22(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__22"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDReclosed" , 1)
			oColumn.Name = "TimeIDReclosed"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDReclosed" , 1)
			oColumn.Name = "TimeIDReclosed"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub23(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__23"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDRepairComplete" , 1)
			oColumn.Name = "TimeIDRepairComplete"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDRepairComplete" , 1)
			oColumn.Name = "TimeIDRepairComplete"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub24(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__24"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDRepairCompletePromise" , 1)
			oColumn.Name = "TimeIDRepairCompletePromise"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDRepairCompletePromise" , 1)
			oColumn.Name = "TimeIDRepairCompletePromise"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub25(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__25"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDRepairStarted" , 1)
			oColumn.Name = "TimeIDRepairStarted"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDRepairStarted" , 1)
			oColumn.Name = "TimeIDRepairStarted"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub26(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__26"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDReopened" , 1)
			oColumn.Name = "TimeIDReopened"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDReopened" , 1)
			oColumn.Name = "TimeIDReopened"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub27(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__27"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDVoided" , 1)
			oColumn.Name = "TimeIDVoided"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDVoided" , 1)
			oColumn.Name = "TimeIDVoided"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub28(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__28"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleNumber" , 1)
			oColumn.Name = "VehicleNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleNumber" , 1)
			oColumn.Name = "VehicleNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub29(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__29"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleLicensePlateStateID" , 1)
			oColumn.Name = "VehicleLicensePlateStateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleLicensePlateStateID" , 1)
			oColumn.Name = "VehicleLicensePlateStateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub30(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__30"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateAgreedFlag" , 1)
			oColumn.Name = "AuditedEstimateAgreedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateAgreedFlag" , 1)
			oColumn.Name = "AuditedEstimateAgreedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub31(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__31"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateBettermentAmt" , 1)
			oColumn.Name = "AuditedEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateBettermentAmt" , 1)
			oColumn.Name = "AuditedEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub32(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__32"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateDeductibleAmt" , 1)
			oColumn.Name = "AuditedEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateDeductibleAmt" , 1)
			oColumn.Name = "AuditedEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub33(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__33"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateGrossAmt" , 1)
			oColumn.Name = "AuditedEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateGrossAmt" , 1)
			oColumn.Name = "AuditedEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub34(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__34"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateNetAmt" , 1)
			oColumn.Name = "AuditedEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateNetAmt" , 1)
			oColumn.Name = "AuditedEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub35(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__35"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "AuditedEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "AuditedEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub36(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__36"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AuditedEstimatewoBettAmt" , 1)
			oColumn.Name = "AuditedEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AuditedEstimatewoBettAmt" , 1)
			oColumn.Name = "AuditedEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub37(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__37"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CFProgramFlag" , 1)
			oColumn.Name = "CFProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CFProgramFlag" , 1)
			oColumn.Name = "CFProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub38(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__38"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClaimStatusCD" , 1)
			oColumn.Name = "ClaimStatusCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClaimStatusCD" , 1)
			oColumn.Name = "ClaimStatusCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub39(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__39"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClientClaimNumber" , 1)
			oColumn.Name = "ClientClaimNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClientClaimNumber" , 1)
			oColumn.Name = "ClientClaimNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub40(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__40"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeAssignToEstBusDay" , 1)
			oColumn.Name = "CycleTimeAssignToEstBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeAssignToEstBusDay" , 1)
			oColumn.Name = "CycleTimeAssignToEstBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub41(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__41"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeAssignToEstCalDay" , 1)
			oColumn.Name = "CycleTimeAssignToEstCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeAssignToEstCalDay" , 1)
			oColumn.Name = "CycleTimeAssignToEstCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub42(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__42"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeAssignToEstHrs" , 1)
			oColumn.Name = "CycleTimeAssignToEstHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeAssignToEstHrs" , 1)
			oColumn.Name = "CycleTimeAssignToEstHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub43(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__43"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeEstToCloseBusDay" , 1)
			oColumn.Name = "CycleTimeEstToCloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeEstToCloseBusDay" , 1)
			oColumn.Name = "CycleTimeEstToCloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub44(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__44"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeEstToCloseCalDay" , 1)
			oColumn.Name = "CycleTimeEstToCloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeEstToCloseCalDay" , 1)
			oColumn.Name = "CycleTimeEstToCloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub45(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__45"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeEstToCloseHrs" , 1)
			oColumn.Name = "CycleTimeEstToCloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeEstToCloseHrs" , 1)
			oColumn.Name = "CycleTimeEstToCloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub46(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__46"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToCloseBusDay" , 1)
			oColumn.Name = "CycleTimeNewToCloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToCloseBusDay" , 1)
			oColumn.Name = "CycleTimeNewToCloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub47(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__47"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToCloseCalDay" , 1)
			oColumn.Name = "CycleTimeNewToCloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToCloseCalDay" , 1)
			oColumn.Name = "CycleTimeNewToCloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub48(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__48"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToCloseHrs" , 1)
			oColumn.Name = "CycleTimeNewToCloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToCloseHrs" , 1)
			oColumn.Name = "CycleTimeNewToCloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub49(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__49"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToEstBusDay" , 1)
			oColumn.Name = "CycleTimeNewToEstBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToEstBusDay" , 1)
			oColumn.Name = "CycleTimeNewToEstBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub50(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__50"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToEstCalDay" , 1)
			oColumn.Name = "CycleTimeNewToEstCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToEstCalDay" , 1)
			oColumn.Name = "CycleTimeNewToEstCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub51(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__51"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToEstHrs" , 1)
			oColumn.Name = "CycleTimeNewToEstHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToEstHrs" , 1)
			oColumn.Name = "CycleTimeNewToEstHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub52(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__52"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToRecloseBusDay" , 1)
			oColumn.Name = "CycleTimeNewToRecloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToRecloseBusDay" , 1)
			oColumn.Name = "CycleTimeNewToRecloseBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub53(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__53"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToRecloseCalDay" , 1)
			oColumn.Name = "CycleTimeNewToRecloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToRecloseCalDay" , 1)
			oColumn.Name = "CycleTimeNewToRecloseCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub54(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__54"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeNewToRecloseHrs" , 1)
			oColumn.Name = "CycleTimeNewToRecloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeNewToRecloseHrs" , 1)
			oColumn.Name = "CycleTimeNewToRecloseHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub55(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__55"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairPromiseStartToEndBusDay" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairPromiseStartToEndBusDay" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub56(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__56"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairPromiseStartToEndCalDay" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairPromiseStartToEndCalDay" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub57(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__57"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairPromiseStartToEndHrs" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairPromiseStartToEndHrs" , 1)
			oColumn.Name = "CycleTimeRepairPromiseStartToEndHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub58(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__58"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairStartToEndBusDay" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairStartToEndBusDay" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndBusDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub59(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__59"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairStartToEndCalDay" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairStartToEndCalDay" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndCalDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub60(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__60"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CycleTimeRepairStartToEndHrs" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CycleTimeRepairStartToEndHrs" , 1)
			oColumn.Name = "CycleTimeRepairStartToEndHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub61(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__61"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DemoFlag" , 1)
			oColumn.Name = "DemoFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DemoFlag" , 1)
			oColumn.Name = "DemoFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub62(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__62"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("EnabledFlag" , 1)
			oColumn.Name = "EnabledFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("EnabledFlag" , 1)
			oColumn.Name = "EnabledFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub63(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__63"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ExposureCD" , 1)
			oColumn.Name = "ExposureCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ExposureCD" , 1)
			oColumn.Name = "ExposureCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub64(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__64"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FeeRevenueAmt" , 1)
			oColumn.Name = "FeeRevenueAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FeeRevenueAmt" , 1)
			oColumn.Name = "FeeRevenueAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub65(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__65"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppAgreedFlag" , 1)
			oColumn.Name = "FinalAuditedSuppAgreedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppAgreedFlag" , 1)
			oColumn.Name = "FinalAuditedSuppAgreedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub66(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__66"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppBettermentAmt" , 1)
			oColumn.Name = "FinalAuditedSuppBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppBettermentAmt" , 1)
			oColumn.Name = "FinalAuditedSuppBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub67(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__67"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppDeductibleAmt" , 1)
			oColumn.Name = "FinalAuditedSuppDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppDeductibleAmt" , 1)
			oColumn.Name = "FinalAuditedSuppDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub68(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__68"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppGrossAmt" , 1)
			oColumn.Name = "FinalAuditedSuppGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppGrossAmt" , 1)
			oColumn.Name = "FinalAuditedSuppGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub69(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__69"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppNetAmt" , 1)
			oColumn.Name = "FinalAuditedSuppNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppNetAmt" , 1)
			oColumn.Name = "FinalAuditedSuppNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub70(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__70"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalAuditedSuppOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalAuditedSuppOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub71(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__71"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalAuditedSuppwoBettAmt" , 1)
			oColumn.Name = "FinalAuditedSuppwoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalAuditedSuppwoBettAmt" , 1)
			oColumn.Name = "FinalAuditedSuppwoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub72(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__72"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimateBettermentAmt" , 1)
			oColumn.Name = "FinalEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimateBettermentAmt" , 1)
			oColumn.Name = "FinalEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub73(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__73"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimateDeductibleAmt" , 1)
			oColumn.Name = "FinalEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimateDeductibleAmt" , 1)
			oColumn.Name = "FinalEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub74(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__74"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimateGrossAmt" , 1)
			oColumn.Name = "FinalEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimateGrossAmt" , 1)
			oColumn.Name = "FinalEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub75(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__75"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimateNetAmt" , 1)
			oColumn.Name = "FinalEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimateNetAmt" , 1)
			oColumn.Name = "FinalEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub76(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__76"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub77(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__77"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalEstimatewoBettAmt" , 1)
			oColumn.Name = "FinalEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalEstimatewoBettAmt" , 1)
			oColumn.Name = "FinalEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub78(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__78"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementBettermentAmt" , 1)
			oColumn.Name = "FinalSupplementBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementBettermentAmt" , 1)
			oColumn.Name = "FinalSupplementBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub79(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__79"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementDeductibleAmt" , 1)
			oColumn.Name = "FinalSupplementDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementDeductibleAmt" , 1)
			oColumn.Name = "FinalSupplementDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub80(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__80"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementGrossAmt" , 1)
			oColumn.Name = "FinalSupplementGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementGrossAmt" , 1)
			oColumn.Name = "FinalSupplementGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub81(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__81"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementNetAmt" , 1)
			oColumn.Name = "FinalSupplementNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementNetAmt" , 1)
			oColumn.Name = "FinalSupplementNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub82(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__82"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalSupplementOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementOtherAdjustmentAmt" , 1)
			oColumn.Name = "FinalSupplementOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub83(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__83"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FinalSupplementwoBettAmt" , 1)
			oColumn.Name = "FinalSupplementwoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FinalSupplementwoBettAmt" , 1)
			oColumn.Name = "FinalSupplementwoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub84(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__84"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("IndemnityAmount" , 1)
			oColumn.Name = "IndemnityAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("IndemnityAmount" , 1)
			oColumn.Name = "IndemnityAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub85(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__85"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRateBodyAmt" , 1)
			oColumn.Name = "LaborRateBodyAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRateBodyAmt" , 1)
			oColumn.Name = "LaborRateBodyAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub86(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__86"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRateFrameAmt" , 1)
			oColumn.Name = "LaborRateFrameAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRateFrameAmt" , 1)
			oColumn.Name = "LaborRateFrameAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub87(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__87"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRateMechAmt" , 1)
			oColumn.Name = "LaborRateMechAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRateMechAmt" , 1)
			oColumn.Name = "LaborRateMechAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub88(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__88"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRateRefinishAmt" , 1)
			oColumn.Name = "LaborRateRefinishAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRateRefinishAmt" , 1)
			oColumn.Name = "LaborRateRefinishAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub89(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__89"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRepairAmt" , 1)
			oColumn.Name = "LaborRepairAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRepairAmt" , 1)
			oColumn.Name = "LaborRepairAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub90(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__90"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborReplaceAmt" , 1)
			oColumn.Name = "LaborReplaceAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborReplaceAmt" , 1)
			oColumn.Name = "LaborReplaceAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub91(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__91"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborTotalAmt" , 1)
			oColumn.Name = "LaborTotalAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborTotalAmt" , 1)
			oColumn.Name = "LaborTotalAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub92(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__92"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LossDate" , 1)
			oColumn.Name = "LossDate"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 135
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LossDate" , 1)
			oColumn.Name = "LossDate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 16
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub93(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__93"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LynxID" , 1)
			oColumn.Name = "LynxID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LynxID" , 1)
			oColumn.Name = "LynxID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub94(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__94"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MaxEstSuppSequenceNumber" , 1)
			oColumn.Name = "MaxEstSuppSequenceNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MaxEstSuppSequenceNumber" , 1)
			oColumn.Name = "MaxEstSuppSequenceNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub95(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__95"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimateBettermentAmt" , 1)
			oColumn.Name = "OriginalEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimateBettermentAmt" , 1)
			oColumn.Name = "OriginalEstimateBettermentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub96(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__96"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimateDeductibleAmt" , 1)
			oColumn.Name = "OriginalEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimateDeductibleAmt" , 1)
			oColumn.Name = "OriginalEstimateDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub97(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__97"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimateGrossAmt" , 1)
			oColumn.Name = "OriginalEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimateGrossAmt" , 1)
			oColumn.Name = "OriginalEstimateGrossAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub98(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__98"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimateNetAmt" , 1)
			oColumn.Name = "OriginalEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimateNetAmt" , 1)
			oColumn.Name = "OriginalEstimateNetAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub99(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__99"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "OriginalEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimateOtherAdjustmentAmt" , 1)
			oColumn.Name = "OriginalEstimateOtherAdjustmentAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub100(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__100"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OriginalEstimatewoBettAmt" , 1)
			oColumn.Name = "OriginalEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OriginalEstimatewoBettAmt" , 1)
			oColumn.Name = "OriginalEstimatewoBettAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub101(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__101"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsAFMKReplacedAmt" , 1)
			oColumn.Name = "PartsAFMKReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsAFMKReplacedAmt" , 1)
			oColumn.Name = "PartsAFMKReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub102(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__102"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsLKQReplacedAmt" , 1)
			oColumn.Name = "PartsLKQReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsLKQReplacedAmt" , 1)
			oColumn.Name = "PartsLKQReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub103(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__103"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsOEMDiscountAmt" , 1)
			oColumn.Name = "PartsOEMDiscountAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsOEMDiscountAmt" , 1)
			oColumn.Name = "PartsOEMDiscountAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub104(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__104"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsOEMReplacedAmt" , 1)
			oColumn.Name = "PartsOEMReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsOEMReplacedAmt" , 1)
			oColumn.Name = "PartsOEMReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub105(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__105"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsRemanReplacedAmt" , 1)
			oColumn.Name = "PartsRemanReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsRemanReplacedAmt" , 1)
			oColumn.Name = "PartsRemanReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub106(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__106"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsTotalReplacedAmt" , 1)
			oColumn.Name = "PartsTotalReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsTotalReplacedAmt" , 1)
			oColumn.Name = "PartsTotalReplacedAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub107(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__107"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyNumber" , 1)
			oColumn.Name = "PolicyNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 20
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyNumber" , 1)
			oColumn.Name = "PolicyNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 20
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub108(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__108"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyDeductibleAmt" , 1)
			oColumn.Name = "PolicyDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyDeductibleAmt" , 1)
			oColumn.Name = "PolicyDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub109(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__109"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyLimitAmt" , 1)
			oColumn.Name = "PolicyLimitAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyLimitAmt" , 1)
			oColumn.Name = "PolicyLimitAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub110(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__110"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyRentalDeductibleAmt" , 1)
			oColumn.Name = "PolicyRentalDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyRentalDeductibleAmt" , 1)
			oColumn.Name = "PolicyRentalDeductibleAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub111(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__111"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyRentalLimitAmt" , 1)
			oColumn.Name = "PolicyRentalLimitAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyRentalLimitAmt" , 1)
			oColumn.Name = "PolicyRentalLimitAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub112(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__112"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PolicyRentalMaxDays" , 1)
			oColumn.Name = "PolicyRentalMaxDays"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PolicyRentalMaxDays" , 1)
			oColumn.Name = "PolicyRentalMaxDays"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub113(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__113"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ProgramCD" , 1)
			oColumn.Name = "ProgramCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ProgramCD" , 1)
			oColumn.Name = "ProgramCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub114(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__114"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ReinspectionCount" , 1)
			oColumn.Name = "ReinspectionCount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ReinspectionCount" , 1)
			oColumn.Name = "ReinspectionCount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub115(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__115"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ReinspectionDeviationAmt" , 1)
			oColumn.Name = "ReinspectionDeviationAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ReinspectionDeviationAmt" , 1)
			oColumn.Name = "ReinspectionDeviationAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub116(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__116"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ReinspectionDeviationCount" , 1)
			oColumn.Name = "ReinspectionDeviationCount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ReinspectionDeviationCount" , 1)
			oColumn.Name = "ReinspectionDeviationCount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub117(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__117"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RentalDays" , 1)
			oColumn.Name = "RentalDays"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RentalDays" , 1)
			oColumn.Name = "RentalDays"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub118(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__118"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RentalAmount" , 1)
			oColumn.Name = "RentalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RentalAmount" , 1)
			oColumn.Name = "RentalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub119(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__119"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RentalCostPerDay" , 1)
			oColumn.Name = "RentalCostPerDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RentalCostPerDay" , 1)
			oColumn.Name = "RentalCostPerDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub120(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__120"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceLossOfUseFlag" , 1)
			oColumn.Name = "ServiceLossOfUseFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceLossOfUseFlag" , 1)
			oColumn.Name = "ServiceLossOfUseFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub121(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__121"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceSubrogationFlag" , 1)
			oColumn.Name = "ServiceSubrogationFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceSubrogationFlag" , 1)
			oColumn.Name = "ServiceSubrogationFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub122(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__122"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceTotalLossFlag" , 1)
			oColumn.Name = "ServiceTotalLossFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceTotalLossFlag" , 1)
			oColumn.Name = "ServiceTotalLossFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub123(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__123"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceTotalTheftFlag" , 1)
			oColumn.Name = "ServiceTotalTheftFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceTotalTheftFlag" , 1)
			oColumn.Name = "ServiceTotalTheftFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub124(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__124"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("SupplementTotalAmt" , 1)
			oColumn.Name = "SupplementTotalAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("SupplementTotalAmt" , 1)
			oColumn.Name = "SupplementTotalAmt"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub125(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__125"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleDriveableFlag" , 1)
			oColumn.Name = "VehicleDriveableFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleDriveableFlag" , 1)
			oColumn.Name = "VehicleDriveableFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub126(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__126"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleLicensePlateNumber" , 1)
			oColumn.Name = "VehicleLicensePlateNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleLicensePlateNumber" , 1)
			oColumn.Name = "VehicleLicensePlateNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub127(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__127"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleMake" , 1)
			oColumn.Name = "VehicleMake"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleMake" , 1)
			oColumn.Name = "VehicleMake"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub128(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__128"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleModel" , 1)
			oColumn.Name = "VehicleModel"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleModel" , 1)
			oColumn.Name = "VehicleModel"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub129(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__129"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleYear" , 1)
			oColumn.Name = "VehicleYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleYear" , 1)
			oColumn.Name = "VehicleYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub130(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__130"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("VehicleVIN" , 1)
			oColumn.Name = "VehicleVIN"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 17
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("VehicleVIN" , 1)
			oColumn.Name = "VehicleVIN"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 17
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub131(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__131"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask1_Trans_Sub132(ByVal oCustomTask1 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask1.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__132"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask1.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub2 for task DTSTask_DTSExecuteSQLTask_1 (Execute SQL Task: DTWH Meta Data)
Public Sub Task_Sub2(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask2 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_1"
Set oCustomTask2 = oTask.CustomTask

	oCustomTask2.Name = "DTSTask_DTSExecuteSQLTask_1"
	oCustomTask2.Description = "Execute SQL Task: DTWH Meta Data"
	oCustomTask2.SQLStatement = "    SELECT  Table_Name," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Column_Name," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Data_Type," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Character_Maximum_Length," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Numeric_Precision," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Numeric_Scale," & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "            Is_Nullable" & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "    FROM INFORMATION_SCHEMA.COLUMNS" & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "    WHERE   Table_Name LIKE 'utb_dtwh_%'" & vbCrLf
	oCustomTask2.SQLStatement = oCustomTask2.SQLStatement & "    ORDER BY Table_Name, Ordinal_Position"
	oCustomTask2.ConnectionID = 5
	oCustomTask2.CommandTimeout = 0
	oCustomTask2.OutputAsRecordset = False
	
goPackage.Tasks.Add oTask
Set oCustomTask2 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub3 for task DTSTask_DTSDataPumpTask_1 (Transform DTWHDATYPE)
Public Sub Task_Sub3(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask3 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_1"
Set oCustomTask3 = oTask.CustomTask

	oCustomTask3.Name = "DTSTask_DTSDataPumpTask_1"
	oCustomTask3.Description = "Transform DTWHDATYPE"
	oCustomTask3.SourceConnectionID = 5
	oCustomTask3.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask3.SourceSQLStatement = oCustomTask3.SourceSQLStatement & "       'DTWHDATYPE' AS data_strip_file," & vbCrLf
	oCustomTask3.SourceSQLStatement = oCustomTask3.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask3.SourceSQLStatement = oCustomTask3.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask3.SourceSQLStatement = oCustomTask3.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask3.SourceSQLStatement = oCustomTask3.SourceSQLStatement & "  FROM utb_dtwh_dim_assignment_type"
	oCustomTask3.DestinationConnectionID = 8
	oCustomTask3.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDATYPE.download"
	oCustomTask3.ProgressRowCount = 1000
	oCustomTask3.MaximumErrorCount = 0
	oCustomTask3.FetchBufferSize = 1
	oCustomTask3.UseFastLoad = True
	oCustomTask3.InsertCommitSize = 0
	oCustomTask3.ExceptionFileColumnDelimiter = "|"
	oCustomTask3.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask3.AllowIdentityInserts = False
	oCustomTask3.FirstRow = 0
	oCustomTask3.LastRow = 0
	oCustomTask3.FastLoadOptions = 2
	oCustomTask3.ExceptionFileOptions = 1
	oCustomTask3.DataPumpOptions = 0
	
Call oCustomTask3_Trans_Sub1( oCustomTask3 )
Call oCustomTask3_Trans_Sub2( oCustomTask3 )
Call oCustomTask3_Trans_Sub3( oCustomTask3 )
Call oCustomTask3_Trans_Sub4( oCustomTask3 )
Call oCustomTask3_Trans_Sub5( oCustomTask3 )
Call oCustomTask3_Trans_Sub6( oCustomTask3 )
Call oCustomTask3_Trans_Sub7( oCustomTask3 )
goPackage.Tasks.Add oTask
Set oCustomTask3 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub1(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub2(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub3(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AssignmentTypeID" , 1)
			oColumn.Name = "AssignmentTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32792
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AssignmentTypeID" , 1)
			oColumn.Name = "AssignmentTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub4(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AssignmentTypeDescription" , 1)
			oColumn.Name = "AssignmentTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AssignmentTypeDescription" , 1)
			oColumn.Name = "AssignmentTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub5(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub6(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask3_Trans_Sub7(ByVal oCustomTask3 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask3.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask3.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub4 for task DTSTask_DTSDataPumpTask_2 (Transform DTWHDCUST)
Public Sub Task_Sub4(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask4 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_2"
Set oCustomTask4 = oTask.CustomTask

	oCustomTask4.Name = "DTSTask_DTSDataPumpTask_2"
	oCustomTask4.Description = "Transform DTWHDCUST"
	oCustomTask4.SourceConnectionID = 5
	oCustomTask4.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask4.SourceSQLStatement = oCustomTask4.SourceSQLStatement & "       'DTWHDCUST' AS data_strip_file," & vbCrLf
	oCustomTask4.SourceSQLStatement = oCustomTask4.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask4.SourceSQLStatement = oCustomTask4.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask4.SourceSQLStatement = oCustomTask4.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask4.SourceSQLStatement = oCustomTask4.SourceSQLStatement & "  FROM utb_dtwh_dim_customer"
	oCustomTask4.DestinationConnectionID = 9
	oCustomTask4.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCUST.download"
	oCustomTask4.ProgressRowCount = 1000
	oCustomTask4.MaximumErrorCount = 0
	oCustomTask4.FetchBufferSize = 1
	oCustomTask4.UseFastLoad = True
	oCustomTask4.InsertCommitSize = 0
	oCustomTask4.ExceptionFileColumnDelimiter = "|"
	oCustomTask4.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask4.AllowIdentityInserts = False
	oCustomTask4.FirstRow = 0
	oCustomTask4.LastRow = 0
	oCustomTask4.FastLoadOptions = 2
	oCustomTask4.ExceptionFileOptions = 1
	oCustomTask4.DataPumpOptions = 0
	
Call oCustomTask4_Trans_Sub1( oCustomTask4 )
Call oCustomTask4_Trans_Sub2( oCustomTask4 )
Call oCustomTask4_Trans_Sub3( oCustomTask4 )
Call oCustomTask4_Trans_Sub4( oCustomTask4 )
Call oCustomTask4_Trans_Sub5( oCustomTask4 )
Call oCustomTask4_Trans_Sub6( oCustomTask4 )
Call oCustomTask4_Trans_Sub7( oCustomTask4 )
Call oCustomTask4_Trans_Sub8( oCustomTask4 )
Call oCustomTask4_Trans_Sub9( oCustomTask4 )
Call oCustomTask4_Trans_Sub10( oCustomTask4 )
Call oCustomTask4_Trans_Sub11( oCustomTask4 )
Call oCustomTask4_Trans_Sub12( oCustomTask4 )
Call oCustomTask4_Trans_Sub13( oCustomTask4 )
Call oCustomTask4_Trans_Sub14( oCustomTask4 )
goPackage.Tasks.Add oTask
Set oCustomTask4 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub1(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub2(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 9
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 9
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub3(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub4(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClientUserId" , 1)
			oColumn.Name = "ClientUserId"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClientUserId" , 1)
			oColumn.Name = "ClientUserId"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub5(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("InsuranceCompanyID" , 1)
			oColumn.Name = "InsuranceCompanyID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("InsuranceCompanyID" , 1)
			oColumn.Name = "InsuranceCompanyID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub6(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("InsuranceCompanyName" , 1)
			oColumn.Name = "InsuranceCompanyName"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("InsuranceCompanyName" , 1)
			oColumn.Name = "InsuranceCompanyName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub7(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OfficeClientId" , 1)
			oColumn.Name = "OfficeClientId"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OfficeClientId" , 1)
			oColumn.Name = "OfficeClientId"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub8(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OfficeID" , 1)
			oColumn.Name = "OfficeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OfficeID" , 1)
			oColumn.Name = "OfficeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub9(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OfficeName" , 1)
			oColumn.Name = "OfficeName"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OfficeName" , 1)
			oColumn.Name = "OfficeName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub10(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RepNameFirst" , 1)
			oColumn.Name = "RepNameFirst"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RepNameFirst" , 1)
			oColumn.Name = "RepNameFirst"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub11(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RepNameLast" , 1)
			oColumn.Name = "RepNameLast"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RepNameLast" , 1)
			oColumn.Name = "RepNameLast"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub12(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__12"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("UserID" , 1)
			oColumn.Name = "UserID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("UserID" , 1)
			oColumn.Name = "UserID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub13(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__13"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask4_Trans_Sub14(ByVal oCustomTask4 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask4.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__14"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask4.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub5 for task DTSTask_DTSDataPumpTask_3 (Transform DTWHDCOVTY)
Public Sub Task_Sub5(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask5 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_3"
Set oCustomTask5 = oTask.CustomTask

	oCustomTask5.Name = "DTSTask_DTSDataPumpTask_3"
	oCustomTask5.Description = "Transform DTWHDCOVTY"
	oCustomTask5.SourceConnectionID = 5
	oCustomTask5.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask5.SourceSQLStatement = oCustomTask5.SourceSQLStatement & "       'DTWHDCOVTY' AS data_strip_file," & vbCrLf
	oCustomTask5.SourceSQLStatement = oCustomTask5.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask5.SourceSQLStatement = oCustomTask5.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask5.SourceSQLStatement = oCustomTask5.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask5.SourceSQLStatement = oCustomTask5.SourceSQLStatement & "  FROM utb_dtwh_dim_coverage_type"
	oCustomTask5.DestinationConnectionID = 10
	oCustomTask5.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDCOVTY.download"
	oCustomTask5.ProgressRowCount = 1000
	oCustomTask5.MaximumErrorCount = 0
	oCustomTask5.FetchBufferSize = 1
	oCustomTask5.UseFastLoad = True
	oCustomTask5.InsertCommitSize = 0
	oCustomTask5.ExceptionFileColumnDelimiter = "|"
	oCustomTask5.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask5.AllowIdentityInserts = False
	oCustomTask5.FirstRow = 0
	oCustomTask5.LastRow = 0
	oCustomTask5.FastLoadOptions = 2
	oCustomTask5.ExceptionFileOptions = 1
	oCustomTask5.DataPumpOptions = 0
	
Call oCustomTask5_Trans_Sub1( oCustomTask5 )
Call oCustomTask5_Trans_Sub2( oCustomTask5 )
Call oCustomTask5_Trans_Sub3( oCustomTask5 )
Call oCustomTask5_Trans_Sub4( oCustomTask5 )
Call oCustomTask5_Trans_Sub5( oCustomTask5 )
Call oCustomTask5_Trans_Sub6( oCustomTask5 )
Call oCustomTask5_Trans_Sub7( oCustomTask5 )
Call oCustomTask5_Trans_Sub8( oCustomTask5 )
Call oCustomTask5_Trans_Sub9( oCustomTask5 )
goPackage.Tasks.Add oTask
Set oCustomTask5 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub1(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub2(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub3(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CoverageTypeID" , 1)
			oColumn.Name = "CoverageTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32792
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CoverageTypeID" , 1)
			oColumn.Name = "CoverageTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub4(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CoverageTypeCD" , 1)
			oColumn.Name = "CoverageTypeCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CoverageTypeCD" , 1)
			oColumn.Name = "CoverageTypeCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub5(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CoverageTypeDescription" , 1)
			oColumn.Name = "CoverageTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CoverageTypeDescription" , 1)
			oColumn.Name = "CoverageTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub6(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartyCD" , 1)
			oColumn.Name = "PartyCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartyCD" , 1)
			oColumn.Name = "PartyCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub7(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartyDescription" , 1)
			oColumn.Name = "PartyDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartyDescription" , 1)
			oColumn.Name = "PartyDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub8(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask5_Trans_Sub9(ByVal oCustomTask5 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask5.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask5.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub6 for task DTSTask_DTSDataPumpTask_4 (Transform DTWHDDISPT)
Public Sub Task_Sub6(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask6 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_4"
Set oCustomTask6 = oTask.CustomTask

	oCustomTask6.Name = "DTSTask_DTSDataPumpTask_4"
	oCustomTask6.Description = "Transform DTWHDDISPT"
	oCustomTask6.SourceConnectionID = 5
	oCustomTask6.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask6.SourceSQLStatement = oCustomTask6.SourceSQLStatement & "       'DTWHDDISPT' AS data_strip_file," & vbCrLf
	oCustomTask6.SourceSQLStatement = oCustomTask6.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask6.SourceSQLStatement = oCustomTask6.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask6.SourceSQLStatement = oCustomTask6.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask6.SourceSQLStatement = oCustomTask6.SourceSQLStatement & "  FROM utb_dtwh_dim_disposition_type"
	oCustomTask6.DestinationConnectionID = 11
	oCustomTask6.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDISPT.download"
	oCustomTask6.ProgressRowCount = 1000
	oCustomTask6.MaximumErrorCount = 0
	oCustomTask6.FetchBufferSize = 1
	oCustomTask6.UseFastLoad = True
	oCustomTask6.InsertCommitSize = 0
	oCustomTask6.ExceptionFileColumnDelimiter = "|"
	oCustomTask6.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask6.AllowIdentityInserts = False
	oCustomTask6.FirstRow = 0
	oCustomTask6.LastRow = 0
	oCustomTask6.FastLoadOptions = 2
	oCustomTask6.ExceptionFileOptions = 1
	oCustomTask6.DataPumpOptions = 0
	
Call oCustomTask6_Trans_Sub1( oCustomTask6 )
Call oCustomTask6_Trans_Sub2( oCustomTask6 )
Call oCustomTask6_Trans_Sub3( oCustomTask6 )
Call oCustomTask6_Trans_Sub4( oCustomTask6 )
Call oCustomTask6_Trans_Sub5( oCustomTask6 )
Call oCustomTask6_Trans_Sub6( oCustomTask6 )
Call oCustomTask6_Trans_Sub7( oCustomTask6 )
goPackage.Tasks.Add oTask
Set oCustomTask6 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub1(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub2(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub3(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DispositionTypeID" , 1)
			oColumn.Name = "DispositionTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DispositionTypeID" , 1)
			oColumn.Name = "DispositionTypeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub4(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DispositionTypeCD" , 1)
			oColumn.Name = "DispositionTypeCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DispositionTypeCD" , 1)
			oColumn.Name = "DispositionTypeCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub5(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DispositionTypeDescription" , 1)
			oColumn.Name = "DispositionTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DispositionTypeDescription" , 1)
			oColumn.Name = "DispositionTypeDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub6(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask6_Trans_Sub7(ByVal oCustomTask6 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask6.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask6.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub7 for task DTSTask_DTSDataPumpTask_5 (Transform DTWHDLHAND)
Public Sub Task_Sub7(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask7 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_5"
Set oCustomTask7 = oTask.CustomTask

	oCustomTask7.Name = "DTSTask_DTSDataPumpTask_5"
	oCustomTask7.Description = "Transform DTWHDLHAND"
	oCustomTask7.SourceConnectionID = 5
	oCustomTask7.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask7.SourceSQLStatement = oCustomTask7.SourceSQLStatement & "       'DTWHDLHAND' AS data_strip_file," & vbCrLf
	oCustomTask7.SourceSQLStatement = oCustomTask7.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask7.SourceSQLStatement = oCustomTask7.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask7.SourceSQLStatement = oCustomTask7.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask7.SourceSQLStatement = oCustomTask7.SourceSQLStatement & "  FROM utb_dtwh_dim_lynx_handler"
	oCustomTask7.DestinationConnectionID = 12
	oCustomTask7.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDLHAND.download"
	oCustomTask7.ProgressRowCount = 1000
	oCustomTask7.MaximumErrorCount = 0
	oCustomTask7.FetchBufferSize = 1
	oCustomTask7.UseFastLoad = True
	oCustomTask7.InsertCommitSize = 0
	oCustomTask7.ExceptionFileColumnDelimiter = "|"
	oCustomTask7.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask7.AllowIdentityInserts = False
	oCustomTask7.FirstRow = 0
	oCustomTask7.LastRow = 0
	oCustomTask7.FastLoadOptions = 2
	oCustomTask7.ExceptionFileOptions = 1
	oCustomTask7.DataPumpOptions = 0
	
Call oCustomTask7_Trans_Sub1( oCustomTask7 )
Call oCustomTask7_Trans_Sub2( oCustomTask7 )
Call oCustomTask7_Trans_Sub3( oCustomTask7 )
Call oCustomTask7_Trans_Sub4( oCustomTask7 )
Call oCustomTask7_Trans_Sub5( oCustomTask7 )
Call oCustomTask7_Trans_Sub6( oCustomTask7 )
Call oCustomTask7_Trans_Sub7( oCustomTask7 )
Call oCustomTask7_Trans_Sub8( oCustomTask7 )
goPackage.Tasks.Add oTask
Set oCustomTask7 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub1(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub2(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub3(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LynxHandlerID" , 1)
			oColumn.Name = "LynxHandlerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LynxHandlerID" , 1)
			oColumn.Name = "LynxHandlerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub4(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("UserID" , 1)
			oColumn.Name = "UserID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("UserID" , 1)
			oColumn.Name = "UserID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub5(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("UserNameFirst" , 1)
			oColumn.Name = "UserNameFirst"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("UserNameFirst" , 1)
			oColumn.Name = "UserNameFirst"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub6(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("UserNameLast" , 1)
			oColumn.Name = "UserNameLast"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("UserNameLast" , 1)
			oColumn.Name = "UserNameLast"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub7(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask7_Trans_Sub8(ByVal oCustomTask7 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask7.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask7.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub8 for task DTSTask_DTSDataPumpTask_6 (Transform DTWHDDOCSR)
Public Sub Task_Sub8(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask8 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_6"
Set oCustomTask8 = oTask.CustomTask

	oCustomTask8.Name = "DTSTask_DTSDataPumpTask_6"
	oCustomTask8.Description = "Transform DTWHDDOCSR"
	oCustomTask8.SourceConnectionID = 5
	oCustomTask8.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask8.SourceSQLStatement = oCustomTask8.SourceSQLStatement & "       'DTWHDDOCSR' AS data_strip_file," & vbCrLf
	oCustomTask8.SourceSQLStatement = oCustomTask8.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask8.SourceSQLStatement = oCustomTask8.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask8.SourceSQLStatement = oCustomTask8.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask8.SourceSQLStatement = oCustomTask8.SourceSQLStatement & "  FROM utb_dtwh_dim_document_source"
	oCustomTask8.DestinationConnectionID = 13
	oCustomTask8.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDDOCSR.download"
	oCustomTask8.ProgressRowCount = 1000
	oCustomTask8.MaximumErrorCount = 0
	oCustomTask8.FetchBufferSize = 1
	oCustomTask8.UseFastLoad = True
	oCustomTask8.InsertCommitSize = 0
	oCustomTask8.ExceptionFileColumnDelimiter = "|"
	oCustomTask8.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask8.AllowIdentityInserts = False
	oCustomTask8.FirstRow = 0
	oCustomTask8.LastRow = 0
	oCustomTask8.FastLoadOptions = 2
	oCustomTask8.ExceptionFileOptions = 1
	oCustomTask8.DataPumpOptions = 0
	
Call oCustomTask8_Trans_Sub1( oCustomTask8 )
Call oCustomTask8_Trans_Sub2( oCustomTask8 )
Call oCustomTask8_Trans_Sub3( oCustomTask8 )
Call oCustomTask8_Trans_Sub4( oCustomTask8 )
Call oCustomTask8_Trans_Sub5( oCustomTask8 )
Call oCustomTask8_Trans_Sub6( oCustomTask8 )
Call oCustomTask8_Trans_Sub7( oCustomTask8 )
goPackage.Tasks.Add oTask
Set oCustomTask8 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub1(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub2(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub3(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DocumentSourceID" , 1)
			oColumn.Name = "DocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DocumentSourceID" , 1)
			oColumn.Name = "DocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub4(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DocumentSourceName" , 1)
			oColumn.Name = "DocumentSourceName"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DocumentSourceName" , 1)
			oColumn.Name = "DocumentSourceName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub5(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ElectronicSourceFlag" , 1)
			oColumn.Name = "ElectronicSourceFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ElectronicSourceFlag" , 1)
			oColumn.Name = "ElectronicSourceFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub6(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask8_Trans_Sub7(ByVal oCustomTask8 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask8.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask8.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub9 for task DTSTask_DTSDataPumpTask_7 (Transform DTWHDREPLC)
Public Sub Task_Sub9(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask9 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_7"
Set oCustomTask9 = oTask.CustomTask

	oCustomTask9.Name = "DTSTask_DTSDataPumpTask_7"
	oCustomTask9.Description = "Transform DTWHDREPLC"
	oCustomTask9.SourceConnectionID = 5
	oCustomTask9.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask9.SourceSQLStatement = oCustomTask9.SourceSQLStatement & "       'DTWHDREPLC' AS data_strip_file," & vbCrLf
	oCustomTask9.SourceSQLStatement = oCustomTask9.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask9.SourceSQLStatement = oCustomTask9.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask9.SourceSQLStatement = oCustomTask9.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask9.SourceSQLStatement = oCustomTask9.SourceSQLStatement & "  FROM utb_dtwh_dim_repair_location"
	oCustomTask9.DestinationConnectionID = 14
	oCustomTask9.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDREPLC.download"
	oCustomTask9.ProgressRowCount = 1000
	oCustomTask9.MaximumErrorCount = 0
	oCustomTask9.FetchBufferSize = 1
	oCustomTask9.UseFastLoad = True
	oCustomTask9.InsertCommitSize = 0
	oCustomTask9.ExceptionFileColumnDelimiter = "|"
	oCustomTask9.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask9.AllowIdentityInserts = False
	oCustomTask9.FirstRow = 0
	oCustomTask9.LastRow = 0
	oCustomTask9.FastLoadOptions = 2
	oCustomTask9.ExceptionFileOptions = 1
	oCustomTask9.DataPumpOptions = 0
	
Call oCustomTask9_Trans_Sub1( oCustomTask9 )
Call oCustomTask9_Trans_Sub2( oCustomTask9 )
Call oCustomTask9_Trans_Sub3( oCustomTask9 )
Call oCustomTask9_Trans_Sub4( oCustomTask9 )
Call oCustomTask9_Trans_Sub5( oCustomTask9 )
Call oCustomTask9_Trans_Sub6( oCustomTask9 )
Call oCustomTask9_Trans_Sub7( oCustomTask9 )
Call oCustomTask9_Trans_Sub8( oCustomTask9 )
Call oCustomTask9_Trans_Sub9( oCustomTask9 )
Call oCustomTask9_Trans_Sub10( oCustomTask9 )
Call oCustomTask9_Trans_Sub11( oCustomTask9 )
Call oCustomTask9_Trans_Sub12( oCustomTask9 )
Call oCustomTask9_Trans_Sub13( oCustomTask9 )
Call oCustomTask9_Trans_Sub14( oCustomTask9 )
Call oCustomTask9_Trans_Sub15( oCustomTask9 )
Call oCustomTask9_Trans_Sub16( oCustomTask9 )
Call oCustomTask9_Trans_Sub17( oCustomTask9 )
goPackage.Tasks.Add oTask
Set oCustomTask9 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub1(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub2(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub3(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub4(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StandardDocumentSourceID" , 1)
			oColumn.Name = "StandardDocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StandardDocumentSourceID" , 1)
			oColumn.Name = "StandardDocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub5(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StateID" , 1)
			oColumn.Name = "StateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StateID" , 1)
			oColumn.Name = "StateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub6(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CEIProgramFlag" , 1)
			oColumn.Name = "CEIProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CEIProgramFlag" , 1)
			oColumn.Name = "CEIProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub7(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CFProgramFlag" , 1)
			oColumn.Name = "CFProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CFProgramFlag" , 1)
			oColumn.Name = "CFProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub8(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("City" , 1)
			oColumn.Name = "City"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("City" , 1)
			oColumn.Name = "City"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub9(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("County" , 1)
			oColumn.Name = "County"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("County" , 1)
			oColumn.Name = "County"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub10(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LYNXSelectProgramFlag" , 1)
			oColumn.Name = "LYNXSelectProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LYNXSelectProgramFlag" , 1)
			oColumn.Name = "LYNXSelectProgramFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub11(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PPGPaintBuyerFlag" , 1)
			oColumn.Name = "PPGPaintBuyerFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PPGPaintBuyerFlag" , 1)
			oColumn.Name = "PPGPaintBuyerFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub12(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__12"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ShopLocationID" , 1)
			oColumn.Name = "ShopLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ShopLocationID" , 1)
			oColumn.Name = "ShopLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub13(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__13"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ShopLocationName" , 1)
			oColumn.Name = "ShopLocationName"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ShopLocationName" , 1)
			oColumn.Name = "ShopLocationName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub14(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__14"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StandardEstimatePackage" , 1)
			oColumn.Name = "StandardEstimatePackage"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StandardEstimatePackage" , 1)
			oColumn.Name = "StandardEstimatePackage"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub15(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__15"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ZipCode" , 1)
			oColumn.Name = "ZipCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 8
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ZipCode" , 1)
			oColumn.Name = "ZipCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub16(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__16"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask9_Trans_Sub17(ByVal oCustomTask9 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask9.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__17"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask9.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub10 for task DTSTask_DTSDataPumpTask_8 (Transform DTWHDSCHNL)
Public Sub Task_Sub10(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask10 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_8"
Set oCustomTask10 = oTask.CustomTask

	oCustomTask10.Name = "DTSTask_DTSDataPumpTask_8"
	oCustomTask10.Description = "Transform DTWHDSCHNL"
	oCustomTask10.SourceConnectionID = 5
	oCustomTask10.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask10.SourceSQLStatement = oCustomTask10.SourceSQLStatement & "       'DTWHDSCHNL' AS data_strip_file," & vbCrLf
	oCustomTask10.SourceSQLStatement = oCustomTask10.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask10.SourceSQLStatement = oCustomTask10.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask10.SourceSQLStatement = oCustomTask10.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask10.SourceSQLStatement = oCustomTask10.SourceSQLStatement & "  FROM utb_dtwh_dim_service_channel"
	oCustomTask10.DestinationConnectionID = 15
	oCustomTask10.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSCHNL.download"
	oCustomTask10.ProgressRowCount = 1000
	oCustomTask10.MaximumErrorCount = 0
	oCustomTask10.FetchBufferSize = 1
	oCustomTask10.UseFastLoad = True
	oCustomTask10.InsertCommitSize = 0
	oCustomTask10.ExceptionFileColumnDelimiter = "|"
	oCustomTask10.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask10.AllowIdentityInserts = False
	oCustomTask10.FirstRow = 0
	oCustomTask10.LastRow = 0
	oCustomTask10.FastLoadOptions = 2
	oCustomTask10.ExceptionFileOptions = 1
	oCustomTask10.DataPumpOptions = 0
	
Call oCustomTask10_Trans_Sub1( oCustomTask10 )
Call oCustomTask10_Trans_Sub2( oCustomTask10 )
Call oCustomTask10_Trans_Sub3( oCustomTask10 )
Call oCustomTask10_Trans_Sub4( oCustomTask10 )
Call oCustomTask10_Trans_Sub5( oCustomTask10 )
Call oCustomTask10_Trans_Sub6( oCustomTask10 )
Call oCustomTask10_Trans_Sub7( oCustomTask10 )
goPackage.Tasks.Add oTask
Set oCustomTask10 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub1(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub2(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub3(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceChannelID" , 1)
			oColumn.Name = "ServiceChannelID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub4(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceChannelCD" , 1)
			oColumn.Name = "ServiceChannelCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceChannelCD" , 1)
			oColumn.Name = "ServiceChannelCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub5(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ServiceChannelDescription" , 1)
			oColumn.Name = "ServiceChannelDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ServiceChannelDescription" , 1)
			oColumn.Name = "ServiceChannelDescription"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub6(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask10_Trans_Sub7(ByVal oCustomTask10 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask10.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask10.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub11 for task DTSTask_DTSDataPumpTask_9 (Transform DTWHDSTATE)
Public Sub Task_Sub11(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask11 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_9"
Set oCustomTask11 = oTask.CustomTask

	oCustomTask11.Name = "DTSTask_DTSDataPumpTask_9"
	oCustomTask11.Description = "Transform DTWHDSTATE"
	oCustomTask11.SourceConnectionID = 5
	oCustomTask11.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask11.SourceSQLStatement = oCustomTask11.SourceSQLStatement & "       'DTWHDSTATE' AS data_strip_file," & vbCrLf
	oCustomTask11.SourceSQLStatement = oCustomTask11.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask11.SourceSQLStatement = oCustomTask11.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask11.SourceSQLStatement = oCustomTask11.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask11.SourceSQLStatement = oCustomTask11.SourceSQLStatement & "  FROM utb_dtwh_dim_state"
	oCustomTask11.DestinationConnectionID = 16
	oCustomTask11.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDSTATE.download"
	oCustomTask11.ProgressRowCount = 1000
	oCustomTask11.MaximumErrorCount = 0
	oCustomTask11.FetchBufferSize = 1
	oCustomTask11.UseFastLoad = True
	oCustomTask11.InsertCommitSize = 0
	oCustomTask11.ExceptionFileColumnDelimiter = "|"
	oCustomTask11.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask11.AllowIdentityInserts = False
	oCustomTask11.FirstRow = 0
	oCustomTask11.LastRow = 0
	oCustomTask11.FastLoadOptions = 2
	oCustomTask11.ExceptionFileOptions = 1
	oCustomTask11.DataPumpOptions = 0
	
Call oCustomTask11_Trans_Sub1( oCustomTask11 )
Call oCustomTask11_Trans_Sub2( oCustomTask11 )
Call oCustomTask11_Trans_Sub3( oCustomTask11 )
Call oCustomTask11_Trans_Sub4( oCustomTask11 )
Call oCustomTask11_Trans_Sub5( oCustomTask11 )
Call oCustomTask11_Trans_Sub6( oCustomTask11 )
Call oCustomTask11_Trans_Sub7( oCustomTask11 )
Call oCustomTask11_Trans_Sub8( oCustomTask11 )
Call oCustomTask11_Trans_Sub9( oCustomTask11 )
Call oCustomTask11_Trans_Sub10( oCustomTask11 )
Call oCustomTask11_Trans_Sub11( oCustomTask11 )
goPackage.Tasks.Add oTask
Set oCustomTask11 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub1(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub2(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 10
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub3(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StateID" , 1)
			oColumn.Name = "StateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StateID" , 1)
			oColumn.Name = "StateID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub4(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StateCode" , 1)
			oColumn.Name = "StateCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 2
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StateCode" , 1)
			oColumn.Name = "StateCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub5(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StateName" , 1)
			oColumn.Name = "StateName"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StateName" , 1)
			oColumn.Name = "StateName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub6(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CountryCode" , 1)
			oColumn.Name = "CountryCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CountryCode" , 1)
			oColumn.Name = "CountryCode"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub7(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CountryName" , 1)
			oColumn.Name = "CountryName"
			oColumn.Ordinal = 1
			oColumn.Flags = 8
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CountryName" , 1)
			oColumn.Name = "CountryName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub8(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RegionCD" , 1)
			oColumn.Name = "RegionCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 4
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RegionCD" , 1)
			oColumn.Name = "RegionCD"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub9(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RegionName" , 1)
			oColumn.Name = "RegionName"
			oColumn.Ordinal = 1
			oColumn.Flags = 104
			oColumn.Size = 50
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RegionName" , 1)
			oColumn.Name = "RegionName"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 50
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub10(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask11_Trans_Sub11(ByVal oCustomTask11 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask11.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask11.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub12 for task DTSTask_DTSDataPumpTask_10 (Transform DTWHDTIME)
Public Sub Task_Sub12(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask12 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_10"
Set oCustomTask12 = oTask.CustomTask

	oCustomTask12.Name = "DTSTask_DTSDataPumpTask_10"
	oCustomTask12.Description = "Transform DTWHDTIME"
	oCustomTask12.SourceConnectionID = 5
	oCustomTask12.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask12.SourceSQLStatement = oCustomTask12.SourceSQLStatement & "       'DTWHDTIME' AS data_strip_file," & vbCrLf
	oCustomTask12.SourceSQLStatement = oCustomTask12.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask12.SourceSQLStatement = oCustomTask12.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask12.SourceSQLStatement = oCustomTask12.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask12.SourceSQLStatement = oCustomTask12.SourceSQLStatement & "  FROM utb_dtwh_dim_time"
	oCustomTask12.DestinationConnectionID = 17
	oCustomTask12.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHDTIME.download"
	oCustomTask12.ProgressRowCount = 1000
	oCustomTask12.MaximumErrorCount = 0
	oCustomTask12.FetchBufferSize = 1
	oCustomTask12.UseFastLoad = True
	oCustomTask12.InsertCommitSize = 0
	oCustomTask12.ExceptionFileColumnDelimiter = "|"
	oCustomTask12.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask12.AllowIdentityInserts = False
	oCustomTask12.FirstRow = 0
	oCustomTask12.LastRow = 0
	oCustomTask12.FastLoadOptions = 2
	oCustomTask12.ExceptionFileOptions = 1
	oCustomTask12.DataPumpOptions = 0
	
Call oCustomTask12_Trans_Sub1( oCustomTask12 )
Call oCustomTask12_Trans_Sub2( oCustomTask12 )
Call oCustomTask12_Trans_Sub3( oCustomTask12 )
Call oCustomTask12_Trans_Sub4( oCustomTask12 )
Call oCustomTask12_Trans_Sub5( oCustomTask12 )
Call oCustomTask12_Trans_Sub6( oCustomTask12 )
Call oCustomTask12_Trans_Sub7( oCustomTask12 )
Call oCustomTask12_Trans_Sub8( oCustomTask12 )
Call oCustomTask12_Trans_Sub9( oCustomTask12 )
Call oCustomTask12_Trans_Sub10( oCustomTask12 )
Call oCustomTask12_Trans_Sub11( oCustomTask12 )
Call oCustomTask12_Trans_Sub12( oCustomTask12 )
Call oCustomTask12_Trans_Sub13( oCustomTask12 )
goPackage.Tasks.Add oTask
Set oCustomTask12 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub1(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub2(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 9
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 9
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub3(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeID" , 1)
			oColumn.Name = "TimeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeID" , 1)
			oColumn.Name = "TimeID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub4(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DateValue" , 1)
			oColumn.Name = "DateValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 135
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DateValue" , 1)
			oColumn.Name = "DateValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 16
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub5(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("HourValue" , 1)
			oColumn.Name = "HourValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("HourValue" , 1)
			oColumn.Name = "HourValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub6(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DayOfMonth" , 1)
			oColumn.Name = "DayOfMonth"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DayOfMonth" , 1)
			oColumn.Name = "DayOfMonth"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub7(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("JulianDay" , 1)
			oColumn.Name = "JulianDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("JulianDay" , 1)
			oColumn.Name = "JulianDay"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub8(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MonthOfYear" , 1)
			oColumn.Name = "MonthOfYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MonthOfYear" , 1)
			oColumn.Name = "MonthOfYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub9(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("Quarter" , 1)
			oColumn.Name = "Quarter"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("Quarter" , 1)
			oColumn.Name = "Quarter"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub10(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("WeekOfYear" , 1)
			oColumn.Name = "WeekOfYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("WeekOfYear" , 1)
			oColumn.Name = "WeekOfYear"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub11(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("YearValue" , 1)
			oColumn.Name = "YearValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 2
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("YearValue" , 1)
			oColumn.Name = "YearValue"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 2
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub12(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__12"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask12_Trans_Sub13(ByVal oCustomTask12 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask12.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__13"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask12.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub13 for task DTSTask_DTSDataPumpTask_11 (Transform DTWHFEST)
Public Sub Task_Sub13(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask13 As DTS.DataPumpTask2
Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
oTask.Name = "DTSTask_DTSDataPumpTask_11"
Set oCustomTask13 = oTask.CustomTask

	oCustomTask13.Name = "DTSTask_DTSDataPumpTask_11"
	oCustomTask13.Description = "Transform DTWHFEST"
	oCustomTask13.SourceConnectionID = 5
	oCustomTask13.SourceSQLStatement = "SELECT 'APD' AS data_strip_system, " & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & "       'DTWHFEST' AS data_strip_file," & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & "       *," & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & "       'DTS_CLAIMS_EXTRACT' AS data_strip_application," & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & "       CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 101) AS data_strip_date" & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & "  FROM utb_dtwh_fact_Estimate" & vbCrLf
	oCustomTask13.SourceSQLStatement = oCustomTask13.SourceSQLStatement & " "
	oCustomTask13.DestinationConnectionID = 18
	oCustomTask13.DestinationObjectName = "\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\apd_DTWHFEST.download"
	oCustomTask13.ProgressRowCount = 1000
	oCustomTask13.MaximumErrorCount = 0
	oCustomTask13.FetchBufferSize = 1
	oCustomTask13.UseFastLoad = True
	oCustomTask13.InsertCommitSize = 0
	oCustomTask13.ExceptionFileColumnDelimiter = "|"
	oCustomTask13.ExceptionFileRowDelimiter = vbCrLf
	oCustomTask13.AllowIdentityInserts = False
	oCustomTask13.FirstRow = 0
	oCustomTask13.LastRow = 0
	oCustomTask13.FastLoadOptions = 2
	oCustomTask13.ExceptionFileOptions = 1
	oCustomTask13.DataPumpOptions = 0
	
Call oCustomTask13_Trans_Sub1( oCustomTask13 )
Call oCustomTask13_Trans_Sub2( oCustomTask13 )
Call oCustomTask13_Trans_Sub3( oCustomTask13 )
Call oCustomTask13_Trans_Sub4( oCustomTask13 )
Call oCustomTask13_Trans_Sub5( oCustomTask13 )
Call oCustomTask13_Trans_Sub6( oCustomTask13 )
Call oCustomTask13_Trans_Sub7( oCustomTask13 )
Call oCustomTask13_Trans_Sub8( oCustomTask13 )
Call oCustomTask13_Trans_Sub9( oCustomTask13 )
Call oCustomTask13_Trans_Sub10( oCustomTask13 )
Call oCustomTask13_Trans_Sub11( oCustomTask13 )
Call oCustomTask13_Trans_Sub12( oCustomTask13 )
Call oCustomTask13_Trans_Sub13( oCustomTask13 )
Call oCustomTask13_Trans_Sub14( oCustomTask13 )
Call oCustomTask13_Trans_Sub15( oCustomTask13 )
Call oCustomTask13_Trans_Sub16( oCustomTask13 )
Call oCustomTask13_Trans_Sub17( oCustomTask13 )
Call oCustomTask13_Trans_Sub18( oCustomTask13 )
Call oCustomTask13_Trans_Sub19( oCustomTask13 )
Call oCustomTask13_Trans_Sub20( oCustomTask13 )
Call oCustomTask13_Trans_Sub21( oCustomTask13 )
Call oCustomTask13_Trans_Sub22( oCustomTask13 )
Call oCustomTask13_Trans_Sub23( oCustomTask13 )
Call oCustomTask13_Trans_Sub24( oCustomTask13 )
Call oCustomTask13_Trans_Sub25( oCustomTask13 )
Call oCustomTask13_Trans_Sub26( oCustomTask13 )
Call oCustomTask13_Trans_Sub27( oCustomTask13 )
Call oCustomTask13_Trans_Sub28( oCustomTask13 )
Call oCustomTask13_Trans_Sub29( oCustomTask13 )
Call oCustomTask13_Trans_Sub30( oCustomTask13 )
Call oCustomTask13_Trans_Sub31( oCustomTask13 )
Call oCustomTask13_Trans_Sub32( oCustomTask13 )
Call oCustomTask13_Trans_Sub33( oCustomTask13 )
Call oCustomTask13_Trans_Sub34( oCustomTask13 )
Call oCustomTask13_Trans_Sub35( oCustomTask13 )
Call oCustomTask13_Trans_Sub36( oCustomTask13 )
Call oCustomTask13_Trans_Sub37( oCustomTask13 )
Call oCustomTask13_Trans_Sub38( oCustomTask13 )
Call oCustomTask13_Trans_Sub39( oCustomTask13 )
Call oCustomTask13_Trans_Sub40( oCustomTask13 )
Call oCustomTask13_Trans_Sub41( oCustomTask13 )
Call oCustomTask13_Trans_Sub42( oCustomTask13 )
Call oCustomTask13_Trans_Sub43( oCustomTask13 )
Call oCustomTask13_Trans_Sub44( oCustomTask13 )
Call oCustomTask13_Trans_Sub45( oCustomTask13 )
Call oCustomTask13_Trans_Sub46( oCustomTask13 )
Call oCustomTask13_Trans_Sub47( oCustomTask13 )
Call oCustomTask13_Trans_Sub48( oCustomTask13 )
Call oCustomTask13_Trans_Sub49( oCustomTask13 )
Call oCustomTask13_Trans_Sub50( oCustomTask13 )
Call oCustomTask13_Trans_Sub51( oCustomTask13 )
Call oCustomTask13_Trans_Sub52( oCustomTask13 )
Call oCustomTask13_Trans_Sub53( oCustomTask13 )
Call oCustomTask13_Trans_Sub54( oCustomTask13 )
Call oCustomTask13_Trans_Sub55( oCustomTask13 )
Call oCustomTask13_Trans_Sub56( oCustomTask13 )
Call oCustomTask13_Trans_Sub57( oCustomTask13 )
Call oCustomTask13_Trans_Sub58( oCustomTask13 )
Call oCustomTask13_Trans_Sub59( oCustomTask13 )
Call oCustomTask13_Trans_Sub60( oCustomTask13 )
Call oCustomTask13_Trans_Sub61( oCustomTask13 )
Call oCustomTask13_Trans_Sub62( oCustomTask13 )
Call oCustomTask13_Trans_Sub63( oCustomTask13 )
goPackage.Tasks.Add oTask
Set oCustomTask13 = Nothing
Set oTask = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub1(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__1"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_system" , 1)
			oColumn.Name = "data_strip_system"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 3
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub2(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__2"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_file" , 1)
			oColumn.Name = "data_strip_file"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub3(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__3"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FactID" , 1)
			oColumn.Name = "FactID"
			oColumn.Ordinal = 1
			oColumn.Flags = 32784
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FactID" , 1)
			oColumn.Name = "FactID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub4(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__4"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("ClaimAspectID" , 1)
			oColumn.Name = "ClaimAspectID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("ClaimAspectID" , 1)
			oColumn.Name = "ClaimAspectID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub5(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__5"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("CustomerID" , 1)
			oColumn.Name = "CustomerID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub6(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__6"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DocumentSourceID" , 1)
			oColumn.Name = "DocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DocumentSourceID" , 1)
			oColumn.Name = "DocumentSourceID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub7(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__7"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RepairLocationID" , 1)
			oColumn.Name = "RepairLocationID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub8(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__8"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDAssignment" , 1)
			oColumn.Name = "TimeIDAssignment"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDAssignment" , 1)
			oColumn.Name = "TimeIDAssignment"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub9(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__9"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TimeIDReceived" , 1)
			oColumn.Name = "TimeIDReceived"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TimeIDReceived" , 1)
			oColumn.Name = "TimeIDReceived"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub10(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__10"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjAppearanceAllowanceAmount" , 1)
			oColumn.Name = "AdjAppearanceAllowanceAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjAppearanceAllowanceAmount" , 1)
			oColumn.Name = "AdjAppearanceAllowanceAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub11(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__11"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjBettermentAmount" , 1)
			oColumn.Name = "AdjBettermentAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjBettermentAmount" , 1)
			oColumn.Name = "AdjBettermentAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub12(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__12"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjDeductibleAmount" , 1)
			oColumn.Name = "AdjDeductibleAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjDeductibleAmount" , 1)
			oColumn.Name = "AdjDeductibleAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub13(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__13"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjOtherAmount" , 1)
			oColumn.Name = "AdjOtherAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjOtherAmount" , 1)
			oColumn.Name = "AdjOtherAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub14(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__14"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjRelatedPriorDamAmount" , 1)
			oColumn.Name = "AdjRelatedPriorDamAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjRelatedPriorDamAmount" , 1)
			oColumn.Name = "AdjRelatedPriorDamAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub15(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__15"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("AdjUnrelatedPriorDamAmount" , 1)
			oColumn.Name = "AdjUnrelatedPriorDamAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("AdjUnrelatedPriorDamAmount" , 1)
			oColumn.Name = "AdjUnrelatedPriorDamAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub16(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__16"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("BodyRepairHrs" , 1)
			oColumn.Name = "BodyRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("BodyRepairHrs" , 1)
			oColumn.Name = "BodyRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub17(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__17"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("BodyReplaceHrs" , 1)
			oColumn.Name = "BodyReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("BodyReplaceHrs" , 1)
			oColumn.Name = "BodyReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub18(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__18"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DaysSinceAssignment" , 1)
			oColumn.Name = "DaysSinceAssignment"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 3
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DaysSinceAssignment" , 1)
			oColumn.Name = "DaysSinceAssignment"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 4
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub19(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__19"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("DocumentID" , 1)
			oColumn.Name = "DocumentID"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 20
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("DocumentID" , 1)
			oColumn.Name = "DocumentID"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub20(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__20"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("EstimateBalancedFlag" , 1)
			oColumn.Name = "EstimateBalancedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 11
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("EstimateBalancedFlag" , 1)
			oColumn.Name = "EstimateBalancedFlag"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 5
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub21(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__21"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("EstSuppSequenceNumber" , 1)
			oColumn.Name = "EstSuppSequenceNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 17
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("EstSuppSequenceNumber" , 1)
			oColumn.Name = "EstSuppSequenceNumber"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 1
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub22(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__22"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FrameRepairHrs" , 1)
			oColumn.Name = "FrameRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FrameRepairHrs" , 1)
			oColumn.Name = "FrameRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub23(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__23"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("FrameReplaceHrs" , 1)
			oColumn.Name = "FrameReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("FrameReplaceHrs" , 1)
			oColumn.Name = "FrameReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub24(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__24"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborBodyHours" , 1)
			oColumn.Name = "LaborBodyHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborBodyHours" , 1)
			oColumn.Name = "LaborBodyHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub25(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__25"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborBodyRate" , 1)
			oColumn.Name = "LaborBodyRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborBodyRate" , 1)
			oColumn.Name = "LaborBodyRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub26(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__26"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborBodyAmount" , 1)
			oColumn.Name = "LaborBodyAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborBodyAmount" , 1)
			oColumn.Name = "LaborBodyAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub27(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__27"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborFrameHours" , 1)
			oColumn.Name = "LaborFrameHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborFrameHours" , 1)
			oColumn.Name = "LaborFrameHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub28(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__28"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborFrameRate" , 1)
			oColumn.Name = "LaborFrameRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborFrameRate" , 1)
			oColumn.Name = "LaborFrameRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub29(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__29"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborFrameAmount" , 1)
			oColumn.Name = "LaborFrameAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborFrameAmount" , 1)
			oColumn.Name = "LaborFrameAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub30(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__30"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborMechHours" , 1)
			oColumn.Name = "LaborMechHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborMechHours" , 1)
			oColumn.Name = "LaborMechHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub31(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__31"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborMechRate" , 1)
			oColumn.Name = "LaborMechRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborMechRate" , 1)
			oColumn.Name = "LaborMechRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub32(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__32"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborMechAmount" , 1)
			oColumn.Name = "LaborMechAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborMechAmount" , 1)
			oColumn.Name = "LaborMechAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub33(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__33"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRefinishHours" , 1)
			oColumn.Name = "LaborRefinishHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRefinishHours" , 1)
			oColumn.Name = "LaborRefinishHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub34(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__34"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRefinishRate" , 1)
			oColumn.Name = "LaborRefinishRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRefinishRate" , 1)
			oColumn.Name = "LaborRefinishRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub35(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__35"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborRefinishAmount" , 1)
			oColumn.Name = "LaborRefinishAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborRefinishAmount" , 1)
			oColumn.Name = "LaborRefinishAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub36(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__36"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborTotalHours" , 1)
			oColumn.Name = "LaborTotalHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborTotalHours" , 1)
			oColumn.Name = "LaborTotalHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub37(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__37"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("LaborTotalAmount" , 1)
			oColumn.Name = "LaborTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("LaborTotalAmount" , 1)
			oColumn.Name = "LaborTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub38(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__38"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MaterialsHours" , 1)
			oColumn.Name = "MaterialsHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MaterialsHours" , 1)
			oColumn.Name = "MaterialsHours"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub39(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__39"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MaterialsRate" , 1)
			oColumn.Name = "MaterialsRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MaterialsRate" , 1)
			oColumn.Name = "MaterialsRate"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub40(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__40"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MaterialsAmount" , 1)
			oColumn.Name = "MaterialsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MaterialsAmount" , 1)
			oColumn.Name = "MaterialsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub41(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__41"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MechanicalRepairHrs" , 1)
			oColumn.Name = "MechanicalRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MechanicalRepairHrs" , 1)
			oColumn.Name = "MechanicalRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub42(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__42"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("MechanicalReplaceHrs" , 1)
			oColumn.Name = "MechanicalReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("MechanicalReplaceHrs" , 1)
			oColumn.Name = "MechanicalReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub43(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__43"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("NetAmount" , 1)
			oColumn.Name = "NetAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("NetAmount" , 1)
			oColumn.Name = "NetAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub44(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__44"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("OtherAmount" , 1)
			oColumn.Name = "OtherAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("OtherAmount" , 1)
			oColumn.Name = "OtherAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub45(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__45"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartDiscountAmount" , 1)
			oColumn.Name = "PartDiscountAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartDiscountAmount" , 1)
			oColumn.Name = "PartDiscountAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub46(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__46"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartMarkupAmount" , 1)
			oColumn.Name = "PartMarkupAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartMarkupAmount" , 1)
			oColumn.Name = "PartMarkupAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub47(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__47"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsNewAmount" , 1)
			oColumn.Name = "PartsNewAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsNewAmount" , 1)
			oColumn.Name = "PartsNewAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub48(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__48"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsLKQAmount" , 1)
			oColumn.Name = "PartsLKQAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsLKQAmount" , 1)
			oColumn.Name = "PartsLKQAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub49(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__49"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsAFMKAmount" , 1)
			oColumn.Name = "PartsAFMKAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsAFMKAmount" , 1)
			oColumn.Name = "PartsAFMKAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub50(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__50"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsRemanAmount" , 1)
			oColumn.Name = "PartsRemanAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsRemanAmount" , 1)
			oColumn.Name = "PartsRemanAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub51(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__51"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("PartsTotalAmount" , 1)
			oColumn.Name = "PartsTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("PartsTotalAmount" , 1)
			oColumn.Name = "PartsTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub52(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__52"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RefinishRepairHrs" , 1)
			oColumn.Name = "RefinishRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RefinishRepairHrs" , 1)
			oColumn.Name = "RefinishRepairHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub53(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__53"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("RefinishReplaceHrs" , 1)
			oColumn.Name = "RefinishReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 131
			oColumn.Precision = 9
			oColumn.NumericScale = 1
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("RefinishReplaceHrs" , 1)
			oColumn.Name = "RefinishReplaceHrs"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 19
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub54(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__54"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("StorageAmount" , 1)
			oColumn.Name = "StorageAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("StorageAmount" , 1)
			oColumn.Name = "StorageAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub55(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__55"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("SubletAmount" , 1)
			oColumn.Name = "SubletAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("SubletAmount" , 1)
			oColumn.Name = "SubletAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub56(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__56"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TaxLaborAmount" , 1)
			oColumn.Name = "TaxLaborAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TaxLaborAmount" , 1)
			oColumn.Name = "TaxLaborAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub57(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__57"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TaxPartsAmount" , 1)
			oColumn.Name = "TaxPartsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TaxPartsAmount" , 1)
			oColumn.Name = "TaxPartsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub58(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__58"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TaxMaterialsAmount" , 1)
			oColumn.Name = "TaxMaterialsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TaxMaterialsAmount" , 1)
			oColumn.Name = "TaxMaterialsAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub59(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__59"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TaxTotalAmount" , 1)
			oColumn.Name = "TaxTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TaxTotalAmount" , 1)
			oColumn.Name = "TaxTotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub60(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__60"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TotalAmount" , 1)
			oColumn.Name = "TotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 24
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TotalAmount" , 1)
			oColumn.Name = "TotalAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub61(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__61"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("TowingAmount" , 1)
			oColumn.Name = "TowingAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 120
			oColumn.Size = 0
			oColumn.DataType = 6
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("TowingAmount" , 1)
			oColumn.Name = "TowingAmount"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 8
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub62(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__62"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = False
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_application" , 1)
			oColumn.Name = "data_strip_application"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 18
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

Public Sub oCustomTask13_Trans_Sub63(ByVal oCustomTask13 As Object)

	Dim oTransformation As DTS.Transformation2
	Dim oTransProps as DTS.Properties
	Dim oColumn As DTS.Column
	Set oTransformation = oCustomTask13.Transformations.New("DTS.DataPumpTransformCopy")
		oTransformation.Name = "DTSTransformation__63"
		oTransformation.TransformFlags = 63
		oTransformation.ForceSourceBlobsBuffered = 0
		oTransformation.ForceBlobsInMemory = False
		oTransformation.InMemoryBlobSize = 1048576
		oTransformation.TransformPhases = 4
		
		Set oColumn = oTransformation.SourceColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 96
			oColumn.Size = 30
			oColumn.DataType = 129
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.SourceColumns.Add oColumn
		Set oColumn = Nothing

		Set oColumn = oTransformation.DestinationColumns.New("data_strip_date" , 1)
			oColumn.Name = "data_strip_date"
			oColumn.Ordinal = 1
			oColumn.Flags = 0
			oColumn.Size = 30
			oColumn.DataType = 8
			oColumn.Precision = 0
			oColumn.NumericScale = 0
			oColumn.Nullable = True
			
		oTransformation.DestinationColumns.Add oColumn
		Set oColumn = Nothing

	Set oTransProps = oTransformation.TransformServerProperties

		
	Set oTransProps = Nothing

	oCustomTask13.Transformations.Add oTransformation
	Set oTransformation = Nothing

End Sub

'------------- define Task_Sub14 for task DTSTask_DTSCreateProcessTask_1 (Execute Process Task: PUT DTWH files)
Public Sub Task_Sub14(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask14 As DTS.CreateProcessTask2
Set oTask = goPackage.Tasks.New("DTSCreateProcessTask")
oTask.Name = "DTSTask_DTSCreateProcessTask_1"
Set oCustomTask14 = oTask.CustomTask

	oCustomTask14.Name = "DTSTask_DTSCreateProcessTask_1"
	oCustomTask14.Description = "Execute Process Task: PUT DTWH files"
	oCustomTask14.ProcessCommandLine = "ftp -n -s:\\sftmapdprdsql\n$\database\sqlserver\sql2k\admin\pickup\DTWH.ftp ugofarg1.ccs.ppg.com"
	oCustomTask14.SuccessReturnCode = 0
	oCustomTask14.Timeout = 0
	oCustomTask14.TerminateProcessAfterTimeout = False
	oCustomTask14.FailPackageOnTimeout = True
	
goPackage.Tasks.Add oTask
Set oCustomTask14 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub15 for task DTSTask_DTSSendMailTask_1 (Send Mail Task: Completion Notification)
Public Sub Task_Sub15(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask15 As DTS.SendMailTask
Set oTask = goPackage.Tasks.New("DTSSendMailTask")
oTask.Name = "DTSTask_DTSSendMailTask_1"
Set oCustomTask15 = oTask.CustomTask

	oCustomTask15.Name = "DTSTask_DTSSendMailTask_1"
	oCustomTask15.Description = "Send Mail Task: Completion Notification"
	oCustomTask15.Profile = "MS Exchange Settings"
	oCustomTask15.ToLine = "prdreporting@lynxservices.com; dl-lynxapdprddbadmin@lynxservices.com; homison@ppg.com"
	oCustomTask15.Subject = "NOTICE: Production Admin - DTWH Data Export"
	oCustomTask15.MessageText = "The APD Data Warehouse Data Export was successfully implemented against the Production APD database."
	oCustomTask15.IsNTService = False
	oCustomTask15.SaveMailInSentItemsFolder = True
	
goPackage.Tasks.Add oTask
Set oCustomTask15 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub16 for task DTSTask_DTSSendMailTask_2 (Send Mail Task: Completion Notification)
Public Sub Task_Sub16(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask16 As DTS.SendMailTask
Set oTask = goPackage.Tasks.New("DTSSendMailTask")
oTask.Name = "DTSTask_DTSSendMailTask_2"
Set oCustomTask16 = oTask.CustomTask

	oCustomTask16.Name = "DTSTask_DTSSendMailTask_2"
	oCustomTask16.Description = "Send Mail Task: Completion Notification"
	oCustomTask16.Profile = "MS Exchange Settings"
	oCustomTask16.ToLine = "prdreporting@lynxservices.com; dl-lynxapdprddbadmin@lynxservices.com; homison@ppg.com"
	oCustomTask16.Subject = "ALERT: Production Admin - DTWH Data Export FAILURE!"
	oCustomTask16.MessageText = "The APD Data Warehouse Data Export FAILED against the Production APD database."
	oCustomTask16.IsNTService = False
	oCustomTask16.SaveMailInSentItemsFolder = True
	
goPackage.Tasks.Add oTask
Set oCustomTask16 = Nothing
Set oTask = Nothing

End Sub

