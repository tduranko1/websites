Attribute VB_Name = "Module1"
'****************************************************************
'Microsoft SQL Server 2000
'Visual Basic file generated for DTS Package
'File Name: D:\Work\Database\APD\DTS\upkg_DBA_Restore_udb_fnol_dev.bas
'Package Name: upkg_DBA_Restore_udb_fnol_dev
'Package Description: DTS package to restore a production FNOL backup to DEV
'Generated Date: 6/29/2004
'Generated Time: 10:13:06 AM
'****************************************************************

Option Explicit
Public goPackageOld As New DTS.Package
Public goPackage As DTS.Package2
Private Sub Main()
        Set goPackage = goPackageOld

        goPackage.Name = "upkg_DBA_Restore_udb_fnol_dev"
        goPackage.Description = "DTS package to restore a production FNOL backup to DEV"
        goPackage.WriteCompletionStatusToNTEventLog = False
        goPackage.FailOnError = False
        goPackage.PackagePriorityClass = 2
        goPackage.MaxConcurrentSteps = 4
        goPackage.LineageOptions = 0
        goPackage.UseTransaction = True
        goPackage.TransactionIsolationLevel = 4096
        goPackage.AutoCommitTransaction = True
        goPackage.RepositoryMetadataOptions = 0
        goPackage.UseOLEDBServiceComponents = True
        goPackage.LogToSQLServer = False
        goPackage.LogServerName = "(local)"
        goPackage.LogServerFlags = 256
        goPackage.FailPackageOnLogFailure = False
        goPackage.ExplicitGlobalVariables = True
        goPackage.PackageType = 0
        

'---------------------------------------------------------------------------
' begin to write package global variables information
'---------------------------------------------------------------------------

        Dim oGlobal As DTS.GlobalVariable

        Set oGlobal = goPackage.GlobalVariables.New("DBName")
        oGlobal = "FNOL_db_200406280220.bak"
        goPackage.GlobalVariables.Add oGlobal
        Set oGlobal = Nothing

        Set oGlobal = goPackage.GlobalVariables.New("FTPSourceFileName")
        oGlobal = "FNOL_db_200406280220.ba_;'';'';"
        goPackage.GlobalVariables.Add oGlobal
        Set oGlobal = Nothing

        Set oGlobal = goPackage.GlobalVariables.New("DBBackupFileName")
        oGlobal = "FNOL_db_200406280220.bak"
        goPackage.GlobalVariables.Add oGlobal
        Set oGlobal = Nothing

        Set oGlobal = goPackage.GlobalVariables.New("ZIPSourceFileName")
        oGlobal = "FNOL_db_200406280220.ba_"
        goPackage.GlobalVariables.Add oGlobal
        Set oGlobal = Nothing


'---------------------------------------------------------------------------
' create package connection information
'---------------------------------------------------------------------------

Dim oConnection As DTS.Connection2

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("SQLOLEDB")

        oConnection.ConnectionProperties("Integrated Security") = "SSPI"
        oConnection.ConnectionProperties("Persist Security Info") = True
        oConnection.ConnectionProperties("Initial Catalog") = "master"
        oConnection.ConnectionProperties("Data Source") = "SFTMAPDDEVDB1"
        oConnection.ConnectionProperties("Connect Timeout") = 60
        oConnection.ConnectionProperties("Application Name") = "DTS Designer"
        
        oConnection.Name = "SFTMAPDDEVDB1"
        oConnection.ID = 2
        oConnection.Reusable = True
        oConnection.ConnectImmediate = False
        oConnection.DataSource = "SFTMAPDDEVDB1"
        oConnection.ConnectionTimeout = 60
        oConnection.Catalog = "master"
        oConnection.UseTrustedConnection = True
        oConnection.UseDSL = False
        
        'If you have a password for this connection, please uncomment and add your password below.
        'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'------------- a new connection defined below.
'For security purposes, the password is never scripted

Set oConnection = goPackage.Connections.New("SQLOLEDB")

        oConnection.ConnectionProperties("Integrated Security") = "SSPI"
        oConnection.ConnectionProperties("Persist Security Info") = True
        oConnection.ConnectionProperties("Initial Catalog") = "master"
        oConnection.ConnectionProperties("Data Source") = "SFTMAPDPRDSQL"
        oConnection.ConnectionProperties("Connect Timeout") = 60
        oConnection.ConnectionProperties("Application Name") = "DTS Designer"
        
        oConnection.Name = "SFTMAPDPRDSQL"
        oConnection.ID = 4
        oConnection.Reusable = True
        oConnection.ConnectImmediate = False
        oConnection.DataSource = "SFTMAPDPRDSQL"
        oConnection.ConnectionTimeout = 60
        oConnection.Catalog = "master"
        oConnection.UseTrustedConnection = True
        oConnection.UseDSL = False
        
        'If you have a password for this connection, please uncomment and add your password below.
        'oConnection.Password = "<put the password here>"

goPackage.Connections.Add oConnection
Set oConnection = Nothing

'---------------------------------------------------------------------------
' create package steps information
'---------------------------------------------------------------------------

Dim oStep As DTS.Step2
Dim oPrecConstraint As DTS.PrecedenceConstraint

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_Restore"
        oStep.Description = "Execute SQL Task: Restore"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_Restore"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_Secure"
        oStep.Description = "Execute SQL Task: Define security"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_Secure"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSActiveScriptTask_Dialog"
        oStep.Description = "ActiveX Script Task: User Dialog"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSActiveScriptTask_Dialog"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_SetFlags"
        oStep.Description = "Execute SQL Task: Set Flags"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_SetFlags"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSDynamicPropertiesTask_PassVars"
        oStep.Description = "Dynamic Properties Task: Variable passing"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSDynamicPropertiesTask_PassVars"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_Zip"
        oStep.Description = "Execute SQL Task: ZIP Backup File"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_Zip"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSFTPTask_CopyBackup"
        oStep.Description = "File Transfer Protocol Task: Copy backup file"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSFTPTask_CopyBackup"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_UnZip"
        oStep.Description = "Execute SQL Task: UNZIP Backup File"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_UnZip"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = False
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_CleanupDestBak"
        oStep.Description = "Execute SQL Task: Cleanup Destination Backup File"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_CleanupDestBak"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = True
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_CleanupDestZip"
        oStep.Description = "Execute SQL Task: Cleanup Destination ZIP Backup File"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_CleanupDestZip"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = True
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a new step defined below

Set oStep = goPackage.Steps.New

        oStep.Name = "DTSStep_DTSExecuteSQLTask_CleanupSrcZip"
        oStep.Description = "Execute SQL Task: Cleanup Source ZIP Backup File"
        oStep.ExecutionStatus = 1
        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_CleanupSrcZip"
        oStep.CommitSuccess = False
        oStep.RollbackFailure = False
        oStep.ScriptLanguage = "VBScript"
        oStep.AddGlobalVariables = True
        oStep.RelativePriority = 3
        oStep.CloseConnection = False
        oStep.ExecuteInMainThread = False
        oStep.IsPackageDSORowset = False
        oStep.JoinTransactionIfPresent = False
        oStep.DisableStep = True
        oStep.FailPackageOnError = False
        
goPackage.Steps.Add oStep
Set oStep = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_Restore")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_UnZip")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_UnZip"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_Secure")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_Restore")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_Restore"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_SetFlags")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_Secure")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_Secure"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSDynamicPropertiesTask_PassVars")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSActiveScriptTask_Dialog")
        oPrecConstraint.StepName = "DTSStep_DTSActiveScriptTask_Dialog"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_Zip")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSDynamicPropertiesTask_PassVars")
        oPrecConstraint.StepName = "DTSStep_DTSDynamicPropertiesTask_PassVars"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSFTPTask_CopyBackup")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_Zip")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_Zip"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_UnZip")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSFTPTask_CopyBackup")
        oPrecConstraint.StepName = "DTSStep_DTSFTPTask_CopyBackup"
        oPrecConstraint.PrecedenceBasis = 1
        oPrecConstraint.Value = 0
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_CleanupDestBak")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_SetFlags")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_SetFlags"
        oPrecConstraint.PrecedenceBasis = 0
        oPrecConstraint.Value = 4
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_CleanupDestZip")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_SetFlags")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_SetFlags"
        oPrecConstraint.PrecedenceBasis = 0
        oPrecConstraint.Value = 4
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'------------- a precedence constraint for steps defined below

Set oStep = goPackage.Steps("DTSStep_DTSExecuteSQLTask_CleanupSrcZip")
Set oPrecConstraint = oStep.PrecedenceConstraints.New("DTSStep_DTSExecuteSQLTask_SetFlags")
        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_SetFlags"
        oPrecConstraint.PrecedenceBasis = 0
        oPrecConstraint.Value = 4
        
oStep.PrecedenceConstraints.Add oPrecConstraint
Set oPrecConstraint = Nothing

'---------------------------------------------------------------------------
' create package tasks information
'---------------------------------------------------------------------------

'------------- call Task_Sub1 for task DTSTask_DTSActiveScriptTask_Dialog (ActiveX Script Task: User Dialog)
Call Task_Sub1(goPackage)

'------------- call Task_Sub2 for task DTSTask_DTSDynamicPropertiesTask_PassVars (Dynamic Properties Task: Variable passing)
Call Task_Sub2(goPackage)

'------------- call Task_Sub3 for task DTSTask_DTSExecuteSQLTask_Zip (Execute SQL Task: ZIP Backup File)
Call Task_Sub3(goPackage)

'------------- call Task_Sub4 for task DTSTask_DTSFTPTask_CopyBackup (File Transfer Protocol Task: Copy backup file)
Call Task_Sub4(goPackage)

'------------- call Task_Sub5 for task DTSTask_DTSExecuteSQLTask_UnZip (Execute SQL Task: UNZIP Backup File)
Call Task_Sub5(goPackage)

'------------- call Task_Sub6 for task DTSTask_DTSExecuteSQLTask_Restore (Execute SQL Task: Restore)
Call Task_Sub6(goPackage)

'------------- call Task_Sub7 for task DTSTask_DTSExecuteSQLTask_Secure (Execute SQL Task: Define security)
Call Task_Sub7(goPackage)

'------------- call Task_Sub8 for task DTSTask_DTSExecuteSQLTask_SetFlags (Execute SQL Task: Set Flags)
Call Task_Sub8(goPackage)

'------------- call Task_Sub9 for task DTSTask_DTSExecuteSQLTask_CleanupDestBak (Execute SQL Task: Cleanup Destination Backup File)
Call Task_Sub9(goPackage)

'------------- call Task_Sub10 for task DTSTask_DTSExecuteSQLTask_CleanupDestZip (Execute SQL Task: Cleanup Destination ZIP Backup File)
Call Task_Sub10(goPackage)

'------------- call Task_Sub11 for task DTSTask_DTSExecuteSQLTask_CleanupSrcZip (Execute SQL Task: Cleanup Source ZIP Backup File)
Call Task_Sub11(goPackage)

'---------------------------------------------------------------------------
' Save or execute package
'---------------------------------------------------------------------------

goPackage.SaveToSQLServer "SFTMAPDDEVMON1", "PPGNA\csr2720", "", "256"
'goPackage.Execute
tracePackageError goPackage
goPackage.UnInitialize
'to save a package instead of executing it, comment out the executing package lines above and uncomment the saving package line
Set goPackage = Nothing

Set goPackageOld = Nothing

End Sub


'-----------------------------------------------------------------------------
' error reporting using step.GetExecutionErrorInfo after execution
'-----------------------------------------------------------------------------
Public Sub tracePackageError(oPackage As DTS.Package)
Dim ErrorCode As Long
Dim ErrorSource As String
Dim ErrorDescription As String
Dim ErrorHelpFile As String
Dim ErrorHelpContext As Long
Dim ErrorIDofInterfaceWithError As String
Dim i As Integer

        For i = 1 To oPackage.Steps.Count
                If oPackage.Steps(i).ExecutionResult = DTSStepExecResult_Failure Then
                        oPackage.Steps(i).GetExecutionErrorInfo ErrorCode, ErrorSource, ErrorDescription, _
                                        ErrorHelpFile, ErrorHelpContext, ErrorIDofInterfaceWithError
                        MsgBox oPackage.Steps(i).Name & " failed" & vbCrLf & ErrorSource & vbCrLf & ErrorDescription
                End If
        Next i

End Sub

'------------- define Task_Sub1 for task DTSTask_DTSActiveScriptTask_Dialog (ActiveX Script Task: User Dialog)
Public Sub Task_Sub1(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask1 As DTS.ActiveScriptTask
Set oTask = goPackage.Tasks.New("DTSActiveScriptTask")
oTask.Name = "DTSTask_DTSActiveScriptTask_Dialog"
Set oCustomTask1 = oTask.CustomTask

        oCustomTask1.Name = "DTSTask_DTSActiveScriptTask_Dialog"
        oCustomTask1.Description = "ActiveX Script Task: User Dialog"
        oCustomTask1.ActiveXScript = "'**********************************************************************" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "'  Visual Basic ActiveX Script" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "'************************************************************************" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "Function Main()" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "             Dim dtTemp, intLen" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "             dtTemp = Year(Now)" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "             IF Month(Now) < 10 THEN dtTemp = dtTemp & ""0""" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "             dtTemp = dtTemp & Month(Now) " & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "             IF Day(Now) < 10 THEN dtTemp = dtTemp & ""0""" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     dtTemp = dtTemp & Day(Now)" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     DTSGlobalVariables(""DBName"").Value=""FNOL_db_"" & InputBox(""Database Backup Date   Format: yyyymmdd:"", ""Database Restore"", dtTemp ) &""0220.bak""" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     DTSGlobalVariables(""DBBackupFileName"").Value= InputBox(""Database Backup File:"", ""Database Restore"", DTSGlobalVariables(""DBName"").Value)" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     intLen = Len(DTSGlobalVariables(""DBBackupFileName"").Value)" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     DTSGlobalVariables(""ZIPSourceFileName"").Value=Left(DTSGlobalVariables(""DBBackupFileName"").Value, intLen - 1) & ""_""" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     DTSGlobalVariables(""FTPSourceFileName"").Value=DTSGlobalVariables(""ZIPSourceFileName"").Value & "";'';'';""" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "     Main = DTSTaskExecResult_Success" & vbCrLf
        oCustomTask1.ActiveXScript = oCustomTask1.ActiveXScript & "End Function"
        oCustomTask1.FunctionName = "Main"
        oCustomTask1.ScriptLanguage = "VBScript"
        oCustomTask1.AddGlobalVariables = True
        
goPackage.Tasks.Add oTask
Set oCustomTask1 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub2 for task DTSTask_DTSDynamicPropertiesTask_PassVars (Dynamic Properties Task: Variable passing)
Public Sub Task_Sub2(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask2 As DTSCustTasks.DynamicPropertiesTask
Set oTask = goPackage.Tasks.New("DTSDynamicPropertiesTask")
oTask.Name = "DTSTask_DTSDynamicPropertiesTask_PassVars"
Set oCustomTask2 = oTask.CustomTask

        oCustomTask2.Name = "DTSTask_DTSDynamicPropertiesTask_PassVars"
        oCustomTask2.Description = "Dynamic Properties Task: Variable passing"
        
        Dim oAssignment As DTSCustTasks.DynamicPropertiesTaskAssignment
        '------- An Assignment is defined here

        Set oAssignment = oCustomTask2.Assignments.New
                oAssignment.SourceType = 2
                oAssignment.SourceQueryConnectionID = -1
                oAssignment.SourceGlobalVariable = "FTPSourceFileName"
                oAssignment.DestinationPropertyID = "'Tasks';'DTSTask_DTSFTPTask_CopyBackup';'Properties';'SourceFilename'"
                
        oCustomTask2.Assignments.Add oAssignment
        Set oAssignment = Nothing

goPackage.Tasks.Add oTask
Set oCustomTask2 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub3 for task DTSTask_DTSExecuteSQLTask_Zip (Execute SQL Task: ZIP Backup File)
Public Sub Task_Sub3(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask3 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_Zip"
Set oCustomTask3 = oTask.CustomTask

        oCustomTask3.Name = "DTSTask_DTSExecuteSQLTask_Zip"
        oCustomTask3.Description = "Execute SQL Task: ZIP Backup File"
        oCustomTask3.SQLStatement = "DECLARE @cmd AS VARCHAR(2000)" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "DECLARE @file AS VARCHAR(2000)" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "DECLARE @ZIPfile AS VARCHAR(2000)" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "DECLARE @rc AS INT" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "SET @file = '\\sftmapdprdsql.nac.ppg.com\n$\database\sqlserver\sql2k\admin\maintenance\backup\fnol\' + ?" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "SET @ZIPfile = '\\sftmapdprdsql.nac.ppg.com\n$\database\sqlserver\sql2k\admin\maintenance\backup\fnol\' + ?" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "SET @cmd = '\\sftmapdprdsql.nac.ppg.com\n$\database\sqlserver\sql2k\admin\tools\compress.exe -r -zx ' + @file" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "EXEC master..xp_fileexist @ZIPfile, @rc OUTPUT" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "IF @rc <> 1   -- if file doesn't exists, build it" & vbCrLf
        oCustomTask3.SQLStatement = oCustomTask3.SQLStatement & "    EXEC master..xp_cmdshell @cmd"
        oCustomTask3.ConnectionID = 4
        oCustomTask3.CommandTimeout = 0
        oCustomTask3.InputGlobalVariableNames = """DBBackupFileName"";""ZIPSourceFileName"""
        oCustomTask3.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask3 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub4 for task DTSTask_DTSFTPTask_CopyBackup (File Transfer Protocol Task: Copy backup file)
Public Sub Task_Sub4(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask4 As DTSCustTasks.DTSFTPTask
Set oTask = goPackage.Tasks.New("DTSFTPTask")
oTask.Name = "DTSTask_DTSFTPTask_CopyBackup"
Set oCustomTask4 = oTask.CustomTask

        oCustomTask4.Name = "DTSTask_DTSFTPTask_CopyBackup"
        oCustomTask4.Description = "File Transfer Protocol Task: Copy backup file"
        oCustomTask4.SourceLocation = 1
        oCustomTask4.SourceSite = "\\SFTMAPDPRDSQL.NAC.PPG.COM\N$\Database\SQLServer\SQL2K\admin\maintenance\backup\fnol"
        oCustomTask4.SourceFilename = "FNOL_db_200406280220.ba_;'';'';"
        oCustomTask4.DestSite = "\\SFTMAPDDEVDB1.NAC.PPG.COM\D$\Database\SQLServer\SQL2K\admin\maintenance\backup\udb_fnol"
        oCustomTask4.NonOverwritable = False
        oCustomTask4.NumRetriesOnSource = 0
        
goPackage.Tasks.Add oTask
Set oCustomTask4 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub5 for task DTSTask_DTSExecuteSQLTask_UnZip (Execute SQL Task: UNZIP Backup File)
Public Sub Task_Sub5(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask5 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_UnZip"
Set oCustomTask5 = oTask.CustomTask

        oCustomTask5.Name = "DTSTask_DTSExecuteSQLTask_UnZip"
        oCustomTask5.Description = "Execute SQL Task: UNZIP Backup File"
        oCustomTask5.SQLStatement = "DECLARE @cmd AS VARCHAR(500)" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "DECLARE @file AS VARCHAR(2000)" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "DECLARE @ZIPfile AS VARCHAR(2000)" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "DECLARE @rc AS INT" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "SET @file = '\\sftmapddevdb1.nac.ppg.com\d$\database\sqlserver\sql2k\admin\maintenance\backup\udb_fnol\' + ?" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "SET @ZIPfile = '\\sftmapddevdb1.nac.ppg.com\d$\database\sqlserver\sql2k\admin\maintenance\backup\udb_fnol\' + ?" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "SET @cmd = '\\sftmapdprdsql.nac.ppg.com\n$\database\sqlserver\sql2k\admin\tools\expand.exe -r ' + @ZIPfile" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "EXEC master..xp_fileexist @file, @rc OUTPUT" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "IF @rc <> 1   -- if backup file doesn't exists, build it" & vbCrLf
        oCustomTask5.SQLStatement = oCustomTask5.SQLStatement & "        EXEC master..xp_cmdshell @cmd"
        oCustomTask5.ConnectionID = 2
        oCustomTask5.CommandTimeout = 0
        oCustomTask5.InputGlobalVariableNames = """DBBackupFileName"";""ZIPSourceFileName"""
        oCustomTask5.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask5 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub6 for task DTSTask_DTSExecuteSQLTask_Restore (Execute SQL Task: Restore)
Public Sub Task_Sub6(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask6 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_Restore"
Set oCustomTask6 = oTask.CustomTask

        oCustomTask6.Name = "DTSTask_DTSExecuteSQLTask_Restore"
        oCustomTask6.Description = "Execute SQL Task: Restore"
        oCustomTask6.SQLStatement = "DECLARE @Statement as varchar(8000)" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "DECLARE @DBName as varchar(100)" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "USE master" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "SET NOCOUNT ON" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "SET @DBName =?" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "SET @Statement = '" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "RESTORE DATABASE udb_fnol_dev" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   FROM DISK= ''D:\Database\SQLServer\SQL2K\admin\maintenance\backup\udb_fnol\' + @DBName + '''" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   WITH" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   MOVE ''FNOL_Data'' TO ''D:\Database\SQLServer\SQL2K\storage\data\udb_fnol_dev\fnol_data.mdf''," & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   MOVE ''FNOL_Log'' TO ''D:\Database\SQLServer\SQL2K\storage\log\udb_fnol_dev\fnol_log.ldf''," & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   REPLACE'" & vbCrLf
        oCustomTask6.SQLStatement = oCustomTask6.SQLStatement & "   EXEC (@Statement)"
        oCustomTask6.ConnectionID = 2
        oCustomTask6.CommandTimeout = 0
        oCustomTask6.InputGlobalVariableNames = """DBBackupFileName"""
        oCustomTask6.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask6 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub7 for task DTSTask_DTSExecuteSQLTask_Secure (Execute SQL Task: Define security)
Public Sub Task_Sub7(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask7 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_Secure"
Set oCustomTask7 = oTask.CustomTask

        oCustomTask7.Name = "DTSTask_DTSExecuteSQLTask_Secure"
        oCustomTask7.Description = "Execute SQL Task: Define security"
        oCustomTask7.SQLStatement = "DECLARE @dbname AS VARCHAR(32)" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "SET @dbname = 'udb_fnol_dev'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "USE udb_fnol_dev" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- !!!! FOR NEW SERVERS ONLY !!!!" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- Remove existing users (their status may be invalid within sysusers)" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "IF EXISTS (SELECT * FROM sysusers WHERE name = 'PPGNA\lynxapddevelopers')" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "    EXEC sp_revokedbaccess 'PPGNA\lynxapddevelopers'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- !!!! FOR NEW SERVERS ONLY !!!!" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- Drop logins:" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- IF EXISTS (SELECT loginname FROM master.dbo.syslogins WHERE loginname = 'fnol_dbo')" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "--     EXEC sp_droplogin 'fnol_dbo'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- Create new SQL Server logins:" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "--EXEC sp_grantlogin 'PPGNA\lynxapddevelopers'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- Add security accounts in the current database:" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & " " & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "EXEC sp_grantdbaccess 'PPGNA\lynxapddevelopers'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "EXEC sp_change_users_login 'Update_One', 'fnol_dbo', 'fnol_dbo'" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "-- Add security accounts as a member of an existing database role in the current database:" & vbCrLf
        oCustomTask7.SQLStatement = oCustomTask7.SQLStatement & "EXEC sp_addrolemember 'db_owner', 'PPGNA\lynxapddevelopers'"
        oCustomTask7.ConnectionID = 2
        oCustomTask7.CommandTimeout = 0
        oCustomTask7.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask7 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub8 for task DTSTask_DTSExecuteSQLTask_SetFlags (Execute SQL Task: Set Flags)
Public Sub Task_Sub8(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask8 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_SetFlags"
Set oCustomTask8 = oTask.CustomTask

        oCustomTask8.Name = "DTSTask_DTSExecuteSQLTask_SetFlags"
        oCustomTask8.Description = "Execute SQL Task: Set Flags"
        oCustomTask8.SQLStatement = "USE udb_fnol_dev" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "TRUNCATE TABLE dbo.tblFlags" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "INSERT INTO dbo.tblFlags" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "       (bitEMailErrors," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        bitSubmitAPD," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        strAPDConnection," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        strAPDConnection_Dev," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        strAPDUrl_Demo," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        strAPDUrl_Dev," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        strInstance)" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "VALUES" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "       (1," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        0," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        'provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_fnol_dev;user id=usl_fnolload;password=d20ll0nf;'," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        'provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_fnol_dev;user id=usl_fnolload;password=d20ll0nf;'," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        'sftmapddevweb/fnolvalidate.asp '," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        'lynxapddev/fnolvalidate.asp '," & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "        'Test')" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "--*******************************************************************************************" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "--  This script it to be run  Dev, Test, or Training environments after refreshing those " & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "--  environments from Production" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "--*******************************************************************************************" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "-- First delete all destinations from tblData_Reports but mine " & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "DELETE FROM tblData_Reports" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "WHERE txtDestination <> 'jleatherwood@lynxservices.com'" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "-- Disable all destinations in utb_destinations but mine" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "UPDATE utb_destinations" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "SET enabledflag = 0" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "WHERE EmailAddress <> 'jleatherwood@lynxservices.com'" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "-- Turn off all Task Runner Tasks" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "UPDATE utb_task_detail" & vbCrLf
        oCustomTask8.SQLStatement = oCustomTask8.SQLStatement & "SET enabledflag = 0"
        oCustomTask8.ConnectionID = 2
        oCustomTask8.CommandTimeout = 0
        oCustomTask8.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask8 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub9 for task DTSTask_DTSExecuteSQLTask_CleanupDestBak (Execute SQL Task: Cleanup Destination Backup File)
Public Sub Task_Sub9(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask9 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_CleanupDestBak"
Set oCustomTask9 = oTask.CustomTask

        oCustomTask9.Name = "DTSTask_DTSExecuteSQLTask_CleanupDestBak"
        oCustomTask9.Description = "Execute SQL Task: Cleanup Destination Backup File"
        oCustomTask9.SQLStatement = "DECLARE @cmd AS VARCHAR(500)" & vbCrLf
        oCustomTask9.SQLStatement = oCustomTask9.SQLStatement & "SET @cmd = 'del \\sftmapddevdb1.nac.ppg.com\d$\database\sqlserver\sql2k\admin\maintenance\backup\udb_fnol\' + ?" & vbCrLf
        oCustomTask9.SQLStatement = oCustomTask9.SQLStatement & "EXEC master..xp_cmdshell @cmd"
        oCustomTask9.ConnectionID = 2
        oCustomTask9.CommandTimeout = 0
        oCustomTask9.InputGlobalVariableNames = """DBBackupFileName"""
        oCustomTask9.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask9 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub10 for task DTSTask_DTSExecuteSQLTask_CleanupDestZip (Execute SQL Task: Cleanup Destination ZIP Backup File)
Public Sub Task_Sub10(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask10 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_CleanupDestZip"
Set oCustomTask10 = oTask.CustomTask

        oCustomTask10.Name = "DTSTask_DTSExecuteSQLTask_CleanupDestZip"
        oCustomTask10.Description = "Execute SQL Task: Cleanup Destination ZIP Backup File"
        oCustomTask10.SQLStatement = "DECLARE @cmd AS VARCHAR(500)" & vbCrLf
        oCustomTask10.SQLStatement = oCustomTask10.SQLStatement & "SET @cmd = 'del \\sftmapddevdb1.nac.ppg.com\d$\database\sqlserver\sql2k\admin\maintenance\backup\udb_fnol\' + ?" & vbCrLf
        oCustomTask10.SQLStatement = oCustomTask10.SQLStatement & "EXEC master..xp_cmdshell @cmd"
        oCustomTask10.ConnectionID = 2
        oCustomTask10.CommandTimeout = 0
        oCustomTask10.InputGlobalVariableNames = "ZIPSourceFileName"
        oCustomTask10.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask10 = Nothing
Set oTask = Nothing

End Sub

'------------- define Task_Sub11 for task DTSTask_DTSExecuteSQLTask_CleanupSrcZip (Execute SQL Task: Cleanup Source ZIP Backup File)
Public Sub Task_Sub11(ByVal goPackage As Object)

Dim oTask As DTS.Task
Dim oLookup As DTS.Lookup

Dim oCustomTask11 As DTS.ExecuteSQLTask2
Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
oTask.Name = "DTSTask_DTSExecuteSQLTask_CleanupSrcZip"
Set oCustomTask11 = oTask.CustomTask

        oCustomTask11.Name = "DTSTask_DTSExecuteSQLTask_CleanupSrcZip"
        oCustomTask11.Description = "Execute SQL Task: Cleanup Source ZIP Backup File"
        oCustomTask11.SQLStatement = "DECLARE @cmd AS VARCHAR(500)" & vbCrLf
        oCustomTask11.SQLStatement = oCustomTask11.SQLStatement & "SET @cmd = 'del \\sftmapdprdsql.nac.ppg.com\n$\database\sqlserver\sql2k\admin\maintenance\backup\udb_fnol\' + ?" & vbCrLf
        oCustomTask11.SQLStatement = oCustomTask11.SQLStatement & "EXEC master..xp_cmdshell @cmd"
        oCustomTask11.ConnectionID = 4
        oCustomTask11.CommandTimeout = 0
        oCustomTask11.InputGlobalVariableNames = "ZIPSourceFileName"
        oCustomTask11.OutputAsRecordset = False
        
goPackage.Tasks.Add oTask
Set oCustomTask11 = Nothing
Set oTask = Nothing

End Sub

