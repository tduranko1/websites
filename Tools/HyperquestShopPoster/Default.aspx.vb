﻿Public Class _Default
    Inherits System.Web.UI.Page

    Dim sResponseXML As String = ""
    Dim sRequestXML As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sRqUID As String = ""
        Dim dtNow As New Date

        '<HQProviderSearchRq xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        '	<RqUID></RqUID>					<!-- Request ID for this search REQUIRED -->
        '	<SearchDt></SearchDt>			<!-- Date / Time stamp of request REQUIRED -->
        '	<PartnerKey></PartnerKey>		<!-- Partner requesting this search REQUIRED -->
        '	<CompanyID></CompanyID>			<!-- Company ID of Partner Requesting this search REQUIRED -->
        '	<ZipCode></ZipCode>				<!-- Zip code of Shop to search for. REQUIRED -->
        '	<Mileage></Mileage>				<!--0 or 1-10 miles : 0 = No Radius Search DEFAULT: 10 miles -->
        '	<ShopNameLike></ShopNameLike>	<!-- Shop name begins with this string. DEFAULT: all shops within range -->
        '	<ShopTelephone></ShopTelephone>	<!-- Shop Telephone number in CEICA 4.6 format +Ccccc-Aaaaa-LLLlllll+XXXXXXXXXX Ex: +1-800-5551212-12345 -->
        '	<ShopEmail></ShopEmail>			<!-- Shop Email Address -->
        '	<SortOrderCode></SortOrderCode>	<!-- Requested Sort Order Code: Valid codes are: SN [ShopName] | CT [City] | ZIP [ZipPostalCode] | DS [Distance]. If Not specified or invalid then Distance will be the default -->
        '</HQProviderSearchRq>

        sRqUID = System.Guid.NewGuid.ToString()
        dtNow = Date.Now

        sRequestXML = "<HQProviderSearchRq xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">"
        sRequestXML += "<RqUID>" & sRqUID & "</RqUID>"
        'sRequestXML += "<SearchDt>" & dtNow & "</SearchDt>"
        sRequestXML += "<SearchDt>2015-10-19T00:00:01</SearchDt>"
        sRequestXML += "<PartnerKey>LYNX</PartnerKey>"
        sRequestXML += "<CompanyID>184</CompanyID>"
        sRequestXML += "<ZipCode>15301</ZipCode>"
        sRequestXML += "<MilesRadius>50</MilesRadius>"
        sRequestXML += "<ShopNameLike></ShopNameLike>"
        sRequestXML += "<ShopTelephone></ShopTelephone>"
        sRequestXML += "<ShopEmail></ShopEmail>"
        sRequestXML += "<SortOrderCode>DS</SortOrderCode>"
        sRequestXML += "</HQProviderSearchRq>"
        txtXML.Text = sRequestXML
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSend.Click
        Dim oHyperquestService = New HyperquestService.ProviderSearchServiceClient

        Dim ShopSearchClient As New HyperquestService.ProviderSearchServiceClient
        ShopSearchClient.ClientCredentials.UserName.UserName = "corp\B2BLynxProv"
        ShopSearchClient.ClientCredentials.UserName.Password = "Apple?.!11"

        sResponseXML = ShopSearchClient.GetProviderSearchResult(sRequestXML)
        txtXML.Text = sResponseXML

    End Sub

    Protected Sub btnSendProd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSendProd.Click
        Dim oHyperquestService = New HyperquestServiceProd.ProviderSearchServiceClient

        Dim ShopSearchClient As New HyperquestServiceProd.ProviderSearchServiceClient
        ShopSearchClient.ClientCredentials.UserName.UserName = "Corp\ProdProvSrch"
        ShopSearchClient.ClientCredentials.UserName.Password = "Q8Welcome?.!11"

        sResponseXML = ShopSearchClient.GetProviderSearchResult(sRequestXML)
        txtXML.Text = ""
        txtXML.Text = sResponseXML

    End Sub
End Class