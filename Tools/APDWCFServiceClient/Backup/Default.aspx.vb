﻿Imports System.Xml
Imports System.Xml.Serialization
Imports APDWCFServiceClient.APDFoundation
Imports APDWCFServiceClient.APDFoundationLocal
Imports APDWCFServiceClient.APDFoundationDirectLocal
Imports System.IO

Imports System.Net
Imports System.Xml.Schema


Public Class _Default
    Inherits System.Web.UI.Page
    'Dim APDFoundation As New APDFoundation.APDService
    'Dim wsAPDClaim As New APDFoundation.APDClaim
    'Dim wsAPDVehicle As APDFoundation.APDVehicle

    '---------------------------------------------
    ' This WS connects to the DEV front end WS and 
    ' processes to the backend DEV WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDClaimPointFoundationDEV.APDService

    '---------------------------------------------
    ' This WS connects to the Stage front end WS and 
    ' processes to the backend WS Foundation
    '---------------------------------------------
    Dim APDFoundation As New APDClaimPointFoundation.APDService

    '---------------------------------------------
    ' This WS connects to the PRODUCTION front end WS and 
    ' processes to the backend PRODUCTION WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDClaimPointFoundationPROD.APDService

    '---------------------------------------------
    ' This WS connects to the back end DEV WS and 
    ' processes without the front end
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundation.APDService

    '---------------------------------------------
    ' This WS connects directly to Local DotNet 
    ' running debug session 
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundationLocal.APDService

    '---------------------------------------------
    ' This WS connects directly to Stage backend
    ' WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundationDirectLocal.APDService

    'Dim wsAPDClaim As New APDFoundationLocal.APDClaim
    'Dim wsAPDVehicle As APDFoundationLocal.APDVehicle

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim XMLInput As New XmlDocument
        Dim sLynxID As String = ""

        'XMLInput.Load("c:\temp\QBEWSClaim13Stage.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13AddVehicle.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13MinData.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13MinDataMultiVehicle.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13.xml")

        '------------------------------------------
        ' Dev Test Cases - Carrier Rep validate/create
        '------------------------------------------
        '---- Good DEV PS Claim ---
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_DEV_1st_Party_MASTER.xml") '-- (GOOD - Tested 08Jul2014)

        '---- Misc ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Westfield_PS_DEV_Request-MinimalVehicleData.xml") '-- (GOOD - Tested 24Oct2013)
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Real_ClaimPoint_PS_Claim.xml") '-- 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP.xml")  ' -- GOOD Claim Process --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_Diff_Ins_Inv.xml")  ' -- GOOD Claim Process Different Insured vs Involved --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_DEV_NoRep.xml")  ' -- No CarrierRepID passed, but CarrierRepEmail is good --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_DEV_CreateNewCarrierRep.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle_two_first_party.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle_Exact_Claimpoint.xml")

        '---- Process a single vehicle claim and then a vehicle add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_SingleVehicle.xml")  '-- Create new Claim Rep with Office

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_SingleVehicle.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_AddVehicle_Exp3.xml")  '-- Vehicle Add Exp 3 Good
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_AddVehicle_Exp1.xml")  '-- Vehicle Add Exp 3 Good
        '---- Rental Testcase ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_UM_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_UIM_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_COMP_Request_With_Rental.xml")  '-- Claim Rep via RepID
        '---- Create new Claim Rep with Office/Claimpoint Add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_Request_With_New_Claim_Rep.xml")  '-- Create new Claim Rep with Office

        '===============================================
        ' Stage Test Cases - Carrier Rep validate/create
        '===============================================
        ' === QBE RRP ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Westfield_PS_STAGE_Request-MinimalVehicleData.xml") '-- (GOOD - Tested 24Oct2013)

        ' === Jason Testcase ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013.xml")

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c01.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c02.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c03.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c04.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c05.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c06.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c07.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_c08.xml")

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_a09.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_a10.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_a11.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_a12.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_a13.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013_b14.xml")

        ' === Westfield Claim Rep Tests ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WestfieldPayload24Oct2014.xml")  ' Live Test PROD
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WestfieldPayload24Oct2014_PROD.xml")  ' Live Westfield PROD Test
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WestfieldPayload_05Jun2015a.xml")

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_09Oct2014_RepTest1.xml")  ' Test using <CarrierRepUserID>
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_09Oct2014_RepTest2.xml")  ' Test using <CarrierRepEmailAddress>
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_09Oct2014_RepTest3x.xml")  ' Test creating a new Carrier Rep
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_09Oct2014_RepTest4.xml")  ' Test actual Westfield live XML
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_28Apr2015_RepTest5.xml")  ' Test creating a new Carrier Rep

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_09Feb2015_SpecialChar.xml")  ' Test special char in name

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_STAGE_Request.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData_ClaimRep_via_Email.xml")  '-- Claim Rep via Email
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData_ClaimRep_New.xml")  '-- Claim Rep doesn't exist, New create
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_STAGE_Request_Multi_Vehicle.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_MultiVehicle_Exact_Claimpoint.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadXML_13Mar2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadXML_13Mar2013_Orig.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_25Jul2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130319.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130319x.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Test100674Ncc1212.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_TestKen002.xml")
        '---- Process a single vehicle claim and then a vehicle add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_SingleVehicle.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_02Apr2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130404.xml")
        '---- Carrier Rep Testing ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep3.xml")  '-- Create new 1st party new carrier rep.  Comp/
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep4.xml")  '-- Veh Add 3rd party same carrier rep as above.  Liab/
        '---- Below errors 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep1.xml")  '-- Create new 3rd party new carrier rep.  Liab/1756447
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep2.xml")  '-- Veh Add 1st party same carrier rep as above.  Comp/1756447

        ' === Misc ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ClaimpointUsingWS.xml")

        ' === Electric DRP ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ElectReprocess_22Oct2014.xml") '-- (reprocess)
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ElectReprocess_20Feb2015.xml") '-- (reprocess)

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013.xml") '-- (GOOD - Tested 24Oct2013) 1st party
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_STAGE_Request.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_STAGE_Request_2_Vehicles.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\LivePayload_08Apr2013A.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ShopError.xml") '-- Correcting shop key constraints
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_3rd_Party_First.xml") '--3rd party 1st create insured involved.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ServChanError.xml") '--Service Channel Error.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ServChanErrorA.xml") '--Service Channel Error.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_Error.xml") '--Multi 1st party???

        'REPUBLIC
        '--------------------------
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Republic_Stage_1st_Party_02Nov2015.xml") '-- (GOOD - Tested 02Nov2015) 1st party
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\RepublicPayload_08Dev2015.xml") ' REPUBLIC Payload attempt
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\RepublicPayload_11Mar2016.xml") ' REPUBLIC Payload attempt
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Republic_Stage_1st_Party_CAPS.xml") '-- (GOOD - Tested 02Nov2015) 1st party

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\TXFBPayload_14Sep2016-1.xml") ' REPUBLIC Payload attempt

        'CINCINNATI
        '--------------------------
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Republic_Stage_1st_Party_02Nov2015.xml") '-- (GOOD - Tested 02Nov2015) 1st party
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Cincinnati_Stage_1st_Party_08Jun2017.xml") ' CINC Payload attempt
        XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Cincinnati_Stage_1st_Party_23Jul2018.xml") ' CINC Payload attempt
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_19Sep2016.xml") ' Good Cinc Claim -- BAD ONE
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Good Claim.xml") ' Good Cinc Claim PROD
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_19Sep2016_NewClaimRep.xml") ' Cinc with new rep 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_20Oct2016.xml") ' Cinc with new rep 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_09May2017-1.xml") ' Cinc with new rep 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_13Sep2016x.xml") ' REPUBLIC Payload attempt
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_14Sep2016.xml") ' REPUBLIC Payload attempt

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\CincPayload_PRODUCTION_03Nov2016.xml") ' Cinc PRODUCTION TEST Claim 

        '--------------------------
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_3rdAdd_Party_17Apr2013.xml") '--3rd party
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_NewRep.xml") '--1st party NewRep

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\westfield_payload_27Feb2014.xml")  ' -- Westfield Live

        ' === Choice Testing ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim.xml") ' PS/DRP Choice Assignment-Using Lynx Shop 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NoShopID.xml") ' PS/DRP Choice Assignment-No ShopID 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service go uAssignments\ChoiceAssignmentTEST_NoneExistingShopID.xml") ' PS/DRP Choice Assignment-None Existing ShopID 

        ' STAGE
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_ACE.xml") ' PS/DRP Choice Assignment-Using Lynx Shop for ACE - RRP 
        '*** XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_UTICA.xml") ' PS/DRP Choice Assignment-Using Lynx Shop for UTICA - RRP -- GOOD
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_UTICA1.xml") ' PS/DRP Choice Assignment-Using Lynx Shop for UTICA - RRP 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_UTICA2_New_Choice_Shop.xml") ' PS/DRP Choice Assignment-Using HQ Shop for UTICA - RRP 

        ' DEV
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_UTICA1_DEV.xml") ' PS/DRP Choice Assignment-Using Lynx Shop for UTICA - RRP 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ChoiceAssignmentTEST_NormalClaim_UTICA2_New_Choice_Shop.xml") ' PS/DRP Choice Assignment-Using HQ Shop for UTICA - RRP 

        '------------------------------------------
        ' Production Test Cases - 
        '------------------------------------------
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Westfield_test_03Mar2015.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Electric_test_03Mar2015.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Westfield_test_23Apr2015.xml")

        ' === ClaimPoint Direct WS Claims ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Real_ClaimPoint_PS_Claim.xml")

        'XMLInput.Load("c:\temp\NugenPayload1.xml")

        '-------------------------
        ' Add basic authentication
        '-------------------------
        APDFoundation.Credentials = New System.Net.NetworkCredential("NugenWSSUser", "Audaexplore!")
        'Response.Write(APDFoundation.Ping())

        '-----------------------------------------
        ' Process the claim
        '-----------------------------------------
        sLynxID = APDFoundation.CreateAPDClaim(XMLInput.InnerXml)
        Response.Write(sLynxID)
        'sLynxID = APDFoundation.CreateAPDClaim("x")
        'Response.Write(APDFoundation.LogEvent("TVD1", "TVD1", "TVD1", "TVD1", "TVD1"))

        'Response.Write(Server.HtmlDecode(sLynxID))
        'Response.Write(Server.HtmlEncode(sLynxID))

        'sLynxID = APDWCFFoundation.Ping()
        'sLynxID = APDWCFFoundation.CreatewsAPDClaim(wsAPDClaim, APDVehicle)
        'sLynxID = APDFoundation.CreateAPDClaim(wsAPDClaim, wsAPDVehicle)

        'Dim oXS As XmlSerializer = New XmlSerializer(GetType(APDClaim))
        'Dim oLucky As New APDClaim()
        'Response.Write(Server.HtmlEncode(oLucky.NewClaimFlag))

        'Dim oStmW As StreamWriter
        'oStmW = New StreamWriter(Server.MapPath("lucky.xml"))
        'oXS.Serialize(oStmW, oLucky)
        'oStmW.Close()
        'Response.Write(Server.HtmlEncode(XMLLynx.InnerText))
    End Sub

    'Private Function SerializeAnObject(ByVal AnObject As Object) As String
    '    Dim Xml_Serializer As XmlSerializer = New XmlSerializer(AnObject.GetType)
    '    Dim Writer As StringWriter = New StringWriter
    '    Xml_Serializer.Serialize(Writer, AnObject)
    '    Return Writer.ToString
    'End Function
End Class