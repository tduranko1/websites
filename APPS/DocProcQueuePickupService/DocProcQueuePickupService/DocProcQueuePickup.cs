﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.Xsl;
using System.Net;

//***************************************************************************************
// Procedure : NOLQueuePickupService
// DateTime  : 03/26/2021 
// Author    : Thomas Duranko
// Purpose   : Transforms Doc Processor XML job files into APD PostOffice XML format
//
// Version   : V1.0 Initial development created by Mahes Kumar
// Version   : V1.1 26Mar2012 - TVD - Adding debugging for a problem with postoffice null insert.
// Version   : V1.2 09Sep2021 - TVD - Removing the EML job in the default bundle send.
//
//*************************************************************************************** 

namespace NOLQueuePickupService
{
    partial class DocProcQueuePickup : ServiceBase
    {
        Timer timer = new Timer();
        string strJobQueue = DocProcQueuePickupService.Properties.Settings.Default.strJobQueue;
        string strArchivepath = DocProcQueuePickupService.Properties.Settings.Default.ArchivePath;


        #region Global Declaration
        string HIGH_PRIORITY = "1_";
        string NORMAL_PRIORITY = "2_";
        string LOW_PRIORITY = "3_";
        string strQueuePath = DocProcQueuePickupService.Properties.Settings.Default.strQueuePath;
        string strPickupPath = DocProcQueuePickupService.Properties.Settings.Default.strPickupPath;
        //string strJobQueue = Properties.Settings.Default.strJobQueue;
        DocProcQueuePickupService.APDFoundation.APDService objAPDFoundation = new DocProcQueuePickupService.APDFoundation.APDService();
        #endregion
        public DocProcQueuePickup()
        {
            InitializeComponent();


        }

        protected override void OnStart(string[] args)
        {
            try
            {
                // TODO: Add code here to start your service.
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnStart()", "Starting...", "");
                if (!System.IO.File.Exists(strJobQueue))
                {

                    using (System.IO.FileStream fs = System.IO.File.Create(strJobQueue))
                    {
                        WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnStart()", "Creating JobQueue.XML....", "");
                        var bytes = Encoding.UTF8.GetBytes("<Jobs/>");
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = DocProcQueuePickupService.Properties.Settings.Default.timerInterval;
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnStart()", "Source : " + ex.Source + "Message : " + ex.Message + ", Trace : " + ex.StackTrace, "");
            }

        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnElapsedTime()", "Windows Service is recall", "");
                //reloadqueue();
                startProcessing();
            }
            catch (Exception ex)
            {
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnElapsedTime()", "Source : " + ex.Source + "Message : " + ex.Message + ", Trace : " + ex.StackTrace, "");
            }


        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-OnStop()", "Stopping...", "");

        }

        #region Function to Write Log
        //***************************************************************************************
        // Procedure : WriteToLogFile
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : Use this procedure to add logs to the text file
        //*************************************************************************************** 
        //public void WriteToLogFile(string strLogMsg)
        //{
        //    //string path = AppDomain.CurrentDomain.BaseDirectory + "\\NOLQueuePickupLogs";
        //    string path = DocProcQueuePickupService.Properties.Settings.Default.LogPath + "\\DocProcQueuePickupService";
        //    if (!Directory.Exists(path))
        //    {
        //        Directory.CreateDirectory(path);
        //    }
        //    //string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\NOLQueuePickupLogs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
        //    string filepath = DocProcQueuePickupService.Properties.Settings.Default.LogPath + "\\DocProcQueuePickupService\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
        //    if (!File.Exists(filepath))
        //    {
        //        // Create a file to write to.   
        //        using (StreamWriter objSWCreate = File.CreateText(filepath))
        //        {
        //            objSWCreate.WriteLine(strLogMsg);
        //            objSWCreate.Close();
        //        }
        //    }
        //    else
        //    {
        //        using (StreamWriter objSWAppend = File.AppendText(filepath))
        //        {
        //            objSWAppend.WriteLine(strLogMsg);
        //            objSWAppend.Close();
        //        }
        //    }   
        //}
        #endregion

        #region Function to Write Logs to Database
        public void WriteToDBLog(string strEventType, string strEventStatus, string strEventDesc, string strEventDetDesc, string strEventXml)
        {
            //using (DocProcQueuePickupService.APDFoundation.APDService objAPDFoundationLog = new DocProcQueuePickupService.APDFoundation.APDService())
            //{
            //objAPDFoundation = new DocProcQueuePickupService.APDFoundation.APDService();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            objAPDFoundation.LogEvent(strEventType, strEventStatus, strEventDesc, strEventDetDesc, strEventXml);
            //objAPDFoundation = null;

            ///}
        }
        #endregion
        //bool isProcessEnd = true;
        private object isLock = new object();
        public void startProcessing()
        {
            lock (isLock)
            {
                reloadqueue();
                XmlDocument objJobQueueDoc = new XmlDocument();

                XmlNode oJobNode;
                try
                {
                    //isProcessEnd &&
                    if ( File.Exists(strJobQueue))
                    {
                        //isProcessEnd = false;
                        using (FileStream fs = System.IO.File.Open(strJobQueue, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            objJobQueueDoc.Load(fs);
                        }


                        WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-startProcessing()", "Start Processing Jobs." + objJobQueueDoc.GetElementsByTagName("Job").Count + " Jobs found", objJobQueueDoc.OuterXml);
                        while (objJobQueueDoc.GetElementsByTagName("Job").Count > 0)
                        {
                            //process high priority job
                            oJobNode = objJobQueueDoc.SelectSingleNode("/Jobs/Job[@priority = 'high']");

                            if ((oJobNode) != null)
                            {
                                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-startProcessing()", "Job Count :" + objJobQueueDoc.GetElementsByTagName("Job").Count, oJobNode.OuterXml);
                                processJob(ref objJobQueueDoc, ref oJobNode);
                            }
                            else
                            {
                                //no high priority job exists. Now run the normal ones
                                oJobNode = objJobQueueDoc.SelectSingleNode("/Jobs/Job[@priority = 'normal']");
                                if ((oJobNode) != null)
                                {
                                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-startProcessing()", "Job Count :" + objJobQueueDoc.GetElementsByTagName("Job").Count, oJobNode.OuterXml);
                                    processJob(ref objJobQueueDoc, ref oJobNode);
                                }
                                else
                                {
                                    //no normal priority job exists.Now run the low priority ones
                                    oJobNode = objJobQueueDoc.SelectSingleNode("/Jobs/Job[@priority = 'low']");
                                    if ((oJobNode) != null)
                                        processJob(ref objJobQueueDoc, ref oJobNode);
                                }
                            }
                            oJobNode = null;


                        }
                        objJobQueueDoc = null;
                        //GC.Collect();
                        //GC.WaitForPendingFinalizers();
                        //reloadqueue();
                        //isProcessEnd = true;
                    }
                    else
                        WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-startProcessing()", "Job.xml not found at the path: " + strJobQueue, "");



                }
                catch (Exception Ex)
                {
                    WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-startProcessing()", "Source : " + Ex.Source + "Message : " + Ex.Message + ", Trace : " + Ex.StackTrace, "");
                    
                }
            }
        }
        public void processJob(ref XmlDocument objJobQueueDoc, ref XmlNode oJobNode)
        {
            string response = "";
            //string strresponseHTTPPost = "";
            //int strHttpstepID = 5;
            //XmlDocument objJobQueueDoc = new XmlDocument();
            WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", "Job started...", "");
            try
            { 
                string strJobFileName = oJobNode.Attributes["file"].Value;
                //XmlDocument docJobQueueXML = new XmlDocument();
                //docJobQueueXML.LoadXml(strJobQueue);
                XmlNode tempNode;
                string strJobId = oJobNode.Attributes["id"].Value;
                string JobFilePath = oJobNode.Attributes["file"].Value;
                string LynxID = Regex.Match(oJobNode.Attributes["description"].Value, String.Format(@"LYNX ID:\s*(.*?)\s*-"), RegexOptions.IgnoreCase).Groups[1].Value;

                //-- 26Mar2012 - TVD - Adding debugging for a problem with postoffice null insert.
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob(PARMS)", "Parameters before execute: ", string.Format("Params: strJobId: {0}, strJobFileName: {2}, JobFilePath: {3},  LynxID: {2}, Description: {3}, ", strJobId, JobFilePath, strJobFileName, LynxID, oJobNode.Attributes["description"].Value));

                if (File.Exists(strJobFileName))
                {
                    oJobNode.Attributes["status"].Value = "Processing";
                    using (FileStream fs = System.IO.File.Open(strJobQueue, FileMode.Open, FileAccess.ReadWrite, FileShare.Write))
                    {
                        objJobQueueDoc.Save(fs);
                    }
                    //docJobQueueXML.DocumentElement.SelectSingleNode("/Jobs/Job[@id='" + strJobId + "']").SetAttribute("status", "Processing");

                    XmlDocument docJobXML = new XmlDocument();
                    using (FileStream fs = System.IO.File.Open(JobFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        docJobXML.Load(fs);
                    }
                    string strsendVia = (docJobXML.SelectSingleNode("/Job/Package")).ChildNodes[0].Name;
                    string strpostofficeXML = XSLTtransfrom(docJobXML, strJobId, LynxID);
                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", "XSLT transformation successfully completed.Response XML: ", strpostofficeXML);

                    string response_BUND = SendtoPostOffice(strpostofficeXML, "BUND", LynxID, "1");
                    if (response_BUND.ToLower() != "success")
                    {
                        throw new Exception("postoffice service failed reason :" + response_BUND);
                    }
                    if (strsendVia != "Filecopy")
                    {
                        response = processDestination(strpostofficeXML, strsendVia, LynxID);
                    }

                    //-- 09Sep2021 - TVD - Removing the EML job in the default bundle send.
                    //else
                    //{
                    //    response_BUND = SendtoPostOffice(strpostofficeXML, "EML", LynxID, "4");
                    //}
                    
                    //for (int i = 0; i < (docJobXML.SelectSingleNode("/Job/Package")).ChildNodes.Count; i++)
                    //{
                    //    if ((docJobXML.SelectSingleNode("/Job/Package")).ChildNodes[i].Name == "HTTPPost")
                    //    {

                    //        strresponseHTTPPost = SendtoPostOffice(strpostofficeXML, "HTTP", LynxID, (strHttpstepID++).ToString()); 
                    //    }
                    //}
                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", "Deleting job file from " + strJobFileName, "");
                    //File.Move(JobFilePath, Path.Combine(strArchivepath, Path.GetFileName(JobFilePath)));
                    File.Copy(JobFilePath, Path.Combine(strArchivepath, Path.GetFileName(JobFilePath)), true);
                    if (File.Exists(strJobFileName))
                    {
                        File.Delete(JobFilePath);
                        WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", JobFilePath + " deleted successfully", "");
                    }
                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", "Removing the Node from jobs.XML with JobId = " + strJobId, objJobQueueDoc.OuterXml);

                    tempNode = objJobQueueDoc.SelectSingleNode("/Jobs/Job[@id='" + strJobId + "']");
                    tempNode.ParentNode.RemoveChild(tempNode);

                    //using (FileStream fs = System.IO.File.Open(strJobQueue, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                    //{
                    objJobQueueDoc.Save(strJobQueue);
                    //}
                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-processJob()", "Removed the Node from jobs.XML with JobId = " + strJobId, objJobQueueDoc.OuterXml);

                    //objJobQueueDoc = null;
                    docJobXML = null;
                    tempNode = null;


                }
                else
                {
                    WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-processJob()", "Job file not found " + JobFilePath, "");

                    tempNode = objJobQueueDoc.SelectSingleNode("/Jobs/Job[@id='" + strJobId + "']");
                    tempNode.ParentNode.RemoveChild(tempNode);
                    objJobQueueDoc.Save(strJobQueue);
                }
            }
            catch (Exception ex)
            {
                WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-processJob()", "Error_Description : " + ex.Message + " Exception trace Details : " + ex.StackTrace, "");

            }
        }

        public string processDestination(string strpostofficeXML, string strSendVia, string LynxId)
        {
            string strPostofficeResponse = "";
            switch (strSendVia)
            {
                case "Email":
                    strPostofficeResponse = SendtoPostOffice(strpostofficeXML, "EML", LynxId, "4");
                    break;
                case "Fax":
                    strPostofficeResponse = SendtoPostOffice(strpostofficeXML, "FAX", LynxId, "3");
                    break;
                case "FTP":
                    strPostofficeResponse = SendtoPostOffice(strpostofficeXML, "FTP", LynxId, "2");
                    break;
                //case "Filecopy":
                //    strPostofficeResponse = SendtoPostOffice(strpostofficeXML, "BUND", LynxId);
                //    break;
                default:
                    strPostofficeResponse = "No response";
                    break;
            }
            return strPostofficeResponse;
        }

        //public string SendtoPostOffice(string strpostofficeXML,string strMethod,string LynxID,string strStepID)
        //{
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; ;
        //    DocProcQueuePickupService.APDPostofficeService.APDService objPostOffice = new DocProcQueuePickupService.APDPostofficeService.APDService();
        //    return (objPostOffice.CreatePostOfficeJob(strStepID,strMethod, "0", LynxID,"", "", "", "", strpostofficeXML));
        //}
        public string SendtoPostOffice(string strpostofficeXML, string strMethod, string LynxID, string strStepID)
        {
            string strStoredProcedure = "uspPostOffice_AddJob";
            string strResponse = "";
            Double irowAffected;
            string strParameters = "";
            string strJobId = System.Guid.NewGuid().ToString();
            try
            {
                //objAPDFoundation = new DocProcQueuePickupService.APDFoundation.APDService();
                //DocProcQueuePickupService.APDFoundation.APDService objAPDFoundation = new DocProcQueuePickupService.APDFoundation.APDService();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                strParameters = "@JobID='" + strJobId + "',@StepID=" + strStepID + ",@Method=" + strMethod + ",@ClientID=NULL,@LynxID=" + LynxID + ",@ClaimNumber=NULL,@JobPriority=5,@ResponseRequired=false,@ResponseThresholdMins=0,@JobDetails='" + strpostofficeXML + "'";

                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-SendtoPostOffice()", "stored procedure : " + strStoredProcedure + ", Parameters : " + strParameters, strpostofficeXML);

                strResponse = objAPDFoundation.ExecuteSp(strStoredProcedure, strParameters);
                if (Double.TryParse(strResponse, out irowAffected))
                {
                    strResponse = "success";
                    WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-SendtoPostOffice()", "Data successfully posted to postoffice_jobs table", "");

                }
                //objAPDFoundation = null;
            }
            catch (Exception ex)
            {
                WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-SendtoPostOffice()", "ErrorDescription: " + ex.Message + ", Trace Details : " + ex.StackTrace, "");

            }

            return strResponse;
        }

        #region Function XSLTTransform
        //***************************************************************************************
        // Procedure : XSLTtransfrom
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : Use this procedure transform the jobXML to PostjobofficeXML
        //***************************************************************************************
        private string XSLTtransfrom(XmlDocument jobXml, string strJobId, string strLynxID)
        {
            string postofficeXml = null;
            string strXSLTPath = AppDomain.CurrentDomain.BaseDirectory + DocProcQueuePickupService.Properties.Settings.Default.XSLTPath + @"\XSLT_convert_doc_proc-postOffice.xslt";
            //string strOutputFile = Directory.GetCurrentDirectory() + @"\OutputXML\postofficeJob" + strJobId + ".xml";

            //string strXSLTPath = DocProcQueuePickupService.Properties.Settings.Default.XSLTPath + @"\XSLT_convert_doc_proc-postOffice.xslt";
            //string strOutputFile = Properties.Settings.Default.Outputfilepath + @"\postofficeJob" + strJobId + ".xml";
            try
            {
                if (File.Exists(strXSLTPath))
                {
                    XslCompiledTransform xslt = new XslCompiledTransform(false);
                    using (StreamReader srXslt = new StreamReader(strXSLTPath))
                    {
                        XmlReader readerXslt = XmlReader.Create(srXslt);
                        xslt.Load(readerXslt);
                    }

                    #region code for sending parameters to xslt file(Arguments)
                    // Create an XsltArgumentList.
                    XsltArgumentList xslArg = new XsltArgumentList();
                    xslArg.AddParam("LynxID", "", strLynxID);

                    #endregion

                    #region Transform XML to the given file path
                    // Create and open the output file
                    //using (FileStream fsOutput = File.Create(strOutputFile))
                    //{
                    //    XmlWriterSettings xmlSettings = new XmlWriterSettings();
                    //    xmlSettings.Indent = true;
                    //    XmlWriter writerXML = XmlTextWriter.Create(fsOutput, xmlSettings);

                    //    // Open the input file
                    //    //using (XmlReader readerInput = XmlReader.Create(jobXml))
                    //    //{
                    //    xslt.Transform(jobXml, null, writerXML);
                    //    //}
                    //}
                    #endregion

                    #region Transfrom XML to string Variable and return
                    using (TextWriter writer = new Utf8StringWriter())
                    {

                        using (XmlWriter objXmlWriter = XmlWriter.Create(writer))
                        {
                            // Transform the file.
                            xslt.Transform(jobXml, xslArg, objXmlWriter);
                            objXmlWriter.Close();
                        }
                        postofficeXml = writer.ToString();
                    }
                    #endregion
                }
                else
                {
                    //WriteToLogFile("Error : Function XSLTTransform --xslt file not found at the path" + strXSLTPath);
                    WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-XSLTtransfrom()", "Function XSLTTransform --xslt file not found at the path" + strXSLTPath, "");

                    throw new Exception("XSLT file not found at the location " + strXSLTPath);
                }
            }
            catch (Exception ex)
            {
                //WriteToLogFile("Error:FunctionXSLT transform source:" + ex.Source);
                WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-XSLTtransfrom()", "source:" + ex.Source, "");

            }

            return postofficeXml;
        }
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }
        #endregion




        #region reloadqueue
        //***************************************************************************************
        // Procedure : reload queue
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : Use this procedure to reload queue with priority
        //***************************************************************************************
        public void reloadqueue()
        {
            try
            {
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-reloadqueue()", "Reloading jobs Queue............", "");

                int iHighPriorityCount = getJobsQueue("high");
                int iNormalPriorityCount = getJobsQueue("normal");
                int iLowPriorityCount = getJobsQueue("low");
                //WriteToLogFile("Done ...[" + iHighPriorityCount + " High; " + iNormalPriorityCount + " Normal; " + iLowPriorityCount + " Low;]");
                WriteToDBLog("DocProcQueuePickup", "DEBUGGING", "DocProcQueuePickup.cs-reloadqueue()", "Done ...[" + iHighPriorityCount + " High; " + iNormalPriorityCount + " Normal; " + iLowPriorityCount + " Low;]", "");
            }
            catch (Exception Ex)
            {
                WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-reloadqueue()", "ErrorDescription: " + Ex.Message + ", Trace Details : " + Ex.StackTrace, "");
            }

        }
        #endregion

        #region getJobsQueue
        //***************************************************************************************
        // Procedure : getJobsQueue
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : It returns the Count of job in queue and adds new job that present in the queue folder
        //***************************************************************************************

        public int getJobsQueue(string strPriority)
        {
            XmlDocument objJobsQueueXML = new XmlDocument();
            string[] strFileNames;
            //string strFilename;
            string strFilePattern = "";
            string strPriorityTxt = "";
            string strPriorityPrefix = "";
            string strJobID = "";
            XmlElement objJobQueueNode;
            XmlNode objJobNode;
            XmlDocument objJobXML = new XmlDocument();
            string strJobDesc = "";
            DateTime dteFileModified;
            int iFileCount;
            int i = 0;

            int iRetNodeCount = 0;

            //reading the Jobs.xml file from queue if file not exist it initialize as a empty job

            //objNolq.WriteToLogFile("Start : (JobsModule.getJobsQueue())");

            try
            {
                if (File.Exists(strJobQueue))
                    if (new FileInfo(strJobQueue).Length == 0)
                    {
                        // file is empty
                        objJobsQueueXML.LoadXml("<Jobs/>");
                    }
                    else
                    {
                        using (FileStream fs = System.IO.File.Open(strJobQueue, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            objJobsQueueXML.Load(fs);
                        }
                    }

                else
                    objJobsQueueXML.LoadXml("<Jobs/>");

                switch (strPriority)
                {
                    case "high":
                        strPriorityPrefix = HIGH_PRIORITY;
                        break;
                    case "normal":
                        strPriorityPrefix = NORMAL_PRIORITY;
                        break;
                    case "low":
                        strPriorityPrefix = LOW_PRIORITY;
                        break;
                }

                strFilePattern = strPriorityPrefix + "*.*";
                strFileNames = Directory.GetFiles(strQueuePath, strFilePattern);
                iFileCount = strFileNames.Length;

                while (iFileCount != 0)
                {
                    strJobID = (Path.GetFileNameWithoutExtension(strFileNames[i])).Substring(2);
                    dteFileModified = File.GetLastWriteTime(strFileNames[i]);
                    //let us wait for the job file write to complete and we will give about 10 seconds
                    if ((DateTime.Now - dteFileModified).TotalSeconds >= 10)
                    {
                        objJobNode = objJobsQueueXML.SelectSingleNode("/Jobs/Job[@id='" + strJobID + "']");
                        if ((objJobNode) == null)
                        {
                            //job not in queue. add to the queue now
                            objJobXML.Load(strFileNames[i]);
                            if (objJobXML.OuterXml != null)
                            {
                                //reuse the objJobNode object
                                objJobNode = objJobXML.SelectSingleNode("/Job");
                                if (objJobNode.OuterXml != null)
                                    strJobDesc = objJobNode.Attributes["description"] == null ? "" : objJobNode.Attributes["description"].Value;
                                //add job to the queue
                                objJobQueueNode = objJobsQueueXML.CreateElement("Job");
                                objJobQueueNode.SetAttribute("id", strJobID);
                                objJobQueueNode.SetAttribute("description", strJobDesc);
                                objJobQueueNode.SetAttribute("priority", strPriority.ToLower());
                                objJobQueueNode.SetAttribute("status", "Waiting");
                                objJobQueueNode.SetAttribute("file", strFileNames[i]);

                                objJobsQueueXML.DocumentElement.AppendChild(objJobQueueNode);

                            }
                        }
                        else
                        {
                            //job already defined in the queue. check if its priority changed
                            strPriorityTxt = objJobNode.Attributes["status"] == null ? "" : objJobNode.Attributes["status"].Value;
                            if (strPriorityTxt != "Processing" && strPriorityTxt != strPriority)
                            {
                                objJobNode.Attributes["priority"].Value = strPriority.ToLower();
                                objJobNode.Attributes["file"].Value = strFileNames[i];
                            }
                        }

                    }
                    i++; iFileCount--;
                    //objJobXML = null;
                }

                using (FileStream fs = System.IO.File.Open(strJobQueue, FileMode.Open, FileAccess.ReadWrite, FileShare.Write))
                {
                    objJobsQueueXML.Save(fs);
                }
                iRetNodeCount = (objJobsQueueXML.SelectNodes("Jobs/Job[@priority='" + strPriority.ToLower() + "']")).Count;
                objJobsQueueXML = null;
                objJobQueueNode = null;
                objJobNode = null;
                objJobXML = null;
                //((IDisposable)objJobsQueueXML).Dispose();
            }
            catch (Exception Ex)
            {
                //WriteToLogFile("Error (getJobsQueue()),Source : " + Ex.Source + "Message : " + Ex.Message + ", Trace : " + Ex.StackTrace);
                WriteToDBLog("DocProcQueuePickup", "ERROR", "DocProcQueuePickup.cs-getJobsQueue()", "Source : " + Ex.Source + "Message : " + Ex.Message + ", Trace : " + Ex.StackTrace, "");
            
            }
            //objNolq.WriteToLogFile("Stop : (JobsModule.getJobsQueue())");
            return iRetNodeCount;
        }
        #endregion

    }
}
