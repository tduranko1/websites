﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <!--<xsl:output cdata-section-elements="File"/>-->
  <xsl:output method="xml" indent="yes"/>
  <xsl:param name="LynxID" select='""'/>


  <xsl:template match="/">
    <JobDetail clientid="0" clientname="" lynxid="{$LynxID}" claimnumber="" submittingserver="hyapdschdst1">
      <xsl:apply-templates select="Job"/>
    </JobDetail>
  </xsl:template>

  <xsl:template match="Job">
    <FileList>
      <xsl:apply-templates select="Document"/>
    </FileList>
    <!--Choose Nodes Email Fax Etc.. from inputXML & applying its template-->
    <xsl:choose>
      <xsl:when test="Package/Email">
        <xsl:apply-templates select="Package/Email"/>
      </xsl:when>
      <xsl:when test="Package/Fax">
        <xsl:apply-templates select="Package/Fax"/>
      </xsl:when>
      <xsl:when test="Package/FTP">
        <xsl:apply-templates select="Package/FTP"/>
      </xsl:when>
      <xsl:when test="Package/Filecopy">
        <xsl:apply-templates select="Package/Filecopy"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="Document">
    <File primary="1">
      <xsl:attribute name="source">
        <xsl:value-of select="@filepath" />
      </xsl:attribute>
      <xsl:attribute name="filename">
        <xsl:value-of select="@filename" />
      </xsl:attribute>
      <xsl:attribute name="filetype">
        <xsl:value-of select="@filetype" />
      </xsl:attribute>
      <xsl:if test="@filetype='xml'">
        <xsl:attribute name="xslpath">
          <xsl:value-of select="@xslpath" />
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="merge">
        <xsl:value-of select="@merge" />
      </xsl:attribute>
      <xsl:attribute name="noconvert">
        <xsl:value-of select="@noconvert" />
      </xsl:attribute>
      <xsl:attribute name="outputfilename">
        <xsl:value-of select="@outputfilename" />
      </xsl:attribute>
      <!--<xsl:value-of select="." />-->
      <xsl:if test=". != ''">
        <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
        <xsl:value-of select="." disable-output-escaping="yes" />
        <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </xsl:if>
    </File>
    <!-- </xsl:if> -->
  </xsl:template>
  <!--Create EmailNode-->
  <xsl:template match="Package/Email">
    <xsl:variable name="i" select="position()" />
    <EMAIL ignoreenvoverride="yes" priority="5" stepid="{$i}">
      <xsl:apply-templates select="@* | node()"/>
    </EMAIL>
  </xsl:template>
  <!--Email - From -->
  <xsl:template match="From">
    <From>
      <xsl:value-of select="../From"/>
    </From>
  </xsl:template>
  <!--Email - To -->
  <xsl:template match="To">
    <Recipient type="to">
      <xsl:value-of select="../To" />
    </Recipient>
  </xsl:template>

  <!--Ignoring Email - Reply to -->
  <xsl:template match="Column[@SourceColumn='ReplyTo']|ReplyTo" />

  <!--Email - Subject -->
  <xsl:template match="Subject">
    <Subject>
      <xsl:value-of select="../Subject"/>
    </Subject>
  </xsl:template>

  <!--Email - Body -->
  <xsl:template match="Body">
    <Body>
      <xsl:value-of select="../Body"/>
    </Body>
  </xsl:template>

  <!--Create FAX element -->
  <xsl:template match="Package/Fax">
    <xsl:variable name="i" select="position()" />
    <FAX responseexpected="" ignoreenvoverride="5" stepid="{$i}">
      <xsl:apply-templates select="@* | node()"/>
    </FAX>
  </xsl:template>
  <xsl:template match="@to">
    <Number>
      <xsl:value-of select="../@to"/>
    </Number>
  </xsl:template>
  <!--Ignoring FAX - Name attribute -->
  <xsl:template match="Column[@SourceColumn='@name']|@name" />

  <!--Create FTP Element -->
  <xsl:template match="Package/FTP">
    <xsl:variable name="i" select="position()" />
    <FTP ignoreenvoverride="1" protocol="ftp" stepid="{$i}">
      <xsl:attribute name="server">
        <xsl:value-of select="remoteserver" />
      </xsl:attribute>
      <xsl:attribute name="user">
        <xsl:value-of select="remoteUID" />
      </xsl:attribute>
      <xsl:attribute name="password">
        <xsl:value-of select="remotePWD" />
      </xsl:attribute>
      <xsl:attribute name="targetpath">
        <xsl:value-of select="path" />
      </xsl:attribute>
    </FTP>
  </xsl:template>

  <xsl:template match="Package/Filecopy">
    <Bundling ignoreenvoverride="1" priority="5" stepid="1">
      <xsl:comment>Package Types - PDF - 1 PDF with all files in it</xsl:comment>
      <xsl:comment>Package Types - ZIP - 1 ZIP with all files in it</xsl:comment>
      <xsl:comment>Package Types - "" and outputfile - "" means no actual package.  Process files directly</xsl:comment>
      <Bundle merge="no">
        <xsl:attribute name="type">
          <xsl:value-of select="../@type" />
        </xsl:attribute>
        <xsl:attribute name="outputfile">
          <xsl:value-of select="../@outputfile" />
        </xsl:attribute>
        <!--<xsl:choose>
          <xsl:when test="../@type  [.!= 'pdf' and .!='zip']">
            <xsl:attribute name="merge">no</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="merge">yes</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>-->
      </Bundle>
      <ElectBundling merge="no">
        <!--<xsl:choose>
          <xsl:when test="../@type  [.!= 'pdf' and .!='zip']">
            <xsl:attribute name="merge">no</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="merge">yes</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>-->
        <xsl:comment>(Target Location to Save the Document Too)</xsl:comment>
        <UNCTarget>
          <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
          <xsl:value-of select="./UNC" disable-output-escaping="yes" />
          <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
        </UNCTarget>
      </ElectBundling>
    </Bundling>
    <EMAIL ignoreenvoverride="yes" priority="5" stepid="4">
      <Subject>
        <xsl:value-of select="../@outputfile" />
      </Subject>
      <Recipient type="to">tduranko@lynxservices.com;mkumar@lynxservices.com</Recipient>
    </EMAIL>
    <Archiving ignoreenvoverride="yes" priority="5" stepid="5">
      <ArchPath />
    </Archiving>
  </xsl:template>
</xsl:stylesheet>
