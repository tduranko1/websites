﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MSXML2;
using System.Configuration;
using System.IO;
using System.Xml;

namespace NOLQueuePickupService
{
    class JobsModule
    {
        string HIGH_PRIORITY = "1_";
        string NORMAL_PRIORITY = "2_";
        string LOW_PRIORITY = "3_";
        string strQueuePath = DocProcQueuePickupService.Properties.Settings.Default.strQueuePath;//(ConfigurationManager.AppSettings["strQueuePath"]);
        string strPickupPath = DocProcQueuePickupService.Properties.Settings.Default.strPickupPath;//(ConfigurationManager.AppSettings["strPickupPath"]);
        string strJobQueue = DocProcQueuePickupService.Properties.Settings.Default.strJobQueue;//(ConfigurationManager.AppSettings["strJobQueue"]);
        DocProcQueuePickup objNolq = new DocProcQueuePickup();
        XmlDocument objJobsQueueXML = new XmlDocument();
        //Constructor
        //public JobsModule()
        //{
           
        //}



        public string getJobTitle(string strJobID)
        {
            string strJobtitle = "";
            XmlDocument oJobsXML = new XmlDocument();
            XmlNode oJob;//= new XmlNode();

            //objNolq.WriteToLogFile("begin function getJobTitle with parameter JobID : " + strJobID);
            if (JobInQueue(strJobID))

                strJobtitle = strJobID;

            if(File.Exists(strJobQueue))
            {
                oJobsXML.Load(strJobQueue);
                if (oJobsXML.OuterXml != null)
                {
                    oJob = oJobsXML.SelectSingleNode("/Jobs/Job[@id='" + strJobID + "']");
                    if (oJob.OuterXml != null)
                        strJobtitle = oJob.Attributes["description"].Value;
                }
            }

            return strJobtitle;
        }

        public string getJobStatus(string strJobID)
        {
            string strJobStatus = "";
            XmlDocument oJobsXML = new XmlDocument();
            XmlNode oJob;//= new XmlNode();

            //objNolq.WriteToLogFile("begin function JobsModule.getJobStatus with parameter JobID : " + strJobID);
            if (JobInQueue(strJobID))

                strJobStatus = strJobID;

            if (File.Exists(strJobQueue))
            {
                oJobsXML.Load(strJobQueue);
                if (oJobsXML.OuterXml != null)
                {
                    oJob = oJobsXML.SelectSingleNode("/Jobs/Job[@id='" + strJobID + "']");
                    if (oJob.OuterXml != null)
                        strJobStatus = oJob.Attributes["status"].Value;
                }
            }

            return strJobStatus;
        }

        #region reloadqueue
        //***************************************************************************************
        // Procedure : reload queue
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : Use this procedure to reload queue with priority
        //***************************************************************************************
        public void reloadqueue()
        {
            //objNolq.WriteToLogFile("Reloading jobs Queue............");
            int iHighPriorityCount = getJobsQueue("High");
            int iNormalPriorityCount = getJobsQueue("Normal");
            int iLowPriorityCount = getJobsQueue("Low");
            //objNolq.WriteToLogFile("Done ...[" + iHighPriorityCount + " High; "+iNormalPriorityCount+" Normal; "+iLowPriorityCount+" Low;]");
        }
        #endregion

        #region getJobsQueue
        //***************************************************************************************
        // Procedure : getJobsQueue
        // DateTime  : 14/4/2020 11:51
        // Author    : 
        // Purpose   : It returns the Count of job in queue and adds new job that present in the queue folder
        //***************************************************************************************

        public int getJobsQueue(string strPriority)
        {
            string[] strFileNames;
            //string strFilename;
            string strFilePattern = "";
            string strPriorityTxt = "";
            string strPriorityPrefix = "";
            string strJobID = "";
            XmlElement objJobQueueNode;
            XmlNode objJobNode;
            XmlDocument objJobXML = new XmlDocument();
            string strJobDesc = "";
            DateTime dteFileModified;
            int iFileCount;
            int i=0;

            //int iRet = 0;

            //reading the Jobs.xml file from queue if file not exist it initialize as a empty job

            //objNolq.WriteToLogFile("Start : (JobsModule.getJobsQueue())");

            try
            {
                if (File.Exists(strJobQueue))
                    if (new FileInfo(strJobQueue).Length == 0)
                    {
                        // file is empty
                        objJobsQueueXML.LoadXml("<Jobs/>");
                    }
                    else
                    {
                        objJobsQueueXML.Load(strJobQueue);
                    }
            
                else
                    objJobsQueueXML.LoadXml("<Jobs/>");

                switch (strPriority)
                {
                    case "High":
                        strPriorityPrefix = HIGH_PRIORITY;
                        break;
                    case "Normal":
                        strPriorityPrefix = NORMAL_PRIORITY;
                        break;
                    case "Low":
                        strPriorityPrefix = LOW_PRIORITY;
                        break;
                }

                strFilePattern = strPriorityPrefix + "*.*";
                strFileNames = Directory.GetFiles(strQueuePath, strFilePattern);
                iFileCount = strFileNames.Length;
            
                while (iFileCount != 0)
                {
                    strJobID = (Path.GetFileNameWithoutExtension(strFileNames[i])).Substring(2);
                    dteFileModified = File.GetLastWriteTime(strFileNames[i]);
                    //let us wait for the job file write to complete and we will give about 10 seconds
                    if ((DateTime.Now - dteFileModified).TotalSeconds >= 10)
                    {
                        objJobNode = objJobsQueueXML.SelectSingleNode("/Jobs/Job[@id='" + strJobID + "']");
                        if ((objJobNode) == null)
                        {
                            //job not in queue. add to the queue now
                            objJobXML.Load(strFileNames[i]);
                            if (objJobXML.OuterXml != null)
                            {
                                //reuse the objJobNode object
                                objJobNode = objJobXML.SelectSingleNode("/Job");
                                if (objJobNode.OuterXml != null)
                                    strJobDesc = objJobNode.Attributes["description"] == null ? "" : objJobNode.Attributes["description"].Value;
                                //add job to the queue
                                objJobQueueNode = objJobsQueueXML.CreateElement("Job");
                                objJobQueueNode.SetAttribute("id", strJobID);
                                objJobQueueNode.SetAttribute("description", strJobDesc);
                                objJobQueueNode.SetAttribute("priority", strPriority.ToLower());
                                objJobQueueNode.SetAttribute("status", "Waiting");
                                objJobQueueNode.SetAttribute("file", strFileNames[i]);

                                objJobsQueueXML.DocumentElement.AppendChild(objJobQueueNode);

                            }
                        }
                        else
                        {
                            //job already defined in the queue. check if its priority changed
                            strPriorityTxt = objJobNode.Attributes["status"] != null ? "" : objJobNode.Attributes["status"].Value;
                            if (strPriorityTxt != "Processing" && strPriorityTxt != strPriority)
                            {
                                objJobNode.Attributes["priority"].Value = strPriority;
                                objJobNode.Attributes["file"].Value = strFileNames[i];
                            }
                        }

                    }
                    i++; iFileCount--;
                }

                objJobsQueueXML.Save(strJobQueue);
            }
            catch(Exception Ex)
            {
                //objNolq.WriteToLogFile("Error (JobsModule.getJobsQueue()),Source : " + Ex.Source + "Message : " + Ex.Message + ", Trace : "+ Ex.StackTrace);
            }
            //objNolq.WriteToLogFile("Stop : (JobsModule.getJobsQueue())");
            return (objJobsQueueXML.SelectNodes("Jobs/Job[@priority='" + strPriority.ToLower() + "']")).Count;
        }
        #endregion

        #region JobInqueue
        //***************************************************************************************
        // Procedure : JobInqueue
        // DateTime  : 17/4/2020 11:51
        // Author    : 
        // Purpose   : It returns the true or false when the file exist with the Joibid
        //***************************************************************************************
        public bool JobInQueue(string strJobID)
        {
            string strJobHighFileName;
            string strJobNormalFileName; 
            string strJobLowFileName;


            strJobHighFileName = strQueuePath + HIGH_PRIORITY + strJobID + ".xml";
            strJobNormalFileName = strQueuePath + NORMAL_PRIORITY + strJobID + ".xml";
            strJobLowFileName = strQueuePath + LOW_PRIORITY + strJobID + ".xml";
            if (File.Exists(strJobHighFileName) || File.Exists(strJobNormalFileName) || File.Exists(strJobLowFileName))
                return true;
            return false;
            
        }
        #endregion
    }
}
