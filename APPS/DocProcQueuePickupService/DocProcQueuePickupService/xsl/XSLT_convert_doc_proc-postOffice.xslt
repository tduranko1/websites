﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">

  <!--<xsl:output cdata-section-elements="File"/>-->
  <xsl:output method="xml" indent="yes"/>
  <xsl:param name="LynxID" select='NULL'/>


  <xsl:template match="/">
    <JobDetails clientid="0" clientname="" lynxid="{$LynxID}" claimnumber="" submittingserver="hyapdschdst1">
      <xsl:apply-templates select="Job"/>
    </JobDetails>
  </xsl:template>

  <xsl:template match="Job">
    <FileList>
      <xsl:apply-templates select="Document"/>
    </FileList>
    <Bundling ignoreenvoverride="yes" priority="5" stepid="1">
      <xsl:apply-templates select="Package"/>
    </Bundling>
    <!--Choose Nodes Email Fax Etc.. from inputXML & applying its template-->
    <xsl:choose>
      <xsl:when test="Package/Email">
        <xsl:apply-templates select="Package/Email"/>
      </xsl:when>
      <xsl:when test="Package/Fax">
        <xsl:apply-templates select="Package/Fax"/>
      </xsl:when>
      <xsl:when test="Package/FTP">
        <xsl:apply-templates select="Package/FTP"/>
      </xsl:when>
      <xsl:when test="Package/Filecopy">
        <xsl:apply-templates select="Package/Filecopy"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="Document">
    <xsl:variable name="outputfilename" select ="../Package/@outputfile" />
    <xsl:variable name="char_position">
      <xsl:call-template name="last-index-of">
        <xsl:with-param name="txt" select="../Package/@outputfile"/>
        <xsl:with-param name="delimiter" select="'-'"></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <File primary="1">
      <xsl:attribute name="source">
        <xsl:value-of select="concat(@filepath,'\',@filename)" />
      </xsl:attribute>
      <xsl:attribute name="filename">
        <xsl:value-of select="@filename" />
      </xsl:attribute>
      <!--<xsl:choose>
        <xsl:when test="contains(@outputfilename,'Invoice') and contains(@outputfilename,'.doc')">
          <xsl:attribute name="filetype">
            <xsl:value-of select="'pdf'" />
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="filetype">
            <xsl:value-of select="@filetype" />
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>-->
      <xsl:attribute name="filetype">
        <xsl:value-of select="@filetype" />
      </xsl:attribute>
      <xsl:if test="@filetype='xml' and @xslpath !=''">
        <xsl:attribute name="xslpath">
          <xsl:value-of select="@xslpath" />
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="merge">
        <xsl:value-of select="@merge" />
      </xsl:attribute>
      <xsl:attribute name="noconvert">
        <xsl:value-of select="@noconvert" />
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@outputfilename='Claim Message.txt'">
          <xsl:choose>
            <xsl:when test="contains(../Package/@outputfile, 'Claim Documents') and contains(../Package/@outputfile, '-') and not(contains(../Package/@outputfile, '--'))">
              <xsl:attribute name="outputfilename">
                <!--<xsl:value-of select="concat(concat(substring(../Package/@outputfile,1,string-length(../Package/@outputfile)-2),'-',substring(../Package/@outputfile,string-length(../Package/@outputfile)-1)),'.','pdf')" />-->
                <xsl:value-of select="concat(concat(substring(../Package/@outputfile,1,($char_position)-1),'-',substring(../Package/@outputfile,($char_position))),'.','htm')" />
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="outputfilename">
                <xsl:value-of select="concat(../Package/@outputfile,'.','htm')" />
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <!--if the file list contains .doc file extention for invoice document extention changed to .pdf-->
        <!--<xsl:when test="contains(@outputfilename,'Invoice') and contains(@outputfilename,'.doc')">
          <xsl:attribute name="outputfilename">
            <xsl:value-of select="concat(substring(@outputfilename,1,string-length(@outputfilename)-3),'pdf')" />
          </xsl:attribute>
        </xsl:when>-->
        <!--changing the extention of job cover sheet pdf to htm-->
        <xsl:when test="contains(@outputfilename,'_coversheet')">
          <xsl:attribute name="outputfilename">
            <xsl:value-of select="concat(substring(@outputfilename,1,string-length(@outputfilename)-3),'htm')" />
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="outputfilename">
            <xsl:value-of select="@outputfilename" />
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <!--<xsl:value-of select="." />-->
      <xsl:if test=". != ''">
        <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
        <xsl:value-of select="." disable-output-escaping="yes" />
        <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </xsl:if>
    </File>
    <!-- </xsl:if> -->
  </xsl:template>
  <!--Adding the Bundling node as default node for every Methods-->
  <xsl:template match="Package">
    <xsl:comment>Package Types - PDF - 1 PDF with all files in it</xsl:comment>
    <xsl:comment>Package Types - ZIP - 1 ZIP with all files in it</xsl:comment>
    <xsl:comment>Package Types - "" and outputfile - "" means no actual package.  Process files directly</xsl:comment>
    <xsl:variable name="char_position">
      <xsl:call-template name="last-index-of">
        <xsl:with-param name="txt" select="../Package/@outputfile"/>
        <xsl:with-param name="delimiter" select="'-'"></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <Bundle>
      <xsl:variable name="lowerCase">abcdefghijklmnopqrstuvwxyz</xsl:variable>
      <xsl:variable name="upperCase">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
      <xsl:attribute name="type">
        <xsl:value-of select="translate(@type,$lowerCase,$upperCase)" />
        <!--<xsl:value-of select="''" />-->
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="contains(@outputfile, '--')">
          <xsl:attribute name="outputfile">
            <xsl:value-of select="concat(substring(../Package/@outputfile,1,($char_position)-1),substring(../Package/@outputfile,($char_position)+1))" />
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="contains(@outputfile, 'Claim Documents')">
              <xsl:attribute name="outputfile">
                <xsl:value-of select="@outputfile" />
                <!--<xsl:value-of select="concat(substring(../Package/@outputfile,1,($char_position)-1),'-',substring(../Package/@outputfile,($char_position)))" />-->
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="outputfile">
                <!--<xsl:value-of select="@outputfile" />-->
                <xsl:value-of select="concat(substring(../Package/@outputfile,1,($char_position)-1),'-',substring(../Package/@outputfile,($char_position)))" />
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="merge">
        <xsl:value-of select="'Yes'" />
      </xsl:attribute>
    </Bundle>
    <xsl:choose>
      <xsl:when test="Filecopy">
        <ElectBundling>
          <!--<xsl:choose>
          <xsl:when test="../@type  [.!= 'pdf' and .!='zip']">
            <xsl:attribute name="merge">no</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="merge">yes</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>-->
          <xsl:comment>(Target Location to Save the Document Too)</xsl:comment>
          <UNCTarget>
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
            <xsl:value-of select="Filecopy/UNC" disable-output-escaping="yes" />
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
          </UNCTarget>
        </ElectBundling>
      </xsl:when>
    </xsl:choose>
    <!--Adding HTTPPost -->
    <xsl:choose>
      <xsl:when test="HTTPPost/@DocumentNodeName='Document'">
        <NoteMsg ignoreenvoverride="yes" priority="5">
          <xsl:apply-templates select="HTTPPost[@DocumentNodeName='Document']"/>
        </NoteMsg>
      </xsl:when>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="HTTPPost/@DocumentNodeName='File'">
        <DocUpload ignoreenvoverride="yes" priority="5">
          <xsl:apply-templates select="HTTPPost[@DocumentNodeName='File']"/>
        </DocUpload>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <!--Create EmailNode-->


  <xsl:template match="Package/Email">
    <xsl:variable name="i" select="position()" />
    <!--<EMAIL ignoreenvoverride="yes" priority="5" stepid="{$i}">-->
    <EMAIL ignoreenvoverride="yes" priority="5" stepid="4">
      <xsl:apply-templates select="@* | node()"/>
    </EMAIL>
  </xsl:template>
  <!--Email - From -->
  <xsl:template match="From">
    <From>
      <xsl:value-of select="../From"/>
    </From>
    <Sender>
      <xsl:value-of select="../From"/>
    </Sender>
  </xsl:template>

  <!--Email - To -->
  <xsl:template match="To">
    <Recipient type="to">
      <xsl:value-of select="../To" />
    </Recipient>
  </xsl:template>

  <!--Ignoring Email - Reply to -->
  <xsl:template match="Column[@SourceColumn='ReplyTo']|ReplyTo" />

  <!--Email - Subject -->
  <xsl:template match="Subject">
    <Subject>
      <xsl:value-of select="../Subject"/>
    </Subject>
  </xsl:template>

  <!--Email - Body -->
  <xsl:template match="Body">
    <Body>
      <xsl:value-of select="../Body"/>
    </Body>
  </xsl:template>

  <!--Create FAX element -->
  <xsl:template match="Package/Fax">
    <xsl:variable name="i" select="position()" />
    <!--<FAX responseexpected="" ignoreenvoverride="5" stepid="{$i}">-->
    <FAX responseexpected="" ignoreenvoverride="yes" stepid="3">
      <xsl:apply-templates select="@* | node()"/>
    </FAX>
  </xsl:template>
  <xsl:template match="@to">
    <Number>
      <xsl:value-of select="../@to"/>
    </Number>
  </xsl:template>
  <!--Ignoring FAX - Name attribute -->
  <xsl:template match="Column[@SourceColumn='@name']|@name" />

  <!--Create FTP Element -->
  <xsl:template match="Package/FTP">
    <xsl:variable name="i" select="position()" />
    <!--<FTP ignoreenvoverride="1" protocol="ftp" stepid="{$i}">-->
    <FTP ignoreenvoverride="yes" protocol="ftp" stepid="2">
      <xsl:attribute name="server">
        <xsl:value-of select="remoteserver" />
      </xsl:attribute>
      <xsl:attribute name="user">
        <xsl:value-of select="remoteUID" />
      </xsl:attribute>
      <xsl:attribute name="password">
        <xsl:value-of select="remotePWD" />
      </xsl:attribute>
      <xsl:attribute name="targetpath">
        <xsl:value-of select="path" />
      </xsl:attribute>
    </FTP>
  </xsl:template>


  <!--Applying HTTPPOST Template-->
  <xsl:template match="HTTPPost">
    <xsl:apply-templates select="node()"/>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="PostTemplate/root/File">
    <File xmlns:dt="urn:schemas-microsoft-com:datatypes" dt:dt="bin.base64">
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="FileExt">
        <xsl:value-of select="''" />
      </xsl:attribute>
    </File>
  </xsl:template>

  <!--Template for  finding position of a character from string-->
  <xsl:template name="last-index-of">
    <xsl:param name="txt"/>
    <xsl:param name="remainder" select="$txt"/>
    <xsl:param name="delimiter" select="' '"/>

    <xsl:choose>
      <xsl:when test="contains($remainder, $delimiter)">
        <xsl:call-template name="last-index-of">
          <xsl:with-param name="txt" select="$txt"/>
          <xsl:with-param name="remainder" select="substring-after($remainder, $delimiter)"/>
          <xsl:with-param name="delimiter" select="$delimiter"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="lastIndex" select="string-length(substring($txt, 1, string-length($txt)-string-length($remainder)))+1"/>
        <xsl:choose>
          <xsl:when test="string-length($remainder)=0">
            <xsl:value-of select="string-length($txt)"/>
          </xsl:when>
          <xsl:when test="$lastIndex>0">
            <xsl:value-of select="($lastIndex - string-length($delimiter))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
