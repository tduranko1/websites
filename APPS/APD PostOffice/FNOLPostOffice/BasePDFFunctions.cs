﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using PGW.Shared.SiteUtilities;

using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.XlsIO;
using Syncfusion.ExcelToPdfConverter;
using Syncfusion.HtmlConverter;
using Syncfusion.XPS;
using Syncfusion.Pdf.Parsing;
using System.IO;

namespace FNOLPostOffice
{
    class BasePDFFunctions : BaseJob
    {
        int iEventSeq = 1;
        string sEventTransactionID = string.Empty;
        string sProcessingServer = string.Empty;
        string sAPPPath = string.Empty;
        string sNewPathFile = string.Empty;

        AppSettingsReader appSettingsReader = new AppSettingsReader();

        //---------------------------//
        // New Init                  //
        //---------------------------//
        public BasePDFFunctions(ref Events events, string jobID, int jobStep)
        : base(ref events, jobID, jobStep)
        {
            // ----------------------------------------
            //  Logging and Monitoring
            // ----------------------------------------
            sProcessingServer = appSettingsReader.GetValue("ServerID", typeof(string)).ToString();
            sAPPPath = appSettingsReader.GetValue("APPPath", typeof(string)).ToString();
            sEventTransactionID = System.Guid.NewGuid().ToString();

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(START)", iEventSeq.ToString(), "STARTING", DateTime.Now + " - BasePDFFunctions Start: Starting the BasePDFFunctions process...", "", "");
            iEventSeq = (iEventSeq + 1);
        }

        //---------------------------//
        // Convert to PDF            //
        //---------------------------//
        public string ConvertToPDF(string sDocType, string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath)
        {
            LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "DEBUG 1", iEventSeq.ToString(), "DEBUG 1", DateTime.Now + " - DEBUG 1", "sTargetPath: " + sTargetPath, "");
            iEventSeq = (iEventSeq + 1);

            StreamWriter oJobDetails;
            string sPath = sTargetPath + "JobDetails.txt";
 
            //if (sOriginalFile.Contains(".htm")) sDocType = ".htm";

            try
            {
                //-- Create or Use the JobDetails.txt file
                if (!File.Exists(sPath))
                {
                    // Create a file to write to.
                    oJobDetails = File.CreateText(sPath);
                    oJobDetails.WriteLine("JobDetails:");
                    oJobDetails.WriteLine("-- File Types:");
                }
                else
                {
                    //-- Open a stream to the existing JobDetails.txt file
                    oJobDetails = File.AppendText(sPath);
                }

                LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(SUPPORTED-DOC-TYPE)", iEventSeq.ToString(), "Checking if Supported Doc Type", DateTime.Now + " - Current DocType: Checking supported Doc Type in BasePDFFunctions process...", "DocType: " + sDocType.ToLower(), "");
                iEventSeq = (iEventSeq + 1);

                switch (sDocType.ToLower())
                {
                    case ".doc":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(DOC-CONVERSION)", iEventSeq.ToString(), "Converting .doc to .pdf", DateTime.Now + " - Converting from DocType .doc to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(DEBUGGGG1)", iEventSeq.ToString(), "DEBUGGGG1: ", "DEBUGGGG", "", "");
                        //iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");

                        //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(DEBUGGGG2)", iEventSeq.ToString(), "DEBUGGGG2: ", "DEBUGGGG", string.Format("VARS: {0},{1},{2},{3},{4}", sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType), "");
                        //iEventSeq = (iEventSeq + 1);

                        ConvertWordToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        //ConvertWordToPDF("A022064AAA9646E48A0BECFE8E46B3CA-2.adoc", "\\\\fileprd01\\APDDocuments$\\STG\\DocProcessor\\Pickup\\A022064AAA9646E48A0BECFE8E46B3CA-2.doc", "\\\\fileprd01\\APDDocuments$\\STG\\DocProcessor\\Pickup\\A022064AAA9646E48A0BECFE8E46B3CA-2.doc", "D:\\Apps\\PostOffice\\StageArea", ".doc");

                        //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(DEBUGGGG3)", iEventSeq.ToString(), "DEBUGGGG3: ", "DEBUGGGG", "", "");
                        //iEventSeq = (iEventSeq + 1);

                        break;
                    case ".docx":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(DOCX-CONVERSION)", iEventSeq.ToString(), "Converting .docx to .pdf", DateTime.Now + " - Converting from DocType .docx to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");

                       // LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "DEBUGGER-DOC", iEventSeq.ToString(), "DEBUGGER", DateTime.Now + " DEBUGGER", "DocType: " + sDocType.ToLower(), "FileName: " + sFileName + ", OriginalFile: " + sOriginalFile + ", OriginalPath: " + sOriginalPath + ", TargetPath: " + sTargetPath + ", DocType: " + sDocType);
                       // iEventSeq = (iEventSeq + 1);

                        ConvertWordToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".xls":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(XLS-CONVERSION)", iEventSeq.ToString(), "Converting .xls to .pdf", DateTime.Now + " - Converting from DocType .xls to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertExcelToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".xlsx":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(XLSX-CONVERSION)", iEventSeq.ToString(), "Converting .xlsx to .pdf", DateTime.Now + " - Converting from DocType .xlsx to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertExcelToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".rtf":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(RTF-CONVERSION)", iEventSeq.ToString(), "Converting .rtf to .pdf", DateTime.Now + " - Converting from DocType .rtf to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertRTFToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".tif":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(TIF-CONVERSION)", iEventSeq.ToString(), "Converting .tif to .pdf", DateTime.Now + " - Converting from DocType .tif to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertTIFToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".tiff":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(TIFF-CONVERSION)", iEventSeq.ToString(), "Converting .tiff to .pdf", DateTime.Now + " - Converting from DocType .tiff to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertTIFToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".png":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(PNG-CONVERSION)", iEventSeq.ToString(), "Converting .png to .pdf", DateTime.Now + " - Converting from DocType .png to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertIMGToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".jpg":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(JPG-CONVERSION)", iEventSeq.ToString(), "Converting .jpg to .pdf", DateTime.Now + " - Converting from DocType .jpg to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertIMGToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".jpeg":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(JPEG-CONVERSION)", iEventSeq.ToString(), "Converting .jpeg to .pdf", DateTime.Now + " - Converting from DocType .jpeg to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertIMGToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".gif":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(GIF-CONVERSION)", iEventSeq.ToString(), "Converting .gif to .pdf", DateTime.Now + " - Converting from DocType .gif to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertIMGToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".pdf":
                        //-- No convert just add to PDF
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(PDF-CONVERSION)", iEventSeq.ToString(), "Converting .pdf to .pdf", DateTime.Now + " - Converting from DocType .pdf to .pdf,  This is just a file copy...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        CopyFile(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".htm":
                        //LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "DEBUG 2", iEventSeq.ToString(), "DEBUG 2", DateTime.Now + " - DEBUG 2", "sTargetPath: " + sTargetPath, "");
                        //iEventSeq = (iEventSeq + 1);

                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(HTM-CONVERSION)", iEventSeq.ToString(), "Converting .htm to .pdf", DateTime.Now + " - Converting from DocType .htm to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        //LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "DEBUGGER", iEventSeq.ToString(), "DEBUGGER", DateTime.Now + " DEBUGGER", "DocType: " + sDocType.ToLower(), "FileName: " + sFileName + ", OriginalFile: " + sOriginalFile + ", OriginalPath: " + sOriginalPath + ", TargetPath: " + sTargetPath + ", DocType: " + sDocType);
                        //iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertHTMLToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".html":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(HTML-CONVERSION)", iEventSeq.ToString(), "Converting .html to .pdf", DateTime.Now + " - Converting from DocType .htm to .pdf...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        ConvertHTMLToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    case ".xml":
                        LocalLogEvent(PROCESSINGSERVER, sEventTransactionID, "PostOffice-BasePDFFunctions(XML-CONVERSION)", iEventSeq.ToString(), "Converting .XML to .htm", DateTime.Now + " - Converting from DocType .xml to .htm...", "DocType: " + sDocType.ToLower(), "");
                        iEventSeq = (iEventSeq + 1);

                        oJobDetails.WriteLine(">>>> (SUPPORTED) " + sDocType + " - " + sOriginalFile + " is a Supported File Type.  Type: ");
                        //ConvertHTMLToPDF(sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocType);
                        break;
                    default:
                        //-- Log the unprocessed file to JobDetails.txt
                        oJobDetails.WriteLine("<<<< (UN-SUPPORTED) " + sDocType + " - " + sOriginalFile + " is an UnSupported File Type.  Type: ");
                        break;
                }

                //oJobDetails.WriteLine("JobDetails END:");
                oJobDetails.Close();
                return "SUCCESS";
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", ("ConvertToPDF Failed: Failed to BasePDFFunctions.Convert file to PDF (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: ConvertToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
                return sError;
            }
        }

        //---------------------------//
        // Convert PNG to PDF        //
        //---------------------------//
        public void ConvertIMGToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();

                //Add a section to the PDF document
                PdfSection section = pdfDocument.Sections.Add();

                //Declare the PDF page
                PdfPage page;
                page = section.Pages.Add();

                //Declare PDF page graphics
                PdfGraphics graphics;

                //Load PNG Photo
                PdfBitmap PNGImage = new PdfBitmap(sOriginalFile);

                section.PageSettings.Margins.All = 0;
                graphics = page.Graphics;
                PNGImage.ActiveFrame = 0;
                graphics.DrawImage(PNGImage, 0, 0);

                //-- Replace the Original Path with the StageArea Path
                sNewPathFile = sTargetPath + sFileName.Replace(sDocumentType, ".pdf");

                //Save the PDF file to file system
                pdfDocument.Save(sNewPathFile);
                //Close the document
                pdfDocument.Close(true);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: ConvertIMGToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert WordDoc to PDF    //
        //---------------------------//
        public void ConvertWordToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(DEBUGGGG3)", iEventSeq.ToString(), "DEBUGGGG3: ", "DEBUGGGG3", "sFileName: " + sFileName + ", sOriginalFile: " + sOriginalFile + ", sOriginalPath: " + sOriginalPath + ", sTargetPath: " + sTargetPath + ", sDocumentType: " + sDocumentType, "");
                //iEventSeq = (iEventSeq + 1);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertWordToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, string.Format("sFileName: {0}, sOriginalFile: {1}, sOriginalPath: {2}, sTargetPath: {3}, sDocumentType: {4} ", sFileName, sOriginalFile, sOriginalPath, sTargetPath, sDocumentType), "");
                iEventSeq = (iEventSeq + 1);

                // Convert WordToPDF
                //WordDocument wordDocument = new WordDocument(sOriginalFile, FormatType.Docx);
                WordDocument wordDocument = new WordDocument(sOriginalFile, FormatType.Doc);
                DocToPDFConverter converter = new DocToPDFConverter(); 
                PdfDocument pdfDocument = converter.ConvertToPDF(wordDocument);

                sNewPathFile = sTargetPath + sFileName;
                sNewPathFile = sNewPathFile.Replace(".doc", ".pdf");


                //-- Replace the Original Path with the StageArea Path
                //sNewPathFile = sTargetPath + sOriginalFile.Replace(sDocumentType, ".pdf");

                //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(DEBUGGGG3)", iEventSeq.ToString(), "DEBUGGGG3: ", "DEBUGGGG3", "sNewPathFile: " + sNewPathFile, "");
                //iEventSeq = (iEventSeq + 1);


                //Save the PDF file to file system
                pdfDocument.Save(sNewPathFile);

            //Releases all resources used by the object.
            converter.Dispose();

            //Closes the instance of document objects
            pdfDocument.Close(true);
            wordDocument.Close();

            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertWordToPDF: " + sOriginalFile + " to PDF Ending.", "", "", "");
            iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting Word Doc to PDF occured: ConvertWordToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert EXCEL to PDF      //
        //---------------------------//
        public void ConvertExcelToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertExcelToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    IWorkbook workbook = application.Workbooks.Open(sOriginalFile, ExcelOpenType.Automatic);

                    //Open the Excel document to Convert
                    ExcelToPdfConverter converter = new ExcelToPdfConverter(workbook);

                    //Initialize PDF document
                    PdfDocument pdfDocument = new PdfDocument();

                    //Convert Excel document into PDF document
                    pdfDocument = converter.Convert();

                    //-- Replace the Original Path with the StageArea Path
                    sNewPathFile = sTargetPath + sFileName.Replace(sDocumentType, ".pdf");

                    //Save the PDF file to file system
                    pdfDocument.Save(sNewPathFile);
                    
                    //Releases all resources used by the object.
                    converter.Dispose();

                    //Closes the instance of document objects
                    pdfDocument.Close(true);
                }
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting Excel to PDF occured: ConvertExcelToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert HTML to PDF       //
        //---------------------------//
        public void ConvertHTMLToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            string sHTMLFileData = string.Empty;

            // ----------------------------------------
            //  Logging - Processing Email Body
            // ----------------------------------------
            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(ConvertHTMLToPDF)", iEventSeq.ToString(), "Converting HTML Email Body to PDF...", "Converting HTML Email Body Details file: " + DateTime.Now + " Temp Body FileName: " + sOriginalFile, "FileName: " + sFileName + ", Target: " + sTargetPath, "");
            iEventSeq = (iEventSeq + 1);

            string sNewFileName = string.Empty;

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);
            WebKitConverterSettings webKitSettings = new WebKitConverterSettings();

            //webKitSettings.TempPath = sAPPPath + "QtBinaries";

            string baseUrl = string.Empty;
            webKitSettings.WebKitPath = sAPPPath + "QtBinaries";
            htmlConverter.ConverterSettings = webKitSettings;

            try
            {
                //-- Replace the Original Path with the StageArea Path
                //if (sFileName.Contains(".txt"))
                //{
                //    sDocumentType = ".htm";
                //}

                sNewPathFile = sTargetPath + sFileName;
                sNewPathFile = sNewPathFile.Replace(".htm", ".pdf");
                sNewPathFile = sNewPathFile.Replace(".txt", ".pdf");

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(PRE_HTMLToPDF)", iEventSeq.ToString(), "Ready to convert HTML Email Body to PDF...", "Converting HTML Email Body Details file: " + DateTime.Now + " New Body FileName: " + sNewPathFile, "FileName: " + sNewPathFile, "HTML Data: " + sOriginalFile);
                iEventSeq = (iEventSeq + 1);

                sHTMLFileData = File.ReadAllText(sOriginalFile);

                PdfDocument document = htmlConverter.Convert(sHTMLFileData, baseUrl);
                // PdfDocument document = htmlConverter.Convert("<html><body>hello</body></html>", baseUrl);

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(ConvertHTMLToPDF)", iEventSeq.ToString(), "Done Converting HTML Email Body to PDF...", "Done Converting HTML Email Body Details file: " + DateTime.Now + " New Body FileName: " + sNewPathFile, "New Body FileName: " + sNewPathFile, "");
                iEventSeq = (iEventSeq + 1);

                //Save the PDF file to file system
                document.Save(sNewPathFile);

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(ConvertHTMLToPDF)", iEventSeq.ToString(), "Saving Converted HTML Email Body to PDF...", "Saving Converted HTML Email Body Details file: " + DateTime.Now + " Saving Body FileName: " + sNewPathFile, "Saving Body FileName: " + sNewPathFile, "");
                iEventSeq = (iEventSeq + 1);

                //Closes the instance of document objects
                document.Close(true);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error Occured: PDF convert failed...", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert RTF to PDF        //
        //---------------------------//
        public void ConvertRTFToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertRTFToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                WordDocument rtfDocument = new WordDocument(sOriginalFile);

                //create an instance of DocToPDFConverter - responsible for Word to PDF conversion
                DocToPDFConverter converter = new DocToPDFConverter();

                //Set the image quality
                converter.Settings.ImageQuality = 100;

                //Set the image resolution
                converter.Settings.ImageResolution = 640;

                //Set true to optimize the memory usage for identical images
                converter.Settings.OptimizeIdenticalImages = true;

                //Convert Word document into PDF document
                PdfDocument pdfDocument = converter.ConvertToPDF(rtfDocument);

                //Save the PDF file to file system
                pdfDocument.Save(sNewPathFile);

                //close the instance of document objects
                pdfDocument.Close(true);
                rtfDocument.Close();
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting RTF to PDF occured: ConvertRTFToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert TIF to PDF        //
        //---------------------------//
        public void ConvertTIFToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertTIFToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();

                //Add a section to the PDF document
                PdfSection section = pdfDocument.Sections.Add();

                //Declare the PDF page
                PdfPage page;

                //Declare PDF page graphics
                PdfGraphics graphics;

                //Load multi frame TIFF image
                PdfBitmap tiffImage = new PdfBitmap(sOriginalFile);

                //Get the frame count
                int frameCount = tiffImage.FrameCount;

                //Access each frame draw into the page
                for (int i = 0; i < frameCount; i++)
                {
                    page = section.Pages.Add();
                    section.PageSettings.Margins.All = 0;
                    graphics = page.Graphics;
                    tiffImage.ActiveFrame = i;
                    graphics.DrawImage(tiffImage, 0, 0, page.GetClientSize().Width, page.GetClientSize().Height);
                }

                //-- Replace the Original Path with the StageArea Path
                sNewPathFile = sTargetPath + sFileName.Replace(sDocumentType, ".pdf");
                //Save the PDF file to file system
                pdfDocument.Save(sNewPathFile);
                //Close the document
                pdfDocument.Close(true);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting TIF to PDF occured: ConvertTIFToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert XPS to PDF        //
        //---------------------------//
        public void ConvertXPSToPDF(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-Coversions(CONVERT2PDF)", iEventSeq.ToString(), "ConvertXPSToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();
                XPSToPdfConverter XPSDocument = new XPSToPdfConverter();

                //Convert the XPS to PDF.
                PdfDocument document = XPSDocument.Convert(sOriginalFile);

                //-- Replace the Original Path with the StageArea Path
                sNewPathFile = sTargetPath + sFileName.Replace(sDocumentType, ".pdf");
                //Save the PDF file to file system
                pdfDocument.Save(sNewPathFile);

                //Close the document
                document.Close(true);
                pdfDocument.Close(true);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting XPS to PDF occured: ConvertXPSToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert PDF to Image      //
        //---------------------------//
        public void ConvertPDFToImage(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            ////Create a PDF document
            //PdfDocumentView documentViewer = new PdfDocumentView();

            ////Load the PDF document
            //documentViewer.Load(sOriginalFile);

            ////Export PDF page to image
            //Bitmap image = documentViewer.ExportAsImage(0);

            ////Save the image.
            //image.Save("Output.png", ImageFormat.Png);
            ////Save the PDF file to file system
            //image.Save(sOriginalFile.Replace(sDocumentType, ".png"));
            //documentViewer.Dispose();
        }

        //---------------------------//
        // Convert PDF to PDF        //
        //---------------------------//
        public void CopyFile(string sFileName, string sOriginalFile, string sOriginalPath, string sTargetPath, string sDocumentType)
        {
            string sNewPathFile = string.Empty;

            try
            {
                //-- Don't do anything, just copy PDF Document to StageArea
                sNewPathFile = sTargetPath + sFileName;
                System.IO.File.Copy(sOriginalFile, sNewPathFile);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting PDF to PDF occured: CopyFile Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //public Boolean CreatePDFBundle(string sOutputFile)
        //{
        //    LocalLogEvent(sProcessingServer, sEventTransactionID, "<<<DEBUG>>>", iEventSeq.ToString(), "MERGE: Ready to Merge", "", "", "");
        //    iEventSeq = (iEventSeq + 1);

        //    PdfLoadedDocument lDoc1 = new PdfLoadedDocument("D:\\Apps\\PostOffice\\StageArea\\TEST.pdf");
        //    PdfLoadedDocument lDoc2 = new PdfLoadedDocument("D:\\Apps\\PostOffice\\StageArea\\TEST1.pdf");

        //    PdfDocument NewPDFDocument = new PdfDocument();

        //    NewPDFDocument.ImportPage(lDoc1, 0);
        //    NewPDFDocument.ImportPageRange(lDoc2, 0, lDoc1.Pages.Count - 1);

        //    LocalLogEvent(sProcessingServer, sEventTransactionID, "<<<DEBUG-1>>>", iEventSeq.ToString(), "MERGE: DONE Merging", "", "", "");
        //    iEventSeq = (iEventSeq + 1);

        //    try
        //    {
        //        NewPDFDocument.Save("D:\\Apps\\PostOffice\\StageArea\\Sample.pdf");

        //        NewPDFDocument.Close(true);
        //        lDoc1.Close(true);
        //        lDoc2.Close(true);
        //    }
        //    catch (Exception oExcept)
        //    {
        //        // ---------------------------------
        //        //  Error handler and notifications
        //        // ---------------------------------
        //        string sError = string.Empty;
        //        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //        sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

        //        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-BasePDFFunctions(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error Saving PDF occured: ConvertXPSToPDF Exception thrown", sError, "");
        //        iEventSeq = (iEventSeq + 1);
        //    }

        //    LocalLogEvent(sProcessingServer, sEventTransactionID, "<<<DEBUG-2>>>", iEventSeq.ToString(), "SAVE: DONE Saving", "", "", "");
        //    iEventSeq = (iEventSeq + 1);


        //    //PdfLoadedDocument lDoc1 = new PdfLoadedDocument("D:\\Apps\\PostOffice\\StageArea\\TEST.pdf");
        //    //PdfLoadedDocument lDoc2 = new PdfLoadedDocument("D:\\Apps\\PostOffice\\StageArea\\TEST1.pdf");

        //    //PdfDocument NewPDFDocument = new PdfDocument();


        //    //NewPDFDocument.ImportPage(lDoc1, 0);
        //    //NewPDFDocument.ImportPageRange(lDoc2, 0, lDoc1.Pages.Count - 1);

        //    //NewPDFDocument.Save("D:\\Apps\\PostOffice\\StageArea\\Sample.pdf");

        //    LocalLogEvent(sProcessingServer, sEventTransactionID, "<<<DEBUG-2>>>", iEventSeq.ToString(), "SAVE: Saving", "", "", "");
        //    iEventSeq = (iEventSeq + 1);

        //    //NewPDFDocument.Close(true);

        //    return true;
        //}

        private Boolean CreateZIPBundle(string sOutputFile)
        {
            return true;
        }

    }
}
