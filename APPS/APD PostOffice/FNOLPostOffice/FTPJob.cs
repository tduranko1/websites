﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using WinSCP;

namespace FNOLPostOffice
{    

    public class FTPJob : BaseJob
    {


        public FTPJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetails)
        {            
            bool success = false;
            bool ignoreEnvOv = false;
            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Method = "FTP";
            XmlNode ftpNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("FTP[@stepid='{0}']", _jobStep.ToString()),false);
            FTPDestinationInfo destInfo = null;

            if (ftpNode != null)
            {

                ignoreEnvOv = FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref ftpNode, "@ignoreenvoverride"));
                destInfo = new FTPDestinationInfo(ftpNode);
                if ((_isProduction) || (ignoreEnvOv))
                {
                    success = FTPFilesForJob(ref jobDetails, ref destInfo);
                }
                else if (!_isProduction)
                {
                    //Notification of destination overrid(suppression) due to environment.
                    OverrideNotification("FTP", jobDetails);
                    pjr.Success = true;
                }
            }
            else
            {
                success = false;
                string errInfo = "FTP node for job step is not present in job details.";                
                RaiseError(errInfo);
                JobFailNotification("FTP", errInfo, jobDetails);
            }

            pjr.Success = success;                        
            return pjr;
        }

        private bool FTPFilesForJob(ref XmlDocument jobDetails, ref FTPDestinationInfo dest)//, int count)
        {
            string errorInfo = "";
            bool success = false;
            
            XmlNode fileNode = null;
            string sourceFile = "";
            string remoteFile = "";
            TransferOperationResult tor = null;
            XmlNode filesToFTP = XmlUtils.GetChildNode(ref jobDetails, "FileList");

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(_jobFileFolder);
            if (!(di.Exists)) {
                di.Create();
            }
            string logPath = _jobFileFolder;
            string debugLog = GenUtils.AppendPath(_jobFileFolder,string.Format("ftp_debug_{0}.log", _jobStep.ToString()));
            string sessionLog = GenUtils.AppendPath(_jobFileFolder, string.Format("ftp_session_{0}.log", _jobStep.ToString()));

            WinSCP.Session ftp = new Session();

            ftp.ExecutablePath = _events.cSettings.GetParsedSetting("WINSCP/@exepath");
            if (FPOUtils.AsBoolean(_events.cSettings.GetParsedSetting("WINSCP/@enabledebuglogging")))
                ftp.DebugLogPath = debugLog;
            ftp.SessionLogPath = sessionLog;
            SessionOptions so = new SessionOptions();
            TransferOptions txfrOpt = new TransferOptions();
            txfrOpt.TransferMode = (dest.EnableAsciiTransferMode ? TransferMode.Ascii : TransferMode.Binary);
            txfrOpt.PreserveTimestamp = false;            
            so.FtpMode = FtpMode.Passive;
            so.UserName = dest.UserName;
            so.Password = dest.Password;
            so.HostName = dest.Server;
            so.Protocol = dest.FTPProtocol;
            
            switch (so.Protocol)
            {
                case Protocol.Sftp:
                    so.FtpSecure = FtpSecure.None;
                    so.SshHostKeyFingerprint = dest.HostKey;
                    break;
                default:
                    if (dest.HostKey.Length > 0)
                    {
                        so.FtpSecure = FtpSecure.ExplicitSsl;
                        so.SslHostCertificateFingerprint = dest.HostKey;
                    }
                    else
                    {
                        so.FtpSecure = FtpSecure.None;
                    }
                    break;
            }
            _events.Trace(string.Format("Opening {0} Connection to: {1}", dest.FTPProtocol.ToString(), dest.Server));

            try
            {
                ftp.Open(so);
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                _events.HandleEvent(ex, false);
                JobFailNotification("FTP", string.Format("Received following error when attempting to open FTP connection: \n\n {0}", ex.ToString()), jobDetails);
            }
            if (success)
            {               
                int ftpFails = 0;
                
                foreach (XmlNode f in XmlUtils.GetChildNodeList(ref filesToFTP, "File"))
                {
                    fileNode = f;
                    sourceFile = XmlUtils.GetChildNodeText(ref fileNode, "@source");
                    remoteFile = string.Format("{0}/{1}",dest.TargetPath ,XmlUtils.GetChildNodeText(ref fileNode, "@filename"));

                    _events.Trace(string.Format("FTPing file '{0}' to '{1}'", sourceFile, remoteFile));

                    try
                    {
                        tor = ftp.PutFiles(sourceFile, remoteFile, false, txfrOpt);
                        if (!tor.IsSuccess)
                        {
                            _events.Trace("Failed to FTP file {0}", sourceFile);
                            foreach (SessionRemoteException sre in tor.Failures)
                            {
                                _events.HandleEvent(((Exception)sre), false);
                            }
                            ftpFails += 1;
                        }

                    }
                    catch (Exception ex)
                    {
                        ftpFails += 1;
                        _events.HandleEvent(ex, false);
                        errorInfo = ex.ToString();
                    }


                }
                success = (ftpFails == 0);
                if (!success)
                {
                    JobFailNotification("FTP", errorInfo, jobDetails); 
                }
            }

            if (ftp != null)
            {
                ftp.Dispose();
                ftp = null;
            }
            return success;        

        }
    }

}
