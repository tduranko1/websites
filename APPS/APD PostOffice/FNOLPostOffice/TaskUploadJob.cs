﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;

using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Linq;

namespace FNOLPostOffice
{
    public class TaskUploadJob : BaseJob
    {
        public Boolean bIsValid = false;
        private string _defaultSender = string.Empty;
        private string sParams = string.Empty;
       
        public TaskUploadJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            //-- Initialize
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskUpload(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - TaskUploadJob Start: Starting the TaskUpload process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetailsXML)
        {
            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - TaskPerformJob Start: Starting the TaskUpload process, TaskPerformJob...", "", "JobDetails: " + jobDetailsXML.InnerXml);
            intEventSeq = (intEventSeq + 1);

            //ValidateXML("D:\\Apps\\PostOffice\\XSDSchemas\\DocumentUpload.xml", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");
            Boolean bCheckIfValid = true;
            string sLynxID = string.Empty;
            string sUserID = string.Empty;
            string sPertainsTo = string.Empty;
            string sVehicleNumber = string.Empty;
            string sInsuranceCompanyID = string.Empty;
            string sClaimAspectID = string.Empty;
            string sClaimAspectServiceChannelID = string.Empty;
            string sTaskID = string.Empty;
            string sAlarmDate = string.Empty;
            string sUserTaskDescription = string.Empty;
            string sAssignedUserID = string.Empty;
            string sAssignmentPoolFunctionCD = string.Empty;
            string SupervisorFlag = string.Empty;

            int iUserID = 0;
            Boolean bProcessedTaskd = false;
            XmlNode oXMLNode = null;


            try
            {
                bool success = false;
                long wcfResults = 0;
                string sAppPath = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
                StringBuilder errInfo = new StringBuilder();

                XmlDocument docTaskUploadXML = new XmlDocument();
                ProcessJobResult pjr = new ProcessJobResult();
                XmlSchemaSet schemas = new XmlSchemaSet();

                string sDocumentID = string.Empty;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - TaskPerformJob Start: Starting the TaskUpload process, TaskPerformJob...", "", "JobDetails: " + jobDetailsXML.InnerXml);
                intEventSeq = (intEventSeq + 1);

                try
                {
                    if (jobDetailsXML != null)
                    {
                        //-- Get the DocmentUpload Level details from the XML
                        XmlNode jobTaskUploadXML = jobDetailsXML.SelectSingleNode("JobDetails/TaskUpload");

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - Application Path", string.Format("<<<Application Path: {0} >>>", sAppPath), "");
                        intEventSeq = (intEventSeq + 1);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - TaskUpload XML Section", string.Format("<<<XML TaskUploadXML: {0} >>>", jobTaskUploadXML.OuterXml), "");
                        intEventSeq = (intEventSeq + 1);

                        //    //ValidateXML("D:\\Apps\\PostOffice\\XSDSchemas\\DocumentUpload.xml", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");
                        //    //bCheckIfValid = ValidateXML(fsjobDocumentUploadXML.OuterXml, sAppPath + "\\XSDSchemas\\TaskUpload.xsd");
                        bCheckIfValid = true;

                        //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XSDSCHEMA)", intEventSeq.ToString(), "XSDSchema", DateTime.Now + " - Validating the JobDetails XML against the TaskUpload.xsd", string.Format("<<<XSD TaskDocumentUploadXSD: {0} >>>", sAppPath + "\\XSDSchemas\\TaskDocumentUpload.xsd"), "XSDValid: " + bCheckIfValid.ToString());
                        //    intEventSeq = (intEventSeq + 1);

                        // If xml is valid process the Task to APD
                        if (bCheckIfValid)
                        {
                            try
                            {
                                //Parse the XML to get the job details
                                oXMLNode = jobTaskUploadXML.SelectSingleNode("/JobDetails/TaskUpload");
                                //GetXMLSingleNodeValue
                                //GetXMLNodeAttributeValue
                                sLynxID = GetXMLSingleNodeValue(oXMLNode, "LynxID");
                                sUserID = GetXMLSingleNodeValue(oXMLNode, "UserID");
                                sPertainsTo = GetXMLSingleNodeValue(oXMLNode, "PertainsTo");
                                sVehicleNumber = sPertainsTo.Substring(3, (sPertainsTo.Length) - 3);

                                sTaskID = GetXMLSingleNodeValue(oXMLNode, "TaskID");
                                sAlarmDate = GetXMLSingleNodeValue(oXMLNode, "AlarmDate");
                                sUserTaskDescription = GetXMLSingleNodeValue(oXMLNode, "UserTaskDescription");
                                sAssignedUserID = GetXMLSingleNodeValue(oXMLNode, "AssignedUserID");
                                sAssignmentPoolFunctionCD = GetXMLSingleNodeValue(oXMLNode, "AssignmentPoolFunctionCD");
                                SupervisorFlag = GetXMLSingleNodeValue(oXMLNode, "SupervisorFlag");
                            }
                            catch
                            {
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XMLPARSE_ERROR)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - (ERROR) Parsing the XML Data..", string.Format("<<<VARS: {0},{1},{2},{3},{4},{5},{6},{7},{8},{9} >>>", sLynxID, sUserID, sPertainsTo, sVehicleNumber, sTaskID, sAlarmDate, sUserTaskDescription, sAssignedUserID, sAssignmentPoolFunctionCD, SupervisorFlag), "");
                                intEventSeq = (intEventSeq + 1);
                            }

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XMLPARSE)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Parsing the XML Data..", string.Format("<<<VARS: {0},{1},{2},{3},{4},{5},{6},{7},{8},{9} >>>", sLynxID, sUserID, sPertainsTo, sVehicleNumber, sTaskID, sAlarmDate, sUserTaskDescription, sAssignedUserID, sAssignmentPoolFunctionCD, SupervisorFlag), "");
                                    intEventSeq = (intEventSeq + 1);

                                    XmlNode XMLReturn = null;
                                    XMLReturn = GetInscCompIDByLynxID(Int32.Parse(sLynxID), Int32.Parse(sVehicleNumber));

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(GetInscCompIDByLynxID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - GetInscCompIDByLynxID XML Data..", XMLReturn.OuterXml, "");
                                    intEventSeq = (intEventSeq + 1);

                                    if (!XMLReturn.OuterXml.Contains("ERROR"))
                                    {
                                        oXMLNode = XMLReturn.SelectSingleNode("/Claim");
                                        sInsuranceCompanyID = GetXMLNodeAttributeValue(oXMLNode, "InsuranceCompanyID");
                                        sClaimAspectID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectID");
                                        sClaimAspectServiceChannelID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectServiceChannelID");

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(ClaimNode)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Parsing the Claim Aspect Details", "Returned VARS: sInsuranceCompanyID: " + sInsuranceCompanyID + ", sClaimAspectID: " + sClaimAspectID + ", sClaimAspectServiceChannelID: " + sClaimAspectServiceChannelID, "");
                                        intEventSeq = (intEventSeq + 1);

                                        //-- Lookup Andriod UserID --//
                                        XMLReturn = GetAndroidUserID();
                                        if (!XMLReturn.OuterXml.Contains("ERROR"))
                                        {
                                            sUserID = GetXMLNodeAttributeValue(XMLReturn, "UserID");

                                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(ANDROIDUSERID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Getting the Android UserID.", string.Format("<<<Android UserID: {0}, sInsuranceCompanyID: {1}, sClaimAspectID: {2}, sClaimAspectServiceChannelID: {2} >>>", sUserID, sInsuranceCompanyID, sClaimAspectID, sClaimAspectServiceChannelID), "");
                                            intEventSeq = (intEventSeq + 1);

                                    //                XMLReturn = GetClaimDetailsByLynxID(Int32.Parse(sLynxID), Int32.Parse(sInsuranceCompanyID));

                                    //                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(GetClaimDetailsByLynxID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Getting the claim details by LynxID.", string.Format("<<<XMLReturn: {0} >>>", XMLReturn.OuterXml), "");
                                    //                intEventSeq = (intEventSeq + 1);

                                    //                if (!XMLReturn.OuterXml.Contains("ERROR"))
                                    //                {
                                    //                    oXMLNode = XMLReturn.SelectSingleNode("/Claim");
                                    //                    //sInsuranceCompanyID = GetXMLNodeAttributeValue(oXMLNode, "InsuranceCompanyID");
                                    //                    //sClaimAspectID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectID");
                                    //                    //sInsuranceCompanyID = GetXMLNodeAttributeValue(oXMLNode, "InsuranceCompanyID");
                                    //                    //sClaimAspectID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectID");

                                    //                    // TEST RESULTS
                                    //                    if (bProcessedTaskd == false)
                                    //                    {
                                    //                        wcfResults = -1;
                                    //                    }
                                    //                    else
                                    //                    {
                                    //                    }
                                    //                }
                                    //                else
                                    //                {
                                    //                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetClaimDetailsByLynxID(ERROR): ERROR Getting Claim Details - Failed...  " + XMLReturn.OuterXml));
                                    //                }


                                    if (Int32.Parse(sUserID) >= 1)
                                    {
                                        wcfResults = 0;

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(SQL)", intEventSeq.ToString(), "APDInsertTask", DateTime.Now + " - processing the APDInsertTask into APD.", string.Format("<<<Params: sClaimAspectServiceChannelID: {0}, sTaskID: {1}, sAlarmDate: {2}, sUserTaskDescription: {3}, sAssignedUserID: {4}, sAssignmentPoolFunctionCD: {5}, SupervisorFlag: {6}, iUserID: {7}  >>>", sClaimAspectServiceChannelID, sTaskID, sAlarmDate, sUserTaskDescription, sAssignedUserID, sAssignmentPoolFunctionCD, SupervisorFlag, iUserID), "");
                                        intEventSeq = (intEventSeq + 1);

                                        bProcessedTaskd = APDInsertTask(Int32.Parse(sClaimAspectServiceChannelID), Int32.Parse(sTaskID), sAlarmDate, sUserTaskDescription, sAssignedUserID, sAssignmentPoolFunctionCD, Int32.Parse(SupervisorFlag), iUserID);

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(PROCESSED)", intEventSeq.ToString(), "Processing Results", DateTime.Now + " - Processing Results.", string.Format("<<<Results: {0}>>>", bProcessedTaskd.ToString()), "");
                                        intEventSeq = (intEventSeq + 1);

                                    }
                                    else
                                            {
                                                wcfResults = -1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(ERROR)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - ERROR GetInscCompIDByLynxID failed to process successfully.", "", "");
                                        intEventSeq = (intEventSeq + 1);
                                    }
                        }

                        // ----------------------------------------
                        //  Logging
                        // ----------------------------------------
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(SQL)", intEventSeq.ToString(), "SQL(uspDiaryInsDetail)", DateTime.Now + " - Uploading the Task to the server via SQL(uspDiaryInsDetail)...", string.Format("<<<Diary Task Response TaskID: {0} >>>", wcfResults), "");
                        intEventSeq = (intEventSeq + 1);

                        if (wcfResults >= 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                            errInfo.Append("ERROR: Task failed to upload into server...");
                            RaiseError(errInfo.ToString());
                        }
                    }
                    else
                    {
                        success = false;
                        errInfo.Append("XMLValidationError: No JobDetails XML Found...");
                        RaiseError(errInfo.ToString());
                    }

                    if (!success)
                    {
                        JobFailNotification("Task", errInfo.ToString(), jobDetailsXML);
                    }
                }
                catch (Exception oExcept)
                {
                    // ------------------------------
                    //  Email Notify of the Error
                    // ------------------------------
                    string strError = "";
                    StackFrame FunctionName = new StackFrame();
                    strError = string.Format("Error: {0}", ("TaskUploadJob Failed: TaskPerformJob process failed. (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Details: " + strError, "");
                    intEventSeq = (intEventSeq + 1);

                    success = false;
                    return pjr;
                }

                pjr.Method = "Task";
                pjr.Success = success;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - TaskPerformJob End: Ending the TaskUpload process, TaskPerformJob...", "", "");
                intEventSeq = (intEventSeq + 1);

                return pjr;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("WebServices Failed: ProcessJobResult process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                ProcessJobResult pjr = new ProcessJobResult();
                pjr.Method = "Task";
                pjr.Success = false;
                return pjr;
            }
            finally
            {
            }
        }

        public Boolean ValidateXML(string strXML, string strSchmaPath)
        {
            bIsValid = true;

            try
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XSDVALIDATIONHANDLER)", intEventSeq.ToString(), "XSDValidationHandler", DateTime.Now + " - Validating XML vs XSD.", string.Format("<<<XMLPath: {0}, XSDPath: {1} >>>", strXML, strSchmaPath), "");
                intEventSeq = (intEventSeq + 1);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add("", strSchmaPath);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler += new ValidationEventHandler(XSDValidatedeventHandler);

                XmlReader rd = XmlReader.Create(strXML, settings);
                while (rd.Read()) ;

                rd.Close();
                return bIsValid;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-ValidateXML: ValidateXML process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                bIsValid = false;
                return bIsValid;
            }
        }

        public void XSDValidatedeventHandler(object sender, ValidationEventArgs e)
        {
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-XSDValidatedeventHandler(XSDERRORHANDLER)", intEventSeq.ToString(), "XSDValidatedeventHandler", DateTime.Now + " - an XSD Error was thrown.", string.Format("<<<XSDError: {0} >>>", e.Message.Replace("'", "''")), "");
            intEventSeq = (intEventSeq + 1);

            try
            {
                if (e.Severity == XmlSeverityType.Warning)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XSDWARNING)", intEventSeq.ToString(), "XSDSchemaValidationWarning", DateTime.Now + " - a Warning was thrown while validating the TaskUpload XML against the TaskUpload.xsd", string.Format("<<<Validation Warning Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                    bIsValid = false;
                }
                else if (e.Severity == XmlSeverityType.Error)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-TaskPerformJob(XSDERROR)", intEventSeq.ToString(), "XSDSchemaValidationError", DateTime.Now + " - an Error was thrown while validating the TaskUpload XML against the TaskUpload.xsd", string.Format("<<<Validation Error Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                    bIsValid = false;
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-XSDValidatedeventHandler: ValidateXMLeventHandler process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);
                bIsValid = false;
            }
        }

        public Boolean APDInsertTask(int iClaimAspectServiceChannelID, int iTaskID, string sAlarmDate, string sUserTaskDescription, string sAssignedUserID, string sAssignmentPoolFunctionCD, int iSupervisorFlag, int iUserID)
        {
            string sRC = string.Empty;

            //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertTask(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertTask Process Start", "", "");
            //intEventSeq = (intEventSeq + 1);

            try
            {
                sParams = "@ClaimAspectServiceChannelID = " + iClaimAspectServiceChannelID;
                sParams += ", @TaskID = " + iTaskID;
                sParams += ", @AlarmDate = '" + sAlarmDate + "'";
                sParams += ", @UserTaskDescription = '" + sUserTaskDescription + "'";
                sParams += ", @AssignedUserID = " + sAssignedUserID;
                sParams += ", @AssignmentPoolFunctionCD = " + sAssignmentPoolFunctionCD;
                sParams += ", @SupervisorFlag = " + iSupervisorFlag;
                sParams += ", @UserID = " + iUserID;

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertTask(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertTask Process Start: Starting the insert of a Task into APD.", "", "uspDiaryInsDetail ");
                intEventSeq = (intEventSeq + 1);

                sRC = wsAPDFoundation.ExecuteSpAsString("uspDiaryInsDetail", sParams);

                if (sRC.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-APDInsertTask(ERROR): ERROR APD Insert Task Failed...  " + sRC));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertTask(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - APDInsertTask Process End: Ending the insert of a Task into APD.", "", "");
                intEventSeq = (intEventSeq + 1);

                return true;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-EXECAsString: Insert Task process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                return false;
            }
        }

        public Boolean APDInsertDiary(int iClaimAspectServiceChannelID, int iTaskID, string sAlarmDate, string sUserTaskDescription, string sAssignedUserID, string sAssignmentPoolFunctionCD, bool bSupervisorFlag, int iUserID)
        {
            string sRC = string.Empty;

            try
            {
                sParams = "@ClaimAspectServiceChannelID = " + iClaimAspectServiceChannelID;
                sParams += ", @TaskID = " + iTaskID;
                sParams += ", @AlarmDate = '" + sAlarmDate +"'";
                sParams += ", @UserTaskDescription = '" + sUserTaskDescription + "'";
                sParams += ", @AssignedUserID = " + sAssignedUserID;
                sParams += ", @AssignmentPoolFunctionCD = '" + sAssignmentPoolFunctionCD + "'";
                sParams += ", @SupervisorFlag = " + bSupervisorFlag;
                sParams += ", @UserID = " + iUserID;

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertDiary(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertDiary Process Start: Starting the insert of a Task into APD.", "", "uspDiaryInsDetail " + sParams);
                intEventSeq = (intEventSeq + 1);

                sRC = wsAPDFoundation.ExecuteSpAsString("uspDiaryInsDetail", sParams);

                if (sRC.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-APDInsertDiary(ERROR): ERROR APD Insert Diary Failed...  " + sRC));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertDiary(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - APDInsertDiary Process End: Ending the insert of a Task into APD.", "", "");
                intEventSeq = (intEventSeq + 1);

                return true;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-EXECAsString: Insert Diary process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                return false;
            }
        }
    }
}
