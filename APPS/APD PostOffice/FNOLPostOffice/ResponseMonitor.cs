﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice
{
    public class ResponseMonitor
    {
        private Events _events = null;
        private bool _serviceRunning = false;
        private ManualResetEvent _mre = null;
        private FNOLDataAccess _dataAccess = null;
        private string _smptServer = "";
        private string _sender = "";
        private string _configFile;
        private int _dom = 0;

        public ResponseMonitor(string config, ManualResetEvent mre)
        {
            _configFile = config;
            _dom = DateTime.Now.Day;
            _mre = mre;
            InitializeEventsObject();
            _smptServer = _events.cSettings.GetParsedSetting("SMTPServer");
            _sender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");
        }

        private void InitializeEventsObject()
        {
            _events = new Events();
            _events.ComponentInstance = "ResponseMonitor";
            _events.Initialize(_configFile, true);
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);
        }

        public void StopProcessing()
        {
            _events.Trace("Received Stop Signal..");
            _serviceRunning = false;
        }

        public void StartProcessing()
        {
            DataTable dt = null;
            _serviceRunning = true;
            int cdom = 0;

            while (_serviceRunning)
            {
                cdom = DateTime.Now.Day;
                if (cdom != _dom)
                {
                    // Day has changed restart logs
                    _events.Dispose();
                    _dataAccess = null;
                    _events = null;
                    GC.Collect(2);
                    InitializeEventsObject();
                    _dom = cdom;
                }
                dt = GetListOfJobsExpectingResponses();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        System.Collections.IEnumerator ptr = dt.Rows.GetEnumerator();
                        DataRow dr = null;
                        while (ptr.MoveNext())
                        {
                            dr = (DataRow)ptr.Current;
                            HandleNoResponseReceivedCondition(ref dr);
                        }                        
                    }
                }
                _events.DumpLogBufferToDisk();
                Thread.Sleep(10000);
            }
            _events.Dispose();
            _events = null;
            //signal that we're done processing any jobs.
            if (_mre != null) _mre.Set();
        }

        private DataTable GetListOfJobsExpectingResponses()
        {
            DataTable dt = null;
            try
            {
                NameValueCollection p = new NameValueCollection();
                p.Add("CallingServer", Environment.MachineName.ToUpper());
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("PostOfficeNoResponseReceived", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = null;
            }
            return dt;
        }

        private void HandleNoResponseReceivedCondition(ref DataRow dr)
        {
            XmlDocument jobDetails = new XmlDocument();
            string tmp = Convert.ToString(dr["JobDetails"]);
            jobDetails.LoadXml(tmp);
            string jobID = Convert.ToString(dr["JobID"]);
            string clientID = Convert.ToString(dr["ClientID"]);
            string stepID = Convert.ToInt32(dr["StepID"]).ToString();
            string method = Convert.ToString(dr["Method"]);
            string clientName = Convert.ToString(dr["ClientName"]);
            string productType = Convert.ToString(dr["ProductType"]);
            string lynxID = Convert.ToString(dr["LynxID"]);
            DateTime jobSent = Convert.ToDateTime(dr["JobSent"]);
            string fileName = XmlUtils.GetChildNodeText(ref jobDetails,"//FileList/File/@filename");
            if (method == "EML") method = "EMAIL";

            string recipients  = XmlUtils.GetChildNodeText(ref jobDetails,string.Format("{0}[@stepid='{1}']/@noresponsenotificationrecipient",method,stepID));

            if (recipients.Length == 0) recipients = _events.cSettings.GetParsedSetting("NoResponseNotificationDefaultRecipients");
            
            string subj = string.Format("No Response Received for LYNX ID#{0}", lynxID);

            StringBuilder body = new StringBuilder();
            body.Append(string.Format("No response was received for {0} submission of {1} to {2} -- Lynx ID#{3}.\n\n",method,productType,clientName,lynxID));
            body.Append(string.Format("Job ID = {0}\n",jobID));
            body.Append(string.Format("Job Sent = {0}\n", jobSent.ToString("MM/dd/yyyy HH:mm:ss")));

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(_sender);
            mail.Subject = subj;
            mail.Body = body.ToString();
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;

            foreach (string recipient in recipients.Split('|'))
            {
                try
                {
                    mail.To.Add(new MailAddress(recipient));
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, string.Format("Email Address = {0}", recipient), false);
                }
            }
            if (mail.To.Count > 0)
            {
                SmtpClient sc = new SmtpClient(_smptServer);
                sc.Send(mail);
            }                      

        }
        

    }
}
