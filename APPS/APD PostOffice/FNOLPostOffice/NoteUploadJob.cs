﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;

using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Linq;

namespace FNOLPostOffice
{
    public class NoteUploadJob : BaseJob
    {
        public Boolean bIsValid = false;
        private string _defaultSender = string.Empty;
        private string sParams = string.Empty;
       
        public NoteUploadJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            //-- Initialize
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NoteUpload(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - NoteUploadJob Start: Starting the NoteUpload process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetailsXML)
        {
            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - NotePerformJob Start: Starting the NoteUpload process, NotePerformJob...", "", "JobDetails: " + jobDetailsXML.InnerXml);
            intEventSeq = (intEventSeq + 1);

            //ValidateXML("D:\\Apps\\PostOffice\\XSDSchemas\\DocumentUpload.xml", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");
            Boolean bCheckIfValid = true;
            string sLynxID = string.Empty;
            string sUserID = string.Empty;
            string sPertainsTo = string.Empty;
            string sVehicleNumber = string.Empty;
            string sSuppSeqNumber = string.Empty;
            string sDocumentType = string.Empty;
            string sDocumentSource = string.Empty;
            string sNoteType = string.Empty;
            string sStatus = string.Empty;
            string sNote = string.Empty;
            string sPrivate = string.Empty;
            string sInsuranceCompanyID = string.Empty;
            string sClaimAspectID = string.Empty;
            int iUserID = 0;
            Boolean bProcessedNoted = false;
            XmlNode oXMLNode = null;


            try
            {
                bool success = false;
                long wcfResults = 0;
                string sAppPath = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
                StringBuilder errInfo = new StringBuilder();

                XmlDocument docNoteUploadXML = new XmlDocument();
                ProcessJobResult pjr = new ProcessJobResult();
                XmlSchemaSet schemas = new XmlSchemaSet();

                string sDocumentID = string.Empty;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - NotePerformJob Start: Starting the NoteUpload process, NotePerformJob...", "", "JobDetails: " + jobDetailsXML.InnerXml);
                intEventSeq = (intEventSeq + 1);

                try
                {
                    if (jobDetailsXML != null)
                    {
                        //-- Get the DocmentUpload Level details from the XML
                        XmlNode jobDocumentUploadXML = jobDetailsXML.SelectSingleNode("JobDetails/NoteUpload");

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - Application Path", string.Format("<<<Application Path: {0} >>>", sAppPath), "");
                        intEventSeq = (intEventSeq + 1);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - DocumentUpload XML Section", string.Format("<<<XML DocumentUploadXML: {0} >>>", jobDocumentUploadXML.OuterXml), "");
                        intEventSeq = (intEventSeq + 1);

                        //ValidateXML("D:\\Apps\\PostOffice\\XSDSchemas\\DocumentUpload.xml", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");
                        //bCheckIfValid = ValidateXML(fsjobDocumentUploadXML.OuterXml, sAppPath + "\\XSDSchemas\\NoteUpload.xsd");
                        bCheckIfValid = true;

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XSDSCHEMA)", intEventSeq.ToString(), "XSDSchema", DateTime.Now + " - Validating the JobDetails XML against the NoteUpload.xsd", string.Format("<<<XSD NoteDocumentUploadXSD: {0} >>>", sAppPath + "\\XSDSchemas\\NoteDocumentUpload.xsd"), "XSDValid: " + bCheckIfValid.ToString());
                        intEventSeq = (intEventSeq + 1);

                        // If xml is valid process the Note to APD
                        if (bCheckIfValid)
                        {
                            try
                            {
                                //Parse the XML to get the job details
                                oXMLNode = jobDocumentUploadXML.SelectSingleNode("/JobDetails/NoteUpload");
                                //GetXMLSingleNodeValue
                                //GetXMLNodeAttributeValue
                                sLynxID = GetXMLSingleNodeValue(oXMLNode, "LynxID");
                                sUserID = GetXMLSingleNodeValue(oXMLNode, "UserID");
                                sPertainsTo = GetXMLSingleNodeValue(oXMLNode, "PertainsTo");
                                sVehicleNumber = sPertainsTo.Substring(3, (sPertainsTo.Length) - 3);
                                sSuppSeqNumber = GetXMLSingleNodeValue(oXMLNode, "SuppSeqNumber");
                                sDocumentType = GetXMLSingleNodeValue(oXMLNode, "DocumentType");
                                sDocumentSource = GetXMLSingleNodeValue(oXMLNode, "DocumentSource");
                                sNoteType = GetXMLSingleNodeValue(oXMLNode, "NoteType");
                                sStatus = GetXMLSingleNodeValue(oXMLNode, "Status");
                                sNote = GetXMLSingleNodeValue(oXMLNode, "Note");
                                sPrivate = GetXMLSingleNodeValue(oXMLNode, "Private");
                            }
                            catch
                            {
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XMLPARSE_ERROR)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - (ERROR) Parsing the XML Data..", string.Format("<<<VARS: {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10} >>>", sLynxID, sUserID, sPertainsTo, sVehicleNumber, sSuppSeqNumber, sDocumentType, sDocumentSource, sNoteType, sStatus, sNote, sPrivate), "");
                                intEventSeq = (intEventSeq + 1);
                            }

                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XMLPARSE)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Parsing the XML Data..", string.Format("<<<VARS: {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10} >>>", sLynxID, sUserID, sPertainsTo, sVehicleNumber, sSuppSeqNumber, sDocumentType, sDocumentSource, sNoteType, sStatus, sNote, sPrivate), "");
                            intEventSeq = (intEventSeq + 1);

                            XmlNode XMLReturn = null;
                            XMLReturn = GetInscCompIDByLynxID(Int32.Parse(sLynxID), Int32.Parse(sVehicleNumber));

                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(GetInscCompIDByLynxID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - GetInscCompIDByLynxID XML Data..", XMLReturn.OuterXml, "");
                            intEventSeq = (intEventSeq + 1);

                            if (!XMLReturn.OuterXml.Contains("ERROR"))
                            {
                                oXMLNode = XMLReturn.SelectSingleNode("/Claim");
                                sInsuranceCompanyID = GetXMLNodeAttributeValue(oXMLNode, "InsuranceCompanyID");
                                sClaimAspectID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectID");

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(ClaimNode)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - sInsuranceCompanyID: " + sInsuranceCompanyID + ", sClaimAspectID" + sClaimAspectID, "", "");
                                intEventSeq = (intEventSeq + 1);

                                //-- Lookup Andriod UserID --//
                                XMLReturn = GetAndroidUserID();
                                if (!XMLReturn.OuterXml.Contains("ERROR"))
                                {
                                    //oXMLNode = XMLReturn.SelectSingleNode("/Root");
                                    sUserID = GetXMLNodeAttributeValue(XMLReturn, "UserID");

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(ANDROIDUSERID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Getting the Android UserID.", string.Format("<<<Android UserID: {0}, sInsuranceCompanyID: {1}, sClaimAspectID: {2} >>>", sUserID, sInsuranceCompanyID, sClaimAspectID), "");
                                    intEventSeq = (intEventSeq + 1);

                                    XMLReturn = GetClaimDetailsByLynxID(Int32.Parse(sLynxID), Int32.Parse(sInsuranceCompanyID));

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(GetClaimDetailsByLynxID)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - XXXX Getting the claim details by LynxID.", string.Format("<<<XMLReturn: {0} >>>", XMLReturn.OuterXml), "");
                                    intEventSeq = (intEventSeq + 1);

                                    if (!XMLReturn.OuterXml.Contains("ERROR"))
                                    {
                                        oXMLNode = XMLReturn.SelectSingleNode("/Claim");

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(GETPARAMS)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - Getting the additional params.", string.Format("<<<Params: sInsuranceCompanyID: {0}, sClaimAspectID: {1} >>>", sInsuranceCompanyID, sClaimAspectID), "");
                                        intEventSeq = (intEventSeq + 1);

                                        bProcessedNoted = APDInsertNote(Int32.Parse(sClaimAspectID), 6, 500, sNote, Int32.Parse(sUserID), 0);

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(PROCESSED)", intEventSeq.ToString(), "Processing Results", DateTime.Now + " - Processing Results.", string.Format("<<<Results: {0}>>>", bProcessedNoted.ToString()), "");
                                        intEventSeq = (intEventSeq + 1);

                                        // TEST RESULTS
                                        if (bProcessedNoted == false)
                                        {
                                            wcfResults = -1;
                                        }
                                        else
                                        {
                                            wcfResults = 0;
                                        }
                                    }
                                    else
                                    {
                                        throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetClaimDetailsByLynxID(ERROR): ERROR Getting Claim Details - Failed...  " + XMLReturn.OuterXml));
                                    }
                                }
                            }
                            else
                            {
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(ERROR)", intEventSeq.ToString(), "XMLParse", DateTime.Now + " - ERROR GetInscCompIDByLynxID failed to process successfully.", "", "");
                                intEventSeq = (intEventSeq + 1);
                            }


                            //if (XMLReturn.OuterXml.Contains("InsuranceCompany"))
                            //{
                            //    APDFoundation.APDService.ExecuteSpAsXML("uspNoteInsDetail", sParams);
                            //}
                            //else
                            //{

                            //}
                        }

                        // ----------------------------------------
                        //  Logging
                        // ----------------------------------------
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(SQL)", intEventSeq.ToString(), "SQL(uspNoteInsDetail)", DateTime.Now + " - Uploading the Note to the server via SQL(uspNoteInsDetail)...", string.Format("<<<ECADAccessor Response DocumentID: {0} >>>", wcfResults), "");
                        intEventSeq = (intEventSeq + 1);

                        if (wcfResults >= 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                            errInfo.Append("Note failed to upload into server...");
                            RaiseError(errInfo.ToString());
                        }
                    }
                    else
                    {
                        success = false;
                        errInfo.Append("XMLValidationError: No JobDetails XML Found...");
                        RaiseError(errInfo.ToString());
                    }

                    if (!success)
                    {
                        JobFailNotification("Note", errInfo.ToString(), jobDetailsXML);
                    }
                }
                catch (Exception oExcept)
                {
                    // ------------------------------
                    //  Email Notify of the Error
                    // ------------------------------
                    string strError = "";
                    StackFrame FunctionName = new StackFrame();
                    strError = string.Format("Error: {0}", ("NoteUploadJob Failed: NotePerformJob process failed. (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Details: " + strError, "");
                    intEventSeq = (intEventSeq + 1);

                    success = false;
                    return pjr;
                }

                pjr.Method = "NOTE";
                pjr.Success = success;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - NotePerformJob End: Ending the NoteUpload process, NotePerformJob...", "", "");
                intEventSeq = (intEventSeq + 1);

                return pjr;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("WebServices Failed: ProcessJobResult process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                ProcessJobResult pjr = new ProcessJobResult();
                pjr.Method = "NOTE";
                pjr.Success = false;
                return pjr;
            }
            finally
            {
            }
        }

        public Boolean ValidateXML(string strXML, string strSchmaPath)
        {
            bIsValid = true;

            try
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XSDVALIDATIONHANDLER)", intEventSeq.ToString(), "XSDValidationHandler", DateTime.Now + " - Validating XML vs XSD.", string.Format("<<<XMLPath: {0}, XSDPath: {1} >>>", strXML, strSchmaPath), "");
                intEventSeq = (intEventSeq + 1);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add("", strSchmaPath);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler += new ValidationEventHandler(XSDValidatedeventHandler);

                XmlReader rd = XmlReader.Create(strXML, settings);
                while (rd.Read()) ;

                rd.Close();
                return bIsValid;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-ValidateXML: ValidateXML process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                bIsValid = false;
                return bIsValid;
            }
        }

        public void XSDValidatedeventHandler(object sender, ValidationEventArgs e)
        {
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-XSDValidatedeventHandler(XSDERRORHANDLER)", intEventSeq.ToString(), "XSDValidatedeventHandler", DateTime.Now + " - an XSD Error was thrown.", string.Format("<<<XSDError: {0} >>>", e.Message.Replace("'", "''")), "");
            intEventSeq = (intEventSeq + 1);

            try
            {
                if (e.Severity == XmlSeverityType.Warning)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XSDWARNING)", intEventSeq.ToString(), "XSDSchemaValidationWarning", DateTime.Now + " - a Warning was thrown while validating the NoteUpload XML against the NoteUpload.xsd", string.Format("<<<Validation Warning Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                    bIsValid = false;
                }
                else if (e.Severity == XmlSeverityType.Error)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-NotePerformJob(XSDERROR)", intEventSeq.ToString(), "XSDSchemaValidationError", DateTime.Now + " - an Error was thrown while validating the NoteUpload XML against the NoteUpload.xsd", string.Format("<<<Validation Error Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                    bIsValid = false;
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-XSDValidatedeventHandler: ValidateXMLeventHandler process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);
                bIsValid = false;
            }
        }

        public Boolean APDInsertNote(int iClaimAspectID, int iNoteType, int iStatusID, string sNote, int iUserID, int iPrivateFlag)
        {
            string sRC = string.Empty;

            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertNote(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertNote Process Start: Starting the insert of a Note into APD.", "", "Incoming Params: iClaimAspectID=" + iClaimAspectID.ToString() + ", iNoteType = " + iNoteType.ToString() + ", iStatusID = " + iStatusID.ToString() + ", sNote = " + sNote + ", iUserID = " + iUserID.ToString() + ", iPrivateFlag = " + iPrivateFlag.ToString());
            intEventSeq = (intEventSeq + 1);

            try
            {
                sParams = "@ClaimAspectID = " + iClaimAspectID;
                sParams += ", @NoteTypeID = " + iNoteType;
                sParams += ", @StatusID = " + iStatusID;
                sParams += ", @Note = '" + sNote + "'";
                sParams += ", @UserID = " + iUserID;
                sParams += ", @PrivateFlag = " + iPrivateFlag;
                sParams += ", @ClaimAspectServiceChannelID = NULL";    

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertNote(PARAMS)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertNote Processing Params: Insert of a Note into APD using these Params.", "", "uspNoteInsDetail " + sParams);
                intEventSeq = (intEventSeq + 1);

                sRC = wsAPDFoundation.ExecuteSpAsString("uspNoteInsDetail", sParams);

                if (sRC.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-APDInsertNote(ERROR): ERROR APD Insert Note Failed...  " + sRC));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertNote(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - APDInsertNote Process End: Ending the insert of a Note into APD.", "", "");
                intEventSeq = (intEventSeq + 1);

                return true;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-EXECAsString: Insert Note process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                return false;
            }
        }

        public Boolean APDInsertDiary(int iClaimAspectServiceChannelID, int iTaskID, string sAlarmDate, string sUserTaskDescription, string sAssignedUserID, string sAssignmentPoolFunctionCD, bool bSupervisorFlag, int iUserID)
        {
            string sRC = string.Empty;

            try
            {
                sParams = "@ClaimAspectServiceChannelID = " + iClaimAspectServiceChannelID;
                sParams += ", @TaskID = " + iTaskID;
                sParams += ", @AlarmDate = '" + sAlarmDate +"'";
                sParams += ", @UserTaskDescription = '" + sUserTaskDescription + "'";
                sParams += ", @AssignedUserID = " + sAssignedUserID;
                sParams += ", @AssignmentPoolFunctionCD = '" + sAssignmentPoolFunctionCD + "'";
                sParams += ", @SupervisorFlag = " + bSupervisorFlag;
                sParams += ", @UserID = " + iUserID;

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertDiary(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - APDInsertDiary Process Start: Starting the insert of a Note into APD.", "", "uspDiaryInsDetail " + sParams);
                intEventSeq = (intEventSeq + 1);

                sRC = wsAPDFoundation.ExecuteSpAsString("uspDiaryInsDetail", sParams);

                if (sRC.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-APDInsertDiary(ERROR): ERROR APD Insert Diary Failed...  " + sRC));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-APDInsertDiary(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - APDInsertDiary Process End: Ending the insert of a Note into APD.", "", "");
                intEventSeq = (intEventSeq + 1);

                return true;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-EXECAsString: Insert Diary process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                return false;
            }
        }
    }
}
