﻿using System;

using System.Diagnostics;
using System.Xml;
using PGW.Shared.SiteUtilities;
using System.IO;
using System.Text;
using PGW.Shared;
using System.Net;

namespace FNOLPostOffice
{
    class HTTPPostJob : BaseJob 
    {
        private string _defaultSender = "";

        public HTTPPostJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - HTTPPostJob Start: Starting the HTTPPostJob process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetailsXML)
        {
            try
            {
                //-- ****** FROM EMAILJOB ******
                bool success = false;
                //bool ignoreEnvOv = false;
                //StringBuilder errInfo = new StringBuilder();
                //XmlNode emailNode = XmlUtils.GetChildNode(ref jobDetailsXML, string.Format("EMAIL[@stepid='{0}']", _jobStep.ToString()), false);

                //-- LIVE CODE
                ProcessJobResult pjr = new ProcessJobResult();
                XmlNode oXMLNode = null;
                string sIgnoreEnvOverride = string.Empty;
                string sPriority = string.Empty;
                string sStepID = string.Empty;
                string sFileEmbed = string.Empty;
                string sFilePath = string.Empty;
                string sDocumentNodeName = string.Empty;
                string sURL = string.Empty;
                string sPost = string.Empty;
                byte[] bytes;
                HttpWebResponse response;

                //--  Logging
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(PROCESSING)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - HTTPPostJob Processing: Processing the HTTPPostJob process...", "", "");
                intEventSeq = (intEventSeq + 1);

                if (jobDetailsXML != null)
                {
                    //----------------------------------//
                    //-- HTTPPOSTING the Note information
                    //----------------------------------//
                    try
                    {
                        if (jobDetailsXML.InnerXml.Contains("<NoteMsg"))
                        { 
                            //-- Process the NoteMsg 
                            //--  Logging
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(NOTEMSGXML)", intEventSeq.ToString(), "NOTEMSGXML", DateTime.Now + " - HTTPPostJob NOTEMSGXML: Processing the HTTPPostJob NOTEMSGXML...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //-- Get the Node Level details from the XML
                            XmlNode jobBundlingNoteMsgXML = jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/NoteMsg");

                            if (jobBundlingNoteMsgXML.InnerXml != "")
                            {
                                //-- Get HTTPPost Base Attributes
                                sIgnoreEnvOverride = GetXMLNodeAttributeValue(jobBundlingNoteMsgXML, "ignoreenvoverride");
                                sPriority = GetXMLNodeAttributeValue(jobBundlingNoteMsgXML, "priority");

                                //-- Log Event
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(XMLBASEATTRIBUTES)", intEventSeq.ToString(), "XMLBASEATTRIBUTES", DateTime.Now + " - Base Vars", string.Format("<<<Base Vars: IgnoreEnvOverride: {0}, Priority: {1} >>>", sIgnoreEnvOverride, sPriority), "");
                                intEventSeq = (intEventSeq + 1);

                                //-- Get NoteMsg Output Nodes
                                oXMLNode = jobBundlingNoteMsgXML.SelectSingleNode("/JobDetails/Bundling/NoteMsg");
                                sURL = GetXMLSingleNodeValue(oXMLNode, "url");

                                oXMLNode = jobBundlingNoteMsgXML.SelectSingleNode("/JobDetails/Bundling/NoteMsg/PostTemplate");
                                sPost = oXMLNode.InnerXml;

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(XMLNOTEMSGJOBDETAILS)", intEventSeq.ToString(), "XMLNOTEMSGJOBDETAILS", DateTime.Now + " - NoteMsg Job Detail Vars", string.Format("<<<NoteMsg Job Detail Vars: URL: {0}, PostXML: {1} >>>", sURL, sPost), "");
                                intEventSeq = (intEventSeq + 1);

                                //-- Get the details and HTTP Post it to the URL
                                HttpWebRequest NoteMsgReq = (HttpWebRequest)WebRequest.Create(sURL);
                                bytes = System.Text.Encoding.ASCII.GetBytes(sPost);

                                NoteMsgReq.ContentType = "text/xml; encoding='utf-8'";
                                NoteMsgReq.ContentLength = bytes.Length;
                                NoteMsgReq.Method = "POST";

                                Stream NoteMsgReqStream = NoteMsgReq.GetRequestStream();
                                NoteMsgReqStream.Write(bytes, 0, bytes.Length);
                                NoteMsgReqStream.Close();

                                response = (HttpWebResponse)NoteMsgReq.GetResponse();
                                //if (response.StatusCode == HttpStatusCode.OK)
                                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.Accepted)
                                {
                                    Stream NoteMsgResStream = response.GetResponseStream();
                                    string responseStr = new StreamReader(NoteMsgResStream).ReadToEnd();

                                    //-- Log Event
                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(HTTPPOSTSTATUS)", intEventSeq.ToString(), "HTTPPOSTSTATUS", DateTime.Now + " - Posting the XML to the URL and getting response.", string.Format("ResponseCode: {0}, PostStatus: {1}", response.StatusCode, responseStr), "");
                                    intEventSeq = (intEventSeq + 1);

                                    success = true;
                                }
                                else
                                {
                                    success = false;

                                    var message = String.Format("PostOffice-HTTPPostJob(ERROR) NoteMsg Request failed. Received HTTP {0}", response.StatusCode);
                                    throw new ApplicationException(message);
                                }
                            }
                            else
                            {
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(XMLNOTEMSG)", intEventSeq.ToString(), "XMLNOTEMSG", DateTime.Now + " - NO NoteMsg to process", string.Format("NoteMsg XML: {0}", jobBundlingNoteMsgXML.InnerXml), "");
                                intEventSeq = (intEventSeq + 1);
                            }
                        }
                    }
                    catch (Exception oExcept)
                    {
                        // ------------------------------
                        //  Email Notify of the Error
                        // ------------------------------
                        string sError = "";
                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                        sError = string.Format("Error: {0}", ("HTTPPost Job Failed: Failed to Post NoteMsg to URL (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - HTTPPost NoteMsg Job Failed: Failed to Post NoteMsg to URL", sError, "");
                        intEventSeq = (intEventSeq + 1);

                        success = false;
                    }

                    try
                    {
                        //-------------------------------------------//
                        //-- HTTPPOSTING the SENT Upload information
                        //-------------------------------------------//
                        //-- Initialize
                        sIgnoreEnvOverride = string.Empty;
                        sPriority = string.Empty;
                        sURL = string.Empty;
                        sPost = string.Empty;

                        //-- Process the DocUpload 
                        //--  Logging
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(DOCUPLOAD)", intEventSeq.ToString(), "DOCUPLOAD", DateTime.Now + " - HTTPPostJob DOCUPLOAD: Processing the HTTPPostJob DOCUPLOAD...", "", "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Get the Node Level details from the XML
                        XmlNode jobBundlingUploadXML = jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/DocUpload");

                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "<<DEBUG-1>>", intEventSeq.ToString(), "<<DEBUG-1>>", DateTime.Now + " - <<DEBUG-1>>", jobBundlingUploadXML.InnerXml, "");
                        //intEventSeq = (intEventSeq + 1);

                        //-- Get HTTPPost Base Attributes
                        sIgnoreEnvOverride = GetXMLNodeAttributeValue(jobBundlingUploadXML, "ignoreenvoverride");
                        sPriority = GetXMLNodeAttributeValue(jobBundlingUploadXML, "priority");

                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "<<DEBUG-2>>", intEventSeq.ToString(), "<<DEBUG-2>>", DateTime.Now + " - <<DEBUG-2>>", "sIgnoreEnvOverride: " + sIgnoreEnvOverride + ", sPriority: " + sPriority, "");
                        //intEventSeq = (intEventSeq + 1);

                        //-- Log Event
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(XMLBASEATTRIBUTES)", intEventSeq.ToString(), "XMLBASEATTRIBUTES", DateTime.Now + " - Base Vars", string.Format("<<<Base Vars: IgnoreEnvOverride: {0}, Priority: {1} >>>", sIgnoreEnvOverride, sPriority), "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Get NoteMsg Output Nodes
                        oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/DocUpload");
                        sURL = GetXMLSingleNodeValue(oXMLNode, "url");

                        oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/DocUpload/PostTemplate");
                        sPost = oXMLNode.InnerXml;

                        //-- Log Event
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(POSTDETAILS)", intEventSeq.ToString(), "POSTDETAILS", DateTime.Now + " - HTTPPost Details Vars", string.Format("<<<HTTPPost Vars: URL: {0}, Post: {1} >>>", sURL, sPost), "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Get the details and HTTP Post it to the URL
                        HttpWebRequest DocUploadReq = (HttpWebRequest)WebRequest.Create(sURL);
                        bytes = System.Text.Encoding.ASCII.GetBytes(sPost);

                        DocUploadReq.ContentType = "text/xml; encoding='utf-8'";
                        DocUploadReq.ContentLength = bytes.Length;
                        DocUploadReq.Method = "POST";

                        //-- Log Event
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(BASE64)", intEventSeq.ToString(), "BASE64", DateTime.Now + " - Base64 convert of File", string.Format("Base64 Length: {0}", bytes.Length), "");
                        intEventSeq = (intEventSeq + 1);

                        Stream DocUploadReqStream = DocUploadReq.GetRequestStream();
                        DocUploadReqStream.Write(bytes, 0, bytes.Length);
                        DocUploadReqStream.Close();

                        //-- Log Event
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(DOCSTREAMING)", intEventSeq.ToString(), "DOCSTREAMING", DateTime.Now + " - Doc Streaming from File", "", "");
                        intEventSeq = (intEventSeq + 1);

                        response = (HttpWebResponse)DocUploadReq.GetResponse();
                        //if (response.StatusCode == HttpStatusCode.OK)

                        //-- Log Event
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(POSTRESPONSE)", intEventSeq.ToString(), "POSTRESPONSE", DateTime.Now + " - HTTPPost Response", string.Format("<<<Response: {0}, ResDesciption: {1} >>>", response.StatusCode, response.StatusDescription), "");
                        intEventSeq = (intEventSeq + 1);

                        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.Accepted)
                        {
                            Stream responseStream = response.GetResponseStream();
                            string responseStr = new StreamReader(responseStream).ReadToEnd();

                            //-- Log Event
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(HTTPPOSTSTATUS)", intEventSeq.ToString(), "HTTPPOSTSTATUS", DateTime.Now + " - Posting the XML to the URL and getting response.", string.Format("ResponseCode: {0}, PostStatus: {1}", response.StatusCode, responseStr), "");
                            intEventSeq = (intEventSeq + 1);

                            success = true;
                        }
                        else
                        {
                            success = false;

                            var message = String.Format("PostOffice-HTTPPostJob(ERROR) DocUpload Request failed. Received HTTP {0}", response.StatusCode);
                            throw new ApplicationException(message);
                        }
                    }
                    catch (Exception oExcept)
                    {
                        // ------------------------------
                        //  Email Notify of the Error
                        // ------------------------------
                        string sError = "";
                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                        sError = string.Format("Error: {0}", ("HTTPPost Job Failed: Failed to Post to URL (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - HTTPPost Job Failed: Failed to Post to URL", sError, "");
                        intEventSeq = (intEventSeq + 1);

                        success = false;
                    }

                    success = true;
                }

                pjr.Method = "HTTPPOST";
                pjr.Success = success;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-HTTPPostJob(END-HTTPPost)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - HTTPPostPerformJob End: Ending the HTTPPostJob process, HTTPPostPerformJob...", "", "");
                intEventSeq = (intEventSeq + 1);

                return pjr;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("Processing Failed: ProcessJobResult process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                ProcessJobResult pjr = new ProcessJobResult();
                pjr.Method = "Task";
                pjr.Success = false;
                return pjr;
            }
        }
    }
}
