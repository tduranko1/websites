﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading;
using PGW.Shared;
using PGW.Shared.SiteUtilities;


namespace FNOLPostOffice
{
    public class ServiceImplementation : IWindowService
    {
        private Thread _workerThread = null;
        private Thread _faxMonitorThread = null;
        private Thread _responseMonitorThread = null;
        private Thread _referenceSyncThread = null;
        private FNOLPostOfficeSvc _postOffice = null;
        private FaxMonitor _faxMonitor = null;
        private ResponseMonitor _responseMonitor = null;
        private FBITNReferenceSync _referenceSync = null;
        private ManualResetEvent[] _mre = null;
        private EventLog _eventLog = null;

        public ServiceImplementation()
        {
            _mre = new ManualResetEvent[4];
            _eventLog = new EventLog("PGW");
            _eventLog.Source = "FNOLPostOffice";
        }


        #region IWindowService Members

        public void OnStart(string[] args)
        {
            _eventLog.WriteEntry("FNOL Post Office Service Started", EventLogEntryType.Information);

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string configFile = GenUtils.AppendPath(false, path, "po_config.xml");

            if (Environment.UserInteractive && !File.Exists(configFile))
            {
                if (args.Length > 0)
                    configFile = args[0];
            }


            if (!File.Exists(configFile))
            {
                _eventLog.WriteEntry(string.Format("Service stopped.  Configuration file, '{0}', missing/not found.", configFile), EventLogEntryType.Error);
                return;

            }
            else
            {
                _mre[0] = null;
                _mre[1] = null;
                _mre[2] = null;
                _mre[3] = null;
                _postOffice = new FNOLPostOfficeSvc(configFile, _mre[0]);
                _faxMonitor = new FaxMonitor(configFile, _mre[1]);

                //-- TVD - 05Mar2020 - Not used in APD only FNOL --//
                //_responseMonitor = new ResponseMonitor(configFile, _mre[2]);
                //_referenceSync = new FBITNReferenceSync(configFile, _mre[3]);
            }

            if (_workerThread == null)
            {
                ThreadStart ts = null;
                if (_postOffice != null)
                {
                     ts = new ThreadStart(_postOffice.StartProcessing);
                    _workerThread = new Thread(ts);
                    _workerThread.Start();
                }

                if (_faxMonitor != null)
                {
                    ts = new ThreadStart(_faxMonitor.StartProcessing);
                    _faxMonitorThread = new Thread(ts);
                    _faxMonitorThread.Start();
                }

                //-- TVD - 05Mar2020 - Not used in APD only FNOL --//
                //if (_responseMonitor != null)
                //{
                //    ts = new ThreadStart(_responseMonitor.StartProcessing);
                //    _responseMonitorThread = new Thread(ts);
                //    _responseMonitorThread.Start();
                //}

                //if (_referenceSync != null)
                //{
                //    ts = new ThreadStart(_referenceSync.StartProcessing);
                //    _referenceSyncThread = new Thread(ts);
                //    _referenceSyncThread.Start();
                //}
            }
        }

        public void OnStop()
        {
            _eventLog.WriteEntry("Service stop requested.", EventLogEntryType.Information);
            if (_postOffice != null) _postOffice.StopProcessing();
            if (_faxMonitor != null) _faxMonitor.StopProcessing();

            //-- TVD - 05Mar2020 - Not used in APD only FNOL --//
            //if (_responseMonitor != null) _responseMonitor.StopProcessing();
            //if (_referenceSync != null) _referenceSync.StopProcessing();

            if (_workerThread != null) _workerThread.Join(new TimeSpan(0, 2, 0));
            if (_faxMonitorThread != null) _faxMonitorThread.Join(new TimeSpan(0, 2, 0));
            if (_responseMonitorThread != null) _responseMonitorThread.Join(new TimeSpan(0, 2, 0));

            _eventLog.WriteEntry("Service has stopped.", EventLogEntryType.Information);
        }

        public void OnPause()
        {

        }

        public void OnContinue()
        {

        }

        public void OnShutdown()
        {

        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _postOffice = null;
            _mre = null;
            _eventLog = null;
        }

        #endregion
    }
}
