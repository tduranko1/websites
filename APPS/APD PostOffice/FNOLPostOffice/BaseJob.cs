﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

using System.Diagnostics;
using System.IO;

namespace FNOLPostOffice
{
    public class BaseJob
    {
        //---------------------------------------//
        // Global Vars
        //---------------------------------------//
        public APDFoundation.APDService wsAPDFoundation = new APDFoundation.APDService();
        protected ECADAccessor.EcadAccessorClient wsECADAccessor = new ECADAccessor.EcadAccessorClient();

        protected Events _events = null;
        protected string _jobID = "";
        protected int _jobStep = 0;
        protected string _jobFileFolder = "";
        protected string _XSDPath = "";
        protected bool _isProduction = false;

        public string PROCESSINGSERVER = string.Empty;
        public string strEventTransactionID = string.Empty;
        public int intEventSeq = 1;

        public string sStagePath = string.Empty;
        public string sZipStagePath = string.Empty;
        protected string sValidExt = string.Empty;
        protected string sDocProcPickUpPath = string.Empty;
        protected string sDocProcArchivePath = string.Empty;

        public BaseJob(ref Events events, string jobID, int jobStep)
        {
            _events = events;
            _jobID = jobID;
            _jobStep = jobStep;
            _jobFileFolder = GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("WorkPath"), jobID);
            _isProduction = ((_events.cSettings.Environment == "PRD") || (_events.cSettings.Environment == "Production")); ;
            _XSDPath = GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("XSDPath"), "");
            sStagePath = _events.cSettings.GetParsedSetting("StageArea");
            //sZipStagePath = _events.cSettings.GetParsedSetting("ZipStageArea");
            sDocProcPickUpPath = _events.cSettings.GetParsedSetting("DocProcPickUpPath");
            sDocProcArchivePath = _events.cSettings.GetParsedSetting("DocProcArchivePath");

            sValidExt = _events.cSettings.GetParsedSetting("ValidExt");

            //-- Load Global po_config.xml Data
            PROCESSINGSERVER = _events.cSettings.GetParsedSetting("ServerName");
            strEventTransactionID = System.Guid.NewGuid().ToString();
        }

        public XmlNode GetClaimDetailsByLynxID(int iLynxID, int iInsuranceCompanyID)
        {
            string sRC = string.Empty;
            XmlNode XMLReturn = null;
            string sParams = string.Empty;

            try
            {
                sParams = "@LynxID = " + iLynxID;
                sParams += ", @InsuranceCompanyID = " + iInsuranceCompanyID;

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetClaimDetailsByLynxID(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - GetClaimDetailsByLynxID Process Start: Starting the insert of a Note into APD.", "", "uspDiaryInsDetail " + sParams);
                intEventSeq = (intEventSeq + 1);

                XMLReturn = wsAPDFoundation.ExecuteSpAsXML("uspClaimCondGetDetailWSXML", sParams);

                if (XMLReturn.OuterXml.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetClaimDetailsByLynxID(ERROR): ERROR APD Get Details Failed...  " + XMLReturn.OuterXml));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetClaimDetailsByLynxID(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - GetClaimDetailsByLynxID Process End: Ending the insert of a Note into APD.", "", "");
                intEventSeq = (intEventSeq + 1);

                return XMLReturn;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-GetClaimDetailsByLynxID: GetClaimDetailsByLynxID process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<ERROR><Msg>" + strError + "</Msg></ERROR>");

                return doc;
            }
        }

        public XmlNode GetAndroidUserID()
        {
            string sUserID = string.Empty;
            string sParams = string.Empty;
            XmlNode XMLReturn = null;
            //XmlNode XMLReturnNode = null;

            try
            {
                sParams = "@EmailAddress = 'APDAndroidUser@lynxservices.com'";
                XMLReturn = wsAPDFoundation.ExecuteSpAsXML("uspAdmUserGetDetailWSXML", sParams);

                if (XMLReturn.OuterXml.Contains("APDAndroid"))
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetAndroidUserID(ANDROID)", intEventSeq.ToString(), "Android User Exists: ", DateTime.Now + " - Android user exists in APD: Good UserID...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    intEventSeq = (intEventSeq + 1);

                    //XMLReturnNode = XMLReturn.SelectSingleNode("/Root");
                    //sUserID = GetXMLNodeAttributeValue(XMLReturnNode, "UserID");

                    //Int32.Parse(sUserID);
                }
                else
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetAndroidUserID(ANDROID)", intEventSeq.ToString(), "Android User DOES NOT Exists: ", DateTime.Now + " - Android User DOES NOT exists in APD: Bad UserID...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    intEventSeq = (intEventSeq + 1);

                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetAndroidUserID(ANDROID): ERROR Android User DOES NOT Exists...  " + XMLReturn.OuterXml));
                }

                return XMLReturn;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-GetAndroidUserID: Getting the Android UserID process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<ERROR><Msg>" + strError + "</Msg></ERROR>");

                return doc;
            }
        }

        public XmlNode GetInscCompIDByLynxID(int iLynxID, int iVehicleNum)
        {
            string sRC = string.Empty;
            XmlNode XMLReturn = null;

            string sParams = string.Empty;

            try
            {
                sParams = "@LynxID = " + iLynxID;
                sParams += ", @VehicleNumber = " + iVehicleNum;

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetInscCompIDByLynxID(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - GetInscCompIDByLynxID Process Start: Getting the InsuranceCompID by LynxID and VehicleNumber.", "", "uspGetInscCompByLynxIDWSXML " + sParams);
                intEventSeq = (intEventSeq + 1);

                XMLReturn = wsAPDFoundation.ExecuteSpAsXML("uspGetInscCompByLynxIDWSXML", sParams);

                if (XMLReturn.OuterXml.Contains("InsuranceCompanyID"))
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM)", intEventSeq.ToString(), "APD Claim/LynxID Exists: ", DateTime.Now + " - LynxID exists in APD: Good Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    intEventSeq = (intEventSeq + 1);
                }
                else
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM)", intEventSeq.ToString(), "APD Claim/LynxID DOES NOT Exists: ", DateTime.Now + " - LynxID DOES NOT exists in APD: Bad Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    intEventSeq = (intEventSeq + 1);

                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM): ERROR APD Claim/LynxID DOES NOT Exists...  " + XMLReturn.OuterXml));
                }

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-GetInscCompIDByLynxID(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - GetInscCompIDByLynxID Process End: Ending getting the InsuranceCompID by LynxID and VehicleNumber.", "", "");
                intEventSeq = (intEventSeq + 1);

                return XMLReturn;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-GetInscCompIDByLynxID: Getting the InsuranceCompID process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<ERROR><Msg>" + strError + "</Msg></ERROR>");

                return doc;
            }
        }

        public void OverrideNotification(string method, XmlNode jobDetails)
        {
            _events.Trace("***>>>Destination for job overridden due to environment.");
            string smtpServer = _events.cSettings.GetParsedSetting("SMTPServer");

            string subj = string.Format("Destination Override Notification for Job {0}  Step {1}", _jobID,_jobStep.ToString());

            string body = string.Format("The {0} destination for job {1}  Step {2} was overriden due to current environment.\n {3}", method, _jobID, _jobStep.ToString(), jobDetails.OuterXml);

            string recipient = _events.cSettings.GetParsedSetting("OverrideNotification").Replace(";",",");

            if (recipient.Length > 0)
            {
                try
                {
                    SmtpClient sc = new SmtpClient(smtpServer);
                    sc.Send(_events.cSettings.GetParsedSetting("SMTPServer/@defaultsender"),
                            recipient, subj, body);
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, string.Format("Override Notification for job {0}  Step {1}: {2}", _jobID, _jobStep.ToString(), jobDetails.OuterXml), false);
                }
            }

        }

        public void JobFailNotification(string method, string errorInfo, XmlNode jobDetails)
        {
            string smtpServer = _events.cSettings.GetParsedSetting("SMTPServer");

            string clientID = XmlUtils.GetChildNodeText(ref jobDetails, "JobDetail/@clientid").Trim();
            string clientName = XmlUtils.GetChildNodeText(ref jobDetails, "JobDetail/@clientname").Trim();
            string lynxid = XmlUtils.GetChildNodeText(ref jobDetails, "JobDetail/@lynxid").Trim();
            string claimNumber = XmlUtils.GetChildNodeText(ref jobDetails, "JobDetail/@claimnumber");

            StringBuilder bodyheader = new StringBuilder();
            if (!string.IsNullOrEmpty(clientID)) bodyheader.Append(string.Format("Client: {0} - {1}\n", clientID, clientName));
            if (!string.IsNullOrEmpty(lynxid)) bodyheader.Append(string.Format("Lynx ID#: {0}\n", lynxid));
            if (!string.IsNullOrEmpty(claimNumber)) bodyheader.Append(string.Format("Claim #: {0}\n",claimNumber));
            if (bodyheader.Length > 0) bodyheader.Append("\n\n");
                        
            string subject = (string.IsNullOrEmpty(clientName) ? "FNOL Post Office Job Failure Notification" : string.Format("FNOL Post Office Job Failure Notification - {0}", clientName));
            string body = string.Format("{0} The {1} destination for job {2}  Step {3} was failed due with the following error: \n\n {4}", bodyheader.ToString(), method, _jobID, _jobStep.ToString(), errorInfo);
            string recipient = _events.cSettings.GetParsedSetting("EventHandling/Email").Replace(";",",");

            if (recipient.Length > 0)
            {
                try
                {
                    SmtpClient sc = new SmtpClient(smtpServer);
                    sc.Send(_events.cSettings.GetParsedSetting("SMTPServer/@defaultsender"),
                            recipient, subject, body);
                }
                catch (Exception ex)
                {                    
                    _events.HandleEvent(ex, string.Format("The {0} destination for job {1}  Step {2} was failed due with the following error: \n\n {3}", method, _jobID, _jobStep.ToString(), errorInfo), false);
                }
            }
        }

        public virtual ProcessJobResult PerformJob(ref XmlDocument jobDetails) 
        {
            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Success = false;
            return pjr; 
        }

        protected void RaiseError(string errorInformation)
        {
            try
            {
                throw new PostOfficeException(_jobID, _jobStep, errorInformation);
            }
            catch (PostOfficeException ex)
            {
                _events.HandleEvent(ex, false);
            }
        }

        public string LocalLogEvent(string sProcessingServer, string sEventTransactionID, string sEventType, string sEventSeq, string sEventStatus, string sEventDescription, string sEventDetailedDescription, string sEventXml)
        {
            try
            {
                string sReturn = string.Empty;

                //-------------------------------//
                // Database access
                //-------------------------------//  
                string sStoredProcedure = "uspAPDPostOfficeEventInsLogEntry";
                string sParams = "@vProcessingServer = '" + sProcessingServer + "', @vEventTransactionID = '" + sEventTransactionID + "', @vEventType = '" + sEventType + "', @iEventSeq = " + sEventSeq + ", @vEventStatus = '" + sEventStatus + "', @vEventDescription = '" + sEventDescription + "', @vEventDetailedDescription = '" + sEventDetailedDescription + "', @vEventXML = '" + sEventXml + "', @vRecID = ''";

                _events.Trace("<<<BaseJob>>>Logging Local Event: " + DateTime.Now + " - " + sStoredProcedure + " " + sParams);

                // -------------------------------
                //  Call WS and Process Request
                // -------------------------------
                sReturn = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams);

                // -------------------------------------------
                //  Check response for errors
                // -------------------------------------------
                if (sReturn == "0")
                {
                    return sReturn;
                }
                else {
                    throw new System.Exception(string.Format("Error: {0}", "WebSerices Failed: APDPostOffice event logging failed to insert a row (LogEvent)...  " + sReturn));
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                sError = string.Format("Error: {0}", ("WebServices Failed: APDPostOffice event logging failed to insert a row (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                wsAPDFoundation.LogEvent("APDPostOffice", "ERROR", "Error occurred...", sError, "");

                // SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
                return sError;
            }
            finally
            {
            }
        }

        public string GetXMLNodeAttributeValue(XmlNode XMLnode, string strAttributes)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.Attributes[strAttributes] == null))
                    {
                        strRetVal = XMLnode.Attributes[strAttributes].InnerText;
                    }

                }

                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("GetXMLNodeAttrbuteValue Failed: Failed to parse XMLNode (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Error: " + strError, "");
                return strError;
            }
        }

        public string GetXMLSingleNodeValue(XmlNode XMLnode, string strNode)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.SelectSingleNode(strNode) == null))
                    {
                        strRetVal = XMLnode.SelectSingleNode(strNode).InnerText;
                    }

                }

                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("GetXMLSingleNodeValue Failed: Failed to parse XMLNode (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Error: " + strError, "");
                return sBody;
            }
        }

        public void ArchiveMaintenance(string operationCD)
        {
            string archiveFolder = _events.cSettings.GetParsedSetting("ArchivePath");
            string archiveFile = GenUtils.AppendPath(archiveFolder, string.Format("{0}.zip", _jobID));
            System.Collections.Specialized.NameValueCollection p = new System.Collections.Specialized.NameValueCollection();
            FNOLDataAccess da = new FNOLDataAccess(ref _events);
            p.Add("JobID", _jobID);
            switch (operationCD.ToUpper())
            {
                case "A":
                    //Archive was added and an hour passed so clean up work job folder
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(_jobFileFolder);
                    try
                    {
                        if (di.Exists)
                        {
                            di.Delete(true);
                            p.Add("ArchiveStatus", "Y"); // indicate completion of archiving process
                        }
                        else
                        {
                            p.Add("ArchiveStatus", (System.IO.File.Exists(archiveFile) ? "Y" : "X"));
                        }                        
                        da.ExecuteDatabaseTransactionNoReturn("PostOfficeSetArchiveStatus", ref p);
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex,string.Format("Attempted clean up of work folder for job id={0} failed.", _jobID), false);
                    }
                    break;
                case "D": 
                    // The set number of days passed for archive storage --time to remove the archived file
                    System.IO.FileInfo fi = new System.IO.FileInfo(archiveFile);
                    try
                    {
                        if (fi.Exists)
                        {
                            fi.Delete();                           
                        }
                        p.Add("ArchiveStatus", "X"); // indicate archive file has been deleted.
                        da.ExecuteDatabaseTransactionNoReturn("PostOfficeSetArchiveStatus", ref p);
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, string.Format("Attempted clean up of aged archive file for job id={0} failed.", _jobID), false);
                    }
                    break;
                default:
                    break;
            }

        }

        public void ArchiveJobFiles()
        {
            bool archiveCreated = false;
            string archiveFolder = _events.cSettings.GetParsedSetting("ArchivePath");

            string archiveFile = GenUtils.AppendPath(archiveFolder, string.Format("{0}.zip", _jobID));
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(_jobFileFolder);
            if ((di.Exists))
            {
                FastZip fzip = new FastZip();
                try
                {
                    fzip.CreateZip(archiveFile, _jobFileFolder, true, "");
                    archiveCreated = true;
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
                finally
                {
                    fzip = null;
                    GC.Collect(2);
                }
            }


            if (archiveCreated)
            {
                System.Collections.Specialized.NameValueCollection p = new System.Collections.Specialized.NameValueCollection();
                p.Add("JobID", _jobID);
                p.Add("ArchiveStatus", "P");

                FNOLDataAccess da = new FNOLDataAccess(ref _events);
                try
                {
                    da.ExecuteDatabaseTransactionNoReturn("PostOfficeSetArchiveStatus", ref p);
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, string.Format("Unable to set Archive status for Job ID={0} to 'P'", _jobID), false);
                }
            }           
        }

    public class PostOfficeException : Exception
    {
        private string _jobID = "";
        private int _jobStep = 0;
        public PostOfficeException(string jobID, int stepID, string errorInformation)
            : base(errorInformation)
        {
            _jobID = jobID;
            _jobStep = stepID;
        }

        public string JobID { get { return _jobID; } }
        public int JobStep { get { return _jobStep; } }

        public override string ToString()
        {
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append(base.ToString());
            sb.Append(string.Format("\nJob ID={0}\nJob Step={1}\n", _jobID, _jobStep));
            return sb.ToString();
        }
    }

//public ProcessJobResult CleanUpProcess(string sStagePath, string sZIPStagePath)
    //-- CleanUpProcess Stage/PickUp/Archive
    public ProcessJobResult CleanUpProcess(string sStagePath)
    {
            ProcessJobResult pjr = new ProcessJobResult();

            //XmlNode oXMLNode = null;
            //string sOutputFileType = string.Empty;
            //string sOutputPathFileName = string.Empty;
            //string sOutputFileName = string.Empty;
            //string sMergeFlag = string.Empty;
            //string sIgnoreEnvOverride = string.Empty;
            //string sPriority = string.Empty;
            //string sStepID = string.Empty;

            try
            {
                //-- CleanUp Stage Area
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUPPROCESS)", intEventSeq.ToString(), "CleanUp\\Stage", DateTime.Now + " - Cleaning up the Stage area files.", "", "");
                intEventSeq = (intEventSeq + 1);

                //-- CleanUp Stage Area
                System.IO.DirectoryInfo source = new System.IO.DirectoryInfo(sStagePath);
                if (source.Exists)
                {
                    foreach (FileInfo fi in source.EnumerateFiles())
                    {
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUPDELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\Delete", DateTime.Now + " - Deleting file " + string.Format("FileName: {0}", fi.FullName) + " from the Stage Area", "", "");
                        intEventSeq = (intEventSeq + 1);

                        //Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                        try
                        {
                            fi.Delete();

                            //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_STAGE_POST-DELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\Delete", DateTime.Now + " - File Deleted from the Stage Area", string.Format("FileName: {0}", fi.FullName), "");
                            //intEventSeq = (intEventSeq + 1);
                        }
                        catch (Exception ex)
                        {
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(ERROR)", intEventSeq.ToString(), "CleanUp\\Stage\\Failed", DateTime.Now + " - ERROR - CleanUp Delete of Stage Area file " + fi.FullName + " FAILED...", ex.Message, "");
                            intEventSeq = (intEventSeq + 1);

                            pjr.Success = false;
                        }
                    }
                    pjr.Success = true;
                }

                //-- CleanUp ZIP Stage Area
                //System.IO.DirectoryInfo ZipArea = new System.IO.DirectoryInfo(sZIPStagePath);
                //if (ZipArea.Exists)
                //{
                //    foreach (FileInfo fi in ZipArea.EnumerateFiles())
                //    {
                //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUPZIPDELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\ZIP\\Delete", DateTime.Now + " - Deleting file " + string.Format("FileName: {0}", fi.FullName) + " from the Zip Stage Area", "", "");
                //        intEventSeq = (intEventSeq + 1);

                //        //Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                //        try
                //        {
                //            fi.Delete();

                //            //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_STAGE_POST-DELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\Delete", DateTime.Now + " - File Deleted from the Stage Area", string.Format("FileName: {0}", fi.FullName), "");
                //            //intEventSeq = (intEventSeq + 1);
                //        }
                //        catch (Exception ex)
                //        {
                //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(ERROR)", intEventSeq.ToString(), "CleanUp\\Stage\\ZIP\\Failed", DateTime.Now + " - ERROR - CleanUp Delete of Zip Stage Area file " + fi.FullName + " FAILED...", ex.Message, "");
                //            intEventSeq = (intEventSeq + 1);

                //            pjr.Success = false;
                //        }
                //    }

                //    pjr.Success = true;
                //}


                ////-- Get the Bundling tag details so we know where to process from
                ////-- Get the Node Level details from the XML
                //XmlNode jobBundlingUploadXML = jobDetails.SelectSingleNode("JobDetails/Bundling");

                ////-- Get Bundling Attributes
                //oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling");
                //sIgnoreEnvOverride = GetXMLNodeAttributeValue(oXMLNode, "ignoreenvoverride");
                //sPriority = GetXMLNodeAttributeValue(oXMLNode, "priority");
                //sStepID = GetXMLNodeAttributeValue(oXMLNode, "stepid");

                ////-- DEBUG
                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(DEBUG)", intEventSeq.ToString(), "DEBUG CleanUp\\Stage", DateTime.Now + " - DEBUG", "", "");
                //intEventSeq = (intEventSeq + 1);

                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_ATT)", intEventSeq.ToString(), "CleanUp\\Att", DateTime.Now + " - Attributes", string.Format("VARS: IgnoreEnvOverride: {0}, Priority: {1}, StepID: {2}", sIgnoreEnvOverride, sPriority, sStepID), "");
                //intEventSeq = (intEventSeq + 1);

                //oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/Bundle");
                //sOutputFileType = GetXMLNodeAttributeValue(oXMLNode, "type");
                //sOutputPathFileName = sStagePath + "\\" + GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                //sOutputFileName = GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                //sMergeFlag = GetXMLNodeAttributeValue(oXMLNode, "merge");

                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_BUNDLE_ATT)", intEventSeq.ToString(), "CleanUp\\Bundle\\Att", DateTime.Now + " - Attributes", string.Format("VARS: OutputFileType: {0}, OutputPathFileName: {1}, OutputFileName: {2}, MergeFlag: {3}", sOutputFileType, sOutputPathFileName, sOutputFileName, sMergeFlag), "");
                //intEventSeq = (intEventSeq + 1);

                //System.IO.DirectoryInfo source = new System.IO.DirectoryInfo(sStagePath);
                //if (source.Exists)
                //{
                //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_STAGE_AREA)", intEventSeq.ToString(), "CleanUp\\Stage\\Area", DateTime.Now + " - Cleaning Up the Stage Area", string.Format("File Count: {0}", source.EnumerateFiles().Count()), "");
                //    intEventSeq = (intEventSeq + 1);

                //    foreach (FileInfo fi in source.EnumerateFiles())
                //    {
                //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_STAGE_PRE-DELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\Delete", DateTime.Now + " - Deleting files from the Stage Area", string.Format("FileName: {0}", fi.FullName), "");
                //        intEventSeq = (intEventSeq + 1);

                //        //Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                //        try
                //        {
                //            fi.Delete();

                //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(CLEANUP_STAGE_POST-DELETE)", intEventSeq.ToString(), "CleanUp\\Stage\\Delete", DateTime.Now + " - File Deleted from the Stage Area", string.Format("FileName: {0}", fi.FullName), "");
                //            intEventSeq = (intEventSeq + 1);
                //        }
                //        catch (Exception ex)
                //        {
                //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(ERROR)", intEventSeq.ToString(), "CleanUp\\Stage\\Failed", DateTime.Now + " - ERROR - CleanUp Delete of Stage Area file " + fi.FullName  + " FAILED...", ex.Message, "");
                //            intEventSeq = (intEventSeq + 1);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(ERROR)", intEventSeq.ToString(), "CleanUp\\Stage\\Failed", DateTime.Now + " - ERROR - CleanUp of Stage Area FAILED...",ex.Message, "");
                intEventSeq = (intEventSeq + 1);

                pjr.Success = false;
            }


            //-- Create Archive ZIP

            //-- CleanUp Doc Proc Pickup 

            return pjr;
    }

        //------------------------------
        // Base64 Encode the file data
        //------------------------------
        public string GetBinData(String fileName)
        {
            string theBinLength = string.Empty;
            byte[] buff = null;
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    long numBytes = new FileInfo(fileName).Length;
                    theBinLength = numBytes.ToString();

                    buff = br.ReadBytes((int)numBytes);

                    br.Close();
                    br.Dispose();
                    fs.Close();
                    fs.Dispose();
                }
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BaseJob(ERROR)", intEventSeq.ToString(), "GetBinData Failed", DateTime.Now + " - ERROR - GetBinData FAILED...", oExcept.Message, "");
                intEventSeq = (intEventSeq + 1);
            }

            return Convert.ToBase64String(buff).ToString();
        }

    }
}
