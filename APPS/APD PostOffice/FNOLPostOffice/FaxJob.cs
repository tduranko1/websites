﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice
{
    class FaxJob : BaseJob
    {
        private List<string> _faxServers = new List<string>();
        private string _clientName = "";
        private string _lynxID = "";
        private string _claimNumber = "";

        public FaxJob(ref Events events, string jobID, int jobStep, string clientName, string lynxID, string claimNumber)
            : base(ref events, jobID, jobStep)
        {
            _clientName = clientName;
            _lynxID = lynxID;
            _claimNumber = claimNumber;
            GetFaxServerList();
        }


        public override ProcessJobResult PerformJob(ref XmlDocument jobDetails)
        {
            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Success = true;
            bool ignoreEnvOv = false;
            XmlNode faxNode = null;
            string errorInfo = "";
            RFCOMAPILib.FaxServerClass fsc = null;

            if (_faxServers.Count == 0)
            {
                errorInfo = "No fax servers are enabled.";
                pjr.Success = false;
                RaiseError(errorInfo);
                JobFailNotification("FAX", errorInfo, jobDetails);
            }
            if (pjr.Success)
            {
                faxNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("FAX[@stepid='{0}']", _jobStep.ToString()), false);
                pjr.Success = (faxNode != null);
                if (!pjr.Success)
                {
                    errorInfo = "No FAX node defined for job step in job details.";
                    RaiseError(errorInfo);
                    JobFailNotification("FAX", errorInfo, jobDetails);
                }
            }

            if (pjr.Success)
            {
                ignoreEnvOv = FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref faxNode, "@ignoreenvoverride"));

                if ((_isProduction) || (ignoreEnvOv))
                {
                    fsc = OpenFaxServer();
                    pjr.Success = (fsc != null);
                    if (pjr.Success)
                    {
                        pjr = SubmitFaxJobToFaxServer(ref faxNode, ref fsc, ref jobDetails);
                    }
                    else
                    {
                        errorInfo = "Unable to open connection to any fax server.";
                        RaiseError(errorInfo);
                        JobFailNotification("FAX", errorInfo, jobDetails);
                    }
                }
                else
                {
                    OverrideNotification("FAX", jobDetails);
                    pjr.Success = true;
                }


            }
            return pjr;

        }

        private ProcessJobResult SubmitFaxJobToFaxServer(ref XmlNode faxNode, ref RFCOMAPILib.FaxServerClass fsc, ref XmlDocument jobDetails)
        {
            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Success = true;
            RFCOMAPILib.Fax fax = null;
            try
            {
                string senderName = _events.cSettings.GetParsedSetting("FAX/@sendername");
                string faxNumber = faxNode.InnerText;
                string faxDesc = string.Format("{0}{1}{2}", _clientName,
                    ((_lynxID.Trim().Length > 0) ? string.Format("-L#{0}", _lynxID) : ""),
                    ((_claimNumber.Trim().Length > 0) ? string.Format("-C#{0}", _claimNumber) : ""));

                _events.Trace(string.Format("sender: {0} fax: {1} fax desc {2}", senderName, faxNode, faxDesc));

                fax = (RFCOMAPILib.Fax)fsc.get_CreateObject(RFCOMAPILib.CreateObjectType.coFax);
                fax.FromName = senderName;
                if (!faxNumber.StartsWith("1")) faxNumber = GenUtils.Append("1", faxNumber);
                fax.ToFaxNumber = faxNumber;
                fax.ToName = faxDesc;
                fax.UniqueID = _jobID;

                XmlNode fileNode = null;
                string attachmentUrl = "";
                IEnumerator fptr = XmlUtils.GetChildNodeList(ref jobDetails, "FileList/File").GetEnumerator();
                while (fptr.MoveNext())
                {
                    fileNode = (XmlNode)fptr.Current;
                    attachmentUrl = XmlUtils.GetChildNodeText(ref fileNode, "@source");
                    fax.Attachments.Add(attachmentUrl);
                }

                fax.Send();
                pjr.Success = true;
                pjr.FaxHandle = fax.Handle;
                pjr.FaxServer = fsc.ServerName;

                _events.Trace(string.Format("sender: {0} fax: {1} fax desc: {2} status: {3}", senderName, faxNode, faxDesc, true));
            }
            catch (Exception ex)
            {
                ex.ToString();
                JobFailNotification("Fax", ex.ToString(), jobDetails);
                _events.HandleEvent(ex, false);
                pjr.Success = false;

                _events.Trace(string.Format("sending fax error: {0} jobDetails: {1}", ex.Message, jobDetails));
            }
            finally
            {
                fax = null;
                GC.Collect(2);
            }

            return pjr;
        }


        private RFCOMAPILib.FaxServerClass OpenFaxServer()
        {
            bool success = false;
            int faxPtr = 0;
            RFCOMAPILib.FaxServerClass fsc = null;

            while ((!success) && (faxPtr < _faxServers.Count))
            {
                try
                {
                    _events.Trace(string.Format("attempt to connect to fax server {0} ", _faxServers[faxPtr]));

                    fsc = new RFCOMAPILib.FaxServerClass();
                    fsc.ServerName = _faxServers[faxPtr];
                    fsc.Protocol = RFCOMAPILib.CommunicationProtocolType.cpSecTCPIP;
                    fsc.AuthorizationUserID = "APDPOSTOFFICEFX";
                    fsc.AuthorizationUserPassword = "Lynx2020!";
                    fsc.UseNTAuthentication = RFCOMAPILib.BoolType.False;

                    fsc.OpenServer();
                    success = true;
                }
                catch (Exception ex)
                {
                    _events.Trace(string.Format("attempt to connect to fax server {0} failed: {1}", _faxServers[faxPtr], ex.Message));
                    faxPtr += 1;
                }
            }
            if (!success)
                fsc = null;

            return fsc;
        }

        private void GetFaxServerList()
        {
            string response = string.Empty;
            FNOLDataAccess da = null;

            try
            {
                da = new FNOLDataAccess(ref _events);
                response = da.ExecuteDatabaseTransactionXml2("RetrieveFaxServerListXML");

                _events.Trace(string.Format("FaxServerList: {0} ", response.ToString()));

                if (!string.IsNullOrEmpty(response))
                {
                    XmlDocument fax = new XmlDocument();
                    fax.LoadXml(response);

                    XmlNode faxServersNode = fax.DocumentElement;

                    if (XmlUtils.GetChildNodeText(ref faxServersNode, "@status").ToLower() == "ok")
                    {
                        foreach (XmlNode faxServer in XmlUtils.GetChildNodeList(ref faxServersNode, "Server[@preferenceOrder!='0']"))
                        {
                            XmlNode temp = faxServer;
                            _faxServers.Add(XmlUtils.GetChildNodeText(ref temp, "@name"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                da = null;
            }
        }
    }
}
