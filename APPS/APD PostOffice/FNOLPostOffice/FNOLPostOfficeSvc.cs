﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.SiteUtilities; 

//----------------------------------------------------------------//
// PROCEDURE:    APDPostOffice
// SYSTEM:       Lynx Services APD
// AUTHOR:       Thomas Duranko
// FUNCTION:     This service is a DB Queing style 
//               service that processes Email, FTP, Faxing, Uploading
//               Bundling
//               
// UPDATES:		23Jul2019 - TVD - Initial Port over from FNOL
//  			23Jul2019 - TVD - Adding NOL Functions
//              02Sep2020 - TVD - Adding DB Logging
//
// VERSION:     23Oct2020 - TVD - V1.0 - Port of FNOL PostOffice to APD
//
//----------------------------------------------------------------//
namespace FNOLPostOffice
{
    public class FNOLPostOfficeSvc
    {      
        private Events _events = null;
        private bool _serviceRunning = false;
        private FNOLDataAccess _dataAccess = null;
        private ManualResetEvent _mre = null;
        private int _archiveMaintenanceTimer = 60;
        private string _configFile = "";
        private int _dom = 0; 

        private string PROCESSINGSERVER = string.Empty;
        private string strEventTransactionID = string.Empty;
        private Int32 intEventSeq = 1;


        public FNOLPostOfficeSvc(string config, ManualResetEvent mre)
        {
            _dom = DateTime.Now.Day;
            _configFile = config;           
            _mre = mre;
            InitializeEventsObject();
            if (!int.TryParse(_events.cSettings.GetParsedSetting("ArchiveMaintenanceTimer"), out _archiveMaintenanceTimer))
                _archiveMaintenanceTimer = 60;

            PROCESSINGSERVER = _events.cSettings.GetParsedSetting("ServerName");
            strEventTransactionID = System.Guid.NewGuid().ToString();
        }

        private void InitializeEventsObject()
        {
            _events = new Events();
            _events.ComponentInstance = "PostOffice";
            _events.Initialize(_configFile, true);
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);
        }

        public void StopProcessing()
        {
            _events.Trace("Received Stop Signal..");
            _serviceRunning = false;
        }

        public void StartProcessing()
        {
            //-- TVD Adding Logging to this code //
            _events.Trace("<<<TIMER>>>APD Post office job started: " + DateTime.Now);

            DateTime lastCleanUpTimeStamp = DateTime.Parse("1/1/2000");
            DataTable dt = null;
            int cdom = 0;
           
            _serviceRunning = true;

            while (_serviceRunning)
            {
                cdom = DateTime.Now.Day;
                if (cdom != _dom)
                {
                    // Day has changed restart logs
                    _events.DumpLogBufferToDisk();
                    _dataAccess = null;
                    _events = null;
                    GC.Collect(2);
                    InitializeEventsObject();
                    _dom = cdom;
                }

                if ((DateTime.Now - lastCleanUpTimeStamp).Minutes > _archiveMaintenanceTimer)
                {
                    dt = GetArchiveMaintenanceTasks();
                    PerformArchiveMaintenance(ref dt);

                    dt = null;
                    lastCleanUpTimeStamp = DateTime.Now;

                    //-- CleanUp the Stage Area
                    BaseJob oBase = new BaseJob(ref _events, "0", 0);
                    oBase.CleanUpProcess(oBase.sStagePath);
                }

                dt = GetNextJob();
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        //-- Debug Code -->
                        _events.Trace("<<<TIMER>>>Jobs waiting to be processed: " + DateTime.Now + " - " + dt.Rows.Count);

                        ProcessJob(ref dt);
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, false);
                    }
                    _events.DumpLogBufferToDisk();
                }
                else
                {
                    //-- Debug Code -->
                    _events.Trace("<<<TIMER>>>NO JOBS to Process... Sleep(1000): " + DateTime.Now);

                    // No jobs so need to wait a few moments before checking again
                    //_events.Trace("No jobs currently.. taking a 10 second smoke break...be back soon...");
                    Thread.Sleep(10000);
                }
               
            }
            if (!(_events == null))
            {
                _events.DumpLogBufferToDisk();             
            }
            _events = null;            
            //signal that we're done processing any jobs.
            if (_mre != null) _mre.Set();
        }

        private DataTable GetArchiveMaintenanceTasks()
        {
            DataTable dt = null;
            try
            {
                NameValueCollection p = new NameValueCollection();
                p.Add("CallingServer", Environment.MachineName.ToUpper());
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("PostOfficeArchiveMaintenanceList", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = new DataTable();
            }

            return dt;
        }

        private DataTable GetNextJob()
        {
            DataTable dt = null;
            try
            {
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("PostOfficeGetNextJob");
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = new DataTable();
            }

            return dt;
        }

        private void PerformArchiveMaintenance(ref DataTable dt)
        {
            DataRow dr = null;
            string jobID = "";
            string operationCD = "";
            BaseJob bj = null;
            System.Collections.IEnumerator ptr = dt.Rows.GetEnumerator();
            while (ptr.MoveNext())
            {
                dr = (DataRow)ptr.Current;
                jobID = Convert.ToString(dr["JobID"]);
                operationCD = Convert.ToString(dr["Operation"]);

                bj = new BaseJob(ref _events, jobID, 0);
                try
                {
                    bj.ArchiveMaintenance(operationCD);
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, string.Format("Job ID = {0}", jobID), false);
                }                
            }

            dt = null;
            bj = null;

        }


        private void ProcessJob(ref DataTable dt)
        {
            //-- Debug Code -->
            _events.Trace("<<<PROCESSING>>>Processing Started: " + DateTime.Now);

            ProcessJobResult pjr = null;
            string jobID = "";
            int jobStep = 0;
            string clientName = "";
            string lynxID = "";
            string claimNumber = "";
            bool bRC = false;
            int failCount = 0;
            //bool success = false;
            XmlDocument jobDetails = null;
            string jobMethod = "";
            int priority = 5;
            DataRow dr = null;
            System.Collections.IEnumerator ptr = dt.Rows.GetEnumerator();

            // A job can contain several sub jobs 
            while (ptr.MoveNext() && _serviceRunning)
            {
                dr = (DataRow)ptr.Current;
                jobID = Convert.ToString(dr["JobID"]);
                jobStep = Convert.ToInt32(dr["StepID"]);
                clientName = Convert.ToString(dr["ClientName"]);
                lynxID = Convert.ToString(dr["LynxID"]);
                claimNumber = Convert.ToString(dr["ClaimNumber"]);
                jobDetails = ExtractJobDetails(ref dr);
                jobMethod = Convert.ToString(dr["Method"]);
                priority = Convert.ToInt32(dr["JobPriority"]);
                XmlUtils.AddElement(ref jobDetails, "Priority").InnerText = priority.ToString();

                BaseJob oBase = new BaseJob(ref _events, jobID, jobStep);

                if (_serviceRunning)
                {
                    // ----------------------------------------
                    //  Logging
                    // ----------------------------------------
                    //oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(STARTED)", intEventSeq.ToString(), "JOB STARTING", DateTime.Now + " - Job Start: Starting the " + jobMethod + " Job process...", "", "");
                    intEventSeq = (intEventSeq + 1);

                    switch (jobMethod)
                    {
                        case "FTP":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing FTP Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<FTP>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing FTP Job: ", ">>>>" + DateTime.Now + " - FTP Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            FTPJob ftpJob = new FTPJob(ref _events, jobID, jobStep);
                            pjr = ftpJob.PerformJob(ref jobDetails);
                            ftpJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "EMAIL":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing EMAIL Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<EMAIL>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing EMAIL Job: ", ">>>>" + DateTime.Now + " - EMAIL Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            EmailJob emailJob = new EmailJob(ref _events, jobID, jobStep);
                            pjr = emailJob.PerformJob(ref jobDetails);
                            emailJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "EML":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing EML Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<EML>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing EML Job: ", ">>>>" + DateTime.Now + " - EML Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            EmailJob emlJob = new EmailJob(ref _events, jobID, jobStep);
                            pjr = emlJob.PerformJob(ref jobDetails);
                            emailJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "FAX":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing FAX Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<FAX>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing FAX Job: ", ">>>>" + DateTime.Now + " - FAX Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            FaxJob faxJob = new FaxJob(ref _events, jobID, jobStep, clientName, lynxID, claimNumber);
                            pjr = faxJob.PerformJob(ref jobDetails);
                            faxJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "WS":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing WS Job (Not Used): " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<WS>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing WS Job: ", ">>>>" + DateTime.Now + " - WS Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "UPL":
                            //-- Logging - 16Aug2019 Adding code to upload documents --//
                            _events.Trace("<<<JOB-METHOD>>>Processing Document Upload Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<UPL>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing UPL Job: ", ">>>>" + DateTime.Now + " - UPL Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //-- 16Aug2019 Adding code to upload documents
                            DocUploadJob docUpload = new DocUploadJob(ref _events, jobID, jobStep);
                            pjr = docUpload.PerformJob(ref jobDetails);
                            emailJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "NOTE":
                            //-- Logging - 20Jan2020 Adding code to upload a Note into APD Notes --//
                            _events.Trace("<<<JOB-METHOD>>>Processing Note Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<NOTE>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing NOTE Job: ", ">>>>" + DateTime.Now + " - NOTE Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //-- 16Aug2019 Adding code to add APD Note document
                            NoteUploadJob NoteUpload = new NoteUploadJob(ref _events, jobID, jobStep);
                            _events.Trace("<<<JOB-METHOD>>>instantiating Note Job: " + DateTime.Now + " JobDetails: " + jobDetails.InnerXml);
                            pjr = NoteUpload.PerformJob(ref jobDetails);
                            _events.Trace("<<<JOB-METHOD>>>Note Job Processing Done: " + DateTime.Now);
                            emailJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "TASK":
                            _events.Trace("<<<JOB-METHOD>>>Processing Task Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<TASK>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing TASK Job: ", ">>>>" + DateTime.Now + " - TASK Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //-- 16Aug2019 Adding code to add APD Task document
                            TaskUploadJob taskUpload = new TaskUploadJob(ref _events, jobID, jobStep);
                            _events.Trace("<<<JOB-METHOD>>>instantiating Task Job: " + DateTime.Now + " JobDetails: " + jobDetails.InnerXml);
                            pjr = taskUpload.PerformJob(ref jobDetails);
                            _events.Trace("<<<JOB-METHOD>>>Task Job Processing Done: " + DateTime.Now);
                            emailJob = null;

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "POST":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing POST Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<POST>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing POST Job: ", ">>>>" + DateTime.Now + " - POST Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            WebPostJob webPost = new WebPostJob(ref _events, jobID, jobStep, clientName);
                            pjr = webPost.PerformJob(ref jobDetails);

                            //After each job is run Archive work folder then clean up
                            //CleanUp(failCount, oBase, oBase.sStagePath);

                            break;
                        case "BUND":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing Bundling Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<BUND>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing BUND Job: ", ">>>>" + DateTime.Now + " - BUND Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            BundlingJob bundlingJob = new BundlingJob(ref _events, jobID, jobStep);
                            pjr = bundlingJob.PerformJob(ref jobDetails);
                            bundlingJob = null;

                            //-- If Successful Bundle, Sent HTTPPost NoteMsg and Document Upload
                            if (pjr.Success) 
                            {
                                //-- Debug Code --//
                                _events.Trace("<<<JOB-METHOD>>>Processing HTTPPost Job: " + DateTime.Now);

                                // ----------------------------------------
                                //  Logging
                                // ----------------------------------------
                                oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<HTTPPOST>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing HTTPPost Job: ", ">>>>" + DateTime.Now + " - HTTPPost Job Processing...", "", "");
                                intEventSeq = (intEventSeq + 1);

                                HTTPPostJob HTTPPostJob = new HTTPPostJob(ref _events, jobID, jobStep);
                                pjr = HTTPPostJob.PerformJob(ref jobDetails);
                                HTTPPostJob = null;
                            }

                            break;
                        case "HTTP":
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Processing HTTPPost Job: " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<HTTPPOST>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing HTTPPost Job: ", ">>>>" + DateTime.Now + " - HTTPPost Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //HTTPPostJob HTTPPostJob = new HTTPPostJob(ref _events, jobID, jobStep);
                            //pjr = HTTPPostJob.PerformJob(ref jobDetails);
                            //HTTPPostJob = null;

                            break;
                        default:
                            //-- Debug Code --//
                            _events.Trace("<<<JOB-METHOD>>>Unknown Job: " + jobMethod + " " + DateTime.Now);

                            // ----------------------------------------
                            //  Logging
                            // ----------------------------------------
                            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(<<<DEFAULT>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing DEFAULT Job: ", ">>>>" + DateTime.Now + " - DEFAULT Job Processing...", "", "");
                            intEventSeq = (intEventSeq + 1);

                            break;
                    }

                    if (pjr.Success) 
                    {
                        //-- Debug Code --//
                        _events.Trace("<<<JOBSUCCESS>>>Process Successfully: " + DateTime.Now);

                        // ----------------------------------------
                        //  Logging
                        // ----------------------------------------
                        oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(JOBSUCCESS)", intEventSeq.ToString(), "JOB PROCESSED SUCCESSFULLY", ">>>>" + DateTime.Now + " - Job Processed: Successfully processed the " + jobMethod + " Job process successfully...", "", "");
                        intEventSeq = (intEventSeq + 1);

                        MarkJobSent(jobID, jobStep, pjr);
                    }
                    else
                    {
                        //-- Debug Code --//
                        _events.Trace("<<<JOBSUCCESS>>>ERROR: Process Failed: " + DateTime.Now);

                        // ----------------------------------------
                        //  Logging
                        // ----------------------------------------
                        oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(JOBFAILED)", intEventSeq.ToString(), "JOB PROCESS FAILED", ">>>>" + DateTime.Now + " - Job Failed: Failed processing the " + jobMethod + " Job...", "" , "");
                        intEventSeq = (intEventSeq + 1);

                        MarkJobIssue(jobID, jobStep);                        
                        failCount += 1;                      
                    }
                }
                else
                {
                    //service was stopped.  Need to return remaining items to queue before actually stopping
                    PlaceJobBackOnQueue(jobID, jobStep);
                }

                //-- Debug Code -->
                _events.Trace("<<<PROCESSING>>>Processing Completed: " + DateTime.Now);

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(ENDED)", intEventSeq.ToString(), "JOB ENDING", DateTime.Now + " - Job End: Ending the " + jobMethod + " Job process...", "", "");
                intEventSeq = (intEventSeq + 1);

            }

            //After all jobs are run Archive work folder then clean up
            if (failCount == 0)
            {
                //-- Verify Successful, if so archive the bundling pickup docs.
                _events.Trace("<<<JOB-METHOD-BUNDLING>>>ArchiveAndCleanUpProcess Job: " + DateTime.Now);

                BaseJob oBase = new BaseJob(ref _events, jobID, 0);
                oBase.ArchiveJobFiles();

                //-- CleanUp the Stage Area
                //bj.CleanUpProcess(ref jobDetails);

                //After each job is run Archive work folder then clean up
                //CleanUp(failCount, oBase, oBase.sStagePath);
            }
        }

       

        private void PlaceJobBackOnQueue(string jobID, int jobStep)
        {
            NameValueCollection pms = new NameValueCollection();
            pms.Add("JobID", jobID);
            pms.Add("StepID", jobStep.ToString());

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeReturnToQueue", ref pms);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

        }

        private void MarkJobSent(string jobID, int jobStep, ProcessJobResult pjr)
        {
            NameValueCollection pms = new NameValueCollection();
            pms.Add("JobID", jobID);
            pms.Add("StepID", jobStep.ToString());
            if (pjr.FaxHandle > 0)
            {
                pms.Add("FaxHandle", pjr.FaxHandle.ToString());
                pms.Add("FaxServer", pjr.FaxServer);
                pms.Add("EnvOverride", (pjr.EnvironmentOverride ? "1" : "0"));
            }

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeMarkSent", ref pms);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

            
        }
        private void MarkJobIssue(string jobID, int jobStep)
        {
            NameValueCollection pms = new NameValueCollection();
            pms.Add("JobID", jobID);
            pms.Add("StepID", jobStep.ToString());

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeMarkIssue", ref pms);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }


        }


        private XmlDocument ExtractJobDetails(ref DataRow dr)
        {            
            XmlDocument jobDetails = new XmlDocument();
            try
            {
                jobDetails.LoadXml(Convert.ToString(dr["JobDetails"]));
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                jobDetails = null;
            }

            return jobDetails;
        }

//        private void CleanUp(int failCount, BaseJob oBase, string sStagePath, string sZIPStagePath)
        private void CleanUp(int failCount, BaseJob oBase, string sStagePath)
        {
            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(CLEANUP)", intEventSeq.ToString(), "Cleaning Up", DateTime.Now + " - Cleaning up processing...", "Any Failures: " + failCount, "");
            intEventSeq = (intEventSeq + 1);

            if (failCount == 0)
            {
                //-- Verify Successful, if so archive the bundling pickup docs.
                _events.Trace("<<<JOB-METHOD-BUNDLING>>>CleanUpProcess Job: " + DateTime.Now);

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                oBase.LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-ProcessJob(CLEANUP)", intEventSeq.ToString(), "Cleaning up", DateTime.Now + " - Cleaning up processing...", "", "");
                intEventSeq = (intEventSeq + 1);

                //BaseJob bj = new BaseJob(ref _events, jobID, 0);
                oBase.ArchiveJobFiles();

                //-- CleanUp the Stage Area
                //oBase.CleanUpProcess(sStagePath);
            }
        }
    }

    public class ProcessJobResult
    {
        internal ProcessJobResult() { FaxHandle = 0; EnvironmentOverride = false; }
        public string Method;
        public bool Success;
        public int FaxHandle;
        public string FaxServer;
        public bool EnvironmentOverride;
    }


}
