﻿using System;
using System.Collections.Generic;

using System.Diagnostics;
using System.Xml;
using PGW.Shared.SiteUtilities;
using System.Collections;
using Syncfusion.Pdf.Parsing;
using Syncfusion.Pdf;
using Syncfusion.Compression.Zip;
using Syncfusion.Compression;
using System.IO;
using System.Text;
using PGW.Shared;
using System.Xml.Xsl;
using System.Configuration;

namespace FNOLPostOffice
{
    class BundlingJob : BasePDFFunctions
    {
        private string _defaultSender = "";

        public BundlingJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - BundlingJob Start: Starting the BundlingJob process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetailsXML)
        {
            AppSettingsReader appSettingsReader = new AppSettingsReader();

            try
            {
                //-- ****** FROM EMAILJOB ******
                bool success = false;
                bool ignoreEnvOv = false;
                StringBuilder errInfo = new StringBuilder();
                //XmlNode emailNode = XmlUtils.GetChildNode(ref jobDetailsXML, string.Format("EMAIL[@stepid='{0}']", _jobStep.ToString()), false);

                //-- LIVE CODE
                ProcessJobResult pjr = new ProcessJobResult();
                XmlNode oXMLNode = null;
                //-- Process multiple nodes so that all get processed --//
                XmlNodeList objDetailsNodeList;
                string sIgnoreEnvOverride = string.Empty;
                string sPriority = string.Empty;
                string sStepID = string.Empty;
                string sOutputFileType = string.Empty;
                string sOutputFileName = string.Empty;
                string sMergeFlag = string.Empty;
                string sUNCTarget = string.Empty;
                string sRC = string.Empty;
                int iPageIndex = 0;
                string sElecMerge = string.Empty;
                string sOutputFileNameOnly = string.Empty;
                string sDocProcOutputFileName = string.Empty;
                string sPickupPath = string.Empty;
                string sHTMLOutputFilePath = string.Empty;

                //-- File Processing Vars
                string sType = string.Empty;
                string sSource = string.Empty;
                string sFileName = string.Empty;
                string sPathFile = string.Empty;
                string sXSLTFileName = string.Empty;
                string sOutputPathFileName = string.Empty;
                bool bElectRC = false;
                bool bSentFolderCompressedRC = false;
                bool bXSLTConvertRC = false;

                //-- Class file Definition
                string sPrimary = string.Empty;
                string sMerge = string.Empty;
                string sNoConvert = string.Empty;

                string sOutputBundleFileName = string.Empty;

                Int32 iFileCount = 0;
                ArrayList aFileNameList = new ArrayList();
                ArrayList aOutputFileNameList = new ArrayList();
                ArrayList aSourceFileNameList = new ArrayList();
                ArrayList aOriginalTypeList = new ArrayList();
                ArrayList aXSLTFileNameList = new ArrayList();

                PdfDocument NewPDFDocument = new PdfDocument();
                List<Class.JobFileDetails> liJobFileData = new List<Class.JobFileDetails>();

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(PROCESSING)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - BundlingJob Processing: Processing the BundlingJob process...", "", "");
                intEventSeq = (intEventSeq + 1);

                if (jobDetailsXML != null)
                {
                    //-- Get the Node Level details from the XML
                    XmlNode jobBundlingUploadXML = jobDetailsXML.SelectSingleNode("JobDetails/Bundling");

                    //-- Get Bundling Attributes
                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling");
                    sIgnoreEnvOverride = GetXMLNodeAttributeValue(oXMLNode, "ignoreenvoverride");
                    sPriority = GetXMLNodeAttributeValue(oXMLNode, "priority");
                    sStepID = GetXMLNodeAttributeValue(oXMLNode, "stepid");

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XMLBASEATTRIBUTES)", intEventSeq.ToString(), "XMLBASEATTRIBUTES", DateTime.Now + " - Base Vars", string.Format("<<<Base Vars: IgnoreEnvOverride: {0}, Priority: {1}, StepID: {2}  >>>", sIgnoreEnvOverride, sPriority, sStepID), "");
                    intEventSeq = (intEventSeq + 1);

                    //-- Get Bundling Output Attributes
                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/Bundle");
                    sOutputFileType = GetXMLNodeAttributeValue(oXMLNode, "type");
                    //sOutputFileName = sStagePath + "\\" + GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                    sOutputFileName = sStagePath + GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                    sOutputBundleFileName = sStagePath + GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                    sOutputFileNameOnly = GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                    sMergeFlag = GetXMLNodeAttributeValue(oXMLNode, "merge");

                    //-- ??????? IF sOutputFileType is empty set it to PDF.  I think we will alway have to set to PDF, ZIP will brake things ??????

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XMLBASEATTRIBUTES)", intEventSeq.ToString(), "XMLBASEATTRIBUTES", DateTime.Now + " - Base Vars", string.Format("<<<Base Vars: OutputFileType: {0}, OutputFileName: {1}, MergeFlag: {2}  >>>", sOutputFileType, sOutputFileName, sMergeFlag), "");
                    intEventSeq = (intEventSeq + 1);

                    //-- Get ElectBundling Node
                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/ElectBundling");
                    sUNCTarget = GetXMLSingleNodeValue(oXMLNode, "UNCTarget") + "\\";
                    sElecMerge = GetXMLSingleNodeValue(oXMLNode, "merge");

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XMLBUNDLEOUTPUTATTRIBUTES)", intEventSeq.ToString(), "XMLBUNDLEOUTPUTATTRIBUTES", DateTime.Now + " - Bundle Output Vars", string.Format("<<<Bundle Output Vars: Type: {0}, OutputFile: {1}, Merge: {2}, UNCTarget: {3}, ValidExt: {4}  >>>", sOutputFileType, sOutputFileName, sMergeFlag, sUNCTarget, sValidExt), "");
                    intEventSeq = (intEventSeq + 1);

                    //-- Get all the files from the Job and Processes
                    objDetailsNodeList = jobDetailsXML.SelectNodes("JobDetails/FileList/File");

                    //---------------------------------------------
                    //-- This loop converts all of the documents
                    //-- to PDF's including XML/XSLT Conversions
                    //---------------------------------------------
                    int iFileIndex = 0;
                    foreach (XmlNode objNode in objDetailsNodeList)
                    {
                        sType = GetXMLNodeAttributeValue(objNode, "filetype");
                        sSource = GetXMLNodeAttributeValue(objNode, "source");
                        sFileName = GetXMLNodeAttributeValue(objNode, "filename");
                        sOutputPathFileName = sStagePath + GetXMLNodeAttributeValue(objNode, "outputfilename");
                        sOutputFileName = GetXMLNodeAttributeValue(objNode, "outputfilename");
                        sXSLTFileName = GetXMLNodeAttributeValue(objNode, "xslpath");

                        //-- Go build the JobFileDetails
                        sPrimary = GetXMLNodeAttributeValue(objNode, "primary");
                        sMerge = GetXMLNodeAttributeValue(objNode, "merge");
                        sNoConvert = GetXMLNodeAttributeValue(objNode, "noconvert");

                        if (sXSLTFileName != "")
                        {
                            sPickupPath = sSource.Replace(sFileName, "");
                            sXSLTFileName = sPickupPath + GetXMLNodeAttributeValue(objNode, "xslpath");
                        }

                        sDocProcOutputFileName = GetXMLNodeAttributeValue(objNode, "outputfilename");
                        sPathFile = sSource;
                        sHTMLOutputFilePath = sUNCTarget + sDocProcOutputFileName;

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(READFILELIST)", intEventSeq.ToString(), "READFILELIST", DateTime.Now + " - ReadFileList", string.Format("<<<ReadFileList(" + iFileCount + ") Vars: Type: {0}, Source: {1}, FileName: {2}, PathFile: {3}, Merge: {4}, ValidExt: {5}, OutputFileName: {6}, XSLTFileName: {7} >>>", sType, sSource, sFileName, sPathFile, sMergeFlag, sValidExt, sDocProcOutputFileName, sXSLTFileName), "");
                        intEventSeq = (intEventSeq + 1);

                        //----------------------------------//
                        // Main Processing Code             //
                        //----------------------------------//
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE)", intEventSeq.ToString(), "MERGING", DateTime.Now + " - BundlingJob Start: Merging the files into 1 - " + sOutputFileType + " ...", string.Format("CONVERTING - Params: Type: {0}, FileName: {1}, PathFile: {2}, PathFile: {3}, StagePath: {4}", sType, sFileName, sPathFile, sSource, sStagePath), "");
                        intEventSeq = (intEventSeq + 1);

                        aXSLTFileNameList.Add(sXSLTFileName);
                        aOutputFileNameList.Add(sDocProcOutputFileName);
                        aSourceFileNameList.Add(sSource);
                        aOriginalTypeList.Add("." + sType);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(<<<FILELIST-ARRAY>>>)", intEventSeq.ToString(), "FILELIST-File", DateTime.Now + " - Processing (" + sSource + ") ...", string.Format("aFileNameList: {0}, aOutputFileNameList: {1}, aSourceFileNameList: {2}, type: {3}", sFileName.Replace(sType, "pdf"), sDocProcOutputFileName, sSource, sType), "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Move/Convert the files from Document Processor Pickup Queue 
                        //-- If not pdf BasePDFFunctions.Convert to pdf and save into stage area
                        //-- for packaging and save names in array

                        if (sType.ToUpper() != "XML")
                        {
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(<<<SENT-ADD>>>)", intEventSeq.ToString(), "Sent Add File", DateTime.Now + " - Adding file to SentPDF", string.Format("sType: {0}, sFileName: {1}, sPathFile: {2}, sSource: {3}, sStagePath: {4}", "." + sType, sFileName, sPathFile, sSource, sStagePath), "");
                            intEventSeq = (intEventSeq + 1);

                            sRC = ConvertToPDF("." + sType, sOutputFileName, sPathFile, sSource, sStagePath);

                            //-- Check Return code and continue
                            if (sRC.ToUpper().Contains("ERROR"))
                            {
                                //-- Call ErrorHandler and log and fail the Job
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - ERROR: Merging the files RC: " + sRC, "", "");
                                intEventSeq = (intEventSeq + 1);

                                success = false;
                            }

                            //-- Check to make sure file was converted successfully
                            if (File.Exists(sStagePath + sOutputFileName.Replace(sType, "pdf")))
                            {
                                //-- File Exists Log this for better debugging
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(####" + sType + "-COVERT-CHECK####)", intEventSeq.ToString(), "SUCCESSFUL " + sType + "-2-PDF Convert", DateTime.Now + " - Checking " + sType + " convert to PDF", string.Format("OutputPathFileName: {0}, Type: {1}", sStagePath + sOutputFileName.Replace(sType, "pdf"), sType), "");
                                intEventSeq = (intEventSeq + 1);
                            }
                            else
                            {
                                //-- File did not convert successfully.  Notify someone

                                //-- Log it distinctively 
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(####" + sType + "-COVERT-CHECK-ERROR####)", intEventSeq.ToString(), "ERROR " + sType + "-2-PDF Convert", DateTime.Now + " - ERROR: " + sType + " convert to PDF", string.Format("OutputPathFileName: {0}, Type: {1}", sStagePath + sOutputFileName.Replace(sType, "pdf"), sType), "");
                                intEventSeq = (intEventSeq + 1);

                                //-- Email notify of failed convert
                                wsAPDFoundation.SendMail(appSettingsReader.GetValue("ErrorToEmail", typeof(string)).ToString(), appSettingsReader.GetValue("ErrorFromEmail", typeof(string)).ToString(), "", "APDPostOffice(ERROR " + sType + "-2-PDF) Convert", "The APDPostOffice failed to convert the file " + sOutputFileName.Replace(sType, "pdf") + " from " + sType + " to PDF.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());
                            }
                        }
                        else
                        {
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(<<<XML-SENT-ADD>>>)", intEventSeq.ToString(), "XML - Sent Add File", DateTime.Now + " - Adding file to SentPDF", string.Format("sType: {0}, sFileName: {1}, sPathFile: {2}, sSource: {3}, sStagePath: {4}", "." + sType, sFileName, sPathFile, sSource, sStagePath), "");
                            intEventSeq = (intEventSeq + 1);

                            if (sXSLTFileName != string.Empty)
                            {
                                //bElectRC = ElectronicBundling(sUNCTarget, aSourceFileNameList, iFileIndex, aOutputFileNameList, aOriginalTypeList, sFile, sOutputFileType, sOutputFileName, aXSLTFileNameList);

                                //aFileNameList.Add(sDocProcOutputFileName);
                                bXSLTConvertRC = XSLTConvertToHTM(sDocProcOutputFileName, aXSLTFileNameList, iFileCount, aSourceFileNameList, sHTMLOutputFilePath, aOutputFileNameList, sUNCTarget);

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "TVDTEST", intEventSeq.ToString(), "After XSLT", DateTime.Now + " - After the xslt convert", "bXSLTConvertRC: " + bXSLTConvertRC + ", sDocProcOutputFileName: " + sDocProcOutputFileName, "");
                                intEventSeq = (intEventSeq + 1);

                                //if (File.Exists(sDocProcOutputFileName))
                                if (bXSLTConvertRC)
                                {
                                    //File.Copy(sDocProcOutputFileName, sUNCTarget + aOutputFileNameList[iFileCount].ToString(), true);
                                    sRC = ConvertToPDF(".htm", sDocProcOutputFileName, sStagePath + sDocProcOutputFileName, sStagePath, sStagePath);

                                    //-- Check to make sure file was converted successfully
                                    if (File.Exists(sStagePath + sDocProcOutputFileName.Replace(sType, "pdf")))
                                    {
                                        //-- File Exists Log this for better debugging
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(####XML-COVERT-CHECK####)", intEventSeq.ToString(), "SUCCESSFUL XML-2-PDF Convert", DateTime.Now + " - Checking XML/XSLT convert to PDF", string.Format("OutputPathFileName: {0}", sStagePath + sDocProcOutputFileName.Replace(sType, "pdf")), "");
                                        intEventSeq = (intEventSeq + 1);
                                    }
                                    else
                                    {
                                        //-- File did not convert successfully.  Notify someone

                                        //-- Log it distinctively 
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(????XML-COVERT-CHECK-ERROR????)", intEventSeq.ToString(), "ERROR XML-2-PDF Convert", DateTime.Now + " - ERROR: XML/XSLT convert to PDF", string.Format("OutputPathFileName: {0}", sStagePath + sDocProcOutputFileName.Replace(sType, "pdf")), "");
                                        intEventSeq = (intEventSeq + 1);

                                        //-- Email notify of failed convert
                                        wsAPDFoundation.SendMail(appSettingsReader.GetValue("ErrorToEmail", typeof(string)).ToString(), appSettingsReader.GetValue("ErrorFromEmail", typeof(string)).ToString(), "", "APDPostOffice(ERROR XML-2-PDF) Convert", "The APDPostOffice failed to convert the file " + sDocProcOutputFileName.Replace(sType, "pdf") + " from XML/XSLT to PDF.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());
                                    }
                                }
                            }
                        }

                        //-- Copy orignal files to Elect Folder
                        if (sUNCTarget != "")
                        {
                            //-- No htm/xml or doc files in elect folder
                            if (sType.ToUpper() != "HTM" && sType.ToUpper() != "XML" && sType.ToUpper() != "DOC")
                            {
                                try
                                {
                                    File.Copy(sSource, sUNCTarget + sDocProcOutputFileName, true);
                                }
                                catch (Exception oExcept)
                                {
                                    // ------------------------------
                                    //  Email Notify of the Error
                                    // ------------------------------
                                    string sError = "";
                                    System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                                    sError = string.Format("Error: {0}", ("ERROR: Source file copy failed... (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SOURCE_COPY)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Source file copy failed.  Exception thrown", sError, "");
                                    intEventSeq = (intEventSeq + 1);

                                    success = false;
                                }

                            }
                        }

                        Class.JobFileDetails oJobFileDetails = new Class.JobFileDetails();
                        oJobFileDetails = BuildJobFileDetails(sPrimary, sSource, sFileName, sType, sXSLTFileName, sMerge, sNoConvert, sOutputPathFileName.Replace(".htm",".pdf"), iFileIndex);
                        liJobFileData.Add(oJobFileDetails);
                        //????????????????????
                        //-- Check for errors in BuildJobFileDetails
                        //liJobFileData
                        //????????????????????

                        iFileCount += 1;
                        iFileIndex += 1;
                    }

                    //---------------------------------------------
                    //-- This loop creates the merged single PDF 
                    //-- that contains all of the PDF's above
                    //---------------------------------------------
                    //-- Processing from JobFileDetails Class File
                    int iPDFPageIndex = 0;
                    string sNewFileName = string.Empty;
                    foreach (Class.JobFileDetails oJobFiles in liJobFileData)
                    {
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(<<<PROCESSING_FILELIST_FILES>>>)", intEventSeq.ToString(), "Processing FileList/Files", DateTime.Now + " - Processing file: " + oJobFiles.OutputFile, string.Format("Primary: {0}, Source: {1}, FileName: {2}, FileType: {3}, XSLPath: {4}, Merge: {5}, NoConvert: {6}, OutputFile: {7}, OutputBundleFileName: {8}, STATUS: {9}", oJobFiles.Primary, oJobFiles.Source, oJobFiles.FileName, oJobFiles.FileType, oJobFiles.XSLPath, oJobFiles.Merge, oJobFiles.NoConvert, oJobFiles.OutputFile, sOutputBundleFileName, oJobFiles.Error), "Details: " + oJobFiles.FileName);
                        intEventSeq = (intEventSeq + 1);

                        //-------------------------------------------
                        //-- Sent All Files into 1 PDF from OutputType
                        //-------------------------------------------
                        //-- Adjust File Names
                        //sNewFileName = oJobFiles.OutputFile;
                        sNewFileName = oJobFiles.OutputFile.Replace(oJobFiles.FileType,"pdf");

                        //-- Process XML Documents
                        if (oJobFiles.FileType.ToUpper() == "XML")
                        {
                            sNewFileName = oJobFiles.OutputFile.Replace(".htm", ".pdf");
                        }

                        //-- Process DOC Documents
                        if (oJobFiles.FileType.ToUpper() == "DOC")
                        {
                            sNewFileName = oJobFiles.OutputFile.Replace(".doc", ".pdf");
                        }

                        //-- Process DOC Documents
                        if (oJobFiles.FileType.ToUpper() == "JPG")
                        {
                            sNewFileName = oJobFiles.OutputFile.Replace(".jpg", ".pdf");
                        }

                        bSentFolderCompressedRC = SentFolderCompressed1(sOutputBundleFileName, sOutputFileType, oJobFiles.FileIndex, sNewFileName, sOutputBundleFileName);

                        //-- If Merge = Yes then just copy the above compressed file over to the stage area

                        //-- If Merge = No just copy convert each file to PDF and save in stage area

                        //-- Copy Coversheet to elect as a PDF
                        //if (oJobFiles.OutputFile.Contains("coversheet") || oJobFiles.FileType.ToUpper() == "DOC")
                        if (oJobFiles.OutputFile.Contains("coversheet") || oJobFiles.FileType.ToUpper() == "DOC" || oJobFiles.FileType.ToUpper() == "HTM")
                            {
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(COVER-PDF-COPY)", intEventSeq.ToString(), "Processing cover sheet PDF's", DateTime.Now + " - Processing cover sheet PDF file: " + oJobFiles.OutputFile + " to Elec Folder", string.Format("oJobFiles.OutputFile : {0}, OutputFileASPDF: {1}, sUNCTarget: {2}, JustOutputFileName: {3}, DocType: {4}", oJobFiles.OutputFile, oJobFiles.OutputFile.Replace(".htm", ".pdf"), sUNCTarget, oJobFiles.OutputFile.Replace(sStagePath, ""), oJobFiles.FileType), "");
                                intEventSeq = (intEventSeq + 1);

                            try
                            {
                                File.Copy(oJobFiles.OutputFile, sUNCTarget + oJobFiles.OutputFile.Replace(sStagePath, ""));
                            }
                            catch (Exception oExcept)
                            {
                                // ------------------------------
                                //  Email Notify of the Error
                                // ------------------------------
                                string sError = "";
                                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                                sError = string.Format("Error: {0}", ("ERROR: Cover special file copy failed... (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(COVER-PDF-COPY)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Cover sheet file copy failed.  Exception thrown", sError, "");
                                intEventSeq = (intEventSeq + 1);

                                success = false;
                            }
                        }

                        //-- Copy Invoice to elect as a PDF
                        if (oJobFiles.FileType.ToUpper() == "DOC")
                        {
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(INVOICE-PDF)", intEventSeq.ToString(), "Processing invoice PDF", DateTime.Now + " - Processing invoice PDF file: " + oJobFiles.OutputFile + " to Elec Folder", oJobFiles.FileType, "");
                            intEventSeq = (intEventSeq + 1);

                            try
                            {
                                File.Copy(sNewFileName, sUNCTarget + sNewFileName.Replace(sStagePath, ""));
                            }
                            catch (Exception oExcept)
                            {
                                // ------------------------------
                                //  Email Notify of the Error
                                // ------------------------------
                                string sError = "";
                                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                                sError = string.Format("Error: {0}", ("ERROR: Cover invoice file copy failed... (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(INVOICE-PDF-COPY)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Invoice file copy failed.  Exception thrown", sError, "");
                                intEventSeq = (intEventSeq + 1);

                                success = false;
                            }
                        }


                        iPDFPageIndex += 1;
                    }

                    //-------------------------------------------
                    //-- Create the Elec Bundling files by just
                    //-- copying the files to the elec folder
                    //-------------------------------------------
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ELEC-BUNDLING)", intEventSeq.ToString(), "Copy complete PDF bundle.", DateTime.Now + " - Copy complete PDF to Elect file: " + sOutputBundleFileName + " to: " + sUNCTarget, string.Format("sStagePath: {0}, sOutputBundleFileName: {1}, sUNCTarget: {2}, sOutputFileNameOnly {3}", sStagePath, sOutputBundleFileName, sUNCTarget, sOutputFileNameOnly), "");
                    intEventSeq = (intEventSeq + 1);

                    try
                    {
                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "DEBUG", intEventSeq.ToString(), "DEBUG", DateTime.Now + " - DEBUG", string.Format("sOutputBundleFileName: {0}, sUNCTarget: {1}, sOutputFileNameOnly: {2}", sOutputBundleFileName, sUNCTarget, sOutputFileNameOnly), "");
                        //intEventSeq = (intEventSeq + 1);

                        //File.Copy(sOutputBundleFileName, sUNCTarget + sOutputFileNameOnly);
                    }
                    catch (Exception oExcept)
                    {
                        // ------------------------------
                        //  Email Notify of the Error
                        // ------------------------------
                        string sError = "";
                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                        sError = string.Format("Error: {0}", ("ERROR: PDF file copy failed... (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(PDF_COPY)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: PDF file copy failed.  Exception thrown", sError, "");
                        intEventSeq = (intEventSeq + 1);

                        success = false;
                    }

                    //-- ??????????????
                    //-- Check to make sure files created
                    //-- ??????????????


                    FileDetails oFileDetails = new FileDetails();
                    //oFileDetails = BuildBundleParams(sOutputFileName);
                    oFileDetails = BuildBundleParams(sOutputBundleFileName);

                    //-- Parse the JobXML and replace the FileLength and FileBase64Data
                    oXMLNode = jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/DocUpload/PostTemplate");

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGEFILEDATA)", intEventSeq.ToString(), "Merge file data returned in FileDetails class.", DateTime.Now + " - File size returned: " + oFileDetails.FileLength, "DocUploadXML: " + oXMLNode.InnerXml, "");
                    intEventSeq = (intEventSeq + 1);

                    XmlElement oFileExtAttr = (XmlElement)jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/DocUpload/PostTemplate/root/File");
                    if (oFileExtAttr != null)
                    {
                        oFileExtAttr.SetAttribute("FileExt", oFileDetails.FileExt);
                    }

                    XmlElement oFileLengthAttr = (XmlElement)jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/DocUpload/PostTemplate/root/File");
                    if (oFileLengthAttr != null)
                    {
                        oFileLengthAttr.SetAttribute("FileLength", oFileDetails.FileLength);
                    }

                    XmlElement oFileDataNode = (XmlElement)jobDetailsXML.SelectSingleNode("/JobDetails/Bundling/DocUpload/PostTemplate/root/File");
                    if (oFileDataNode != null)
                    {
                        oFileDataNode.InnerText = oFileDetails.FileBase64Data;
                    }

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(FILEDATACHANGE)", intEventSeq.ToString(), "Changed file data from FileDetails class.", DateTime.Now + " - XML Updated Data", "File: " + oXMLNode.InnerXml, "jobDetailsXML: " + jobDetailsXML.InnerXml);
                    intEventSeq = (intEventSeq + 1);

                    //-- ????? Process new job HTTP REQ
                    //}

                    //}

                    //---------------------------------------------
                    //-- Archive all the files for future use
                    //-- into the fileprd01 file system
                    //---------------------------------------------
                    try
                    {
                        //-- Archive the files
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIP-ARCHIVE)", intEventSeq.ToString(), "Starting Archive zip file process.", DateTime.Now + " - Zip Archive file " + sOutputFileName + " processing", "StagePath: " + sStagePath, "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Prepare Doc Proc Archive
                        ZipArchive zipDocProcArchive = new ZipArchive();
                        zipDocProcArchive.DefaultCompressionLevel = CompressionLevel.Best;

                        //string[] sFileArray = Directory.GetFiles(sStagePath, "*.*");
                        string[] sFileArray = Directory.GetFiles(sStagePath, "*.*");

                        foreach (string sZipPathFile in sFileArray)
                        {
                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIPFILEADDLOOP)", intEventSeq.ToString(), "Archive Zip file create and add files.", DateTime.Now + " - Zip Archive file " + sZipPathFile + " add files processing", "", "");
                            intEventSeq = (intEventSeq + 1);

                            //-- Add the file to Doc Proc Archive zip
                            zipDocProcArchive.AddFile(sZipPathFile);

                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIPFILEADDNEXTFILE)", intEventSeq.ToString(), "File added getting next file.", DateTime.Now + " - File added to Zip Archive " + sZipPathFile + ", getting next file.", "", "");
                            intEventSeq = (intEventSeq + 1);
                        }

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ARCHIVE-2-ZIP)", intEventSeq.ToString(), "Archive to Zip process", DateTime.Now + " - Archive file " + sOutputFileName, "", "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Close Doc Proc Archive
                        sOutputFileName = sOutputFileName.Replace(".pdf", DateTime.Now.ToString("yyyyddMM-hhmmss") + ".zip");
                        sOutputFileName = sOutputFileName.Replace(".PDF", DateTime.Now.ToString("yyyyddMM-hhmmss") + ".zip");

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIPFILESAVE)", intEventSeq.ToString(), "Saving Archive Zip file", DateTime.Now + " - Zip Archive file saved at " + sOutputFileName + "zip", "", "");
                        intEventSeq = (intEventSeq + 1);

                        zipDocProcArchive.Save(sOutputFileName + "zip");
                        zipDocProcArchive.Close();

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIP-ARCHIVE-END)", intEventSeq.ToString(), "Zip file ended.", DateTime.Now + " - Zip Archive file completed successful", "", "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Doc Processor Archive
                        sOutputFileName = sOutputFileName + "zip";

                        if (File.Exists(sOutputFileName))
                        {
                            // Remove path from the file name.
                            string sNewOutputFileName = sOutputFileName.Substring(sStagePath.Length + 1);

                            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIP-ARCHIVE)", intEventSeq.ToString(), "Creating Archive zip file and moving to document processor archive location.", DateTime.Now + " - Zip Archive file " + sOutputFileName + " moving to " + sNewOutputFileName + " in Document Processor Archive.", "sDocProcArchivePath: " + sDocProcArchivePath, "");
                            intEventSeq = (intEventSeq + 1);

                            //-- ????????  TEMP REMOVE SO THEY DONT GET SENT ???????
                            //File.Move(sNewOutputFileName, sUNCTarget + sFileName);

                            //-- Move the zip archive file over to Doc Proc Archive folder
                            //-- Check if Archive Directory Exists if not create it
                            if (Directory.Exists(sDocProcArchivePath + DateTime.Now.ToString("yyyyddMM")))
                            {
                                File.Move(sOutputFileName, sDocProcArchivePath + DateTime.Now.ToString("yyyyddMM") + "\\" + sNewOutputFileName);
                            }
                            else
                            {
                                //-- Make Directory 1st then move
                                Directory.CreateDirectory(sDocProcArchivePath + DateTime.Now.ToString("yyyyddMM"));

                                //-- Move the file to the new Directory
                                File.Move(sOutputFileName, sDocProcArchivePath + DateTime.Now.ToString("yyyyddMM") + "\\" + sNewOutputFileName);
                            }
                        }
                        else
                        {
                            //-- OutputFile didn't get created.  Throw error
                            throw new Exception(string.Format("Error: {0}", "PostOffice-BundlingJob(OUTPUTFILECHECK): ERROR OutputFile did not get successfully created...  " + sOutputFileName));
                        }

                        ////-- Clean up the staging area for the next run
                        ////-- after verifying the archive and copies where successful
                        //sFileArray = Directory.GetFiles(sStagePath, "*.*");
                        //foreach (string sCleanPathFile in sFileArray)
                        //{
                        //    //-- CleanUp:  Delete all files in StageArea
                        //    //-- COMMENT the line below for debugging the files.
                        //    File.Delete(sCleanPathFile);

                        //    //--???????????????????????????????????????????
                        //    //-- ???????? CleanUp Doc Proc Pickup table as well after ?????????
                        //    //-- Archive is successful.
                        //}
                    }
                    catch (Exception oExcept)
                    {
                        // ------------------------------
                        //  Email Notify of the Error
                        // ------------------------------
                        string sError = "";
                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                        sError = string.Format("Error: {0}", ("Archive Processing Failed: Failed to Zip/Move archive file to DocProc Archive (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ZIP-ARCHIVE)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Merge into a single ZIP and Move to Archive Exception thrown", sError, "");
                        intEventSeq = (intEventSeq + 1);

                        success = false;
                    }

                    success = true;
                }

                pjr.Method = "BUND";
                pjr.Success = success;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(END-BUNDLING)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - BundPerformJob End: Ending the BundlingJob process, BundPerformJob...", "", "");
                intEventSeq = (intEventSeq + 1);

                return pjr;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("Processing Failed: ProcessJobResult process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                ProcessJobResult pjr = new ProcessJobResult();
                pjr.Method = "Task";
                pjr.Success = false;
                return pjr;
            }
        }

        private FileDetails BuildBundleParams(string sMergePathFile)
        {
            string sFileLength = string.Empty;
            string sFileExt = string.Empty;
            string sBase64FileData = string.Empty;

            try
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(BUILDBUNDLEPARAMS)", intEventSeq.ToString(), "Build Bundle Params Processing.", DateTime.Now + " - Processing Build Bundle Params file " + sMergePathFile, "", "");
                intEventSeq = (intEventSeq + 1);

                if (File.Exists(sMergePathFile))
                {

                    //-- Get file details from the MergePathFile
                    FileInfo fiMergePathFile = new FileInfo(sMergePathFile);

                    sFileLength = fiMergePathFile.Length.ToString();
                    sFileExt = fiMergePathFile.Extension;
                    sFileExt = sFileExt.Substring(1, sFileExt.Length - 1).ToUpper();

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGEFILEDETAILS)", intEventSeq.ToString(), "Merge File Details.", DateTime.Now + " - Merge file params " + sMergePathFile, "FileName: " + fiMergePathFile.Name + ", FileLength: " + sFileLength + ", FileExt: " + sFileExt, "");
                    intEventSeq = (intEventSeq + 1);

                    //-- BaseEncode64 the file
                    sBase64FileData = GetBinData(sMergePathFile);

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(BUILDBUNDLEPARAMS)", intEventSeq.ToString(), "Getting merge file size and base64 data.", DateTime.Now + " - file size and base64 data " + sMergePathFile, "FileLength: " + sFileLength + ", Base64FileData: " + sBase64FileData, "");
                    intEventSeq = (intEventSeq + 1);

                    //-- Update the JobDetail XML to represent the file

                    //-- Return this back for processing
                    FileDetails oFileDetails = new FileDetails();
                    oFileDetails.FileExt = sFileExt;
                    oFileDetails.FileLength = sFileLength;
                    oFileDetails.FileBase64Data = sBase64FileData;

                    return oFileDetails;
                }
                else
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ERROR)", intEventSeq.ToString(), "ERROR: Merge Bundle file does not exist.", DateTime.Now + " - Merge Bundle file DOES NOT exists", "", "");
                    intEventSeq = (intEventSeq + 1);
                }

                FileDetails oFileDetailsError = new FileDetails();
                return oFileDetailsError;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("Processing Failed: BuildBundleParams process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                FileDetails oFileDetails = new FileDetails();
                oFileDetails.FileExt = string.Empty;
                oFileDetails.FileLength = "0";
                oFileDetails.FileBase64Data = string.Empty;

                return oFileDetails;
            }
        }

        public class FileDetails
        {
            //internal FileDetails() {}
            public string FileExt;
            public string FileLength;
            public string FileBase64Data;
        }

        //private bool ElectronicBundling(string sUNCTarget, ArrayList aSourceFileNameList, int iFileIndex, ArrayList aOutputFileNameList, ArrayList aOriginalTypeList, string sFile, string sOutputFileType, string sOutputFileName, ArrayList aXSLTFileNameList)
        //{
        //    bool success = false;
        //    string sRC = string.Empty;
        //    string sTransformedHtml = string.Empty;
        //    string sHTMLOutputFilePath = string.Empty;
        //    bool bXSLTConvertRC = false;

        //    sHTMLOutputFilePath = sUNCTarget + aOutputFileNameList[iFileIndex].ToString().Replace(".pdf", ".html");

        //    //-- Process individual file as well as merged ALWAYS
        //    try
        //    {
        //        //-- Electronic Bundling 
        //        if (sUNCTarget != string.Empty)
        //        {
        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DOC-CONVERT-BUNDLE)", intEventSeq.ToString(), "Starting .DOC convert to .PDF bundle process.", DateTime.Now + " - DOC convert bundle process started for file " + sOutputFileName, string.Format("File: {0}, FileIndex: {1}, aOutputFileNameList[iFileIndex]: {2}, UNCTarget: {3}, Type: {4}, XSLTFileName: {5}", aSourceFileNameList[iFileIndex].ToString(), iFileIndex.ToString(), aOutputFileNameList[iFileIndex].ToString(), sUNCTarget, aOriginalTypeList[iFileIndex].ToString(), aXSLTFileNameList[iFileIndex].ToString()), "");
        //            intEventSeq = (intEventSeq + 1);

        //            //-- XSLT Conversion Processing
        //            //-- If the file is a .txt file, then convert it to PDF before copying to 
        //            if (aOriginalTypeList[iFileIndex].ToString().ToUpper() == ".DOC")
        //            {





        //                //-- UNCTarget
        //                //if (aSourceFileNameList[iFileIndex].ToString().Contains(".htm"))
        //                string sOrigFilePath = aSourceFileNameList[iFileIndex].ToString();
        //                string sNewFilePath = aOutputFileNameList[iFileIndex].ToString();
        //                //string sOutputFileNameOnlyPDF = sOutputFileNameOnly + ".pdf";
        //                //sNewFilePath = sNewFilePath.Replace(".htm", ".pdf");
        //                sNewFilePath = sNewFilePath.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf");

        //                //string sCurrentFileName = aOutputFileNameList[iFileIndex].ToString();

        //                //sNewFilePath = sNewPathFileName.Replace(sNewFilePath, "");

        //                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(HTM-FILE)", intEventSeq.ToString(), "HTM file exist convert.", DateTime.Now + " - Converting .txt file to .pdf " + sOutputFileName, string.Format("NewFile: {0}, OriginalFileName: {1}, UNCPath: {2}", sNewFilePath, sOrigFilePath, sUNCTarget),"");
        //                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(TYPE-FILE)", intEventSeq.ToString(), aOriginalTypeList[iFileIndex].ToString() + " file exist convert.", DateTime.Now + " - Converting " + aOriginalTypeList[iFileIndex].ToString() + " file to .pdf " + sOutputFileName, string.Format("File: {0}, OriginalFileName: {1}, UNCPath: {2}, Type: ", sFile, sOrigFilePath, sUNCTarget, aOriginalTypeList[iFileIndex].ToString()), "");
        //                intEventSeq = (intEventSeq + 1);

        //                //sRC = ConvertToPDF(".htm", sOutputFileNameOnly, sOrigFilePath, sFile.Replace(".htm", ".pdf"), sUNCTarget);
        //                //sRC = ConvertToPDF(".htm", sDocProcOutputFileName, sOrigFilePath, sFile.Replace(".htm", ".pdf"), sUNCTarget);
        //                //sRC = ConvertToPDF(".htm", sNewFilePath, sOrigFilePath, sFile.Replace(".htm", ".pdf"), sUNCTarget);
        //                sRC = ConvertToPDF(aOriginalTypeList[iFileIndex].ToString(), sNewFilePath, sOrigFilePath, sFile.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf"), sUNCTarget);

        //                //    string[] aOutputFileNameOnly = sOutputFileNameOnly.Split('-');
        //            }
        //            else
        //            {
        //                //-- Electronic But Not HTM
        //                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(NON-HTM-FILE)", intEventSeq.ToString(), "Not HTM file exist convert.", DateTime.Now + " - Converting " + sOutputFileType + " file to .pdf " + aOutputFileNameList[iFileIndex], string.Format("File: {0}, OriginalFileName: {1}, OutputFile: {2}, UNCPath: {3}", sFile, aSourceFileNameList[iFileIndex].ToString(), aOutputFileNameList[iFileIndex], sUNCTarget), "");
        //                intEventSeq = (intEventSeq + 1);

        //                string sOrigFilePath = aSourceFileNameList[iFileIndex].ToString();
        //                string sNewFilePath = aOutputFileNameList[iFileIndex].ToString();
        //                sNewFilePath = sNewFilePath.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf");

        //                //sRC = ConvertToPDF(aOriginalTypeList[iFileIndex].ToString(), sNewFilePath, sOrigFilePath, sFile.Replace(sOutputFileType, ".pdf"), sUNCTarget);

        //                File.Copy(aSourceFileNameList[iFileIndex].ToString(), sUNCTarget + aOutputFileNameList[iFileIndex]);
        //            }

        //            //-- Verify that is was copied successfully
        //        }

        //        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DIRECT-COPY)", intEventSeq.ToString(), "Direct copy document processor pickup files into staging area.", DateTime.Now + " - BundlingJob Merger into ZIP Start: Merging the files into 1 - " + sOutputFileType + " ...", "File: " + sFile, "");
        //        //intEventSeq = (intEventSeq + 1);

        //        //-- No Merge, direct copy files to stage area
        //        //File.Copy(Path.Combine(sDocProcPickUpPath, sFile), Path.Combine(sStagePath, sFile), true);
        //        //File.Copy(sFile, Path.Combine(sStagePath, aOutputFileNameList[iFileIndex].ToString()), true);
        //    }
        //    catch (Exception oExcept)
        //    {
        //        // ------------------------------
        //        //  Email Notify of the Error
        //        // ------------------------------
        //        string sError = "";
        //        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //        sError = string.Format("Error: {0}", ("Copy Direct No Merge Bundling Failed: Failed to Copy files (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-ZIP)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Merger into a single ZIP Exception thrown", sError, "");
        //        intEventSeq = (intEventSeq + 1);

        //        success = false;
        //    }

        //    return success;
        //}

        private bool ElectronicBundling(string sUNCTarget, ArrayList aSourceFileNameList, int iFileIndex, ArrayList aOutputFileNameList, ArrayList aOriginalTypeList, string sFile, string sOutputFileType, string sOutputFileName, ArrayList aXSLTFileNameList)
        {
            bool success = false;
            string sRC = string.Empty;
            string sTransformedHtml = string.Empty;
            string sHTMLOutputFilePath = string.Empty;
            bool bXSLTConvertRC = false;

            sHTMLOutputFilePath = sUNCTarget + aOutputFileNameList[iFileIndex].ToString().Replace(".pdf", ".html");

            //-- Process individual file as well as merged ALWAYS
            try
            {
                //-- Electronic Bundling 
                if (sUNCTarget != string.Empty)
                {
                    //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ELEC-BUNDLE)", intEventSeq.ToString(), "Starting electronic bundle process.", DateTime.Now + " - Electronic bundle process started for file " + sOutputFileName, string.Format("File: {0}, FileIndex: {1}, aOutputFileNameList[iFileIndex]: {2}, UNCTarget: {3}, FileName: {4}, StepID: {5}", aSourceFileNameList[iFileIndex].ToString(), iFileIndex.ToString(), aOutputFileNameList[iFileIndex].ToString(), sUNCTarget, sFileName, sStepID), "");
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ELEC-BUNDLE)", intEventSeq.ToString(), "Starting electronic bundle process.", DateTime.Now + " - Electronic bundle process started for file " + sOutputFileName, string.Format("File: {0}, FileIndex: {1}, aOutputFileNameList[iFileIndex]: {2}, UNCTarget: {3}, Type: {4}, XSLTFileName: {5}", aSourceFileNameList[iFileIndex].ToString(), iFileIndex.ToString(), aOutputFileNameList[iFileIndex].ToString(), sUNCTarget, aOriginalTypeList[iFileIndex].ToString(), aXSLTFileNameList[iFileIndex].ToString()), "");
                    intEventSeq = (intEventSeq + 1);

                    //-- XSLT Conversion Processing
                    //if (aXSLTFileNameList[iFileIndex].ToString() != string.Empty)

                    //-- If the file is a .txt file, then convert it to PDF before copying to 
                    //-- UNCTarget
                    //if (aSourceFileNameList[iFileIndex].ToString().Contains(".htm"))
                    if (aOriginalTypeList[iFileIndex].ToString().ToUpper() == ".HTM" || aOriginalTypeList[iFileIndex].ToString().ToUpper() == ".DOC")
                    {
                        string sOrigFilePath = aSourceFileNameList[iFileIndex].ToString();
                        string sNewFilePath = aOutputFileNameList[iFileIndex].ToString();
                        //string sOutputFileNameOnlyPDF = sOutputFileNameOnly + ".pdf";
                        //sNewFilePath = sNewFilePath.Replace(".htm", ".pdf");
                        sNewFilePath = sNewFilePath.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf");

                        //string sCurrentFileName = aOutputFileNameList[iFileIndex].ToString();

                        //sNewFilePath = sNewPathFileName.Replace(sNewFilePath, "");

                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(HTM-FILE)", intEventSeq.ToString(), "HTM file exist convert.", DateTime.Now + " - Converting .txt file to .pdf " + sOutputFileName, string.Format("NewFile: {0}, OriginalFileName: {1}, UNCPath: {2}", sNewFilePath, sOrigFilePath, sUNCTarget),"");
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(TYPE-FILE)", intEventSeq.ToString(), aOriginalTypeList[iFileIndex].ToString() + " file exist convert.", DateTime.Now + " - Converting " + aOriginalTypeList[iFileIndex].ToString() + " file to .pdf " + sOutputFileName, string.Format("File: {0}, OriginalFileName: {1}, UNCPath: {2}, Type: ", sFile, sOrigFilePath, sUNCTarget, aOriginalTypeList[iFileIndex].ToString()), "");
                        intEventSeq = (intEventSeq + 1);

                        sRC = ConvertToPDF(aOriginalTypeList[iFileIndex].ToString(), sNewFilePath, sOrigFilePath, sFile.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf"), sUNCTarget);

                        ////-- Check to make sure electronic file was converted successfully
                        //if (File.Exists(sStagePath + sOutputFileName.Replace(sType, "pdf")))
                        //{
                        //    //-- File Exists Log this for better debugging
                        //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(####" + sType + "-COVERT-CHECK####)", intEventSeq.ToString(), "SUCCESSFUL " + sType + "-2-PDF Convert", DateTime.Now + " - Checking " + sType + " convert to PDF", string.Format("OutputPathFileName: {0}, Type: {1}", sStagePath + sOutputFileName.Replace(sType, "pdf"), sType), "");
                        //    intEventSeq = (intEventSeq + 1);
                        //}
                        //else
                        //{
                        //    //-- File did not convert successfully.  Notify someone

                        //    //-- Log it distinctively 
                        //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-Syncfusion(####" + sType + "-COVERT-CHECK-ERROR####)", intEventSeq.ToString(), "ERROR " + sType + "-2-PDF Convert", DateTime.Now + " - ERROR: " + sType + " convert to PDF", string.Format("OutputPathFileName: {0}, Type: {1}", sStagePath + sOutputFileName.Replace(sType, "pdf"), sType), "");
                        //    intEventSeq = (intEventSeq + 1);

                        //    //-- Email notify of failed convert
                        //    wsAPDFoundation.SendMail(appSettingsReader.GetValue("ErrorToEmail", typeof(string)).ToString(), appSettingsReader.GetValue("ErrorFromEmail", typeof(string)).ToString(), "", "APDPostOffice(ERROR " + sType + "-2-PDF) Convert", "The APDPostOffice failed to convert the file " + sOutputFileName.Replace(sType, "pdf") + " from " + sType + " to PDF.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());
                        //}

                        //    string[] aOutputFileNameOnly = sOutputFileNameOnly.Split('-');
                    }
                    else
                    {
                        //-- Electronic But Not HTM
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(NON-HTM-FILE)", intEventSeq.ToString(), "Not HTM file exist convert.", DateTime.Now + " - Converting " + sOutputFileType + " file to .pdf " + aOutputFileNameList[iFileIndex], string.Format("File: {0}, OriginalFileName: {1}, OutputFile: {2}, UNCPath: {3}", sFile, aSourceFileNameList[iFileIndex].ToString(), aOutputFileNameList[iFileIndex], sUNCTarget), "");
                        intEventSeq = (intEventSeq + 1);

                        string sOrigFilePath = aSourceFileNameList[iFileIndex].ToString();
                        string sNewFilePath = aOutputFileNameList[iFileIndex].ToString();
                        sNewFilePath = sNewFilePath.Replace(aOriginalTypeList[iFileIndex].ToString(), ".pdf");

                        //sRC = ConvertToPDF(aOriginalTypeList[iFileIndex].ToString(), sNewFilePath, sOrigFilePath, sFile.Replace(sOutputFileType, ".pdf"), sUNCTarget);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "DEBUG TVD", intEventSeq.ToString(), "DEBUG", DateTime.Now + " - DEBUG", "", "");
                        intEventSeq = (intEventSeq + 1);

                        //File.Copy(aSourceFileNameList[iFileIndex].ToString(), sUNCTarget + aOutputFileNameList[iFileIndex]);
                    }

                    //-- Verify that is was copied successfully
                }

                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DIRECT-COPY)", intEventSeq.ToString(), "Direct copy document processor pickup files into staging area.", DateTime.Now + " - BundlingJob Merger into ZIP Start: Merging the files into 1 - " + sOutputFileType + " ...", "File: " + sFile, "");
                //intEventSeq = (intEventSeq + 1);

                //-- No Merge, direct copy files to stage area
                //File.Copy(Path.Combine(sDocProcPickUpPath, sFile), Path.Combine(sStagePath, sFile), true);
                //File.Copy(sFile, Path.Combine(sStagePath, aOutputFileNameList[iFileIndex].ToString()), true);
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", ("Copy Direct No Merge Bundling Failed: Failed to Copy files (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-ZIP)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Merger into a single ZIP Exception thrown", sError, "");
                intEventSeq = (intEventSeq + 1);

                success = false;
            }

            return success;
        }

        private bool SentFolderCompressed1(string sOutputBundleFileName, string sOutputFileType, int iPageIndex, string sFile, string sOutputFileName)
        {
            bool success = false;
            PdfDocument NewPDFDocument = new PdfDocument();

            try
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-FOLDER-COMPRESSED)", intEventSeq.ToString(), "MERGING into Single File (PDF or ZIP)", DateTime.Now + " - BundlingJob Merger into " + sOutputFileType + " Start: Merging Sent files into 1 file - " + sOutputFileType + " ...", "OutputFileType: " + sOutputFileType, "OutputBundleFileName: " + sOutputBundleFileName + ", sOutputFileName: " + sOutputFileName);
                intEventSeq = (intEventSeq + 1);

                if (sOutputFileType.ToUpper() == "PDF")
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-INTO-PDF)", intEventSeq.ToString(), "MERGING into PDF", DateTime.Now + " - BundlingJob Merger into PDF Start: Merging the files into 1 - " + sOutputFileType + " ...", "sOutputFileName: " + sOutputFileName, "");
                    intEventSeq = (intEventSeq + 1);

                    if (iPageIndex == 0)
                    {
                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-INITAL-PDF)", intEventSeq.ToString(), "Creating the Initial Merge PDF file.", DateTime.Now + " - Index is 0 so creating a new merge PDF.  Initial PDF file: " + sOutputFileName, "1st File: " + sStagePath + sFile, "");
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-INITAL-PDF)", intEventSeq.ToString(), "Creating the Initial Merge PDF file.", DateTime.Now + " - Index is 0 so creating a new merge PDF.  Initial PDF file: " + sOutputFileName, "1st File: " + sFile, "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Create the new output file
                        //PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
                        PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sFile);
                        NewPDFDocument.ImportPage(lDoc1, iPageIndex);

                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DEBUGxxx)", intEventSeq.ToString(), "DEBUGxxx", DateTime.Now + " - Initial PDF file: " + sOutputFileName, "1st File: " + sStagePath + sFile, "");
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DEBUGxxx)", intEventSeq.ToString(), "DEBUGxxx", DateTime.Now + " - Initial PDF file: " + sOutputFileName, "1st File: " + sFile, "");
                        intEventSeq = (intEventSeq + 1);

                        NewPDFDocument.Save(sOutputBundleFileName);
                        lDoc1.Close(true);
                        NewPDFDocument.Close(true);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-INITAL-PDF-DONE)", intEventSeq.ToString(), "Finished creating the Initial Merge PDF file.", DateTime.Now + " - Finished creating initial PDF.", "1st File: " + sFile, "");
                        intEventSeq = (intEventSeq + 1);
                    }
                    else
                    {
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(FILE-ADD)", intEventSeq.ToString(), "Add another file to the merge PDF.", DateTime.Now + " - Index is > 0 so adding another file to the new merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sFile, "");
                        intEventSeq = (intEventSeq + 1);

                        //-- Open the existing output file
                        PdfLoadedDocument srcDoc = new PdfLoadedDocument(sOutputFileName);

                        //-- Open the next file to be appended
                        PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sFile);

                        srcDoc.Append(lDoc1);
                        srcDoc.Save();
                        srcDoc.Close(true);
                        lDoc1.Close(true);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(FILE-ADD-DONE)", intEventSeq.ToString(), "Done adding another file to the merge PDF.", DateTime.Now + " - Done adding another file to the new merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sFile, "");
                        intEventSeq = (intEventSeq + 1);
                    }

                    iPageIndex += 1;
                }

                if (sOutputFileType.ToUpper() == "ZIP")
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-INTO-ZIP)", intEventSeq.ToString(), "MERGING into ZIP", DateTime.Now + " - BundlingJob Merger into ZIP Start: Merging the files into 1 - " + sOutputFileType + " ...", "", "");
                    intEventSeq = (intEventSeq + 1);

                    if (iPageIndex == 0)
                    {
                        //-- Add to single ZIP file 
                        //-- Create the new output file
                        ZipArchive zipArchive = new ZipArchive();
                        zipArchive.DefaultCompressionLevel = CompressionLevel.Best;
                        //-- Add the file to zip
                        //zipArchive.AddFile(sStagePath + sFile);
                        zipArchive.AddFile(sStagePath + sFile);
                        //-- Zip file name and location
                        zipArchive.Save(sOutputFileName);
                        zipArchive.Close();
                    }
                    else
                    {
                        //-- Open the existing output file
                        ZipArchive zipArchive = new ZipArchive();
                        zipArchive.DefaultCompressionLevel = CompressionLevel.Best;
                        zipArchive.Open(sOutputFileName);
                        //-- Add the file to zip
                        zipArchive.AddFile(sStagePath + sFile);
                        zipArchive.AddFile(sStagePath + sFile);
                        //-- Zip file name and location
                        zipArchive.Save(sOutputFileName);
                        zipArchive.Close();
                    }

                    iPageIndex += 1;
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", ("Sent Folder Compressed files Failed: Failed to add all files from bundle to PDF (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-PDF-CREATE)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Adding files to single PDF Exception thrown", sError, "");
                intEventSeq = (intEventSeq + 1);

                success = false;
            }

            return success;
        }

        //private bool SentFolderCompressed(string sOutputFileType, ArrayList aXSLTFileNameList, int iFileIndex, int iPageIndex, string sFile, string sOutputFileName, PdfDocument NewPDFDocument, ArrayList aSourceFileNameList, string sHTMLOutputFilePath, ArrayList aOutputFileNameList, string sUNCTarget)
        //{
        //    bool success = false;
        //    //bool bXSLTConvertRC = false;

        //    try 
        //    {
        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-FOLDER-COMPRESSED)", intEventSeq.ToString(), "MERGING into PDF", DateTime.Now + " - BundlingJob Merger into PDF Start: Merging Sent files into 1 PDF - " + sOutputFileType + " ...", "OutputFileType: " + sOutputFileType + ", aXSLTFileNameList[iFileIndex]: " + aXSLTFileNameList[iFileIndex].ToString(), "");
        //        intEventSeq = (intEventSeq + 1);

        //        if (sOutputFileType.ToUpper() == "PDF")
        //        {
        //            //if (aXSLTFileNameList[iFileIndex].ToString().Contains(".xsl"))
        //            //{
        //            //    //-- XSLT Exists, Convert XML/XSLT to HTM
        //            //    bXSLTConvertRC = XSLTConvertToHTM(sOutputFileName, aXSLTFileNameList, iFileIndex, aSourceFileNameList, sHTMLOutputFilePath, aOutputFileNameList, sUNCTarget);
        //            //}

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-PDF)", intEventSeq.ToString(), "MERGING into PDF", DateTime.Now + " - BundlingJob Merger into PDF Start: Merging the files into 1 - " + sOutputFileType + " ...", "", "");
        //                intEventSeq = (intEventSeq + 1);

        //                try
        //                {
        //                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-PDF)", intEventSeq.ToString(), "Creating Merge PDF", DateTime.Now + " - BundlingJob Create Merge PDF,  Adding file: " + sFile, "PageIndex: " + iPageIndex.ToString() + ", lDoc1: " + sStagePath + sFile, "");
        //                    intEventSeq = (intEventSeq + 1);

        //                    if (iPageIndex == 0)
        //                    {
        //                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-INITAL-PDF)", intEventSeq.ToString(), "Creating the Initial Merge PDF file.", DateTime.Now + " - Index is 0 so creating a new merge PDF.  Initial PDF file: " + sOutputFileName, "1st File: " + sStagePath + sFile, "");
        //                        intEventSeq = (intEventSeq + 1);

        //                        //-- Create the new output file
        //                        //PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
        //                    PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
        //                    NewPDFDocument.ImportPage(lDoc1, iPageIndex);

        //                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(DEBUGxxx)", intEventSeq.ToString(), "DEBUGxxx", DateTime.Now + " - Initial PDF file: " + sOutputFileName, "1st File: " + sStagePath + sFile, "");
        //                    intEventSeq = (intEventSeq + 1);

        //                    NewPDFDocument.Save(sOutputFileName);
        //                        lDoc1.Close(true);
        //                        NewPDFDocument.Close(true);

        //                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(CREATE-INITAL-PDF-DONE)", intEventSeq.ToString(), "Finished creating the Initial Merge PDF file.", DateTime.Now + " - Finished creating initial PDF.", "1st File: " + sStagePath + sFile, "");
        //                        intEventSeq = (intEventSeq + 1);
        //                    }
        //                    else
        //                    {
        //                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(FILE-ADD)", intEventSeq.ToString(), "Add another file to the merge PDF.", DateTime.Now + " - Index is > 0 so adding another file to the new merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sStagePath + sFile, "");
        //                        intEventSeq = (intEventSeq + 1);

        //                        //-- Open the existing output file
        //                        PdfLoadedDocument srcDoc = new PdfLoadedDocument(sOutputFileName);
        //                        //-- Open the next file to be appended
        //                        //PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
        //                    PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
        //                    srcDoc.Append(lDoc1);
        //                        srcDoc.Save();
        //                        srcDoc.Close(true);
        //                        lDoc1.Close(true);

        //                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(FILE-ADD-DONE)", intEventSeq.ToString(), "Done adding another file to the merge PDF.", DateTime.Now + " - Done adding another file to the new merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sStagePath + sFile, "");
        //                        intEventSeq = (intEventSeq + 1);
        //                    }
        //                    iPageIndex += 1;

        //                    //-- Stage area Cleanup
        //                    //File.Delete(sStagePath + sFile);

        //                    success = true;
        //                }
        //                catch (Exception oExcept)
        //                {
        //                    // ------------------------------
        //                    //  Email Notify of the Error
        //                    // ------------------------------
        //                    string sError = "";
        //                    System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //                    sError = string.Format("Error: {0}", ("ConvertToPDF Failed: Failed to BasePDFFunctions.Convert file to PDF (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

        //                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-PDF)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Merger into a single PDF Exception thrown", sError, "");
        //                    intEventSeq = (intEventSeq + 1);

        //                    success = false;
        //                }

        //                try
        //                {
        //                    if (sOutputFileType.ToUpper() == "ZIP")
        //                    {
        //                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-ZIP)", intEventSeq.ToString(), "MERGING into ZIP", DateTime.Now + " - BundlingJob Merger into ZIP Start: Merging the files into 1 - " + sOutputFileType + " ...", "", "");
        //                        intEventSeq = (intEventSeq + 1);

        //                        if (iPageIndex == 0)
        //                        {
        //                            //-- Add to single ZIP file 
        //                            //-- Create the new output file
        //                            ZipArchive zipArchive = new ZipArchive();
        //                            zipArchive.DefaultCompressionLevel = CompressionLevel.Best;
        //                            //-- Add the file to zip
        //                            //zipArchive.AddFile(sStagePath + sFile);
        //                        zipArchive.AddFile(sStagePath + sFile);
        //                        //-- Zip file name and location
        //                        zipArchive.Save(sOutputFileName);
        //                            zipArchive.Close();
        //                        }
        //                        else
        //                        {
        //                            //-- Open the existing output file
        //                            ZipArchive zipArchive = new ZipArchive();
        //                            zipArchive.DefaultCompressionLevel = CompressionLevel.Best;
        //                            zipArchive.Open(sOutputFileName);
        //                            //-- Add the file to zip
        //                            zipArchive.AddFile(sStagePath + sFile);
        //                        zipArchive.AddFile(sStagePath + sFile);
        //                        //-- Zip file name and location
        //                        zipArchive.Save(sOutputFileName);
        //                            zipArchive.Close();
        //                        }
        //                    }
        //                    iPageIndex += 1;

        //                    //-- Stage area Cleanup
        //                    //File.Delete(sStagePath + sFile);
        //                }
        //                catch (Exception oExcept)
        //                {
        //                    // ------------------------------
        //                    //  Email Notify of the Error
        //                    // ------------------------------
        //                    string sError = "";
        //                    System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //                    sError = string.Format("Error: {0}", ("ConvertToZIP Failed: Failed to BasePDFFunctions.Convert file to ZIP (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

        //                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(MERGE-ZIP)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Merger into a single ZIP Exception thrown", sError, "");
        //                    intEventSeq = (intEventSeq + 1);

        //                    success = false;
        //                }
        //            }
        //            //else
        //            //{
        //            //    //-- XSLT Exists, Convert XML/XSLT to HTM
        //            //    bXSLTConvertRC = XSLTConvertToHTM(sOutputFileName, aXSLTFileNameList, iFileIndex, aSourceFileNameList, sHTMLOutputFilePath, aOutputFileNameList, sUNCTarget);
        //            //}
        //        //}

        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-FOLDER-COMPRESSED)", intEventSeq.ToString(), "MERGING into PDF", DateTime.Now + " - BundlingJob Merger into PDF Complete", "", "");
        //        intEventSeq = (intEventSeq + 1);
        //    }
        //    catch (Exception oExcept)
        //    {
        //        // ------------------------------
        //        //  Email Notify of the Error
        //        // ------------------------------
        //        string sError = "";
        //        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //        sError = string.Format("Error: {0}", ("Sent Folder Compressed files Failed: Failed to add all files from bundle to PDF (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-PDF-CREATE)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Adding files to single PDF Exception thrown", sError, "");
        //        intEventSeq = (intEventSeq + 1);

        //        success = false;
        //    }

        //    return success;
        //}

        private bool XSLTConvertToHTM(string sOutputFileName, ArrayList aXSLTFileNameList, int iFileIndex, ArrayList aSourceFileNameList, string sHTMLOutputFilePath, ArrayList aOutputFileNameList, string sUNCTarget)
        {
            bool success = false;
            string sTransformedHtml = string.Empty;

            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS)", intEventSeq.ToString(), "Starting XSLT Transformation on bundle.", DateTime.Now + " - File " + sOutputFileName + " needs XSLT Transformed", string.Format("File: {0}, FileIndex: {1}, XSLTFileName: {2} ", aSourceFileNameList[iFileIndex].ToString(), iFileIndex.ToString(), aXSLTFileNameList[iFileIndex].ToString()), "");
            intEventSeq = (intEventSeq + 1);

            try
            {
                if (File.Exists(aXSLTFileNameList[iFileIndex].ToString()))
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-EXISTS)", intEventSeq.ToString(), "XSLT Transformation file exists.", DateTime.Now + " - File " + aXSLTFileNameList[iFileIndex].ToString() + " exists", string.Format("aXSLTFileNameList: {0}", aXSLTFileNameList[iFileIndex].ToString()), "");
                    intEventSeq = (intEventSeq + 1);

                    XslCompiledTransform xslt = new XslCompiledTransform(false);
                    var xslsettings = new XsltSettings();
                    xslsettings.EnableScript = true;
                    using (StreamReader srXslt = new StreamReader(aXSLTFileNameList[iFileIndex].ToString()))
                    {
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-LOAD)", intEventSeq.ToString(), "XSLT Transformation file loading.", DateTime.Now + " - File " + aXSLTFileNameList[iFileIndex].ToString() + " loading", string.Format("aXSLTFileNameList: {0}", aXSLTFileNameList[iFileIndex].ToString()), "");
                        intEventSeq = (intEventSeq + 1);

                        XmlReader readerXslt = XmlReader.Create(srXslt);
                        xslt.Load(readerXslt, xslsettings, null);
                    }

                    #region code for sending parameters to xslt file(Arguments)
                    // Create an XsltArgumentList.
                    //XsltArgumentList xslArg = new XsltArgumentList();
                    //xslArg.AddParam("LynxID", "", strLynxID);

                    #endregion

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-LOAD-OBJ)", intEventSeq.ToString(), "XSLT Transformation OBJ file loading.", DateTime.Now + " - OBJ File " + aSourceFileNameList[iFileIndex].ToString() + " loading", string.Format("aSourceFileNameList[iFileIndex]: {0}", aSourceFileNameList[iFileIndex].ToString()), "");
                    intEventSeq = (intEventSeq + 1);

                    XmlDocument objxmldata = new XmlDocument();
                    objxmldata.Load(aSourceFileNameList[iFileIndex].ToString());

                    #region Transform XML to the given file path
                    // Create and open the output file
                    //using (FileStream fsOutput = File.Create(@"D:\Users\GLSD451\Desktop\XSLT_test\jobcover page\output.html"))

                    //-- Send to correct location.  If Elect go to FilePrd01 if not goto APDPostOffice Stage
                    if (sUNCTarget != string.Empty)
                    {
                        //-- NOT Elect Bunding
                        //sHTMLOutputFilePath = sStagePath + "\\" + aOutputFileNameList[iFileIndex].ToString();
                        sHTMLOutputFilePath = sStagePath + aOutputFileNameList[iFileIndex].ToString();
                    }

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-OUTPUT)", intEventSeq.ToString(), "XSLT Transformation create output file.", DateTime.Now + " - Output File " + sHTMLOutputFilePath + " creating", string.Format("HTMLStagePath: {0}", sHTMLOutputFilePath), "");
                    intEventSeq = (intEventSeq + 1);

                    using (FileStream fsOutput = File.Create(sHTMLOutputFilePath))
                    {
                        XmlWriterSettings xmlSettings = new XmlWriterSettings();
                        xmlSettings.Indent = true;
                        XmlWriter writerXML = XmlTextWriter.Create(fsOutput, xmlSettings);

                        // Open the input file
                        //using (XmlReader readerInput = XmlReader.Create(jobXml))
                        //{
                        xslt.Transform(objxmldata, null, writerXML);
                        //}
                    }
                    #endregion

                    #region Transfrom XML to string Variable and return
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-SAVE)", intEventSeq.ToString(), "XSLT Transformation file saving.", DateTime.Now + " - File " + sHTMLOutputFilePath + " saving", string.Format("HTMLStagePath: {0}", sHTMLOutputFilePath), "");
                    intEventSeq = (intEventSeq + 1);

                    using (TextWriter writer = new Utf8StringWriter())
                    {

                        using (XmlWriter objXmlWriter = XmlWriter.Create(writer))
                        {
                            // Transform the file.
                            xslt.Transform(objxmldata, null, objXmlWriter);
                            objXmlWriter.Close();
                        }
                        sTransformedHtml = writer.ToString();
                    }
                    #endregion

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(XSLT-TRANS-DONE)", intEventSeq.ToString(), "XSLT Transformation file completed.", DateTime.Now + " - XSLT File " + sHTMLOutputFilePath + " complete", string.Format("HTMLStagePath: {0}", sHTMLOutputFilePath), "");
                    intEventSeq = (intEventSeq + 1);
                }
                else
                {
                    throw new Exception("XSLT file not found at the location " + aXSLTFileNameList[iFileIndex].ToString());
                }

                success = true;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", ("XSLT Conversion Failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: XSLT Converting file to .htm", sError, "");
                intEventSeq = (intEventSeq + 1);

                success = false;
            }

            return success;
        }

        private Class.JobFileDetails BuildJobFileDetails(string sPrimary, string sSource, string sFileName, string sFileType, string sXSLPath, string sMerge, string sNoConvert, string sOutputFile, int iFileIndex)
        {
            bool success = false;
            Class.JobFileDetails oJobFileDetails = new Class.JobFileDetails();

            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(JOB_FILE_DETAILS)", intEventSeq.ToString(), "BuildJobFileDetails Started.", DateTime.Now + " - Loading FileList Files into JobFileDetails Class", string.Format("FileIndex: {0}, Primary: {1}, Source: {2}, FileName: {3}, FileType: {4}, XSLPath: {5}, Merge: {6}, NoConvert: {7}, OutputFile: {8}", iFileIndex.ToString(), sPrimary, sSource, sFileName, sFileType, sXSLPath, sMerge, sNoConvert, sOutputFile), "");
            intEventSeq = (intEventSeq + 1);

            try
            {

                oJobFileDetails = new Class.JobFileDetails();

                oJobFileDetails.FileIndex = iFileIndex;
                oJobFileDetails.Primary = sSource;
                oJobFileDetails.FileName = sFileName;
                oJobFileDetails.FileType = sFileType;
                oJobFileDetails.XSLPath = sXSLPath;
                oJobFileDetails.Merge = sMerge;
                oJobFileDetails.NoConvert = sNoConvert;
                oJobFileDetails.OutputFile = sOutputFile;
                oJobFileDetails.Error = "Successful";

                //liJobFileData.Add(oJobFileDetails);

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(JOB_FILE_DETAILS)", intEventSeq.ToString(), "BuildJobFileDetails Completed.", DateTime.Now + " - DONE Loading FileList Files", "oJobFileData: " + oJobFileDetails.FileName, "FileIndex: " + iFileIndex.ToString());
                intEventSeq = (intEventSeq + 1);

                //foreach (string name in listOfNames)
                //    Console.WriteLine(name);

                //-- Removing a row from the list
                //liJobFileData.Remove(oJobFileData[0]);

                //-- Index based row remove from list
                //liJobFileData.RemoveAt(0);

                //-- Remove all using query
                //liJobFileData.RemoveAll(name =>
                //{
                //    if (name.StartsWith("J"))
                //        return true;
                //    else
                //        return false;
                //});

                //-- Sorting rows
                //liJobFileData.Sort();
                //liJobFileData.Reverse();
                //foreach (string name in listOfNames)
                //    Console.WriteLine(name);

                success = true;
                return oJobFileDetails;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", ("Loading Class FileList/Files Failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJobClass(ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Loading the class data for the FileList/Files", sError, "");
                intEventSeq = (intEventSeq + 1);

                success = false;
            }

            if (!success)
            {
                //Class.JobFileDetails oJobFileDetails = new Class.JobFileDetails();

                oJobFileDetails = new Class.JobFileDetails();

                oJobFileDetails.FileIndex = 1;
                oJobFileDetails.Primary = "";
                oJobFileDetails.FileName = "";
                oJobFileDetails.FileType = "";
                oJobFileDetails.XSLPath = "";
                oJobFileDetails.Merge = "";
                oJobFileDetails.NoConvert = "";
                oJobFileDetails.OutputFile = "";
                oJobFileDetails.Error = "ERROR: Failed to load Class file.";
            }

            return oJobFileDetails;
        }

        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        //private bool MergedSentDocumentProcessing(string sFile, int iPageIndex, string sOutputFileName)
        //{
        //    bool success = false;
        //    sOutputFileName = sOutputFileName + "pdf";

        //    //-- This the code that creates a merge of all the files processed during this
        //    //-- Bundle Job.  This is important because we need to upload this into APD to 
        //    //-- show all the files sent to a customer in the Documents/Sent tab of APD
        //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-PDF)", intEventSeq.ToString(), "MERGING ALL Documents into a single PDF", DateTime.Now + " - BundlingJob Sent Documents Merger into PDF Start: Merging the files into 1 - PDF", "OutputFileName: " + sOutputFileName, "sFile: " + sFile);
        //    intEventSeq = (intEventSeq + 1);

        //    try
        //    {
        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-CREATE-PDF)", intEventSeq.ToString(), "Creating Merge PDF", DateTime.Now + " - Adding file: " + sFile, "PageIndex: " + iPageIndex.ToString(), "");
        //        intEventSeq = (intEventSeq + 1);

        //        if (iPageIndex == 0)
        //        {
        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-INIT-PDF)", intEventSeq.ToString(), "Creating the Sent Docs Initial Merge PDF file.", DateTime.Now + " - Index is 0 creating merge PDF.  InitPDF: " + sOutputFileName, "1st File: " + sStagePath + "\\" + sFile, "");
        //            intEventSeq = (intEventSeq + 1);

        //            //-- Create the new output file
        //            //PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "DEBUG 1", intEventSeq.ToString(), "DEBUG 1", DateTime.Now + " - DEBUG 1 File: " + sFile, "", "");
        //            intEventSeq = (intEventSeq + 1);

        //            PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sFile);

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "DEBUG 2", intEventSeq.ToString(), "DEBUG 2", DateTime.Now + " - DEBUG 2 OutputFile: " + sOutputFileName, "", "");
        //            intEventSeq = (intEventSeq + 1);

        //            NewPDFDocument.ImportPage(lDoc1, iPageIndex);

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "DEBUG 3", intEventSeq.ToString(), "DEBUG 3", DateTime.Now + " - DEBUG 3 OutputFile: " + sOutputFileName, "", "");
        //            intEventSeq = (intEventSeq + 1);

        //            NewPDFDocument.Save(sOutputFileName);
        //            lDoc1.Close(true);
        //            NewPDFDocument.Close(true);

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-CREATE-INITAL-PDF-DONE)", intEventSeq.ToString(), "Finished Sent Docs creating the Initial Merge PDF file.", DateTime.Now + " - Finished send docs creating initial PDF.", "1st File: " + sStagePath + "\\" + sFile, "");
        //            intEventSeq = (intEventSeq + 1);
        //        }
        //        else
        //        {
        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-FILE-ADD)", intEventSeq.ToString(), "Add another file to the sent docs merge PDF.", DateTime.Now + " - Index is > 0 so adding another file to the new sent docs merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sStagePath + "\\" + sFile, "");
        //            intEventSeq = (intEventSeq + 1);

        //            //-- Open the existing output file
        //            PdfLoadedDocument srcDoc = new PdfLoadedDocument(sOutputFileName);
        //            //-- Open the next file to be appended
        //            //PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sStagePath + sFile);
        //            PdfLoadedDocument lDoc1 = new PdfLoadedDocument(sFile);
        //            srcDoc.Append(lDoc1);
        //            srcDoc.Save();
        //            srcDoc.Close(true);
        //            lDoc1.Close(true);

        //            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-FILE-ADD-DONE)", intEventSeq.ToString(), "Done adding another file to the sent docs merge PDF.", DateTime.Now + " - Done adding another file to the new sent docs merge PDF.  Merge PDF file: " + sOutputFileName, "Additional File: " + sStagePath + "\\" + sFile, "");
        //            intEventSeq = (intEventSeq + 1);
        //        }
        //        iPageIndex += 1;

        //        success = true;
        //        return success;
        //    }
        //    catch (Exception oExcept)
        //    {
        //        // ------------------------------
        //        //  Email Notify of the Error
        //        // ------------------------------
        //        string sError = "";
        //        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
        //        sError = string.Format("Error: {0}", ("ConvertToPDF Failed: Failed to create sent docs PDF (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

        //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(SENT-DOCS-MERGE-PDF)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Sent docs merger into a single PDF Exception thrown", sError, "");
        //        intEventSeq = (intEventSeq + 1);

        //        success = false;
        //        return success;
        //    }
        //}


    }
}
