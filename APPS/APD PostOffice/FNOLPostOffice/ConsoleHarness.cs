﻿using System;

namespace FNOLPostOffice
{
    public static class ConsoleHarness
    {

        public static void Run(string[] args, IWindowService service)
        {
            string serviceName = service.GetType().Name;
            bool isRunning = true;

            service.OnStart(args);

            while (isRunning)
            {
                WriteToConsole(ConsoleColor.Yellow, "Enter either [Q]uit, [P]ause, [R]esume : ");
                isRunning = HandleConsoleInput(service, Console.ReadLine());
            }


            service.OnStop();
            service.OnShutdown();
        }

        private static bool HandleConsoleInput(IWindowService service, string line)
        {
            bool canContinue = true;

            if (line != null)
            {
                switch (line.ToUpper())
                {
                    case "Q":
                        canContinue = false;
                        break;
                    case "P":
                        service.OnPause();
                        break;
                    case "R":
                        service.OnContinue();
                        break;
                    default:
                        WriteToConsole(ConsoleColor.Red, "Did not understand that input, try again.");
                        break;
                }
            }
            return canContinue;
        }

        private static void WriteToConsole(ConsoleColor foregroundColor, string format, params object[] formatArgs)
        {
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;

            Console.WriteLine(format, formatArgs);
            Console.Out.Flush();

            Console.ForegroundColor = originalColor;
        }
    }
}
