﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using FNOLPostOffice.WebPostInterface;
using System.Net;

namespace FNOLPostOffice
{
    class WebPostJob : BaseJob
    {
        string _clientName = "";
        //private static readonly HttpClient client = new HttpClient();

        public WebPostJob(ref Events events, string jobID, int jobStep, string clientName)
            : base(ref events, jobID, jobStep)
        {
            ProcessJobResult pjr = new ProcessJobResult();

            //--  Logging
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(POST-START)", intEventSeq.ToString(), "WS POST Job Initializing", DateTime.Now + " - Bundling WS PostJob Initialize: Initializing the WS Post for the EmailFaxDocStatUpd and documentFileUpload...", "", "");
            intEventSeq = (intEventSeq + 1);


            //--  Logging
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-BundlingJob(POST-END)", intEventSeq.ToString(), "WS POST Job Ended", DateTime.Now + " - Bundling WS PostJob Complete...", "", "");
            intEventSeq = (intEventSeq + 1);
        }


        public override ProcessJobResult PerformJob(ref XmlDocument jobDetails)
        {
            //--  Logging
            //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(PERFORMJOB)", intEventSeq.ToString(), "WS POST Job Performing", DateTime.Now + " - WS PostJob Perform: Performing the WS Post for the EmailFaxDocStatUpd and documentFileUpload...", "", "");
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(PERFORMJOB)", intEventSeq.ToString(), "WS POST Job Performing", DateTime.Now + " - WS PostJob Perform: Performing the WS Post for the EmailFaxDocStatUpd and documentFileUpload...", jobDetails.InnerXml, "");
            intEventSeq = (intEventSeq + 1);

            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Success = true;
            bool ignoreEnvOv = false;

            XmlNode webPostNode = null;
            string errorInfo = "";

            webPostNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("WEBSERVICE[@stepid='{0}']", _jobStep.ToString()), false);
            pjr.Success = (webPostNode != null);

            _events.Trace("<<<PROCESSING POST JOB>>>Processing POST Job: " + DateTime.Now + " -- Get webPostNode: " + pjr.Success);

            //--  Logging
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(JOBSTEP)", intEventSeq.ToString(), "Job Step Details", DateTime.Now + " - WS PostJob JobDetails: Getting the POST Job Details...", webPostNode.InnerXml, "");
            intEventSeq = (intEventSeq + 1);

            //-- No Node Details
            if (!pjr.Success)
            {
                errorInfo = "No WEBPOST node defined for job step in job details.";

                //--  Logging
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(ERROR)", intEventSeq.ToString(), "ERROR", DateTime.Now + " - ERROR: WS PostJob Failed...", "RC: " + errorInfo, "");
                intEventSeq = (intEventSeq + 1);

                RaiseError(errorInfo);
                JobFailNotification("WEBSERVICE", errorInfo, jobDetails);
            }

            //-- Good Node, process the job details
            if (pjr.Success)
            {
                ignoreEnvOv = FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref webPostNode, "@ignoreenvoverride"));

                if ((_isProduction) || (ignoreEnvOv))
                {
                    //-- Make it so...
                    pjr = PerformWebPost(ref webPostNode, ref jobDetails, _jobStep.ToString());
                }
                else
                {
                    OverrideNotification("WEBSERVICE", jobDetails);
                    pjr.Success = true;
                }
            }

            //-- Check Type of WS Post
            //switch (clientName.ToUpper())
            //{
            //    case "APDPOST":
            //        pjr = PerformJob(ref jobDetails);
            //        bundlingJob = null;

            //        break;
            //    default:
            //        //-- Debug Code --//
            //        _events.Trace("<<<JOB-METHOD>>>Unknown Post Job: " + clientName + " " + DateTime.Now);

            //        // ----------------------------------------
            //        //  Logging
            //        // ----------------------------------------
            //        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(<<<DEFAULT>>>)", intEventSeq.ToString(), "<<<JOB-METHOD>>> Processing DEFAULT Job: ", ">>>>" + DateTime.Now + " - DEFAULT Job Processing...", "", "");
            //        intEventSeq = (intEventSeq + 1);

            //        break;
            //}

            //-- Process the Call to the PerformJob

            //-- Cleanup if needed

            //-- Return results

            return pjr;
        }

        private ProcessJobResult PerformWebPost(ref XmlNode webPostNode, ref XmlDocument jobDetails, string stepID)
        {
            string endPoint = string.Empty;
            string sParams = string.Empty;
            string connInterface = string.Empty;
            string sNotesMsg = string.Empty;

            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Success = true;
            IWebPost webPost = null;
            string failureReason = "";

            endPoint = XmlUtils.GetChildNodeText(ref webPostNode, "EndPoint").Trim();
            connInterface = XmlUtils.GetChildNodeText(ref webPostNode, "Interface").Trim();
            sParams = XmlUtils.GetChildNodeText(ref webPostNode, "Params").Trim();
            //sNotesMsg = XmlUtils.GetChildNodeText(ref webPostNode, "NotesMsg").Trim();
            //sNotesMsg = XmlUtils. GetXMLNodeAttributeValue(webPostNode, "claimAspectID");

            pjr.Success = !string.IsNullOrEmpty(connInterface);

            _events.Trace("<<<PerformWebPost>>>End point and interface: " + DateTime.Now + " -- Get endPoint: " + endPoint + ", Get connInterface: " + connInterface);

            //--  Logging
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(PerformWebPost)", intEventSeq.ToString(), "Performing Web Post", DateTime.Now + " - Performing Web Post...", "endPoint: " + endPoint + ", connInterface: " + connInterface + ", sParams: " + sParams + ", sNotesMsg: " + sNotesMsg, "");
            intEventSeq = (intEventSeq + 1);

            //-- Get the details and HTTP Post it to the URL
            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", endPoint, connInterface));
            var responseValue = string.Empty;

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/xml";

            try
            {
                //if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                //{
                //    CredentialCache credentialCache = new CredentialCache();
                //    credentialCache.Add(new Uri(EndPoint), "Basic", new NetworkCredential(userName, password));
                //    request.Credentials = credentialCache;
                //}

                _events.Trace("start creating the web request");
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    _events.Trace(String.Format("received web response. Received HTTP {0}", response.StatusCode));

                    //--  Logging
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(WebResponse)", intEventSeq.ToString(), "Web Response received", DateTime.Now + " - Received a response back from the Web Post...", "WebResponse: " + response.StatusCode, "");
                    intEventSeq = (intEventSeq + 1);

                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        // grab the response
                        using (var responseStream = response.GetResponseStream())
                            if (responseStream != null)
                                using (var reader = new StreamReader(responseStream))
                                    responseValue = reader.ReadToEnd();
                    }
                    else
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        responseValue = reader.ReadToEnd();

                //--  Logging
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(ERROR)", intEventSeq.ToString(), "ERROR: Response received", DateTime.Now + " - Received an ERROR response back from the Web Post...", "WebErrorResponse: " + wex.Response, "");
                intEventSeq = (intEventSeq + 1);

                pjr.Success = false;
            }
            catch (Exception ex)
            {
                //--  Logging
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(ERROR)", intEventSeq.ToString(), "ERROR: Response received", DateTime.Now + " - Received an ERROR response back from the Web Post...", "WebErrorResponse: " + ex.Message, "");
                intEventSeq = (intEventSeq + 1);

                pjr.Success = false;

                throw ex; 
            }



            if (!pjr.Success)
            {
                // Need to report that interface was not declared.
                failureReason = "Interface for Web Post job was not defined.";

                //--  Logging
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-WebPostJob(ERROR)", intEventSeq.ToString(), "ERROR: Response received", DateTime.Now + " - Received an ERROR response back from the Web Post...", "failureReason: " + failureReason, "");
                intEventSeq = (intEventSeq + 1);
            }

            if (pjr.Success)
            {
                pjr.Success = (!string.IsNullOrEmpty(endPoint));
                if (!pjr.Success)
                {
                    // URL or Endpoint Not defined
                    failureReason = "URL or endpoint Name for web post job was not defined.";
                }

            }

            if (pjr.Success)
            {

                _events.Trace("<<<InstantiateWebPost>>>Instantiate Web Post: " + DateTime.Now + " -- connInterface: " + connInterface + ", jobDetails: " + jobDetails.InnerXml + ", stepID: " + stepID);

                webPost = InstantiateWebPost(connInterface, ref jobDetails, stepID);
                pjr.Success = (webPost != null);
                if (!pjr.Success)
                {
                    failureReason = string.Format("Invalid interface name or unable to instantiate interface: {0}", connInterface);
                }
            }

            if (pjr.Success)
            {
                _events.Trace("<<<PostToService>>>Post To Service: " + DateTime.Now + " -- endPoint: " + endPoint);

                pjr.Success = webPost.PostToService(endPoint, out failureReason);

            }
            if (!pjr.Success)
            {
                JobFailNotification("WEBSERVICE", failureReason, jobDetails);
            }
            if (webPost != null) webPost.Deinitialize();

            return pjr;
        }

        private IWebPost InstantiateWebPost(string webPostInterfaceName, ref XmlDocument jobDetails, string stepID)
        {
            IWebPost wp = null;

            string className = string.Format("FNOLPostOffice.WebPostInterface.{0}WebPost", webPostInterfaceName);
            //string className = string.Format("{0}", webPostInterfaceName);

            _events.Trace("<<<InstantiateWebPost>>>Instantiate Web Post - className: " + DateTime.Now + " -- className: " + className);

            try
            {
                Type type = Type.GetType(className);
                _events.Trace("<<<InstantiateWebPost>>>Instantiate Web Post - className: " + DateTime.Now + " -- className: " + type.Name);
                wp = (IWebPost)Activator.CreateInstance(type);
                _events.Trace("<<<InstantiateWebPost>>>Instantiate Web Post - Activator: " + DateTime.Now);
                wp.Initialize(ref _events, ref jobDetails, stepID);
                _events.Trace("<<<InstantiateWebPost>>>Instantiate Web Post - Events: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                wp = null;
            }

            return wp;
        }
            
    }
}
