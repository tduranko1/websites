﻿using System;

namespace FNOLPostOffice
{
    public interface IWindowService  : IDisposable
    {
        void OnStart(string[] args);

        void OnStop();

        void OnPause();

        void OnContinue();

        void OnShutdown();


    }
}
