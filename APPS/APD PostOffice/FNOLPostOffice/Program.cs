﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace FNOLPostOffice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            using (var implementation = new ServiceImplementation())
            {
                if (Environment.UserInteractive)
                {
                    ConsoleHarness.Run(args, implementation);
                }
                else
                {

                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] 
			            { 
				            new WindowsServiceHarness(implementation)
			            };
                    ServiceBase.Run(ServicesToRun);

                }
            }
        }
    }
}
