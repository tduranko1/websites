﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace FNOLPostOffice
{
    public partial class WindowsServiceHarness : ServiceBase
    {
        private IWindowService _implementer;

        internal WindowsServiceHarness()
        {
            InitializeComponent();
        }

        public WindowsServiceHarness(IWindowService implementation) : this()        
        {
            _implementer = implementation;
        }

        protected override void OnStart(string[] args)
        {
            _implementer.OnStart(args);
        }

        protected override void OnStop()
        {
            _implementer.OnStop();
        }

        protected override void OnPause()
        {
            _implementer.OnPause();
        }

        protected override void OnContinue()
        {
            _implementer.OnContinue();
        }

        protected override void OnShutdown()
        {
            _implementer.OnShutdown();
        }
    }
}
