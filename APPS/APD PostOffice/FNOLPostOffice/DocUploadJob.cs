﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;

using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Linq;

namespace FNOLPostOffice
{
    public class DocUploadJob : BaseJob
    {
        private string _defaultSender = "";

        public DocUploadJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            //-- Initialize
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-DocUpload(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - DocUploadJob Start: Starting the DocUpload process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetailsXML)
        {
            try
            {
                bool success = false;
                long wcfResults = 0;
                string sAppPath = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
                StringBuilder errInfo = new StringBuilder();

                XmlDocument docDocumentUploadXML = new XmlDocument();
                ProcessJobResult pjr = new ProcessJobResult();
                XmlSchemaSet schemas = new XmlSchemaSet();

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(START)", intEventSeq.ToString(), "PROCESSING", DateTime.Now + " - PerformJob Start: Starting the DocUpload process, PerformJob...", "", "JobDetails: " + jobDetailsXML.InnerXml);
                intEventSeq = (intEventSeq + 1);

                try
                {
                    if (jobDetailsXML != null)
                    {
                        //-- Get the DocmentUpload Level details from the XML
                        XmlNode jobDocumentUploadXML = jobDetailsXML.SelectSingleNode("JobDetails/DocumentUpload");

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - Application Path", string.Format("<<<Application Path: {0} >>>", sAppPath), "");
                        intEventSeq = (intEventSeq + 1);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XMLConfigVars)", intEventSeq.ToString(), "XMLConfigVars", DateTime.Now + " - DocumentUpload XML Section", string.Format("<<<XML DocumentUploadXML: {0} >>>", jobDocumentUploadXML.OuterXml), "");
                        intEventSeq = (intEventSeq + 1);

                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XSDSCHEMA)", intEventSeq.ToString(), "XSDSchema", DateTime.Now + " - Validating the JobDetails XML against the DocumentUpload.xsd", string.Format("<<<XSD DocumentUploadXSD: {0} >>>", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd"), "");
                        intEventSeq = (intEventSeq + 1);

                        //ValidateXML("D:\\Apps\\PostOffice\\XSDSchemas\\DocumentUpload.xml", sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");
                        //ValidateXML(jobDocumentUploadXML.OuterXml, sAppPath + "\\XSDSchemas\\DocumentUpload.xsd");

                        //string tmpXML = "<DocumentUpload><LynxID>1846699</LynxID><UserID>0</UserID><PertainsTo>VEH1</PertainsTo><SuppSeqNumber>0</SuppSeqNumber><DocumentSource>Email</DocumentSource><DocumentType>Other</DocumentType><FileName/><FileExtension/><FileLength/><FileData/></DocumentUpload>";
                        wcfResults = wsECADAccessor.PutXML(jobDocumentUploadXML.OuterXml);

                        // ----------------------------------------
                        //  Logging
                        // ----------------------------------------
                        //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(ECADAccessor)", intEventSeq.ToString(), "ECADAccessor.PutXML", DateTime.Now + " - Uploading the document to the server via ECADAccessor...", string.Format("<<<ECADAccessor Response DocumentID: {0} >>>", wcfResults), eventHandler.ToString());
                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(ECADAccessor)", intEventSeq.ToString(), "ECADAccessor.PutXML", DateTime.Now + " - Uploading the document to the server via ECADAccessor...", string.Format("<<<ECADAccessor Response DocumentID: {0} >>>", wcfResults), "");
                        intEventSeq = (intEventSeq + 1);

                        if (wcfResults >= 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                            errInfo.Append("ECADAccessorError: Document failed to upload into server...");
                            RaiseError(errInfo.ToString());
                        }
                    }
                    else
                    {
                        success = false;
                        errInfo.Append("XMLValidationError: No JobDetails XML Found...");
                        RaiseError(errInfo.ToString());
                    }

                    if (!success)
                    {
                        JobFailNotification("EMAIL", errInfo.ToString(), jobDetailsXML);
                    }
                }
                catch (Exception oExcept)
                {
                    // ------------------------------
                    //  Email Notify of the Error
                    // ------------------------------
                    string strError = "";
                    StackFrame FunctionName = new StackFrame();
                    strError = string.Format("Error: {0}", ("DocUploadJob Failed: PerformJob process failed. (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Details: " + strError, "");
                    intEventSeq = (intEventSeq + 1);

                    success = false;
                    return pjr;
                }

                pjr.Method = "UPL";
                pjr.Success = success;

                // ----------------------------------------
                //  Logging
                // ----------------------------------------
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(END)", intEventSeq.ToString(), "ENDING", DateTime.Now + " - PerformJob End: Ending the DocUpload process, PerformJob...", "", "");
                intEventSeq = (intEventSeq + 1);

                return pjr;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("WebServices Failed: ProcessJobResult process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);

                ProcessJobResult pjr = new ProcessJobResult();
                pjr.Method = "UPL";
                pjr.Success = false;
                return pjr;
            }
            finally
            {
            }
        }

        public void ValidateXML(string strXML, string strSchmaPath)
        {
            try
            {
                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XSDVALIDATIONHANDLER)", intEventSeq.ToString(), "XSDValidationHandler", DateTime.Now + " - Validating XML vs XSD.", string.Format("<<<XMLPath: {0}, XSDPath: {1} >>>", strXML, strSchmaPath), "");
                intEventSeq = (intEventSeq + 1);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add("", strSchmaPath);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler += new ValidationEventHandler(XSDValidatedeventHandler);

                //XmlReader rd = XmlReader.Create(strXMLpath, settings);
                XmlReader rd = XmlReader.Create(new System.IO.StringReader(strXML), settings);
                XmlDocument doc = new XmlDocument();
                doc.Load(rd);

                while (rd.Read()) ;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-ValidateXML: ValidateXML process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);
            }
         }

        public void XSDValidatedeventHandler(object sender, ValidationEventArgs e)
        {
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-XSDValidatedeventHandler(XSDERRORHANDLER)", intEventSeq.ToString(), "XSDValidatedeventHandler", DateTime.Now + " - an XSD Error was thrown.", string.Format("<<<XSDError: {0} >>>", e.Message.Replace("'", "''")), "");
            intEventSeq = (intEventSeq + 1);

            try
            {
                if (e.Severity == XmlSeverityType.Warning)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XSDWARNING)", intEventSeq.ToString(), "XSDSchemaValidationWarning", DateTime.Now + " - a Warning was thrown while validating the DocumentUpload XML against the DocumentUpload.xsd", string.Format("<<<Validation Warning Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                }
                else if (e.Severity == XmlSeverityType.Error)
                {
                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-PerformJob(XSDERROR)", intEventSeq.ToString(), "XSDSchemaValidationError", DateTime.Now + " - an Error was thrown while validating the DocumentUpload XML against the DocumentUpload.xsd", string.Format("<<<Validation Error Results: {0} >>>", e.Message.Replace("'", "''")), "");
                    intEventSeq = (intEventSeq + 1);
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-XSDValidatedeventHandler: ValidateXMLeventHandler process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                intEventSeq = (intEventSeq + 1);
            }
        }
    }
}
