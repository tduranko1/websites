﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.SiteUtilities;
using System.Runtime.InteropServices;


namespace FNOLPostOffice
{
    public class FaxMonitor
    {
        private Events _events = null;
        private bool _serviceRunning = false;
        private FNOLDataAccess _dataAccess = null;
        private ManualResetEvent _mre = null;
        private string _configFile = "";
        private int _dom = 0;
        private int _sleepTimeBetweenPasses = 300000;

        public FaxMonitor(string config, ManualResetEvent mre)
        {
            _configFile = config;
            _mre = mre;
            _dom = DateTime.Now.Day;
            InitializeEventsObject();

        }

        private void InitializeEventsObject()
        {
            _events = new Events();
            _events.ComponentInstance = "FaxMonitor";
            _events.Initialize(_configFile, true);
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);

            try
            {
                string tmp = _events.cSettings.GetParsedSetting("FaxMonitorSleepTimeBetweenPasses");
                _sleepTimeBetweenPasses = int.Parse(tmp);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                _sleepTimeBetweenPasses = 300000;
            }

        }

        public void StopProcessing()
        {
            _events.Trace("Received Stop Signal..");
            _serviceRunning = false;
        }

        public void StartProcessing()
        {
            DataTable dt = null;
            _serviceRunning = true;
            int cdom = 0;

            while (_serviceRunning)
            {
                cdom = DateTime.Now.Day;
                if (cdom != _dom)
                {
                    // Day has changed restart logs
                    _events.Dispose();
                    _dataAccess = null;
                    _events = null;
                    GC.Collect(2);
                    InitializeEventsObject();
                    _dom = cdom;
                }
                dt = GetListOfPendingFaxes();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ProcessPendingList(ref dt);
                    }
                }
                if (_events != null) _events.DumpLogBufferToDisk();
                Thread.Sleep(_sleepTimeBetweenPasses);
            }
            _events.Dispose();
            _events = null;
            //signal that we're done processing any jobs.
            if (_mre != null) _mre.Set();
        }

        private DataTable GetListOfPendingFaxes()
        {
            DataTable dt = null;
            try
            {
                NameValueCollection p = new NameValueCollection();
                p.Add("CallingServer", Environment.MachineName.ToUpper());
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("PostOfficePendingFaxes", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = null;
            }
            return dt;
        }

        private void ProcessPendingList(ref DataTable dt)
        {
            System.Collections.IEnumerator ptr = dt.Rows.GetEnumerator();
            DataRow dr = null;
            string jobID = "";
            string jobStep = "";
            string faxServer = "";
            int faxHandle = 0;
            PendingFaxStatus pfs = null;

            while (ptr.MoveNext())
            {
                dr = (DataRow)ptr.Current;
                jobID = Convert.ToString(dr["JobID"]);
                jobStep = Convert.ToInt32(dr["StepID"]).ToString();
                faxHandle = Convert.ToInt32(dr["FaxHandle"]);
                faxServer = Convert.ToString(dr["FaxServer"]);

                pfs = GetPendingFaxStatus(faxServer, faxHandle);
                if (pfs != null)
                {
                    UpdateJobStatus(jobID, jobStep, pfs);
                }
            }
        }

        private PendingFaxStatus GetPendingFaxStatus(string faxServer, int faxHandle)
        {
            PendingFaxStatus pfs = null;
            RFCOMAPILib.FaxServerClass fsc = new RFCOMAPILib.FaxServerClass();
            RFCOMAPILib.Fax fax = null;

            try
            {
                fsc.ServerName = faxServer;
                fsc.Protocol = RFCOMAPILib.CommunicationProtocolType.cpSecTCPIP;
                fsc.AuthorizationUserID = "FNOLFX";
                fsc.AuthorizationUserPassword = "FnolFX2017";
                fsc.UseNTAuthentication = RFCOMAPILib.BoolType.False;
                fsc.OpenServer();

                _events.Trace(string.Format("FaxServer: {0} open", faxServer));
                fax = fsc.get_Fax(faxHandle);
                if (fax == null)
                {
                    throw new Exception("Unable to aquire fax object from fax server");
                }

                _events.Trace(string.Format("Faxhandle: {0} status:{1}", faxHandle, fax.FaxStatus.ToString()));
                pfs = new PendingFaxStatus();
                pfs.JobStatusDetail = fax.StatusDescription;
                switch (fax.FaxStatus)
                {
                    case RFCOMAPILib.FaxStatusType.fsDoneOK:
                        //pfs.Delivered = true;
                        pfs.JobStatus = "Completed";
                        break;
                    case RFCOMAPILib.FaxStatusType.fsDoneError:
                        //pfs.Delivered = false;
                        pfs.JobStatus = "Failed";
                        break;
                    /*
                    case RFCOMAPILib.FaxStatusType.fsInSchedule:
                        pfs.JobStatus = "Scheduled";
                        break;
                    case RFCOMAPILib.FaxStatusType.fsInSend:
                        pfs.JobStatus = "In Send";
                        break;
                    case RFCOMAPILib.FaxStatusType.fsInValidation:
                        pfs.JobStatus = "In Validation";
                        break;                    
                    */
                    default:
                        //pfs.Delivered = false;
                        pfs.JobStatus = "Submitted to Fax Server";
                        break;
                }
                fsc.CloseServer();

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, string.Format("Fax Server = {0}   Fax Handle = {1}", faxServer, faxHandle.ToString()), false);
            }
            finally
            {
                Marshal.ReleaseComObject(fsc);
            }



            return pfs;
        }

        private void UpdateJobStatus(string jobID, string jobStep, PendingFaxStatus pfs)
        {
            NameValueCollection p = new NameValueCollection();
            p.Add("JobID", jobID);
            p.Add("StepID", jobStep);
            p.Add("JobStatus", pfs.JobStatus);
            p.Add("JobStatusDetail", pfs.JobStatusDetail);

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeMarkFaxStatus", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, string.Format("Job ID = {0}  Step ID={1}  Fax Status = {2}", jobID, jobStep, pfs.ToString()), false);
            }

        }


        internal class PendingFaxStatus
        {

            internal PendingFaxStatus() { }

            public string JobStatus;
            public string JobStatusDetail;
            //public bool Delivered;            
        }
    }
}
