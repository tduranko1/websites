﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared.SiteUtilities;
using PGW.Shared;

namespace FNOLPostOffice.WebPostInterface
{
    //string urlOrEndPointName, ref Events events, ref XmlNode filelistNode, ref Dictionary<string,string> parameters, out string failureReason
    public class APDPost : IWebPost
    {
        private static string _encryptionKey = "Lynx@CM!";

        private Events _events = null;
        private string _claimNumber = "";
        private XmlNode _filelistNode = null;
        private XmlNode _webPostNode = null;

        private string _userName = "";
        private string _password = "";

        public void Initialize(ref Events events, ref XmlDocument jobDetails, string stepID)
        {
            BaseJob oBase = new BaseJob(ref events, "xxx1", Int32.Parse(stepID));

            //--  Logging
            oBase.LocalLogEvent(oBase.PROCESSINGSERVER, oBase.strEventTransactionID, "PostOffice-BundlingJob(WS-START)", oBase.intEventSeq.ToString(), "WS Job Initializing", DateTime.Now + " - Bundling WS PostJob Initialize: Initializing the WS Post for the EmailFaxDocStatUpd and documentFileUpload...", "", "");
            oBase.intEventSeq = (oBase.intEventSeq + 1);

            string tmp = string.Empty;
            _events = events;
            _claimNumber = XmlUtils.GetChildNodeText(ref jobDetails, "@claimnumber");
            _filelistNode = XmlUtils.GetChildNode(ref jobDetails, "FileList");
            _webPostNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("WEBSERVICE[@stepid='{0}']", stepID), true);

            _userName = XmlUtils.GetChildNodeText(ref _webPostNode, "UserName").Trim();
            tmp = XmlUtils.GetChildNodeText(ref _webPostNode, "Password").Trim();

            if (tmp.Length > 0) _password = FPOUtils.Decrypt(_encryptionKey, tmp);

            GenUtils.Assert((_filelistNode != null), "FileList not present in JobDetail definition");
        }

        public void Deinitialize()
        {
            _events = null;
            _filelistNode = null;
            _webPostNode = null;
        }

        public bool PostToService(string urlOrEndPointName, out string failureReason)
        {
            bool success = false;
            string postResponse = "";
            int postingFails = 0;
            string reason = "";

            try
            {

                postResponse = "SUCCESS";

                success = (postResponse == "SUCCESS");
                failureReason = (success ? "" : reason);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                failureReason = ex.Message;
                postingFails += 1;
            }

            return success;
        }
    }
}
