﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.WebPostInterface
{
    //string urlOrEndPointName, ref Events events, ref XmlNode filelistNode, ref Dictionary<string,string> parameters, out string failureReason
    interface IWebPost
    {
        void Initialize(ref Events events, ref XmlDocument jobDetails, string stepID);
        void Deinitialize();
        bool PostToService(string urlOrEndPointNamem, out string failureReason);
    }
}
