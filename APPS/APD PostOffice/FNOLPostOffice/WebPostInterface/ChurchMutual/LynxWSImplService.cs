﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5485
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 

namespace FNOLPostOffice.WebPostInterface.ChurchMutual
{

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="LynxWSImplServiceSoapBinding", Namespace="http://lynxws.churchmutual.com/")]
public partial class LynxWSImplService : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    private System.Threading.SendOrPostCallback sendWCClaimToCMICOperationCompleted;
    
    /// <remarks/>
    public LynxWSImplService() {
        this.Url = "http://localhost:9080/lynxwebservice/LynxWS";
    }
    
    /// <remarks/>
    public event sendWCClaimToCMICCompletedEventHandler sendWCClaimToCMICCompleted;
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://lynxws.churchmutual.com/", ResponseNamespace="http://lynxws.churchmutual.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("result", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string sendWCClaimToCMIC([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] sendWCClaimToCMICDocFileInput docFileInput, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] out string reason) {
        object[] results = this.Invoke("sendWCClaimToCMIC", new object[] {
                    docFileInput});
        reason = ((string)(results[1]));
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginsendWCClaimToCMIC(sendWCClaimToCMICDocFileInput docFileInput, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("sendWCClaimToCMIC", new object[] {
                    docFileInput}, callback, asyncState);
    }
    
    /// <remarks/>
    public string EndsendWCClaimToCMIC(System.IAsyncResult asyncResult, out string reason) {
        object[] results = this.EndInvoke(asyncResult);
        reason = ((string)(results[1]));
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public void sendWCClaimToCMICAsync(sendWCClaimToCMICDocFileInput docFileInput) {
        this.sendWCClaimToCMICAsync(docFileInput, null);
    }
    
    /// <remarks/>
    public void sendWCClaimToCMICAsync(sendWCClaimToCMICDocFileInput docFileInput, object userState) {
        if ((this.sendWCClaimToCMICOperationCompleted == null)) {
            this.sendWCClaimToCMICOperationCompleted = new System.Threading.SendOrPostCallback(this.OnsendWCClaimToCMICOperationCompleted);
        }
        this.InvokeAsync("sendWCClaimToCMIC", new object[] {
                    docFileInput}, this.sendWCClaimToCMICOperationCompleted, userState);
    }
    
    private void OnsendWCClaimToCMICOperationCompleted(object arg) {
        if ((this.sendWCClaimToCMICCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.sendWCClaimToCMICCompleted(this, new sendWCClaimToCMICCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    public new void CancelAsync(object userState) {
        base.CancelAsync(userState);
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://lynxws.churchmutual.com/")]
public partial class sendWCClaimToCMICDocFileInput {
    
    private byte[] docFileField;
    
    private string fileNameField;
    
    private long fileSizeField;
    
    private string claimNumberField;
    
    private string docTypeField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="base64Binary")]
    public byte[] docFile {
        get {
            return this.docFileField;
        }
        set {
            this.docFileField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string fileName {
        get {
            return this.fileNameField;
        }
        set {
            this.fileNameField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public long fileSize {
        get {
            return this.fileSizeField;
        }
        set {
            this.fileSizeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string claimNumber {
        get {
            return this.claimNumberField;
        }
        set {
            this.claimNumberField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string docType {
        get {
            return this.docTypeField;
        }
        set {
            this.docTypeField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void sendWCClaimToCMICCompletedEventHandler(object sender, sendWCClaimToCMICCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class sendWCClaimToCMICCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal sendWCClaimToCMICCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[0]));
        }
    }
    
    /// <remarks/>
    public string reason {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[1]));
        }
    }
}
}