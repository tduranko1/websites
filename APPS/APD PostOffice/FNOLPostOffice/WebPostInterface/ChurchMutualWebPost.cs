﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using FNOLPostOffice.WebPostInterface.ChurchMutual;

namespace FNOLPostOffice.WebPostInterface
{
    public class ChurchMutualWebPost : IWebPost
    {

        private static string _encryptionKey = "Lynx@CM!";

        private Events _events = null;
        private string _claimNumber = "";
        private XmlNode _filelistNode = null;
        private XmlNode _webPostNode = null;

        private string _userName = "";
        private string _password = "";


        public ChurchMutualWebPost()
        {
           
        }




        #region IWebPost Members
        public void Initialize(ref Events events, ref XmlDocument jobDetails, string stepID)
        {
            string tmp = string.Empty;
            _events = events;
            _claimNumber = XmlUtils.GetChildNodeText(ref jobDetails, "@claimnumber");
            _filelistNode = XmlUtils.GetChildNode(ref jobDetails, "FileList");
            _webPostNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("WEBSERVICE[@stepid='{0}']", stepID), true);

            _userName = XmlUtils.GetChildNodeText(ref _webPostNode, "UserName").Trim();
            tmp = XmlUtils.GetChildNodeText(ref _webPostNode, "Password").Trim();

            if (tmp.Length > 0) _password = FPOUtils.Decrypt(_encryptionKey, tmp);


            GenUtils.Assert((_filelistNode != null), "FileList not present in JobDetail definition");
        }

        public void Deinitialize()
        {
            _events = null;
            _filelistNode = null;
            _webPostNode = null;
        }


        //ref XmlNode filelistNode, ref Dictionary<string,string> parameters,
        public bool PostToService(string urlOrEndPointName, out string failureReason)
        {
           
            XmlNode fileNode = null;
            string sourceFile = "";
            string fileName = "";
            sendWCClaimToCMICDocFileInput doc = null;
            string reason = "";
            string postResponse = "";
            int postingFails = 0;
            bool success = false;            
                      

            LynxWSImplService svc = new LynxWSImplService();           

            svc.Url = urlOrEndPointName;

            if (!string.IsNullOrEmpty(_userName))
            {
                NetworkCredential nc = new NetworkCredential(_userName, _password);
                Uri uri =  new Uri(svc.Url);
                ICredentials credentials = nc.GetCredential(uri, "Basic");
                svc.Credentials = credentials;                
                _events.Trace(string.Format("Adding credentials: User = '{0}'", _userName));

                
            }

            
                    
            fileNode = XmlUtils.GetChildNode(ref _filelistNode,"File");
            sourceFile = XmlUtils.GetChildNodeText(ref fileNode, "@source");
            fileName = XmlUtils.GetChildNodeText(ref fileNode, "@filename");

            _events.Trace(string.Format("Posting file {0} to {1}",fileName,urlOrEndPointName));

            try
            {
                FileInfo fi = new FileInfo(sourceFile);
                _events.Trace(string.Format("Claim #{0} | FileName: {1}  | FileSize: {2}", _claimNumber, fi.Name, fi.Length.ToString()));
                doc = new sendWCClaimToCMICDocFileInput();
                doc.claimNumber = _claimNumber;
                doc.docType = "PDF";
                doc.fileName = fi.Name;
                doc.fileSize = fi.Length;

                byte[] buffer = new byte[fi.Length];

                using (FileStream fs = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read))
                {
                    fs.Read(buffer, 0, (int)fi.Length);
                }
                //string b64str = Convert.ToBase64String(buffer);

                //_events.Trace(string.Format("File Data: {0}", b64str));

                doc.docFile = buffer;  // System.Text.Encoding.UTF8.GetBytes(b64str);
                postResponse = svc.sendWCClaimToCMIC(doc, out reason);


                success = (postResponse == "SUCCESS");
                failureReason = (success ? "" : reason);

                _events.Trace(string.Format("Posting Response: {0} - {1}", postResponse, reason));
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                failureReason = ex.Message;
                postingFails += 1;
            }


            return success;
        }


        #endregion
    }


    public class AuthHeader : SoapHeader
    {
        public string Username;
        public string Password;
    }
}
