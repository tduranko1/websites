﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNOLPostOffice.Class
{
    class JobFileDetails
    {
        public int FileIndex;
        public string Primary;
        public string Source;
        public string FileName;
        public string FileType;
        public string XSLPath;
        public string Merge;
        public string NoConvert;
        public string OutputFile;
        public string Error;
    }
}
