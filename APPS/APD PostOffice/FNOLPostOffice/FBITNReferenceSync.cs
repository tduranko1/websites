﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.SiteUtilities;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Net;
using System.Xml.Xsl;


namespace FNOLPostOffice
{
    class FBITNReferenceSync
    {
        private Events _events = null;
        private ManualResetEvent _mre = null;
        private FNOLDataAccess _dataAccess = null;
        private string _configFile = string.Empty;
        private int _dom = 0;
        private int _countDays = 0;
        private bool _serviceRunning = false;
        private string _styleSheetLoc = string.Empty;
        private string _clientID = string.Empty;

        RestClient _restClient = null;
        XmlDocument cxmlConfig = new XmlDocument();

        public FBITNReferenceSync(string config, ManualResetEvent mre)
        {
            _configFile = config;
            _dom = DateTime.Now.Day;
            _mre = mre;
            InitializeEventsObject();
        }

        private void InitializeEventsObject()
        {
            _events = new Events();
            _events.ComponentInstance = "FBITNReferenceSync";
            _events.Initialize(_configFile, true);
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);
        }

        public void StartProcessing()
        {
            _serviceRunning = true;
            int cdom = 0;

            try
            {
                while (_serviceRunning)
                {
                    cdom = DateTime.Now.Day;
                    if (cdom != _dom)
                    {
                        // Day has changed restart logs
                        _events.Dispose();
                        _dataAccess = null;
                        _events = null;
                        GC.Collect(2);
                        InitializeEventsObject();
                        _dom = cdom;
                        _countDays += 1;
                    }

                    _clientID = _events.cSettings.GetParsedSetting("//FBITNReferenceSync/@clientid");

                    bool syncReference = IsSyncFBITN();

                    if (syncReference)
                    {
                        _styleSheetLoc = _events.cSettings.GetParsedSetting("FBITNReferenceSync/StyleSheet/@url");

                        if (!string.IsNullOrEmpty(_styleSheetLoc) && !string.IsNullOrEmpty(_clientID))
                        {
                            _countDays = 0;
                            ProcessFBITNCauseofLossValues();
                            ProcessFBITNReferenceValues();
                        }
                    }

                    _events.DumpLogBufferToDisk();

                    //Day * Milliseconds *  Minutes
                    Thread.Sleep(1*60000*10);                    
                }
                _events.Dispose();
                _events = null;

                //signal that we're done processing any jobs.
                if (_mre != null) _mre.Set();
            }
            catch (Exception ex) { _events.HandleEvent(ex, false); }
        }

        public void StopProcessing()
        {
            _events.Trace("Received Stop Signal..");
            _serviceRunning = false;
        }

        private bool IsSyncFBITN()
        {
            DataTable dtLastSync = null;
            int daysCount = 0, 
                RefDayCount=0, 
                RefCOLDayCount=0;
            bool isCorrectTime = false,
                isAlreadyExec = false;
            string Count = string.Empty,
                systime = string.Empty,
                configtime = string.Empty,
                timeformat = string.Empty,
                dayofWeek = string.Empty,
                enabled = "1";

            try
            {
                enabled = _events.cSettings.GetParsedSetting("FBITNReferenceSync/@enabled");

                if (enabled == "0") return false;

                enabled = _events.cSettings.GetParsedSetting("FBITNReferenceSync/Weekly/@enabled");

                dtLastSync = new DataTable();
                dtLastSync = GetLastReferenceSyncDate("FBITNReferenceSyncLastDate");

                if (dtLastSync != null && dtLastSync.Rows.Count > 0)
                {
                    TimeSpan tsReference = DateTime.Now.Date - Convert.ToDateTime(dtLastSync.Rows[0]["ReferenceLastUpdate"]).Date;
                    TimeSpan tsReferenceCauseofLoss = DateTime.Now.Date - Convert.ToDateTime(dtLastSync.Rows[0]["ReferenceCauseofLossLastUpdate"]).Date;
                    
                    RefDayCount = Convert.ToInt32(tsReference.TotalDays);
                    RefCOLDayCount = Convert.ToInt32(tsReferenceCauseofLoss.TotalDays);

                    isAlreadyExec = RefDayCount == 0 || RefCOLDayCount == 0;

                    // If enabled is equal to zero then it will execute based on day(s) count else it will execute based on DayofWeek.
                    if (enabled == "0")
                    {
                        Count = _events.cSettings.GetParsedSetting("FBITNReferenceSync/SleepTime/@count");
                        configtime = _events.cSettings.GetParsedSetting("FBITNReferenceSync/SleepTime/@synctime");
                        timeformat = _events.cSettings.GetParsedSetting("FBITNReferenceSync/SleepTime/@timeformat").ToUpper();

                        if (string.IsNullOrEmpty(Count) || Count == "0" || string.IsNullOrEmpty(configtime) || string.IsNullOrEmpty(timeformat)) return false;

                        daysCount = Convert.ToInt32(Count);

                        systime = DateTime.Now.ToString("h tt").ToUpper();
                        isCorrectTime = systime.Equals(configtime + " " + timeformat);

                        return (daysCount <= RefCOLDayCount || daysCount <= RefDayCount) && isCorrectTime;
                    }
                    else
                    {
                        dayofWeek = _events.cSettings.GetParsedSetting("FBITNReferenceSync/Weekly/@day");
                        configtime = _events.cSettings.GetParsedSetting("FBITNReferenceSync/Weekly/@synctime");
                        timeformat = _events.cSettings.GetParsedSetting("FBITNReferenceSync/Weekly/@timeformat").ToUpper();

                        if (string.IsNullOrEmpty(dayofWeek) || string.IsNullOrEmpty(configtime) || string.IsNullOrEmpty(timeformat)) return false;

                        systime = DateTime.Now.ToString("h tt").ToUpper();
                        isCorrectTime = systime.Equals(configtime + " " + timeformat);

                        return (dayofWeek.ToLower().Equals(DateTime.Now.DayOfWeek.ToString().ToLower())) && isCorrectTime && !isAlreadyExec;
                    }
                }
            }
            catch (Exception ex) { throw ex; }

            return false;
        }

        private void ProcessFBITNReferenceValues()
        {
            string referenceTableLists = string.Empty, procedureName = string.Empty;

            referenceTableLists = _events.cSettings.GetParsedSetting("//FBITNReferenceSync/ReferenceTableLists/@supporttablecode");
            procedureName = "FBITNReferenceTable";

            if (!string.IsNullOrEmpty(referenceTableLists))
                GetValues(referenceTableLists, procedureName);
        }

        private void ProcessFBITNCauseofLossValues()
        {
            string referenceTableLists = string.Empty, procedureName = string.Empty;

            referenceTableLists = _events.cSettings.GetParsedSetting("//FBITNReferenceSync/ReferenceCauseofLossLists/@supporttablecode");
            procedureName = "FBITNReferenceCauseOfLoss";

            if (!string.IsNullOrEmpty(referenceTableLists))
                GetValues(referenceTableLists, procedureName);
        }

        private void GetValues(string strReferenceTableLists, string ProcedureName)
        {
            XmlDocument SupportTableXml = new XmlDocument();
            string _endPointName = string.Empty;
            string jsonResponse = string.Empty, xmlResponse = string.Empty;
            string[] supportTableCodes = null;

            try
            {

                supportTableCodes = strReferenceTableLists.Split('|');
                SupportTableXml.LoadXml("<SupportTables/>");
                _endPointName = _events.cSettings.GetParsedSetting("//FBITNReferenceSync/@url");

                foreach (string supportTableCode in supportTableCodes)
                {
                    string requestUrl = string.Empty;
                    requestUrl = string.Format("{0}{1}", _endPointName, supportTableCode);

                    _restClient = new RestClient(requestUrl, _events);

                    _events.Trace(string.Format("JSON Request : {0}", requestUrl));

                    jsonResponse = _restClient.MakeRequest();

                    _events.Trace(string.Format("JSON Response : {0}", jsonResponse));

                    if (!string.IsNullOrEmpty(jsonResponse))
                    {
                        xmlResponse = JsonToXml(jsonResponse);

                        _events.Trace(string.Format("Transform JsonToXml : {0}", xmlResponse));

                        SupportTableXml.DocumentElement.InnerXml += XsltTransformation(xmlResponse);
                    }
                }


                RemoveReferenceValue(ref SupportTableXml);

                DBInsertUpdateReferenceValue(SupportTableXml.OuterXml, ProcedureName);
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// Check po_config.xml file in <RemoveReference enabled="1"> section.
        /// This method is used to remove unwanted values returned by FBITN system, the values are configured in above section.
        /// </summary>
        /// <param name="SupportTableXml"></param>
        private void RemoveReferenceValue(ref XmlDocument SupportTableXml)
        {
            XmlNode RemoveReference = _events.cSettings.GetParsedNode("FBITNReferenceSync/RemoveReference");

            XmlNodeList RemoveReferenceList = XmlUtils.GetChildNodeList(ref RemoveReference, "//Reference");

            foreach (XmlNode reference in RemoveReferenceList)
            {
                XmlNode temp = reference;
                XmlNodeList RemoveNodeList = null;
                string refitem = XmlUtils.GetChildNodeText(ref temp, "@refitemname");
                string caption = XmlUtils.GetChildNodeText(ref temp, "@caption");
                string shotcap = XmlUtils.GetChildNodeText(ref temp, "@shortcaption");

                if (!string.IsNullOrEmpty(refitem) && !string.IsNullOrEmpty(caption) && !string.IsNullOrEmpty(shotcap))
                    RemoveNodeList = XmlUtils.GetChildNodeList(ref SupportTableXml, string.Format("//Record [@ReferenceItemName='{0}' and @Caption='{1}' and @CaptionShort='{2}']", refitem, caption, shotcap));
                else if (!string.IsNullOrEmpty(refitem) && !string.IsNullOrEmpty(caption))
                    RemoveNodeList = XmlUtils.GetChildNodeList(ref SupportTableXml, string.Format("//Record [@ReferenceItemName='{0}' and @Caption='{1}']", refitem, caption));
                else if (!string.IsNullOrEmpty(refitem) && !string.IsNullOrEmpty(shotcap))
                    RemoveNodeList = XmlUtils.GetChildNodeList(ref SupportTableXml, string.Format("//Record [@ReferenceItemName='{0}' and @CaptionShort='{1}']", refitem, shotcap));

                if (RemoveNodeList != null)
                {
                    foreach (XmlNode RemoveNode in RemoveNodeList)
                        RemoveNode.ParentNode.RemoveChild(RemoveNode);
                }
            }

        }

        private void DBInsertUpdateReferenceValue(string InputXML, string ProcedureName)
        {
            NameValueCollection param = null;

            try
            {
                _events.Trace(string.Format("Database update beginning..."));

                _events.Trace(string.Format("Stored Procedure Name : {0} and InputXML : {1}", ProcedureName, InputXML));

                param = new NameValueCollection();
                param.Add("ClientID", _clientID);
                param.Add("ReferenceValues", InputXML);

                _dataAccess.ExecuteDatabaseTransactionNoReturn(ProcedureName, ref param);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, string.Format("ProcedureName = {0} ; failed reference Sync.", ProcedureName), false);
                throw ex;
            }
        }

        private DataTable GetLastReferenceSyncDate(string ProcedureName)
        {
            NameValueCollection param = null;
            DataTable dtLastSync = null;

            try
            {
                _events.Trace(string.Format("Retrive last update date..."));

                param = new NameValueCollection();
                param.Add("ClientID", _clientID);

                dtLastSync = new DataTable();
                dtLastSync = _dataAccess.ExecuteDatabaseTransactionReturnTable(ProcedureName, ref param);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, string.Format("ProcedureName = {0} ; failed reference Sync.", ProcedureName), false);
                throw ex;
            }

            return dtLastSync;
        }

        private string JsonToXml(string jsonString)
        {
            using (var stream = new MemoryStream(Encoding.Default.GetBytes(jsonString)))
            {
                var quotas = new XmlDictionaryReaderQuotas();
                return Convert.ToString(XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(stream, quotas)));
            }
        }

        private string XsltTransformation(string xmlResponse)
        {
            string result = string.Empty;

            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform(false);
                XmlUrlResolver resolver = new XmlUrlResolver();
                StringBuilder sbXML = new StringBuilder();

                using (XmlWriter writer = XmlWriter.Create(sbXML))
                {
                    xslt.Load(_styleSheetLoc, new XsltSettings(true, true), resolver);
                    using (XmlReader reader = XmlReader.Create(new StringReader(xmlResponse)))
                    {
                        xslt.Transform(reader, writer);
                        result = sbXML.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n", "").Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\n", "").Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                    }
                }
            }
            catch (Exception ex) { throw ex; }

            return result;
        }
    }

    public class RestClient
    {
        public string EndPoint { get; set; }
        public string ContentType { get; set; }
        Events LogEvents = null;

        public RestClient()
        {
            EndPoint = "";
            ContentType = "text/xml";
        }

        public RestClient(string endpoint, Events logevents)
        {
            EndPoint = endpoint;
            LogEvents = logevents;
            ContentType = "text/json";
        }

        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters, string userName = "", string password = "")
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", EndPoint, parameters));
            var responseValue = string.Empty;

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = ContentType;

            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    CredentialCache credentialCache = new CredentialCache();
                    credentialCache.Add(new Uri(EndPoint), "Basic", new NetworkCredential(userName, password));
                    request.Credentials = credentialCache;
                }

                LogEvents.Trace("start creating the json web request");
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    LogEvents.Trace(String.Format("received json response. Received HTTP {0}", response.StatusCode));
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        // grab the response
                        using (var responseStream = response.GetResponseStream())
                            if (responseStream != null)
                                using (var reader = new StreamReader(responseStream))
                                    responseValue = reader.ReadToEnd();
                    }
                    else
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    return responseValue;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        responseValue = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex; ;
            }

            return responseValue;
        }
    }
}