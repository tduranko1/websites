﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice
{
    public class EmailJob : BaseJob
    {
        private string _defaultSender = "";
        private bool bMerge = false;

        public EmailJob(ref Events events, string jobID, int jobStep)
            : base(ref events, jobID, jobStep)
        {
            _defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");

            // ----------------------------------------
            //  Logging
            // ----------------------------------------
            LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(START)", intEventSeq.ToString(), "STARTING", DateTime.Now + " - EmailJob Start: Starting the EmailJob process...", "", "");
            intEventSeq = (intEventSeq + 1);
        }

        public override ProcessJobResult PerformJob(ref XmlDocument jobDetails)
        {
            bool success = false;
            bool ignoreEnvOv = false;
            StringBuilder errInfo = new StringBuilder();
            XmlNode oXMLNode = null;

            XmlNode emailNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("EMAIL[@stepid='{0}']", _jobStep.ToString()), false);

            //-- See if a Bundling tag exists in the XML, if so process based on its specifications.
            //XmlNode bundlingNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("Bundling[@stepid='{0}']", _jobStep.ToString()), false);
            //XmlNode bundlingNode = XmlUtils.GetChildNode(ref jobDetails, string.Format("Bundling[@stepid='{0}']", "1"), false);

            //-- ??????????????? CASE ISSUES ????????????????
            oXMLNode = jobDetails.SelectSingleNode("JobDetails/Bundling");
            string sStep = GetXMLNodeAttributeValue(oXMLNode, "stepid");

            string sNode = string.Format("Bundling[@stepid='{0}']", sStep);
            sNode = sNode + "/Bundle";
            XmlNode bundlingNode = XmlUtils.GetChildNode(ref jobDetails, sNode, false);

            XmlNode fileNode = null;         
            int fileCount = 0;
            Attachment fa = null;
            Attachment fa1 = null;
            string attachmentName = "";
            string attachmentOutputName = string.Empty;
            string attachmentUrl = "";
            string sFileType = string.Empty;

            string smtpServer = _events.cSettings.GetParsedSetting("SMTPServer");
            string defaultSender = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");
            if (emailNode != null)
            {
                ignoreEnvOv = FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref emailNode, "@ignoreenvoverride"));
                fileCount = 0;                
                EmailInfo info = null;
                try
                {
                    info = new EmailInfo(ref emailNode, defaultSender);
                }
                catch (Exception ex)
                {
                    info = null;
                    errInfo.Append(ex.ToString());
                    RaiseError(errInfo.ToString());
                }
                if (info != null)
                {

                    if (info.To.Count == 0)
                    {
                        // No visible recipients
                        success = false;
                        errInfo.Append("No visible email recipients defined for job step.");
                        RaiseError(errInfo.ToString());
                    }
                    else
                    {
                        if ((_isProduction) || (ignoreEnvOv))
                        {
                            try
                            {
                                MailMessage mail = new MailMessage();
                                mail.From = info.Sender;
                                mail.Subject = info.Subject;
                                mail.IsBodyHtml = info.BodyIsHtml;
                                mail.Body = info.Body;
                                foreach (MailAddress ma in info.To)
                                    mail.To.Add(ma);

                                foreach (MailAddress ma in info.CC)
                                    mail.CC.Add(ma);

                                foreach (MailAddress ma in info.BCC)
                                    mail.Bcc.Add(ma);

                                mail.Priority = info.Priority;
                                SmtpClient sc = new SmtpClient(smtpServer);

                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL)", intEventSeq.ToString(), "EMAIL", DateTime.Now + " - Email process started.", string.Format("VARS: Subject: {0}, Step: {1}, EmailNode: {2}", mail.Subject, sStep, emailNode.InnerXml), "");
                                intEventSeq = (intEventSeq + 1);

                                //-- Check if this is a output bundle.  If so process the files differently
                                if (bundlingNode != null)
                                {
                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLINGNODE)", intEventSeq.ToString(), "Bundling Node", DateTime.Now + " - exists, process started.", string.Format("VARS: bundlingNode: {0}, merge: {1}", bundlingNode.InnerXml, XmlUtils.GetChildNodeText(ref bundlingNode, "@merge").ToUpper()), "");
                                    intEventSeq = (intEventSeq + 1);

                                    if (XmlUtils.GetChildNodeText(ref bundlingNode, "@merge").ToUpper() == "YES")
                                    {
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(MERGING)", intEventSeq.ToString(), "Merging Bundle ", DateTime.Now + " - Merging.", "", "");
                                        intEventSeq = (intEventSeq + 1);

                                        bMerge = true;
                                    }
                                    else
                                    {
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(NOT-MERGING)", intEventSeq.ToString(), "Not Merging this Bundle ", DateTime.Now + " - Not Merging.", "", "");
                                        intEventSeq = (intEventSeq + 1);

                                        bMerge = false;
                                    }
                                }

                                //-- Check to decide if we need to merger or not
                                if (bMerge)
                                {
                                    //-- Merge into 1 PDF

                                    //XmlNode oXMLNode = null;
                                    string sIgnoreEnvOverride = string.Empty;
                                    string sPriority = string.Empty;
                                    string sStepID = string.Empty;
                                    string sOutputFileType = string.Empty;
                                    string sOutputPathFileName = string.Empty;
                                    string sOutputFileName = string.Empty;
                                    string sMergeFlag = string.Empty;
                                    string sUNCTarget = string.Empty;
                                    string sElecMerge = string.Empty;

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLING)", intEventSeq.ToString(), "Bundling\\Merge", DateTime.Now + " - FileList will be Merged into 1 file and sent.", "Merge: " + XmlUtils.GetChildNodeText(ref bundlingNode, "@merge"), "");
                                    intEventSeq = (intEventSeq + 1);

                                    //-- Get the Bundling tag details so we know where to process from
                                    //-- Get the Node Level details from the XML
                                    XmlNode jobBundlingUploadXML = jobDetails.SelectSingleNode("JobDetails/Bundling");

                                    //-- Get Bundling Attributes
                                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling");
                                    sIgnoreEnvOverride = GetXMLNodeAttributeValue(oXMLNode, "ignoreenvoverride");
                                    sPriority = GetXMLNodeAttributeValue(oXMLNode, "priority");
                                    sStepID = GetXMLNodeAttributeValue(oXMLNode, "stepid");

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLING_ATT)", intEventSeq.ToString(), "Bundling\\Merge", DateTime.Now + " - Attributes", string.Format("VARS: IgnoreEnvOverride: {0}, Priority: {1}, StepID: {2}", sIgnoreEnvOverride, sPriority, sStepID), "");
                                    intEventSeq = (intEventSeq + 1);

                                    //-- Get Bundling Output Attributes
                                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/Bundle");
                                    sOutputFileType = GetXMLNodeAttributeValue(oXMLNode, "type");
                                    sOutputPathFileName = sStagePath + "\\" + GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                                    sOutputFileName = GetXMLNodeAttributeValue(oXMLNode, "outputfile") + "." + sOutputFileType;
                                    sMergeFlag = GetXMLNodeAttributeValue(oXMLNode, "merge");

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLING_OUTPUT_ATT)", intEventSeq.ToString(), "Bundling\\Merge", DateTime.Now + " - Attributes", string.Format("VARS: OutputFileType: {0}, OutputFileName: {1}, MergeFlag: {2}", sOutputFileType, sOutputFileName, sMergeFlag), "");
                                    intEventSeq = (intEventSeq + 1);

                                    //-- Get ElectBundling Node
                                    oXMLNode = jobBundlingUploadXML.SelectSingleNode("/JobDetails/Bundling/ElectBundling");
                                    sUNCTarget = GetXMLSingleNodeValue(oXMLNode, "UNCTarget") + "\\";
                                    sElecMerge = GetXMLSingleNodeValue(oXMLNode, "merge");

                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLING_OUTPUT_ATT)", intEventSeq.ToString(), "Bundling\\Merge", DateTime.Now + " - Attributes", string.Format("VARS: UNCTarget: {0}, ElecMerge: {1}", sUNCTarget, sElecMerge), "");
                                    intEventSeq = (intEventSeq + 1);

                                    //-- Check and make sure the merged bundling package has been created successfully
                                    if (File.Exists(sOutputPathFileName))
                                    {
                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(BUNDLE_READY)", intEventSeq.ToString(), "Bundling\\Merge\\READY", DateTime.Now + " - Merged Bundle ready to send via Email", string.Format("VARS: OutputFileName: {0}", sOutputFileName), "");
                                        intEventSeq = (intEventSeq + 1);

                                        //-- Attach the merged file into the email.
                                        GenUtils.Assert(File.Exists(sOutputPathFileName), string.Format("Can not find file '{0}' to attach to email for job id {1}  step {2}", sOutputPathFileName, "", sStepID));

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(FILENAME_OVERRIDE)", intEventSeq.ToString(), "Bundling\\FileName\\Override", DateTime.Now + " - Override the file name?", string.Format("VARS: FileNameOverride: {0}", info.FileNameOverridePrimary), "");
                                        intEventSeq = (intEventSeq + 1);

                                        if (info.FileNameOverridePrimary)
                                        {
                                            if (FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref fileNode, "@primary")))
                                                attachmentName = info.FileNameOverride;
                                            else if (info.FileNameOverrideAdditionalFiles)
                                            {
                                                fileCount += 1;
                                                string tmp = info.FileNameOverride.Replace(info.FileNameOverrideExt, "");
                                                attachmentName = string.Format("{0}_{1}{2}", tmp, fileCount.ToString(), info.FileNameOverrideExt);
                                            }
                                        }

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_ATTACH)", intEventSeq.ToString(), "Bundling\\MergeFile\\Attachment", DateTime.Now + " - Attaching merged bundle to email", string.Format("VARS: OutputPathFileName: {0}, FileName {1}", sOutputPathFileName, sOutputFileName), "");
                                        intEventSeq = (intEventSeq + 1);

                                        fa = new Attachment(sOutputPathFileName);
                                        fa.Name = sOutputFileName;
                                        mail.Attachments.Add(fa);

                                        //-- Clean up stage area

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_ATTACH_COMPLETE)", intEventSeq.ToString(), "Bundling\\MergeFile\\Attachment\\Complete", DateTime.Now + " - Attaching merged bundle to email Complete", "", "");
                                        intEventSeq = (intEventSeq + 1);
                                    }
                                    else
                                    {
                                        throw new Exception(string.Format("Merge file source path, '{0}', for attachment is not a UNC path nor a local or mapped file path.", sOutputFileName));
                                    }
                                }
                                else
                                {
                                    //-- Direct send as in doc proc job file
                                    //-- but as PDF's
                                    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_NORMAL)", intEventSeq.ToString(), "Email\\Normal", DateTime.Now + " - Normal email processing started", "", "");
                                    intEventSeq = (intEventSeq + 1);

                                    IEnumerator fptr = XmlUtils.GetChildNodeList(ref jobDetails, "FileList/File").GetEnumerator();
                                    while (fptr.MoveNext())
                                    {
                                        fileNode = (XmlNode)fptr.Current;
                                        attachmentUrl = XmlUtils.GetChildNodeText(ref fileNode, "@source");
                                        attachmentName = XmlUtils.GetChildNodeText(ref fileNode, "@filename");
                                        attachmentOutputName = XmlUtils.GetChildNodeText(ref fileNode, "@outputfilename");
                                        sFileType = XmlUtils.GetChildNodeText(ref fileNode, "@filetype");


                                        _events.Trace(string.Format("Attaching file: {0}", attachmentUrl));

                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_NORMAL_FILE_ATT)", intEventSeq.ToString(), "Email\\Normal\\File\\Attributes", DateTime.Now + " - Attributes", string.Format("VARS: fileNode: {0}, attachmentUrl: {1}, attachmentName: {2}, jobID: {3}, jobStep: {4}", fileNode.InnerText, attachmentUrl, attachmentName, _jobID, _jobStep), "");
                                        intEventSeq = (intEventSeq + 1);

                                        if (!(attachmentUrl.StartsWith(@"\\") || attachmentUrl.Substring(1, 2) == @":\"))
                                        {
                                            // source is not UNC path or local/mapped filepath
                                            throw new Exception(string.Format("source path, '{0}', for attachment is not a UNC path nor a local or mapped file path.", attachmentUrl));
                                        }
                                        else
                                        {
                                            GenUtils.Assert(File.Exists(attachmentUrl), string.Format("Can not find file '{0}' to attach to email for job id {1}  step {2}", attachmentUrl, _jobID, _jobStep));
                                        }

                                        if (info.FileNameOverridePrimary)
                                        {
                                            if (FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref fileNode, "@primary")))
                                                attachmentName = info.FileNameOverride;
                                            else if (info.FileNameOverrideAdditionalFiles)
                                            {
                                                fileCount += 1;
                                                string tmp = info.FileNameOverride.Replace(info.FileNameOverrideExt, "");
                                                attachmentName = string.Format("{0}_{1}{2}", tmp, fileCount.ToString(), info.FileNameOverrideExt);
                                            }
                                        }

                                        fa = new Attachment(attachmentUrl);
                                        fa.Name = attachmentOutputName;
                                        mail.Attachments.Add(fa);

                                        ////-- If not Merged, attach the PDF's
                                        //if (bMerge == false)
                                        //{
                                        //    LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(NON-MERGE-PDF-ATTACH)", intEventSeq.ToString(), "Not merged attach PDF", DateTime.Now + " - Attach PDF to Email", string.Format("VARS: sStagePath: {0}, attachmentOutputName: {1}, sFileType: {2}", sStagePath, attachmentOutputName.Replace("htm", "pdf"), sFileType), "");
                                        //    intEventSeq = (intEventSeq + 1);

                                        //    if (attachmentOutputName.ToUpper().Contains("HTM"))
                                        //    {
                                        //        fa1 = new Attachment(attachmentUrl);
                                        //        fa1.Name = sStagePath + attachmentOutputName.Replace("htm", "pdf");
                                        //        mail.Attachments.Add(fa1);
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    fa = new Attachment(attachmentUrl);
                                        //    fa.Name = attachmentOutputName;
                                        //    mail.Attachments.Add(fa);
                                        //}


                                        LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_NORMAL_ATT_ADD)", intEventSeq.ToString(), "Email\\Normal\\Att\\Add", DateTime.Now + " - attachment ADD processing started", "FileName: " + fa.Name, "");
                                        intEventSeq = (intEventSeq + 1);

                                    }
                                }

                                //-- Continue processing
                                LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "PostOffice-EmailJob(EMAIL_SEND)", intEventSeq.ToString(), "Email\\Send", DateTime.Now + " - Sending the email", "", "");
                                intEventSeq = (intEventSeq + 1);

                                _events.Trace(string.Format("Sending email for job id {0}  step {1}", _jobStep, _jobStep.ToString()));
                                sc.Send(mail);
                                success = true;
                            }
                            catch (Exception ex)
                            {
                                errInfo.Append(ex.ToString());
                                JobFailNotification("Email", ex.ToString(), jobDetails);
                                _events.HandleEvent(ex, false);
                                success = false;
                            }
                        }
                        else if (!_isProduction)
                        {
                            //Notification of destination overrid(suppression) due to environment.
                            _events.Trace("Enivornment Override Enforced.");
                            OverrideNotification("EMAIL", jobDetails);
                            success = true;
                        }
                    }
                }

            }
            else
            {
                success = false;
                errInfo.Append("Email node for job step not present in job details.");
                RaiseError(errInfo.ToString());
            }

            ProcessJobResult pjr = new ProcessJobResult();
            pjr.Method = "EMAIL";
            pjr.Success = success;
            
            if (!success)
            {
                JobFailNotification("EMAIL", errInfo.ToString(), jobDetails);
            }

            return pjr;
        }

    }


    public class EmailInfo
    {
        private XmlNode _emailNode = null;
        private MailAddressCollection _mainRecipients = new MailAddressCollection();
        private MailAddressCollection _ccRecipients = new MailAddressCollection();
        private MailAddressCollection _bccRecipients = new MailAddressCollection();
        private string _defaultSender = "";
        private MailPriority _priority = MailPriority.Normal;

        public EmailInfo(ref XmlNode emailNode, string defaultSender) {
            _emailNode = emailNode;
            _defaultSender = defaultSender;
            IEnumerator ptr = XmlUtils.GetChildNodeList(ref emailNode, "Recipient").GetEnumerator();
            XmlNode rec = null;
            string tmp = XmlUtils.GetChildNodeText(ref _emailNode, "@priority").Trim().ToLower();
            int p;
            if (int.TryParse(tmp, out p))
            {
                if (p < 5) _priority = MailPriority.High;
                if (p > 5) _priority = MailPriority.Low;
            }

            while (ptr.MoveNext())
            {
                rec = (XmlNode)ptr.Current;
                string recType = XmlUtils.GetChildNodeText(ref emailNode, "@type").ToLower();
                //there have been some cases where addresses recevied from certain sources actually contains multiple addresses divided by either spaces or commas
                //instead of a semi-colon.  So we need to convert these so the list is a semi-colon delimited list before processing.
                string recipTmp = rec.InnerText.Trim().Replace("  ", "").Replace(' ', ';').Replace(',',';');

                string[] recipients = recipTmp.Split(';');
                foreach (string recipient in recipients)
                {
                    switch (recType)
                    {
                        case "cc":
                            _ccRecipients.Add(recipient);
                            break;
                        case "bcc":
                            _bccRecipients.Add(recipient);
                            break;
                        default:
                            _mainRecipients.Add(recipient);
                            break;

                    }
                }

                if (_mainRecipients.Count == 0)
                {
                    if (_ccRecipients.Count > 0)
                    {
                        _mainRecipients = _ccRecipients;
                        _ccRecipients = new MailAddressCollection();
                    }
                    else
                    {
                        //No public destinations defined.
                    }
                }

            }


        }

        public MailAddress Sender 
        { 
            get 
            {                
                string sender = XmlUtils.GetChildNodeText(ref _emailNode, "Sender").Trim();
                if (sender.Length == 0) sender = _defaultSender;

                return new MailAddress(sender);
            } 
        }
        public string Subject { get { return XmlUtils.GetChildNodeText(ref _emailNode, "Subject").Trim(); } }
        public string Body { get { return XmlUtils.GetChildNodeText(ref _emailNode, "Body").Trim(); } }
        public bool BodyIsHtml
        {
            get
            {
                bool rv = false;
                switch (XmlUtils.GetChildNodeText(ref _emailNode, "Body/@html").ToLower())
                {
                    case "yes":
                    case "1":
                    case "true":
                        rv = true;
                        break;
                    default:
                        break;
                }
                return rv;
            }
        }

        public MailAddressCollection To { get { return _mainRecipients; } }
        public MailAddressCollection CC { get { return _ccRecipients; } }
        public MailAddressCollection BCC { get { return _bccRecipients; } }

        public MailPriority Priority { get { return _priority; } }

        public bool FileNameOverridePrimary
        {
            get
            {
                return (XmlUtils.GetChildNodeText(ref _emailNode, "FileNameOverride").Trim().Length > 0);
            }                        
        }
        public bool FileNameOverrideAdditionalFiles
        {
            get 
            {
                return FPOUtils.AsBoolean(XmlUtils.GetChildNodeText(ref _emailNode, "FileNameOverride/@applytoadditionaldocuments").ToLower().Trim());
            }
        }

        public string FileNameOverride
        {
            get
            {
                return (XmlUtils.GetChildNodeText(ref _emailNode, "FileNameOverride").Trim());
            }
        }
        public string FileNameOverrideExt
        {
            get             
            {
                return ((FileNameOverride.Length == 0)?"":(new System.IO.FileInfo(XmlUtils.GetChildNodeText(ref _emailNode, "FileNameOverride").Trim()).Extension));
            }
        }

       
    }
}
