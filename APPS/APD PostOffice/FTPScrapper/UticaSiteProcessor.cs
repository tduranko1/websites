﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PGW.Shared;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.FTPScrapper
{
    class UticaSiteProcessor : BaseSiteProcessor, ISiteProcessor    
    {
        #region IFileProcessor Members

        public UticaSiteProcessor() : base() { }

        public override bool RetrieveAndProcessFiles()
        {
            bool success = true;

            success = base.RetreiveFilesFromSite();
            if (success)
            {
                FileInfo[] files = _workDir.GetFiles();
                _events.Trace(string.Format("No. of files to process={0}.",files.Length.ToString()));
                foreach (FileInfo fi in _workDir.GetFiles())
                {
                    ProcessFiles(fi);
                }

            }

            return success;

        }

        private bool ProcessFiles(FileInfo file)
        {
            bool success = false;

            //Utica files are single record(line), pipe deliminated
            //      Position 1 = filename w/ extention
            //      Position 2 = Number of pages imported
            //      Position 3 = Disposition {P or F  where P = Pass and F = Fail}
            _events.Trace("Processing retrieved files..");
            try
            {
                StreamReader fs = file.OpenText();
                string fileData = fs.ReadToEnd();
                fs.Close();

                string[] parsedFileData = fileData.Split('|');
                bool ack = ((parsedFileData.Length == 3) && (parsedFileData[2].Trim() == "P"));
                //job ID for Utica should be the file name less the extension
                if (_debug)
                {
                    _events.Trace(string.Format("processing: {0} .... Ack Status: {1}", file.Name, (ack ? "success" : "failed")));
                }

                string jobId = file.Name.Replace(file.Extension, "");
                RecordAcknowledgement(jobId, ack);
                success = true;
                if (_archiveDir != null)
                    file.MoveTo(GenUtils.AppendPath(false, _archiveDir.FullName, file.Name));
                else
                    file.Delete();
                //file.Delete();
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

            return success;
        }

        #endregion
    }
}
