﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.FTPScrapper
{
    public interface ISiteProcessor
    {       
        void Initialize(ref Events events, XmlNode siteNode);
        
        bool RetrieveAndProcessFiles();
    }
}
