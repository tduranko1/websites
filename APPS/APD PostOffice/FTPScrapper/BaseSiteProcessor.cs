﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.SiteUtilities;
using WinSCP;

namespace FNOLPostOffice.FTPScrapper
{
    public class BaseSiteProcessor : ISiteProcessor
    {
        protected Events _events = null;
        protected FNOLDataAccess _dataAccess = null;
        protected XmlNode _siteNode = null;
        protected DirectoryInfo _workDir = null;
        protected DirectoryInfo _archiveDir = null;
        protected DirectoryInfo _badFileDir = null;
        protected bool _debug = false;
        protected string _siteName = "";

        public BaseSiteProcessor()
        {            
        }


        protected void RecordAcknowledgement(string partialJobID, string claimNumber, bool successAck)
        {
            NameValueCollection p = new NameValueCollection();
            p.Add("JobID", partialJobID);
            p.Add("ClaimNumber", claimNumber);
            p.Add("ResponseResult", (successAck ? "1" : "0"));

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeMarkResponseRecievedClaimNumberAndPartialJobID", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

        }

        protected void RecordAcknowledgement(string jobID, bool successAck, string jobStatusDetail="")
        {
            NameValueCollection p = new NameValueCollection();
            p.Add("JobID", jobID);
            p.Add("StepID", "1");
            p.Add("ResponseResult", (successAck ? "1" : "0"));
            p.Add("JobStatusDetail", jobStatusDetail);
            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("PostOfficeMarkResponseRecieved", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }
        }

        protected bool SendAcknowledgementFile(Acknowledgements.IFTPAcknowledgement ack)
        {
            bool success = false;
            XmlNode ftpNode = XmlUtils.GetChildNode(ref _siteNode, "FTP");
            FTPDestinationInfo destInfo = new FTPDestinationInfo(ftpNode);
            _events.Trace(string.Format("Sending Acknowledgement file to {0}({1})..", _siteName, destInfo.Server));

            using (Session ftp = new Session())
            {
                ftp.ExecutablePath = _events.cSettings.GetParsedSetting("WINSCP/@exepath");
                _events.Trace(string.Format("WINSCP/@exepath = {0}", _events.cSettings.GetParsedSetting("WINSCP/@exepath")));

                SessionOptions so = new SessionOptions();
                TransferOptions txfrOpts = new TransferOptions();
                txfrOpts.TransferMode = (destInfo.EnableAsciiTransferMode ? TransferMode.Ascii : TransferMode.Binary);
                txfrOpts.PreserveTimestamp = false;
                so.FtpMode = FtpMode.Passive;
                so.UserName = destInfo.UserName;
                so.Password = destInfo.Password;
                so.HostName = destInfo.Server;
                so.Protocol = destInfo.FTPProtocol;

                switch (so.Protocol)
                {
                    case Protocol.Sftp:
                        so.FtpSecure = FtpSecure.None;
                        so.SshHostKeyFingerprint = destInfo.HostKey;
                        break;
                    default:
                        if (destInfo.HostKey.Length > 0)
                        {
                            so.FtpSecure = FtpSecure.ExplicitSsl;
                            so.SslHostCertificateFingerprint = destInfo.HostKey;
                        }
                        else
                        {
                            so.FtpSecure = FtpSecure.None;
                        }
                        break;
                }

                if (_debug)
                    _events.Trace(string.Format("Opening {0} Connection to: {1}", destInfo.FTPProtocol.ToString(), destInfo.Server));

                try
                {
                    ftp.Open(so);
                    success = true;
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
                if (success)
                {
                    string remoteDir = string.Format(@"{0}/{1}",destInfo.AcknowledgementPath,ack.AcknowledgementFileName);
                    string localDir = ack.AcknowledgementFilePath;

                    try
                    {
                        TransferOperationResult tor = ftp.PutFiles(localDir, remoteDir, false, txfrOpts);
                        
                        success = tor.IsSuccess;
                        if (_debug) _events.Trace(string.Format("{0} files retrieved.", tor.Transfers.Count.ToString()));
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, false);
                        success = false;
                    }
                    if (success)
                    {
                       // ack.CleanUp(); 
                    }
                }
            }

            return success;

        }



        protected bool RetreiveFilesFromSite()
        {
           
            bool success = false;
            XmlNode ftpNode = XmlUtils.GetChildNode(ref _siteNode, "FTP");
            FTPDestinationInfo destInfo = new FTPDestinationInfo(ftpNode);
            _events.Trace(string.Format("Retrieveing file from {0}({1})..", _siteName, destInfo.Server));

            using (Session ftp = new Session())
            {
                ftp.ExecutablePath = _events.cSettings.GetParsedSetting("WINSCP/@exepath");
                SessionOptions so = new SessionOptions();
                TransferOptions txfrOpts = new TransferOptions();
                txfrOpts.TransferMode = (destInfo.EnableAsciiTransferMode ? TransferMode.Ascii : TransferMode.Binary);
                txfrOpts.PreserveTimestamp = false;
                so.FtpMode = FtpMode.Passive;
                so.UserName = destInfo.UserName;
                so.Password = destInfo.Password;
                so.HostName = destInfo.Server;
                so.Protocol = destInfo.FTPProtocol;

                switch (so.Protocol)
                {
                    case Protocol.Sftp:
                        so.FtpSecure = FtpSecure.None;
                        so.SshHostKeyFingerprint = destInfo.HostKey;
                        break;
                    default:
                        if (destInfo.HostKey.Length > 0)
                        {
                            so.FtpSecure = FtpSecure.ExplicitSsl;
                            so.SslHostCertificateFingerprint = destInfo.HostKey;
                        }
                        else
                        {
                            so.FtpSecure = FtpSecure.None;
                        }
                        break;
                }

                if (_debug)
                    _events.Trace(string.Format("Opening {0} Connection to: {1}", destInfo.FTPProtocol.ToString(), destInfo.Server));

                try
                {
                    ftp.Open(so);
                    success = true;
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
                if (success)
                {
                    string remoteDir = destInfo.TargetPath;
                    string localDir = GenUtils.AppendPath(false, _workDir.FullName, "*.*");

                    try
                    {
                       TransferOperationResult tor = ftp.GetFiles(remoteDir, localDir, destInfo.RemoveFilesFromSiteAfterTrasnfer, txfrOpts);
                       
                       success = tor.IsSuccess;
                       if (_debug) _events.Trace(string.Format("{0} files retrieved.", tor.Transfers.Count.ToString()));
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, false);
                        success = false;
                    }
                }
            }

            return success;
        }


        #region IFileProcessor Members

        public virtual void Initialize(ref Events events, XmlNode siteNode)
        {
            _events = events;
            _siteNode = siteNode;
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);
            _workDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("WorkPath"), XmlUtils.GetChildNodeText(ref _siteNode,"@name")));
            _badFileDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("BadFilePath"), XmlUtils.GetChildNodeText(ref _siteNode, "@name")));
            _debug = "true|yes|on|1".Contains(events.cSettings.GetParsedSetting("Debug").ToLower());
            _siteName = XmlUtils.GetChildNodeText(ref siteNode, "@name");
            string archiveRoot = _events.cSettings.GetParsedSetting("ArchivePath");
            if (archiveRoot.Length > 0) _archiveDir = new DirectoryInfo(GenUtils.AppendPath(false, archiveRoot, XmlUtils.GetChildNodeText(ref _siteNode, "@name")));


            if (!_workDir.Exists) _workDir.Create();
            if ((_archiveDir != null) && (!_archiveDir.Exists)) _archiveDir.Create();

        }

        public void SendBadFileNotification(string fileName, string badFileDetails)
        {
            string recpients = (XmlUtils.NodeExists(ref _siteNode, "BadFileNotificationRecipients") ? XmlUtils.GetChildNodeText(ref _siteNode, "BadFileNotificationRecipients") : _events.cSettings.GetParsedSetting("BadFileNotificationRecipients"));
            string subject = string.Format("Bad File Received Processing for {0}", _siteName);
            StringBuilder sb = new StringBuilder();

            sb.Append("All or part of the data received in the following file was 'bad'.\n\n");
            sb.Append(string.Format("File Name: {0}\n\n", fileName));
            sb.Append(badFileDetails);

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.Sender = new System.Net.Mail.MailAddress(_events.cSettings.GetParsedSetting("EventHandling/Email/@from"));
            msg.To.Add(recpients);
            msg.Subject = subject;
            msg.IsBodyHtml = false;
            msg.Body = sb.ToString();

            SendNotification(msg);

        }

        protected void SendNotification(MailMessage mail)
        {
            mail.From = new MailAddress(_events.cSettings.GetParsedSetting("SMTPServer/@defaultsender"));
            switch (_events.cSettings.Environment.ToLower())
            {
                case "prd":
                case "production":
                    mail.Priority = MailPriority.High;
                    break;
                case "tst":
                case "stage/test":
                    mail.Priority = MailPriority.Normal;
                    break;
                default:
                    mail.Priority = MailPriority.Low;
                    break;
            }

            SmtpClient sc = new SmtpClient(_events.cSettings.GetParsedSetting("SMTPServer"));
            sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;            
            try
            {
                sc.Send(mail);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }
        }

        public virtual bool RetrieveAndProcessFiles()
        {
            return RetreiveFilesFromSite();
        }

        #endregion
    }
}
