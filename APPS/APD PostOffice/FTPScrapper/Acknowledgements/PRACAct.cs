﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.SiteUtilities;
using WinSCP;

namespace FNOLPostOffice.FTPScrapper.Acknowledgements
{
    public class PRACAck : IFTPAcknowledgement
    {
        private System.Collections.Specialized.StringCollection _files = null;
        private bool _initialized = false;
        private Events _events;
        private XmlNode _siteNode;
        private bool _isProduction = false;
        private XmlDocument _ack = new XmlDocument();
       
        private string _acknoweldgementFile = "";
        private FileInfo _ackFile = null;        
        
        public PRACAck()
        {
        }

        public void Initialize(ref Events events, ref XmlNode siteNode)
        {
            _events = events;
            _siteNode = siteNode;
            _isProduction = ((_events.cSettings.Environment.ToUpper() == "PRD") || (_events.cSettings.Environment.ToUpper() == "PRODUCTION"));
            _ack.XmlResolver = null;
            _ack.LoadXml("<Ack><FileList /></Ack>");
            _files = new System.Collections.Specialized.StringCollection();
        }

        public void AddAcknowledmentForFile(FileInfo fi)
        {
            _files.Add(fi.Name);
        }

        public FileInfo GenerateAcknowledgement()
        {
            FileInfo rv = null;
            FinalizeAcknowledgement();
            if (SaveOutFile())
            {
                rv = _ackFile;                       
            }
            return rv;
        }

        public string[] FileListForAck
        {
            get
            {
                string[] rv = new string[_files.Count - 1];

                _files.CopyTo(rv, 0);
                return rv;
            }
        }

        public void CleanUp()
        {
            if (_ackFile.Exists)
            {
                try
                {
                    _ackFile.Delete();
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, String.Format("Unable to delete acknowledgement file: {0}", _ackFile.Name), false);
                }
            }
        }

        public string AcknowledgementFilePath { get { return ((_ackFile != null) ? _ackFile.FullName : ""); } }
        public string AcknowledgementFileName { get { return ((_ackFile != null) ? _ackFile.Name : ""); } }

        private void FinalizeAcknowledgement()
        {
            
            DateTime now = DateTime.UtcNow;

            _acknoweldgementFile = string.Format("{0}_LYNX_{1}.ack", (_isProduction ? "PROD" : "TEST"), now.ToString("yyyyMMddHHmmss"));

            XmlNode fileListNode = XmlUtils.GetChildNode(ref _ack, "FileList");

            foreach (string file in _files) {
                XmlNode fileNode = XmlUtils.AddElement(ref fileListNode, "File");
                XmlUtils.AddAttribute(ref fileNode, "name", file);
            }

        }

        private bool SaveOutFile()
        {
            bool success = false;
            string clientID = ((_siteNode != null) ? XmlUtils.GetChildNodeText(ref _siteNode, "ClientID") : "000");
            string ackRootPath = _events.cSettings.GetParsedSetting("AckFilePath");
            _ackFile = new FileInfo(GenUtils.AppendPath(false, ackRootPath, clientID, _acknoweldgementFile));
            System.Text.ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] buffer;
            buffer = encoding.GetBytes(_ack.DocumentElement.OuterXml);

            try
            {
                if (!_ackFile.Directory.Exists)
                    _ackFile.Directory.Create();
                using (FileStream fs = _ackFile.Create())
                {
                    fs.Write(buffer, 0, buffer.Length);
                }
                success = true;
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

            return success;
           
        }

        private void SendAckFileToCustomer()
        {
          
        }



    }
}
