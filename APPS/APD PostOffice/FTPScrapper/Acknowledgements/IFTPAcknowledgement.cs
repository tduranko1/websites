﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FNOLPostOffice.FTPScrapper.Acknowledgements
{
    public interface IFTPAcknowledgement
    {
        void Initialize(ref PGW.Shared.SiteUtilities.Events events, ref System.Xml.XmlNode siteNode);
        void AddAcknowledmentForFile(FileInfo fi);
        FileInfo GenerateAcknowledgement();
        string[] FileListForAck { get; }
        void CleanUp();
        string AcknowledgementFilePath { get; }
        string AcknowledgementFileName { get; }
    }
}
