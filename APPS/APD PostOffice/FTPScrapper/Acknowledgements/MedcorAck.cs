﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.SiteUtilities;
using WinSCP;
using Emmanuel.Cryptography.GnuPG;

namespace FNOLPostOffice.FTPScrapper.Acknowledgements
{
    public class MedcorAck : IFTPAcknowledgement
    {
        private System.Collections.Specialized.StringCollection _files = null;
        private bool _initialized = false;
        private Events _events;
        private XmlNode _siteNode;
        private bool _isProduction = false;
        private XmlDocument _ack = new XmlDocument();
        private XmlNode _accepts = null;
        private string _acknoweldgementFile = "";
        private FileInfo _ackFile = null;
        private string _pgpEncryptKey = "";
        private string _gnupgPath = "";

        public MedcorAck()
        {
        }

        public void Initialize(ref Events events, ref XmlNode siteNode)
        {
            _events = events;
            _siteNode = siteNode;
            _isProduction = ((_events.cSettings.Environment.ToUpper() == "PRD") || (_events.cSettings.Environment.ToUpper() == "PRODUCTION"));
            _ack.XmlResolver = null;
            _ack.LoadXml("<MEDCOR_STD_XML_CONF><HEADER/><ACCEPTS/><TRAILER/></MEDCOR_STD_XML_CONF>");
            _accepts = XmlUtils.GetChildNode(ref _ack, "ACCEPTS");
            _files = new System.Collections.Specialized.StringCollection();

            _gnupgPath = _events.cSettings.GetParsedSetting("GNUPG/@exepath");
            _pgpEncryptKey = XmlUtils.GetChildNodeText(ref _siteNode, "PGPEncryptKey");
        }

        public void AddAcknowledmentForFile(FileInfo fi)
        {
            XmlNode accept = XmlUtils.AddElement(ref _accepts, "ACCEPT");
            string callNumber = fi.Name.Substring(0, 8);
            _files.Add(fi.Name);
            XmlUtils.AddElement(ref accept, "Call_Reference_Nbr").InnerText = callNumber;
        }

        public FileInfo GenerateAcknowledgement()
        {
            FileInfo rv = null;
            FinalizeAcknowledgement();
            if (SaveOutFile())
            {
                rv = _ackFile;                       
            }
            return rv;
        }

        public string[] FileListForAck
        {
            get
            {
                string[] rv = new string[_files.Count - 1];

                _files.CopyTo(rv, 0);
                return rv;
            }
        }

        public void CleanUp()
        {
            if (_ackFile.Exists)
            {
                try
                {
                    _ackFile.Delete();
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, String.Format("Unable to delete acknowledgement file: {0}", _ackFile.Name), false);
                }
            }
        }

        public string AcknowledgementFilePath { get { return ((_ackFile != null) ? _ackFile.FullName : ""); } }
        public string AcknowledgementFileName { get { return ((_ackFile != null) ? _ackFile.Name : ""); } }

        private void FinalizeAcknowledgement()
        {
            
            DateTime now = DateTime.UtcNow;
            int NumOfFiles = XmlUtils.GetChildNodeList(ref _accepts, "ACCEPT").Count;
            XmlNode headerNode = XmlUtils.GetChildNode(ref _ack, "HEADER");
            XmlNode trailer = XmlUtils.GetChildNode(ref _ack, "TRAILER");
            _acknoweldgementFile = string.Format("{0}_LYNX_Medcor_ConfXML_{1}.ack", (_isProduction ? "PROD" : "TEST"), now.ToString("yyyyMMddHHmmss"));
            XmlUtils.AddElement(ref headerNode, "File_Sent_Date").InnerText = now.ToString("yyyy-MM-dd");
            XmlUtils.AddElement(ref trailer, "File_Sent_Date").InnerText = now.ToString("yyyy-MM-dd");

            XmlUtils.AddElement(ref headerNode, "File_Sent_Time").InnerText = now.ToString("HH:mm");
            XmlUtils.AddElement(ref trailer, "File_Sent_Time").InnerText = now.ToString("HH:mm");

            XmlUtils.AddElement(ref headerNode, "FileName").InnerText = _acknoweldgementFile;
            XmlUtils.AddElement(ref trailer, "FileName").InnerText = _acknoweldgementFile;
            XmlUtils.AddElement(ref trailer, "Number_Of_Accepted_Calls").InnerText = NumOfFiles.ToString();
            XmlUtils.AddElement(ref trailer, "Number_Of_Rejected_Calls").InnerText = "0";
            XmlUtils.AddElement(ref trailer, "Total_Number_Of_Calls").InnerText = NumOfFiles.ToString();
        }

        private bool SaveOutFile()
        {
            bool success = false;
            string fileName;

            if (XmlUtils.GetChildNodeList(ref _accepts, "ACCEPT").Count > 0)
            {

                string clientID = ((_siteNode != null) ? XmlUtils.GetChildNodeText(ref _siteNode, "ClientID") : "000");
                string ENOLAckRootPath = _events.cSettings.GetParsedSetting("ENOLAckFilePath");
                _ackFile = new FileInfo(GenUtils.AppendPath(false, ENOLAckRootPath, clientID, _acknoweldgementFile));

                System.Text.ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] buffer;
                buffer = encoding.GetBytes(_ack.DocumentElement.OuterXml);

                try
                {
                    if (!_ackFile.Directory.Exists)
                        _ackFile.Directory.Create();
                    using (FileStream fs = _ackFile.Create())
                    {
                        fs.Write(buffer, 0, buffer.Length);
                    }
                    success = true;

                    if (success && _pgpEncryptKey != "")
                    {
                        GnuPGWrapper gpg = new GnuPGWrapper(_gnupgPath);
                        gpg.command = Commands.Encrypt;
                        gpg.recipient = _pgpEncryptKey;
                        gpg.ProcessTimeOutMilliseconds = 30000;
                        fileName = _ackFile.FullName.Replace(".ack", ".ack.pgp");
                        gpg.ExecuteCommandOnFileToFile(_ackFile.FullName, fileName);
                        //_ackFile.Delete();
                        _ackFile = new FileInfo(fileName);
                    }
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
            }

            return success;
           
        }

        private void SendAckFileToCustomer()
        {
          
        }



    }
}
