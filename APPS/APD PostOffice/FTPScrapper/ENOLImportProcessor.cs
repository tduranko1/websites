﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using PGW.Shared;
using PGW.Shared.SiteUtilities;
using Emmanuel.Cryptography.GnuPG;

namespace FNOLPostOffice.FTPScrapper
{
    public class ENOLImportProcessor : BaseSiteProcessor, ISiteProcessor
    {
        private string _clientID = "";
        private DirectoryInfo _unprocessedFolder = null;
        private bool _enforceUniqueFileNames = false;
        private bool _acknowledgementNeeded = false;
        private string _pgpDecryptKey = "";
        private string _pgpDecryptPassphrase = "";
        private string _gnupgPath = "";
        private string _siteDscription = "";
        public ENOLImportProcessor() : base() { }


        #region ISiteProcessor Members

        public override void Initialize(ref Events events, System.Xml.XmlNode siteNode) 
        {
            _events = events;
            _siteNode = siteNode;
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _clientID = XmlUtils.GetChildNodeText(ref siteNode, "ClientID");
            _gnupgPath = _events.cSettings.GetParsedSetting("GNUPG/@exepath");
            _pgpDecryptKey = XmlUtils.GetChildNodeText(ref siteNode, "PGPDecryptKey");
            _pgpDecryptPassphrase = XmlUtils.GetChildNodeText(ref siteNode, "PGPDecryptPassphrase");
            _dataAccess = new FNOLDataAccess(ref _events);
            _workDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("ENOLWorkPath"), XmlUtils.GetChildNodeText(ref _siteNode, "@name")));
            _unprocessedFolder = new DirectoryInfo(_events.cSettings.GetParsedSetting("ENOLWorkPath"));
            _badFileDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("ENOLBadFilePath"), XmlUtils.GetChildNodeText(ref _siteNode, "@name"),_clientID));
            _debug = "true|yes|on|1".Contains(events.cSettings.GetParsedSetting("Debug").ToLower());
            _siteName = XmlUtils.GetChildNodeText(ref siteNode, "@name");
            _enforceUniqueFileNames = "true|yes|on|1".Contains(XmlUtils.GetChildNodeText(ref siteNode,"EnforceUniqueFileNames").ToLower());
            _acknowledgementNeeded = (XmlUtils.GetChildNodeText(ref siteNode, "AcknowledgementObject").Trim().Length > 0);
            _siteDscription = XmlUtils.GetChildNodeText(ref siteNode, "@description");

            if (!_workDir.Exists) _workDir.Create();
            if (!_badFileDir.Exists) _badFileDir.Create();            

            
        }

        public override bool RetrieveAndProcessFiles()
        {
            bool success = base.RetreiveFilesFromSite();

            if (_pgpDecryptKey != string.Empty)
            {
                success = DecryptFiles();
            }

            Acknowledgements.IFTPAcknowledgement ack = null;
            if (_acknowledgementNeeded)
                ack = InstantiateAcknowledgement();

            if (success)
            {
                FileInfo[] files = _workDir.GetFiles();
                _events.Trace(string.Format("No. of files to process = {0}.", files.Length.ToString()));

                foreach (FileInfo fi in files)
                {
                    ProcessFile(fi);
                    if (ack != null)
                    {
                        // Add file to list for acknowledgements
                        ack.AddAcknowledmentForFile(fi);
                    }
                }
                
                if (ack != null)
                {
                    if (ack.GenerateAcknowledgement() != null)
                    {
                        if (!SendAcknowledgementFile(ack))
                        {
                            // Something went wrong delivering the Ack File to customer.  Send out notification of failure.
                            SendAckFailureNotification(ack);
                        }
                        ack.CleanUp();
                    }
                    else
                    {
                        string message = string.Format("System was unable to generate the acknowledgement for Client # {0}.", XmlUtils.GetChildNodeText(ref _siteNode, "ClientID"));
                        SendFailureNotificationEmail("GenerateAcknowledgement Process", message);
                    }
                }
            }

            return success;
        }

        private bool DecryptFiles()
        {
            GnuPGWrapper gpg = new GnuPGWrapper(_gnupgPath);
            bool success = false;
            string fileName;

            try
            {                
                FileInfo[] files = _workDir.GetFiles();
                foreach (FileInfo fi in files)
                {
                    fileName = fi.FullName;
                    fi.MoveTo(fi.FullName.Replace(" ", ""));
                    gpg.command = Commands.Decrypt;
                    gpg.recipient = _pgpDecryptKey;
                    gpg.passphrase = _pgpDecryptPassphrase;
                    gpg.ProcessTimeOutMilliseconds = 30000;
                    gpg.ExecuteCommandOnFileToFile(fi.FullName, fileName.Replace(".pgp", ""));
                    fi.Delete();
                }

                success = true;
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                SendFailureNotificationEmail("File Decryption process", ex.Message);
                success = false;
            }

            return success;
        }

        private void SendFailureNotificationEmail(string emailsubject, string message)
        {
            string recpients = (XmlUtils.NodeExists(ref _siteNode, "FailedSendAckNotificationRecipients") ? XmlUtils.GetChildNodeText(ref _siteNode, "FailedSendAckNotificationRecipients") : _events.cSettings.GetParsedSetting("FailedSendAckNotificationRecipients"));
            string subject = string.Format("{0} failure for {1}", emailsubject, _siteDscription);

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.Sender = new System.Net.Mail.MailAddress(_events.cSettings.GetParsedSetting("EventHandling/Email/@from"));
            msg.To.Add(recpients);
            msg.Subject = subject;
            msg.IsBodyHtml = false;            
            msg.Body = message;

            SendNotification(msg);
        }
        #endregion

        #region Acknowledgements
        private Acknowledgements.IFTPAcknowledgement InstantiateAcknowledgement()
        {
            Acknowledgements.IFTPAcknowledgement ack = null;

            string ackObject = XmlUtils.GetChildNodeText(ref _siteNode, "AcknowledgementObject");
            if (!string.IsNullOrEmpty(ackObject))
            {
                string fullQualifiedObjectName = string.Format("FNOLPostOffice.FTPScrapper.Acknowledgements.{0}", ackObject);

                Type type = Type.GetType(fullQualifiedObjectName);

                if (type != null)
                    ack = (Acknowledgements.IFTPAcknowledgement)Activator.CreateInstance(type);

                if (ack != null)
                {
                    ack.Initialize(ref _events, ref _siteNode);
                }

            }

            return ack;
        }

        private void SendAckFailureNotification(Acknowledgements.IFTPAcknowledgement ack)
        {

            string recpients = (XmlUtils.NodeExists(ref _siteNode, "FailedSendAckNotificationRecipients") ? XmlUtils.GetChildNodeText(ref _siteNode, "FailedSendAckNotificationRecipients") : _events.cSettings.GetParsedSetting("FailedSendAckNotificationRecipients"));
            string subject = string.Format("Send ACK failure for {0}", _siteName);
            string clientid = XmlUtils.GetChildNodeText(ref _siteNode,"ClientID");
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("System was unable to successfully send the acknowledgement file for Client #{0} for the following files received:.\n\n",clientid));

            foreach (string fileName in ack.FileListForAck)
            {
                sb.Append(string.Format("File Name: {0}\n", fileName));
            }

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.Sender = new System.Net.Mail.MailAddress(_events.cSettings.GetParsedSetting("EventHandling/Email/@from"));
            msg.To.Add(recpients);
            msg.Subject = subject;
            msg.IsBodyHtml = false;
            msg.Body = sb.ToString();

        }


        #endregion

        #region File Processing
        private void ProcessFile(FileInfo fi)
        {
            _events.Trace(string.Format("Processing retrieved file: {0} ", fi.Name));
            bool badFile = false;
            bool success = false;

            //Check to see if PDF is bad file
            badFile = IsBadPDF(fi);
            if (badFile)
            {
                FileInfo badFI = new FileInfo(GenUtils.AppendPath(_badFileDir.FullName, fi.Name));
                fi.MoveTo(badFI.FullName);
                BadFileReceivedNotification(badFI);
                badFI = null;
            }
            else
            {
                //string spName = "Database/StoredProcs/ENOLInsertRecord";
                FileInfo upFI = null;
                bool fileExists = false;
                try
                {
                    string fn = GenUtils.AppendPath(false, _unprocessedFolder.FullName, string.Format("{0}_{1}", _clientID, fi.Name));
                    fileExists = File.Exists(fn);
                    if (fileExists)
                    {
                        if (_enforceUniqueFileNames)
                        {
                            // Client is set up indicating that all ENOLs received should always have a unique name.
                            upFI = null;
                            fn = string.Empty;
                            PossibleDuplicateENOLReceived(fi);
                            FileInfo badFI = new FileInfo(GenUtils.AppendPath(_badFileDir.FullName, fi.Name));
                        }
                        else
                        {
                            int ctr = 0;
                            while (fileExists)
                            {
                                ctr += 1;
                                fn = GenUtils.AppendPath(false, _unprocessedFolder.FullName, string.Format("{0}_{1}_{2}", _clientID, ctr.ToString(), fi.Name));
                                fileExists = File.Exists(fn);
                            }
                        }
                    }
                    if ((!fileExists) && !String.IsNullOrEmpty(fn))
                    {
                        upFI = fi.CopyTo(fn);
                    }

                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                    upFI = null;
                }
                if (upFI != null)
                {

                    System.Collections.Specialized.NameValueCollection parameters = new System.Collections.Specialized.NameValueCollection();
                    string controlFileNumber = "";
                    if (XmlUtils.NodeExists(ref _siteNode, "ControlFileNumber"))
                    {
                        string method = XmlUtils.GetChildNodeText(ref _siteNode, "ControlFileNumber/@type").ToLower();
                        switch (method)
                        {
                            case "static":
                                controlFileNumber = XmlUtils.GetChildNodeText(ref _siteNode, "ControlFileNumber");
                                break;
                            case "coldimport":
                                int index = -1;
                                if (int.TryParse(XmlUtils.GetChildNodeText(ref _siteNode, "ControlFileNumber"), out index))
                                {
                                    string[] coldimptokens = fi.Name.Replace(string.Format(".{0}", fi.Extension), "").Split('_');
                                    if (index < coldimptokens.Length)
                                    {
                                        controlFileNumber = coldimptokens[index];
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    parameters.Add("FileName", upFI.Name);
                    parameters.Add("CurrentPath", upFI.FullName);
                    parameters.Add("ClientID", _clientID);
                    parameters.Add("tmpFileNo", controlFileNumber);
                    parameters.Add("FileReceiveDate", DateTime.Now.ToString());

                    try
                    {
                        _dataAccess.ExecuteDatabaseTransactionNoReturn("ENOLInsertRecord", ref parameters);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, false);
                        FileDBRecordInsertionFailureNotification(fi);
                    }

                    if (success)
                    {
                        try
                        {
                            fi.Delete();
                        }
                        catch (Exception ex)
                        {
                            _events.HandleEvent(ex, string.Format("Failed to delete file: {0}", fi.FullName), false);
                        }
                    }

                }
            }


        }
        private void PossibleDuplicateENOLReceived(FileInfo fi)
        {
            MailMessage mail = new MailMessage();
            mail.Subject = string.Format("Possible Duplicate Image Received for Client ID#{0}", _clientID);
            mail.IsBodyHtml = false;
            mail.Body = string.Format("This client has been set up and configured to indicate that file names for all ENOLs received should be unique.  An ENOL file was received, {0}, for which there already exists a enol image with the same filename.  The image has been moved to the bad file folder for investigation.", _clientID, fi.Name);

            string recipients = _events.cSettings.GetParsedSetting("BadFileNotificationRecipients").Replace(";", ",");

            mail.To.Add(recipients);

            SendNotification(mail);

        }
        private void BadFileReceivedNotification(FileInfo badFileInfo)
        {
            MailMessage mail = new MailMessage();
            mail.Subject = string.Format("Bad ENOL Image Received for Client ID#{0}", _clientID);
            mail.IsBodyHtml = false;
            mail.Body = string.Format("A ENOL PDF was received for Client ID#{0}.  Image File Name = {1}", _clientID, badFileInfo.Name);
            string recipients = _events.cSettings.GetParsedSetting("BadFileNotificationRecipients").Replace(";", ",");

            mail.To.Add(recipients);

            SendNotification(mail);

        }
        private void FileDBRecordInsertionFailureNotification(FileInfo fi)
        {
            MailMessage mail = new MailMessage();
            mail.Subject = string.Format("Database Record Creation Failure for Image Received for Client ID#{0}", _clientID);
            mail.IsBodyHtml = false;
            mail.Body = string.Format("The database record failed to be created for an image for Client ID#{0}.  Image File Name = {1}", _clientID, fi.Name);
            mail.From = new MailAddress(_events.cSettings.GetParsedSetting("SMTPServer/@defaultsender"));
            string recipients = _events.cSettings.GetParsedSetting("BadFileNotificationRecipients").Replace(";", ",");

            mail.To.Add(recipients);
            SendNotification(mail);

        }
        private bool IsBadPDF(FileInfo fi)
        {
            bool badPDF = false;
            bool startTagFound = false;
            bool endTagFound = false;
            string line = "";

            using (StreamReader rdr = new StreamReader(fi.FullName))
            {
                while ((line = rdr.ReadLine()) != null)
                {
                    if (!startTagFound && line.Contains("%PDF")) startTagFound = true;
                    if (!endTagFound && line.Contains("%%EOF"))
                    {
                        endTagFound = true;
                        break;
                    }
                }

                badPDF = (!startTagFound && !endTagFound);
            }
            return badPDF;
        }
        #endregion

        
        
    }
}
