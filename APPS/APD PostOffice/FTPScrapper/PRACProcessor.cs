﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PGW.Shared;
using PGW.Shared.SiteUtilities;
using System.Collections.Specialized;
using System.Data;

namespace FNOLPostOffice.FTPScrapper
{
    class PRACProcessor : BaseSiteProcessor, ISiteProcessor
    {
        #region IFileProcessor Members

        public PRACProcessor() : base() { }

        public override bool RetrieveAndProcessFiles()
        {
            bool success = true;
            string fileName;
            string processingDatetime;

            success = base.RetreiveFilesFromSite();
            if (success)
            {
                Acknowledgements.IFTPAcknowledgement ack = InstantiateAcknowledgement();
                processingDatetime = XmlUtils.GetChildNodeText(ref _siteNode, "ProcessDate");
                if (processingDatetime.ToLower() == "$currentday")
                {
                    fileName = string.Format("ActivityReportBalance_{0}", DateTime.Now.ToString("yyyy-MM-dd"));
                }
                else
                {
                    fileName = string.Format("ActivityReportBalance_{0}", DateTime.Parse(processingDatetime).ToString("yyyy-MM-dd"));
                }

                //string fileName = string.Format("ActivityReportBalance_{0}", "2016-11-11");
                //string dateStart = "2016-11-11 14:30:00";
                //string dateEnd = "2016-11-11 14:40:00";
                string dateStart = "";
                string dateEnd = "";

                FileInfo[] files = _workDir.GetFiles().Where(f=> f.Name.StartsWith(fileName)).ToArray();
                _events.Trace(string.Format("No. of files to process={0}.", files.Length.ToString()));
                foreach (FileInfo fi in files)
                {
                    ProcessFiles(fi, ref ack, dateStart, dateEnd);
                }

                if (ack != null)
                {
                    if (ack.GenerateAcknowledgement() != null)
                    {
                        SendAcknowledgementFile(ack);
                        ack.CleanUp();
                    }
                }
            }

            return success;

        }
        private Acknowledgements.IFTPAcknowledgement InstantiateAcknowledgement()
        {
            Acknowledgements.IFTPAcknowledgement ack = null;

            string ackObject = XmlUtils.GetChildNodeText(ref _siteNode, "AcknowledgementObject");
            if (!string.IsNullOrEmpty(ackObject))
            {
                string fullQualifiedObjectName = string.Format("FNOLPostOffice.FTPScrapper.Acknowledgements.{0}", ackObject);

                Type type = Type.GetType(fullQualifiedObjectName);

                if (type != null)
                    ack = (Acknowledgements.IFTPAcknowledgement)Activator.CreateInstance(type);

                if (ack != null)
                {
                    ack.Initialize(ref _events, ref _siteNode);
                }

            }

            return ack;
        }
        private DataTable GetJobByDate(string dateStart="", string dateEnd="")
        {
            DataTable dt = null;
            try
            {
                NameValueCollection p = new NameValueCollection();
                p.Add("ClientID", "134");
                if (dateStart.Length == 0)
                {
                    p.Add("DateStart", DateTime.Now.AddHours(-4).ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else
                {
                    p.Add("DateStart", dateStart);
                }
                if (dateEnd.Length == 0)
                {
                    p.Add("DateEnd", DateTime.Now.AddMinutes(-1).ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else
                {
                    p.Add("DateEnd", dateEnd);
                }
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("PostOfficeGetJobByDate", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = new DataTable();
            }

            return dt;
        }
        private bool ProcessFiles(FileInfo file, ref Acknowledgements.IFTPAcknowledgement ack, string dateStart = "", string dateEnd = "")
        {
            string jobId = string.Empty;
            DataTable dt = GetJobByDate(dateStart, dateEnd);
            if (dt.Rows.Count > 0)
            {
                jobId = dt.Rows[0]["JobID"].ToString();
            }
            bool success = false;

            _events.Trace("Processing retrieved files..");
            try
            {
                using (StreamReader fs = file.OpenText())
                {
                    int i = 0;
                    int j = 0;
                    int batchIdCol = 0;
                    int processedCol = 0;
                    int processStatusCol = 0;
                    int lynxIdCol = 0;
                    int pracCompanyIdCol = 0;
                    int pracCompanyCodeCol = 0;
                    List<string> processedLynxIds=new List<string>();
                    while (fs.Peek() >= 0)
                    {
                        string fileData = fs.ReadLine();
                        string[] parsedFileData = fileData.Split(',');
                        if (i == 0)
                        {
                            foreach (string c in parsedFileData)
                            {
                                switch (c)
                                {
                                    case "BatchId": batchIdCol = j; break;
                                    case "Lynx_ID": lynxIdCol = j; break;
                                    case "PRAC_Processed": processedCol = j; break;
                                    case "PRAC_ProcessStatus": processStatusCol = j; break;
                                    case "PRAC_CompanyId": pracCompanyIdCol = j; break;
                                    case "PRAC_CompanyName": pracCompanyCodeCol = j; break;
                                }
                                j++;
                            }
                        }
                        else
                        {
                            if (parsedFileData[processedCol] == "Y" && parsedFileData[pracCompanyIdCol] !="")
                            {
                                if (!(processedLynxIds.Contains(parsedFileData[lynxIdCol])))
                                {
                                    processedLynxIds.Add(parsedFileData[lynxIdCol]);
                                    NameValueCollection p = new NameValueCollection();
                                    p.Add("lynxId", parsedFileData[lynxIdCol]);
                                    p.Add("pracCompanyId", parsedFileData[pracCompanyIdCol]);
                                    p.Add("pracCompanyCode", parsedFileData[pracCompanyCodeCol]);
                                    p.Add("processStatus", parsedFileData[processStatusCol]);
                                    p.Add("referenceNumber", parsedFileData[batchIdCol]);
                                    _dataAccess.ExecuteDatabaseTransactionNoReturn("UpdatePRACClaim", ref p);
                                }
                                
                            }
                        }
                        i++;
                    }
                    fs.Close();
                }
                if (ack != null)
                {
                    ack.AddAcknowledmentForFile(file);
                }

                if (jobId.Length > 0)
                {
                    RecordAcknowledgement(jobId, true,string.Format("{0} processed", file.Name));
                }
                
                success = true;
                if (_archiveDir != null) {
                    string archiveFileName = GenUtils.AppendPath(false, _archiveDir.FullName, file.Name);
                    if (!File.Exists(archiveFileName))
                    {
                        file.MoveTo(archiveFileName);
                    }
                }                    
                else
                    file.Delete();

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

            return success;
        }

        #endregion
    }
}
