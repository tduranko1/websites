﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PGW.Shared;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.FTPScrapper
{
    class EncompassENOLDeliveryAckProcessor : BaseSiteProcessor, ISiteProcessor
    {

        public EncompassENOLDeliveryAckProcessor() : base() { }

        #region ISiteProcessor Members

        //we'll let the base class' version handle this
        //void ISiteProcessor.Initialize(ref Events events, System.Xml.XmlNode siteNode)
        //{
        //    throw new NotImplementedException();
        //}

        public override bool RetrieveAndProcessFiles()
        {
            bool success = base.RetreiveFilesFromSite();
            if (success)
            {
                FileInfo[] files = _workDir.GetFiles();
                _events.Trace(string.Format("No. of files to process={0}.", files.Length.ToString()));
                foreach (FileInfo fi in files)
                {
                    ProcessFile(fi);
                }
            }

            return success;
        }

        #endregion

        private bool ProcessFile(FileInfo file)
        {
            bool success = false;

            //Encompass ENOL Delivery Ack Files are one or more lines of pipe-deliminated acknowledgements
            // position 1 = ClaimNumber
            // position 2 = UniqueID (our jobid for the sending)
            // position 3 = filename
            // position 4 = contentid
            // position 5 = status  (Y/N)
            _events.Trace(string.Format("Processing retrieved file..{0}", file.Name));
            StreamReader fs = null;
            StringBuilder sbBadData = new StringBuilder();
            bool badFile = false;
            try
            {
                bool ack = false;
                string claimNumber = "";
                string jobID = "";
                string lineData = "";
                string[] parsedData = null;                
                fs = file.OpenText();
                int lineNumber = 0;
                while (!fs.EndOfStream)
                {
                    lineData = fs.ReadLine();
                    lineNumber += 1;
                    parsedData = lineData.Split('|');
                    if (parsedData.Length == 5)
                    {
                        ack = (parsedData[4] == "Y");
                        claimNumber = parsedData[0];
                        jobID = parsedData[1];
                        RecordAcknowledgement(jobID,claimNumber, ack);
                    }
                    else
                    {
                        if (lineData.Trim().Length > 0)
                        {
                            string bdinfo = string.Format("Bad Line[{0}]: {1}", lineNumber.ToString(), lineData);
                            _events.Trace(bdinfo);
                            sbBadData.Append(GenUtils.Append(bdinfo, "\n"));
                        }
                        else
                        {
                            //Was blank line--no data-- ignore
                        }
                    }
                }
                badFile = (sbBadData.Length > 0);
                success = true;
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);                
            }
            finally
            {
                if (fs != null) fs.Close();
            }

            if (badFile)
            {
                SendBadFileNotification(file.Name, sbBadData.ToString());
                try
                {
                    // Move the file to bad file path.
                    file.MoveTo(GenUtils.AppendPath(false, _badFileDir.FullName, file.Name));
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
            }
            else
            {

                try
                {
                    if (_archiveDir != null)
                        file.MoveTo(GenUtils.AppendPath(false, _archiveDir.FullName, file.Name));
                    else
                        file.Delete();
                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                }
            }
            return success;
        }
    }
}
