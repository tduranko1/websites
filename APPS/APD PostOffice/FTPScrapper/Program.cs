﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.FTPScrapper
{
    class Program
    {
        static void Main(string[] args)
        {            
            string configFile = GenUtils.AppendPath(false,
                (Debugger.IsAttached?(new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.FullName:AppDomain.CurrentDomain.BaseDirectory), 
                "fs_config.xml");

            string fileProcessorName = "";

            Events events = new Events();
            GenUtils.Assert(File.Exists(configFile), string.Format("Configuration file, '{0}', not found.", configFile));
            
            events.ComponentInstance = "FTP Scrapper";
            events.Initialize(configFile);
            events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");

            XmlNode sitesToCheck = events.cSettings.GetParsedNode("Sites");
            
            ISiteProcessor sp = null;
            object[] spargs = {};
            Type type;

            foreach (XmlNode site in XmlUtils.GetChildNodeList(ref sitesToCheck, "CheckSite[@enabled='1']"))
            {
                XmlNode checkSite = site;
                fileProcessorName = string.Format("FNOLPostOffice.FTPScrapper.{0}", XmlUtils.GetChildNodeText(ref checkSite, "@processor"));
                type = Type.GetType(fileProcessorName);

                sp = (ISiteProcessor)Activator.CreateInstance(type);
                ((BaseSiteProcessor)sp).Initialize(ref events, checkSite);
                sp.RetrieveAndProcessFiles();
            }

            events.DumpLogBufferToDisk();
            events.Dispose();
            events = null;

        }
    }
}
