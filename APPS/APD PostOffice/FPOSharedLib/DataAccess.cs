﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Xml;
using System.Linq;
using System.Text;
using PGW.Shared;
using PGW.Shared.SiteUtilities;
using PGW.Shared.DataAccess;

namespace FNOLPostOffice
{
    public class FNOLDataAccess
    {
        private Events _events = null;
        private string _defaultDatabase = "Database";
        private MssqlDBAccessor _dataAccess = null;
        //private FileAccessor _fileAccess = null;

        internal class DatabaseInfo
        {
            private string _storedProcedureName;
            private string _connectString;
            private string _connectStringXml;

            public DatabaseInfo(ref Events events, string procedureName, string database)
            {
                if (database.Trim().Length == 0) database = "Database";

                _storedProcedureName = events.cSettings.GetParsedSetting(String.Format("{0}/StoredProcs/{1}", database, procedureName));
                _connectString = events.cSettings.GetParsedSetting(string.Format("{0}/ConnectionStringStd", database));
                _connectStringXml = events.cSettings.GetParsedSetting(string.Format("{0}/ConnectionStringXml", database));
            }

            public string StoredProcedureName { get { return _storedProcedureName; } }
            public string ConnectString { get { return _connectString; } }
            public string ConnectStringXml { get { return _connectStringXml; } }


        }

        public FNOLDataAccess(ref Events events)
        {
            _events = events;
            _dataAccess = new MssqlDBAccessor(ref _events);
            //_fileAccess = new FileAccessor(ref _events);
        }

        public FNOLDataAccess(ref Events events, string defaultDatabase)
        {
            _events = events;
            if (defaultDatabase.Trim().Length > 0) _defaultDatabase = defaultDatabase;
        }

        public string ExecuteDatabaseTransactionXml(string procName, string database, params object[] parameters)
        {
            DatabaseInfo di = new DatabaseInfo(ref _events, procName, database);
            _dataAccess.SetConnectString(di.ConnectString, di.ConnectStringXml);
            return _dataAccess.GetXMLNamedParams(di.StoredProcedureName, parameters);
        }

        public string ExecuteDatabaseTransactionXml(string procName)
        {
            return ExecuteDatabaseTransactionXml(procName, "");
        }
        public string ExecuteDatabaseTransactionXml(string procName, string database)
        {
            NameValueCollection tmp = new NameValueCollection(0);
            return ExecuteDatabaseTransactionXml(procName, ref tmp, database);
        }
        public string ExecuteDatabaseTransactionXml(string procName, ref NameValueCollection parameters)
        {
            return ExecuteDatabaseTransactionXml(procName, ref parameters, "");
        }
        public string ExecuteDatabaseTransactionXml(string procName, ref NameValueCollection parameters, string database)
        {
            string responseXml = "";
            DatabaseInfo dbi = new DatabaseInfo(ref _events, procName, database);

            object[] parameterList = NamedValueToObjectParam(ref parameters);

            _dataAccess.SetConnectString(dbi.ConnectString, dbi.ConnectStringXml);
            responseXml = _dataAccess.GetXMLNamedParams(dbi.StoredProcedureName, parameterList);

            return responseXml;
        }
        public string ExecuteDatabaseTransactionXml2(string procName)
        {
            return ExecuteDatabaseTransactionXml2(procName, "");
        }
        public string ExecuteDatabaseTransactionXml2(string procName, string database)
        {
            NameValueCollection tmp = new NameValueCollection(0);
            return ExecuteDatabaseTransactionXml2(procName, ref tmp, database);
        }
        public string ExecuteDatabaseTransactionXml2(string procName, ref NameValueCollection parameters)
        {
            return ExecuteDatabaseTransactionXml2(procName, ref parameters, "");
        }
        public string ExecuteDatabaseTransactionXml2(string procName, ref NameValueCollection parameters, string database)
        {
            string responseXml = "";
            DatabaseInfo dbi = new DatabaseInfo(ref _events, procName, database);

            object[] parameterList = NamedValueToObjectParam(ref parameters);
            _dataAccess.SetConnectString(dbi.ConnectString);
            responseXml = _dataAccess.ExecuteXmlReaderNamedParams(dbi.StoredProcedureName, parameterList);

            return responseXml;

        }


        public DataTable ExecuteDatabaseTransactionReturnTable(string procName)
        {
            return ExecuteDatabaseTransactionReturnTable(procName, "");
        }
        public DataTable ExecuteDatabaseTransactionReturnTable(string procName, string database)
        {
            NameValueCollection tmp = new NameValueCollection(0);
            return ExecuteDatabaseTransactionReturnTable(procName, ref tmp, database);
        }
        public DataTable ExecuteDatabaseTransactionReturnTable(string procName, ref NameValueCollection parameters)
        {
            return ExecuteDatabaseTransactionReturnTable(procName, ref parameters, "");
        }
        public DataTable ExecuteDatabaseTransactionReturnTable(string procName, ref NameValueCollection parameters, string database)
        {

            DataTable dt = null;
            DatabaseInfo dbi = new DatabaseInfo(ref _events, procName, database);

            object[] tmp = NamedValueToObjectParam(ref parameters);
            _dataAccess.SetConnectString(dbi.ConnectString);
            dt = _dataAccess.GetDataSetNamedParams(dbi.StoredProcedureName, tmp).Tables[0];

            return dt;
        }


        public DataRow ExecuteDatabaseTransactionReturnRow(string procName)
        {
            return ExecuteDatabaseTransactionReturnRow(procName, "");
        }
        public DataRow ExecuteDatabaseTransactionReturnRow(string procName, string database)
        {
            NameValueCollection tmp = new NameValueCollection(0);
            return ExecuteDatabaseTransactionReturnRow(procName, ref tmp, database);
        }
        public DataRow ExecuteDatabaseTransactionReturnRow(string procName, ref NameValueCollection parameters)
        {
            return ExecuteDatabaseTransactionReturnRow(procName, ref parameters, "");
        }
        public DataRow ExecuteDatabaseTransactionReturnRow(string procName, ref NameValueCollection parameters, string database)
        {
            DataRow dr = null;

            DataTable dt = ExecuteDatabaseTransactionReturnTable(procName, ref parameters, database);
            switch (dt.Rows.Count)
            {
                case 0:
                    dr = null;
                    break;
                case 1:
                    dr = dt.Rows[0];
                    break;
                default:
                    _events.Trace(String.Format("SP={0} \n {1}", procName, parameters.ToString()));
                    throw new Exception("Transaction yielded multiple rows when only expecting a single row.");
            }

            return dr;
        }



        public void ExecuteDatabaseTransactionNoReturn(string procName)
        {
            ExecuteDatabaseTransactionNoReturn(procName, "");
        }
        public void ExecuteDatabaseTransactionNoReturn(string procName, string database)
        {
            NameValueCollection tmp = new NameValueCollection(0);
            ExecuteDatabaseTransactionNoReturn(procName, ref tmp, database);
        }
        public void ExecuteDatabaseTransactionNoReturn(string procName, ref NameValueCollection parameters)
        {
            ExecuteDatabaseTransactionNoReturn(procName, ref parameters, "");
        }
        public void ExecuteDatabaseTransactionNoReturn(string procName, ref NameValueCollection parameters, string database)
        {

            DatabaseInfo dbi = new DatabaseInfo(ref _events, procName, database);
            object[] tmp = NamedValueToObjectParam(ref parameters);
            _dataAccess.SetConnectString(dbi.ConnectString);
            _dataAccess.ExecuteNamedParams(dbi.StoredProcedureName, tmp);

        }

        //#region File Access

        //public XmlDocument RetrieveXmlFile(string fullFilePath)
        //{
        //    return _fileAccess.RetrieveXmlDocument(fullFilePath);
        //}
        //public void WriteXmlFile(string fullFilePath, bool overwrite, ref XmlDocument document, bool compress)
        //{
        //    WriteXmlFile(fullFilePath, overwrite, ref document, compress, false);
        //}
        //public void WriteXmlFile(string fullFilePath, bool overwrite, ref XmlDocument document, bool compress, bool encrypt)
        //{
        //    WriteFileAttributes wfa = new WriteFileAttributes(fullFilePath, overwrite, compress, encrypt);
        //    _fileAccess.WriteXmlDocument(ref document, wfa);
        //}

        //public string RetrieveTextFile(string fullFilePath)
        //{
        //    return _fileAccess.RetrieveTextFileAsString(fullFilePath);
        //}
        //public void WriteTextFile(string fullFilePath, bool overwrite, string fileData, bool compress)
        //{
        //    WriteTextFile(fullFilePath, overwrite, fileData, compress, false);
        //}
        //public void WriteTextFile(string fullFilePath, bool overwrite, string fileData, bool compress, bool encrypt)
        //{
        //    WriteFileAttributes wfa = new WriteFileAttributes(fullFilePath, overwrite, compress, encrypt);
        //    _fileAccess.WriteStringToTextFile(fileData, wfa);
        //}


        //public byte[] RetrieveBinaryFile(string fullFilePath)
        //{
        //    return _fileAccess.RetrieveBinaryFile(fullFilePath);
        //}
        //public void WriteBinaryFile(string fullFilePath, bool overwrite, ref byte[] fileData, bool compress)
        //{
        //    WriteFileAttributes wfa = new WriteFileAttributes(fullFilePath, overwrite, compress, false);
        //    _fileAccess.WriteBinaryFile(ref fileData, wfa);
        //}

        //public void RemoveFile(string fileName)
        //{
        //    try
        //    {
        //        //				cobjPretender.Impersonate();
        //        File.Delete(fileName);
        //    }
        //    catch { }
        //    finally
        //    {
        //        //				if (cobjPretender.Impersonating)
        //        //					cobjPretender.Revert();
        //    }
        //}

        //#endregion

        private object[] NamedValueToObjectParam(ref NameValueCollection parameters)
        {
            object[] Param = new object[0];
            if (parameters != null)
            {
                if (parameters.Count > 0)
                {
                    string paramName = "";
                    string paramValue = "";
                    Param = new object[parameters.Count * 2];
                    for (int ptr = 0; ptr < parameters.Count; ptr++)
                    {
                        paramName = parameters.GetKey(ptr);
                        paramValue = parameters.Get(ptr);

                        Param[(ptr * 2)] = GenUtils.Append("@", paramName);
                        Param[((ptr * 2) + 1)] = paramValue;
                    }
                }
            }
            return Param;
        }
    }
}
