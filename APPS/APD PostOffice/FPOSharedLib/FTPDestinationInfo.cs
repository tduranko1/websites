﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using WinSCP;

namespace FNOLPostOffice
{
    public class FTPDestinationInfo
    {

        private string _server = "";
        private string _username = "";
        private string _password = "";
        private string _targetpath = "";
        private string _acknowledgementpath = "";
        private string _hostkey = "";
        private bool _enableAsciiTransferMode = false;
        private Protocol _protocol = Protocol.Ftp;
        private bool _removeFilesAfterXfer = true;

        private const string DECRYPTKEY = "LYNX2Ftp!";

        public FTPDestinationInfo(XmlNode destinationInfo)
        {


            _server = XmlUtils.GetChildNodeText(ref destinationInfo, "@server");
            _username = XmlUtils.GetChildNodeText(ref destinationInfo, "@user");
            _password = DecryptString(XmlUtils.GetChildNodeText(ref destinationInfo, "@password"), DECRYPTKEY);
            _targetpath = XmlUtils.GetChildNodeText(ref destinationInfo, "@targetpath");
            _acknowledgementpath = XmlUtils.GetChildNodeText(ref destinationInfo, "@acknowledgementpath");
            _hostkey = DecryptString(XmlUtils.GetChildNodeText(ref destinationInfo, "@hostkey"), DECRYPTKEY);
            _enableAsciiTransferMode = (XmlUtils.GetChildNodeText(ref destinationInfo, "@transfermode") == "ascii");
            _protocol = ((XmlUtils.GetChildNodeText(ref destinationInfo, "@protocol").ToLower() == "sftp") ? Protocol.Sftp : Protocol.Ftp);
            if (XmlUtils.GetChildNodeText(ref destinationInfo, "@removefilesafterxfer").Trim().Length > 0)
            {
                _removeFilesAfterXfer = "true|yes|on|1".Contains(XmlUtils.GetChildNodeText(ref destinationInfo, "@removefilesafterxfer").Trim().ToLower());
            }

        }

        public FTPDestinationInfo(string destinationInfo)
        {

            string[] tokens = destinationInfo.Split('|');
            string paramName = "";
            string paramValue = "";
            int ptr = 0;
            foreach (string token in tokens)
            {
                paramName = "";
                paramValue = "";
                ptr = token.IndexOf("=");
                if (ptr > 0)
                {
                    paramName = token.Substring(0, ptr);
                    paramValue = token.Substring((ptr + 1));
                    switch (paramName.ToLower())
                    {
                        case "server":
                        case "host":
                            _server = paramValue.Replace("~", "|");
                            break;
                        case "username":
                        case "user":
                            _username = paramValue;
                            break;
                        case "pass":
                        case "password":
                            _password = paramValue;
                            break;
                        case "path":
                        case "directory":
                        case "dir":
                        case "targetpath":
                            _targetpath = paramValue;
                            break;
                        case "acknowledgementpath":
                            _acknowledgementpath = paramValue;
                            break;
                        default:
                            break;
                    }
                }
            }

        }

        private string DecryptString(string encryptedString, string encryptionKey)
        {
            string decryptedString = "";
            if (encryptedString.Length > 0)
            {
                SymmetricEncryption symenc = new SymmetricEncryption(SymmetricEncryption.Provider.TripleDES, true);
                Data key = new Data(encryptionKey);
                Data encryptedStringData = new Data();
                encryptedStringData.Base64 = encryptedString;
                decryptedString = symenc.Decrypt(encryptedStringData, key).ToString();
            }
            return decryptedString;
        }

        public string Server { get { return _server; } set { _server = value; } }
        public string UserName { get { return _username; } set { _username = value; } }
        public string Password { get { return _password; } set { _password = value; } }
        public string TargetPath { get { return _targetpath; } set { _targetpath = value; } }
        public string AcknowledgementPath { get { return _acknowledgementpath; } set { _acknowledgementpath = value; } }
        public bool EnableAsciiTransferMode { get { return _enableAsciiTransferMode; } }
        public string HostKey { get { return _hostkey; } }
        public Protocol FTPProtocol { get { return _protocol; } }
        public bool RemoveFilesFromSiteAfterTrasnfer { get { return _removeFilesAfterXfer; } }

        public bool Valid
        {
            get
            {
                return ((_server.StartsWith("$INI")) ||
                        ((_server.Length > 0) && (_username.Length > 0) && (_password.Length > 0)));
            }
        }
    }
}
