﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGW.Shared.Encryption;

namespace FNOLPostOffice
{
    public static class FPOUtils
    {

        public static bool AsBoolean(string value)
        {
            bool rv = false;
            switch (value)
            {
                case "yes":
                case "true":
                case "on":
                case "1":
                    rv = true;
                    break;
                default:
                    break;
            }
            return rv;
        }

        public static string Decrypt(string encryptionKey, string encryptedString)
        {
            string rv = "";
            if (!string.IsNullOrEmpty(encryptedString))
            {
                SymmetricEncryption enc = new SymmetricEncryption(SymmetricEncryption.Provider.TripleDES, true);
                Data key = new Data(encryptionKey);
                Data encryptedData = new Data();
                encryptedData.Base64 = encryptedString;
                rv = enc.Decrypt(encryptedData, key).ToString();
            }
            return rv; 
        }

    }
}
