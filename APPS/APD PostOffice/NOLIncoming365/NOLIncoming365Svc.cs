﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using System.Configuration;
using System.Timers;
using Microsoft.Exchange.WebServices.Data;
using System.Xml;
using System.IO;
using System.Collections;

using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.DocIO;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.ExcelToPdfConverter;
using Syncfusion.XlsIO;
using Syncfusion.XPS;
using Syncfusion.HtmlConverter;
using System.Text.RegularExpressions;

using Syncfusion.Compression.Zip;

//using Microsoft.Win32;

//using ICSharpCode.SharpZipLib.Zip;
//using ICSharpCode.SharpZipLib.Core;

//----------------------------------------------------------------//
// PROCEDURE:    NOLIncoming365Svc
// SYSTEM:       Lynx Services APD
// AUTHOR:       Thomas Duranko
// FUNCTION:     This Office/365 service reads mail queues and 
//               processes attachments into APD
//               
// UPDATES:		25Jul2019 - TVD - Initial Development
//
// VERSION:     23Oct2020 - TVD - V1.0 - ReDesign of original
//                  NOLIncoming to DotNet and Office365
//              15Dec2020 - TVD - V1.1 - Added checks for properly 
//                  formatting of the LynxID
//              21Apr2021 - TVD - V1.2 - Fixed issue with vehicle numbers 
//                  greater an 99
//
//----------------------------------------------------------------//
namespace NOLIncoming365Svc
{
    public partial class NOLIncoming365Svc : ServiceBase
    {
        //---------------------------------------//
        // Global Vars
        //---------------------------------------//
        PGWAPDFoundation.APDService wsPGWAPDFoundation = new PGWAPDFoundation.APDService();
        PGWClaimPointFoundation.APDService wsPGWClaimPointFoundation = new PGWClaimPointFoundation.APDService();
        AppSettingsReader appSettingsReader = new AppSettingsReader();
        ExchangeService _service;
        private static Timer oTimer;

        string sAPPPath = string.Empty;
        string sAPPZipPath = string.Empty;
        string sAPPPDFPath = string.Empty;

        int iEventSeq = 1;
        bool bDebug = false;
        bool isRunning = false;
        bool bCleanUp = false;
        bool bInitParams = false;
        string sEventTransactionID = string.Empty;
        string sProcessingServer = string.Empty;
        string sTimeInterVal = string.Empty;
        string sDebug = string.Empty;
        string sOffice365User = string.Empty;
        string sOffice365UserPW = string.Empty;
        string sOffice365URL = string.Empty;

        string sEnvironmentCode = string.Empty;
        string sFailNotifyEnabled = string.Empty;
        string sFailNotifyFromEmail = string.Empty;
        string sFailNotifyRecipients = string.Empty;
        string sFailNotifySubject = string.Empty;
        string sValidExt = string.Empty;
        string sBlackListeddHosts = string.Empty;

        string sDatabaseName = string.Empty;
        string sAttachmentStageArea = string.Empty;
        string sAPPAttachmentStageArea = string.Empty;
        string sArchivePath = string.Empty;

        //-- Other Tools --//
        string sAPPAPDImage = string.Empty;

        // -- Claim Info
        XmlNode oXMLNode = null;
        string sInsuranceCompanyID = string.Empty;
        string sClaimAspectID = string.Empty;
        string sClaimAspectServiceChannelID = string.Empty;

        // -- Office365 Folders
        string sNOLReviewFolderID = string.Empty;
        string sNOLArchivedFolderID = string.Empty;
        FolderId oNOLReviewFolderID = null;
        FolderId oNOLArchivedFolderID = null;
        string sCurrSubject = string.Empty;

        // -- Email Valid Hosts --//
        string sSender = string.Empty;
        string sDomain = string.Empty;

        // -- FileIO & Cleanup --//
        ArrayList aFileNameList = new ArrayList();
        string sFileLength = string.Empty;
        string sFileData = string.Empty;
        string sFileExtension = string.Empty;
        private bool bRC;

        public NOLIncoming365Svc()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                // ----------------------------------------
                //  Variables
                // ----------------------------------------
                sTimeInterVal = appSettingsReader.GetValue("TimerInterval", typeof(string)).ToString();
                sProcessingServer = appSettingsReader.GetValue("ServerID", typeof(string)).ToString();
                sDebug = appSettingsReader.GetValue("Debug", typeof(string)).ToString();
                sOffice365User = appSettingsReader.GetValue("Office365User", typeof(string)).ToString();
                sOffice365UserPW = appSettingsReader.GetValue("Office365UserPW", typeof(string)).ToString();
                sOffice365URL = appSettingsReader.GetValue("Office365URL", typeof(string)).ToString();
                sAPPPath = appSettingsReader.GetValue("APPPath", typeof(string)).ToString();
                sAPPAPDImage = appSettingsReader.GetValue("APPPathImage", typeof(string)).ToString();
                sAPPZipPath = appSettingsReader.GetValue("APPPathZip", typeof(string)).ToString();
                sAPPPDFPath = appSettingsReader.GetValue("APPPathPDF", typeof(string)).ToString();
                sAPPAttachmentStageArea = appSettingsReader.GetValue("AttachmentStageArea", typeof(string)).ToString();


                //iEventSeq = 1;

                // ----------------------------------------
                //  Get Processing Server and Debug
                // ----------------------------------------
                if (sDebug.ToUpper() == "TRUE")
                {
                    bDebug = true;
                }
                else {
                    bDebug = false;
                }

                // ----------------------------------------
                //  Logging and Monitoring
                // ----------------------------------------
                sEventTransactionID = System.Guid.NewGuid().ToString();

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                //wsPGWAPDFoundation.LogEvent("NOLWebService", "Start", "Only for test", "", "");
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(START)", iEventSeq.ToString(), "STARTING", DateTime.Now + " - Main Start: Starting the NOLIncoming365Svc service...", "", "");
                iEventSeq = (iEventSeq + 1);


                // -------------------------------------
                //  Setup the timing event for polling
                // -------------------------------------
                oTimer = new System.Timers.Timer();
                oTimer.Interval = Int32.Parse(sTimeInterVal);
                oTimer.Elapsed += OnTimedEvent;
                oTimer.AutoReset = true;
                oTimer.Enabled = true;


                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(TIMERTICK)", iEventSeq.ToString(), "Timer initialized.", DateTime.Now + " - TimerTick: Timer initialized and started.", ("Timer Interval (In Millisec): " + oTimer.Interval), "");
                iEventSeq = (iEventSeq + 1);

                oTimer.Start();
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error Occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        protected override void OnStop()
        {
            // ----------------------------------------
            //  Debugging
            // ----------------------------------------
            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(END)", iEventSeq.ToString(), "STOPPING", DateTime.Now + " - Main End: Ending the NOLIncoming365Svc service...", "", "");
            iEventSeq = (iEventSeq + 1);
        }

        protected void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                int iStartPos = 0;
                int iEndPos = 0;
                bool bCleanUp = false;
                string sExchangeFolders = string.Empty;
                string sNoEmailsToProcess = string.Empty;
                string sEmailParams = string.Empty;
                string sSubject = string.Empty;
                string sLynxID = string.Empty;
                string sClaimNo = string.Empty;
                string sClientID = string.Empty;
                string sLossDate = string.Empty;
                string sInsured = string.Empty;
                string sNewFileName = string.Empty;
                string sBody = string.Empty;
                string sTagCleaned = string.Empty;
                string sClientName = string.Empty;
                string sApp = string.Empty;
                string sUserID = string.Empty;
                string sRC = string.Empty;
                string sPertainsTo = string.Empty;
                string sSuppSeqNumber = "0";
                string sDocumentType = string.Empty;
                string sCurrentFileExtension = string.Empty;
                string sDocumentSource = "Email";
                string sDocGUID = string.Empty;
                string sBodyFileName = string.Empty;
                string sSMTPEmailAddress = string.Empty;

                //-- Email Structure --//
                string sFrom = string.Empty;
                string sTo = string.Empty;
                string sCc = string.Empty;
                string sSent = string.Empty;
                string sNewEmail = string.Empty;
                string sSimpleTextEmail = string.Empty;
                string sAttachments = string.Empty;

                string sVehNumber = "1";

                XmlNode XMLReturn = null;
                string sCleanKeyWord = string.Empty;

                int iVehNumberLength = 0;

                //-- ZIP Processing Variables
                bool bZipFile = false;
                ArrayList aZipFileNames = new ArrayList();
                DirectoryInfo ZipDirInfo = null;
                
                //bool bInitParams = false;
                string sNOLReviewFolder = "";
                string sNOLArchivedFolder = "";
                int iAttachmentCnt = 0;
                //string sBody = "";
                //iEventSeq = 3;

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(TIMER)", iEventSeq.ToString(), "TIMER TICK", DateTime.Now + " - Timer_Tick - Processing Started: Processing the NOLIncoming365Svc events...", "", "");
                iEventSeq = (iEventSeq + 1);

                // -----------------------------------
                //  Email Processing Event
                // -----------------------------------
                if ((isRunning == false))
                {
                    isRunning = true;
                    sAttachmentStageArea = appSettingsReader.GetValue("AttachmentStageArea", typeof(string)).ToString();
                    sArchivePath = appSettingsReader.GetValue("ArchiveArea", typeof(string)).ToString();

                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILPROCESSING)", iEventSeq.ToString(), "Email Processing - Started", DateTime.Now + " - Email Processing Started: Reading Emails from Office/365...", "IsRunning: " + isRunning, "");
                    iEventSeq = (iEventSeq + 1);

                    //-----------------------------------
                    // Registration and Login Office/365         
                    //-----------------------------------
                    #region Registration/Init and Login Office/365
                    try
                    {
                        // ----------------------------------------
                        //  Debugging
                        // ----------------------------------------
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILREGISTRATION)", iEventSeq.ToString(), "Email Office/365 Registration - Started", DateTime.Now + " - Email Office/365 Registration Started: Logging into Office365 as apdnol365@lynxservices.com...", "", "");
                        iEventSeq = (iEventSeq + 1);

                        _service = new ExchangeService
                        {
                            Credentials = new WebCredentials(sOffice365User, sOffice365UserPW)
                        };
                    }
                    catch (Exception oExcept)
                    {
                        //---------------------------------//
                        // Office/365 Login Failed         //
                        //---------------------------------//
                        string sError = string.Empty;
                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                        sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                        LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Office/365 login failed.", sError, "");
                        iEventSeq = (iEventSeq + 1);

                        return;
                    }

                    //-----------------------------
                    // Office/365 Login Successful
                    //-----------------------------
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILREGISTRATION)", iEventSeq.ToString(), "Office/365 Login Successful...", DateTime.Now + " - Office/365 Login processed successfully: Office365 login processing complete...", "", "");
                    iEventSeq = (iEventSeq + 1);

                    //---------------------------------
                    // Office/365 - Processing Emails
                    //---------------------------------
                    _service.Url = new Uri(sOffice365URL);

                    //------------------------------------//
                    // Initalize Params from Config.xml   //
                    //------------------------------------//
                     bInitParams = InitParams();

                    //------------------------------------//
                    // Clean Up the file paths            //
                    //------------------------------------//
                    bCleanUp = CleanUp();
                    #endregion

                    //-----------------------------------------------//
                    // Go see what Office/365 folders are available  //
                    //-----------------------------------------------//
                    #region Office/365 folder processing
                    FindFoldersResults findResults = _service.FindFolders(WellKnownFolderName.Root, new FolderView(int.MaxValue) { Traversal = FolderTraversal.Deep });

                    //foreach (Folder folder in rootfolder.FindFolders(new FolderView(100)))
                    foreach (Folder folder in findResults.Folders)
                    {
                        //sExchangeFolders += (folder.DisplayName) + Environment.NewLine;
                        sExchangeFolders += (folder.DisplayName) + " : ";

                        //Find the NOLReview Fold
                        if (folder.DisplayName == "NOLReview")
                        {
                            // ----------------------------------------
                            //  NOLReview Folder Found
                            // ----------------------------------------
                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILFOLDERS)", iEventSeq.ToString(), "Searching for NOLReview folders in Office/365...", DateTime.Now + " - NOLReview folder found: FolderID/Name: " + folder.Id + " - " + folder.DisplayName, "", "");
                            iEventSeq = (iEventSeq + 1);
                            //sNOLReviewFolderID = "NOLReviewFolder" + folder.Id;
                            sNOLReviewFolderID = folder.Id.ToString();
                            oNOLReviewFolderID = folder.Id;
                            sNOLReviewFolder = folder.DisplayName;
                        }

                        // Find the NOLArchived Fold
                        if (folder.DisplayName == "NOLArchived")
                        {
                            // ----------------------------------------
                            //  NOLArchived Folder Found
                            // ----------------------------------------
                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILFOLDERS)", iEventSeq.ToString(), "Searching for NOLArchived folders in Office/365...", DateTime.Now + " - NOLArchived folder found: FolderID/Name: " + folder.Id + " - " + folder.DisplayName, "", "");
                            iEventSeq = (iEventSeq + 1);
                            //sNOLArchivedFolderID = "NOLArchivedFolder" + folder.Id;
                            sNOLArchivedFolderID = folder.Id.ToString();
                            oNOLArchivedFolderID = folder.Id;
                            sNOLArchivedFolder = folder.DisplayName;
                        }
                    }

                    if (sNOLReviewFolderID == string.Empty || sNOLArchivedFolderID == string.Empty)
                    {
                        // ----------------------------------------
                        //  ERROR: Share Folders not found
                        // ----------------------------------------
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "One or more NOL folders NOT found in Office/365...", DateTime.Now + " - NOLReview folder NOT found: NOLReviewFolderID: " + sNOLReviewFolderID + " sNOLArchivedFolderID: " + sNOLArchivedFolderID, "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                      
                    // ----------------------------------------
                    //  Show available folders
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILFOLDERS)", iEventSeq.ToString(), "Available Folders from Office/365...", DateTime.Now + " - Available Folders: Office365 Folders: " + sExchangeFolders, "", "");
                    iEventSeq = (iEventSeq + 1);
                    #endregion

                    //-------------------------------------//
                    // Check if any emails from Office/365 //
                    //-------------------------------------//
                    sNoEmailsToProcess = appSettingsReader.GetValue("NoEmailsToProcess", typeof(string)).ToString();

                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILCOUNT)", iEventSeq.ToString(), "EMAIL COUNT", DateTime.Now + " - EMAIL READY TO PROCESS", " - COUNT OF EMAILS WAITING: " + sNoEmailsToProcess, "");
                    iEventSeq = (iEventSeq + 1);

                    FindItemsResults<Item> InboxResults = _service.FindItems(WellKnownFolderName.Inbox, new ItemView(Int32.Parse(sNoEmailsToProcess)));
                    if (InboxResults.TotalCount >= 1)
                    {
                        //-----------------------------------//
                        // Go get the emails from Office/365 //
                        //-----------------------------------//
                        foreach (EmailMessage email in _service.FindItems(WellKnownFolderName.Inbox, new ItemView(Int32.Parse(sNoEmailsToProcess))))
                        {
                            //===================================//
                            // Processing each of the emails in  //
                            // the apdnol365@lynxservices.com    //
                            // inbox from Office/365             //
                            //===================================//
                            #region Processing Emails from Office/365
                            
                            // Check the Host to make sure they are valid NOL users
                            System.Net.Mail.MailAddress addr = new System.Net.Mail.MailAddress(email.From.Address);
                            sSender = addr.User;
                            sDomain = addr.Host;

                            if (!sBlackListeddHosts.Contains(sDomain.ToLower()))
                            {
                                // ----------------------------------------
                                //  Debugging
                                // ----------------------------------------
                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILHOST)", iEventSeq.ToString(), "Valid Email Host Found ", DateTime.Now + " - Valid email host found: " + sDomain, "", "");
                                iEventSeq = (iEventSeq + 1);

                                // Grab the mail details
                                sFrom = email.From.ToString();
                                sSMTPEmailAddress = email.Sender.Address;
                                //sReplyTo = email.ReplyTo.ToString();
                                sSubject = string.Empty;
                                sEmailParams = string.Empty;
                                sSubject = email.Subject.ToString().ToUpper();
                                sCurrSubject = sSubject;

                                sDocGUID = string.Empty;

                                // ----------------------------------------
                                //  Debugging
                                // ----------------------------------------
                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILS)", iEventSeq.ToString(), "Processing Emails Subject from Office/365...", DateTime.Now + " - Email Subject: " + sSubject, "", "");
                                iEventSeq = (iEventSeq + 1);

                                //=====================================//
                                // Processing the Subject Line details //
                                //=====================================//
                                #region Parsing Subject Line
                                // Parse Subject
                                if (sSubject.Contains("LYNX ID: "))
                                {
                                    string[] aKeyWords = sSubject.Split('/');
                                    //aKeyWords[0].Trim();

                                    foreach (string sKeyWord in aKeyWords)
                                    {
                                        //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 0", iEventSeq.ToString(), "DEBUGGING 0 - KEYWORD", DateTime.Now + " - DEBUGGING 0 ", "TOTAL KEYS: " + aKeyWords.Count() + "KEYWORD: " + sCleanKeyWord, "");
                                        //iEventSeq = (iEventSeq + 1);

                                        sCleanKeyWord = sKeyWord.ToUpper().Trim();

                                        // LynxID
                                        if (sCleanKeyWord.Contains("LYNX ID:"))
                                        {
                                            iStartPos = (sCleanKeyWord.IndexOf("LYNX ID: ", 0) + 9);
                                            sLynxID = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            if (sLynxID.Contains("-"))
                                            {
                                                //-- 15Dev2020 - TVD - Adding checks for properly formatted LynxID
                                                if (System.Text.RegularExpressions.Regex.IsMatch(sLynxID, @"^[\d-]+$"))
                                                {
                                                    iStartPos = (sLynxID.IndexOf("-", 1));
                                                    sVehNumber = sLynxID.Substring(iStartPos + 1, sLynxID.Length - (iStartPos + 1));
                                                    iVehNumberLength = sVehNumber.Length;

                                                    //-- Remove the Vehicle --//
                                                    //-- 21Apr2021 - TVD - Fixed issue with vehicle numbers greater an 99
                                                    //sLynxID = sLynxID.Substring(0, sLynxID.Length - 2);  
                                                    sLynxID = sLynxID.Substring(0, sLynxID.Length - (iVehNumberLength + 1));

                                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(LYNXID-VEH)", iEventSeq.ToString(), "LYNXID-VEH Split", DateTime.Now + " - Splitting the LYNXID tag to get LynxID and Vehicle.", "LynxIDOnly: " + sLynxID + ", Vehicle: " + sVehNumber, "");
                                                    iEventSeq = (iEventSeq + 1);
                                                }
                                                else
                                                {
                                                    //-- Badly formatted LynxID.  Reject this.
                                                    //-- Send email back to from explaining to check the LynxID formatting
                                                    //if (!sFrom.Contains(sOffice365User.Replace("@lynxservices.com", "")))
                                                    //{
                                                    //    ErrorNotify(sFrom, "NOLIncoming Error", "The LYNX ID in the subject line is not properly formatted.  Must Be LYNX ID: xxxxxxxx-x with nothing after it.  Exp: LYNX ID: 8150486-1, LynxID Received: " + sLynxID);
                                                    //}

                                                    NotifyBadLynxID(sSMTPEmailAddress, sFrom, sLynxID, email);
                                                    goto CleanUp;
                                                    //goto Done;
                                                }
                                            }
                                            sEmailParams += " : " + "LynxID: " + sLynxID;

                                            //-- Create DocGUID --//
                                            sDocGUID = sLynxID + "-" + System.Guid.NewGuid().ToString();

                                        }

                                        // Claim Number
                                        if (sCleanKeyWord.Contains("CLAIM #:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 2", iEventSeq.ToString(), "DEBUGGING 2 - CLAIM", DateTime.Now + " - DEBUGGING 2 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("CLAIM #: ", 0) + 9);
                                            sClaimNo = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "ClaimNo: " + sClaimNo;
                                        }

                                        // Loss Date
                                        if (sCleanKeyWord.Contains("LOSS DATE:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 3", iEventSeq.ToString(), "DEBUGGING 3 - LOSSDATE", DateTime.Now + " - DEBUGGING 3 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("LOSS DATE: ", 0) + 11);
                                            sLossDate = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "LossDate: " + sLossDate;
                                        }

                                        // Insured
                                        if (sCleanKeyWord.Contains("INSURED:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 4", iEventSeq.ToString(), "DEBUGGING 4 - INSURED", DateTime.Now + " - DEBUGGING 4 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("INSURED: ", 0) + 9);
                                            sInsured = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "Insured: " + sInsured;
                                        }

                                        // -- APDImaging/OnBase - If APP: APDIMAGE or ONBASE is on the subject line
                                        // -- this says send it via fax to the APD Imaging queue or OnBase queue.
                                        if (sCleanKeyWord.Contains("APP:"))
                                        {
                                            //-- Notify the sender that the email is being reviewed
                                            wsPGWAPDFoundation.SendMail(sSMTPEmailAddress, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(), "", "EMAIL REVIEW - APD Imaging.", "This email is being sent to our APD Imaging Queue for further review.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());

                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APP)", iEventSeq.ToString(), "APP: tag found in subject line", DateTime.Now + " - APP: tag found in subject line, APP: processing started ", "APP: " + sCleanKeyWord, "");
                                            iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("APP: ", 0) + 5);

                                            sApp = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APPTAG)", iEventSeq.ToString(), "APP: tag", DateTime.Now + " - APP: tag is ", "APP: " + sApp, "");
                                            iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "APP: " + sApp;
                                        }

                                        // Client ID
                                        if (sCleanKeyWord.Contains("CLIENTID:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 6", iEventSeq.ToString(), "DEBUGGING 6 - CLIENTID", DateTime.Now + " - DEBUGGING 6 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("CLIENTID: ", 0) + 9);
                                            sClientID = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "ClientID: " + sClientID;
                                        }

                                        // Client Name
                                        if (sCleanKeyWord.Contains("CLIENTNAME:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 7", iEventSeq.ToString(), "DEBUGGING 7 - CLIENTNAME", DateTime.Now + " - DEBUGGING 7 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("CLIENTNAME: ", 0) + 9);
                                            sClientName = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "ClientName: " + sClientName;
                                        }

                                        // UserID 
                                        if (sCleanKeyWord.Contains("USERID:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 8", iEventSeq.ToString(), "DEBUGGING 8 - USERID", DateTime.Now + " - DEBUGGING 8 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("USERID: ", 0) + 9);
                                            sUserID = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "UserID: " + sUserID;
                                        }

                                        // PertainsTo
                                        if (sCleanKeyWord.Contains("PERTAINSTO:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 9", iEventSeq.ToString(), "DEBUGGING 9 - PERTAINSTO", DateTime.Now + " - DEBUGGING 9 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("PERTAINSTO: ", 0) + 9);
                                            sPertainsTo = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "PertainsTo: " + sPertainsTo;
                                        }

                                        // DocumentType
                                        if (sCleanKeyWord.Contains("DOCUMENTTYPE:"))
                                        {
                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 10", iEventSeq.ToString(), "DEBUGGING 10 - DOCUMENTTYPE", DateTime.Now + " - DEBUGGING 10 ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            iStartPos = (sCleanKeyWord.IndexOf("DOCUMENTTYPE: ", 0) + 9);
                                            sDocumentType = sCleanKeyWord.Substring(iStartPos, sCleanKeyWord.Length - iStartPos);

                                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "DONE", iEventSeq.ToString(), "DONE", DateTime.Now + " - DONE ", "", "");
                                            //iEventSeq = (iEventSeq + 1);

                                            sEmailParams += " : " + "DocumentType: " + sDocumentType;
                                        }
                                    }

                                    //-- Default UserID --//
                                    if (sUserID == string.Empty)
                                    {
                                        sUserID = "0";
                                    }

                                    //-- Default PertainsTo --//
                                    if (sPertainsTo == string.Empty)
                                    {
                                        if (sVehNumber == string.Empty)
                                        {
                                            sPertainsTo = "VEH01";
                                        }
                                        else
                                        {
                                            //-- 14Oct2021 - TVD - Fixing > 99 vehicle numbers
                                            if (sVehNumber.Length >= 2)
                                            {
                                                sPertainsTo = "VEH" + sVehNumber;
                                            }
                                            else
                                            {
                                                sPertainsTo = "VEH0" + sVehNumber;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        sPertainsTo = "VEH01";
                                    }

                                    //-- Default DocumentType --//
                                    if (sDocumentType == string.Empty)
                                    {
                                        sDocumentType = "Other";
                                    }

                                    //-- Default DocumentSource --//
                                    if (sDocumentSource == string.Empty)
                                    {
                                        sDocumentSource = "Email";
                                    }

                                    //-- Default App --//
                                    if (sApp == string.Empty)
                                    {
                                        sApp = "APD";
                                    }


                                    //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUGGING 11", iEventSeq.ToString(), "DEBUGGING 11 - DONE PARSING", DateTime.Now + " - DEBUGGING 11 ", "VARS: " + Int32.Parse(sLynxID) + " -- " + Int32.Parse(sVehNumber), "");
                                    //iEventSeq = (iEventSeq + 1);

                                    // ----------------------------------------
                                    //  If clientid or clientname are blank
                                    //  go get it based on lynxid
                                    // ----------------------------------------
                                    XMLReturn = GetInscCompIDByLynxID(Int32.Parse(sLynxID), Int32.Parse(sVehNumber));

                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-TaskPerformJob(GetInscCompIDByLynxID)", iEventSeq.ToString(), "XMLParse", DateTime.Now + " - GetInscCompIDByLynxID XML Data..", XMLReturn.OuterXml, "");
                                    iEventSeq = (iEventSeq + 1);

                                    if (!XMLReturn.OuterXml.Contains("ERROR"))
                                    {
                                        oXMLNode = XMLReturn.SelectSingleNode("/Claim");
                                        sInsuranceCompanyID = GetXMLNodeAttributeValue(oXMLNode, "InsuranceCompanyID");
                                        sClaimAspectID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectID");
                                        sClaimAspectServiceChannelID = GetXMLNodeAttributeValue(oXMLNode, "ClaimAspectServiceChannelID");
                                    }

                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILCONTENTS)", iEventSeq.ToString(), "Email Details...", DateTime.Now + " - Email Details: Details: " + sEmailParams, sInsuranceCompanyID, "");
                                    iEventSeq = (iEventSeq + 1);
                                    #endregion

                                    // If the ONBASE flag is set in the Subject line
                                    // send the file to HQ OnBase
                                    if (sApp == "ONBASE")
                                    {
                                        //-- Sending job over to OnBase for processing
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APP-ONBASE)", iEventSeq.ToString(), "OnBase processing ", "OnBase: " + DateTime.Now + " forwarding to OnBase for processing...", "", "");
                                        iEventSeq = (iEventSeq + 1);

                                        bRC = MoveEmailToOnBase(email);

                                        if (!bRC)
                                        {
                                            //-- Badly formatted LynxID.  Reject this.
                                            //-- Send email back to from explaining to check the LynxID formatting
                                            ErrorNotify(sSMTPEmailAddress, "NOLIncoming Error (OnBase)", "Email couldn't be forwarded to OnBase due to an error.  Check subject line format.  APP: ONBASE");

                                            throw new System.Exception(string.Format("Error: {0}", "PostOffice-NOLIncoming(APP-ONBASE): ERROR: Forward of email to OnBase failed...  "));
                                        }

                                        goto Done;
                                    }

                                    //===================================//
                                    // Processing Attachments found in   //
                                    // the apdnol365@lynxservices.com    //
                                    // inbox from Office/365             //
                                    //===================================//
                                    #region Processing Attachments from Office/365

                                    //---------------------------------
                                    // Check if Attachment Exists      
                                    // and process the attachments     
                                    //---------------------------------
                                    if (email.HasAttachments)
                                    {

                                        // -- CleanUp -- //
                                        aFileNameList.Clear();

                                        // Save the attachments to a staging area
                                        EmailMessage message = EmailMessage.Bind(_service, email.Id, new PropertySet(ItemSchema.Attachments));

                                        // ----------------------------------------
                                        //  Debugging
                                        // ----------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILATTACHMENTS)", iEventSeq.ToString(), "Email Attachments Exist...", DateTime.Now + " - Email Attachments Exist: ", "Attachment Count Including Embedded is: " + message.Attachments.Count(), "");
                                        iEventSeq = (iEventSeq + 1);

                                        foreach (Attachment attachment in message.Attachments)
                                        {
                                            //-- Check and make sure the attachment is NOT InLine meaning it's not in the
                                            //-- Body of the email
                                            if (attachment is FileAttachment && !attachment.IsInline)
                                            {
                                                ++iAttachmentCnt;

                                                // -- Get details about attachment from Outlook365 -- //
                                                FileAttachment fileAttachment = attachment as FileAttachment;

                                                // Verify that the attachments are valid document types
                                                System.IO.Path.GetExtension(fileAttachment.Name.ToLower()).Contains(sValidExt);
                                                if (sValidExt.Contains(System.IO.Path.GetExtension(fileAttachment.Name.ToLower())))
                                                {
                                                    // ----------------------------------------
                                                    //  Logging - Good Extension
                                                    // ----------------------------------------
                                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILATTACHMENTS)", iEventSeq.ToString(), "Attachment extension accepted...", DateTime.Now + " - Email Attachment Extension is supported and saved to stage area: Attachement Extension Details: " + System.IO.Path.GetExtension(fileAttachment.Name.ToLower()), "", "");
                                                    iEventSeq = (iEventSeq + 1);

                                                    // Save the Attachment to staging - Office/365
                                                    string sCurrentFileName = string.Empty;
                                                    sCurrentFileName = sDocGUID + fileAttachment.Name;

                                                    try
                                                    {
                                                        fileAttachment.Load(sAttachmentStageArea + sCurrentFileName);

                                                        // Save Current Attachment files for later deletion
                                                        aFileNameList.Add(sCurrentFileName);
                                                    }
                                                    catch (Exception oExcept)
                                                    {
                                                        // ---------------------------------
                                                        //  Error handler and notifications
                                                        // ---------------------------------
                                                        string sError = string.Empty;
                                                        System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                                                        sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Attachment Load Error Occured: Exception thrown", sError, "");
                                                        iEventSeq = (iEventSeq + 1);

                                                        goto Done;
                                                    }

                                                    //--------------------------------------
                                                    // Set DocumentType based on attachment
                                                    // extension.
                                                    //--------------------------------------

                                                    sCurrentFileExtension = System.IO.Path.GetExtension(fileAttachment.Name.ToLower());

                                                    switch (sCurrentFileExtension)
                                                    {
                                                        case ".jpg":
                                                        case ".jpeg":
                                                        case ".png":
                                                        case ".gif":
                                                        case ".bit":
                                                            sDocumentType = "Photograph";
                                                            break;
                                                        case ".doc":
                                                        case ".docx":
                                                        case ".rtf":
                                                        case ".txt":
                                                            sDocumentType = "Closing Documents";
                                                            break;
                                                        case ".xsl":
                                                        case ".xslx":
                                                            sDocumentType = "Estimate";
                                                            break;
                                                        case ".tif":
                                                        case ".tiff":
                                                            sDocumentType = "Fax Cover Sheet";
                                                            break;
                                                        //case ".zip":
                                                            //sDocumentType = "Other";
                                                            //break;
                                                        default:
                                                            sDocumentType = "Other";
                                                            break;
                                                    }

                                                    // ------------------------------------------
                                                    //  Logging - Check PDF Created Successfully
                                                    // ------------------------------------------
                                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(DOCTYPE)", iEventSeq.ToString(), "DocumentType Identified as: " + sDocumentType, "FileExtension: " + sCurrentFileExtension.ToLower(), "", "");
                                                    iEventSeq = (iEventSeq + 1);

                                                    //-----------------------------------//
                                                    // Check if attachment is a ZIP.  If
                                                    // so unpack it and process       
                                                    //-----------------------------------//
                                                    if (sCurrentFileExtension.ToLower() == ".zip")
                                                    {
                                                        bZipFile = true;

                                                        // ------------------------------------------
                                                        //  Logging - Check PDF Created Successfully
                                                        // ------------------------------------------
                                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ZIPFILE)", iEventSeq.ToString(), "Attachment Identified as ZIP.  Processing using ZIP Process.", "FileExtension: " + sCurrentFileExtension.ToLower(), "PathFile: " + sAPPAttachmentStageArea + sCurrentFileName + ", CurrentFileName: " + sCurrentFileName + " APPZipPath: " + sAPPZipPath, "");
                                                        iEventSeq = (iEventSeq + 1);

                                                        aZipFileNames = ProcessZipFile(fileAttachment, sCurrentFileName, sAPPZipPath + sCurrentFileName, sAPPZipPath);

                                                        // ------------------------------------------
                                                        //  Logging - Check PDF Created Successfully
                                                        // ------------------------------------------
                                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ZIPFILE)", iEventSeq.ToString(), "Done processing.", "", "", "");
                                                        iEventSeq = (iEventSeq + 1);

                                                        //goto Done;                             
                                                    }

                                                    if (!bZipFile)
                                                    {
                                                        //-- Not a ZIP File
                                                        sCurrentFileName = ConvertSendAPDPostOffice(sAttachmentStageArea, sCurrentFileName);
                                                    }
                                                    else
                                                    {
                                                        //-- IS a ZIP File
                                                        //-- Process the files in the ZipStageArea
                                                        foreach (string sZipFile in aZipFileNames)
                                                        {
                                                            // ------------------------------------------
                                                            //  Logging 
                                                            // ------------------------------------------
                                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(UNZIPPEDFILES)", iEventSeq.ToString(), "UnZipped Files: " + sZipFile + " being processed to APDPostOffice Queue...", "UnZipped attachment File being processed to APD PostOffice Queue: " + DateTime.Now + " FileName: " + sZipFile, "", "");
                                                            iEventSeq = (iEventSeq + 1);

                                                            //-- Convert and send to APD PostOffice
                                                            ConvertSendAPDPostOffice(sAPPZipPath, sZipFile);

                                                            //-- Insert job into APD PostOffice
                                                            InsertJobIntoAPDPostOffice(sLynxID, sClientID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sAPPZipPath, sZipFile);

                                                            //-- Delete processed file from Zip Area
                                                            //ZipDirInfo = new DirectoryInfo(sAPPZipPath);
                                                            //ZipDirInfo.GetFiles(sZipFile);
                                                            //ZipDirInfo.Delete();

                                                            // ------------------------------------------
                                                            //  Logging
                                                            // ------------------------------------------
                                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ZIPFILEIO)", iEventSeq.ToString(), "Deleting processed zip stage file. " + sNewFileName + " was deleted Successfully from zip stage area...", "File from ZIP successfully deleted: " + DateTime.Now + " Path/FileName: " + sAPPZipPath + sZipFile, "", "");
                                                            iEventSeq = (iEventSeq + 1);

                                                        }
                                                    }

                                                    // If the APDIMAGE flag is set in the Subject line
                                                    // send the file to APDImaging
                                                    if (sApp == "APDIMAGE")
                                                    {
                                                        System.IO.FileInfo fiAttachment2 = new System.IO.FileInfo(sAttachmentStageArea + sNewFileName);
                                                        if (fiAttachment2.Exists)
                                                        {
                                                            fiAttachment2.CopyTo(sAPPAPDImage + "ATTACH_" + sNewFileName);
                                                        }

                                                        // ------------------------------------------
                                                        //  Logging - Check PDF Created Successfully
                                                        // ------------------------------------------
                                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDIMAGE_FILEIO)", iEventSeq.ToString(), "Attachment File " + sNewFileName + " was saved to APDImaging Queue Successfully...", "Attachment File successfully saved to APDImaging Queue: " + DateTime.Now + " Path/FileName: " + sAPPAPDImage + "ATTACH_" + sNewFileName, "", "");
                                                        iEventSeq = (iEventSeq + 1);

                                                        goto Done;
                                                    }

                                                    // -------------------------------------
                                                    //  Check if the new PDF file exists
                                                    // -------------------------------------
                                                    //sNewFileName = sNewFileName.Replace(sCurrentFileExtension, ".pdf");

                                                    if (!bZipFile)
                                                    {
                                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(PDFEXISTS)", iEventSeq.ToString(), "StageArea: " + sAttachmentStageArea + ", Attachment File: " + sCurrentFileName + " ready for processing in PDFStagingArea...", "Attachment READY to INSERT into PostOffice: " + DateTime.Now + " Path/FileName: " + sAttachmentStageArea + sCurrentFileName, "", "");
                                                        iEventSeq = (iEventSeq + 1);

                                                        //if (File.Exists(sAttachmentStageArea + sCurrentFileName))
                                                        //{
                                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(PDFEXISTS)", iEventSeq.ToString(), "StageArea: " + sAttachmentStageArea + ", Attachment PathFile: " + sAttachmentStageArea + sCurrentFileName + " EXISTS...", "IFEXISTS: " + DateTime.Now + " Path/FileName: " + (File.Exists(sAttachmentStageArea + sCurrentFileName)), "", "");
                                                            iEventSeq = (iEventSeq + 1);

                                                            if (File.Exists(sAttachmentStageArea + sCurrentFileName))
                                                            {
                                                                //-- Insert job into APD PostOffice
                                                                InsertJobIntoAPDPostOffice(sLynxID, sClientID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sAttachmentStageArea, sCurrentFileName);
                                                            }
                                                            else
                                                            {
                                                                // ------------------------------------------
                                                                //  ERROR - PDF Create UnSuccessful
                                                                // ------------------------------------------
                                                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + sNewFileName + " was NOT created Successfully...", "ERROR - File NOT successfully converted to PDF: " + DateTime.Now + " ERROR FileName: " + sNewFileName, "", "");
                                                                iEventSeq = (iEventSeq + 1);
                                                            }
                                                        //}
                                                    }

                                                    // ----------------------------------------
                                                    //  Logging - Attachment Save Stage Area
                                                    // ----------------------------------------
                                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILATTACHMENTS)", iEventSeq.ToString(), "Attachment Converted and Stage Saved...", DateTime.Now + " - Email Attachment Converted and saved to stage area: Attachement Stage Save Details: " + sAttachmentStageArea + sNewFileName, "", "");
                                                    iEventSeq = (iEventSeq + 1);
                                                }
                                                else
                                                {
                                                    // ----------------------------------------
                                                    //  Logging - Bad Extension
                                                    // ----------------------------------------
                                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "Bad attachment extension...", DateTime.Now + " - Email Attachment Extension is not supported: Attachement Extension Details: " + System.IO.Path.GetExtension(fileAttachment.Name.ToLower()), "", "");
                                                    iEventSeq = (iEventSeq + 1);
                                                }
                                            }
                                            else // Attachment is an item attachment.
                                            {
                                                //??????? NOT SURE WHERE THIS CODE IS GOING ????????
                                                // ----------------------------------------
                                                //  Logging - Unknown Attachment Type
                                                // ----------------------------------------
                                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "Unknown attachment type...", DateTime.Now + " - Email Attachment type is not known: Attachement Extension Details: " + System.IO.Path.GetFileName(attachment.Name.ToLower()), "", "");
                                                iEventSeq = (iEventSeq + 1);

                                                //ItemAttachment itemAttachment = attachment as ItemAttachment;
                                                // Load attachment into memory and write out the subject.
                                                // This does not save the file like it does with a file attachment.
                                                // This call results in a GetAttachment call to EWS.
                                                //itemAttachment.Load();
                                                //lstStatus.Items.Add(DateTime.Now + " - Item attachment name: " + itemAttachment.Name);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        // ----------------------------------------
                                        //  Logging - No Attachment
                                        // ----------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILATTACHMENTS)", iEventSeq.ToString(), "Email No Attachents...", DateTime.Now + " - Email NO Attachments Exist: Email NO Attachments Exist.", "", "");
                                        iEventSeq = (iEventSeq + 1);
                                    }
                                    #endregion

                                    //===================================//
                                    // Processing Body of Email as a     //
                                    // Separate document                 //
                                    //===================================//
                                    #region Processing the Body of the Office/365 Email
                                    // ----------------------------------------
                                    //  Logging - Processing Email Body
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(HTMLEmailBody)", iEventSeq.ToString(), "HTML Email Body Starting...", "HTML Email Body Starting: " + DateTime.Now + "", "", "");
                                    iEventSeq = (iEventSeq + 1);

                                    //---------------------------------
                                    // Body or Email - Save to File
                                    //---------------------------------
                                    email.Load(new PropertySet(new PropertyDefinitionBase[] { EmailMessageSchema.DateTimeSent, EmailMessageSchema.CcRecipients, EmailMessageSchema.Attachments, EmailMessageSchema.Subject, EmailMessageSchema.DisplayTo, EmailMessageSchema.From, EmailMessageSchema.Sender, EmailMessageSchema.ToRecipients, EmailMessageSchema.InternetMessageHeaders, EmailMessageSchema.Body }));
                                    //email.Load(new PropertySet(ItemSchema.MimeContent));
                                    //sBodyText = (email as EmailMessage).Body.Text;

                                    sSubject = (email as EmailMessage).Subject;
                                    sFrom = (email as EmailMessage).From.Name;
                                    sTo = (email as EmailMessage).DisplayTo;
                                    try
                                    {
                                        sCc = (email as EmailMessage).DisplayCc;
                                    }
                                    catch
                                    {
                                        sCc = "";
                                    }
                                    sSent = (email as EmailMessage).DateTimeSent.ToString();
                                    //sBody = (email as EmailMessage).Body;
                                    sBody = (email as EmailMessage).Body;

                                    try
                                    {
                                        if ((email as EmailMessage).HasAttachments)
                                        {
                                            sAttachments = "Total Attachments: " + iAttachmentCnt;
                                        }
                                        else
                                        {
                                            sAttachments = "None";
                                        }
                                    }
                                    catch
                                    {
                                        sAttachments = "None";
                                    }

                                    //sNewEmail = "<font size='6'>";
                                    sNewEmail = "<b>From: </b>" + sFrom;
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<b>Sent: </b>" + sSent;
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<b>To: </b>" + sTo;
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<b>Cc: </b>" + sCc;
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<b>Subject: </b>" + sSubject;
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<br/>";
                                    sNewEmail += "<b>Attachments: </b>" + sAttachments;
                                    sNewEmail += "<br/>";
                                    sNewEmail += HTMLRemoveImages(sBody);
                                    sNewEmail += "<br/>";
                                    //sNewEmail += "</font>";
                                    //var mimeContent = email.MimeContent;

                                    sBodyFileName = sDocGUID + ".eml";

                                    ConvertHTMLToPDF(sNewEmail, sBodyFileName);

                                    // ---------------------------------------- //
                                    //  Simple text version of email for notes
                                    // ---------------------------------------- //
                                    sSimpleTextEmail = "From: " + sFrom + "\r\n";
                                    sSimpleTextEmail += "Sent: " + sSent + "\r\n";
                                    sSimpleTextEmail += "To: " + sTo + "\r\n\r\n";
                                    sSimpleTextEmail += "Cc: " + sCc + "\r\n";
                                    sSimpleTextEmail += "Subject: " + sSubject + "\r\n\r\n";
                                    sSimpleTextEmail += "Body: " + HTMLToText(sBody) + "\r\n\r\n";
                                    sSimpleTextEmail += "Attachments: " + sAttachments + "\r\n";

                                   // sSimpleTextEmail = HTMLToText(sBody);

                                    // ----------------------------------------
                                    //  Logging - Processing Email Body
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(HTMLEmailBody)", iEventSeq.ToString(), "HTML Email Body Details...", "Creating temp HTML Email Body Details file: " + DateTime.Now + " Temp Body FileName: " + sBodyFileName, string.Format("Simple Note Email: {0}", sSimpleTextEmail), "");
                                    iEventSeq = (iEventSeq + 1);

                                    //-- Write the temp file to storage -->
                                    using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(sAttachmentStageArea, sBodyFileName), true))
                                    {
                                        outputFile.WriteLine(sBody);
                                    }

                                    sNewFileName = sBodyFileName.Replace(System.IO.Path.GetExtension(sBodyFileName), ".pdf");
                                    sDocumentType = "Other";

                                    // -------------------------------------
                                    //  Check if the new PDF file exists
                                    // -------------------------------------
                                    if (File.Exists(sAttachmentStageArea + sNewFileName))
                                    {
                                        // ------------------------------------------
                                        //  Logging - Check PDF Created Successfully
                                        // ------------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(FILEIO)", iEventSeq.ToString(), "File " + sNewFileName + " was created Successfully...", "File successfully converted from TXT to PDF: " + DateTime.Now + " Body FileName: " + sNewFileName, "", "");
                                        iEventSeq = (iEventSeq + 1);

                                        //if (sAPP == "APDIMAGE")
                                        //{
                                        //    lstStatus.Items.Add(DateTime.Now + " - Sending JOB to APDImaging. ");
                                        //    document.Save(sAPPAPDImage + "EMAIL_" + sNewFileName);
                                        //    lstStatus.Items.Add("Sent");
                                        //    goto Done;
                                        //}

                                        // ------------------------------------------
                                        //  IF APDIMAGE in Subject send the body of
                                        //  the email to APDImaging Queue
                                        // ------------------------------------------
                                        if (sApp == "APDIMAGE")
                                        {
                                            System.IO.FileInfo fiAttachment2 = new System.IO.FileInfo(sAttachmentStageArea + sNewFileName);
                                            if (fiAttachment2.Exists)
                                            {
                                                fiAttachment2.CopyTo(sAPPAPDImage + "EMAIL_" + sNewFileName);
                                            }

                                            // ------------------------------------------
                                            //  Logging - Check PDF Created Successfully
                                            // ------------------------------------------
                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDIMAGE_FILEIO)", iEventSeq.ToString(), "Attachment File " + sNewFileName + " was saved to APDImaging Queue Successfully...", "Attachment File successfully saved to APDImaging Queue: " + DateTime.Now + " Path/FileName: " + sAPPAPDImage + "ATTACH_" + sNewFileName, " Path/FileName: " + sAPPAPDImage + "ATTACH_" + sNewFileName, "");
                                            iEventSeq = (iEventSeq + 1);
                                        }

                                        System.IO.FileInfo fiBody1 = new System.IO.FileInfo(sAttachmentStageArea + sNewFileName);

                                        sFileLength = fiBody1.Length.ToString();
                                        sFileExtension = fiBody1.Extension;
                                        sFileExtension = sFileExtension.Substring(1, sFileExtension.Length - 1);

                                        sFileData = GetBinData(sAttachmentStageArea + sNewFileName);

                                        // ------------------------------------------
                                        //  Logging - Body file processed.  Time to 
                                        //  clean up the mess
                                        // ------------------------------------------
                                        fiBody1.MoveTo(sArchivePath + "BODY_" + sNewFileName);

                                        if (File.Exists(sAttachmentStageArea + "BODY_" + sNewFileName))
                                        {
                                            // ------------------------------------------
                                            //  ERROR - Move PDF to Archive Failed
                                            // ------------------------------------------
                                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + "BODY_" + sNewFileName + " was NOT moved Successfully to the archive...", "ERROR - File NOT successfully moved from StageArea to Archive: " + DateTime.Now + " ERROR Archive FileName: " + "BODY_" + sNewFileName, "", "");
                                            iEventSeq = (iEventSeq + 1);
                                        }
                                    }
                                    else
                                    {
                                        // ------------------------------------------
                                        //  ERROR - PDF Create UnSuccessful
                                        // ------------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + sNewFileName + " was NOT created Successfully...", "ERROR - File NOT successfully converted from TXT to PDF: " + DateTime.Now + " ERROR Body FileName: " + sNewFileName, "", "");
                                        iEventSeq = (iEventSeq + 1);
                                    }
                                    //Console.WriteLine(File.Exists(curFile) ? "File exists." : "File does not exist.");

                                    // ----------------------------------------
                                    //  Logging - Processing Email Body
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(HTMLEmailBody)", iEventSeq.ToString(), "HTML Email Body Ending...", "HTML Email Body Ending: " + DateTime.Now + "", "", "");
                                    iEventSeq = (iEventSeq + 1);

                                    // ----------------------------------------
                                    //  Logging - Processing Email Body
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILCONTENTS)", iEventSeq.ToString(), "Email Body Details...", DateTime.Now + " - Email Body Details: Details: " + sBody, "", "");
                                    iEventSeq = (iEventSeq + 1);
                                    #endregion

                                    Done:

                                    //=================================//
                                    // Move email to Review Folder     //
                                    //=================================//
                                    #region Moving Emails that need Reviewed
                                    PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject, EmailMessageSchema.ParentFolderId);
                                    EmailMessage beforeMessage = EmailMessage.Bind(_service, email.Id, propSet);

                                    Folder rootfolder = Folder.Bind(_service, WellKnownFolderName.MsgFolderRoot);
                                    rootfolder.Load();

                                    if (sNOLReviewFolderID == string.Empty || sNOLArchivedFolderID == string.Empty)
                                    {
                                        foreach (Folder folder in rootfolder.FindFolders(new FolderView(100)))
                                        {
                                            // Find the NOLReview Fold
                                            if (folder.DisplayName == "NOLReview")
                                            {
                                                //lstStatus.Items.Add(DateTime.Now + " - NOLReview Folder Found... ");
                                                oNOLReviewFolderID = folder.Id;
                                                sNOLReviewFolder = folder.DisplayName;
                                            }

                                            // Find the NOLArchived Fold
                                            if (folder.DisplayName == "NOLArchived")
                                            {
                                                //lstStatus.Items.Add(DateTime.Now + " - NOLArchived Folder Found... ");
                                                oNOLArchivedFolderID = folder.Id;
                                                sNOLArchivedFolder = folder.DisplayName;
                                            }
                                        }

                                        // Moving the Bad Email to NOLReview
                                        //Item item = beforeMessage.Move(oNOLReviewFolderID);

                                        //lstStatus.Items.Add(DateTime.Now + " - Moving email '" + email.Subject + "' to '" + sNOLReviewFolder + "' folder.");
                                    }
                                }
                                else
                                {
                                    NotifyBadLynxID(sSMTPEmailAddress, sFrom, sLynxID, email);
                                    goto CleanUp;
                                }
                                #endregion

                                //-------------------------------------//
                                // Go see whats in the Archive folder  //
                                //-------------------------------------//
                                #region Moving Completed Emails to the Archive Folder
                                //indFoldersResults findArchiveFolder = _service.FindFolders(WellKnownFolderName.Root, new FolderView(int.MaxValue) { Traversal = FolderTraversal.Deep });
                                //foreach (Folder folder in findArchiveFolder.Folders)
                                //{
                                    //txtFolders.Text += (folder.DisplayName) + Environment.NewLine;
                                //}


                                //-- If all was good and no errors, send everything to APD PostOffice


                                //--------------------------------------
                                // Check Lynxid to make sure its valid
                                //--------------------------------------
                                XMLReturn = wsPGWAPDFoundation.ExecuteSpAsXML("uspClaimSearchWSXML", "@LynxID=" + sLynxID);

                                if (XMLReturn.OuterXml.Contains("InsuranceCompany"))
                                {
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CHECKCLAIM)", iEventSeq.ToString(), "APD Claim/LynxID Exists: ", DateTime.Now + " - LynxID exists in APD: Good Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                                else
                                {
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CHECKCLAIM)", iEventSeq.ToString(), "APD Claim/LynxID DOES NOT Exists: ", DateTime.Now + " - LynxID DOES NOT exists in APD: Bad Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                                    iEventSeq = (iEventSeq + 1);

                                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-NOLIncoming(CHECKCLAIM): ERROR APD Claim/LynxID DOES NOT Exists...  " + XMLReturn.OuterXml));
                                }
                                #endregion


                                //=======================================//
                                // Create the BODY APD PostOffice JobXML //
                                //=======================================//
                                #region Creating the PostOffice Job

                                //// ----------------------------------------
                                ////  Debugging
                                //// ----------------------------------------
                                //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUG 1", iEventSeq.ToString(), "DEBUG 1", DateTime.Now + " - Ready to Create PostOfficeJob.", "", "");
                                //iEventSeq = (iEventSeq + 1);

                                sRC = CreatePostOfficeJob(sLynxID, sInsuranceCompanyID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sNewFileName, sFileExtension, sFileLength, sFileData);

                                //// ----------------------------------------
                                ////  Debugging
                                //// ----------------------------------------
                                //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUG 2", iEventSeq.ToString(), "DEBUG 2", DateTime.Now + " - Done Creating PostOfficeJob.", "RC: " + sRC, "");
                                //iEventSeq = (iEventSeq + 1);

                                if (sRC.ToUpper() == "SUCCESS")
                                {

                                    //=======================================//
                                    // Create the BODY/APD Notes APD         //
                                    // PostOffice JobXML                     //
                                    //=======================================//
                                    #region Creating the PostOffice Job
                                    //sRC = CreatePostOfficeJob(sLynxID, sClientID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sNewFileName, sFileExtension, sFileLength, sFileData);


                                    #endregion

                                    //----------------------
                                    // Good Response
                                    //----------------------
                                    //Move email to archive
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILMOVE)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Parameters...", string.Format("Params: {0} <---> {1}", email.Id.ToString(), oNOLArchivedFolderID.ToString()), "");
                                    iEventSeq = (iEventSeq + 1);

                                    Boolean bRC = MoveEmail(email, oNOLArchivedFolderID);

                                    if (bRC)
                                    {
                                        // -- Successful -- //
                                        // ----------------------------------------
                                        //  Debugging
                                        // ----------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILARCH)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Email SUCCESSFULLY moved to the archive...", string.Format("RC: {0}", bRC.ToString()), "");
                                        iEventSeq = (iEventSeq + 1);
                                    }
                                    else
                                    {
                                        // -- Failed -- //
                                        // ----------------------------------------
                                        //  Debugging
                                        // ----------------------------------------
                                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILARCH)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Email FAILED move to archive...", string.Format("RC: {0}", bRC.ToString()), "");
                                        iEventSeq = (iEventSeq + 1);
                                    }
                                }
                                else
                                {
                                    //Throw Error
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "APD Postoffice job insert failed: ", DateTime.Now + " - Job failed to insert into the APD PostOffice...", string.Format("RC: {0}", sRC), "");
                                    iEventSeq = (iEventSeq + 1);
                                }

                                // -------------------------------------- //
                                // Add note to APD that details the body  //
                                // of the email                           //
                                // -------------------------------------- //
                                #region Creating the PostOffice NOTE Job
                                sRC = CreatePostOfficeNoteJob(sLynxID, sInsuranceCompanyID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sNewFileName, sFileExtension, sFileLength, sFileData, sSimpleTextEmail);

                                if (sRC.ToUpper() == "SUCCESS")
                                {
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(NOTEJOB)", iEventSeq.ToString(), "Process NOTE Job in PostOffice: ", DateTime.Now + " - NOTE Job was successfully processed into PostOffice.", string.Format("Ret: {0}", sRC.ToUpper()), "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                                else
                                {
                                    //Throw Error
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "APD Postoffice NOTE job insert failed: ", DateTime.Now + " - Job failed to insert NOTE into the APD PostOffice...", string.Format("RC: {0}", sRC), "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                                #endregion


                                // ----------------------------------------
                                //  Debugging
                                // ----------------------------------------
                                //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDPOSTOFFICE)", iEventSeq.ToString(), "APD PostOffice response: ", DateTime.Now + " - APD PostOffice response in EventXML...", string.Format("Params: RC: {0}", sRC), "");
                                //iEventSeq = (iEventSeq + 1);

                                //??? TEMP LEAVE LOOP - For Debug only //
                                //break;
                            }
                            else
                            {
                                // Invalid Host Authorization, move to review folder.
                                Boolean bRC = MoveEmail(email, oNOLReviewFolderID);

                                if (bRC)
                                {
                                    // -- Successful -- //
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILHOSTBLACKLISTED)", iEventSeq.ToString(), "UnAuthorized BlackListed Host email moved to the review folder.  ", DateTime.Now + " - UnAuthorized BlackListed email host.  Move to review folder.", "Email Host: " + sSender + "@" + sDomain, "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                                else
                                {
                                    // -- Failed -- //
                                    // ----------------------------------------
                                    //  Debugging
                                    // ----------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR: UnAuthorized BlackListed Host email moved to the review folder.  ", DateTime.Now + " - ERROR: UnAuthorized BlackListed email host.  Move to review folder.", "Email Host: " + sSender + "@" + sDomain, "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                            }
                            #endregion
                        }
                        #endregion

                        CleanUp:
                        // ----------------------------------------
                        //  File Cleanup
                        // ----------------------------------------
                        #region Cleaning up the Junk
                        foreach (string sAttachFileName in aFileNameList)
                        {
                            // ----------------------------------------
                            //  Debugging
                            // ----------------------------------------
                            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CLEANUP)", iEventSeq.ToString(), "Cleanup ", DateTime.Now + " - Cleanup process started...", "Num of files to cleanup: " + aFileNameList.Count, "");
                            iEventSeq = (iEventSeq + 1);

                            System.IO.FileInfo fiBody1 = new System.IO.FileInfo(sAttachmentStageArea + sAttachFileName);

                            //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(DEBUG 1)", iEventSeq.ToString(), "DEBUG - File: " + sAttachmentStageArea + sAttachFileName, "", "", "");
                            //iEventSeq = (iEventSeq + 1);

                            // ------------------------------------------
                            //  Logging - Body file processed.  Time to 
                            //  clean up the mess
                            // ------------------------------------------
                            try
                            {
                                fiBody1.MoveTo(sArchivePath + "ORIG_" + sAttachFileName);

                                if (File.Exists(sAttachmentStageArea + sAttachFileName))
                                {
                                    // ------------------------------------------
                                    //  ERROR - Move PDF to Archive Failed
                                    // ------------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + "ORIG_" + sAttachFileName + " was NOT moved Successfully to the archive...", "ERROR - File NOT successfully moved from StageArea to Archive: " + DateTime.Now + " ERROR Archive FileName: " + "ORIG_" + sAttachFileName, "", "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                                else
                                {
                                    // ------------------------------------------
                                    //  Move PDF to Archive Complete
                                    // ------------------------------------------
                                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(FILEMOVE)", iEventSeq.ToString(), "File " + "ORIG_" + sAttachFileName + " was moved Successfully to the archive...", "File successfully moved from StageArea to Archive: " + DateTime.Now + " Archive FileName: " + "ORIG_" + sAttachFileName, "", "");
                                    iEventSeq = (iEventSeq + 1);
                                }
                            }
                            catch (FileNotFoundException ex)
                            {
                                // ------------------------------------------
                                //  WARNING - File Not Found
                                // ------------------------------------------
                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(FILEIO_WARNING)", iEventSeq.ToString(), "Warning - File " + sAttachFileName + " was NOT found during the CleanUp process...", "ERROR - File was NOT successfully CleanedUp: " + DateTime.Now + "Warning: CleanUp FileName: " + sAttachFileName, "", "");
                                iEventSeq = (iEventSeq + 1);
                            }
                            catch (Exception oExcept)
                            {
                                // ------------------------------------------
                                //  ERROR - CleanUp/Move PDF to Archive Failed
                                // ------------------------------------------
                                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + sAttachFileName + " caused a exception when attempting to CleanUp...", "ERROR - File NOT successfully CleanedUp: " + DateTime.Now + " ERROR Archive FileName: " + sAttachFileName, "", "");
                                iEventSeq = (iEventSeq + 1);
                            }
                        }
                        #endregion
                    }
                }
                else
                { 
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(PROCESSING)", iEventSeq.ToString(), "Processing", DateTime.Now + " - Process still running: Last process is still running, waiting for the cycle...", "", "");
                    iEventSeq = (iEventSeq + 1);
                }

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILPROCESSING)", iEventSeq.ToString(), "Email Processing - Ended", DateTime.Now + " - Email Processing Ended: Office365 processing complete...", "", "");
                iEventSeq = (iEventSeq + 1);

                isRunning = false;

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(TIMER)", iEventSeq.ToString(), "TIMER TICK", DateTime.Now + " - Timer_Tick - Processing Ended: Processing the NOLIncoming365Svc events...", "IsRunning: " + isRunning, "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                // SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            }
            finally
            {
            }
        }

        public string LocalLogEvent(string sProcessingServer, string sEventTransactionID, string sEventType, string sEventSeq, string sEventStatus, string sEventDescription, string sEventDetailedDescription, string sEventXml)
        {
            try
            {
                string sReturn = string.Empty;

                //-------------------------------//
                // Database access
                //-------------------------------//  
                string sStoredProcedure = "uspNOLEventInsLogEntry";
                string sParams = "@vProcessingServer = '" + sProcessingServer + "', @vEventTransactionID = '" + sEventTransactionID + "', @vEventType = '" + sEventType + "', @iEventSeq = " + sEventSeq + ", @vEventStatus = '" + sEventStatus + "', @vEventDescription = '" + sEventDescription + "', @vEventDetailedDescription = '" + sEventDetailedDescription + "', @vEventXML = '" + sEventXml + "', @vRecID = ''";

                // -------------------------------
                //  Call WS and Process Request
                // -------------------------------
                sReturn = wsPGWAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams);

                // -------------------------------------------
                //  Check response for errors
                // -------------------------------------------
                if (sReturn == "0")
                {
                    return sReturn; 
                }
                else {
                    throw new System.Exception(string.Format("Error: {0}", "WebSerices Failed: NOLIncoming event logging failed to insert a row (LogEvent)...  " + sReturn));
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                sError = string.Format("Error: {0}", ("WebServices Failed: NOLIncoming event logging failed to insert a row (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                wsPGWAPDFoundation.LogEvent("NOLWebService", "ERROR", "Error occurred...", sError, "");

                // SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
                return sBody;
            }
            finally
            {
            }
        }

        private bool CleanUp()
        {
            try
            {
                return true;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return false;
            }
        }

        private bool InitParams()
        {
            try
            {
                XmlDocument oReturnXML = new XmlDocument();
                XmlNode oXMLNode = null;

                XmlTextReader reader = new XmlTextReader(appSettingsReader.GetValue("ConfigFilePath", typeof(string)).ToString());
                reader.WhitespaceHandling = WhitespaceHandling.None;
                oReturnXML.Load(reader);

                // Config.xml Vars
                sEnvironmentCode = oReturnXML.SelectSingleNode("/Root/Application/Environment/@EnvironmentCode").Value;

                //------------------------------------------//
                // Environment Variable from NOLConfig.xml  //
                //------------------------------------------//
                // FailNotify
                oXMLNode = oReturnXML.SelectSingleNode("/Root/Application/Environment[@name='" + sEnvironmentCode + "']/FailNotify");
                sFailNotifyEnabled = GetXMLNodeAttrbuteValue(oXMLNode, "enabled");
                sFailNotifyFromEmail = GetXMLNodeAttrbuteValue(oXMLNode, "from");
                sFailNotifyRecipients = GetXMLNodeValue(oXMLNode, "Recipients");
                sFailNotifySubject = GetXMLNodeValue(oXMLNode, "Subject");

                // Database 
                oXMLNode = oReturnXML.SelectSingleNode("/Root/Application/Environment[@name='" + sEnvironmentCode + "']/Database");
                sDatabaseName = GetXMLNodeAttrbuteValue(oXMLNode, "DBName");

                // FileIO
                oXMLNode = oReturnXML.SelectSingleNode("/Root/Application/Environment[@name='" + sEnvironmentCode + "']/FileIO/FileExtentions");
                sValidExt = GetXMLNodeAttrbuteValue(oXMLNode, "ValidExt");

                // HostAddress
                oXMLNode = oReturnXML.SelectSingleNode("/Root/Application/Environment[@name='" + sEnvironmentCode + "']/Host/HostAddress");
                sBlackListeddHosts = GetXMLNodeAttrbuteValue(oXMLNode, "BlackListedHosts").ToLower();

                oXMLNode = oReturnXML.SelectSingleNode("/Root/Application/Environment[@name='" + sEnvironmentCode + "']/FileIO/FileAttachmentStagePath");
                sAttachmentStageArea = GetXMLNodeAttrbuteValue(oXMLNode, "Path");

                oXMLNode = oReturnXML.SelectSingleNode("Application/Globals/SMTPServer");

                return true;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return false;
            }
        }

        protected string GetXMLNodeAttrbuteValue(XmlNode XMLnode, string strAttributes)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.Attributes[strAttributes] == null))
                    {
                        strRetVal = XMLnode.Attributes[strAttributes].InnerText;
                    }
                }
                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return sError;
            }
        }

        protected string GetXMLNodeValue(XmlNode XMLnode, string strNode)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.SelectSingleNode(strNode) == null))
                    {
                        strRetVal = XMLnode.SelectSingleNode(strNode).InnerText;
                    }
                }
                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return sError;
            }
        }

        //---------------------------//
        // Convert Document to PDF   //
        //---------------------------//

        public void ConvertToPDF(string sOriginalFile, string sDocumentType)
        {
            switch (sDocumentType.ToLower())
            {
                case ".txt":
                    ConvertTXTToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".doc":
                    ConvertWordToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".docx":
                    //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(DEBUGGING x3)", iEventSeq.ToString(), "Converting: " + sOriginalFile + " " + sDocumentType, "", "", "");
                    //iEventSeq = (iEventSeq + 1);

                    ConvertWordToPDF(sOriginalFile, sDocumentType);

                    //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(DEBUGGING x4)", iEventSeq.ToString(), "Converting: " + sOriginalFile + " " + sDocumentType, "", "", "");
                    //iEventSeq = (iEventSeq + 1);

                    break;
                case ".xls":
                    ConvertExcelToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".xlsx":
                    ConvertExcelToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".rtf":
                    ConvertRTFToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".tif":
                    ConvertTIFToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".tiff":
                    ConvertTIFToPDF(sOriginalFile, sDocumentType);
                    break;
                case ".png": case ".jpg": case ".jpeg": case ".gif":
                    ConvertIMGToPDF(sOriginalFile, sDocumentType);
                    break;
            }
        }

        //---------------------------//
        // Convert PNG to PDF        //
        //---------------------------//
        public void ConvertIMGToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-IMG-2-PDF)", iEventSeq.ToString(), "ConvertIMGToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();
      
                //Add a section to the PDF document
                PdfSection section = pdfDocument.Sections.Add();

                //Declare the PDF page
                PdfPage page;
                page = section.Pages.Add();

                //Declare PDF page graphics
                PdfGraphics graphics;

                //Load PNG Photo
                PdfBitmap PNGImage = new PdfBitmap(sOriginalFile);

                section.PageSettings.Margins.All = 0;
                graphics = page.Graphics;
                PNGImage.ActiveFrame = 0;
                graphics.DrawImage(PNGImage, 0, 0);

                //Save the PDF file to file system
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));
                //Close the document
                pdfDocument.Close(true);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-IMG-2-PDF)", iEventSeq.ToString(), "ConvertIMGToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: ConvertIMGToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert TXT to PDF        //
        //---------------------------//
        public void ConvertTXTToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-TXT-2-PDF)", iEventSeq.ToString(), "ConvertTXTToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();

                //Add a section to the PDF document
                PdfSection section = pdfDocument.Sections.Add();

                //Declare the PDF page
                PdfPage page;
                page = section.Pages.Add();

                //Declare PDF page graphics
                //PdfGraphics graphics;
                PdfGraphics graphics = page.Graphics;

                //set the standard font
                PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 12);
                //Read the text from the text file
                StreamReader reader = new StreamReader(sOriginalFile, Encoding.ASCII);
                string text = reader.ReadToEnd();
                reader.Close();
                //Set the formats for the text
                PdfStringFormat format = new PdfStringFormat();
                format.Alignment = PdfTextAlignment.Justify;
                format.LineAlignment = PdfVerticalAlignment.Top;
                format.ParagraphIndent = 15f;

                //Draw the text
                graphics.DrawString(text, font, PdfBrushes.Black, new RectangleF(new PointF(0, 0), page.GetClientSize()), format);

                //Save the PDF file to file system
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));
                //Close the document
                pdfDocument.Close(true);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-TXT-2-PDF)", iEventSeq.ToString(), "ConvertTXTToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured: ConvertTXTToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert WordDoc to PDF    //
        //---------------------------//
        public void ConvertWordToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            { 
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-WORD-2-PDF)", iEventSeq.ToString(), "ConvertWORDToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                // Convert WordToPDF
                WordDocument wordDocument = new WordDocument(sOriginalFile, FormatType.Docx);
                DocToPDFConverter converter = new DocToPDFConverter();
                PdfDocument pdfDocument = converter.ConvertToPDF(wordDocument);
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));

                //Releases all resources used by the object.
                converter.Dispose();

                //Closes the instance of document objects
                pdfDocument.Close(true);
                wordDocument.Close();

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-WORD-2-PDF)", iEventSeq.ToString(), "ConvertWORDToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting Word Doc to PDF occured: ConvertWordToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert EXCEL to PDF      //
        //---------------------------//
        public void ConvertExcelToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-EXCEL-2-PDF)", iEventSeq.ToString(), "ConvertEXCELToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    IWorkbook workbook = application.Workbooks.Open(sOriginalFile, ExcelOpenType.Automatic);

                    //Open the Excel document to Convert
                    ExcelToPdfConverter converter = new ExcelToPdfConverter(workbook);

                    //Initialize PDF document
                    PdfDocument pdfDocument = new PdfDocument();

                    //Convert Excel document into PDF document
                    pdfDocument = converter.Convert();

                    //Save the PDF file
                    pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));
                }

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-EXCEL-2-PDF)", iEventSeq.ToString(), "ConvertEXCELToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting Excel to PDF occured: ConvertExcelToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert HTML to PDF       //
        //---------------------------//
        public void ConvertHTMLToPDF(string sOriginalHTMLString, string sExistingFileName)
        {
            // ----------------------------------------
            //  Logging - Processing Email Body
            // ----------------------------------------
            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-HTML-2-PDF)", iEventSeq.ToString(), "ConvertHTMLToPDF: " + sOriginalHTMLString + " to PDF Starting.", "ExistingFileName: " + sExistingFileName, "", "");
            iEventSeq = (iEventSeq + 1);

            string sNewFileName = string.Empty;
 
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);
            //WebKitConverterSettings webKitSettings = new WebKitConverterSettings();

            //webKitSettings.TempPath = sAPPPath + "QtBinaries";

            string baseUrl = string.Empty;
            //webKitSettings.WebKitPath = sAPPPath + "QtBinaries";
            //htmlConverter.ConverterSettings = webKitSettings;

            try
            {
                sNewFileName = sExistingFileName.Replace(System.IO.Path.GetExtension(sExistingFileName), ".pdf");

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(PRE_HTML-2-PDF)", iEventSeq.ToString(), "Ready to convert HTML Email Body to PDF...", "Converting HTML Email Body Details file: " + DateTime.Now + " New Body FileName: " + sAPPPath + sNewFileName, "", "HTML Data: " + sOriginalHTMLString);
                iEventSeq = (iEventSeq + 1);

                PdfDocument document = htmlConverter.Convert(sOriginalHTMLString, baseUrl);
               // PdfDocument document = htmlConverter.Convert("<html><body>hello</body></html>", baseUrl);

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ConvertHTMLToPDF)", iEventSeq.ToString(), "Done Converting HTML Email Body to PDF...", "Done Converting HTML Email Body Details file: " + DateTime.Now + " New Body FileName: " + sAPPPath + sNewFileName, "", "");
                iEventSeq = (iEventSeq + 1);

                //-- Save Body of Email as PDF --//
                document.Save(sAPPPath + "StagingArea\\" + sNewFileName);

                // ----------------------------------------
                //  Logging - Processing Email Body
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ConvertHTMLToPDF)", iEventSeq.ToString(), "Saving Converted HTML Email Body to PDF...", "Saving Converted HTML Email Body Details file: " + DateTime.Now + " Saving Body FileName: " + sAPPPath + sNewFileName, "", "");
                iEventSeq = (iEventSeq + 1);

                document.Close(true);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-HTML-2-PDF)", iEventSeq.ToString(), "ConvertHTMLToPDF to PDF Ending.", "", "NewPDF: " + sAPPPath + "StagingArea\\" + sNewFileName, "");
                iEventSeq = (iEventSeq + 1);

            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error Occured: PDF convert failed...", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert RTF to PDF        //
        //---------------------------//
        public void ConvertRTFToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-RTF-2-PDF)", iEventSeq.ToString(), "ConvertRTFToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                WordDocument rtfDocument = new WordDocument(sOriginalFile);

                //create an instance of DocToPDFConverter - responsible for Word to PDF conversion
                DocToPDFConverter converter = new DocToPDFConverter();

                //Set the image quality
                converter.Settings.ImageQuality = 100;

                //Set the image resolution
                converter.Settings.ImageResolution = 640;

                //Set true to optimize the memory usage for identical images
                converter.Settings.OptimizeIdenticalImages = true;

                //Convert Word document into PDF document
                PdfDocument pdfDocument = converter.ConvertToPDF(rtfDocument);

                //Save the PDF file to file system
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));

                //close the instance of document objects
                pdfDocument.Close(true);
                rtfDocument.Close();

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-RTF-2-PDF)", iEventSeq.ToString(), "ConvertRTFToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting RTF to PDF occured: ConvertRTFToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert TIF to PDF        //
        //---------------------------//
        public void ConvertTIFToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-TIF-2-PDF)", iEventSeq.ToString(), "ConvertTIFToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();

                //Add a section to the PDF document
                PdfSection section = pdfDocument.Sections.Add();

                //Declare the PDF page
                PdfPage page;

                //Declare PDF page graphics
                PdfGraphics graphics;

                //Load multi frame TIFF image
                PdfBitmap tiffImage = new PdfBitmap(sOriginalFile);

                //Get the frame count
                int frameCount = tiffImage.FrameCount;

                //Access each frame draw into the page
                for (int i = 0; i < frameCount; i++)
                {
                    page = section.Pages.Add();
                    section.PageSettings.Margins.All = 0;
                    graphics = page.Graphics;
                    tiffImage.ActiveFrame = i;
                    graphics.DrawImage(tiffImage, 0, 0, page.GetClientSize().Width, page.GetClientSize().Height);
                }

                //Save the PDF file to file system
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));
                //Close the document
                pdfDocument.Close(true);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-TIF-2-PDF)", iEventSeq.ToString(), "ConvertTIFToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting TIF to PDF occured: ConvertTIFToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        //---------------------------//
        // Convert XPS to PDF        //
        //---------------------------//
        public void ConvertXPSToPDF(string sOriginalFile, string sDocumentType)
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-XPS-2-PDF)", iEventSeq.ToString(), "ConvertXPSToPDF: " + sOriginalFile + " to PDF Starting.", "DocumentType: " + sDocumentType, "", "");
                iEventSeq = (iEventSeq + 1);

                //Create a PDF document
                PdfDocument pdfDocument = new PdfDocument();
                XPSToPdfConverter XPSDocument = new XPSToPdfConverter();

                //Convert the XPS to PDF.
                PdfDocument document = XPSDocument.Convert(sOriginalFile);

                //Save the PDF file to file system
                pdfDocument.Save(sOriginalFile.Replace(sDocumentType, ".pdf"));
                document.Close(true);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CONVERT-XPS-2-PDF)", iEventSeq.ToString(), "ConvertXPSToPDF to PDF Ending.", "", "NewPDF: " + sOriginalFile.Replace(sDocumentType, ".pdf"), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error converting XPS to PDF occured: ConvertXPSToPDF Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }
}

        //---------------------------//
        // Convert PDF to Image      //
        //---------------------------//
        public void ConvertPDFToImage(string sOriginalFile, string sDocumentType)
        {
            ////Create a PDF document
            //PdfDocumentView documentViewer = new PdfDocumentView();

            ////Load the PDF document
            //documentViewer.Load(sOriginalFile);

            ////Export PDF page to image
            //Bitmap image = documentViewer.ExportAsImage(0);

            ////Save the image.
            //image.Save("Output.png", ImageFormat.Png);
            ////Save the PDF file to file system
            //image.Save(sOriginalFile.Replace(sDocumentType, ".png"));
            //documentViewer.Dispose();
        }

        //------------------------------
        // Base64 Encode the file data
        //------------------------------
        private string GetBinData(String fileName)
        {
            string theBinLength = string.Empty;
            byte[] buff = null;
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    long numBytes = new FileInfo(fileName).Length;
                    theBinLength = numBytes.ToString();

                    buff = br.ReadBytes((int)numBytes);

                    br.Close();
                    br.Dispose();
                    fs.Close();
                    fs.Dispose();
                }
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured while Base64 Encoding: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);
            }

            return Convert.ToBase64String(buff).ToString();
        }

        //---------------------------//
        // Resize Images             //
        //---------------------------//

        //=================================//
        // Upload Email Job to PostOffice  //
        //=================================//
        public string CreatePostOfficeJob(string sLynxID, string sClientID, string sClientName, string sClaimNo, string sApp, string sUserID, string sPertainsTo, string sSuppSeqNumber, string sDocumentType, string sDocumentSource, string sNewFileName, string sFileExtension, string sFileLength, string sFileData)
        {
            string sRC = string.Empty;
            string sJobXML = string.Empty;
            try
            { 
                //----------------------------------
                // Create the APD PostOffice JobXML
                //----------------------------------
                sJobXML = "<JobDetails clientid = \"" + sClientID + "\"";
                sJobXML += " clientname = \"" + sClientName + "\"";
                sJobXML += " lynxid = \"" + sLynxID + "\"";
                sJobXML += " claimnumber = \"" + sClaimNo + "\"";
                sJobXML += " submittingserver = \"" + sProcessingServer + "\">";
                sJobXML += "<TargetApp>";
                sJobXML += "<App>" + sApp + "</App>";
                sJobXML += "</TargetApp>";
                sJobXML += "<DocumentUpload>";
                sJobXML += "<LynxID>" + sLynxID + "</LynxID>";
                sJobXML += "<UserID>" + sUserID + "</UserID>";
                sJobXML += "<PertainsTo>" + sPertainsTo + "</PertainsTo>";
                sJobXML += "<SuppSeqNumber>" + sSuppSeqNumber + "</SuppSeqNumber>";
                sJobXML += "<DocumentType>" + sDocumentType + "</DocumentType>";
                sJobXML += "<DocumentSource>" + sDocumentSource + "</DocumentSource>";
                sJobXML += "<FileName>" + sNewFileName + "</FileName>";
                sJobXML += "<FileExtension>" + sFileExtension + "</FileExtension>";
                sJobXML += "<FileLength>" + sFileLength + "</FileLength>";
                sJobXML += "<FileData>" + sFileData + "</FileData>";
                sJobXML += "</DocumentUpload>";
                sJobXML += "</JobDetails>";

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDPOSTOFFICE)", iEventSeq.ToString(), "APD PostOffice Job Processing: ", DateTime.Now + " - APD PostOffice Job Processing: Calling APDPostOfficeWS...", string.Format("Params: {0}, {1}, {2}, {3}, {4}, {5}, {6} " + "UPL", sClientID, sLynxID, sClaimNo, "5", "0", string.Empty, sJobXML), "");
                iEventSeq = (iEventSeq + 1);

                //--------------------------------
                // Create the APD PostOffice Job
                //--------------------------------
                sRC = wsPGWClaimPointFoundation.CreatePostOfficeJob("UPL", sClientID, sLynxID, sClaimNo, "5", "0", string.Empty, sJobXML);

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(INSAPDPOSTOFFICE)", iEventSeq.ToString(), "APD PostOffice Job Insert: ", DateTime.Now + " - APD PostOffice UPL Insert Job...", string.Format("APDPostOffice UPL Job Insert RC: {0}", sRC), "");
                iEventSeq = (iEventSeq + 1);

            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured while Base64 Encoding: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                sRC = sError;
            }
            return sRC;
        }

        //=================================//
        // Upload Email Job to PostOffice  //
        //=================================//
        public string CreatePostOfficeNoteJob(string sLynxID, string sClientID, string sClientName, string sClaimNo, string sApp, string sUserID, string sPertainsTo, string sSuppSeqNumber, string sDocumentType, string sDocumentSource, string sNewFileName, string sFileExtension, string sFileLength, string sFileData, string sNewEmail)
        {
            string sRC = string.Empty;
            string sJobXML = string.Empty;

            // ----------------------------------------
            //  Debugging
            // ----------------------------------------
            //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDPOSTOFFICE)", iEventSeq.ToString(), "APD PostOffice NOTE Job Processing: ", DateTime.Now + " - APD PostOffice NOTE Job Processing: Calling CreatePostOfficeJob...", string.Format("Params: {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12} " + "NOTE", sClientID, sLynxID, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sNewFileName, sFileExtension, sFileData), "");
            //iEventSeq = (iEventSeq + 1);

            try
            {
                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(NOTE)", iEventSeq.ToString(), "APD PostOffice NOTE Job Processing Started: ", DateTime.Now + " - APD PostOffice NOTE Job Processing Started.", string.Format("LYNXID: {0} ", sLynxID), "");
                iEventSeq = (iEventSeq + 1);

                //---------------------------------------
                // Create the APD PostOffice Note JobXML
                //---------------------------------------
                sJobXML = "<JobDetails clientid = \"" + sClientID + "\"";
                sJobXML += " clientname = \"" + sClientName + "\"";
                sJobXML += " lynxid = \"" + sLynxID + "\"";
                sJobXML += " claimnumber = \"" + sClaimNo + "\"";
                sJobXML += " submittingserver = \"" + sProcessingServer + "\">";

                sJobXML += "<TargetApp>";
                sJobXML += "<App>" + sApp + "</App>";
                //sJobXML += "<App>APD</App>";
                sJobXML += "</TargetApp>";
                 
                sJobXML += "<NoteUpload ignoreenvoverride = \"yes\" priority = \"5\" stepid = \"1\">";
                sJobXML += "<LynxID>" + sLynxID + "</LynxID>";
                sJobXML += "<UserID>" + sUserID + "</UserID>";
                sJobXML += "<PertainsTo>" + sPertainsTo + "</PertainsTo>";
                sJobXML += "<SuppSeqNumber>" + sSuppSeqNumber + "</SuppSeqNumber>";
                sJobXML += "<DocumentType>Note</DocumentType>";
                sJobXML += "<DocumentSource>Email</DocumentSource>";
                sJobXML += "<NoteType>Summary</NoteType>";
                sJobXML += "<Status>500</Status>";
                sJobXML += "<Note><![CDATA[" + sNewEmail + "]]></Note>";

                //string sDebug = "<Note>><![CDATA[" + sNewEmail + "]]></Note>";
                //sJobXML += "<Note>HI TOM</Note>";
                sJobXML += "<Private>0</Private>";
                sJobXML += "</NoteUpload>";

                sJobXML += "</JobDetails>";

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(APDPOSTOFFICE)", iEventSeq.ToString(), "APD PostOffice NOTE Job Processing: ", DateTime.Now + " - APD PostOffice NOTE Job Processing: JOBXML: ", string.Format("Params: {0} ", sJobXML), "");
                iEventSeq = (iEventSeq + 1);

                //--------------------------------
                // Create the APD PostOffice Job
                //--------------------------------
                sRC = wsPGWClaimPointFoundation.CreatePostOfficeJob("NOTE", sClientID, sLynxID, sClaimNo, "5", "0", string.Empty, sJobXML);

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(CREATEPOSTOFFICENOTE)", iEventSeq.ToString(), "Creating the APD PostOffice NOTE Job: ", DateTime.Now + " - Calling the APD PostOffice PGWClaimPointFoundation.CreatePostOfficeJob and creating the NOTE Job: ", string.Format("Response: {0} ", sRC), "");
                iEventSeq = (iEventSeq + 1);

                //??????????????????????????? ADD CODE TO TEST NOT CREATE ?????????????????????

                //sRC = "OK";
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured while Base64 Encoding: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                sRC = sError;
            }
            return sRC;
        }

        //---------------------------//
        // Resize Images             //
        //---------------------------//
        public void ResizeImage(string fileName, int width, int height)
        {
            using (Image image = Image.FromFile(fileName))
            {
                new Bitmap(image, width, height).Save(fileName);
            }
        }

        //------------------------------
        // Move NOL Email
        //------------------------------
        private Boolean MoveEmailToOnBase(EmailMessage email)
        {
            Boolean bRC = false;
            string sMessageBody = "NOLIncoming has forwarded this email to OnBase for processing.";
            string sOnBaseEmailAddress = string.Empty;

            try
            {
                sOnBaseEmailAddress = appSettingsReader.GetValue("OnBaseEmailAddress", typeof(string)).ToString();

                //-------------------------------------
                // Logging - Move NOL Email
                //-------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ONBASE)", iEventSeq.ToString(), "Foward Email - Process Started...", DateTime.Now + " - Forward Email: Forward Email to OnBase: ", "Params: " + email.Id.ToString(), "OnBaseEmailAddress: " + sOnBaseEmailAddress);
                iEventSeq = (iEventSeq + 1);

                //---------------------------------//
                // Move email to Review Folder     //
                //---------------------------------//
                PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject, EmailMessageSchema.ParentFolderId);
                EmailMessage beforeMessage = EmailMessage.Bind(_service, email.Id, propSet);

                // Forward the Email
                EmailAddress[] addresses = new EmailAddress[1];
                addresses[0] = new EmailAddress(sOnBaseEmailAddress);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ONBASE-FORWARD)", iEventSeq.ToString(), "Foward Email - Processing...", DateTime.Now + " - Forwarding to OnBase: ", "MessageBody: " + sMessageBody + ", OnBaseEmailAddress: " + sOnBaseEmailAddress, "");
                iEventSeq = (iEventSeq + 1);

                email.Forward(sMessageBody, addresses);

                bRC = true;

                //-------------------------------------
                // Logging - Move NOL Email
                //-------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ONBASE)", iEventSeq.ToString(), "Forward Email - Process Ended...", DateTime.Now + " - Forward Email Complete", "", "");
                iEventSeq = (iEventSeq + 1);

                return bRC;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured while forwarding email to OnBase: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return bRC = false;
            }
        }

        //------------------------------
        // Move NOL Email
        //------------------------------
        private Boolean MoveEmail(EmailMessage email, FolderId oNOLFolderID)
        {
            Boolean bRC = false;

            try
            {
                //-------------------------------------
                // Logging - Move NOL Email
                //-------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILMOVE)", iEventSeq.ToString(), "Move Email - Process Started...", DateTime.Now + " - Move Email: Moving Email to: ", "Params: " + email.Id.ToString() + " - " + oNOLFolderID.FolderName.ToString(), "");
                iEventSeq = (iEventSeq + 1);

                //---------------------------------//
                // Move email to Review Folder     //
                //---------------------------------//
                PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject, EmailMessageSchema.ParentFolderId);
                EmailMessage beforeMessage = EmailMessage.Bind(_service, email.Id, propSet);

                // Moving the Email
                Item item = beforeMessage.Move(oNOLFolderID);

                bRC = true;

                //-------------------------------------
                // Logging - Move NOL Email
                //-------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILMOVE)", iEventSeq.ToString(), "Move Email - Process Ended...", DateTime.Now + " - Move Email Complete", "", "");
                iEventSeq = (iEventSeq + 1);

                return bRC;
            }
            catch (Exception oExcept)
            {
                // ---------------------------------
                //  Error handler and notifications
                // ---------------------------------
                string sError = string.Empty;
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                sError = string.Format("Error: {0}", (" (" + (FunctionName.GetMethod().Name + (")...  " + oExcept.ToString()))));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "NOLIncoming Service(ERROR)", iEventSeq.ToString(), "ERROR", DateTime.Now + " - Error occured while Base64 Encoding: Exception thrown", sError, "");
                iEventSeq = (iEventSeq + 1);

                return bRC = false;
            }
        }

        private XmlNode GetInscCompIDByLynxID(int iLynxID, int iVehicleNum)
        {
            string sRC = string.Empty;
            XmlNode XMLReturn = null;

            string sParams = string.Empty;

            try
            {
                sParams = "@LynxID = " + iLynxID;
                sParams += ", @VehicleNumber = " + iVehicleNum;

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetInscCompIDByLynxID(START)", iEventSeq.ToString(), "PROCESSING", DateTime.Now + " - GetInscCompIDByLynxID Process Start: Getting the InsuranceCompID by LynxID and VehicleNumber.", "", "uspGetInscCompByLynxIDWSXML " + sParams);
                iEventSeq = (iEventSeq + 1);

                XMLReturn = wsPGWAPDFoundation.ExecuteSpAsXML("uspGetInscCompByLynxIDWSXML", sParams);

                if (XMLReturn.OuterXml.Contains("InsuranceCompanyID"))
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM)", iEventSeq.ToString(), "APD Claim/LynxID Exists: ", DateTime.Now + " - LynxID exists in APD: Good Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    iEventSeq = (iEventSeq + 1);
                }
                else
                {
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM)", iEventSeq.ToString(), "APD Claim/LynxID DOES NOT Exists: ", DateTime.Now + " - LynxID DOES NOT exists in APD: Bad Claim...", string.Format("Params: {0}", XMLReturn.OuterXml), "");
                    iEventSeq = (iEventSeq + 1);

                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetInscCompIDByLynxID(CHECKCLAIM): ERROR APD Claim/LynxID DOES NOT Exists...  " + XMLReturn.OuterXml));
                }

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetInscCompIDByLynxID(END)", iEventSeq.ToString(), "ENDING", DateTime.Now + " - GetInscCompIDByLynxID Process End: Ending getting the InsuranceCompID by LynxID and VehicleNumber.", "", "");
                iEventSeq = (iEventSeq + 1);

                return XMLReturn;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-GetInscCompIDByLynxID: Getting the InsuranceCompID process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<ERROR><Msg>" + strError + "</Msg></ERROR>");

                return doc;
            }
        }

        private XmlNode GetClaimDetailsByLynxID(int iLynxID, int iInsuranceCompanyID)
        {
            string sRC = string.Empty;
            XmlNode XMLReturn = null;
            string sParams = string.Empty;

            try
            {
                sParams = "@LynxID = " + iLynxID;
                sParams += ", @InsuranceCompanyID = " + iInsuranceCompanyID;

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetClaimDetailsByLynxID(START)", iEventSeq.ToString(), "PROCESSING", DateTime.Now + " - GetClaimDetailsByLynxID Process Start: Starting the insert of a Note into APD.", "", "uspDiaryInsDetail " + sParams);
                iEventSeq = (iEventSeq + 1);

                XMLReturn = wsPGWAPDFoundation.ExecuteSpAsXML("uspClaimCondGetDetailWSXML", sParams);

                if (XMLReturn.OuterXml.Contains("ERROR"))
                {
                    throw new System.Exception(string.Format("Error: {0}", "PostOffice-GetClaimDetailsByLynxID(ERROR): ERROR APD Get Details Failed...  " + XMLReturn.OuterXml));
                }

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-GetClaimDetailsByLynxID(END)", iEventSeq.ToString(), "ENDING", DateTime.Now + " - GetClaimDetailsByLynxID Process End: Ending the insert of a Note into APD.", "", "");
                iEventSeq = (iEventSeq + 1);

                return XMLReturn;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-GetClaimDetailsByLynxID: GetClaimDetailsByLynxID process failed (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<ERROR><Msg>" + strError + "</Msg></ERROR>");

                return doc;
            }
        }

        private string GetXMLNodeAttributeValue(XmlNode XMLnode, string strAttributes)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.Attributes[strAttributes] == null))
                    {
                        strRetVal = XMLnode.Attributes[strAttributes].InnerText;
                    }
                }

                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("GetXMLNodeAttrbuteValue Failed: Failed to parse XMLNode (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);

                return strError;
            }
        }

        private string HTMLToText(string HTMLCode)
        {
            // Remove new lines since they are not visible in HTML
            HTMLCode = HTMLCode.Replace("\n", " ");
            // Remove tab spaces
            HTMLCode = HTMLCode.Replace("\t", " ");
            // Remove multiple white spaces from HTML
            HTMLCode = Regex.Replace(HTMLCode, "\\s+", " ");
            // Remove HEAD tag
            HTMLCode = Regex.Replace(HTMLCode, "<head.*?</head>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            // Remove any JavaScript
            HTMLCode = Regex.Replace(HTMLCode, "<script.*?</script>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Remove any Image Sources
            HTMLCode = Regex.Replace(HTMLCode, "<img.*?</img>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Convert &#43 to a +
            HTMLCode = Regex.Replace(HTMLCode, "&#43", "+", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Replace special characters like &, <, >, " etc.
            StringBuilder sbHTML = new StringBuilder(HTMLCode);
            // Note: There are many more special characters, these are just
            // most common. You can add new characters in this arrays if needed
            string[] OldWords = { "&nbsp;", "&amp;", "&quot;", "&lt;", "&gt;", "&reg;", "&copy;", "&bull;", "&trade;" };
            string[] NewWords = { " ", "&", "\"", "<", ">", "Â®", "Â©", "â€¢", "â„¢" };
            for (int i = 0; i < OldWords.Length; i++)
            {
                sbHTML.Replace(OldWords[i], NewWords[i]);
            }

            // Check if there are line breaks (<br>) or paragraph (<p>)
            sbHTML.Replace("<br>", "\n<br>");
            sbHTML.Replace("<br ", "\n<br ");
            sbHTML.Replace("<p ", "\n<p ");

            // Finally, remove all HTML tags and return plain text
            return Regex.Replace(sbHTML.ToString(), "<[^>]*>", "");
        }

        private string HTMLRemoveImages(string HTMLCode)
        {
            // Remove any Image Sources
            HTMLCode = Regex.Replace(HTMLCode, "<img.*?</img>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return HTMLCode;
        }

        private void ZipCleanStageArea()
        {
            try
            {
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ZipCleanStageArea(START)", iEventSeq.ToString(), "PROCESSING", DateTime.Now + " - ZipCleanStageArea Process Start: Deleting files from the ZIP and PDF Stage Areas", "", "");
                iEventSeq = (iEventSeq + 1);

                DirectoryInfo di = new DirectoryInfo(sAPPZipPath);
                foreach (FileInfo Zipfile in di.GetFiles())
                {
                    Zipfile.Delete();
                }

                int iZipFileCnt = di.GetFiles().Count();

                //-- Create or clear PDF Stage Area
                di = new DirectoryInfo(sAPPPDFPath);
                foreach (FileInfo PDFfile in di.GetFiles())
                {
                    PDFfile.Delete();
                }

                int iPDFFileCnt = di.GetFiles().Count();

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ZipCleanStageArea(END)", iEventSeq.ToString(), "FINISHED", DateTime.Now + " - ZipCleanStageArea Process Ended: Deleted ALL files from the ZIP and PDF Stage Areas", "File Counts: ZIP: " + iZipFileCnt.ToString() + " PDF: " + iPDFFileCnt.ToString(), "");
                iEventSeq = (iEventSeq + 1);
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("ZipCleanStageArea Failed: Failed to delete files from the stage areas (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        private string ConvertSendAPDPostOffice(string sAttachmentStageArea, string sCurrentFileName)
        {
            string sNewFileName = string.Empty;

            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(START)", iEventSeq.ToString(), "Convert and Send to APDPosOffice Started ", DateTime.Now + " - Converting attachments to PDF and sending to the APDPostOffice", "AttachmentStageArea: " + sAttachmentStageArea + ", CurrentFileName: " + sCurrentFileName, "");
            iEventSeq = (iEventSeq + 1);

            try
            {
                //-- Start of PostOffice Processing --//
                System.IO.FileInfo fiAttachment1 = new System.IO.FileInfo(sAttachmentStageArea + sCurrentFileName);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(CONVERT2PDF)", iEventSeq.ToString(), "Converting: " + sAttachmentStageArea + sCurrentFileName + " to PDF", "", "", "");
                iEventSeq = (iEventSeq + 1);

                ConvertToPDF(sAttachmentStageArea + sCurrentFileName, System.IO.Path.GetExtension(sCurrentFileName));

                // -------------------------------------
                //  Check if the new PDF file exists
                // -------------------------------------
                try
                {
                    if (File.Exists(sAttachmentStageArea + sCurrentFileName))
                    {
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(FILECONVERTED)", iEventSeq.ToString(), "SUCCESSFULLY Converted " + sAttachmentStageArea + sCurrentFileName + " to PDF.", "", "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                    else
                    {
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(FILECONVERTFAILED)", iEventSeq.ToString(), "FAILED Converting File " + sAttachmentStageArea + sCurrentFileName + " to PDF.", "", "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                }
                catch (Exception oExcept)
                {
                    // ------------------------------
                    //  Email Notify of the Error
                    // ------------------------------
                    string strError = "";
                    StackFrame FunctionName = new StackFrame();
                    strError = string.Format("Error: {0}", ("PostOffice-ConvertSendAPDPostOffice Failed: Failed to convert and files(" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                    LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - File Convert Error occurred...", "Error: " + strError, "", "");
                    iEventSeq = (iEventSeq + 1);

                    return "";
                }

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(NEWFILENAME)", iEventSeq.ToString(), "ReNaming " + sAttachmentStageArea + sCurrentFileName + " to PDF.", "", "", "");
                iEventSeq = (iEventSeq + 1);

                sNewFileName = fiAttachment1.Name.Replace(System.IO.Path.GetExtension(sCurrentFileName), ".pdf");
                //sNewFileName = fiAttachment1.Name;

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(NEWPDFFILENAME)", iEventSeq.ToString(), "New File Name " + sNewFileName , "", "", "");
                iEventSeq = (iEventSeq + 1);

                // -------------------------------------
                //  Rename PDF file
                // -------------------------------------
                try
                {
                    if (File.Exists(sAttachmentStageArea + sNewFileName))
                    {
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(FILERENAMED)", iEventSeq.ToString(), "SUCCESSFULLY Renamed " + sAttachmentStageArea + sNewFileName + " to PDF.", "", "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                    else
                    {
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(FILERENAMEFAILED)", iEventSeq.ToString(), "FAILED Renaming File " + sAttachmentStageArea + sNewFileName + " to PDF.", "", "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                }
                catch (Exception oExcept)
                {
                    // ------------------------------
                    //  Email Notify of the Error
                    // ------------------------------
                    string strError = "";
                    StackFrame FunctionName = new StackFrame();
                    strError = string.Format("Error: {0}", ("PostOffice-ConvertSendAPDPostOffice Failed: Failed to rename and files(" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                    LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - File Rename Error occurred...", "Error: " + strError, "", "");
                    iEventSeq = (iEventSeq + 1);

                    return "";
                }

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(NEWFILENAME)", iEventSeq.ToString(), "New File is " + sAttachmentStageArea + sNewFileName, "", "", "");
                iEventSeq = (iEventSeq + 1);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-ConvertSendAPDPostOffice(END)", iEventSeq.ToString(), "Convert and Send to APDPosOffice Ended ", DateTime.Now + " - Process Ended.", "", "");
                iEventSeq = (iEventSeq + 1);

                return sNewFileName;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("PostOffice-ConvertSendAPDPostOffice Failed: Failed convert and send files to APDPostOffice(" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);

                return "";
            }
        }

        private void InsertJobIntoAPDPostOffice(string sLynxID, string sClientID, string sClientName, string sClaimNo, string sApp, string sUserID, string sPertainsTo, string sSuppSeqNumber, string sDocumentType, string sDocumentSource, string sAttachmentStageArea, string sNewFileName)
        {
            string sFileLength = string.Empty;
            string sFileData = string.Empty;
            string sFileExtension = string.Empty;
            string sRC = string.Empty;

            if (File.Exists(sAttachmentStageArea + sNewFileName))
            {
                // ------------------------------------------
                //  Logging - Check PDF Created Successfully
                // ------------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(FILEIO)", iEventSeq.ToString(), "File " + sNewFileName + " was created Successfully...", "File successfully converted to PDF: " + DateTime.Now + " FileName: " + sNewFileName, "", "");
                iEventSeq = (iEventSeq + 1);

                System.IO.FileInfo fiAttach1 = new System.IO.FileInfo(sAttachmentStageArea + sNewFileName);

                sFileLength = fiAttach1.Length.ToString();
                sFileExtension = fiAttach1.Extension;
                sFileExtension = sFileExtension.Substring(1, sFileExtension.Length - 1);

                sFileData = GetBinData(sAttachmentStageArea + sNewFileName);

                //System.IO.File.WriteAllText(sAttachmentStageArea + "TVDTEST.PDF", sFileData);

                //----------------------------------
                // Create the APD PostOffice JobXML
                //----------------------------------
                sRC = CreatePostOfficeJob(sLynxID, sClientID, sClientName, sClaimNo, sApp, sUserID, sPertainsTo, sSuppSeqNumber, sDocumentType, sDocumentSource, sNewFileName, sFileExtension, sFileLength, sFileData);

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUG 1", iEventSeq.ToString(), "DEBUG 1", DateTime.Now + " - Done Creating PostOfficeJob.", "RC: " + sRC, "");
                //iEventSeq = (iEventSeq + 1);

                if (sRC.ToUpper() == "SUCCESS")
                {
                    // ------------------------------------------
                    //  Logging - Attachment file processed.  
                    //  Time to clean up the mess
                    // ------------------------------------------
                    // -- Move New file to Archive --//

                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUG 1", iEventSeq.ToString(), "DEBUG 1", DateTime.Now + " - Moving the file.", "sArchivePath: " + sArchivePath + ", sNewFileName: " + sNewFileName, "");
                    iEventSeq = (iEventSeq + 1);

                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    //LocalLogEvent(sProcessingServer, sEventTransactionID, "DEBUG 3", iEventSeq.ToString(), "DEBUG 3", DateTime.Now + " - File Moved.", "", "");
                    //iEventSeq = (iEventSeq + 1);

                    if (File.Exists(sAttachmentStageArea + sNewFileName))
                    {
                        fiAttach1.MoveTo(sArchivePath + "ATTACH_" + sEventTransactionID + DateTime.Now.ToString("MMddyyyyHHmmss") + sNewFileName);

                        // ------------------------------------------
                        //  ERROR - Move PDF to Archive Failed
                        // ------------------------------------------
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ARCHFILEIO)", iEventSeq.ToString(), "File " + "ATTACH_" + sNewFileName + " was moved Successfully to the archive...", "File successfully moved from StageArea to Archive: " + DateTime.Now + " Archive FileName: " + "ATTACH_" + sNewFileName, "", "");
                        iEventSeq = (iEventSeq + 1);
                    }
                    else
                    {
                        // ------------------------------------------
                        //  ERROR - Move PDF to Archive Failed
                        // ------------------------------------------
                        LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + sNewFileName + " Does NOT Exists and can't be moved Successfully to the archive...", "ERROR - File DOES NOT EXISTS and can't successfully moved from StageArea to Archive: " + DateTime.Now + " ERROR FileName: " + sNewFileName, "", "");
                        iEventSeq = (iEventSeq + 1);
                    }

                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    //LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILMOVE)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Parameters...", string.Format("Params: {0} <---> {1}", email.Id.ToString(), oNOLArchivedFolderID.ToString()), "");
                    //iEventSeq = (iEventSeq + 1);

                    //Boolean bRC = MoveEmail(email, oNOLArchivedFolderID);

                    //if (bRC)
                    //{
                    //    // -- Successful -- //
                    //    // ----------------------------------------
                    //    //  Debugging
                    //    // ----------------------------------------
                    //    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILARCH)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Email SUCCESSFULLY moved to the archive...", string.Format("RC: {0}", bRC.ToString()), "");
                    //    iEventSeq = (iEventSeq + 1);
                    //}
                    //else
                    //{
                    //    // -- Failed -- //
                    //    // ----------------------------------------
                    //    //  Debugging
                    //    // ----------------------------------------
                    //    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILARCH)", iEventSeq.ToString(), "MoveEmail to Archive: ", DateTime.Now + " - Email FAILED move to archive...", string.Format("RC: {0}", bRC.ToString()), "");
                    //    iEventSeq = (iEventSeq + 1);
                    //}
                }
                else
                {
                    //Throw Error
                    // ----------------------------------------
                    //  Debugging
                    // ----------------------------------------
                    LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "APD Postoffice job insert failed: ", DateTime.Now + " - Job failed to insert into the APD PostOffice...", string.Format("RC: {0}", sRC), "");
                    iEventSeq = (iEventSeq + 1);
                }
            }
            else
            {
                // ------------------------------------------
                //  ERROR - PDF Create UnSuccessful
                // ------------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(ERROR)", iEventSeq.ToString(), "ERROR - File " + sNewFileName + " was NOT created Successfully...", "ERROR - File NOT successfully converted to PDF: " + DateTime.Now + " ERROR FileName: " + sNewFileName, "", "");
                iEventSeq = (iEventSeq + 1);
            }
        }

        private ArrayList ProcessZipFile(FileAttachment fileAttachment, string sCurrentFileName, string sTempZipPathFile, string sTempZipArea)
        {
            ArrayList aZipFileNames = new ArrayList();

            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming-ProcessZipFile(START)", iEventSeq.ToString(), "PROCESSING ZIP Started ", DateTime.Now + " - ProcessZipFile Start: UnPacking ZIP and building stage areas", "sTempZipPathFile: " + sTempZipPathFile + ", sTempZipArea: " + sTempZipArea + ", AttachmentFileName: " + fileAttachment.FileName, "");
            iEventSeq = (iEventSeq + 1);

            try
            {
                //-- Clear file name array
                aZipFileNames.Clear();

                //-- Create or clear temp zip area
                ZipCleanStageArea();

                ZipArchive zip = new ZipArchive();
                //zip.Open("D:\\Apps\\NOLIncoming365Svc\\StagingArea\\1849744-6d8a394a-c2a5-45b3-88db-c1e985ca2332Sample.zip");
                //fileAttachment.FileName
                //zip.Open(sTempZipPathFile);
                zip.Open(fileAttachment.FileName);

                LocalLogEvent(sProcessingServer, sEventTransactionID, "<<<DEBUG>>>", iEventSeq.ToString(), "ZIP COUNT ", DateTime.Now + " - Zip files count", "Count: " + zip.Count, "");
                iEventSeq = (iEventSeq + 1);

                for (int i = 0; i < zip.Count; i++)
                {
                    ZipArchiveItem item = zip[i];
                    string itemName = sTempZipArea + item.ItemName;

                    //checking whether the item is root file
                    if (itemName.Contains("/"))
                    {
                        itemName = itemName.Replace("/", "\\");
                    }

                    //-- Check whether the Directory is present or not
                    if (!Directory.Exists(itemName) || itemName.Contains("\\"))
                    {
                        int index = itemName.LastIndexOf("\\");
                        string directoryPath = itemName.Remove(index, itemName.Length - index);
                        Directory.CreateDirectory(directoryPath);
                    }

                    FileStream fileStream = new FileStream(itemName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    MemoryStream memoryStream = item.DataStream as MemoryStream;
                    memoryStream.WriteTo(fileStream);
                    fileStream.Flush();
                    fileStream.Close();

                    //-- Convert each file to PDF
                    fileAttachment.Load(sAttachmentStageArea + sCurrentFileName);

                    //-- Save Current Attachment files for further processing
                    aZipFileNames.Add(item.ItemName);
                }

                //-- Cleanup ZipStage area

                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming-ProcessZipFile(END)", iEventSeq.ToString(), "PROCESSING ZIP Ended ", DateTime.Now + " - ProcessZipFile Ended.", "", "");
                iEventSeq = (iEventSeq + 1);

                return aZipFileNames;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("ProcessZipFile Failed: Failed to parse ZIP files (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                LocalLogEvent(sProcessingServer, sEventTransactionID, "ERROR", iEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Error: " + strError, "", "");
                iEventSeq = (iEventSeq + 1);

                return aZipFileNames;
            }
        }

        private void ErrorNotify(string sReplyEmailAddress, string sErrorSubject, string sErrorMessage)
        {
            //-- Notify the sender that the email is being reviewed
            wsPGWAPDFoundation.SendMail(sReplyEmailAddress, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(), "", sErrorSubject, sErrorMessage, appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());
        }

        private void NotifyBadLynxID(string sSMTPEmailAddress, string sFrom, string sLynxID, EmailMessage email)
        {
            //-------------------------------------
            // Logging - No LynxID in Subject line
            //-------------------------------------
            //-- Email user to inform them they didn't include the LYNXID
            //-- Notify the sender that the email is being reviewed or disqualified
            //sTo = (email as EmailMessage).DisplayTo;
            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(NOLYNXIDEMAIL)", iEventSeq.ToString(), "No LynxID in Subject, Reply to the sender: ", DateTime.Now + " - sender response LynxID was not found in subject line.  Processing delayed...", string.Format("To: {0}, From: {1}, SMTPServer {2}", sSMTPEmailAddress, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(), appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString()), "");
            iEventSeq = (iEventSeq + 1);

            //-- Send email back to from explaining to check the LynxID formatting
            if (!sFrom.Contains(sOffice365User.Replace("@lynxservices.com", "")))
            {
                //ErrorNotify(sFrom, "NOLIncoming Error", "The LYNX ID in the subject line is not properly formatted.  Must Be LYNX ID: xxxxxxxx-x with nothing after it.  Exp: LYNX ID: 8150486-1, LynxID Received: " + sLynxID);
                wsPGWAPDFoundation.SendMail(sSMTPEmailAddress, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(), "", "NOLIncoming Error: (BAD EMAIL) - Invalid Subject Line Syntax.", "The LYNX ID in the subject line of the NOLIncoming email you sent is not properly formatted.  Must Be LYNX ID: xxxxxxxx-x with nothing after it.  Exp: LYNX ID: 8150486-1, LynxID Received: " + sLynxID, appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());
            }

            Boolean bRC = MoveEmail(email, oNOLReviewFolderID);

            if (bRC)
            {
                // -- Successful -- //

                //-- Notify the sender that the email is being reviewed or disqualified
                //wsPGWAPDFoundation.SendMail(sFrom, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(),"","BAD EMAIL - Invalid Subject Line Syntax.", "The subject line was not properly formatted.  It must contain the tag: LynxID: nnnnnnn.  The email has been moved to the review folder.  Please consider resending again with the correct format.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILREVIEW)", iEventSeq.ToString(), "Move Email to Review: ", DateTime.Now + " - Email SUCCESSFULLY moved to the review because LynxID was not found...", string.Format("RC: {0}, EmailFrom: {1}", bRC.ToString(), sFrom), "");
                iEventSeq = (iEventSeq + 1);
            }
            else
            {
                // -- Failed -- //

                //-- Notify the sender that the email is being reviewed or disqualified
                wsPGWAPDFoundation.SendMail(sSMTPEmailAddress, appSettingsReader.GetValue("FromEmail", typeof(string)).ToString(), "", "BAD EMAIL - Invalid Subject Line Syntax.", "The subject line was not properly formatted.  It must contain the tag: LynxID: nnnnnnn.  The email has been moved to the review folder.  Please consider resending again with the correct format.", appSettingsReader.GetValue("SMTPServer", typeof(string)).ToString());

                // ----------------------------------------
                //  Debugging
                // ----------------------------------------
                LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILREVIEW)", iEventSeq.ToString(), "Move Email to Review: ", DateTime.Now + " - Email FAILED move to Review...", string.Format("RC: {0}, EmailFrom: {1}", bRC.ToString(), sFrom), "");
                iEventSeq = (iEventSeq + 1);
            }

            LocalLogEvent(sProcessingServer, sEventTransactionID, "PostOffice-NOLIncoming(EMAILREVIEW)", iEventSeq.ToString(), "Email Processing - No LynxID in Subject line", DateTime.Now + " - Email: No LynxID in Subject line: Please Review this attachment...", "", "");
            iEventSeq = (iEventSeq + 1);
        }

    }
}
