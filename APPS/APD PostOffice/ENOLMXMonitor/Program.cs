﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using PGW.Shared;
using PGW.Shared.SiteUtilities;

namespace FNOLPostOffice.ENOLMXMonitor
{
    class Program
    {
        static void Main(string[] args)
        {

            string configFile = GenUtils.AppendPath(false,
                (Debugger.IsAttached ? (new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)).Parent.Parent.FullName : AppDomain.CurrentDomain.BaseDirectory),
                "enolmxmonitor_config.xml");

            Events events = new Events();
            GenUtils.Assert(File.Exists(configFile), string.Format("Configuration file, '{0}', not found.", configFile));

            events.ComponentInstance = "ENOLMXMonitor";
            events.Initialize(configFile);
            events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");

            XmlNode mxListRootNode = events.cSettings.GetParsedNode("Mailboxes");
            XmlNode mailbox = null;

            IEnumerator mxPtr = XmlUtils.GetChildNodeList(ref mxListRootNode, "Mailbox[@enabled='1']").GetEnumerator();
            IMailboxProcessor mlbxproc = null;
            string processorName = "";
            Type type;

            while (mxPtr.MoveNext())
            {
                mailbox = (XmlNode)mxPtr.Current;

                processorName = string.Format("FNOLPostOffice.ENOLMXMonitor.{0}", XmlUtils.GetChildNodeText(ref mailbox,"@processor"));
                type = Type.GetType(processorName);

                mlbxproc = (IMailboxProcessor)Activator.CreateInstance(type);
                mlbxproc.Initialize(ref events, mailbox);
                
                try
                {
                    mlbxproc.CleanUpTempFolder();
                    mlbxproc.RetrieveAndLogNewMail();
                    mlbxproc.ProcessNewMail();
                }
                catch (Exception ex)
                {
                    events.HandleEvent(ex, false);
                }                    

                mlbxproc = null;
            }

            events.DumpLogBufferToDisk();
            events.Dispose();
            events = null;
        }
    }
}
