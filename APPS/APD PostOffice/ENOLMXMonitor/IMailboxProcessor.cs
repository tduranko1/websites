﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using PGW.Shared.SiteUtilities;
using PGW.Shared.Encryption;
using OpenPop.Mime;

namespace FNOLPostOffice.ENOLMXMonitor
{
    interface IMailboxProcessor
    {
        void Initialize(ref Events events, XmlNode mailboxNode);

        void RetrieveAndLogNewMail();
        void ProcessNewMail();
        void CleanUpTempFolder();
        //bool RetrieveAndProcessMail();
    }

    interface IMessageProcessor
    {
        void Initialize(ref Events events, ref XmlNode mailboxNode);
        MessageDisposition ProcessMessage(ref Message msg, FileInfo msgSave, string MXUID);
        void HandleExceptionProcessingMessage(ref Message msg, FileInfo msgSave);
    }
}
