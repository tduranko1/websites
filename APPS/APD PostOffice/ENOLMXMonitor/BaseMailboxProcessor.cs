﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using OpenPop.Pop3;
using OpenPop.Mime;
using OpenPop.Common;
//using MailKit.Net.Pop3;

namespace FNOLPostOffice.ENOLMXMonitor
{
    class BaseMailboxProcessor : IMailboxProcessor
    {
        protected Events _events = null;
        protected FNOLDataAccess _dataAccess = null;
        protected XmlNode _mailboxNode = null;
        protected DirectoryInfo _archiveRootDir = null;

        protected bool _debug = false;
        protected string _mailboxName = "";
        protected string _clientID = "";
        private const string DECRYPTKEY = "LYNX2Ftp!";
        private System.Globalization.CultureInfo _cultureInfo = new System.Globalization.CultureInfo("en-US");

        public BaseMailboxProcessor()
        {

        }

        private string DecryptString(string encryptedString)
        {   
            string decryptedString = "";
            if (encryptedString.Length > 0)
            {
                SymmetricEncryption symenc = new SymmetricEncryption(SymmetricEncryption.Provider.TripleDES, true);
                Data key = new Data(DECRYPTKEY);
                Data encryptedStringData = new Data();
                encryptedStringData.Base64 = encryptedString;
                decryptedString = symenc.Decrypt(encryptedStringData, key).ToString();
            }
            return decryptedString;
        }

        private MailKit.Net.Pop3.Pop3Client OpenMailbox2()
        {
            MailKit.Net.Pop3.Pop3Client p3c = new MailKit.Net.Pop3.Pop3Client();
            try
            {
                string userID = XmlUtils.GetChildNodeText(ref _mailboxNode, "@user");
                string password = DecryptString(XmlUtils.GetChildNodeText(ref _mailboxNode, "@password"));
                string host = _events.cSettings.GetParsedSetting("MailServer/@name");
                int port = int.Parse(_events.cSettings.GetParsedSetting("MailServer/@port"));

                if (_debug) _events.Trace("Opening connection to Mail Server");
                //Open POP3 session
                p3c.Connect(host, port, true);

                //Authenticate user/password for mailbox
                if (_debug) _events.Trace(string.Format("Authenticating for {0}", userID));
                p3c.Authenticate(userID, password);

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                p3c = null;
            }
            return p3c;
        }
        private Pop3Client OpenMailbox()
        {
            Pop3Client p3c = new Pop3Client();
            try
            {
                string userID = XmlUtils.GetChildNodeText(ref _mailboxNode, "@user");
                string password = DecryptString(XmlUtils.GetChildNodeText(ref _mailboxNode, "@password"));
                string host = _events.cSettings.GetParsedSetting("MailServer/@name");
                int port = int.Parse(_events.cSettings.GetParsedSetting("MailServer/@port"));

                if (_debug) _events.Trace("Opening connection to Mail Server");
                //Open POP3 session
                p3c.Connect(host, port, true);

                //Authenticate user/password for mailbox
                if (_debug) _events.Trace(string.Format("Authenticating for {0}",userID));
                p3c.Authenticate(userID, password);

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                p3c = null;
            }
            return p3c;

        }
        private List<MimeKit.MimeMessage> FetchAllMessages2(ref MailKit.Net.Pop3.Pop3Client p3c)
        {
            int msgCount = p3c.Count;
            List<MimeKit.MimeMessage> messages = null;
            if (msgCount > 0)
            {
                messages = new List<MimeKit.MimeMessage>();
                for (int i = 0; i < msgCount; i++)
                {
                    try
                    {
                        messages.Add(p3c.GetMessage(i));
                    }
                    catch (Exception ex)
                    {
                        _events.HandleEvent(ex, false);
                    }
                }
            }

            return messages;
        }
        private Dictionary<int, Message> FetchAllMessages(ref Pop3Client p3c)
        {
            int msgCount = p3c.GetMessageCount();
            Dictionary<int, Message> messages = null;
            if (msgCount > 0)
            {
                messages = new Dictionary<int, Message>(msgCount);
                for (int i = msgCount; i > 0; i--)
                {
                    try
                    {
                        messages.Add(i, p3c.GetMessage(i));
                    }
                    catch { }
                }
            }

            return messages;            
        }

        #region IMailboxProcessor Members

        public void Initialize(ref Events events, XmlNode mailboxNode)
        {
            _events = events;
            _mailboxNode = mailboxNode;
            _events.cSettings.SetIdentity(Environment.MachineName.ToLower(), "");
            _dataAccess = new FNOLDataAccess(ref _events);
            _mailboxName = XmlUtils.GetChildNodeText(ref mailboxNode, "@name");
            _clientID = XmlUtils.GetChildNodeText(ref mailboxNode, "@clientid");
            _debug = "true|yes|on|1".Contains(events.cSettings.GetParsedSetting("Debug").ToLower());
            
            //_badFileDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("BadFilePath"), XmlUtils.GetChildNodeText(ref _mailboxNode, "@name")));
            _archiveRootDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("MailArchive"), _clientID));



            if (!_archiveRootDir.Exists) _archiveRootDir.Create();

        }

        public virtual void CleanUpTempFolder()
        {
            DateTime now = DateTime.Now;
            DirectoryInfo workRootDir = new DirectoryInfo(_events.cSettings.GetParsedSetting("TempWorkPath"));
            if (workRootDir.Exists)
            {
                DirectoryInfo[] tempFolders = workRootDir.GetDirectories();
                foreach (DirectoryInfo tmpFolder in tempFolders)
                {
                    if ((now - tmpFolder.LastWriteTime).Days > 5)
                    {
                        try
                        {
                            tmpFolder.Delete(true);
                        }
                        catch (Exception ex)
                        {
                            _events.HandleEvent(ex, false);
                        }
                    }
                }
            }

        }


        public virtual void RetrieveAndLogNewMail()
        {
          
            //Pop3Client p3c = OpenMailbox();
            MailKit.Net.Pop3.Pop3Client p3c = OpenMailbox2();            

            if ((p3c != null) && p3c.IsConnected)
            {
                int msgCount = p3c.Count;
                
                if (msgCount > 0)                
                {
                    DateTime dtNow = DateTime.UtcNow;
                    FileInfo emlFile = null;
                    MimeKit.MimeMessage msg = null;
                    
                    bool added = false;

                    for (int i = msgCount-1; i > -1; i--)
                    {
                        try
                        {
                            msg = p3c.GetMessage(i);
                            emlFile = new FileInfo(GenUtils.AppendPath(false, _archiveRootDir.FullName, dtNow.ToString("yyyyMMdd"), string.Format("{0}.eml", msg.MessageId)));
                            added = false;
                           
                            if (!emlFile.Directory.Exists) emlFile.Directory.Create();
                            msg.WriteTo(emlFile.FullName);
                            added = InsertMXRecord(ref msg, emlFile);

                            if (added) p3c.DeleteMessage(i);
                        }
                        catch (Exception ex)
                        {
                            _events.HandleEvent(ex, false);
                        }
                    }
                }                

                p3c.Disconnect(true);
            }


        }

        public virtual void ProcessNewMail()
        {
            
            Message msg = null;           
            MessageDisposition disp = null;
            string msgProcessorName = XmlUtils.GetChildNodeText(ref _mailboxNode, "@msgprocessor");
            Type type = Type.GetType(string.Format("FNOLPostOffice.ENOLMXMonitor.{0}", msgProcessorName));
            IMessageProcessor msgProc = (IMessageProcessor)Activator.CreateInstance(type);
            msgProc.Initialize(ref _events, ref _mailboxNode);
            DataTable newMail = GetNewMXList();
            string mxuid = string.Empty;
            FileInfo emlFile = null;
            string invalidReason = "";

            if (newMail != null)
            {
                bool ok = false;
                foreach (DataRow dr in newMail.Rows)
                {
                    mxuid = Convert.ToString(dr["MXUID"]);
                    emlFile = new FileInfo(Convert.ToString(dr["EMLFilePath"]));
                    if (emlFile.Exists)
                    {
                        ok = false;
                        try
                        {
                            msg = Message.Load(emlFile);
                            if (IsValidEmail(ref msg, out invalidReason))
                            {
                                try
                                {
                                    disp = msgProc.ProcessMessage(ref msg, emlFile, mxuid);
                                    ok = (disp != null);
                                }
                                catch (Exception ex)
                                {
                                    // An exception happened while trying to process email.  We will send it to exception queue.
                                    _events.HandleEvent(ex, string.Format("MXUID = {0}", mxuid), false);
                                    disp = new MessageDisposition() { Code = MessageDispositionCd.SendToException, Details = string.Format("Exception occurred processing enol: {0}", ex.Message) };
                                }
                                if (!ok)
                                {
                                    try
                                    {
                                        msgProc.HandleExceptionProcessingMessage(ref msg, emlFile);
                                        disp = new MessageDisposition() { Code = MessageDispositionCd.SendToException, Details = "Exception occurred processing enol." };
                                    }
                                    catch (Exception ex)
                                    {
                                        _events.HandleEvent(ex, string.Format("MXUID = '{0}", mxuid), false);
                                        string tmp = disp.Details;
                                        disp = new MessageDisposition() { Code = MessageDispositionCd.Error, Details = string.Format("Exception occured sending enol to exception: {0}", ex.Message) };
                                    }
                                }
                                SetDisposition(mxuid, disp);

                            }
                            else
                            {
                                disp = new MessageDisposition() { Code = MessageDispositionCd.InvalidMail, Details = invalidReason };
                                SetDisposition(mxuid, disp);
                            }


                        }
                        catch (Exception ex)
                        {
                            disp = new MessageDisposition() { Code = MessageDispositionCd.InvalidMail, Details = ex.Message };
                            SetDisposition(mxuid, disp);
                            _events.HandleEvent(ex, false);

                        }  

                        
                    }
                    else
                    {
                        // Could not find eml store.
                    }

                }

            }

        }

        protected bool IsValidEmail(ref Message msg, out string invalidReason)
        {
            invalidReason = "";    
            bool valid = msg.Headers.From.HasValidMailAddress;
            if (!valid)
                invalidReason = "From address on email was not valid";

            return valid;
        }
    

        //public virtual bool RetrieveAndProcessMail()
        //{
        //    bool success = true;
        //    // Open Connection to Mailbox
        //    Pop3Client p3c = OpenMailbox();

        //    if ((p3c != null) && p3c.Connected)
        //    {
        //        Dictionary<int, Message> messages = FetchAllMessages(ref p3c);
        //        if (messages != null)
        //        {
        //            _events.Trace(string.Format("Unprocessed Messages for {0} on this cycle: {1}", _mailboxName, messages.Count.ToString()));
        //            if (messages.Count > 0)
        //            {
        //                DateTime dtNow = DateTime.UtcNow;
        //                FileInfo msgSave = null;
        //                MessageDisposition msgDisp = null;
        //                Message msg = null;
        //                string msgProcessorName = XmlUtils.GetChildNodeText(ref _mailboxNode, "@msgprocessor");
        //                Type type = Type.GetType(string.Format("FNOLPostOffice.ENOLMXMonitor.{0}", msgProcessorName));
        //                IMessageProcessor msgProc = (IMessageProcessor)Activator.CreateInstance(type);
        //                msgProc.Initialize(ref _events, ref _mailboxNode);
        //                int msgNumber = 0;
        //                foreach (KeyValuePair<int, Message> msgEntry in messages)
        //                {
        //                    msgNumber = msgEntry.Key;
        //                    msg = msgEntry.Value;
        //                    //Save original email to archive                        
        //                    msgSave = new FileInfo(GenUtils.AppendPath(false, _archiveRootDir.FullName, dtNow.ToString("yyyyMMdd"), string.Format("{0}.eml", msg.Headers.MessageId)));
        //                    if (!msgSave.Directory.Exists) msgSave.Directory.Create();
        //                    msg.Save(msgSave);
        //                    //Log Reciept of email
        //                    //LogRecipietOfMessage(ref msg);

        //                    //Process Email
        //                    msgDisp = msgProc.ProcessMessage(ref msg, msgSave);
                            
        //                    //SetDispositionOfMsg(ref msg, MessageDisposition msgDisp);

        //                    //remove email from mail server
        //                    p3c.DeleteMessage(msgNumber);
        //                }

        //            }
        //            else
        //                success = true;
        //        }
        //        p3c.Disconnect();
        //    }
            
        //    p3c = null;

        //    return success;
        //}


        #endregion

        private void SetDisposition(string mxuid, MessageDisposition disp)
        {
            string status = string.Empty;
            string statusdet = "";

            switch (disp.Code)
            {
                case MessageDispositionCd.Success:
                    status = "ENOL";
                    break;
                case MessageDispositionCd.Error:
                    status = "ERROR";
                    statusdet = disp.Details;                    
                    break;
                case MessageDispositionCd.SendToException:
                    status = "EXCEPTION";
                    statusdet = disp.Details;
                    break;
                case MessageDispositionCd.ReturnToSender:
                    status = "RTS";
                    statusdet = disp.Details;
                    break;
                case MessageDispositionCd.InvalidMail:
                    status = "INVALID";
                    break;
                default:
                    status = "UNK";                    
                    break;
            }

            NameValueCollection p = new NameValueCollection();
            p.Add("MXUID", mxuid);
            p.Add("NewStatus", status);
            p.Add("NewStatusDetail", disp.Details);

            try
            {
                _dataAccess.ExecuteDatabaseTransactionNoReturn("MXUpdateStatus", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
            }

        }

        private DataTable GetNewMXList()
        {
            DataTable dt = null;
            NameValueCollection p = new NameValueCollection();
            p.Add("ClientID", _clientID);
            try
            {
                dt = _dataAccess.ExecuteDatabaseTransactionReturnTable("MXRetreiveList", ref p);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                dt = null;
            }            

            if ((dt != null) && (dt.Rows.Count == 0)) dt = null;

            return dt;
        }

        private bool InsertMXRecord(ref MimeKit.MimeMessage msg, FileInfo emlPath)
        {
            bool success = false;
            DateTime now = DateTime.Now;
            string mxuid = Guid.NewGuid().ToString();

            string sender = "UNKNOWN";
            if (msg.From != null)
            {
                sender = msg.From.ToString();
            }
            
            string source = (_events.cSettings.GetParsedSetting("FAXServerSendingEmailAddresses").Contains(sender) ? "FAX" : "EMAIL");
            

            NameValueCollection p = new NameValueCollection();

            try
            {
                p.Add("MXUID", mxuid);
                p.Add("ClientID", _clientID);
                p.Add("MessageID", msg.MessageId);
                p.Add("OriginationSource", source);
                p.Add("EmailReceived", msg.Date.ToString());
                p.Add("EmailSender", sender.Replace("\"", ""));
                p.Add("EmailSubject", msg.Subject);
                p.Add("AttachmentCount", msg.Attachments.ToList().Count.ToString());
                p.Add("EMLFilePath", emlPath.FullName);            
                _dataAccess.ExecuteDatabaseTransactionNoReturn("MXInsertRecord", ref p);
                success = true;
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex,false);
            }
            return success;
        }

    }
}
