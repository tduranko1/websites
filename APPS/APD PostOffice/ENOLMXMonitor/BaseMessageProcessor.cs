﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Xml;
using PGW.Shared;
using PGW.Shared.DataAccess;
using PGW.Shared.Encryption;
using PGW.Shared.SiteUtilities;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.codec;
//using OpenPop.Pop3;
using OpenPop.Mime;
//using OpenPop.Common;
//using Syncfusion.DocIO;
//using Syncfusion.DocToPDFConverter;
//using Syncfusion.DocIO.DLS;
//using Syncfusion.XlsIO;
//using SynchPDF = Syncfusion.Pdf;
//using SynchPDFG = Syncfusion.Pdf.Graphics;

//using MOW = Microsoft.Office.Interop.Word;




namespace FNOLPostOffice.ENOLMXMonitor
{
    class BaseMessageProcessor : IMessageProcessor, IDisposable
    {

        private Message _message = null;
        private Events _events = null;
        private XmlNode _mailboxNode = null;
        private DirectoryInfo _workRootDir = null;
        private DirectoryInfo _msgWorkDir = null;
        //private DirectoryInfo _archiveDir = null;
        //private DirectoryInfo _errorDir = null;
        protected FNOLDataAccess _dataAccess = null;
        private string _msgENOLUID;

        private bool _debug = false;
        //private string _msgSource = "";
        private StringBuilder _mailHeaderAndBody = null;
        private MsgAttachments _attachmentInfo;
        protected DirectoryInfo _archiveRootDir;
        protected string _clientID = "";

        private FileInfo _headerAndBodyPDF = null;

        public BaseMessageProcessor() { }

        #region IDisposable Members

        public void Dispose()
        {
            _events = null;
            _message = null;
            _mailboxNode = null;
        }

        #endregion
        
    
        #region IMessageProcessor Members

        public void  Initialize(ref Events events,ref XmlNode mailboxNode)
        {
            _events = events;
            _dataAccess = new FNOLDataAccess(ref _events);
            _mailboxNode = mailboxNode;           
            _debug = "true|yes|on|1".Contains(events.cSettings.GetParsedSetting("Debug").ToLower());
            _workRootDir = new DirectoryInfo(_events.cSettings.GetParsedSetting("TempWorkPath"));
            if (_workRootDir.Exists) _workRootDir.Create();

            _clientID = XmlUtils.GetChildNodeText(ref mailboxNode, "@clientid");
            
            _archiveRootDir = new DirectoryInfo(GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting("MailArchive"), _clientID));

            if (!_archiveRootDir.Exists) _archiveRootDir.Create();
        }
        

        public MessageDisposition ProcessMessage(ref Message msg, FileInfo msgSave, string MXUID)
        {
            MessageDisposition disp = null;        
            FileInfo messageAsPDF = null;
            _msgENOLUID = MXUID;
            _msgWorkDir = new DirectoryInfo(GenUtils.AppendPath(false, _workRootDir.FullName, _msgENOLUID));
            if (!_msgWorkDir.Exists) _msgWorkDir.Create();
            _mailHeaderAndBody = new StringBuilder();
            _message = msg;
           
            bool unsupportedTypeFound = false;

            //Extract Attachments
            StringCollection noNameTextAttachments = new StringCollection();
            ExtractAttachments(ref unsupportedTypeFound);
            if (unsupportedTypeFound)
            {
                // Unsupported File Attachment Found.  Need to Foward to Unsupported Recipient/Mailbox
                disp = new MessageDisposition() { Code = MessageDispositionCd.ReturnToSender, Details = "Unsupported Attachment Format" };
                HandleMessageWithUnsupportedAttachmentType(ref msg, msgSave);
            }
            else
            {
                try
                {
                    messageAsPDF = ConvertMessageToPDF();
                    if (messageAsPDF != null)
                    {
                        disp = SendToENOLProcess(ref msg,  messageAsPDF);
                        //disp = new MessageDisposition() { Code = MessageDispositionCd.Success, MessageAsPDF = messageAsPDF };
                    }

                }
                catch (Exception ex)
                {
                    _events.HandleEvent(ex, false);
                    disp = new MessageDisposition() { Code = MessageDispositionCd.Error, Details = ex.Message };
                }
            }

            int iAttempts = 0;
            bool cleanedUp = false;
            while (!cleanedUp && (iAttempts < 3))
            {
                try
                {
                    _msgWorkDir.Delete(true);
                    cleanedUp = true;
                }
                catch (Exception ex)
                {
                    if (iAttempts == 3)
                        _events.HandleEvent(ex, false);
                    else
                    {
                        iAttempts += 1;
                        System.Threading.Thread.Sleep(2000);
                    }
                }
            }

            return disp;
        }

        #endregion

        protected FileInfo ConvertMessageToPDF()
        {
            bool success = false;            
            string tmpFileName = "";
            FileInfo messagePDF = new FileInfo(GenUtils.AppendPath(false, _msgWorkDir.FullName, string.Format("{0}.pdf", _msgENOLUID)));
            
            //int i = 0;
            List<string> pdfFilesToMerge = new List<string>();
            try
            {
                ExtractHeader();
                //merge.AppendPDFFile(_headerAndBodyPDF.FullName);
                pdfFilesToMerge.Add(_headerAndBodyPDF.FullName);

                if (_attachmentInfo.HasAttachments && _attachmentInfo.HasOfficeDoc)
                {                    
                    ConvertAndAddSupportedOfficeDocs(ref _attachmentInfo, ref pdfFilesToMerge);
                }

                if (_attachmentInfo.HasAttachments && !_attachmentInfo.OnlyPDForWordAttachments)
                {
                    tmpFileName = GenUtils.AppendPath(false, _msgWorkDir.FullName, "ConvertedAttachments.pdf");
                    Document doc = new Document();
                    PdfWriter.GetInstance(doc, new FileStream(tmpFileName, FileMode.Create));
                    doc.Open();

                    if (_attachmentInfo.HasTextDoc)
                    {
                        foreach (FileInfo f in _attachmentInfo.TextDoc)
                        {
                            using (StreamReader rdr = new StreamReader(f.FullName))
                                doc.Add(new iTextSharp.text.Paragraph(rdr.ReadToEnd()));
                        }
                    }                   
                    if (_attachmentInfo.HasImages)
                    {
                        foreach (FileInfo f in _attachmentInfo.Images)
                        {
                                switch (f.Extension.Replace(".", "").ToUpper())
                                {
                                    case "TIF":
                                    case "TIFF":                                          
                                        AddTif(ref doc, f);
                                        //image = Image.GetInstance(imgStream, );
                                        break;
                                    case "GIF":
                                        try
                                        {
                                            AddGif(ref doc, f);
                                        }
                                        catch (Exception ex)
                                        {
                                            _events.HandleEvent(ex, false);
                                            AddImage(ref doc, f);
                                        }
                                        break;
                                    default:
                                        AddImage(ref doc, f);
                                        //image = Image.GetInstance(imgStream);
                                        break;
                                }

                        }
                    }
                    

                    doc.Close();
                    doc.Dispose();
                    doc = null;
                    pdfFilesToMerge.Add(tmpFileName);
                }
                    
                
                if (_attachmentInfo.HasPDFDoc)
                {
                    foreach (FileInfo f in _attachmentInfo.PDFDoc)
                        pdfFilesToMerge.Add(f.FullName);
                }

                
                success = MergeAllPDFs(messagePDF, pdfFilesToMerge);

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                success = false;
            }


            return (success ? messagePDF : null);           
        }
     


        private void ConvertAndAddSupportedOfficeDocs(ref MsgAttachments _attachmentInfo, ref List<string> pdfFilesToMerge)
        {

            DocumentConverter.ConversionResult result = null;
            Exception wex = null;
            string fn = "";

            foreach (FileInfo f in _attachmentInfo.OfficeDoc)
            {
                if (f.Extension.Replace(".", "").ToLower().StartsWith("doc"))
                    result = DocumentConverter.ConvertWordToPDF(f);
                else if (f.Extension.Replace(".", "").ToLower().StartsWith("xls"))
                    result = DocumentConverter.ConvertExcelToPDF(f);
                else if (f.Extension.Replace(".","").ToLower().StartsWith("ppt"))
                    result = DocumentConverter.ConvertPowerPointToPDF(f);
                else 
                    throw new Exception(string.Format("Failed to convert attachment {0} to PDF Format.  Extension not recognized.", f.Extension));
                if (result.Success)
                    pdfFilesToMerge.Add(result.ConvertedFile.FullName);
                else
                    wex = result.Exception;

                if (wex != null)
                {
                    fn = f.Name;
                    break;
                }
            }
            result = null;
            GC.Collect(2);
            if (wex != null)
                throw new Exception(string.Format("Failed to convert attachment {0} to PDF Format", fn), wex);
                     
        }

  


        private void AddTif(ref Document doc, FileInfo f)
        {
            RandomAccessFileOrArray ra = new RandomAccessFileOrArray(f.FullName);
            int n = TiffImage.GetNumberOfPages(ra);
            Image img;
            for (int i = 1; i <= n; i++)
            {
                img = TiffImage.GetTiffImage(ra, i);
                SetImageScale(ref img);
                doc.Add(img);
            }
        }

        private void AddGif(ref Document doc, FileInfo f)
        {            
            GifImage gfimg = new GifImage(f.FullName);
            int n = gfimg.GetFrameCount();
            Image img;
            for (int i = 1; i <= n; i++)
            {
                img = gfimg.GetImage(i);
                SetImageScale(ref img);
                doc.Add(img);
            }
        }

        private void AddImage(ref Document doc, FileInfo f)
        {
            Image img = Image.GetInstance(f.FullName);
            SetImageScale(ref img);
            doc.Add(img);
        }

        private void SetImageScale(ref Image image)
        {
            if (image.Height > image.Width)
            {
                if (image.Height > 700)
                {
                    float perc = 0.0f;
                    perc = 700 / image.Height;
                    image.ScalePercent(perc * 100);
                }
            }
            else
            {
                if (image.Width > 500)
                {
                    float perc = 0.0f;
                    perc = 500 / image.Width;
                    image.ScalePercent(perc * 100);
                }
            }
        }

        protected bool MergeAllPDFs2(FileInfo output, List<string> pdfsToMerge)
        {
            bool success = true;
            Document doc = null;
            PdfCopy writer = null;
            int idx = 0;
            try
            {          
                while (idx < pdfsToMerge.Count)
                {
                    PdfReader rdr = new PdfReader(pdfsToMerge[idx]);
                    if (rdr.AcroForm != null)
                    {
                        rdr = new PdfReader(FlattenPDFformToBytes(rdr));
                    }

                    int n = rdr.NumberOfPages;

                    if (idx == 0)
                    {
                        doc = new Document(rdr.GetPageSizeWithRotation(1));                     
                        writer = new PdfCopy(doc, new FileStream(output.FullName, FileMode.Create));
                        doc.Open();
                    }

                    writer.AddDocument(rdr);
                    writer.FreeReader(rdr);
                    
                    //writer.CopyDocumentFields(rdr);


                    idx += 1;

                    
                    rdr.Close();
                    rdr.Dispose();
                    rdr = null;
                    
                }

                

                doc.Close();
                doc.Dispose();
                doc = null;
                success = true;

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                success = false;
            }
            return success;
        }

        protected bool MergeAllPDFs(FileInfo output, List<string> pdfsToMerge)
        {
            bool success = true;
            try
            {
                int file = 0;
                PdfReader rdr;

                using (rdr = new PdfReader(pdfsToMerge[file]))
                {

                    if (rdr.AcroForm != null)
                    {
                        rdr = new PdfReader(FlattenPDFformToBytes(rdr));
                    }


                    int noOfPages = rdr.NumberOfPages;

                    using (iTextSharp.text.Document document = new Document(rdr.GetPageSizeWithRotation(1)))
                    {
                        using (PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(output.FullName, FileMode.Create)))
                        {
                            document.Open();

                            PdfContentByte cb = writer.DirectContent;
                            PdfImportedPage page;
                            
                            
                            int rotation;

                            while (file < pdfsToMerge.Count)
                            {
                                int i = 0;

                                while (i < noOfPages)
                                {
                                    i += 1;
                                    document.SetPageSize(rdr.GetPageSizeWithRotation(i));
                                    document.NewPage();
                                    page = writer.GetImportedPage(rdr, i);
                                    rotation = rdr.GetPageRotation(i);

                                    if (rotation == 90 || rotation == 270)
                                        cb.AddTemplate(page, 0, -1f, 1f, 0, 0, rdr.GetPageSizeWithRotation(i).Height);
                                    else
                                        cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                                }
                                                                 
                                file += 1;

                                if (file < pdfsToMerge.Count)
                                {
                                    rdr = new PdfReader(pdfsToMerge[file]);
                                    if (rdr.AcroForm != null)
                                    {
                                        rdr = new PdfReader(FlattenPDFformToBytes(rdr));
                                    }
                                    noOfPages = rdr.NumberOfPages;
                                }
                            }

                            document.Close();

                        }

                    }


                    rdr.Close();
                    rdr.Dispose();
                }
                success = true;
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                success = false;
            }

            return success;
        }

        private byte[] FlattenPDFformToBytes(PdfReader rdr)
        {
            var ms = new MemoryStream();
            var stamper = new PdfStamper(rdr, ms) { FormFlattening = true };
            stamper.Close();
            rdr.Close();
            rdr.Dispose();
            rdr = null;
            return ms.ToArray();
        }

        protected void ExtractHeader()
        {
            
            StringBuilder tmp = new StringBuilder();
            _mailHeaderAndBody.AppendLine(string.Format("From : {0}", _message.Headers.From.ToString()));
            foreach(OpenPop.Mime.Header.RfcMailAddress rma in _message.Headers.To) {
                tmp.Append(string.Format("{0}{1}",((tmp.Length > 0)?";":""),rma.Address));
            }
            _mailHeaderAndBody.AppendLine(string.Format("To : {0}", tmp.ToString()));
            _mailHeaderAndBody.AppendLine(string.Format("Subj : {0}", _message.Headers.Subject));
            //_mailHeaderAndBody.AppendLine(string.Format(" Date Sent: {0}", _message.Headers.DateSent.ToLongDateString()));
            _mailHeaderAndBody.AppendLine(string.Format("Date : {0}", _message.Headers.Date));
            _mailHeaderAndBody.Append("\r\n\r\n");

            foreach (MessagePart mp in _message.FindAllTextVersions())
            {
                

                switch (mp.ContentType.MediaType.ToLower())
                {
                    case "text/html":
                        //HtmlToText htt = new HtmlToText();
                        //_mailHeaderAndBody.Append(htt.Convert(mp.GetBodyAsText()));
                        _mailHeaderAndBody.Append(MXUtils.ConvertHTMLToText(mp.GetBodyAsText()));
                        break;
                    default:
                        _mailHeaderAndBody.Append(mp.GetBodyAsText());
                        break;
                }

            }

            _headerAndBodyPDF = new FileInfo(GenUtils.AppendPath(false, _msgWorkDir.FullName,"headerAndBody.pdf"));
            _events.Trace("Converting header and body");
            using (FileStream fs = new FileStream(_headerAndBodyPDF.FullName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                using (iTextSharp.text.Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc,fs))
                    {
                        doc.Open();
                        doc.Add(new iTextSharp.text.Paragraph(_mailHeaderAndBody.ToString()));
                        doc.Close();
                    }
                }
                fs.Close();
            }
            _events.Trace("Converted header and body");
            


        }
        protected void ExtractAttachments(ref bool unsupportedTypeFound)
        {
            List<MessagePart> attachments = _message.FindAllAttachments();
            List<MessagePart> textParts = _message.FindAllTextVersions();
            bool hasMailAttachment = false;
            _attachmentInfo = new MsgAttachments();
            FileInfo fi;
            FileType ft;

            foreach (MessagePart tp in textParts)
            {
                if (tp.ContentType != null)
                    if ((!string.IsNullOrEmpty(tp.ContentType.MediaType) && (tp.ContentType.MediaType.ToLower() == "message/rfc822")))
                        hasMailAttachment = true;
            }

            if (hasMailAttachment)
            {
                unsupportedTypeFound = true;
                return;
            }
            
            foreach (MessagePart attachment in attachments)
            {
                if (attachment.FileName != null)
                {
                    if (attachment.FileName == "(no name)")
                    {
                        //if (attachment.ContentType.MediaType == "message/rfc8322")
                        //{
                        //    //Attachment is an attached email
                        //}
                        unsupportedTypeFound = true;
                        break;
                    }
                    else
                    {
                        fi = new FileInfo(GenUtils.AppendPath(false, _msgWorkDir.FullName, attachment.FileName));
                        ft = DetermineFileType(fi);                        
                        if (ft == FileType.Unsupported)
                        {
                            unsupportedTypeFound = true;
                            break;
                        }
                        else
                        {                            
                            attachment.Save(fi);
                            _attachmentInfo.Add(fi, ft);
                        }
                    }
                }
            }

            
        }
        private FileType DetermineFileType(FileInfo fi)
        {
            FileType ft = FileType.Unsupported;
            XmlNode fileTypes = _events.cSettings.GetParsedNode("SupportedFileTypes");

            string fileTypeDesc = XmlUtils.GetChildNodeText(ref fileTypes,string.Format("FileType[@ext='{0}']/@type", fi.Extension.Replace(".","").ToUpper()));
            if (string.IsNullOrEmpty(fileTypeDesc)) fileTypeDesc = "Unsupported";

            try
            {
                ft =(FileType)Enum.Parse(ft.GetType(), fileTypeDesc);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, false);
                ft = FileType.Unsupported;
            }

            return ft;
        }
        private string DetermineSource(ref Message msg)
        {
            string sender = msg.Headers.From.Address.Trim();

            string[] faxList = _events.cSettings.GetParsedSetting("FAXServerSendingEmailAddresses").Split(';');

            string source = (faxList.Contains(sender) ? "FAX" : "EMAIL");

            return source;
        }

        public virtual void HandleExceptionProcessingMessage(ref Message msg, FileInfo msgSave)
        {
            string server = _events.cSettings.GetParsedSetting("SMTPServer");
            string from = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");
            string recipients = _events.cSettings.GetParsedSetting("ErrorProcessingMailNotification");
            string body = "Exception occured attempting to convert enol to single pdf.";
            string subject =  string.Format("Exception: {0}", msg.Headers.Subject);
            if (XmlUtils.NodeExists(ref _mailboxNode, "UnsupportedFormatHandling"))
            {
                recipients = XmlUtils.GetChildNodeText(ref _mailboxNode, "UnsupportedFormatHandling/@recipient");                                
            }

            MailMessage mm = new MailMessage(from, recipients);
            mm.Subject = subject;
            mm.Body = body;

            mm.Attachments.Add(new Attachment(msgSave.FullName));

            try
            {
                SmtpClient sc = new SmtpClient(server);
                sc.Send(mm);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex, string.Format("Email File: {0}", msgSave.ToString()), false);
            }

        }

        protected virtual void HandleMessageWithUnsupportedAttachmentType(ref Message msg, FileInfo msgSave)
        {
            string server = _events.cSettings.GetParsedSetting("SMTPServer");
            string from = _events.cSettings.GetParsedSetting("SMTPServer/@defaultsender");
            string recipients = _events.cSettings.GetParsedSetting("ErrorProcessingMailNotification");
            string body = "";
            string subject = "Unsupported Attachment found";
            if (XmlUtils.NodeExists(ref _mailboxNode,"UnsupportedFormatHandling"))
            {
                recipients = XmlUtils.GetChildNodeText(ref _mailboxNode,"UnsupportedFormatHandling/@recipient");
                subject = XmlUtils.GetChildNodeText(ref _mailboxNode, "UnsupportedFormatHandling/@subject").Replace("{subject}", msg.Headers.Subject);
                body = XmlUtils.GetChildNodeText(ref _mailboxNode,"UnsupportedFormatHandling/@body");
            }

            //SmtpClient sc1 = new SmtpClient();
            //sc1.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            //DirectoryInfo pdl = new DirectoryInfo(GenUtils.AppendPath(false, _workRootDir.FullName, "SMTP"));
            //if (!pdl.Exists) pdl.Create();
            //sc1.PickupDirectoryLocation = pdl.FullName;
            //sc1.Send(msg.ToMailMessage());


            MailMessage mm = new MailMessage(from, recipients);
            mm.Subject = subject;
            mm.Body = body;

            mm.Attachments.Add(new Attachment(msgSave.FullName));

            try
            {
                SmtpClient sc = new SmtpClient(server);
                sc.Send(mm);
            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex,string.Format("Email File: {0}",msgSave.ToString()),false);
            }

        }

        private MessageDisposition SendToENOLProcess(ref Message msg, FileInfo messageAsPDF)
        {
            MessageDisposition msgDisp = null;
            System.Collections.Specialized.NameValueCollection parameters = new NameValueCollection();            

            try
            {
                //Move message PDF to ENOL Queue and Archive                

                string archiveFileName = GenUtils.AppendPath(false, _archiveRootDir.FullName, DateTime.UtcNow.ToString("yyyyMMdd"),"PDFDocument", messageAsPDF.Name);
                FileInfo archiveFile = new FileInfo(archiveFileName);
                if (!archiveFile.Directory.Exists) archiveFile.Directory.Create();
                messageAsPDF.CopyTo(archiveFileName);

                string queueName = XmlUtils.GetChildNodeText(ref _mailboxNode, "@queue");
                string unProcessedFileName = GenUtils.AppendPath(false, _events.cSettings.GetParsedSetting(string.Format("ENOLQueuePath[@name='{0}']", queueName)), messageAsPDF.Name);
                FileInfo unProcessedFile = new FileInfo(unProcessedFileName);

                if (unProcessedFile.Directory.Exists)
                {
                    messageAsPDF.CopyTo(unProcessedFileName);

                    parameters.Add("FileName", messageAsPDF.Name);
                    parameters.Add("CurrentPath", unProcessedFileName);
                    parameters.Add("ClientID", XmlUtils.GetChildNodeText(ref _mailboxNode, "@clientid"));
                    parameters.Add("tmpFileNo", _msgENOLUID);
                    parameters.Add("FileReceiveDate", msg.Headers.Date);
                    parameters.Add("EmailReceiveDate", msg.Headers.Date);
                    parameters.Add("Source", DetermineSource(ref msg));
                    parameters.Add("SourceDetail", msg.Headers.From.ToString().Replace("\"", ""));
                    _dataAccess.ExecuteDatabaseTransactionNoReturn("ENOLInsertRecord", ref parameters);
                    msgDisp = new MessageDisposition() { Code = MessageDispositionCd.Success };
                }
                else
                {
                    XmlNode errorEmailNode = _events.cSettings.GetParsedNode("ENOLQueuePathErrorEmail");
                    string emailFrom=XmlUtils.GetChildNodeText(ref errorEmailNode, "@emailfrom");
                    string emailTo = XmlUtils.GetChildNodeText(ref errorEmailNode, "@emailto");
                    string emailSubject = XmlUtils.GetChildNodeText(ref errorEmailNode, "@emailsubject");
                    string emailBody = XmlUtils.GetChildNodeText(ref errorEmailNode, "@emailbody");
                    _events.SendEmail(emailFrom, emailTo, emailSubject, emailBody, false, PGW.Shared.EmailImportance.High);
                }

            }
            catch (Exception ex)
            {
                _events.HandleEvent(ex,false);
                msgDisp = new MessageDisposition(){ Code = MessageDispositionCd.Error, Details=ex.Message};
            }
            return msgDisp;

        }


    }

    public class MsgAttachments
    {
        public MsgAttachments()
        {
        }

        public void Add(FileInfo fi, FileType ft)
        {
            switch (ft)
            {
                case FileType.Image:
                    if (Images == null) Images = new List<FileInfo>();
                    Images.Add(fi);
                    break;
                case FileType.PDF:
                    if (PDFDoc == null) PDFDoc = new List<FileInfo>();
                    PDFDoc.Add(fi);
                    break;
                case FileType.Text:
                    if (TextDoc == null) TextDoc = new List<FileInfo>();
                    TextDoc.Add(fi);
                    break;
                case FileType.OfficeDoc:
                    if (OfficeDoc == null) OfficeDoc = new List<FileInfo>();
                    OfficeDoc.Add(fi);
                    break;
                default:
                    break;
            }

        }

        public bool HasAttachments
        {
            get { return (HasImages || HasOfficeDoc || HasTextDoc || HasPDFDoc); }
        }

        public bool OnlyPDForWordAttachments
        {
            get { return ((HasPDFDoc || HasOfficeDoc) && (!(HasTextDoc || HasImages))); }
        }

        public bool HasImages { get { return ((Images != null) && (Images.Count > 0)); } }
        public List<FileInfo> Images { get; set; }
        public bool HasOfficeDoc { get { return ((OfficeDoc != null) && (OfficeDoc.Count > 0)); } }
        public List<FileInfo> OfficeDoc { get; set; }
        public bool HasTextDoc { get { return ((TextDoc != null) && (TextDoc.Count > 0)); } }
        public List<FileInfo> TextDoc { get; set; }
        public bool HasPDFDoc { get { return ((PDFDoc != null) && (PDFDoc.Count > 0)); } }
        public List<FileInfo> PDFDoc { get; set; }        
    }

    public class AttachmentInfo
    {
        public string FileName;
        public FileType FileType;
    }

    public enum FileType
    {
        Image,
        Text,
        PDF,
        OfficeDoc,        
        Unsupported,
        Ignored
    }

    public class MessageDisposition
    {
        public MessageDispositionCd Code { get; set; }
        public string Details { get; set; }
        public FileInfo MessageAsPDF { get; set; }
    }

    public enum MessageDispositionCd
    {
        Success,
        Error,
        ReturnToSender,
        SendToException,
        InvalidMail
    }

}
