﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Syncfusion.DocToPDFConverter;
using Syncfusion.DocIO.DLS;
using Syncfusion.ExcelToPdfConverter;
using Syncfusion.OfficeChart;
using Syncfusion.OfficeChartToImageConverter;
using Syncfusion.Presentation;
using Syncfusion.PresentationToPdfConverter;
using Syncfusion.XlsIO;
using SynchPDF = Syncfusion.Pdf;
using SynchPDFG = Syncfusion.Pdf.Graphics;


namespace FNOLPostOffice.ENOLMXMonitor
{
    public class DocumentConverter
    {

        public static ConversionResult ConvertPowerPointToPDF(FileInfo originalFile)
        {
            ConversionResult result = new ConversionResult();
            string convertedFileName = originalFile.FullName.Replace(originalFile.Extension, ".pdf");
            SynchPDF.PdfDocument pdf = null;
            IPresentation ppt = null;

            try
            {
                ppt = Presentation.Open(originalFile.FullName);
                ppt.ChartToImageConverter = new ChartToImageConverter();
                pdf = PresentationToPdfConverter.Convert(ppt);

                pdf.Save(convertedFileName);
                result.ConvertedFile = new FileInfo(convertedFileName);
                if (!result.ConvertedFile.Exists)
                    throw new FileNotFoundException(string.Format("Converted file, {0},' not found for '{1}'.", convertedFileName, originalFile.FullName));

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            finally
            {
                if (ppt != null) ppt.Close();
                if (pdf != null) pdf.Close(true);

                ppt = null;
                pdf = null;
                
            }

            return result;

        }

        public static ConversionResult ConvertWordToPDF(FileInfo originalFile)
        {
            string convertedFileName = originalFile.FullName.Replace(originalFile.Extension, ".pdf");
            WordDocument  doc = null;
            SynchPDF.PdfDocument pdf = null;
            ConversionResult result = new ConversionResult();
            DocToPDFConverter converter = new DocToPDFConverter();

            try
            {
                doc = new WordDocument(originalFile.FullName);
                doc.ChartToImageConverter = new ChartToImageConverter();
                pdf = converter.ConvertToPDF(doc);
                pdf.Save(convertedFileName);

                result.ConvertedFile = new FileInfo(convertedFileName);
                if (!result.ConvertedFile.Exists)
                    throw new FileNotFoundException(string.Format("Converted file, {0},' not found for '{1}'.", convertedFileName, originalFile.FullName));

                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            finally
            {
                if (doc != null) doc.Close();
                if (pdf != null) pdf.Close(true);
                doc = null;
                pdf = null;
                converter = null;
            }

            return result;
        }

        public static ConversionResult ConvertExcelToPDF(FileInfo originalFile)
        {
            ConversionResult result = new ConversionResult();
            result.ConvertedFile = new FileInfo(originalFile.FullName.Replace(originalFile.Extension, ".pdf"));
            ExcelToPdfConverter converter = null;
            SynchPDF.PdfDocument pdf = null;
            try
            {
                converter = new ExcelToPdfConverter(originalFile.FullName);
                pdf = new SynchPDF.PdfDocument();
                //Intialize the ExcelToPdfConverterSettings class
                ExcelToPdfConverterSettings settings = new ExcelToPdfConverterSettings();
                settings.LayoutOptions = LayoutOptions.FitAllColumnsOnOnePage;

                settings.TemplateDocument = pdf;
                settings.DisplayGridLines = GridLinesDisplayStyle.Visible;

                pdf = converter.Convert(settings);

                pdf.Save(result.ConvertedFile.FullName);

                if (!result.ConvertedFile.Exists)
                    throw new FileNotFoundException(string.Format("Converted file, {0},' not found for '{1}'.", result.ConvertedFile.FullName, originalFile.FullName));
                result.Success = true;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            finally
            {
                if (pdf != null) pdf.Close(true);
                pdf = null;
                converter = null;
            }

            return result;
            
        }


        public class ConversionResult
        {

            internal ConversionResult() {}

            public bool Success { get; internal set; }
            public FileInfo ConvertedFile { get; internal set; }
            public Exception Exception { get; internal set; }
        }

    }

    

}
