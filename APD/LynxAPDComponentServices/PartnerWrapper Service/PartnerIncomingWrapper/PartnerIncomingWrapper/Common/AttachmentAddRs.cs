﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;


namespace PartnerIncomingWrapper.Common
{
    [Serializable()]
    [XmlRoot("AttachmentAddRs")]
    public class AttachmentAddRs
    {
        [XmlElement("RqUID", Namespace = "http://www.cieca.com/BMS")]
        public string strRqUID { get; set; }

        [XmlElement("AsyncRqUID", Namespace = "http://www.cieca.com/BMS")]
        public string strAsyncRqUID { get; set; }

        [XmlElement("PartnerKey", Namespace = "http://www.cieca.com/BMS")]
        public string strPartnerKey { get; set; }
              
        
        private ApplicaitonStatus objApplicationStatus = new ApplicaitonStatus();

        [XmlElement("ApplicaitonStatus", Namespace = "http://www.cieca.com/BMS")]
        public ApplicaitonStatus ApplicaitonStatus
        {
            get { return objApplicationStatus; }
            set { objApplicationStatus = value; }
        }
    }

    public class ApplicaitonStatus
    {
        private string strApplicationStatusCode;

        [XmlElement("ApplicationStatusCode")]
        public string ApplicationStatusCode
        {
            get { return strApplicationStatusCode; }
            set { strApplicationStatusCode = value; }
        }

        private ApplicationError objApplicationError = new ApplicationError();

        [XmlElement("ApplicationError")]
        public ApplicationError applicationError
        {
            get { return objApplicationError; }
            set { objApplicationError = value; }
        }
               
    }

    public class ApplicationError
    {
        private string strErrorCode;
        [XmlElement("ErrorCode")]
        public string ErrorCode
        {
            get { return strErrorCode; }
            set { strErrorCode = value; }
        }

        public bool ShouldSerializeErrorCode()
        {
            return !(strErrorCode == null || strErrorCode.Length <= 0);
        }

        private string strSeverity;
        [XmlElement("Severity")]
        public string Severity
        {
            get { return strSeverity; }
            set { strSeverity = value; }
        }

        public bool ShouldSerializeSeverity()
        {
            return !(strSeverity == null || strSeverity.Length <= 0);
        }

        private string strErrorDesc;
        [XmlElement("ErrorDesc")]
        public string ErrorDesc
        {
            get { return strErrorDesc; }
            set { strErrorDesc = value; }
        }

        public bool ShouldSerializeErrorDesc()
        {
            return !(strErrorDesc == null || strErrorDesc.Length <= 0);
        }
    }

}