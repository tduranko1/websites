﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace PartnerIncomingWrapper.Common
{
    public class WriteLog
    {
        /// <summary>
        /// Write log functionality
        /// </summary>
        /// <param name="strEventType"></param>
        /// <param name="strEventStatus"></param>
        /// <param name="strEventDescription"></param>
        /// <param name="strEventDetailedDescritpion"></param>
        /// <param name="strEventXML"></param>
        //public void LogEventPartner(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        //{
        //    string strConnectionString = string.Empty, strProcName = string.Empty, strError = string.Empty;
        //    //long lngSPreturnID = 0;
        //    try
        //    {
        //        strConnectionString = GetConnectionString("Partner");
        //        strProcName = "uspPartnerEventInsLogEntry";

        //        using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
        //        {
        //            using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
        //            {
        //                SqlParameter sqlReturnParm = new SqlParameter("@iRecID", SqlDbType.BigInt);
        //                sqlReturnParm.Direction = ParameterDirection.Output;
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.Add(sqlReturnParm);
        //                command.Parameters.AddWithValue("@vEventTransactionID", Guid.NewGuid());
        //                command.Parameters.AddWithValue("@vEventType", strEventType);
        //                command.Parameters.AddWithValue("@vEventStatus", strEventStatus);
        //                command.Parameters.AddWithValue("@vEventDescription", strEventDescription);
        //                command.Parameters.AddWithValue("@vEventDetailedDescription", strEventDetailedDescritpion);
        //                command.Parameters.AddWithValue("@vEventXML", strEventXML);

        //                sqlConnection.Open();
        //                command.ExecuteNonQuery();
        //                //lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);

        //                //if (lngSPreturnID < 0)
        //                // throw new Exception("LynxAPDComponent Service Failed: Event logging failed to insert a row (LogEvent)...");
        //            }
        //        }

        //        //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
        //        //DevwsAPDFoundation.LogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
        //    }
        //    catch (Exception e)
        //    {
        //        //Send(ex, string.Concat("EventDescription: ", strEventDescription, " EventDetailedDescription: ", strEventDetailedDescritpion, " EventXML: ", strEventXML), "Error: LynxAPDComponentServices");
        //    }
        //    finally
        //    {
        //        // if(sql)
        //    }
        //}

        //public void LogEvent(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        //{
        //    string strConnectionString = string.Empty, strProcName = string.Empty, strError = string.Empty;
        //    long lngSPreturnID = 0;
        //    try
        //    {
        //        strConnectionString = GetConnectionString("APD");
        //        strProcName = "uspAPDEventInsLogEntry";

        //        using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
        //        {
        //            using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
        //            {
        //                SqlParameter sqlReturnParm = new SqlParameter("@iRecID", SqlDbType.BigInt);
        //                sqlReturnParm.Direction = ParameterDirection.Output;
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.Add(sqlReturnParm);
        //                command.Parameters.AddWithValue("@vEventType", strEventType);
        //                command.Parameters.AddWithValue("@vEventStatus", strEventStatus);
        //                command.Parameters.AddWithValue("@vEventDescription", strEventDescription);
        //                command.Parameters.AddWithValue("@vEventDetailedDescription", strEventDetailedDescritpion);
        //                command.Parameters.AddWithValue("@vEventXML", strEventXML);

        //                sqlConnection.Open();
        //                command.ExecuteNonQuery();
        //                lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);

        //                if (lngSPreturnID < 0)
        //                    throw new Exception("LynxAPDComponent Service Failed: Event logging failed to insert a row (LogEvent)...");
        //            }
        //        }

        //        //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
        //        //DevwsAPDFoundation.LogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //}

        ///// <summary>
        ///// Get DB connection string from Config
        ///// </summary>
        ///// <param name="ConnectionType"></param>
        ///// <returns></returns>
        //public string GetConnectionString(string ConnectionType)
        //{
        //    string returnConnectionString = string.Empty;

        //    if (ConnectionType == "APD")
        //    {
        //        switch (PGW.Business.Environment.getEnvironment())
        //        {
        //            case "DEV":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
        //                break;
        //            case "STG":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_STG"];
        //                break;
        //            case "PRD":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_PROD"];
        //                break;
        //            default:
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
        //                break;
        //        }
        //    }
        //    else if (ConnectionType == "Partner")
        //    {
        //        switch (PGW.Business.Environment.getEnvironment())
        //        {
        //            case "DEV":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
        //                break;
        //            case "STG":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_STG"];
        //                break;
        //            case "PRD":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_PROD"];
        //                break;
        //            default:
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
        //                break;
        //        }

        //    }
        //    else if (ConnectionType == "FNOL")
        //    {
        //        switch (PGW.Business.Environment.getEnvironment())
        //        {
        //            case "DEV":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
        //                break;
        //            case "STG":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_STG"];
        //                break;
        //            case "PRD":
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_PROD"];
        //                break;
        //            default:
        //                returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
        //                break;
        //        }

        //    }

        //    return returnConnectionString;
        //}
    }
}