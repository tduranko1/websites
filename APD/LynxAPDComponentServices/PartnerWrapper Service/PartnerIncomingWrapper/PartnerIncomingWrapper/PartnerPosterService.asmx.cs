﻿using System;
using System.Web.Services;
using System.Xml;
using System.Configuration;
using System.Net;
using System.IO;
using System.Reflection;
using PartnerIncomingWrapper.Common;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using System.Web.Services.Protocols;
using System.Security.Cryptography;
using System.Text;
using PartnerIncomingWrapper.AuthorizationWebService;
using System.Collections;

namespace PartnerIncomingWrapper
{
    /// <summary>
    /// Summary description for PartnerPosterService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PartnerPosterService : System.Web.Services.WebService
    {
        public bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        public AuthHeader Authenticate;
        public string _UserName = string.Empty,
                    _Password = string.Empty;

        string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }

        /// <summary>
        /// Function to check the service test
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string CheckService()
        {
            return string.Concat("Working Good ", DateTime.Now);
        }

        /// <summary>
        /// Push the Transaction Xml to PushUrl
        /// </summary>
        /// <param name="strPushURL"></param>
        /// <param name="strRequestXML"></param>
        /// <param name="strResponse"></param>
        /// <param name="strCertName"></param>
        /// <param name="strUN"></param>
        /// <param name="strPW"></param>
        /// <param name="strSoapAction"></param>
        /// <param name="strUN1"></param>
        /// <param name="strPW1"></param>
        /// <returns></returns>
        [WebMethod]
        public bool PushStringToURL(string strPushURL, string strRequestXML, string strResponse, string strCertName, string strUN, string strPW, string strSoapAction)
        {
            string strResponseData = string.Empty;
            bool blnPush = false;
            object objPush = null;
            Type clsPush = null;

            try
            {
                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - PushStringToURL()", "START", "");

                if (!string.IsNullOrEmpty(strUN))
                    strUN = strUN.Trim();
                else
                    strUN = string.Empty;

                if (!string.IsNullOrEmpty(strPW))
                    strPW = strPW.Trim();
                else
                    strPW = string.Empty;

                if (!string.IsNullOrEmpty(strSoapAction))
                    strSoapAction = strSoapAction.Trim();
                else
                    strSoapAction = string.Empty;

                clsPush = Type.GetTypeFromProgID("GLAXISPush.clsPush");
                objPush = Activator.CreateInstance(clsPush);
                blnPush = (bool)clsPush.InvokeMember("PushStringToURL", BindingFlags.InvokeMethod, null, objPush, new string[] { strPushURL, strRequestXML, "", strCertName, strUN, strPW, strSoapAction });

                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - PushStringToURL()", "Push result: " + blnPush.ToString(), "");

                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - PushStringToURL()", "END", "");
            }
            catch (Exception Ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - PushStringToURL()", Ex.Message, "");
            }
            return blnPush;
        }

        /// <summary>
        /// Receive the request from HQ side
        /// Submit the HyperQuest Incoming Xml to PartnerData WCF service to process
        /// </summary>
        /// <param name="strIncomingRequest"></param>
        /// <returns></returns>
        [SoapHeader("Authenticate")]
        [WebMethod]
        public AttachmentAddRs SubmitEstimate(string strIncomingRequest)
        {
            XmlDocument xmlTransaction = null, xmlResponse = null;
            AttachmentAddRs objAttachementResonse = null;
            XmlNode nodeTemp = null;
            StringBuilder strErrorXml = new StringBuilder();

            string strRqUID = string.Empty,
                strAsyncRqUID = string.Empty,
                strPartnerKey = string.Empty,
                strErrCode = string.Empty,
                strErrMessage = string.Empty,
                strResponse = string.Empty;

            try
            {
                if (isDebug)
                {
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", "START", "");
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", "Request Xml:", strIncomingRequest);
                }

                xmlTransaction = xmlResponse = new XmlDocument();

                if (!string.IsNullOrEmpty(strIncomingRequest))
                    xmlTransaction.LoadXml(strIncomingRequest);
                else
                    throw new Exception("Request is null or empty");

                nodeTemp = xmlTransaction.SelectSingleNode("//RqUID");
                if (nodeTemp != null)
                    strRqUID = nodeTemp.InnerText;

                nodeTemp = xmlTransaction.SelectSingleNode("//AsyncRqUID");
                if (nodeTemp != null)
                    strAsyncRqUID = nodeTemp.InnerText;

                nodeTemp = xmlTransaction.SelectSingleNode("//PartnerKey");
                if (nodeTemp != null)
                    strPartnerKey = nodeTemp.InnerText;


                UserName = Authenticate.UserName.Trim();
                Password = Authenticate.Password.Trim();
                //LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", string.Concat("GetCurrentMethod:", MethodBase.GetCurrentMethod().DeclaringType.FullName, ",", MethodBase.GetCurrentMethod().Name), "");
                // LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", string.Concat("username:",Authenticate.UserName), "");
                //LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", string.Concat("username:", Authenticate.Password), "");

                if (!CheckAuthorization(MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name))
                    throw new Exception("Authentication failed");
                else
                {
                    strResponse = SendToPartner("http://LynxAPDComponentServices/IPartnerData/ProcessHqRequest", "<ProcessHqRequest xmlns=\"http://LynxAPDComponentServices\"><strxml>" + "<![CDATA[" + strIncomingRequest + "]]>" + "</strxml></ProcessHqRequest>");
                }

                xmlResponse.LoadXml(strResponse);
                objAttachementResonse = GetResponse(xmlResponse);

                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SubmitEstimate()", "END", "");
            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - SubmitEstimate()", ex.Message, "");
                strErrorXml = new StringBuilder();
                strResponse = string.Empty;

                if (ex.Message == "Authentication failed")
                {
                    strErrCode = "403";
                    strErrMessage = "Authentication failed";
                }
                else
                {
                    strErrCode = "000";
                    strErrMessage = "An internal error occured while processing the request. Please contact LYNX";
                }

                strErrorXml.Append("<AttachmentAddRs xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                strErrorXml.Append("<RqUID>" + strRqUID + "</RqUID>");
                strErrorXml.Append("<AsyncRqUID>" + strAsyncRqUID + "</AsyncRqUID>");
                strErrorXml.Append("<PartnerKey>" + strPartnerKey + "</PartnerKey>");
                strErrorXml.Append("<ApplicationStatus>");
                strErrorXml.Append("<ApplicationStatusCode>Reject</ApplicationStatusCode>");
                strErrorXml.Append("<ApplicationError><ErrorCode>" + strErrCode + "</ErrorCode><Severity>Error</Severity><ErrorDesc>" + strErrMessage + "</ErrorDesc></ApplicationError>");
                strErrorXml.Append("</ApplicationStatus>");
                strErrorXml.Append("</AttachmentAddRs>");

                strResponse = strErrorXml.ToString();
                xmlResponse.LoadXml(strResponse);
                objAttachementResonse = GetResponse(xmlResponse);
            }
            finally
            {
                strErrorXml = null;
                xmlTransaction = null;
                nodeTemp = null;
            }

            LogEvent("PartnerIncomingWrapper", "Debugging", "PartnerPosterService.asmx.cs - SubmitEstimate()", "Return Xml Value:", strResponse);
            return objAttachementResonse;
        }

        [SoapHeader("Authenticate")]
        [WebMethod]
        public string SendWorkflowNotification(string PassthruData, string EventID, string Description)
        {
            StringBuilder strErrorXml;
            string strResponse = string.Empty,
                strErrCode = string.Empty,
                strErrMessage = string.Empty,
                strPassthruData = string.Empty,
                strEventID = string.Empty;

            try
            {
                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SendWorkflowNotification()", "START", "");

                UserName = Authenticate.UserName.Trim();
                Password = Authenticate.Password.Trim();

                if (!CheckAuthorization(MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name))
                    throw new Exception("Authentication failed");
                else
                {
                    if (string.IsNullOrEmpty(PassthruData))
                        throw new Exception("Err-001");

                    if (string.IsNullOrEmpty(EventID))
                        throw new Exception("Err-002");

                    if (Description.Length > 500)
                    {
                        Description = Description.Substring(0, 497);
                        Description = Description.Substring(0, Description.LastIndexOf(" "));
                        Description = Description + "...";
                    }

                    strResponse = SendToPartner("http://LynxAPDComponentServices/IPartnerData/SendWorkflowNotification", "<SendWorkflowNotification xmlns=\"http://LynxAPDComponentServices\"><PassthruData>" + PassthruData + "</PassthruData><EventID>" + EventID + "</EventID><Description><![CDATA[" + Description + "]]></Description></SendWorkflowNotification>");
                }

                if (isDebug)
                    LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SendWorkflowNotification()", "END", "");

            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - SendWorkflowNotification()", ex.Message, "");
                strErrorXml = new StringBuilder();
                strResponse = string.Empty;
                Hashtable htErrorCode = new Hashtable();

                htErrorCode.Add("Err-001", "PassthruData doesn't exist in the request.");
                htErrorCode.Add("Err-002", "Event ID doesn't exist in the request");

                if (ex.Message == "Authentication failed")
                {
                    strErrCode = "403";
                    strErrMessage = "Authentication failed";
                }
                else if (ex.Message.Contains("Err-"))
                {
                    strErrCode = ex.Message;
                    strErrMessage = htErrorCode[ex.Message].ToString();
                }
                else
                {
                    strErrCode = "000";
                    strErrMessage = "An internal error occured while processing the request. Please contact LYNX";
                }

                strErrorXml.Append("<WorflowNotifyRs xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                strErrorXml.Append("<PassthruData>" + PassthruData + "</PassthruData>");
                strErrorXml.Append("<EventID>" + EventID + "</EventID>");
                strErrorXml.Append("<strstrDescription>" + Description + "</strstrDescription>");
                strErrorXml.Append("<ApplicationStatus>");
                strErrorXml.Append("<ApplicationStatusCode>Reject</ApplicationStatusCode>");
                strErrorXml.Append("<ApplicationError><ErrorCode>" + strErrCode + "</ErrorCode><Severity>Error</Severity><ErrorDesc>" + strErrMessage + "</ErrorDesc></ApplicationError>");
                strErrorXml.Append("</ApplicationStatus>");
                strErrorXml.Append("</WorflowNotifyRs>");

                strResponse = strErrorXml.ToString();
            }
            LogEvent("PartnerIncomingWrapper", "Debugging", "PartnerPosterService.asmx.cs - SendWorkflowNotification()", "Return Xml Value:", strResponse);
            return strResponse;
        }

        /// <summary>
        /// Authentication check process
        /// </summary>
        /// <param name="strWebServiceName"></param>
        /// <param name="strWebServiceMethod"></param>
        /// <returns></returns>
        private bool CheckAuthorization(string strWebServiceName, string strWebServiceMethod)
        {
            AuthorizationServiceClient authService;
            bool blnReturn = false;
            try
            {
                authService = GetAuthorizationServiceInstance();

                /* Check if authentication is turned on for this service */
                if (authService.CheckAuthentication(strWebServiceName, strWebServiceMethod))
                {
                    /* Validate the service credential by AWARE. */
                    if (Login(UserName, Password))
                    {
                        /* The user has access to the service but can they access this particular method? */
                        if (authService.CheckAuthorization(strWebServiceName, strWebServiceMethod, UserName))
                            blnReturn = true;
                    }
                }

            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - CheckAuthorization()", ex.Message, "");
                throw ex;
            }

            return blnReturn;
        }

        /// <summary>
        /// Validate the user credential
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private bool Login(string userName, string password)
        {
            AWARE.clsAWARE aware = new AWARE.clsAWARE();
            short result = 0;
            bool blnReturn = false;

            try
            {
                result = (short)aware.Authenticate(userName, password);

                LogEvent("PartnerIncomingWrapper", "DEBUGGING", "PartnerPosterService.asmx.cs - Login()", "Result:", result.ToString());
                if (result == 400)
                    blnReturn = true;

            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - Login()", ex.Message, "");
            }

            return blnReturn;
        }

        /// <summary>
        /// Instance creation for Authorization webservice
        /// </summary>
        /// <returns></returns>
        private AuthorizationServiceClient GetAuthorizationServiceInstance()
        {
            try
            {
                AuthorizationServiceClient authorizationService = new AuthorizationServiceClient();

                authorizationService.Endpoint.Address = new System.ServiceModel.EndpointAddress(GetAuthorizationUrl());
                return authorizationService;
               
            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - GetAuthorizationServiceInstance()", ex.Message, "");
                throw ex;
            }

        }

        /// <summary>
        /// get the Authorization webservice URL
        /// </summary>
        /// <returns></returns>
        private string GetAuthorizationUrl()
        {
            string strAuthorizationUrl = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        strAuthorizationUrl = Convert.ToString(ConfigurationManager.AppSettings["AuthorizationUrl_DEV"]);
                        break;
                    case "STG":
                        strAuthorizationUrl = Convert.ToString(ConfigurationManager.AppSettings["AuthorizationUrl_STG"]);
                        break;
                    case "PRD":
                        strAuthorizationUrl = Convert.ToString(ConfigurationManager.AppSettings["AuthorizationUrl_PRD"]);
                        break;
                    case "DR":
                        strAuthorizationUrl = Convert.ToString(ConfigurationManager.AppSettings["AuthorizationUrl_DR"]);
                        break;
                    default:
                        strAuthorizationUrl = Convert.ToString(ConfigurationManager.AppSettings["AuthorizationUrl_DEV"]);
                        break;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return strAuthorizationUrl;
        }

        /// <summary>
        /// Encrypt the credential
        /// </summary>
        /// <param name="strClearText"></param>
        /// <returns></returns>
        private string Encrypt(string strClearText)
        {
            string strEncryptText = string.Empty,
                strEncryptionKey = "LYNX";
            byte[] clearBytes;
            Rfc2898DeriveBytes pdb;
            try
            {
                using (Aes encryptor = Aes.Create())
                {
                    clearBytes = Encoding.Unicode.GetBytes(strClearText);
                    pdb = new Rfc2898DeriveBytes(strEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        strEncryptText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception Ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - Encrypt()", Ex.Message, "");
                throw Ex;
            }

            return strEncryptText;
        }

        /// <summary>
        /// Decrypt the credential value
        /// </summary>
        /// <param name="strEncryptText"></param>
        /// <returns></returns>
        private string Decrypt(string strEncryptText)
        {
            string strDecrypt = string.Empty,
                strEncryptionKey = ConfigurationManager.AppSettings["AuthorizationKey"].ToString();
            byte[] cipherBytes;
            Rfc2898DeriveBytes pdb;
            try
            {
                strDecrypt = strEncryptText.Replace(" ", "+");
                cipherBytes = Convert.FromBase64String(strDecrypt);
                using (Aes encryptor = Aes.Create())
                {
                    pdb = new Rfc2898DeriveBytes(strEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        strDecrypt = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - Decrypt()", ex.Message, "");
                throw ex;
            }

            return strDecrypt;
        }

        /// <summary>
        /// Send Request to PartnerData WCF service
        /// </summary>
        /// <param name="strIncomingXml"></param>
        /// <returns></returns>
        private string SendToPartner(string strSoapAction, string strArgs)
        {
            string strPartnerServiceURL = string.Empty, strXmlParameters = string.Empty, strResponseData = string.Empty, strReturnData = string.Empty;
            XmlDocument xmlRequest = null, xmlResponse = null;
            HttpWebRequest webRequest;
            Stream responseStream;
            WebResponse response;
            try
            {
                xmlResponse = new XmlDocument();
                strPartnerServiceURL = GetPartnerServiceURL();
              // strPartnerServiceURL = "http://localhost:55052/PartnerData.svc";

                //Handle for HTTPWebRequest Issue (Underlying Connection CLosed)
                System.Net.ServicePointManager.DefaultConnectionLimit = 1000;
                System.Net.ServicePointManager.Expect100Continue = false;

                webRequest = (HttpWebRequest)WebRequest.Create(strPartnerServiceURL);
                strXmlParameters = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header></s:Header><s:Body>" + strArgs + "</s:Body></s:Envelope>";
                xmlRequest = new XmlDocument();
                xmlRequest.LoadXml(strXmlParameters);
                webRequest.Method = "POST";
                webRequest.Timeout = 180000;
                webRequest.ReadWriteTimeout = 12000;
                webRequest.ContentType = "text/xml;charset=utf-8";
                webRequest.KeepAlive = false;
                //webRequest.Proxy = null;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.Headers.Add("SOAPAction", strSoapAction);

                responseStream = webRequest.GetRequestStream();
                //xmlRequest.PreserveWhitespace = false;
                xmlRequest.Save(responseStream);
                
                responseStream.Close();
                response = webRequest.GetResponse();

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    strResponseData = Convert.ToString(streamReader.ReadToEnd());
                }

                if (!string.IsNullOrEmpty(strResponseData.Trim()))
                {
                    xmlResponse.LoadXml(Server.HtmlDecode(strResponseData));
                    if (xmlResponse.GetElementsByTagName("AttachmentAddRs")[0] != null)
                    {
                        strReturnData = xmlResponse.GetElementsByTagName("AttachmentAddRs")[0].OuterXml;
                    }
                    else if (xmlResponse.GetElementsByTagName("WorflowNotifyRs")[0] != null)
                    {
                        strReturnData = xmlResponse.GetElementsByTagName("WorflowNotifyRs")[0].OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlRequest = null;
            }
            return strReturnData;
        }

        /// <summary>
        /// Get partner service url
        /// </summary>
        /// <returns></returns>
        private string GetPartnerServiceURL()
        {
            string strServiceUrl = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        strServiceUrl = ConfigurationManager.AppSettings["DevPartner"];
                        break;
                    case "STG":
                        strServiceUrl = ConfigurationManager.AppSettings["STGPartner"];
                        break;
                    case "PRD":
                    case "PROD":
                        strServiceUrl = ConfigurationManager.AppSettings["PRDPartner"];
                        break;
                    default:
                        strServiceUrl = ConfigurationManager.AppSettings["DevPartner"];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strServiceUrl;
        }

        /// <summary>
        /// Log the details into DB
        /// </summary>
        /// <param name="strEventType"></param>
        /// <param name="strEventStatus"></param>
        /// <param name="strEventDescription"></param>
        /// <param name="strEventDetailedDescritpion"></param>
        /// <param name="strEventXML"></param>
        /// <returns></returns>
        private long LogEvent(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        {
            long lngReturn = 0;
            string strLogArgs = string.Empty;
            try
            {
                strLogArgs = "<WriteLog xmlns=\"http://LynxAPDComponentServices\"><strEventType>" + strEventType + "</strEventType><strEventStatus>" + strEventStatus + "</strEventStatus><strEventDescription>" + strEventDescription + "</strEventDescription><strEventDetailedDescritpion>" + strEventDetailedDescritpion + "</strEventDetailedDescritpion><strEventXML>" + "<![CDATA[" + strEventXML + "]]>" + "</strEventXML></WriteLog>";
                SendToPartner("http://LynxAPDComponentServices/IPartnerData/WriteLog", strLogArgs);
            }
            catch (Exception)
            {
                // throw ex;
            }
            return lngReturn;
        }

        /// <summary>
        /// Generate response as Stronly Type Object
        /// </summary>
        /// <param name="xmlResponseDoc"></param>
        /// <returns></returns>     
        private AttachmentAddRs GetResponse(XmlDocument xmlResponseDoc)
        {
            AttachmentAddRs objAttachmentRes = null;
            try
            {
                objAttachmentRes = new AttachmentAddRs();
                xmlResponseDoc = RemoveXmlns(xmlResponseDoc.OuterXml);
                objAttachmentRes.strRqUID = xmlResponseDoc.SelectSingleNode("//RqUID").InnerText;
                objAttachmentRes.strAsyncRqUID = xmlResponseDoc.SelectSingleNode("//AsyncRqUID").InnerText;
                objAttachmentRes.strPartnerKey = xmlResponseDoc.SelectSingleNode("//PartnerKey").InnerText;

                if (xmlResponseDoc.SelectSingleNode("//ApplicationStatusCode") != null)
                    objAttachmentRes.ApplicaitonStatus.ApplicationStatusCode = xmlResponseDoc.SelectSingleNode("//ApplicationStatusCode").InnerText;
                else
                    objAttachmentRes.ApplicaitonStatus.ApplicationStatusCode = string.Empty;

                if (xmlResponseDoc.SelectSingleNode("//ErrorCode") != null)
                    objAttachmentRes.ApplicaitonStatus.applicationError.ErrorCode = xmlResponseDoc.SelectSingleNode("//ErrorCode").InnerText;
                else
                    objAttachmentRes.ApplicaitonStatus.applicationError.ErrorCode = string.Empty;

                if (xmlResponseDoc.SelectSingleNode("//Severity") != null)
                    objAttachmentRes.ApplicaitonStatus.applicationError.Severity = xmlResponseDoc.SelectSingleNode("//Severity").InnerText;
                else
                    objAttachmentRes.ApplicaitonStatus.applicationError.Severity = string.Empty;

                if (xmlResponseDoc.SelectSingleNode("//ErrorDesc") != null)
                    objAttachmentRes.ApplicaitonStatus.applicationError.ErrorDesc = xmlResponseDoc.SelectSingleNode("//ErrorDesc").InnerText;
                else
                    objAttachmentRes.ApplicaitonStatus.applicationError.ErrorDesc = string.Empty;

            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - GetResponse()", ex.Message, "");
                throw ex;
            }
            return objAttachmentRes;
        }

        /// <summary>
        /// Remove the xmlns namespace
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private XmlDocument RemoveXmlns(String xml)
        {
            var xmlDocument = new XmlDocument();
            XDocument d;
            try
            {
                d = XDocument.Parse(xml);
                d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

                foreach (var elem in d.Descendants())
                    elem.Name = elem.Name.LocalName;

                xmlDocument.Load(d.CreateReader());
            }
            catch (Exception ex)
            {
                LogEvent("PartnerIncomingWrapper", "ERROR", "PartnerPosterService.asmx.cs - RemoveXmlns()", ex.Message, "");
                throw ex;
            }
            return xmlDocument;
        }

    }

    /// <summary>
    /// Basic Authenticate class 
    /// </summary>
    public class AuthHeader : SoapHeader
    {
        public string UserName;
        public string Password;
    }
}
