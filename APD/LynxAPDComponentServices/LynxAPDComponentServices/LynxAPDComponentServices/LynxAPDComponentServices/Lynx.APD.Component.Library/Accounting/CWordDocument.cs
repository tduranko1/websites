﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.Xml;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Lynx.APD.Component.Library.Common;

namespace Lynx.APD.Component.Library.Accounting
{
    class CWordDocument
    {
        string MODULE_NAME = "LAPDAccounting.CWordDocument.";
        Application mobjWord = null;
        Document mobjDoc = null;
        MLAPDAccounting mLAPDAccounting = null;
        MDomUtils mDomUtils = null;

        /// <summary>
        /// Class clean-up.
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                CloseDocument();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Returns the MS Word application object.
        /// </summary>
        public Document Document
        {
            get
            {
                if (mobjWord != null)
                    mobjDoc.Activate();

                return mobjDoc;
            }
        }

        /// <summary>
        /// Creates and initializes a MS Word application object.
        /// </summary>
        /// <param name="strTemplateFile"></param>
        private void LoadFromTemplate(string strTemplateFile)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "LoadFromTemplate: ");
            mLAPDAccounting = new MLAPDAccounting();
            try
            {             

                //Close any existing document.
                CloseDocument();

                //Create the MS Word object.
                mobjWord = new Application();

                //Initialize the MS Word object.
                mobjWord.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                //mobjWord.FeatureInstall = null;//msoFeatureInstallNone
                mobjWord.PrintPreview = false;
                mobjWord.Visible = false;

                mobjWord.Application.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                // mobjWord.Application.FeatureInstall = null;//'msoFeatureInstallNone
                mobjWord.Application.PrintPreview = false;
                mobjWord.Application.Visible = false;

                mobjDoc = mobjWord.Documents.Open(strTemplateFile, false, true, false, "", "", true);            

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Prints the word document out to a file.
        /// </summary>
        /// <param name="strFaxPrinter"></param>
        private void PrintToFile(string strFaxPrinter, string strFile)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "PrintToFile: ");
            try
            {
                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace(string.Concat(strFaxPrinter, " : ", strFile), string.Concat(PROC_NAME, "Begin Print"));

                Process[] ps = Process.GetProcessesByName(strFile);

                foreach (Process kp in ps)
                    kp.Kill();

                mobjWord.ActivePrinter = strFaxPrinter;

                mobjDoc.Activate();
                mobjDoc.PrintOut(false, false, strFile, true);

                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace(string.Concat(strFaxPrinter, " : ", strFile), string.Concat(PROC_NAME, "Print Complete"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Prints the word document out to a file.
        /// </summary>
        /// <param name="strFile"></param>
        public void SaveToFile(string strFile)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "SaveToFile: ");
            try
            {
                mobjDoc.SaveAs(strFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Turns on document protection.
        /// </summary>
        public void ProtectDocument()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ProtectDocument: ");
            try
            {
                mobjDoc.Protect(WdProtectionType.wdAllowOnlyFormFields);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Closes up the word document
        /// </summary>
        public void CloseDocument()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CloseDocument: ");
            try
            {
                if (mobjDoc != null)
                {
                    mobjDoc.Close(WdSaveOptions.wdDoNotSaveChanges);
                    mobjDoc = null;
                }
                if (mobjWord != null)
                {
                    mobjWord.Quit(WdSaveOptions.wdDoNotSaveChanges);
                    mobjWord = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Loop through every bookmark and insert values from the converted
        /// XML that match up with the bookmark name.
        /// </summary>
        /// <param name="objXmlDoc"></param>
        public void LoadBookmarksFromXML(ref MSXML2.DOMDocument40 objXmlDoc, bool blnForceMatch = false)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "LoadBookmarksFromXML: ");
            string strName = string.Empty;
            bool blnFound = false;
            int intIdx = 0;
            try
            {
                mDomUtils = new MDomUtils();

                foreach (Bookmark objBM in mobjWord.ActiveDocument.Bookmarks)
                {
                    objBM.Select();
                    strName = objBM.Name;
                    blnFound = false;

                    //'Double underscores indicate copies of a data
                    intIdx = strName.IndexOf('_', 1);

                    if (intIdx > 0)  //remove anything after the double underscore.
                        strName = Left(strName, intIdx - 1);


                    //Underscores allow parent specification, i.e. Carrier_FirstName ==> //Carrier/@FirstName
                    if (strName.IndexOf('_', 1) > 0)
                    {
                        for (intIdx = strName.Length; intIdx >= 1; intIdx--)
                        {
                            if (Mid(strName, intIdx, 1) == "'_")
                            {
                                blnFound = true;
                                strName = Left(strName, intIdx - 1) + strName.Replace("_", "/@");
                                strName = "//" + strName.Replace('_', '/');
                            }
                        }
                    }
                    //Or just find any matching attribute, i.e. InsuredFirstName ==> //@InsuredFirstName
                    if (!blnFound)
                        strName = string.Concat("//@", strName);

                    mobjWord.Selection.InsertAfter(objXmlDoc.selectSingleNode(strName).text);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///Adds rows to a pre-defined table and populates the cells with attribute valuse from the indicated nodelist.
        /// </summary>
        public void CreateLineItemsFromXML(XmlDocument objDataDom, string strCellsXml, string strLineItemXPath, int intLineItemTable)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CreateLineItemsFromXML: ");
            XmlDocument objXMLCells = null;
            XMLNode tmpXmlnode = null;

            try
            {  //Add parameters to trace data.
                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace(string.Concat("Params: ", Environment.NewLine, "strCellsXml=", strCellsXml, Environment.NewLine, "strLineItemXPath = ", strLineItemXPath, Environment.NewLine, "intLineItemTable = ", intLineItemTable), string.Concat(PROC_NAME, "Started"));

                //Validate our parameters.

                mLAPDAccounting.g_objEvents.Assert(Convert.ToBoolean(strCellsXml != ""), "No XML passed.");
                mLAPDAccounting.g_objEvents.Assert(Convert.ToBoolean(strLineItemXPath != ""), "No XPath passed.");
                mLAPDAccounting.g_objEvents.Assert(Convert.ToBoolean(intLineItemTable > 0), "Invalid table number passed.");

                mDomUtils.LoadXml(ref objXMLCells, ref strCellsXml, PROC_NAME, "Table Column Headings");

                //Loop through each node in the node list indicated by the strLineItemXPath param.
                foreach (XmlNode objNode in objDataDom.SelectNodes(strLineItemXPath))
                {
                    //Add a new row to the end of the line item table
                    mobjWord.Documents[1].Tables[intLineItemTable].Rows.Add();

                    //Loop through each cell in the newly added row and populate the cell with the proper attribute value from the line item node
                    foreach (Microsoft.Office.Interop.Word.Cell objCell in mobjWord.Documents[1].Tables[intLineItemTable].Rows[mobjWord.Documents[1].Tables[intLineItemTable].Rows.Count].Cells)
                    {
                        objCell.Select();
                        tmpXmlnode = (XMLNode)objNode;
                        mobjWord.Selection.InsertAfter(objDataDom.SelectSingleNode(string.Concat("@", objXMLCells.DocumentElement.Attributes[objCell.ColumnIndex - 1].InnerText)).InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objXMLCells = null;

            }
        }
        public string Left(string param, int length)
        {
            try
            {
                string strLeft = param.Substring(0, length);
                return strLeft;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Right(string param, int length)
        {
            try
            {
                string strRight = param.Substring(param.Length - length, length);
                return strRight;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Mid(string param, int startIndex, int length)
        {
            try
            {
                string strMid = param.Substring(startIndex, length);
                return strMid;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Mid(string param, int startIndex)
        {
            try
            {
                string strMid = param.Substring(startIndex);
                return strMid;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
