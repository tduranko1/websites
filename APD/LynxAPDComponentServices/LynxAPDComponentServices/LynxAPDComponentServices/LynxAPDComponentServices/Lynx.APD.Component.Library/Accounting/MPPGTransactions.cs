﻿using System;
using Lynx.APD.Component.Library.PartnerDataMgr;
using MSXML2;
using System.Threading;
using System.Configuration;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    ///  Module MPPGTransactions
    /// 
    ///  Code common to PPG transactions.
    /// </summary>
    class MPPGTransactions
    {
        #region Declarations
        private const string APP_NAME = "LAPDAccounting.";
        private string MODULE_NAME = string.Concat(APP_NAME, "MPPGTransactions.");
        MLAPDAccounting objMLAPDAccounting = null;
        Common.WriteLog objWriteLog = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["AccoutingDebug"]);
        #endregion

        #region Enumerators
        //Internal error codes for this module.
        private enum EventCodes : ulong
        {
            eElectronicPostBadStatus = 0x80065000 + 0x300
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Syntax:      BuildTransactionForPPG
        /// 
        /// Parameters:  strTransactionType - A token identifying the transaction type.
        ///              strMessageBody - The XML to put within the wrapper.
        ///              strDestination - A token indentfying the message destination.
        ///              strReferenceId - An internal identifyer for the message.
        /// 
        /// Purpose:     Wraps the passed XML with a PPG SOAP style wrapper.
        /// 
        /// Returns:     The completed XML.
        /// 
        /// Requires:    Settings in the config.xml file.
        /// </summary>
        /// <param name="strTransactionType"></param>
        /// <param name="strMessageBody"></param>
        /// <param name="strDestination"></param>
        /// <param name="strReferenceId"></param>
        /// <returns></returns>
        public string BuildTransactionForPPG(string strTransactionType, string strMessageBody, string strDestination, string strReferenceId)
        {
            string PROC_NAME = string.Empty;
            string strTrans = string.Empty,
                 strEnvelopeId = string.Empty,
             strOriginationId = string.Empty,
             strDestinationId = string.Empty,
             strBuildTransaction = string.Empty;

            bool blnUseSOAP;
            bool blnUseCDATA;

            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "BuildTransactionForPPG: ");
                objWriteLog = new Common.WriteLog();
                objMLAPDAccounting = new MLAPDAccounting();
                //Add parameters to trace data.

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.MPPGTransactions.cs - BuildTransactionForPPG", "Process started.", string.Concat("TransactionType = '" + strTransactionType + "'  Dest = '" + strDestination
                    + "'  RefId = '" + strReferenceId + "'"));

                //Get SOAP and CDATA settings from configuration.
                blnUseSOAP = Convert.ToBoolean(objMLAPDAccounting.GetConfig("PPG_SOAP_Header/UseSOAP") == "True");
                blnUseCDATA = Convert.ToBoolean(objMLAPDAccounting.GetConfig("PPG_SOAP_Header/UseCDATA") == "True");


                //This configuration setting allows us to disable the SOAP envelope if need be.
                if (!blnUseSOAP)
                    strBuildTransaction = strMessageBody;
                else
                {
                    //Get source and dest PIDs from the config file.
                    strOriginationId = objMLAPDAccounting.GetConfig("PPG_SOAP_Header/OriginationPID");
                    strDestinationId = objMLAPDAccounting.GetConfig("PPG_SOAP_Header/DestinationPID[@name='" + strDestination + "']");

                    //Generate the envelope id.
                    strEnvelopeId = string.Concat(strOriginationId, "#", strDestinationId, "#", strReferenceId, "#", DateTime.Now);

                    //Start building the transaction XML.
                    strTrans = "<StdEnvelope EnvelopeID='" + strEnvelopeId + "' Version='1.0'>";

                    //Add in the environment code.
                    strTrans = string.Concat(strTrans, "<Header><Delivery><Environment>", objMLAPDAccounting.GetConfig("@EnvironmentCode"), "</Environment>");

                    //Add in the origination company.
                    strTrans = string.Concat(strTrans, "<OriginationCompany>", objMLAPDAccounting.GetConfig("PPG_SOAP_Header/OriginationCompany"), "</OriginationCompany>");

                    //Add in the origination ID.
                    strTrans = string.Concat(strTrans, "<OriginationPID>", strOriginationId, "</OriginationPID>");

                    //Add in the destination company and Id.
                    strTrans = string.Concat(strTrans, "<DestinationCompany>", strDestination, "</DestinationCompany>"
                        , "<DestinationPID>", strDestinationId, "</DestinationPID>");

                    //Add in the transaction type.
                    strTrans = string.Concat(strTrans, "<TransactionType>", strTransactionType, "</TransactionType>");

                    //Add in the sender's reference Id.
                    strTrans = string.Concat(strTrans, "<SendersReferenceID>", strReferenceId, "</SendersReferenceID>");

                    //Add in the current time and date.
                    strTrans = string.Concat(strTrans, "<TransactionDateTime>", DateTime.Now, "</TransactionDateTime>");

                    //Close out the header, begin the message body.
                    strTrans = string.Concat(strTrans, "</Delivery></Header><MessageBody>");

                    //Add in CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "<![CDATA[");

                    //Add in the message body.
                    strTrans = string.Concat(strTrans, strMessageBody);

                    //Close CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "]]>");

                    //Close the message and envelope tags - we are done!
                    strTrans = string.Concat(strTrans, "</MessageBody></StdEnvelope>");

                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "END", "Accouting.MPPGTransactions.cs - BuildTransactionForPPG", "Process completed.", string.Concat(" Envelope: ", strTrans));

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTrans;
        }

        /// <summary>
        /// Syntax:      XmlHttpPost
        /// Parameters:  strUrl - The URL to post to.
        ///              strXml - The XML data to POST.
        ///              g_objEvents - An instance of SiteUtilities::CEvents to use.
        /// Purpose:     Posts the passed data to a url.
        /// Returns:     The status.
        /// </summary>
        /// <param name="strUrl"></param>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public long XmlHttpPost(string strUrl, string strXML)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "XmlHttpPost: " });
            long lngStatus = 0;
            Int32 intFailOverCount = 0;
            const Int32 MaxFailoverAttempts = 10;
            const Int32 FailoverSleepPeriod = 1000;
            MSXML2.XMLHTTP objXmlHttp = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.MPPGTransactions.cs", string.Concat("Process Started"), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));
                while (intFailOverCount <= MaxFailoverAttempts)
                {
                    try
                    {
                        objXmlHttp = new XMLHTTP();
                        objXmlHttp.open("POST", strUrl, false);
                        objXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        objXmlHttp.send(strXML);

                        lngStatus = objXmlHttp.status;
                        if (objXmlHttp.status == 200)
                            break;
                        else
                        {
                            objWriteLog.LogEvent("COM Conversion", "ERROR", "Accouting.MPPGTransactions.cs", string.Concat("HTTP Send", "Bad status returned from post to", strUrl, "Status: ", Convert.ToString(objXmlHttp.status)), "");
                            throw new Exception(string.Concat(PROC_NAME, "HTTP Send", "Bad status returned from post to", strUrl, "Status: ", Convert.ToString(objXmlHttp.status)));    
                        }

                        intFailOverCount++;
                        Thread.Sleep(FailoverSleepPeriod);

                    }
                    catch (Exception ex)
                    {
                        intFailOverCount++;
                        Thread.Sleep(FailoverSleepPeriod);
                    }
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.MPPGTransactions.cs", string.Concat("Process Completed"), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));

            }
            catch (Exception ex)
            {
                objXmlHttp = null;
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accouting.MPPGTransactions.cs", string.Concat("Exception: ", ex.Message), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return lngStatus;
        }
        #endregion

    }
}
