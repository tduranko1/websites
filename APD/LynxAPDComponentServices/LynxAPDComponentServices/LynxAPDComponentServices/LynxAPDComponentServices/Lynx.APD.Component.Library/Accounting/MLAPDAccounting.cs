﻿using System;
using System.Threading;
using SiteUtilities;
using System.Configuration;
using System.Xml;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Accounting
{
    class MLAPDAccounting
    {
        #region Variable Declaration
        public const string APP_NAME = "LAPDAccounting.";
        public string MODULE_NAME = string.Concat(APP_NAME, "MLAPDAccounting.");
        public const long LAPDAccounting_FirstError = 0x80066800;

        //Global events and data access objects.
        public CEvents g_objEvents = null;
        public string g_strSupportDocPath = string.Empty;
        public bool g_blnDebugMode = false;
        PartnerDataMgr.MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        // public DataAccessor.CDataAccessor g_objApdRsAccessor = null;
        //public DataAccessor.CDataAccessor g_objApdXmlAccessor = null;
        //public DataAccessor.CDataAccessor g_objIngresAccessor = null;

        //I used multiple DataAccessor components here because we have two separate
        //databases to hit, one with two different providers.  All of this has to be
        //wrapped in the same transaction.  You cannot switch provider or database
        //in the middle of a transaction.  Ugly, ugly.

        public string g_strIngresConnect = string.Empty;
        #endregion

        #region Enum Declaration
        //Error codes specific to LAPDAccounting
        public enum LAPDAccounting_ErrorCodes : ulong
        {
            //General error codes applicable to all modules of this component
            eCreateObjectExError = LAPDAccounting_FirstError,
            eIngresReturnedUnknownError = LAPDAccounting_FirstError + 0x50,
            eIngresRequiresVehicleDesc = LAPDAccounting_FirstError + 0x60,
            eIngresRequiresIngresAccountingID = LAPDAccounting_FirstError + 0x70,

            //Module specific error ranges.
            CBilling_CLS_ErrorCodes = LAPDAccounting_FirstError + 0x100,
            eHandlingFeeError = LAPDAccounting_FirstError + 0x110,

            MRecordsetUtils_BAS_ErrorCodes = LAPDAccounting_FirstError + 0x180
        }

        public enum EDatabase
        {
            eAPD_Recordset,
            eAPD_XML,
            eIngres_Recordset
        }
        #endregion

        #region Public Methods

        //API declares

        ///Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
        ///Parameters:  strObjectName = The object to call CreateObject for.
        ///Purpose:     Wraps CreateObject() with better debug information.
        ///Returns:     The Object created or Nothing.

        public object CreateObjectEx(string strObjectName)
        {
            object mobjCreateObject = null;
            Type createdObj = null;
            try
            {
                createdObj = Type.GetTypeFromProgID(strObjectName);
                if (createdObj == null)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(LAPDAccounting_ErrorCodes.eCreateObjectExError), ",", "", ",", "CreateObject('", strObjectName, "') returned Nothing." }));

                mobjCreateObject = Activator.CreateInstance(createdObj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                createdObj = null;
            }

            return mobjCreateObject;
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }

        /// <summary>
        /// Intialize the shared object
        /// </summary>
        public void InitializeGlobals()
        {
            //Create DataAccessor.
            string strConfig = string.Empty;
            try
            {
                g_objEvents = new CEvents();
                //Get path to support document directory
                g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["Accounting"]);
                strConfig = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Rlease the object instance
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                if (g_objEvents != null)
                    Marshal.ReleaseComObject(g_objEvents);
                g_objEvents = null;
                // g_objIngresAccessor = null;
                // g_objApdXmlAccessor = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Construct the SQL date and time.
        /// </summary>
        /// <param name="strXmlDate"></param>
        /// <param name="strXmlTime"></param>
        /// <param name="blnNowAsDefault"></param>
        /// <returns></returns>
        public string ConstructSqlDateTime(string strXmlDate, string strXmlTime, bool blnNowAsDefault = true)
        {
            string strDateTime = string.Empty,
                strTemp = string.Empty;
            try
            {
                //Should be in format 'mmddyyyy' as per spec.
                if (strXmlDate.Length == 8)
                    strDateTime = string.Concat(new string[] { Right(strXmlDate, 4), "-", Left(strXmlDate, 2), "-", Mid(strXmlDate, 3, 2) });
                else if (blnNowAsDefault)
                    strDateTime = DateTime.Now.ToString("yyyy-MM-dd");

                //Dont bother with time if we dont have date.
                if (strDateTime.Length > 0)
                {
                    //Should be in format 'hhmmss' as per spec.
                    if (strXmlTime.Length == 6)
                        strDateTime = string.Concat(new string[] { strDateTime, "T", Left(strXmlTime, 2), ":", Mid(strXmlTime, 3, 2), ":", Right(strXmlTime, 2) });

                        //If date was passed but no time then use default of 0 time.
                    else if (strXmlDate.Length > 0)
                        strDateTime = string.Concat(strDateTime, "T00:00:00");
                    //If no date or time were passed then use now if allowed.
                    else if (blnNowAsDefault)
                        strDateTime = string.Concat(strDateTime, DateTime.Now.ToString("Thh:mm:ss"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDateTime;
        }

        /// <summary>
        /// Get the value from Config file.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public string GetConfig(string strSetting)
        {
            //g_objEvents = new CEvents();
            //return g_objEvents.mSettings.GetParsedSetting(strSetting);

            string strReturn = string.Empty, strMachineName = string.Empty, strAppPath = string.Empty, strEnvPath = string.Empty,
strMachOnly = string.Empty, strEnvInsPath = string.Empty, strMachPath = string.Empty, strMachInstPath = string.Empty, strInstPath = string.Empty, strConfig = string.Empty;
            XmlDocument xmlDocConfig = null;
            bool blnFoundInst = false;
            string mstrInstance = "test";
            XmlNode xmlNode;
            try
            {
                xmlDocConfig = new XmlDocument();
                strAppPath = "Root/Application/";
                strMachineName = Environment.MachineName;
                strEnvPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']]/";
                strEnvInsPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']"; //Need to Add //[Instance[@name='" & mstrInstance & "']]]/"
                strMachOnly = "//Machine[@name='" + strMachineName + "']"; //[Instance[@name='" & mstrInstance & "']]"
                strMachPath = strAppPath + "Environment/Machine[@name='" + strMachineName + "']/";
                strMachInstPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "'][Instance[@name='" + mstrInstance + "']]]/";
                strInstPath = strMachPath; //"Instance[@name='" & mstrInstance & "']/"

                strConfig = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);

                xmlDocConfig.Load(strConfig);

                xmlNode = xmlDocConfig.SelectSingleNode(strMachPath + strSetting);
                if (xmlNode == null)
                    blnFoundInst = false;

                if (!blnFoundInst && xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strEnvPath + strSetting);

                if (xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strAppPath + strSetting);

                strReturn = xmlNode.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Connect to DB
        /// </summary>
        /// <param name="edatabase"></param>
        /// <returns></returns>
        public string ConnectToDB(EDatabase edatabase)
        {
            string strConnect = string.Empty;
            string strConfig = string.Empty;
            try
            {
                //Get configuration settings for FNOL database connect string.
                switch (edatabase)
                {
                    case EDatabase.eIngres_Recordset:
                        strConfig = "Ingres";
                        break;
                    case EDatabase.eAPD_Recordset:
                        strConfig = "APD";
                        break;
                    case EDatabase.eAPD_XML:
                        strConfig = "APD";
                        break;
                }

                //strConnect = GetConfig(strConfig);
                //  g_objEvents.Assert(strConnect.Length > 0, string.Concat("Blank connection string returned from config file: ", strConfig));

                //Set the connect string.
                //switch (edatabase)
                //{
                //    case EDatabase.eAPD_Recordset:
                //        g_objApdRsAccessor.SetConnectString(strConnect);
                //        break;
                //    case EDatabase.eAPD_XML:
                //        g_objApdXmlAccessor.SetConnectString(strConnect);
                //        break;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strConfig;
        }

        /// <summary>
        /// Build file name to process
        /// </summary>
        /// <param name="lngLynxID"></param>
        /// <returns></returns>
        public string BuildFileName(long lngLynxID)
        {
            int fileCount = 0;
            string strImageFolder = string.Empty;
            string strLynxID = string.Empty;
            string strDateTime = string.Empty;
            string strBuildFileName = string.Empty;
            try
            {
                //Use the current date time to make the file name unique.
                strDateTime = DateTime.Now.ToString("yyyyMMddhhmmss");

                //Get the last 4 digits of the lynx id for the file folder hierarchy.
                strLynxID = Right(Convert.ToString(lngLynxID), 4);

                for (fileCount = 1; fileCount <= 4; fileCount++)
                {
                    strImageFolder = string.Concat(strImageFolder, @"\", Mid(strLynxID, fileCount, 1));
                }
                strImageFolder = string.Concat(strImageFolder, @"\");

                //Put it all together now...
                strBuildFileName = string.Concat(new string[] { strImageFolder, "INV", strDateTime, "LYNXID", Convert.ToString(lngLynxID), "Doc.doc" });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strBuildFileName;
        }

        /// <summary>
        /// Builds the year/make/model/body style string
        /// </summary>
        /// <param name="strYear"></param>
        /// <param name="strMake"></param>
        /// <param name="strModel"></param>
        /// <param name="strBody"></param>
        /// <returns></returns>
        public string BuildVehicleString(string strYear, string strMake, string strModel, string strBody)
        {
            string strTemp = string.Empty;
            string strBuildVehicleString = string.Empty;
            try
            {
                strTemp = strYear;
                if (strMake != null)
                    strTemp = string.Concat(strTemp, " ", strMake);
                if (strModel != null)
                    strTemp = string.Concat(strTemp, " ", strModel);
                if (strBody != null)
                    strTemp = string.Concat(strTemp, " ", strBody);

                strBuildVehicleString = strTemp.ToUpper();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strBuildVehicleString;
        }

        ///Syntax:      DoSleep( 500 )
        ///Parameters:  lngMS - milliseconds to sleep.
        ///Purpose:     Wraps Sleep() with calls to VB DoEvents().
        ///Returns:     Nothing.
        public void DoSleep(long lngMS)
        {
            long lCount = 0;
            try
            {
                for (lCount = 1; lCount <= lngMS / 10; lCount++)
                {
                    Thread.Sleep(10);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
    }
}
