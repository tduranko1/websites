﻿using System;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using ADODB;
using DocumentMgr;
using System.Configuration;
using System.Collections;
using objDataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using Lynx.APD.Component.Library.Common;
using System.Data;
using LynxServices.Library.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    /// Billing process
    /// </summary>
    public class CBilling
    {
        private string MODULE_NAME = "LAPDAccounting.CBilling.";

        MLAPDAccounting mobjMLAPDAccounting = null;
        CInvoiceItemManager mobjInvoiceItemManager = null;
        MDomUtils mobjMDomUtils = null;
        MPPGTransactions mobjMPPGTransactions = null;
        objDataAccessor.CDataAccessor objIngresAccessor = null;
        objDataAccessor.CDataAccessor objApdXmlAccessor = null;
        //Common.WriteLog objWriteLog = null;
        string m_strRevisedInvoiceXML = string.Empty;
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["AccoutingExtensiveDebug"]);
        bool m_blnReleaseFundsMode = false;

        /// <summary>
        /// Enum for Error codes.
        /// </summary>
        private enum EventCodes : ulong
        {
            eNoOpenBillingOrPayment = MLAPDAccounting.LAPDAccounting_ErrorCodes.CBilling_CLS_ErrorCodes,
            eMissingDispatchNumber,
            eNoOpenPayment,
            eNoOpenBilling,
            eBulkBillingAndPayment
        }

        //Backup global termination
        private void Class_Terminate()
        {
            mobjMLAPDAccounting = new MLAPDAccounting();
            mobjMLAPDAccounting.TerminateGlobals();
        }

        /// Procedure : BillAndCreateInvoice
        /// DateTime  : 3/16/2005 10:03
        /// Author    : CSR0901
        /// Purpose   : 1 - Pull APD Invoice records for the passed ClaimAspectID.
        /// 2 - Apply business rules for totaling payments and attaching fees.
        /// 3 - Send the items to Ingres.
        /// 4 - Retrieve a Dispatch number for each item sent to Ingres..
        /// 5 - Apply the Dispatch number to the APD Invoice records that comprise the  Dispatch
        ///     and update the Status of those records.
        /// 6 - Create a 'paper' Invoice document.
        /// 7 - Attach the document to the claim.
        public long BillAndCreateInvoice(long lngInsuranceCompanyID, long lngClaimAspectID, long intUserID)
        {
            XmlDocument objXmlDoc = null;

            //Primitives
            string strXML = string.Empty;
            bool blnIngresEnabled = false;
            int intSentToIngresCount = 0;
            string strParamXML = string.Empty;
            CDocMgr objDocMgr = null;
            string strTempGetConfig = string.Empty, strApplication = string.Empty;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CBilling.cs - BillAndCreateInvoice", "Process started.", string.Concat("Vars: InsuranceCompanyID = ", lngInsuranceCompanyID, " ClaimAspectID = ", lngClaimAspectID, " UserID = ", intUserID));
                //Objects that must be destroyed.
                mobjMDomUtils = new MDomUtils();
                mobjMLAPDAccounting = new MLAPDAccounting();
                objApdXmlAccessor = new objDataAccessor.CDataAccessor();
                string PROC_NAME = string.Concat(MODULE_NAME, "BillAndCreateInvoice: ");

                mobjMLAPDAccounting.InitializeGlobals();

                if (StringExtensions.IsNullOrWhiteSpace(Convert.ToString(lngInsuranceCompanyID)))
                    objWriteLog.LogEvent("COM Conversion", "Validate", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Nothing passed for Insurance Company ID."), "");

                if (StringExtensions.IsNullOrWhiteSpace(Convert.ToString(lngClaimAspectID)))
                    objWriteLog.LogEvent("COM Conversion", "Validate", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Nothing passed for Claim Aspect ID."), "");

                if (StringExtensions.IsNullOrWhiteSpace(Convert.ToString(intUserID)))
                    objWriteLog.LogEvent("COM Conversion", "Validate", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Nothing passed for User ID."), "");

                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("/Accounting/IngresEnabled")) ? true : false;

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", "Checking is Ingres Enabled from the Config.xml.", string.Concat("Vars: IsIngresEnabled  = ", blnIngresEnabled));

                //Load accounting pickup info into a DOM.
                objXmlDoc = new XmlDocument();
                //Flip this config to True to load the accounting xml from disk.
                if (mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile") == "true")
                {
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", "Load the accounting xml from disk. ", string.Concat("Vars: FilePath  = ", strTempGetConfig));
                    strTempGetConfig = mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile/@file");
                    mobjMDomUtils.LoadXmlFile(ref objXmlDoc, ref strTempGetConfig, "", "");
                }
                else
                {
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("SP should pull all billing and payment records billed or otherwise and compute new total."), "");

                    strApplication = mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);
                    // TODO:  proc should pull all billing and payment records billed or otherwise and compute new total.
                    hashTable = new Hashtable();
                    hashTable.Add("@InvoiceTypeCD", "P");
                    hashTable.Add("@InsuranceCompanyID", lngInsuranceCompanyID);
                    hashTable.Add("@ClaimAspectID", lngClaimAspectID);
                    hashTable.Add("@ToDate", mobjMLAPDAccounting.ConstructSqlDateTime("", "", true));
                    hashTable.Add("@UserID", intUserID);

                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", "Executing SP to pull the billing data.", string.Concat("Vars: StoredProc = ", mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingSP"),
                            "@InvoiceTypeCD = ", "P",
                            "@InsuranceCompanyID = ", lngInsuranceCompanyID,
                            "@ClaimAspectID = ", lngClaimAspectID,
                            "@ToDate = ", mobjMLAPDAccounting.ConstructSqlDateTime("", "", true),
                            "@UserID = ", intUserID));

                    strXML = objApdXmlAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingSP"), hashTable, strApplication);
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Execute SP: ", mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingSP")), string.Concat("SQLXML: ", strXML));
                    mobjMDomUtils.LoadXml(ref objXmlDoc, ref strXML, PROC_NAME, "Billing");
                }
                // Create the Invoice Item Manager.
                mobjInvoiceItemManager = new CInvoiceItemManager();

                // Initialize the InvoiceItemManager which will in turn construct the final InvoiceItems that will be sent to Ingres and
                // appear on the Invoice document or replace the raw Invoice nodes in the XML source for the billing file.
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Calling Initialize function from mobjInvoiceItemManager()."), string.Concat("Vars: ParamXML = ", objXmlDoc.OuterXml, "InvoiceType = ePaper"));
                mobjInvoiceItemManager.Initialize(objXmlDoc.OuterXml, CInvoiceItemManager.eInvoiceType.ePaper);

                // Send InvoiceItems to Ingres and update the retrieved Dispatch number to the APD database and the InvoiceItem objects.
                if (blnIngresEnabled)
                {
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Send InvoiceItems to Ingres and update the retrieved Dispatch number to the APD database and the InvoiceItem objects."), "");
                    intSentToIngresCount = mobjInvoiceItemManager.SendToIngres();
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("Return value from SendToIngres()."), string.Concat("Vars: IngresReturnValue = ", intSentToIngresCount));
                }

                // If items were sent to Ingres, indicated by a returned count, an invoice document must be created.
                if (intSentToIngresCount > 0)
                {
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("If the intSentToIngresCount value is greater then 0, we will generate the invoice document."), "");
                    // Generate the invoice and save to disk.
                    // Create the XML to be used to generate Invoice document.
                    strXML = mobjInvoiceItemManager.CreateInvoiceSourceXML(CInvoiceItemManager.eInvoiceType.ePaper);
                    objDocMgr = new CDocMgr();
                    // Build xml input required by the DocumentMgr.GenerateInvoiceDocument function.
                    //strParamXML = "<Invoice NTUserID='" & strCSRNo & "' ProtectDocument='True' DocumentType='Invoice' />"
                    strParamXML = "<Invoice ProtectDocument='true' DocumentType='Invoice' />";
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("GenerateInvoiceDocument is calling to generate the invoice document."), string.Concat("Vars: strParamXML =", strParamXML, "strXML = ", strXML));
                    // Call DocumentMgr to create invoice document and attach that document to the claim.
                    objDocMgr.GenerateInvoiceDocument(strParamXML, strXML);
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CBilling.cs - BillAndCreateInvoice", string.Concat("BillAndCreateInvoice Completed."), "");

                mobjMLAPDAccounting.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objXmlDoc = null;
                mobjInvoiceItemManager = null;
                if (objDocMgr != null)
                    Marshal.ReleaseComObject(objDocMgr);
                objDocMgr = null;
            }
            return 0;
        }

        ///Procedure : ElectronicInvoicing
        ///DateTime  : 3/16/2005 10:03
        ///Author    : CSR0901
        ///Purpose   : 1 - Pull APD Invoice records for the passed ClaimAspectID.
        /// 2 - Apply business rules for totaling payments and attaching fees.
        /// 3 - Send the items to Ingres.
        /// 4 - Retrieve a Dispatch number for each item sent to Ingres.
        /// 5 - Apply the Dispatch number to the APD Invoice records that comprise the Dispatch
        ///     and update the Status of those records.
        /// 6 - Create the source XML that will be sent to GLAXIS and used to create the billing file
        ///     which will be transmitted to the client.
        /// 7 - Post the XML to GLAXIS.
        public long ElectronicInvoicing(long lngInsuranceCompanyID, DateTime dtToDate)
        {
            //Objects that must be destroyed.
            XmlDocument objXmlDoc = null;
            mobjInvoiceItemManager = new CInvoiceItemManager();

            //Primitives
            string strEnabled = string.Empty;
            string strPPGDestCode = string.Empty;
            string strPPGXML = string.Empty;
            string strXML = string.Empty;
            string strXmitUrl = string.Empty;
            bool blnIngresEnabled = false;
            int intSentToIngresCount = 0;
            DateTime Expression;
            string strTempGetConfig = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMLAPDAccounting = new MLAPDAccounting();
                mobjMDomUtils = new MDomUtils();
                mobjMLAPDAccounting = new MLAPDAccounting();
                mobjMPPGTransactions = new MPPGTransactions();
                objApdXmlAccessor = new objDataAccessor.CDataAccessor();
                objWriteLog = new Common.WriteLog();

                string PROC_NAME = string.Concat(MODULE_NAME, "ElectronicInvoicing: ");

                mobjMLAPDAccounting.InitializeGlobals();

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CBilling.cs - ElectronicInvoicing", "START", string.Concat("Vars: InsuranceCompanyID = ", lngInsuranceCompanyID, " dtToDate = ", dtToDate));

                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(lngInsuranceCompanyID > 0, "Nothing passed for Insurance Company ID.");

                mobjMLAPDAccounting.g_objEvents.Assert(DateTime.TryParse(Convert.ToString(dtToDate), out Expression), "Invalid Date passsed.");

                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("Accounting/IngresEnabled = true")) ? true : false;

                // Extract all non-billed billing and payment records from APD as XML.
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ElectronicInvoicing", "Extracting APD Records", "");

                //Load accounting pickup info into a DOM.
                objXmlDoc = new XmlDocument();

                //Flip this config to True to load the accounting xml from disk.
                if (mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile") == "true")
                {
                    strTempGetConfig = mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile/@file");
                    mobjMDomUtils.LoadXmlFile(ref objXmlDoc, ref strTempGetConfig, PROC_NAME, "Test Billing Pickup Data");
                }
                //Get the accounting xml from the database.
                else
                {
                    mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);
                    htParams = new Hashtable();
                    htParams.Add("@InvoiceTypeCD", "F");
                    htParams.Add("@InsuranceCompanyID", lngInsuranceCompanyID);
                    htParams.Add("@ClaimAspectID", "Default");
                    htParams.Add("@ToDate", string.Concat("'", Convert.ToString(dtToDate), "'"));
                    htParams.Add("@UserID", 0);

                    strXML = objApdXmlAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingElectronicSP"), htParams, "APD");
                    mobjMDomUtils.LoadXml(ref objXmlDoc, ref strXML, PROC_NAME, "Billing");
                }

                // Create the Invoice Item Manager.
                mobjInvoiceItemManager = new CInvoiceItemManager();

                // Initialize the InvoiceItemManager which will in turn construct the final InvoiceItems that will be sent to Ing
                // appear on the Invoice document or replace the raw Invoice nodes in the XML source for the billing file.
                mobjInvoiceItemManager.Initialize(objXmlDoc.OuterXml, CInvoiceItemManager.eInvoiceType.ePaper);

                //Send InvoiceItems to Ingres and update the retrieved Dispatch number to the APD database and the InvoiceItem o
                if (blnIngresEnabled)
                    intSentToIngresCount = mobjInvoiceItemManager.SendToIngres();

                // If Ingres calls are enabled in this environment but there was nothing to send or nothing was sent successfully
                if (intSentToIngresCount == 0 && blnIngresEnabled)
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ElectronicInvoicing", "Nothing to invoice. Terminating Process.", "");
                    else
                    {
                        // Create the Billing file XML to be sent to GLAXIS.
                        strXML = mobjInvoiceItemManager.CreateInvoiceSourceXML(CInvoiceItemManager.eInvoiceType.eFile);

                        //Get the 'nickname' used to identify the current company in the SOAP header.
                        strPPGDestCode = mobjMLAPDAccounting.GetConfig(string.Concat("Accounting/ElectronicInvoice/Insurance[@ID='", Convert.ToString(lngInsuranceCompanyID), "']/@PPGDe"));

                        //Do we override the electronic invoice in this environment?
                        strEnabled = mobjMLAPDAccounting.GetConfig(string.Concat("PPG_SOAP_Header/DestinationPID[@name='", strPPGDestCode, "']/@enabled"));

                        //Get the submit url to GO from the config xml file.  The electronic invoicing process will use the same URLs a
                        strXmitUrl = mobjMLAPDAccounting.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                        //Add the PPG SOAP wrapper to the Invoice XML.
                        strPPGXML = mobjMPPGTransactions.BuildTransactionForPPG("Invoice", strXML, strPPGDestCode, strPPGDestCode);

                        if (strEnabled != "true")
                        {
                            if (isExtensiveDebug)
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ElectronicInvoicing", string.Concat("ELECTRONIC TRANSMISSION DISABLED TO ", strPPGDestCode, " IN THE ", mobjMLAPDAccounting.g_objEvents.mSettings.Environment), "");
                            else
                            {
                                //Electronic transmission is now reduced to just and http post to GO.
                                mobjMPPGTransactions.XmlHttpPost(strXmitUrl, strPPGXML);
                            }
                        }
                    }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ElectronicInvoicing", "END", "");

                mobjMLAPDAccounting.TerminateGlobals();
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accouting.CBilling.cs - ElectronicInvoicing", string.Concat(ex.Message, ex.InnerException), "");
                return -1; // Return -1 if error throws
                throw ex;
            }
            finally
            {
                objXmlDoc = null;
                mobjInvoiceItemManager = null;
                objWriteLog = null;
            }
            return 0;
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        /// <summary>
        /// Rlease payment process
        /// </summary>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public int ReleaseFunds(string strXML)
        {
            XmlDocument objReleasePaymentXML = null;
            XmlDocument objOriginalTxXML = null;
            //object objIngresAccessor=null;
            XmlElement objRoot = null;
            XmlDocument objRevisedInvoiceXML = null;
            XmlElement objRevisedInvoiceFeeItem = null;

            string strInsuranceCompanyName = string.Empty;
            string strLynxID = string.Empty;
            string strClaimAspectNumber = string.Empty;
            string strDispatchNumber = string.Empty;
            string strOriginalDispatchNumber = string.Empty;
            string strNewDispatchNumber = string.Empty;
            string strInvoiceDate = string.Empty;
            string strInvoiceSubmitDate = string.Empty;
            string strInvoiceAmount = string.Empty;
            string strPayeeID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayeePhone = string.Empty;
            string strPayeeWarrantyFlag = string.Empty;
            string strDescription = string.Empty;
            string strHistoryComments = string.Empty;
            string strReleaseAmount = string.Empty;
            string strDeductibleAmount = string.Empty;
            string strTaxTotalAmount = string.Empty;
            string strWarrantyFlag = string.Empty;
            string strFileName = string.Empty;
            string strImageRoot = string.Empty;
            string strMessageText = string.Empty;
            string strMessageInd = string.Empty;
            string strReturnValue = string.Empty;
            string strIngresProc = string.Empty;
            string strUpdateDispatchProc = string.Empty;
            string strUserID = string.Empty;
            string strOldInvoiceIDFee = string.Empty;
            string strNewFeeID = string.Empty;
            string strNewFeeServices = string.Empty;
            string strDispositionCD = string.Empty;
            string strFeeAmount = string.Empty;
            bool blnIngresEnabled = false;
            string strManualFormRequestTo = string.Empty;
            string h_apd_lynx_id = string.Empty;
            string h_apd_shop_id = string.Empty;
            string h_claim_amt = string.Empty;
            string h_claim_no = string.Empty;
            string h_comment_text = string.Empty;
            string h_commit_ind = string.Empty;
            string h_dispatch_no = string.Empty;
            string h_fee_amt = string.Empty;
            string h_rtlr_fee_amt = string.Empty;
            string h_insrd_first_nm = string.Empty;
            string h_insrd_last_nm = string.Empty;
            string h_ins_pc_uid_no = string.Empty;
            string h_job_type_cd = string.Empty;
            string h_msg_ind = string.Empty;
            string h_msg_text = string.Empty;
            string h_release_fund_amt = string.Empty;
            string h_release_fund_ind = string.Empty;
            string h_warranty_ind = string.Empty;
            string h_tries = string.Empty;
            string h_user_id_no = string.Empty;
            string h_user_session_no = string.Empty;
            string strEmailSubject = string.Empty;
            string strEmailFrom = string.Empty;
            string strEmailBody = string.Empty;
            string strReviseInvoice = string.Empty;
            string strIngresErrorCode = string.Empty;
            string lngIngresErrorNumber = string.Empty;
            string strIngresMessageText = string.Empty;
            string strSrc = string.Empty;
            string strDesc = string.Empty;
            string strConfigPath = string.Empty;
            string commandText = string.Empty;
            string strRelaseXML = string.Empty;

            long lngNum = 0;
            int strReleaseFunds = 0, iOut = 0,
             intReturnVal = 0;

            bool blnIngresTransaction = false;
            bool blnAPDTransaction = false;
            bool blnIngresErrorElectronicBilling = false;

            DataLayer dbLayer = null;
            LynxServices.Library.Data.Parameters parameters = null;
            DbTransaction transaction = null;
            AppSettingsReader appReader = null;
            Hashtable hashTable = null;
            SqlConnection sqlConnection = null;
            SqlTransaction sqlTransaction = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                appReader = new AppSettingsReader();
                mobjMLAPDAccounting = new MLAPDAccounting();
                mobjMDomUtils = new MDomUtils();
                hashTable = new Hashtable();
                objReleasePaymentXML = new XmlDocument();
                objDataAccessor.CDataAccessor objDataAccess = new objDataAccessor.CDataAccessor();
                sqlConnection = new SqlConnection(objDataAccess.GetConnectionString("APD"));
                sqlConnection.Open();

                blnAPDTransaction = true;
                sqlTransaction = sqlConnection.BeginTransaction();

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CBilling.cs - ReleaseFunds", "Process started.", string.Concat("Vars: ParamXML: ", strXML));


                if (strXML.Contains("![CDATA["))
                    strXML = strXML.Replace("![CDATA[", string.Empty);
                if (strXML.Contains("]]"))
                    strXML = strXML.Replace("]]", string.Empty);

                dbLayer = new DataLayer(objDataAccess.GetConnectionString("Ingres"), DataLayer.DatabaseType.Ingres);

                string PROC_NAME = string.Concat(MODULE_NAME, "ReleasePayment: ");
                m_blnReleaseFundsMode = true;

                mobjMLAPDAccounting.InitializeGlobals();

                if (!string.IsNullOrEmpty(strXML.Trim()))
                    objReleasePaymentXML.LoadXml(strXML);
                else
                    throw new Exception(string.Concat("Parameter xml string passed as empty", "Param strXML: ", strXML));
                //mobjMDomUtils.LoadXml(ref objReleasePaymentXML, ref strXML, string.Empty, string.Empty);

                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@insuranceCompanyName") != null)
                    strInsuranceCompanyName = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@insuranceCompanyName").Value; //DocumentElement.GetAttribute("insuranceCompanyName").ToString();
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@lynxID") != null)
                    strLynxID = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@lynxID").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@claimAspectNumber") != null)
                    strClaimAspectNumber = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@claimAspectNumber").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@dispatchNumber") != null)
                    strDispatchNumber = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@dispatchNumber").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@originalDispatchNumber") != null)
                    strOriginalDispatchNumber = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@originalDispatchNumber").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceDate") != null)
                    strInvoiceDate = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceDate").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceSubmitDate") != null)
                    strInvoiceSubmitDate = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceSubmitDate").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceAmount") != null)
                    strInvoiceAmount = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@invoiceAmount").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeID") != null)
                    strPayeeID = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeID").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeName") != null)
                    strPayeeName = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeName").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeePhone") != null)
                    strPayeePhone = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeePhone").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeWarrantyFlag") != null)
                    strPayeeWarrantyFlag = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@payeeWarrantyFlag").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@description") != null)
                    strDescription = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@description").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@historyComment") != null)
                    strHistoryComments = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@historyComment").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@releaseAmt") != null)
                    strReleaseAmount = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@releaseAmt").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@deductibleAmt") != null)
                    strDeductibleAmount = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@deductibleAmt").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@taxTotal") != null)
                    strTaxTotalAmount = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@taxTotal").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@warrantyFlag") != null)
                    strWarrantyFlag = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@warrantyFlag").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@oldInvoiceIDFee") != null)
                    strOldInvoiceIDFee = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@oldInvoiceIDFee").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@newFeeID") != null)
                    strNewFeeID = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@newFeeID").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@newFeeServices") != null)
                    strNewFeeServices = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@newFeeServices").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@dispositionCD") != null)
                    strDispositionCD = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@dispositionCD").Value;
                if (objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@userID") != null)
                    strUserID = objReleasePaymentXML.SelectSingleNode("/Root/ReleasePayment/@userID").Value;

                strWarrantyFlag = (Convert.ToBoolean(strWarrantyFlag == "1")) ? "Y" : "N";
                if (string.IsNullOrEmpty(strOriginalDispatchNumber))
                {
                    strOriginalDispatchNumber = strDispatchNumber;
                    strUpdateDispatchProc = mobjMLAPDAccounting.GetConfig("Accounting/Payment/UpdateReleaseFundsDispatchSP");
                }

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", "Details", string.Concat("Vars: strLynxID: ", strLynxID, " strDispatchNumber: ", strDispatchNumber, " strPayeeID: ", strPayeeID, " strReleaseAmount:", strReleaseAmount));

                //g_objEvents.Assert (strPayeeWarrantyFlag = "1") And (strDispositionCD <> "TL" And strDispositionCD <> "CO"), "Cannot release funds for warranty payee for a TL/CO vehicle."
                //g_objEvents.Assert (CCur(strReleaseAmount) > 0) And (strDispositionCD <> "TL" And strDispositionCD <> "CO"), "Release funds cannot be greater than zero for a TL/CO vehicle."

                if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                {
                    if (strPayeeWarrantyFlag == "1")
                        throw new Exception(string.Concat(Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Cannot release funds for warranty payee for a TL/CO vehicle."));

                    if (Convert.ToDecimal(strReleaseAmount) > 0)
                        throw new Exception(string.Concat(Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Release funds cannot be greater than zero for a TL/CO vehicle."));
                }

                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("Accounting/IngresEnabled"));

                if (blnIngresEnabled)
                {
                    if (strPayeeWarrantyFlag == "0")
                    {
                        if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                        {
                            //need to revise the invoice
                            if (ReviseInvoice(string.Concat("<Root claimAspectID='' dispatchNumber='", strDispatchNumber,
                                             "' revisedAmount='", strReleaseAmount,
                                             "' oldInvoiceIDFee='", strOldInvoiceIDFee,
                                             "' newFeeID='", strNewFeeID,
                                             "' newFeeServices='", strNewFeeServices,
                                             "' userID='", strUserID, "'/>")) != 0)
                                throw new Exception(string.Concat(Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Error revising the invoice."));

                            //read the new invoice line items
                            objRevisedInvoiceXML = new XmlDocument();
                            mobjMDomUtils.LoadXml(ref objRevisedInvoiceXML, ref m_strRevisedInvoiceXML, string.Empty, string.Empty);

                            objRevisedInvoiceFeeItem = (XmlElement)objRevisedInvoiceXML.SelectSingleNode(string.Concat("//Invoice[@DispatchNumber='", strDispatchNumber, "' and @ItemType='Fee']"));
                            if (objRevisedInvoiceFeeItem != null)
                                strFeeAmount = Convert.ToString(objRevisedInvoiceFeeItem.GetAttribute("Amount"));
                        }

                        strImageRoot = mobjMLAPDAccounting.GetConfig("Document/RootDirectory");
                        if (Left(strImageRoot, 4) == "\\\\")
                            strImageRoot = Regex.Replace(strImageRoot, @"\\", @"\", RegexOptions.IgnoreCase);

                        //check if the original transaction xml
                        strFileName = Right(strLynxID, 4);
                        strFileName = string.Concat(Mid(strFileName, 0, 1), @"\",
                                      Mid(strFileName, 1, 1), @"\",
                                      Mid(strFileName, 2, 1), @"\",
                                      Mid(strFileName, 3, 1), @"\",
                                      "IngresParams", strOriginalDispatchNumber, ".xml");
                        if (Right(strImageRoot, 1) != @"\")
                            strFileName = string.Concat(strImageRoot, @"\", strFileName);
                        else
                            strFileName = string.Concat(strImageRoot, strFileName);

                        FileInfo objFileInfo = new FileInfo(strFileName);

                        if (objFileInfo.Exists && objFileInfo.Length > 0)
                        {
                            objOriginalTxXML = new XmlDocument();
                            mobjMDomUtils.LoadXmlFile(ref objOriginalTxXML, ref strFileName, string.Empty, string.Empty);

                            objRoot = objOriginalTxXML.DocumentElement;

                            //load the values from the original transaction
                            h_apd_lynx_id = objRoot.GetAttribute("h_apd_lynx_id").ToString();
                            h_apd_shop_id = objRoot.GetAttribute("h_apd_shop_id").ToString();
                            h_claim_amt = objRoot.GetAttribute("h_claim_amt").ToString();
                            h_claim_no = objRoot.GetAttribute("h_claim_no").ToString();
                            h_comment_text = strDescription; //objRoot.getAttribute("h_comment_text")
                            h_commit_ind = objRoot.GetAttribute("h_commit_ind").ToString();
                            h_dispatch_no = strDispatchNumber;
                            h_fee_amt = objRoot.GetAttribute("h_fee_amt").ToString();
                            h_rtlr_fee_amt = objRoot.GetAttribute("h_rtlr_fee_amt").ToString();
                            h_insrd_first_nm = objRoot.GetAttribute("h_insrd_first_nm").ToString();
                            h_insrd_last_nm = objRoot.GetAttribute("h_insrd_last_nm").ToString();
                            h_ins_pc_uid_no = objRoot.GetAttribute("h_ins_pc_uid_no").ToString();
                            h_job_type_cd = objRoot.GetAttribute("h_job_type_cd").ToString();
                            h_msg_ind = objRoot.GetAttribute("h_msg_ind").ToString();
                            h_msg_text = objRoot.GetAttribute("h_msg_text").ToString();
                            h_release_fund_amt = strReleaseAmount;
                            h_release_fund_ind = "Y";
                            h_warranty_ind = strWarrantyFlag;
                            h_tries = objRoot.GetAttribute("h_tries").ToString();
                            h_user_id_no = objRoot.GetAttribute("h_user_id_no").ToString();
                            h_user_session_no = objRoot.GetAttribute("h_user_session_no").ToString();

                            if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                            {
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(" Ingres parameter overrides for TL/CO: OLD Values: ",
                                                        "h_rtlr_fee_amt=", h_rtlr_fee_amt, " h_fee_amt=", h_fee_amt,
                                                        "  NEW Values: h_rtlr_fee_amt=0 h_fee_amt=", strFeeAmount), "");

                                h_rtlr_fee_amt = Convert.ToString(0);
                                h_fee_amt = strFeeAmount != string.Empty ? strFeeAmount : "0";
                            }

                            if (isExtensiveDebug)
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", "", string.Concat(" Ingres parameters: retVal=", Convert.ToString(ParameterDirectionEnum.adParamReturnValue),
                                                                    " h_apd_lynx_id=", h_apd_lynx_id, " h_apd_shop_id=", h_apd_shop_id,
                                                                    " h_claim_amt=", h_claim_amt, " h_claim_no=", h_claim_no,
                                                                    " h_comment_text=", h_comment_text, " h_commit_ind=1  h_dispatch_no=", h_dispatch_no,
                                                                    " h_fee_amt=", h_fee_amt, " h_rtlr_fee_amt=", h_rtlr_fee_amt, " h_insrd_first_nm=", h_insrd_first_nm,
                                                                    " h_insrd_last_nm=", h_insrd_last_nm, " h_ins_pc_uid_no=", h_ins_pc_uid_no,
                                                                    " h_job_type_cd=15 h_msg_ind=  h_msg_text=", h_msg_text,
                                                                    " h_tries=1  h_user_id_no=", h_user_id_no, " h_user_session_no=",
                                                                    " h_release_fund_amt=", h_release_fund_amt, " h_release_fund_ind=", h_release_fund_ind,
                                                                    " h_warranty_ind=", h_warranty_ind));

                            // Create and initialize connection to Ingres
                            objIngresAccessor = new objDataAccessor.CDataAccessor();

                            strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);

                            mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                            blnIngresTransaction = true;
                            //objIngresAccessor.BeginTransaction();//TODO Need to implement Begin tranaction for Ingres

                            blnAPDTransaction = true;
                            // objIngresAccessor.BeginTransaction();//TODO Need to implement Begin tranaction for APD

                            strIngresProc = mobjMLAPDAccounting.GetConfig("Accounting/IngressBillingSP");

                            parameters = new LynxServices.Library.Data.Parameters();

                            // Send each InvoiceItem to Ingres and retreive the returned Dispatch number.
                            commandText = string.Concat("{? = call lynxdba.", strIngresProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

                            parameters.AddReturnParameter("retval", DbType.Int32);
                            parameters.AddInputParameter("h_apd_lynx_id", h_apd_lynx_id, DbType.String, 25);
                            parameters.AddInputParameter("h_apd_shop_id", Convert.ToInt32(h_apd_shop_id), DbType.Int32, 4);
                            parameters.AddInputParameter("h_claim_amt", Convert.ToDouble(h_claim_amt), DbType.Double, 0); //-1
                            parameters.AddInputParameter("h_claim_no", h_claim_no, DbType.String, 20);
                            parameters.AddInputParameter("h_comment_text", h_comment_text, DbType.String, 300);

                            parameters.AddInputParameter("h_commit_ind", h_commit_ind, DbType.String, 1);
                            parameters.AddInputOutputParameter("h_dispatch_no", Convert.ToInt32(strDispatchNumber), DbType.Int32, 4);
                            parameters.AddInputParameter("h_fee_amt", Convert.ToDouble(h_fee_amt), DbType.Double, 0); //-1
                            parameters.AddInputParameter("h_rtlr_fee_amt", Convert.ToDouble(h_rtlr_fee_amt), DbType.Double, 0); //-1
                            parameters.AddInputParameter("h_insrd_first_nm", h_insrd_first_nm, DbType.String, 20);
                            parameters.AddInputParameter("h_insrd_last_nm", h_insrd_last_nm, DbType.String, 20);
                            parameters.AddInputParameter("h_ins_pc_uid_no", Convert.ToInt32(h_ins_pc_uid_no), DbType.Int32, 4);
                            parameters.AddInputParameter("h_job_type_cd", h_job_type_cd, DbType.Int32, 1);

                            // require this value to become dynamic.
                            parameters.AddInputOutputParameter("h_msg_ind", h_msg_ind, DbType.String, 1);

                            // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                            parameters.AddInputOutputParameter("h_msg_text", h_msg_text, DbType.String, 200);

                            //Early Billing
                            parameters.AddInputParameter("h_release_fund_amt", Convert.ToDouble(strReleaseAmount), DbType.Double, 0); //-1
                            parameters.AddInputParameter("h_release_fund_ind", "Y", DbType.String, 1);
                            parameters.AddInputParameter("h_warranty_ind", strWarrantyFlag, DbType.String, 1);
                            parameters.AddInputParameter("h_tries", Convert.ToInt32(h_tries), DbType.Int32, 4);

                            // Pass 'lynxdba' for system batch jobs.
                            parameters.AddInputParameter("h_user_id_no", h_user_id_no, DbType.String, 32);
                            parameters.AddInputParameter("h_user_session_no", h_user_session_no, DbType.String, 32);

                            transaction = dbLayer.BeginTransaction();
                            dbLayer.ExecuteNonQuery(commandText, CommandType.StoredProcedure, parameters, transaction);

                            strReturnValue = Convert.ToString(parameters["retval"].Value);
                            strNewDispatchNumber = Convert.ToString(parameters["h_dispatch_no"].Value);
                            strMessageText = Convert.ToString(parameters["h_msg_text"].Value);
                            strMessageInd = Convert.ToString(parameters["h_msg_ind"].Value);

                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(PROC_NAME, "Ingres return:  strReturnValue=", strReturnValue, " h_dispatch_no=", strDispatchNumber,
                                                                  " h_msg_text=", strMessageText, " h_msg_ind=", strMessageInd), string.Concat("ParamXML : ", strXML));

                            //Verify what we got back from Ingres.
                            if (strReturnValue == "-1")
                            {
                                // Extract the code and text portions of the Ingres error message.
                                strIngresErrorCode = Left(strMessageText, 4);
                                strIngresMessageText = Right(strMessageText, strMessageText.Length - 4);

                                //Ensure a valid Ingres error code was retrieved
                                if (int.TryParse(strIngresErrorCode, out iOut))
                                {
                                    // Convert the Ingres error code to an APD error code that can be resolved in the message store (error.xml).
                                    lngIngresErrorNumber = strIngresErrorCode;
                                    if (Convert.ToInt32(lngIngresErrorNumber) != 0)
                                        lngIngresErrorNumber = MLAPDAccounting.LAPDAccounting_FirstError + lngIngresErrorNumber;

                                    blnIngresErrorElectronicBilling = true;
                                    if (isExtensiveDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat("INGRES: Return Value = '", strReturnValue, "' Message Text = '", strMessageText,
                                            "' Message Indicator = '", strMessageInd, "'"), "");
                                    if (isExtensiveDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(strIngresErrorCode, "|", strIngresMessageText,
                                            "' LynxID='", strLynxID,
                                           "' Dispatch='", strDispatchNumber, "' ", Convert.ToString(true)), "");
                                }
                                else
                                {
                                    // For electronic billing handle the error and continue.
                                    blnIngresErrorElectronicBilling = true;
                                    if (isExtensiveDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat("INGRES: Return Value = '", strReturnValue, "' Message Text = '", strMessageText,
                                           "' Message Indicator = '", strMessageInd, "'"), "");
                                    if (isExtensiveDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(strIngresErrorCode, "|", strIngresMessageText,
                                            "' LynxID='", strLynxID,
                                           "' Dispatch='", strDispatchNumber, "' ", Convert.ToString(true)), "");
                                }
                            }
                            else if (strReturnValue != "0")
                            {
                                // For electronic billing handle the error and continue.
                                blnIngresErrorElectronicBilling = true;

                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat("INGRES: Return Value = '", strReturnValue, "' Message Text = '", strMessageText,
                                          "' Message Indicator = '", strMessageInd, "'"), "");
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(strIngresErrorCode, "|", strIngresMessageText,
                                        "' LynxID='", strLynxID,
                                       "' Dispatch='", strDispatchNumber, "' ", Convert.ToString(true)), "");
                            }

                            // Ensure Dispatch number has a value.
                            if ((!blnIngresErrorElectronicBilling) && (strNewDispatchNumber.Length > 0))
                            {
                                //Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                                hashTable = new Hashtable();
                                hashTable.Add("@DispatchNumber", strDispatchNumber);
                                hashTable.Add("@DispatchNumberNew", strNewDispatchNumber);
                                hashTable.Add("@ReleaseFundAmount", strReleaseAmount);
                                hashTable.Add("@Description", string.Concat("'", Regex.Replace(strHistoryComments, "'", "", RegexOptions.IgnoreCase), "'"));
                                hashTable.Add("@UserID", strUserID);

                                objDataAccess.ExecuteSp(strUpdateDispatchProc, hashTable, "APD");
                            }
                        }
                        else
                        {
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", "original dispatch XML not found", string.Concat("Filepath: ", strFileName));
                        }
                    }
                    else
                    {
                        //strPayeeWarrantyFlag = "0"
                        //get the email destinations for the manual request
                        strManualFormRequestTo = mobjMLAPDAccounting.GetConfig("Accounting/Payment/ManualPayment/@ToEmail");

                        if (!string.IsNullOrEmpty(strManualFormRequestTo.Trim()))
                        {
                            strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                            strEmailSubject = string.Concat("APD Manual Check Processing Request for LYNX ID: ", strLynxID, "-", strClaimAspectNumber);
                            strEmailBody = string.Concat("             APD MANUAL CHECK PROCESSING REQUEST             ", Environment.NewLine,
                                            "             ===================================             ", Environment.NewLine, Environment.NewLine,
                                            "                Date: ", DateTime.Now.ToString("mm/dd/yyyy"), Environment.NewLine,
                                            "           Installer: ", strPayeeName, Environment.NewLine,
                                            "UID Number (LYNX ID): ", strLynxID, "-", strClaimAspectNumber, Environment.NewLine,
                                            "    Telephone Number: ", strPayeePhone, Environment.NewLine,
                                            "     Dispatch Number: ", strDispatchNumber, Environment.NewLine,
                                            "           Insurance: ", strInsuranceCompanyName, Environment.NewLine,
                                            "      Invoice Number: ", strLynxID, "-", strClaimAspectNumber, Environment.NewLine,
                                            "        Created Date: ", strInvoiceDate, Environment.NewLine,
                                            "      Statement Date: ", strInvoiceSubmitDate, Environment.NewLine,
                                            "       Resubmit Date: ", Environment.NewLine,
                                            "      Invoice Amount: ", strInvoiceAmount, Environment.NewLine,
                                            "Release Funds Amount: ", strReleaseAmount, Environment.NewLine,
                                            "            Comments: ", strDescription);

                            //generate the custom form request
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(PROC_NAME, "Manual Payment Request:",
                                                                " From: ", strEmailFrom,
                                                                " To: ", strManualFormRequestTo, Environment.NewLine,
                                                                " Subject: ", strEmailSubject, Environment.NewLine,
                                                                " Body: ", strEmailBody), "");

                            mobjMLAPDAccounting.g_objEvents.SendEmail(strEmailFrom, strManualFormRequestTo, strEmailSubject, strEmailBody, false, 2);

                            // Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                            strNewDispatchNumber = strDispatchNumber;

                            //connect to the database APD Database transaction
                            mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                            //'start a transaction
                            //'blnAPDTransaction = True
                            //'g_objApdRsAccessor.BeginTransaction

                            //record the release funds
                            hashTable = new Hashtable();
                            hashTable.Add("@DispatchNumber", strDispatchNumber);
                            hashTable.Add("@DispatchNumberNew", strNewDispatchNumber);
                            hashTable.Add("@ReleaseFundAmount", strReleaseAmount);
                            hashTable.Add("@Description", string.Concat("'", strHistoryComments.Replace("'", string.Empty), "'"));
                            hashTable.Add("@UserID", strUserID);

                            objDataAccess.ExecuteSp(strUpdateDispatchProc, hashTable, "APD");
                        }
                        else //no manual form destination
                        {
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReleaseFunds", string.Concat(PROC_NAME, "No Manual Payment Request destination."), string.Concat("ParamXML: ", strXML));

                            objReleasePaymentXML = null;
                        }
                    }
                    strReleaseFunds = 0;
                }

                mobjMLAPDAccounting.TerminateGlobals();

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CBilling.cs - ReleaseFunds", "Process completed.", string.Concat("Vars: ParamXML: ", strXML));
            }
            catch (Exception ex)
            {
                blnIngresTransaction = false;
                blnAPDTransaction = false;
                strReleaseFunds = -1;
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accouting.CBilling.cs - ReleaseFunds", "ERROR Details", string.Concat("Error Details: ", ex.Message, "Trace :", ex.StackTrace));
                throw ex;
            }
            finally
            {
                /* Either commit the transaction or roll it back */
                if (blnIngresTransaction)
                    dbLayer.CommitTransaction();
                else
                    dbLayer.RollbackTransaction();

                //Commit or Rollback the SQL DB process
                if (blnAPDTransaction)
                    sqlTransaction.Commit();
                else
                    sqlTransaction.Rollback();

                blnIngresTransaction = false;
                blnAPDTransaction = false;

                /* Dispose of the data layer */
                dbLayer.Dispose();
                if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
                    sqlConnection.Close();
                sqlTransaction.Dispose();

                objIngresAccessor = null;
                m_blnReleaseFundsMode = false;
                objWriteLog = null;
            }
            return strReleaseFunds;
        }

        /// <summary>
        /// Release invoice
        /// </summary>
        /// <param name="strReleaseXML"></param>
        /// <returns></returns>
        public int ReviseInvoice(string strReleaseXML)
        {
            XmlDocument objParamXML = null;
            XmlDocument objXML = null;
            XmlNodeList objInvoiceNodes = null;
            XmlElement objInvoiceNode = null;
            XmlNode passNodeClaimAspectID = null;
            XmlNode passRevisedAmount = null;
            XmlNode passUserID = null;
            XmlNode passDispatchNumber = null;
            XmlNode passOldInvoiceIDFee = null;
            XmlNode passNewFeeID = null;
            XmlNode passNewFeeServices = null;
            XmlNode sendLynxID = null;
            XmlNode sendDispatchNumber = null;
            XmlNode sendTotalAmount = null;
            XmlNode sendFeeAmount = null;
            XmlNode sendInvoiceNodes = null;
            DocumentMgr.CDocMgr objDocMgr = null;

            string strXML = string.Empty,
            strParamXML = string.Empty,
             strFeeAmount = string.Empty,
             strNewFeeServices = string.Empty,
             strNewFeeID = string.Empty,
             strOldInvoiceIDFee = string.Empty,
             strUserID = string.Empty,
             strRevisedAmount = string.Empty,
             strClaimAspectID = string.Empty,
             strEmailBody = string.Empty,
             strEmailSubject = string.Empty,
             strEmailFrom = string.Empty,
             strManualFormRequestTo = string.Empty,
             strTotalAmount = string.Empty,
             strDispatchNumber = string.Empty,
             strLynxID = string.Empty,
             strInvoiceNode = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            int intReturn = 0;
            try
            {
                objIngresAccessor = new objDataAccessor.CDataAccessor();
                mobjMDomUtils = new MDomUtils();
                mobjMLAPDAccounting = new MLAPDAccounting();
                objWriteLog = new WriteLog();

                string PROC_NAME = string.Concat(MODULE_NAME, "ReviseInvoice: ");

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CBilling.cs - ReviseInvoice", "Process Started.", string.Concat("Vars: ParamXML: ", strReleaseXML));

                if (!m_blnReleaseFundsMode)
                    mobjMLAPDAccounting.InitializeGlobals();

                //Check passed parameters.
                if (string.IsNullOrEmpty(strReleaseXML.Trim()))
                    throw new Exception("Nothing passed in XML string.");

                if (strReleaseXML.Contains("<ReviseInvoice>![CDATA["))
                    strReleaseXML = strReleaseXML.Replace("<ReviseInvoice>![CDATA[", string.Empty);
                if (strReleaseXML.Contains("]]</ReviseInvoice>"))
                    strReleaseXML = strReleaseXML.Replace("]]</ReviseInvoice>", string.Empty);

                objParamXML = new XmlDocument();
                mobjMDomUtils.LoadXml(ref objParamXML, ref strReleaseXML, string.Empty, string.Empty);

                //strRevisedAmount = objParamXML.DocumentElement.GetAttribute("revisedAmount").ToString();
                //strUserID = objParamXML.DocumentElement.GetAttribute("userID").ToString();
                //strDispatchNumber = objParamXML.DocumentElement.GetAttribute("dispatchNumber").ToString();
                //strOldInvoiceIDFee = objParamXML.DocumentElement.GetAttribute("oldInvoiceIDFee").ToString();
                //strNewFeeID = objParamXML.DocumentElement.GetAttribute("newFeeID").ToString();
                //strNewFeeServices = objParamXML.DocumentElement.GetAttribute("userID").ToString();

                passNodeClaimAspectID = objParamXML.SelectSingleNode("/Root/@claimAspectID");
                passRevisedAmount = objParamXML.SelectSingleNode("/Root/@revisedAmount");
                passUserID = objParamXML.SelectSingleNode("/Root/@userID");
                passDispatchNumber = objParamXML.SelectSingleNode("/Root/@dispatchNumber");
                passOldInvoiceIDFee = objParamXML.SelectSingleNode("/Root/@oldInvoiceIDFee");
                passNewFeeID = objParamXML.SelectSingleNode("/Root/@newFeeID");
                passNewFeeServices = objParamXML.SelectSingleNode("/Root/@newFeeServices");

                if (passNodeClaimAspectID != null)
                    strClaimAspectID = mobjMDomUtils.GetChildNodeText(ref passNodeClaimAspectID, "/Root/@claimAspectID", false);
                if (passRevisedAmount != null)
                    strRevisedAmount = mobjMDomUtils.GetChildNodeText(ref passRevisedAmount, "/Root/@revisedAmount", false);
                if (passUserID != null)
                    strUserID = mobjMDomUtils.GetChildNodeText(ref passUserID, "/Root/@userID", false);
                if (passDispatchNumber != null)
                    strDispatchNumber = mobjMDomUtils.GetChildNodeText(ref passDispatchNumber, "/Root/@dispatchNumber", false);
                if (passOldInvoiceIDFee != null)
                    strOldInvoiceIDFee = mobjMDomUtils.GetChildNodeText(ref passOldInvoiceIDFee, "/Root/@oldInvoiceIDFee", false);
                if (passNewFeeID != null)
                    strNewFeeID = mobjMDomUtils.GetChildNodeText(ref passNewFeeID, "/Root/@newFeeID", false);
                if (passNewFeeServices != null)
                    strNewFeeServices = mobjMDomUtils.GetChildNodeText(ref passNewFeeServices, "/Root/@newFeeServices", false);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReviseInvoice", "Debug Parameters", string.Concat("  strClaimAspectID = ", strClaimAspectID,
                     "  strRevisedAmount = ", strRevisedAmount,
                     "  strUserID = ", strUserID,
                     "  strDispatchNumber = ", strDispatchNumber,
                     "  strOldInvoiceIDFee = ", strOldInvoiceIDFee,
                     "  strNewFeeID = ", strNewFeeID,
                     "  strNewFeeServices = ", strNewFeeServices));

                //Check passed parameters.
                if (string.IsNullOrEmpty(strClaimAspectID.Trim()))
                    throw new Exception("Invalid Claim Aspect ID.");
                if (string.IsNullOrEmpty(strRevisedAmount.Trim()))
                    throw new Exception("Invalid Revised Amount.");
                if (string.IsNullOrEmpty(strUserID.Trim()))
                    throw new Exception("Invalid User ID.");
                //mobjMLAPDAccounting.g_objEvents.Assert(strClaimAspectID != string.Empty, "Invalid Claim Aspect ID.");
                ////g_objEvents.Assert strDispatchNumber <> "", "Invalid Dispatch Number."
                //mobjMLAPDAccounting.g_objEvents.Assert(strRevisedAmount != string.Empty, "Invalid Revised Amount.");
                //mobjMLAPDAccounting.g_objEvents.Assert(strUserID != string.Empty, "Invalid User ID.");

                mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);



                //revise the recent invoice amount
                htParams = new Hashtable();
                htParams.Add("@ClaimAspectID", strClaimAspectID);
                if (!string.IsNullOrEmpty(strDispatchNumber.Trim()))
                    htParams.Add("@DispatchNumber", strDispatchNumber);
                else
                    htParams.Add("@DispatchNumber", DBNull.Value);
                htParams.Add("@RevisedIndemnityAmount", strRevisedAmount);
                if (!string.IsNullOrEmpty(strOldInvoiceIDFee.Trim()))
                    htParams.Add("@OldInvoiceIDFee", strOldInvoiceIDFee);
                else
                    htParams.Add("@OldInvoiceIDFee", DBNull.Value);
                if (!string.IsNullOrEmpty(strNewFeeID.Trim()))
                    htParams.Add("@NewFeeID", strNewFeeID);
                else
                    htParams.Add("@NewFeeID", DBNull.Value);
                if (!string.IsNullOrEmpty(strNewFeeServices.Trim()))
                    htParams.Add("@NewFeeServices", string.Concat("'", strNewFeeServices, "'"));
                else
                    htParams.Add("@NewFeeServices", DBNull.Value);
                htParams.Add("@UserID", strUserID);
                objIngresAccessor.ExecuteSp(mobjMLAPDAccounting.GetConfig("Accounting/ReviseInvoiceSP"), htParams, "APD");
                //objIngresAccessor.ExecuteSp(mobjMLAPDAccounting.GetConfig("Accounting/ReviseInvoiceSP"), strClaimAspectID, strDispatchNumber, strRevisedAmount, strOldInvoiceIDFee,
                //                                strNewFeeID, string.Concat("'", strNewFeeServices), "'", strUserID);

                mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);

                //create a revised invoicet
                htParams = new Hashtable();
                htParams.Add("@ClaimAspectID", strClaimAspectID);
                if (!string.IsNullOrEmpty(strDispatchNumber.Trim()))
                    htParams.Add("@DispatchNumber", strDispatchNumber);
                else
                    htParams.Add("@DispatchNumber", DBNull.Value);
                htParams.Add("@UserID", strUserID);

                strXML = objIngresAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/RevisedPickupBillingSP"), htParams, "APD");

                if (!string.IsNullOrEmpty(strXML))
                {
                    m_strRevisedInvoiceXML = strXML;
                    objDocMgr = new CDocMgr();

                    // Build xml input required by the DocumentMgr.GenerateInvoiceDocument function.
                    strParamXML = "<RevisedInvoice ProtectDocument='true' DocumentType='Invoice' />";

                    //Call DocumentMgr to create invoice document and attach that document to the claim.
                    objDocMgr.GenerateInvoiceDocument(strParamXML, strXML);

                    //Send Email notification
                    strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                    strManualFormRequestTo = mobjMLAPDAccounting.GetConfig("Accounting/Payment/ManualPayment/@ToEmail");

                    objXML = new XmlDocument();
                    mobjMDomUtils.LoadXml(ref objXML, ref strXML, string.Empty, string.Empty);
                    sendLynxID = objXML.SelectSingleNode("/Root/Claim/@LynxID");
                    sendDispatchNumber = objXML.SelectSingleNode("/Root/Claim/Exposure/Invoice/@DispatchNumber");
                    sendTotalAmount = objXML.SelectSingleNode("/Root/@TotalInvoiceAmount");
                    sendFeeAmount = objXML.SelectSingleNode("//Invoice[@ItemType='Fee']/@Amount");

                    strLynxID = mobjMDomUtils.GetChildNodeText(ref sendLynxID, "/Root/Claim/@LynxID", true);
                    strDispatchNumber = mobjMDomUtils.GetChildNodeText(ref sendDispatchNumber, "/Root/Claim/Exposure/Invoice/@DispatchNumber", true);
                    strTotalAmount = mobjMDomUtils.GetChildNodeText(ref sendTotalAmount, "/Root/@TotalInvoiceAmount", true);

                    if (strNewFeeID != string.Empty)
                        strFeeAmount = mobjMDomUtils.GetChildNodeText(ref sendFeeAmount, "//Invoice[@ItemType='Fee']/@Amount", false);
                    else
                        strFeeAmount = "NO CHANGE";

                    if (!string.IsNullOrEmpty(strManualFormRequestTo))
                    {
                        strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                        strEmailSubject = string.Concat("APD Revised Invoice for LYNX ID: ", strLynxID);
                        strEmailBody = string.Concat("             APD REVISED INVOICE CREATED            ", Environment.NewLine,
                                        "             ===========================             ", Environment.NewLine, Environment.NewLine,
                                        "             LYNX ID: ", strLynxID, Environment.NewLine,
                                        "     Dispatch Number: ", strDispatchNumber, Environment.NewLine,
                                        "      Revised Amount: ", strTotalAmount, Environment.NewLine,
                                        "  Revised Fee Amount: ", strFeeAmount, Environment.NewLine, Environment.NewLine, Environment.NewLine,
                                        "     AMOUNT DESCRIPTION", Environment.NewLine,
                                        "----------- -----------------------------------------------------------", Environment.NewLine);
                        sendInvoiceNodes = objXML.SelectSingleNode("//Invoice");

                        objInvoiceNodes = mobjMDomUtils.GetChildNodeList(ref sendInvoiceNodes, "//Invoice", true);

                        foreach (XmlNode tempobjInvoiceNode in objInvoiceNodes)
                        {
                            objInvoiceNode = (XmlElement)tempobjInvoiceNode;

                            strInvoiceNode = Convert.ToString(objInvoiceNode.GetAttribute("Amount"));
                            strEmailBody = string.Concat(strEmailBody, String.Format(String.Format(strInvoiceNode, "$#####0.00"), "@@@@@@@@@@@ "));

                            if (!string.IsNullOrEmpty(objInvoiceNode.GetAttribute("PayeeName")))
                                strEmailBody = string.Concat(strEmailBody, Convert.ToString(objInvoiceNode.GetAttribute("PayeeName")), Environment.NewLine);
                            else
                                strEmailBody = string.Concat(strEmailBody, Convert.ToString(objInvoiceNode.GetAttribute("InvoiceDescription")), Environment.NewLine);
                        }

                        strEmailBody = string.Concat(strEmailBody,
                                        "===========", Environment.NewLine,
                                       String.Format(String.Format(strTotalAmount, "$#####0.00"), "@@@@@@@@@@@ "), "TOTAL", Environment.NewLine,
                                        "===========", Environment.NewLine);

                        //generate the custom form request

                        if (isExtensiveDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CBilling.cs - ReviseInvoice", "Debug Parameters", string.Concat(PROC_NAME, "Revised Invoice Notification:",
                                                            " From: ", strEmailFrom,
                                                            " To: ", strManualFormRequestTo, Environment.NewLine,
                                                            " Subject: ", strEmailSubject, Environment.NewLine,
                                                            " Body: ", strEmailBody));

                        mobjMLAPDAccounting.g_objEvents.SendEmail(strEmailFrom, strManualFormRequestTo, strEmailSubject, strEmailBody, false, 2);
                    }
                }
                if (!m_blnReleaseFundsMode)
                    mobjMLAPDAccounting.TerminateGlobals();
                intReturn = 0;
            }
            catch (Exception ex)
            {
                throw ex;
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accouting.CBilling.cs - ReviseInvoice", "ERROR Details", string.Concat("Error Details: ", ex.Message, "Trace :", ex.StackTrace, "ParamXML: ", strReleaseXML));
                intReturn = -1;
            }
            finally
            {
                if (objDocMgr != null)
                    Marshal.ReleaseComObject(objDocMgr);
                objDocMgr = null;
                objXML = null;
                objInvoiceNodes = null;
                objInvoiceNode = null;
                objWriteLog = null;
            }
            return intReturn;
        }

        public string TestIngres(long lDispatchNo, string sWebUidNo, long lUserIdNo, int iUserTypeNo, string sEnvId, string sStylesheet = "")
        {
            string sHeader = string.Empty,
                 sFooter = string.Empty;
            bool bIVR;
            string sOutput;

            APDDataAccessor.CDataAccessor objDataAccess = new objDataAccessor.CDataAccessor();

            DbTransaction dbTransaction = null;

            string sRetVal = string.Empty,
                   sIncoID = string.Empty;
            string sUserIDNo = string.Empty; // 11/09/07 Format UserID to w00####.
            string sStoredProc = string.Empty; // 10/30/08 Added for profile check.
            string sIncoCmntInd = string.Empty; // 10/30/08 Added for profile check.

            string sUID = string.Empty,
                sUSessID = string.Empty;
            string sInsID = string.Empty;
            long sProfileUID;

            string commandText = string.Empty;
            bool commitTransaction = true;

            LynxServices.Library.Data.Parameters parameters = null;
            DataLayer dbLayer = null;
            try
            {	 // Object initiallization. 
                //   oXMLDom = new FreeThreadedDOMDocument40();

                parameters = new LynxServices.Library.Data.Parameters();
                // objIngresConnectionString = new IngresConnectionString();

                dbLayer = new DataLayer(objDataAccess.GetConnectionString("Ingres"), DataLayer.DatabaseType.Ingres);

                // 11/09/07 Format UserID to w00####.
                sUserIDNo = string.Concat("w", Right(string.Concat("000000", Convert.ToString(lUserIdNo)), 6));

                // 11/29/07 - Add check to see if insurance company is State Farm (#91).
                // 12/18/08 - Modified for other insuranse companies.
                if (Left((Right(sWebUidNo, 4)), 1) == "*")
                {
                    //sInsID = Replace(Right(sWebUidNo, 4), "*", "")
                    sInsID = Right(sWebUidNo, 4).Replace("*", "");
                    sUSessID = Left(sWebUidNo, sWebUidNo.Length - 4);
                }
                else
                {
                    sInsID = "0";
                    sUSessID = sWebUidNo;
                }

                // Check Company profile to determine if Comments should be shown.
                sStoredProc = "dp_get_profile_item";
                sUID = Convert.ToString(lUserIdNo);
                sProfileUID = Convert.ToInt32(sInsID); // insurance company uid #  (i.e. 23 for Allstate or 91 for SF).

                commandText = string.Concat("{? = call ", sStoredProc, " (?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                parameters.AddReturnParameter("retval", DbType.Int32);
                parameters.AddInputOutputParameter("h_msg", "", DbType.String, 200);
                parameters.AddInputOutputParameter("h_msg_ind", "", DbType.String, 1);
                parameters.AddInputOutputParameter("h_user_id_no", sUID, DbType.String, 32);
                parameters.AddInputOutputParameter("h_user_session_id", sUSessID, DbType.String, 32);
                parameters.AddInputOutputParameter("h_attrib_amt", 0, DbType.Currency);
                parameters.AddInputOutputParameter("h_attrib_cd", "", DbType.String, 50);
                parameters.AddInputOutputParameter("h_attrib_dt", DateTime.Now.ToString("MMM/dd/yyyy"), DbType.Date, 0);
                parameters.AddInputOutputParameter("h_attrib_flt_no", 0, DbType.Int32);
                parameters.AddInputOutputParameter("h_attrib_nm", "web_comments_display_ind", DbType.String, 32);
                parameters.AddInputOutputParameter("h_attrib_no", 0, DbType.Int32, 1);
                parameters.AddInputParameter("h_commit_ind", "Y", DbType.String, 1);
                parameters.AddInputParameter("h_profile_object", "inco", DbType.String, 32);
                parameters.AddInputParameter("h_profile_uid_no", Convert.ToInt32(sProfileUID), DbType.Int32, 1);

                // Setup the connection.
                dbTransaction = dbLayer.BeginTransaction();
                dbLayer.CommandTimeout = 90;
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, dbTransaction);

                // Indicator used to determine if Insurance companies are able to view dispatch status
                sIncoCmntInd = Convert.ToString(parameters["h_attrib_cd"].Value);


            }
            catch (Exception ex)
            {
                commitTransaction = false;
                throw ex;
            }
            finally
            {
                // Either commit the transaction or roll it back
                if (commitTransaction)
                    dbLayer.CommitTransaction();
                else
                    dbLayer.RollbackTransaction();

                // Dispose of the data layer 
                dbLayer.Dispose();
            }
            return sIncoCmntInd;
        }
    }
}
