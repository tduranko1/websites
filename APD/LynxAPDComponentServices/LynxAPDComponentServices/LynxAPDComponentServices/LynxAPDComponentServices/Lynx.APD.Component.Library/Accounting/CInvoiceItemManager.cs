﻿using System;
using System.Xml;
using System.Collections;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data;
using Lynx.APD.Component.Library.Common;
using dataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using LynxServices.Library.Data;
using System.Data.Common;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    /// Module    : CInvoiceItemManager
    /// DateTime  : 3/21/2005 14:19
    /// Author    : Dan Price
    /// Purpose   : This is a collection object whose purpose is to build and maintain a collection
    ///             of InvoiceItem objects. This object implements the business rules governing how
    ///             InvoiceItems are constructed from Invoice records from the APD database.  A
    ///             typical InvoiceItem object may be composed of several Invoice records including
    ///             multiple Payment records and a Handling Fee record.  This object also provides
    ///             the ability to ship the InvoiceItems, once constructed, to Ingres and sync the
    ///             APD database to Ingres with the returned Dispatch numbers.
    /// </summary>
    public class CInvoiceItemManager
    {
        #region Declarations
        private string MODULE_NAME = "clsInvoiceItemManager.";

        private Collection mcolInvoiceItems = new Collection();
        private XmlDocument mobjParamDOM;
        private CInvoiceItem mobjHandlingFeeInvoiceItem;
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
        Common.WriteLog objWriteLog = null;

        private string mstrInitialPayment;
        private string mstrInsuranceCompanyID;
        private string mstrEarlyBillFlag;
        private string mstrReleaseFundInd;
        private string mstrWarrantyExistsFlag;
        private string mstrAmount;
        private string mstrAdminAmount;
        private string mstrIngresAccountingID;
        private long mlngInvoiceID;
        private string mstrPayeeID;
        private string mstrPayeeName;
        private string mstrPayeeType;
        private string mstrItemType;
        private string mstrNTUserID;
        private string mstrUserID;
        private string mstrLynxID;
        private string mstrCarrierClaimNumber;
        private string mstrComment;
        private string mstrInsuredNameFirst;
        private string mstrInsuredNameLast;
        private string mstrInsuredBusinessName;
        private string mstrClaimantNameFirst;
        private string mstrClaimantNameLast;
        private string mstrClaimantBusinessName;
        private string mstrCarrierRepNameFirst;
        private string mstrCarrierRepnameLast;
        private string mstrHandlingPayeeID;
        private string mstrAPDPayeeID;
        private long mlngClaimAspectID;
        private double mdblInvoiceTotal;
        private int mintInvoiceItemID;
        private int mintInvoiceType;
        private bool mblnHandlingFeeIndemnityFlagged;
        private string mstrDeductibleAmt;
        private string mstrTaxTotalAmt;

        MDomUtils objMdomUtils = null;
        // CDataAccessor objIngresAccessor = null;
        MLAPDAccounting objMLAPDAccounting = null;
        dataAccessor.CDataAccessor objDataAccessor = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["AccoutingExtensiveDebug"]);
        #endregion

        #region Enumerators
        /// <summary>
        /// Enum for file type.
        /// </summary>
        public enum eInvoiceType : ulong
        {
            eFile,
            ePaper
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="intInvoiceType"></param>
        public void Initialize(string strParamXML, eInvoiceType intInvoiceType)
        {
            string PROC_NAME = string.Empty;

            // Objects that need to be destroyed.
            XmlNodeList objExposureList = null;
            XmlNodeList objInvoiceList = null;
            CInvoiceItem objInvoiceItem = null;
            XmlElement objElem = null;
            bool blnPaymentAdded;
            bool blnHandlingFeeAdded;
            // Primitives
            string strDispatchNumber;
            string strPayeeName;
            string strPayeeAddress;
            string strPayeeCity;
            string strPayeeState;
            bool blnLastIndemnityAssignment;
            bool blnClaimAspectPaymentExists;

            try
            {
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - Initialize", "Process started.", "");
                PROC_NAME = string.Concat(MODULE_NAME, "Initialize");
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();
                // Initialization.
                mintInvoiceType = Convert.ToInt32(intInvoiceType);
                mdblInvoiceTotal = 0;
                mintInvoiceItemID = 0;

                // Load the parameter XML into a DOM object
                objMdomUtils.LoadXml(ref mobjParamDOM, ref strParamXML, PROC_NAME, "Invoice");

                // Retrieve Root level attributes.
                mstrNTUserID = mobjParamDOM.SelectSingleNode("//@NTUserID").InnerText;
                mstrUserID = mobjParamDOM.SelectSingleNode("//@UserID").InnerText;
                mstrIngresAccountingID = mobjParamDOM.SelectSingleNode("//@IngresAccountingID").InnerText;
                mstrInsuranceCompanyID = mobjParamDOM.SelectSingleNode("//@InsuranceCompanyID").InnerText;
                mstrEarlyBillFlag = mobjParamDOM.SelectSingleNode("//@EarlyBillFlag").InnerText;
                mstrWarrantyExistsFlag = mobjParamDOM.SelectSingleNode("//@WarrantyExistsFlag").InnerText;
                mstrReleaseFundInd = mobjParamDOM.SelectSingleNode("//@ReleaseFundsFlag").InnerText;
                mstrReleaseFundInd = (mstrReleaseFundInd == "1" ? "Y" : "N");

                // Loop through each Claim node.
                foreach (XmlNode objClaimNode in mobjParamDOM.SelectNodes("/Root/Claim"))
                {
                    // Retrieve Claim level attributes.
                    mstrLynxID = objClaimNode.SelectSingleNode("./@LynxID").InnerText;
                    mstrCarrierClaimNumber = objClaimNode.SelectSingleNode("./@CarrierClaimNumber").InnerText;
                    mstrInsuredNameFirst = objClaimNode.SelectSingleNode("./@InsuredNameFirst").InnerText;
                    mstrInsuredNameLast = objClaimNode.SelectSingleNode("./@InsuredNameLast").InnerText;
                    mstrInsuredBusinessName = objClaimNode.SelectSingleNode("./@InsuredBusinessName").InnerText;
                    mstrCarrierRepNameFirst = objClaimNode.SelectSingleNode("./@CarrierRepNameFirst").InnerText;
                    mstrCarrierRepnameLast = objClaimNode.SelectSingleNode("./@CarrierRepNameLast").InnerText;

                    // Retrieve Exposure nodes.
                    objExposureList = objClaimNode.SelectNodes("./Exposure");

                    // Loop through each Exposure node.
                    foreach (XmlNode objExposureNode in objExposureList)
                    {
                        // Ensure no Handling Fee is hanging around from a previous Exposure.
                        mobjHandlingFeeInvoiceItem = null;

                        // Retrieve Exposure level properties.
                        mstrClaimantNameFirst = objExposureNode.SelectSingleNode("./@ClaimantNameFirst").InnerText;
                        mstrClaimantNameLast = objExposureNode.SelectSingleNode("./@ClaimantNameLast").InnerText;
                        mstrClaimantBusinessName = objExposureNode.SelectSingleNode("./@ClaimantBusinessName").InnerText;
                        mstrComment = objExposureNode.SelectSingleNode("./@Vehicle").InnerText.ToUpper();
                        mstrHandlingPayeeID = objExposureNode.SelectSingleNode("./@HandlingShopLocationID").InnerText;
                        mstrAPDPayeeID = objExposureNode.SelectSingleNode("./@APDShopLocationID").InnerText;
                        mlngClaimAspectID = Convert.ToInt64(objExposureNode.SelectSingleNode("./@ClaimAspectID").InnerText);

                        // Reinitialize for each exposure. Indicates whether at least 1 indemnity payment exists for this exposure.
                        blnClaimAspectPaymentExists = false;
                        // Reinitialize for each exposure. Indicates whether an indemnity to which the handling fee should be attached has been located and flagged.
                        mblnHandlingFeeIndemnityFlagged = false;
                        // Retrieve the Invoice nodes.
                        objInvoiceList = objExposureNode.SelectNodes("./Invoice");
                        // Loop through the Invoice nodes.
                        foreach (XmlNode objInvoiceNode in objInvoiceList)
                        {
                            // Retrieve Invoice level properties.
                            mstrItemType = objInvoiceNode.SelectSingleNode("./@ItemType").InnerText;
                            mstrAmount = objInvoiceNode.SelectSingleNode("./@Amount").InnerText;
                            mstrAdminAmount = objInvoiceNode.SelectSingleNode("./@AdminFeeAmount").InnerText;
                            mstrDeductibleAmt = objInvoiceNode.SelectSingleNode("./@DeductibleAmt").InnerText;
                            mstrTaxTotalAmt = objInvoiceNode.SelectSingleNode("./@TotalTaxAmt").InnerText;
                            mlngInvoiceID = Convert.ToInt64(objInvoiceNode.SelectSingleNode("./@InvoiceID").InnerText);
                            mstrPayeeID = objInvoiceNode.SelectSingleNode("./@PayeeID").InnerText;
                            mstrPayeeName = objInvoiceNode.SelectSingleNode("./@PayeeName").InnerText;
                            mstrPayeeType = objInvoiceNode.SelectSingleNode("./@PayeeType").InnerText;
                            mstrInitialPayment = objInvoiceNode.SelectSingleNode("./@InitialPayment").InnerText;

                            // Process Payments.
                            if (mstrItemType != "Fee")
                            {
                                // Initialize value that determines whether payment was added to an existing InvoiceItem or should constitute a new InvoiceItem.
                                blnPaymentAdded = false;

                                // If there is an existing InvoiceItem for this Payee add the payment amount to that InvoiceItem.
                                if (isDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", "Calling AddToExistingInvoiceItem() If there is an existing InvoiceItem for this Payee add the payment amount to that InvoiceItem.", "");

                                blnPaymentAdded = AddToExistingInvoiceItem();

                                // If the Payment amount has not already been added to an existing InvoiceItem.
                                if (!blnPaymentAdded)
                                {
                                    if (isDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", "Adding Payment as new line item/dispatch for Claim Aspect:", string.Concat("Vars: ClaimAspectID = ", mlngClaimAspectID));
                                    // Create a new InvoiceItem from the InvoiceNode
                                    objInvoiceItem = CreateInvoiceItem(objInvoiceNode);

                                    // Add the new InvoiceItem to the InvoiceItems collection.
                                    AppendInvoiceItem(objInvoiceItem);
                                }
                                // This indicates at least one payment exists for this exposure.
                                blnClaimAspectPaymentExists = true;
                            }
                            // Process Fees.
                            else
                            {
                                // Create a new InvoiceItem from the InvoiceNode
                                objInvoiceItem = CreateInvoiceItem(objInvoiceNode);

                                // Determine if this Fee is a Handling Fee.
                                if (objInvoiceNode.SelectSingleNode("./@FeeCategoryCD").InnerText == "H")
                                {
                                    if (isDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", string.Concat("Handling Fee found for Claim Aspect: ", objInvoiceItem.ClaimAspectID, ". Making copy to be attached to indemnity."), "");
                                    // Save a copy of the Handling Fee to be added later to most recent shop payee's Indemnity Payment.
                                    // Exactly 1 handling fee is required.
                                    mobjHandlingFeeInvoiceItem = CopyInvoiceItem(objInvoiceItem);
                                }
                                else
                                {
                                    if (isDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", string.Concat("Non-Handling Fee found for Claim Aspect: ", objInvoiceItem.ClaimAspectID, ". Adding fee as separate line item/dispatch."), "");

                                    //' Add the new InvoiceItem to the InvoiceItems collection.
                                    AppendInvoiceItem(objInvoiceItem);
                                }
                            }
                        }
                        // This attribute is required when creating a paper Invoice document.
                        if (objInvoiceItem != null)
                        {
                            XmlElement objXmlElemAppend = (XmlElement)objExposureNode;
                            objMdomUtils.AddAttribute(ref objXmlElemAppend, "Claimant", objInvoiceItem.Claimant);
                        }
                        // If there is a handling fee, it must be added to an existing payment if appropriate or added to
                        // the collection as a separate InvoiceItem.
                        if (mobjHandlingFeeInvoiceItem != null)
                        {
                            blnHandlingFeeAdded = false;

                            // Find last indemnity payee and add the handling fee info.
                            foreach (CInvoiceItem objInvoice in mcolInvoiceItems)
                            {
                                // Limit this search to InvoiceItems for the current ClaimAspectID.
                                if (objInvoice.ClaimAspectID == mlngClaimAspectID)
                                {
                                    // One payee should be flagged as the last Shop to be payed an Indemnity.
                                    if (objInvoice.HandlingFeeGoesHere)
                                    {
                                        if (isDebug)
                                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", string.Concat("Adding handling fee to line item containing payment to Payee ID: ", objInvoice.PayeeID), "");

                                        // If found add the Handling fee.
                                        objInvoice.Fee = mobjHandlingFeeInvoiceItem.Fee;
                                        objInvoice.adminFee = mobjHandlingFeeInvoiceItem.adminFee;
                                        objInvoice.TaxTotalAmt = mobjHandlingFeeInvoiceItem.TaxTotalAmt;

                                        // Also add the Handling Fee's InvoiceID to the InvoiceItems's InvoiceIDs collection.
                                        objInvoice.AddInvoiceID = mobjHandlingFeeInvoiceItem.FirstInvoiceID;

                                        // Set the ClientFeeCode from the fee item
                                        objInvoice.ClientFeeCode = mobjHandlingFeeInvoiceItem.ClientFeeCode;

                                        // Set the flag to indicate the Handling fee has been handled.
                                        blnHandlingFeeAdded = true;
                                        // This ride is making me dizzy, let me off.
                                        break;
                                    }
                                }
                            }
                            // If no indemnity payment could be found to attach this Handling fee to...
                            if (!blnHandlingFeeAdded)
                            {
                                if (isDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", string.Concat("Handling fee being added as separate line item/dispatch. InvoiceID: ", mobjHandlingFeeInvoiceItem.FirstInvoiceID), "");

                                // ... then add the Handling fee as a separate item.
                                AppendInvoiceItem(mobjHandlingFeeInvoiceItem);
                                // If there is at least 1 payment for this exposure, then the handling fee should have been attached to it.  Find out why it was not.
                            }
                        }
                    }
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - Initialize", "This attribute is required when creating a paper Invoice document.", "");
                // This attribute is required when creating a paper Invoice document.
                objElem = mobjParamDOM.DocumentElement;
                objMdomUtils.AddAttribute(ref objElem, "TotalInvoiceAmount", GetCurrencyFormattedString(mdblInvoiceTotal));
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CInvoiceItemManager.cs - Initialize", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        ///  Procedure : SendToIngres
        ///  DateTime  : 3/16/2005 09:29
        ///  Author    : Dan Price
        ///  Purpose   : Send each InvoiceItem in the collection to Ingres, retrieve a Dispatch number,
        ///              update the APD Invoice records with the Dispatch number, and update the
        ///              InvoiceItem object with the Dispatch number.  This ensures that the APD
        ///              database is in sync with Ingres and that the Dispatch number will be available
        ///              when an Invoice file or document is created.
        /// </summary>
        /// <returns></returns>
        internal int SendToIngres()
        {
            string PROC_NAME = string.Empty;

            // Objects that must be destroyed.
            Collection colInvoiceIDs = null;
            // Primitives
            long lngNum = 0;
            string strSrc = string.Empty;
            string strDesc = string.Empty;
            string strReturnValue = string.Empty;
            string strDispatchTemp = string.Empty;
            string strMessageText = string.Empty;
            string strMessageInd = string.Empty;
            string strUpdateDispatchProc = string.Empty;
            string strReleaseFundsUpdateDispatchProc = string.Empty;
            string strNowSqlDateTime = string.Empty;
            string strLynxID = string.Empty;
            string strPayeeID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayment = string.Empty;
            string strClaimNumber = string.Empty;
            string strComment = string.Empty;
            string strDispatchNumber = string.Empty;
            string strFee = string.Empty;
            string strRetailerFee = string.Empty;
            string strInsuredNameFirst = string.Empty;
            string strInsuredNameLast = string.Empty;
            string strInsuredBusinessName = string.Empty;
            string strCarrierRepName = string.Empty;
            string strIngresProc = string.Empty;
            string strDeductibleAmt = string.Empty;
            string strTaxTotalAmt = string.Empty;
            string strReleaseFundAmt = string.Empty;
            string strWarrantyInd = string.Empty;
            string strHistoryComments = string.Empty;
            string strIngresErrorCode = string.Empty;
            string strIngresMessageText = string.Empty;
            string strRoot = string.Empty;
            long lngIngresErrorNumber;

            bool blnIngresTransaction = false;
            bool blnAPDTransaction = false;
            int intReturnVal = 0;
            bool blnIngresErrorElectronicBilling = false;

            XmlDocument objIngresParams = null;
            XmlElement objIngresParamsRoot = null;
            string strImageRoot = string.Empty;
            string strFileName = string.Empty;
            double dblResult;
            bool isValid = false;
            DataLayer dbLayer = null;
            LynxServices.Library.Data.Parameters parameters = null;
            DbTransaction transaction = null;
            string commandText = string.Empty;
            Hashtable hashTable = null;
            bool commitTransaction = true;
            UInt64 errorCode, ingresErrorCode;
            try
            {
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - Initialize", "Process started.", "");
                PROC_NAME = string.Concat(MODULE_NAME, "SendToIngres: ");
                objMLAPDAccounting = new MLAPDAccounting();
                objMLAPDAccounting.InitializeGlobals();

                //Get values required by this process.
                strUpdateDispatchProc = objMLAPDAccounting.GetConfig("Accounting/Billing/UpdateDispatchSP");
                strIngresProc = objMLAPDAccounting.GetConfig("Accounting/IngressBillingSP");
                strNowSqlDateTime = objMLAPDAccounting.ConstructSqlDateTime(string.Empty, string.Empty, true);
                strImageRoot = objMLAPDAccounting.GetConfig("Document/RootDirectory");

                if(strImageRoot != "")
                if (strImageRoot.Substring(0, 4) == "\\\\")
                    strImageRoot = Regex.Replace(strImageRoot, "\\\\", "\\", RegexOptions.IgnoreCase);

                // A bad Ingres Accounting ID will lock Ingres up for a few minutes and then cause APD to crash.
                if ((mstrIngresAccountingID.Length) == 0 || mstrIngresAccountingID == "0")
                    throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresRequiresIngresAccountingID, PROC_NAME));


                objMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - Initialize", "Ingres connection establish.", "");
                //Ingres Connection
                blnIngresTransaction = true;
                objDataAccessor = new dataAccessor.CDataAccessor();

                dbLayer = new DataLayer(objDataAccessor.GetConnectionString("Ingres"), DataLayer.DatabaseType.Ingres);

                blnAPDTransaction = true;
                // objMLAPDAccounting.g_objApdRsAccessor.BeginTransaction();

                // Loop through the InvoiceItems collection.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Retrieve the properties from the current InvoiceItem that are required for the Ingres call.
                    strLynxID = objInvoiceItem.LynxID;
                    strPayeeID = objInvoiceItem.PayeeID;
                    strPayeeName = objInvoiceItem.PayeeName;
                    strPayment = objInvoiceItem.Payment;

                    strClaimNumber = objInvoiceItem.ClaimNumber;
                    if (strClaimNumber.Length > 20)
                        strClaimNumber = strClaimNumber.Substring(0, 20);

                    strComment = objInvoiceItem.Comment;
                    if (strComment.Length > 300)
                        strComment = strComment.Substring(0, 300);

                    strDispatchNumber = "0";     // This will need to be pulled from an InvoiceItem property when updates are implemented.
                    strFee = objInvoiceItem.Fee;
                    strRetailerFee = objInvoiceItem.adminFee;
                    strDeductibleAmt = objInvoiceItem.DeductibleAmt;
                    strTaxTotalAmt = objInvoiceItem.TaxTotalAmt;

                    strCarrierRepName = (string.Concat(objInvoiceItem.CarrierRepNameFirst, " ", objInvoiceItem.CarrierRepNameLast).Trim());

                    if (strCarrierRepName.Length > 20)
                        strCarrierRepName = strCarrierRepName.Substring(0, 200);
                    // Use Insured Business Name if available, otherwise use Insured first and last names.
                    strInsuredNameLast = objInvoiceItem.InsuredBusinessName;

                    if (strInsuredNameLast.Length > 20)
                        strInsuredNameLast = strInsuredNameLast.Substring(0, 20);

                    if ((strInsuredNameLast.Length) == 0)
                    {
                        strInsuredNameLast = (objInvoiceItem.InsuredNameLast);

                        if (strInsuredNameLast.Length > 20)
                            strInsuredNameLast = strInsuredNameLast.Substring(0, 20);

                        strInsuredNameFirst = (objInvoiceItem.InsuredNameFirst);

                        if (strInsuredNameFirst.Length > 20)
                            strInsuredNameFirst = strInsuredNameFirst.Substring(0, 20);
                    }
                    else
                    {
                        strInsuredNameFirst = string.Empty;
                    }

                    if (strRetailerFee == string.Empty)
                        strRetailerFee = "0.00";

                    if (mstrEarlyBillFlag == "1")
                        if (mstrReleaseFundInd == "Y")
                            strReleaseFundAmt = strPayment;
                        else
                            strReleaseFundAmt = "0";
                    else
                        strReleaseFundAmt = strPayment;

                    strWarrantyInd = "N";
                    if (mstrWarrantyExistsFlag == "1")
                        strWarrantyInd = "Y";

                    //not implemented yet on AGC side
                    //" h_deductible_amt = " & strDeductibleAmt & " h_tax_total_amt = " & strTaxTotalAmt

                    // Do not send items to Ingres if both amounts are zero.  These do not need to appear on the invoice, and Ingres will blow.
                    if (Convert.ToDouble(strPayment) + Convert.ToDouble(strFee) == 0)
                    {
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "Skipping Ingres call because both payment and fee amounts are zero.", string.Concat("LynxID: ", strLynxID));

                        //change the status of invoice items to FS (invoiced). We need to stuff a dummy dispatch number too
                        strDispatchNumber = "0";

                        // Retrieve the InvoiceIDs associated with this InvoiceItem.
                        colInvoiceIDs = (Collection)objInvoiceItem.InvoiceIDs;

                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "Setting invoice items as invoiced (StatusCD=FS).", "");
                        // Loop through the InvoiceItem's InvoiceIDs collection.
                        foreach (object vInvoiceID in colInvoiceIDs)
                        {
                            hashTable = new Hashtable();
                            hashTable.Add("@InvoiceID", Convert.ToInt64(vInvoiceID));
                            hashTable.Add("@InvoiceDate", strNowSqlDateTime);
                            hashTable.Add("@DispatchNumber", strDispatchNumber);
                            hashTable.Add("@UserID", 0);

                            objDataAccessor.ExecuteSp(strUpdateDispatchProc, hashTable, "APD");
                        }

                        // Remove this InvoiceItem from the collection so that it will not appear in the output.
                        RemoveInvoiceItem(objInvoiceItem.InvoiceItemID);
                        // Continue processing normally.
                    }
                    else
                    {
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "", string.Concat("Vars: ", "h_apd_lynx_id=", strLynxID, " h_apd_shop_id=", strPayeeID,
                                              " h_claim_amt=", strPayment, " h_claim_no=", strClaimNumber,
                                              " h_comment_text=", strComment, " h_commit_ind=1  h_dispatch_no=", strDispatchNumber,
                                              " h_fee_amt=", strFee, " h_rtlr_fee_amt=", strRetailerFee, " h_insrd_first_nm=", strInsuredNameFirst,
                                              " h_insrd_last_nm=", strInsuredNameLast, " h_ins_pc_uid_no=", mstrIngresAccountingID,
                                              " h_job_type_cd=15 h_msg_ind=  h_msg_text=", strCarrierRepName,
                                              " h_tries=1  h_user_id_no=", mstrNTUserID, " h_user_session_no=",
                                              " h_release_fund_amt=", strReleaseFundAmt, " h_release_fund_ind=", mstrReleaseFundInd,
                                              " h_warranty_ind=", strWarrantyInd));
                        //create the Ingres Parameter XML so we can save it later.
                        if (mstrEarlyBillFlag == "1")
                        {
                            strRoot = "<Root/>";
                            objIngresParams = new XmlDocument();
                            objMdomUtils.LoadXml(ref objIngresParams, ref strRoot, PROC_NAME, "SendToIngres");
                            objIngresParamsRoot = objIngresParams.DocumentElement;
                            objIngresParamsRoot.SetAttribute("retval", "0");
                            objIngresParamsRoot.SetAttribute("h_apd_lynx_id", strLynxID);
                            objIngresParamsRoot.SetAttribute("h_apd_shop_id", strPayeeID);
                            objIngresParamsRoot.SetAttribute("h_claim_amt", strPayment);
                            objIngresParamsRoot.SetAttribute("h_claim_no", strClaimNumber);
                            objIngresParamsRoot.SetAttribute("h_comment_text", strComment);
                            //objIngresParamsRoot.SetAttributee "h_comment_ind", ""
                            objIngresParamsRoot.SetAttribute("h_commit_ind", "");
                            objIngresParamsRoot.SetAttribute("h_dispatch_no", strDispatchNumber);
                            objIngresParamsRoot.SetAttribute("h_fee_amt", strFee);
                            objIngresParamsRoot.SetAttribute("h_rtlr_fee_amt", strRetailerFee);
                            objIngresParamsRoot.SetAttribute("h_insrd_first_nm", strInsuredNameFirst);
                            objIngresParamsRoot.SetAttribute("h_insrd_last_nm", strInsuredNameLast);
                            objIngresParamsRoot.SetAttribute("h_ins_pc_uid_no", mstrIngresAccountingID);
                            objIngresParamsRoot.SetAttribute("h_job_type_cd", "15");   // 15 indicates an APD claim.  Later WAG and OCG will require this value to become dynamic.
                            objIngresParamsRoot.SetAttribute("h_msg_ind", "");

                            // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                            objIngresParamsRoot.SetAttribute("h_msg_text", strCarrierRepName);

                            //Early Billing
                            objIngresParamsRoot.SetAttribute("h_release_fund_amt", strReleaseFundAmt);
                            objIngresParamsRoot.SetAttribute("h_release_fund_ind", mstrReleaseFundInd);
                            objIngresParamsRoot.SetAttribute("h_warranty_ind", strWarrantyInd);
                            objIngresParamsRoot.SetAttribute("h_tries", "1");

                            // Pass 'lynxdba' for system batch jobs.
                            objIngresParamsRoot.SetAttribute("h_user_id_no", mstrNTUserID);
                            objIngresParamsRoot.SetAttribute("h_user_session_no", "");
                        }

                        // Send each InvoiceItem to Ingres and retreive the returned Dispatch number.
                        //objIngresAccessor = new CDataAccessor();

                        parameters = new LynxServices.Library.Data.Parameters();

                        if (isDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "Executing Ingres SP", string.Concat("Vars: StoredProc = ", strIngresProc));

                        commandText = string.Concat("{? = call lynxdba.", strIngresProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

                        parameters.AddReturnParameter("retval", DbType.Int32);
                        parameters.AddInputParameter("h_apd_lynx_id", strLynxID, DbType.String, 25);
                        parameters.AddInputParameter("h_apd_shop_id", Convert.ToInt32(strPayeeID), DbType.Int32, 4);
                        parameters.AddInputParameter("h_claim_amt", Convert.ToDouble(strPayment), DbType.Double, 0); //-1
                        parameters.AddInputParameter("h_claim_no", strClaimNumber, DbType.String, 20);
                        parameters.AddInputParameter("h_comment_text", strComment, DbType.String, 300);

                        parameters.AddInputParameter("h_commit_ind", "", DbType.String, 1);
                        parameters.AddInputOutputParameter("h_dispatch_no", Convert.ToInt32(strDispatchNumber), DbType.Int32, 4);
                        parameters.AddInputParameter("h_fee_amt", Convert.ToDouble(strFee), DbType.Double, 0); //-1
                        parameters.AddInputParameter("h_rtlr_fee_amt", Convert.ToDouble(strRetailerFee), DbType.Double, 0); //-1
                        parameters.AddInputParameter("h_insrd_first_nm", strInsuredNameFirst, DbType.String, 20);
                        parameters.AddInputParameter("h_insrd_last_nm", strInsuredNameLast, DbType.String, 20);
                        parameters.AddInputParameter("h_ins_pc_uid_no", Convert.ToInt32(mstrIngresAccountingID), DbType.Int32, 4);
                        parameters.AddInputParameter("h_job_type_cd", 15, DbType.Int32, 1);

                        // require this value to become dynamic.
                        parameters.AddInputOutputParameter("h_msg_ind", "", DbType.String, 1);

                        // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                        parameters.AddInputOutputParameter("h_msg_text", strCarrierRepName, DbType.String, 200);

                        //Early Billing
                        parameters.AddInputParameter("h_release_fund_amt", Convert.ToDouble(strReleaseFundAmt), DbType.Double, 0); //-1
                        parameters.AddInputParameter("h_release_fund_ind", mstrReleaseFundInd, DbType.String, 1);
                        parameters.AddInputParameter("h_warranty_ind", strWarrantyInd, DbType.String, 1);
                        parameters.AddInputParameter("h_tries", 1, DbType.Int32, 4);

                        // Pass 'lynxdba' for system batch jobs.
                        parameters.AddInputParameter("h_user_id_no", mstrNTUserID, DbType.String, 32);
                        parameters.AddInputParameter("h_user_session_no", "", DbType.String, 32);

                        transaction = dbLayer.BeginTransaction();
                        dbLayer.ExecuteNonQuery(commandText, CommandType.StoredProcedure, parameters, transaction);

                        strReturnValue = Convert.ToString(parameters["retval"].Value);
                        strDispatchNumber = Convert.ToString(parameters["h_dispatch_no"].Value);
                        strMessageText = Convert.ToString(parameters["h_msg_text"].Value);
                        strMessageInd = Convert.ToString(parameters["h_msg_ind"].Value);

                        // objIngresAccessor.FpCleanUp();
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "Ingres return data.", string.Concat("strReturnValue = ", strReturnValue, " h_dispatch_no = ", strDispatchNumber,
                                                                " h_msg_text = ", strMessageText, " h_msg_ind = ", strMessageInd));

                        if (mstrEarlyBillFlag == "1")
                        {
                            if (objIngresParamsRoot != null)
                            {
                                objIngresParamsRoot.SetAttribute("h_dispatch_no", strDispatchNumber);
                                objIngresParamsRoot.SetAttribute("h_msg_text", strMessageText);
                                objIngresParamsRoot.SetAttribute("h_msg_ind", strMessageInd);
                            }
                        }

                        //Verify what we got back from Ingres.
                        if (strReturnValue == "-1")
                        {
                            // Extract the code and text portions of the Ingres error message.
                            strIngresErrorCode = strMessageText.Substring(0, 4);
                            strIngresMessageText = strMessageText.Substring((strMessageText.Length - ((strMessageText).Length - 4)), (strMessageText).Length - 4);

                            // Ensure a valid Ingres error code was retrieved
                            isValid = double.TryParse(strIngresErrorCode, out dblResult);
                            if (isValid)
                            {
                                // Convert the Ingres error code to an APD error code that can be resolved in the message store (error.xml).
                                lngIngresErrorNumber = Convert.ToInt64(strIngresErrorCode);
                                if (lngIngresErrorNumber != 0)
                                    lngIngresErrorNumber = MLAPDAccounting.LAPDAccounting_FirstError + lngIngresErrorNumber;

                                // Raise the error normally for invoice methods other than 'file'.
                                if (mintInvoiceType != Convert.ToInt32(eInvoiceType.eFile))
                                {
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", string.Concat("Ingres Billing SP ", strIngresErrorCode, "|", strIngresMessageText), "");
                                    throw new Exception(string.Concat(lngIngresErrorNumber, "Ingres Billing SP", strIngresErrorCode, "|", strIngresMessageText));
                                }
                                // For 'file' (electronic) billing, handle the error and continue.
                                else
                                {
                                    blnIngresErrorElectronicBilling = true;
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", string.Concat("Ingres Return value SP - ", strReturnValue, "Message Text -", strMessageText, "Message Indicator -", strMessageInd), "");
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", string.Concat("IngresErrorCode = ", lngIngresErrorNumber, strIngresErrorCode, "|", strIngresMessageText), string.Concat("Vars: Insco = ", mstrInsuranceCompanyID, "LynxID = ", strLynxID
                                        , "UserID = 0", "IngresAccoutingID = ", mstrIngresAccountingID, "PayeeID = ", strPayeeID, "Dispatch Number =", strDispatchNumber, "Total Payment = ", strPayment, "Total Billing =", strFee));

                                    objMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(lngIngresErrorNumber), "Ingres Billing SP", string.Concat(strIngresErrorCode, "|", strIngresMessageText),
                                    string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                     "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                    "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                    "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                                }
                            }
                            else
                            {
                                // Raise the generic Ingres error.
                                ingresErrorCode = Convert.ToUInt64(eInvoiceType.eFile);
                                if (mintInvoiceType != (int)ingresErrorCode)
                                    throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError, "IngresBillingSP", strIngresErrorCode, "|", strMessageText));
                                else
                                {
                                    // For electronic billing handle the error and continue.
                                    blnIngresErrorElectronicBilling = true;
                                    if (isDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "Accouting.CInvoiceItemManager.cs - SendToIngres", "Ingres return value", string.Concat("Returnvalue = ", strReturnValue, "Message Text -", strMessageText, "Message Indicator -", strMessageInd));

                                    errorCode = Convert.ToUInt64(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError);
                                    objMLAPDAccounting.g_objEvents.HandleEvent((int)errorCode, "Ingres Billing SP", string.Concat(strReturnValue, "|", strIngresMessageText),
                                        string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                        "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                        "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                        "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                                }
                            }
                        }
                        else if (strReturnValue != "0")
                        {
                            // Raise the generic Ingres error.
                            if (mintInvoiceType != Convert.ToInt32(eInvoiceType.eFile))
                                throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError, "IngresBillingSP", strReturnValue, "|", strMessageText));

                            else
                            {
                                // For electronic billing handle the error and continue.
                                blnIngresErrorElectronicBilling = true;

                                objMLAPDAccounting.g_objEvents.Env("INGRES: Return Value = '" + strReturnValue
                                    + "' Message Text = '" + strMessageText
                                    + "' Message Indicator = '" + strMessageInd + "'");

                                errorCode = Convert.ToUInt64(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError);
                                objMLAPDAccounting.g_objEvents.HandleEvent((int)errorCode, "Ingres Billing SP", string.Concat(strReturnValue, "|", strIngresMessageText),
                                   string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                    "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                    "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                    "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                            }
                        }

                        //If an Ingres error occurred during electronic billing, skip the updates to the InvoiceItem and the APD database.
                        if (blnIngresErrorElectronicBilling)
                        {
                            if (isDebug)
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs - SendToIngres", string.Concat(PROC_NAME, "Removing invoice item due to Ingres error."), "Ingres error during electronic billing.");

                            // Remove this InvoiceItem from the collection so that it will not appear in the output.
                            RemoveInvoiceItem(objInvoiceItem.InvoiceItemID);

                            // Reset flag.
                            blnIngresErrorElectronicBilling = false;
                        }
                        else
                        {
                            // Ensure Dispatch number has a value.
                            if ((strDispatchNumber.Length) > 0)
                            {
                                // Increment value indicating how many dispatches have been created in Ingres.
                                intReturnVal = intReturnVal + 1;

                                // Retrieve the InvoiceIDs associated with this InvoiceItem.
                                colInvoiceIDs = (Collection)objInvoiceItem.InvoiceIDs;

                                // Loop through the InvoiceItem's InvoiceIDs collection.

                                foreach (object vInvoiceID in colInvoiceIDs)
                                {
                                    // Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                                    if (isDebug)
                                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs - SendToIngres", "Executing SP to get the invoce records", string.Concat("Vars: StoredProc = ", strUpdateDispatchProc, " InvoiceID:", Convert.ToInt64(vInvoiceID), " InvoiceDate:", strNowSqlDateTime, " DispatchNumber:", strDispatchNumber, " UserID:", 0));
                                    hashTable = new Hashtable();
                                    hashTable.Add("@InvoiceID", Convert.ToInt64(vInvoiceID));
                                    hashTable.Add("@InvoiceDate", strNowSqlDateTime);
                                    hashTable.Add("@DispatchNumber", strDispatchNumber);
                                    hashTable.Add("@UserID", 0);
                                    objDataAccessor.ExecuteSp(strUpdateDispatchProc, hashTable, "APD");
                                }
                                // Set the InvoiceItem's DispatchNumber property so the DispatchNumber will appear on the invoice or in the billing file source
                                // XML as appropriate.  IMPORTANT -- ALWAYS perform this step prior to generating a paper invoice or sending the billing file source
                                // to GLAXIS.
                                objInvoiceItem.DispatchNumber = strDispatchNumber;

                                //save the Ingres transaction so we can use it for early bill release funds
                                if (mstrEarlyBillFlag == "1")
                                {
                                    //Save the release amount
                                    if ((Convert.ToDouble(strReleaseFundAmt)) > 0)
                                    {
                                        strReleaseFundsUpdateDispatchProc = objMLAPDAccounting.GetConfig("Accounting/Payment/UpdateReleaseFundsDispatchSP");
                                        strHistoryComments = string.Concat(Regex.Replace(strPayeeName, "'", "", RegexOptions.IgnoreCase), " [", strPayeeID, "]");
                                        // objMLAPDAccounting.g_objApdRsAccessor.ExecuteSp(strReleaseFundsUpdateDispatchProc, new object[] { strDispatchNumber, strDispatchNumber, strReleaseFundAmt, "'System: Payment Released to " + strHistoryComments + "'", mstrUserID });
                                        if (isDebug)
                                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs - SendToIngres", "Execute SP to save the ingres so we can use it for early bill release funds ", string.Concat("Vars: StoredProc = ", strReleaseFundsUpdateDispatchProc, " DispatchNumber = ", strDispatchNumber, "DispatchNumberNew = ", strDispatchNumber, "ReleaseFundAmount = ", strReleaseFundAmt, "Description = ", "System: Payment Released to ", strHistoryComments, "UserID = ", mstrUserID));
                                        hashTable = new Hashtable();
                                        hashTable.Add("@DispatchNumber", strDispatchNumber);
                                        hashTable.Add("@DispatchNumberNew", strDispatchNumber);
                                        hashTable.Add("@ReleaseFundAmount", strReleaseFundAmt);
                                        hashTable.Add("@Description", "System: Payment Released to " + strHistoryComments);
                                        hashTable.Add("@UserID", mstrUserID);
                                        objDataAccessor.ExecuteSp(strReleaseFundsUpdateDispatchProc, hashTable, "APD");
                                    }
                                    if (objIngresParamsRoot != null)
                                    {
                                        strFileName = strLynxID.Substring(strLynxID.Substring(1, strLynxID.IndexOf("-", 1)).Length - 4, 4);
                                        strFileName = string.Concat(strFileName.Substring(0, 1), "\\", strFileName.Substring(1, 1), "\\", strFileName.Substring(2, 1), "\\", strFileName.Substring(3, 1), "\\", "IngresParams", strDispatchNumber, ".xml");

                                        if ((strImageRoot.Substring(strImageRoot.Length - 1, 1) != "\\"))
                                            strFileName = string.Concat(strImageRoot, "\\", strFileName);
                                        else
                                            strFileName = string.Concat(strImageRoot, strFileName);

                                        objIngresParams.Save(strFileName);
                                        if (isDebug)
                                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs - SendToIngres", "Saved the Ingres Parameters to ", strFileName);
                                    }
                                }
                            }
                        }
                    }
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CInvoiceItemManager.cs - Initialize", "Process completed.", "");
            }
            catch (Exception ex)
            {
                commitTransaction = false;
                throw ex;
            }
            finally
            {
                /* Either commit the transaction or roll it back */
                if (commitTransaction)
                    dbLayer.CommitTransaction();
                else
                    dbLayer.RollbackTransaction();

                /* Dispose of the data layer */
                dbLayer.Dispose();
                objWriteLog = null;
            }
            return intReturnVal;
        }

        /// <summary>
        /// Procedure : CreateInvoiceSourceXML
        /// DateTime  : 3/16/2005 11:19
        /// Author    : Dan Price
        /// Purpose   : Alter the XML retrieved from the APD database by replacing the original
        ///             Invoice nodes, which represent APD Invoice records, with new Invoice nodes,
        ///             which represent InvoiceItem objects.
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public string CreateInvoiceSourceXML(eInvoiceType iType)
        {
            //Primitives
            long lngClaimAspectID;
            string strDispatchNumber = string.Empty;
            string PROC_NAME = string.Empty;
            string strXslFile;
            string strReturn = string.Empty;
            string strXml = string.Empty;
            string strBillingFile = string.Empty;
            int intInvoiceNodeCount;
            int intExposureNodeCount;

            XmlDocument objXmlDoc = null;
            XmlDocument objInvoiceDoc = null;
            XmlNodeList objExposureList = null;
            XmlNodeList objInvoiceList = null;
            XmlNode objParentNode = null;
            XmlNodeList objClaimList = null;
            XmlNode objChildNode = null;
            XmlNode objNodeAppend = null;
            XmlNode objInvoiceNodeAppend = null;

            object objParam = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - CreateInvoiceSourceXML", "Process started.", "");
                PROC_NAME = string.Concat(MODULE_NAME, "CreateInvoiceSourceXML");

                // Initialization
                objXmlDoc = new XmlDocument();
                objInvoiceDoc = new XmlDocument();
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();
                intInvoiceNodeCount = 0;
                intExposureNodeCount = 0;

                // Create a new document so that this process does not alter the original.
                strXml = mobjParamDOM.OuterXml;
                objMdomUtils.LoadXml(ref objXmlDoc, ref strXml, PROC_NAME, "Electronic Billing");

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Electronic Billing: Invoice source XML before altering to invoice format.", mobjParamDOM.OuterXml);
                // Loop thru exposures
                objExposureList = objXmlDoc.SelectNodes("/Root/Claim/Exposure");

                foreach (XmlNode objNode in objExposureList)
                {
                    // Grab ClaimAspectID
                    lngClaimAspectID = Convert.ToInt64(objNode.SelectSingleNode("./@ClaimAspectID").InnerText);

                    // Remove the current Exposure's Invoice nodes.
                    objInvoiceList = objNode.SelectNodes("./Invoice");
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Removing invoice nodes for Claim Aspect ID.", string.Concat("ClaimAspectID = ", lngClaimAspectID));

                    foreach (XmlNode objInvoiceNode in objInvoiceList)
                        objNode.RemoveChild(objInvoiceNode);

                    // Loop thru the InvoiceItems collection
                    foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                    {
                        // Locate the InvoiceItems for the current Claim Aspect (Exposure).
                        if (objInvoiceItem.ClaimAspectID == lngClaimAspectID)
                        {
                            intInvoiceNodeCount = intInvoiceNodeCount + 1;

                            // Add a new Invoice node to the Exposure node with the InvoiceItem xml.
                            if (mintInvoiceType == Convert.ToInt32(eInvoiceType.eFile))
                            {
                                // Get Invoice node xml formatted for electronic billing.
                                strBillingFile = objInvoiceItem.GetBillingFileXML();
                                objMdomUtils.LoadXml(ref objInvoiceDoc, ref strBillingFile, "ElectronicInvoicing", "InvoiceItem");
                            }
                            else
                            {
                                // Get Invoice node xml formatted for 'paper' Invoice document.
                                strBillingFile = objInvoiceItem.GetInvoiceDocumentXML();
                                objMdomUtils.LoadXml(ref objInvoiceDoc, ref strBillingFile, "ElectronicInvoicing", "InvoiceItem");

                                // Grab the Dispatch number from the InvoiceItem with the handling fee.  For the 'paper' Invoice document, the
                                // handling fee will be a separate line item and we need to stick the correct Dispatch number in the Handling fee
                                // InvoiceItem.
                                if (objInvoiceItem.HandlingFeeGoesHere)
                                    strDispatchNumber = objInvoiceItem.DispatchNumber;
                            }
                            if (isDebug)
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Replacing invoice node for Claim Aspect ID: ", string.Concat(Convert.ToString(lngClaimAspectID), Environment.NewLine, objInvoiceDoc.OuterXml));

                            objInvoiceNodeAppend = objInvoiceDoc.SelectSingleNode("//Invoice");
                            objNode.AppendChild(objNode.OwnerDocument.ImportNode(objInvoiceNodeAppend, true));
                        }
                    }

                    // Hack for paper invoices.
                    if ((mintInvoiceType == Convert.ToInt32(eInvoiceType.ePaper)) && (strDispatchNumber.Length) > 0)
                    {
                        if (mobjHandlingFeeInvoiceItem != null)
                        {
                            //Add the Dispatch number to the Handling fee InvoiceItem.
                            mobjHandlingFeeInvoiceItem.DispatchNumber = strDispatchNumber;

                            // Get the InvoiceItem objects xml formatted for the paper invoice.
                            strBillingFile = mobjHandlingFeeInvoiceItem.GetInvoiceDocumentXML();
                            objMdomUtils.LoadXml(ref objInvoiceDoc, ref strBillingFile, "ElectronicInvoicing", "InvoiceItem");

                            // Add the Invoice line item xml to the document.
                            //objNode.AppendChild(objInvoiceDoc.DocumentElement);
                            objInvoiceNodeAppend = objInvoiceDoc.SelectSingleNode("//Invoice");
                            objNode.AppendChild(objNode.OwnerDocument.ImportNode(objInvoiceNodeAppend, true));
                        }
                    }
                }
                // Clean up XML This to ensure no dud nodes are forwarded  through the invoice process. -----------------------------------
                // Some InvoiceItems might have been removed during the SendToIngresProcess due to zero billing
                // and zero payment or because of an Ingres error.  This could leave some exposure nodes with no child invoice
                // nodes.  These exposure nodes should be removed.

                objExposureList = objXmlDoc.SelectNodes("/Root/Claim/Exposure[count(Invoice) = 0]");
                foreach (XmlNode objExposureNode in objExposureList)
                {
                    objParentNode = objExposureNode.ParentNode;
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Removing empty Exposure node. ", objExposureNode.OuterXml);

                    objChildNode = objParentNode.RemoveChild(objExposureNode);
                    objChildNode = null;
                }

                // Removing exposure nodes could leave some claim nodes with no child exposure nodes.  These claim nodes shold be removed.
                objClaimList = objXmlDoc.SelectNodes("/Root/Claim[count(Exposure) = 0]");
                foreach (XmlNode objClaimNode in objClaimList)
                {
                    objParentNode = objClaimNode.ParentNode;
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Removing empty Claim node. ", objClaimNode.Name);

                    objChildNode = objParentNode.RemoveChild(objClaimNode);
                    objChildNode = null;
                }

                // Initialize return value
                strReturn = string.Empty;

                // Electronic files may need to be altered to conform to GLAXIS' Biztalk map.  This will be accomplished through an XSLT.
                if (iType == eInvoiceType.eFile)
                {
                    // Check config to determine if any implementation specific xml file structure is required.  ------------------------------
                    strXslFile = objMLAPDAccounting.GetConfig(string.Concat("Accounting/ElectronicInvoice/Insurance[@ID=", mstrInsuranceCompanyID, "]/@xsl"));
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "XSLT transaformation process", string.Concat("XSL file path = ", strXslFile));

                    // If an xsl file is found do the transformation.
                    if ((strXslFile).Trim().Length > 0)
                        strXslFile = string.Concat(objMLAPDAccounting.g_strSupportDocPath, "\\", strXslFile);

                    objParam = objMLAPDAccounting.g_objEvents;
                    objInvoiceDoc = objMdomUtils.TransformDomAsDom(ref objXmlDoc, strXslFile);

                    // Grab the first child so we can determine if it is a processing instruction.
                    objNodeAppend = objInvoiceDoc.FirstChild;

                    // Remove processing instruction if present.
                    if (objNodeAppend.NodeType.ToString() == "NODE_PROCESSING_INSTRUCTION")
                    {
                        if (isDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Removing processing instruction from XML: ", objInvoiceDoc.FirstChild.OuterXml);

                        objInvoiceDoc.RemoveChild(objNodeAppend);
                        objNodeAppend = objInvoiceDoc.FirstChild;

                        // Remove the formatiing object attribute if present.
                        objNodeAppend.Attributes.RemoveNamedItem("xmlns:fo");

                        // Set the return value.  An XSLT was performed.
                        strReturn = objInvoiceDoc.OuterXml;
                    }
                    else
                    {
                        // Set the return value.  There was no XSLT required for this client.
                        strReturn = objXmlDoc.OuterXml;
                    }
                }
                else
                {
                    // Set the return value.  No additional XSLT required for paper billing.
                    strReturn = objXmlDoc.OuterXml;
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "CInvoiceItemManager.cs-CreateInvoiceSourceXML", "Process completed. ", string.Concat("Result = ", strReturn));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            // Return the reformatted XML.
            return strReturn;
        }

        #endregion

        #region Private Helper Functions
        private void Class_Initialize()
        {
            try
            {
                mcolInvoiceItems = new Collection();
                mobjParamDOM = new XmlDocument();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Procedure : Class_Terminate
        /// DateTime  : 3/16/2005 11:26
        /// Author    : Dan Price
        /// Purpose   : Destroy the InvoiceItems collection and all of the InvoiceItem objects
        ///             it contains.
        /// </summary>
        private void Class_Terminate()
        {
            //item As CInvoiceItem

            mobjParamDOM = null;
            mobjHandlingFeeInvoiceItem = null;
            CInvoiceItem itemClear = null;
            try
            {
                foreach (CInvoiceItem item in mcolInvoiceItems)
                {
                    itemClear = item;
                    itemClear = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mcolInvoiceItems = null;
            }
        }

        /// <summary>
        /// Procedure : CreateInvoiceItem
        /// DateTime  : 3/16/2005 11:15
        /// Author    : Dan Price
        /// Purpose   : Build a new InvoiceItem object that contains all the properties needed to
        ///             support paper or electronic invoicing.
        /// </summary>
        /// <param name="objInvoiceNode"></param>
        /// <returns></returns>
        private CInvoiceItem CreateInvoiceItem(XmlNode objInvoiceNode)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CreateInvoiceItem");

            // Objects that must be destroyed.
            CInvoiceItem objInvoiceItem = null;

            // Primitives
            string strAuthorizingClientUserID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayeeCity = string.Empty;
            string strPayeeState = string.Empty;
            string strDispatchNumber = string.Empty;
            string strInvoiceDescription = string.Empty;
            string strClientFeeCode = string.Empty;
            string strInvoiceDate = string.Empty;
            string strDeductible = string.Empty;
            string strServiceChannel = string.Empty;
            bool blnHandlingFeeGoesHere = false;
            string strTaxTotalAmt = string.Empty;
            double outNumeric;
            bool isNumeric = false;
            try
            {
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - CreateInvoiceItem", "", string.Concat("Invoice Node XML =", objInvoiceNode.OuterXml));

                // Retrieve properties from Invoice node.
                strAuthorizingClientUserID = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@AuthorizingClientUserID", false);
                strPayeeName = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeName", true);
                strPayeeCity = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeCity", true);
                strPayeeState = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeState", true);
                strDispatchNumber = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@DispatchNumber");
                strInvoiceDescription = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@InvoiceDescription");
                strClientFeeCode = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@ClientFeeCode");
                strInvoiceDate = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@InvoiceDate");
                strDeductible = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@DeductibleAmt");
                strTaxTotalAmt = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@TotalTaxAmt");
                strServiceChannel = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@ServiceChannel");

                // For indemnities, determine if the Payment is to the first assigned Shop to receive an indemnity payment.  The Handling Fee will be added to the shop
                // where this is true.
                if (mstrItemType == "Indemnity" && mstrPayeeType == "Shop" && !mblnHandlingFeeIndemnityFlagged)
                {
                    isNumeric = double.TryParse(mstrHandlingPayeeID, out outNumeric);
                    if (isNumeric)
                    {
                        if (mstrHandlingPayeeID == mstrPayeeID)
                            blnHandlingFeeGoesHere = true;

                        // Indicate that the indemnity to which the handling fee will be attached has been located and flagged.  This will prevent the method
                        // from flagging any other indemnity.
                        mblnHandlingFeeIndemnityFlagged = true;
                    }
                    else
                        blnHandlingFeeGoesHere = false;
                }
                else
                    blnHandlingFeeGoesHere = false;

                // Add Amount of current Invoice record to the Invoice total.
                mdblInvoiceTotal = mdblInvoiceTotal + Convert.ToDouble(mstrAmount);

                // Increment InvoiceItemID.  This value will can be used to explicitly identify an InvoiceItem without trudging through
                // all the other crap in the InvoiceItem.
                mintInvoiceItemID = mintInvoiceItemID + 1;

                // Retrieve Invoice level properties.
                objInvoiceItem = new CInvoiceItem();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "Accouting.CInvoiceItemManager.cs - CreateInvoiceItem", string.Concat("Calling Intialize from CInvoiceItem Class."), "");
                // Call to the InvoiceItem 'constructor'.
                objInvoiceItem.Initialize(mintInvoiceItemID, mlngClaimAspectID, mstrItemType, mstrLynxID, mstrCarrierClaimNumber, mstrCarrierRepNameFirst,
                                         mstrCarrierRepnameLast, mstrComment, strDeductible, mstrInsuredNameFirst, mstrInsuredNameLast, mstrInsuredBusinessName,
                                         mstrClaimantNameFirst, mstrClaimantNameLast, mstrClaimantBusinessName, strAuthorizingClientUserID,
                                         mstrAmount, mstrAdminAmount, mstrPayeeID, strPayeeName, strPayeeCity, strPayeeState, strDispatchNumber,
                                         strInvoiceDescription, mlngInvoiceID, strClientFeeCode, strInvoiceDate, mstrPayeeType, strServiceChannel,
                                         mstrInitialPayment, strTaxTotalAmt, blnHandlingFeeGoesHere);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CInvoiceItemManager.cs - CreateInvoiceItem", string.Concat("Completed"), "");
                // Return the newly created object.
                return objInvoiceItem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Procedure : AppendInvoiceItem
        /// DateTime  : 3/16/2005 11:25
        /// Author    : Dan Price
        /// Purpose   : Add an existing InvoiceItem to the InvoiceItems collection.
        /// </summary>
        private void AppendInvoiceItem(CInvoiceItem objInvoiceItem)
        {
            try
            {
                mcolInvoiceItems.Add(objInvoiceItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Procedure : RemoveInvoiceItem
        ///  DateTime  : 4/1/2005 11:50
        ///  Author    : Dan Price
        ///  Purpose   : Remove the InvoiceItem identified by the passed InvoiceItemID from the
        ///              InvoiceItems collection.
        /// </summary>
        /// <param name="intInvoiceItemID"></param>
        private void RemoveInvoiceItem(int intInvoiceItemID)
        {
            CInvoiceItem objTemp = null;
            // Primitives
            int intIndex;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                // Initialize index for 1 based collection.
                intIndex = 1;
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "CInvoiceItemManager.cs - RemoveInvoiceItem", "Started", "InvoiceID: '" + intInvoiceItemID + "'");

                // Loop through the InvoiceItems collection.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Locate the InvoiceItem with the passed InvoiceItemID
                    if (objInvoiceItem.InvoiceItemID == intInvoiceItemID)
                    {
                        objTemp = new CInvoiceItem();

                        // Added objTemp because there were some items being errantly removed.
                        //*******TODO****              
                        objTemp = (CInvoiceItem)mcolInvoiceItems[intIndex];

                        if (objTemp.InvoiceItemID == intInvoiceItemID)
                        {
                            if (isDebug)
                                objWriteLog.LogEvent("COM Conversion", "Debugging", "CInvoiceItemManager.cs - RemoveInvoiceItem", string.Concat("Removing invoice item.  InvoiceItemID: ", objTemp.InvoiceItemID,
                                                                          "InvoiceID: ", objTemp.FirstInvoiceID, " ClaimAspectID: ", objTemp.ClaimAspectID,
                                                                          " Fee: ", objTemp.Fee, " Payment: ", objTemp.Payment), "");
                            // Remove the indicated item from the collection.
                            mcolInvoiceItems.Remove(intIndex);
                        }
                        // Bail out of the loop.
                        break;
                    }
                    // Increment the index.
                    intIndex = intIndex + 1;
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "CInvoiceItemManager.cs - RemoveInvoiceItem", "Completed", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Procedure : AddToExistingInvoiceItem
        /// DateTime  : 3/16/2005 11:23
        /// Author    : Dan Price
        /// Purpose   : Determine if an InvoiceItem already exists for the current Payee.  If so,
        ///             add the current Payment amount to that InvoiceItem and return True.  Otherwise,
        ///             return False signaling that a new InvoiceItem should be created.
        /// </summary>
        /// <returns></returns>
        private bool AddToExistingInvoiceItem()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "AddToExistingInvoiceItem");

            //Dim objInvoiceItem As CInvoiceItem
            bool blnPaymentAdded = false;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - AddToExistingInvoiceItem", string.Concat("Started"), "");
                // Loop thru existing InvoiceItems to determine if one has already been created for the current Payee.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Limit this search to InvoiceItems for the current ClaimAspectID.
                    if (objInvoiceItem.ClaimAspectID == mlngClaimAspectID)
                    {
                        // Determine if an InvoiceItem already exists for this Payee for this vehicle. The name is factored in here in case payments were made
                        // to more than 1 CEI shop.  Both would use the default CEI Shop ID and could not therefore be differentiated on the basis of ID.
                        if (objInvoiceItem.PayeeID == mstrPayeeID && objInvoiceItem.PayeeType == mstrPayeeType && objInvoiceItem.PayeeName == mstrPayeeName)
                        {
                            if (isDebug)
                                objWriteLog.LogEvent("COM Conversion", "Debugging", "Accouting.CInvoiceItemManager.cs - AddToExistingInvoiceItem", string.Concat("Amount -", mstrAmount, "AmountType -", Convert.ToString(CInvoiceItem.AmountType.eFee), "InvoiceID -", mlngInvoiceID), "");
                            // Add Payment Amount to the existing InvoiceItem and add the InvoiceID to the InvoiceItem's InvoiceIDs collection.
                            blnPaymentAdded = objInvoiceItem.Add(mstrAmount, mstrAdminAmount, mstrDeductibleAmt, mstrTaxTotalAmt, CInvoiceItem.AmountType.ePayment, mlngInvoiceID, mstrInitialPayment);
                            if (isDebug)
                                objWriteLog.LogEvent("COM Conversion", "Debugging", "Accouting.CInvoiceItemManager.cs - AddToExistingInvoiceItem", string.Concat("Payment -", blnPaymentAdded ? "ADDED" : "NOT ADDED", " to existing line item/dispatch for Claim Aspect: ", objInvoiceItem.ClaimAspectID), "");
                        }

                        // Check to see if this Payment has been added to an existing InvoiceItem.
                        if (blnPaymentAdded)
                        {
                            // If so, add Amount of current Invoice record to the Invoice Total...
                            mdblInvoiceTotal = mdblInvoiceTotal + Convert.ToDouble(mstrAmount);
                            // ... and bail out.
                            break;
                        }
                    }
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CInvoiceItemManager.cs - AddToExistingInvoiceItem", "Completed", "");
                // Set return value.
                return blnPaymentAdded;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Procedure : CopyInvoiceItem
        /// Da teTime  : 3/18/2005 14:03
        /// Author    : Dan Price
        /// Purpose   : Make a copy of an existing InvoiceItem and return the new object to the caller.
        ///             This is necessary because direct assignment of a reference object simply creates
        ///             an additional pointer to the existing object.  We actually require a new object here.
        /// </summary>
        /// <param name="objInvoiceItem"></param>
        /// <returns></returns>
        private CInvoiceItem CopyInvoiceItem(CInvoiceItem objInvoiceItem)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CopyInvoiceItem");

            CInvoiceItem objNew = null;
            Collection col = null;

            try
            {
                objWriteLog = new WriteLog();
                objMLAPDAccounting = new MLAPDAccounting();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accouting.CInvoiceItemManager.cs - CopyInvoiceItem", "Started ", string.Concat("ClaimAspectID: ", objInvoiceItem.ClaimAspectID,
        "  Invoice Description: ", objInvoiceItem.InvoiceDescription, "  Payee Name: ", objInvoiceItem.PayeeName));

                objNew = new CInvoiceItem();

                objNew.CarrierRepNameFirst = objInvoiceItem.CarrierRepNameFirst;
                objNew.CarrierRepNameLast = objInvoiceItem.CarrierRepNameLast;
                objNew.ClaimAspectID = objInvoiceItem.ClaimAspectID;
                objNew.ClaimNumber = objInvoiceItem.ClaimNumber;
                objNew.Comment = objInvoiceItem.Comment;
                objNew.DispatchNumber = objInvoiceItem.DispatchNumber;
                objNew.Fee = objInvoiceItem.Fee;
                objNew.adminFee = objInvoiceItem.adminFee;
                objNew.InsuredBusinessName = objInvoiceItem.InsuredBusinessName;
                objNew.InsuredNameFirst = objInvoiceItem.InsuredNameFirst;
                objNew.InsuredNameLast = objInvoiceItem.InsuredNameLast;
                objNew.LynxID = objInvoiceItem.LynxID;
                objNew.PayeeID = objInvoiceItem.PayeeID;
                objNew.Payment = objInvoiceItem.Payment;
                objNew.AuthorizingClientUserID = objInvoiceItem.AuthorizingClientUserID;
                objNew.Amount = objInvoiceItem.Amount;
                objNew.ClientFeeCode = objInvoiceItem.ClientFeeCode;
                objNew.InvoiceDate = objInvoiceItem.InvoiceDate;
                objNew.InvoiceDescription = objInvoiceItem.InvoiceDescription;
                objNew.PayeeName = objInvoiceItem.PayeeName;
                objNew.PayeeCity = objInvoiceItem.PayeeCity;
                objNew.PayeeState = objInvoiceItem.PayeeState;
                objNew.PayeeType = objInvoiceItem.PayeeType;
                objNew.InvoiceItemID = objInvoiceItem.InvoiceItemID;

                col = (Collection)objInvoiceItem.InvoiceIDs;

                foreach (object item in col)
                    objNew.AddInvoiceID = Convert.ToInt64(item.ToString());
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accouting.CInvoiceItemManager.cs - CopyInvoiceItem", string.Concat("Completed"), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return objNew;
        }

        /// <summary>
        ///  Procedure : GetCurrencyFormattedString
        ///  DateTime  : 3/18/2005 14:03
        ///  Author    : Dan Price
        ///  Purpose   : Ensure a string representing a dollar amount has a precision of 2 decimal
        ///              places.
        /// </summary>
        /// <param name="dblAmount"></param>
        /// <returns></returns>
        private string GetCurrencyFormattedString(double dblAmount)
        {
            string strAmount = string.Empty;
            long lngPos;
            try
            {
                strAmount = Convert.ToString(dblAmount);
                lngPos = strAmount.IndexOf(".");

                if (lngPos == 0)
                    strAmount = string.Concat(strAmount, ".00");
                else if (lngPos == ((strAmount).Length - 1))
                    strAmount = string.Concat(strAmount, "0");

                return strAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
