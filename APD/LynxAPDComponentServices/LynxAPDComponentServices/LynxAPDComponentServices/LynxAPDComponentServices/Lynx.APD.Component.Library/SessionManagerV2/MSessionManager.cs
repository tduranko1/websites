﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.SessionManagerV2
{
    /// <summary>
    /// Component SessionManager : Module MDataAccessor
    /// 
    /// Global Consts and Utility Methods
    /// </summary>
    class MSessionManager
    {       

        #region Public Functions
        /// <summary>
        ///  Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
        ///  Parameters:  strObjectName = The object to call CreateObject for.
        ///  Purpose:     Wraps CreateObject() with better debug information.
        ///  Returns:     The Object created or Nothing.
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns></returns>
        public object CreateObjectEx(string strObjectName)
        {
            object obj = null;
            try
            {
                //obj = CreateObject(strObjectName);

                if (obj == null)
                    throw new Exception(string.Concat("", "CreateObject('" + strObjectName + "') returned Nothing."));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return obj;
        }
        #endregion
    }
}
