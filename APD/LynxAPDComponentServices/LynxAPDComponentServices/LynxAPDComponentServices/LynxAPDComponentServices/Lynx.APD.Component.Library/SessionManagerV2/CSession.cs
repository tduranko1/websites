﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Runtime.InteropServices;
using ADODB;
using System.Xml;
using SiteUtilities;
using objDataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using Lynx.APD.Component.Library.Common;
using System.Collections;

namespace Lynx.APD.Component.Library.SessionManagerV2
{
    /// <summary>
    /// Component SessionManager : Class CSession
    /// 
    /// CSession evolved from TSession and QSession in the original SessionManager.
    /// Independant transactional and non-tranactional classes are no longer
    /// necessary.
    /// </summary>
    public class CSession
    {
        #region Declarations
        //Name of this object
        private const string APP_NAME = "SessionManager.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CSession.");
        private CEvents mobjEvents = null;
        private objDataAccessor.CDataAccessor mobjDataAccessor = null;
        private bool mblnDebugMode;
        MCommon objMCommon = null;
        //These variables used for Xsl Session calls
        private string mstrXslSessionKey;
        private string mstrXslWindowID;
        #endregion

        #region Enumerators
        //Internal error codes for this class.
        private enum ErrorCodes
        {
            eBadSessionVariableName,
            eBadSessionKey
        }

        //Session fields in the SessionManager Database
        public enum enumSM_SessionMgr
        {
            geSM_SessionKey,
            geSM_VariableName,
            geSM_Value,
            geSM_ModifiedBy,
            geSM_LastAccessDateTime
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Creates a new session in DB, returns Encrypted ID.
        /// strWebRoot param is no longer used.
        /// 
        /// </summary>
        /// <param name="strUserID"></param>
        /// <param name="strWebRoot"></param>
        /// <returns></returns>
        public string CreateSession(string strUserID, string strWebRoot = "")
        {
            string PROC_NAME = string.Empty;
            string strSessionKey = string.Empty;
            string strProcName = string.Empty;
            string strReturnKey = string.Empty;
            string returnSessionKey = string.Empty;
            Hashtable htParams = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "CreateSession: ");

                InitializeGlobals();
                objMCommon = new MCommon();
                //Add passed arguments to debug info.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(
                    "UserID = ", strUserID, Environment.NewLine,
                    "  WebRoot = ", strWebRoot), PROC_NAME);

                //set Site Name, registry subkey setting
                strProcName = GetConfig("SessionManager/CreateSessionSP");  //"uspSessionIns"

                /*if (objMCommon.HasValue(strUserID))
                    strUserID = "0";*/

                htParams = new Hashtable();
                htParams.Add("@ModifiedBy", strUserID);

                strSessionKey = mobjDataAccessor.ExecuteSession(strProcName, htParams, "APD");

                strReturnKey = objMCommon.EncryptSessionKeys(strSessionKey);
                returnSessionKey = string.Concat("{", strReturnKey, "}");

                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat("SessionKey = ", strSessionKey), PROC_NAME);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnSessionKey;
        }

        /// <summary>
        /// Update the session value
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <param name="varValue"></param>
        /// <param name="strWebRoot"></param>
        public void UpdateSession(string strSessionKey, string strVariableName, string varValue, string strWebRoot = "")
        {
            try
            {
                objMCommon = new MCommon();
                InitializeGlobals();

                /*if (objMCommon.HasValue(strSessionKey))
                    throw new Exception(string.Concat("No session key passed on the strSessionKey", strSessionKey));*/

                strSessionKey = objMCommon.DecryptSessionKey(strSessionKey);
                UpdateSingleRecord(strSessionKey, strVariableName, varValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the session value
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <param name="strWebRoot"></param>
        /// <returns></returns>
        public string GetValue(string strSessionKey, string strVariableName, string strWebRoot = "")
        {
            string strProcName = string.Empty, PROC_NAME = string.Empty;
            Hashtable htParams = null;
            try
            {
                PROC_NAME = MODULE_NAME + "GetValue: ";
                InitializeGlobals();
                objMCommon = new MCommon();
                mobjDataAccessor = new objDataAccessor.CDataAccessor();

                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat("SessionKey = ", strSessionKey, Environment.NewLine, "  VarName = ", strVariableName, Environment.NewLine, "  WebRoot = ", strWebRoot, PROC_NAME));
                /*if (!objMCommon.HasValue(strSessionKey))
                    throw new Exception("Invalid Session Key");
                if(!objMCommon.HasValue(strVariableName))
                    throw new Exception("Invalid Session Variable Key"); */

                strProcName = GetConfig("SessionManager/SelectSessionDataSP"); //"uspSessionDataGet"
                strSessionKey = objMCommon.DecryptSessionKey(strSessionKey);

                htParams = new Hashtable();
                htParams.Add("@SessionID", strSessionKey);
                htParams.Add("@SessionVariable", strVariableName);

                return mobjDataAccessor.GetSessionValue(strProcName, htParams, "APD");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks if session exsist, and return the bool value.
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <param name="strWebRoot"></param>
        /// <returns></returns>
        public bool Exists(string strSessionKey, string strVariableName, string strWebRoot = "")
        {
            string strProcName = string.Empty,
                 PROC_NAME = string.Empty;
            Hashtable htParams = null;
            ADODB.Recordset objRS = null;
            bool blnReturn = false;
            try
            {
                PROC_NAME = MODULE_NAME + "Exists: ";
                InitializeGlobals();
                objMCommon = new MCommon();

                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat("SessionKey = ", strSessionKey, Environment.NewLine, "  VarName = ", strVariableName, Environment.NewLine, "  WebRoot = ", strWebRoot, PROC_NAME));
                /*if (!objMCommon.HasValue(strSessionKey))
                    throw new Exception("Invalid Session Key");
                if (!objMCommon.HasValue(strVariableName))
                    throw new Exception("Invalid Session Variable Key"); */
                strSessionKey = objMCommon.DecryptSessionKey(strSessionKey);

                if (strVariableName.Trim() == string.Empty)
                    strVariableName = null;

                if (!objMCommon.HasValue(strVariableName))
                {
                    strProcName = GetConfig("SessionManager/ExistSessionSP"); //uspSessionExists"
                    htParams = new Hashtable();
                    htParams.Add("@SessionID", strSessionKey);
                    blnReturn = mobjDataAccessor.SessionExists(strProcName, htParams, "APD");
                }
                else
                {
                    objRS = GetSelectedRecord(strSessionKey, strVariableName);
                    objRS.MoveFirst();

                    if (objRS.RecordCount > 0)
                        blnReturn = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnReturn;
        }

        /// <summary>
        /// Update the session date/time
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strWebRoot"></param>
        public void UpdateLastAccessDateTime(string strSessionKey, string strWebRoot)
        {
            Hashtable htParams = null;
            long lngRet = 0;
            string strProcName = "";
            try
            {
                string PROC_NAME = MODULE_NAME + "UpdateLastAccessDateTime: ";
                InitializeGlobals();
                objMCommon = new MCommon();

                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat("SessionKey = ", strSessionKey, Environment.NewLine, "  WebRoot = ", strWebRoot, PROC_NAME));

                strSessionKey = objMCommon.DecryptSessionKey(strSessionKey);
                strProcName = GetConfig("SessionManager/UpdateSessionSP"); //"uspSessionUpd"

                htParams = new Hashtable();
                htParams.Add("@SessionID", strSessionKey);
                htParams.Add("@ModifiedDateTime", null);

                lngRet = mobjDataAccessor.ExecuteSp(strProcName, htParams, "APD"); //'use server timestamp
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Intialize the xsl session variables
        /// </summary>
        /// <param name="strXslSessionKey"></param>
        /// <param name="strXslWindowID"></param>
        public void XslInitSession(string strXslSessionKey, string strXslWindowID)
        {
            try
            {
                mstrXslSessionKey = strXslSessionKey;
                mstrXslWindowID = strXslWindowID;

                if (StringExtensions.IsNullOrWhiteSpace(strXslSessionKey))
                    throw new Exception("Passed Session Key was blank.");
                if (StringExtensions.IsNullOrWhiteSpace(strXslWindowID))
                    throw new Exception("Passed Window ID was blank.");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the session value for the XSL page.
        /// </summary>
        /// <param name="strVariableName"></param>
        /// <returns></returns>
        public string XslGetSession(string strVariableName)
        {
            string strReturn = string.Empty;
            try
            {
                if (StringExtensions.IsNullOrWhiteSpace(mstrXslSessionKey))
                    throw new Exception("Session Key was blank.  InitSession must be called first.");
                if (StringExtensions.IsNullOrWhiteSpace(mstrXslWindowID))
                    throw new Exception("Window ID was blank.  InitSession must be called first.");
                if (StringExtensions.IsNullOrWhiteSpace(strVariableName))
                    throw new Exception("Passed variable name was blank.");

                strReturn = GetValue(mstrXslSessionKey, string.Concat(strVariableName, "-", mstrXslWindowID), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// Update the xsl session value.
        /// </summary>
        /// <param name="strVariableName"></param>
        /// <param name="strValue"></param>
        public void XslUpdateSession(string strVariableName, string strValue)
        {
            try
            {
                if (StringExtensions.IsNullOrWhiteSpace(mstrXslSessionKey))
                    throw new Exception("Session Key was blank.  InitSession must be called first.");
                if (StringExtensions.IsNullOrWhiteSpace(mstrXslWindowID))
                    throw new Exception("Window ID was blank.  InitSession must be called first.");
                if (StringExtensions.IsNullOrWhiteSpace(strVariableName))
                    throw new Exception("Passed variable name was blank.");

                UpdateSession(mstrXslSessionKey, string.Concat(strVariableName, "-", mstrXslWindowID), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates a session variable value with XML from a XMlNodeList
        /// </summary>
        /// <param name="strVariableName"></param>
        /// <param name="objNodeList"></param>
        public void XslUpdateSessionNodeList(string strVariableName, ref XmlNodeList objNodeList)
        {
            XmlNodeList oNodeList = null;
            string strXml = string.Empty;

            try
            {
                oNodeList = objNodeList;

                foreach (XmlNode oNode in oNodeList)
                {
                    strXml = string.Concat(strXml, oNode.OuterXml);
                }

                XslUpdateSession(strVariableName, strXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private Helper Functions

        private void Class_Terminate()
        {
            try
            {
                TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void InitializeGlobals()
        {
            string configPath = string.Empty;
            string strConnectionString = string.Empty;
            try
            {


                //Create private member DataAccessor.
                mobjDataAccessor = new objDataAccessor.CDataAccessor();

                //Share DataAccessor's events object.
                mobjEvents = new CEvents();

                //Use our own log file.
                mobjEvents.ComponentInstance = "Session Manager";

                configPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);

                //Initialize our member components first, so we have logging set up.
                mobjEvents.Initialize(configPath);
                strConnectionString = GetConfig("SessionManager/ConnectionString");

                //Get debug mode status.
                mblnDebugMode = true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void TerminateGlobals()
        {
            try
            {
                mobjEvents = null;
                mobjDataAccessor = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        private string GetConfig(string strSetting)
        {
            try
            {
                return mobjEvents.mSettings.GetParsedSetting(strSetting);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the Session records
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <returns></returns>
        private Recordset GetSelectedRecord(string strSessionKey, string strVariableName)
        {

            string PROC_NAME = string.Empty;
            string strProcName = string.Empty;
            Hashtable htParams = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "CreateSession: ");

                InitializeGlobals();
                objMCommon = new MCommon();
                //Add passed arguments to debug info.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(
                    "Session Key = ", strSessionKey, Environment.NewLine,
                    "Session Variable Name = ", strVariableName), PROC_NAME);

                /* if (!objMCommon.HasValue(strSessionKey))
                     throw new Exception("Invalid Session Key");
                 if (!objMCommon.HasValue(strVariableName))
                     throw new Exception("Invalid Session Variable Name"); */

                //set Site Name, registry subkey setting
                strProcName = GetConfig("SessionManager/SelectSessionDataSP"); //"uspSessionDataGet"

                /*if ((objMCommon.HasValue(strSessionKey)) || (objMCommon.HasValue(strVariableName)))
                    return null; */

                htParams = new Hashtable();
                htParams.Add("@SessionID", strSessionKey);
                htParams.Add("@SessionVariable", strVariableName);

                return mobjDataAccessor.OpenRecordsetSp(strProcName, htParams, "APD");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Support function to insert Session Variable.
        /// This method is DEPRACATED as the stored procedures now decide whether to
        ///   do an insert or an update.
        /// strWebRoot param is no longer used.
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <param name="varValue"></param>
        /// <returns></returns>
        private object InsertSingleRecord(string strSessionKey, string strVariableName, object varValue)
        {
            string PROC_NAME = string.Empty;
            string strProcName = string.Empty;
            long lngRet;

            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "InsertSingleRecord: ");

                InitializeGlobals();

                //Add passed arguments to debug info.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(
                    "SessionKey = ", strSessionKey, Environment.NewLine,
                    "  VarName = ", strVariableName, Environment.NewLine,
                    "  VarValue = ", Convert.ToString(varValue)), PROC_NAME);

                strProcName = GetConfig("SessionManager/InsertSessionDataSP");  //"uspSessionDataIns"

               // lngRet = mobjDataAccessor.ExecuteSp(strProcName, new object[] { "'" + strSessionKey + "'", "'" + strVariableName + "'", "'" + varValue + "'", "'0'" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strSessionKey;
        }

        /// <summary>
        /// Support function to update Session Variable
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <param name="strVariableName"></param>
        /// <param name="varValue"></param>
        /// <returns></returns>
        private void UpdateSingleRecord(string strSessionKey, string strVariableName, object varValue)
        {
            string PROC_NAME = string.Empty;
            string strProcName = string.Empty;
            long lngRet;
            Hashtable htParams = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "UpdateSingleRecord: ");

                InitializeGlobals();

                //Add passed arguments to debug info.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(
                    "SessionKey = ", strSessionKey, Environment.NewLine,
                    "  VarName = ", strVariableName, Environment.NewLine,
                    "  VarValue = ", Convert.ToString(varValue)), PROC_NAME);

                strProcName = GetConfig("SessionManager/UpdateSessionDataSP"); //"uspSessionDataUpd"

                htParams = new Hashtable();
                htParams.Add("@SessionID", strSessionKey);
                htParams.Add("@SessionVariable", strVariableName);
                htParams.Add("@SessionValue", varValue);

                lngRet = mobjDataAccessor.ExecuteSp(strProcName, htParams, "APD");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Support function to delete Session Variable
        /// Garbage cleanup is done automatically by the database, so this call is no
        ///   longer used but is still valid.
        /// </summary>
        /// <param name="strSessionKey"></param>
        /// <returns></returns>
        private object DeleteSingleRecord(string strSessionKey)
        {
            string PROC_NAME = string.Empty;
            string strProcName = string.Empty;
            long lngReturn = 0;
            Hashtable htParams = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "DeleteSingleRecord: ");

                InitializeGlobals();

                //Add passed arguments to debug info.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat("SessionKey = ", strSessionKey), PROC_NAME);

                strProcName = GetConfig("SessionManager/DeleteSessionSP"); //"uspSessionDel"
                htParams = new Hashtable();
                htParams.Add("@SessionID",string.Concat("'",strSessionKey,"'"));
                lngReturn = mobjDataAccessor.ExecuteSp(strProcName,htParams,"APD");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngReturn;
        }

        #endregion
    }
}
