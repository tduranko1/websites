﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.VisualBasic;

namespace Lynx.APD.Component.Library.SessionManagerV2
{
    /// <summary>
    /// Component SessionManager : Module MCommon
    /// 
    /// Common utility functions
    /// </summary>
    class MCommon
    {
        int giEncryptKey = 117;

        // ======================== //
        public bool HasValue(object objSessionKey)
        {
            bool blnreturn = false; 
            try
            {
                //if(objSessionKey == null)
                    //blnreturn

                return ((objSessionKey == null) || (isArray(objSessionKey)) || (Convert.ToString(objSessionKey).Length == 0));
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool isArray(object objSessionKey)
        {
            return objSessionKey.GetType().IsArray;
        }

        public bool isGUID(string objSessionKey)
        {
            return (objSessionKey.StartsWith("{") || objSessionKey.EndsWith("}") || (objSessionKey.Length > 0));
        }

        public string EncryptSessionKeys(string objSessionKey)
        {
            return (isGUID(objSessionKey) ? objSessionKey : "");
        }


        public string EncryptString(string strText, int intEncryptKey)
        {            
            string strTemp = "";
            if ((intEncryptKey > 255) || (intEncryptKey <= 0))
                intEncryptKey = 255;
            for (int index = 0; index < strText.Length; index++)
            {
                strTemp += Strings.Chr(Strings.Asc(strText.Substring(index, 1)) ^ intEncryptKey);
            }
            return strTemp;
        }


        public string DecryptSessionKey(string strSessionKey)
        {
            if (isGUID(strSessionKey))
                return strSessionKey;
            else
                return EncryptString(strSessionKey, giEncryptKey);
        }

        public string ZeroPad(long lngValue, int intDigits)
        {
            string strLngValue = Convert.ToString(lngValue);
            while ((strLngValue.Length) < intDigits)
            {
                strLngValue = "0" + strLngValue;
            }
            return strLngValue;
        }
        // =============================== //



        #region Private Helper Functions
        /// <summary>
        ///  Pads a number out with zeros to make the requested number of digits.
        /// </summary>
        /// <param name="lngValue"></param>
        /// <param name="intDigits"></param>
        /// <returns></returns>      
        #endregion
    }
}
