﻿using System;
using System.Xml;
using System.Configuration;
using Lynx.APD.Component.Library.Common;

namespace Lynx.APD.Component.Library.PartnerData
{
    public partial class CExecute
    {
        //PartnerDataClasses.MLAPDPartnerData mobjMLAPDPartnerData = new MLAPDPartnerData();

        // MLAPDPartnerData objMLAPDPartnerData = new MLAPDPartnerData();
        PartnerDataMgr.CExecute mobjExecute = null;
        public string APP_NAME = "LAPDPartnerData",
        MODULE_NAME = "CExecute";
        private Boolean mblnDebugMode;
        private Boolean mblnDisableXmlReturn;       
        // public clsHelper mobjclsHelper = new clsHelper();
        AppSettingsReader appSettingsReader;        
        //private enum Error_Codes
        //{
        //    //eXmlParseError = MLAPDPartnerData.LAPDPartnerData_FirstError + Convert.ToInt64(100) //MLAPDPartnerData.LAPDPartnerData_FirstError + 0x100  //100 added from LAPDPartnerData_FirstError
        //}

        /// <summary>
        /// Initialize and Terminate
        /// </summary>
        /// 
        public CExecute()
        {
            InitializeGlobals();
        }

        /// <summary>
        /// Destructor to clear the objects
        /// </summary>
        //~CExecute()
        //{
        //    TerminateGlobals();
        //}

        /// <summary>
        /// Intialize the Config details
        /// </summary>
        private void InitializeGlobals()
        {
            string configPath = string.Empty;
            try
            {
                //mobjEvents = new SiteUtilities.CEvents();
                //mobjEvents.ComponentInstance = "Partner Data";
                //appSettingsReader = new AppSettingsReader();
                //configPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                ////mobjEvents.Initialize(AppDomain.CurrentDomain.BaseDirectory + "\\..\\config\\config.xml");
                //mobjEvents.Initialize(configPath);
                //mblnDebugMode = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Clear the objects reference
        /// </summary>
        public void TerminateGlobals()
        {
            //mobjEvents = null;
        }

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public long SendXmlDoc(string strXmlDoc, string strTradingPartner)
        {
            string strResult = string.Empty;
            long sendXmlDoc = 1;
            try
            {
                mblnDisableXmlReturn = true;
                strResult = SendXmlRequest(strXmlDoc, strTradingPartner);

                if (!StringExtensions.IsNullOrWhiteSpace(strResult))  
                {
                    if (strResult.StartsWith("<Error"))
                        sendXmlDoc = 0;
                }
                else
                    sendXmlDoc = 1;

                mblnDisableXmlReturn = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sendXmlDoc;
        }

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public string SendXmlRequest(string strXmlDoc, string strTradingPartner)
        {
            // object objLDM = null;
            XmlDocument objXmldoc = null;
            bool blnRaiseToCaller = false;
            string PROC_NAME = string.Empty,
                strSendXmlRequest = string.Empty;

            try
            {
                mobjExecute = new PartnerDataMgr.CExecute();             
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, APP_NAME, "SendXmlRequest" });
                blnRaiseToCaller = false;

                //'Used by our error handler in LYNX to create a log of the components work
                InitializeGlobals(); //TODO - Check and delete this line, since we have constructor.              

                blnRaiseToCaller = true;    //'The following does relate to caller.

                //'Validate our parameters and trace them IF they are invalid.
                if (string.IsNullOrEmpty(strXmlDoc.Trim()))
                    throw new Exception("No XML Document Received.");                   

                if (string.IsNullOrEmpty(strTradingPartner.Trim()))
                    throw new Exception("No Trading Partner value received");     
                   
                objXmldoc = new XmlDocument();

                if (!string.IsNullOrEmpty(strXmlDoc.Trim()))
                    objXmldoc.LoadXml(strXmlDoc);

                blnRaiseToCaller = false;

                //objLDM = objMLAPDPartnerData.CreateObjectEx("LAPDPartnerDataMgr.CExecute");
                strSendXmlRequest = mobjExecute.ConsumePartnerXmlTransaction(strXmlDoc, strTradingPartner, !mblnDisableXmlReturn);
            }
            catch (Exception ex)
            {               
                throw ex;
            }
            finally
            {
                mobjExecute = null;
                objXmldoc = null;             
                TerminateGlobals();
            }
            return strSendXmlRequest;
        }
    }
}
