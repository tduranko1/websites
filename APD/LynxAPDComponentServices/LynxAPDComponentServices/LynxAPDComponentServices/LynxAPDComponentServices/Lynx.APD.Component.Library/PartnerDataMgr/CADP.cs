﻿using System;
using System.Xml;
using SiteUtilities;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class CADP
    {
        #region Variables
        private string MODULE_NAME = "LAPDPartnerDataMgr.CADP.";
        CPartner mobjPartner = null;
        private Common.MDomUtils mobjMDomUtils = null;
        private MLAPDPartnerDataMgr mobjPartnerDataMgr = null;
        public SharedData objShared = null;
        private CEvents mobjCEvents = null;
        public XmlDocument mobjPassedDocument = new XmlDocument();
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        //bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);
        #endregion

        #region Enum
        //Error codes private to this module.
        private enum APD_Error_Codes : ulong
        {
            eNull = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CAPD_CLS_ErrorCodes
        }

        //These are error codes that will come
        //back in negative acknowledgements from APD.
        private enum CAPD_NACK_ErrorCodes
        {
            eNullNack
        }
        #endregion

        #region Methods

        /// <summary>
        /// Constructor with shared class variable
        /// </summary>
        /// <param name="objCADPShared"></param>
        public CADP(SharedData objCADPShared)
        {
            try
            {
                objShared = objCADPShared;
                mobjPartner = new CPartner(objShared);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            long InsertPrintImage = 0;
            try
            {
                InsertPrintImage = mobjPartner.InsertPrintImageIntoAPDV2(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return InsertPrintImage;
        }

        /// <summary>
        /// Process the transactions in Partner side.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "PartnerDatabaseTransferV2:");
            UInt64 errorCode;
            bool isPartnerDatabaseTransfer = false;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new Common.WriteLog();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransferV2", string.Concat("Process started"), "");
                // mobjPartner = new CPartner();
                mobjCEvents = new CEvents();

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb_v2();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ExtractSequenceNumber_v2("//EstimateIdentifier", string.Empty);
                        mobjPartner.StorePrintImageInPartnerDB_v2();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjPartner.ExtractSequenceNumber_v2("//TRANS_TYPE", "//SUPP_NO");
                        mobjPartner.StoreEstimateDataInPartnerDB_v2();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt_v2("ADPReceipt.XML");
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb_v2();
                        break;
                }

                //An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
                //it in udb_partner and notify development to correct the XML and resubmit it.
                if (mobjPartner.LynxID == "UNK" && mobjPartner.VehicleNumber == "UNK" && mobjPartner.AssignmentID == "0")
                {
                    errorCode = Convert.ToUInt64(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eLynxIdUnknown);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, Convert.ToString(mobjPartner.PartnerTransID), "", "", false);
                    isPartnerDatabaseTransfer = false;
                    //Do not process in APD.
                }
                else
                {
                    isPartnerDatabaseTransfer = true;
                    //Process in APD.
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransferV2", string.Concat("Process completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransferV2", string.Concat("Exception: ", ex.Message), "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
            return isPartnerDatabaseTransfer;
        }

        /// <summary>
        /// Process the APD side.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ApdDatabaseTransferV2: ");
            string strApdDatabaseTransfer = string.Empty;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new Common.WriteLog();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransferV2", string.Concat("Process started"), "");
                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt_v2("//FailureReasonCode", "//FailureReasonText");
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.ProcessBinaryFiles_v2(ref mobjPassedDocument, "//DigitalImageFile", "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", "JPG", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification_v2("ElectronicDocumentReceived", string.Concat("SHOP (", mobjPartner.TradingPartner, "): Digital images received for Vehicle ", Convert.ToString(mobjPartner.VehicleNumber)), "", "VEH", "");
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate_v2(ref mobjPartner);
                        break;
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransferV2", string.Concat("Process completed."), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransferV2", string.Concat("Exception: ", ex.Message), "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return strApdDatabaseTransfer;
        }

        /// <summary>
        /// Intial process document.
        /// </summary>
        /// <param name="strTradingPartner"></param>
        /// <param name="strPassedDocument"></param>
        /// <returns></returns>
        public bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            bool islInitProcessDocument = false;
            string PROC_NAME = string.Concat(MODULE_NAME, "InitProcessDocumentV2: ");
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new Common.WriteLog();
                // mobjPartner = new CPartner();
                mobjMDomUtils = new Common.MDomUtils();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.ADP.cs - CPartner_InitProcessDocumentV2", string.Concat("Process started"), "");

                mobjPartner.TradingPartner = strTradingPartner;
                mobjPartner.PassedDocument = strPassedDocument;

                mobjMDomUtils.LoadXml(ref mobjPassedDocument, ref strPassedDocument, string.Empty, string.Empty);  //TODO:mobjPassedDocument replaced by MLAPDPartnerDataMgr.g_objPassedDocument

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mobjPartnerDataMgr.GetChildNodeText(ref mobjPassedDocument, "/ADPTransaction/@TransactionType", true);

                //Extract the LYNXID and Vehicle number
                mobjPartner.ExtractLynxIDAndVehNum_v2();

                //Are we configured to process this document at this time?
                islInitProcessDocument = mobjPartner.CanProcessDocument_v2();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.ADP.cs - CPartner_InitProcessDocumentV2", string.Concat("Process completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.ADP.cs - CPartner_InitProcessDocumentV2", string.Concat("Exception: ", ex.Message), "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return islInitProcessDocument;
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        public string CPartner_TransferToPartner()
        {
            throw new Exception(string.Concat(Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "TransferToPartner()", "Method not implemented"));
        }

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        public bool CPartner_InitProcessDocument()
        {
            bool isInitProcessDocument = false;
            string PROC_NAME = string.Concat(MODULE_NAME, "InitProcessDocument: ");
            try
            {
                // mobjPartner = new CPartner();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mobjPartnerDataMgr.GetChildNodeText(ref mobjPassedDocument, "/ADPTransaction/@TransactionType", true);

                //Extract the LYNXID and Vehicle number
                mobjPartner.ExtractLynxIDAndVehNum();

                //Are we configured to process this document at this time?
                isInitProcessDocument = mobjPartner.CanProcessDocument();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isInitProcessDocument;
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransfer = false;
            string PROC_NAME = string.Concat(MODULE_NAME, "PartnerDatabaseTransfer: ");
            UInt64 errorCode;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new Common.WriteLog();
                mobjCEvents = new CEvents();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransfer", string.Concat("Process started"), "");

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjPartner.StorePrintImageInPartnerDB();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjPartner.ExtractSequenceNumber("//TRANS_TYPE", "//SUPP_NO");
                        mobjPartner.StoreEstimateDataInPartnerDB();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt("ADPReceipt.XML");
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                }
                //An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
                //it in udb_partner and notify development to correct the XML and resubmit it.
                if (objShared.g_strLynxID == "UNK" && objShared.g_strVehicleNumber == "UNK" && objShared.g_strAssignmentID == "0")
                {
                    errorCode = Convert.ToUInt64(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eLynxIdUnknown);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, Convert.ToString(mobjPartner.PartnerTransID), "", "", false);
                    isPartnerDatabaseTransfer = false;
                    //Do not process in APD.
                }
                else
                {
                    isPartnerDatabaseTransfer = true;
                    //Process in APD.
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransfer", string.Concat("Process Completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.ADP.cs - CPartner_PartnerDatabaseTransfer", string.Concat("Process Completed"), "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
            return isPartnerDatabaseTransfer;
        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        //TODO   mobjPartner.ProcessEstimate("") parameter as dynamic Class name
        public string CPartner_ApdDatabaseTransfer()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ApdDatabaseTransfer: ");
            string strApdDatabaseTransfer = string.Empty;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new Common.WriteLog();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransfer", string.Concat("Process started"), "");

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt("//FailureReasonCode", "//FailureReasonText");
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        //mobjPartner.ProcessBinaryFiles g_objPassedDocument, "//DigitalImageFile", _
                        //"//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", _
                        //"PDF", mobjPartner.DocTypeString, eDigitalImage

                        mobjPartner.ProcessBinaryFiles(ref mobjPassedDocument, "//DigitalImageFile", "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", "PDF", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification("ElectronicDocumentReceived", string.Concat("SHOP (", objShared.g_strPartner, "): Digital images received for Vehicle ", Convert.ToString(objShared.g_strVehicleNumber)), "", "VEH", "");
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        //mobjPartner.ProcessEstimate(this);
                        break;
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransfer", string.Concat("Process completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.ADP.cs - CPartner_ApdDatabaseTransfer", string.Concat("Exception: ", ex.Message), "");
                throw ex;
            }
            return strApdDatabaseTransfer;
        }

        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        public long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            // mobjPartner = new CPartner();
            try
            {
                return mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            throw new Exception(string.Concat(Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "SendHeartBeatRequest()", "Method not implemented"));
        }
        #endregion
    }
}
