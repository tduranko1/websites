﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    /// Contains properties to share with other classes
    /// </summary>
    public class SharedData
    {
        #region Properties
        private string _g_strPartner = string.Empty;
        private string _g_strPartnerSmallID = string.Empty;
        private string _g_strPassedDocument = string.Empty;
        private string _g_strLynxID = string.Empty;
        private string _g_strActualLynxID = string.Empty;
        private string _g_strVehicleNumber = string.Empty;
        private string _g_strAssignmentID = string.Empty;
        private string _g_strTaskID = string.Empty;
        private string _g_strActionCode = string.Empty;
        private string _g_strStatus = string.Empty;
        private string _g_strSupportDocPath = string.Empty;
        private object _g_varDocumentReceivedDate;
        private object _g_varTransactionDate;
        private bool _g_blnUsePartnerTypesForDocTypes;
        private XmlDocument _g_objPassedDocument = new XmlDocument();

        /// <summary>
        /// Partner transaction type.
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strPartner
        {
            get { return _g_strPartner; }
            set { _g_strPartner = value; }
        }

        /// <summary>
        /// Contains the PartnerID return from the Partner DB
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strPartnerSmallID
        {
            get { return _g_strPartnerSmallID; }
            set { _g_strPartnerSmallID = value; }
        }

        /// <summary>
        /// Contains the Partner xml string
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strPassedDocument
        {
            get { return _g_strPassedDocument; }
            set { _g_strPassedDocument = value; }
        }

        /// <summary>
        /// Contains the lynxID with vehicle number details
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strLynxID
        {
            get { return _g_strLynxID; }
            set { _g_strLynxID = value; }
        }

        /// <summary>
        /// Contains the actual lynxID
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strActualLynxID
        {
            get { return _g_strActualLynxID; }
            set { _g_strActualLynxID = value; }
        }

        /// <summary>
        /// Contains the vehicle number of the claim
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strVehicleNumber
        {
            get { return _g_strVehicleNumber; }
            set { _g_strVehicleNumber = value; }
        }

        /// <summary>
        /// Contians the assignmentID of the claim
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strAssignmentID
        {
            get { return _g_strAssignmentID; }
            set { _g_strAssignmentID = value; }
        }

        /// <summary>
        /// TaskID use to generate the Event and WorkFlow
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strTaskID
        {
            get { return _g_strTaskID; }
            set { _g_strTaskID = value; }
        }

        /// <summary>
        /// ActionID is get from the config to preform the correct action
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strActionCode
        {
            get { return _g_strActionCode; }
            set { _g_strActionCode = value; }
        }

        /// <summary>
        /// Status of the claim
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strStatus
        {
            get { return _g_strStatus; }
            set { _g_strStatus = value; }
        }

        /// <summary>
        /// Conatains the document path for Partner transactions
        /// </summary>
        [DataMember(IsRequired = false)]
        public string g_strSupportDocPath
        {
            get { return _g_strSupportDocPath; }
            set { _g_strSupportDocPath = value; }
        }

        /// <summary>
        /// Document datetime 
        /// </summary>
        [DataMember(IsRequired = false)]
        public object g_varDocumentReceivedDate
        {
            get { return _g_varDocumentReceivedDate; }
            set { _g_varDocumentReceivedDate = value; }
        }

        /// <summary>
        /// Tansaction datetime
        /// </summary>
        [DataMember(IsRequired = false)]
        public object g_varTransactionDate
        {
            get { return _g_varTransactionDate; }
            set { _g_varTransactionDate = value; }
        }

        [DataMember(IsRequired = false)]
        public bool g_blnUsePartnerTypesForDocTypes
        {
            get { return _g_blnUsePartnerTypesForDocTypes; }
            set { _g_blnUsePartnerTypesForDocTypes = value; }
        }

        [DataMember(IsRequired = false)]
        public XmlDocument g_objPassedDocument
        {
            get { return _g_objPassedDocument; }
            set { _g_objPassedDocument = value; }
        }

        #endregion
    }
}
