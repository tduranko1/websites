﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Web;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MBase64Utils
    {
        /// <summary>
        /// Component PartnerDataMgr : Module MBase64Utils
        /// 
        /// Utility methods used with Base64 encoding.
        /// Utility methods used with CDATA encoding.
        /// Utility methods for text and binary file access.
        /// Utility methods for decoding HexBinary data
        /// 
        /// Requires: Microsoft Scripting Runtime
        /// </summary>

        #region Declarations
        MLAPDPartnerDataMgr mlapdPartnerDataMgr = new MLAPDPartnerDataMgr();
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "MBase64Utils." });
        #endregion

        #region Enumerator
        /// <summary>
        /// Internal error codes for this module.
        /// </summary>
        private enum EventCodes : ulong
        {
            eFileDoesNotExist = 0x80065000 + 0x400,
            eHexDataInvalid
        }
        #endregion


        #region Public Function

        /// <summary>
        /// Reads in the passed file and returns a Byte array.
        /// </summary>
        public object ReadBinaryDataFromFile(string strFileName)
        {
            Int32 intFile = 0;
            long lngLen = 0;
            byte[] arrBytes = null;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadBinaryDataFromFile: " });
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (string.IsNullOrEmpty(strFileName.Trim()))
                    throw new Exception(string.Concat("PartnerDataMgr.MBaseUtils.cs:ReadBinaryDataFromFile - ", "File Name param was blank."));
                //Open the file.
                File.Open(strFileName, FileMode.Open);
                //Write the binary data.
                lngLen = strFileName.Length;
                if (lngLen < 1)
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "Open", "File not found" }));

                arrBytes = File.ReadAllBytes(strFileName);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, ex.Source, ex.Message, " File Name = ", strFileName }));
            }
            finally
            {
                objWriteLog = null;
            }
            //Return the binary data
            return arrBytes;
        }

        /// <summary>
        /// Writes the passed Byte Array to the passed file.
        /// </summary>
        public void WriteBinaryDataToFile(string strFileName, byte[] arrBuffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteBinaryDataToFile: " });
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();               
                if (string.IsNullOrEmpty(strFileName.Trim()))
                    throw new Exception("PartnerDataMgr.MBaseUtils.WriteBinaryDataToFile: File Name param was blank.");

                // objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("Before writing the file."), "");

                using (var fs = new BinaryWriter(new FileStream(strFileName, FileMode.Append, FileAccess.Write)))
                {
                    // objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("Reading the binary data."), "");
                    fs.Write(arrBuffer);
                    // objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("File write in the shared folder"), "");
                    fs.Close();
                    // objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("Completed"), "");
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("Message: ", ex.Message, "TraceLog: ", ex.StackTrace, "InnerException: ", ex.InnerException), "");
                throw ex;
            }
        }

        /// <summary>
        /// Writes binary data to a Base64 encoded XML DOM element.
        /// </summary>
        public object ReadBinaryDataFromDomElement(XmlElement objElement)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadBinaryDataFromDomElement: " });
            object varType = null;
            try
            {

                //Save the data type for restoration later.
                varType = objElement.GetType();

                //Ensure the type is base64.
                objElement.GetType().Equals("bin.base64");

                //Restore the data type.
                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Now get the binary data.
            return objElement.InnerText;
        }

        /// <summary>
        /// Writes binary data to a Base64 encoded XML DOM element.
        /// </summary>
        public void WriteBinaryDataToDomElement(XmlElement objElement, object varBuffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteBinaryDataToDomElement: " });
            object varType;
            try
            {
                //Save the data type for restoration later.
                varType = objElement.GetType();

                //Ensure the type is base64.
                objElement.GetType().Equals("bin.base64");

                //Now store the binary data.
                objElement.Value = varBuffer.ToString();

                //Restore the data type.
                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///Reads in a text file from the passed file name and returns a String that
        ///contains the entire contents of the file.
        /// </summary>
        public string ReadTextDataFromFile(string strFileName)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadTextDataFromFile: " });
            string readedContent = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (string.IsNullOrEmpty(strFileName.Trim()))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "PartnerDataMgr.MBaseUtils.cs - WriteBinaryDataToFile", string.Concat("PartnerDataMgr.MbaseUtils.ReadTextDataFromFile - ", "File Name param was blank."), "");

                using (StreamReader srFileReader = new StreamReader(strFileName))
                {
                    readedContent = srFileReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return readedContent;
        }

        /// <summary>
        ///Writes a text file from the passed String and file name.
        /// </summary>
        public void WriteTextDataToFile(string strFileName, string strData)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteTextDataToFile: " });
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (string.IsNullOrEmpty(strFileName.Trim()))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "PartnerDataMgr.MBaseUtils.cs - WriteTextDataToFile", string.Concat("PartnerDataMgr.MbaseUtils.ReadTextDataFromFile - ", "File Name param was blank."), "");

                using (StreamWriter swcontentWriter = new StreamWriter(strFileName, true))
                {
                    swcontentWriter.WriteLine(strData);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        ///Reads a text string from a CDATA element.
        /// </summary>
        public string ReadTextFromCDATA(XmlCDataSection objCdata)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadTextFromCDATA: " });
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (objCdata == null)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "PartnerDataMgr.MBaseUtils.cs - ReadTextFromCDATA", string.Concat("PartnerDataMgr.MbaseUtils.ReadTextFromCDATA - ", "DOM node param was Nothing."), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return objCdata.Data;
        }

        /// <summary>
        ///Writes the passed string to the passed CDATA element.
        /// </summary>
        public void WriteTextToCDATA(XmlCDataSection objCdata, string strData)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteTextToCDATA: " });
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (objCdata == null)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "PartnerDataMgr.MBaseUtils.cs - WriteTextToCDATA", string.Concat("PartnerDataMgr.MbaseUtils.WriteTextToCDATA - ", "DOM node param was Nothing."), "");

                objCdata.Data = strData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCdata = null;
            }
        }

        /// <summary>
        ///Decode a hexbinary
        /// </summary>
        public long DecodeHexBin(string HexString, ref byte[] buffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DecodeHexBin: " });
            string hexValue = string.Empty,
                HexStringToDecode = string.Empty;
            int intValue = 0,
                ptr = 0,
                i = 0,
                bufferSize = 0;
            double dblValue = 0;
            try
            {
                HexStringToDecode = (HexString.Trim().ToUpper());

                // MLAPDPartnerDataMgr.g_objEvents.Assert(ValidateHexString(HexStringToDecode), "Hex encoded data contains invalid character.");

                //Determine size of buffer needed.
                bufferSize = Convert.ToInt32(HexString.Length / 2);
                buffer = new byte[bufferSize];

                //Now walk through and perform decoding
                for (i = 0; i < HexString.Length - 1; i += 2)
                {
                    hexValue = string.Concat(new string[] { "&H", HexString.Substring(i, 2), "&" });
                    dblValue = Microsoft.VisualBasic.Conversion.Val(hexValue);
                    buffer[ptr] = (byte)dblValue;
                    ptr = ptr + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bufferSize;
        }

        /// <summary>
        /// Encode a byte buffer into HexBin string
        /// </summary>
        public string EncodeHexBin(long bufferSize, byte[] buffer)
        {
            string hexValue = string.Empty;
            string HexString = string.Empty;
            string strTempFile = string.Empty;
            Int32 intValue = 0;
            long ptr;
            ADODB.Stream objStream = null;

            try
            {
                objStream.Type = ADODB.StreamTypeEnum.adTypeText;
                objStream.Open();
                for (ptr = 0; ptr <= bufferSize - 1; ptr++)
                {
                    intValue = buffer[ptr];
                    if (intValue <= 15)
                        hexValue = "0" + int.Parse(intValue.ToString(), NumberStyles.HexNumber);
                    else
                        hexValue = int.Parse(intValue.ToString(), NumberStyles.HexNumber).ToString();

                    objStream.WriteText(hexValue, ADODB.StreamWriteEnum.adWriteChar);
                }
                objStream.Position = 0;
                return objStream.ReadText((int)ADODB.StreamReadEnum.adReadAll);

            }
            catch (Exception ex)
            {
                objStream = null;
                throw ex;
            }
            finally
            {
                objStream.Close();
            }

        }

        #endregion

        #region Private Helper Functions

        private bool ValidateHexString(string HexString)
        {
            Int32 ptr = 0,
                intAscVal = 0;
            bool blnOk = true;  // Innocent until proven guilt

            try
            {
                for (ptr = 1; ptr <= HexString.Length; ptr++)
                {
                    intAscVal = Convert.ToInt32(Encoding.ASCII.GetBytes(HexString.Substring(ptr, 1))[0]);
                    if ((intAscVal < 48 || intAscVal > 70 || (intAscVal > 57 && intAscVal < 65)))
                    {
                        // Not a valid Hex Character
                        // Seems its guilty afterall
                        blnOk = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnOk;
        }
        #endregion
    }
}
