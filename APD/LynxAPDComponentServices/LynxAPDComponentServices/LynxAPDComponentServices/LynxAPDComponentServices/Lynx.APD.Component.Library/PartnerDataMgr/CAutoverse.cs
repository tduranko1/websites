﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Xsl;
using Scripting;
using SiteUtilities;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using EcadMgr = Lynx.APD.Component.Library.EcadAccessorMgr;
using System.Configuration;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{

    class CAutoverse
    {

        #region Public Variables
        string MODULE_NAME = string.Concat(new string[] { "LAPDPartnerDataMgr.", "CAutoverse." });
        public CPartner mobjCpartner = null;
        public SharedData objShared = null;
        public CEvents mobjCEvents = null;
        public Common.MDomUtils mobjMdomUtils = null;
        //public PartnerData.MLAPDPartnerData mobjMLAPDPartnerData = null;
        public MPPGTransactions mobjMPPGTransactions = null;
        DataAccess.CDataAccessor mobjDataAccessor = null;
        public MLAPDPartnerDataMgr mobjMLAPDParterDataMgr = null;
        public MBase64Utils mobjMBase64Utils = null;
        EcadMgr.CWebAssignment mobjECAD = null;
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;

        string cstrTransactionHeaderTag = string.Empty,
            cstrAssignmentTag = string.Empty,
            cstrClaimMessageTag = string.Empty,
            cstrLynxAPDInsCoID = string.Empty,
            cstrClaimNumber = string.Empty,
            cstrSenderName = string.Empty,
            cstrLynxAPDInsCoDefaultAdjLogon = string.Empty,
            clngAutoverseUserID = string.Empty,
            cstrSourceApplicationPassThruData = string.Empty;
        VehData objVehData;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        #endregion

        #region Struct
        public struct VehData
        {
            public string Year;
            public string Make;
            public string Model;
        };
        #endregion

        #region Enums
        public enum Autoverse_Error_Codes : ulong
        {
            eBadAssignmentCode = 0x80067480,
            eAssignmentInvalid,
            eAttachmentIdNotFound,
            eClaimNumberInvalid,
            eNoPassThruData,
            eBadPassThruDataFormat,
            eUnhandledAutoverseNack,
            eBlankCefReturnedForEstimate,
            eXmlTransactionTooLarge,
            eInternalError,
            eInvalidEventCode,
            eMissingMessageOrDocumentNode,
            eUnexpectedReceipt,
            eInvalidInsuranceCompanyCode,
        }
        #endregion

        #region Methods
        public CAutoverse(SharedData objAutoverseShared)
        {
            try
            {
                objShared = objAutoverseShared;
                mobjCpartner = new CPartner(objShared);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendAReceipt(ref string strTemplateFile, [Optional] string strSuccess, [Optional] string lngErrorCode, [Optional] string strErrorDesc)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt:" });
            XmlDocument xmlDocumentLYNXReceipt, xmlTransDocument = null;
            string strWrappedReceipt = string.Empty,
                strPost2URL = string.Empty,
                strTransactionID = string.Empty, strTemplateDocPath = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();
                xmlTransDocument = new XmlDocument();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - SendAReceipt", "Process started", string.Concat("Params: ", "strTemplateFile: ", strTemplateFile, " strSuccess:", strSuccess, " lngErrorCode:", lngErrorCode, " strErrorDesc:", strErrorDesc));

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.SendAReceipt: Issue while loading the transaction xml.");

                //First load the receipt template from disk.
                xmlDocumentLYNXReceipt = new XmlDocument();
                strTemplateDocPath = string.Concat(objShared.g_strSupportDocPath, "\\", strTemplateFile);
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentLYNXReceipt, ref strTemplateDocPath, "SendAReceipt", "Receipt");

                strTransactionID = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:SenderTransactionID");

                SetHeaderSchtuff(ref xmlDocumentLYNXReceipt, string.Empty);
                xmlDocumentLYNXReceipt.SelectSingleNode("//ReceiptFor").InnerText = strTransactionID;
                xmlDocumentLYNXReceipt.SelectSingleNode("//SuccessFlag").InnerText = strSuccess;
                xmlDocumentLYNXReceipt.SelectSingleNode("//FailureReasonCode").InnerText = lngErrorCode;
                xmlDocumentLYNXReceipt.SelectSingleNode("//FailureReasonText").InnerText = strErrorDesc;

                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", xmlDocumentLYNXReceipt.OuterXml, objShared.g_strPartner, string.Concat(cstrClaimNumber, "-", cstrLynxAPDInsCoID));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - SendAReceipt", "Process completed", strWrappedReceipt);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CAutoverse.cs - SendAReceipt", string.Concat("Exception: ", ex.Message, " Trace: ", ex.StackTrace), string.Concat("Receipt data: ", strWrappedReceipt));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Send a business event to Partner.
        /// </summary>
        /// <param name="strEventCode"></param>
        /// <param name="strEventNote"></param>
        /// <param name="strSuccess"></param>
        /// <param name="strEventOpcode"></param>
        public void SendBusinessEvent(string strEventCode, string strEventNote, string strSuccess, string strEventOpcode)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendBusinessEvent:" });

            XmlDocument xmlDocumentObj, xmlDocumentSession, xmlTransDocument = null;
            string strEventText = string.Empty,
             strAreaCode = string.Empty,
             strExchange = string.Empty,
             strUnitNo = string.Empty,
             strExt = string.Empty,
             strEmail = string.Empty,
             strWrappedXml = string.Empty,
             strPost2URL = string.Empty;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();
                mobjDataAccessor = new DataAccess.CDataAccessor();
                xmlTransDocument = new XmlDocument();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - SendBusinessEvent", "Process started", string.Concat("Params: ", "strEventCode: ", strEventCode, " strEventNote:", strEventNote, " strSuccess:", strSuccess, " strEventOpcode:", strEventOpcode));

                //First Load Receipt template from disk
                xmlDocumentObj = new XmlDocument();
                string strPath = string.Concat(objShared.g_strSupportDocPath, "\\AutoverseBusinessEvent.XML");
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentObj, ref strPath, PROC_NAME, "Business Event");

                SetHeaderSchtuff(ref xmlDocumentObj, "");

                xmlDocumentObj.SelectSingleNode("//InsuranceCarrierID").InnerText = cstrLynxAPDInsCoID;
                xmlDocumentObj.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;
                xmlDocumentObj.SelectSingleNode("//EventCode").InnerText = strEventCode;

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.SendBusinessEvent: Issue while loading the transaction xml.");

                switch (strEventCode)
                {
                    case "Claim Assigned":
                        xmlDocumentSession = new XmlDocument();
                        //Get the claim session detail from database so we can populate adjuster info
                        hashTable = new Hashtable();
                        hashTable.Add("@LynxID", objShared.g_strLynxID);

                        string strxmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/SessionClaimDetailSP"), hashTable, "APD");
                        mobjMdomUtils.LoadXml(ref xmlDocumentSession, ref strxmlPath, PROC_NAME, "Session Claim Detail");

                        strAreaCode = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneAreaCode").InnerText;
                        strExchange = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneExchangeNumber").InnerText;
                        strUnitNo = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneUnitNumber").InnerText;
                        strExt = xmlDocumentSession.SelectSingleNode("//@OwnerUserExtensionNumber").InnerText;
                        strEmail = xmlDocumentSession.SelectSingleNode("//@OwnerUserEmail").InnerText;

                        if (xmlTransDocument.SelectSingleNode("//V_MODEL_YR") != null)
                            xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = xmlTransDocument.SelectSingleNode("//V_MODEL_YR").InnerText;
                        if (xmlTransDocument.SelectSingleNode("//V_MAKECODE") != null)
                            xmlDocumentObj.SelectSingleNode("//VehicleMake").InnerText = xmlTransDocument.SelectSingleNode("//V_MAKECODE").InnerText;
                        if (xmlTransDocument.SelectSingleNode("//V_MODEL") != null)
                            xmlDocumentObj.SelectSingleNode("//VehicleModel").InnerText = xmlTransDocument.SelectSingleNode("//V_MODEL").InnerText;

                        strEventText = string.Concat(new string[] { cstrClaimNumber, " was received by LYNX.", "Lynx ID:", objShared.g_strLynxID, "Assigned To:" });

                        strEventText = string.Concat(new string[] { strEventText, xmlDocumentSession.SelectSingleNode("//@OwnerUserNameFirst").InnerText, xmlDocumentSession.SelectSingleNode("//@OwnerUserNameLast").InnerText });

                        //Space(13) equivalent to PadRight(13)
                        strEventText = string.Concat(new string[] { strEventText, strEventText.PadRight(13, ' '), "(", strAreaCode, ")", strExchange, "-", strUnitNo });

                        if (strEventText.Length > 0)
                            strEventText = string.Concat(new string[] { strEventText, "Ext.", strExt });

                        strEventText = string.Concat(new string[] { strEventText, Environment.NewLine, strEmail });
                        break;

                    case "Claim Cancel":
                        objVehData = new VehData();
                        xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                        xmlDocumentObj.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                        xmlDocumentObj.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;

                        strEventText = "Cancellation Request Received by LYNX";
                        break;

                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(Autoverse_Error_Codes.eInvalidEventCode), "", "Invalid Event Code for Autoverse Business Event" }));
                }
                xmlDocumentObj.SelectSingleNode("//EventText").InnerText = strEventText;

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = mobjMPPGTransactions.BuildTransactionForPPG("BusinessEvent", xmlDocumentObj.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - SendBusinessEvent", "Process completed", string.Concat("XML posted: ", strWrappedXml));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CAutoverse.cs - SendBusinessEvent", string.Concat("Exception: ", ex.Message, " Trace: ", ex.StackTrace), string.Concat("Receipt data: ", strWrappedXml));
                throw ex;
            }
            finally
            {
                xmlDocumentObj = null;
                xmlDocumentSession = null;
                objWriteLog = null;
            }
        }


        /// <summary>
        /// Fills out some generic header schtuff.
        /// </summary>
        /// <param name="xmlDocumentObj"></param>
        /// <param name="strIdent"></param>
        private void SetHeaderSchtuff(ref XmlDocument xmlDocumentObj, string strIdent)
        {
            string strDate = string.Empty,
                strTime = string.Empty,
                strLynxID = string.Empty,
                    strVehNum = string.Empty;
            Int32 intPtr;
            DateTime dtNow;
            XmlNode SourcePassPassedDoc = null, xmlNodeSourcePassDom = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.SetHeaderSchtuff: Issue while loading the transaction xml.");
                dtNow = DateTime.Now;

                for (intPtr = 1; intPtr <= (7 - objShared.g_strLynxID.Length); intPtr++)
                    strLynxID = string.Concat(new string[] { strLynxID, "0" });

                strLynxID = string.Concat(new string[] { strLynxID, objShared.g_strLynxID });

                if (objShared.g_strVehicleNumber == string.Empty)
                    strVehNum = "0";
                else
                    strVehNum = objShared.g_strVehicleNumber;

                xmlDocumentObj.SelectSingleNode("//@TransactionID").InnerText = string.Concat(new string[] { dtNow.ToString("yyyyMMddhhnnssfff"), "#", strLynxID, "-", strVehNum, "#" });
                xmlDocumentObj.SelectSingleNode("//@TransactionDate").InnerText = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//@TransactionTime").InnerText = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                if (xmlDocumentObj.SelectSingleNode("@TransactionType") != null)
                    if (xmlDocumentObj.SelectSingleNode("@TransactionType").InnerText == "ACK")
                        dtNow = Convert.ToDateTime(mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:TransactionDateTime"));

                xmlDocumentObj.SelectSingleNode("//DateTime/@Date").InnerText = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//DateTime/@Time").InnerText = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//DateTime/@TimeZone").InnerText = "EST";

                //TODO Need to Confirm
                //SourcePassPassedDoc = MLAPDPartnerDataMgr.g_objPassedDocument.SelectSingleNode("//SourcePassThruData").CloneNode(true);
                //xmlNodeSourcePassDom = xmlDocumentObj.SelectSingleNode("//SourcePassThruData");
                //xmlNodeSourcePassDom.ParentNode.ReplaceChild(SourcePassPassedDoc, xmlNodeSourcePassDom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// CPartner_InitProcessDocument
        /// </summary>
        /// <returns></returns>
        public bool CPartner_InitProcessDocument()
        {
            string strTransactionID = string.Empty;
            XmlDocument xmlDocument, xmlTransDocument = null;
            XmlNode xmlNodeObj = null;
            XmlCDataSection xmlCDataSecObj;
            string strCRMId = string.Empty,
                strInsuranceCompanyID = string.Empty,
                strProc = string.Empty,
                strNamespaces = string.Empty,
                strTemplateField = string.Empty;
            bool isProcessDoc = false;
            Hashtable hashTable = null;
            XmlNamespaceManager xmlNameSpace = null;
            UInt64 errorCode;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjCEvents = new CEvents();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjDataAccessor = new DataAccess.CDataAccessor();
                objWriteLog = new Common.WriteLog();

                //strTransactionID = GetTransactionID(MLAPDPartnerDataMgr.g_objPassedDocument.OuterXml);
                xmlTransDocument = new XmlDocument();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - CPartner_InitProcessDocument", "Process started", "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    objShared.g_objPassedDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.CPartner_InitProcessDocument: Issue while loading the transaction xml.");

                //xmlNameSpace = new XmlNamespaceManager(MLAPDPartnerDataMgr.g_objPassedDocument.NameTable);
                ////Extract the transaction type from transaction header
                //foreach (XmlAttribute xmlObjAttrNode in MLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.Attributes)
                //{
                //    if ((xmlObjAttrNode.Name.Length) >= 6)
                //    {
                //        if (Left(xmlObjAttrNode.Name, 6) == "xmlns:")
                //        {
                //            strNamespaces = string.Empty;
                //            strNamespaces = xmlObjAttrNode.Value;
                //            if (xmlObjAttrNode.Name.Contains("head"))
                //                xmlNameSpace.AddNamespace(Right(xmlObjAttrNode.Name, 4), strNamespaces);
                //            if (xmlObjAttrNode.Name.Contains("com"))
                //                xmlNameSpace.AddNamespace(Right(xmlObjAttrNode.Name, 3), strNamespaces);
                //        }
                //    }
                //}
                objShared.g_blnUsePartnerTypesForDocTypes = true;
                mobjCpartner.RawDocTypeString = Convert.ToString(objShared.g_objPassedDocument.DocumentElement.Name);
                objShared.g_blnUsePartnerTypesForDocTypes = false;
                xmlTransDocument = objShared.g_objPassedDocument;
                strCRMId = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:TransactionHeader/head:InsCoCode");

                cstrClaimNumber = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:TransactionHeader/head:ClaimNumber");

                TranslateCCCInsCoToAPDInsCo(strCRMId);
                xmlNodeObj = objShared.g_objPassedDocument.CreateElement("SourcePassThruData");
                xmlCDataSecObj = objShared.g_objPassedDocument.CreateCDataSection(mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:TransactionHeader"));
                xmlNodeObj.AppendChild(xmlCDataSecObj);
                objShared.g_objPassedDocument.DocumentElement.AppendChild(xmlNodeObj);
                //Update the Shared xml document
                objShared.g_strPassedDocument = objShared.g_objPassedDocument.OuterXml;

                if (cstrLynxAPDInsCoID == string.Empty)
                {
                    strTemplateField = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateField, "N", Convert.ToString(Autoverse_Error_Codes.eInvalidInsuranceCompanyCode), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/InvalidInsuranceCompanyCode"));
                    errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eInvalidInsuranceCompanyCode);
                    mobjCEvents.HandleEvent((int)errorCode, "CPartner_InitProcessDocument", "");
                }

                cstrLynxAPDInsCoDefaultAdjLogon = mobjMLAPDParterDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/CCAV/InsuranceCompany[@CRMId='", strCRMId, "']/@DefaultUser" }));

                strProc = mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp");

                //Set Database connection
                xmlDocument = new XmlDocument();
                //Call stored procedure to retrieve claim info based on client claim number.  Load results into a DOM object.
                hashTable = new Hashtable();
                hashTable.Add("@ClientClaimNumber", cstrClaimNumber);
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@SourceApplicationID", 6);

                string strSpNamedParamsXML = mobjDataAccessor.ExecuteSpNamedParamsXML(strProc, hashTable, "APD");
                mobjMdomUtils.LoadXml(ref xmlDocument, ref strSpNamedParamsXML, string.Empty, string.Empty);

                if (xmlDocument.SelectSingleNode("//@LynxID") != null)
                    objShared.g_strLynxID = xmlDocument.SelectSingleNode("//@LynxID").InnerText;
                if (xmlDocument.SelectSingleNode("//@VehicleNumber") != null)
                    objShared.g_strVehicleNumber = xmlDocument.SelectSingleNode("//@VehicleNumber").InnerText;
                if (xmlDocument.SelectSingleNode("//@Status") != null)
                    objShared.g_strStatus = xmlDocument.SelectSingleNode("//@Status").InnerText;

                objVehData = new VehData();
                objVehData.Year = xmlDocument.SelectSingleNode("//@VehicleYear").InnerText;
                objVehData.Make = xmlDocument.SelectSingleNode("//@Make").InnerText;
                objVehData.Model = xmlDocument.SelectSingleNode("//@Model").InnerText;

                isProcessDoc = mobjCpartner.CanProcessDocument();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - CPartner_InitProcessDocument", "Process completed", "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CAutoverse.cs - CPartner_InitProcessDocument", string.Concat("Exception: ", ex.Message, " Trace: ", ex.StackTrace), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
            return isProcessDoc;
        }

        /// <summary>
        /// it set cstrLynxAPDInsCoID ID
        /// </summary>
        /// <param name="strCCCInsCo"></param>
        private void TranslateCCCInsCoToAPDInsCo(string strCCCInsCo)
        {
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                cstrLynxAPDInsCoID = mobjMLAPDParterDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/CCAV/InsuranceCompany[@CRMId='", strCCCInsCo, "']/@ApdID" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// It call InsertPrintImageIntoAPD methods
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        private long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            try
            {
                return mobjCpartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // ToDO Partially:
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer: " });
            string strTemplateFile = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - CPartner_PartnerDatabaseTransfer", string.Concat("CPartner_PartnerDatabaseTransfer Started"), "");

                strTemplateFile = "AutoverseReceipt.XML";
                switch (mobjCpartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjCpartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjCpartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.eHeartBeat:
                        //These are not stored.
                        break;
                    case CPartner.EDocumentType.eReceipt:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.eShopSearch:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        string strRaiseError = string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), PROC_NAME, "We don't process doc type '", mobjCpartner.DocTypeString, "' [", Convert.ToString(mobjCpartner.DocTypeEnum), "] for partner ", objShared.g_strPartner });
                        throw new Exception(strRaiseError);
                    case CPartner.EDocumentType.eAssignment:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - CPartner_PartnerDatabaseTransfer", string.Concat("CPartner_PartnerDatabaseTransfer completed"), "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CAutoverse.cs - CPartner_PartnerDatabaseTransfer", string.Concat("Exception: ", ex.Message, " Trace: ", ex.StackTrace), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return true;
        }

        private void CPartner_SendHeartBeatRequest()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest: " });
            XmlDocument xmlDocumentObj;
            XmlNode xmlNodeNetStat = null;
            string strWrappedXml = string.Empty,
                strPost2URL = string.Empty;
            bool g_blnDebugMode;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();

                //First load the HeartBeat template from disk.
                xmlDocumentObj = new XmlDocument();
                string strPath = string.Concat(new string[] { objShared.g_strSupportDocPath, "\\SGHeartBeat.XML" });
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentObj, ref strPath, "SendAHeartBeat", "HeartBeat");
                SetHeaderSchtuff(ref xmlDocumentObj, "HB");

                xmlNodeNetStat = xmlDocumentObj.SelectSingleNode("//NetworkStatusTest");

                //Remove the received date/time - to be filled in by SG.
                xmlNodeNetStat.RemoveChild(xmlDocumentObj.SelectSingleNode("//ReceivedDate"));

                //Remove the reply date/time - to be filled in by SG.
                xmlNodeNetStat.RemoveChild(xmlDocumentObj.SelectSingleNode("//ReplyDate"));

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = mobjMPPGTransactions.BuildTransactionForPPG("", xmlDocumentObj.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlDocumentObj = null;
                xmlNodeNetStat = null;
            }
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner: " });
            XmlNodeList xmlNodeListObj = null;
            XmlNode xmlNodeObj = null, xmlNodeVeh = null;
            XmlDocument xmlDocumentClaimVeh, xmlTransDocument = null;
            bool g_blnDebugMode;
            long lngClaimAspectID;
            string strPostingUrl = string.Empty,
                strXML = string.Empty,
                strXmlPath = string.Empty;
            Hashtable hashTable = null;
            UInt64 errorCode;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjDataAccessor = new DataAccess.CDataAccessor();
                xmlTransDocument = new XmlDocument();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - CPartner_TransferToPartner", string.Concat("CPartner_TransferToPartner started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.CPartner_TransferToPartner: Issue while loading the transaction xml.");

                if ((xmlTransDocument.SelectSingleNode("//ClaimMessage") == null) && (xmlTransDocument.SelectSingleNode("//Document") == null))
                {
                    errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eMissingMessageOrDocumentNode);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, "Transaction must contain a ClaimMessage node or at least 1 Document node", "", "", false);
                }

                //Extract Header Schtuff(to quote carpenter)
                cstrSenderName = xmlTransDocument.SelectSingleNode("//Header/SenderName").InnerText;
                objShared.g_strLynxID = xmlTransDocument.SelectSingleNode("//Header/LynxID").InnerText;
                cstrClaimNumber = xmlTransDocument.SelectSingleNode("//Header/ClaimNumber").InnerText;
                cstrLynxAPDInsCoID = xmlTransDocument.SelectSingleNode("//Header/InsuranceCarrierID").InnerText;
                objShared.g_strVehicleNumber = xmlTransDocument.SelectSingleNode("//Header/VehicleNumber").InnerText;
                strPostingUrl = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                // objVehData = new VehData();
                objVehData.Year = xmlTransDocument.SelectSingleNode("//Header/VehicleYear").InnerText;
                objVehData.Make = xmlTransDocument.SelectSingleNode("//Header/VehicleMake").InnerText;
                objVehData.Model = xmlTransDocument.SelectSingleNode("//Header/VehicleModel").InnerText;

                //We need to obtain the ClaimAspectID
                hashTable = new Hashtable();
                hashTable.Add("@ClientClaimNumber", cstrClaimNumber);
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@SourceApplicationID", 6);

                xmlDocumentClaimVeh = new XmlDocument();
                strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), hashTable, "APD");
                mobjMLAPDParterDataMgr.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                strXmlPath = string.Empty;

                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");

                if (xmlNodeVeh == null)
                    lngClaimAspectID = 0;
                else
                    lngClaimAspectID = Convert.ToInt64(mobjMLAPDParterDataMgr.GetChildNodeText(ref xmlDocumentClaimVeh, "//Vehicle/@ClaimAspectID", false));// Convert.ToInt64(xmlDocumentClaimVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                xmlNodeVeh = null;
                xmlNodeVeh = null;

                if (lngClaimAspectID > 0)
                {
                    xmlDocumentClaimVeh = new XmlDocument();
                    hashTable = new Hashtable();
                    hashTable.Add("@LynxID", objShared.g_strLynxID);
                    strXmlPath = mobjDataAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/PassThruSP"), hashTable, "APD");
                    // strXmlPath = MLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/PassThruSP"), new object[] { MLAPDPartnerDataMgr.g_strLynxID });
                    mobjMLAPDParterDataMgr.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                    strXmlPath = string.Empty;

                    cstrSourceApplicationPassThruData = xmlDocumentClaimVeh.SelectSingleNode(string.Concat("//ClaimAspect[@ClaimAspectID='", Convert.ToString(lngClaimAspectID), "']/@SourceApplicationPassThruData")).InnerText;
                }
                //Is there a Claim Message to send?

                if (xmlTransDocument.SelectSingleNode("//ClaimMessage").InnerText.Trim() != string.Empty)
                {
                    SendMessageToPartner(strPostingUrl, xmlTransDocument.SelectSingleNode("//ClaimMessage"));
                    mobjCpartner.SendWorkflowNotification("External Claim Message Sent", "Extenal Claim Message Sent to Partner");
                }
                xmlNodeListObj = xmlTransDocument.SelectNodes("//Document");

                if (xmlNodeListObj.Count > 0)
                {
                    foreach (XmlNode xmlDocNode in xmlNodeListObj)
                    {
                        TransferDocumentToPartner(strPostingUrl, xmlDocNode);
                    }

                    //Fire off event indicating that Documents were sent.
                    mobjCpartner.SendWorkflowNotification("Documents Sent To Carrier", "Documents Sent to Carrier", "", "VEH", "");
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - CPartner_TransferToPartner", string.Concat("CPartner_TransferToPartner completed"), "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CAutoverse.cs - CPartner_TransferToPartner", string.Concat("Exception:", ex.Message, " Trace: ", ex.StackTrace), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
            return "";
        }

        /// <summary>
        /// For assignment transaction CCC did not put a qualified namespace for the
        /// Assignment Root Element like they did for all other transactions rather they
        /// namespaced the document itself.  This caused havoc with styling
        /// </summary>
        /// <param name="strPostingUrl"></param>
        /// <param name="xmlDocNode"></param>
        private void StripDocNamespace()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StripDocNameSpace:" }),
                strXML = string.Empty,
                strPartToRemove = string.Empty,
                strNamespaces = string.Empty;
            int intStartPtr, intEndPtr;
            XmlDocument xmlTransDocument = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.StripDocNamespace: Issue while loading the transaction xml.");

                strXML = xmlTransDocument.OuterXml;

                intStartPtr = strXML.IndexOf(string.Concat(new string[] { "xmlns=", string.Empty }), 1, StringComparison.CurrentCulture);

                if (intStartPtr < 1)
                    return;

                intEndPtr = strXML.IndexOf(string.Empty, (intStartPtr + 7), StringComparison.CurrentCulture);
                strPartToRemove = this.Mid(strXML, intStartPtr, (intEndPtr - intStartPtr + 1));
                strXML = strXML.Replace(strPartToRemove, "");
                xmlTransDocument = new XmlDocument();
                xmlTransDocument.LoadXml(strXML);

                foreach (XmlAttribute objxmlAttr in xmlTransDocument.Attributes)
                {
                    if (objxmlAttr.Name.Length >= 6)
                    {
                        if (this.Left(objxmlAttr.Name, 6) == "xmlns:")
                            strNamespaces = string.Concat(new string[] { strNamespaces, objxmlAttr.Name, "='", objxmlAttr.Value, "' " });
                    }
                }
                // TODO Set Property using XmlNameSpaceManager
                XmlNamespaceManager xmlNameSpaceManager = new XmlNamespaceManager(xmlTransDocument.NameTable);
                xmlNameSpaceManager.AddNamespace("SelectionNamespaces", strNamespaces);
                // mobjMLAPDParterDataMgr.g_objPassedDocument.SetProperty("SelectionNamespaces", strNamespaces);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDocumentEMSData"></param>
        /// <returns></returns>
        private bool ValidateEMSData(XmlDocument xmlDocumentEMSData)
        {

            XmlDocument xmlDocumentValidateXsl;
            bool blnValidEMS = true;
            string strEMSIssues = string.Empty,
                strValue = string.Empty,
                strTrace = string.Empty,
                strResult = string.Empty,
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ValidateEMSData: " });
            UInt64 errorCode;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - ValidateEMSData", string.Concat("ValidateEMSData started"), xmlDocumentEMSData.OuterXml);

                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjCEvents = new CEvents();
                //EMS only passes a 2 digit vehicle year -- this needs to be changed to a 4 digit year
                if (xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText.Length > 0)
                {
                    strValue = xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText.Trim();
                    double dNum;
                    bool isNum = double.TryParse(strValue, out dNum);
                    if (isNum)
                    {
                        if (Convert.ToInt32(strValue) > 50)
                            strValue = string.Concat("19", strValue);
                        else
                            strValue = string.Concat("20", strValue);

                        xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText = strValue;
                    }
                }

                //Insure the Claim Number specified in EMS Payload is the same as the value in header
                if (cstrClaimNumber.Trim() != xmlDocumentEMSData.SelectSingleNode("//AD1/CLM_NO").InnerText.Trim())
                {
                    strEMSIssues = string.Concat(strEMSIssues, mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/MismatchedClaimNumbers").Replace("%headerclaimnumber%", cstrClaimNumber).Replace("%emsclaimnumber%", xmlDocumentEMSData.SelectSingleNode("//AD1/CLM_NO").InnerText.Trim()));
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - ValidateEMSData", strEMSIssues, xmlDocumentEMSData.OuterXml);
                    blnValidEMS = false;
                }
                string strPath = string.Concat(objShared.g_strSupportDocPath, "\\AutoverseEMSValidate.xsl");
                xmlDocumentValidateXsl = new XmlDocument();
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentValidateXsl, ref strPath, PROC_NAME, "");
                //Todo: xsl
                if (xmlDocumentEMSData != null)
                {
                    using (StringReader sReader = new StringReader(xmlDocumentEMSData.OuterXml))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform objTransformXSL = new XslCompiledTransform();
                        objTransformXSL.Load(xmlDocumentValidateXsl);
                        objTransformXSL.Transform(xReader, xWriter);
                        strResult = sWriter.ToString();
                    }
                }

                strEMSIssues = string.Concat(strEMSIssues, strResult);
                if (!strEMSIssues.Equals(string.Empty))
                {
                    string strTemplate = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplate, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), strEMSIssues);
                    errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eAssignmentInvalid);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, strEMSIssues, "", "", false); //TODO Need to convert the Int data type
                    blnValidEMS = false;
                }

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - ValidateEMSData", string.Concat("ValidateEMSData completed"), xmlDocumentEMSData.OuterXml);
                goto ValidateEMSExit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
        ValidateEMSExit:
            return blnValidEMS;
        }

        /// <summary>
        /// Transfer to partner DB
        /// </summary>
        /// <param name="strPostingUrl"></param>
        /// <param name="xmlNodeObj"></param>
        private void TransferDocumentToPartner(string strPostingUrl, XmlNode xmlNodeObj)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner: " });
            string strPartnerXml = string.Empty,
                  strTransactionID = string.Empty,
                  strInsCo = string.Empty,
                  strVehYr = string.Empty,
                  strVehMake = string.Empty,
                  strVehMode = string.Empty,
                  strXML = string.Empty,
                  strTemplate = string.Empty,
                  strDocType = string.Empty,
                  strDocumentID = string.Empty,
                  strTransType = string.Empty,
                  strEncodedFile = string.Empty;
            XmlDocument xmlDocumentPartner;
            DateTime dtNow;
            XmlCDataSection xmlCDataSectionObj;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                objWriteLog = new Common.WriteLog();

                dtNow = DateTime.Now;
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - TransferDocumentToPartner", string.Concat("TransferDocumentToPartner started"), "");

                strDocumentID = xmlNodeObj.SelectSingleNode("@DocumentID").InnerText;
                strDocType = xmlNodeObj.SelectSingleNode("DocumentType").InnerText;
                strTransType = strDocType;

                switch (strDocType)
                {
                    case "DigitalImage":
                        strTemplate = "AutoverseDigitalImage.xml";
                        break;
                    case "EstimatePI":
                        strTemplate = "AutoversePrintImage.xml";
                        break;
                    default:
                        if (this.Left(strDocType, 8) == "Document")
                        {
                            strTransType = "Document";
                            strDocType = this.Mid(strDocType, 10, strDocType.Length);
                            strTemplate = "AutoverseDocumentXfer.xml";
                        }
                        else
                            strDocType = string.Empty;
                        break;
                }

                if (strDocType != string.Empty)
                {
                    //Instantiate and Load Template from disk
                    xmlDocumentPartner = new XmlDocument();
                    string strPath = string.Concat(new string[] { objShared.g_strSupportDocPath, "\" & strTemplate" });
                    mobjMdomUtils.LoadXmlFile(ref xmlDocumentPartner, ref strPath, "TransferDocumentsToPartner", "Document Xfer");
                    strTransactionID = string.Concat(dtNow.ToString("yyyyMMddhhnnssfff"), "#", strDocumentID, "#");

                    //Populate all common elements
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionID").Value = strTransactionID;
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionDate").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionTime").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                    xmlDocumentPartner.SelectSingleNode("//InsuranceCarrierID").InnerText = strInsCo;
                    xmlDocumentPartner.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;

                    objVehData = new VehData();

                    xmlDocumentPartner.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                    xmlDocumentPartner.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                    xmlDocumentPartner.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;

                    xmlDocumentPartner.SelectSingleNode("//DateTime/@Date").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                    xmlDocumentPartner.SelectSingleNode("//DateTime/@Time").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                    //Add in Souce Pass Thru Info
                    xmlCDataSectionObj = xmlDocumentPartner.CreateCDataSection(cstrSourceApplicationPassThruData);
                    xmlDocumentPartner.SelectSingleNode("//SourcePassThruData").AppendChild(xmlCDataSectionObj);

                    strEncodedFile = xmlNodeObj.SelectSingleNode("FilePath").InnerText;
                    // Now populate specific elements based on document type

                    switch (strDocType)
                    {
                        case "DigitalImage":
                            xmlDocumentPartner.SelectSingleNode("//ImageLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//FileName").InnerText = string.Concat(new string[] { "LYNX_", strDocumentID, ".jpg" });
                            xmlDocumentPartner.SelectSingleNode("//ImageFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//EstimateIdentifier").InnerText = xmlNodeObj.SelectSingleNode("SequenceNumber").InnerText;
                            break;
                        case "EstimatePI":
                            xmlDocumentPartner.SelectSingleNode("//FileLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//ImageFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//EstimateIdentifier").InnerText = xmlNodeObj.SelectSingleNode("SequenceNumber").InnerText;
                            break;
                        default:
                            xmlDocumentPartner.SelectSingleNode("//FileLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//DocumentFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//DocumentType").InnerText = strDocType;
                            break;
                    }
                    //Wrap in SOAP
                    strXML = mobjMPPGTransactions.BuildTransactionForPPG(strTransType, xmlDocumentPartner.OuterXml, objShared.g_strPartner, string.Concat(new string[] { "D-", strDocumentID }));

                    //Smellin like Irish Spring off to PPG we go
                    mobjMPPGTransactions.XmlHttpPost(strPostingUrl, strXML);
                    mobjCpartner.SetDocumentSendTransactionID(strDocumentID, strTransactionID);

                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - TransferDocumentToPartner", string.Concat("TransferDocumentToPartner completed"), "");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlCDataSectionObj = null;
                xmlDocumentPartner = null;
                xmlNodeObj = null;
                objWriteLog = null;
            }
        }

        private string EncodeFile(string strFileToEncode)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "EncodeFile:" });
            ADODB.Stream objStream = null;

            Byte[] buffer = null;
            int bufferSize;
            FileSystemObject fso;
            try
            {
                mobjCEvents = new CEvents();
                mobjMBase64Utils = new MBase64Utils();
                fso = new FileSystemObject();
                objStream = new ADODB.Stream();
                mobjCEvents.Assert(fso.FileExists(strFileToEncode), string.Concat(new string[] { "File specified, '", strFileToEncode, "', does not exist or can not be accessed." }));

                objStream.Type = ADODB.StreamTypeEnum.adTypeBinary;
                objStream.Open();

                objStream.LoadFromFile(strFileToEncode);
                bufferSize = objStream.Size;

                Array.Resize(ref buffer, bufferSize);

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, objStream.Read());
                    buffer = memoryStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objStream == null)
                {
                    if (objStream.State != ADODB.ObjectStateEnum.adStateClosed)
                        objStream.Close();

                    objStream = null;
                }
            }
            return mobjMBase64Utils.EncodeHexBin(bufferSize, buffer);
        }

        private string ExtractDocumentIDFromTransactionID(string TransactionID)
        {
            //string PROC_NAME = "ExtractDocumentIDFromTransactionID:";
            Int32 intStartPtr, intEndPtr;
            string strDocumentID = string.Empty, strReturn = string.Empty;
            try
            {
                intStartPtr = TransactionID.IndexOf("#", 1);

                if (intStartPtr > 0)
                {
                    intStartPtr = intStartPtr + 1;
                    if (intStartPtr < TransactionID.Length)
                    {
                        intEndPtr = TransactionID.IndexOf("#", intStartPtr);
                        strReturn = Mid(TransactionID, intStartPtr, (intEndPtr - intStartPtr)).Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                return strReturn;
            }
            return strReturn;
        }

        private void HandleDocumentXferReceipt()
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "HandleDocumentXferReceipt:" });

            string strErrCode = string.Empty,
             strDocumentID = string.Empty,
             strTransactionID = string.Empty,
             strErrDesc = string.Empty,
             strDocSendStatus = string.Empty,
             strTrace = string.Empty;
            long errorCode;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjCEvents = new CEvents();
                xmlTransDocument = new XmlDocument();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - HandleDocumentXferReceipt", string.Concat("HandleDocumentXferReceipt Started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.HandleDocumentXferReceipt: Issue while loading the transaction xml.");

                strErrCode = xmlTransDocument.SelectSingleNode("//ErrorCode").InnerText;
                strTransactionID = xmlTransDocument.SelectSingleNode("//head:SenderTransactionID").InnerText;

                strDocumentID = ExtractDocumentIDFromTransactionID(strTransactionID);

                if (strDocumentID.IndexOf("-") > 0)
                {
                    return;                    //this is an ack for claim msg that glaxis made from business event. // Need to confirm
                }

                double dNum;
                bool isNum = double.TryParse(strDocumentID, out dNum);

                if (strDocumentID == string.Empty || !isNum)
                    throw new Exception(string.Concat(Convert.ToString(Autoverse_Error_Codes.eInternalError), "Invalid Document ID returned in NACK SenderTransactionID"));

                switch (Convert.ToInt16(strErrCode))
                {
                    case 0:
                        //ASK
                        strDocSendStatus = "S";
                        break;
                    default:
                        //NASK
                        strDocSendStatus = "NS";
                        strTrace = string.Concat("LynxID=", objShared.g_strLynxID, "  DocumentID=", strDocumentID);
                        //Send out notification to APD Production Monitoring
                        errorCode = Convert.ToInt64(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived);
                        mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, "Document/Message Failed to Send", strTrace, string.Empty, false);
                        break;
                }
                mobjCpartner.UpdateDocumentSendStatus(strDocumentID, strDocSendStatus);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - HandleDocumentXferReceipt", string.Concat("HandleDocumentXferReceipt Completed"), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
        }

        private void SendMessageToPartner(string strPostingUrl, XmlNode xmlMsgNode)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendMessageToPartner:" });
            string strMessageText = string.Empty,
                objPartnerXml = string.Empty,
                strTransactionID = string.Empty,
                strSenderName = string.Empty,
                strXML = string.Empty,
                strDocumentID = string.Empty,
                  strPath = string.Empty;
            DateTime dtNow;
            XmlDocument xmlDocumentPartner = null;
            XmlCDataSection xmlCDataSectionObj;
            Common.WriteLog objWriteLog = null;
            try
            {              
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - SendMessageToPartner", string.Concat("SendMessageToPartner Started"), "");

                strMessageText = xmlMsgNode.InnerText;
                strDocumentID = xmlMsgNode.SelectSingleNode("@DocumentID").InnerText;

                dtNow = DateTime.Now;
                //Load Message Template from disk
                strPath = string.Concat(objShared.g_strSupportDocPath, "\\AutoverseMessage.xml");
                xmlDocumentPartner = new XmlDocument();
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentPartner, ref strPath, "SendMessageToPartner", "Message");

                strTransactionID = string.Concat(dtNow.ToString("yyyyMMddhhnnssfff#"), strDocumentID, "#");

                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionID").Value = strTransactionID;
                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionDate").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionTime").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                xmlDocumentPartner.SelectSingleNode("//InsuranceCarrierID").InnerText = cstrLynxAPDInsCoID;
                xmlDocumentPartner.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;
                xmlDocumentPartner.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                xmlDocumentPartner.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                xmlDocumentPartner.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;
                xmlDocumentPartner.SelectSingleNode("//SenderName").InnerText = cstrSenderName;
                xmlDocumentPartner.SelectSingleNode("//MessageText").InnerText = strMessageText;
                xmlDocumentPartner.SelectSingleNode("//MessageLength").InnerText = Convert.ToString(strMessageText.Length);
                xmlDocumentPartner.SelectSingleNode("//DateTime/@Date").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentPartner.SelectSingleNode("//DateTime/@Time").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                //Insert Pass Thru data into Transaction
                xmlCDataSectionObj = xmlDocumentPartner.CreateCDataSection(cstrSourceApplicationPassThruData);

                xmlDocumentPartner.SelectSingleNode("//SourcePassThruData").AppendChild(xmlCDataSectionObj);

                //Build SOAP Envelope
                strXML = mobjMPPGTransactions.BuildTransactionForPPG("Message", xmlDocumentPartner.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Post da puppy
                mobjMPPGTransactions.XmlHttpPost(strPostingUrl, strXML);

                mobjCpartner.SetDocumentSendTransactionID(strDocumentID, strTransactionID);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - SendMessageToPartner", string.Concat("SendMessageToPartner Completed"), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlDocumentPartner = null;
                strPath = string.Empty;
            }
        }

        private void ProcessNewAssignmentTransaction()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessNewAssignmentTransaction:" });
            object objECAD = null,
             objTempEvents = null;
            bool blnValidShop, blnPossibleTotalLoss = false;
            long lngLynxShopID, lngClaimAspectID, lngPackageSize, lngNumber;
            byte[] buffer = null;
            XmlNode xmlNodeElem1 = null, xmlNodeVeh = null;
            XmlCDataSection xmlCDataSectionObj = null;
            XmlDocument xmlDocumentWithEmsXml = null,
                xmlDocumentEms = null,
                xmlDocumentAssignment = null,
                xmlDocumentAssignmentVerify = null, xmlDocumentClaimAspect = null, objTempPassedDocument = null, xmlTransDocument = null;
            string strCRMId = string.Empty,
                strCCCShopID = string.Empty,
                strCarrierRepInfo = string.Empty,
                strHexEncodedEmsPackage = string.Empty,
                strAssignmentType = string.Empty,
                strLossState = string.Empty,
                strRFWarningMessage = string.Empty,
                strAssignVerify = string.Empty,
                strRFWarningNote = string.Empty,
                strError = string.Empty,
                strSource = string.Empty,
                strXmlPath = string.Empty,
                strDocumentAssignment = string.Empty,
                strTempPath = string.Empty,
                strAutoverseReceipt = string.Empty, strPartner = string.Empty;
            Hashtable hashTable = null;
            UInt64 errorCode;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("ProcessNewAssignmentTransaction Start"), "");
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMBase64Utils = new MBase64Utils();
                mobjMdomUtils = new Common.MDomUtils();
                mobjDataAccessor = new DataAccess.CDataAccessor();
                mobjECAD = new EcadMgr.CWebAssignment();
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.ProcessNewAssignmentTransaction: Issue while loading the transaction xml.");
                // mobjCEvents.Trace("Processing 'New Assignment' transaction.");
                // he assignment "data" comes in as a HEXBinary EMS Package embedded inside an XML transaction
                // In order for us to process this we'll need to extract that information, decode it, and convert
                // the resulting ems package into XML.

                //TODO Need to change as SelectSingleNode instead of GetElementsByTagName
                strHexEncodedEmsPackage = xmlTransDocument.GetElementsByTagName("EMSPayload").Item(0).InnerText;
                // strHexEncodedEmsPackage = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//EMSPayload");              


                lngPackageSize = mobjMBase64Utils.DecodeHexBin(strHexEncodedEmsPackage, ref buffer);
                xmlDocumentEms = mobjCpartner.ConvertEmsDBFToXml(ref buffer);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("EMS xml data ", xmlDocumentEms.OuterXml), "");

                if (!ValidateEMSData(xmlDocumentEms))
                    return;

                switch (xmlDocumentEms.SelectSingleNode("//TLOS_IND").InnerText.ToLower())
                {
                    case "true":
                        blnPossibleTotalLoss = true;
                        break;
                    case "yes":
                        blnPossibleTotalLoss = true;
                        break;
                }
                //Now we need to replace the EMSPayLoad hexbin data with xml data //TODO need to do the correct XML Element functionality
                xmlNodeElem1 = xmlTransDocument.CreateElement(xmlDocumentEms.DocumentElement.Name);
                xmlTransDocument.GetElementsByTagName("EMSPayload").Item(0).InnerText = string.Empty;
                xmlNodeElem1.InnerXml = xmlDocumentEms.DocumentElement.InnerXml;
                xmlTransDocument.GetElementsByTagName("EMSPayload").Item(0).AppendChild(xmlNodeElem1);

                //MLAPDPartnerDataMgr.g_objPassedDocument.GetElementsByTagName("EMSPayload").Item(0).ReplaceChild(xmlDocumentEms.DocumentElement.CloneNode(true), xmlNodeElem1);

                xmlDocumentEms = null;

                //Add element indicating mapped APD Client ID //TODO need to to do the function works
                xmlNodeElem1 = xmlTransDocument.CreateElement("LynxInsCoID");
                xmlNodeElem1.InnerText = cstrLynxAPDInsCoID;
                xmlTransDocument.DocumentElement.AppendChild(xmlNodeElem1);

                //Prepare Assignment
                objTempPassedDocument = xmlTransDocument;
                // objTempEvents = MLAPDPartnerDataMgr.g_objEvents;
                strTempPath = string.Concat(objShared.g_strSupportDocPath, "\\AutoverseAssignmentXfer.xsl");
                xmlDocumentAssignment = mobjMdomUtils.TransformDomAsDom(ref objTempPassedDocument, strTempPath);// mobjMdomUtils.TransformDomAsDom(ref objTempPassedDocument, strTempPath, ref objTempEvents);

                xmlDocumentAssignmentVerify = new XmlDocument();
                hashTable = new Hashtable();
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@AssignmentTypeID", xmlDocumentAssignment.SelectSingleNode("//Vehicle/@AssignmentTypeID").InnerText);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/VerifyAssignmentType"), "SP Param: ", cstrLynxAPDInsCoID, " ", xmlDocumentAssignment.SelectSingleNode("//Vehicle/@AssignmentTypeID").InnerText), "");
                strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/VerifyAssignmentType"), hashTable, "APD");
                mobjMdomUtils.LoadXml(ref xmlDocumentAssignmentVerify, ref strXmlPath, string.Empty, string.Empty);
                strAssignVerify = xmlDocumentAssignmentVerify.SelectSingleNode("//@result").InnerText;
                strXmlPath = string.Empty;

                if (strAssignVerify.ToLower() != "ok")
                {
                    //Assignment Type is not valid for this client
                    errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eAssignmentInvalid);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, strAssignVerify, string.Empty, string.Empty, false);
                    string strTemplateFile = "AutoverseReceipt.XML";
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Post the receipt to the URL ", mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL")), "");
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), strAssignVerify);
                    return;
                }

                //Extract CCC's Shop ID from header of passed documents, retrieve Lynx's Shop Information from AutoverseID
                // Since CCC Does not pass us Loss State will assume the shop's state is loss state

                strLossState = string.Empty;

                strCCCShopID = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:AssignedToID");
                if (strCCCShopID != string.Empty)
                {
                    XmlDocument objShopXml = new XmlDocument();
                    hashTable = new Hashtable();
                    hashTable.Add("@AutoverseID", strCCCShopID);
                    hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/ShopInfoFromAutoverseID")), "");
                    strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/ShopInfoFromAutoverseID"), hashTable, "APD");
                    mobjMdomUtils.LoadXml(ref objShopXml, ref strXmlPath, string.Empty, string.Empty);
                    strXmlPath = string.Empty;

                    if (objShopXml.DocumentElement.ChildNodes.Count == 0)
                        blnValidShop = false;
                    else
                        blnValidShop = (objShopXml.SelectSingleNode("//Shop/@AllowSelection").InnerText == "1");

                    if (!blnValidShop)
                    {
                        //Can not match the AutoverseID to a Lynx DRP
                        strRFWarningNote = string.Concat(
                           "Assignment was received with a shop selected that could not be mapped to a shop in Lynx DRP: ",
                           Environment.NewLine,
                           Environment.NewLine,
                           "Repair Facility Information Supplied with Assignment: ",
                           "   Autoverse ID: ", strCCCShopID, Environment.NewLine,
                           "           Name: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CO_NM"), Environment.NewLine,
                           "        Address: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ADDR1"), Environment.NewLine,
                           "City, State Zip: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CITY"), ", ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ST"), ", ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ZIP"), Environment.NewLine,
                           "        Phone #: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_PH1"), Environment.NewLine,
                           "          Fax #: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_FAX"), Environment.NewLine,
                           "   Contact Name: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CT_FN"), " ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CT_LN")
                           );

                        strRFWarningMessage = string.Concat(
                           "Received an Assignment from Autoverse for Insurance Company = ", cstrLynxAPDInsCoID, " in which the Repair Facility could not be mapped to a Lynx DRP.", Environment.NewLine,
                           "Pertaining to Lynx ID#%lynxid%",
                           Environment.NewLine,
                           Environment.NewLine,
                           "Repair Facility Information: ",
                           Environment.NewLine,
                           "   Autoverse ID: ", strCCCShopID, Environment.NewLine,
                           "           Name: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CO_NM"), Environment.NewLine,
                           "        Address: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ADDR1"), Environment.NewLine,
                           "City, State Zip: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CITY"), ", ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ST"), ", ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_ZIP"), Environment.NewLine,
                           "        Phone #: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_PH1"), Environment.NewLine,
                           "          Fax #: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_FAX"), Environment.NewLine,
                           "   Contact Name: ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CT_FN"), " ", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/RF_CT_LN")
                         );
                        strLossState = xmlTransDocument.SelectSingleNode("//AD2/RF_ST").InnerText;
                        xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ShopLocationID").Value = mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/DefaultDRPShopID");
                    }
                    else
                    {
                        //Set the Shop Location ID
                        xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ShopLocationID").Value = objShopXml.SelectSingleNode("//Shop/@ShopLocationID").InnerText;
                        strLossState = objShopXml.SelectSingleNode("//Shop/@AddressState").InnerText;
                    }
                    objShopXml = null;
                }

                if (strLossState == string.Empty)
                {
                    //Either this is not a program shop assignment or shop information did not have a state?
                    if (mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/LOC_ST").Trim() != string.Empty)
                    {
                        //Use vehicle's current location state as loss state
                        strLossState = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD2/LOC_ST").Trim();
                    }
                    else if (mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD1/INSD_ST").Trim() != string.Empty)
                    {
                        //Use insured's state as loss state
                        strLossState = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//AD1/INSD_ST").Trim();
                    }
                    else
                    {
                        //throw nack
                        string strTemplateFile = "AutoverseReceipt.XML";
                        SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/UndeterminableLossState"));
                        errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eAssignmentInvalid);
                        mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, "Received assignment where Loss State could not be determined.  NACK sent.");
                        return;
                    }
                }

                xmlDocumentAssignment.SelectSingleNode("//Claim/@LossAddressState").Value = strLossState;

                // Apparently one possible impact code EMS has is a total loss code.
                // if after translations we have this then we need to NAK

                if (xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ImpactLocations").InnerText.IndexOf("TL", 1) > 0)
                {
                    //TL was indicated -- we will still accept the assignment but we need to indicate that they indicate this could be a total loss
                    blnPossibleTotalLoss = true;
                    //Need to strip out the 'TL'
                    xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ImpactLocations").Value = StripTotalLossImpactPoint(xmlDocumentAssignment.SelectSingleNode("//Vehicle/ImpactLocations").InnerText);
                }

                if (xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepLogin").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepNameFirst").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepNameLast").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepEmailAddress").InnerText.ToUpper() == string.Empty)
                {
                    //no claim rep information sent. Build a string so we can add a note/notify
                    strCarrierRepInfo = BuildCarrierRepInfo();
                    //now lets use the default user as the source of this claim.
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepLogin").InnerText = cstrLynxAPDInsCoDefaultAdjLogon;
                }

                xmlDocumentAssignment.SelectSingleNode("//Claim/@FNOLUserID").InnerText = Convert.ToString(clngAutoverseUserID);
                strDocumentAssignment = xmlDocumentAssignment.OuterXml;
                // Error            
                try
                {
                    strPartner = objShared.g_strPartner;
                    objShared.g_strLynxID = Convert.ToString((mobjECAD.ApdLoadAssignmentExternal(ref strDocumentAssignment, ref strPartner, 0)));
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("New claim assignment created with the LynxID ", objShared.g_strLynxID), "");
                }
                catch (Exception ex)
                {
                    strAutoverseReceipt = "AutoverseReceipt.XML";
                    SendAReceipt(ref strAutoverseReceipt, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/ClaimInsertionError"));
                    //Need to implement the SendReceipt function here. If error throws we need to Send receipt to Glaxis
                }
                // line no 820 to 940               

                xmlDocumentClaimAspect = new XmlDocument();
                hashTable = new Hashtable();
                hashTable.Add("@ClientClaimNumber", cstrClaimNumber);
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@SourceApplicationID", 6);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp")), "");
                strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), hashTable, "APD");
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimAspect, ref strXmlPath, string.Empty, string.Empty);
                xmlNodeVeh = xmlDocumentClaimAspect.SelectSingleNode("//Vehicle");
                strXmlPath = string.Empty;

                if (strCarrierRepInfo.Length > 0)
                {
                    //Error
                    if (xmlNodeVeh != null)
                    {
                        //Insert Note into claim indicating that claim may be possible total
                        hashTable = new Hashtable();
                        hashTable.Add("@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);
                        hashTable.Add("@NoteTypeID", 1);
                        hashTable.Add("@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);
                        hashTable.Add("@Note", strCarrierRepInfo);
                        hashTable.Add("@UserID", clngAutoverseUserID);
                        if (isDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail")), "");
                        mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), hashTable, "APD");
                        // Error line 1020 to1070
                    }
                }

                if (blnPossibleTotalLoss)
                {
                    //Insert Note into claim indicating that claim may be possible total
                    hashTable = new Hashtable();
                    hashTable.Add("@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);
                    hashTable.Add("@NoteTypeID", 1);
                    hashTable.Add("@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);
                    hashTable.Add("@Note", strCarrierRepInfo);
                    hashTable.Add("@UserID", clngAutoverseUserID);
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail")), "");
                    mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), hashTable, "APD");
                }

                if (strRFWarningMessage.Trim().Length > 0)
                {
                    hashTable = new Hashtable();
                    hashTable.Add("@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);
                    hashTable.Add("@NoteTypeID", 1);
                    hashTable.Add("@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);
                    hashTable.Add("@Note", strCarrierRepInfo);
                    hashTable.Add("@UserID", clngAutoverseUserID);
                    if (isDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Executing SP ", mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail")), "");
                    mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), hashTable, "APD");
                }

                //Send back and AdjAssigned business event
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("Sending Business Event to ", mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL")), "");
                SendBusinessEvent("Claim Assigned", "", "Y", "");

                //Repair Facility specified could not be mapped.  Sent out Notification
                if (strRFWarningMessage != string.Empty)
                {
                    errorCode = Convert.ToUInt64(Autoverse_Error_Codes.eInternalError);
                    mobjCEvents.HandleEvent((int)errorCode, PROC_NAME, strRFWarningMessage.Replace("%lynxid%", objShared.g_strLynxID), string.Empty, string.Empty, false);
                }
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - ProcessNewAssignmentTransaction", string.Concat("ProcessNewAssignmentTransaction Complete"), "");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
        }

        private void ProcessCancelAssignmentRequest()
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessCancelAssignmentRequest:" });
            XmlDocument xmlDocumentClaimVeh;
            XmlNode xmlNodeVeh;
            string strError = string.Empty,
                strSource = string.Empty,
                strFrom = string.Empty,
                strSubject = string.Empty,
                strBody = string.Empty,
                strXmlPath = string.Empty;
            long lngClaimAspectID, lngStatusID;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjDataAccessor = new DataAccess.CDataAccessor();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - ProcessCancelAssignmentRequest", string.Concat("ProcessCancelAssignmentRequest Started"), "");

                //mobjCEvents.Trace(string.Concat(new string[] { "Processing Cancellation request for LynxID: ", objShared.g_strLynxID }));
                strSubject = "Received Cancellation Request through Autoverse for Claim";
                strBody = string.Concat(new string[] { "LYNX has received the cancellation request for Claim #:", cstrClaimNumber });

                //For cancellation we'll firs
                //Retrieve Claim Aspect
                hashTable = new Hashtable();
                hashTable.Add("@ClientClaimNumber", cstrClaimNumber);
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@SourceApplicationID", 6);

                strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), hashTable, "APD");
                xmlDocumentClaimVeh = new XmlDocument();
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");
                lngClaimAspectID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                if (lngClaimAspectID == 0)
                {
                    string strTemplateFile = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/CancelClaimNoClaimFound"));                    
                    return;
                }
                lngStatusID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);

                hashTable = new Hashtable();
                hashTable.Add("@ClaimAspectID", lngClaimAspectID);
                hashTable.Add("@NoteTypeID", 1);
                hashTable.Add("@StatusID", lngStatusID);
                hashTable.Add("@Note", strSubject);
                hashTable.Add("@UserID", clngAutoverseUserID);

                mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), hashTable, "APD");

                mobjCpartner.SendWorkflowNotification("ExternalClaimMessageReceived", strSubject, "", "VEH", "");

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - ProcessCancelAssignmentRequest", string.Concat("ProcessCancelAssignmentRequest Completed"), "");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        private void ProcessClaimMessage()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessClaimMessage:" });
            XmlDocument xmlDocumentClaimVeh = null, xmlTransDocument = null;
            XmlNode xmlNodeVeh;
            string strError = string.Empty,
                strSource = string.Empty,
                strFrom = string.Empty,
                strSubject = string.Empty,
                strBody = string.Empty,
                strXmlPath = string.Empty, strSenderName = string.Empty, strSubjectLine = string.Empty, strMessageText = string.Empty;
            long lngClaimAspectID, lngStatusID;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjDataAccessor = new DataAccess.CDataAccessor();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();              
                xmlTransDocument = new XmlDocument();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - ProcessClaimMessage", string.Concat("ProcessCancelAssignmentRequest Started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.ProcessClaimMessage: Issue while loading the transaction xml.");

                //Retrieve Claim Aspect
                hashTable = new Hashtable();
                hashTable.Add("@ClientClaimNumber", cstrClaimNumber);
                hashTable.Add("@InsuranceCompanyID", cstrLynxAPDInsCoID);
                hashTable.Add("@SourceApplicationID", 6);
                strXmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), hashTable, "APD");
                xmlDocumentClaimVeh = new XmlDocument();
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                strXmlPath = string.Empty;

                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");

                lngClaimAspectID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                if (lngClaimAspectID == 0)
                {
                    string strTemplateFile = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/CancelClaimNoClaimFound"));                   
                    return;
                }
                lngStatusID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);

                if (xmlTransDocument.GetElementsByTagName("SenderName") != null)
                    strSenderName = xmlTransDocument.GetElementsByTagName("SenderName").Item(0).InnerText;
                if (xmlTransDocument.GetElementsByTagName("SubjectLine") != null)
                    strSubjectLine = xmlTransDocument.GetElementsByTagName("SubjectLine").Item(0).InnerText;
                if (xmlTransDocument.GetElementsByTagName("MessageText") != null)
                    strMessageText = xmlTransDocument.GetElementsByTagName("MessageText").Item(0).InnerText;

                //Build Note text
                strBody = string.Concat(
                    "Sender: ", strSenderName,
                    Environment.NewLine,
                    "Subject: ", strSubjectLine,
                    Environment.NewLine, Environment.NewLine,
                    strMessageText);

                hashTable = new Hashtable();
                hashTable.Add("@ClaimAspectID", lngClaimAspectID);
                hashTable.Add("@NoteTypeID", 1);
                hashTable.Add("@StatusID", lngStatusID);
                hashTable.Add("@Note", strBody);
                hashTable.Add("@UserID", clngAutoverseUserID);

                mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), hashTable, "APD");

                mobjCpartner.SendWorkflowNotification("ExternalClaimMessageReceived", string.Concat("External Message with subject '",
                       strSubjectLine, "' received"), "", "VEH", "");

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - ProcessClaimMessage", string.Concat("ProcessCancelAssignmentRequest Completed"), "");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string StripTotalLossImpactPoint(string strImpactPoints)
        {
            string[] strImpactPointsList;
            string strNewImpactPoints = string.Empty;
            Int32 intPtr;
            try
            {
                strImpactPointsList = strImpactPoints.Split(',');

                for (intPtr = strImpactPointsList.GetLowerBound(0); intPtr < strImpactPointsList.GetUpperBound(0); intPtr++)
                {
                    if (strImpactPointsList[intPtr] != "TL")
                    {
                        strNewImpactPoints = string.Concat(new string[] { strNewImpactPoints, strImpactPointsList[intPtr] });

                        if (intPtr < strImpactPointsList.GetUpperBound(0))
                            strNewImpactPoints = string.Concat(new string[] { strNewImpactPoints, "," });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strNewImpactPoints;
        }

        private string BuildCarrierRepInfo()
        {
            string strCarrierRepInfo = string.Empty;
            XmlDocument xmlTransDocument = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.BuildCarrierRepInfo: Issue while loading the transaction xml.");

                strCarrierRepInfo = string.Concat("Adjuster Information: ", Environment.NewLine, Environment.NewLine,
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CT_FN"), Environment.NewLine,
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CT_ADDR1"), Environment.NewLine,
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "/CLM_CT_ADDR2"), Environment.NewLine,
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CITY"), ", ",
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_ST"), "  ",
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_ZIP"), Environment.NewLine,
                  mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CT_PH"));

                if (mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CT_PHX").Length > 0)
                    strCarrierRepInfo = string.Concat(new string[] { strCarrierRepInfo, " x", mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//CLM_CT_PHX") });

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCarrierRepInfo;
        }

        private bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2:" });

            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CPartner_ApdDatabaseTransfer()
        {
            string strAssignmentType = string.Empty, strError = string.Empty, strSource = string.Empty, strClaimOwnerEmail = string.Empty, strFrom = string.Empty, strSubject = string.Empty,
                strBody = string.Empty, strReturn = string.Empty, strReceiptForTransactionType = string.Empty, strAutoverseReceipt = string.Empty;
            long lngClaimAspectID, lngNumber;
            XmlNode objElem1 = null;
            XmlCDataSection objCdata = null;
            long errorCode;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjCEvents = new CEvents();
                xmlTransDocument = new XmlDocument();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CAutoverse.cs - CPartner_ApdDatabaseTransfer", string.Concat("CPartner_ApdDatabaseTransfer Started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CAutoverse.CPartner_ApdDatabaseTransfer: Issue while loading the transaction xml.");

                strAutoverseReceipt = "AutoverseReceipt.XML";
                clngAutoverseUserID = Convert.ToString(mobjMLAPDParterDataMgr.GetDefaultPartnerUserID(cstrLynxAPDInsCoDefaultAdjLogon, "CCAV"));

                switch (mobjCpartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        strReceiptForTransactionType = string.Empty;
                        strReceiptForTransactionType = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//head:TransactionType");
                        switch (strReceiptForTransactionType)
                        {
                            case "estimate":
                            case "digital image":
                            case "document":
                            case "message":
                            case "claim message":
                                HandleDocumentXferReceipt();
                                break;
                            default:
                                errorCode = Convert.ToInt64(Autoverse_Error_Codes.eUnexpectedReceipt);
                                mobjCEvents.HandleEvent((int)errorCode, "CAutoverse.CPartner_ApdDatabaseTransfer", string.Concat("Received an unexpected receipt from Partner: Receipt for Transaction Type=", strReceiptForTransactionType), "", "", false);
                                break;
                        }
                        break;
                    case CPartner.EDocumentType.eAssignment:
                        strAssignmentType = mobjMLAPDParterDataMgr.PrefixGetChildNodeText(ref xmlTransDocument, "//com:AssignmentType");
                        switch (strAssignmentType)
                        {
                            case "A":
                            case "S":
                            case "C":
                                if (objShared.g_strLynxID != string.Empty)
                                {
                                    if (strAssignmentType == "A")
                                    {
                                        SendAReceipt(ref strAutoverseReceipt, "N", Convert.ToString(Autoverse_Error_Codes.eClaimNumberInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/DuplicateClaimNewAssignment"));
                                        //Need to log here
                                    }
                                    else
                                    {
                                        SendAReceipt(ref strAutoverseReceipt, "N", Convert.ToString(Autoverse_Error_Codes.eClaimNumberInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/DuplicateClaimResendOrChangeAssignment"));
                                        //Need to log here  
                                    }
                                }
                                else
                                    ProcessNewAssignmentTransaction();
                                break;
                            case "X":  //Cancel Assignment
                                if (objShared.g_strLynxID.Length == 0)
                                    SendAReceipt(ref strAutoverseReceipt, "N", Convert.ToString(Autoverse_Error_Codes.eClaimNumberInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/CancelClaimNoClaimFound"));
                                else
                                    ProcessCancelAssignmentRequest();
                                break;
                            default:
                                throw new Exception(string.Concat(Convert.ToString(Autoverse_Error_Codes.eBadAssignmentCode), "CAutoverse.CPartner_ApdDatabaseTransfer", "Bad Assignment Type = ", strAssignmentType));
                        }
                        break;
                    case CPartner.EDocumentType.eClaimMessage:
                        ProcessClaimMessage();
                        break;
                }

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CAutoverse.cs - CPartner_ApdDatabaseTransfer", string.Concat("CPartner_ApdDatabaseTransfer Completed"), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
            return strReturn;
        }

        private bool CPartner_PartnerDatabaseTransferV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long CPartner_PartnerDatabaseTransferV2(string strXML, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2:" });

            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the transactionID for identification
        /// </summary>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public string GetTransactionID(string strXML)
        {
            string strTransactionID = string.Empty;
            XmlDocument xmlDocument = null;
            try
            {
                xmlDocument = new XmlDocument();
                if (strXML.Trim() != string.Empty && strXML.Trim() != null)
                    xmlDocument.LoadXml(strXML);
                strTransactionID = xmlDocument.SelectSingleNode("//SenderTransactionId").InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTransactionID;
        }

        #endregion

        #region Common Methods

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion

    }
}
