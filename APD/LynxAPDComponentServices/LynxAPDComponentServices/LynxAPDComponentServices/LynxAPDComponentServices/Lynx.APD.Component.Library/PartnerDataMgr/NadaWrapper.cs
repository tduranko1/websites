﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGW.Business;
using Lynx.APD.Component.Library.Common;
using System.Xml;
using System.Collections;
using System.Net;
using System.IO;
using System.Configuration;
using System.Xml.Linq;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class NadaWrapper
    {
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;
        Common.WriteLog objWritelog = null;
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);

        /// <summary>
        /// Get VIN details from NADA service
        /// </summary>
        /// <param name="strVIN"></param>
        /// <returns></returns>
        public Hashtable GetVIN(string strVIN)
        {
            string strData = string.Empty, strVINDetails = string.Empty;
            XmlDocument xmlDocument = null;
            Hashtable htVIN = null;
            try
            {
                xmlDocument = new XmlDocument();
                htVIN = new Hashtable();
                objWritelog = new WriteLog();

                xmlDocument = NadaVINProcess(strVIN);

                if (xmlDocument != null)
                {
                    if (xmlDocument.SelectSingleNode("NewDataSet/VehicleStruc") != null)
                    {
                        htVIN.Add("Year", xmlDocument.SelectSingleNode("//Year").InnerText);
                        htVIN.Add("Make", xmlDocument.SelectSingleNode("//MakeDescr").InnerText);
                        htVIN.Add("Model", xmlDocument.SelectSingleNode("//SeriesDescr").InnerText);
                    }
                }
                else
                    htVIN = null;
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.NadaWraper.cs - GetVIN", ex.Message, string.Concat("Details of the VIN number - ", strVIN));
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return htVIN;
        }
        
        /// <summary>
        /// Get the NADA service URL
        /// </summary>
        /// <returns></returns>
        public string GetNADAURL()
        {
            string strReturnURL = string.Empty;
            try
            {

                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        strReturnURL = "http://hyapdschdst1.pgw.local/nadaservice/Service.asmx";
                        break;
                    case "STG":
                        strReturnURL = "http://hyapdschdst1.pgw.local/nadaservice/Service.asmx";
                        break;
                    case "PRD":
                        strReturnURL = "http://HYAPDSCHDPRD1.pgw.local/nadaservice/Service.asmx";
                        break;
                    default:
                        strReturnURL = "http://hyapdschdst1.pgw.local/nadaservice/Service.asmx";
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnURL;
        }

        private string GetNadaUser()
        {
            string strUserName = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        strUserName = Convert.ToString(ConfigurationManager.AppSettings["NadaUserID"]);
                        break;
                    case "STG":
                        strUserName = Convert.ToString(ConfigurationManager.AppSettings["NadaUserID"]);
                        break;
                    case "PRD":
                        strUserName = Convert.ToString(ConfigurationManager.AppSettings["NadaPRDID"]);
                        break;
                    default:
                        strUserName = Convert.ToString(ConfigurationManager.AppSettings["NadaUserID"]);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strUserName;
        }

        private string GetNadaPS()
        {
            string strPassword = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        strPassword = Convert.ToString(ConfigurationManager.AppSettings["NadaPassword"]);
                        break;
                    case "STG":
                        strPassword = Convert.ToString(ConfigurationManager.AppSettings["NadaPassword"]);
                        break;
                    case "PRD":
                        strPassword = Convert.ToString(ConfigurationManager.AppSettings["NadaPRDPS"]);
                        break;
                    default:
                        strPassword = Convert.ToString(ConfigurationManager.AppSettings["NadaPassword"]);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strPassword;
        }

        private XmlDocument RemoveXmlns(String xml)
        {
            var xmlDocument = new XmlDocument();
            try
            {
                XDocument d = XDocument.Parse(xml);
                d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

                foreach (var elem in d.Descendants())
                    elem.Name = elem.Name.LocalName;

                xmlDocument.Load(d.CreateReader());
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.NadaWraper.cs - RemoveXmlns", ex.Message, string.Concat("Error on removing the namespace."));
            }
            return xmlDocument;
        }

        private XmlDocument NadaVINProcess(string strNadaVin)
        {
            string strReturnVIN = string.Empty;
            NADAWS.Service objNADA = null;
            XmlDocument xmlDocument = null;

            try
            {
                xmlDocument = new XmlDocument();
                objNADA = new NADAWS.Service();
                objNADA.Url = GetNADAURL();

                strReturnVIN = objNADA.VinLookup(strNadaVin);
                if (strReturnVIN != null)
                {
                    if (!string.IsNullOrEmpty(strReturnVIN.Trim()))
                        xmlDocument.LoadXml(strReturnVIN);
                    else
                        xmlDocument = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNADA = null;
            }
            return xmlDocument;
        }

        private XmlDocument GetAllAsXMLDoc(string strLossDate, string strRegion, string strRegionDesc, string strYear, string strMake, string strModel,
                                             string strBody, string strMileage, string strVIN, string strNADAVehicleId,
                                           string strAction, string strZip, string strReadOnly, bool blnXformOPtoHTML)
        {
            string strReturnVIN = string.Empty, strResponseData = string.Empty, strDomain = string.Empty;
            HttpWebRequest httpReq = null;
            HttpWebResponse httpResp = null;
            ASCIIEncoding encoding = null;
            byte[] postData = null;
            Stream ReqStrm = null;
            StreamReader respStrm = null;
            XmlDocument xmlDocument = null;
            try
            {
                xmlDocument = new XmlDocument();
                httpReq = (HttpWebRequest)WebRequest.Create(GetNADAURL() + "/VinLookup");
                encoding = new ASCIIEncoding();
                postData = encoding.GetBytes(String.Concat("strTokenId=" + GetToken(), "&strRegion=" + strRegion, "&strRegionDesc=" + strRegionDesc, "&strYear=" + strYear, "&strModel=" + strModel, "&strBody=" + strBody, "&strMileage=" + strMileage,
                    "&strVIN=" + strVIN, "&strVehId=" + strNADAVehicleId, "&strAction=" + strAction, "&strZip=" + strZip, "&strReadOnly=" + strReadOnly, "&strRegionList=" + ""));
                httpReq.ContentType = "application/x-www-form-urlencoded";
                httpReq.Method = "POST";
                httpReq.ContentLength = postData.Length;
                httpReq.UseDefaultCredentials = true;
                //httpReq.Credentials = new NetworkCredential(GetNadaUser(), GetNadaPS());
                ReqStrm = httpReq.GetRequestStream();
                ReqStrm.Write(postData, 0, postData.Length);
                ReqStrm.Close();

                httpResp = (HttpWebResponse)httpReq.GetResponse();
                respStrm = new StreamReader(
                httpResp.GetResponseStream(), Encoding.ASCII);
                strResponseData = respStrm.ReadToEnd();

                strResponseData = strResponseData.Replace("&lt;", "<");
                strResponseData = strResponseData.Replace("&gt;", ">");
                strResponseData = strResponseData.Replace("&quot;", "''");

                xmlDocument = RemoveXmlns(strResponseData);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                httpResp.Close();
                respStrm.Close();
            }
            return xmlDocument;
        }


        //Need to implement the GetAllAsXML2 functions.
        public string GetAllAsXML2(string strLossDate, string strRegion, string strRegionDesc, string strYear, string strMake, string strModel,
                                             string strBody, string strMileage, string strVIN, string strNADAVehicleId,
                                           string strAction, string strZip, string strReadOnly, bool blnXformOPtoHTML)
        {
            string strReturn = string.Empty, strToken = string.Empty;
            XmlDocument xmlDocument = null;
            NADAWS.Service objNADA = null;
            try
            {
                xmlDocument = new XmlDocument();
                objNADA = new NADAWS.Service();
                objNADA.Url = GetNADAURL();

                strReturn = objNADA.GetAllAsXML(GetToken(), strRegion, strRegionDesc, strYear, strMake, strModel, strBody, strMileage, strVIN, strNADAVehicleId, strAction, strZip, strReadOnly, "");
                // xmlDocument = GetAllAsXMLDoc(strLossDate, strRegion, strRegionDesc, strYear, strMake, strModel, strBody, strMileage, strVIN, strNADAVehicleId, strAction, strZip, strReadOnly, blnXformOPtoHTML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNADA = null;
            }
            return strReturn;
        }


        private string GetToken()
        {
            string strToken = string.Empty;
            NADAWS.Service objNADA = null;
            try
            {
                objNADA = new NADAWS.Service();
                strToken = objNADA.GetToken();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNADA = null;
            }
            return strToken;
        }

        public static string getLynxDataServiceUrl()
        {
            Common.WriteLog objWriteLog = new Common.WriteLog();
            string strServiceUrl = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().Trim().ToUpper())
                {
                    case "DEV":
                        strServiceUrl = ConfigurationManager.AppSettings["DevEMSService"];
                        break;
                    case "STAGE":
                    case "STG":
                        strServiceUrl = ConfigurationManager.AppSettings["STGEMSService"];
                        break;

                    case "PRD":
                    case "PROD":
                    case "DR":
                        strServiceUrl = ConfigurationManager.AppSettings["PRDEMSService"];
                        break;
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("LynxIncomingPartnerService ", "ERROR", "TransactionProcess.SubmitAPD.cs - getLynxDataServiceUrl()", ex.ToString(), "");
                throw ex;
            }
            return strServiceUrl;
        }
    }
}
