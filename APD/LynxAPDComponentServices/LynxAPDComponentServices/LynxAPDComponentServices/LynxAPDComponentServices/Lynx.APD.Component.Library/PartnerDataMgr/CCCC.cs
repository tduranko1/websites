﻿using System;
using System.Xml;
using Lynx.APD.Component.Library.Common;
using System.Collections;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using System.Configuration;
namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    /// CCC transactions
    /// </summary>
    public class CCCC
    {
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CCCC." });
        private CPartner mobjPartner = null;
        private MLAPDPartnerDataMgr mLAPDPartnerDataMgr = null;
        private MPPGTransactions mppgTransactions = null;
        private MBase64Utils mBase64utils = null;
        private MFlatfile mFlatfile = null;
        private MDomUtils mDomDomUtils = null;
        DataAccess.CDataAccessor mobjDataAccessor = null;
        SharedData objShared = null;
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        //bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        private enum CCC_Error_Codes : ulong
        {
            eFailedAddElement = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CCCC_CLS_ErrorCodes
        }

        /// <summary>
        /// These are error codes that will come
        ///  back in negative acknowledgements from CCC.
        /// </summary>
        private enum CCC_NACK_ErrorCodes : ulong
        {
            Invalid_Data_Assignment = 00102,
            Invalid_External_Alias_1 = 00103,
            Invalid_External_Alias_2 = 00213,
            Processing_Exception_Occurred = 00200,
            Invalid_Data_Exception = 00202
        }

        /// <summary>
        /// Constructor to assgin the shared object
        /// </summary>
        /// <param name="objCCCShared"></param>
        public CCCC(SharedData objCCCShared)
        {
            try
            {
                objShared = objCCCShared;
                mobjPartner = new CPartner(objShared);
                mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), "TransferToPartner()", "Method not implemented" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_InitProcessDocument()
        {
            bool isInitProcessDocument;
            string strTransactionId = string.Empty;
            XmlDocument xmlTransDocument = new XmlDocument();
            Common.WriteLog objWritelog = null;
            try
            {
                //'Extract the doc type from the passed root element.
                objWritelog = new WriteLog();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - CPartner_InitProcessDocument", string.Concat("Process started"), "");
                xmlTransDocument = new XmlDocument();
                mLAPDPartnerDataMgr.InitializeGlobals();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.CPartner_InitProcessDocument: Issue while loading the transaction xml.");

                mobjPartner.RawDocTypeString = Convert.ToString(xmlTransDocument.DocumentElement.Name);

                //'Extract the LYNXID and Vehicle number for reference by any procedure
                ExtractLynxIDAndVehNum(mobjPartner.DocTypeEnum);
                isInitProcessDocument = mobjPartner.CanProcessDocument();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - CPartner_InitProcessDocument", string.Concat("Process completed"), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - CPartner_InitProcessDocument", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
                xmlTransDocument = null;
            }
            return isInitProcessDocument;
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransfer = true;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - CPartner_PartnerDatabaseTransfer", string.Concat("CPartner_PartnerDatabaseTransfer Started"), "");

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        ExtractSequenceNumber();
                        mobjPartner.StorePrintImageInPartnerDB();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        ExtractSequenceNumber();
                        mobjPartner.StoreEstimateDataInPartnerDB();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        mobjPartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - CPartner_PartnerDatabaseTransfer", string.Concat("CPartner_PartnerDatabaseTransfer Completed"), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - CPartner_PartnerDatabaseTransfer", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
            }
            return isPartnerDatabaseTransfer;
        }


        /// <summary>
        /// Used for backwards compatibility problems with partner_db
        /// </summary>
        private void AddEchoField(string strElement, string strParent)
        {
            mDomDomUtils = new MDomUtils();
            XmlElement objElement = null;
            XmlElement objParent = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.AddEchoField: Issue while loading the transaction xml.");

                objElement = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref xmlTransDocument, string.Concat(new string[] { "//", strElement }), false);

                if (objElement == null)
                {
                    objParent = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref xmlTransDocument, string.Concat(new string[] { "//", strParent }), true);
                    objElement = mDomDomUtils.AddElement(ref objParent, strElement);
                    if (objElement == null)
                    {
                        throw new Exception(string.Concat(CCC_Error_Codes.eFailedAddElement.ToString(), "AddEchoField", "Failed to add element ", "Method not implemented", strElement, " to ", strParent));
                    }
                    objElement.InnerText = string.Concat(objShared.g_strLynxID, "-", objShared.g_strVehicleNumber, "-", objShared.g_strAssignmentID);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                objElement = null;
                objParent = null;
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransfer()
        {
            string strApdDatabaseTransfer = string.Empty;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                xmlTransDocument = new XmlDocument();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - CPartner_ApdDatabaseTransfer", string.Concat("Process started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.CPartner_ApdDatabaseTransfer: Issue while loading the transaction xml.");

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt("//ErrorCode", "//ErrorDescription");
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        //There are no specific handlers for business events required
                        //because we only receive one business event from CCC.
                        mobjPartner.SendWorkflowNotification("AssignmentDownloadedByShop", string.Concat("SHOP (CCC): Assignment for Vehicle ", (objShared.g_strVehicleNumber), " ", (mLAPDPartnerDataMgr.GetChildNode(ref xmlTransDocument, "//EventNotes", true).InnerText)), "", "VEH", "");
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.ProcessBinaryFiles(ref xmlTransDocument, "//DigitalImageFile", "//CreateDate", "//CreateTime", "JPG", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification("ElectronicDocumentReceived", string.Concat("SHOP (", objShared.g_strPartner, "): Digital images received for Vehicle ", objShared.g_strVehicleNumber), "", "VEH", "");
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate(this);
                        break;
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - CPartner_ApdDatabaseTransfer", string.Concat("Process completed"), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - CPartner_ApdDatabaseTransfer", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
            }
            return strApdDatabaseTransfer;
        }


        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        ///  Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <returns></returns>
        public long CPartner_InsertPrintImageIntoAPD(string strXml, long lngPartnerTransID)
        {
            mBase64utils = new MBase64Utils();
            mFlatfile = new MFlatfile();
            mDomDomUtils = new MDomUtils();
            XmlDocument xmlDocumentPrintImage = null;
            XmlElement objImageFile = null;
            DateTime currentDate;
            string strImageType = string.Empty;
            string strPrintImage = string.Empty;
            string strFileName = string.Empty;
            string strPath = string.Empty, strTempByteArrData;
            long lngInsertPrintImageIntoAPD;
            long lngStart = 0;
            long lngEnd = 0;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - CPartner_InsertPrintImageIntoAPD", string.Concat("Proccess started"), string.Concat("Vars: XML: ", strXml, " PartnerTransID: ", lngPartnerTransID));

                currentDate = DateTime.Now;
                lngStart = 0;
                lngEnd = 0;
                // objPrintImage = new MSXML2.IXMLDOMDocument2();
                //objImageFile = new XmlElement();
                xmlDocumentPrintImage = new XmlDocument();

                //  xmlDocumentTemp = (XmlDocument)objPrintImage;
                mDomDomUtils.LoadXml(ref xmlDocumentPrintImage, ref strXml, string.Empty, string.Empty);

                objImageFile = (XmlElement)(xmlDocumentPrintImage.SelectSingleNode("//ImageFile"));

                if ((objImageFile != null))
                {
                    strPrintImage = objImageFile.InnerText;
                }
                else
                {
                    //lngStart = string.Intern(1, strXml, "CDATA[", Constants.vbBinaryCompare) + 6;
                    //lngEnd =  string.Intern(1, strXml, "]]", Constants.vbBinaryCompare);
                    lngStart = strXml.IndexOf("CDATA[", 1) + 6;
                    lngEnd = strXml.IndexOf("]]", 1);

                    if (lngStart <= 0 || lngEnd <= 0 || lngEnd <= lngStart)
                    {
                        throw new Exception(string.Concat(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eCDataTagNotFound.ToString(), "PrintImage()", "CDATA tag not found in print image."));
                    }
                    strPrintImage = Mid(strXml, Convert.ToInt32(lngStart), Convert.ToInt32(lngEnd - lngStart));
                }
                // objPrintImage = null;

                //'Get the document file name.
                strFileName = mobjPartner.BuildFinalFileName(1, DateTime.Now.ToString("yyyyMMddhhmmss"), "PrintImage");

                // mFlatfile.ConvertFlatfileToXML(CPartner.EDocumentType.ePrintImage.ToString(), strXml.Substring(Convert.ToInt32(lngStart), Convert.ToInt32((lngEnd - lngStart))), "PrintImage", "66", "Estimate", strFileName);
                strImageType = "XML";
                if (strPrintImage.IndexOf("Job Number", 1, StringComparison.CurrentCultureIgnoreCase) > 0)
                {
                    //content is in the old ez-net format with plain text.
                    mFlatfile.ConvertFlatfileToXML(CPartner.EDocumentType.ePrintImage, CPartner.EDocumentType.ePrintImage.ToString(), strPrintImage, "PrintImage", "Estimate", strFileName);

                }
                else
                {
                    //this could be from CCC One
                    byte[] obj = null;
                    strPath = string.Empty; strTempByteArrData = string.Empty;
                    strTempByteArrData = Convert.ToString(mBase64utils.ReadBinaryDataFromDomElement(objImageFile));
                    obj = Convert.FromBase64String(strTempByteArrData);

                    strFileName = strFileName.Replace(".xml", ".pdf");
                    //strPath = Strings.Replace((GetConfig("Document/RootDirectory") + strFileName), "\\\\", "\\");
                    strPath = (string.Concat(mLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFileName).Replace("\\\\", "\\"));

                    mBase64utils.WriteBinaryDataToFile(strPath, obj);

                    if (IsDebug)
                        objWritelog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CCCC.cs - CPartner_InsertPrintImageIntoAPD", string.Concat("File saved in following path ", strPath), "");
                    strImageType = "PDF";
                }
                lngInsertPrintImageIntoAPD = mobjPartner.InsertLAPDDocument(CPartner.EDocumentType.ePrintImage, Convert.ToInt64(objShared.g_strAssignmentID), Convert.ToInt64(objShared.g_strLynxID), Convert.ToInt32(objShared.g_strVehicleNumber), Convert.ToInt32(mobjPartner.mintEstimateSequenceNumber), mobjPartner.BuildPartnerTransID(), strFileName, strImageType, "", false);
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - CPartner_InsertPrintImageIntoAPD", string.Concat("CPartner_InsertPrintImageIntoAPD Completed."), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - CPartner_InsertPrintImageIntoAPD", exception.Message, string.Concat("Vars: XML: ", strXml, " PartnerTransID: ", lngPartnerTransID));
                throw exception;
            }
            finally
            {
                objWritelog = null;
            }
            //'Insert new document record into APD database
            return lngInsertPrintImageIntoAPD;
        }


        /// <summary>
        /// private void ExtractLynxIDAndVehNum()
        ///Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        ///This routine is now modified to handle two versions of CCC transactions:
        ///those that include the echo field and those that don't.  This makes the logic
        ///rather messy, but it will save resubmission of docs by hand in PRD.
        /// </summary>
        /// <param name="enmDocType"></param>
        private void ExtractLynxIDAndVehNum(CPartner.EDocumentType enmDocType)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractLynxIDAndVehNum:" });
            bool blnNeedAssignmentID;
            string strResults = string.Empty;
            string[] strValues = null;
            string strDate = string.Empty;
            string strTime = string.Empty;
            int intLynxIDStart;
            Hashtable hashTable = null;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            try
            {
                mobjDataAccessor = new DataAccess.CDataAccessor();
                xmlTransDocument = new XmlDocument();
                objWritelog = new WriteLog();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - ExtractLynxIDAndVehNum", string.Concat("Process started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.ExtractLynxIDAndVehNum: Issue while loading the transaction xml.");

                intLynxIDStart = 0;
                blnNeedAssignmentID = false;//' This is where the LynxID begins                
                //New "Secret Echo Field" extraction.
                if (enmDocType == CPartner.EDocumentType.eReceipt)
                {
                    objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//PartnerUniqueId");
                }
                else if (enmDocType == CPartner.EDocumentType.eBusinessEvent)
                {
                    blnNeedAssignmentID = true;
                    //Flag that we dont have the assignment ID.
                    objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//CustomerRefId", true);
                    //Old version still had the colon.  New version does not.  See Test Track 2037.
                    if (objShared.g_strActualLynxID.IndexOf(":", 1) > 0)
                    {
                        objShared.g_strActualLynxID = Mid(objShared.g_strActualLynxID, objShared.g_strActualLynxID.IndexOf(":", 1) + 2);
                    }
                    else if (objShared.g_strActualLynxID.IndexOf("ID", 1) > 0)
                    {
                        objShared.g_strActualLynxID = Mid(objShared.g_strActualLynxID, objShared.g_strActualLynxID.IndexOf("ID", 1) + 3);
                    }
                }
                else if (enmDocType == CPartner.EDocumentType.eDigitalImage || enmDocType == CPartner.EDocumentType.ePrintImage)
                {
                    objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//InsuranceUniqueID");
                }
                else if (enmDocType == CPartner.EDocumentType.eEstimateData)
                {
                    objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//SecretField");
                }
                else
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), " ", "Unknown document type: '", enmDocType.ToString(), "'." }));
                }
                //'Backwards compatibility to old legacy documents
                if (objShared.g_strActualLynxID.Length == 0)
                {
                    blnNeedAssignmentID = true;
                    //Flag that we dont have the assignment ID.
                    if (enmDocType == CPartner.EDocumentType.eReceipt)
                    {
                        objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//CustomerRefId", true);
                    }
                    else if (enmDocType == CPartner.EDocumentType.eDigitalImage || enmDocType == CPartner.EDocumentType.ePrintImage)
                    {
                        objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//ClaimNumber", true);
                    }
                    else if (enmDocType == CPartner.EDocumentType.eEstimateData)
                    {
                        objShared.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//ReceiverTransactionId", true);
                        intLynxIDStart = objShared.g_strActualLynxID.IndexOf("#");
                    }
                }

                //'Split up the values by the dashes
                strValues = objShared.g_strActualLynxID.Split('-');

                if (strValues.Length - 1 < 0)
                    objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - ExtractLynxIDAndVehNum", string.Concat("Invalid Document Ref ID (", objShared.g_strActualLynxID, ")"), "");
                //MLAPDPartnerDataMgr.g_objEvents.Assert((strValues.Length - 1) > 0, string.Concat("Invalid Document Ref ID (", objShared.g_strActualLynxID, ")"));

                //Extract the LynxID
                objShared.g_strLynxID = Convert.ToString(mLAPDPartnerDataMgr.AsNumber(strValues[0]));

                //Extracts and zero fill the vehicle number
                objShared.g_strVehicleNumber = mLAPDPartnerDataMgr.ZeroPad(mLAPDPartnerDataMgr.AsNumber(strValues[1]), 3);

                //'Extract the assignment id.
                //'Do we need to get it from the database using LynxID, VehNum and Suffix?
                // Need to get the suffix of the vehicle number.
                if (strValues.Length - 1 < 2)
                {
                    string strEnd = string.Empty;
                    int intSuffixLen = 0;
                    strEnd = strValues[1];
                    intSuffixLen = strEnd.Length - Convert.ToString(Convert.ToInt32(objShared.g_strVehicleNumber)).Length; //

                    if (intSuffixLen >= 0)
                    {
                        strEnd = Right(strEnd, intSuffixLen);

                        //Take only the first character, and cover for estimates with their '#' character.
                        strEnd = ((Left(strEnd, 1) == "#") ? "" : Left(strEnd, 1));
                    }

                    hashTable = new Hashtable();
                    hashTable.Add("@LynxID", Convert.ToInt64(objShared.g_strLynxID));
                    hashTable.Add("@VehicleNumber", Convert.ToInt32(objShared.g_strVehicleNumber));
                    if (!string.IsNullOrEmpty(strEnd.Trim()))
                        hashTable.Add("@AssignmentSuffix", strEnd);
                    else
                        hashTable.Add("@AssignmentSuffix", DBNull.Value);

                    objShared.g_strAssignmentID = Convert.ToString(mobjDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/AssignmentDecodeSP"), hashTable, "APD"));
                }
                else
                    objShared.g_strAssignmentID = Convert.ToString(mLAPDPartnerDataMgr.AsNumber(strValues[2]));

                //'Extract the assignment / cancel character.
                //'Do we need to get it from the database using LynxID, VehNum and Suffix?
                if ((strValues.Length - 1) > 2)
                    objShared.g_strActionCode = Left(strValues[3], 1);

                if (objShared.g_strActionCode != "C")
                    objShared.g_strActionCode = "N";

                //Extract the transaction sent date/time.
                //Need to change the Mid index as 2 instead of 3
                strDate = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionDate", true);
                strTime = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionTime", true);
                objShared.g_varTransactionDate = string.Concat(Left(strDate, 2), "/", Mid(strDate, 2, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Mid(strTime, 2, 2));
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - ExtractLynxIDAndVehNum", string.Concat("Process completed."), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - ExtractLynxIDAndVehNum", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
            }
        }


        /// <summary>
        /// Extracts the Estimate Sequence Number from the Print Image or Estimate Data and stores it in intEstimateSequenceNumber.
        /// The function returns true if a value is found, false if not
        /// </summary>
        private void ExtractSequenceNumber()
        {
            string strNodeText = string.Empty;// Text value of node we're looking at
            bool blnExtractSequenceNumber;
            double dNum;
            bool isNum;
            XmlNodeList nodCurrent = null;// The nodes we're looking at
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                xmlTransDocument = new XmlDocument();
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - ExtractSequenceNumber", string.Concat("Process started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.ExtractSequenceNumber: Issue while loading the transaction xml.");

                //' Set default return value
                blnExtractSequenceNumber = false;
                //'CCC Print Image
                if (mobjPartner.DocTypeEnum == CPartner.EDocumentType.ePrintImage)
                {
                    mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                    blnExtractSequenceNumber = true;
                }
                //Estimate Data
                else if (mobjPartner.DocTypeEnum == CPartner.EDocumentType.eEstimateData)
                {
                    nodCurrent = xmlTransDocument.GetElementsByTagName("TransactionType");
                    strNodeText = nodCurrent.Item(0).InnerText;

                    // This is the value as it comes from the XML document
                    // In a ED, the estimate Supplement Identifier comes from a combination of two elements, one which designates
                    // E or S (for estimate and supplement), and a second which designates the number.  If the value of the first
                    // element is "E", just set the sequence number to 0.  Other, we'll need to get the sequence number from
                    // the second element.
                    if (strNodeText == "E")
                    {
                        mobjPartner.mintEstimateSequenceNumber = 0;
                        blnExtractSequenceNumber = true;
                    }
                    if (strNodeText == "S")
                    {
                        // This is a supplement, get the Supplement Sequence Nubmber
                        nodCurrent = xmlTransDocument.GetElementsByTagName("EstimateSupplementNmbr");
                        strNodeText = nodCurrent.Item(0).InnerText;
                        // This is the value as it comes from the XML document
                        isNum = double.TryParse(strNodeText, out dNum);
                        if (isNum)
                        {
                            mobjPartner.mintEstimateSequenceNumber = Convert.ToInt32(strNodeText);
                            blnExtractSequenceNumber = true;
                        }
                    }
                }
                else
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), " ", " Unknown document type : '", mobjPartner.DocTypeString, "'." }));
                }

                if (!blnExtractSequenceNumber)
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber.ToString(), "ExtractSequenceNumber() ", "Unable to extract Estimate Sequence Number" }));
                }
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - ExtractSequenceNumber", string.Concat("Process completed."), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - ExtractSequenceNumber", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
            }
        }

        /// <summary>
        /// Send a rececipt to the Glaxis URL
        /// </summary>
        private void SendAReceipt()
        {
            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransType = string.Empty;
            XmlDocument objLYNXReceipt = null,
                xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            try
            {
                mppgTransactions = new MPPGTransactions();
                mDomDomUtils = new MDomUtils();
                objLYNXReceipt = new XmlDocument();
                xmlTransDocument = new XmlDocument();
                objWritelog = new WriteLog();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - SendAReceipt", string.Concat("Process started"), "");

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CCCC.SendAReceipt: Issue while loading the transaction xml.");

                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();
                string strPath = string.Concat(objShared.g_strSupportDocPath, "\\CCCReceipt.XML");
                mDomDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //Fill out the template with values from the submitted document.
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//SenderTransactionId", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//SenderTransactionId", true);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ReceiverTransactionId", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//ReceiverTransactionId", true);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionDate", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString());
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionTime", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString());
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//CustomerRefId", true).InnerText = string.Concat(objShared.g_strLynxID, "-", objShared.g_strVehicleNumber);

                //The CCC transaction type is the root node name minus the 4 digit version number.
                strTransType = xmlTransDocument.DocumentElement.Name;
                strTransType = Left(strTransType, (strTransType.Length) - 4);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionType", true).InnerText = strTransType;
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ErrorCode", true).InnerText = "00000";
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ErrorDescription", true).InnerText = string.Concat(new string[] { mobjPartner.DocTypeString, " processed" });

                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//EventCreatedDateTime/DlDate/Date", true).InnerText = string.Concat(new string[] { mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString()), " ", mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString()) });
                //DevwsAPDFoundation.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - SendAReceipt", string.Concat("Creating the Acknowledgement Receipt XML for CCC"), "");
                //Build the SOAP Envelope and put the receipt into the envelope
                strWrappedReceipt = mppgTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");
                //DevwsAPDFoundation.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - SendAReceipt", string.Concat("Posting XML to PPG URL- ", strPost2URL, "Acknowledgement XML - ", strWrappedReceipt), "");

                //Do the post to PPG
                mppgTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);
                //DevwsAPDFoundation.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - SendAReceipt", string.Concat("Receipt sent via HTTP"), "");
                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - SendAReceipt", string.Concat("Procecss completed."), "");
            }
            catch (Exception exception)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CCCC.cs - SendAReceipt", exception.Message, "");
                throw exception;
            }
            finally
            {
                objWritelog = null;
                objLYNXReceipt = null;
            }
        }


        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), MODULE_NAME, "SendHeartBeatRequest()", "Method not implemented" }));

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// CPartner_PartnerDatabaseTransferV2
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_PartnerDatabaseTransferV2:" });
            try
            {
                return true;
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));

            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// CPartner_ApdDatabaseTransferV2
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_ApdDatabaseTransferV2:" });
            try
            {
                return string.Empty;
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// CPartner_InitProcessDocumentV2
        /// </summary>
        /// <param name="strTradingPartner"></param>
        /// <param name="strPassedDocument"></param>
        /// <returns></returns>
        public bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_InitProcessDocumentV2:" });
            try
            {
                return true;
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// CPartner_InsertPrintImageIntoAPDV2
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        private long CPartner_InsertPrintImageIntoAPDV2(string strXml, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_InsertPrintImageIntoAPDV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// Get the left side of the value using index
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Left(string param, int length)
        {
            string strLeft = string.Empty;
            try
            {
                if (param != string.Empty)
                    strLeft = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strLeft;
        }

        /// <summary>
        /// Get the right side of the value using index
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Right(string param, int length)
        {
            string strRight = string.Empty;
            try
            {
                if (param != string.Empty)
                    strRight = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strRight;
        }

        /// <summary>
        /// Get the mid value with length
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex, int length)
        {
            string strMid = string.Empty;
            try
            {
                if (param != string.Empty)
                    strMid = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        /// <summary>
        /// Get the mid value
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex)
        {
            string strMid = string.Empty;
            try
            {
                if (param != string.Empty)
                    strMid = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        /// <summary>
        /// Get the transactionID for identification
        /// </summary>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public string GetTransactionID(string strXML)
        {
            string strTransactionID = string.Empty;
            XmlDocument xmlDocument = null;
            try
            {
                xmlDocument = new XmlDocument();
                if (strXML.Trim() != string.Empty && strXML.Trim() != null)
                    xmlDocument.LoadXml(strXML);
                strTransactionID = xmlDocument.SelectSingleNode("//SenderTransactionId").InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTransactionID;
        }

    }
}
