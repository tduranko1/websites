﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using SiteUtilities;
using Lynx.APD.Component.Library.Common;
using apdDataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using System.Threading;
using System.Configuration;
using System.Collections;


namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class MLAPDPartnerDataMgr
    {
        string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = "LAPDPartnerDataMgr.MLAPDPartnerDataMgr.cs";
        public long LAPDPartnerDataMgr_FirstError = 0x80067000;

        public enum MLAPDPartnerDataMgr_ErrorCodes : ulong
        {
            eInvalidTradingPartner = 0x80067000 + 0x80,
            eCDataTagNotFound,
            eLynxIdInvalid,
            eBadTradingPartner,
            eBadDocumentType,
            eDuplicateDocument,
            eNackReceived,
            eNotImplemented,
            eBadSequenceNumber,
            eUnableToInitializeTrans,
            eNoRecordsReturned,
            eLynxIdUnknown,
            eShopSearchMissingParams,
            //Module specific error ranges.
            MPartnerDataMgr_BAS_ErrorCodes = 0x80067000 + 0x100,
            CPartnerDataXfer_CLS_ErrorCodes = 0x80067000 + 0x180,
            CAPD_CLS_ErrorCodes = 0x80067200 + 0x200,
            CCCC_CLS_ErrorCodes = 0x80067280 + 0x280,
            CExecute_CLS_ErrorCodes = 0x80067300 + 0x300,
            CSceneGenesis_CLS_ErrorCodes = 0x80067300 + 0x380,
            CPartner_CLS_ErrorCodes = 0x80067000 + 0x400,
            CAutoVerse_CLS_ErrorCodes = 0x80067000 + 0x480,
            CGrange_CLS_ErrorCodes = 0x80067000 + 0x560
        }


        //public static SiteUtilities.CEvents g_objEvents = null;
        //  public static DataAccessor.CDataAccessor g_objDataAccessor = null;
        apdDataAccessor.CDataAccessor objDataAccessor = null;
        public static bool g_blnDebugMode;
       // public static bool g_blnUsePartnerTypesForDocTypes;
       // public static string g_strSupportDocPath = string.Empty;
        //public static string
        //    //g_strPartnerConnStringXml = string.Empty,     //Current strConn to DataAccesor for PARTNER XML data
        //    //g_strPartnerConnStringStd = string.Empty,    //Current strConn to DataAccesor for PARTNER recordset data
        //    //g_strLYNXConnStringXML = string.Empty,       //Current strConn to DataAccessor for LYNX XML data
        //    //g_strLYNXConnStringStd = string.Empty,        //Current strConn to DataAccessor for LYNX recordset data
        // g_strPartner = string.Empty,        //Trading partner name
        // g_strPartnerSmallID = string.Empty,  //Small version of partner name (i.e. "SG" instead of "SceneGenesis")
        // g_strPassedDocument = string.Empty,
        // g_strLynxID = string.Empty,       //'LynxID associated with the inbound document as in 6008 only
        // g_strActualLynxID = string.Empty,   //'Concatenated version from the Xml as in : 6008-1
        // g_strVehicleNumber = string.Empty,  //'Vehicle Number
        // g_strAssignmentID = string.Empty,   //'Assignment ID
        // g_strTaskID = string.Empty,
        // g_strActionCode = string.Empty,     //'Action Code: "N" for new assignment, "C" for cancellation.
        // g_strStatus = string.Empty;

        //public static XmlDocument g_objPassedDocument;
        //public static object g_varDocumentReceivedDate;  //'The date/time that the document was received.
        //public static object g_varTransactionDate;       //'The date/time that the document was sent.

        //'This counter used to ensure the globals get intialized
        //'and desposed of only once.  This is required because various
        //'parts of the ECAD stuff have different entry points.
        private int mintInitCount;

        //'API declares
        // Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

        //[DllImport("kernel32")]
        //public void sleep(long dwMilliseconds);


        #region Struct
        struct typError
        {
            public long Number;
            public string Description;
            public string Source;
        }

        private typError objError;

        #endregion


        #region Methods

        //public MLAPDPartnerDataMgr(SharedData objPartnerMgrShared)
        //{
        //    try
        //    {
        //        objShared = objPartnerMgrShared;                
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        // PushErr and PopErr function not been used

        /// <summary>
        /// Intialize the count value
        /// </summary>
        public bool IsInitialized
        {
            get { return Convert.ToBoolean(mintInitCount != 0); }
        }

        /// <summary>
        /// Initialize Globals
        /// </summary>
        public void InitializeGlobals()
        {
            string configPath = string.Empty;
            try
            {
                if (!IsInitialized)
                {
                    //Create private member DataAccessor.:                        
                    //g_objDataAccessor = new DataAccessor.CDataAccessor();

                    //Share DataAccessor's events object.
                    //g_objEvents = new CEvents();

                    //Get path to support document directory             
                    //objShared.g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDPartnerDataMgr"]);                   
                    //Initialize our member components first, so we have logging set up.
                    //configPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);                 
                                          
                }
                mintInitCount = mintInitCount + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Terminate Globals
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                if (mintInitCount <= 1)
                {
                    //g_objEvents = null;
                    mintInitCount = 0;
                }
                else
                {
                    mintInitCount = mintInitCount - 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        /// <summary>
        /// Wraps CreateObject() with better debug information.
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns>The Object created or Nothing.</returns>
        public object CreateObjectEx(string strObjectName)
        {
            object createdObject = null;
            Type CreateObj;
            try
            {
                //object obj = null;
                //obj = 
                CreateObj = Type.GetTypeFromProgID(strObjectName);
                createdObject = Activator.CreateInstance(CreateObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createdObject;
        }

        public long GetDefaultPartnerUserID(string strLogonID, string strApplicationCD = "APD")
        {
            //TODO Confirm the long Intialization
            long DefaultPartnerUserID = 0;
            string returnExecuteSpNamedParamsXML = string.Empty,
                tempDefaultPartnerUserID = string.Empty;
            XmlDocument objResults = null;
            Hashtable htParams = null;
            try
            {
                objResults = new XmlDocument();
                objDataAccessor = new apdDataAccessor.CDataAccessor();

                htParams = new Hashtable();
                htParams.Add("@Login", strLogonID);
                htParams.Add("@ApplicationCD", strApplicationCD);
                returnExecuteSpNamedParamsXML = objDataAccessor.ExecuteSpNamedParamsXML(GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), htParams, "APD");

                if (!string.IsNullOrEmpty(returnExecuteSpNamedParamsXML))
                    objResults.LoadXml(returnExecuteSpNamedParamsXML);
                else
                    LoadXml(ref objResults, ref returnExecuteSpNamedParamsXML, "GetDefaultPartnerUserID", "Partner User Detail");

                tempDefaultPartnerUserID = GetChildNodeText(ref objResults, "/Root/User/@UserID");
                DefaultPartnerUserID = Convert.ToInt64(tempDefaultPartnerUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DefaultPartnerUserID;
        }

        /// <summary>
        /// Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public string GetConfig(string strSetting)
        {
            string strReturn = string.Empty, strMachineName = string.Empty, strAppPath = string.Empty, strEnvPath = string.Empty,
strMachOnly = string.Empty, strEnvInsPath = string.Empty, strMachPath = string.Empty, strMachInstPath = string.Empty, strInstPath = string.Empty, strConfigPath = string.Empty;
            XmlDocument xmlDocConfig = null;
            bool blnFoundInst = false;
            string mstrInstance = "test";
            XmlNode xmlNode;
            try
            {
                xmlDocConfig = new XmlDocument();
                strAppPath = "Root/Application/";
                strMachineName = Environment.MachineName;
                strEnvPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']]/";
                strEnvInsPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']"; //Need to Add //[Instance[@name='" & mstrInstance & "']]]/"
                strMachOnly = "//Machine[@name='" + strMachineName + "']"; //[Instance[@name='" & mstrInstance & "']]"
                strMachPath = strAppPath + "Environment/Machine[@name='" + strMachineName + "']/";
                strMachInstPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "'][Instance[@name='" + mstrInstance + "']]]/";
                strInstPath = strMachPath; //"Instance[@name='" & mstrInstance & "']/"

                strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                xmlDocConfig.Load(strConfigPath);

                xmlNode = xmlDocConfig.SelectSingleNode(strMachPath + strSetting);
                if (xmlNode == null)
                    blnFoundInst = false;

                if (!blnFoundInst && xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strEnvPath + strSetting);

                if (xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strAppPath + strSetting);

                strReturn = xmlNode.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Builds up a date string to be used in document file names.
        /// </summary>
        /// <returns></returns>
        public string BuildFileNameDate()
        {
            string strBuildFileNameDate = string.Empty;
            try
            {
                strBuildFileNameDate = string.Concat(ConstructXmlDate(Convert.ToString(DateTime.Now)), Convert.ToString(DateTime.Now.ToString("HHmmssff")));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strBuildFileNameDate;
        }

        /// <summary>
        /// Builds a specifically formatted time string, according to our XML docs, as "mmddyyyy".
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string ConstructXmlDate(string date)
        {
            string
                //strMonth = string.Empty,
                //strDay = string.Empty,
                //strYear = string.Empty,
                strCurrDate = string.Empty;
            DateTime strBaseDate;
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    strBaseDate = Convert.ToDateTime(date);
                    strCurrDate = strBaseDate.ToString("MMddyyyy");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCurrDate;
        }

        /// <summary>
        /// Builds a specifically formatted time string, according to our XML docs, as "hhmmss"
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public string ConstructXmlTime(string time)
        {
            string strCurrTime = string.Empty;
            DateTime strBaseTime;
            try
            {
                if (!string.IsNullOrEmpty(time))
                {
                    strBaseTime = Convert.ToDateTime(time);
                    strCurrTime = strBaseTime.ToString("HHmmss");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCurrTime;
        }

        public string ConstructSqlDateTime(string strXmlDate, string strXmlTime, bool blnNowAsDefault = true)
        {
            string strDateTime = string.Empty,
                strTemp = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strXmlDate))
                {
                    if (strXmlDate.Length == 8)
                        strDateTime = string.Concat(Right(strXmlDate, 4), "-", Left(strXmlDate, 2), "-", Mid(strXmlDate, 3, 2));
                    else if (blnNowAsDefault)
                        strDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                }

                if (!string.IsNullOrEmpty(strXmlTime))
                {
                    if (strDateTime.Length > 0)
                    {
                        if (strDateTime.Length > 6)
                            strDateTime = string.Concat(strDateTime, "T", Left(strXmlTime, 2), ":", Mid(strXmlTime, 3, 2), ":", Right(strXmlTime, 2));
                        else if (strXmlDate.Length > 0)
                            strDateTime = string.Concat(strDateTime, "T00:00:00");
                        else if (blnNowAsDefault)
                            strDateTime = string.Concat(strDateTime, DateTime.Now.ToString("THH:mm:ss"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDateTime;
        }


        /// <summary>
        /// Spits recordset contents out to the debug log.
        /// </summary>
        /// <param name="RS"></param>

        public void DebugRS(ref ADODB.Recordset RS)
        {
            string strRecord = string.Empty,
                strvalue = string.Empty;
            //ADODB.Field strField;
            try
            {
                RS.MoveFirst();

                strRecord = string.Concat(new string[] { "Record Count = ", Convert.ToString(RS.RecordCount), System.Environment.NewLine });
                strRecord = string.Concat(new string[] { strRecord, " Fields ", "," });

                foreach (ADODB.Field strField in RS.Fields)
                    strRecord = string.Concat(strRecord, strField.Name, ",");


                strRecord = string.Concat(new string[] { strRecord, System.Environment.NewLine });
                if (RS.RecordCount > 0)
                {
                    while (!RS.EOF)
                    {
                        strRecord = string.Concat(new string[] { strRecord, "  Data:" }); ;
                        foreach (ADODB.Field strField in RS.Fields)
                        {
                            if (strField.Value != null)
                            {
                                strvalue = Convert.ToString(strField.Value);
                                strvalue = strvalue.Replace(System.Environment.NewLine, "");
                                strvalue = strvalue.TrimEnd();
                                strvalue = strvalue.TrimStart();
                                if (!string.IsNullOrEmpty(strvalue) && strvalue.Length > 0)
                                {
                                    if ((Left(strvalue, 1) == "<") && (Right(strvalue, 1) == ">"))
                                        strRecord = string.Concat(new string[] { strRecord, "(XML NOT LOGGED)" });
                                    else
                                        strRecord = string.Concat(new string[] { strRecord, "'", Convert.ToString(strField.Value) });
                                }
                                else
                                    strRecord = string.Concat(new string[] { strRecord, "'' ," });
                            }
                            else
                                strRecord = string.Concat(new string[] { strRecord, "Null, " });
                        }
                        strRecord = string.Concat(new string[] { strRecord, System.Environment.NewLine });
                        RS.MoveNext();
                        strvalue = string.Empty;
                    }                  
                    RS.MoveFirst();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Pads a number out with zeros to make the requested number of digits.
        /// </summary>
        /// <param name="lngValue"></param>
        /// <param name="intDigits"></param>
        /// <returns></returns>
        public string ZeroPad(long lngValue, int intDigits)
        {
            string strValue = string.Empty,
                strZeroPad = string.Empty;
            try
            {
                strValue = Convert.ToString(lngValue);
                while (strValue.Length < intDigits)
                {
                    strValue = string.Concat(new string[] { "0", strValue });
                }
                strZeroPad = strValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strZeroPad;
        }

        /// <summary>
        /// AsNumber - returns a Long based upon the first numeric portion of str.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public long AsNumber(string str)
        {
            long lngAsNumebr = 0;
            int countNumber,
                tempint;
            try
            {
                for (countNumber = 1; countNumber <= str.Length; countNumber++)
                {
                    if (int.TryParse(Left(str, countNumber), out tempint))
                        lngAsNumebr = Convert.ToInt64(Left(str, countNumber));
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngAsNumebr;
        }

        public void DoSleep(long lngMS)
        {
            long lCount = 0;
            try
            {
                for (lCount = 1; lCount <= lngMS / 10; lCount++)
                {
                    Thread.Sleep(10);
                    //TODO
                    //DoEvents();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TODO Need to remove the becasue this functionalities from MDomUtils .bas file
        // Need to confirm
        public void LoadXml(ref XmlDocument objDom, ref string strXml, string strCallerName, string strXmlDescription)
        {
            try
            {
                //loadXml returns true if the load succeeded.
                objDom.LoadXml(strXml);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Returns the specified child node text of the passed node.
        /// </summary>
        /// <param name="objXml"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public string GetChildNodeText(ref XmlDocument objXml, string strPath, Boolean blnMustExist = false)
        {
            string childNode = null;
            XmlNode objChild = null;
            try
            {

                objChild = GetChildNode(ref objXml, strPath, blnMustExist);

                if (objChild == null)
                {
                    childNode = "";
                }
                else
                {
                    childNode = objChild.InnerText;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return childNode;

        }

        public string PrefixGetChildNodeText(ref XmlDocument xmlDocument, string strPath)
        {
            string strReturn = string.Empty, strNamespaces = string.Empty;
            XmlNamespaceManager xmlNameSpace = null;
            string[] prefix = null;
            try
            {
                xmlNameSpace = new XmlNamespaceManager(xmlDocument.NameTable);
                foreach (XmlAttribute xmlObjAttrNode in xmlDocument.DocumentElement.Attributes)
                {
                    if ((xmlObjAttrNode.Name.Length) >= 6)
                    {
                        if (Left(xmlObjAttrNode.Name, 6) == "xmlns:")
                        {
                            strNamespaces = string.Empty;
                            prefix = xmlObjAttrNode.Name.Split(':');
                            strNamespaces = xmlObjAttrNode.Value;
                            xmlNameSpace.AddNamespace(prefix[1], strNamespaces);
                            prefix = null;
                        }
                    }
                }
                if (xmlDocument.SelectSingleNode(strPath, xmlNameSpace) != null)
                    strReturn = xmlDocument.SelectSingleNode(strPath, xmlNameSpace).InnerText;
                else
                    strReturn = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Returns the specified child node of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNode GetChildNode(ref XmlDocument objXml, string strPath, bool blnMustExist = false)
        {
            XmlNode xmlNodeChild = null;
            try
            {
                XmlNode objChild;
                objChild = objXml.SelectSingleNode(strPath);

                if (objChild == null)
                {
                    if (blnMustExist == true)
                    {
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MDomUtils.EventCodes.eXmlMissingElement), "GetChildNode('", strPath, "')" }));
                        // Err().Raise(eXmlMissingElement, "GetChildNode('" + strPath + "')", objNode.nodeName + " is missing a '" + strPath + "' child node.");
                    }
                }
                else
                {
                    xmlNodeChild = objChild;
                }

                objChild = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlNodeChild;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion

    }

}
