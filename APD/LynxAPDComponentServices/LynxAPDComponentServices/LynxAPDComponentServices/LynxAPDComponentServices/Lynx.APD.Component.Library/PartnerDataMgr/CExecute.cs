﻿using System;
using System.Xml;
using System.Data;
using System.Reflection;
using Lynx.APD.Component.Library.Common;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using System.Collections;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    /// Call the main function to process the transactions.
    /// </summary>
    public class CExecute
    {

        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CExecute." });
        private bool mblnPartnerDbXfer;
        private bool mblnApdDbXfer;
        private bool mblnSendReceipt;
        private MLAPDPartnerDataMgr mLAPDPartnerDataMgr = null;
        private MDomUtils mDomUtils = null;
        DataAccess.CDataAccessor mobjDataAccessor = null;
        public SharedData objShared = new SharedData();
        WriteLog objWriteLog = null;
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);

        #endregion

        /// <summary>
        /// Constructor for Intialize the Gloabal variables
        /// </summary>
        public CExecute()
        {
            try
            {
                //Default is to submit to both databases.
                //They will only be overrid by backoffice document submission.
                mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                mblnPartnerDbXfer = true;
                mblnApdDbXfer = true;
                mblnSendReceipt = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Class_Terminate()
        {
            try
            {
                //Ensure that object clean-up happens in spite of errors.
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Obsoleted - call ConsumePartnerXmlTransaction below instead.
        /// Added a new method rather than break binary compatability.
        /// </summary>
        /// <returns></returns>
        public long SendPartnerDocAsXml(string strXMLDoc, string strTradingPartner)
        {
            try
            {
                throw new Exception(string.Concat("SendPartnerDocAsXml", "Method Obsolete - call ConsumePartnerXmlTransaction instead."));
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This function processes an Xml document sent from outside of PPG,
        /// Specifically from one of our partners (CCC, ADP, SG, etc.)
        /// </summary>
        /// <returns></returns>
        public string ConsumePartnerXmlTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)//Todo return value
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ConsumePartnerXmlTransaction: " });
            string strConsumePartnerXmlTransaction = string.Empty,
                strDynamicObj = string.Empty;
            bool blnRaise = false;
            bool blnContinue;

            string strTransactionId = string.Empty;
            try
            {
                //This bool is used to add additional control over when we raise to the caller.
                blnRaise = false;
                blnContinue = false;
                objWriteLog = new WriteLog();
                mDomUtils = new MDomUtils();

                //strTransactionId = mDomUtils.GetTransactionID(strXMLDoc, strTradingPartner);
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CExecute.cs - ConsumePartnerXmlTransaction", string.Concat("Started "), strXMLDoc);
                //Raise to the caller for any problems with the passed parameters.
                blnRaise = true;

                objShared.g_varDocumentReceivedDate = DateTime.Now;
                objShared.g_strPartner = strTradingPartner;
                objShared.g_strPassedDocument = strXMLDoc;

                mLAPDPartnerDataMgr.InitializeGlobals();
                switch (strTradingPartner)
                {
                    case "ADP":
                        ADPTransaction(strXMLDoc, strTradingPartner, blnReturnXml);
                        break;
                    case "CCC":
                        CCCTransaction(strXMLDoc, strTradingPartner, blnReturnXml);
                        break;
                    case "CCAV":
                        CCAVTransaction(strXMLDoc, strTradingPartner, blnReturnXml);
                        break;
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CExecute.cs - ConsumePartnerXmlTransaction", string.Concat("Excecption: ", ex.Message, "InnerException: ", ex.InnerException, "Stack Trace: ", ex.StackTrace, "TransactionType: ", strTradingPartner), strXMLDoc);
                throw ex;
            }
            finally
            {
                mLAPDPartnerDataMgr.TerminateGlobals();
                strTransactionId = string.Empty;
                objWriteLog = null;
            }
            return strConsumePartnerXmlTransaction;
        }

        /// <summary>
        /// Process the CCC transactions
        /// </summary>
        /// <returns></returns>
        private string CCCTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)
        {
            string strReturn = string.Empty;
            CCCC objCCCC = null;
            bool blnContinue = false, blnRaise = false;
            try
            {
                objCCCC = new CCCC(objShared);
                objShared.g_varDocumentReceivedDate = DateTime.Now;
                objShared.g_strPartner = strTradingPartner;
                objShared.g_strPassedDocument = strXMLDoc;
                objShared.g_strPartnerSmallID = mLAPDPartnerDataMgr.GetConfig(string.Concat("PartnerSettings/", strTradingPartner, "/@SmallID"));
                objShared.g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDPartnerDataMgr"]);

                if (objCCCC.CPartner_InitProcessDocument())
                {
                    if (mblnPartnerDbXfer)
                        blnContinue = objCCCC.CPartner_PartnerDatabaseTransfer(mblnSendReceipt);
                    else
                        blnContinue = true;

                    blnRaise = false;

                    if (mblnPartnerDbXfer && blnContinue)
                        strReturn = objCCCC.CPartner_ApdDatabaseTransfer();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Process the ADP transactions
        /// </summary>
        /// <param name="strXMLDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <param name="blnReturnXml"></param>
        /// <returns></returns>
        private string ADPTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)
        {
            string strReturn = string.Empty;
            CADP objADP = null;
            bool blnContinue = false, blnRaise = false;
            try
            {
                objADP = new CADP(objShared);
                objShared.g_varDocumentReceivedDate = DateTime.Now;
                objShared.g_strPartner = strTradingPartner;
                objShared.g_strPassedDocument = strXMLDoc;
                objShared.g_strPartnerSmallID = mLAPDPartnerDataMgr.GetConfig(string.Concat("PartnerSettings/", strTradingPartner, "/@SmallID"));
                objShared.g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDPartnerDataMgr"]);

                if (objADP.CPartner_InitProcessDocumentV2(strTradingPartner, strXMLDoc))
                {
                    if (mblnPartnerDbXfer)
                        blnContinue = objADP.CPartner_PartnerDatabaseTransferV2(mblnSendReceipt);
                    else
                        blnContinue = true;

                    blnRaise = false;

                    if (mblnPartnerDbXfer && blnContinue)
                        strReturn = objADP.CPartner_ApdDatabaseTransferV2();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        private string CCAVTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)
        {
            string strReturn = string.Empty;
            CAutoverse objAutoverse = null;
            bool blnContinue = false, blnRaise = false;
            try
            {
                objAutoverse = new CAutoverse(objShared);
                objShared.g_varDocumentReceivedDate = DateTime.Now;
                objShared.g_strPartner = strTradingPartner;
                objShared.g_strPassedDocument = strXMLDoc;
                objShared.g_strPartnerSmallID = mLAPDPartnerDataMgr.GetConfig(string.Concat("PartnerSettings/", strTradingPartner, "/@SmallID"));
                objShared.g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDPartnerDataMgr"]);

                if (objAutoverse.CPartner_InitProcessDocument())
                {
                    if (mblnPartnerDbXfer)
                        blnContinue = objAutoverse.CPartner_PartnerDatabaseTransfer(mblnSendReceipt);
                    else
                        blnContinue = true;

                    blnRaise = false;

                    if (mblnPartnerDbXfer && blnContinue)
                        strReturn = objAutoverse.CPartner_ApdDatabaseTransfer();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Syntax:      object.SendHeartBeatRequest()
        /// Purpose:     Network Status Test
        /// </summary>
        /// <returns></returns>
        public void SendHeartBeatRequest(string strTradingPartner)//Todo returnvalue
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest: " });
            CPartner objPartner = null;
            try
            {
                objPartner = new CPartner(objShared);
                objShared.g_strPartner = strTradingPartner;
                objShared.g_strPassedDocument = "<Root/>";

                mLAPDPartnerDataMgr.InitializeGlobals();

                //Get a partner object to work with.
                // objPartner = GetPartnerObject(); //TODO
                objPartner.SendHeartBeatRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
        }

        /// <summary>
        /// Backoffice utility for submitting to Partner database only.
        /// </summary>
        /// <returns></returns>
        public long SubmitDocumentToPartnerDbOnly(string strXMLDoc, string strTradingPartner, bool blnSendReceipt)
        {
            long lngSubmitDocumentToPartnerDbOnly;
            string strResult = string.Empty;

            try
            {
                lngSubmitDocumentToPartnerDbOnly = 0;
                strResult = null;
                mblnPartnerDbXfer = true;
                mblnApdDbXfer = false;
                mblnSendReceipt = blnSendReceipt;
                strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, false);

                if (Left(strResult, 6) == "<Error")
                    lngSubmitDocumentToPartnerDbOnly = 0;
                else
                    lngSubmitDocumentToPartnerDbOnly = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lngSubmitDocumentToPartnerDbOnly;
        }

        /// <summary>
        /// Backoffice utility for submitting to APD database only.
        /// </summary>
        /// <returns></returns>
        public long SubmitDocumentToApdDbOnly(string strXMLDoc, string strTradingPartner, bool blnSendReceipt)
        {
            long lngSubmitDocumentToApdDbOnly;
            string strResult = string.Empty;

            try
            {
                lngSubmitDocumentToApdDbOnly = 0;
                strResult = null;
                mblnPartnerDbXfer = false;
                mblnApdDbXfer = true;
                mblnSendReceipt = blnSendReceipt;
                strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, false);

                if (Left(strResult, 6) == "<Error")
                    lngSubmitDocumentToApdDbOnly = 0;
                else
                    lngSubmitDocumentToApdDbOnly = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngSubmitDocumentToApdDbOnly;
        }

        /// <summary>
        /// Returns a CPartner based upon g_strPartner
        /// </summary>
        /// <returns></returns>
        private string GetPartnerObject()//Todo
        {
            // mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string objGetPartnerObject = string.Empty;
            try
            {
                switch ((objShared.g_strPartner.ToUpper()))
                {
                    case "CCC":
                        objGetPartnerObject = "CCCC";
                        break;
                    case "ADP":
                        objGetPartnerObject = "CADP";
                        break;
                    case "CCAV":
                        objGetPartnerObject = "CAutoverse";
                        break;
                    case "GRANGEOHIO":
                        objGetPartnerObject = "CGrange";
                        break;
                    case "MITCHELL":
                        objGetPartnerObject = "CMitchel";
                        break;
                    case "SCENEGENESIS":
                        objGetPartnerObject = "CSceneGenesis";
                        break;
                    case "FCS":
                        objGetPartnerObject = "CFCS";
                        break;
                    default:
                        throw new Exception(string.Concat(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eInvalidTradingPartner.ToString(), "GetPartnerObject()", "Trading partner", objShared.g_strPartner, " passed was unknown."));

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objGetPartnerObject;

        }

        /// <summary>
        /// Syntax:      object.InitiatePartnerXmlTransaction strXmlDoc
        /// Parameters:  strXmlDoc = Xml document received from calling application
        /// Purpose:     Receive an Xml from an internal application and then kick off
        /// a transaction with an external (partner) application.
        /// Returns:     Blank or any response XML.
        /// </summary>
        /// <returns></returns>
        public string InitiatePartnerXmlTransaction(string strXMLDoc)//todo return value
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitiatePartnerXmlTransaction: " });
            string strInitiatePartnerXmlTransaction = string.Empty;
            XmlDocument xmlTransDocument = null;
            //Object vars that need clean up

            Type objTypeDynamic = null;
            object objDynamic = null;
            string strDynamicObj = string.Empty;
            CAutoverse objcAutoverse = null;
            CCCC objCCC = null;
            CADP objADP = null;

            try
            {
                xmlTransDocument = new XmlDocument();
                //Store the received date/time for later.
                objShared.g_varDocumentReceivedDate = DateTime.Now;
                objShared.g_strSupportDocPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDPartnerDataMgr"]);

                // Put these in global scope for access by all procedures
                // including "InitalizeGlobals"
                objShared.g_strPassedDocument = strXMLDoc;

                // Dump the passed in values into the bit bucket
                strXMLDoc = "";

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CExecute.InitiatePartnerXmlTransaction: Issue while loading the transaction xml.");
                mLAPDPartnerDataMgr.InitializeGlobals();

                //Extract the partner from the XML, i.e. "SceneGenesis".
                objShared.g_strPartner = mLAPDPartnerDataMgr.GetChildNodeText(ref xmlTransDocument, "//@Partner", true);

                //Get a little partner string from the config, i.e. "SG".
                objShared.g_strPartnerSmallID = Convert.ToString(mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/@SmallID" })));

                switch (objShared.g_strPartner.ToUpper())
                {
                    case "CCC":
                        objCCC = new CCCC(objShared);
                        strInitiatePartnerXmlTransaction = objCCC.CPartner_TransferToPartner();
                        break;
                    case "ADP":
                        objADP = new CADP(objShared);
                        strInitiatePartnerXmlTransaction = objADP.CPartner_TransferToPartner();
                        break;
                    case "CCAV":
                        objcAutoverse = new CAutoverse(objShared);
                        strInitiatePartnerXmlTransaction = objcAutoverse.CPartner_TransferToPartner();
                        break;
                }

                ////Get a partner object to work with.               
                //strDynamicObj = string.Concat("Lynx.APD.Component.Library.PartnerDataMgr.", GetPartnerObject());
                //objTypeDynamic = Type.GetTypeFromProgID(strDynamicObj);
                //objDynamic = Activator.CreateInstance(objTypeDynamic);

                ////Processing of document by partner specific class...
                //strInitiatePartnerXmlTransaction = Convert.ToString(objTypeDynamic.InvokeMember("TransferToPartner", BindingFlags.InvokeMethod, null, objDynamic, null));  //objPartner.TransferToPartner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }

            return strInitiatePartnerXmlTransaction;
        }

        /// <summary>
        /// Procedure : GetUnprocessedPartnerData
        /// DateTime  : 11/23/2004 14:33
        /// Author    : csr0901
        /// Purpose   : Gets a list of udb_partner.transaction_header records with 'unk' LynxIDs.These records represent data sent to Lynx by a shop before that shop
        /// received an electronic assignment from Lynx.  Due to that the incoming
        /// data contained no LynxID, VehicleNumber, nor AssignmentID.  That data is
        /// now stuck in the partner database until someone manually supplies the
        /// Missing required data listed above.
        /// </summary>
        /// <returns></returns>
        public string GetUnprocessedPartnerData(string strSessionKey, string strWindowID, string strUserID, string strErrorXML)
        {
            mDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetUnprocessedPartnerData: " });
            string strGetUnprocessedPartnerData = string.Empty,
            strConnect = string.Empty,
            strProcName = string.Empty,
            strDataSource = string.Empty,
            strDataSourceCode = string.Empty,
            strPartnerTransID = string.Empty,
            strTransactionDate = string.Empty,
            strTransactionSource = string.Empty,
            strTransactionType = string.Empty,
            strClaimNo = string.Empty,
            strInsdLN = string.Empty,
            strOwnerLN = string.Empty,
            strModelYear = string.Empty,
            strMake = string.Empty,
            strModel = string.Empty,
            strBodyStyle = string.Empty,
            strpath = string.Empty,
            strPartnerRecord = string.Empty, strWebRoot = string.Empty;
            XmlDocument objDataDOM = null;
            XmlDocument objReturnDOM = null;
            XmlDocument objErrorDOM = null,
                objResultDOM = null;
            XmlElement objRootElement = null;
            XmlAttribute objAttribute = null;
            XmlNodeList objNodeList = null;
            XmlNode objNode = null, objTempNode = null, objErrorNode;
            XmlElement objxmlElement;
            //object objExe = null;          
            long lngLynxID = 0;
            string strAppend = string.Empty;
            Hashtable hashTable = null;
            Common.MDomUtils objMDomUtils = null;
            DataPresenter.CExecute objExe = null;
            DataSet dsPartnerRecord = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                //Objects that need cleaning up
                strpath = "<Root></Root>";
                objDataDOM = new XmlDocument();
                objReturnDOM = new XmlDocument();
                objErrorDOM = new XmlDocument();
                objResultDOM = new XmlDocument();
                objMDomUtils = new MDomUtils();
                //objRootElement = new XmlElement();
                //objAttribute = new XmlAttribute();               
                mobjDataAccessor = new DataAccess.CDataAccessor();
                dsPartnerRecord = new DataSet();
                lngLynxID = 0;
                strWebRoot = ConfigurationManager.AppSettings["BaseIIS"];

                //Primitives that do not
                // ERROR: Not supported in C#: OnErrorStatement
                mLAPDPartnerDataMgr.InitializeGlobals();

                //Initialize DOM objects.
                objDataDOM = new XmlDocument();
                objReturnDOM = new XmlDocument();

                //Get the procedure name from the config XML.
                strProcName = "uspTransactionHeaderUnprocessedGetDetail";

                //Get the XML from the database.
                hashTable = new Hashtable();
                hashTable.Add("@PartnerTransID", DBNull.Value);
                dsPartnerRecord = mobjDataAccessor.OpenDataSetSP(strProcName, hashTable, "Partner");

                //Creates an emp.
                mDomUtils.LoadXml(ref objReturnDOM, ref strpath, PROC_NAME, "Parameter");

                if (dsPartnerRecord != null && dsPartnerRecord.Tables.Count > 0)
                {
                    strPartnerRecord = dsPartnerRecord.GetXml();
                    objResultDOM.LoadXml(strPartnerRecord);

                    //Get the root element of the return XML.
                    objRootElement = objReturnDOM.DocumentElement;
                    objNodeList = objResultDOM.SelectNodes("/NewDataSet/Table");
                }

                foreach (XmlNode objXmlNode in objNodeList)
                {
                    //Loads and validates the xml.
                    strAppend = objXmlNode.SelectSingleNode("XML").InnerText;
                    //strAppend = objRS.Fields["XML"].Value.ToString();
                    mDomUtils.LoadXml(ref objDataDOM, ref strAppend, PROC_NAME, "data");

                    // Get the values from the recordset and embedded XML that will be returned to the caller.
                    strPartnerTransID = objXmlNode.SelectSingleNode("PartnerTransID").InnerText; //Convert.ToString(objRS.Fields["PartnerTransID"].Value);
                    strTransactionDate = objXmlNode.SelectSingleNode("TransactionDate").InnerText; //Convert.ToString(objRS.Fields["TransactionDate"].Value);
                    strTransactionSource = objXmlNode.SelectSingleNode("TransactionSource").InnerText; //Convert.ToString(objRS.Fields["TransactionSource"].Value);
                    strTransactionType = objXmlNode.SelectSingleNode("TransactionType").InnerText; //Convert.ToString(objRS.Fields["TransactionType"].Value);
                    strClaimNo = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//CLM_NO");
                    strInsdLN = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//INSD_LN");
                    strOwnerLN = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//OWNER_LN");
                    strModelYear = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MODEL_YR");
                    strMake = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MAKEDESC");
                    strModel = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MODEL");
                    strBodyStyle = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_BSTYLE");

                    // Create a node to hold the retrieved values for this record and add it to the Root.
                    //ToDo: Verify
                    //objNode = mDomUtils.AddElement(ref objNode, "PartnerRecord");
                    objNode = objReturnDOM.CreateNode(XmlNodeType.Element, "PartnerRecord", "");
                    objxmlElement = objReturnDOM.CreateElement("PartnerRecord");
                    //objxmlElement = objRootElement.OwnerDocument.CreateElement("PartnerRecord");
                    // objNode.AppendChild(objxmlElement);
                    objxmlElement.SetAttribute("PartnerTransID", strPartnerTransID);
                    objxmlElement.SetAttribute("TransactionDate", strTransactionDate);
                    objxmlElement.SetAttribute("TransactionSource", strTransactionSource);
                    objxmlElement.SetAttribute("TransactionType", strTransactionType);
                    objxmlElement.SetAttribute("ClaimNo", strClaimNo);
                    objxmlElement.SetAttribute("InsdLN", strInsdLN);
                    objxmlElement.SetAttribute("Year", strModelYear);
                    objxmlElement.SetAttribute("Make", strMake);
                    objxmlElement.SetAttribute("Model", strModel);
                    objxmlElement.SetAttribute("BodyStyle", strBodyStyle);

                    objReturnDOM.DocumentElement.AppendChild(objxmlElement);
                }

                // If there are errors append them to the xml to be returned.
                if (!string.IsNullOrEmpty(strErrorXML.Trim()))
                {
                    objErrorDOM = new XmlDocument();

                    // Load and validate the passed xml error string.
                    mDomUtils.LoadXml(ref objErrorDOM, ref strErrorXML, PROC_NAME, "errors");

                    // Grab the Errors node which contains the individual Error nodes.
                    objTempNode = objErrorDOM.SelectSingleNode("/Root/Errors");

                    // Append the Errrors node to the Root node of the XML to be transformed
                    if (objTempNode != null)
                    {
                        objErrorNode = objReturnDOM.ImportNode(objTempNode, true);
                        objReturnDOM.DocumentElement.AppendChild(objErrorNode);
                    }
                    //objReturnDOM.DocumentElement.AppendChild((XmlElement)objNode);
                }

                // Create an instance of DataPresenter to perform the XML to HTML transformation.
                //  objExe = CreateObjectEx("DataPresenter.CExecute");

                objExe = new DataPresenter.CExecute();
                // Initialize DataPresenter.
                objExe.InitializeEx(strWebRoot, strSessionKey, strWindowID, strUserID);
                // objResultDOM = objMDomUtils.TransformDomAsDom(ref objReturnDOM, string.Concat("C:\\IntranetSites\\APD\\webroot\\xsl\\", "UnprocessedPartnerData.xsl"));
                //Perform the transformation and return the resultant HTML.
                strGetUnprocessedPartnerData = objExe.TransformXML(objReturnDOM.OuterXml, "UnprocessedPartnerData.xsl");

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.cExecute.cs - GetUnprocessedPartnerData", string.Concat("Exception: ", ex.Message, ex.InnerException), "");
                throw ex;
            }
            finally
            {
                if (objExe != null)
                {
                    Marshal.ReleaseComObject(objExe);
                }               
                objExe = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
            return strGetUnprocessedPartnerData;
        }

        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values.
        /// </summary>
        /// <returns></returns>
        public string FixUnprocessedPartnerData(string strData, string strSessionKey, string strWindowID, string strUserID)
        {
            mDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "FixUnprocessedPartnerData:" });
            string strRoot = string.Empty,
            strReturnData = string.Empty,
            strFixUnprocessedPartnerData = string.Empty,
            strAssignmentID = string.Empty,
            strErrorXML = string.Empty,
            strPartner = string.Empty,
            strPartnerTransID = string.Empty,
            strResult = string.Empty,
            strTransactionType = string.Empty,
            strXML = string.Empty,
            strPath = string.Empty;
            long lngIte,
            lngUBound,
            strLynxID;
            int intErrorCount,
            strVehicleNumber;
            XmlDocument xmlDocument, objReturnDOM, objDataDOM;
            XmlNode objNode, objErrorNode;
            XmlAttribute objAttribute;
            string[] varrData = null;
            string[] varrDataRow = null;
            ADODB.Recordset objRS = null;
            DataRow drDataRow = null;
            string[] strSplitarray = null;
            Hashtable hashTable = null;
            XmlDocument xmlTransDocment = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {

                // Objects that require cleaning up.
                strRoot = "<Root><Errors></Errors></Root>";
                strReturnData = "ReturnData";
                lngIte = 0;
                lngUBound = 0;
                strLynxID = 0;
                intErrorCount = 0;
                strVehicleNumber = 0;
                xmlDocument = new XmlDocument();
                objReturnDOM = new XmlDocument();
                objDataDOM = new XmlDocument();
                mobjDataAccessor = new DataAccess.CDataAccessor();
                objRS = new ADODB.Recordset();
                mLAPDPartnerDataMgr.InitializeGlobals();
                drDataRow = null;

                // Initialize local objects and variables.
                intErrorCount = 0;
                mDomUtils.LoadXml(ref objReturnDOM, ref strRoot, PROC_NAME, "ReturnData");
                objErrorNode = objReturnDOM.SelectSingleNode("/Root/Errors");

                if (!string.IsNullOrEmpty(strData.Trim()))
                {
                    // Remove trailing semicolon (;) if it exists.
                    if (Right(strData, 1) == ";")
                        strData = Left(strData, (strData.Length - 1));

                    // Break the data into individual PartnerTransID,LynxID,VehicleNumber strings.
                    varrDataRow = strData.Split(';');
                    strSplitarray = strData.Split(';');

                    // Get the upper bound of the newly created array.
                    //lngUBound = strData.varrDataRow;
                    lngUBound = varrDataRow.Length - 1;

                    // Loop through the array.
                    for (lngIte = 0; lngIte <= lngUBound; lngIte++)
                    {
                        // Break the data row into individual data items.                   
                        varrData = strSplitarray[lngIte].Split(',');

                        // Extract the individual data items.
                        strPartnerTransID = Convert.ToString(varrData[0]);
                        strLynxID = Convert.ToInt64(varrData[1]);
                        strVehicleNumber = Convert.ToInt32(varrData[2]);

                        // Point data accessor to the APD database.
                        // Temporarily turn off error handling
                        // ERROR: Not supported in C#: OnErrorStatement
                        //Get the assignment ID of the assignment for this vehicle.
                        hashTable = new Hashtable();
                        hashTable.Add("@LynxID", Convert.ToInt64(strLynxID));
                        hashTable.Add("@VehicleNumber", Convert.ToInt32(strVehicleNumber));
                        hashTable.Add("@AssignmentSuffix", DBNull.Value);

                        strAssignmentID = mobjDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig(("ClaimPoint/SPMap/AssignmentDecodeSP").ToString()), hashTable, "APD").ToString();

                        // If any error occured, assume an AssignmentID could not be found for the passed LynxID/Vehicle Number.
                        // This will be handled below due to the fact that strAssignmentID is still empty.
                        //Err.Clear();
                        // Resume normal error handling.
                        // ERROR: Not supported in C#: OnErrorStatement
                        // Continue processing this partner record only if an AssignmentID was found.
                        if (strAssignmentID != null)
                        {
                            // Point data accessor to the Partner database.
                            // Get the transaction header record.
                            hashTable = new Hashtable();
                            hashTable.Add("@PartnerTransID", Convert.ToInt64(strPartnerTransID));

                            objRS = mobjDataAccessor.OpenRecordsetNamedParams("uspTransactionHeaderUnprocessedGetDetail", hashTable, "Partner");

                            // Make sure a record still exists for this PartnerTransID.                        
                            if (!objRS.EOF)
                            {
                                // Load xml string into the DOM.
                                strPath = objRS.Fields["XML"].Value.ToString();
                                mDomUtils.LoadXml(ref objDataDOM, ref strPath, PROC_NAME, "utb_transaction_header.xml");
                                strPath = string.Empty;

                                // Get some values from the retrieved record.
                                strPartner = Convert.ToString(objRS.Fields["TransactionSource"].Value);
                                strTransactionType = Convert.ToString(objRS.Fields["TransactionType"].Value);

                                if (strPartner == "ADP")
                                {
                                    // Set the missing values in the XML.
                                    objDataDOM.DocumentElement.Attributes.GetNamedItem("LynxID").InnerText = strLynxID.ToString();
                                    objDataDOM.DocumentElement.Attributes.GetNamedItem("VehicleID").InnerText = strVehicleNumber.ToString();
                                    objDataDOM.DocumentElement.Attributes.GetNamedItem("AssignmentID").InnerText = strAssignmentID.ToString();

                                    // Hack neceesary because InitializeGlobals() assumes any XML was passed in original call,
                                    // however here we pull the XML from the database after InitializeGlobals has already been called.
                                    if (xmlTransDocment == null)
                                        xmlTransDocment = new XmlDocument();

                                    objShared.g_strPassedDocument = objDataDOM.OuterXml;

                                    if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                                        xmlTransDocment.LoadXml(objShared.g_strPassedDocument);

                                    // Submit the xml back thru the system
                                    strResult = ConsumePartnerXmlTransaction(objDataDOM.OuterXml, strPartner, false);

                                    // Handle any error return from the data submission process.
                                    if (!string.IsNullOrEmpty(strResult.Trim()))
                                    {
                                        if (Left(strResult, 6) == "<Error")
                                        {
                                            // PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
                                            //ToDo: Verify
                                            //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                                            objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                                            objErrorNode.AppendChild(objNode);
                                            objNode.InnerText = string.Concat("The specified ", strTransactionType, " (PartnerTransID=", strPartnerTransID, ") could ", "not be successfully loaded into the APD database.");
                                            intErrorCount = intErrorCount + 1;
                                            //g_objevents.HandleEvent
                                        }
                                    }
                                    else
                                    {
                                        // Point data accessor to the Partner database.
                                        // Delete the original Partner record.
                                        hashTable = new Hashtable();
                                        hashTable.Add("@PartnerTransID", Convert.ToInt64(strPartnerTransID));

                                        mobjDataAccessor.ExecuteSpNamedParams("uspTransactionHeaderDel", hashTable, "Partner");
                                    }
                                }
                                else
                                {
                                    // Not ADP data.
                                    //ToDo: Verify
                                    // objNode = mDomUtils.AddElement(ref(XmlElement)(objErrorNode), "Error");
                                    objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                                    objErrorNode.AppendChild(objNode);
                                    objNode.InnerText = string.Concat("This process is currently only supported for ADP estimates.  Could not attach this ", strTransactionType, " to LynxID: ", strLynxID.ToString(), "-", strVehicleNumber.ToString(), ".");
                                    intErrorCount = intErrorCount + 1;
                                    //g_objevents.HandleEvent
                                }
                            }
                            else
                            {
                                // PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
                                //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                                objNode = objErrorNode.OwnerDocument.CreateElement("Error").SelectSingleNode("Error");
                                objNode.InnerText = string.Concat("The specified ", strTransactionType, " (PartnerTransID=", strPartnerTransID, ") could not be found in the ", "Partner database.  Check your claim, someone may have already fixed the data you are attempting to fix.");
                                intErrorCount = intErrorCount + 1;

                            }
                        }
                        else
                        {
                            // AssignmentID not found.  Create a node to hold the error text and add it to the Root.
                            //ToDo: Verify
                            //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                            objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                            objErrorNode.AppendChild(objNode);
                            objNode.InnerText = string.Concat("No Assignment ID could be found for claim, ", strLynxID.ToString(), "-", strVehicleNumber.ToString(), ".  Please ensure you ", "entered a valid LynxID/Vehicle Number combination and that that vehicle's assignment is still valid.");
                            intErrorCount = intErrorCount + 1;
                            //g_objevents.HandleEvent
                        }
                    }
                }

                // Grab error XML.  If there were no errors set the value to null
                strErrorXML = (intErrorCount == 0 ? string.Empty : objReturnDOM.OuterXml);

                // Call GetUnprocessedPartnerData to get the updated list of partner records an perorm the transformation to HTML.  This is a
                // little hokey but these are highly specalized procedures, this provides a way of including any error xml in the
                // transformation, and saves the web page a round trip.  Nice rationalization, huh?
                strFixUnprocessedPartnerData = GetUnprocessedPartnerData(strSessionKey, strWindowID, strUserID, strErrorXML);

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.cExecute.cs - FixUnprocessedPartnerData", string.Concat("Exception: ", ex.Message, ex.InnerException), "");
                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                    }
                }
                objRS = null;
                xmlTransDocment = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
            return strFixUnprocessedPartnerData;
        }

        /// <summary>
        /// String Left Process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Left(string param, int length)
        {
            try
            {
                string strLeft = param.Substring(0, length);
                return strLeft;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Right Process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Right(string param, int length)
        {
            try
            {
                string strRight = param.Substring(param.Length - length, length);
                return strRight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Mid
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex, int length)
        {
            try
            {
                string strMid = param.Substring(startIndex, length);
                return strMid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Mid - Overloaded
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex)
        {
            try
            {
                string strMid = param.Substring(startIndex);
                return strMid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
