﻿using System;
using System.Xml;
using System.Collections;
using Lynx.APD.Component.Library.Common;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using COMRules = RulesEngine;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Configuration;
using SiteUtilities;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    /// Transfer Estimate data to APD DB.
    /// </summary>
    public class CPartnerDataXfer
    {
        /// <summary>
        /// Component PartnerDataXferManager : ClassModule CPartnerDataXfer
        /// 
        /// Routines for transferring data from partner db to apd db.
        /// </summary> 
        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CPartnerDataXfer." });
        MEscapeUtilities objMEscapeUtilities = null;
        MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        MDomUtils objMdomUtilis = null;
        SharedData objShared = null;
        DataAccess.CDataAccessor objCDataAccessor = null;
        //Private vars used to save passing vars to private utility functions
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        //bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);        
        private XmlNode mobjCurNode;       // Working node for data extract
        private XmlNode mobjRootNode;       //Root node of objAPDEstimateDataDom
        #endregion

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        #region Enumerators
        private enum ErrorCodes : ulong
        {
            ePartnerXferError = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CPartnerDataXfer_CLS_ErrorCodes,
            eEstimateDataAuditFailed = 1
        }

        //These enumerated types mirror the arguments to uspEstimateDetailUpdDetail.
        // They are used to simplify reading and updating the code that calls this proc.

        private enum EEstimateDetailUpdDetail
        {
            eDocumentID,                          // DocumentID of estimate
            eDetailNumber,                                                    // Line number from detail
            eAPDPartCategoryAgreedCD,                                         //The Agreed APD part category "bucket" this part belongs to "PAT", "PAN", "POT", "PON", etc
            eAPDPartCategoryOriginalCD,                                       //The Original APD part category "bucket" this part belongs to "PAT", "PAN", "POT", "PON", etc
            eComment,                                                         // Comment entered by shop for this line
            eDBLaborHoursOriginal,                                            //The Labor Hours from the estimate package's database
            eDBPriceOriginal,                                                 // The part price from the estimate package's database
            eDBRefinishLaborHoursOriginal,                                    // The Refinish Labor Hours from the estimate package's database
            eDescription,                                                     //Description
            eLaborHoursAgreed,                                                // Labor hours - agreed value
            eLaborHoursChangedFlag,                                           // Labor hours changed on estimate (this has nothing to do with original/agreed)
            eLaborHoursIncludedAgreedFlag,                                    // Labor hours included somewhere else - agreed value
            eLaborHoursIncludedOriginalFlag,                                  // Labor hours included somewhere else - original value
            eLaborHoursOriginal,                                              // Labor hours - original value
            eLaborTypeAgreedCD,                                               // (B)ody, (F)rame, (M)echanical - agreed value
            eLaborTypeOriginalCD,                                             // (B)ody, (F)rame, (M)echanical - original value
            eLynxComment,                                                     // Comment entered by Lynx Rep for this line
            eManualLineFlag,                                                  // Indicates whether this line was entered manually by the shop
            eOperationCD,                                                     // CIECA standard operation code "Op#"
            ePartTypeAgreedCD,                                               // CIECA standard part category code "PAL", "PAN", "PAS", etc - agreed value
            ePartTypeOriginalCD,                                              // CIECA standard part category code "PAL", "PAN", "PAS", etc - original value
            ePriceAgreed,                                                     // Agreed Price
            ePriceChangedFlag,                                                // Price changed on estimate (again, nothing to do with original/agreed)
            ePriceIncludedAgreedFlag,                                         // Part price included somewhere else - agreed value
            ePriceIncludedOriginalFlag,                                       // Part price included somewhere else - original value
            ePriceOriginal,                                                   // Price on estimate
            eQuantityAgreed,                                                  // Quantity
            eQuantityOriginal,                                                // Quantity on estimate
            eRefinishLaborHoursAgreed,                                        // Refinish hours
            eRefinishLaborHoursChangedFlag,                                   // Refinish hours changed on estimate (again, nothing to do with original/agreed)
            eRefinishLaborHoursOriginal,                                      // Refinish hours on estimate
            eRefinishLaborIncludedAgreedFlag,                                 // Refinsih Labor hours included somewhere else - agreed value
            eRefinishLaborIncludedOriginalFlag,                              // Refinish Labor hours included somewhere else - original value
            eUserID,                                                          // User ID (use either this OR @UserName)
            eUserName,                                                        // NT User Name ('System' is also acceptable)(use either this OR @UserID)
            eSysLastUpdatedDate,                                              // If update, syslastupdatedate.  Ignored on inserts
            eEstimateDetailUpdDetail_ParameterCount
        }

        #endregion

        #region Public Functions

        public CPartnerDataXfer(SharedData objPartnerDataXfer)
        {
            try
            {
                objShared = objPartnerDataXfer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert the Estimate data to APD
        /// </summary>
        /// <param name="objEstimateDataDom"></param>
        /// <param name="lngAPDDocumentID"></param>
        /// <param name="strPassedDocument"></param>
        /// <param name="strPartnerSmallID"></param>
        /// <returns></returns>
        public string TransferDataToAPD_v2(XmlDocument objEstimateDataDom, long lngAPDDocumentID, string strPassedDocument, string strPartnerSmallID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferDataToApd: " });
            //Objects that need clean up.

            XmlDocument objStyleSheetDom = null;   //Holds loaded stylesheet
            XmlDocument objAPDEstimateDataDom = null;  // Holds transformed "APD" formatted estimate data
            XmlNodeList objNodeList = null;      // Working Nodelist for data extraction
            XmlDocument objClaimXML = null;     //This hold the claim information for the given assignment id\
            XmlElement objAssignmentVehNode = null;   // Holds vehicle node in the claim information XML           
            XmlDocument objAPDEstimateRules = null;   // APD Estimate Rules
            XmlDocument objRulesOutputXML = null;   //Rules Engine output XML object
            XmlNodeList objRulesFiredList = null;      // The list of rules fired
            XmlElement objRuleFired = null;    // Rule Fired item
            COMRules.CRulesEngine objRules = null;
            //Primitives that dont.
            string strAction = string.Empty,   // Specific area of proc being executed (used for error tracing)
                strAPDEstimateXML = string.Empty, // String representation of transformed Estimate Data
                strTaxTypeCD = string.Empty;    //Translated Database insertion value for TaxTypeCD in Summary          

            int intTaxableFlag = 0;      // Transalated Database insertion value for Taxable flag In Summary
            object varResults = null;   // and store
            string strYear = string.Empty;
            string strMake = string.Empty;
            string strModel = string.Empty;
            bool blnInTrans = false;
            string strAssignmentID = string.Empty;
            string strRulesOutput = string.Empty;
            int iAuditWeight = 0;
            int i = 0;
            string strInsertSummarySPName = string.Empty;
            string strInsertDetailSPName = string.Empty;
            string strInsertAuditResultSPName = string.Empty;
            string strNTUserID = null;
            string strVin = string.Empty;
            bool blnDAAssignment = false;

            string sDetailNumber = string.Empty,
                                              sKeyNames = string.Empty,
                                              sKeyValues = string.Empty, strImpactArea = string.Empty;
            string[] aKeyName = null,
                aKeyValue = null;
            string strappend = string.Empty;
            object objAppend = null;
            int intRepairDays = 0;
            Hashtable hashTable = null;
            string strVehicleYear = string.Empty;
            string strRepairDays = string.Empty;
            string strDetailparam = string.Empty;
            string strEstimateSummary = string.Empty;
            int intVehicleYear = 0;
            //SqlConnection sqlConnection = null;
            //SqlTransaction transaction = null;
            Hashtable htVINDetails = null;
            NadaWrapper objNadaWS = null;
            UInt64 errorCode;
            Common.WriteLog objWriteLog = null;
            CEvents mobjCEvents = null;
            try
            {

                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMdomUtilis = new MDomUtils();
                objCDataAccessor = new DataAccess.CDataAccessor();
                objNadaWS = new NadaWrapper();
                objWriteLog = new WriteLog();
                mobjCEvents = new CEvents();

                if (objEstimateDataDom == null)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.TransferDataToAPD_v2", " Passed XML DOM was Nothing."));

                if (lngAPDDocumentID == 0)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.TransferDataToAPD_v2", " Passed APD Document ID was zero."));

                //***  Transform the Partner Data's XML into the "standard" APD format  ***
                strAction = "Transforming Partner Estimate Data";
                //Do the transfer.  Note that each partner has its own XSL.
                //objAppend = MLAPDPartnerDataMgr.g_objEvents;
                objAPDEstimateDataDom = objMdomUtilis.TransformDomAsDom(ref objEstimateDataDom, (string.Concat(objShared.g_strSupportDocPath, "//", strPartnerSmallID, "EstimateXfer.xsl")));

                strAPDEstimateXML = objAPDEstimateDataDom.OuterXml;
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat(" Results from Transform: ", Environment.NewLine, strAPDEstimateXML), "");
                //***  The transformed XML is now awaiting processing in objAPDEstimateDataDom  ***
                mobjRootNode = objAPDEstimateDataDom.DocumentElement;

                //Get the assignment id from the APDEstimate. We will use this later to obtain the claim information and
                // use it for estimate audit
                strAssignmentID = RootChild("Header/@AssignmentID");

                strNTUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/NTUserID");

                //sqlConnection = new SqlConnection(objCDataAccessor.GetConnectionString("APD"));
                //sqlConnection.Open();

                //transaction = sqlConnection.BeginTransaction();
                blnInTrans = true;   //Flag new transaction state.

                // Insert the estimate header information
                strAction = "Inserting APD Estimate Header";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat(strAction), "");
                // Compile parameter information into a variant array.  We have to do it this way or we will break VB's
                // line continuation limit.              
                hashTable = new Hashtable();
                hashTable.Add("@DocumentID", lngAPDDocumentID);
                hashTable.Add("@AppraiserLicenseNumber", RootChild("Header/Appraiser/@AppraiserLicenseNumber"));
                hashTable.Add("@AppraiserName", RootChild("Header/Appraiser/@AppraiserName"));
                hashTable.Add("@ClaimantName", RootChild("Header/@ClaimantName"));
                hashTable.Add("@ClaimNumber", RootChild("Header/@ClaimNumber"));
                hashTable.Add("@CompanyAddress", RootChild("Header/Estimator/@CompanyAddress"));
                hashTable.Add("@CompanyAreaCode", RootChild("Header/Estimator/@CompanyAreaCode"));
                hashTable.Add("@CompanyCity", RootChild("Header/Estimator/@CompanyCity"));
                hashTable.Add("@CompanyExchangeNumber", RootChild("Header/Estimator/@CompanyExchangeNumber"));
                hashTable.Add("@CompanyName", RootChild("Header/Estimator/@CompanyName"));
                hashTable.Add("@CompanyState", RootChild("Header/Estimator/@CompanyState"));
                hashTable.Add("@CompanyUnitNumber", RootChild("Header/Estimator/@CompanyUnitNumber"));
                hashTable.Add("@CompanyZip", RootChild("Header/Estimator/@CompanyZip"));

                // Detail Editable?
                // In an effort to protect the user from using bad data which we may receive from our VAN, a flag is used to indicate
                // whether the detail is editable or not.  If the data passes our audit tests, we set this flag to yes, allowing the
                // user to access the data.  If it doesn't, this flag is turned off to prevent write access.  In order to pass this test,
                //there must be no variance and all stylesheet audits must have passed.

                if (RootChild("Summary/LineItem[@Type='Variance']/@ExtendedAmount") != "0")
                {
                    errorCode = Convert.ToUInt64(ErrorCodes.eEstimateDataAuditFailed);
                    mobjCEvents.HandleEvent((int)errorCode, string.Concat(new string[] { PROC_NAME, "Estimate Audit" }),
                         "Estimate Audit Failed: There must be no variance.",
                        string.Concat(new string[] {"Partner document = " , Environment.NewLine , strPassedDocument , Environment.NewLine , Environment.NewLine ,
                         "After APD Transform = " , Environment.NewLine,strAPDEstimateXML}), Environment.NewLine, false);

                }
                else if (mobjRootNode.SelectNodes("Audit/AuditItem[@Passed='No']").Count > 0)
                {
                    errorCode = Convert.ToUInt64(ErrorCodes.eEstimateDataAuditFailed);
                    mobjCEvents.HandleEvent((int)errorCode, string.Concat(PROC_NAME, "Estimate Audit"), "Estimate Audit Failed: A stylesheet audit has failed.",
                        string.Concat("Partner document = ", Environment.NewLine, strPassedDocument, Environment.NewLine, Environment.NewLine,
                         "After APD Transform = ", Environment.NewLine, strAPDEstimateXML), Environment.NewLine, false);
                }
                else
                    hashTable.Add("@DetailEditableFlag", 1);

                hashTable.Add("@InspectionAddress", RootChild("Header/Inspection/@InspectionAddress"));
                hashTable.Add("@InspectionAreaCode", RootChild("Header/Inspection/@InspectionAreaCode"));
                hashTable.Add("@InspectionCity", RootChild("Header/Inspection/@InspectionCity"));
                hashTable.Add("@InspectionExchangeNumber", RootChild("Header/Inspection/@InspectionExchangeNumber"));
                hashTable.Add("@InspectionLocation", RootChild("Header/Inspection/@InspectionLocation"));
                hashTable.Add("@InspectionState", RootChild("Header/Inspection/@InspectionState"));
                hashTable.Add("@InspectionType", RootChild("Header/Inspection/@InspectionType"));
                hashTable.Add("@InspectionUnitNumber", RootChild("Header/Inspection/@InspectionUnitNumber"));
                hashTable.Add("@InspectionZip", RootChild("Header/Inspection/@InspectionZip"));
                hashTable.Add("@InsuranceCompanyName", RootChild("Header/@InsuranceCompanyName"));
                hashTable.Add("@InsuredName", RootChild("Header/@InsuredName"));
                hashTable.Add("@LossDate", ((RootChild("Header/@LossDate")).Length == 0 ? "NULL" : RootChild("Header/@LossDate")));
                hashTable.Add("@LossType", RootChild("Header/@LossType"));
                hashTable.Add("@OwnerAddress", RootChild("Header/Owner/@OwnerAddress"));
                hashTable.Add("@OwnerCity", RootChild("Header/Owner/@OwnerCity"));
                hashTable.Add("@OwnerDayAreaCode", RootChild("Header/Owner/@OwnerDayAreaCode"));
                hashTable.Add("@OwnerDayExchangeNumber", RootChild("Header/Owner/@OwnerDayExchangeNumber"));
                hashTable.Add("@OwnerDayExtensionNumber", RootChild("Header/Owner/@OwnerDayExtensionNumber"));
                hashTable.Add("@OwnerDayUnitNumber", RootChild("Header/Owner/@OwnerDayUnitNumber"));
                hashTable.Add("@OwnerEveningAreaCode", RootChild("Header/Owner/@OwnerEveningAreaCode"));
                hashTable.Add("@OwnerEveningExchangeNumber", RootChild("Header/Owner/@OwnerEveningExchangeNumber"));
                hashTable.Add("@OwnerEveningExtensionNumber", RootChild("Header/Owner/@OwnerEveningExtensionNumber"));
                hashTable.Add("@OwnerEveningUnitNumber", RootChild("Header/Owner/@OwnerEveningUnitNumber"));
                hashTable.Add("@OwnerName", RootChild("Header/Owner/@OwnerName"));
                hashTable.Add("@OwnerState", RootChild("Header/Owner/@OwnerState"));
                hashTable.Add("@OwnerZip", RootChild("Header/Owner/@OwnerZip"));
                hashTable.Add("@PolicyNumber", RootChild("Header/@PolicyNumber"));
                hashTable.Add("@Remarks", RootChild("Header/@Remarks"));

                strRepairDays = RootChild("Header/@RepairDays");
                if (strRepairDays != string.Empty)
                    intRepairDays = Convert.ToInt32(strRepairDays.Length == 0 ? 0 : Convert.ToInt32(strRepairDays));

                hashTable.Add("@RepairDays", intRepairDays);
                hashTable.Add("@ShopAddress", RootChild("Header/Shop/@ShopAddress"));
                hashTable.Add("@ShopAreaCode", RootChild("Header/Shop/@ShopAreaCode"));
                hashTable.Add("@ShopCity", RootChild("Header/Shop/@ShopCity"));
                hashTable.Add("@ShopExchangeNumber", RootChild("Header/Shop/@ShopExchangeNumber"));
                hashTable.Add("@ShopName", RootChild("Header/Shop/@ShopName"));
                hashTable.Add("@ShopRegistrationNumber", RootChild("Header/Shop/@ShopRegistrationNumber"));
                hashTable.Add("@ShopState", RootChild("Header/Shop/@ShopState"));
                hashTable.Add("@ShopUnitNumber", RootChild("Header/Shop/@ShopUnitNumber"));
                hashTable.Add("@ShopZip", RootChild("Header/Shop/@ShopZip"));
                hashTable.Add("@VehicleBodyStyle", RootChild("Header/Vehicle/@VehicleBodyStyle"));
                hashTable.Add("@VehicleColor", RootChild("Header/Vehicle/@VehicleColor"));
                hashTable.Add("@VehicleEngineType", RootChild("Header/Vehicle/@VehicleEngineType"));

                // Impact Points - These are made up of 0 or more elements.  the Values of these elements need to be
                // concatenated together.
                strImpactArea = RootChild("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='Yes']/@ImpactArea");
                // Enumerate any additional impact points
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='No']");

                mobjCurNode = null;
                foreach (XmlNode objImpactNode in objNodeList)
                {
                    mobjCurNode = objImpactNode;
                    strImpactArea = string.Concat(strImpactArea, ", ", CurChild("@ImpactArea"));
                }
                hashTable.Add("@VehicleImpactPoint", strImpactArea);
                hashTable.Add("@VehicleMake", RootChild("Header/Vehicle/@VehicleMake"));
                if ((RootChild("Header/Vehicle/@VehicleMileage")).Length == 0)
                    hashTable.Add("@VehicleMileage", RootChild("Header/Vehicle/@VehicleMileage"));
                else
                    hashTable.Add("@VehicleMileage", RootChild("Header/Vehicle/@VehicleMileage"));
                // hashTable.Add("@VehicleMileage", (RootChild("Header/Vehicle/@VehicleMileage")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleMileage"));
                hashTable.Add("@VehicleModel", RootChild("Header/Vehicle/@VehicleModel"));

                // Options - Like impact points, there may be 0 or more elements.  These also must be concatenated together
                //hashTable["@VehicleOptions"] = "'";
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//VehicleOption");
                if (objNodeList.Count > 0)
                {
                    mobjCurNode = null;
                    foreach (XmlNode objCurNode in objNodeList)
                    {
                        mobjCurNode = objCurNode;
                        hashTable["@VehicleOptions"] = string.Concat(hashTable["@VehicleOptions"], CurChild("@Option"), ", ");
                    }
                }
                else
                    hashTable["@VehicleOptions"] = string.Empty;

                if ((RootChild("Header/Vehicle/@VehicleYear")).Length == 0)
                    hashTable.Add("@VehicleYear", DBNull.Value);
                else
                    hashTable.Add("@VehicleYear", RootChild("Header/Vehicle/@VehicleYear"));
                //strVehicleYear = ((RootChild("Header/Vehicle/@VehicleYear")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleYear"));
                //intVehicleYear = Convert.ToInt32(strVehicleYear);
                //hashTable.Add("@VehicleYear", intVehicleYear);
                hashTable.Add("@Vin", RootChild("Header/Vehicle/@Vin"));

                if ((RootChild("Header/@WrittenDate")).Length < 6)
                    hashTable.Add("@WrittenDate", DBNull.Value);
                else
                    hashTable.Add("@WrittenDate", RootChild("Header/@WrittenDate"));

                //hashTable.Add("@WrittenDate", ((RootChild("Header/@WrittenDate")).Length < 6 ? "NULL" : RootChild("Header/@WrittenDate")));
                hashTable.Add("@UserID", 0);
                hashTable.Add("@UserName", "DEFAULT");
                hashTable.Add("@SysLastUpdatedDate", "DEFAULT");

                // OK, time to insert the estimate header record
                objCDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), hashTable, "APD");
                //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), hashTable);
                // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), strParameters);

                // Insert Estimate Detail Records
                strAction = "Inserting APD Estimate Details";

                // We'll need to use the parameters array again, reset it first to 0 to clear it, then back to the
                // length we need

                strInsertDetailSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertDetailSPName");
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat(strAction), "");
                // Get a list of detail lines for for details.
                objNodeList = mobjRootNode.SelectNodes("Detail/DetailLine");
                mobjCurNode = null;
                foreach (XmlNode objCurNode in objNodeList)
                {
                    mobjCurNode = objCurNode;
                    hashTable = new Hashtable();

                    // Set Parameter array values
                    hashTable.Add("@DocumentID", lngAPDDocumentID);

                    strDetailparam = CurChild("@DetailNumber");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DetailNumber", Convert.ToInt16(strDetailparam));
                    else
                        hashTable.Add("@DetailNumber", 0);

                    switch (CurChild("Part/@APDPartCategory"))
                    {
                        case "TaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PAT");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PAT");
                            break;
                        case "NonTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PAN");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PAN");
                            break;
                        case "OtherTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "POT");
                            hashTable.Add("@APDPartCategoryOriginalCD", "POT");
                            break;
                        case "OtherNonTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PON");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PON");
                            break;
                        case "Storage":
                            hashTable.Add("@APDPartCategoryAgreedCD", "STG");
                            hashTable.Add("@APDPartCategoryOriginalCD", "STG");
                            break;
                        case "Sublet":
                            hashTable.Add("@APDPartCategoryAgreedCD", "SUB");
                            hashTable.Add("@APDPartCategoryOriginalCD", "SUB");
                            break;
                        case "Towing":
                            hashTable.Add("@APDPartCategoryAgreedCD", "TOW");
                            hashTable.Add("@APDPartCategoryOriginalCD", "TOW");
                            break;
                        case "Other":
                            hashTable.Add("@APDPartCategoryAgreedCD", "OTH");
                            hashTable.Add("@APDPartCategoryOriginalCD", "OTH");
                            break;
                        default:
                            hashTable.Add("@APDPartCategoryAgreedCD", "");
                            hashTable.Add("@APDPartCategoryOriginalCD", "");
                            break;
                    }

                    hashTable.Add("@Comment", CurChild("@Comment"));

                    strDetailparam = CurChild("Labor[@LaborType!='Refinish']/@DBLaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBLaborHoursOriginal", 0);

                    strDetailparam = CurChild("Part/@DBPartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBPriceOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBPriceOriginal", 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@DBLaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBRefinishLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBRefinishLaborHoursOriginal", 0);

                    hashTable.Add("@Description", CurChild("@Description"));


                    strDetailparam = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@LaborHoursAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@LaborHoursAgreed", 0);

                    hashTable.Add("@LaborHoursChangedFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@LaborHoursIncludedAgreedFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@LaborHoursIncludedOriginalFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);

                    if (strDetailparam != string.Empty)
                        hashTable.Add("@LaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@LaborHoursOriginal", 0);

                    switch (CurChild("Labor[@LaborType!='Refinish']/@LaborType"))
                    {
                        case "Body":
                            hashTable.Add("@LaborTypeAgreedCD", "B");
                            hashTable.Add("@LaborTypeOriginalCD", "B");
                            break;
                        case "Frame":
                            hashTable.Add("@LaborTypeAgreedCD", "F");
                            hashTable.Add("@LaborTypeOriginalCD", "F");
                            break;
                        case "Mechanical":
                            hashTable.Add("@LaborTypeAgreedCD", "M");
                            hashTable.Add("@LaborTypeOriginalCD", "M");
                            break;
                        default:
                            hashTable.Add("@LaborTypeAgreedCD", "");
                            hashTable.Add("@LaborTypeOriginalCD", "");
                            break;
                    }

                    hashTable.Add("@LynxComment", "DEFAULT");
                    hashTable.Add("@ManualLineFlag", CurChild("@ManualLineFlag") == "Yes" ? 1 : 0);
                    // strParameters[(int)EEstimateDetailUpdDetail.eLynxComment] = "DEFAULT";
                    //strParameters[(int)EEstimateDetailUpdDetail.eManualLineFlag] = (CurChild("@ManualLineFlag") == "Yes" ? 1 : 0).ToString();

                    switch (CurChild("@OperationCode"))
                    {
                        case "":
                            hashTable.Add("@OperationCD", "");
                            break;
                        case "Alignment":
                            hashTable.Add("@OperationCD", "ALGN");
                            break;
                        case "Comment":
                            hashTable.Add("@OperationCD", "COM");
                            break;
                        case "Other":
                            hashTable.Add("@OperationCD", "OTH");
                            break;
                        case "Refinish":
                            hashTable.Add("@OperationCD", "REFN");
                            break;
                        case "Remove/Install":
                            hashTable.Add("@OperationCD", "R&I");
                            break;
                        case "Repair":
                            hashTable.Add("@OperationCD", "REPR");
                            break;
                        case "Replace":
                            hashTable.Add("@OperationCD", "REPL");
                            break;
                        case "Sublet":
                            hashTable.Add("@OperationCD", "SUB");
                            break;
                        default:
                            hashTable.Add("@OperationCD", "!ND!");
                            break;
                    }

                    switch (CurChild("Part/@PartType"))
                    {
                        case "Aftermarket":
                            hashTable.Add("@PartTypeAgreedCD", "AFMK");
                            hashTable.Add("@PartTypeOriginalCD", "AFMK");
                            break;
                        case "Glass":
                            hashTable.Add("@PartTypeAgreedCD", "GLS");
                            hashTable.Add("@PartTypeOriginalCD", "GLS");
                            break;
                        case "LKQ":
                            hashTable.Add("@PartTypeAgreedCD", "LKQ");
                            hashTable.Add("@PartTypeOriginalCD", "LKQ");
                            break;
                        case "New":
                            hashTable.Add("@PartTypeAgreedCD", "NEW");
                            hashTable.Add("@PartTypeOriginalCD", "NEW");
                            break;
                        case "Other":
                            hashTable.Add("@PartTypeAgreedCD", "OTH");
                            hashTable.Add("@PartTypeOriginalCD", "OTH");
                            break;
                        case "Reconditioned":
                            hashTable.Add("@PartTypeAgreedCD", "REC");
                            hashTable.Add("@PartTypeOriginalCD", "REC");
                            break;
                        case "Sublet":
                            hashTable.Add("@PartTypeAgreedCD", "SUB");
                            hashTable.Add("@PartTypeOriginalCD", "SUB");
                            break;
                        default:
                            hashTable.Add("@PartTypeAgreedCD", "");
                            hashTable.Add("@PartTypeOriginalCD", "");
                            break;
                    }

                    strDetailparam = CurChild("Part/@PartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@PriceAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@PriceAgreed", 0);

                    hashTable.Add("@PriceChangedFlag", CurChild("Part/@PriceChangedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@PriceIncludedAgreedFlag", CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@PriceIncludedOriginalFlag", CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0);

                    strDetailparam = CurChild("Part/@PartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@PriceOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@PriceOriginal", 0);

                    strDetailparam = CurChild("Part/@Quantity");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@QuantityAgreed", Convert.ToInt32(strDetailparam));
                    else
                        hashTable.Add("@QuantityAgreed", 0);

                    strDetailparam = CurChild("Part/@Quantity");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@QuantityOriginal", Convert.ToInt32(strDetailparam));
                    else
                        hashTable.Add("@QuantityOriginal", 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@RefinishLaborHoursAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@RefinishLaborHoursAgreed", 0);

                    hashTable.Add("@RefinishLaborHoursChangedFlag", CurChild("Labor[@LaborType='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@RefinishLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@RefinishLaborHoursOriginal", 0);

                    hashTable.Add("@RefinishLaborIncludedAgreedFlag", CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@RefinishLaborIncludedOriginalFlag", CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@UserID", 0);
                    hashTable.Add("@UserName", strNTUserID);
                    hashTable.Add("@SysLastUpdatedDate", "DEFAULT");

                    // Insert the record
                    objCDataAccessor.ExecuteSp(strInsertDetailSPName, hashTable, "APD");
                    //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, strInsertDetailSPName, hashTable);
                }
                // Insert Estimate Summary records
                // Estimate Summary records are inserted as individual line items (ie.  Parts Total, Refinish Labor Total,
                // etc.)  We'll need to retrieve each of these out of the XML and insert them 1 at a time into summary
                strAction = "Inserting APD Estimate Summary";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat(strAction), "");
                // Get a list of line items for the summary.
                objNodeList = mobjRootNode.SelectNodes("Summary/LineItem[@TransferLine='Yes']");

                strInsertSummarySPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertSummarySPName");
                mobjCurNode = null;
                foreach (XmlNode mobjCurNod in objNodeList)
                {
                    mobjCurNode = mobjCurNod;
                    // Have to set the Database insertion values
                    intTaxableFlag = (CurChild("@Taxable") == "Yes" ? 1 : 0);

                    switch (CurChild("@TaxBase"))
                    {
                        case "Parts":
                            strTaxTypeCD = "PT";
                            break;
                        case "Materials":
                            strTaxTypeCD = "SP";
                            break;
                        case "PartsAndMaterials":
                            strTaxTypeCD = "MT";
                            break;
                        case "Labor":
                            strTaxTypeCD = "LB";
                            break;
                        case "AdditionalCharges":
                            strTaxTypeCD = "AC";
                            break;
                        case "Subtotal":
                            strTaxTypeCD = "ST";
                            break;
                        default:
                            strTaxTypeCD = "OT";
                            break;
                    }

                    hashTable = new Hashtable();
                    hashTable.Add("@EstimateSummaryID", 0);
                    hashTable.Add("@EstimateSummaryTypeID", null);
                    hashTable.Add("@EstimateSummaryType", CurChild("@Type"));
                    hashTable.Add("@EstimateSummaryTypeCD", CurChild("@Category"));
                    hashTable.Add("@DocumentID", lngAPDDocumentID);

                    strEstimateSummary = CurChild("@ExtendedAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedExtendedAmt", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedExtendedAmt", 0);

                    strEstimateSummary = CurChild("@Hours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedHrs", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedHrs", 0);

                    strEstimateSummary = CurChild("@Percent");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedPct", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedPct", 0);

                    hashTable.Add("@AgreedTaxableFlag", intTaxableFlag);

                    strEstimateSummary = CurChild("@UnitAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedUnitAmt", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedUnitAmt", 0);

                    hashTable.Add("@Comments", CurChild("@Note"));

                    strEstimateSummary = CurChild("@ExtendedAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalExtendedAmt", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalExtendedAmt", 0);

                    strEstimateSummary = CurChild("@Hours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalHrs", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalHrs", 0);

                    strEstimateSummary = CurChild("@Percent");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalPct", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalPct", 0);

                    hashTable.Add("@OriginalTaxableFlag", intTaxableFlag);

                    strEstimateSummary = CurChild("@UnitAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalUnitAmt", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalUnitAmt", 0);

                    hashTable.Add("@TaxTypeCD", strTaxTypeCD);
                    hashTable.Add("@UserID", 0);
                    hashTable.Add("@UserName", strNTUserID);
                    hashTable.Add("@SysLastUpdatedDate", "DEFAULT");
                    hashTable.Add("@ApplicationCD", "APD");
                    hashTable.Add("@ReturnSysLastUpdate", 1);

                    strEstimateSummary = CurChild("@RepairHours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalRepairHours", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalRepairHours", 0);


                    strEstimateSummary = CurChild("@ReplaceHours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalReplaceHours", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalReplaceHours", 0);

                    // Insert the record
                    objCDataAccessor.ExecuteSp(strInsertSummarySPName, hashTable, "APD");
                    //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, strInsertSummarySPName, hashTable);
                }
                // Insert Estimate Audit record
                // The Estimate's audit record will contain data not already saved off in the estimate records in APD.
                // It contains data used by APD to specifically audit the estimate.
                strAction = "Inserting APD Estimate Audit Record";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat(strAction), "");
                //Get the vehicle VIN.
                strVin = RootChild("Header/Vehicle/@Vin");

                //Do a VIN lookup if the VIN is not blank.
                if (strVin != string.Empty)
                {

                    //We need to run the VIN through NADA first.  We're not going to make the determination whether or not the
                    // VIN is valid here.  We're simply going to retrieve the info for determination later.
                    // objNada = objMLAPDPartnerDataMgr.CreateObjectEx("NadaWrapper.CNadaWrapper");   

                    if (IsDebug)
                        objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Calling VINLookup functionality for getting the Vehicle Year, Make and Model."), "");

                    htVINDetails = objNadaWS.GetVIN(strVin);

                    //Strip out single quotes from results.
                    //Believe it or not, there are some bogus single quotes in the NADA database.
                    if (htVINDetails != null)
                    {
                        if (htVINDetails.ContainsKey("Year"))
                            strYear = (string)htVINDetails["Year"];
                        if (htVINDetails.ContainsKey("Make"))
                            strMake = (string)htVINDetails["Make"];
                        if (htVINDetails.ContainsKey("Model"))
                            strModel = (string)htVINDetails["Model"];
                    }

                    //objNada = null;
                }
                //Commit what we have in the transaction so far.  As per Test Track item 1430,
                //the estimate audit which follows does not need to be included in the transaction,
                // MLAPDPartnerDataMgr.g_objDataAccessor.CommitTransaction();
                //try
                //{
                //    if (transaction.Connection.State == System.Data.ConnectionState.Open)
                //    {
                //        transaction.Commit();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    blnInTrans = false;
                //    objWriteLog.LogEvent("COM Conversion", "TRANSACTION ERROR", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Transaction commited for DocumentID : ", lngAPDDocumentID), string.Concat("Exception: ", ex.Message, ex.InnerException));
                //}

                blnInTrans = false; //Flag new transaction state.
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Transaction Committed Successfully."), "");

                //New Estimate Auditing process
                if (strAssignmentID != string.Empty)
                {
                    if (strAssignmentID.Length > 0)
                    {
                        //get the claim detail for the assignment
                        hashTable = new Hashtable();
                        hashTable.Add("@AssignmentID", Convert.ToInt64(strAssignmentID));

                        blnDAAssignment = Convert.ToBoolean(objCDataAccessor.ExecuteSpNamedParams(objMLAPDPartnerDataMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), hashTable, "APD"));
                        //blnDAAssignment = Convert.ToBoolean(MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(
                        //     objMLAPDPartnerDataMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), new object[] {
                        //              "@AssignmentID", strAssignmentID}));

                        if (!blnDAAssignment)
                        {
                            //Auditing needs to be done for program shop estimates only.
                            //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                            objClaimXML = new XmlDocument();

                            hashTable = new Hashtable();
                            hashTable.Add("@AssignmentID", Convert.ToInt64(strAssignmentID));

                            strappend = objCDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), hashTable, "APD");
                            // strappend = MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), new object[] { "@AssignmentID", strAssignmentID });
                            objMLAPDPartnerDataMgr.LoadXml(ref objClaimXML, ref strappend, PROC_NAME, "TransferDataToAPD");

                            //Set the connect string to APD
                            //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                            //get the vehicle node so we can add the nada values
                            objAssignmentVehNode = (XmlElement)objClaimXML.SelectSingleNode("/Root/Vehicle");

                            if (objAssignmentVehNode != null)
                            {
                                objAssignmentVehNode.SetAttribute("VehicleYearNADA", strYear);
                                objAssignmentVehNode.SetAttribute("VehicleMakeNADA", strMake);
                                objAssignmentVehNode.SetAttribute("VehicleModelNADA", strModel);
                            }

                            //load the APD Estimate audit rules
                            objAPDEstimateRules = new XmlDocument();
                            strappend = string.Concat(objShared.g_strSupportDocPath, "//", "APDEstimateRules.xml");
                            objMdomUtilis.LoadXmlFile(ref objAPDEstimateRules, ref strappend, PROC_NAME, "TransferDataToAPD");
                            if (IsDebug)
                                objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Before Create RulesEngine object"), "");
                            //create the Rules Engine object
                            objRules = new COMRules.CRulesEngine();
                            strRulesOutput = objRules.EngineExecute(objAPDEstimateDataDom.OuterXml,
                                                                            objAPDEstimateRules.OuterXml,
                                                                            objClaimXML.OuterXml);
                            // if (IsDebug)
                            objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("XML Returned", strRulesOutput), "");
                            if (strRulesOutput != string.Empty)
                            {
                                objRulesOutputXML = new XmlDocument();
                                objMLAPDPartnerDataMgr.LoadXml(ref objRulesOutputXML, ref strRulesOutput, PROC_NAME, "TransferDataToAPD");

                                //get the audit weight
                                iAuditWeight = Convert.ToInt32(objRulesOutputXML.SelectSingleNode("/Root/@auditWeight").InnerText);
                                hashTable = new Hashtable();
                                hashTable.Add("@DocumentID", lngAPDDocumentID);
                                hashTable.Add("@AuditWeight", iAuditWeight);
                                if (IsDebug)
                                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Updateding uspAuditWeightUpdDetail with - DocumentID", lngAPDDocumentID, " AuditWeight", iAuditWeight), "");
                                //update the document's audit weight
                                objCDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), hashTable, "APD");
                                // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), strParameters);
                                //now log the rules fired
                                objRulesFiredList = objRulesOutputXML.SelectNodes("/Root/RulesFired/Rule");

                                if (objRulesFiredList != null)
                                {
                                    strInsertAuditResultSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertAuditResultSPName");

                                    foreach (XmlElement objRuleFiredlist in objRulesFiredList)
                                    {
                                        //insert the rule fired into the audit results table
                                        sDetailNumber = string.Empty;

                                        if (objRuleFiredlist.GetAttribute("recursive") == "Y")
                                        {
                                            //get the detail number from the key values
                                            sKeyNames = objRuleFiredlist.GetAttribute("keyName");
                                            sKeyValues = objRuleFiredlist.GetAttribute("keyValue");

                                            if (sKeyNames.IndexOf("DetailNumber") != -1)
                                            {
                                                aKeyName = sKeyNames.Split('|');

                                                for (i = aKeyName.GetLowerBound(0); i < aKeyName.GetUpperBound(0); i++)
                                                {
                                                    if ((aKeyName[i].ToLower()) == "detailnumber")
                                                    {
                                                        aKeyValue = sKeyValues.Split('|');
                                                        if (i <= aKeyValue.GetUpperBound(0))
                                                        {
                                                            sDetailNumber = aKeyValue[i];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (IsDebug)
                                            objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Saving the Audit Result usinguspAuditResultInsDetail"), "");
                                        hashTable = new Hashtable();
                                        hashTable.Add("@EstimateDocumentID", lngAPDDocumentID);
                                        if (!string.IsNullOrEmpty(sDetailNumber.Trim()))
                                            hashTable.Add("@EstimateDetailNumber", Convert.ToInt32(sDetailNumber));
                                        else
                                            hashTable.Add("@EstimateDetailNumber", DBNull.Value);
                                        hashTable.Add("@AuditRuleName", objRuleFiredlist.GetAttribute("desc"));
                                        hashTable.Add("@AuditDescription", objRuleFiredlist.SelectSingleNode("Action").InnerText);
                                        hashTable.Add("@AppliedWeight", Convert.ToInt32(objRuleFiredlist.GetAttribute("weight")));
                                        hashTable.Add("@UserLastName", strNTUserID);
                                        objCDataAccessor.ExecuteSp(strInsertAuditResultSPName, hashTable, "APD");
                                        // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertAuditResultSPName, strParameters);
                                    }
                                }
                                if (IsDebug)
                                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD_v2", string.Concat("Rule based audit complete."), "");
                            }
                        }
                    }
                }
                // *** Estimate Data Save is now complete ***
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objStyleSheetDom = null;
                objAPDEstimateDataDom = null;
                mobjRootNode = null;
                objNodeList = null;
                objClaimXML = null;
                objAssignmentVehNode = null;
                objAPDEstimateRules = null;
                objRulesOutputXML = null;
                objRulesFiredList = null;
                objRuleFired = null;
                if (objRules != null)
                    Marshal.ReleaseComObject(objRules);
                objRules = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
                //Need to roll back a transaction?
                //if (blnInTrans)
                //    transaction.Rollback();
                objWriteLog = null;
            }

            //Return the transformed XML for debug purposes.
            return strAPDEstimateXML;
        }

        /// <summary>
        ///  "Back-end" utility method to allow attachment of estimate data to an
        ///  existing APD document.  Used to fix up estimates that have failed on insert.
        /// 
        ///  strXmlDoc = Estimate xml document received from calling application
        ///  lngAPDDocumentID = APD Document to attach estimate to.
        /// 
        ///  Returns: the transformed Common Estimate Format (CEF) for debug purposes.
        ///  </summary>
        public string AttachEstimateToAPDDocument(string strXMLDoc, string strTradingPartner, long lngAPDDocumentID)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "AttachEstimateToApdDocument: ");
            XmlDocument xmlTransDocument = null;
            string strReturn = string.Empty;
            try
            {
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                xmlTransDocument = new XmlDocument();

                //Store the received date/time for later.
                objShared.g_varDocumentReceivedDate = DateTime.Now;

                //Put these in global scope for access by all procedures,
                //including "InitalizeGlobals" which does some initial work with it.
                objShared.g_strPassedDocument = strXMLDoc;
                objShared.g_strPartner = strTradingPartner;

                objMLAPDPartnerDataMgr.InitializeGlobals();

                if (lngAPDDocumentID == 0)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.AttachEstimateToAPDDocument", "Passed APD Document ID was zero."));

                if (objShared.g_strPartner.Trim().Length < 0)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.AttachEstimateToAPDDocument", "No Trading Partner value received"));

                if (objShared.g_strPassedDocument.Trim().Length < 0)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.AttachEstimateToAPDDocument", "A blank Xml Document was received"));

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.AttachEstimateToAPDDocument: Issue while loading the transaction xml.");

                //Get a little partner string from the config, i.e. "SG".
                objShared.g_strPartnerSmallID = objMLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/@SmallID" }));

                strReturn = TransferDataToAPD(xmlTransDocument, lngAPDDocumentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Now call the method below to do all the real work.
            return strReturn;
        }

        /// <summary>
        ///  Handles the insertion of the estimate data in APD.
        ///  
        ///  objEstimateDataDom   - Reference to a DOM document containing the Estimate Data XML
        ///  lngApdDocumentID  - ID of related document in APD Database.
        ///                       Updated value is stored here once completed.
        /// 
        ///  Returns: the transformed Common Estimate Format (CEF) for debug purposes.
        ///  </summary>
        public string TransferDataToAPD(XmlDocument objEstimateDataDom, long lngAPDDocumentID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferDataToApd: " });
            //Objects that need clean up.

            XmlDocument objStyleSheetDom = null;   //Holds loaded stylesheet
            XmlDocument objAPDEstimateDataDom = null;  // Holds transformed "APD" formatted estimate data
            XmlNodeList objNodeList = null;      // Working Nodelist for data extraction
            XmlDocument objClaimXML = null;     //This hold the claim information for the given assignment id\
            XmlElement objAssignmentVehNode = null;   // Holds vehicle node in the claim information XML           
            XmlDocument objAPDEstimateRules = null;   // APD Estimate Rules
            XmlDocument objRulesOutputXML = null;   //Rules Engine output XML object
            XmlNodeList objRulesFiredList = null;      // The list of rules fired
            XmlElement objRuleFired = null;    // Rule Fired item
            COMRules.CRulesEngine objRules = null;
            //Primitives that dont.
            string strAction = string.Empty,   // Specific area of proc being executed (used for error tracing)
                strAPDEstimateXML = string.Empty, // String representation of transformed Estimate Data
                strTaxTypeCD = string.Empty;    //Translated Database insertion value for TaxTypeCD in Summary          

            int intTaxableFlag = 0;      // Transalated Database insertion value for Taxable flag In Summary
            object varResults = null;   // and store
            string strYear = string.Empty;
            string strMake = string.Empty;
            string strModel = string.Empty;
            bool blnInTrans = false;
            string strAssignmentID = string.Empty;
            string strRulesOutput = string.Empty;
            int iAuditWeight = 0;
            int i = 0;
            string strInsertSummarySPName = string.Empty;
            string strInsertDetailSPName = string.Empty;
            string strInsertAuditResultSPName = string.Empty;
            string strNTUserID = null;
            string strVin = string.Empty;
            bool blnDAAssignment = false;
            string sDetailNumber = string.Empty,
                                              sKeyNames = string.Empty,
                                              sKeyValues = string.Empty, strImpactArea = string.Empty;
            string[] aKeyName = null,
                aKeyValue = null;
            string strappend = string.Empty;
            object objAppend = null;
            int intRepairDays = 0;
            Hashtable hashTable = null;
            string strVehicleYear = string.Empty;
            string strRepairDays = string.Empty;
            string strDetailparam = string.Empty;
            string strEstimateSummary = string.Empty;
            int intVehicleYear = 0;
            //SqlConnection sqlConnection = null;
            //SqlTransaction transaction = null;
            Hashtable htVINDetails = null;
            NadaWrapper objNadaWS = null;
            UInt64 errorCode;
            Common.WriteLog objWriteLog = new WriteLog();
            CEvents mobjCEvents = null;
            try
            {

                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMdomUtilis = new MDomUtils();
                objCDataAccessor = new DataAccess.CDataAccessor();
                objNadaWS = new NadaWrapper();
                mobjCEvents = new CEvents();

                if (objEstimateDataDom == null)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.TransferDataToAPD", " Passed XML DOM was Nothing."));

                if (lngAPDDocumentID == 0)
                    throw new Exception(string.Concat("PartnerDataMgr.CPartnerXfer.TransferDataToAPD", " Passed APD Document ID was zero."));

                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("TransferDataToAPD Started."), "");
                //***  Transform the Partner Data's XML into the "standard" APD format  ***
                strAction = "Transforming Partner Estimate Data";

                //Do the transfer.  Note that each partner has its own XSL.
                //objAppend = MLAPDPartnerDataMgr.g_objEvents;
                objAPDEstimateDataDom = objMdomUtilis.TransformDomAsDom(ref objEstimateDataDom, (string.Concat(objShared.g_strSupportDocPath, "//", objShared.g_strPartnerSmallID, "EstimateXfer.xsl")));

                strAPDEstimateXML = objAPDEstimateDataDom.OuterXml;
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat(" Results from Transform: ", Environment.NewLine, strAPDEstimateXML), "");
                //***  The transformed XML is now awaiting processing in objAPDEstimateDataDom  ***

                mobjRootNode = objAPDEstimateDataDom.DocumentElement;

                //Get the assignment id from the APDEstimate. We will use this later to obtain the claim information and
                // use it for estimate audit
                strAssignmentID = RootChild("Header/@AssignmentID");

                strNTUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/NTUserID");

                //sqlConnection = new SqlConnection(objCDataAccessor.GetConnectionString("APD"));
                //sqlConnection.Open();

                //transaction = sqlConnection.BeginTransaction();
                blnInTrans = true;   //Flag new transaction state.

                // Insert the estimate header information
                strAction = "Inserting APD Estimate Header";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat(strAction), "");
                // Compile parameter information into a variant array.  We have to do it this way or we will break VB's
                // line continuation limit.              
                hashTable = new Hashtable();
                hashTable.Add("@DocumentID", lngAPDDocumentID);
                hashTable.Add("@AppraiserLicenseNumber", RootChild("Header/Appraiser/@AppraiserLicenseNumber"));
                hashTable.Add("@AppraiserName", RootChild("Header/Appraiser/@AppraiserName"));
                hashTable.Add("@ClaimantName", RootChild("Header/@ClaimantName"));
                hashTable.Add("@ClaimNumber", RootChild("Header/@ClaimNumber"));
                hashTable.Add("@CompanyAddress", RootChild("Header/Estimator/@CompanyAddress"));
                hashTable.Add("@CompanyAreaCode", RootChild("Header/Estimator/@CompanyAreaCode"));
                hashTable.Add("@CompanyCity", RootChild("Header/Estimator/@CompanyCity"));
                hashTable.Add("@CompanyExchangeNumber", RootChild("Header/Estimator/@CompanyExchangeNumber"));
                hashTable.Add("@CompanyName", RootChild("Header/Estimator/@CompanyName"));
                hashTable.Add("@CompanyState", RootChild("Header/Estimator/@CompanyState"));
                hashTable.Add("@CompanyUnitNumber", RootChild("Header/Estimator/@CompanyUnitNumber"));
                hashTable.Add("@CompanyZip", RootChild("Header/Estimator/@CompanyZip"));

                // Detail Editable?
                // In an effort to protect the user from using bad data which we may receive from our VAN, a flag is used to indicate
                // whether the detail is editable or not.  If the data passes our audit tests, we set this flag to yes, allowing the
                // user to access the data.  If it doesn't, this flag is turned off to prevent write access.  In order to pass this test,
                //there must be no variance and all stylesheet audits must have passed.

                if (RootChild("Summary/LineItem[@Type='Variance']/@ExtendedAmount") != "0")
                {
                    errorCode = Convert.ToUInt64(ErrorCodes.eEstimateDataAuditFailed);
                    mobjCEvents.HandleEvent((int)errorCode, string.Concat(PROC_NAME, "Estimate Audit"),
                         "Estimate Audit Failed: There must be no variance.",
                        string.Concat("Partner document = ", Environment.NewLine, objShared.g_strPassedDocument, Environment.NewLine, Environment.NewLine,
                         "After APD Transform = ", Environment.NewLine, strAPDEstimateXML), Environment.NewLine, false);

                }
                else if (mobjRootNode.SelectNodes("Audit/AuditItem[@Passed='No']").Count > 0)
                {
                    errorCode = Convert.ToUInt64(ErrorCodes.eEstimateDataAuditFailed);
                    mobjCEvents.HandleEvent((int)errorCode, string.Concat(PROC_NAME, "Estimate Audit"), "Estimate Audit Failed: A stylesheet audit has failed.",
                        string.Concat("Partner document = ", Environment.NewLine, objShared.g_strPassedDocument, Environment.NewLine, Environment.NewLine,
                         "After APD Transform = ", Environment.NewLine, strAPDEstimateXML), Environment.NewLine, false);
                }
                else
                    hashTable.Add("@DetailEditableFlag", 1);

                hashTable.Add("@InspectionAddress", RootChild("Header/Inspection/@InspectionAddress"));
                hashTable.Add("@InspectionAreaCode", RootChild("Header/Inspection/@InspectionAreaCode"));
                hashTable.Add("@InspectionCity", RootChild("Header/Inspection/@InspectionCity"));
                hashTable.Add("@InspectionExchangeNumber", RootChild("Header/Inspection/@InspectionExchangeNumber"));
                hashTable.Add("@InspectionLocation", RootChild("Header/Inspection/@InspectionLocation"));
                hashTable.Add("@InspectionState", RootChild("Header/Inspection/@InspectionState"));
                hashTable.Add("@InspectionType", RootChild("Header/Inspection/@InspectionType"));
                hashTable.Add("@InspectionUnitNumber", RootChild("Header/Inspection/@InspectionUnitNumber"));
                hashTable.Add("@InspectionZip", RootChild("Header/Inspection/@InspectionZip"));
                hashTable.Add("@InsuranceCompanyName", RootChild("Header/@InsuranceCompanyName"));
                hashTable.Add("@InsuredName", RootChild("Header/@InsuredName"));
                if ((RootChild("Header/@LossDate")).Length == 0)
                    hashTable.Add("@LossDate", DBNull.Value);
                else
                    hashTable.Add("@LossDate", RootChild("Header/@LossDate"));
                //hashTable.Add("@LossDate", ((RootChild("Header/@LossDate")).Length == 0 ? DBNull.Value.ToString() : RootChild("Header/@LossDate")));
                hashTable.Add("@LossType", RootChild("Header/@LossType"));
                hashTable.Add("@OwnerAddress", RootChild("Header/Owner/@OwnerAddress"));
                hashTable.Add("@OwnerCity", RootChild("Header/Owner/@OwnerCity"));
                hashTable.Add("@OwnerDayAreaCode", RootChild("Header/Owner/@OwnerDayAreaCode"));
                hashTable.Add("@OwnerDayExchangeNumber", RootChild("Header/Owner/@OwnerDayExchangeNumber"));
                hashTable.Add("@OwnerDayExtensionNumber", RootChild("Header/Owner/@OwnerDayExtensionNumber"));
                hashTable.Add("@OwnerDayUnitNumber", RootChild("Header/Owner/@OwnerDayUnitNumber"));
                hashTable.Add("@OwnerEveningAreaCode", RootChild("Header/Owner/@OwnerEveningAreaCode"));
                hashTable.Add("@OwnerEveningExchangeNumber", RootChild("Header/Owner/@OwnerEveningExchangeNumber"));
                hashTable.Add("@OwnerEveningExtensionNumber", RootChild("Header/Owner/@OwnerEveningExtensionNumber"));
                hashTable.Add("@OwnerEveningUnitNumber", RootChild("Header/Owner/@OwnerEveningUnitNumber"));
                hashTable.Add("@OwnerName", RootChild("Header/Owner/@OwnerName"));
                hashTable.Add("@OwnerState", RootChild("Header/Owner/@OwnerState"));
                hashTable.Add("@OwnerZip", RootChild("Header/Owner/@OwnerZip"));
                hashTable.Add("@PolicyNumber", RootChild("Header/@PolicyNumber"));
                hashTable.Add("@Remarks", RootChild("Header/@Remarks"));

                strRepairDays = RootChild("Header/@RepairDays");
                if (strRepairDays != string.Empty)
                    intRepairDays = Convert.ToInt32(strRepairDays.Length == 0 ? 0 : Convert.ToInt32(strRepairDays));

                hashTable.Add("@RepairDays", intRepairDays);
                hashTable.Add("@ShopAddress", RootChild("Header/Shop/@ShopAddress"));
                hashTable.Add("@ShopAreaCode", RootChild("Header/Shop/@ShopAreaCode"));
                hashTable.Add("@ShopCity", RootChild("Header/Shop/@ShopCity"));
                hashTable.Add("@ShopExchangeNumber", RootChild("Header/Shop/@ShopExchangeNumber"));
                hashTable.Add("@ShopName", RootChild("Header/Shop/@ShopName"));
                hashTable.Add("@ShopRegistrationNumber", RootChild("Header/Shop/@ShopRegistrationNumber"));
                hashTable.Add("@ShopState", RootChild("Header/Shop/@ShopState"));
                hashTable.Add("@ShopUnitNumber", RootChild("Header/Shop/@ShopUnitNumber"));
                hashTable.Add("@ShopZip", RootChild("Header/Shop/@ShopZip"));
                hashTable.Add("@VehicleBodyStyle", RootChild("Header/Vehicle/@VehicleBodyStyle"));
                hashTable.Add("@VehicleColor", RootChild("Header/Vehicle/@VehicleColor"));
                hashTable.Add("@VehicleEngineType", RootChild("Header/Vehicle/@VehicleEngineType"));

                // Impact Points - These are made up of 0 or more elements.  the Values of these elements need to be
                // concatenated together.
                strImpactArea = RootChild("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='Yes']/@ImpactArea");
                // Enumerate any additional impact points
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='No']");

                mobjCurNode = null;
                foreach (XmlNode objImpactNode in objNodeList)
                {
                    mobjCurNode = objImpactNode;
                    strImpactArea = string.Concat(strImpactArea, ", ", CurChild("@ImpactArea"));
                }
                hashTable.Add("@VehicleImpactPoint", strImpactArea);
                hashTable.Add("@VehicleMake", RootChild("Header/Vehicle/@VehicleMake"));
                if ((RootChild("Header/Vehicle/@VehicleMileage").Length == 0))
                    hashTable.Add("@VehicleMileage", DBNull.Value);
                else
                    hashTable.Add("@VehicleMileage", RootChild("Header/Vehicle/@VehicleMileage"));
                // hashTable.Add("@VehicleMileage", (RootChild("Header/Vehicle/@VehicleMileage")).Length == 0 ? DBNull.Value.ToString() : RootChild("Header/Vehicle/@VehicleMileage"));
                hashTable.Add("@VehicleModel", RootChild("Header/Vehicle/@VehicleModel"));

                // Options - Like impact points, there may be 0 or more elements.  These also must be concatenated together
                //hashTable["@VehicleOptions"] = "'";
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//VehicleOption");
                if (objNodeList.Count > 0)
                {
                    mobjCurNode = null;
                    foreach (XmlNode objCurNode in objNodeList)
                    {
                        mobjCurNode = objCurNode;
                        hashTable["@VehicleOptions"] = string.Concat(hashTable["@VehicleOptions"], CurChild("@Option"), ", ");
                    }
                }
                else
                    hashTable["@VehicleOptions"] = string.Empty;

                if ((RootChild("Header/Vehicle/@VehicleYear")).Length == 0)
                    hashTable.Add("@VehicleYear", DBNull.Value);
                else
                    hashTable.Add("@VehicleYear", RootChild("Header/Vehicle/@VehicleYear"));
                //  strVehicleYear = ((RootChild("Header/Vehicle/@VehicleYear")).Length == 0 ? DBNull.Value.ToString() : RootChild("Header/Vehicle/@VehicleYear"));
                //  intVehicleYear = Convert.ToInt32(strVehicleYear);
                //hashTable.Add("@VehicleYear", intVehicleYear);
                hashTable.Add("@Vin", RootChild("Header/Vehicle/@Vin"));
                if ((RootChild("Header/@WrittenDate")).Length == 0)
                    hashTable.Add("@WrittenDate", DBNull.Value);
                else
                    hashTable.Add("@WrittenDate", RootChild("Header/@WrittenDate"));
                // hashTable.Add("@WrittenDate", ((RootChild("Header/@WrittenDate")).Length < 6 ? DBNull.Value.ToString() : RootChild("Header/@WrittenDate")));
                hashTable.Add("@UserID", 0);
                hashTable.Add("@UserName", "DEFAULT");
                hashTable.Add("@SysLastUpdatedDate", "DEFAULT");

                // OK, time to insert the estimate header record
                objCDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), hashTable, "APD");
                //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), hashTable);
                // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), strParameters);

                // Insert Estimate Detail Records
                strAction = "Inserting APD Estimate Details";

                // We'll need to use the parameters array again, reset it first to 0 to clear it, then back to the
                // length we need
                strInsertDetailSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertDetailSPName");
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat(strAction), "");
                // Get a list of detail lines for for details.
                objNodeList = mobjRootNode.SelectNodes("Detail/DetailLine");
                mobjCurNode = null;
                foreach (XmlNode objCurNode in objNodeList)
                {
                    mobjCurNode = objCurNode;
                    hashTable = new Hashtable();
                    // Set Parameter array values
                    hashTable.Add("@DocumentID", lngAPDDocumentID);

                    strDetailparam = CurChild("@DetailNumber");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DetailNumber", Convert.ToInt16(strDetailparam));
                    else
                        hashTable.Add("@DetailNumber", 0);

                    switch (CurChild("Part/@APDPartCategory"))
                    {
                        case "TaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PAT");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PAT");
                            break;
                        case "NonTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PAN");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PAN");
                            break;
                        case "OtherTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "POT");
                            hashTable.Add("@APDPartCategoryOriginalCD", "POT");
                            break;
                        case "OtherNonTaxableParts":
                            hashTable.Add("@APDPartCategoryAgreedCD", "PON");
                            hashTable.Add("@APDPartCategoryOriginalCD", "PON");
                            break;
                        case "Storage":
                            hashTable.Add("@APDPartCategoryAgreedCD", "STG");
                            hashTable.Add("@APDPartCategoryOriginalCD", "STG");
                            break;
                        case "Sublet":
                            hashTable.Add("@APDPartCategoryAgreedCD", "SUB");
                            hashTable.Add("@APDPartCategoryOriginalCD", "SUB");
                            break;
                        case "Towing":
                            hashTable.Add("@APDPartCategoryAgreedCD", "TOW");
                            hashTable.Add("@APDPartCategoryOriginalCD", "TOW");
                            break;
                        case "Other":
                            hashTable.Add("@APDPartCategoryAgreedCD", "OTH");
                            hashTable.Add("@APDPartCategoryOriginalCD", "OTH");
                            break;
                        default:
                            hashTable.Add("@APDPartCategoryAgreedCD", "");
                            hashTable.Add("@APDPartCategoryOriginalCD", "");
                            break;
                    }

                    hashTable.Add("@Comment", CurChild("@Comment"));

                    strDetailparam = CurChild("Labor[@LaborType!='Refinish']/@DBLaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBLaborHoursOriginal", 0);

                    strDetailparam = CurChild("Part/@DBPartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBPriceOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBPriceOriginal", 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@DBLaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@DBRefinishLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@DBRefinishLaborHoursOriginal", 0);

                    hashTable.Add("@Description", CurChild("@Description"));


                    strDetailparam = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@LaborHoursAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@LaborHoursAgreed", 0);

                    hashTable.Add("@LaborHoursChangedFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@LaborHoursIncludedAgreedFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@LaborHoursIncludedOriginalFlag", CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);

                    if (strDetailparam != string.Empty)
                        hashTable.Add("@LaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@LaborHoursOriginal", 0);

                    switch (CurChild("Labor[@LaborType!='Refinish']/@LaborType"))
                    {
                        case "Body":
                            hashTable.Add("@LaborTypeAgreedCD", "B");
                            hashTable.Add("@LaborTypeOriginalCD", "B");
                            break;
                        case "Frame":
                            hashTable.Add("@LaborTypeAgreedCD", "F");
                            hashTable.Add("@LaborTypeOriginalCD", "F");
                            break;
                        case "Mechanical":
                            hashTable.Add("@LaborTypeAgreedCD", "M");
                            hashTable.Add("@LaborTypeOriginalCD", "M");
                            break;
                        default:
                            hashTable.Add("@LaborTypeAgreedCD", "");
                            hashTable.Add("@LaborTypeOriginalCD", "");
                            break;
                    }

                    hashTable.Add("@LynxComment", "DEFAULT");
                    hashTable.Add("@ManualLineFlag", CurChild("@ManualLineFlag") == "Yes" ? 1 : 0);
                    // strParameters[(int)EEstimateDetailUpdDetail.eLynxComment] = "DEFAULT";
                    //strParameters[(int)EEstimateDetailUpdDetail.eManualLineFlag] = (CurChild("@ManualLineFlag") == "Yes" ? 1 : 0).ToString();

                    switch (CurChild("@OperationCode"))
                    {
                        case "":
                            hashTable.Add("@OperationCD", "");
                            break;
                        case "Alignment":
                            hashTable.Add("@OperationCD", "ALGN");
                            break;
                        case "Comment":
                            hashTable.Add("@OperationCD", "COM");
                            break;
                        case "Other":
                            hashTable.Add("@OperationCD", "OTH");
                            break;
                        case "Refinish":
                            hashTable.Add("@OperationCD", "REFN");
                            break;
                        case "Remove/Install":
                            hashTable.Add("@OperationCD", "R&I");
                            break;
                        case "Repair":
                            hashTable.Add("@OperationCD", "REPR");
                            break;
                        case "Replace":
                            hashTable.Add("@OperationCD", "REPL");
                            break;
                        case "Sublet":
                            hashTable.Add("@OperationCD", "SUB");
                            break;
                        default:
                            hashTable.Add("@OperationCD", "!ND!");
                            break;
                    }

                    switch (CurChild("Part/@PartType"))
                    {
                        case "Aftermarket":
                            hashTable.Add("@PartTypeAgreedCD", "AFMK");
                            hashTable.Add("@PartTypeOriginalCD", "AFMK");
                            break;
                        case "Glass":
                            hashTable.Add("@PartTypeAgreedCD", "GLS");
                            hashTable.Add("@PartTypeOriginalCD", "GLS");
                            break;
                        case "LKQ":
                            hashTable.Add("@PartTypeAgreedCD", "LKQ");
                            hashTable.Add("@PartTypeOriginalCD", "LKQ");
                            break;
                        case "New":
                            hashTable.Add("@PartTypeAgreedCD", "NEW");
                            hashTable.Add("@PartTypeOriginalCD", "NEW");
                            break;
                        case "Other":
                            hashTable.Add("@PartTypeAgreedCD", "OTH");
                            hashTable.Add("@PartTypeOriginalCD", "OTH");
                            break;
                        case "Reconditioned":
                            hashTable.Add("@PartTypeAgreedCD", "REC");
                            hashTable.Add("@PartTypeOriginalCD", "REC");
                            break;
                        case "Sublet":
                            hashTable.Add("@PartTypeAgreedCD", "SUB");
                            hashTable.Add("@PartTypeOriginalCD", "SUB");
                            break;
                        default:
                            hashTable.Add("@PartTypeAgreedCD", "");
                            hashTable.Add("@PartTypeOriginalCD", "");
                            break;
                    }

                    strDetailparam = CurChild("Part/@PartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@PriceAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@PriceAgreed", 0);

                    hashTable.Add("@PriceChangedFlag", CurChild("Part/@PriceChangedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@PriceIncludedAgreedFlag", CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@PriceIncludedOriginalFlag", CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0);

                    strDetailparam = CurChild("Part/@PartPrice");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@PriceOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@PriceOriginal", 0);

                    strDetailparam = CurChild("Part/@Quantity");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@QuantityAgreed", Convert.ToInt32(strDetailparam));
                    else
                        hashTable.Add("@QuantityAgreed", 0);

                    strDetailparam = CurChild("Part/@Quantity");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@QuantityOriginal", Convert.ToInt32(strDetailparam));
                    else
                        hashTable.Add("@QuantityOriginal", 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@RefinishLaborHoursAgreed", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@RefinishLaborHoursAgreed", 0);

                    hashTable.Add("@RefinishLaborHoursChangedFlag", CurChild("Labor[@LaborType='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0);

                    strDetailparam = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    if (strDetailparam != string.Empty)
                        hashTable.Add("@RefinishLaborHoursOriginal", Convert.ToDecimal(strDetailparam));
                    else
                        hashTable.Add("@RefinishLaborHoursOriginal", 0);

                    hashTable.Add("@RefinishLaborIncludedAgreedFlag", CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@RefinishLaborIncludedOriginalFlag", CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0);
                    hashTable.Add("@UserID", 0);
                    hashTable.Add("@UserName", strNTUserID);
                    hashTable.Add("@SysLastUpdatedDate", "DEFAULT");

                    // Insert the record
                    objCDataAccessor.ExecuteSp(strInsertDetailSPName, hashTable, "APD");
                    //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, strInsertDetailSPName, hashTable);
                }
                // Insert Estimate Summary records
                // Estimate Summary records are inserted as individual line items (ie.  Parts Total, Refinish Labor Total,
                // etc.)  We'll need to retrieve each of these out of the XML and insert them 1 at a time into summary
                strAction = "Inserting APD Estimate Summary";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat(strAction), "");
                // Get a list of line items for the summary.
                objNodeList = mobjRootNode.SelectNodes("Summary/LineItem[@TransferLine='Yes']");

                strInsertSummarySPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertSummarySPName");
                mobjCurNode = null;
                foreach (XmlNode mobjCurNod in objNodeList)
                {
                    mobjCurNode = mobjCurNod;
                    // Have to set the Database insertion values
                    intTaxableFlag = (CurChild("@Taxable") == "Yes" ? 1 : 0);
                    switch (CurChild("@TaxBase"))
                    {
                        case "Parts":
                            strTaxTypeCD = "PT";
                            break;
                        case "Materials":
                            strTaxTypeCD = "SP";
                            break;
                        case "PartsAndMaterials":
                            strTaxTypeCD = "MT";
                            break;
                        case "Labor":
                            strTaxTypeCD = "LB";
                            break;
                        case "AdditionalCharges":
                            strTaxTypeCD = "AC";
                            break;
                        case "Subtotal":
                            strTaxTypeCD = "ST";
                            break;
                        default:
                            strTaxTypeCD = "OT";
                            break;
                    }

                    hashTable = new Hashtable();
                    hashTable.Add("@EstimateSummaryID", 0);
                    hashTable.Add("@EstimateSummaryTypeID", null);
                    hashTable.Add("@EstimateSummaryType", CurChild("@Type"));
                    hashTable.Add("@EstimateSummaryTypeCD", CurChild("@Category"));
                    hashTable.Add("@DocumentID", lngAPDDocumentID);

                    strEstimateSummary = CurChild("@ExtendedAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedExtendedAmt", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedExtendedAmt", 0);

                    strEstimateSummary = CurChild("@Hours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedHrs", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedHrs", 0);

                    strEstimateSummary = CurChild("@Percent");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedPct", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedPct", 0);

                    hashTable.Add("@AgreedTaxableFlag", intTaxableFlag);

                    strEstimateSummary = CurChild("@UnitAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@AgreedUnitAmt", strEstimateSummary);
                    else
                        hashTable.Add("@AgreedUnitAmt", 0);

                    hashTable.Add("@Comments", CurChild("@Note"));

                    strEstimateSummary = CurChild("@ExtendedAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalExtendedAmt", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalExtendedAmt", 0);

                    strEstimateSummary = CurChild("@Hours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalHrs", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalHrs", 0);

                    strEstimateSummary = CurChild("@Percent");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalPct", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalPct", 0);

                    hashTable.Add("@OriginalTaxableFlag", intTaxableFlag);

                    strEstimateSummary = CurChild("@UnitAmount");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalUnitAmt", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalUnitAmt", 0);

                    hashTable.Add("@TaxTypeCD", strTaxTypeCD);
                    hashTable.Add("@UserID", 0);
                    hashTable.Add("@UserName", strNTUserID);
                    hashTable.Add("@SysLastUpdatedDate", "DEFAULT");
                    hashTable.Add("@ApplicationCD", "APD");
                    hashTable.Add("@ReturnSysLastUpdate", 1);

                    strEstimateSummary = CurChild("@RepairHours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalRepairHours", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalRepairHours", 0);


                    strEstimateSummary = CurChild("@ReplaceHours");
                    if (strEstimateSummary != string.Empty)
                        hashTable.Add("@OriginalReplaceHours", strEstimateSummary);
                    else
                        hashTable.Add("@OriginalReplaceHours", 0);

                    // Insert the record
                    objCDataAccessor.ExecuteSp(strInsertSummarySPName, hashTable, "APD");
                    //objCDataAccessor.ExecuteSP(ref sqlConnection, ref transaction, strInsertSummarySPName, hashTable);
                }
                // Insert Estimate Audit record
                // The Estimate's audit record will contain data not already saved off in the estimate records in APD.
                // It contains data used by APD to specifically audit the estimate.
                strAction = "Inserting APD Estimate Audit Record";
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat(strAction), "");

                //We changed the commit transaction before the NADA service call
                objWriteLog.LogEvent("COM Conversion", "TRANSACTION", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Transaction going to commit for DocumentID : ", lngAPDDocumentID), "");
                //try
                //{
                //    if (transaction.Connection.State == System.Data.ConnectionState.Open)
                //    {
                //        transaction.Commit();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    blnInTrans = false;
                //    objWriteLog.LogEvent("COM Conversion", "TRANSACTION ERROR", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Transaction commited for DocumentID : ", lngAPDDocumentID), string.Concat("Exception: ", ex.Message, ex.InnerException));
                //}
                blnInTrans = false; //Flag new transaction state.
                objWriteLog.LogEvent("COM Conversion", "TRANSACTION", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Transaction commited for DocumentID : ", lngAPDDocumentID), "");
                strVin = RootChild("Header/Vehicle/@Vin");

                //Do a VIN lookup if the VIN is not blank.
                if (strVin != string.Empty)
                {
                    //We need to run the VIN through NADA first.  We're not going to make the determination whether or not the
                    // VIN is valid here.  We're simply going to retrieve the info for determination later.
                    // objNada = objMLAPDPartnerDataMgr.CreateObjectEx("NadaWrapper.CNadaWrapper");               
                    htVINDetails = objNadaWS.GetVIN(strVin);
                    if (IsDebug)
                        objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Calling VINLookup functionality for getting the Vehicle Year, Make and Model."), "");
                    //Strip out single quotes from results.
                    //Believe it or not, there are some bogus single quotes in the NADA database.
                    if (htVINDetails != null)
                    {
                        if (htVINDetails.ContainsKey("Year"))
                            strYear = (string)htVINDetails["Year"];
                        if (htVINDetails.ContainsKey("Make"))
                            strMake = (string)htVINDetails["Make"];
                        if (htVINDetails.ContainsKey("Model"))
                            strModel = (string)htVINDetails["Model"];
                    }

                    //objNada = null;
                }
                //Commit what we have in the transaction so far.  As per Test Track item 1430,
                //the estimate audit which follows does not need to be included in the transaction,
                // MLAPDPartnerDataMgr.g_objDataAccessor.CommitTransaction();
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Transaction Committed Successfully."), "");

                //New Estimate Auditing process
                if (strAssignmentID != string.Empty)
                {
                    if (strAssignmentID.Length > 0)
                    {
                        //get the claim detail for the assignment
                        hashTable = new Hashtable();
                        hashTable.Add("@AssignmentID", Convert.ToInt64(strAssignmentID));

                        blnDAAssignment = Convert.ToBoolean(objCDataAccessor.ExecuteSpNamedParams(objMLAPDPartnerDataMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), hashTable, "APD"));

                        if (!blnDAAssignment)
                        {
                            //Auditing needs to be done for program shop estimates only.                          
                            objClaimXML = new XmlDocument();

                            hashTable = new Hashtable();
                            hashTable.Add("@AssignmentID", Convert.ToInt64(strAssignmentID));

                            strappend = objCDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), hashTable, "APD");
                            // strappend = MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), new object[] { "@AssignmentID", strAssignmentID });
                            objMLAPDPartnerDataMgr.LoadXml(ref objClaimXML, ref strappend, PROC_NAME, "TransferDataToAPD");

                            //get the vehicle node so we can add the nada values
                            objAssignmentVehNode = (XmlElement)objClaimXML.SelectSingleNode("/Root/Vehicle");

                            if (objAssignmentVehNode != null)
                            {
                                objAssignmentVehNode.SetAttribute("VehicleYearNADA", strYear);
                                objAssignmentVehNode.SetAttribute("VehicleMakeNADA", strMake);
                                objAssignmentVehNode.SetAttribute("VehicleModelNADA", strModel);
                            }

                            //load the APD Estimate audit rules
                            objAPDEstimateRules = new XmlDocument();
                            strappend = string.Concat(objShared.g_strSupportDocPath, "//", "APDEstimateRules.xml");
                            objMdomUtilis.LoadXmlFile(ref objAPDEstimateRules, ref strappend, PROC_NAME, "TransferDataToAPD");
                            objWriteLog.LogEvent("COM Conversion", "TRANSACTION", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Process for Rules Engine COM ", lngAPDDocumentID), "");
                            //create the Rules Engine object
                            objRules = new COMRules.CRulesEngine();
                            strRulesOutput = objRules.EngineExecute(objAPDEstimateDataDom.OuterXml,
                                                objAPDEstimateRules.OuterXml,
                                                objClaimXML.OuterXml);
                            if (IsDebug)
                                objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("XML Returned", strRulesOutput), "");
                            if (strRulesOutput != string.Empty)
                            {
                                objRulesOutputXML = new XmlDocument();
                                objMLAPDPartnerDataMgr.LoadXml(ref objRulesOutputXML, ref strRulesOutput, PROC_NAME, "TransferDataToAPD");

                                //get the audit weight
                                iAuditWeight = Convert.ToInt32(objRulesOutputXML.SelectSingleNode("/Root/@auditWeight").InnerText);
                                if (IsDebug)
                                    objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Audit Weight -", iAuditWeight), "");
                                hashTable = new Hashtable();
                                hashTable.Add("@DocumentID", lngAPDDocumentID);
                                hashTable.Add("@AuditWeight", iAuditWeight);
                                //update the document's audit weight
                                objCDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), hashTable, "APD");
                                // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), strParameters);
                                //now log the rules fired
                                objRulesFiredList = objRulesOutputXML.SelectNodes("/Root/RulesFired/Rule");

                                if (objRulesFiredList != null)
                                {
                                    strInsertAuditResultSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertAuditResultSPName");

                                    foreach (XmlElement objRuleFiredlist in objRulesFiredList)
                                    {
                                        //insert the rule fired into the audit results table
                                        sDetailNumber = string.Empty;

                                        if (objRuleFiredlist.GetAttribute("recursive") == "Y")
                                        {
                                            //get the detail number from the key values
                                            sKeyNames = objRuleFiredlist.GetAttribute("keyName");
                                            sKeyValues = objRuleFiredlist.GetAttribute("keyValue");

                                            if (sKeyNames.IndexOf("DetailNumber") != -1)
                                            {
                                                aKeyName = sKeyNames.Split('|');

                                                for (i = aKeyName.GetLowerBound(0); i < aKeyName.GetUpperBound(0); i++)
                                                {
                                                    if ((aKeyName[i].ToLower()) == "detailnumber")
                                                    {
                                                        aKeyValue = sKeyValues.Split('|');
                                                        if (i <= aKeyValue.GetUpperBound(0))
                                                        {
                                                            sDetailNumber = aKeyValue[i];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (IsDebug)
                                            objWriteLog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Saving the Audit Result usinguspAuditResultInsDetail"), "");
                                        hashTable = new Hashtable();
                                        hashTable.Add("@EstimateDocumentID", lngAPDDocumentID);
                                        if (!string.IsNullOrEmpty(sDetailNumber.Trim()))
                                            hashTable.Add("@EstimateDetailNumber", Convert.ToInt32(sDetailNumber));
                                        else
                                            hashTable.Add("@EstimateDetailNumber", DBNull.Value);
                                        hashTable.Add("@AuditRuleName", objRuleFiredlist.GetAttribute("desc"));
                                        hashTable.Add("@AuditDescription", objRuleFiredlist.SelectSingleNode("Action").InnerText);
                                        hashTable.Add("@AppliedWeight", Convert.ToInt32(objRuleFiredlist.GetAttribute("weight")));
                                        hashTable.Add("@UserLastName", strNTUserID);
                                        objCDataAccessor.ExecuteSp(strInsertAuditResultSPName, hashTable, "APD");
                                        // MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertAuditResultSPName, strParameters);
                                    }
                                }
                                if (IsDebug)
                                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("TransferDataToAPD Completed."), "");
                            }
                        }
                    }
                }
                // *** Estimate Data Save is now complete ***
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartnerDataXfer.cs - TransferDataToAPD", string.Concat("Exception: ", ex.Message, ex.InnerException), "");
                throw ex;
            }
            finally
            {
                objStyleSheetDom = null;
                objAPDEstimateDataDom = null;
                mobjRootNode = null;
                objNodeList = null;
                objClaimXML = null;
                objAssignmentVehNode = null;
                objAPDEstimateRules = null;
                objRulesOutputXML = null;
                objRulesFiredList = null;
                objRuleFired = null;
                if (objRules != null)
                    Marshal.ReleaseComObject(objRules);
                objRules = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
                //Need to roll back a transaction?
                //if (blnInTrans)
                //    transaction.Rollback();
                objWriteLog = null;
            }
            //Return the transformed XML for debug purposes.
            return strAPDEstimateXML;
        }


        #endregion

        #region Private Helper Functions

        private string RootChild(string strPath)
        {
            string strFormatpath = string.Empty;
            try
            {
                objMEscapeUtilities = new MEscapeUtilities();
                if (mobjRootNode != null)
                {
                    if (mobjRootNode.SelectSingleNode(strPath) != null)
                        strFormatpath = mobjRootNode.SelectSingleNode(strPath).InnerText;
                }

                return objMEscapeUtilities.SQLQueryString(ref strFormatpath);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string CurChild(string strPath)
        {
            string strFormatpath = string.Empty;
            try
            {
                objMEscapeUtilities = new MEscapeUtilities();
                if (mobjCurNode != null)
                {
                    if (mobjCurNode.SelectSingleNode(strPath) != null)
                        strFormatpath = mobjCurNode.SelectSingleNode(strPath).InnerText;
                }

                return objMEscapeUtilities.SQLQueryString(ref strFormatpath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        #endregion

    }
}
