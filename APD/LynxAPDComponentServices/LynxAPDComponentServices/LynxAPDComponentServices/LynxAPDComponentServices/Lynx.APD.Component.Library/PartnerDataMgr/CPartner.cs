﻿using System;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Lynx.APD.Component.Library.Common;
using Scripting;
using SiteUtilities;
using System.Threading;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using System.Collections;
using System.Reflection;
using System.IO;
using Ionic.Zip;
using System.Configuration;


namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class CPartner
    {
        #region Variables
        private const string MODULE_NAME = "LAPDPartnerDataMgrCPartner";

        private MDomUtils mobjMDomUtils = null;
        private MLAPDPartnerDataMgr mobjLAPDPartnerMgr = null;
        private MEscapeUtilities mobjMEscapeUtilities = null;
        private MBase64Utils mobjBase64utils = null;
        MPPGTransactions mobjMPPGTransactions = null;
        DataAccess.CDataAccessor objCdataAccessor = new DataAccess.CDataAccessor();
        SharedData objShared = null;
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerDebug"]);
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        private EDocumentType menmDocType;
        int intEstimateSequenceNumber = 0;
        private string mstrDocType = string.Empty,
        mstrRawDocType = string.Empty,
        mstrPartnerConfig = string.Empty,
        mstrExistingEstimateDataXml = string.Empty,
        mstrExistingPrintImageXml = string.Empty,
        mstrPartner = string.Empty,
        mstrActualLynxID = string.Empty,
        mstrLynxID = string.Empty,
        mstrAssignmentID = string.Empty,
        mstrTaskID = string.Empty,
        mstrVehicleNumber = string.Empty,
        mstrActionCode = string.Empty,
        mstrPassedDocument = string.Empty,
        mstrPartnerSmallID = string.Empty;
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;
        long mlngPartnerTransID = 0,
     mlngExistingEstimateDataID = 0,
     mlngExistingPrintImageID = 0;
        public long mintEstimateSequenceNumber = 0;

        object mvarTransactionDate = null;

        XmlDocument mobjPassedDocument = null;
        #endregion

        #region Enum
        /// <summary>  
        /// 'Error codes private to this module.
        /// </summary>
        private enum CPartner_Error_Codes : ulong
        {
            eNotCreateable = 0x80067400,
            eShopSearchMissingParams
        }


        public enum EDocumentType
        {
            eAssignment,
            eEstimateData,
            ePrintImage,
            eDigitalImage,
            eBusinessEvent,
            eReceipt,
            eHeartBeat,
            eShopSearch,
            eFolderUpload,
            eClaimMessage
        }
        #endregion

        #region Properties
        /// <summary>
        /// Sets the Document Type Enum and String
        /// </summary>
        internal EDocumentType DocTypeEnum
        {
            get
            {
                return menmDocType;
            }
            set
            {
                menmDocType = value;
                SetDocTypeEnumProperty(menmDocType);
            }
        }


        /// <summary>
        /// Gets the Document Type String
        /// </summary>
        internal string DocTypeString
        {
            get
            {
                return mstrDocType;
            }
        }


        /// <summary>
        /// Gets the document type specific configuration XPath
        /// </summary>
        internal string DocTypeConfig
        {
            get
            {
                return string.Concat(mstrPartnerConfig, mstrDocType, "/");
            }

        }


        /// <summary>
        /// Gets the raw document type.
        /// </summary>
        public string RawDocTypeString
        {
            get
            {
                return mstrDocType;
            }
            set
            {

                mstrDocType = value;
                RawDocTypeStringEncode(mstrDocType);
            }
        }


        /// <summary>
        /// Gets the document ID of any document added to the partner database.
        /// </summary>
        internal long PartnerTransID
        {
            get
            {
                return mlngPartnerTransID;
            }
        }


        /// <summary>
        /// The estimate sequence number.
        /// </summary>
        internal int EstimateSequenceNumber   //TODO:
        {
            get
            {
                return intEstimateSequenceNumber;
            }
            set
            {
                intEstimateSequenceNumber = value;
                intEstimateSequenceNumber = Convert.ToInt32(mintEstimateSequenceNumber);
            }
        }


        internal string DocTypeStringForDatabase
        {
            get
            {
                return DocTypeStringForDB(menmDocType);
            }
        }


        //Gets the Actual LynxID
        internal string ActualLynxID
        {
            get
            {
                return mstrActualLynxID;
            }
        }


        //Gets the LynxID
        internal string LynxID
        {
            get
            {
                return mstrLynxID;
            }
        }


        //Gets the AssignmentID
        internal string AssignmentID
        {
            get
            {
                return mstrAssignmentID;
            }
        }

        // Gets the TaskID
        internal string TaskID
        {
            get
            {
                return mstrTaskID;
            }
        }

        //Gets the Vehicle Number
        internal string VehicleNumber
        {
            get
            {
                return mstrVehicleNumber;
            }
        }

        //Gets the Action Code
        internal string ActionCode
        {
            get
            {
                return mstrActionCode;
            }
        }

        //Gets the Transaction Date
        internal object TransactionDate
        {
            get
            {
                return mvarTransactionDate;
            }
        }

        //Gets the Passed Document
        internal string PassedDocument
        {
            get
            {
                return mstrPassedDocument;
            }
            set
            {
                mstrPassedDocument = value;
                XmlDocument mobjPassedDocument = new XmlDocument();
                mobjMDomUtils = new MDomUtils();
                mobjMDomUtils.LoadXml(ref mobjPassedDocument, ref mstrPassedDocument, string.Empty, string.Empty);
            }
        }

        //Gets the Partner
        internal string TradingPartner
        {
            get
            {
                return mstrPartner;
            }
            set
            {
                mstrPartner = value;
                mstrPartnerSmallID = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", mstrPartner, "/@SmallID" }));
            }
        }
        #endregion

        #region Methods
        public CPartner(SharedData objPartnerShared)
        {
            try
            {
                objShared = objPartnerShared;
                mobjLAPDPartnerMgr = new MLAPDPartnerDataMgr();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method initializes processing of the documents
        /// </summary>
        /// <returns></returns>
        public bool InitProcessDocument()
        {
            return true;
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "InitProcessDocument() Method not implemented" }));
        }

        /// <summary>
        /// This method initializes processing of the documents.
        /// </summary>
        /// <param name="strTradingPartner"></param>
        /// <param name="strPassedDocument"></param>
        /// <returns></returns>
        public bool InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            bool isInitProcessDocumentV2 = false;
            try
            {
                return isInitProcessDocumentV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "InitProcessDocumentV2() Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// This method processes documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransfer = false;
            try
            {
                return isPartnerDatabaseTransfer;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "PartnerDatabaseTransfer()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// This method processes documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransferV2 = false;
            try
            {
                return isPartnerDatabaseTransferV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "PartnerDatabaseTransferV2()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// This method processes documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string ApdDatabaseTransfer()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "ApdDatabaseTransfer()", ",", "Method not implemented" }));
        }

        /// <summary>
        /// This method processes documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string ApdDatabaseTransferV2()
        {
            string strApdDatabaseTransferV2 = string.Empty;
            try
            {
                return strApdDatabaseTransferV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "ApdDatabaseTransferV2()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// This method transmits documents to the partner. 
        /// </summary>
        /// <returns></returns>
        public string TransferToPartner()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "TransferToPartner()", ",", "Method not implemented" }));
        }

        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        public long InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            long lngInsertPrintImageIntoAPD = 0;
            XmlDocument objXmlDoc = null;
            try
            {
                objXmlDoc = new XmlDocument();
                mobjMDomUtils = new MDomUtils();

                mobjMDomUtils.LoadXml(ref objXmlDoc, ref strXML, "InsertPrintImageIntoAPD", "Print Image XML");
                lngInsertPrintImageIntoAPD = ProcessBinaryFiles(ref objXmlDoc, "//ImageFile", "", "", "PDF", "PrintImage", EDocumentType.ePrintImage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objXmlDoc = null;
            }
            return lngInsertPrintImageIntoAPD;
        }

        /// <summary>
        ///Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        public long InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            long lnginsertPrintImageIntoAPDV2 = 0;
            XmlDocument objXmlDocument = null;

            try
            {
                objXmlDocument = new XmlDocument();
                mobjMDomUtils = new MDomUtils();

                mobjMDomUtils.LoadXml(ref objXmlDocument, ref strXML, "InsertPrintImageIntoAPDV2", "Print Image XML");
                lnginsertPrintImageIntoAPDV2 = ProcessBinaryFiles_v2(ref objXmlDocument, "//ImageFile", "", "", "PDF", "PrintImage", EDocumentType.ePrintImage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lnginsertPrintImageIntoAPDV2;
        }

        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void SendHeartBeatRequest()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "SendHeartBeatRequest()", ",", "Method not implemented" }));
        }

        /// <summary>
        /// SetDocTypeEnumProperty
        /// </summary>
        /// <param name="enmDocType"></param>
        internal void SetDocTypeEnumProperty(EDocumentType enmDocType)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SetDocTypeEnumPropertyDocTypeEnum: " });

            try
            {
                mstrPartnerConfig = string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/" });
                switch (enmDocType)
                {
                    case EDocumentType.eAssignment:
                        mstrDocType = "Assignment";
                        break;
                    case EDocumentType.eEstimateData:
                        mstrDocType = "EstimateData";
                        break;
                    case EDocumentType.ePrintImage:
                        mstrDocType = "PrintImage";
                        break;
                    case EDocumentType.eDigitalImage:
                        mstrDocType = "DigitalImage";
                        break;
                    case EDocumentType.eBusinessEvent:
                        mstrDocType = "BusinessEvent";
                        break;
                    case EDocumentType.eReceipt:
                        mstrDocType = "Receipt";
                        break;
                    case EDocumentType.eHeartBeat:
                        mstrDocType = "HeartBeat";
                        break;
                    case EDocumentType.eShopSearch:
                        mstrDocType = "ShopSearch";
                        break;
                    case EDocumentType.eFolderUpload:
                        mstrDocType = "FolderUpload";
                        break;
                    case EDocumentType.eClaimMessage:
                        mstrDocType = "ClaimMessage";
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType), ",", PROC_NAME, ",", "Unknown document type : '", Convert.ToString(enmDocType), "'." }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// RawDocTypeStringEncode
        /// </summary>
        /// <param name="strRawDocType"></param>
        public void RawDocTypeStringEncode(string strRawDocType)
        {
            string PROC_NAME = string.Empty;
            mstrPartnerConfig = string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/" });
            mstrRawDocType = strRawDocType;
            mstrDocType = "";

            try
            {
                if ((strRawDocType.Length) > 0)
                    if (objShared.g_blnUsePartnerTypesForDocTypes)
                    {
                        if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "EstimateData/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eEstimateData;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "PrintImage/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.ePrintImage;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "DigitalImage/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eDigitalImage;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "HeartBeat/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eHeartBeat;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "Receipt/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eReceipt;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "BusinessEvent/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eBusinessEvent;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "ShopSearch/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eShopSearch;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "Assignment/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eAssignment;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "FolderUpload/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eFolderUpload;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "ClaimMessage/@PartnerType")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eClaimMessage;
                    }
                    else
                    {
                        if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "EstimateData/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eEstimateData;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "PrintImage/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.ePrintImage;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "DigitalImage/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eDigitalImage;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "HeartBeat/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eHeartBeat;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "Receipt/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eReceipt;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "BusinessEvent/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eBusinessEvent;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "ShopSearch/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eShopSearch;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "Assignment/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eAssignment;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "FolderUpload/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eFolderUpload;
                        else if (mobjLAPDPartnerMgr.GetConfig(string.Concat(mstrPartnerConfig, "ClaimMessage/@Type")) == strRawDocType)
                            DocTypeEnum = EDocumentType.eClaimMessage;
                    }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// DocTypeStringForDB
        /// </summary>
        /// <param name="menmdoctype"></param>
        /// <returns></returns>
        internal string DocTypeStringForDB(EDocumentType menmdoctype)
        {
            string strDocType = string.Empty;
            try
            {
                switch (menmdoctype)
                {
                    case EDocumentType.eAssignment:
                        strDocType = "Assignment";
                        break;
                    case EDocumentType.eEstimateData:
                        strDocType = "EstimateTransaction";
                        break;
                    case EDocumentType.ePrintImage:
                        strDocType = "EstimatePI";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDocType = "EstimateDI";
                        break;
                    case EDocumentType.eBusinessEvent:
                        strDocType = "BusinessEvent";
                        break;
                    case EDocumentType.eReceipt:
                        strDocType = "ExternalAck";
                        break;
                    case EDocumentType.eHeartBeat:
                        strDocType = "HeartBeat";
                        break;
                    case EDocumentType.eShopSearch:
                        strDocType = "ShopSearch";
                        break;
                    case EDocumentType.eFolderUpload:
                        strDocType = "FolderUpload";
                        break;
                    case EDocumentType.eClaimMessage:
                        strDocType = "ClaimMessage";
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType), "Unknown document type : ", Convert.ToString(menmdoctype) }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDocType;
        }

        /// <summary>
        ///  Returns True if we are supposed to process this document type.
        /// </summary>
        internal bool CanProcessDocument()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CanProcessDocument: " });
            bool isProcessDocument = false;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - CanProcessDocument", string.Concat("CanProcessDocument Started"), "");
                //'Ignore any transaction with LynxID = 'TEST_LYNX_ID'
                if (objShared.g_strLynxID == "TEST_LYNX_ID")
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument", string.Concat(mstrDocType, " Received. Ignoring transaction with LynxID = 'TEST_LYNX_ID'"), "");
                }
                //Check to see if we are supposed to process this document type.
                else if ("True" != mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "@Process" })))
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument", string.Concat("We are not configured to process ", objShared.g_strPartner, " document type ", mstrDocType), "");
                }
                //Otherwise return True for normal document consumption.
                else
                {
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument", string.Concat(mstrDocType, " Received "), "");
                    isProcessDocument = true;
                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - CanProcessDocument", string.Concat(mstrDocType, " Received "), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return isProcessDocument;
        }

        /// <summary>
        ///  Returns True if we are supposed to process this document type.
        /// </summary>
        /// 
        internal bool CanProcessDocument_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CanProcessDocument_v2: " });
            bool isCanProcessDocument_v2 = false;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - CanProcessDocument_v2", string.Concat("CanProcessDocument_v2 Started"), "");
                //Ignore any transaction with LynxID = 'TEST_LYNX_ID'
                if (mstrLynxID == "TEST_LYNX_ID")
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument_v2", string.Concat(mstrDocType, " Received. Ignoring transaction with LynxID = 'TEST_LYNX_ID'"), "");
                }
                //Check to see if we are supposed to process this document type.
                else if ("True" != mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "@Process" })))
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument_v2", string.Concat("We are not configured to process ", objShared.g_strPartner, " document type ", mstrDocType), "");
                }
                //Otherwise return True for normal document consumption.
                else
                {
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - CanProcessDocument_v2", string.Concat(mstrDocType, " Received "), "");
                    isCanProcessDocument_v2 = true;
                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - CanProcessDocument_v2", string.Concat(mstrDocType, " Received "), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return isCanProcessDocument_v2;
        }

        /// <summary>
        /// BuildFinalFileName
        /// </summary>
        /// <returns></returns>
        internal string BuildFinalFileName(int intSuffix, string strDate, string strDocType)
        {
            // Construct for the Digital Image's actual filename as it will be stored on disk
            int intImageCount = 0;
            string strImageFolder = string.Empty;
            string strLynxID = string.Empty;
            string strPrefix = string.Empty;
            string strSuffix = string.Empty;
            string strfinalFileName = string.Empty;
            try
            {
                //TODO changed g_strLynxID to mstrLynxID also g_strVehicleNumber to mstrVehicleNumber
                strLynxID = Right(objShared.g_strLynxID, 4);
                for (intImageCount = 0; intImageCount < 4; intImageCount++)
                {
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\", Mid(strLynxID, intImageCount, 1) });
                }
                strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\" });

                // The first letter in the name of the file for imaging purposes
                strPrefix = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/", strDocType, "/", "@ImageFilenamePrefix" }));
                strSuffix = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/", strDocType, "/", "@ImageFilenameSuffix" }));

                strfinalFileName = string.Concat(new string[] { strImageFolder, strPrefix, strDate, "LYNXID", objShared.g_strLynxID, "V", objShared.g_strVehicleNumber, "Doc", mobjLAPDPartnerMgr.ZeroPad(intSuffix, 3), ".", strSuffix });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strfinalFileName;
        }

        /// <summary>
        /// Build Final File Name
        /// </summary>
        internal string BuildFinalFileName_v2(int intSuffix, string strDate, string strDocType)
        {
            int intImageCount = 0;
            string strImageFolder = null;
            string strLynxID = null;
            string strPrefix = null;
            string strSuffix = null;
            string strGUID = null;
            string strfinalFileName_v2 = string.Empty;

            Guid generateGUID = new Guid();
            try
            {
                // Construct for the Digital Image's actual filename as it will be stored on disk
                strLynxID = Right(mstrLynxID, 4);

                for (intImageCount = 0; intImageCount < 4; intImageCount++)
                {
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\", Mid(strLynxID, intImageCount, 1) });
                }
                strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\" });

                // The first letter in the name of the file for imaging purposes
                strPrefix = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/", strDocType, "/", "@ImageFilenamePrefix" }));
                strSuffix = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objShared.g_strPartner, "/", strDocType, "/", "@ImageFilenameSuffix" }));

                generateGUID = Guid.NewGuid();
                strGUID = generateGUID.ToString();
                strGUID = strGUID.Replace("{", "");
                strGUID = strGUID.Replace("}", "");
                strGUID = strGUID.Replace("-", "");

                strfinalFileName_v2 = string.Concat(new string[] { strImageFolder, strPrefix, strGUID, "LYNXID", mstrLynxID, "V", mstrVehicleNumber, "Doc", mobjLAPDPartnerMgr.ZeroPad(intSuffix, 3), ".", strSuffix });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strfinalFileName_v2;
        }

        /// <summary>
        /// Insert LAPD Document
        /// Calls a DB stored procedure to add or update an APD document record.
        /// Parameters:  objDataAccessor   - The instance of Data Accessor to use
        /// strDocType        - Document Type
        /// lngLynxID         - The LynxID to attach the document to
        /// intVehicleNumber  - Vehicle Number to attach the document to
        /// intSequenceNumber - Estimate/Supplement Sequence Number
        /// strpartner        - Partner
        /// lngPartnerTransID - Identity ID of document in the partner database
        /// strImagePath      - The image file path to be stored with the document record
        /// strImageType      - The image type being stored (jpg, txt, xml, etc)
        /// lngAPDDocumentID  - Identity of associated record in APD Database (if available)
        ///                    (The resulting APD Document ID will be here when complete)
        /// </summary>
        public long InsertLAPDDocument(EDocumentType enmDocType, long lngAssignmentID,
                                         long lngLynxID, long intVehicleNumber, int intSequenceNumber, string strPartnerTransID, string strImagePath,
                                         string strImageType, string strCreatedDate, bool blnNotifyEvent)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertLAPDDocument: " });
            string strDBDocumentType = string.Empty;
            string strDBImagePath = string.Empty;
            string strDBImageDate = string.Empty;
            string strDBDocumentSource = string.Empty;
            long lngLAPDDocument = 0;
            Hashtable hashTable = null;
            int intRetryTimeOut, intRetryCount;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
                intRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("InsertLAPDDocument Started"), "");
                //Determine the DBDocumentType
                switch (enmDocType)
                {
                    case EDocumentType.ePrintImage:
                    case EDocumentType.eEstimateData:
                        if (intSequenceNumber == 0)
                            strDBDocumentType = "Estimate";
                        else
                            strDBDocumentType = "Supplement";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDBDocumentType = "Photograph";
                        break;
                }

                strDBDocumentSource = mobjLAPDPartnerMgr.GetConfig(string.Concat("PartnerSettings/", objShared.g_strPartner, "/@SourceID"));
                //Determine the DBImagePath and Date
                if (strImagePath == string.Empty)
                {
                    strDBImagePath = string.Empty;
                    strDBImageDate = string.Empty;
                }
                else
                {
                    strDBImagePath = strImagePath;
                    strDBImageDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
                }
                //Use Now for the created date if not passed.
                if (string.IsNullOrEmpty(strCreatedDate))
                    strCreatedDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

                //Set the connect string to APD               
                hashTable = new Hashtable();
                hashTable.Add("@NTUserID", mobjLAPDPartnerMgr.GetConfig("WorkFlow/NTUserID"));
                hashTable.Add("@PertainsTo", string.Concat("VEH", Convert.ToString(intVehicleNumber)));
                hashTable.Add("@LynxID", lngLynxID);
                hashTable.Add("@AssignmentID", lngAssignmentID);
                hashTable.Add("@NotifyEvent", (blnNotifyEvent ? 1 : 0));
                hashTable.Add("@CreatedDate", strCreatedDate);
                hashTable.Add("@ImageDate", strDBImageDate);
                hashTable.Add("@ImageLocation", strDBImagePath);
                hashTable.Add("@DocumentSource", strDBDocumentSource);
                hashTable.Add("@DocumentType", strDBDocumentType);
                hashTable.Add("@SupplementSeqNumber", intSequenceNumber);
                hashTable.Add("@ExternalReference", strPartnerTransID);
                hashTable.Add("@ImageType", strImageType);

                bool isProcessed = false;
                int iRetry = 0;
                while (iRetry < 10 && !isProcessed)
                {
                    try
                    {
                        lngLAPDDocument = objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/InsertSP"), hashTable, "APD");
                        isProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        switch (iRetry)
                        {
                            case 0:
                                //objWritelog.Send(ex, string.Concat("WARNING: PartnerDataMgr.CCCDocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                objWritelog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "");
                                break;
                            case 10:
                                //5th attempt failure, notify as Error
                                objWritelog.Send(ex, string.Concat("ERROR: PartnerDataMgr.CCCDocumentUpload - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                objWritelog.LogEvent("COMConversion", "ERROR", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("SERIOUS - DocumentUpload - 10th Attempt failed. Reason: ", ex.Message), "");
                                throw new Exception("An unexpected error has caused a delay in upload the document.");
                                break;
                            default:
                                objWritelog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("DocumentUpload -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                break;
                        }
                        isProcessed = false;
                    }
                    if (isProcessed)
                        break;
                    System.Threading.Thread.Sleep(intRetryTimeOut);
                    iRetry++;

                    // lngLAPDDocument = objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/InsertSP"), hashTable, "APD");

                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("InsertLAPDDocument Completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument", string.Concat("Error occur in InsertLAPDDocument"), ex.Message);
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
            return lngLAPDDocument;
        }

        /// <summary>
        /// Insert LAPD Document

        /// Calls a DB stored procedure to add or update an APD document record.

        /// Parameters:  objDataAccessor   - The instance of Data Accessor to use
        /// strDocType        - Document Type
        /// lngLynxID         - The LynxID to attach the document to
        /// intVehicleNumber  - Vehicle Number to attach the document to
        /// intSequenceNumber - Estimate/Supplement Sequence Number
        /// strpartner        - Partner
        /// lngPartnerTransID - Identity ID of document in the partner database
        /// strImagePath      - The image file path to be stored with the document record
        /// strImageType      - The image type being stored (jpg, txt, xml, etc)
        /// lngAPDDocumentID  - Identity of associated record in APD Database (if available)
        ///                    (The resulting APD Document ID will be here when complete)
        /// </summary>
        internal long InsertLAPDDocument_v2(EDocumentType enmDocType, long lngAssignmentID,
                                            long lngLynxID, long intVehicleNumber, int intSequenceNumber, string strPartnerTransID, string strImagePath,
                                            string strImageType, string strCreatedDate, bool blnNotifyEvent)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertLAPDDocument_v2: " });
            string strDBDocumentType = string.Empty;
            string strDBImagePath = string.Empty;
            string strDBImageDate = string.Empty;
            string strDBDocumentSource = string.Empty;
            long lngLAPDDocument_v2 = 0;
            Hashtable hashTable = null;
            int intRetryTimeOut, intRetryCount;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
                intRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                //Determine the DBDocumentType
                switch (enmDocType)
                {
                    case EDocumentType.ePrintImage:
                    case EDocumentType.eEstimateData:

                        if (intSequenceNumber == 0)
                            strDBDocumentType = "Estimate";
                        else
                            strDBDocumentType = "Supplement";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDBDocumentType = "Photograph";
                        break;
                }
                strDBDocumentSource = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", mstrPartner, "/@SourceID" }));

                // Determine the DBImagePath and Date
                if (string.IsNullOrEmpty(strImagePath))
                {
                    strDBImagePath = "";
                    strDBImageDate = "";
                }
                else
                {
                    strDBImagePath = strImagePath;
                    strDBImageDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
                }

                //Use Now for the created date if not passed.
                if (string.IsNullOrEmpty(strCreatedDate))
                    strCreatedDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

                strCreatedDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

                // Perform the insert
                //lngLAPDDocument_v2 = objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/InsertSP"), new object[] { "@NTUserID", mobjLAPDPartnerMgr.GetConfig("WorkFlow/NTUserID"), "@PertainsTo", string.Concat(new string[] { "VEH", Convert.ToString(intVehicleNumber) }), "@AssignmentID", lngAssignmentID, "@LynxID", lngLynxID, "@NotifyEvent",
                //(blnNotifyEvent ? 1 : 0), "@CreatedDate", strCreatedDate, "@ImageDate", strDBImageDate, "@ImageLocation", strDBImagePath, "@DocumentSource", strDBDocumentSource, "@DocumentType",
                //strDBDocumentType, "@SupplementSeqNumber", intSequenceNumber, "@ExternalReference", strPartnerTransID, "@ImageType", strImageType});

                hashTable = new Hashtable();
                hashTable.Add("@NTUserID", mobjLAPDPartnerMgr.GetConfig("WorkFlow/NTUserID"));
                hashTable.Add("@PertainsTo", string.Concat("VEH", Convert.ToString(intVehicleNumber)));
                hashTable.Add("@LynxID", lngLynxID);
                hashTable.Add("@AssignmentID", lngAssignmentID);
                hashTable.Add("@NotifyEvent", (blnNotifyEvent ? 1 : 0));
                hashTable.Add("@CreatedDate", strCreatedDate);
                hashTable.Add("@ImageDate", strDBImageDate);
                hashTable.Add("@ImageLocation", strDBImagePath);
                hashTable.Add("@DocumentSource", strDBDocumentSource);
                hashTable.Add("@DocumentType", strDBDocumentType);
                hashTable.Add("@SupplementSeqNumber", intSequenceNumber);
                hashTable.Add("@ExternalReference", strPartnerTransID);
                hashTable.Add("@ImageType", strImageType);

                bool isProcessed = false;
                int iRetry = 0;
                while (iRetry < 10 && !isProcessed)
                {
                    try
                    {
                        lngLAPDDocument_v2 = objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/InsertSP"), hashTable, "APD");
                        isProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        switch (iRetry)
                        {
                            case 0:
                                //objWriteLog.Send(ex, string.Concat("WARNING: PartnerDataMgr.ADPDocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument_v2", string.Concat("DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "");
                                break;
                            case 10:
                                //5th attempt failure, notify as Error
                                objWriteLog.Send(ex, string.Concat("ERROR: PartnerDataMgr.ADPDocumentUpload - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "ERROR", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument_v2", string.Concat("SERIOUS - DocumentUpload - 10th Attempt failed. Reason: ", ex.Message), "");
                                throw new Exception("An unexpected error has caused a delay in upload the document.");
                                break;
                            default:
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - InsertLAPDDocument_v2", string.Concat("DocumentUpload -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                break;
                        }
                        isProcessed = false;
                    }
                    if (isProcessed)
                        break;
                    System.Threading.Thread.Sleep(intRetryTimeOut);
                    iRetry++;

                    //lngLAPDDocument_v2 = objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/InsertSP"), hashTable, "APD");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return lngLAPDDocument_v2;
        }

        /// <summary>
        /// Sends a workflow notification.
        /// </summary>
        internal void SendWorkflowNotification(string strCodeName, string strDescription, string strKey = "", string strPertainsTo = "VEH", string strConditionValue = "")
        {
            string strEventID = string.Empty;
            Hashtable hashtable = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("SendWorkflowNotification Started"), "");

                strEventID = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "WorkFlow/EventCodes/Code[@name='", strCodeName, "']" }));
                if (string.IsNullOrEmpty(strKey.Trim()))
                {
                    //this could be an RRP assignment. We will formulate a special key and pass it in
                    //  we don't want to make another call to figure out the assignment sequence number
                    //  but let the workflowNotifyEvent proc to figure it out.
                    strKey = string.Concat(new string[] { "$AssignmentID$=", objShared.g_strAssignmentID });
                }
                //Add vehicle number to pertains to if needed.
                if (strPertainsTo == "VEH")
                    strPertainsTo = string.Concat(new string[] { strPertainsTo, objShared.g_strVehicleNumber });

                if (!string.IsNullOrEmpty(strEventID))
                {
                    strDescription = strDescription.Replace("'", "''");
                    hashtable = new Hashtable();
                    hashtable.Add("@EventID", Convert.ToInt64(strEventID));
                    hashtable.Add("@LynxID", objShared.g_strLynxID);
                    hashtable.Add("@PertainsTo", strPertainsTo);
                    hashtable.Add("@Description", strDescription);
                    hashtable.Add("@Key", strKey);
                    hashtable.Add("@UserID", mobjLAPDPartnerMgr.GetConfig("WorkFlow/SysUserID"));
                    hashtable.Add("@ConditionValue", strConditionValue);

                    objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("WorkFlow/SPName"), hashtable, "APD");
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("SendWorkflowNotification completed. Workflow Notification Sent : ", strCodeName), "");
                }
                else
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("EventID For ", strCodeName, " not found in the config file.  Bypassing workflow call."), "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
        }

        /// <summary>
        /// Sends a workflow notification.
        /// </summary>
        internal void SendWorkflowNotification_v2(string strCodeName, string strDescription, string strKey = "", string strPertainsTo = "VEH", string strConditionValue = "")
        {
            string strEventID = null;
            Hashtable hashTable = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification_v2", string.Concat("SendWorkflowNotification_v2 Started"), "");

                strEventID = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { "WorkFlow/EventCodes/Code[@name='", strCodeName, "']" }));

                if (string.IsNullOrEmpty(strKey.Trim()))
                {
                    //this could be an RRP assignment. We will formulate a special key and pass it in
                    //  we don't want to make another call to figure out the assignment sequence number
                    //  but let the workflowNotifyEvent proc to figure it out.
                    strKey = string.Concat(new string[] { "$AssignmentID$=", objShared.g_strAssignmentID });
                }

                //Add vehicle number to pertains to if needed.
                if (strPertainsTo == "VEH")
                {
                    strPertainsTo = string.Concat(new string[] { strPertainsTo, Convert.ToString(mstrVehicleNumber) });
                }
                if (!string.IsNullOrEmpty(strEventID))
                {
                    strDescription = strDescription.Replace("'", "''");
                    hashTable = new Hashtable();
                    hashTable.Add("@EventID", Convert.ToInt64(strEventID));
                    hashTable.Add("@LynxID", mstrLynxID);
                    hashTable.Add("@PertainsTo", strPertainsTo);
                    hashTable.Add("@Description", strDescription);
                    hashTable.Add("@Key", strKey);
                    hashTable.Add("@UserID", mobjLAPDPartnerMgr.GetConfig("WorkFlow/SysUserID"));
                    hashTable.Add("@ConditionValue", strConditionValue);

                    objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("WorkFlow/SPName"), hashTable, "APD");
                }
                else
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification_v2", string.Concat("EventID For ", strCodeName, " not found in the config file.  Bypassing workflow call."), "");
                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification_v2", string.Concat("SendWorkflowNotification_v2 Completed"), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWritelog = null;
            }
        }

        /// <summary>
        /// Stores a estimate print image transaction in the partner database.
        /// </summary>
        internal void StorePrintImageInPartnerDB()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StorePrintImageInPartnerDB: " });

            XmlDocument objBinaryNode = null;
            ADODB.Recordset objRS = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                objBinaryNode = new XmlDocument();
                objRS = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb();
                if (objRS.EOF && objRS.BOF)
                {
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), PROC_NAME, "No Records returned from partner DB for Print Image submission." }));
                }

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StorePrintImageInPartnerDB: Issue while loading the transaction xml.");

                mstrExistingPrintImageXml = xmlTransDocument.OuterXml;
                mlngExistingPrintImageID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.
                if (Convert.ToBoolean(objRS.Fields["DocumentExists"].Value))
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingPrintImageID, "", "", False

                    //Otherwise, if available, extract the 'Other' document so that we have both ED and PI.
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"].Value != DBNull.Value)
                        mstrExistingEstimateDataXml = Convert.ToString(objRS.Fields["OtherTransXML"].Value);

                    if (objRS.Fields["OtherPartnerTransID"].Value != DBNull.Value)
                        mlngExistingEstimateDataID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objBinaryNode = null;
                xmlTransDocument = null;
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Stores a estimate print image transaction in the partner database.
        /// </summary>
        internal void StorePrintImageInPartnerDB_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StorePrintImageInPartnerDB_v2: " });

            XmlDocument objBinaryNode = null;
            ADODB.Recordset objRS = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                objBinaryNode = new XmlDocument();
                objRS = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb_v2();

                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Print Image submission." }));

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StorePrintImageInPartnerDB_v2: Issue while loading the transaction xml.");

                mstrExistingPrintImageXml = xmlTransDocument.OuterXml;
                mlngExistingPrintImageID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.

                if (Convert.ToBoolean(objRS.Fields["DocumentExists"].Value))
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingPrintImageID, "", "", False

                    //Otherwise, if available, extract the 'Other' document so that we have both ED and PI.
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"].Value != DBNull.Value)
                        mstrExistingEstimateDataXml = Convert.ToString(objRS.Fields["OtherTransXML"].Value);

                    if (objRS.Fields["OtherPartnerTransID"].Value != DBNull.Value)
                        mlngExistingEstimateDataID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objBinaryNode = null;
                xmlTransDocument = null;
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }

        /// <summary>
        ///Stores estimate data in the partner database. 
        /// </summary>
        internal void StoreEstimateDataInPartnerDB()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreEstimateDataInPartnerDB: " });
            ADODB.Recordset objRS = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                objRS = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb();
                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Estimate Data submission." }));

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StoreEstimateDataInPartnerDB: Issue while loading the transaction xml.");

                mstrExistingEstimateDataXml = xmlTransDocument.OuterXml;
                mlngExistingEstimateDataID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.
                if (Convert.ToBoolean(objRS.Fields["DocumentExists"].Value))
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingEstimateDataID, "", "", False
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"].Value != DBNull.Value)
                        mstrExistingPrintImageXml = Convert.ToString(objRS.Fields["OtherTransXML"].Value);

                    if (objRS.Fields["OtherPartnerTransID"].Value != DBNull.Value)
                        mlngExistingPrintImageID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }

        /// <summary>
        ///  Stores estimate data in the partner database.
        /// </summary>
        internal void StoreEstimateDataInPartnerDB_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreEstimateDataInPartnerDB_v2: " });

            ADODB.Recordset objRS = null;
            XmlDocument xmlTransDocument = null;
            try
            {
                objRS = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb_v2();

                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Estimate Data submission." }));

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StoreEstimateDataInPartnerDB_v2: Issue while loading the transaction xml.");

                mstrExistingEstimateDataXml = xmlTransDocument.OuterXml;
                mlngExistingEstimateDataID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.

                if (Convert.ToBoolean(objRS.Fields["DocumentExists"].Value))
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingEstimateDataID, "", "", False
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"].Value != DBNull.Value)
                        mstrExistingPrintImageXml = Convert.ToString(objRS.Fields["OtherTransXML"].Value);

                    if (objRS.Fields["OtherPartnerTransID"].Value != DBNull.Value)
                        mlngExistingPrintImageID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }

        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal ADODB.Recordset StoreDocumentInPartnerDb()
        {
            ADODB.Recordset objRS = null;
            ADODB.Recordset StoreDocumentInPartnerDb = null;
            Hashtable hashTable = null;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWriteLog = null;
            int intRetryTimeOut, intRetryCount;
            string strAssignmentID = string.Empty, strLynxID = string.Empty, strVehNum = string.Empty, strTransDate = string.Empty;
            try
            {
                objRS = new ADODB.Recordset();
                StoreDocumentInPartnerDb = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                objWriteLog = new WriteLog();
                intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
                intRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StoreDocumentInPartnerDb: Issue while loading the transaction xml.");

                //if (objShared.g_strAssignmentID == null)
                //    strAssignmentID = string.Empty;
                //if (objShared.g_strLynxID == null)
                //    strLynxID = string.Empty;
                //if (objShared.g_strVehicleNumber == null)
                //    strVehNum = string.Empty;

                //Initialize the connect string for std output
                //Store the estimate data, getting the stored proc to call from the config file.
                hashTable = new Hashtable();
                hashTable.Add("@AssignmentID", objShared.g_strAssignmentID);
                hashTable.Add("@LynxID", objShared.g_strLynxID);
                hashTable.Add("@VehicleNumber", objShared.g_strVehicleNumber);
                hashTable.Add("@SequenceNumber", Convert.ToString(mintEstimateSequenceNumber));
                hashTable.Add("@TransactionDate", Convert.ToString(objShared.g_varTransactionDate));
                hashTable.Add("@TransactionSource", objShared.g_strPartnerSmallID);
                hashTable.Add("@TransactionType", DocTypeStringForDatabase);
                hashTable.Add("@PartnerXML", xmlTransDocument.OuterXml.Replace("'", "''"));

                bool isProcessed = false;
                int iRetry = 0;

                while (iRetry < 10 && !isProcessed)
                {
                    try
                    {
                        objRS = objCdataAccessor.OpenRecordsetSp(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/PartnerLoadSP"), hashTable, "Partner");
                        isProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        switch (iRetry)
                        {
                            case 0:
                                // objWriteLog.Send(ex, string.Concat("WARNING: CCC ParterInsertion - 1st Attempt failed.  Reason: ", ex.Message, "TransactionXML: ", xmlTransDocument.OuterXml), "Warning: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb", string.Concat("StoreDocumentInPartnerDb.Partner - 1st Attempt failed.  Reason: ", ex.Message), "");
                                break;
                            case 10:
                                //5th attempt failure, notify as Error
                                objWriteLog.Send(ex, string.Concat("ERROR: CCC ParterInsertion - 10th Attempt failed.  Reason: ", ex.Message, "TransactionXML: ", xmlTransDocument.OuterXml), "Error: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "ERROR", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb", string.Concat("SERIOUS - StoreDocumentInPartnerDb.Partner - 10th Attempt failed. Reason: ", ex.Message), "");
                                throw new Exception("An unexpected error has caused a delay while processing into the Partner DB.");
                                break;
                            default:
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb", string.Concat("StoreDocumentInPartnerDb.Partner -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), string.Concat("TransactionXML: ", xmlTransDocument.OuterXml));
                                break;
                        }
                        isProcessed = false;
                    }
                    if (isProcessed)
                        break;
                    System.Threading.Thread.Sleep(intRetryTimeOut);
                    iRetry++;
                }
                //objRS = objCdataAccessor.OpenRecordsetSp(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/PartnerLoadSP"), hashTable, "Partner");

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    mobjLAPDPartnerMgr.DebugRS(ref objRS);

                mlngPartnerTransID = Convert.ToInt64(objRS.Fields["PartnerTransID"].Value);
                StoreDocumentInPartnerDb = objRS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
            return StoreDocumentInPartnerDb;
        }

        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal ADODB.Recordset StoreDocumentInPartnerDb_v2()
        {
            ADODB.Recordset RSStoreDocument = null;
            ADODB.Recordset objRS = null;
            string strAppend = string.Empty;
            Hashtable hashTable = null;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWriteLog = new WriteLog();
            int intRetryTimeOut, intRetryCount;
            try
            {
                RSStoreDocument = new ADODB.Recordset();
                objRS = new ADODB.Recordset();
                xmlTransDocument = new XmlDocument();
                intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
                intRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.StoreDocumentInPartnerDb_v2: Issue while loading the transaction xml.");

                //Initialize the connect string for std output
                //Store the estimate data, getting the stored proc to call from the config file.
                hashTable = new Hashtable();
                hashTable.Add("@AssignmentID", mstrAssignmentID);
                hashTable.Add("@LynxID", mstrLynxID);
                hashTable.Add("@VehicleNumber", mstrVehicleNumber);
                hashTable.Add("@SequenceNumber", Convert.ToString(mintEstimateSequenceNumber));
                hashTable.Add("@TransactionDate", Convert.ToString(objShared.g_varTransactionDate));
                hashTable.Add("@TransactionSource", mstrPartnerSmallID);
                hashTable.Add("@TransactionType", DocTypeStringForDatabase);
                hashTable.Add("@PartnerXML", xmlTransDocument.OuterXml.Replace("'", "''"));

                bool isProcessed = false;
                int iRetry = 0;

                while (iRetry < 10 && !isProcessed)
                {
                    try
                    {
                        objRS = objCdataAccessor.OpenRecordsetSp(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/PartnerLoadSP"), hashTable, "Partner");
                        isProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        switch (iRetry)
                        {
                            case 0:
                                //objWriteLog.Send(ex, string.Concat("WARNING: ADP ParterInsertion - 1st Attempt failed.  Reason: ", ex.Message, "TransactionXML: ", xmlTransDocument.OuterXml), "Warning: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb_v2", string.Concat("StoreDocumentInPartnerDb_v2.Partner - 1st Attempt failed.  Reason: ", ex.Message), "");
                                break;
                            case 10:
                                //5th attempt failure, notify as Error
                                objWriteLog.Send(ex, string.Concat("ERROR: ADP ParterInsertion - 10th Attempt failed.  Reason: ", ex.Message, "TransactionXML: ", xmlTransDocument.OuterXml), "Error: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "ERROR", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb_v2", string.Concat("SERIOUS - StoreDocumentInPartnerDb_v2.Partner - 10th Attempt failed. Reason: ", ex.Message), "");
                                throw new Exception("An unexpected error has caused a delay while processing into the Partner DB.");
                                break;
                            default:
                                objWriteLog.LogEvent("COMConversion", "WARNING", "PartnerDataMgr.CPartner.cs - StoreDocumentInPartnerDb_v2", string.Concat("StoreDocumentInPartnerDb_v2.Partner -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), string.Concat("TransactionXML: ", xmlTransDocument.OuterXml));
                                break;
                        }
                        isProcessed = false;
                    }
                    if (isProcessed)
                        break;
                    System.Threading.Thread.Sleep(intRetryTimeOut);
                    iRetry++;
                }

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    mobjLAPDPartnerMgr.DebugRS(ref objRS);

                mlngPartnerTransID = Convert.ToInt64(objRS.Fields["PartnerTransID"].Value);

                RSStoreDocument = objRS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
            return RSStoreDocument;
        }

        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        private void StoreCefInPartnerDb(long lngPartnerTransID, ref string strCEF)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreCefInPartnerDb: " });
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                mobjMEscapeUtilities = new MEscapeUtilities();
                objWriteLog = new WriteLog();

                //Assert what we were passed.
                if (lngPartnerTransID == 0)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE FAILED", "PartnerDataMgr.CPartner.cs - StoreCefInPartnerDb", string.Concat("Partner Trans ID was zero."), "");

                if (string.IsNullOrEmpty(strCEF.Trim()))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE FAILED", "PartnerDataMgr.CPartner.cs - StoreCefInPartnerDb", string.Concat("CEF string was blank."), "");

                //Initialize the connect string for std output
                //Store the CEF data, getting the stored proc to call from the config file.
                hashTable = new Hashtable();
                hashTable.Add("@PartnerTransID", lngPartnerTransID);
                hashTable.Add("@CEFXML", mobjMEscapeUtilities.SQLQueryString(ref strCEF));

                objCdataAccessor.ExecuteSp(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/CefUpdateSP"), hashTable, "Partner");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal string RetrieveCefFromPartnerDb(long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "RetrieveCefFromPartnerDb: " });

            string strRetrieveCef = null;
            ADODB.Recordset objRS = null;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objRS = new ADODB.Recordset();
                objWriteLog = new WriteLog();
                //Assert what we were passed.
                if (lngPartnerTransID == 0)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE FAILED", "PartnerDataMgr.CPartner.cs - StoreCefInPartnerDb", string.Concat("Partner Trans ID was zero."), "");

                //Initialize the connect string for std output
                //Retrieve the CEF data, getting the stored proc to call from the config file.
                hashTable = new Hashtable();
                hashTable.Add("@PartnerTransID", lngPartnerTransID);

                objRS = objCdataAccessor.OpenRecordsetSp(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/CefRetrieveSP"), hashTable, "Partner");

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    mobjLAPDPartnerMgr.DebugRS(ref objRS);

                strRetrieveCef = Convert.ToString(objRS.Fields["CEFXML"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.CancelUpdate();
                        objRS.Close();
                    }
                }
                objRS = null;
                objWriteLog = null;
            }
            return strRetrieveCef;
        }

        /// <summary>
        /// Extracts the Estimate Sequence Number from the passed XML and stores it
        /// in intEstimateSequenceNumber.
        /// The estimate Identifier comes in the form [E|S]#, the first character
        /// designating Estimate or Supplement, the second denoting the number.
        /// As a special case, the first estimate is represented as E01.
        /// We are primarily interested in the number, which we'll extract as is.
        /// In the special case, we change this to 0 for business logic reasons. 
        /// </summary>
        internal void ExtractSequenceNumber(string strIdentPath, [Optional]string strNumPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractSequenceNumber: " });

            string strNodeText = string.Empty;
            string strNodeTextLen = string.Empty;
            int intResult = 0;
            XmlDocument xmlTransDocument = null;
            try
            {
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ExtractSequenceNumber: Issue while loading the transaction xml.");
                // Text value of node we're looking at
                strNodeText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strIdentPath, true);

                //Validate value returned
                if ((strNodeText.Length > 0) && ((Left(strNodeText, 1) == "E") || (Left(strNodeText, 1) == "S")))
                {
                    if (Left(strNodeText, 1) == "E")
                        mintEstimateSequenceNumber = 0;
                    else
                    {
                        if (strNumPath == string.Empty)
                        {
                            //strNodeTextLen = Right(strNodeText, strNodeText.Length - 1);
                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                        else
                        {
                            strNodeText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strNumPath, true);

                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                    }
                }
                else
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Estimate Identifier began with neither 'S' nor 'E'." }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// Extracts the Estimate Sequence Number from the passed XML and stores it
        /// in intEstimateSequenceNumber.
        /// The estimate Identifier comes in the form [E|S]#, the first character
        /// designating Estimate or Supplement, the second denoting the number.
        /// As a special case, the first estimate is represented as E01.
        /// We are primarily interested in the number, which we'll extract as is.
        /// In the special case, we change this to 0 for business logic reasons.
        /// </summary>
        internal void ExtractSequenceNumber_v2(string strIdentPath, [Optional]string strNumPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractSequenceNumber_v2: " });

            string strNodeText = string.Empty;
            string strNodeTextLen = string.Empty;
            int intResult = 0;
            XmlDocument xmlTransDocument = null;
            try
            {
                xmlTransDocument = new XmlDocument();

                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ExtractSequenceNumber_v2: Issue while loading the transaction xml.");
                strNodeText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strIdentPath, true);

                // Validate value returned
                if ((strNodeText.Length) > 0 && ((Left(strNodeText, 1) == "E") || (Left(strNodeText, 1) == "S")))
                {
                    // Value is valid, parse it
                    if (Left(strNodeText, 1) == "E")
                        mintEstimateSequenceNumber = 0;
                    else
                    {
                        if (strNumPath == string.Empty)
                        {
                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                        else
                        {
                            strNodeText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strNumPath, true);

                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                    }
                }
                else
                {
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Estimate Identifier began with neither 'S' nor 'E'." }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events. 
        /// </summary>       
        internal void ProcessReceipt(string strErrorCodePath, string strErrorDescPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessReceipt: " });
            // Now decide if there was error or not in the outbound shop assingment
            // based on the error code contained in the inbound Xml we are playing with here.
            string strErrorCode = string.Empty;
            string strMonitor = string.Empty;
            string strErrorDesc = string.Empty;
            string strErrorKey = string.Empty;
            string strWorkFlow = string.Empty;
            int intErrorCode = 0;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            CEvents mobjCEvents = null;
            UInt64 errorCode;
            try
            {
                objWritelog = new WriteLog();
                mobjCEvents = new CEvents();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CCCC.cs - ProcessReceipt", string.Concat("ProcessReceipt Started"), "");

                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ProcessReceipt: Issue while loading the transaction xml.");

                strErrorCode = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strErrorCodePath);

                if (string.IsNullOrEmpty(strErrorCode))
                    strErrorCode = "0";

                //Receipt for cancellation.
                if (objShared.g_strActionCode == "C")
                {
                    //Log that we do Nothing
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt", string.Concat("Receipt for Cancellation received: Ignoring."), objShared.g_strPassedDocument.Trim());
                }
                //Receipt for successful assignment.
                else if (intErrorCode == 0)
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt", string.Concat("Receipt for Assignment received: Sending Workflow Notification."), objShared.g_strPassedDocument.Trim());
                    SendWorkflowNotification("AssignmentReceiptReceivedFromVAN", string.Concat("SHOP (", objShared.g_strPartner, "): Assignment for Vehicle ", Convert.ToString(objShared.g_strVehicleNumber), " succeeded."), "", "VEH", "");
                    //Errors at Partner!
                }
                else
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt", string.Concat("NEGATIVE Receipt for Assignment received: Sending Workflow Notification."), objShared.g_strPassedDocument.Trim());
                    strErrorDesc = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strErrorDescPath);
                    //Allow unknown codes from partner.  An email will get sent from SiteUtilities and that
                    //is enough to allow us to get any new codes from partner.  Fall into the default task key.
                    strErrorKey = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']" }));
                    strWorkFlow = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@WorkFlow" }));
                    strMonitor = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@Monitor" }));

                    if (strErrorKey.Length == 0)
                    {
                        strErrorKey = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']" }));
                        strWorkFlow = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@WorkFlow" }));
                        strMonitor = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@Monitor" }));
                    }
                    SendWorkflowNotification(strWorkFlow, string.Concat(new string[] { "SHOP (", objShared.g_strPartner, "): Assignment for Vehicle ", Convert.ToString(objShared.g_strVehicleNumber), " FAILED!  ", strErrorDesc }), strErrorKey, "VEH", "");

                    //Added the Monitor attribute to config.xml to allow selectivity of internal emails.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"
                    if (strMonitor == "True")
                    {
                        errorCode = Convert.ToUInt64(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived);
                        mobjCEvents.HandleEvent(Convert.ToInt32(errorCode), PROC_NAME, string.Concat(strErrorCode, "|", strErrorDesc, "|", strErrorKey), string.Concat("XML = ", objShared.g_strPassedDocument), "", false);
                    }
                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CCCC.cs - ProcessReceipt", string.Concat("ProcessReceipt Completed."), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
                objWritelog = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
            }
        }

        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>       
        internal void ProcessReceipt_v2(string strErrorCodePath, string strErrorDescPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessReceipt_v2: " });
            // Now decide if there was error or not in the outbound shop assingment
            // based on the error code contained in the inbound Xml we are playing with here.
            string strErrorCode = string.Empty;
            string strMonitor = string.Empty;
            string strErrorDesc = string.Empty;
            string strErrorKey = string.Empty;
            string strWorkFlow = string.Empty;
            int intErrorCode = 0;
            XmlDocument xmlTransDocument = null;
            Common.WriteLog objWritelog = null;
            CEvents mobjCEvents = null;
            UInt64 errorCode;
            try
            {
                objWritelog = new WriteLog();
                xmlTransDocument = new XmlDocument();
                mobjCEvents = new CEvents();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ProcessReceipt_v2: Issue while loading the transaction xml.");
                strErrorCode = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strErrorCodePath);

                if (string.IsNullOrEmpty(strErrorCode))
                    strErrorCode = "0";
                intErrorCode = Convert.ToInt32(strErrorCode);
                if (mstrActionCode == "C")
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt_v2", "Receipt for Cancellation received: Ignoring.", objShared.g_strPassedDocument.Trim());
                }
                //Receipt for successful assignment.
                else if (intErrorCode == 0)
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt_v2", "Receipt for Assignment received: Sending Workflow Notification.", objShared.g_strPassedDocument.Trim());
                    SendWorkflowNotification_v2("AssignmentReceiptReceivedFromVAN", string.Concat("SHOP (", mstrPartner, "): Assignment for Vehicle ", Convert.ToString(mstrVehicleNumber), " succeeded."), "", "VEH", "");
                    //Errors at Partner!
                }
                else
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CCCC.cs - ProcessReceipt_v2", "NEGATIVE Receipt for Assignment received: Sending Workflow Notification.", objShared.g_strPassedDocument.Trim());

                    strErrorDesc = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strErrorDescPath);
                    //Allow unknown codes from partner.  An email will get sent from SiteUtilities and that
                    //is enough to allow us to get any new codes from partner.  Fall into the default task key.

                    strErrorKey = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']" }));
                    strWorkFlow = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@WorkFlow" }));
                    strMonitor = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@Monitor" }));

                    if (strErrorKey.Length == 0)
                    {
                        strErrorKey = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']" }));
                        strWorkFlow = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@WorkFlow" }));
                        strMonitor = mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@Monitor" }));
                    }
                    SendWorkflowNotification_v2(strWorkFlow, string.Concat("SHOP (", mstrPartner, "): Assignment for Vehicle ", Convert.ToString(mstrVehicleNumber), " FAILED!  ", strErrorDesc, ",", strErrorKey), "", "VEH", "");

                    //Added the Monitor attribute to config.xml to allow selectivity of internal emails.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"
                    if (strMonitor == "True")
                    {
                        errorCode = Convert.ToUInt64(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived);
                        mobjCEvents.HandleEvent(Convert.ToInt32(errorCode), PROC_NAME, string.Concat(new string[] { strErrorCode, "|", strErrorDesc, "|", strErrorKey }), string.Concat(new string[] { "XML = ", mstrPassedDocument }), "", false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
                if (mobjCEvents != null)
                    Marshal.ReleaseComObject(mobjCEvents);
                mobjCEvents = null;
                objWritelog = null;
            }
        }

        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>
        /// //TODO Changing the Parameter as dynamic object
        internal void ProcessEstimate(CCCC objCCCC)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessEstimate: " });
            //Objects that need clean up.
            XmlDocument objEstimateDataDom = null, xmlTransDocument = null;
            CPartnerDataXfer objDataXfer = null;
            //Primitives that don't
            long lngAPDDocumentID = 0;
            string strCEF = string.Empty;
            bool blnDAAssignment = false;
            string strConditionValue = string.Empty;
            bool blnProcessEstimateData = false;
            Type objTypeDynamic = null;
            object objDynamic = null;
            Hashtable hashTable = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                blnProcessEstimateData = (mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "@ProcessEstimateData" })) == "False" ? false : true);

                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ProcessEstimate: Issue while loading the transaction xml.");

                //If we now have both the estimate data and the print image for this estimate,
                //submit them both into the APD database and make the appropriate workflow calls.

                if (((mstrExistingPrintImageXml.Length) != 0 && (mstrExistingEstimateDataXml.Length) != 0) || ((mstrExistingPrintImageXml.Length) != 0 && !blnProcessEstimateData))
                {
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate - CCC", string.Concat("Inserts an estimate print image into the APD document system."), "");
                    //Inserts an estimate print image into the APD document system.
                    //Returns the APD document ID through lngAPDDocumentID.  Will be new if not passed in.
                    //Dynamically create the object for Class like CCC, ADP, Scene Genisis
                    //objTypeDynamic = Type.GetType(dynamicClassName);
                    //objDynamic = Activator.CreateInstance(objTypeDynamic, objShared);

                    ////lngAPDDocumentID = objPartner.InsertPrintImageIntoAPD(mstrExistingPrintImageXml, mlngExistingPrintImageID);
                    //lngAPDDocumentID = Convert.ToInt64(objTypeDynamic.InvokeMember("CPartner_InsertPrintImageIntoAPD", BindingFlags.InvokeMethod, null, objDynamic, new object[] { mstrExistingPrintImageXml, mlngExistingPrintImageID }));

                    lngAPDDocumentID = objCCCC.CPartner_InsertPrintImageIntoAPD(mstrExistingPrintImageXml, mlngExistingPrintImageID);

                    if ((mstrExistingEstimateDataXml.Length) != 0 && blnProcessEstimateData)
                    {
                        //If the passed doc was the estimate data we can use it.
                        //Otherwise load up what we extracted from the partner database.
                        if (menmDocType == CPartner.EDocumentType.eEstimateData)
                            objEstimateDataDom = xmlTransDocument;
                        else
                        {
                            objEstimateDataDom = new XmlDocument();
                            mobjLAPDPartnerMgr.LoadXml(ref objEstimateDataDom, ref mstrExistingEstimateDataXml, PROC_NAME, "Existing Estimate");
                        }

                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate - CCC", string.Concat("Handles the insertion of the estimate data in APD."), "");
                        //Handles the insertion of the estimate data in APD.
                        //Returns the transformed estimate in Common Estimate Format (CEF).
                        objDataXfer = new CPartnerDataXfer(objShared);
                        strCEF = objDataXfer.TransferDataToAPD(objEstimateDataDom, lngAPDDocumentID);
                        objDataXfer = null;
                        //Save the CEF back into the partner database.                        
                        StoreCefInPartnerDb(mlngExistingEstimateDataID, ref strCEF);
                    }

                    hashTable = new Hashtable();
                    hashTable.Add("@AssignmentID", Convert.ToInt64(objShared.g_strAssignmentID));

                    blnDAAssignment = Convert.ToBoolean(objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), hashTable, "APD"));

                    strConditionValue = (blnDAAssignment ? string.Empty : "NONUSERSOURCE");
                    //Send a workflow event notification to the database.
                    SendWorkflowNotification("EstimateDataTransferredToAPD", string.Concat("SHOP (", objShared.g_strPartner, "): Estimate data received for Vehicle ", objShared.g_strVehicleNumber), "", "VEH", strConditionValue);
                    SendWorkflowNotification("EstimatePrintImageFileReceived", string.Concat("SHOP (", objShared.g_strPartner, "): Estimate print image received for Vehicle ", objShared.g_strVehicleNumber), "", "VEH", "");
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate - CCC", string.Concat("WorkFlow Notification is sent for Estimate and PrintImage"), "");
                }
                else
                {
                    //Need to implement the functionality suggested by Tom's.
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate - CCC", string.Concat("Process is halt due to reason - We do not have both the PI and the ED yet - processing halted."), objShared.g_strPassedDocument);
                }
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - ProcessEstimate - CCC", string.Concat(ex.Message, ex.InnerException, ex.Source, ex.StackTrace), "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                objEstimateDataDom = null;
                xmlTransDocument = null;
                objDataXfer = null;
            }
        }

        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>
        internal void ProcessEstimate_v2(ref CPartner objPartner)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessEstimate_v2: " });

            //Objects that need clean up.
            XmlDocument objEstimateDataDom = null, xmlTransDocument = null;
            CPartnerDataXfer objDataXfer = null;

            //Primitives that don't
            long lngAPDDocumentID = 0;
            string strCEF = string.Empty;
            bool blnDAAssignment = false;
            string strConditionValue = string.Empty;
            bool blnProcessEstimateData = false;
            Hashtable hashTable = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                blnProcessEstimateData = (mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "@ProcessEstimateData" })) == "False" ? false : true);

                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ProcessEstimate_v2: Issue while loading the transaction xml.");
                //If we now have both the estimate data and the print image for this estimate,
                //submit them both into the APD database and make the appropriate workflow calls.

                if (((mstrExistingPrintImageXml.Length) != 0 && (mstrExistingEstimateDataXml.Length) != 0) || ((mstrExistingPrintImageXml.Length) != 0 && !blnProcessEstimateData))
                {
                    //Inserts an estimate print image into the APD document system.
                    //Returns the APD document ID through lngAPDDocumentID.  Will be new if not passed in.
                    lngAPDDocumentID = objPartner.InsertPrintImageIntoAPDV2(mstrExistingPrintImageXml, mlngExistingPrintImageID);

                    if ((mstrExistingEstimateDataXml.Length) != 0 && blnProcessEstimateData)
                    {
                        //If the passed doc was the estimate data we can use it.
                        //Otherwise load up what we extracted from the partner database.
                        if (menmDocType == CPartner.EDocumentType.eEstimateData)
                            objEstimateDataDom = xmlTransDocument;
                        else
                        {
                            objEstimateDataDom = new XmlDocument();
                            mobjLAPDPartnerMgr.LoadXml(ref objEstimateDataDom, ref mstrExistingEstimateDataXml, PROC_NAME, "Existing Estimate");
                        }

                        objDataXfer = new CPartnerDataXfer(objShared);
                        if (isExtensiveDebug)
                            objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate_v2 - ADP", string.Concat("Handles the insertion of the estimate data in APD."), "");
                        //Handles the insertion of the estimate data in APD.
                        //Returns the transformed estimate in Common Estimate Format (CEF).
                        strCEF = objDataXfer.TransferDataToAPD_v2(objEstimateDataDom, lngAPDDocumentID, mstrPassedDocument, mstrPartnerSmallID);
                        objDataXfer = null;
                        //Save the CEF back into the partner database.                      
                        StoreCefInPartnerDb(mlngExistingEstimateDataID, ref strCEF);
                    }
                    hashTable = new Hashtable();
                    hashTable.Add("@AssignmentID", mstrAssignmentID);

                    blnDAAssignment = Convert.ToBoolean(objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), hashTable, "APD"));

                    strConditionValue = (blnDAAssignment ? string.Empty : "NONUSERSOURCE");
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate_v2 - ADP", string.Concat("Send WorkFlow Notification for Estimate and PrintImage"), "");
                    //Send a workflow event notification to the database.
                    SendWorkflowNotification_v2("EstimateDataTransferredToAPD", string.Concat("SHOP (", mstrPartner, "): Estimate data received for Vehicle ", mstrVehicleNumber), "", "VEH", strConditionValue);
                    SendWorkflowNotification_v2("EstimatePrintImageFileReceived", string.Concat("SHOP (", mstrPartner, "): Estimate print image received for Vehicle ", mstrVehicleNumber), "", "VEH", "");

                }
                else
                {
                    //Need to implement the functionality suggested by Tom's.
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessEstimate_v2 - ADP", string.Concat("Process is halt due to reason - We do not have both the PI and the ED yet - processing halted."), "");
                }
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - ProcessEstimate_v2 - ADP", ex.Message, "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                objEstimateDataDom = null;
                xmlTransDocument = null;
                objDataXfer = null;
            }
        }

        /// <summary>
        /// Decodes digital images and adds them to the document system one at a time.
        /// Note that it does not deal with Thumbnails even though the document contains
        /// them.  We generate thumbnails on the fly in the APD application.
        /// Returns the APD Document ID of the added document.
        /// </summary>
        internal long ProcessBinaryFiles(ref XmlDocument objDom, string strNodeNames, string strCreateDateNodeNames, string strCreateTimeNodeNames, string strFileExtension, string strDocType, EDocumentType enmDocType)
        {
            XmlNodeList objNodeList = null;
            XmlNodeList objCreateDateNodeList = null;
            XmlNodeList objCreateTimeNodeList = null;
            XmlElement objTempElement = null;
            object objTempArrayObject = null;

            int intImageCount = 0;
            long lngProcessBinaryFiles = 0;
            string strDate = string.Empty; // Holds a date and time used to make filenames unique
            string strFileName = string.Empty;
            string strCreatedDate = string.Empty;
            byte[] arrBuffer = null;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles", string.Concat("ProcessBinaryFiles Started"), "");
                mobjBase64utils = new MBase64Utils();

                //Build the current date for dissection by the app.
                strDate = mobjLAPDPartnerMgr.BuildFileNameDate();

                // Create the object collection containing the encoded pics
                objNodeList = objDom.SelectNodes(strNodeNames);

                if (!string.IsNullOrEmpty(strCreateDateNodeNames))
                    objCreateDateNodeList = objDom.SelectNodes(strCreateDateNodeNames);

                if (!string.IsNullOrEmpty(strCreateTimeNodeNames))
                    objCreateTimeNodeList = objDom.SelectNodes(strCreateTimeNodeNames);

                //Loop through each image and store them in separate files.
                for (intImageCount = 0; intImageCount <= objNodeList.Count - 1; intImageCount++)
                {
                    //Create a unique file name for the document.\
                    strFileName = BuildFinalFileName(intImageCount, strDate, strDocType);
                    strFileName = (string.Concat(mobjLAPDPartnerMgr.GetConfig("Document/RootDirectory"), strFileName)).Replace("\\\\", "\\");

                    //Create binary data from the Base64 encoded info.
                    objTempElement = (XmlElement)objNodeList.Item(intImageCount);

                    objTempArrayObject = mobjBase64utils.ReadBinaryDataFromDomElement(objTempElement);
                    arrBuffer = Convert.FromBase64String(Convert.ToString(objTempArrayObject));
                    //Build created date in format "yyyy-mm-ddThh:mm:ss"
                    if (((objCreateDateNodeList != null)) && ((objCreateTimeNodeList != null)))
                    {
                        if ((intImageCount < objCreateDateNodeList.Count) && (intImageCount < objCreateTimeNodeList.Count))
                            strCreatedDate = mobjLAPDPartnerMgr.ConstructSqlDateTime(objCreateDateNodeList.Item(intImageCount).InnerText, objCreateTimeNodeList.Item(intImageCount).InnerText);
                    }
                    //Write the binary data to a file in the document structure.
                    mobjBase64utils.WriteBinaryDataToFile(strFileName, arrBuffer);
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles", string.Concat("Document Created ", "File Name =", strFileName), "");
                    //Insert new document record into APD database
                    //TODO issue while insert the data using CCC Digital Image
                    lngProcessBinaryFiles = InsertLAPDDocument(enmDocType, Convert.ToInt64(objShared.g_strAssignmentID), Convert.ToInt64(objShared.g_strLynxID), Convert.ToInt32(objShared.g_strVehicleNumber), Convert.ToInt32(mintEstimateSequenceNumber), BuildPartnerTransID(), BuildFinalFileName(intImageCount, strDate, strDocType), strFileExtension, strCreatedDate, false);

                    objTempElement = null;
                    objTempArrayObject = null;
                }
                //Add another trace message to note exit
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles", string.Concat("ProcessBinaryFiles Completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles", string.Concat(ex.Message, "Trace: ", ex.StackTrace), "");
                throw ex;
            }
            finally
            {
                objNodeList = null;
                objCreateDateNodeList = null;
                objWritelog = null;
                objCreateTimeNodeList = null;
            }
            return lngProcessBinaryFiles;
        }

        /// <summary>
        /// Decodes digital images and adds them to the document system one at a time.
        /// Note that it does not deal with Thumbnails even though the document contains
        /// them.  We generate thumbnails on the fly in the APD application.
        /// Returns the APD Document ID of the added document. 
        /// </summary>
        internal long ProcessBinaryFiles_v2(ref XmlDocument objDom, string strNodeNames, string strCreateDateNodeNames, string strCreateTimeNodeNames, string strFileExtension, string strDocType, EDocumentType enmDocType)
        {
            XmlNodeList objNodeList = null;
            XmlNodeList objCreateDateNodeList = null,
            objCreateTimeNodeList = null, nodeListTransactionID = null;
            FileSystemObject objFSO = null;
            XmlElement objTempElement = null;
            object objTempArrayObject = null;

            int intImageCount = 0;
            long lngProcessBinaryFiles = 0;
            string strDate = string.Empty; // Holds a date and time used to make filenames unique
            string strDocumentRoot = string.Empty;
            string strFileName = string.Empty;
            string strCreatedDate = string.Empty, strTransactionID = string.Empty;
            byte[] arrBuffer = null;

            //check if the recommended file is unique. else find a unique file
            string strOutFilePath = string.Empty;
            string strOutFileName = string.Empty;
            string strOutFileExt = string.Empty;
            string strOutFileDBPath = string.Empty;
            string strBase64Value = string.Empty;
            int intDupCount = 0;
            Common.WriteLog objWritelog = new WriteLog();
            try
            {                
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles_v2", string.Concat("ProcessBinaryFiles_v2 Started"), "");
                mobjBase64utils = new MBase64Utils();

                //Build the current date for dissection by the app.
                strDate = mobjLAPDPartnerMgr.BuildFileNameDate();

                // Create the object collection containing the encoded pics
                objNodeList = objDom.SelectNodes(strNodeNames);

                if (!string.IsNullOrEmpty(strCreateDateNodeNames))
                    objCreateDateNodeList = objDom.SelectNodes(strCreateDateNodeNames);

                if (!string.IsNullOrEmpty(strCreateTimeNodeNames))
                    objCreateTimeNodeList = objDom.SelectNodes(strCreateTimeNodeNames);

                if (objDom.SelectSingleNode("/ADPTransaction/@TransactionID") != null)
                    strTransactionID = objDom.SelectSingleNode("/ADPTransaction/@TransactionID").Value;

                objWritelog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles_v2", string.Concat("TransactionID with File Name: ", strTransactionID), "");

                //Loop through each image and store them in separate files.
                for (intImageCount = 0; intImageCount <= objNodeList.Count - 1; intImageCount++)
                {
                    strFileName = string.Empty;
                    arrBuffer = null;
                    //Create a unique file name for the document.
                    strFileName = BuildFinalFileName_v2(intImageCount, strDate, strDocType);
                    strDocumentRoot = mobjLAPDPartnerMgr.GetConfig("Document/RootDirectory");
                    strFileName = string.Concat(strDocumentRoot, strFileName).Replace("\\\\", "\\");

                    //Create binary data from the Base64 encoded info.
                    objTempElement = (XmlElement)objNodeList.Item(intImageCount);
                    objTempArrayObject = mobjBase64utils.ReadBinaryDataFromDomElement(objTempElement);
                    arrBuffer = Convert.FromBase64String(Convert.ToString(objTempArrayObject));

                    //Build created date in format "yyyy-mm-ddThh:mm:ss"
                    if (((objCreateDateNodeList != null)) && ((objCreateTimeNodeList != null)))
                    {
                        if ((intImageCount < objCreateDateNodeList.Count) && (intImageCount < objCreateTimeNodeList.Count))
                            strCreatedDate = mobjLAPDPartnerMgr.ConstructSqlDateTime(objCreateDateNodeList.Item(intImageCount).InnerText, objCreateTimeNodeList.Item(intImageCount).InnerText);
                    }
                    objFSO = new FileSystemObject();

                    strOutFilePath = objFSO.GetParentFolderName(strFileName);
                    strOutFileName = objFSO.GetBaseName(strFileName);
                    strOutFileExt = objFSO.GetExtensionName(strFileName);
                    intDupCount = 1;
                    while (objFSO.FileExists(strFileName) == true)
                    {
                        strFileName = objFSO.BuildPath(strOutFilePath, string.Concat(new string[] { strOutFileName, "_", Convert.ToString(intDupCount), ".", strOutFileExt }));
                        intDupCount = intDupCount + 1;
                    }
                    objFSO = null;
                    strOutFileDBPath = Mid(strFileName, (strDocumentRoot.Replace("\\\\", "\\")).Length);
                    strOutFileDBPath = strOutFileDBPath.Replace("\\", "\\\\");

                    //Write the binary data to a file in the document structure.
                    mobjBase64utils.WriteBinaryDataToFile(strFileName, arrBuffer);
                    if (isExtensiveDebug)
                        objWritelog.LogEvent("COM Conversion", "DEBUGGING", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles_v2", string.Concat("Document Created ", "File Name =", strFileName), "");

                    //Insert new document record into APD database
                    lngProcessBinaryFiles = InsertLAPDDocument_v2(enmDocType, Convert.ToInt64(mstrAssignmentID), Convert.ToInt64(mstrLynxID), Convert.ToInt32(mstrVehicleNumber), Convert.ToInt32(mintEstimateSequenceNumber), BuildPartnerTransID(), strOutFileDBPath, strFileExtension, strCreatedDate, false);

                    objTempElement = null;
                    objTempArrayObject = null;
                }
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles_v2", string.Concat("ProcessBinaryFiles_v2 Completed"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - ProcessBinaryFiles_v2", ex.Message, "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                objNodeList = null;
                objCreateDateNodeList = null;
                objCreateTimeNodeList = null;
                objFSO = null;
            }
            return lngProcessBinaryFiles;
        }

        /// <summary>
        /// Send a receipt back to Partner.
        /// </summary>
        internal void SendAReceipt(string strTemplateFile, string strSuccess = "Y", long lngErrorCode = 0, string strErrorDesc = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt: " });
            XmlDocument objLYNXReceipt = null, xmlTransDocument = null;
            XmlNode objNode = null;

            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransactionID = string.Empty;
            string strPath = string.Empty;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                mobjMDomUtils = new MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "Start", "PartnerDataMgr.CPartner.cs - SendAReceipt", string.Concat("SendAReceipt started"), "");

                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.SendAReceipt: Issue while loading the transaction xml.");

                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();

                strPath = string.Concat(new string[] { objShared.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //temporary variable.
                strPath = string.Empty;

                strTransactionID = (mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionID").Trim());
                if (strTransactionID.Length == 0)
                    strTransactionID = (mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//SenderTransactionId").Trim());

                //Fill out the template with values from the submitted document.
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", strTransactionID });
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionDate", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionTime", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@AssignmentID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@AssignmentID");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@LynxID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@LynxID");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@VehicleID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@VehicleID");

                //TaskID field is optional (specific to SG).
                objNode = mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TaskID");
                if ((objNode != null))
                    objNode.InnerText = objShared.g_strTaskID;

                //ClaimNumber field is optional (specific to Grange).
                objNode = mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//ClaimNumber");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//ClaimNumber", false);

                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//ReceiptFor", true).InnerText = strTransactionID;
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Date", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionDate");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Time", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionTime");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//SuccessFlag", true).InnerText = strSuccess;
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//FailureReasonCode", true).InnerText = Convert.ToString(lngErrorCode);
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//FailureReasonText", true).InnerText = strErrorDesc;

                //Build the SOAP Envelope and put the receipt into the envelope               
                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartnerMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");
                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - SendAReceipt", string.Concat(" SendAReceipt completed. Receipt sent via HTTP"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.CPartner.cs - SendAReceipt", ex.Message, "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                objNode = null;
                xmlTransDocument = null;
                objLYNXReceipt = null;
            }
        }

        /// <summary>
        /// Send a receipt back to Partner.
        /// </summary>
        internal void SendAReceipt_v2(string strTemplateFile, string strSuccess = "Y", long lngErrorCode = 0, string strErrorDesc = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt_v2: " });

            XmlDocument objLYNXReceipt = null, xmlTransDocument = null;
            XmlNode objNode = null;

            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransactionID = string.Empty;
            string strPath = string.Empty;
            Common.WriteLog objWritelog = null;
            try
            {
                objWritelog = new WriteLog();
                mobjMDomUtils = new MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "Start", "PartnerDataMgr.CPartner.cs - SendAReceiptv2", string.Concat("SendAReceiptv2 Started"), "");

                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.SendAReceipt_v2: Issue while loading the transaction xml.");
                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();
                strPath = string.Concat(new string[] { objShared.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //temporary variable.
                strPath = string.Empty;

                strTransactionID = (mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionID").Trim());

                if (strTransactionID.Length == 0)
                    strTransactionID = (mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//SenderTransactionId").Trim());

                //Fill out the template with values from the submitted document.
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", strTransactionID });
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionDate", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TransactionTime", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@AssignmentID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@AssignmentID");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@LynxID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@LynxID");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@VehicleID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@VehicleID");

                //TaskID field is optional (specific to SG).
                objNode = mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//@TaskID");
                if ((objNode != null))
                    objNode.InnerText = objShared.g_strTaskID;

                //ClaimNumber field is optional (specific to Grange).
                objNode = mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//ClaimNumber");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref mobjPassedDocument, "//ClaimNumber", false);

                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//ReceiptFor", true).InnerText = strTransactionID;
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Date", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionDate");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Time", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionTime");
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//SuccessFlag", true).InnerText = strSuccess;
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//FailureReasonCode", true).InnerText = Convert.ToString(lngErrorCode);
                mobjLAPDPartnerMgr.GetChildNode(ref objLYNXReceipt, "//FailureReasonText", true).InnerText = strErrorDesc;

                //Build the SOAP Envelope and put the receipt into the envelope                
                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, mstrPartner, string.Concat(new string[] { mstrLynxID, "-", mstrVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartnerMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendAReceiptv2", string.Concat("Posting XML to PPG URL- ", strPost2URL, "Acknowledgement XML - ", strWrappedReceipt), "");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);
                if (isExtensiveDebug)
                    objWritelog.LogEvent("COM Conversion", "END", "PartnerDataMgr.CPartner.cs - SendAReceiptv2", string.Concat("Receipt sent via HTTP"), "");
            }
            catch (Exception ex)
            {
                objWritelog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.CPartner.cs - SendAReceiptv2", ex.Message, "");
                throw ex;
            }
            finally
            {
                objWritelog = null;
                xmlTransDocument = null;
                objNode = null;
                objLYNXReceipt = null;
            }
        }

        /// <summary>
        /// Send a HeartBeat back to Partner.
        /// </summary>
        internal void SendHeartBeatResponse(string strTemplateFile)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatResponse: " });

            //Object vars that need clean up.
            XmlDocument objHeartBeatDom = null, xmlTransDocument = null;

            //Primitive vars that dont need clean up.
            string strWrappedHeartBeat = string.Empty;
            string strPost2URL = string.Empty;
            string strPath = string.Empty;
            try
            {
                mobjMDomUtils = new MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.SendHeartBeatResponse: Issue while loading the transaction xml.");

                //First load the HeartBeat template from disk.
                objHeartBeatDom = new XmlDocument();
                strPath = string.Concat(new string[] { objShared.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objHeartBeatDom, ref strPath, "SendAHeartBeat", "HeartBeat");

                //temporary variable.
                strPath = string.Empty;

                //Fill out the template with values from the submitted document.
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionID") });
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//@TransactionDate", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//@TransactionTime", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//@AssignmentID", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@AssignmentID");

                //Copy the origination date/time
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@Date", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//OriginationDate/DateTime/@Date");
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@Time", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//OriginationDate/DateTime/@Time");
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@TimeZone", true).InnerText = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//OriginationDate/DateTime/@TimeZone");

                //Set the received date/time
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//ReceivedDate/DateTime/@Date", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlDate(Convert.ToString(objShared.g_varDocumentReceivedDate));
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//ReceivedDate/DateTime/@Time", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlTime(Convert.ToString(objShared.g_varDocumentReceivedDate));

                //Set the reply date/time
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//ReplyDate/DateTime/@Date", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlDate(Convert.ToString(objShared.g_varDocumentReceivedDate));
                mobjLAPDPartnerMgr.GetChildNode(ref objHeartBeatDom, "//ReplyDate/DateTime/@Time", true).InnerText = mobjLAPDPartnerMgr.ConstructXmlTime(Convert.ToString(objShared.g_varDocumentReceivedDate));

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedHeartBeat = mobjMPPGTransactions.BuildTransactionForPPG("NetworkStatusTest", objHeartBeatDom.OuterXml, objShared.g_strPartner, string.Concat(new string[] { objShared.g_strLynxID, "-", objShared.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartnerMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedHeartBeat);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objHeartBeatDom = null;
            }
        }

        /// <summary>
        /// Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        /// </summary>
        internal void ExtractLynxIDAndVehNum()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractLynxIDAndVehNum: " });
            XmlDocument xmlTransDocument = null;
            string strDate = string.Empty;
            string strTime = string.Empty;
            try
            {
                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ExtractLynxIDAndVehNum: Issue while loading the transaction xml.");

                mstrActualLynxID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@LynxID");
                objShared.g_strActualLynxID = mstrActualLynxID;      //GetChildNodeText(g_objPassedDocument, "//@LynxID")

                mstrLynxID = mstrActualLynxID;
                objShared.g_strLynxID = mstrLynxID;                  // g_strActualLynxID

                mstrAssignmentID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@AssignmentID");
                objShared.g_strAssignmentID = mstrAssignmentID;      //GetChildNodeText(g_objPassedDocument, "//@AssignmentID")

                mstrTaskID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TaskID");
                objShared.g_strTaskID = mstrTaskID;                  // GetChildNodeText(g_objPassedDocument, "//@TaskID")

                mstrVehicleNumber = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@VehicleID");
                objShared.g_strVehicleNumber = mstrVehicleNumber;    //GetChildNodeText(g_objPassedDocument, "//@VehicleID")

                mstrActionCode = Left(mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//AssignmentCode"), 1);
                objShared.g_strActionCode = mstrActionCode;          // Left(GetChildNodeText(g_objPassedDocument, "//AssignmentCode"), 1)

                //Special case for ADP - Glaxis was missing the pass through data.
                //In these cases Glaxis sets the LynxID and VehicleNumber to "UNK"
                //and the AssignmentID to "0".  See Test Track #1785.
                //If g_strVehicleNumber <> "UNK" Then
                if (mstrVehicleNumber != "UNK")
                {
                    mstrVehicleNumber = mobjLAPDPartnerMgr.ZeroPad((string.IsNullOrEmpty(objShared.g_strVehicleNumber) ? 0 : Convert.ToInt32(objShared.g_strVehicleNumber)), 3);
                    objShared.g_strVehicleNumber = mstrVehicleNumber; //ZeroPad(IIf(g_strVehicleNumber = "", 0, g_strVehicleNumber), 3)
                }

                //Extract the assignment / cancel character.
                //If g_strActionCode <> "C" Then
                if (mstrActionCode != "C")
                {
                    mstrActionCode = "N";
                    objShared.g_strActionCode = mstrActionCode;         //"N"
                }

                //Extract the transaction sent date/time.
                if ((mobjLAPDPartnerMgr.GetChildNode(ref xmlTransDocument, "//@TransactionDate") == null) || (mobjLAPDPartnerMgr.GetChildNode(ref xmlTransDocument, "//@TransactionTime") == null))
                {
                    strDate = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionDate", true);
                    strTime = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionTime", true);
                }
                else
                {
                    strDate = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionDate", false);
                    strTime = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionTime", false);
                }
                mvarTransactionDate = string.Concat(new string[] { Left(strDate, 2), "/", Mid(strDate, 3, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Mid(strTime, 3, 2), ":", Right(strTime, 2) });
                objShared.g_varTransactionDate = mvarTransactionDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        /// </summary>
        internal void ExtractLynxIDAndVehNum_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractLynxIDAndVehNum_v2: " });
            XmlDocument xmlTransDocument = null;
            string strDate = string.Empty;
            string strTime = string.Empty;
            try
            {
                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ExtractLynxIDAndVehNum_v2: Issue while loading the transaction xml.");

                mstrActualLynxID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@LynxID");   //TODO:mobjPassedDocument replaced by MLAPDPartnerDataMgr.g_objPassedDocument
                objShared.g_strActualLynxID = mstrActualLynxID;

                mstrLynxID = mstrActualLynxID;
                objShared.g_strLynxID = mstrLynxID;

                mstrAssignmentID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@AssignmentID");
                objShared.g_strAssignmentID = mstrAssignmentID;

                mstrTaskID = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TaskID");
                objShared.g_strTaskID = mstrTaskID;

                mstrVehicleNumber = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@VehicleID");
                objShared.g_strVehicleNumber = mstrVehicleNumber;

                mstrActionCode = Left(mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//AssignmentCode"), 1);
                objShared.g_strActionCode = mstrActionCode;

                //Special case for ADP - Glaxis was missing the pass through data.
                //In these cases Glaxis sets the LynxID and VehicleNumber to "UNK"
                //and the AssignmentID to "0".  See Test Track #1785.
                if (mstrVehicleNumber != "UNK")
                    mstrVehicleNumber = mobjLAPDPartnerMgr.ZeroPad((string.IsNullOrEmpty(mstrVehicleNumber) ? 0 : Convert.ToInt32(mstrVehicleNumber)), 3);

                //Extract the assignment / cancel character.
                if (mstrActionCode != "C")
                    mstrActionCode = "N";

                //Extract the transaction sent date/time.
                if ((mobjLAPDPartnerMgr.GetChildNode(ref xmlTransDocument, "//@TransactionDate") == null) || (mobjLAPDPartnerMgr.GetChildNode(ref xmlTransDocument, "//@TransactionTime") == null))
                {
                    strDate = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionDate", true);
                    strTime = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//TransactionTime", true);
                }
                else
                {
                    strDate = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionDate", false);
                    strTime = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, "//@TransactionTime", false);
                }
                mvarTransactionDate = string.Concat(new string[] { Left(strDate, 2), "/", Mid(strDate, 2, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Mid(strTime, 2, 2), ":", Right(strTime, 2) });
                objShared.g_varTransactionDate = mvarTransactionDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlTransDocument = null;
            }
        }

        /// <summary>
        /// Builds a friendly description string for the database.
        /// </summary>
        internal string BuildPartnerTransID()
        {
            string strBuildPartnerTransID = string.Empty;
            try
            {

                if (menmDocType == CPartner.EDocumentType.eEstimateData || menmDocType == CPartner.EDocumentType.ePrintImage)
                    strBuildPartnerTransID = string.Concat(mobjLAPDPartnerMgr.ZeroPad(mlngExistingPrintImageID, 3), ";ED-ID:", mobjLAPDPartnerMgr.ZeroPad(mlngExistingEstimateDataID, 3));
                else
                    strBuildPartnerTransID = string.Concat(mobjLAPDPartnerMgr.ZeroPad(mlngPartnerTransID, 3), ";ED-ID:000");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strBuildPartnerTransID;
        }

        //Conducts a shop search based upon the passed document search parameters
        // strSearchElemPath is the path to the search element that contains the
        //shop search parameters.
        internal string ProcessShopSearchRequest(string strSearchElemPath, string strSourceCodePath = "//@Originator", string strZipCodePath = "//Zip", string strAreaCodePath = "//Area", string strExchangePath = "//Exchange")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessShopSearchRequest: " });
            //Object vars that need clean up.
            XmlElement objSearchElem = null;
            XmlDocument xmlTransDocument = null;
            //Primitive vars that dont need clean up.
            string strShopListXml = string.Empty;
            string strSourceCode = string.Empty;
            int intCompanyID = 0;
            string strAreaCode = string.Empty;
            string strExchange = string.Empty;
            string strZipCode = string.Empty;
            string strShopSearchRequest = string.Empty;
            Hashtable hashTable = null;
            try
            {
                xmlTransDocument = new XmlDocument();
                if (!string.IsNullOrEmpty(objShared.g_strPassedDocument.Trim()))
                    xmlTransDocument.LoadXml(objShared.g_strPassedDocument);
                else
                    throw new Exception("CPartner.ProcessShopSearchRequest: Issue while loading the transaction xml.");

                //Get the carrier specific company code.
                strSourceCode = mobjLAPDPartnerMgr.GetChildNodeText(ref xmlTransDocument, strSourceCodePath, true);

                //Locate the shop search parameter element.
                objSearchElem = (XmlElement)mobjLAPDPartnerMgr.GetChildNode(ref xmlTransDocument, strSearchElemPath, true);

                //Extract some parameters for the shop search.

                strAreaCode = objSearchElem.SelectSingleNode(strAreaCodePath).InnerText;
                strExchange = objSearchElem.SelectSingleNode(strExchangePath).InnerText;
                strZipCode = objSearchElem.SelectSingleNode(strZipCodePath).InnerText;

                //Translate the partner company source code to the APD company id.
                intCompanyID = Convert.ToInt32(mobjLAPDPartnerMgr.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Carrier[@SourceCode='", strSourceCode, "']/@CompanyID" })));

                //Set the connect string to APD for XML retrieval.
                //Zip code only search.
                if (!string.IsNullOrEmpty(strZipCode))
                {
                    hashTable = new Hashtable();
                    hashTable.Add("@InsuranceCompanyID", intCompanyID);
                    hashTable.Add("@ShopTypeCode", "P");
                    hashTable.Add("@Zip", strZipCode);
                    strShopListXml = objCdataAccessor.ExecuteSpNamedParamsXML(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/ShopSearchSP"), hashTable, "APD");
                }
                //Area code and exchange search.
                else if (!string.IsNullOrEmpty(strAreaCode) && !string.IsNullOrEmpty(strExchange))
                {
                    hashTable = new Hashtable();
                    hashTable.Add("@InsuranceCompanyID", intCompanyID);
                    hashTable.Add("@ShopTypeCode", "P");
                    hashTable.Add("@AreaCode", strAreaCode);
                    hashTable.Add("@ExchangeNumber", strExchange);
                    strShopListXml = objCdataAccessor.ExecuteSpNamedParamsXML(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/ShopSearchSP"), hashTable, "APD");
                }
                //Not enough information to do the search - Kaboom!
                else
                    throw new Exception(string.Concat(new string[] { Convert.ToString(CPartner_Error_Codes.eShopSearchMissingParams), PROC_NAME, "Shop Search XML missing required parameters." }));

                //Return the results.
                strShopSearchRequest = strShopListXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objSearchElem = null;
                xmlTransDocument = null;
            }
            return strShopSearchRequest;
        }

        /// <summary>
        ///  Converts a byte array buffer containing the binary data for an EMS DBF package file
        /// (zip file containing files for EMS) and returns the data in XML format.
        /// </summary>
        internal XmlDocument ConvertEmsDBFToXml(ref byte[] buffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ConvertEmsDBFToXml: " });

            XmlDocument mobjXmlDoc = null;
            FileSystemObject mobjFileSystemObject = null;
            Scripting.File objFile = null;

            Guid GenerateGUID = default(Guid);
            XmlDocument ConvertEmsDBFToXml = null;
            FileInfo objFileInfo = null;
            string[] files = null;
            string[] rootFiles = null;

            string strWorkRootPath = string.Empty,
             strGUID = string.Empty,
             strWorkPath = string.Empty,
             strPackageFile = string.Empty,
             strFileType = string.Empty;
            try
            {
                mobjXmlDoc = new XmlDocument();
                mobjFileSystemObject = new FileSystemObject();
                mobjBase64utils = new MBase64Utils();

                //MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { PROC_NAME, "executed." }));
                strWorkRootPath = mobjLAPDPartnerMgr.GetConfig("PartnerSettings/TempWorkSpace");
                //MLAPDPartnerDataMgr.g_objEvents.Assert(!string.IsNullOrEmpty(strWorkRootPath), "Temp Work Space specified.");
                // MLAPDPartnerDataMgr.g_objEvents.Assert(mobjFileSystemObject.FolderExists(strWorkRootPath), "Temp Work Space specified does not exist.");

                // Generate the temporary work folder for this transaction
                GenerateGUID = Guid.NewGuid();
                strWorkPath = mobjFileSystemObject.BuildPath(strWorkRootPath, GenerateGUID.ToString());
                strPackageFile = mobjFileSystemObject.BuildPath(strWorkPath, "\\ems.zip");
                // First we need to write the buffer out to a file.
                mobjFileSystemObject.CreateFolder(strWorkPath);
                mobjBase64utils.WriteBinaryDataToFile(strPackageFile, buffer);

                //Extract the individual files from package
                ZipFile objZipped = ZipFile.Read(strPackageFile);
                objZipped.ExtractAll(strWorkPath);
                objZipped.Dispose();

                // objUnZip.ZipFile = strPackageFile;
                //objUnZip.UnzipFolder = strWorkPath;
                //objUnZip.Unzip();

                objZipped = null;
                System.IO.File.Delete(strPackageFile);
                // mobjFileSystemObject.DeleteFile(strPackageFile, true);

                // Now cycle through the files and import each file's information into XML document
                // objFolder = mobjFileSystemObject.GetFolder(strWorkPath); 
                rootFiles = Directory.GetDirectories(strWorkPath);

                if (rootFiles.Length > 0)
                    files = Directory.GetFiles(rootFiles[0]);

                mobjXmlDoc.LoadXml("<EMS/>");
                foreach (string file in files)
                {
                    objFileInfo = new FileInfo(file);
                    //objFile = varobjFile;
                    //strFileType = (Right(objFile.ShortName, 3)).ToUpper();
                    strFileType = objFileInfo.Extension;
                    strFileType = strFileType.Remove(0, 1);
                    switch (strFileType.ToUpper())
                    {
                        case "AD1":
                        case "AD2":
                        case "ENV":
                        case "VEH":
                        case "TTL":
                        case "VEN":
                        case "PFP":
                        case "PFT":
                        case "PFM":
                        case "PFL":
                        case "PFH":
                        case "LIN":
                        case "STL":
                            AddDBFTableDataToXml(strFileType.ToUpper(), file, ref objFileInfo, ref mobjXmlDoc);
                            break;
                        default:
                            break;
                        // Do Nothing.
                    }
                }
                // objFile.Delete True
                ConvertEmsDBFToXml = mobjXmlDoc;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objFile = null;
                mobjFileSystemObject = null;
                mobjXmlDoc = null;
            }
            return ConvertEmsDBFToXml;
        }

        /// <summary>
        /// AddDBFTableDataToXml
        /// </summary>
        /// <param name="strElementName"></param>
        /// <param name="strWorkPath"></param>
        /// <param name="objDBFFile"></param>
        /// <param name="objDoc"></param>
        private void AddDBFTableDataToXml(string strElementName, string strWorkPath, ref FileInfo objDBFFile, ref XmlDocument objDoc)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "AddDBFTableDataToXml: " });

            ADODB.Recordset objRS = null;
            ADODB.Connection adodbConnection = null;
            XmlElement objRecord = null;
            XmlElement objField = null;

            int intRecord = 0;
            int intRecordcolumn = 0;
            string strFileName = string.Empty;
            string strDeletePath = string.Empty;
            string strPattern = string.Empty;
            string strSubstitution = string.Empty, strProcessPath = string.Empty;

            try
            {
                adodbConnection = new ADODB.Connection();
                objRS = new ADODB.Recordset();
                objRecord = null;
                objField = null;
                mobjMDomUtils = new MDomUtils();

                strPattern = string.Concat(".", strElementName);
                strSubstitution = ".dbf";
                strFileName = objDBFFile.Name;
                strProcessPath = strWorkPath.Replace(strPattern.ToLower(), strSubstitution);
                // strFileName = Regex.Replace(Convert.ToString(objDBFFile), strPattern, strSubstitution, RegexOptions.IgnoreCase);
                objDBFFile.CopyTo(string.Concat(strProcessPath), true);
                adodbConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                adodbConnection.Open(string.Concat("Driver={Microsoft dBASE Driver (*.dbf)};Dbq=", objDBFFile.DirectoryName));

                objRS.Open(string.Concat(new string[] { "select * from ", strFileName }), adodbConnection, CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, Convert.ToInt32(CommandTypeEnum.adCmdText));

                if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                {
                    while (!objRS.EOF)
                    {
                        // objRecord = mDomUtils.AddElement(ref objDoc.DocumentElement, strElementName);
                        //objRecord = objDoc.DocumentElement;
                        //objRecord = mobjMDomUtils.AddElement(ref objRecord, strElementName);

                        //objRecord = mobjMDomUtils.AddElement(ref objDoc.DocumentElement, strElementName);                        // objDoc.AppendChild(objRecord);                        
                        objRecord = objDoc.CreateElement(strElementName);

                        for (intRecordcolumn = 0; intRecordcolumn <= objRS.Fields.Count - 1; intRecordcolumn++)
                        {
                            objField = mobjMDomUtils.AddElement(ref objRecord, objRS.Fields[intRecordcolumn].Name);

                            if ((objRS.Fields[intRecordcolumn].Value) != null)
                            {
                                switch (objRS.Fields[intRecordcolumn].Type)
                                {
                                    case DataTypeEnum.adDate:
                                    case DataTypeEnum.adDBDate:
                                        if (objRS.Fields[intRecordcolumn].Value != DBNull.Value && objRS.Fields[intRecordcolumn].Value != string.Empty)
                                            objField.InnerText = Convert.ToString(DateTime.Parse(Convert.ToString(objRS.Fields[intRecordcolumn].Value)));
                                        else
                                            objField.InnerText = Convert.ToString(objRS.Fields[intRecordcolumn].Value);
                                        break;
                                    default:
                                        objField.InnerText = Convert.ToString(objRS.Fields[intRecordcolumn].Value);
                                        break;
                                }
                            }
                            else
                                objField.InnerText = "";
                        }
                        objRS.MoveNext();
                        objDoc.DocumentElement.AppendChild(objRecord);
                        Thread.Sleep(1000);
                    }
                }
                //objRS.Close();

                // strDeletePath = string.Concat(strProcessPath);
                System.IO.File.Delete(strProcessPath);

                //if ((objRS != null))
                //{
                //    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                //        objRS.Close();
                //}
                //objRS = null;
                //if ((adodbConnection != null))
                //{
                //    if (adodbConnection.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                //        adodbConnection.Close();
                //}
                //adodbConnection = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adodbConnection.Close();
                adodbConnection = null;
                //objRS.Close();
                objRS = null;
            }
        }

        /// <summary>
        /// SetDocumentSendTransactionID
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="TransactionID"></param>
        internal void SetDocumentSendTransactionID(string DocumentID, string TransactionID)
        {
            string PROC_NAME = string.Empty;
            Hashtable hashTable = null;
            try
            {
                PROC_NAME = "SetDocumentSendTransactionID:";
                hashTable = new Hashtable();
                hashTable.Add("@DocumentID", DocumentID);
                hashTable.Add("@TransactionID", TransactionID);
                objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/SetDocumentSendTransactionID"), hashTable, "APD");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UpdateDocumentSendStatus
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="SendStatus"></param>
        internal void UpdateDocumentSendStatus(string DocumentID, string SendStatus)
        {
            string PROC_NAME = string.Empty;
            Hashtable hashTable = null;
            try
            {
                PROC_NAME = "UpdateDocumentSendStatus:";
                hashTable = new Hashtable();
                hashTable.Add("@DocumentID", DocumentID);
                hashTable.Add("@SendStatusCD", SendStatus);

                objCdataAccessor.ExecuteSpNamedParams(mobjLAPDPartnerMgr.GetConfig("PartnerSettings/UpdateDocumentSendStatus"), hashTable, "APD");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string strLeft = string.Empty;
            try
            {
                if (param != string.Empty)
                    strLeft = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strLeft;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string strRight = string.Empty;
            try
            {
                if (param != string.Empty)
                    strRight = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strRight;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string strMid = string.Empty;
            try
            {
                if (param != string.Empty)
                    strMid = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string strMid = string.Empty;
            try
            {
                if (param != string.Empty)
                    strMid = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        #endregion
    }
}
