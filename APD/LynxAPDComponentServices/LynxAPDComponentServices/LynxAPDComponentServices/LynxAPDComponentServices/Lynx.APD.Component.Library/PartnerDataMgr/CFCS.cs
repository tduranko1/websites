﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using cDataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using System.Collections;


namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    //********************************************************************************
    // Component LAPDPartnerDataMgr : Class CFCS
    //
    // Processing methods specific to FCS
    //
    //********************************************************************************

    class CFCS
    {
        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CFCS." });
        private string clngFCSSystemUserID = string.Empty;
        CPartner mobjPartner = null;
        MLAPDPartnerDataMgr mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
        cDataAccessor.CDataAccessor objCDataAccessor = null;
        #endregion

        #region Enumerator

        private enum FCS_Error_Codes : ulong
        {
            eBadAssignmentCode = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CAutoVerse_CLS_ErrorCodes,
            eAssignmentInvalid,
            eAttachmentIdNotFound,
            eClaimNumberInvalid,
            eNoPassThruData,
            eBadPassThruDataFormat,
            eUnhandledAutoverseNack,
            eBlankCefReturnedForEstimate,
            eXmlTransactionTooLarge,
            eInternalError,
            eInvalidEventCode,
            eMissingExpectedDocumentNode
        }
        #endregion
               
        #region Private Helper function

        private string CPartner_ApdDatabaseTransfer()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer:" });
            try
            {                
                mobjPartner = new CPartner();                

                clngFCSSystemUserID = mLAPDPartnerDataMgr.GetDefaultPartnerUserID("FCS").ToString();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started." }));

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        ProcessReceipt();
                        break;

                    default:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type", mobjPartner.DocTypeString, " [", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Empty;
        }

        private bool CPartner_InitProcessDocument()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocument: " });
            string[] strAssignmentIDTokens = null;
            try
            {
                mobjPartner = new CPartner();                

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = MLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.Name;

                //Extract the LYNXID and Vehicle number.
                //mobjPartner.ExtractLynxIDAndVehNum

                strAssignmentIDTokens = Regex.Split(Convert.ToString(mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Header/ResponseFor", true)), "-", RegexOptions.IgnoreCase);
                //strAssignmentIDTokens = (mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//Header/ResponseFor", true)).Split('-');

                MLAPDPartnerDataMgr.g_strLynxID = strAssignmentIDTokens[0];
                MLAPDPartnerDataMgr.g_strVehicleNumber = strAssignmentIDTokens[1];


                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Are we configured to process this document at this time?
            return mobjPartner.CanProcessDocument();
        }

        private void ProcessReceipt()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessReceipt:" });
            string strReturnCode = string.Empty,
                strErrDesc = string.Empty,
                strNote = string.Empty,
                strClaimAspectID = string.Empty,
                strClaimAspectStatusID = string.Empty,
                strAppendFilename = string.Empty;
            XmlDocument objClaimVehicleInfo = null;
            Hashtable htParams = null;

            try
            {               
                mobjPartner = new CPartner();
                objCDataAccessor = new cDataAccessor.CDataAccessor();

               //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);
                htParams = new Hashtable();
                htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                htParams.Add("@InsuranceCompanyID", 185);
                strAppendFilename = objCDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/VehicleList"), htParams, "APD");
                //strAppendFilename =MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/VehicleList"), new object[] { "@LynxID", MLAPDPartnerDataMgr.g_strLynxID, "@InsuranceCompanyID", 185 });
                mLAPDPartnerDataMgr.LoadXml(ref objClaimVehicleInfo, ref strAppendFilename, PROC_NAME, "ProcessReceipt");

                strClaimAspectID = mLAPDPartnerDataMgr.GetChildNodeText(ref objClaimVehicleInfo, string.Concat(new string[] { "//Vehicle[@VehicleNumber='", MLAPDPartnerDataMgr.g_strVehicleNumber, "']/@ClaimAspectID" }));
                strClaimAspectStatusID = mLAPDPartnerDataMgr.GetChildNodeText(ref objClaimVehicleInfo, string.Concat(new string[] { "//Vehicle[@VehicleNumber='", MLAPDPartnerDataMgr.g_strVehicleNumber, "']/@StatusID" }));

                MLAPDPartnerDataMgr.g_objEvents.Assert((strClaimAspectID != string.Empty), "Could not determine Claim Aspect for Receipt received.");

                strReturnCode = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//ReturnCode");

                switch (strReturnCode.ToLower())
                {
                    case "success":
                        // Assignment successful
                        strNote = "Assignment to FCS successfully sent";
                        break;
                    default:
                        strNote = "Assignment rejected by FCS";
                        break;
                }

               //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringStd);
                htParams = new Hashtable();
                htParams.Add("@ClaimAspectID", strClaimAspectID);
                htParams.Add("@NoteTypeID", 1);
                htParams.Add("@StatusID", strClaimAspectStatusID);
                htParams.Add("@Note", strNote);
                htParams.Add("@UserID", clngFCSSystemUserID);
                objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), htParams, "APD");
               //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[]{
               //                    "@ClaimAspectID", strClaimAspectID,
               //                    "@NoteTypeID", 1,
               //                    "@StatusID", strClaimAspectStatusID,
               //                    "@Note", strNote,    
               //                    "@UserID", clngFCSSystemUserID});
            }
            catch (Exception ex)
            {
                objClaimVehicleInfo = null;
                throw ex;
            }
        }

        private bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer: " });
            string strError = string.Empty;
            try
            {
                mobjPartner = new CPartner();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                mobjPartner.StoreDocumentInPartnerDb();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            try
            {
                mobjPartner = new CPartner();
                return mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CPartner_SendHeartBeatRequest()
        {
            try
            {
                mobjPartner = new CPartner();
                mobjPartner.SendHeartBeatRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CPartner_TransferToPartner()
        {
            try
            {
                mobjPartner = new CPartner();
                mobjPartner.TransferToPartner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
