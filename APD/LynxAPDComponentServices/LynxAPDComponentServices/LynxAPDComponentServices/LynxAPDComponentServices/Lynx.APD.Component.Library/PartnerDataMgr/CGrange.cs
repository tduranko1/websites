﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Diagnostics;
using EcadMgr = Lynx.APD.Component.Library.EcadAccessorMgr;
using PartnerDataMgr = Lynx.APD.Component.Library;
using cDataAccessor = Lynx.APD.Component.Library.APDDataAccessor;
using System.Collections;


namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    ///  Component LAPDPartnerDataMgr : Class CGrange
    ///  Processing methods specific to Grange
    ///  </summary>
    public class CGrange
    {
        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CGrange." });
        PartnerDataMgr.MLAPDPartnerDataMgr mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
        PartnerDataMgr.MPPGTransactions objMPPGTransactions = null;
        PartnerDataMgr.MBase64Utils objMBase64Utils = null;
        PartnerDataMgr.MFlatfile objMFlatfile = null;
        PartnerDataMgr.CPartner mobjPartner = null;
        PartnerDataMgr.MEscapeUtilities objMEscapeUtilities = null;
        Common.MDomUtils objMDomUtils = null;
        PartnerDataMgr.CAutoverse objCAutoverse = null;
        EcadMgr.CWebAssignment objCWebAssignment = null;
        cDataAccessor.CDataAccessor objCDataAccessor = null;
        #endregion

        #region Enumerators

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        private enum Grange_Error_Codes : ulong
        {
            eBadAssignmentCode = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CGrange_CLS_ErrorCodes,
            eAssignmentInvalid,
            eAttachmentIdNotFound,
            eNoPassThruData,
            eBadPassThruDataFormat,
            eUnhandledGrangeNack,
            eBlankCefReturnedForEstimate,
            eXmlTransactionTooLarge
        }

        private enum Lynx_NACK_Codes
        {
            eClaimNumberInvalid = 1
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_InitProcessDocument()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocument: " });
            string strClaimNumber = string.Empty,
                strInsuranceCompanyID = string.Empty,
                strProc = string.Empty,
                strAppend = string.Empty;
            XmlDocument objDom = null;
            XmlElement objElem = null;
            Hashtable htparams = null;
            try
            {
                mobjPartner = new CPartner();
                objMDomUtils = new Common.MDomUtils();
                objCDataAccessor = new cDataAccessor.CDataAccessor();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = MLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.BaseURI;

                //Extract the LYNXID and Vehicle number.
                //mobjPartner.ExtractLynxIDAndVehNum

                //Retrieve parameter values for stored procedure.
                strProc = mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheck");
                strClaimNumber = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//ClaimNumber", true);
                strInsuranceCompanyID = mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", MLAPDPartnerDataMgr.g_strPartner, "/@InsuranceCompanyID" }));

                // Add a new element to the assignment XML to contain the Insurance Company ID.
                objElem = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref MLAPDPartnerDataMgr.g_objPassedDocument, "", true);
                objElem = objMDomUtils.AddElement(ref objElem, "InsuranceCompanyID");
                objElem.InnerText = strInsuranceCompanyID;

                //Set connection string
                //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                objDom = new XmlDocument();

                // Call stored procedure to retrieve claim info based on client claim number.  Load results into a DOM object.
                htparams = new Hashtable();
                htparams.Add("@ClientClaimNumber", strClaimNumber);
                htparams.Add("@InsuranceCompanyID", strInsuranceCompanyID);

                strAppend = objCDataAccessor.ExecuteSpNamedParamsXML(strProc, htparams, "APD");

                // strAppend = MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(strProc, new object[] { "@ClientClaimNumber", strClaimNumber, "@InsuranceCompanyID", strInsuranceCompanyID });
                mLAPDPartnerDataMgr.LoadXml(ref objDom, ref strAppend, PROC_NAME, "CPartner_InitProcessDocument");

                // Set LynxID and VehicleNumber from DOM data.  These should only exist for Grange if this is cancellation request.
                MLAPDPartnerDataMgr.g_strLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref objDom, "//@LynxID");
                MLAPDPartnerDataMgr.g_strVehicleNumber = mLAPDPartnerDataMgr.GetChildNodeText(ref objDom, "//@VehicleNumber");
                MLAPDPartnerDataMgr.g_strStatus = mLAPDPartnerDataMgr.GetChildNodeText(ref objDom, "//@Status");

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                objDom = null;
                objElem = null;
                throw ex;
            }
            finally
            {
                objCDataAccessor = null;
            }
            //Are we configured to process this document at this time?
            return mobjPartner.CanProcessDocument();
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>

        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer: " });
            string strError = string.Empty;
            string strAppend = string.Empty;
            string strTemplateFile = string.Empty;
            XmlDocument xmldocAppend = null;
            object objAppend = null;
            bool isSendReceipt = true;
            try
            {
                mobjPartner = new CPartner();
                objMDomUtils = new Common.MDomUtils();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner ", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.ePrintImage:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eEstimateData:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eHeartBeat:
                    //These are not stored.

                    case CPartner.EDocumentType.eReceipt:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eShopSearch:
                        //            mobjPartner.StoreDocumentInPartnerDb
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eBusinessEvent:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type ", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eAssignment:
                        mobjPartner.StoreDocumentInPartnerDb();

                        //Validate the assignment XML for required information.

                        strAppend = string.Concat(new string[] { MLAPDPartnerDataMgr.g_strSupportDocPath, "\\GrangeAssignmentValidate.xsl" });
                        xmldocAppend = MLAPDPartnerDataMgr.g_objPassedDocument;
                        objAppend = MLAPDPartnerDataMgr.g_objEvents;

                        strError = objMDomUtils.ValidateDomData(ref xmldocAppend, ref strAppend, ref objAppend, false);

                        if (strError != string.Empty)
                        {
                            //Halt further processing
                            strTemplateFile = "GrangeReceipt.XML";

                            //Negative receipt
                            objCAutoverse.SendAReceipt(ref strTemplateFile, "N", Grange_Error_Codes.eAssignmentInvalid.ToString(), strError);

                            //Send notification to development staff.
                            MLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(Grange_Error_Codes.eAssignmentInvalid), PROC_NAME, "Grange Assignment failed validation.  A negative receipt has been sent.", string.Concat(new string[] { "Error: ", strError }), string.Empty, false);
                            isSendReceipt = false;
                        }
                        break;
                }

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return isSendReceipt;
        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransfer()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer: " });
            string strCode = string.Empty,
                strError = string.Empty,
                strSource = string.Empty,
                strClaimOwnerEmail = string.Empty,
                strCarrierRepName = string.Empty,
                strCarrierRepAreaCode = string.Empty,
                strCarrierRepPhone = string.Empty,
                strCarrierRepExtn = string.Empty,
                strFrom = string.Empty,
                strSubject = string.Empty,
                strBody = string.Empty,
                strPath = string.Empty,
                strAppendfilename = string.Empty,
            strxmlDocumentApdLoadAssignment = string.Empty;
            XmlDocument xmlDocumentApdLoadAssignment = null;
            long lngNumber = 0;
            XmlDocument objClaimOwnerDOM = null;
            object objECAD = null;
            object objAppend = null;
            Hashtable htparams = null;
            try
            {
                objCWebAssignment = new EcadMgr.CWebAssignment();
                mobjPartner = new CPartner();
                objMDomUtils = new Common.MDomUtils();
                objCAutoverse = new CAutoverse();
                objCDataAccessor = new cDataAccessor.CDataAccessor();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        //TODO - Put positive ack handling in place for other types besides Assignments.
                        if (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//ReceiptForType") == "Assignment")
                        {
                            //Handle assignment receipt.
                            mobjPartner.ProcessReceipt("//FailureReasonCode", "//FailureReasonText");
                        }

                        else if (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//SuccessFlag") == "N")
                        {
                            //Send developer notification of nack from Grange.
                            MLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(Grange_Error_Codes.eUnhandledGrangeNack), string.Concat(new string[] { PROC_NAME, "Negative Acknowledgement received from Grange: " }),
                               string.Concat(new string[] { " Code = '" , mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//FailureReasonCode") ,
                                 "' Desc = '" , mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//FailureReasonText") , "'"}),
                               string.Concat(new string[] { "XML = ", MLAPDPartnerDataMgr.g_strPassedDocument }), string.Empty, false);
                        }
                        break;
                    case CPartner.EDocumentType.eAssignment:
                        //Get the assignment code of the passed assignment.
                        strCode = (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//ActionCode")).ToUpper();


                        switch (strCode)
                        {
                            case "NEW":
                                // Check for duplicate claim.  For Grange there should only be one vehicle per claim.  So if we were able to retrieve a
                                // LynxID based on the passed ClaimNumber and the LynxID has a non-cancelled, non-voided claim, then this is a duplicate claim.
                                if (MLAPDPartnerDataMgr.g_strLynxID != string.Empty && (MLAPDPartnerDataMgr.g_strStatus != string.Empty || MLAPDPartnerDataMgr.g_strStatus != "Vehicle Cancelled" || MLAPDPartnerDataMgr.g_strStatus != "Vehicle Voided"))
                                {
                                    strAppendfilename = "GrangeReceipt.XML";
                                    // Claim is duplicate.  Send negative ACK.
                                    objCAutoverse.SendAReceipt(ref strAppendfilename, "N", Grange_Error_Codes.eAssignmentInvalid.ToString(), "B1|LYNX APD contains an existing claim with this claim number.  This request could not be processed as it would result in a duplicate claim.");
                                    MLAPDPartnerDataMgr.g_objEvents.Trace("Duplicate claim received.  Negative ACK sent.", PROC_NAME);
                                }
                                else
                                {
                                    MLAPDPartnerDataMgr.g_objEvents.Trace("Processing 'New' request.");

                                    // objECAD = mLAPDPartnerDataMgr.CreateObjectEx("LAPDEcadAccessorMgr.CWebAssignment");
                                    strPath = string.Concat(new string[] { MLAPDPartnerDataMgr.g_strSupportDocPath, "\\GrangeAssignmentXfer.xsl" });
                                    //Process the assignment.
                                    objAppend = MLAPDPartnerDataMgr.g_objEvents;
                                    xmlDocumentApdLoadAssignment = objMDomUtils.TransformXmlAsDom(ref MLAPDPartnerDataMgr.g_strPassedDocument, strPath, ref objAppend);
                                    strxmlDocumentApdLoadAssignment = xmlDocumentApdLoadAssignment.OuterXml;
                                    MLAPDPartnerDataMgr.g_strLynxID = Convert.ToString((objCWebAssignment.ApdLoadAssignmentExternal(ref strxmlDocumentApdLoadAssignment, ref MLAPDPartnerDataMgr.g_strPartner, 0)));

                                    strAppendfilename = "GrangeReceipt.XML";
                                    //Negative receipt
                                    objCAutoverse.SendAReceipt(ref strAppendfilename, "N", Grange_Error_Codes.eAssignmentInvalid.ToString(), "B1|An error occurred while processing your assignment.  A LYNX representative will be in contact with you shortly.");

                                    //Go ahead and raise now if an error happened in the ApdLoadAssignment call.
                                    if (lngNumber != 0)
                                        throw new Exception(string.Concat(new string[] { lngNumber.ToString(), strSource, strError }));

                                    //Send back an AdjAssigned business event.
                                    SendBusinessEvent("AdjAssigned", string.Concat(new string[] {"Assignment '" , mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//AssignmentID") ,
                                        "' was successfully assigned to an adjuster."}), "Y", "-AA");

                                    objECAD = null;
                                }
                                break;
                            case "CANCEL":

                                if ((MLAPDPartnerDataMgr.g_strLynxID.Trim().Length) == 0)
                                {
                                    strAppendfilename = "GrangeReceipt.XML";
                                    objCAutoverse.SendAReceipt(ref strAppendfilename, "N", Lynx_NACK_Codes.eClaimNumberInvalid.ToString(), "B1|The claim number in the cancellation request does not exist in the LYNX APD system.");
                                    MLAPDPartnerDataMgr.g_objEvents.Trace("Received Cancellation request with no LynxID.  Termitating process.");
                                    break;
                                }

                                MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Processing Cancellation request  for LynxID: ", MLAPDPartnerDataMgr.g_strLynxID }));
                                //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                                objClaimOwnerDOM = new XmlDocument();

                                // Retrieve claim owner info.
                                htparams = new Hashtable();
                                htparams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                                htparams.Add("@InsuranceCompanyID", mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//InsuranceCompanyID", true));

                                strAppendfilename = objCDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/ClaimOwnerSP"), htparams, "APD");
                                //strAppendfilename = MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/ClaimOwnerSP"), new object[]{
                                //                                                  "@LynxID", MLAPDPartnerDataMgr.g_strLynxID, "@InsuranceCompanyID",
                                //                                                mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//InsuranceCompanyID", true)});

                                mLAPDPartnerDataMgr.LoadXml(ref objClaimOwnerDOM, ref strAppendfilename, PROC_NAME, "CPartner_ApdDatabaseTransfer");

                                // Retrieve claim owner or default email address to be email recipient.
                                if (mLAPDPartnerDataMgr.GetConfig("PartnerSettings/GrangeOhio/CancellationEmail/@overrideTo") == "yes")
                                    strClaimOwnerEmail = mLAPDPartnerDataMgr.GetConfig("PartnerSettings/GrangeOhio/OverrideEmailTo");
                                else
                                    strClaimOwnerEmail = mLAPDPartnerDataMgr.GetChildNodeText(ref objClaimOwnerDOM, "//@EmailAddress", true);

                                if (strClaimOwnerEmail.Trim().Length == 0)
                                    strClaimOwnerEmail = mLAPDPartnerDataMgr.GetConfig("EventHandling/Email");

                                // Retrieve carrier rep or default email address to be email sender.
                                strFrom = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//@CarrierRepEmailAddress");

                                if (strFrom.Trim().Length == 0)
                                    strFrom = mLAPDPartnerDataMgr.GetConfig("EventHandling/Email/@from");

                                // Attempt to retrieve the carrier rep's name.
                                strCarrierRepName = (string.Concat(new string[] { mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Adjuster/Person/FirstName"), " ", mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Adjuster/Person/LastName") })).Trim();

                                // Retrieve carrier rep phone number fields.
                                strCarrierRepAreaCode = (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Adjuster/Person/Customer/CustomerContact/Contact/Telecom/AreaCode")).Trim();
                                strCarrierRepPhone = (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Adjuster/Person/Customer/CustomerContact/Contact/Telecom/TelecomNum")).Trim();
                                strCarrierRepExtn = (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//Adjuster/Person/Customer/CustomerContact/Contact/Telecom/Extn")).Trim();

                                strCarrierRepPhone = string.Concat(new string[] { strCarrierRepAreaCode, strCarrierRepPhone });

                                // Build carrier rep phone number.
                                if ((strCarrierRepPhone.Length) == 7)
                                    strCarrierRepPhone = string.Concat(new string[] { strCarrierRepPhone.Substring(0, 3), "-", strCarrierRepPhone.Substring(strCarrierRepPhone.Length - 4, 4) });

                                if ((strCarrierRepPhone.Length) == 10)
                                    strCarrierRepPhone = string.Concat(new string[] { strCarrierRepPhone.Substring(0, 3), "-", strCarrierRepPhone.Substring(3, 3), "-", strCarrierRepPhone.Substring(strCarrierRepPhone.Length - 4, 4) });

                                if (strCarrierRepExtn.Length > 0)
                                    strCarrierRepPhone = string.Concat(new string[] { strCarrierRepPhone, " Extn. ", strCarrierRepExtn });

                                if (strCarrierRepPhone.Length > 0)
                                    strCarrierRepPhone = string.Concat(new string[] { "(", strCarrierRepPhone, ")" });

                                // Build email subject.
                                strSubject = string.Concat(new string[] { mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", MLAPDPartnerDataMgr.g_strPartner, "/CancellationEmail/Subject" })), " ", MLAPDPartnerDataMgr.g_strLynxID });

                                // Initialize email body. 
                                strBody = string.Empty;

                                // Build email body.
                                if (strClaimOwnerEmail == mLAPDPartnerDataMgr.GetConfig("EventHandling/Email"))
                                    strBody = string.Concat(new string[] { strBody, "PRD MON:  This email should have gone to claim owner, however claim owner could not be located.  " });

                                // If carrier rep name was provided in cancellation xml, include it in the body.
                                if (strCarrierRepName.Length > 0)
                                    strBody = string.Concat(new string[] { strBody, strCarrierRepName });

                                if (strCarrierRepPhone.Length > 0)
                                    strBody = string.Concat(new string[] { strBody, " ", strCarrierRepPhone });

                                strBody = string.Concat(new string[] { strBody, " at " });

                                // Add the canned cancellation notification message from the config file to the email body.
                                strBody = string.Concat(new string[] { strBody, mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", MLAPDPartnerDataMgr.g_strPartner, "/CancellationEmail/Body" })), Environment.NewLine, Environment.NewLine, MLAPDPartnerDataMgr.g_strLynxID });


                                MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Sending Cancellation request email." , Environment.NewLine , "From: " , strFrom , Environment.NewLine ,
                                          "To: " , strClaimOwnerEmail , Environment.NewLine , "Subject: " , strSubject , Environment.NewLine ,
                                          "Body: " , strBody}));

                                // Send the email.
                                MLAPDPartnerDataMgr.g_objEvents.SendEmail(strFrom, strClaimOwnerEmail, strSubject, strBody);

                                //Send back an AdjAssigned business event.
                                SendBusinessEvent("AdjAssigned", string.Concat(new string[] { "Assignment '" , mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//AssignmentID") ,
                                    "' was successfully assigned to an adjuster."}), "Y", "-AA");
                                break;

                            default:
                                throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                        }
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner", MLAPDPartnerDataMgr.g_strPartner }));

                }

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                objECAD = null;
                objClaimOwnerDOM = null;
                throw ex;
            }
            return string.Empty;
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner: " });
            try
            {
                mobjPartner = new CPartner();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = MLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.Name;

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eFolderUpload:
                        DocumentExchange();
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { PROC_NAME, "We don't process doc type", mobjPartner.DocTypeString, "[", mobjPartner.DocTypeEnum.ToString(), "] for partner ", MLAPDPartnerDataMgr.g_strPartner }));
                }
                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Empty;
        }

        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        public long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            try
            {
                mobjPartner = new CPartner();
                return mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Fills out some generic header schtuff.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strIdent"></param>
        public void SetHeaderSchtuff(XmlDocument objDom, string strIdent)
        {
            string strDate = string.Empty,
                strTime = string.Empty;
            try
            {
                strDate = mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString());
                strTime = mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString());

                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionID", true).InnerText = string.Concat(new string[] {
        strIdent , "-" , strDate , "-" , strTime , "-" ,
       MLAPDPartnerDataMgr.g_strLynxID , "-" , MLAPDPartnerDataMgr.g_strVehicleNumber , "-" , MLAPDPartnerDataMgr.g_strAssignmentID});

                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@LynxID", true).InnerText = MLAPDPartnerDataMgr.g_strLynxID;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@VehicleID", true).InnerText = MLAPDPartnerDataMgr.g_strVehicleNumber;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@AssignmentID", true).InnerText = MLAPDPartnerDataMgr.g_strAssignmentID;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TaskID", true).InnerText = MLAPDPartnerDataMgr.g_strTaskID;

                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionDate", true).InnerText = strDate;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionTime", true).InnerText = strTime;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@Date", true).InnerText = strDate;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@Time", true).InnerText = strTime;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@TimeZone", true).InnerText = "Local";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest: " });
            XmlDocument objDom = null;
            XmlNode objNetStatNode = null;
            string strWrappedXml = string.Empty,
 strPost2URL = string.Empty;
            string strFileNameAppend = string.Empty;
            try
            {
                objMPPGTransactions = new MPPGTransactions();
                objMDomUtils = new Common.MDomUtils();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing HeartBeat" }));

                //First load the HeartBeat template from disk.

                objDom = new XmlDocument();
                strFileNameAppend = string.Concat(new string[] { MLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGHeartBeat.XML" });
                objMDomUtils.LoadXmlFile(ref objDom, ref strFileNameAppend, "SendAHeartBeat", "HeartBeat");

                SetHeaderSchtuff(objDom, "HB");

                objNetStatNode = mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//NetworkStatusTest", true);

                //Remove the received date/time - to be filled in by SG.
                objNetStatNode.RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ReceivedDate", true));

                //Remove the reply date/time - to be filled in by SG.
                objNetStatNode.RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ReplyDate", true));

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = objMPPGTransactions.BuildTransactionForPPG("NetworkStatusTest", objDom.OuterXml, MLAPDPartnerDataMgr.g_strPartner, string.Concat(new string[] { MLAPDPartnerDataMgr.g_strLynxID, "-", MLAPDPartnerDataMgr.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                objMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, "HeartBeat sent via HTTP" }));
            }
            catch (Exception ex)
            {
                objNetStatNode = null;
                objDom = null;
                throw ex;
            }
        }

        /// <summary>
        /// Send a business event to Partner.
        /// </summary>
        /// <param name="strEventCode"></param>
        /// <param name="strEventNote"></param>
        /// <param name="strSuccess"></param>
        /// <param name="strEventOpcode"></param>
        public void SendBusinessEvent(string strEventCode, string strEventNote, string strSuccess, string strEventOpcode)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendBusinessEvent: " });
            XmlDocument objDom = null, objSession = null;
            XmlNode objNode = null;
            string strWrappedXml = string.Empty,
                strPost2URL = string.Empty;

            try
            {
                objMPPGTransactions = new MPPGTransactions();
                objMDomUtils = new Common.MDomUtils();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing Business Event" }));

                //First load the receipt template from disk.

                objDom = new XmlDocument();
                objMDomUtils.LoadXmlFile(ref objDom, ref MLAPDPartnerDataMgr.g_strSupportDocPath, PROC_NAME, "Business Event");  // + "\\GrangeReceipt.XML"

                SetHeaderSchtuff(objDom, string.Concat(new string[] { "BE", strEventOpcode }));

                //Adjuster Assigned
                if (strEventCode.ToUpper() == "ADJASSIGNED")
                {
                    //Add the assigned adjuser information.
                    objSession = new XmlDocument();

                    //Get the claim session detail from the database.
                    //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                    //Set the LYNX rep contact info.
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Person/@FirstName", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserNameFirst", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Person/@LastName", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserNameLast", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/AreaCode", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneAreaCode", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/TelecomNum", true).InnerText = string.Concat(new string[] { mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneExchangeNumber", true), mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneUnitNumber", true) });
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/Extn", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserExtensionNumber", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ContactEmail", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserEmail", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//SuccessFlag", true).InnerText = strSuccess;

                    //ClaimNumber field is optional (specific to Grange).
                    objNode = mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ClaimNumber");

                    if (objNode != null)
                        objNode.InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//ClaimNumber", false);

                    //Assignment Download

                    //Build the SOAP Envelope and put the HeartBeat into the envelope
                    strWrappedXml = objMPPGTransactions.BuildTransactionForPPG("Receipt", objDom.OuterXml, MLAPDPartnerDataMgr.g_strPartner, string.Concat(new string[] { MLAPDPartnerDataMgr.g_strLynxID, "-", MLAPDPartnerDataMgr.g_strVehicleNumber }));

                    //Determine the URL for posting depending on the environment
                    strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                    //Do the post to PPG
                    objMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                    if (MLAPDPartnerDataMgr.g_blnDebugMode)
                        MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, "Business Event sent via HTTP" }));
                }
            }
            catch (Exception ex)
            {
                objDom = null;
                objSession = null;
                throw ex;
            }
        }

        /// <summary>
        /// DocumentExchange()
        /// 
        /// Creates a Folder Upload document based upon the passed XML document.
        /// The passed XML points us to the claim and gives a list of attachments.
        /// This method then takes that list of attachments and decides how many
        /// EstimateUpload and how many EstimateDI transactions are needed.
        /// </summary>
        public void DocumentExchange()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DocumentExchange: " });

            //Objects that need clean up.

            XmlDocument objDocs = null,
                objTrans = null,
                objCEF = null,
                objPassThru = null;
            XmlNodeList objList = null;
            XmlElement objElem = null,
                objDocElem = null,
                objNewDocElem = null;
            XmlCDataSection objCdata = null;
            ADODB.Recordset objRS = null;
            //Primitives that don't.

            string strWrappedXml = string.Empty,
                strPost2URL = string.Empty,
                strPartnerTransID = string.Empty,
                strClaimAspectID = string.Empty,
                                strInsCoID = string.Empty,
                strUserID = string.Empty,
                strPath = string.Empty,
                strFileName = string.Empty,
                strExtension = string.Empty,
                strTemp = string.Empty,
                strDocList = string.Empty,
                strCEF = string.Empty,
                strXML = string.Empty,
                strTaskFields = string.Empty,
                strTaskID = string.Empty,
                strAppendFilename = string.Empty;
            string[] strAttachments = null;

            long lngDocID = 0;
            int intIdx = 0;
            object varAttach = null,
                varData = null;
            DateTime objDate;
            double dNum = 0.0;
            Hashtable htParams = null;

            try
            {
                objMPPGTransactions = new MPPGTransactions();
                objMBase64Utils = new MBase64Utils();
                objMFlatfile = new MFlatfile();
                mobjPartner = new CPartner();
                objMEscapeUtilities = new MEscapeUtilities();
                objMDomUtils = new Common.MDomUtils();
                objCDataAccessor = new cDataAccessor.CDataAccessor();

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing ", mobjPartner.DocTypeString }));

                //Get some useful attributes from the passed document.
                //The attachment list gets split out into individual document IDs.
                strAttachments = (mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//@Attachments", true)).Split(',');

                strInsCoID = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//@InsuranceCompanyID", true);
                strUserID = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//@UserID", true);
                MLAPDPartnerDataMgr.g_strLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref MLAPDPartnerDataMgr.g_objPassedDocument, "//@LynxID", true);

                //Set the connect string to APD to recordset
                //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                htParams = new Hashtable();
                htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                strXML = objCDataAccessor.OpenRecordsetAsClientSideXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), htParams, "APD");
                //Get the pass thru data for this claim and do some validation.
                //strXML = MLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetAsClientSideXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), new object[] { MLAPDPartnerDataMgr.g_strLynxID });
                objPassThru = new XmlDocument();
                objPassThru.LoadXml(strXML);

                if (objPassThru != null)
                {
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "No Records returned from", mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), "for", MLAPDPartnerDataMgr.g_strLynxID }));
                }
                else if (objPassThru.SelectNodes("//ClaimAspect").ToString().Length == 0)
                {
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "No Records returned from", mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), "for", MLAPDPartnerDataMgr.g_strLynxID }));
                }

                if (MLAPDPartnerDataMgr.g_blnDebugMode)
                    MLAPDPartnerDataMgr.g_objEvents.Trace(objPassThru.OuterXml, string.Concat(new string[] { PROC_NAME, "SourceApplicationPassThruData" }));

                //Get all documents for this claim from the database.
                objDocs = new XmlDocument();
                htParams = new Hashtable();
                htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                htParams.Add("@DocumentClassCD", "A");
                htParams.Add("@InsuranceCompanyID", strInsCoID);
                strAppendFilename = objCDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/DocumentListSP"), htParams, "APD");

                //strAppendFilename = MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/DocumentListSP"), new object[]{
                //     "@LynxID", MLAPDPartnerDataMgr.g_strLynxID,
                //     "@DocumentClassCD", "A",
                //     "@InsuranceCompanyID", strInsCoID});

                mLAPDPartnerDataMgr.LoadXml(ref objDocs, ref strAppendFilename, PROC_NAME, "Document List");

                //Loop through the selected attachments and mark them in the document list as selected and either estimate or non-estimate.
                foreach (string varAttac in strAttachments)
                {
                    //Match each passed attachment to the list retrieved from the DB.
                    objElem = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref objDocs, string.Concat(new string[] { "//Document[@DocumentID=", varAttach.ToString(), "]" }));  //TODO: Verify the conversion!!

                    //Set some basic header values.
                    SetHeaderSchtuff(objTrans, "EU");

                    //Set the scene genesis specific fields.
                    objMBase64Utils.WriteTextToCDATA((XmlCDataSection)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TaskFields", true), strTaskFields);
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@TaskID", true).InnerText = strTaskID;

                    //Get the file path and trim off the path info.
                    strPath = objElem.SelectSingleNode("@ImageLocation").InnerText; //TODO need to verify the parameters...
                    strTemp = strPath;

                    while ((strTemp.IndexOf("\\", 1)) > 0)
                        strTemp = strTemp.Substring(strTemp.IndexOf("\\", 1) + 2);          // TODO: Clear the error and the use this code...dont forgot 

                    strFileName = strTemp;

                    //Now trim off everything but the extension.
                    while (strTemp.IndexOf(".", 1) > 0)
                        strTemp = strTemp.Substring(strTemp.IndexOf(".", 1) + 1);

                    strExtension = strTemp.ToUpper();

                    //Get the full path across to the document storage.
                    strPath = (string.Concat(new string[] { mLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strPath }).Replace("\\\\", "\\"));

                    if (MLAPDPartnerDataMgr.g_blnDebugMode)
                        MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Estimate Upload PI" }));

                    //If the file type is XML then we use CDATA.
                    if (strExtension == "XML")
                    {
                        //Fix the file name and extension to be TXT
                        strExtension = "TXT";
                        strFileName = string.Concat(new string[] { strFileName.Substring(strFileName.Length - 3), strExtension });

                        //Set the image encoding element to CDATA.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageEncoding", true).InnerText = "Cdata";

                        //Read in the text data from the file.
                        strTemp = objMBase64Utils.ReadTextDataFromFile(strPath);

                        //Strip out the XML and just get the flatfile.
                        strTemp = objMFlatfile.ConvertXMLToFlatfile(strTemp);

                        //Set the image size element.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageLength", true).InnerText = (strTemp.Length).ToString();

                        //Create a new CDATA section and add it to the image file element.
                        objCdata = objTrans.CreateCDataSection(strTemp);
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFile", true).AppendChild(objCdata);
                        objCdata = null;

                        //Otherwise we Base64 encode it.
                    }
                    else
                    {
                        //Set the image encoding element to Base64.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageEncoding", true).InnerText = "Base64";

                        //Read in the binary data from the file.
                        varData = objMBase64Utils.ReadBinaryDataFromFile(strPath);

                        //Set the image size element.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageLength", true).InnerText = varData.ToString();  //TODO Check UBound(varData) but c#?

                        //Write the binary data to the DOM
                        objMBase64Utils.WriteBinaryDataToDomElement((XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFile", true), varData.ToString());
                    }

                    //Stuff file name into the ImageFileName element.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFileName", true).InnerText = strFileName;

                    //Stuff the file extension into the ImageFileType element.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFileType", true).InnerText = strExtension;

                    //Get and set the image date time info.
                    strTemp = (objElem.SelectSingleNode("@ReceivedDate").InnerText); //TODO need to verify the parameters...
                    objDate = Convert.ToDateTime(string.Concat(new string[] { strTemp.Substring(0, 10), " ", strTemp.Substring(strTemp.Length - 8, 8) }));

                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/CreateDateTime/DateTime/@Date", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(objDate.ToString());
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/CreateDateTime/DateTime/@Time", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(objDate.ToString());

                    //Set the AssignmentID for this estimate.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@AssignmentID", true).InnerText = ((objElem.SelectSingleNode("//@AssignmentID ").InnerText)).ToString(); //TODO need to verify the parameters...

                    //Get the partner trans id for this estimate.
                    strPartnerTransID = (objElem.SelectSingleNode("//@ExternalReferenceDocumentID").InnerText); //TODO need to verify the parameters...
                    strPartnerTransID = strPartnerTransID.Substring(strPartnerTransID.IndexOf(":") + 1);

                    if (MLAPDPartnerDataMgr.g_blnDebugMode)
                        MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Partner Trans ID = '", strPartnerTransID, "'" }), string.Concat(new string[] { PROC_NAME, "Estimate Upload CEF data" }));

                    //Retrieve the CEF data for this estimate from the partner database.
                    bool isNum = double.TryParse(strPartnerTransID, out dNum);


                    if (isNum)
                        strCEF = mobjPartner.RetrieveCefFromPartnerDb(Convert.ToInt64(strPartnerTransID));
                    else
                        strCEF = string.Empty;

                    //Was CEF data found?
                    if (strCEF != string.Empty)
                    {
                        //Stuff it into a DOM.
                        objMDomUtils.LoadXml(ref objCEF, ref strCEF, PROC_NAME, "CEF");

                        //Stuff it into the estimate upload DOM.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TransactionData", true).AppendChild(mLAPDPartnerDataMgr.GetChildNode(ref objCEF, "//APDEstimate", true));
                    }
                    else
                    {
                        //Log the fact that no CEF was retrieved.
                        MLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(Grange_Error_Codes.eBlankCefReturnedForEstimate), string.Concat(new string[] {
                             PROC_NAME , " Estimate Upload CEF Retrieval"}),
                             "Blank CEF! Partner Trans ID = '" + strPartnerTransID + "'", "", "", false);
                    }
                    //Build the SOAP Envelope and put the HeartBeat into the envelope
                    strWrappedXml = objMPPGTransactions.BuildTransactionForPPG("EstimateUpload", objTrans.OuterXml, MLAPDPartnerDataMgr.g_strPartner, MLAPDPartnerDataMgr.g_strLynxID);

                    //Do the post to PPG
                    objMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                    //Build a description of the event. 
                    strTemp = (objElem.SelectSingleNode("//@SupplementSeqNumber").InnerText);
                    if (strTemp == "0")
                        strTemp = "Estimate";
                    else
                        strTemp = string.Concat(new string[] { "Supplement ", strTemp });

                    strTemp = string.Concat(new string[] { strTemp, " uploaded electronically to the insurance carrier through ", MLAPDPartnerDataMgr.g_strPartner, "." });

                    //Send a WorkFlow Event for the electronic transmission.
                    //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                    htParams = new Hashtable();
                    htParams.Add("@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='EstimateSentToCarrier']"));
                    htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                    htParams.Add("@PertainsTo", "CLM");
                    htParams.Add("@Description", strTemp);
                    htParams.Add("@UserID", strUserID);
                    htParams.Add("@ReferenceData", objElem.SelectSingleNode("@DocumentID").InnerText);
                    objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), htParams, "APD");
                    //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), new object[] {
                    //     "@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='EstimateSentToCarrier']"),
                    //     "@LynxID", MLAPDPartnerDataMgr.g_strLynxID,
                    //     "@PertainsTo", "CLM",
                    //     "@Description", strTemp,
                    //     "@UserID", strUserID,
                    //     "@ReferenceData", objElem.SelectSingleNode("@DocumentID").InnerText});

                    //Have the database set the uploaded indicator on the document
                    htParams = new Hashtable();
                    htParams.Add("@DocList", (objElem.SelectSingleNode("@DocumentID").InnerText));
                    htParams.Add("@UserID", strUserID);
                    objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), htParams, "APD");
                    //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), new object[] { "@DocList", (objElem.SelectSingleNode("@DocumentID").InnerText), "@UserID", strUserID });

                    //Stuff the electronic transmission XML in the partner specific database.
                    // MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strPartnerConnStringStd);
                    htParams = new Hashtable();
                    htParams.Add("@AssignmentID", MLAPDPartnerDataMgr.g_strAssignmentID);
                    htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                    htParams.Add("@VehicleNumber", MLAPDPartnerDataMgr.g_strVehicleNumber);
                    htParams.Add("@SequenceNumber", (objElem.SelectSingleNode("//@SupplementSeqNumber").InnerText));
                    htParams.Add("@TransactionDate", string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), " ", DateTime.Now.ToString("hh:mm:ss")));
                    htParams.Add("@TransactionSource", "LYNX");
                    htParams.Add("@TransactionType", "EstimateUpload");
                    htParams.Add("@PartnerXML", objMEscapeUtilities.SQLQueryString(ref strWrappedXml));
                    objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), htParams, "Partner");



                    //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] {
                    //     "@AssignmentID", MLAPDPartnerDataMgr.g_strAssignmentID,
                    //     "@LynxID", MLAPDPartnerDataMgr.g_strLynxID,
                    //     "@VehicleNumber", MLAPDPartnerDataMgr.g_strVehicleNumber,
                    //     "@SequenceNumber", (objElem.SelectSingleNode("//@SupplementSeqNumber").InnerText),  //TODO need to verify the parameters...
                    //     "@TransactionDate", string.Concat(new string[] { DateTime.Now.ToString("yyyy-mm-dd"), " ", DateTime.Now.ToString("hh:mm:ss") }),
                    //     "@TransactionSource", "LYNX",
                    //     "@TransactionType", "EstimateUpload",
                    //     "@PartnerXML", objMEscapeUtilities.SQLQueryString(ref strWrappedXml)});

                    if (MLAPDPartnerDataMgr.g_blnDebugMode)
                        MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }),
           string.Concat(new string[] { PROC_NAME, mobjPartner.DocTypeString, " sent via HTTP" }));

                    //***********************************************************************
                    // Launch one EstimateDI transaction for all other selected documents.
                    //***********************************************************************

                    //Get a list of all the selected estimates.
                    objList = objDocs.SelectNodes("//Document[@Selected='True'][@EstimateTypeFlag='0']");

                    //Only need to go through the following mess if at least one DI exists to upload.
                    if (objList.Count > 0)

                        if (MLAPDPartnerDataMgr.g_blnDebugMode)
                            MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Num Docs = ", objList.Count.ToString() }), string.Concat(new string[] { PROC_NAME, "Document Upload Started" }));

                    //Load up a fresh estimate template from disk.
                    objTrans = new XmlDocument();
                    objMDomUtils.LoadXmlFile(ref objTrans, ref (MLAPDPartnerDataMgr.g_strSupportDocPath), PROC_NAME, "Estimate Load");  //TODO: second param + "\\SGEstimateDI.XML"

                    //Set some basic header values.
                    SetHeaderSchtuff(objTrans, "EU");


                    //Set the scene genesis specific fields.
                    objMBase64Utils.WriteTextToCDATA((XmlCDataSection)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TaskFields", true).FirstChild, strTaskFields);
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@TaskID", true).InnerText = strTaskID;

                    //Set the numeric insurance carrier ID.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//InsuranceCarrierID", true).InnerText = strInsCoID;

                    //Set the number of images.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//NumberOfImages", true).InnerText = (objList.ToString().Length).ToString();

                    //Get the mock-up DigitalImage element in the DI XML.
                    objDocElem = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//DigitalImage", true);

                    //Remove the blank DigitalImage element from the document.
                    objDocElem = (XmlElement)objDocElem.ParentNode.RemoveChild(objDocElem);

                    strDocList = string.Empty;

                    //Loop through each document and add a DigitalImage element for it.
                    foreach (XmlElement objElement in objList)
                    {
                        //Duplicate the document element.
                        objNewDocElem = (XmlElement)objDocElem.CloneNode(true);

                        //Get the file path and trim off the path info.
                        strPath = objElement.SelectSingleNode("@ImageLocation").InnerText;
                        strTemp = strPath;
                        while (strTemp.IndexOf("\\", 1) > 0)
                            strTemp = strTemp.Substring(strTemp.IndexOf("\\", 1) + 2);

                        //Stuff file name into the ImageFileName element.
                        objNewDocElem.SelectSingleNode("ImageFileName").InnerText = strTemp;

                        //Now trim off everything but the extension.
                        while (strTemp.IndexOf(".", 1) > 0)
                            strTemp = strTemp.Substring(strTemp.IndexOf(".", 1) + 1);

                        //Stuff the file extension into the ImageFileType element.
                        objNewDocElem.SelectSingleNode("ImageFileType").InnerText = strTemp;

                        //Stuff the document type.
                        objNewDocElem.SelectSingleNode("ImageFileType").InnerText = string.Concat(new string[] {
                           objDocs.SelectSingleNode("//Reference[@List='DocumentType'][@ReferenceID='").InnerText
                             , (objElement.SelectSingleNode("@DocumentTypeID").InnerText) , "']/@Name"});

                        //Get the full path across to the document storage.
                        strPath = ((string.Concat(new string[] { mLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strPath })).Replace("\\\\", "\\"));

                        //Read in the binary data from the file.
                        varData = objMBase64Utils.ReadBinaryDataFromFile(strPath);

                        //Set the image size element.
                        objNewDocElem.SelectSingleNode("ImageFileLength").InnerText = varData.ToString().ToUpper();    //TODO Must check Parameter..

                        //Write the binary data to the DOM.
                        objMBase64Utils.WriteBinaryDataToDomElement((XmlElement)objNewDocElem.SelectSingleNode("DigitalImageFile"), varData);   //TODO Must check Parameter..

                        //Get and set the image date time info.
                        strTemp = objElement.SelectSingleNode("@ReceivedDate").InnerText; //TODO:need to vrify....
                        objDate = Convert.ToDateTime(string.Concat(new string[] { strTemp.Substring(10), " ", strTemp.Substring(8) }));

                        objNewDocElem.SelectSingleNode("CreateDateTime/DateTime/@Date").InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(objDate.ToString());
                        objNewDocElem.SelectSingleNode("CreateDateTime/DateTime/@Time").InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(objDate.ToString());

                        //Set the AssignmentID for this estimate.
                        objTrans.SelectSingleNode("//@AssignmentID").InnerText =
                           objElement.SelectSingleNode("//@AssignmentID").InnerText;

                        //Now append our newly created digital image element to the primery doc.
                        objTrans.SelectSingleNode("//TransactionData").AppendChild(objNewDocElem);

                        //Build a reference document ID list
                        strDocList = string.Concat(new string[] { strDocList, objElement.SelectSingleNode("@DocumentID").InnerText, "," });

                        //Check the size of the digital image transaction.
                        //Glaxis has a 4M limit on these due to their use of MSMQ.
                        //Compare to 4M - 1K to give a little wiggle room.

                        strTemp = objTrans.InnerXml;

                        if ((strTemp.Length) > ((1048576 * 4) - 1024))
                            throw new Exception(string.Concat(new string[] { "4M Size Check", strTemp.Length.ToString(), "|", ((1048576 * 4) - 1024).ToString() }));

                        //Build the SOAP Envelope and put the HeartBeat into the envelope
                        strWrappedXml = objMPPGTransactions.BuildTransactionForPPG("EstimateDI", strTemp, MLAPDPartnerDataMgr.g_strPartner, MLAPDPartnerDataMgr.g_strLynxID);

                        //Do the post to PPG
                        objMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                        //Build a description of the event.
                        strTemp = string.Concat(new string[] { " Digital documents uploaded electronically to the insurance carrier through ", MLAPDPartnerDataMgr.g_strPartner, "." });

                        //Strip off trailing comma from the doc list.
                        if (strDocList != string.Empty)
                            strDocList = strDocList.Substring(strDocList.Length - 1);

                        //Send a WorkFlow Event for the electronic transmission.
                        //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strLYNXConnStringStd);
                        htParams = new Hashtable();
                        htParams.Add("@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='ImagesSentToCarrier']"));
                        htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                        htParams.Add("@PertainsTo", "CLM");
                        htParams.Add("@Description", strTemp);
                        htParams.Add("@UserID", strUserID);
                        htParams.Add("@ReferenceData", strDocList);
                        objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), htParams, "APD");

                        //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), new object[] {
                        //     "@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='ImagesSentToCarrier']"),
                        //     "@LynxID", MLAPDPartnerDataMgr.g_strLynxID,
                        //     "@PertainsTo", "CLM",
                        //     "@Description", strTemp,
                        //     "@UserID", strUserID,
                        //     "@ReferenceData", strDocList});

                        //Have the database set the uploaded indicator on the document
                        htParams = new Hashtable();
                        htParams.Add("@DocList", strDocList);
                        htParams.Add("@UserID", strUserID);
                        objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), htParams, "APD");

                        //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(
                        //     mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), new object[] {
                        //     "@DocList", strDocList,
                        //     "@UserID", strUserID});

                        //Stuff the electronic transmission XML in the partner specific database.
                        //MLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(MLAPDPartnerDataMgr.g_strPartnerConnStringStd);
                        htParams = new Hashtable();
                        htParams.Add("@AssignmentID", MLAPDPartnerDataMgr.g_strAssignmentID);
                        htParams.Add("@LynxID", MLAPDPartnerDataMgr.g_strLynxID);
                        htParams.Add("@VehicleNumber", MLAPDPartnerDataMgr.g_strVehicleNumber);
                        htParams.Add("@SequenceNumber", "0");
                        htParams.Add("@TransactionDate", string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), " ", DateTime.Now.ToString("hh:mm:ss")));
                        htParams.Add("@TransactionSource", "LYNX");
                        htParams.Add("@TransactionType", "EstimateDI");
                        htParams.Add("@PartnerXML", objMEscapeUtilities.SQLQueryString(ref strWrappedXml));
                        objCDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), htParams, "Partner");

                        //MLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(
                        // mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), new object[]{
                        // "@AssignmentID", MLAPDPartnerDataMgr.g_strAssignmentID,
                        // "@LynxID", MLAPDPartnerDataMgr.g_strLynxID,
                        // "@VehicleNumber", MLAPDPartnerDataMgr.g_strVehicleNumber,
                        // "@SequenceNumber", "0",
                        // "@TransactionDate", string.Concat(new string[] { DateTime.Now.ToString("yyyy-MM-dd"), " ", DateTime.Now.ToString("hh:mm:ss") }),
                        // "@TransactionSource", "LYNX",
                        // "@TransactionType", "EstimateDI",
                        // "@PartnerXML", objMEscapeUtilities.SQLQueryString(ref strWrappedXml)});

                        if (MLAPDPartnerDataMgr.g_blnDebugMode)
                            MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, mobjPartner.DocTypeString, " sent via HTTP" }));

                        if (MLAPDPartnerDataMgr.g_blnDebugMode)
                            MLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Document Upload Finished" }));
                    }
                }
            }
            catch (Exception ex)
            {
                if (objRS != null)
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                }
                objRS = null;

                throw ex;
            }
        }

        public bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2: " });
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string CPartner_ApdDatabaseTransferV2()
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2: " });
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2: " });
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2: " });
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Common
        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion


    }
}
