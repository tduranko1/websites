﻿using System;
using System.Threading;
using MSXML2;
using System.Configuration;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MPPGTransactions
    {
        /// <summary>
        /// Module MPPGTransactions
        /// Code common to PPG transactions.
        /// </summary>

        #region Declarations
        public const string APP_NAME = "LAPDPartnerDataMgr.";
        public string MODULE_NAME = string.Concat(new string[] { APP_NAME, "MPPGTransactions." });
        MLAPDPartnerDataMgr mlapdPartnerDataMgr = new MLAPDPartnerDataMgr();
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerExtensive"]);
        #endregion

        #region Enumerator
        /// <summary>
        /// Internal error codes for this module.
        /// </summary>

        private enum EventCodes : ulong
        {
            eElectronicPostBadStatus = 0x80065000 + 0x300
        }
        #endregion

        #region Public Fuctions

        /// <summary>
        /// Syntax:      BuildTransactionForPPG
        /// Parameters:  strTransactionType - A token identifying the transaction type.
        ///              strMessageBody - The XML to put within the wrapper.
        ///              strDestination - A token indentfying the message destination.
        ///               strReferenceId - An internal identifyer for the message.
        ///               Wraps the passed XML with a PPG SOAP style wrapper.
        ///                The completed XML.
        ///                Settings in the config.xml file.
        ///              </summary>
        public string BuildTransactionForPPG(string strTransactionType, string strMessageBody, string strDestination, string strReferenceId)
        {
            string strTrans = string.Empty,
                strEnvelopeId = string.Empty,
                strOriginationId = string.Empty,
                strDestinationId = string.Empty,
                strReturnTrans = string.Empty;
            bool blnUseSOAP, blnUseCDATA;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "MPPGTransactions.CCCC.cs - BuildTransactionForPPG", string.Concat("Process started"), string.Concat("TransactionType: ", strTransactionType, " MessageBody: ", strMessageBody, " Destination: ", strDestination, " ReferenceID: ", strReferenceId));

                //Get SOAP and CDATA settings from configuration.
                blnUseSOAP = Convert.ToBoolean(mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/UseSOAP") == "True");   //TODO: Need to verify assigned function return value is correct?
                blnUseCDATA = Convert.ToBoolean(mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/UseCDATA") == "True");

                //This configuration setting allows us to disable the SOAP envelope if need be.
                if (!blnUseSOAP)
                    return strMessageBody;
                else
                {
                    //Get source and dest PIDs from the config file.
                    strOriginationId = mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/OriginationPID");
                    strDestinationId = mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/DestinationPID[@name='" + strDestination + "']");
                    //Generate the envelope id.
                    strEnvelopeId = string.Concat(strOriginationId, "#", strDestinationId, "#", strReferenceId, "#");

                    //Start building the transaction XML.
                    strTrans = string.Concat("<StdEnvelope EnvelopeID='", strEnvelopeId, "' Version='1.0'>");

                    //Add in the environment code.
                    strTrans = string.Concat(strTrans, "<Header><Delivery><Environment>", mlapdPartnerDataMgr.GetConfig("@EnvironmentCode"), "</Environment>");

                    //Add in the origination company.
                    strTrans = string.Concat(strTrans, "<OriginationCompany>", mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/OriginationCompany"), "</OriginationCompany>");

                    //Add in the origination ID.
                    strTrans = string.Concat(strTrans, "<OriginationPID>", strOriginationId, "</OriginationPID>");

                    //Add in the destination company and Id.
                    strTrans = string.Concat(strTrans, "<DestinationCompany>", strDestination, "</DestinationCompany>"
                        , "<DestinationPID>", strDestinationId, "</DestinationPID>");

                    //Add in the transaction type.
                    strTrans = string.Concat(strTrans, "<TransactionType>", strTransactionType, "</TransactionType>");

                    //Add in the sender's reference Id.
                    strTrans = string.Concat(strTrans, "<SendersReferenceID>", strReferenceId, "</SendersReferenceID>");

                    //Add in the current time and date.
                    strTrans = string.Concat(strTrans, "<TransactionDateTime>", DateTime.Now.ToString(), "</TransactionDateTime>");

                    //Close out the header, begin the message body.
                    strTrans = string.Concat(strTrans, "</Delivery></Header><MessageBody>");

                    //Add in CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "<![CDATA[");

                    //Add in the message body.
                    strTrans = string.Concat(strTrans, strMessageBody);

                    //Close CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "]]>");

                    //Close the message and envelope tags - we are done!
                    strReturnTrans = string.Concat(strTrans, "</MessageBody></StdEnvelope>");

                    if (IsDebug)
                        objWriteLog.LogEvent("COM Conversion", "END", "MPPGTransactions.CCCC.cs - BuildTransactionForPPG", string.Concat("Proccess completed"), string.Concat("AcknoledgementXML: ", strReturnTrans));
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "MPPGTransactions.CCCC.cs - BuildTransactionForPPG", string.Concat("Exception: ", ex.Message, " ", ex.InnerException), string.Concat("AcknoledgementXML: ", strReturnTrans));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return strReturnTrans;
        }

        /// <summary>
        /// Syntax:      XmlHttpPost
        /// Parameters:  strUrl - The URL to post to.
        ///              strXml - The XML data to POST.
        ///              g_objEvents - An instance of SiteUtilities::CEvents to use.
        /// Purpose:     Posts the passed data to a url.         
        /// Returns:     The status.
        /// </summary>
        public void XmlHttpPost(string strUrl, string strXML)
        {
            Int32 intFailOverCount = 0;
            const Int32 MaxFailoverAttempts = 10;
            const Int32 FailoverSleepPeriod = 1000;
            MSXML2.XMLHTTP objXmlHttp = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDataMgr.MPPGTransactions.cs - XmlHttpPost", string.Concat("Process Started"), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));
                while (intFailOverCount <= MaxFailoverAttempts)
                {
                    try
                    {
                        objXmlHttp = new XMLHTTP();
                        objXmlHttp.open("POST", strUrl, false);
                        objXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        objXmlHttp.send(strXML);

                        if (objXmlHttp.status == 200)
                            break;
                        else
                        {
                            objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.MPPGTransactions.cs - XmlHttpPost", string.Concat("HTTP Send", "Bad status returned from post to", strUrl, "Status: ", Convert.ToString(objXmlHttp.status)), "");
                            throw new Exception(string.Concat("HTTP Send", "Bad status returned from post to", strUrl, "Status: ", Convert.ToString(objXmlHttp.status)));                           
                        }

                        intFailOverCount++;
                        Thread.Sleep(FailoverSleepPeriod);

                    }
                    catch (Exception ex)
                    {
                        intFailOverCount++;
                        Thread.Sleep(FailoverSleepPeriod);
                    }
                }
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDataMgr.MPPGTransactions.cs - XmlHttpPost", string.Concat("Process Completed"), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));
            }
            catch (Exception ex)
            {
                objXmlHttp = null;
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.MPPGTransactions.cs - XmlHttpPost", string.Concat("Exception: ", ex.Message), string.Concat("URL: ", strUrl, " AcknowledgementXML: ", strXML));
                throw ex;
            }
            finally
            {
                objXmlHttp = null;
                objWriteLog = null;
            }
        }

        #endregion
    }
}
