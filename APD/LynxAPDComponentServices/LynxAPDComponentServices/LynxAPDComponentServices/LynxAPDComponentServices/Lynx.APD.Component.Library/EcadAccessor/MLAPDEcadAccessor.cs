﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.EcadAccessor
{
    /// <summary>
    ///  Component LAPDEcadAccessor : Module MLAPDEcadAccessor     
    ///  Global Consts and Utility Methods
    /// </summary>
    class MLAPDEcadAccessor
    {
        #region Global Declarations
        private const long LAPDEcadAccessor_FirstError = 0x80066000;      
        #endregion

        #region public Functions

        /// <summary>
        /// Error codes specific to LAPDEcadAccessor
        /// </summary>
        public enum EcadAccessorErrors : ulong
        {
            eCreateObjectExError = LAPDEcadAccessor_FirstError + 0x100
        }

        /// <summary>
        /// Initializes global objects and variables
        /// </summary>
        public void InitializeGlobals()
        {
            string strConfigPath = string.Empty;
            try
            {
                //appSettingsReader = new AppSettingsReader();
                //Create events object.
              //  mobjEvents = new CEvents();
                //This will give partner data its own log file.
                // mobjEvents.ComponentInstance = "Client Access";
                strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                //Initialize our member components first, so we have logging set up.        
                //mobjEvents.Initialize(strConfigPath);                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Terminates global objects and variables
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                //if (mobjEvents != null)
                //    Marshal.ReleaseComObject(mobjEvents);
                //mobjEvents = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
