﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EcadMgr = Lynx.APD.Component.Library.EcadAccessorMgr;

namespace Lynx.APD.Component.Library.EcadAccessor
{
    /// <summary>
    /// Component LAPDEcadAccessor: Class CDocument
    /// 
    /// LYNX APD clients (Insurance Companies) require the ability to view
    /// information about their claims. This information will be accessible from
    /// the LYNX Services web site. For the purpose of this document, this web
    /// site will be known as Client Access.
    /// 
    /// LYNX APD Client Access will be hosted and developed by E-Commerce
    /// Application Development (ECAD). Initially the site will be read-only and
    /// simply provide Insurance companies access to claim information.
    /// 
    /// This component is used to return binary documents to ECAD.
    /// </summary>
    public class CDocument
    {
        #region Global Declarations      
        MLAPDEcadAccessor objMLAPDEcadAccessor = null;
        EcadMgr.CDocument objMgr = null;
        #endregion

        #region Public Functions

        /// <summary>
        /// '********************************************************************************
        ///'* Syntax:      arrByte = GetDocument("<Document ImageLocation="c:\temp.tif"/>")
        ///'* Parameters:  strParamXML - the only parameter, in XML format.     
        ///'* Purpose:     Retrieves an APD document in binary form of a byte array.
        ///'*              This is for all documents except CCC print images.     
        ///'* Returns:     The byte array containing the binary data.                 
        public string GetDocument(string strParamXML)
        {
            string strReturn = string.Empty;
            bool blnDebugMode = false;
            try
            {
                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMgr = new EcadMgr.CDocument();
                objMLAPDEcadAccessor.InitializeGlobals();               
                strReturn = objMgr.GetDocument(strParamXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return strReturn;
        }

        /// <summary>
        /// Retrieves an XML APD document as a string.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetDocumentXML(string strParamXML)
        {
            string strReturn = string.Empty;
            try
            {
                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new EcadMgr.CDocument();
                strReturn = objMgr.GetDocumentXML(strParamXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return strReturn;
        }

        public string GetReportDocument(string strParamXML)
        {
            string rptDocument = string.Empty;
            try
            {
                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new EcadMgr.CDocument();
                rptDocument = objMgr.GetReportDocument(strParamXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return rptDocument;
        }

        /// <summary>
        /// Get Thumbnail for APD 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width)
        {
            string strReturn = string.Empty;
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new EcadMgr.CDocument();
                strReturn = objMgr.GetAPDThumbnail(strParamXML, Height, Width);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return strReturn;
        }


        /// <summary>
        /// Get Thumbnail for APD 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <param name="Archived"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width, string Archived)
        {
            string strReturn = string.Empty;
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new EcadMgr.CDocument();
                strReturn = objMgr.GetAPDThumbnail(strParamXML, Height, Width, Archived);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return strReturn;
        }



        #endregion

        #region Private Functions

        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                //objMLAPDEcadAccessor.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
