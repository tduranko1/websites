﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EcadAccessorMgr = Lynx.APD.Component.Library.EcadAccessorMgr;
using WriteLog = Lynx.APD.Component.Library.Common;
using System.Xml;

namespace Lynx.APD.Component.Library.EcadAccessor
{
    public class CData
    {
        #region Global Declarations
        private const string APP_NAME = "LAPDEcadAccessor.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CData.");
        EcadAccessorMgr.CData objMgr = null;
        MLAPDEcadAccessor objMLAPDEcadAccessor = null;

        #endregion

        #region Public Functions

        /// <summary>
        /// Syntax:      objEcad.GetXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the only parameter, in XML format.
        /// 
        /// Purpose:     Returns XML data required by ECAD for our Client Access app.
        /// 
        /// Returns:     The XML data, minus any reference and metadata.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetXML(string strParamXML)
        {
            string strReturnValue = string.Empty;
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();

                //Create and call LAPDEcadAccessorMgr to do the real work.                
                objMgr = new EcadAccessorMgr.CData();
                strReturnValue = objMgr.GetXML(strParamXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return strReturnValue;
        }

        /// <summary>
        /// Calls APD DB stored procedures to add data to APD.
        /// This was designed primarly for the Add Comments functionality
        /// in Client Access, but can be extended for anything.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public long PutXML(string strParamXML)
        {
            bool blnDebugMode = false;
            long lngReturn;
            try
            {
                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMgr = new EcadAccessorMgr.CData();
                objMLAPDEcadAccessor.InitializeGlobals();
                lngReturn = objMgr.PutXML(strParamXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMgr = null;
            }
            return lngReturn;
        }

        /// <summary>
        /// Get the report data and create the WebAssignment report, which id failed while submit the claim assingment via Claimpoint
        /// </summary>
        /// <param name="strReportData"></param>
        /// <param name="LynxID"></param>
        /// <returns></returns>
        public string SaveWAReport(string strReportData, long LynxID)
        {
            string strReturnPath = string.Empty;
            EcadAccessorMgr.CWebAssignment objWebAssignment = null;
            XmlDocument objXmlDocument = null;
            WriteLog.WriteLog objWriteLog = new WriteLog.WriteLog();
            try
            {
                objXmlDocument = new XmlDocument();                
                objWebAssignment = new EcadAccessorMgr.CWebAssignment(); //Instance created for EcadAccessorMge class, to call the SaveWAReport().

                objXmlDocument.LoadXml(strReportData);

                strReturnPath = objWebAssignment.SaveWAReport(ref objXmlDocument, LynxID);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "CData.cs - SaveWAReport", ex.Message, string.Concat("Report Data: ", strReportData, " LynxID: ", LynxID));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return strReturnPath;
        }




        /// <summary>
        /// Todo
        /// </summary>
        /// <param name="StrXML"></param>
        /// <param name="LynxID"></param>
        public bool SendShopAssignments(string StrXML, long LynxID, bool blnStaffAppraiserAssignment)
        {
            string strReturnPath = string.Empty; bool isSuccess = false;
            EcadAccessorMgr.CWebAssignment objWebAssignment = null;
            XmlDocument objXmlDocument = null;
            WriteLog.WriteLog objWriteLog = new WriteLog.WriteLog();
            try
            {
                objXmlDocument = new XmlDocument();
                objWebAssignment = new EcadAccessorMgr.CWebAssignment(); //Instance created for EcadAccessorMge class, to call the SendShopAssignments().

                objXmlDocument.LoadXml(StrXML);

                objWebAssignment.SendShopAssignments(objXmlDocument, LynxID, blnStaffAppraiserAssignment);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "CData.cs - SendShopAssignments", ex.Message, string.Concat("StrXML: ", StrXML, " LynxID: ", LynxID));
                throw ex;
                
            }
            finally
            {
                objWriteLog = null;
            }

            return isSuccess;
        }

        #endregion

        #region Private Helper Functions

        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                //objMLAPDEcadAccessor.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
