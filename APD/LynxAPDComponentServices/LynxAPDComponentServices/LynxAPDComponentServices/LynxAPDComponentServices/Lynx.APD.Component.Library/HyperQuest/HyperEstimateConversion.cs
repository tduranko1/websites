﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Xml;
using System.Web;

namespace Lynx.APD.Component.Library.HyperQuest
{
    /// <summary>
    /// Class is use to convert the Hyper Estimate into DBase files.
    /// </summary>
    public class HyperEstimateConversion
    {
        /// <summary>
        /// Get the Hyperquest Estimate and convert into Cicea Dbase file
        /// </summary>
        /// <param name="strEstimateXml"></param>
        public void EstimateConversion(string strEstimateXml)
        {
            DataSet dsEstimate = null;
            XmlTextReader trReader = null;
            StringReader srReader = null;
            string strFileLocation = string.Empty, strRandomValue = string.Empty,
                strDirectoryLocation = string.Empty, strHyperTable = string.Empty;

            try
            {
                dsEstimate = new DataSet();

                if (!string.IsNullOrEmpty(strEstimateXml))
                {
                    srReader = new StringReader(strEstimateXml);
                    trReader = new XmlTextReader(srReader);
                    dsEstimate.ReadXml(trReader);
                }

                if (dsEstimate != null && dsEstimate.Tables.Count > 0)
                {
                    strFileLocation = GetFileLocation();
                    strRandomValue = RandomFileName();
                    strDirectoryLocation = Path.Combine(strFileLocation, strRandomValue);

                    foreach (DataTable dtHyperTable in dsEstimate.Tables)
                    {
                        strHyperTable = dtHyperTable.TableName;

                        switch (strHyperTable)
                        {
                            case "ENV":
                                break;
                            case "AD1":
                                break;
                            case "AD2":
                                break;
                            case "VEH":
                                break;
                            case "LIN":
                                break;
                            case "PFH":
                                break;
                            case "PFP":
                                break;
                            case "PFL":
                                break;
                            case "PFM":
                                break;
                            case "PFT":
                                break;
                            case "PFO":
                                break;
                            case "STL":
                                break;
                            case "TTL":
                                break;
                            case "VEN":
                                break;
                            case "ADC":
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DirectoryLocation"></param>
        /// <param name="RandomValue"></param>
        /// <param name="DBaseType"></param>
        /// <param name="FileType"></param>
        /// <param name="FileName"></param>
        private void CreateDbase(string DirectoryLocation, string RandomValue, string DBaseType, string FileType, string FileName)
        {
            string TemplateName = string.Empty,
                    TemplateSource = string.Empty,
                    SourceLocation = string.Empty,
                    TargetLocation = string.Empty;
            try
            {
                TemplateName = DBaseType + "." + FileType;
                TemplateSource = HttpContext.Current.Server.MapPath(@"\EMSDBConversion\Templates\"); //Need to change the current application path.

                SourceLocation = Path.Combine(TemplateSource, TemplateName);

                if (!string.IsNullOrEmpty(FileName))
                    TargetLocation = Path.Combine(DirectoryLocation, RandomValue + FileName + "." + FileType);
                else
                    TargetLocation = Path.Combine(DirectoryLocation, RandomValue + "." + FileType);

                File.Copy(SourceLocation, TargetLocation, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Create a file for Envelope with .ENV extension
        /// File Name should be xxxxxxx.ENV
        /// Create a xxxxxxx.DBF file for envelope and insert values to xxxxxxx.DBF file
        /// Rename the extension xxxxxxx.DBF to xxxxxxx.ENV
        /// </summary>
        /// <param name="dtEnvXML"></param>
        /// <param name="RandomValue"></param>
        /// <param name="DirectoryLocation"></param>
        private void CreateENVFile(DataTable dtEnvXML, string RandomValue, string DirectoryLocation)
        {
            OleDbConnection oleDbConn = null;
            OleDbCommand cmdENVFormat;

            try
            {
                if (!string.IsNullOrEmpty(DirectoryLocation))
                {
                    // Create the subfolder
                    if (!Directory.Exists(DirectoryLocation))
                        Directory.CreateDirectory(DirectoryLocation);

                    oleDbConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + DirectoryLocation + ";Extended Properties=dBASE IV;");

                    oleDbConn.Open();
                    cmdENVFormat = new OleDbCommand();

                    //Create dbase file for ENV.
                    //CreateENVDBase(RandomValue, oleDbConn, cmdENVFormat);
                    CreateDbase(DirectoryLocation, RandomValue, "ENV", "DBF", string.Empty);

                    if (dtEnvXML.Rows.Count > 0 && dtEnvXML != null)
                    {
                        //cmdENVFormat = new OleDbCommand();
                        cmdENVFormat.Connection = oleDbConn;

                        foreach (DataRow drENV in dtEnvXML.Rows)
                        {
                            cmdENVFormat.CommandText = string.Concat("INSERT INTO ", RandomValue, " VALUES('", ValidateString(drENV["EST_SYSTEM"], 1), "', '", ValidateString(drENV["SW_VERSION"], 10), "','", ValidateString(drENV["DB_VERSION"], 12)
                            , "',",
                            ValidateString(drENV["DB_DATE"], 10)
                            , ",'", ValidateString(drENV["UNQFILE_ID"], 8), "','", ValidateString(drENV["RO_ID"], 8), "','", ValidateString(drENV["ESTFILE_ID"], 38)
                            , "','", ValidateString(drENV["SUPP_NO"], 3), "','", ValidateString(drENV["EST_CTRY"], 3), "','", ValidateString(drENV["TOP_SECRET"], 80), "','", ValidateString(drENV["H_TRANS_ID"], 9)
                            , "','", ValidateString(drENV["H_CTRL_NO"], 9), "','", ValidateString(drENV["TRANS_TYPE"], 1), "',", ValidateString(drENV["STATUS"], 10), ",",
                            ValidateString(drENV["CREATE_DT"], 10), ",'", ValidateString(drENV["CREATE_TM"], 6)
                            , "',",
                           ValidateString(drENV["TRANSMT_DT"], 10)
                            , ",'", ValidateString(drENV["TRANSMT_TM"], 6), "',", ValidateString(drENV["INCL_ADMIN"], 10), ",", ValidateString(drENV["INCL_VEH"], 10)
                            , ",", ValidateString(drENV["INCL_EST"], 10), ",", ValidateString(drENV["INCL_PROFL"], 10)
                            , ",", ValidateString(drENV["INCL_TOTAL"], 10), ",", ValidateString(drENV["INCL_VENDR"], 10)
                            , ",'", ValidateString(drENV["EMS_VER"], 5)
                            , "');");

                            cmdENVFormat.ExecuteNonQuery();
                        }
                    }

                    if (oleDbConn.State == ConnectionState.Open)
                        oleDbConn.Close();

                    if (File.Exists(string.Concat(DirectoryLocation, "\\" + RandomValue, ".DBF")))
                    {
                        File.Move(string.Concat(DirectoryLocation, "\\", RandomValue, ".DBF"), string.Concat(DirectoryLocation, "\\", RandomValue, ".ENV"));
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oleDbConn != null)
                {
                    if (oleDbConn.State == ConnectionState.Open)
                        oleDbConn.Close();

                    oleDbConn.Dispose();
                }
            }
        }

        private void CreateENVDBase(string RandomValue, OleDbConnection oleDbConn, OleDbCommand cmdENVFormat)
        {
            try
            {
                cmdENVFormat.CommandText = "CREATE table " + RandomValue + @" (EST_SYSTEM CHAR(1), SW_VERSION CHAR(10), DB_VERSION CHAR(12),DB_DATE DATE,UNQFILE_ID CHAR(8),
                                                                                        RO_ID CHAR(8),ESTFILE_ID CHAR(38),SUPP_NO CHAR(3),EST_CTRY CHAR(3),TOP_SECRET CHAR(80),
                                                                                        H_TRANS_ID CHAR(9),H_CTRL_NO CHAR(9),TRANS_TYPE CHAR(1),STATUS LOGICAL,CREATE_DT DATE,
                                                                                        CREATE_TM CHAR(6),TRANSMT_DT DATE,TRANSMT_TM CHAR(6),INCL_ADMIN LOGICAL,INCL_VEH LOGICAL,
                                                                                        INCL_EST LOGICAL,INCL_PROFL LOGICAL,INCL_TOTAL LOGICAL,INCL_VENDR LOGICAL,EMS_VER CHAR(5)
                                                                                      );";
                cmdENVFormat.Connection = oleDbConn;
                cmdENVFormat.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                
        /// <summary>
        /// Get Dbase file saving location
        /// </summary>
        /// <returns></returns>
        private string GetFileLocation()
        {
            string DesLocation = string.Empty,
            Location = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        Location = "DBasePath_DEV";
                        break;
                    case "STG":
                        Location = "DBasePath_STG";
                        break;
                    case "PRD":
                        Location = "DBasePath_PRD";
                        break;
                    case "DR":
                        Location = "DBasePath_DR";
                        break;
                    default:
                        Location = "DBasePath_DEV";
                        break;
                }

                DesLocation = ConfigurationManager.AppSettings[Location];

                return DesLocation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Generate random filename with 7 characters
        /// </summary>
        /// <returns></returns>
        private string RandomFileName()
        {
            string DirectoryLocation = string.Empty,
                TempFileName = string.Empty;
            FileInfo fileInfo;

            try
            {
                //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                //var stringChars = new char[7];
                //var random = new Random();

                //for (int i = 0; i < stringChars.Length; i++)
                //    stringChars[i] = chars[random.Next(chars.Length)];
                //var finalString = new String(stringChars);

                ////Check File Exist ot not
                //DirectoryLocation = Path.Combine(GetFileLocation(), finalString);
                //if (File.Exists(DirectoryLocation + ".Zip"))
                //    RandomFileName();

                TempFileName = Path.GetTempFileName();
                fileInfo = new FileInfo(TempFileName);
                TempFileName = fileInfo.Name.Replace(fileInfo.Extension, "");

                if (TempFileName.Length >= 7)
                    TempFileName = TempFileName.Substring(0, 7);
                else
                    RandomFileName();

                //Check File Exist ot not
                DirectoryLocation = Path.Combine(GetFileLocation(), TempFileName);
                if (File.Exists(DirectoryLocation + ".Zip"))
                    RandomFileName();

                return TempFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validate the string length
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="Size"></param>
        /// <returns></returns>
        private string ValidateString(object FieldName, int Size)
        {
            string rtnField = "",
                FieldValue = string.Empty;

            try
            {
                FieldValue = Convert.ToString(FieldName);

                if (!string.IsNullOrEmpty(FieldValue) && FieldValue != "")
                    if (FieldValue.Trim().Length <= Size)
                        rtnField = FieldValue.Trim().Replace("'", "");
                    else if (FieldValue.Trim().Length > Size)
                        rtnField = FieldValue.Trim().Replace("'", "").Substring(0, Size);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtnField;
        }
    }
}
