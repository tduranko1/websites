﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lynx.APD.Component.Library.APDDataAccessor;
using Lynx.APD.Component.Library.PartnerDataMgr;
using System.Xml;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Xml.Xsl;
using System.Reflection;
using System.Web.Hosting;
using System.Security.Cryptography;
using System.Web;

namespace Lynx.APD.Component.Library.HyperQuest
{
    /// <summary>
    /// Process the hyper documents into APD claim
    /// </summary>
    public class IncomingHyper
    {
        bool IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["HQDebug"]);
        Common.WriteLog objWriteLog = new Common.WriteLog();
        Hashtable htAudited,
            htOriginal;
        bool blnAudited = false,
            blnOriginal = false;

        class Document
        {
            public string strUserID = string.Empty,
                strLynxID = string.Empty,
                strClaimNumber = string.Empty,
                strImageDate = string.Empty,
                strFilePath = string.Empty,
                strDocumentSource = string.Empty,
                strEstimate = string.Empty,
                strImageType = string.Empty,
                strDocumentVerCode = string.Empty,
                strDateTime = string.Empty,
                strDocumentType = string.Empty,
                strEstimateTypeCD = string.Empty,
                strSequenceNumber = string.Empty,
                AssingmentID = string.Empty,
                VehicleNumber = string.Empty;
            public bool blnNotifyEvent = false;
        }


        /// <summary>
        /// This function to execute the query
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        private string ExecuteQuery(string strQuery)
        {
            SqlDataReader sqlDataReader = null;
            APDDataAccessor.CDataAccessor objDataAccessor = null;
            string strDBPartnername = string.Empty,
                strRetVal = string.Empty,
                strConnectionString = string.Empty;
            try
            {
                objDataAccessor = new APDDataAccessor.CDataAccessor();
                strConnectionString = objDataAccessor.GetConnectionString("APD");
                using (SqlConnection sqlConn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(strQuery, sqlConn))
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectionTimeOut"]);

                        sqlConn.Open();
                        sqlDataReader = sqlCommand.ExecuteReader();

                        if (sqlDataReader.Read())
                            strRetVal = sqlDataReader.GetValue(0).ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - ExecuteQuery()", ex.Message, "");
                throw ex;
            }
            return strRetVal;
        }

        /// <summary>
        ///  Execute the stored procedure
        /// </summary>
        private string ExecuteSp(string strProcedureName, Hashtable htParams)
        {
            APDDataAccessor.CDataAccessor objDataAccessor = null;
            long lngReturn = 0;

            try
            {
                objDataAccessor = new APDDataAccessor.CDataAccessor();
                lngReturn = objDataAccessor.ExecuteSp(strProcedureName, htParams, "APD");
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - ExecuteSp()", ex.Message, "");
                throw ex;
            }
            finally
            {
                objDataAccessor = null;
            }
            return Convert.ToString(lngReturn);
        }

        /// <summary>
        /// Get the Incoming path to save hyperquest receive xml
        /// </summary>
        /// <returns></returns>
        private string GetIncomingPath()
        {
            string returnPath = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnPath = ConfigurationManager.AppSettings["Hyperquest_DEV"];
                        break;
                    case "STG":
                        returnPath = ConfigurationManager.AppSettings["Hyperquest_STG"];
                        break;
                    case "PRD":
                        returnPath = ConfigurationManager.AppSettings["Hyperquest_PRD"];
                        break;
                    case "DR":
                        returnPath = ConfigurationManager.AppSettings["Hyperquest_DR"];
                        break;
                    default:
                        returnPath = ConfigurationManager.AppSettings["Hyperquest_DEV"];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnPath;
        }

        /// <summary>
        /// Get the Storage path to save hyperquest receive xml
        /// </summary>
        /// <returns></returns>
        private string GetStoragePath()
        {
            string returnPath = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnPath = ConfigurationManager.AppSettings["ClaimPath_DEV"];
                        break;
                    case "STG":
                        returnPath = ConfigurationManager.AppSettings["ClaimPath_STG"];
                        break;
                    case "PRD":
                        returnPath = ConfigurationManager.AppSettings["ClaimPath_PRD"];
                        break;
                    case "DR":
                        returnPath = ConfigurationManager.AppSettings["ClaimPath_DR"];
                        break;
                    default:
                        returnPath = ConfigurationManager.AppSettings["ClaimPath_DEV"];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnPath;
        }

        /// <summary>
        /// Get ClaimNumber using LynxId
        /// </summary>
        /// <param name="strLynxID"></param>
        /// <returns></returns>
        private string GetClaimNumber(string strLynxID)
        {
            Hashtable htParam = null;
            string strClaimNumber = string.Empty, strReturnXml = string.Empty;
            CDataAccessor objDataAccessor = null;
            XmlDocument xmlDocument = null;
            try
            {
                htParam = new Hashtable();
                xmlDocument = new XmlDocument();
                objDataAccessor = new CDataAccessor();
                htParam.Add("@LynxID", strLynxID);

                //Execute SP to retreive the claim number
                strReturnXml = objDataAccessor.ExecuteSpNamedParamsXML("uspSessionGetClaimDetailWSXML", htParam, "APD");

                if (!string.IsNullOrEmpty(strReturnXml.Trim()))
                {
                    xmlDocument.LoadXml(strReturnXml);
                    if (xmlDocument.SelectSingleNode("Root/Claim") != null)
                        strClaimNumber = xmlDocument.SelectSingleNode("Root/Claim/@ClientClaimNumber").InnerText;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strClaimNumber;
        }

        /// <summary>
        /// Get vehicle number from APD DB
        /// </summary>
        /// <param name="lngLynxID"></param>
        /// <param name="isClaimAspectID"></param>
        /// <returns></returns>
        private string GetVehicleNumber(long lngLynxID, bool isClaimAspectID)
        {
            string strReturn = string.Empty, strVehicleNumber = string.Empty, strReturnData = string.Empty, strClaimAspectID = string.Empty;
            CDataAccessor objDataAccessor = null;
            Hashtable htParams = null;
            XmlDocument xmlDoc = null;
            XmlNodeList xmlVehicleNodeList = null;
            try
            {
                objDataAccessor = new CDataAccessor();
                htParams = new Hashtable();
                xmlDoc = new XmlDocument();
                htParams.Add("@LynxID", lngLynxID);
                strReturn = objDataAccessor.ExecuteSpNamedParamsXML("uspEntityOwnerRepGetListWSXML", htParams, "APD");

                if (!string.IsNullOrEmpty(strReturn.Trim()))
                {
                    xmlDoc.LoadXml(strReturn);
                    xmlVehicleNodeList = xmlDoc.SelectNodes("Root/Entity");

                    foreach (XmlNode xmlNode in xmlVehicleNodeList)
                    {
                        if (xmlNode.Attributes["ClaimAspectNumber"] != null)
                        {
                            if (xmlNode.Attributes["ClaimAspectNumber"].InnerText != "0")
                            {
                                strVehicleNumber = xmlNode.Attributes["ClaimAspectNumber"].InnerText;
                                strClaimAspectID = xmlNode.Attributes["ClaimAspectID"].InnerText;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    if (!isClaimAspectID)
                        strReturnData = strVehicleNumber;
                    else
                        strReturnData = strClaimAspectID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnData;
        }

        /// <summary>
        /// To get the sequence number of the estimate documents and determine the document type
        /// </summary>
        /// <param name="nodeTemp"></param>
        /// <param name="objDocumentParam"></param>
        private void GetEstimateDetails(XmlNode nodeTemp, ref Document objDocumentParam)
        {
            Document objDocument;
            XmlDocument xmlEstimate;
            string strEstimateFileDataXml = string.Empty;
            try
            {
                objDocument = new Document();
                objDocument = objDocumentParam;
                xmlEstimate = new XmlDocument();
                if (nodeTemp != null)
                {
                    nodeTemp = nodeTemp.SelectSingleNode("AttachmentIntegrity/IntegrityBinary/EmbeddedAttachment");
                    if (nodeTemp != null && !string.IsNullOrEmpty(nodeTemp.InnerText))
                    {
                        strEstimateFileDataXml = Encoding.UTF8.GetString(System.Convert.FromBase64String(nodeTemp.InnerText));
                        xmlEstimate = new XmlDocument();
                        xmlEstimate.LoadXml(strEstimateFileDataXml);

                        nodeTemp = xmlEstimate.SelectSingleNode("//SUPP_NO");
                        if (nodeTemp != null)
                        {
                            if (nodeTemp.InnerText.Contains('S'))
                            {
                                objDocument.strDocumentType = "Supplement";
                                objDocument.strSequenceNumber = Convert.ToString(Convert.ToInt32(nodeTemp.InnerText.Replace("S", "")));
                            }
                            else
                            {
                                objDocument.strDocumentType = "Estimate";
                                objDocument.strSequenceNumber = "0";
                            }
                        }
                        else
                            throw new Exception("Err-005");
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - GetEstimateDetails()", ex.Message, "");
                throw ex;
            }
        }

        /// <summary>
        /// Construct file path to save the file in the shared folder
        /// </summary>
        ///  <param name="strGUID"></param>
        /// <param name="strBasePath"></param>
        /// <param name="strDate"></param>
        /// <param name="strFileType"></param>
        /// <param name="strLynxID"></param>
        /// <returns></returns>
        private string ConstructFilename(string strGUID, string strBasePath, string strDate, string strFileType, string strLynxID, bool blnAPD, ref string strReturnPath, string strEstimateName)
        {
            string strDocPath = string.Empty, strFullFilePath = string.Empty, strFileName = string.Empty;
            int iLenLynxID = 0;
            try
            {

                if (string.IsNullOrEmpty(strGUID))
                    strGUID = Guid.NewGuid().ToString();

                if (strGUID.Trim().Contains("-"))
                    strGUID = strGUID.Replace("-", string.Empty);
                iLenLynxID = strLynxID.Length;

                if (blnAPD)
                {
                    for (int intIndex = 0; intIndex < 4; intIndex++)
                        strDocPath = string.Concat(strDocPath, "\\", strLynxID.Substring(strLynxID.Length - 4, 4).Substring(intIndex, 1));

                    strBasePath = string.Concat(strBasePath, strDocPath);
                    strFileName = string.Concat("\\", strEstimateName, "-", strDate, "-", "LYNXID", strLynxID, strFileType);
                    strReturnPath = string.Concat(strDocPath, strFileName);
                    strFullFilePath = string.Concat(strBasePath, strFileName);
                }
                else
                {
                    strDocPath = string.Concat(strDocPath, "\\", strEstimateName, "-", strGUID, strDate, "-", "LYNXID", strLynxID, strFileType);
                    strFullFilePath = string.Concat(strBasePath, strDocPath);
                }

                if (!Directory.Exists(strBasePath))
                    Directory.CreateDirectory(strBasePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strFullFilePath;
        }

        /// <summary>
        /// Generate the response Xml
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="strRqUID"></param>
        /// <param name="strAsyncRqUID"></param>
        /// <param name="strPartnerKey"></param>
        /// <returns></returns>
        private string GenerateResponse(string strMessage, string strRqUID, string strAsyncRqUID, string strPartnerKey)
        {
            Hashtable htErrorCode = new Hashtable();
            StringBuilder strErrorXml = new StringBuilder();
            string strErrorCode = string.Empty,
                strErrorMessage = string.Empty;
            try
            {
                htErrorCode.Add("Err-001", "LynxID/Vehicle number doesn't exist in the request.");
                htErrorCode.Add("Err-002", "DocumentVerCode doesn't exist in the request");
                htErrorCode.Add("Err-003", "Create date time is not valid");
                htErrorCode.Add("Err-004", "DocAttachment doesn't exist in the request");
                htErrorCode.Add("Err-005", "SUPP_NO node doesn't exist in the request");
                htErrorCode.Add("Err-006", "AttachmentFileType doesn't exist in the request");
                htErrorCode.Add("Err-007", "AttachmentMemo doesn't exist in the request");
                htErrorCode.Add("Err-008", "RawOrginalEstimate xml doesn't exist in the request");
                htErrorCode.Add("Err-009", "RawFinalEstimate xml doesn't exist in the request");
                htErrorCode.Add("Err-010", "PassThroughInfo node value is missing in the request");

                if (strMessage.Contains("Err-"))
                {
                    strErrorCode = strMessage;
                    strErrorMessage = htErrorCode[strMessage].ToString();
                }
                else
                {
                    strErrorCode = "Err-000";
                    strErrorMessage = "An internal error occured while processing the request. Please contact LYNX";
                }

                strErrorXml.Append("<AttachmentAddRs xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                strErrorXml.Append("<RqUID>" + strRqUID + "</RqUID>");
                strErrorXml.Append("<AsyncRqUID>" + strAsyncRqUID + "</AsyncRqUID>");
                strErrorXml.Append("<PartnerKey>" + strPartnerKey + "</PartnerKey>");
                strErrorXml.Append("<ApplicationStatus>");

                if (strMessage == "Accept")
                    strErrorXml.Append("<ApplicationStatusCode>Accept</ApplicationStatusCode>");
                else
                {

                    strErrorXml.Append("<ApplicationStatusCode>Reject</ApplicationStatusCode>");
                    strErrorXml.Append("<ApplicationError><ErrorCode>" + strErrorCode + "</ErrorCode><Severity>Error</Severity><ErrorDesc>" + strErrorMessage + "</ErrorDesc></ApplicationError>");
                }
                strErrorXml.Append("</ApplicationStatus>");
                strErrorXml.Append("</AttachmentAddRs>");

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - GenerateResponse()", ex.Message, string.Concat("RequestID: ", strRqUID));
            }
            return strErrorXml.ToString();
        }



        /// <summary>
        /// Generate the workflow response Xml
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="strPassthruData"></param>
        /// <param name="strEventID"></param>
        /// <param name="strDescription"></param>
        /// <returns></returns>
        private string GenerateWorkflowResponse(string strMessage, string strPassthruData, string strEventID, string strDescription)
        {
            Hashtable htErrorCode = new Hashtable();
            StringBuilder strErrorXml = new StringBuilder();
            string strErrorCode = string.Empty,
                strErrorMessage = string.Empty;
            try
            {
                htErrorCode.Add("Err-001", "PassthruData doesn't exist in the request.");
                htErrorCode.Add("Err-002", "Event ID doesn't exist in the request");

                if (strMessage.Contains("Err-"))
                {
                    strErrorCode = strMessage;
                    strErrorMessage = htErrorCode[strMessage].ToString();
                }
                else
                {
                    strErrorCode = "Err-000";
                    strErrorMessage = "An internal error occured while processing the request. Please contact LYNX";
                }

                strErrorXml.Append("<WorflowNotifyRs xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                strErrorXml.Append("<PassthruData>" + strPassthruData + "</PassthruData>");
                strErrorXml.Append("<EventID>" + strEventID + "</EventID>");
                strErrorXml.Append("<Description>" + HttpUtility.HtmlEncode(strDescription) + "</Description>");
                strErrorXml.Append("<ApplicationStatus>");

                if (strMessage == "Accept")
                    strErrorXml.Append("<ApplicationStatusCode>Accept</ApplicationStatusCode>");
                else
                {

                    strErrorXml.Append("<ApplicationStatusCode>Reject</ApplicationStatusCode>");
                    strErrorXml.Append("<ApplicationError><ErrorCode>" + strErrorCode + "</ErrorCode><Severity>Error</Severity><ErrorDesc>" + strErrorMessage + "</ErrorDesc></ApplicationError>");
                }
                strErrorXml.Append("</ApplicationStatus>");
                strErrorXml.Append("</WorflowNotifyRs>");

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - GenerateWorkflowResponse()", ex.Message, string.Concat("PassthruData: ", strPassthruData));
            }
            return strErrorXml.ToString();
        }



        /// <summary>
        /// Writes the passed Byte Array to the passed file.
        /// </summary>
        //private void WriteBinaryDataToFile(string strFileName, byte[] arrBuffer)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(strFileName.Trim()))
        //            throw new Exception("HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile: File Name param was blank.");

        //        if (IsDebug)
        //            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("Before writing the file."), "");
        //        using (var fs = new BinaryWriter(new FileStream(strFileName, FileMode.Append, FileAccess.Write)))
        //        {
        //            if (IsDebug)
        //                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("Reading the binary data."), "");

        //            fs.Write(arrBuffer);

        //            if (IsDebug)
        //                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("File write in the shared folder"), "");

        //            fs.Close();

        //            if (IsDebug)
        //                objWriteLog.LogEvent("COM Conversion", "END", "HyperQuest.IncomingHyper.cs- WriteBinaryDataToFile", string.Concat("Completed"), "");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile()", string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), "");
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Writes the passed string to the passed file.
        /// This method creates for the overcome the out of memory issue
        /// </summary>
        private void WriteBinaryDataToFile(string strFileName, byte[] arrBuffer)
        {
            string strFileData = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(strFileName.Trim()))
                    throw new Exception("HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile: File Name param was blank.");

                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("Before writing the file."), "");
                using (var fs = new FileStream(strFileName, FileMode.Append, FileAccess.Write))
                {
                    if (IsDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("File stream object created for ", strFileName), "");

                    using (var cs = new CryptoStream(fs, new FromBase64Transform(), CryptoStreamMode.Write))
                    {
                        if (IsDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("CryptoStream object created for ", strFileName), "");

                        using (var sw = new StreamWriter(cs))
                        {
                            if (IsDebug)
                                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("StreamWriter object created for ", strFileName), "");

                            strFileData = System.Text.Encoding.UTF8.GetString(arrBuffer);

                            sw.Write(strFileData);

                            // if (IsDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile", string.Concat("StreamWriter written the file : ", strFileName), "");


                            sw.Flush();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - WriteBinaryDataToFile()", string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), "");
                throw ex;
            }
        }

        /// <summary>
        /// Document insertion process
        /// </summary>
        /// <param name="strDBFilePath"></param>
        /// <param name="strAttachmentMemo"></param>
        /// <param name="strDateTime"></param>
        /// <param name="objDocumentParam"></param>
        private void DocumentInsertion(string strDBFilePath, string strAttachmentMemo, string strDateTime, string strDocumentName, Document objDocumentParam)
        {
            Document objDocument;
            Hashtable htDocument = null;
            string strPertains = string.Empty;
            try
            {
                htDocument = new Hashtable();
                objDocument = new Document();
                objDocument = objDocumentParam;

                if (objDocument.strImageType.ToUpper().Trim() == "PDF" || objDocument.strImageType.ToUpper().Trim() == "JPEG" || objDocument.strImageType.ToUpper().Trim() == "JPG" || ((strDocumentName.ToUpper().Equals("RAWFINALESTIMATE") || strDocumentName.ToUpper().Equals("RAWESTIMATEREVIEW")) && objDocument.strImageType.ToUpper().Trim() == "XML"))
                {

                    //Pertains to insert the document into the correct vehicle.
                    strPertains = objDocument.VehicleNumber;
                    strPertains = string.Concat("VEH", strPertains);

                    //Loading the parameters to execute the document insert stored procedure
                    htDocument.Add("@NTUserID", GetConfig("WorkFlow/NTUserID"));
                    htDocument.Add("@PertainsTo", strPertains);
                    htDocument.Add("@AssignmentID", objDocument.AssingmentID);
                    htDocument.Add("@LynxID", objDocument.strLynxID);
                    htDocument.Add("@CreatedDate", strDateTime);
                    htDocument.Add("@ImageDate", objDocument.strImageDate);
                    htDocument.Add("@ImageLocation", strDBFilePath.Replace("\\", "\\\\"));
                    htDocument.Add("@DocumentSource", objDocument.strDocumentSource);
                    htDocument.Add("@DocumentType", objDocument.strDocumentType);
                    htDocument.Add("@NotifyEvent", objDocument.blnNotifyEvent);

                    if (!string.IsNullOrEmpty(objDocument.strSequenceNumber.Trim()))
                        htDocument.Add("@SupplementSeqNumber", objDocument.strSequenceNumber);
                    else
                        htDocument.Add("@SupplementSeqNumber", DBNull.Value);
                    htDocument.Add("@ImageType", objDocument.strImageType);

                    if (!string.IsNullOrEmpty(objDocument.strEstimateTypeCD.Trim()))
                        htDocument.Add("@HQEstimateTypeCD", objDocument.strEstimateTypeCD);
                    else
                        htDocument.Add("@HQEstimateTypeCD", DBNull.Value);

                    if (strAttachmentMemo == "HyperQuest Final Print Estimate")
                    {
                        blnAudited = true;
                        htAudited = htDocument;
                    }
                    else if (strAttachmentMemo == "HyperQuest Original Print Estimate")
                    {
                        blnOriginal = true;
                        htOriginal = htDocument;
                    }
                    else
                    //Insert Photograph document and trigger the photograph worflow
                    {
                        //Document insertion
                        ExecuteSp("uspDocumentInsDetail", htDocument);

                        //Workflow trigger for Photograph document
                        if (objDocument.strImageType.ToUpper() == "JPG" || objDocument.strImageType == "JPG")
                            SendWorkFlowNotification("79", objDocument.strLynxID, objDocument.AssingmentID, string.Empty, objDocument.VehicleNumber);
                        //Trigger workflow for Other PDF documents.
                        else if (objDocument.strImageType == "PDF")
                            SendWorkFlowNotification("9", objDocument.strLynxID, objDocument.AssingmentID, string.Empty, objDocument.VehicleNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentInsertion()", ex.Message, "");
                throw ex;
            }
            finally
            {
                htDocument = null;
            }
        }

        /// <summary>
        ///  Original and audited document insertion and xml mapping
        /// </summary>
        /// <param name="xmlTransactionDoc"></param>
        /// <param name="strDBFilePath"></param>
        /// <param name="strAssignmentID"></param>
        /// <param name="strVehicleNumber"></param>
        /// <param name="blnAgreedPrice"></param>
        /// <param name="objDocumentParam"></param>
        private void DocumentMapping(XmlDocument xmlTransactionDoc, string strDBFilePath, string strAssignmentID, string strVehicleNumber, bool blnAgreedPrice, Document objDocumentParam)
        {
            Document objDocument;
            HyperQuestPartnerDataXfer objHyperQuestPartnerDataXfer = null;
            XmlNode nodeTemp = null;
            XmlDocument xmlEstimate = null;
            long lngDocumentID;
            string strEstimateFileDataXml = string.Empty;
            try
            {
                objDocument = new Document();
                objHyperQuestPartnerDataXfer = new HyperQuestPartnerDataXfer();
                xmlEstimate = new XmlDocument();

                objDocument = objDocumentParam;
                lngDocumentID = 0;
                if (blnOriginal == true)
                {
                    // Original estimate document insertion and xml mapping
                    lngDocumentID = Convert.ToInt32(ExecuteSp("uspDocumentInsDetail", htOriginal));

                    nodeTemp = null;
                    nodeTemp = xmlTransactionDoc.SelectSingleNode("//DocAttachment[AttachmentFileName = 'RawOrginalEstimate.XML']");
                    if (nodeTemp != null)
                    {
                        nodeTemp = nodeTemp.SelectSingleNode("AttachmentIntegrity/IntegrityBinary/EmbeddedAttachment");
                        if (nodeTemp != null && !string.IsNullOrEmpty(nodeTemp.InnerText))
                        {
                            strEstimateFileDataXml = Encoding.UTF8.GetString(System.Convert.FromBase64String(nodeTemp.InnerText));
                            xmlEstimate.LoadXml(strEstimateFileDataXml);
                            objHyperQuestPartnerDataXfer.TransferDataToAPD_v2(xmlEstimate, lngDocumentID, "", true);

                            //Tigger Workflow for Estimate Review
                            SendWorkFlowNotification("80", objDocument.strLynxID, objDocument.AssingmentID, "", objDocument.VehicleNumber);
                        }
                        else
                            objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentMapping()", "Original estimate XML data is not available for the estimate pdf(DocumentID: " + lngDocumentID + ")", "");
                    }
                    else
                        objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentMapping()", "Original estimate XML is not available for the estimate pdf(DocumentID: " + lngDocumentID + ")", "");
                }

                lngDocumentID = 0;
                if (blnAudited == true)
                {
                    // Finally insert the audited document and xml mapping
                    lngDocumentID = Convert.ToInt32(ExecuteSp("uspDocumentInsDetail", htAudited));

                    // Update the Agreed price value to corresponding audited estimate document
                    if (blnAgreedPrice == true)
                        ExecuteQuery("update utb_document set AgreedPriceMetCD = 'Y' where DocumentID='" + lngDocumentID + "' and DocumentTypeID ='3'");

                    nodeTemp = null;
                    nodeTemp = xmlTransactionDoc.SelectSingleNode("//DocAttachment[AttachmentFileName = 'RawFinalEstimate.XML']");

                    if (nodeTemp != null)
                    {
                        nodeTemp = nodeTemp.SelectSingleNode("AttachmentIntegrity/IntegrityBinary/EmbeddedAttachment");
                        if (nodeTemp != null && !string.IsNullOrEmpty(nodeTemp.InnerText))
                        {
                            strEstimateFileDataXml = Encoding.UTF8.GetString(System.Convert.FromBase64String(nodeTemp.InnerText));
                            xmlEstimate.LoadXml(strEstimateFileDataXml);
                            objHyperQuestPartnerDataXfer.TransferDataToAPD_v2(xmlEstimate, lngDocumentID, "", false);

                            //Tigger Workflow for Estimate Review
                            SendWorkFlowNotification("80", objDocument.strLynxID, objDocument.AssingmentID, "", objDocument.VehicleNumber);

                            // Cancellation need to be trigger
                            if (objDocument.strDocumentVerCode.Trim().ToUpper() == "V")
                                CancellationProcess(objDocument.strLynxID, strAssignmentID, lngDocumentID, strDBFilePath, strVehicleNumber);
                        }
                        else
                            objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentMapping()", "Audited estimate XML data is not available for the estimate pdf(DocumentID: " + lngDocumentID + ")", "");
                    }
                    else
                        objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentMapping()", "Audited estimate XML is not available for the estimate pdf(DocumentID: " + lngDocumentID + ")", "");
                }

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - DocumentMapping()", ex.Message, "");
                throw ex;
            }
            finally
            {
                objHyperQuestPartnerDataXfer = null;
                xmlEstimate = null;
            }
        }

        /// <summary>
        /// Insert the HQ job details
        /// </summary>
        /// <param name="strGuid"></param>
        /// <param name="strRqUID"></param>
        /// <param name="strAsyncRqUID"></param>
        /// <param name="strPartnerKey"></param>
        /// <param name="strInscCompID"></param>
        /// <param name="strIncomingFilePath"></param>
        /// <param name="objDocumentParam"></param>
        private void JobInsertion(string strGuid, string strRqUID, string strAsyncRqUID, string strPartnerKey, string strInscCompID, string strIncomingFilePath, Document objDocumentParam)
        {
            Document objDocument;
            Hashtable htHqJobs;
            try
            {
                htHqJobs = new Hashtable();
                objDocument = objDocumentParam;
                htHqJobs.Add("@vJobID", strGuid);
                htHqJobs.Add("@vRqUID", strRqUID);
                htHqJobs.Add("@vAsyncRqUID", strAsyncRqUID);
                htHqJobs.Add("@vPartnerKey", strPartnerKey);
                htHqJobs.Add("@iInscCompID", strInscCompID);
                htHqJobs.Add("@iLynxID", objDocument.strLynxID);
                htHqJobs.Add("@vIODirection", "I");
                htHqJobs.Add("@dtReceivedDate", objDocument.strImageDate);
                htHqJobs.Add("@vDocumentTypeName", "Final Estimate");
                htHqJobs.Add("@vDocumentImageType", "XML");
                htHqJobs.Add("@vDocumentSource", "Hyperquest");
                htHqJobs.Add("@vDocumentImageLocation", strIncomingFilePath);
                htHqJobs.Add("@vJobStatus", "Progress");
                htHqJobs.Add("@iJobPriority", "1");
                htHqJobs.Add("@dtJobCreatedDate", objDocument.strImageDate);
                htHqJobs.Add("@bEnabledFlag", "1");
                htHqJobs.Add("@iSysLastUserID", "");

                ExecuteSp("uspHyperquestInsDRPJobDocument", htHqJobs);

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - JobInsertion()", ex.Message, "");
                throw ex;
            }
            finally
            {
                htHqJobs = null;
            }
        }

        /// <summary>
        /// Insert row into the HyperQuest SVC tables.
        /// </summary>
        /// <param name="strLynxID"></param>
        /// <param name="strAssignmentID"></param>
        /// <param name="lngDocumentID"></param>
        /// <param name="strImageFilePath"></param>
        /// <param name="strVehicleNumber"></param>
        private void CancellationProcess(string strLynxID, string strAssignmentID, long lngDocumentID, string strImageFilePath, string strVehicleNumber)
        {
            string strGUID = string.Empty, strClaimAspectID = string.Empty, strClientClaimNumber = string.Empty;
            Hashtable htParam = null;
            try
            {
                strGUID = Guid.NewGuid().ToString();
                strGUID = strGUID.Replace("{", string.Empty);
                strGUID = strGUID.Replace("}", string.Empty);
                strGUID = strGUID.Replace("-", string.Empty);

                //Get ClaimAspectID using LynxID
                //strClaimAspectID = GetVehicleNumber(Convert.ToInt64(strLynxID), true);
                strClaimAspectID = strVehicleNumber;

                strClientClaimNumber = GetClaimNumber(strLynxID);

                //Trigger WorkFlowNotification for Cancellation
                SendWorkFlowNotification("91", strLynxID, strAssignmentID, strClaimAspectID, "");

                //Insert data into the DB table to maintain the status of the Cancellation
                htParam = new Hashtable();
                htParam.Add("@vJobID", strGUID);
                htParam.Add("@iInscCompID", 259);
                htParam.Add("@iLynxID", strLynxID);
                htParam.Add("@vClaimNumber", strClientClaimNumber); //Need to use ClaimNumber
                htParam.Add("@iDocumentID", lngDocumentID);
                htParam.Add("@iAssignmentID", strAssignmentID);
                htParam.Add("@dtReceivedDate", Convert.ToString(DateTime.Now));
                htParam.Add("@iDocumentTypeID", 3);
                htParam.Add("@vDocumentTypeName", "Estimate");
                htParam.Add("@vDocumentSource", "HyperQuest");
                htParam.Add("@vDocumentImageType", "PDF");
                htParam.Add("@vDocumentImageLocation", strImageFilePath.Replace("\\", "\\\\"));
                htParam.Add("@iClaimAspectServiceChannelID", 0);
                htParam.Add("@iClaimAspectID", strClaimAspectID);
                htParam.Add("@vJobXSLFile", string.Empty);
                htParam.Add("@vJobStatus", "Cancelled");
                htParam.Add("@vJobStatusDetails", "HQ Final Estimate");
                htParam.Add("@iJobPriority", 1);
                htParam.Add("@dtJobCreatedDate", Convert.ToString(DateTime.Now));
                htParam.Add("@bEnabledFlag", true);
                htParam.Add("@xJobXML", "NONE");
                htParam.Add("@iSysLastUserID", 0);

                ExecuteSp("uspHyperquestInsDAJobDocument", htParam);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Trigger WorkFlow notification for Cancellation and other documet types.
        /// </summary>
        /// <param name="EventID"></param>
        /// <param name="strLynxID"></param>
        /// <param name="strAssignmentID"></param>
        /// <param name="strClaimAspectID"></param>
        /// <param name="strVehicleNumber"></param>
        private void SendWorkFlowNotification(string EventID, string strLynxID, string strAssignmentID, string strClaimAspectID, string strVehicleNumber)
        {
            string strKey = string.Empty;
            Hashtable htParam = null;

            try
            {
                strKey = string.Concat("$AssignmentID$=", strAssignmentID);

                htParam = new Hashtable();
                htParam.Add("@EventID", EventID);
                htParam.Add("@LynxID", strLynxID);
                if (EventID == "91")
                {
                    htParam.Add("@ClaimAspectID", strClaimAspectID);
                    htParam.Add("@PertainsTo", DBNull.Value);
                }
                else
                    htParam.Add("@PertainsTo", string.Concat("VEH", strVehicleNumber));
                htParam.Add("@Description", DBNull.Value);
                htParam.Add("@Key", strKey);
                htParam.Add("@UserID", GetConfig("WorkFlow/SysUserID"));
                if (EventID == "80")
                    htParam.Add("@ConditionValue", "NONUSERSOURCE");
                else
                    htParam.Add("@ConditionValue", DBNull.Value);

                ExecuteSp("uspWorkFlowNotifyEvent", htParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Writes binary data to a Base64 encoded XML DOM element.
        /// </summary>
        private object ReadBinaryDataFromDomElement(XmlElement objElement)
        {
            object varType = null;
            try
            {

                //Save the data type for restoration later.
                varType = objElement.GetType();

                //Ensure the type is base64.
                objElement.GetType().Equals("bin.base64");

                //Restore the data type.
                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.MBaseUtils.cs - ReadBinaryDataFromDomElement()", ex.Message, "");
                throw ex;
            }
            finally
            {
                varType = null;
            }
            //Now get the binary data.
            return objElement.InnerText;
        }

        /// <summary>
        /// Xslt Xml Transform
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private XmlDocument XsltTransform(XmlDocument xmlDoc)
        {
            string strXml = string.Empty,
                strXsltPath = string.Empty, strXslBasePath = string.Empty;
            try
            {
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - XsltTransform()", "Process Start", "");
                strXml = xmlDoc.OuterXml;
                strXslBasePath = Convert.ToString(ConfigurationManager.AppSettings["HQTemplatePath"]);
                strXsltPath = string.Concat(strXslBasePath, "HypertoPartnerConversion.xslt");
                // strXsltPath = string.Concat(strXslBasePath, "Lynx.APD.Component.Library\\HyperQuest\\HypertoPartnerConversion.xslt");//HyperQuest/HypertoPartnerConversion.xslt");
                // strXsltPath = strXsltPath.Replace("LynxAPDComponentServices\\remove", "Lynx.APD.Component.Library\\HyperQuest\\HypertoPartnerConversion.xslt");
                //"C:\\WEBSites\\APD\\LynxAPDComponentServices\\LynxAPDComponentServices\\LynxAPDComponentServices\\LynxAPDComponentServices\\Lynx.APD.Component.Library\\HyperQuest\\HypertoPartnerConversion.xslt";//string.Concat(HostingEnvironment.ApplicationPhysicalPath, "\\Lynx.APD.Component.Library\\HyperQuest\\HypertoPartnerConversion.xslt");
                //System.Web.Hosting.HostingEnvironment.MapPath("//");//HyperQuest/HypertoPartnerConversion.xslt");
                //System.IO.Path.GetFileName(Assembly.GetExecutingAssembly().Location);
                //System.Web.Hosting.HostingEnvironment.MapPath("");
                //Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "HyperQuest\\HypertoPartnerConversion.xslt");
                //Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"HyperQuest\HypertoPartnerConversion.xslt");
                //strXsltPath = string.Concat(strXsltPath, "\\", "HypertoPartnerConversion.xslt");
                using (StringReader sReader = new StringReader(strXml))
                using (XmlReader xReader = XmlReader.Create(sReader))
                using (StringWriter sWriter = new StringWriter())
                using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                {
                    XslCompiledTransform transformxslt = new XslCompiledTransform();
                    transformxslt.Load(strXsltPath);
                    transformxslt.Transform(xReader, xWriter);
                    strXml = sWriter.ToString();
                    xmlDoc.LoadXml(strXml);
                }
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - XsltTransform()", "Process End", "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - XsltTransform()", ex.Message, "");
                throw;
            }
            return xmlDoc;
        }

        /// <summary>
        /// load document types with APD document types.
        /// </summary>
        /// <returns></returns>
        private Hashtable LoadDocumentType()
        {
            Hashtable htDocumentTypes = null;
            try
            {
                htDocumentTypes = new Hashtable();
                htDocumentTypes.Add("Final Closing Report", "Desk Audit Summary Report");
                htDocumentTypes.Add("MI", "MI");
                htDocumentTypes.Add("PDF", "Other");
                htDocumentTypes.Add("JPEG", "Photograph");
                htDocumentTypes.Add("JPG", "Photograph");
                htDocumentTypes.Add("XML", "Estimate EMS");
            }
            catch (Exception ex)
            {
            }
            return htDocumentTypes;
        }

        /// <summary>
        /// Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public string GetConfig(string strSetting)
        {
            string strReturn = string.Empty, strMachineName = string.Empty, strAppPath = string.Empty, strEnvPath = string.Empty,
                strMachOnly = string.Empty, strEnvInsPath = string.Empty, strMachPath = string.Empty, strMachInstPath = string.Empty, strInstPath = string.Empty, strConfigPath = string.Empty;
            XmlDocument xmlDocConfig = null;
            bool blnFoundInst = false;
            string mstrInstance = "test";
            XmlNode xmlNode;
            try
            {
                xmlDocConfig = new XmlDocument();
                strAppPath = "Root/Application/";
                strMachineName = Environment.MachineName;
                strEnvPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']]/";
                strEnvInsPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']"; //Need to Add //[Instance[@name='" & mstrInstance & "']]]/"
                strMachOnly = "//Machine[@name='" + strMachineName + "']"; //[Instance[@name='" & mstrInstance & "']]"
                strMachPath = strAppPath + "Environment/Machine[@name='" + strMachineName + "']/";
                strMachInstPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "'][Instance[@name='" + mstrInstance + "']]]/";
                strInstPath = strMachPath; //"Instance[@name='" & mstrInstance & "']/"

                strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                xmlDocConfig.Load(strConfigPath);

                xmlNode = xmlDocConfig.SelectSingleNode(strMachPath + strSetting);
                if (xmlNode == null)
                    blnFoundInst = false;

                if (!blnFoundInst && xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strEnvPath + strSetting);

                if (xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strAppPath + strSetting);

                strReturn = xmlNode.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Receive the HyerQuest request xml here
        /// </summary>
        /// <param name="strRequestXml"></param>
        public XmlDocument ProcessHyperIncoming(string strRequestXml)
        {
            Document objDocument;
            XmlDocument xmlHqRequest = null, xmlTransactionDoc = null,
                xmlReturn = null, xmlEstimateReview = null;
            XmlNodeList nodeListDocment = null;
            XmlNode nodeTemp = null, nodeLynxID = null, nodeStatus = null, nodeSettlementType = null;
            XmlElement objElementBinaryData = null;
            Hashtable htAssignment, htDocumentTypes, htDocumentType, htTotalLossInd;
            DateTime dateTime;
            StringBuilder strErrorXml;
            object objBinaryData = null;
            byte[] arrFileData = null;
            string strIncomingPath = string.Empty, strIncomingFilePath = string.Empty, strStoragePath = string.Empty,
                strDBFilePath = string.Empty, strGUID = string.Empty, strRqUID = string.Empty,
                strAsyncRqUID = string.Empty, strPartnerKey = string.Empty, strDateTime = string.Empty,
                strReturn = string.Empty, strClaimLynxID = string.Empty, strAttachmentMemo = string.Empty,
                strEstimateFileDataXml = string.Empty, strAssignmentID = string.Empty, strVehicleNumber = string.Empty,
                strKey = string.Empty, strDocumentName = string.Empty, strSettlementType = string.Empty, strInsCompID = string.Empty,
                strLynxID = string.Empty, strAttachmentType = string.Empty, strCheckDRP = string.Empty, strDocumentType = string.Empty,
                strPotentialTotalLossInd = string.Empty;
            string[] arrString;
            bool blnAgreedPrice = false,
                blnDRPAssignment = false,
                blnChoiceAssignment = false;
            long lnDocument;
            try
            {
                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", "Process started", string.Concat("XML Data: ", strRequestXml));

                xmlHqRequest = new XmlDocument();
                xmlTransactionDoc = new XmlDocument();
                xmlReturn = new XmlDocument();
                xmlEstimateReview = new XmlDocument();
                strErrorXml = new StringBuilder();
                htAudited = new Hashtable();
                htOriginal = new Hashtable();
                htTotalLossInd = new Hashtable();
                objDocument = new Document();


                objDocument.strDateTime = DateTime.Now.ToString("MMddyyyyHHmmssfff");
                strIncomingPath = GetIncomingPath();
                strStoragePath = GetStoragePath();
                htDocumentTypes = LoadDocumentType();

                if (!string.IsNullOrEmpty(strRequestXml.Trim()))
                    xmlHqRequest.LoadXml(strRequestXml);
                else
                    throw new Exception("Xml string is null or empty");

                var strNewXml = xmlHqRequest.OuterXml;
                strNewXml = Regex.Replace(strNewXml, @"xmlns[:xsi|:xsd]*="".*?""", "");
                xmlTransactionDoc.LoadXml(strNewXml);

                nodeLynxID = xmlTransactionDoc.SelectSingleNode("//PassThroughInfo");
                if (nodeLynxID != null)
                {
                    if (!string.IsNullOrEmpty(nodeLynxID.InnerText.Trim()))
                    {
                        strClaimLynxID = nodeLynxID.InnerText.ToUpper();
                        strClaimLynxID = strClaimLynxID.Replace("LYNXID:", string.Empty);
                        arrString = strClaimLynxID.Split('-');

                        if (arrString.Length > 1)
                        {
                            objDocument.strLynxID = arrString[0];
                            strLynxID = arrString[0];
                            strVehicleNumber = arrString[1];
                            if (arrString.Length == 3)
                                strDocumentType = arrString[2];
                            //blnDRPAssignment = true;
                        }
                        else
                        {
                            objDocument.strLynxID = arrString[0];
                            strLynxID = arrString[0];
                            strVehicleNumber = GetVehicleNumber(Convert.ToInt64(strLynxID), false);
                        }
                    }
                    else
                        throw new Exception("Err-010");

                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), string.Concat("Getting LynxID:", strLynxID), "");

                    //Verify the Assignment Type
                    strCheckDRP = ExecuteQuery("Select InitialAssignmentTypeID From utb_Claim_Aspect Where LynxID= " + strLynxID + " and ClaimAspectNumber = " + strVehicleNumber);

                    if (strCheckDRP.Trim() == "4")
                        blnDRPAssignment = true;
                    else if (strCheckDRP.Trim() == "17")
                        blnChoiceAssignment = true;
                    else if (strCheckDRP.Trim() == "7")
                    {
                        blnDRPAssignment = false;
                        blnChoiceAssignment = false;
                    }
                }

                objDocument.VehicleNumber = strVehicleNumber;


                if (string.IsNullOrEmpty(objDocument.strLynxID) || string.IsNullOrEmpty(strVehicleNumber))
                    throw new Exception("Err-001");

                //strInsCompID = ExecuteQuery("select InsuranceCompanyID from utb_claim with (nolock) where LynxID = '" + strLynxID + "'");

                nodeTemp = xmlTransactionDoc.SelectSingleNode("//RqUID");
                if (nodeTemp != null)
                    strRqUID = nodeTemp.InnerText;

                nodeTemp = xmlTransactionDoc.SelectSingleNode("//AsyncRqUID");
                if (nodeTemp != null)
                    strAsyncRqUID = nodeTemp.InnerText;


                //Construciting filename using RqUID or AsyncRqUID
                if (!string.IsNullOrEmpty(strRqUID))
                    strGUID = strRqUID;
                else if (!string.IsNullOrEmpty(strAsyncRqUID))
                    strGUID = strAsyncRqUID;
                else
                    strGUID = Guid.NewGuid().ToString();


                strIncomingFilePath = ConstructFilename(strGUID, strIncomingPath, objDocument.strDateTime, ".xml", objDocument.strLynxID, false, ref strDBFilePath, "HQ-FinalEstimate");


                //Save the request xml into the incoming shared folder
                xmlTransactionDoc.Save(strIncomingFilePath);

                //if (IsDebug)
                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), "Saving the reveived XML", strIncomingFilePath);


                nodeTemp = xmlTransactionDoc.SelectSingleNode("//PartnerKey");
                if (nodeTemp != null)
                    strPartnerKey = nodeTemp.InnerText;

                //Calling function to insert the Job details
                //JobInsertion(strGuid, strRqUID, strAsyncRqUID, strPartnerKey, strInsCompID, strLynxID, strIncomingFilePath);

                //Adding LynxID as a new element 
                XmlNode newElem = xmlTransactionDoc.CreateNode("element", "LynxID", "");
                newElem.InnerText = objDocument.strLynxID;
                XmlNode root = xmlTransactionDoc.SelectSingleNode("//DocumentInfo");
                root.AppendChild(newElem);

                //strVehicleNumber = GetVehicleNumber(Convert.ToInt64(strLynxID), false);

                //Getting an AssignmentID
                htAssignment = new Hashtable();
                htAssignment.Add("@LynxID", objDocument.strLynxID);
                htAssignment.Add("@VehicleNumber", strVehicleNumber);
                htAssignment.Add("@AssignmentSuffix", DBNull.Value);
                strAssignmentID = ExecuteSp("uspWorkflowGetAssignmentID", htAssignment);

                nodeStatus = xmlTransactionDoc.SelectSingleNode("//DocumentVerCode");
                if (nodeStatus == null || string.IsNullOrEmpty(nodeStatus.InnerText))
                    throw new Exception("Err-002");

                objDocument.AssingmentID = strAssignmentID;
                objDocument.strDocumentVerCode = nodeStatus.InnerText;

                //xsl transforamtion to process the documents
                // xmlTransactionDoc = XsltTransform(xmlTransactionDoc);

                nodeTemp = xmlTransactionDoc.SelectSingleNode("//CreateDateTime");
                if (nodeTemp == null || string.IsNullOrEmpty(nodeTemp.InnerText) || !DateTime.TryParse(nodeTemp.InnerText, out dateTime))
                    throw new Exception("Err-003");

                objDocument.strImageDate = Convert.ToDateTime(nodeTemp.InnerText).ToString("s");
                nodeListDocment = xmlTransactionDoc.SelectNodes("//DocAttachment");

                if (nodeListDocment.Count == 0)
                    throw new Exception("Err-004");

                objDocument.strDocumentSource = "HyperQuest";
                htDocumentType = new Hashtable();
                htDocumentType.Add("@iLynxID", objDocument.strLynxID);
                htDocumentType.Add("@iVehicleNumber", strVehicleNumber);
                lnDocument = Convert.ToInt64(ExecuteSp("uspHQEstimateSupplementDetails", htDocumentType));

                //Process the documents from HQ
                foreach (XmlNode node in nodeListDocment)
                {
                    objDocument.strSequenceNumber = string.Empty;
                    objDocument.strEstimateTypeCD = string.Empty;
                    objDocument.strDocumentType = string.Empty;
                    strDocumentName = string.Empty;
                    strAttachmentMemo = string.Empty;
                    strAttachmentType = string.Empty;

                    nodeTemp = node.SelectSingleNode("AttachmentType");
                    if (nodeTemp != null && !string.IsNullOrEmpty(nodeTemp.InnerText))
                        strAttachmentType = nodeTemp.InnerText;

                    nodeTemp = node.SelectSingleNode("AttachmentFileType");
                    if (nodeTemp == null || string.IsNullOrEmpty(nodeTemp.InnerText))
                        throw new Exception("Err-006");

                    objDocument.strImageType = nodeTemp.InnerText;

                    nodeTemp = node.SelectSingleNode("AttachmentMemo");
                    if (nodeTemp == null || string.IsNullOrEmpty(nodeTemp.InnerText))
                        throw new Exception("Err-007");
                    else
                    {
                        strAttachmentMemo = nodeTemp.InnerText;

                        //Process the Original Estimates from HQ
                        if (string.Equals(strAttachmentMemo, "HyperQuest Original Print Estimate", StringComparison.OrdinalIgnoreCase))
                        {
                            objDocument.strEstimateTypeCD = "O";
                            objDocument.blnNotifyEvent = false;

                            if (blnDRPAssignment == true || blnChoiceAssignment == true)
                            {
                                nodeTemp = xmlTransactionDoc.SelectSingleNode("//DocAttachment[AttachmentFileName = 'RawOrginalEstimate.XML']");
                                if (nodeTemp != null)
                                    GetEstimateDetails(nodeTemp, ref objDocument);
                                else
                                    throw new Exception("Err-008");
                            }
                            else
                            {

                                if (lnDocument == -1)
                                {
                                    objDocument.strDocumentType = "Estimate";
                                    objDocument.strSequenceNumber = "0";
                                }
                                else if (lnDocument >= 0)
                                {
                                    objDocument.strDocumentType = "Supplement";
                                    objDocument.strSequenceNumber = Convert.ToString(lnDocument + 1);
                                }
                            }
                        }
                        //Process the Audited Estimates from HQ
                        else if (string.Equals(strAttachmentMemo, "HyperQuest Final Print Estimate", StringComparison.OrdinalIgnoreCase))
                        {
                            objDocument.strEstimateTypeCD = "A";
                            objDocument.blnNotifyEvent = false;

                            if (blnDRPAssignment == true || blnChoiceAssignment == true)
                            {

                                nodeTemp = xmlTransactionDoc.SelectSingleNode("//DocAttachment[AttachmentFileName = 'RawFinalEstimate.XML']");
                                if (nodeTemp != null)
                                    GetEstimateDetails(nodeTemp, ref objDocument);
                                else
                                    throw new Exception("Err-009");
                            }
                            else
                            {

                                if (lnDocument == -1)
                                {
                                    objDocument.strDocumentType = "Estimate";
                                    objDocument.strSequenceNumber = "0";
                                }
                                else if (lnDocument >= 0)
                                {
                                    objDocument.strDocumentType = "Supplement";
                                    objDocument.strSequenceNumber = Convert.ToString(lnDocument + 1);
                                }
                            }
                        }
                        else if (string.Equals(strAttachmentType, "Final Closing Report", StringComparison.OrdinalIgnoreCase))
                        {
                            //Assign the document type name for Summary of Changes document
                            if (blnDRPAssignment == true || blnChoiceAssignment == true)
                            {
                                objDocument.strEstimateTypeCD = "";
                                objDocument.strDocumentType = "Estimate Review Summary of Changes";
                                objDocument.blnNotifyEvent = false;
                            }
                            else
                            {
                                objDocument.strEstimateTypeCD = "";
                                objDocument.strDocumentType = "Desk Audit Summary Report";
                                objDocument.blnNotifyEvent = false;
                            }
                        }
                        else if (string.Equals(strAttachmentType, "MI", StringComparison.OrdinalIgnoreCase))
                        {
                            objDocument.strEstimateTypeCD = "";
                            objDocument.strDocumentType = Convert.ToString(htDocumentTypes[objDocument.strImageType.ToUpper()]);
                            //Tell Document insert SP that no need to trigger the Event for Photograph document.
                            //Need to trigger manually from here.
                            if (objDocument.strImageType.ToUpper() == "JPG" || objDocument.strImageType.ToUpper() == "JPEG")
                                objDocument.blnNotifyEvent = false;
                        }
                    }

                    if (objDocument.strImageType.ToUpper() != "XML" && string.IsNullOrEmpty(objDocument.strDocumentType))
                    {
                        StringBuilder sbDocumentNotMatch = new StringBuilder();

                        sbDocumentNotMatch.Append("Document type does not mapped with APD for the following params");
                        sbDocumentNotMatch.Append(": objDocument.strLynxID - " + objDocument.strLynxID);
                        sbDocumentNotMatch.Append(": objDocument.VehicleNumber - " + objDocument.VehicleNumber);
                        sbDocumentNotMatch.Append(": objDocument.strImageDate - " + objDocument.strImageDate);
                        sbDocumentNotMatch.Append(": objDocument.strDateTime - " + objDocument.strDateTime);

                        objWriteLog.LogEvent("COM Conversion", "WARNING", "HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", sbDocumentNotMatch.ToString(), string.Empty);
                        continue;
                    }

                    nodeTemp = node.SelectSingleNode("AttachmentFileName");
                    if (nodeTemp != null)
                    {
                        strDocumentName = nodeTemp.InnerText;
                        arrString = strDocumentName.Split('.');
                        if (arrString.Length > 1)
                            strDocumentName = arrString[0];

                    }


                    objDocument.strDateTime = DateTime.Now.ToString("MMddyyyyHHmmssfff");


                    //Construciting filename using RqUID or AsyncRqUID
                    if (!string.IsNullOrEmpty(strRqUID))
                        strGUID = strRqUID;
                    else if (!string.IsNullOrEmpty(strAsyncRqUID))
                        strGUID = strAsyncRqUID;
                    else
                        strGUID = Guid.NewGuid().ToString();


                    objDocument.strFilePath = ConstructFilename(strGUID, strStoragePath, objDocument.strDateTime, string.Concat(".", objDocument.strImageType), objDocument.strLynxID, true, ref strDBFilePath, strDocumentName);

                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), "File Path", objDocument.strFilePath);

                    objElementBinaryData = (XmlElement)node.SelectSingleNode("AttachmentIntegrity/IntegrityBinary/EmbeddedAttachment");
                    objBinaryData = ReadBinaryDataFromDomElement(objElementBinaryData);

                    //Glsd451 changed for out of memory issue for FromBase64String
                    // arrFileData = Convert.FromBase64String(Convert.ToString(objBinaryData));

                    arrFileData = System.Text.UnicodeEncoding.UTF8.GetBytes((Convert.ToString(objBinaryData)));
                    WriteBinaryDataToFile(objDocument.strFilePath, arrFileData);

                    //check the estimate document whether it is agreed price or non-agreed
                    if (string.Equals(strDocumentName, "RawEstimateReview", StringComparison.OrdinalIgnoreCase))
                    {
                        xmlEstimateReview.Load(objDocument.strFilePath);

                        if (xmlEstimateReview.SelectSingleNode("//EstimateReview/PotentialTotalLossInd") != null)
                            strPotentialTotalLossInd = xmlEstimateReview.SelectSingleNode("//EstimateReview/PotentialTotalLossInd").InnerText;

                        if (strPotentialTotalLossInd.ToUpper() == "Y")
                        {
                            htTotalLossInd = new Hashtable();
                            htTotalLossInd.Add("@LynxID", objDocument.strLynxID.Trim());
                            htTotalLossInd.Add("@VehicleNumber", strVehicleNumber.Trim());
                            htTotalLossInd.Add("@SysLastUserID", 0);
                            ExecuteSp("uspTotalLossIndicatorInsUpd", htTotalLossInd);

                            SendWorkFlowNotification("111", objDocument.strLynxID, objDocument.AssingmentID, GetVehicleNumber(Convert.ToInt64(strLynxID), true), objDocument.VehicleNumber);

                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), string.Format("Execute the uspTotalLossIndicatorInsUpd: Params: LynxID = {0}, VehicleNumber = {1}", objDocument.strLynxID.Trim(), strVehicleNumber.Trim()),string.Format("RawEstimateReview xml path : {0}", objDocument.strFilePath));
                        }

                        //Set the SummaryOfChanges XML document type to RawEstimateReview xml.
                        objDocument.strDocumentType = "SummaryOfChanges XML";
   
                        nodeSettlementType = xmlEstimateReview.DocumentElement.SelectSingleNode("SettlementType");

                        if (nodeSettlementType != null)
                        {
                            strSettlementType = nodeSettlementType.InnerText;
                            if (strSettlementType == "AGPRI")
                                blnAgreedPrice = true;
                        }
                    }

                    if (strAttachmentMemo.ToUpper().Trim().Equals("HYPERQUEST RESPONSE XML FILE") && strDocumentName.ToUpper().Trim().Equals("RAWFINALESTIMATE") && objDocument.strImageType.ToUpper().Trim().Equals("XML"))
                    {
                        nodeTemp = xmlTransactionDoc.SelectSingleNode("//DocAttachment[AttachmentFileName = 'RawFinalEstimate.XML']");
                        if (nodeTemp != null)
                            GetEstimateDetails(nodeTemp, ref objDocument);
                        else
                            throw new Exception("Err-009");

                        if (objDocument.strDocumentType == "Estimate")
                            objDocument.strDocumentType = "Estimate XML";
                        else if(objDocument.strDocumentType == "Supplement")
                            objDocument.strDocumentType = "Supplement XML";
                    }

                    // Insert all documents except the original and audited estimate
                    DocumentInsertion(strDBFilePath, strAttachmentMemo, strDateTime, strDocumentName, objDocument);
                }

                // Finally insert the original and audited estimate document with xml mapping
                DocumentMapping(xmlTransactionDoc, strDBFilePath, strAssignmentID, strVehicleNumber, blnAgreedPrice, objDocument);
                //Generate response to PartnerDataPoster Service and it will return back the response as Strongly typed object.
                strReturn = GenerateResponse("Accept", strRqUID, strAsyncRqUID, strPartnerKey);

                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), "Return XML", strReturn);

                xmlReturn.LoadXml(strReturn);

                if (IsDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", "Process END", "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", ":Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("LynxID: ", strLynxID, " RequestID: ", strRqUID));
                strReturn = GenerateResponse(ex.Message, strRqUID, strAsyncRqUID, strPartnerKey);
                xmlReturn.LoadXml(strReturn);
            }
            finally
            {
                xmlHqRequest = null;
                xmlTransactionDoc = null;
                strErrorXml = null;
                nodeListDocment = null;
            }

            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "HyperQuest.IncomingHyper.cs - ProcessHyperIncoming()", "Return Xml: ", xmlReturn.OuterXml);
            return xmlReturn;
        }



        /// <summary>
        /// Process the HQ Assignments
        /// </summary>
        /// <param name="strAssignmentID"></param>
        /// <returns></returns>
        public bool ProcessHQAssignment(string strAssignmentID)
        {
            bool blnReturn = false;
            APDDataAccessor.CDataAccessor objDataAccessor = null;
            Hashtable htParam = null;
            string strAssignmentXml = string.Empty, strLynxID = string.Empty, strClaimAspectServiceChannelID = string.Empty, strClaimAspectID = string.Empty,
                strGUID = string.Empty, strInsuranceCompany = string.Empty, strClaimNumber = string.Empty, strVehicleNumber = string.Empty, strShopLocationID = string.Empty,
                strDocumentTypeID = string.Empty, strDocumentType = string.Empty, strJobFileName = string.Empty, strDescription = string.Empty, strPathway = string.Empty,
                strAssignmentType = string.Empty, strServiceChannelCD = string.Empty, strShopName = string.Empty;
            XmlDocument objXmlDoc = null;
            XmlNode objAssignmentDetailsNode = null;

            try
            {
                objDataAccessor = new CDataAccessor();
                htParam = new Hashtable();
                objXmlDoc = new XmlDocument();
                htParam.Add("@AssignmentID", strAssignmentID);

                strAssignmentXml = objDataAccessor.OpenRecordsetAsClientSideXML("uspWorkflowSendShopAssignWSXML", htParam, "APD");

                if (!string.IsNullOrEmpty(strAssignmentXml))
                {
                    objXmlDoc.LoadXml(strAssignmentXml);

                    //Getting the assignment details to insert into Jobdetails.
                    objAssignmentDetailsNode = objXmlDoc.SelectSingleNode("/Assignment");

                    if (objAssignmentDetailsNode != null)
                    {
                        strPathway = objAssignmentDetailsNode.Attributes["EstimatingPackage"].Value;
                        strLynxID = objAssignmentDetailsNode.Attributes["LynxId"].Value;
                        strAssignmentID = objAssignmentDetailsNode.Attributes["AssignmentID"].Value;
                        strAssignmentType = objAssignmentDetailsNode.Attributes["AssignmentCode"].Value;
                        strVehicleNumber = objAssignmentDetailsNode.Attributes["VehicleNumber"].Value;
                    }

                    if (strAssignmentType == "New")
                    {
                        strDocumentType = "Shop Assignment";
                        strDocumentTypeID = "22";
                        strJobFileName = "AssignmentAdd.xsl";
                    }
                    else if (strAssignmentType == "Cancel")
                    {
                        strDocumentType = "Cancel Assignment";
                        strDocumentTypeID = "81";
                        strJobFileName = "AssignmentCancel.xsl";
                    }

                    objAssignmentDetailsNode = null;
                    objAssignmentDetailsNode = objXmlDoc.SelectSingleNode("/Assignment/Coverage");
                    if (objAssignmentDetailsNode != null)
                    {
                        strClaimNumber = objAssignmentDetailsNode.Attributes["ClaimNumber"].Value;
                    }

                    objAssignmentDetailsNode = null;
                    objAssignmentDetailsNode = objXmlDoc.SelectSingleNode("/Assignment/Claim");
                    if (objAssignmentDetailsNode != null)
                    {
                        strInsuranceCompany = objAssignmentDetailsNode.Attributes["InsuranceCompanyID"].Value;
                    }

                    objAssignmentDetailsNode = null;
                    objAssignmentDetailsNode = objXmlDoc.SelectSingleNode("/Assignment/Vehicle");
                    if (objAssignmentDetailsNode != null)
                    {
                        strClaimAspectID = objAssignmentDetailsNode.Attributes["ClaimAspectID"].Value;
                        strClaimAspectServiceChannelID = objAssignmentDetailsNode.Attributes["ClaimAspectServiceChannelID"].Value;
                        strServiceChannelCD = objAssignmentDetailsNode.Attributes["ServiceChannelCD"].Value;
                    }

                    objAssignmentDetailsNode = null;
                    objAssignmentDetailsNode = objXmlDoc.SelectSingleNode("/Assignment/Shop");
                    if (objAssignmentDetailsNode != null)
                    {
                        strShopName = objAssignmentDetailsNode.Attributes["ShopName"].Value;
                        strShopLocationID = objAssignmentDetailsNode.Attributes["ShopLocationID"].Value;
                    }

                    //Insert the job details into Jobtable.
                    if (strPathway == "HQ Link - CCC" || strPathway == "HQ Link - Audatex" || strPathway == "HQ Link - Mitchell")
                    {
                        strGUID = Guid.NewGuid().ToString();
                        htParam = new Hashtable();
                        htParam.Add("@vJobID", strGUID);
                        htParam.Add("@iInscCompID", strInsuranceCompany);
                        htParam.Add("@iLynxID", strLynxID);
                        htParam.Add("@vClaimNumber", strClaimNumber);
                        htParam.Add("@iDocumentID", 0);
                        htParam.Add("@iAssignmentID", 0);
                        htParam.Add("@iVehicleID", strVehicleNumber);
                        htParam.Add("@iShopLocationID", strShopLocationID);
                        htParam.Add("@iSeqID", 0);
                        htParam.Add("@dtReceivedDate", DateTime.Now);
                        htParam.Add("@iDocumentTypeID", strDocumentTypeID);
                        htParam.Add("@vDocumentTypeName", strDocumentType);
                        htParam.Add("@vDocumentSource", "HyperQuest");
                        htParam.Add("@vDocumentImageType", string.Empty);
                        htParam.Add("@vDocumentImageLocation", string.Empty);
                        htParam.Add("@iClaimAspectServiceChannelID", strClaimAspectServiceChannelID);
                        htParam.Add("@iClaimAspectID", strClaimAspectID);
                        htParam.Add("@vJobXSLFile", strJobFileName);
                        htParam.Add("@vJobStatus", "Unprocessed");
                        htParam.Add("@vJobStatusDetails", "APD Shop Assignment to Hyperquest (MANUAL)");
                        htParam.Add("@iJobPriority", 1);
                        htParam.Add("@dtJobCreatedDate", DateTime.Now);
                        htParam.Add("@bEnabledFlag", 1);
                        htParam.Add("@xJobXML", string.Empty);
                        htParam.Add("@iSysLastUserID", 0);

                        objDataAccessor.ExecuteSpNamedParams("uspHyperquestInsDAJobDocument", htParam, "APD");
                    }

                    if (strAssignmentType == "New")
                    {
                        string strKey = string.Empty;
                        strDescription = string.Concat("PROGRAM SHOP (HQ): Assignment for ", strLynxID, "-", strVehicleNumber, " was sent to ", strShopName);
                        strKey = string.Concat("$AssignmentID$=", strAssignmentID);

                        htParam = new Hashtable();
                        htParam.Add("@EventID", Convert.ToInt64(25));
                        htParam.Add("@LynxID", strLynxID);
                        htParam.Add("@PertainsTo", string.Concat("VEH", strVehicleNumber));
                        htParam.Add("@ServiceChannelCD", strServiceChannelCD);
                        htParam.Add("@Description", strDescription);
                        htParam.Add("@Key", strKey);
                        htParam.Add("@UserID", 0);

                        ExecuteSp("uspWorkFlowNotifyEvent", htParam);
                        //Workflow to trigger the for HQ new assignments.
                        //SendWorkFlowNotification("25", strLynxID, strAssignmentID, strClaimAspectID, strVehicleNumber);
                    }
                    blnReturn = true;
                }
            }
            catch (Exception ex)
            {
                blnReturn = false;
                throw ex;
            }
            return blnReturn;
        }

        public XmlDocument SendWorkflowNotification(string strPassthruData, string strEventID, string strDescription)
        {
            XmlDocument xmlClaimSearch, xmlReturn = null;
            XmlNode xmlNodeServiceChannel;
            Hashtable hashtable = null;
            Common.WriteLog objWritelog = null;
            APDDataAccessor.CDataAccessor objDataAccessor = null;
            string strAssignmentID = string.Empty,
                strPertainsTo = "VEH",
                strKey = string.Empty,
                strConditionValue = string.Empty,
                strReturnXml = string.Empty,
                strLynxID = string.Empty,
                strVehicleNumber = string.Empty,
                strReturn = string.Empty,
                strPassthru_data = string.Empty;
            string[] arrString;
            try
            {
                objWritelog = new Common.WriteLog();
                objDataAccessor = new CDataAccessor();
                xmlClaimSearch = new XmlDocument();
                xmlReturn = new XmlDocument();

                if (IsDebug)
                    objWritelog.LogEvent("COM Conversion", "START", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("SendWorkflowNotification Started"), "");

                if (string.IsNullOrEmpty(strPassthruData))
                    throw new Exception("Err-001");

                if (string.IsNullOrEmpty(strEventID))
                    throw new Exception("Err-002");

                strDescription = strDescription.Trim();

                if (strDescription == "\n")
                    strDescription = string.Empty;

                strPassthru_data = strPassthruData;
                strPassthruData = strPassthruData.Replace("LYNXID:", string.Empty);
                arrString = strPassthruData.Split('-');

                if (arrString.Length > 1)
                {
                    strLynxID = arrString[0];
                    strVehicleNumber = arrString[1];
                }

                //Getting an AssignmentID
                hashtable = new Hashtable();
                hashtable.Add("@LynxID", strLynxID);
                hashtable.Add("@VehicleNumber", strVehicleNumber);
                hashtable.Add("@AssignmentSuffix", DBNull.Value);
                strAssignmentID = ExecuteSp("uspWorkflowGetAssignmentID", hashtable);

                //Getting Assignment Type name
                hashtable = new Hashtable();
                hashtable.Add("@ClaimNumber", DBNull.Value);
                hashtable.Add("@LynxId", strLynxID);
                hashtable.Add("@InvolvedNameLast", DBNull.Value);
                hashtable.Add("@InsuranceCompanyID", DBNull.Value);
                strReturnXml = objDataAccessor.ExecuteSpNamedParamsXML("uspClaimSearchWSXML", hashtable, "APD");

                if (!string.IsNullOrEmpty(strReturnXml.Trim()))
                {
                    xmlClaimSearch.LoadXml(strReturnXml);
                    xmlNodeServiceChannel = xmlClaimSearch.SelectSingleNode("Root/Claim/Vehicle[@VehicleNumber= " + strVehicleNumber + "]/ServiceChannel");
                    if (xmlNodeServiceChannel != null)
                        strConditionValue = xmlNodeServiceChannel.Attributes["ServiceChannelCD"].Value;
                }

                // strEventID = GetConfig(string.Concat(new string[] { "WorkFlow/EventCodes/Code[@name='", strCodeName, "']" }));
                if (string.IsNullOrEmpty(strKey.Trim()))
                {
                    //this could be an RRP assignment. We will formulate a special key and pass it in
                    //  we don't want to make another call to figure out the assignment sequence number
                    //  but let the workflowNotifyEvent proc to figure it out.
                    strKey = string.Concat(new string[] { "$AssignmentID$=", strAssignmentID });
                }
                //Add vehicle number to pertains to if needed.
                //if (strPertainsTo == "VEH")
                strPertainsTo = string.Concat(new string[] { strPertainsTo, strVehicleNumber });

                if (!string.IsNullOrEmpty(strEventID))
                {
                    strDescription = strDescription.Replace("'", "''");
                    hashtable = new Hashtable();
                    hashtable.Add("@EventID", Convert.ToInt64(strEventID));
                    hashtable.Add("@LynxID", strLynxID);
                    hashtable.Add("@PertainsTo", strPertainsTo);
                    if (!string.IsNullOrEmpty(strDescription))
                        hashtable.Add("@Description", strDescription);
                    hashtable.Add("@Key", strKey);
                    hashtable.Add("@UserID", GetConfig("WorkFlow/SysUserID"));
                    hashtable.Add("@ConditionValue", strConditionValue);

                    objDataAccessor.ExecuteSpNamedParams(GetConfig("WorkFlow/SPName"), hashtable, "APD");
                    if (IsDebug)
                        objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("SendWorkflowNotification completed. Workflow Notification Sent : ", strEventID, ", PassthruData : ", strPassthruData, ", Description : ", strDescription), "");
                }
                else
                {
                    objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("EventID For ", strEventID, " not found in the config file.  Bypassing workflow call."), "");
                }

                objWritelog.LogEvent("COM Conversion", "Debugging", "PartnerDataMgr.CPartner.cs - SendWorkflowNotification", string.Concat("SendWorkflowNotification completed. Workflow Notification Sent : ", strEventID, ", PassthruData : ", strPassthruData, ", Description : ", strDescription), "");

                strReturn = GenerateWorkflowResponse("Accept", strPassthru_data, strEventID, strDescription);
                xmlReturn.LoadXml(strReturn);

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "HyperQuest.IncomingHyper.cs - SendWorkflowNotification()", ex.Message, string.Concat("LynxID: ", strLynxID, " EventID: ", strEventID));
                strReturn = GenerateWorkflowResponse(ex.Message, strPassthruData, strEventID, strDescription);
                xmlReturn.LoadXml(strReturn);
            }
            finally
            {
                objWritelog = null;
            }
            return xmlReturn;
        }
    }
}