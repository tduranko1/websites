﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
 <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <DocumentUpload>
      <Documents>
        <UserID>
          <xsl:value-of select="AttachmentAddRq/RqUID"/>
        </UserID>
        <LynxID>
          <xsl:value-of select="AttachmentAddRq/DocumentInfo/LynxID"/>
        </LynxID>
        <CreateDateTime>
          <xsl:value-of select="AttachmentAddRq/DocumentInfo/CreateDateTime"/>
        </CreateDateTime>
        <PertainsTo></PertainsTo>
        <ClaimAspectServiceChannelID></ClaimAspectServiceChannelID>
        <SuppSeqNumber></SuppSeqNumber>
        <DocumentSource>Hyperquest</DocumentSource>
        <NotifyEvent></NotifyEvent>
        <DirectionalCD></DirectionalCD>
        <SendToCarrierStatusCD></SendToCarrierStatusCD>
        <WarrantyFlag></WarrantyFlag>

        <xsl:for-each select="AttachmentAddRq/DocAttachment">
          <Document>
            <DocumentType>
              <xsl:value-of select="AttachmentType"/>
            </DocumentType>
            <FileName>
              <xsl:value-of select="AttachmentFileName"/>
            </FileName>
            <FileMemo>
              <xsl:value-of select="AttachmentMemo"/>
            </FileMemo>
            <FileExtension>
              <xsl:value-of select="AttachmentFileType"/>
            </FileExtension>
            <FileLength>
              <xsl:value-of select="AttachmentLength"/>
            </FileLength>
            <FileData  dt:dt='bin.base64' xmlns:dt='urn:schemas-microsoft-com:datatypes'>
              <xsl:value-of select="AttachmentIntegrity/IntegrityBinary/EmbeddedAttachment"/>
            </FileData>
          </Document>
        </xsl:for-each>

      </Documents>
    </DocumentUpload>
  </xsl:template>
</xsl:stylesheet>
