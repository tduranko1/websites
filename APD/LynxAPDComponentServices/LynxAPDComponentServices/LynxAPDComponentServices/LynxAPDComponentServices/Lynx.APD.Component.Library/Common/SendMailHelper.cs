﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace Lynx.APD.Component.Library.Common
{
    class SendMailHelper
    {
        public string From
        {
            get;
            set;
        }

        public string To
        {
            get;
            set;
        }

        public string Subject
        {
            get;
            set;
        }

        public string Body
        {
            get;
            set;
        }

        public string CC
        {
            get;
            set;
        }

        public string BCC
        {
            get;
            set;
        }

        /// <summary>
        /// Get smtp server details
        /// </summary>
        /// <returns></returns>
        public string GetHostName()
        {
            string HostName = string.Empty,
                environment = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        environment = "DevSmtpServer";
                        break;
                    case "STG":
                        environment = "StgSmtpServer";
                        break;
                    case "PRD":
                        environment = "PrdSmtpServer";
                        break;
                    case "PROD":
                        environment = "PrdSmtpServer";
                        break;
                    default:
                        environment = "DevSmtpServer";
                        break;
                }

                HostName = Convert.ToString(ConfigurationManager.AppSettings[environment]);

            }
            catch (Exception ex)
            {

            }
            return HostName;
        }

        private string GetFromMailID()
        {
            string From = string.Empty,
                   ReturnFromID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        From = "DEVExceptionFrom";
                        break;
                    case "STG":
                        From = "STGExceptionFrom";
                        break;
                    case "PRD":
                        From = "PRDExceptionFrom";
                        break;
                    case "DR":
                        From = "DRExceptionFrom";
                        break;
                    default:
                        From = "DEVExceptionFrom";
                        break;
                }
                ReturnFromID = Convert.ToString(ConfigurationManager.AppSettings[From]); 
                //ReturnFromID = ConfigurationManager.AppSettings[From];
            }
            catch (Exception ex)
            {

            }
            return ReturnFromID;
        }

        private string GetToMailID()
        {
            string To = string.Empty,
                  ReturnToID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        To = "DEVExceptionTo";
                        break;
                    case "STG":
                        To = "STGExceptionTo";
                        break;
                    case "PRD":
                        To = "PRDExceptionTo";
                        break;
                    case "DR":
                        To = "DRExceptionTo";
                        break;
                    default:
                        To = "DEVExceptionTo";
                        break;
                }
                ReturnToID = Convert.ToString(ConfigurationManager.AppSettings[To]);
                //ReturnToID = ConfigurationManager.AppSettings[To];
            }
            catch (Exception ex)
            {
               
            }
            return ReturnToID;
        }

        public void Send(Exception exMessage, string strFunationName)
        {
            MailMessage mail = null;
            SmtpClient mailSender = null;
            string mailHostName = null, strFrom = string.Empty, strTo = string.Empty, strCC = string.Empty, strBCC = string.Empty;
            try
            {
                strFrom = GetFromMailID();
                strTo = GetToMailID();
                mail = new MailMessage(strFrom, strTo);
                mail.Subject = "Error: LynxAPDComponentServices";
                mail.Body = GetMailbodyInhtml(exMessage, strFunationName);
                mail.IsBodyHtml = true;
                mailHostName = GetHostName();

                if (!string.IsNullOrEmpty(strCC))
                    mail.CC.Add(strCC);
                if (!string.IsNullOrEmpty(strBCC))
                    mail.Bcc.Add(strBCC);

                mailSender = new SmtpClient(mailHostName);
                mailSender.Send(mail);
            }
            catch (Exception ex)
            {

            }
        }

        public string GetMailbodyInhtml(Exception exMessage, string strFunationName)
        {
            StringBuilder sbMailFormat = null;

            try
            {
                sbMailFormat = new StringBuilder();

                sbMailFormat.Append("<span style='font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append(exMessage);
                sbMailFormat.Append("</span>");

                //Environment 
                sbMailFormat.Append("<table>");
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:15%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Environment");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(PGW.Business.Environment.getEnvironment().ToUpper());
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //DateTime 

                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Date&Time");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(Convert.ToString(DateTime.Now));
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Program Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Program Name");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(strFunationName);
                //Me.Session("WebUserID"))
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Machine Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Machine Name");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(Environment.MachineName);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Error Message
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Error Details");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(exMessage.StackTrace);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");



                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");

                sbMailFormat.Append("</tr>");

                sbMailFormat.Append("</table>");
            }
            catch (Exception ex)
            {
            }

            return sbMailFormat.ToString();
        }
    }
}
