﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.Common
{
    public class ServiceFactory
    {
        public string GetCurrentENV()
        {
            string returnEnvironment = string.Empty;
            try
            {
                returnEnvironment = PGW.Business.Environment.getEnvironment().ToUpper();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnEnvironment;
        }


        //public static lynxdataserviceLogging.APDService getApplogWebserviceReference()
        //{
        //    lynxdataserviceLogging.APDService ApplogService = new lynxdataserviceLogging.APDService();
        //    ApplogService.Url = GetClaimpointUrl();
        //    return ApplogService;
        //}

        private static string GetClaimpointUrl()
        {
            string environment = string.Empty;
            string environmentUrl = string.Empty;
            try
            {
                environment = PGW.Business.Environment.getEnvironment().ToUpper();
                switch (environment)
                {
                    case "DEV":
                        environmentUrl = System.Configuration.ConfigurationManager.AppSettings["DevApplog"];
                        break;
                    case "STG":
                        environmentUrl = System.Configuration.ConfigurationManager.AppSettings["StgApplog"];
                        break;
                    case "PRD":
                        environmentUrl = System.Configuration.ConfigurationManager.AppSettings["PrdApplog"];
                        break;
                    case "DR":
                        environmentUrl = System.Configuration.ConfigurationManager.AppSettings["DrApplog"];
                        break;
                     default:
                        environmentUrl = System.Configuration.ConfigurationManager.AppSettings["DevApplog"];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return environmentUrl;
        }

    }
}
