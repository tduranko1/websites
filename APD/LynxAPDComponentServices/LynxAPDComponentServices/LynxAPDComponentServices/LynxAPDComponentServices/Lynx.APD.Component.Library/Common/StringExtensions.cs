﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.Common
{
    /// <summary>
    /// String Entention functionality
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// This will check for Null or WhitSpaces.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string value)
        {
            if (value == null) return true;
            return string.IsNullOrEmpty(value.Trim());
        }
    }
}
