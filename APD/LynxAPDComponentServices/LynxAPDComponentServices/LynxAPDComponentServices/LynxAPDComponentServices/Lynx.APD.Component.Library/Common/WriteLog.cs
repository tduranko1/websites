﻿using System;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Lynx.APD.Component.Library.Common
{
    /// <summary>
    /// Logging mechanism
    /// </summary>
    public class WriteLog
    {
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;

        /// <summary>
        /// Write log functionality
        /// </summary>
        /// <param name="strEventType"></param>
        /// <param name="strEventStatus"></param>
        /// <param name="strEventDescription"></param>
        /// <param name="strEventDetailedDescritpion"></param>
        /// <param name="strEventXML"></param>
        public void LogEvent(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        {
            string strConnectionString = string.Empty, strProcName = string.Empty, strError = string.Empty;
            long lngSPreturnID = 0;
            try
            {
                if (strEventDescription.Contains("PartnerPosterService.asmx.cs - PushStringToURL()"))
                    PartnerLogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
                else
                {
                    strConnectionString = GetConnectionString("APD");

                    strProcName = "uspAPDEventInsLogEntry";

                    using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                    {
                        using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                        {
                            SqlParameter sqlReturnParm = new SqlParameter("@iRecID", SqlDbType.BigInt);
                            sqlReturnParm.Direction = ParameterDirection.Output;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(sqlReturnParm);
                            command.Parameters.AddWithValue("@vEventType", strEventType);
                            command.Parameters.AddWithValue("@vEventStatus", strEventStatus);
                            command.Parameters.AddWithValue("@vEventDescription", strEventDescription);
                            command.Parameters.AddWithValue("@vEventDetailedDescription", strEventDetailedDescritpion);
                            command.Parameters.AddWithValue("@vEventXML", strEventXML);

                            sqlConnection.Open();
                            command.ExecuteNonQuery();
                            lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);

                            if (lngSPreturnID < 0)
                                throw new Exception("LynxAPDComponent Service Failed: Event logging failed to insert a row (LogEvent)...");
                        }
                    }
                }

                //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
                //DevwsAPDFoundation.LogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
            }
            catch (Exception ex)
            {
                Send(ex, string.Concat("EventDescription: ", strEventDescription, " EventDetailedDescription: ", strEventDetailedDescritpion, " EventXML: ", strEventXML), "Error: LynxAPDComponentServices");
            }
        }

        public void PartnerLogEvent(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        {
            string strConnectionString = string.Empty, strProcName = string.Empty, strError = string.Empty;
            long lngSPreturnID = 0;
            try
            {
                strConnectionString = GetConnectionString("Partner");
                strProcName = "uspPartnerEventInsLogEntry";

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@iRecID", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.Output;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@vEventTransactionID", Guid.NewGuid());
                        command.Parameters.AddWithValue("@vEventType", strEventType);
                        command.Parameters.AddWithValue("@vEventStatus", strEventStatus);
                        command.Parameters.AddWithValue("@vEventDescription", strEventDescription);
                        command.Parameters.AddWithValue("@vEventDetailedDescription", strEventDetailedDescritpion);
                        command.Parameters.AddWithValue("@vEventXML", strEventXML);

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        //lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);

                        if (lngSPreturnID < 0)
                            throw new Exception("LynxAPDComponent Service Failed: Event logging failed to insert a row (LogEvent)...");
                       
                    }
                }

            }
            catch (Exception ex)
            {
                Send(ex, string.Concat("EventDescription: ", strEventDescription, " EventDetailedDescription: ", strEventDetailedDescritpion, " EventXML: ", strEventXML), "Error: LynxAPDComponentServices");
            }
            finally
            {
            }
        }

        /// <summary>
        /// Get DB connection string from Config
        /// </summary>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        public string GetConnectionString(string ConnectionType)
        {
            string returnConnectionString = string.Empty;

            if (ConnectionType == "APD")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                }
            }
            else if (ConnectionType == "Partner")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DR"];
                        break;
                     default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "FNOL")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "Ingres")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DR"];
                        break;
                     default:
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                }
            }

            return returnConnectionString;
        }

        /// <summary>
        /// Get the from address
        /// </summary>
        public string From
        {
            get;
            set;
        }

        /// <summary>
        /// Get the To address
        /// </summary>
        public string To
        {
            get;
            set;
        }

        /// <summary>
        /// Subject of the email
        /// </summary>
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Get the body content of the email
        /// </summary>
        public string Body
        {
            get;
            set;
        }

        /// <summary>
        /// get the CC address
        /// </summary>
        public string CC
        {
            get;
            set;
        }

        /// <summary>
        /// get the BCC address
        /// </summary>
        public string BCC
        {
            get;
            set;
        }

        /// <summary>
        /// Get smtp server details
        /// </summary>
        /// <returns></returns>
        public string GetHostName()
        {
            string HostName = string.Empty,
                environment = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        environment = "DevSmtpServer";
                        break;
                    case "STG":
                        environment = "StgSmtpServer";
                        break;
                    case "PRD":
                        environment = "PrdSmtpServer";
                        break;
                    case "PROD":
                        environment = "PrdSmtpServer";
                        break;
                    case "DR":
                        environment = "DrSmtpServer";
                        break;
                    default:
                        environment = "DevSmtpServer";
                        break;
                }

                HostName = Convert.ToString(ConfigurationManager.AppSettings[environment]);

            }
            catch (Exception ex)
            {

            }
            return HostName;
        }

        /// <summary>
        /// get From email address
        /// </summary>
        /// <returns></returns>
        private string GetFromMailID()
        {
            string From = string.Empty,
                   ReturnFromID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        From = "DEVExceptionFrom";
                        break;
                    case "STG":
                        From = "STGExceptionFrom";
                        break;
                    case "PRD":
                        From = "PRDExceptionFrom";
                        break;
                    case "DR":
                        From = "DRExceptionFrom";
                        break;
                    default:
                        From = "DEVExceptionFrom";
                        break;
                }
                ReturnFromID = Convert.ToString(ConfigurationManager.AppSettings[From]);
                //ReturnFromID = ConfigurationManager.AppSettings[From];
            }
            catch (Exception ex)
            {

            }
            return ReturnFromID;
        }

        /// <summary>
        /// get To email address
        /// </summary>
        /// <returns></returns>
        private string GetToMailID()
        {
            string To = string.Empty,
                  ReturnToID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        To = "DEVExceptionTo";
                        break;
                    case "STG":
                        To = "STGExceptionTo";
                        break;
                    case "PRD":
                        To = "PRDExceptionTo";
                        break;
                    case "DR":
                        To = "DRExceptionTo";
                        break;
                    default:
                        To = "DEVExceptionTo";
                        break;
                }
                ReturnToID = Convert.ToString(ConfigurationManager.AppSettings[To]);
                //ReturnToID = ConfigurationManager.AppSettings[To];
            }
            catch (Exception ex)
            {

            }
            return ReturnToID;
        }

        /// <summary>
        /// Send email process
        /// </summary>
        /// <param name="exMessage"></param>
        /// <param name="strCustomErrMessage"></param>
        /// <param name="strSubject"></param>
        public void Send(Exception exMessage, string strCustomErrMessage, string strSubject)
        {
            MailMessage mail = null;
            SmtpClient mailSender = null;
            string mailHostName = null, strFrom = string.Empty, strTo = string.Empty, strCC = string.Empty, strBCC = string.Empty;
            try
            {
                strFrom = GetFromMailID();
                strTo = GetToMailID();
                mail = new MailMessage(strFrom, strTo);
                if (!string.IsNullOrEmpty(strSubject.Trim()))
                    mail.Subject = strSubject;
                else
                    mail.Subject = "Error: LynxAPDComponentServices";
                mail.Body = GetMailbodyInhtml(exMessage, strCustomErrMessage);
                mail.IsBodyHtml = true;
                mailHostName = GetHostName();

                if (!string.IsNullOrEmpty(strCC))
                    mail.CC.Add(strCC);
                if (!string.IsNullOrEmpty(strBCC))
                    mail.Bcc.Add(strBCC);

                mailSender = new SmtpClient(mailHostName);
                mailSender.Send(mail);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// HTML email Content
        /// </summary>
        /// <param name="exMessage"></param>
        /// <param name="strCustomErrMessage"></param>
        /// <returns></returns>
        public string GetMailbodyInhtml(Exception exMessage, string strCustomErrMessage)
        {
            StringBuilder sbMailFormat = null;

            try
            {
                sbMailFormat = new StringBuilder();

                sbMailFormat.Append("<span style='font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append(exMessage);
                sbMailFormat.Append("</span>");

                //Environment 
                sbMailFormat.Append("<table>");
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:15%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Environment");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(PGW.Business.Environment.getEnvironment().ToUpper());
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //DateTime 

                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Date&Time");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(Convert.ToString(DateTime.Now));
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Program Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Program Name");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(strCustomErrMessage);
                //Me.Session("WebUserID"))
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Machine Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Machine Name");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(Environment.MachineName);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Error Message
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Error Details");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(exMessage.StackTrace);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");



                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");

                sbMailFormat.Append("</tr>");

                sbMailFormat.Append("</table>");
            }
            catch (Exception ex)
            {
            }
            return sbMailFormat.ToString();
        }

        /// <summary>
        /// Get the physical folder path for logging
        /// </summary>
        /// <returns></returns>
        private string EMSLogFileLocation()
        {
            string DesLocation = string.Empty,
                   Location = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        Location = Convert.ToString(ConfigurationManager.AppSettings["LynxAPDComponent_DEV"]);
                        break;
                    case "STG":
                        Location = Convert.ToString(ConfigurationManager.AppSettings["LynxAPDComponent_STG"]);
                        break;
                    case "PRD":
                        Location = Convert.ToString(ConfigurationManager.AppSettings["LynxAPDComponent_PRD"]);
                        break;
                    case "DR":
                        Location = Convert.ToString(ConfigurationManager.AppSettings["LynxAPDComponent_DR"]);
                        break;
                    default:
                        Location = Convert.ToString(ConfigurationManager.AppSettings["LynxAPDComponent_DEV"]);
                        break;
                }

                DesLocation = Location;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return DesLocation;
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <returns></returns>
        private void SendEmail(string strEmailFrom, string strManualFormRequestTo, string strEmailSubject, string strEmailBody, int importance)
        {
            MailMessage mail = null;
            SmtpClient mailSender = null;
            string mailHostName = null;
            try
            {
                mail = new MailMessage(strEmailFrom, strManualFormRequestTo);
                mail.Subject = strEmailSubject;
                mail.Body = strEmailBody;
                mail.IsBodyHtml = true;
                mailHostName = GetHostName();
                switch (importance)
                {
                    case 1:
                        mail.Priority = MailPriority.High;
                        break;
                    case 2:
                        mail.Priority = MailPriority.Normal;
                        break;
                    case 3:
                        mail.Priority = MailPriority.Low;
                        break;
                    default:
                        mail.Priority = MailPriority.Normal;
                        break;
                }

                mailSender = new SmtpClient(mailHostName);
                mailSender.Send(mail);
            }
            catch (Exception ex)
            {
                LogEvent("COM Conversion", "ERROR", "Common.WriteLog.cs - SendEmail", "Error on sending the email.", ex.Message);
            }
        }
    }
}
