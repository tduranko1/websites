﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VBA;
using System.Runtime.InteropServices;
using Scripting;
using MSXML2;

namespace Lynx.APD.Component.Library.Common
{
    /// <summary>
    /// Component SiteUtilities : Class CEvents
    /// 
    /// This class takes care of event handling and logging for applications.
    /// 
    /// Note that CEvents exposes member CXmlCache and CSettings.
    /// </summary>
    class CEvents
    {
        #region Declarations
        private const string APP_NAME = "SiteUtilities.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CEvents.");
        public const long SiteUtilities_FirstError = Constants.vbObjectError + 0x20000;
        #endregion

        #region Enumerators
        //Email importance levels
        public enum enmEmailImportance
        {
            eEmailImportanceNull = -1,
            eEmailImportanceLow = 0,
            eEmailImportanceNormal = 1,
            eEmailImportanceHigh = 2
        }

        //Internal error codes for this class.
        private enum ErrorCodes
        {
            eApplicationNameBlank,
            eMachineNameInvalid,
            eErrTypeInvalid,
            eIdentityNotSet,
            eEmailImportanceInvalid,
            eAssertFailed,
            eLogDirAttributeNotFound,
            eMachineNameNotFound,
            eLogDirFolderNotFound,
            eInstanceNodeNotFound,
            eInstanceNameAttNotFound,
            eInstanceNotFound

        }
        #endregion

        #region Public Members
        public CXmlCache mXmlCache = null;

        public DOMDocument40 mdomSettings;
        #endregion

        #region Private Members


        private string mstrDebugLog;
        private string mstrWebRoot;
        private string mstrExternalMessage;
        private string mstrComponentInstance;
        private string mstrXmlFile;
        //Environment data built up by consecutive calls to Env()
        private Collection marrEnvData;

        //Used to cache log data until this class destructs.
        private string mLogBuffer;

        #endregion


        internal void SetXmlFile(string strXmlFile)
        {
            mstrXmlFile = strXmlFile;

        }

        /// <summary>
        /// Syntax:      object.Initialize( strConfigPath, "Debug" )
        /// Parameters:  strConfigPath - The path to the web application root directory.
        ///              strDebugId - The ini file node to use to determine debug state.
        /// Purpose:     Initializes this object.
        /// Returns:     Nothing
        /// </summary>
        /// <param name="strConfigPath"></param>
        /// <param name="strDebugId"></param>
        /// <param name="blnInitLog"></param>
        public void Initialize(string strConfigPath, [Optional]string strDebugId, [Optional]bool blnInitLog)
        {
            string PROC_NAME = string.Empty;

            string strInstance = string.Empty;
            string strMachine = string.Empty;
            string strIniFile = string.Empty;
            string strErrFile = string.Empty;
            string[] arrPath = null;
            int x;
            FileSystemObject objSystem = null;

            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "Initialize: ");

                mstrWebRoot = strConfigPath;

                //Parse the path to get instance name, if available.
                if (strConfigPath.Substring(strConfigPath.Length - 1, 1) == "\\")
                    strConfigPath = strConfigPath.Substring(0, strConfigPath.Length - 1);

                arrPath = strConfigPath.Split('\\');

                if ((arrPath.GetUpperBound(0)) < 2)
                    throw new Exception(string.Concat("Split strConfigPath", "Passed web root path invalid."));

                //For websites, we will get the webroot and not the path to the config xml.
                //back up one to get the instance, and then go to the config directory.
                if ((strConfigPath.ToUpper()) != ".XML")
                {
                    if ((mstrComponentInstance.Length) > 0)
                        strInstance = mstrComponentInstance;
                    else
                        strInstance = arrPath[(arrPath.GetUpperBound(0)) - 1];

                    //Build path to ini file.
                    for (x = arrPath.GetLowerBound(0); x <= arrPath.GetUpperBound(0) - 1; x++)
                        strIniFile = string.Concat(strIniFile, arrPath[x], "\\");

                    strErrFile = string.Concat(strIniFile, "config\\error.xml");
                    strIniFile = string.Concat(strIniFile, "config\\config.xml");
                }
                else
                {
                    if ((mstrComponentInstance.Length) > 0)
                        strInstance = mstrComponentInstance;
                    else
                        strInstance = "none";

                    strIniFile = strConfigPath;

                    //Build path to possible error file.
                    strErrFile = string.Empty;
                    for (x = arrPath.GetLowerBound(0); x <= arrPath.GetUpperBound(0) - 1; x++)
                        strErrFile = string.Concat(strErrFile, arrPath[x], "\\");

                    strErrFile = string.Concat(strErrFile, "error.xml");
                }

                //Get machine name.
                strMachine = Environment.MachineName;

                if (strMachine.Length == 0)
                    throw new Exception(string.Concat(ErrorCodes.eMachineNameInvalid, "GetWinComputerName", "Could not retrieve the machine name."));

                //Initialize the mSettings DOM document.
                mXmlCache.SetXmlCache(ref mXmlCache);
                SetXmlFile(strIniFile);
                SetIdentity(strMachine, strInstance);

                mXmlCache.SetXmlCache(ref mXmlCache);

                objSystem = new FileSystemObject();

                //Initialize the mMessageStore DOM document from the error.xml file.
                if (strErrFile.Length > 0)
                {
                    if (objSystem.FileExists(strErrFile))
                        mMessageStore.SetXmlFile(strErrFile);
                }

                //If debugging is required then build up a log file name.
                if (mSettings.GetParsedSetting(strDebugId) == "True")
                {
                    //Get log directory from config file.
                    mstrDebugLog = mSettings.GetParsedSetting(string.Concat(strDebugId, "/@logdir"));

                    if (mstrDebugLog.Length == 0)
                        throw new Exception(string.Concat(ErrorCodes.eLogDirAttributeNotFound, "GetSetting(@logdir)", "The 'logdir' attribute is missing from the configuration file for element '" + strDebugId + "'."));

                    if (!objSystem.FolderExists(mstrDebugLog))
                        throw new Exception(string.Concat(ErrorCodes.eLogDirFolderNotFound, "FolderExists", "The logging directory ", mstrDebugLog, " found in the configuration file does not exist on machine ", strMachine));


                    //Per team wishes, we are logging to one file a
                    ///day rather than a separate file per every event.

                    //mstrDebugLog = mstrDebugLog & "\" & strMachine & " - " & mSettings.Application & " - " & strInstance & " - " & CStr(CLng(timeGetTime())) & ".log"

                    if (strInstance != "none")
                        mstrDebugLog = string.Concat(mstrDebugLog, "\\", DateTime.Now.ToString("yyyy-mm-dd")
                            , " - ", mSettings.Application, " - ", strInstance, " - ", strMachine, ".log");
                    else
                        mstrDebugLog = string.Concat(mstrDebugLog, "\\", DateTime.Now.ToString("yyyy-mm-dd")
                            , " - ", mSettings.Application, " ", strMachine, ".log");


                }
                else
                    mstrDebugLog = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      objNode = object.GetParsedNode( "@ConnectionString" )
        /// Parameters:  strXPath = XPath to the DOM node to retrieve.
        /// Purpose:     Retrieves a DOM node from the ini DOM document.
        ///                  Looks first in the proper Instance, then the Machine, then
        ///                  the Environment and then lastly the Application.
        /// Returns:     The node.
        /// 
        /// </summary>
        /// <param name="strXPath"></param>
        /// <returns></returns>
        public object GetParsedNode(string strXPath)
        {
            string PROC_NAME = string.Empty;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetParsedNode: ");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SetIdentity(string strMachine, string strInstance)
        {
            IXMLDOMNodeList objMachineNodeList = null;

            IXMLDOMNode objInstanceNode = null;
            IXMLDOMAttribute objNameAtt = null;
            DOMDocument40 mdomSettings = null;
            string mstrMachine = string.Empty;
            string mstrInstance = string.Empty;
            string mstrApplication = string.Empty;
            string mstrEnvironment = string.Empty;
            try
            {
                CaptureDocument();

                mstrMachine = strMachine;
                mstrInstance = strInstance;
                mstrApplication = GetRawSetting("Root/Application/@name");

                //Check to ensure that passed machine name was found in the config document.

                string strPath;
                strPath = "Root/Application/Environment/Machine[@name='" + strMachine + "']";

                objMachineNodeList = mdomSettings.selectNodes(strPath);

                if (objMachineNodeList.length == 0)
                    throw new Exception(string.Concat(ErrorCodes.eMachineNameNotFound, MODULE_NAME, "SetIdentity()", "Machine name not found in configuration DOM."));

                //If there is more than one machine element matching this machine, then
                //enforce presence of instance elements under that machine.

                if (objMachineNodeList.length > 1)
                {
                    bool blnFoundInstance = false;


                    foreach (IXMLDOMNode objMachineNode in objMachineNodeList)
                    {
                        //Check for Instance node under machine node.
                        objInstanceNode = objMachineNode.selectSingleNode("Instance");

                        if (objInstanceNode == null)
                            throw new Exception(string.Concat(ErrorCodes.eInstanceNodeNotFound, MODULE_NAME, "SetIdentity()", "Instance not found in configuration DOM for Machine '" + strMachine + "'.  There must be an 'Instance' element under all machines that have multiple machine elements in the DOM."));
                        else
                        {
                            //Check for name attribute under instance.
                            objNameAtt = (IXMLDOMAttribute)objInstanceNode.attributes.getNamedItem("name");

                            if (objNameAtt == null)
                                throw new Exception(string.Concat(ErrorCodes.eInstanceNameAttNotFound, MODULE_NAME, "SetIdentity()", "Instance element Machine[@name='" + strMachine + "']/Instance[@name='" + strInstance + "'] did not have a 'name' attribute."));


                            //If the found instance name matches the passed one then flag it.
                            if (objNameAtt.value == strInstance)
                                blnFoundInstance = true;


                        }

                    }

                    //Throw if the passed instance was never found for the machine.
                    if (blnFoundInstance == false)
                        throw new Exception(string.Concat(ErrorCodes.eInstanceNotFound, MODULE_NAME, "SetIdentity()", "Instance element Machine[@name='" + strMachine + "']/Instance[@name='" + strInstance + "'] not found in the configuration DOM."));


                    //Get Environment name
                    mstrEnvironment = GetRawSetting("Root/Application/Environment[Machine[@name='" + strMachine + "'][Instance[@name='" + strInstance + "']]]/@name");
                }
                else
                {
                    //Get Environment name.
                    mstrEnvironment = GetRawSetting("Root/Application/Environment[Machine[@name='" + strMachine + "']]/@name");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mstrEnvironment;
        }

        private void CaptureDocument()
        {
            int mintInitCount = 0;
            try
            {
                if (mdomSettings == null)
                {
                    mXmlCache = new CXmlCache();
                    mdomSettings = (DOMDocument40)mXmlCache.GetDocument(mstrXmlFile);
                    mintInitCount = 0;
                }

                mintInitCount = mintInitCount + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      strSetting = object.GetSetting( "/Root/Elem/@Att" )
        /// Parameters:  strXPath = XPath to the value to retrieve.
        /// Purpose:     Gets a value from the ini DOM document.
        /// Returns:     The value.
        /// </summary>
        /// <param name="strXPath"></param>
        /// <returns></returns>
        public string GetRawSetting(string strXPath)
        {
            string PROC_NAME = string.Empty;
            DOMDocument40 mdomSettings = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetRawSetting: ");

                return Convert.ToString(mdomSettings.selectSingleNode(strXPath));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
