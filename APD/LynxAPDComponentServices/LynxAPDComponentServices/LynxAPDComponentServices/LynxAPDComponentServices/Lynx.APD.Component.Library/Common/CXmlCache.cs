﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VBA;
using BaseUtilitiesLib;

namespace Lynx.APD.Component.Library.Common
{
    /// <summary>
    /// Component SiteUtilities : Class CXmlCache
    /// 
    /// This class is used to cache XML files through DOM documents.
    /// It used to do all the cacheing internally, but this has been moved to
    /// a C++ component for object pooling.
    /// 
    /// </summary>
    class CXmlCache
    {
        #region Declarations
        private const string APP_NAME = "SiteUtilities.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CXmlCache.");
        public const long SiteUtilities_FirstError = Constants.vbObjectError + 0x20000;

        #endregion

        #region Enumerators
        //Internal error codes for this class.
        private enum ErrorCodes
        {
           
        }
        #endregion

      

        //Pooled C++ component used to cache DOM objects.
        private BaseUtilitiesLib.XmlCache mobjXmlCache;


        private void Class_Initialize()
        {
            mobjXmlCache = new BaseUtilitiesLib.XmlCache();
        }


        private void Class_Terminate()
        {
            mobjXmlCache = null;
        }


        internal void SetXmlCache(ref CXmlCache objXmlCache)
        {
            mobjXmlCache = objXmlCache;
        }

        /// <summary>
        ///  Syntax:      object.GetDocument( "C:\xml\myfile.xml" )
        ///  Parameters:  strFileName = Path to the file we want to open.
        ///  Purpose:     This procedure retrieves an XML file as a DOM Document.
        ///                   It caches retrieved files for performance reasons,
        ///                   reloading them when their file date has changed.
        ///  Returns:     The DOMDocument40 for the retrieved file.
        /// </summary>
        public object GetDocument(string strFileName)
        {
            string PROC_NAME = string.Empty;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetDocument: ");

                return mobjXmlCache.GetDom(strFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
