﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lynx.APD.Component.Library.PartnerDataMgr;
using System.Xml;
using Scripting;
using Lynx.APD.Component.Library.Common;

namespace Lynx.APD.Component.Library.RulesEngine
{
    class CRulesEngine
    {
        #region Declarations
        public string MODULE_NAME = "CRulesEngine:";
        MDomUtils objMdomUtilis = null;
        #endregion

        #region Public Functions
        /// <summary>
        ///  Procedure : EngineExecute
        ///  DateTime  : 21/01/2014 05:33PM
        ///  Purpose   : The main Rules Engine execution. This procedure will make a Rules Package
        ///              and transform using the RulesEngine.
        ///  Parameters: strDataXML - The data XML for which we want to execute the rules against
        ///              strRulesDefinitionXML - Rules Definition for the above data.
        ///              strReferenceXML - Optional. Reference data for the data. If the Rules
        ///                definition contains Reference macros then this parameter is required.
        ///                Else the engine will error out.
        /// </summary>
        /// <param name="strDataXML"></param>
        /// <param name="strRuleDefinitionXML"></param>
        /// <param name="strReferenceXML"></param>
        /// <returns></returns>
        public string EngineExecute(string strDataXML, string strRuleDefinitionXML, string strReferenceXML = "")
        {
            string PROC_NAME = string.Empty;
            string strEngineXSLPath = string.Empty;
            string strReturn = string.Empty;

            XmlDocument objRulesPkgXML = null;
            XmlDocument objReturnXML = null;
            FileSystemObject objFSO = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "Execute()");
                objMdomUtilis = new MDomUtils();

                //'validate the input parameters
                //MLAPDPartnerDataMgr.g_objEvents.Assert(strDataXML != "", "The Data XML was blank.");
               // MLAPDPartnerDataMgr.g_objEvents.Assert(strRuleDefinitionXML != "", "The Rules definition XML was blank.");

                //'initialize the XML objects
                objRulesPkgXML = new XmlDocument();
                //objRulesPkgXML.async = false;

                objReturnXML = new XmlDocument();
                // objRulesEngineXSL.async = false;

                objFSO = new FileSystemObject();

                strEngineXSLPath = objFSO.BuildPath("C:\\IntranetSites\\APD\\install\\LAPDPartnerDataMgr", "RulesEngine.xsl");

                //if (MLAPDPartnerDataMgr.g_blnDebugMode)
                //    MLAPDPartnerDataMgr.g_objEvents.Trace("Loading Rules Engine transformation...");

                //'load the rules engine transformation
                //objRulesEngineXSL.LoadXml(strEngineXSLPath);

                //'create the Rules package XML. This will of the following format
                //'<RulesPackage>
                //'   <Data>
                //'       <DataXML>
                //'   </Data>
                //'   <Reference>
                //'       <ReferenceXML/>
                //'   </Reference>
                //'   <RulesDefinitionXML/>
                //'</RulesPackage>

                //if (MLAPDPartnerDataMgr.g_blnDebugMode)
                //    MLAPDPartnerDataMgr.g_objEvents.Trace("Building the Rules Package...");

                objRulesPkgXML.LoadXml("<RulesPackage><Data/><Reference/></RulesPackage>");

                appendXMLFragment(ref objRulesPkgXML, strDataXML, "/RulesPackage/Data");

                //'load the reference data xml
                if (strReferenceXML != "")
                    appendXMLFragment(ref objRulesPkgXML, strReferenceXML, "/RulesPackage/Reference");

                //'load the rule definitions
                appendXMLFragment(ref objRulesPkgXML, strRuleDefinitionXML, "/RulesPackage");

                //'now do the crunching.
                objReturnXML = objMdomUtilis.TransformDomAsDom(ref objRulesPkgXML, strEngineXSLPath);
                //strReturn = objRulesPkgXML.transformNode(objRulesEngineXSL);


                return objReturnXML.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Procedure : appendXMLFragment
        ///  DateTime  : 21/01/2014 05:33PM
        ///  Purpose   : This procedure will add a document fragment to the source XML to a
        ///                particular node
        ///  Parameters: objXMLSource - the source XML object to which the fragment will be added
        ///              strXMLFragment - the XML fragment
        ///              strNode - an existing node in the objXMLSource to which the strXMLFragment
        ///                will be added as a child.
        /// </summary>
        /// <param name="objXMLSource"></param>
        /// <param name="strXMLFragment"></param>
        /// <param name="strNode"></param>
        private void appendXMLFragment(ref XmlDocument objXMLSource, string strXMLFragment, string strNode)
        {
            try
            {
                string PROC_NAME = "appendXMLFragment()";

                XmlDocument objFragmentXML = null;
                XmlNode objAppendNode = null;
                XmlNode objAddXMLNode = null;

                objFragmentXML = new XmlDocument();
                //objFragmentXML.async = false;

                //'load the Data XML
                objFragmentXML.LoadXml(strXMLFragment);
             //   objAddXMLNode.InnerXml = strXMLFragment;

                //'get the node to which the fragment will be appended
                objAppendNode = objXMLSource.SelectSingleNode(strNode);

                if (objAppendNode != null)
                    objAppendNode.AppendChild(objFragmentXML);
                //else   //TODO Need to log here

                    //MLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(PROC_NAME, strNode, " not found."));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
