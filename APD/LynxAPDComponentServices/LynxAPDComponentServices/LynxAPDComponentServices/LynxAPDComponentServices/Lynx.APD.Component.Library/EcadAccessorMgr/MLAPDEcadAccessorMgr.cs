﻿using System;
using SiteUtilities;
using System.Configuration;
using System.Xml;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class MLAPDEcadAccessorMgr
    {

        #region Global Variable Declaration
        public const string APP_NAME = "LAPDEcadAccessorMgr.";
        public const long LAPDEcadAccessorMgr_FirstError = 0x80066000;

        //Global events and data access objects.
        public CEvents g_objEvents = new CEvents();
        public bool g_blnDebugMode = false;

        // This counter used to ensure the globals get intialized
        // and desposed of only once.  This is required because various
        // parts of the ECAD stuff have different entry points.
        private int mintInitCount = 0;

        // Util methods to prevent modification of
        // any error information stored in the Err object.
        #endregion

        #region Enum Declaration
        public enum LAPDEcadAccessorMgr_ErrorCodes : ulong
        {
            eSQLServerErrorMin = 0x80050000,
            eSQLServerErrorMax = 0x8005FFFF,
            eSQLAssignmentIDNotFound = 0x80050072,

            //General error codes applicable to all modules of this component
            eCreateObjectExError = LAPDEcadAccessorMgr_FirstError,

            //Module specific error ranges.
            CWebAssignment_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x80,
            CData_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x100,
            CDocument_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x180
        }

        public enum EDatabase : ulong
        {
            eFNOL_Recordset,
            ePartner_Recordset,
            eAPD_Recordset,
            eAPD_XML
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Create object dynamically
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns></returns>
        public object CreateObjectEx(string strObjectName)
        {
            object mobjCreateObject = null;
            Type createdObj = null;
            try
            {
                createdObj = Type.GetTypeFromProgID(strObjectName);
                if (createdObj == null)
                    throw new Exception(string.Concat(Convert.ToString(LAPDEcadAccessorMgr_ErrorCodes.eCreateObjectExError), ",", "", ",", "CreateObject('", strObjectName, "') returned Nothing."));

                mobjCreateObject = Activator.CreateInstance(createdObj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                createdObj = null;
            }
            return mobjCreateObject;
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = string.Empty;
            try
            {
                strResult = strParam.Substring(0, intLength);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = string.Empty;
            try
            {
                strResult = strParam.Substring(strParam.Length - intLength, intLength);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = string.Empty;
            try
            {
                strResult = strParam.Substring(startIndex, intLength);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = string.Empty;
            try
            {
                strResult = strParam.Substring(intstartIndex);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }

        /// <summary>
        /// Intialize
        /// </summary>
        public void InitializeGlobals()
        {
            string strConfigPath = string.Empty;
            try
            {
                if (mintInitCount == 0)
                {
                    strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                    // Get config debug mode status.
                    g_blnDebugMode = true;
                }
                mintInitCount = mintInitCount + 1;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Terminate the Objects
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                if (g_objEvents != null)
                    Marshal.ReleaseComObject(g_objEvents);
                g_objEvents = null;
                if (mintInitCount <= 1)
                {
                    //  g_objEvents = null;                   
                    mintInitCount = 0;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Getting the cofig data from the Config.xml files
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public string GetConfig(string strSetting)
        {
            string strReturn = string.Empty, strMachineName = string.Empty, strAppPath = string.Empty, strEnvPath = string.Empty,
strMachOnly = string.Empty, strEnvInsPath = string.Empty, strMachPath = string.Empty, strMachInstPath = string.Empty, strInstPath = string.Empty, strConfigPath = string.Empty;
            XmlDocument xmlDocConfig = null;
            bool blnFoundInst = false;
            string mstrInstance = string.Empty;
            XmlNode xmlNode;
            try
            {
                xmlDocConfig = new XmlDocument();
                strAppPath = "Root/Application/";
                strMachineName = Environment.MachineName;
                strEnvPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']]/";
                strEnvInsPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "']"; //Need to Add //[Instance[@name='" & mstrInstance & "']]]/"
                strMachOnly = "//Machine[@name='" + strMachineName + "']"; //[Instance[@name='" & mstrInstance & "']]"
                strMachPath = strAppPath + "Environment/Machine[@name='" + strMachineName + "']/";
                strMachInstPath = strAppPath + "Environment[Machine[@name='" + strMachineName + "'][Instance[@name='" + mstrInstance + "']]]/";
                strInstPath = strMachPath; //"Instance[@name='" & mstrInstance & "']/"

                strConfigPath = Convert.ToString(ConfigurationManager.AppSettings["ConfigPath"]);
                xmlDocConfig.Load(strConfigPath);

                xmlNode = xmlDocConfig.SelectSingleNode(strMachPath + strSetting);
                if (xmlNode == null)
                    blnFoundInst = false;

                if (!blnFoundInst && xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strEnvPath + strSetting);

                if (xmlNode == null)
                    xmlNode = xmlDocConfig.SelectSingleNode(strAppPath + strSetting);

                strReturn = xmlNode.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        /// <summary>
        /// Retruns the DB name, which we need to process the DB.
        /// </summary>
        /// <param name="eDBDetails"></param>
        /// <returns></returns>
        public string ConnectToDB(EDatabase eDBDetails)
        {
            string strConnect = string.Empty,
             strConfig = string.Empty,
             strReturnCS = string.Empty;
            try
            {

                //Get configuration settings for FNOL database connect string.
                //TODO
                switch (eDBDetails)
                {
                    case EDatabase.eFNOL_Recordset:
                        strConfig = @"ClaimPoint/WebAssignment/ConnectionStringStd";
                        strReturnCS = "FNOL";
                        break;
                    case EDatabase.ePartner_Recordset:
                        strConfig = @"PartnerSettings/ConnectionStringStd";
                        strReturnCS = "Partner";
                        break;
                    case EDatabase.eAPD_Recordset:
                        strConfig = @"ConnectionStringStd";
                        strReturnCS = "APD";
                        break;
                    case EDatabase.eAPD_XML:
                        strConfig = @"ConnectionStringXml";
                        strReturnCS = "APD";
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strReturnCS;
        }

        /// <summary>
        /// Adding Zeros for the vehicle number.
        /// </summary>
        /// <param name="lngValue"></param>
        /// <param name="intDigits"></param>
        /// <returns></returns>
        public string ZeroPad(long lngValue, int intDigits)
        {
            string strValue = string.Empty;
            try
            {
                strValue = Convert.ToString(lngValue);
                while (strValue.Length < intDigits)
                    strValue = string.Concat("0", strValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strValue;
        }
        #endregion
    }
}
