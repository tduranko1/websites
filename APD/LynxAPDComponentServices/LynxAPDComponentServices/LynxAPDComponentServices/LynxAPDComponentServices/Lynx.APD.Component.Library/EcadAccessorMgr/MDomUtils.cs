﻿using System;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    /// MDomUtils
    /// </summary>
    public class MDomUtils
    {

        #region Enumerators
        private enum EventCodes : ulong
        {
            eDomDataValidationFailed = 0x80065000 + 0x100,
            eXmlInvalid,
            eXmlMissingAttribute,
            eXmlMissingElement,
            eXmlEmptyNodeSet
        }
        #endregion

        #region Public Functions
        public string Left(string param, int length)
        {
            string strLeft = string.Empty;
            try
            {
                strLeft = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strLeft;
        }

        public string Mid(string param, int startIndex)
        {
            string strMid = string.Empty;
            try
            {
                strMid = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        /// <summary>
        /// Load the xml into XML document
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strXml"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXml(ref XmlDocument objDom, ref string strXml, string strCallerName = null, string strXmlDescription = null)
        {
            bool blnValidXml = false;
            try
            {
                if (!string.IsNullOrEmpty(strXml))
                    objDom.LoadXml(strXml);
                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;
                if (blnValidXml)
                    throw new Exception(string.Concat("MDomUtils.LoadXml -The", strXmlDescription, " XML could not be loaded -", strXml));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// load the xml in to xml document
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strFileName"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXmlFile(ref XmlDocument objDom, ref string strFileName, string strCallerName = null, string strXmlDescription = null)
        {
            bool blnValidXml = false;
            try
            {
                if (!string.IsNullOrEmpty(strFileName))
                    objDom.Load(strFileName);
                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;
                if (blnValidXml)
                    throw new Exception(string.Concat("The ", strXmlDescription, " xml could not be loaded. The XML is -", strFileName));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Use to clean the xml
        /// </summary>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public string EscapeXml(ref string strXml)
        {
            XmlDocument objDom = null;
            string strEscapeXml = string.Empty;
            try
            {
                objDom = new XmlDocument();
                LoadXml(ref objDom, ref strXml, "CleanUpXml");
                strEscapeXml = objDom.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strEscapeXml;
        }

        /// <summary>
        /// Get the Child node
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNode GetChildNode(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNode objChild = null;
            XmlNode xmlnodeGetChildNode = null;
            try
            {
                objChild = objNode.SelectSingleNode(strPath);
                if (objChild == null)
                {
                    if (blnMustExist == true)
                        throw new Exception(string.Concat(EventCodes.eXmlMissingElement.ToString(), "GetChildNode('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child node."));
                }
                else
                    xmlnodeGetChildNode = objChild;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChild = null;
            }
            return xmlnodeGetChildNode;
        }

        /// <summary>
        /// Get the child node text.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public string GetChildNodeText(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNode objChild = null;
            string strGetChildNodeText = string.Empty;
            try
            {
                objChild = GetChildNode(ref objNode, strPath, blnMustExist);
                if (objChild == null)
                    return strGetChildNodeText;
                else
                    strGetChildNodeText = objChild.InnerText;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strGetChildNodeText;
        }

        /// <summary>
        /// Get the child node list
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNodeList GetChildNodeList(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNodeList objChildList = null;
            XmlNodeList xmlnodelistGetChildNodeList = null;
            try
            {
                objChildList = objNode.SelectNodes(strPath);
                if (objChildList == null)
                {
                    if (blnMustExist)
                        throw new Exception(string.Concat(EventCodes.eXmlEmptyNodeSet.ToString(), "GetChildNodeList('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child nodes."));
                    else if (objChildList.ToString().Length == 0 && blnMustExist)
                        throw new Exception(string.Concat(EventCodes.eXmlEmptyNodeSet.ToString(), "GetChildNodeList('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child nodes."));
                    else
                        xmlnodelistGetChildNodeList = objChildList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChildList = null;
            }
            return xmlnodelistGetChildNodeList;
        }

        /// <summary>
        /// Validate the DOM data
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslFileName"></param>
        /// <param name="objEvents"></param>
        /// <param name="blnRaise"></param>
        /// <returns></returns>
        public string ValidateDomData(ref XmlDocument domXML, ref string strXslFileName, ref object objEvents, bool blnRaise = true)
        {
            XmlDocument domXSL = null;
            string strValidateDomData = string.Empty;
            string strResult = string.Empty,
                   strdomXML = string.Empty;
            try
            {
                domXSL = new XmlDocument();
                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslFileName);
                        transformxslt.Transform(xReader, xWriter);
                        strResult = sWriter.ToString();
                    }
                }
                if (Left(strResult, 1) == "|")
                    strResult = Mid(strResult, 2);

                strValidateDomData = strResult;
                if (strResult.Length > 0)
                    throw new Exception(string.Concat(string.Concat(EventCodes.eDomDataValidationFailed, "EcadAccessorMgr.MDomUtils.ValidateDomData", strResult)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXSL = null;
            }
            return strValidateDomData;
        }

        public void AddAttribute(ref XmlElement objElem, string strName, string strValue)
        {
            XmlAttribute objattribute = null;
            try
            {
                objattribute = objElem.OwnerDocument.CreateAttribute(strName);
                objattribute.Value = strValue;
                objElem.SetAttributeNode(objattribute);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objattribute = null;
            }
        }

        public XmlElement AddElement(ref XmlElement objElem, string strName)
        {
            XmlElement objNewElem = null;
            XmlElement xmlelemAddElement = null;
            try
            {
                objNewElem = objElem.OwnerDocument.CreateElement(strName);
                objElem.AppendChild(objNewElem);
                xmlelemAddElement = objNewElem;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNewElem = null;
            }
            return xmlelemAddElement;
        }

        public void RenameAttribute(ref XmlElement objElem, string strPath, string strNewName)
        {
            XmlAttribute objAtt = null;
            XmlNode objNode = null;
            string strValue = string.Empty;
            try
            {
                objNode = (XmlNode)(objElem);
                objAtt = (XmlAttribute)GetChildNode(ref objNode, strPath, false);
                if (objAtt != null)
                {
                    strValue = objAtt.InnerText;
                    objElem.RemoveAttribute(objAtt.Name);
                    AddAttribute(ref objElem, strNewName, strValue);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RenameAllAttributes(ref XmlElement objElem, string strOldName, string strNewName)
        {
            XmlElement objxmlelementtemp = null;
            try
            {
                RenameAttribute(ref objElem, string.Concat("@", strOldName), strNewName);
                if (objElem.ChildNodes.Count > 0)
                {
                    foreach (XmlNode objChild in objElem.ChildNodes)
                    {
                        if (objChild != null)
                        {
                            objxmlelementtemp = (XmlElement)objChild;
                            RenameAllAttributes(ref objxmlelementtemp, strOldName, strNewName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveBlankAttributes(ref XmlElement objElem)
        {
            XmlAttribute objAtt = null;
            int intIdx = 0;
            try
            {
                while (intIdx < objElem.Attributes.Count)
                {
                    objAtt = objElem.Attributes[intIdx];
                    if (objAtt.InnerText == "")
                        objElem.RemoveAttribute(objAtt.Name);
                    else
                        intIdx = intIdx + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveAllChildBlankAttributes(ref XmlElement objElem)
        {
            XmlElement objelemtemp = null;
            try
            {
                RemoveBlankAttributes(ref objElem);
                if (objElem.ChildNodes.Count > 0)
                {
                    foreach (XmlNode objChild in objElem.ChildNodes)
                    {
                        if (objChild != null)
                        {
                            objelemtemp = (XmlElement)objChild;
                            RemoveBlankAttributes(ref objelemtemp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MapAPDReferenceData(ref XmlDocument objDom, string strMapFile, ref object objEvents)
        {
            string strCodeAtt = string.Empty,
            strParent = string.Empty,
            strCode = string.Empty,
            strList = string.Empty,
            strMapAtt = string.Empty,
            strValue = string.Empty;
            XmlDocument objMapDom = null;
            XmlNodeList objMapList = null;
            XmlNodeList objAttParentList = null;
            XmlNodeList objReplaceList = null;
           // XmlNode objxmltmpnode = null;
            XmlNode objTempMapNode = null, objTempAttParentNode = null, objTempReplaceNode = null, objXMLTempNode = null;
            XmlElement objxmlelemtemp = null;

            try
            {
                objMapDom = new XmlDocument();
                LoadXmlFile(ref objMapDom, ref strMapFile, "", "Map");
                objMapList = objMapDom.SelectNodes("//Map");
                foreach (XmlNode objMapNode in objMapList)
                {
                    objReplaceList = objMapNode.SelectNodes("Replace");
                    objTempMapNode = objMapNode;
                    strCodeAtt = GetChildNodeText(ref objTempMapNode, "@Att", true);
                    strParent = GetChildNodeText(ref objTempMapNode, "@Parent", false);
                    strList = GetChildNodeText(ref objTempMapNode, "@List", true);
                    strMapAtt = GetChildNodeText(ref objTempMapNode, "@MapAtt", true);
                    //objxmltmpnode = null;

                    if (strParent == string.Empty)
                        strParent = "Root";

                    objAttParentList = objDom.SelectNodes(string.Concat("//", strParent, "//*[@", strCodeAtt, "]"));
                    foreach (XmlNode objAttParentNode in objAttParentList)
                    {
                        objTempAttParentNode = objAttParentNode;
                        strCode = GetChildNodeText(ref objTempAttParentNode, string.Concat("@", strCodeAtt), true);
                        //objxmltmpnode = null;

                        if (strList == string.Empty)
                            strValue = strCode;
                        else
                        {
                            if (strCode.Length > 0)
                            {
                                objXMLTempNode = (XmlNode)objDom;
                                strValue = GetChildNodeText(ref objXMLTempNode, string.Concat("/Root/Reference", "[@List='", strList, "']", "[@ReferenceID='", strCode, "']", "/@Name"), false);
                            }
                            else
                                strValue = string.Empty;
                        }

                        // objxmltmpnode = null;
                        foreach (XmlNode objReplaceNode in objReplaceList)
                        {
                            objTempReplaceNode = objReplaceNode;
                            strValue = strValue.Replace(GetChildNodeText(ref objTempReplaceNode, "@Find", true), GetChildNodeText(ref objTempReplaceNode, "@Insert", true)).Trim();
                        }
                        //objxmltmpnode = null;
                        //objxmltmpnode = objAttParentNode;
                        if (GetChildNodeText(ref objTempAttParentNode, string.Concat("@", strMapAtt), false).Trim() == string.Empty)
                        {
                            objxmlelemtemp = (XmlElement)objAttParentNode;
                            AddAttribute(ref objxmlelemtemp, strMapAtt, strValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string FormatDomParseError(ref XmlException objErr)
        {
            string strFormatDomParseError = string.Empty;
            try
            {
                strFormatDomParseError = string.Concat("Error code (", objErr.GetHashCode().ToString(), ") was raised during parsing.", Environment.NewLine,
                                         "File location: line = ", objErr.LineNumber.ToString(), " and column = ", objErr.LinePosition.ToString(), ".", Environment.NewLine,
                                         "Bad XML:'", objErr.SourceUri, "'", Environment.NewLine,
                                         "Reason given:", objErr.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strFormatDomParseError;
        }

        public XmlDocument TransformDomAsDom(ref XmlDocument domXML, string strXslPath)
        {
            string PROC_NAME = string.Empty;
            string domResult = string.Empty;
            string strdomXML = string.Empty;
            XmlDocument xmldocTransformDomAsDom = null;
            try
            {
                xmldocTransformDomAsDom = new XmlDocument();
                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslPath);
                        transformxslt.Transform(xReader, xWriter);
                        domResult = sWriter.ToString();
                    }
                    xmldocTransformDomAsDom.LoadXml(domResult);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domResult = null;
            }
            return xmldocTransformDomAsDom;
        }

        public XmlDocument TransformXmlAsDom(ref string strXml, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Empty;
            XmlDocument domXML = null;
            XmlDocument xmldocTransformXmlAsDom = null;
            try
            {
                domXML = new XmlDocument();

                if (string.IsNullOrEmpty(strXml.Trim()))
                    throw new Exception(string.Concat("TransformXmlAsDom() ", "Passed XML string was blank. ", "Xsl Path: ", strXslPath));

                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");
                xmldocTransformXmlAsDom = TransformDomAsDom(ref domXML, strXslPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXML = null;
            }
            return xmldocTransformXmlAsDom;
        }

        public string TransformDomAsXml(ref XmlDocument domXML, string strXslPath, ref object objEvents)
        {
            string strTransformDomAsXml = null;
            try
            {
                strTransformDomAsXml = TransformDomAsDom(ref domXML, strXslPath).OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTransformDomAsXml;
        }

        public string TransformXmlAsXml(ref string strXml, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Empty;
            string strTransformXmlAsXml = null;
            XmlDocument domXML = null;
            try
            {
                domXML = new XmlDocument();

                if (string.IsNullOrEmpty(strXml.Trim()))
                    throw new Exception(string.Concat("TransformXmlAsDom() ", "Passed XML string was blank. ", "Xsl Path: ", strXslPath));

                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");
                strTransformXmlAsXml = TransformDomAsXml(ref domXML, strXslPath, ref objEvents).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXML = null;
            }
            return strTransformXmlAsXml;
        }
        #endregion
    }
}
