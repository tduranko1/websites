﻿using System;
using System.Collections;
using System.Xml;
using ADODB;
using Scripting;
using MSXML2;
using LAPDPartnerDataMgr = Lynx.APD.Component.Library.PartnerDataMgr;
using System.Reflection;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using Common = Lynx.APD.Component.Library.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    /// * Component LAPDEcadAccessor: Class CData
    /// *
    /// * LYNX APD clients (Insurance Companies) require the ability to view
    /// * information about their claims. This information will be accessible from
    /// * the LYNX Services web site. For the purpose of this document, this web
    /// * site will be known as ClaimPoint.
    /// *
    /// * LYNX APD ClaimPoint will be hosted and developed by E-Commerce
    /// * Application Development (ECAD). Initially the site will be read-only and
    /// * simply provide Insurance companies access to claim information.
    /// *
    /// * This component is a simple wrapper for our data access components that
    /// * both limits ECAD's access to our database and makes that access more
    /// * convenient for them to use.
    /// *
    /// </summary>
    /// 
    class CData
    {
        #region Global Declarations
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CData.");
        EcadAccessorMgr.MLAPDEcadAccessorMgr objMLAPDEcadAccessorMgr = null;
        MDomUtils objMDomUtils = null;
        LAPDPartnerDataMgr.MLAPDPartnerDataMgr objLAPDPartnerDataMgr = null;
        MBase64Utils objMBase64Utils = null;
        MEscapeUtilities objMEscapeUtilities = null;
        MRecordsetUtils objMRecordsetUtils = null;
        LAPDPartnerDataMgr.NadaWrapper objNadaWS = null;
        DataAccess.CDataAccessor objDataAccessor = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);
        #endregion

        #region Enumerators
        ///<summary>
        ///Internal error codes for this class.
        ///</summary>
        private enum EventCodes : ulong
        {
            eMissingParamFormat = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CData_CLS_ErrorCodes,
            eInvalidHTTPPostResponse
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Syntax:      objEcad.GetXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the only parameter, in XML format.
        /// 
        /// Purpose:     Returns XML data required by ECAD for our ClaimPoint app.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// Returns:     The XML data, minus any reference and metadata.<returns></returns>
        public string GetXML(string strParamXML)
        {
            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objDataDom = null;
            XmlElement objRootElement = null;
            XmlNodeList objNodeList = null;
            XmlElement objVehNode = null;
            Recordset objRS = null;
            CWebAssignment objWebAsgn = null;
            XmlElement objxmlElementAppend = null;
            XmlNode xmlGlassClaimNode = null;
            Hashtable htParam = null;
            Common.WriteLog objWriteLog = null;
            //Variable declarations
            long lngLynxID;

            string strConnect = string.Empty,
                strEntity = string.Empty,
                strName = string.Empty,
                strDataSource = string.Empty,
                strDataSourceCode = string.Empty,
                strReturn = string.Empty,
                strLogin = string.Empty,
                strXml = string.Empty,
                strDefaultAssignmentUserID = string.Empty,
                strDefaultAssignmentUserMsg = string.Empty,
                strEmailFrom = string.Empty,
                strEmailTo = string.Empty,
                strUserName = string.Empty,
                strServiceChannel = string.Empty,
                strLossState = string.Empty,
                strInsCompany = string.Empty,
                strConncetionString = string.Empty,
            strApplication = string.Empty;
            string strApplicationCD = string.Empty, returnXml = string.Empty;
            bool blnGlassClaim = false;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - GetXML", "GetXml Started", strParamXML);

                objMLAPDEcadAccessorMgr = new EcadAccessorMgr.MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                objDataAccessor = new DataAccess.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Initialize DOM objects.
                objParamDom = new XmlDocument();
                objDataDom = new XmlDocument();

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objParamDom, ref strParamXML, "", "Parameter");

                //Get the root element of the parameter XML.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;

                //* Entity Specific Code
                //* GetWebAssignment must point to the partner DB
                if (strEntity == "GetWebAssignmentXML")
                    strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);
                else if (strEntity == "Status")
                    strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);
                else
                    strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                //* for claim search we need to tweak passed
                //* params to match stored procedure call.
                if (strEntity == "ClaimSearch")
                {
                    if (objRootElement.SelectSingleNode("@InsuredNameLast") != null) // Removing the code for testing -- && objRootElement.SelectSingleNode("@InsuredNameLast").InnerText.Length > 0
                    {
                        objMDomUtils.RenameAttribute(ref objRootElement, "@InsuredNameLast", "InvolvedNameLast");
                        // objMDomUtils.AddAttribute(ref objRootElement, "InvolvedTypeCD", "I");
                    }
                    else if (objRootElement.SelectSingleNode("@ClaimantNameLast") != null && objRootElement.SelectSingleNode("@ClaimantNameLast").InnerText.Length > 0)
                    {
                        objMDomUtils.RenameAttribute(ref objRootElement, "@ClaimantNameLast", "InvolvedNameLast");
                        objMDomUtils.AddAttribute(ref objRootElement, "InvolvedTypeCD", "O");
                    }
                }
                //* End Entity Specific Code
                //***************************************************

                // To search shops from HQ webservice - added GLSD451
                if (strEntity != "HQProviderSearchRq")
                { //Get the procedure name from the config XML.
                    strName = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));
                }

                //* Entity Specific Code
                //* GetWebAssignment expects recordset and bypasses
                //* validation and extraction code.
                if (strEntity == "GetWebAssignmentXML")
                {
                    htParam = new Hashtable();
                    //Convert attributes into parameters.
                    foreach (XmlAttribute objAttributeWebAssignment in objRootElement.Attributes)
                        htParam.Add(string.Concat("@", objAttributeWebAssignment.Name), Uri.UnescapeDataString(objAttributeWebAssignment.Value));

                    //Get the XML from the database.
                    objRS = objDataAccessor.OpenRecordsetSpNpCol(strName, htParam, strApplication);
                    returnXml = objRS.Fields["xml"].ToString();
                    objRS.Close();
                    objRS = null;
                }
                else if (strEntity == "Status")  //Status / version # code
                {
                    htParam = new Hashtable();
                    //Convert attributes into parameters.
                    foreach (XmlAttribute objAttributeStatus in objRootElement.Attributes)
                        htParam.Add(string.Concat("@", objAttributeStatus.Name), Uri.UnescapeDataString(objAttributeStatus.Value));

                    //Get the XML from the database.
                    objRS = objDataAccessor.OpenRecordsetSpNpCol(strName, htParam, strApplication);
                    returnXml = BuildStatusXml(objRS);
                    objRS.Close();
                    objRS = null;
                }
                else if (strEntity == "WebAssignment")  //Web Assignment - this calls the CWebAssignent class to do the work.
                {
                    //Create and call the WebAssignment class to submit the assignment.
                    //This returns the new LYNX ID.
                    objWebAsgn = new CWebAssignment();

                    // HACK: add claim submission to AGC as a separate transaction.  Later this will be included in the ME assignment process.
                    xmlGlassClaimNode = objParamDom.SelectSingleNode("WebAssignment/@SubmitGlassClaim");
                    if (xmlGlassClaimNode != null)
                        blnGlassClaim = Convert.ToBoolean(objParamDom.SelectSingleNode("WebAssignment/@SubmitGlassClaim").InnerText == "true");

                    if (blnGlassClaim)
                    {
                        returnXml = objWebAsgn.DoGlassSubmission(objParamDom);
                        objWebAsgn = null;
                    }
                    else
                    {
                        strDataSource = objParamDom.SelectSingleNode("//DataSource").InnerText;
                        strDataSourceCode = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Name='" + strDataSource + "']/@Code");
                        lngLynxID = objWebAsgn.DoAssignment(objParamDom, strDataSourceCode);

                        if (isExtensiveDebug)
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - GetXML", "New claim submitted.", string.Concat("LynxID = ", lngLynxID));

                        if (lngLynxID != 0)    //Generate the web assignment report and attach to the first vehicle (in this assignment)
                            strReturn = objWebAsgn.SaveWAReport(ref objParamDom, lngLynxID);
                        objWebAsgn = null;

                        //Use the new LYNX ID to get the claim aspect XML.
                        //A zero LynxID indicates that an error happened in DoAssignment
                        //it was logged and supressed internally.
                        if (lngLynxID != 0)
                            strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                        htParam = new Hashtable();
                        htParam.Add("@LynxID", lngLynxID);
                        htParam.Add("@InsuranceCompanyID", objParamDom.SelectSingleNode("//InsuranceCompanyID").InnerText);

                        strXml = objDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetVehicleListSP"), htParam, strApplication);

                        ////if (isDebug)
                        //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - GetXML", "Getting Assignment Information from DB", strXml);
                        // Load returned data into a DOM
                        objMDomUtils.LoadXml(ref objDataDom, ref strXml, "GetXML", "VehicleList");

                        // For FNOLWebAssignment make call to add Claim Owner contact info to xml
                        if (strDataSourceCode == "FNOL")
                            AddClaimOwnerInfoToXML(objDataDom, lngLynxID);

                        // Check for default_assignment user
                        strDefaultAssignmentUserID = objDataDom.SelectSingleNode("/Root/@DefaultAssignmentUserID").InnerText;

                        // APD system has a default assignment user.
                        if (strDefaultAssignmentUserID != string.Empty)
                            //Now check if the default user is part of any vehicles of this claim
                            objNodeList = objDataDom.SelectNodes(string.Concat("//ClaimAspectOwner[@UserID='", strDefaultAssignmentUserID, "']/.."));
                        if (objNodeList != null)
                        {
                            if (objNodeList.Count > 0)
                            {
                                foreach (XmlNode objNodeClaimAspectOwner in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectOwner.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("UserNameFirst"), " ", objVehNode.GetAttribute("UserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectOwner.Attributes["VehicleNumber"].Value, " Owner ",
                                                                           strUserName, Environment.NewLine,
                                                                          "    - Service channel: ", objNodeClaimAspectOwner.Attributes["CurrentServiceChannelName"].Value, Environment.NewLine);
                                }
                            }
                        }

                        objNodeList = objDataDom.SelectNodes("//ClaimAspectOwner[@AnalystUserID='" + strDefaultAssignmentUserID + "']/..");
                        if (objNodeList != null)
                        {
                            if (objNodeList.Count > 0)
                            {
                                foreach (XmlNode objNodeClaimAspectAnalyst in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectAnalyst.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("AnalystUserNameFirst"), " ", objVehNode.GetAttribute("AnalystUserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectAnalyst.Attributes["VehicleNumber"].Value, " Analyst ",
                                                                           strUserName, Environment.NewLine,
                                                                          "    - Service channel: ", objNodeClaimAspectAnalyst.Attributes["CurrentServiceChannelName"].Value, Environment.NewLine);
                                }
                            }
                        }

                        objNodeList = objDataDom.SelectNodes("//ClaimAspectOwner[@SupportUserID='" + strDefaultAssignmentUserID + "']/..");
                        if (objNodeList != null)
                        {
                            if (objNodeList.Count > 0)    //TODO: verify  objNodeList.length changed to objNodeList.Count is Correct?
                            {
                                foreach (XmlNode objNodeClaimAspectSupportUser in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectSupportUser.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("SupportUserNameFirst"), " ", objVehNode.GetAttribute("SupportUserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectSupportUser.Attributes["VehicleNumber"].Value, " Support ",
                                                                           strUserName, Environment.NewLine,
                                                                           "    - Service channel: ", objNodeClaimAspectSupportUser.Attributes["CurrentServiceChannelName"].Value, Environment.NewLine);
                                }
                            }
                        }

                        if (strDefaultAssignmentUserMsg != string.Empty)
                        {
                            strDefaultAssignmentUserMsg = string.Concat("The following entities have been assigned to the default user:", Environment.NewLine,
                                                                   "Environment: ", objMLAPDEcadAccessorMgr.GetConfig("../@name"), Environment.NewLine,
                                                                   "LYNX ID: ", lngLynxID.ToString(), Environment.NewLine,
                                                                   "Insurance Company: ", objDataDom.SelectSingleNode("/Root/@InsuranceCompanyName").InnerText, Environment.NewLine,
                                                                   "Loss State: ", objDataDom.SelectSingleNode("/Root/@LossState").InnerText, Environment.NewLine,
                                                                   strDefaultAssignmentUserMsg);

                            strEmailFrom = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DefaultAssignmentNotification/From");
                            strEmailTo = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DefaultAssignmentNotification/To");

                            // check if the Email From was defined in the config. Else use some default
                            if (strEmailFrom == string.Empty)
                                strEmailFrom = "lynxserv@lynxservices.com";

                            // check if the Email From was defined in the config. Else use some default
                            if (strEmailTo == string.Empty)
                                strEmailTo = "dl-apditgroup@ppg.com";

                            objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strEmailFrom, strEmailTo, "Warning:  Incoming APD Assignment not Assigned!  No available user found!",
                                                  strDefaultAssignmentUserMsg, false);
                        }

                        // If a Web Assignment Document was created, add the path to the returned xml.
                        //TODO-Report Need to uncomment the code, once the report generation is completed.
                        //if (strReturn != string.Empty)
                        if (objDataDom != null)
                        {
                            objxmlElementAppend = objDataDom.DocumentElement;
                            objMDomUtils.AddAttribute(ref objxmlElementAppend, "WebAssignmentDocument", strReturn);
                            returnXml = objDataDom.OuterXml;
                        }
                        else
                        {
                            returnXml = "<Error Description='Error occurred during assignment'/>";
                        }
                    }
                }

            //Extracts an APD configuration value.
                //Example Query: <Config Path='ClaimPoint/HelpdeskEmail' />
                //Example Return: <Config>helpdesk@lynxservices.com</Config>
                else if (strEntity == "NADAValuation")
                    returnXml = GetVehicleValueFromNADA(objParamDom, false);
                else if (strEntity == "Config")
                    returnXml = string.Concat("<Config>", objMLAPDEcadAccessorMgr.GetConfig(objParamDom.SelectSingleNode("//@Path").InnerText), "</Config>");
                else if (strEntity == "ClaimSearch")
                {
                    // Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, ref objRootElement, strName, strEntity, strApplication);

                    if (objRootElement.SelectSingleNode("@InvolvedTypeCD") != null && objRootElement.SelectSingleNode("@InvolvedTypeCD").InnerText == "I")
                    {
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameLast", "InsuredNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameFirst", "InsuredNameFirst");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedLastName", "InsuredNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedFirstName", "InsuredNameFirst");
                    }
                    else if (objRootElement.SelectSingleNode("@InvolvedTypeCD") != null && objRootElement.SelectSingleNode("@InvolvedTypeCD").InnerText == "O")
                    {
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameLast", "ClaimantNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameFirst", "ClaimantNameFirst");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedLastName", "ClaimantNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedFirstName", "ClaimantNameFirst");
                    }
                    //Now return the data XML.
                    returnXml = objDataDom.OuterXml;
                }
                else if (strEntity == "UserSessionDetail")
                {
                    strApplicationCD = objParamDom.SelectSingleNode("//@ApplicationCD").InnerText;

                    // Execute database procedure to retrieve requested data
                    SessionExecuteSPXML(objDataDom, ref objRootElement, strName, strEntity, strApplication);

                    // Now return the data XML.
                    return objDataDom.OuterXml;
                }
                // * End Entity Specific Code
                else if (strEntity == "FNOLService")
                    returnXml = GetFNOLData(objRootElement);
                else if (strEntity == "HQProviderSearchRq")
                {
                    returnXml = GetHQShopData(objParamDom);
                }
                else if (strEntity == "ShopSimilarMatch")
                {
                   /* //Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, ref objRootElement, strName, strEntity, strApplication);
                    //Now return the data XML.
                    int intSimilarMatchCount = objDataDom.SelectNodes("Root/Shop").Count;

                    if (intSimilarMatchCount == 0)
                        GetNewHQShopStandard(ref objDataDom); */

                    GetNewHQShopStandard(ref objDataDom, ref objRootElement, strName, strEntity, strApplication);

                    
                    returnXml = objDataDom.OuterXml;
                }
                else
                {
                    //Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, ref objRootElement, strName, strEntity, strApplication);
                    //Now return the data XML.
                    returnXml = objDataDom.OuterXml;
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - GetXML", "Process Completed.", returnXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMLAPDEcadAccessorMgr.TerminateGlobals();
                objParamDom = null;
                objDataDom = null;
                objRootElement = null;
                objNodeList = null;
                objVehNode = null;
                objRS = null;
                objWebAsgn = null;
                objxmlElementAppend = null;
                xmlGlassClaimNode = null;
                htParam = null;
                strApplication = string.Empty;
                if (objRS != null)
                {
                    objRS.Close();
                    objRS = null;
                }
                objMLAPDEcadAccessorMgr = null;
                objMDomUtils = null;
                objDataAccessor = null;
                objWriteLog = null;
            }
            return returnXml;
        }

        /// <summary>
        /// Syntax:      objEcad.PutXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the data to add, in XML format.
        /// 
        /// Purpose:     Calls APD DB stored procedures to add data to APD.
        ///              This was designed primarly for the Add Comments functionality
        ///              in ClaimPoint, but can be extended for anything.
        /// </summary>
        /// Returns:     1 For Success, 0 For Error.<param name="strParamXML"></param>
        /// <returns></returns>
        public long PutXML(string strParamXML)
        {
            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objResultDom = null;
            XmlElement objRootElement = null;
            XmlNodeList objNodeList = null;
            CWebAssignment objWebAsgn = null;
            Common.WriteLog objWriteLog = null;
            Hashtable htParams = null;
            //Variable declarations
            string strConnect = string.Empty,
                strDataSource = string.Empty,
                strDataSourceCode = string.Empty,
                strEntity = string.Empty,
                strName = string.Empty,
                strResult = string.Empty,
                strParamFormat = string.Empty;
            long returnSuccess = 0;
            string strAppend = string.Empty,
                strApplication = string.Empty;
            bool blnTransaction, blnCarryResults;
            APDDataAccessor.CDataAccessor cDataAccessor = null;
            SqlConnection sqlConnection = null;
            SqlTransaction transaction = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - PutXML", "Process Started.", strParamXML);

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                //objMDomUtils = new MLAPDPartnerDataMgr();
                objMDomUtils = new MDomUtils();
                objMEscapeUtilities = new MEscapeUtilities();
                htParams = new Hashtable();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                cDataAccessor = new APDDataAccessor.CDataAccessor();


                //Initialize DOM objects.
                objParamDom = new XmlDocument();

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objParamDom, ref strParamXML, "", "");

                //Get the root element.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;
                ///objWriteLog.WriteTraceLog(string.Concat("PutXML()", "Entity: ", strEntity));
                //* Entity specific code
                if (strEntity == "SubmitComment")   //Sends a comment email to the APD claims rep.                  
                    returnSuccess = SubmitComment(objParamDom);
                else if (strEntity == "DocumentUpload")//Submits documents to the APD appliation.
                    returnSuccess = DocumentUpload(objParamDom);
                else if (strEntity == "ShopDocumentUpload") //Submits documents to the APD appliation.
                    returnSuccess = ShopDocumentUpload(objParamDom);
                else if (strEntity == "WebAssignment")
                {
                    //Create and call the WebAssignment class to submit the assignment.
                    //This returns the new LYNX ID.                               
                    strDataSource = objParamDom.SelectSingleNode("//DataSource").InnerText;
                    strDataSourceCode = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Name='" + strDataSource + "']/@Code");
                    objWebAsgn = new CWebAssignment();
                    returnSuccess = objWebAsgn.DoAssignment(objParamDom, strDataSourceCode);
                    objWebAsgn = null;
                }
                else if (strEntity == "APDRemittance")
                {
                    // Return success
                    // Process will eventually pull an xsl from the config and convert the param xml into some sort of report
                    // format and email it to accounting.  The accounting recipient list will also reside in the config file.

                    if (ProcessAPDRemittance(objParamDom))
                        returnSuccess = 1;
                    else
                        returnSuccess = 0;
                    //* End Entity Specific Code
                }
                else
                {
                    //Multiple Put Calls as child elements of Root
                    if (strEntity == "Root")
                    {
                        strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                        //Does this series of calls require a transaction?
                        blnTransaction = Convert.ToBoolean(objRootElement.SelectSingleNode("@Transaction").InnerText == "1");
                        //Do the results of one call carry over to the next?
                        blnCarryResults = Convert.ToBoolean(objRootElement.SelectSingleNode("@CarryResults").InnerText == "1");

                        //Create a DOM document for SP call results.
                        objResultDom = new XmlDocument();
                        strResult = string.Empty;

                        cDataAccessor = new DataAccess.CDataAccessor();
                        sqlConnection = new SqlConnection(cDataAccessor.GetConnectionString(strApplication));
                        sqlConnection.Open();

                        transaction = sqlConnection.BeginTransaction();
                        //Loop through all the elements in the passed Root.
                        foreach (XmlElement objElementforchild in objRootElement.ChildNodes)
                        {
                            //The sub-entity name is the element name.
                            strEntity = objElementforchild.Name;
                            if (htParams == null)
                                htParams = new Hashtable();
                            //Get the procedure name from the config XML.
                            strName = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));
                            strParamFormat = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@ParamFormat"));

                            if ((strParamFormat).ToUpper() == "ELEMENTS")
                            {
                                //Loop through all the attributes in the passed element.
                                foreach (XmlElement objParamElementforNode in objElementforchild.ChildNodes)
                                {
                                    strAppend = objParamElementforNode.NodeType.ToString();
                                    htParams.Add(String.Concat("@", objParamElementforNode.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                    strAppend = string.Empty;
                                    //colParams.Add(objMEscapeUtilities.SQLQueryString(objMEscapeUtilities.UnEscapeString(ref strAppend)), string.Concat("@", objParamElementforNode.Name));
                                }
                            }
                            else if ((strParamFormat).ToUpper() == "ATTRIBUTES")
                            {
                                //Convert attributes into parameters.
                                foreach (XmlAttribute objAttributes in objElementforchild.Attributes)
                                {
                                    if (strName == "uspAdmUserUpdDetail" && objAttributes.Name != "ApplicationID")
                                    {
                                        strAppend = objAttributes.Value;
                                        htParams.Add(string.Concat("@", objAttributes.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                        strAppend = string.Empty;
                                        //colParams.Add(objMEscapeUtilities.SQLQueryString(objMEscapeUtilities.UnEscapeString(ref strAppend)), string.Concat("@", objAttributes.Name));
                                    }
                                    else if (strName == "uspAdmUserApplicationInsDetail" && objAttributes.Name != "SysLastUpdatedDate")
                                    {
                                        strAppend = objAttributes.Value;
                                        htParams.Add(string.Concat("@", objAttributes.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                        strAppend = string.Empty;
                                    }
                                    else if (strName != "uspAdmUserUpdDetail" && strName != "uspAdmUserApplicationInsDetail")
                                    {
                                        strAppend = objAttributes.Value;
                                        htParams.Add(string.Concat("@", objAttributes.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                        strAppend = string.Empty;
                                    }
                                }
                            }
                            else
                                throw new Exception(string.Concat(EventCodes.eMissingParamFormat.ToString(), "", "ParamFormat in SPMap Configuration should have been Attributes or Elements"));

                            //Convert last returned XML into parameters.
                            if (blnCarryResults && strResult != string.Empty)
                            {
                                //Select all attributes in returned XML
                                objNodeList = objResultDom.SelectNodes("//@*");

                                //Ignore errors from duplicates
                                //Add a parameter for each attribute.

                                foreach (XmlNode objNodeforlist in objNodeList)  //|| strName == "uspAdmUserSetStatus")
                                {
                                    if (strName == "uspAdmUserApplicationInsDetail" && objNodeforlist.Name != "SysLastUpdatedDate")
                                        htParams.Add(string.Concat("@", objNodeforlist.Name), objNodeforlist.Value);
                                    else if (strName == "uspAdmUserSetStatus" && objNodeforlist.Name != "ApplicationID" && objNodeforlist.Name != "SysLastUpdatedDate")
                                        htParams.Add(string.Concat("@", objNodeforlist.Name), objNodeforlist.Value);
                                    else if (strName != "uspAdmUserApplicationInsDetail" && strName != "uspAdmUserSetStatus")
                                        htParams.Add(string.Concat("@", objNodeforlist.Name), objNodeforlist.Value);
                                }
                                //Reenable error handling.                               
                            }

                            //Put the XML to the database.  Assume results in XML.
                            //strResult = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpColXML(strName, colParams);
                            if (strName == "uspAdmUserUpdDetail" || strName == "uspAdmUserInsDetail")
                                strResult = cDataAccessor.ExecuteSpNpColXMLClaimUserDetailUpdate(ref sqlConnection, ref transaction, strName, htParams, strApplication);
                            else
                                strResult = cDataAccessor.ExecuteSpNpColXML(ref sqlConnection, ref transaction, strName, htParams, strApplication);

                            if (blnCarryResults)
                            {
                                //Load XML Results into the results DOM
                                objMDomUtils.LoadXml(ref objResultDom, ref strResult, "", "");
                            }
                            htParams = null;
                        }
                    }
                    else  //Single Put Call - single element, no Root.
                    {
                        strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);
                        //Get the procedure name from the config XML.
                        strName = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));
                        strParamFormat = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@ParamFormat"));

                        //Create the params collection.
                        //colParams = new Collection();
                        if ((strParamFormat).ToUpper() == "ELEMENTS")
                        {
                            //Loop through all the attributes in the passed element.
                            foreach (XmlElement objElementforChild in objRootElement.ChildNodes)
                            {
                                strAppend = objElementforChild.NodeType.ToString();
                                htParams.Add(string.Concat("@", objElementforChild.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                strAppend = string.Empty;
                            }
                        }
                        else if ((strParamFormat).ToUpper() == "ATTRIBUTES")
                        {
                            //Convert attributes into parameters.
                            foreach (XmlAttribute objAttributefromElement in objRootElement.Attributes)
                            {
                                strAppend = objAttributefromElement.Value;
                                htParams.Add(string.Concat("@", objAttributefromElement.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                                strAppend = string.Empty;
                            }
                        }
                        else
                            throw new Exception(string.Concat(EventCodes.eMissingParamFormat.ToString(), "", "ParamFormat in SPMap Configuration should have been 'Attributes' or 'Elements'"));

                        //Put the XML to the database.                   
                        cDataAccessor.ExecuteSpNpCol(strName, htParams, strApplication);
                        //colParams = null;
                    }
                    //Return success code.
                    returnSuccess = 1;
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - PutXML", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
            finally
            {
                //objMLAPDEcadAccessorMgr.g_objDataAccessor.RollbackTransaction();
                if (returnSuccess == 1 && transaction != null)
                    transaction.Commit();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
                objWriteLog = null;
                htParams = null;
                objParamDom = null;
                objResultDom = null;
                objRootElement = null;
                objNodeList = null;
                objWebAsgn = null;
                cDataAccessor = null;
                sqlConnection = null;
                transaction = null;
                objMLAPDEcadAccessorMgr = null;
            }
            return returnSuccess;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new EcadAccessorMgr.MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Procedure : ExecuteSPXML
        ///  DateTime  : 3/1/2005 08:20
        ///  Author    : Dan Price
        ///  Purpose   : Generic function to execute a stored proc and return xml.
        /// </summary>
        /// <param name="objDataDom"></param>
        /// <param name="objRootElement"></param>
        /// <param name="strProcName"></param>
        /// <param name="strEntity"></param>
        private void ExecuteSPXML(XmlDocument objDataDom, ref XmlElement objRootElement, string strProcName, string strEntity, string strApplicationName)
        {
            XmlNodeList objNodeList = null;
            Hashtable htParams = new Hashtable();
            string strDataXML = string.Empty, strDocumentPath = string.Empty,
                strExtract = string.Empty;
            string strAppend = string.Empty;
            object objAppend = null;
            APDDataAccessor.CDataAccessor cDataAccessor = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - ExecuteSPXML", "Process Started.", "");

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                objMEscapeUtilities = new MEscapeUtilities();
                cDataAccessor = new APDDataAccessor.CDataAccessor();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //Create a collection of all the attributes of the root element.               
                // colParams = new CollectionClass();
                //Convert attributes into parameters.
                foreach (XmlAttribute objAttributeParam in objRootElement.Attributes)
                {
                    if (strProcName == "uspECADClaimVehicleGetDetailXML" && objAttributeParam.Name != "LynxID")
                    {
                        strAppend = objAttributeParam.Value;
                        htParams.Add(string.Concat("@", objAttributeParam.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                    }
                    else if (strProcName == "uspCPGetAllVehicleListXML" && objAttributeParam.Name != "InsuranceCompanyID")
                    {
                        strAppend = objAttributeParam.Value;
                        htParams.Add(string.Concat("@", objAttributeParam.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                    }
                    else if (strProcName == "uspReportsGetListXML" && objAttributeParam.Name != "InsuranceCompanyID")
                    {
                        strAppend = objAttributeParam.Value;
                        htParams.Add(string.Concat("@", objAttributeParam.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                    }
                    else if (strProcName != "uspECADClaimVehicleGetDetailXML" && strProcName != "uspCPGetAllVehicleListXML" && strProcName != "uspReportsGetListXML")
                    {
                        strAppend = objAttributeParam.Value;
                        htParams.Add(string.Concat("@", objAttributeParam.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                    }
                }
                //Get the XML from the database.           
                //TODO - Changing the ExecuteSpNpColXML
                strDataXML = cDataAccessor.ExecuteSpNpColXML(strProcName, htParams, strApplicationName);

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objDataDom, ref strDataXML, "", "data");

                //Get the root element of the data XML.
                objRootElement = objDataDom.DocumentElement;

                //Uses the map file to convert codes to reference values.
                objAppend = objMLAPDEcadAccessorMgr.g_objEvents;
                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);

                objMDomUtils.MapAPDReferenceData(ref objDataDom, string.Concat(strDocumentPath, "\\ConversionMap.xml"), ref objAppend);

                //Do we want to remove some reference data?
                strExtract = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@Extract"));

                if (strExtract != string.Empty)//Search for and remove all metadata and reference elements.
                {
                    objNodeList = objRootElement.SelectNodes(strExtract);

                    foreach (XmlNode objNodeChildRemove in objNodeList)
                        objRootElement.RemoveChild(objNodeChildRemove);
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - ExecuteSPXML", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //DevwsAPDFoundation.Dispose();
                objNodeList = null;
                htParams = null;
                objWriteLog = null;
            }
        }

        /// <summary>
        ///  Procedure : SessionExecuteSPXML
        ///  DateTime  : 3/1/2005 08:20
        ///  Author    : Dan Price
        ///  Purpose   : Generic function to execute a stored proc for session.
        /// </summary>
        /// <param name="objDataDom"></param>
        /// <param name="objRootElement"></param>
        /// <param name="strProcName"></param>
        /// <param name="strEntity"></param>
        private void SessionExecuteSPXML(XmlDocument objDataDom, ref XmlElement objRootElement, string strProcName, string strEntity, string strApplicationName)
        {
            XmlNodeList objNodeList = null;
            Hashtable htParams = new Hashtable();
            string strDataXML = string.Empty, strDocumentPath = string.Empty,
                strExtract = string.Empty;
            string strAppend = string.Empty;
            object objAppend = null;
            APDDataAccessor.CDataAccessor cDataAccessor = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - SessionExecuteSPXML", "Process Started.", "");

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                objMEscapeUtilities = new MEscapeUtilities();
                cDataAccessor = new APDDataAccessor.CDataAccessor();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //Create a collection of all the attributes of the root element.               
                // colParams = new CollectionClass();
                //Convert attributes into parameters.
                foreach (XmlAttribute objAttributeParam in objRootElement.Attributes)
                {
                    strAppend = objAttributeParam.Value;
                    htParams.Add(string.Concat("@", objAttributeParam.Name), objMEscapeUtilities.UnEscapeString(ref strAppend));
                }
                //Get the XML from the database.           
                //TODO - Changing the ExecuteSpNpColXML
                strDataXML = cDataAccessor.ExecuteSpNpColXML(strProcName, htParams, strApplicationName);

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objDataDom, ref strDataXML, "", "data");

                //Get the root element of the data XML.
                objRootElement = objDataDom.DocumentElement;

                //Uses the map file to convert codes to reference values.
                objAppend = objMLAPDEcadAccessorMgr.g_objEvents;
                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);

                objMDomUtils.MapAPDReferenceData(ref objDataDom, string.Concat(strDocumentPath, "\\ConversionMap.xml"), ref objAppend);

                //Do we want to remove some reference data?
                strExtract = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@Extract"));

                if (strExtract != string.Empty)//Search for and remove all metadata and reference elements.
                {
                    objNodeList = objRootElement.SelectNodes(strExtract);

                    foreach (XmlNode objNodeChildRemove in objNodeList)
                        objRootElement.RemoveChild(objNodeChildRemove);
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - SessionExecuteSPXML", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //DevwsAPDFoundation.Dispose();
                objNodeList = null;
                htParams = null;
                objWriteLog = null;
            }
        }

        /// <summary>
        /// Build status XML
        /// </summary>
        /// <param name="objRS"></param>
        /// <returns></returns>
        private string BuildStatusXml(ADODB.Recordset objRS)
        {
            string strXml = string.Empty,
                strConnect = string.Empty;
            int intIdx = 0,
                intEnd = 0;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - BuildStatusXml", "Process Started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //objMLAPDEcadAccessorMgr = new MLAPDPartnerDataMgr();
                strXml = "<Root>";

                //APD Configuration
                strXml = string.Concat(strXml, "<Config application=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Application, "\"");
                strXml = string.Concat(strXml, "environment=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Environment, "\" ");
                strXml = string.Concat(strXml, "version=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.GetRawSetting("/Root/Application/@version"), "\" ");
                strXml = string.Concat(strXml, "instance=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Instance, "\" />");

                //Database
                strXml = string.Concat(strXml, "<Database version=\"", objRS.Fields[0].Value.ToString(), "\" ");
                strConnect = objMLAPDEcadAccessorMgr.GetConfig("/ConnectionStringXml");

                intIdx = strConnect.IndexOf("data source=") + ("data source=").Length;
                intEnd = strConnect.Substring(intIdx).IndexOf(";");
                strXml = string.Concat(strXml, "server=\"", strConnect.Substring(intIdx, intEnd - 1), "\" ");

                intIdx = strConnect.IndexOf("initial catalog=") + ("initial catalog=").Length;
                intEnd = strConnect.Substring(intIdx).IndexOf(";");
                strXml = string.Concat(strXml, "server=\"", strConnect.Substring(intIdx, intEnd - 1), "\" />");

                //Proxy Server
                strXml = string.Concat(strXml, "<Proxy machine=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Machine, "\" ");
                strXml = string.Concat(strXml, "version=\"", Assembly.GetExecutingAssembly().GetName().Version.Major.ToString(), ".", Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString(), ".", Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString(), "\" />");
                strXml = string.Concat(strXml, "</Root>");

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - BuildStatusXml", "Process Completed.", strXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return strXml;
        }

        /// <summary>
        /// Sends a comment email to the APD claims rep.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private long SubmitComment(XmlDocument objParamDom)
        {
            //Objects that need cleanup           
            XmlElement objRootElement = null;
            XmlDocument objResultDom = null;
            Hashtable htParams = null;
            //Primitives that don't.
            string strSubject = string.Empty,
                strFrom = string.Empty,
                strTo = string.Empty,
                strBody = string.Empty,
                strName = string.Empty,
                strEntity = string.Empty,
               strResult = string.Empty,
               strSentToClaimOwner = string.Empty,
               strApplication = string.Empty;
            long lngSuccess = 0;
            APDDataAccessor.CDataAccessor cDataAccessor = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - SubmitComment", "Process Started.", objParamDom.OuterXml);
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMEscapeUtilities = new MEscapeUtilities();
                cDataAccessor = new APDDataAccessor.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //Get the root element.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;

                //Create the params collection.               
                htParams = new Hashtable();
                //Buid the collection of all the attributes of the root element.
                foreach (XmlAttribute objAttributefromRootelem in objRootElement.Attributes)
                    htParams.Add(Convert.ToString(string.Concat("@", objAttributefromRootelem.Name)), objAttributefromRootelem.Value);

                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                //Get the procedure name from the config XML.
                strName = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));

                //Get the Lynx Representative info for this claim from the database.
                // strResult = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpColXML(strName, colParams);
                strResult = cDataAccessor.ExecuteSpNpColXML(strName, htParams, strApplication);

                //Get the from and to addresses.
                strFrom = objParamDom.SelectSingleNode("//@UserEmail").InnerText;

                if ("yes" == objParamDom.SelectSingleNode("//@SendToClaimOwner").InnerText)
                    strTo = objResultDom.SelectSingleNode("//@EmailAddress").InnerText;
                else
                    strTo = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/ClaimCommentMailbox/NotificationEmail");

                //Build a subject.
                strSubject = string.Concat("ClaimPoint Comment Submission: Lynx ID = ", objParamDom.SelectSingleNode("//@LynxID").InnerText);

                //Build the body.
                strBody = string.Concat("From: ", objParamDom.SelectSingleNode("//@UserName").InnerText, Environment.NewLine
                     , Environment.NewLine, objParamDom.SelectSingleNode("//@Comment").InnerText);

                //Send an email to the Lynx Rep.
                objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strFrom, strTo, strSubject, strBody);

                //Return success code.
                lngSuccess = 1;

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - SubmitComment", "Process Completed.", objParamDom.OuterXml);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //DevwsAPDFoundation.Dispose();
                objRootElement = null;
                objResultDom = null;
                htParams = null;
                objWriteLog = null;
            }
            return lngSuccess;
        }

        /// <summary>
        /// Submits documents to the APD appliation.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private long DocumentUpload(XmlDocument objParamDom)
        {
            //Objects that need cleanup
            Recordset objRS = null;
            FileSystemObject objFile = null;
            XmlElement objElementToAppend = null;
            XmlDocument xmlClaimDetail = null;
            Hashtable htParams = null;
            Image objBmpImage = null;
            Bitmap objBitMap = null;
            LAPDPartnerDataMgr.MBase64Utils objPartnerMBaseUtils = null;
            Common.WriteLog objWriteLog = null;
            //Primitives that don't.
            string strVehNum = string.Empty,
                strDocSrc = string.Empty,
                strPertainsTo = string.Empty,
                strLynxID = string.Empty,
                strPartnerTransID = string.Empty,
                strNow = string.Empty,
                strFinalName = string.Empty,
                strNotifyEvent = string.Empty,
                strDirectionalCD = string.Empty,
                strSendToCarrierStatusCD = string.Empty,
                strWarrantyFlag = string.Empty, strBmpName = string.Empty, strFileName = string.Empty,
                strExtension = string.Empty,
                strUserID = string.Empty,
                strFilePath = string.Empty,
                strFinalFileName = string.Empty,
                strBMPFile = string.Empty,
                strJPGFile = string.Empty,
                strApplication = string.Empty,
                strReturnXml = string.Empty,
                strDocumentType = string.Empty,
                InsCompanyID = string.Empty,
                SourceApplicationID = string.Empty,
                DocumentBundleName = string.Empty,
                ClientClaimNumber = string.Empty,
                SourceApplicationPassThruData = string.Empty,
                EMSfileName = string.Empty,
                EMSfilePath = string.Empty;

            long lngDocumentID = 0;
            int intSuffix = 0, intIdx = 0, intRetryTimeOut, intRetryCount;
            byte[] arrBuffer;
            object objTempArrayObject = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - DocumentUpload", "Process Started.", objParamDom.OuterXml);
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objLAPDPartnerDataMgr = new LAPDPartnerDataMgr.MLAPDPartnerDataMgr();
                objMBase64Utils = new MBase64Utils();
                objMEscapeUtilities = new MEscapeUtilities();
                objMRecordsetUtils = new MRecordsetUtils();
                objMDomUtils = new MDomUtils();
                objDataAccessor = new DataAccess.CDataAccessor();
                objFile = new FileSystemObject();
                objPartnerMBaseUtils = new LAPDPartnerDataMgr.MBase64Utils();
                xmlClaimDetail = new XmlDocument();
                intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
                intRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //Build a date time stamp string
                strNow = string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), " ", DateTime.Now.ToString("hh:mm:ss"));
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - DocumentUpload", "hit 1", objParamDom.OuterXml);
                //Extract the Lynx ID.
                strLynxID = objParamDom.SelectSingleNode("//LynxID").InnerText;

                //Extract a vehicle number from the pertains to.
                strPertainsTo = objParamDom.SelectSingleNode("//PertainsTo").InnerText;
                if (!string.IsNullOrEmpty(strPertainsTo.Trim()) && strPertainsTo.Length > 0)
                {
                    if (strPertainsTo.Substring(0, 3).ToUpper() == "VEH")
                        strVehNum = strPertainsTo.Substring(3);
                }


                //Extract the DocumentType 
                if (objParamDom.SelectSingleNode("//DocumentType") != null)
                {
                    strDocumentType = objParamDom.SelectSingleNode("//DocumentType").InnerText;
                    if (strDocumentType.Contains("Closing Documents"))
                    {
                        DocumentBundleName = strDocumentType.Replace("Closing Documents - ", "");
                        strDocumentType = "Closing Documents";
                    }

                }

                //Build the document source string from what was passed OR hard coded.
                strDocSrc = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DocumentSource", false);

                if (strDocSrc == string.Empty)
                    strDocSrc = "ClaimPoint";

                //* Store the document upload in the partner database.
                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - DocumentUpload", "hit 2" + strApplication, objParamDom.OuterXml);
                htParams = new Hashtable();
                htParams.Add("@LynxID", strLynxID);
                htParams.Add("@AssignmentID", "0");
                htParams.Add("@VehicleNumber", strVehNum);
                htParams.Add("@SequenceNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText);
                htParams.Add("@TransactionDate", strNow);
                htParams.Add("@TransactionSource", "CLMPT");
                htParams.Add("@TransactionType", "DocumentUpload");
                htParams.Add("@PartnerXML", objParamDom.OuterXml);

                bool isPartnerProcessed = false;
                int isPartnerRetry = 0;
                while (isPartnerRetry < 10 && !isPartnerProcessed)
                {
                    try
                    {
                        objRS = objDataAccessor.OpenRecordsetNamedParams(objMLAPDEcadAccessorMgr.GetConfig("PartnerSettings/PartnerLoadSP"), htParams, strApplication);
                        isPartnerProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        switch (isPartnerRetry)
                        {
                            case 0:
                                objWriteLog.Send(ex, string.Concat("WARNING: DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "");
                                break;
                            case 10:
                                //5th attempt failure, notify as Error
                                objWriteLog.Send(ex, string.Concat("ERROR: DocumentUpload - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                objWriteLog.LogEvent("COMConversion", "ERROR", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("SERIOUS - DocumentUpload - 10th Attempt failed. Reason: ", ex.Message), "");
                                throw new Exception("An unexpected error has caused a delay in upload the document.");
                                break;
                            default:
                                objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload -  ", isPartnerRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                break;
                        }
                        isPartnerProcessed = false;
                    }
                    if (isPartnerProcessed)
                        break;
                    System.Threading.Thread.Sleep(intRetryTimeOut);
                    isPartnerRetry++;
                }

                strPartnerTransID = Convert.ToString(objRS.Fields["PartnerTransID"].Value);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMRecordsetUtils.DebugRS(objRS);

                objRS.Close();
                objRS = null;

                //Insert the document into the LYXN Select application.
                //Extract the user id, default to system if necessary.
                strUserID = objParamDom.SelectSingleNode("//UserID").InnerText;
                if (strUserID == string.Empty)
                    strUserID = objMLAPDEcadAccessorMgr.GetConfig("WorkFlow/SysUserID");

                //Build the directory structure from the last four digits of the LynxID.
                for (intIdx = 0; intIdx < 4; intIdx++)
                    strFileName = string.Concat(strFileName, "\\\\", strLynxID.Substring(strLynxID.Length - 4, 4).Substring(intIdx, 1));

                strFileName = string.Concat(strFileName, "\\\\");
                strExtension = objParamDom.SelectSingleNode("//FileExtension").InnerText;

                // Appended to end of filename to ensure uniqueness.
                intSuffix = 1;

                //Create a unique file name for the document.
                strFileName = string.Concat(strFileName, "DU", DateTime.Now.ToString("yyyyMMddhhmmss"), "LYNXID", strLynxID
                     , "V", strVehNum, "Doc");

                strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                strFilePath = string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory"), strFinalFileName).Replace("\\\\", "\\");

                //Create binary data from the Base64 encoded info.
                objElementToAppend = (XmlElement)objLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "//FileData", true);
                //arrBuffer = (byte[])objMBase64Utils.ReadBinaryDataFromDomElement(ref objElementToAppend);

                objTempArrayObject = objMBase64Utils.ReadBinaryDataFromDomElement(ref objElementToAppend);
                arrBuffer = Convert.FromBase64String(Convert.ToString(objTempArrayObject));

                if (strExtension.ToLower() == "bmp")
                {
                    strBmpName = objFile.GetBaseName(objFile.GetTempName());
                    strBMPFile = objFile.BuildPath(Convert.ToString(objFile.GetSpecialFolder(SpecialFolderConst.TemporaryFolder).Path), string.Concat(strBmpName, ".bmp"));

                    //Write the binary data to a file in the document structure.
                    objMBase64Utils.WriteBinaryDataToFile(strBMPFile, ref arrBuffer);

                    ///Check if .bmp file is exist in the temp location path. If the file exist with the same name, then it moves to the else part.
                    if (objFile.FileExists(strBMPFile))
                    {
                        objBmpImage = System.Drawing.Image.FromFile(strBMPFile);
                        strFinalFileName = strFinalFileName.Replace("bmp", "jpg");
                        if (objFile.FileExists(strFinalFileName))
                        {
                            intSuffix = intSuffix + 1;
                            strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                            strFilePath = string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory"), strFinalFileName).Replace("\\\\", "\\");
                            strFilePath = strFilePath.Replace("bmp", "jpg");
                            objBitMap = new System.Drawing.Bitmap(objBmpImage);
                            objBitMap.Save(strFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        else
                        {
                            strFilePath = strFilePath.Replace("bmp", "jpg");
                            objBitMap = new System.Drawing.Bitmap(objBmpImage);
                            objBitMap.Save(strFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                    else
                    {
                        //JPG conversion failed. Save it in bmp format
                        // If the file name construced above already exists, increment the suffix until a unique file name is found.
                        while (objFile.FileExists(strFilePath))
                        {
                            // objWritelog.WriteTraceLog(string.Concat("check if file exist()", "strFileName", strFilePath));
                            intSuffix = intSuffix + 1;
                            strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                            strFilePath = (string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                        }
                        objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);
                    }
                }
                else
                {
                    // If the file name construced above already exists, increment the suffix until a unique file name is found.
                    while (objFile.FileExists(strFilePath))
                    {
                        intSuffix = intSuffix + 1;
                        strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                        strFilePath = (string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                    }
                    objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);

                    //Getting Assignment Type name
                    htParams = new Hashtable();
                    htParams.Add("@LynxID", strLynxID);
                    htParams.Add("@VehicleNumber", strVehNum);
                    strReturnXml = objDataAccessor.ExecuteSpNamedParamsXML("uspGetEMSCliamDetailsWSXML", htParams, "APD");

                    if (!string.IsNullOrEmpty(strReturnXml.Trim()))
                    {
                        xmlClaimDetail.LoadXml(strReturnXml);
                        InsCompanyID = xmlClaimDetail.SelectSingleNode("/Root/@InsuranceCompanyID").Value;
                        SourceApplicationID = xmlClaimDetail.SelectSingleNode("/Root/@SourceApplicationID").Value;

                        if (InsCompanyID.Trim() == "194" && SourceApplicationID.Trim() == "7" && strDocumentType.Trim() == "Closing Documents" && strExtension.ToLower() == "pdf")
                        {
                            ClientClaimNumber = xmlClaimDetail.SelectSingleNode("/Root/@ClientClaimNumber").Value;
                            SourceApplicationPassThruData = xmlClaimDetail.SelectSingleNode("/Root/@SourceApplicationPassThruData").Value;
                            EMSfileName = string.Concat("\\\\", ClientClaimNumber, " ", DocumentBundleName, " Closing Documents - ", strLynxID, "-", SourceApplicationPassThruData, "-", strVehNum, ".PDF");
                            EMSfilePath = string.Concat(GetEMSPath(), EMSfileName).Replace("\\\\", "\\");
                            //string EMSfilePath = string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/RootEMSDirectory"), EMSfileName).Replace("\\\\", "\\");

                            objMBase64Utils.WriteBinaryDataToFile(EMSfilePath, ref arrBuffer);
                        }
                    }
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("Document created in the following path =", strFilePath), "");

                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                //HACK ALERT - workaround for pertains to.
                if (objParamDom.SelectSingleNode("//NotifyEvent") != null)
                    strNotifyEvent = objParamDom.SelectSingleNode("//NotifyEvent").InnerText;

                if (strNotifyEvent != "0")
                    strNotifyEvent = "1";

                if (objParamDom.SelectSingleNode("//DirectionalCD") != null)
                    strDirectionalCD = objParamDom.SelectSingleNode("//DirectionalCD").InnerText;

                if (strDirectionalCD != "O")
                    strDirectionalCD = "I";

                if (objParamDom.SelectSingleNode("//SendToCarrierStatusCD") != null)
                    strSendToCarrierStatusCD = objParamDom.SelectSingleNode("//SendToCarrierStatusCD").InnerText;
                if (strSendToCarrierStatusCD != "S")
                    strSendToCarrierStatusCD = "NS";

                //Extract the Warranty Flag
                if (objParamDom.SelectSingleNode("//WarrantyFlag") != null)
                    strWarrantyFlag = objParamDom.SelectSingleNode("//WarrantyFlag").InnerText;

                try
                {
                    htParams = new Hashtable();
                    htParams.Add("@UserID", strUserID);
                    htParams.Add("@PertainsTo", strPertainsTo);
                    htParams.Add("@LynxID", strLynxID);
                    htParams.Add("@NotifyEvent", strNotifyEvent);
                    htParams.Add("@CreatedDate", strNow);
                    htParams.Add("@ImageDate", strNow);
                    htParams.Add("@ImageLocation", strFinalFileName);
                    htParams.Add("@DocumentSource", strDocSrc);
                    htParams.Add("@DirectionalCD", strDirectionalCD);
                    htParams.Add("@DocumentType", strDocumentType);
                    htParams.Add("@SupplementSeqNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText);
                    htParams.Add("@ExternalReference", strPartnerTransID);
                    htParams.Add("@ImageType", objParamDom.SelectSingleNode("//FileExtension").InnerText);
                    htParams.Add("@SendToCarrierStatusCD", strSendToCarrierStatusCD);
                    htParams.Add("@WarrantyFlag", strWarrantyFlag);

                    bool isProcessed = false;
                    int iRetry = 0;
                    while (iRetry < 10 && !isProcessed)
                    {
                        try
                        {
                            lngDocumentID = objDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("PartnerSettings/InsertSP"), htParams, strApplication);
                            isProcessed = true;
                        }
                        catch (Exception ex)
                        {
                            switch (iRetry)
                            {
                                case 0:
                                    //objWriteLog.Send(ex, string.Concat("WARNING: DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                    objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "");
                                    break;
                                case 10:
                                    //5th attempt failure, notify as Error
                                    objWriteLog.Send(ex, string.Concat("ERROR: DocumentUpload - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                    objWriteLog.LogEvent("COMConversion", "ERROR", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("SERIOUS - DocumentUpload - 10th Attempt failed. Reason: ", ex.Message), "");
                                    throw new Exception("An unexpected error has caused a delay in upload the document.");
                                    break;
                                default:
                                    objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                    break;
                            }
                            isProcessed = false;
                        }
                        if (isProcessed)
                            break;
                        System.Threading.Thread.Sleep(intRetryTimeOut);
                        iRetry++;
                    }
                }
                catch (Exception ex)
                {
                    if (strPertainsTo.ToUpper() == "VEH1")                                             //Todo Verify ..error number
                    {
                        //Call the document insert procedure again with the new pertains to.
                        htParams = new Hashtable();
                        htParams.Add("@UserID", strUserID);
                        htParams.Add("@PertainsTo", "VEH2");
                        htParams.Add("@LynxID", strLynxID);
                        htParams.Add("@NotifyEvent", 1);
                        htParams.Add("@CreatedDate", strNow);
                        htParams.Add("@ImageDate", strNow);
                        htParams.Add("@ImageLocation", strFileName);
                        htParams.Add("@DocumentSource", strDocSrc);
                        htParams.Add("@DocumentType", strDocumentType);
                        htParams.Add("@SupplementSeqNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText);
                        htParams.Add("@ExternalReference", strPartnerTransID);
                        htParams.Add("@ImageType", objParamDom.SelectSingleNode("//FileExtension").InnerText);
                        htParams.Add("@ClaimAspectServiceChannelID", objParamDom.SelectSingleNode("//ClaimAspectServiceChannelID").InnerText);

                        bool isProcessed = false;
                        int iRetry = 0;
                        while (iRetry < 10 && !isProcessed)
                        {
                            try
                            {
                                lngDocumentID = objDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("PartnerSettings/InsertSP"), htParams, strApplication);
                                isProcessed = true;
                            }
                            catch (Exception excep)
                            {
                                switch (iRetry)
                                {
                                    case 0:
                                        //objWriteLog.Send(excep, string.Concat("WARNING: DocumentUpload - 1st Attempt failed.  Reason: ", excep.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload - 1st Attempt failed.  Reason: ", excep.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(excep, string.Concat("ERROR: DocumentUpload - 10th Attempt failed.  Reason: ", excep.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("SERIOUS - DocumentUpload - 10th Attempt failed. Reason: ", excep.Message), "");
                                        throw new Exception("An unexpected error has caused a delay in upload the document.");
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CData.cs - DocumentUpload", string.Concat("DocumentUpload -  ", iRetry, " Attempt failed.", "Reason: ", excep.Message), "");
                                        break;
                                }
                                isProcessed = false;
                            }
                            if (isProcessed)
                                break;
                            System.Threading.Thread.Sleep(intRetryTimeOut);
                            iRetry++;
                        }
                    }

                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - DocumentUpload", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFile = null;
                objElementToAppend = null;
                htParams = null;
                objTempArrayObject = null;
                objWriteLog = null;
                if (objRS != null)
                {
                    if (objRS.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        objRS.Close();
                    objRS = null;
                }

            }
            return lngDocumentID;
        }

        /// <summary>
        /// Get the EMS path to move combined pdf
        /// </summary>
        /// <returns></returns>
        private string GetEMSPath()
        {
            string returnPath = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnPath = ConfigurationManager.AppSettings["EMSPath_DEV"];
                        break;
                    case "STG":
                        returnPath = ConfigurationManager.AppSettings["EMSPath_STG"];
                        break;
                    case "PRD":
                        returnPath = ConfigurationManager.AppSettings["EMSPath_PRD"];
                        break;
                    case "DR":
                        returnPath = ConfigurationManager.AppSettings["EMSPath_DR"];
                        break;
                    default:
                        returnPath = ConfigurationManager.AppSettings["EMSPath_DEV"];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnPath;
        }

        /// <summary>
        /// Procedure : AddClaimOwnerInfoToXML
        /// DateTime  : 10/4/2004 13:20
        /// Author    : Dan Price
        /// Purpose   : Finds the owner of claim, lngLynxID, and adds that user's contact
        ///              information as node, <ClaimOwner>, to strParamXML
        /// Parameters: strParamXML - string representation of an xml document.
        ///             lngLynxID - LynxID from which Claim Owner data should be retrieved.
        /// Return    : passed in xml string with <ClaimOwner> node appended.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="lngLynxID"></param>
        private void AddClaimOwnerInfoToXML(XmlDocument objParamDom, long lngLynxID)
        {
            XmlDocument objDataDom = null;
            XmlElement objElem = null;
            XmlElement objElemAppend = null;
            string strName = string.Empty,
            strDataXML = string.Empty,
            strOwnerUserID = string.Empty,
            strOwnerUserFirstName = string.Empty,
            strOwnerUserLastName = string.Empty,
            strOwnerUserPhoneAreaCode = string.Empty,
            strOwnerUserPhoneExchangeNumber = string.Empty,
            strOwnerUserPhoneUnitNumber = string.Empty,
            strOwnerUserPhoneExtensionNumber = string.Empty,
            strOwnerUserEmail = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - AddClaimOwnerInfoToXML", "Process Started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objLAPDPartnerDataMgr = new LAPDPartnerDataMgr.MLAPDPartnerDataMgr();
                objDataAccessor = new DataAccess.CDataAccessor();
                objMDomUtils = new MDomUtils();
                htParams = new Hashtable();

                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //Initialize DOM objects.
                objDataDom = new XmlDocument();

                //Get the procedure name from the config XML.
                strName = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/SPMap/ClaimSessionDetail");

                htParams.Add("@LynxID", lngLynxID);
                // Call proc to return claim session data.
                // strDataXML = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(strName, new object[] { "@LynxID", lngLynxID });
                strDataXML = objDataAccessor.ExecuteSpNamedParamsXML(strName, htParams, "APD");

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objDataDom, ref strDataXML, "", "Data");

                // Grab Claim Owner contact info from the retrieved xml.
                strOwnerUserID = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserID", true);
                strOwnerUserFirstName = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserNameFirst", true);
                strOwnerUserLastName = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserNameLast", true);
                strOwnerUserPhoneAreaCode = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneAreaCode", true);
                strOwnerUserPhoneExchangeNumber = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneExchangeNumber", true);
                strOwnerUserPhoneUnitNumber = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneUnitNumber", true);
                strOwnerUserPhoneExtensionNumber = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserExtensionNumber", true);
                strOwnerUserEmail = objLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserEmail", true);

                // Add a ClaimOwner element to the passed in xml.
                objElemAppend = objParamDom.DocumentElement;
                objElem = objMDomUtils.AddElement(ref objElemAppend, "ClaimOwner");

                // Add ClaimOwner info as attributes to the newly created element.
                objMDomUtils.AddAttribute(ref objElem, "UserID", strOwnerUserID);
                objMDomUtils.AddAttribute(ref objElem, "NameFirst", strOwnerUserFirstName);
                objMDomUtils.AddAttribute(ref objElem, "NameLast", strOwnerUserLastName);
                objMDomUtils.AddAttribute(ref objElem, "PhoneAreaCode", strOwnerUserPhoneAreaCode);
                objMDomUtils.AddAttribute(ref objElem, "PhoneExchangeNumber", strOwnerUserPhoneExchangeNumber);
                objMDomUtils.AddAttribute(ref objElem, "PhoneUnitNumber", strOwnerUserPhoneUnitNumber);
                objMDomUtils.AddAttribute(ref objElem, "PhoneExtensionNumber", strOwnerUserPhoneExtensionNumber);
                objMDomUtils.AddAttribute(ref objElem, "Email", strOwnerUserEmail);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - AddClaimOwnerInfoToXML", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objDataDom = null;
                objElem = null;
                objElemAppend = null;
                htParams = null;
            }
        }

        /// <summary>
        /// Get the vehicle details from NADA DB using the NADA web service
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="blnXformOPtoHTML"></param>
        /// <returns></returns>
        private string GetVehicleValueFromNADA(XmlDocument objParamDom, bool blnXformOPtoHTML)
        {
            string strLossDate = string.Empty,
                  strRegion = string.Empty,
                  strRegionDesc = string.Empty,
                  strYear = string.Empty,
                  strMake = string.Empty,
                  strModel = string.Empty,
                  strBody = string.Empty,
                  strVIN = string.Empty,
                 strMileage = string.Empty,
                  strZip = string.Empty,
                  strNADAVehicleId = string.Empty,
                  strAction = string.Empty,
                  strReadOnly = string.Empty, strReturn = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - GetVehicleValueFromNADA", "Process Started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objLAPDPartnerDataMgr = new LAPDPartnerDataMgr.MLAPDPartnerDataMgr();
                objNadaWS = new LAPDPartnerDataMgr.NadaWrapper();
                objMLAPDEcadAccessorMgr.InitializeGlobals();

                // Retrieve data from passed xml.
                strLossDate = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@LossDate", true);
                strRegion = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Region", false);
                strRegionDesc = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@RegionDesc", false);
                strYear = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Year", false);
                strMake = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Make", false);
                strModel = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Model", false);
                strBody = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Body", false);
                strVIN = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@VIN", false);
                strMileage = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Mileage", false);
                strZip = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Zip", false);
                strNADAVehicleId = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@NADAVehicleId", false);
                strAction = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Action", true);
                strReadOnly = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ReadOnly", true);

                strReturn = objNadaWS.GetAllAsXML2(strLossDate, strRegion, strRegionDesc, strYear, strMake, strModel,
                                              strBody, strMileage, strVIN, strNADAVehicleId,
                                            strAction, strZip, strReadOnly, blnXformOPtoHTML);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - GetVehicleValueFromNADA", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return strReturn;
        }

        /// <summary>
        /// ProcecssAPDRemittance
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private bool ProcessAPDRemittance(XmlDocument objParamDom)
        {
            string strReturn = string.Empty,
                 strSource = string.Empty,
                 strXSLT = string.Empty,
                 strFrom = string.Empty,
                 strSubject = string.Empty,
                 strDest = string.Empty,
                 strDocumentPath = string.Empty;
            bool blnOverrideRecip = false;
            IXMLDOMDocument2 objXSLdoc = null;
            XSLTemplate40 objXSLT = null;
            IXSLProcessor objXSLProc = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - ProcessAPDRemittance", "Process Started.", "");
                objLAPDPartnerDataMgr = new LAPDPartnerDataMgr.MLAPDPartnerDataMgr();

                // Get the source of the remittance.
                strSource = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//InsuranceCompany", true);

                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);
                // Obtain the client specific stylesheet from the config file.
                strXSLT = string.Concat(strDocumentPath, "\\", objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@xsl"),
                          objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/Client[@name='" + strSource + "']"), ".xsl");

                // Load the XSL into a Free Threaded DOM.
                objXSLdoc.async = false;
                objXSLdoc.load(strXSLT);

                // Load the XSL document into the XSL Template object.
                objXSLT.stylesheet = objXSLdoc;

                // Create an XSL processor from the XSL Template object.
                objXSLProc = objXSLT.createProcessor();

                // Load the source XML into the XSL processor.
                objXSLProc.input = objParamDom;

                // Perform the XSL transformation.
                objXSLProc.transform();

                // Retrieve the transormed XML from the XSL processor.
                strReturn = (objXSLProc.output).ToString();

                blnOverrideRecip = Convert.ToBoolean(objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@overrideRecip") == "true");

                if (!blnOverrideRecip)
                    strDest = objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/Recipient");
                else
                    strDest = objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/OverrideRecip");

                strFrom = objMLAPDEcadAccessorMgr.GetConfig("EventHandling/Email/@from");
                strSubject = objMLAPDEcadAccessorMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@emailSubject");

                objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strFrom, strDest, strSubject, strReturn, true);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - ProcessAPDRemittance", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objXSLdoc = null;
                objXSLT = null;
                objXSLProc = null;
            }
            return blnOverrideRecip;
        }

        /// <summary>
        /// Upload the Shop Documents into DB amd returns the DocumentID
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private long ShopDocumentUpload(XmlDocument objParamDom)
        {
            //Objects that need cleanup
            Recordset objRS = null;
            FileSystemObject objFile = null;
            XmlElement objxmlElement = null;
            //Primitives that don't.
            string strShopLocationID = string.Empty,
              strDocSrc = string.Empty,
              strLynxID = string.Empty,
              strNow = string.Empty,
                        strFinalName = string.Empty,
              strComments = string.Empty,
              strEffectiveDate = string.Empty, strExpirationDate = string.Empty,
              strApplication = string.Empty,
             strFileName = string.Empty, strFilePath = string.Empty,
                strExtension = string.Empty,
                                strUserID = string.Empty, strCSRNo = string.Empty, strFinalFileName = string.Empty;
            int intSuffix = 0, intIdx = 0;
            byte[] arrBuffer = null;
            long retShopDocumentUpload = 0;
            object objTempArrayObject = null;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - ShopDocumentUpload", "Process Started.", objParamDom.OuterXml);
                objMBase64Utils = new MBase64Utils();
                objLAPDPartnerDataMgr = new LAPDPartnerDataMgr.MLAPDPartnerDataMgr();
                objDataAccessor = new DataAccess.CDataAccessor();

                //Build a date time stamp string
                strNow = string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), " ", DateTime.Now.ToString("hh:mm:ss"));

                //Extract the Lynx ID.
                strShopLocationID = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//ShopLocationID", true);
                strShopLocationID = string.Format("{0:0000}", Convert.ToInt32(strShopLocationID));

                //Build the document source string from what was passed OR hard coded.
                strDocSrc = objParamDom.SelectSingleNode("//DocumentSource").InnerText;
                if (strDocSrc == string.Empty)
                    strDocSrc = "User";

                //* Store the document upload in the partner database.
                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);

                //* Insert the document into the LYNX Select application.
                //Extract the user id, default to system if necessary.
                strUserID = objParamDom.SelectSingleNode("//UserID").InnerText;
                if (strUserID == string.Empty)
                    strUserID = objMLAPDEcadAccessorMgr.GetConfig("WorkFlow/SysUserID");

                strCSRNo = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//CSRNo", false);
                if (strCSRNo == string.Empty)
                    strCSRNo = "CSR0000";

                strEffectiveDate = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//EffectiveDate", false);
                if (strEffectiveDate == string.Empty)
                    strEffectiveDate = "1/1/1900";

                strExpirationDate = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//ExpirationDate", false);
                if (strExpirationDate == string.Empty)
                    strExpirationDate = "12/31/2300";

                strComments = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Comments", false);

                //Build the directory structure from the last four digits of the LynxID.
                for (intIdx = 0; intIdx < 4; intIdx++)
                    strFileName = string.Concat(strFileName, "\\\\", (strShopLocationID.Substring(strShopLocationID.Length - 4, 4).Substring(intIdx, 1)));

                strFileName = string.Concat(strFileName, "\\\\");

                //Build the path
                strExtension = objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//FileExtension", true);

                objFile = new FileSystemObject();

                // Appended to end of filename to ensure uniqueness.
                intSuffix = 1;

                //Create a unique file name for the document.
                strFileName = string.Concat(strFileName, "F-", DateTime.Now.ToString("yyyyMMddhhmmss"), "-", strCSRNo, "-"
                     , strShopLocationID, "-", "Doc");

                strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                strFilePath = (string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/ShopRootDirectory"), strFinalFileName).Replace("\\\\", "\\"));

                if (BuildPath(objFile.GetParentFolderName(strFilePath)) == false)
                    return 0;
                //exit function need to verify...
                //error creating the path. the build path function will generate an error. no need to handle again here

                // If the file name construced above already exists, increment the suffix until a unique file name is found.
                while (objFile.FileExists(strFilePath))
                {
                    //if (isDebug)
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - ShopDocumentUpload", string.Concat("ShopDocument File already exist - ", strFilePath), objParamDom.OuterXml);

                    intSuffix = intSuffix + 1;
                    strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);  //TODO:verify Zeropad 
                    strFilePath = (string.Concat(objMLAPDEcadAccessorMgr.GetConfig("Document/ShopRootDirectory"), strFinalFileName)).Replace("\\\\", "\\");

                    //if (isDebug)
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessorMgr.CData.cs - ShopDocumentUpload", string.Concat("ShopDocument File already exist,increment the suffix until a unique file name is found = ", strFilePath), "");
                }

                //Create binary data from the Base64 encoded info.
                objxmlElement = (XmlElement)objLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "//FileData", true);
                objTempArrayObject = objMBase64Utils.ReadBinaryDataFromDomElement(ref objxmlElement);
                arrBuffer = Convert.FromBase64String(Convert.ToString(objTempArrayObject));

                //Write the binary data to a file in the document structure.              
                objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);

                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessorMgr.CData.cs - ShopDocumentUpload", string.Concat("Document Created = ", strFilePath), "");

                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                hashTable = new Hashtable();
                hashTable.Add("@ShopLocationID", Convert.ToInt64(strShopLocationID));
                hashTable.Add("@EffectiveDate", strEffectiveDate);
                hashTable.Add("@ExpirationDate", strExpirationDate);
                hashTable.Add("@ImageDate", strNow);
                hashTable.Add("@ImageLocation", strFinalFileName);
                hashTable.Add("@DocumentSource", strDocSrc);
                hashTable.Add("@DocumentType", objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DocumentType", true));
                hashTable.Add("@ImageType", objLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//FileExtension", true));
                hashTable.Add("@Comments", strComments);
                hashTable.Add("@UserID", Convert.ToInt32(strUserID));

                retShopDocumentUpload = objDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("PartnerSettings/ShopDocumentInsertSPName"), hashTable, "APD");
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessorMgr.CData.cs - ShopDocumentUpload", string.Concat("Document details saved into DB in DocumentID = ", retShopDocumentUpload), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (objRS != null)
                    if (objRS.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        objRS.Close();
                objRS = null;
                objFile = null;
                objxmlElement = null;
                hashTable = null;
            }
            return retShopDocumentUpload;
        }

        /// <summary>
        /// BuildPath
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private bool BuildPath(string sPath)
        {
            try
            {
                if (!string.IsNullOrEmpty(sPath))
                {
                    if (!Directory.Exists(sPath))
                        Directory.CreateDirectory(sPath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        /// <summary>
        /// Get the FNOL Data from the FNOL web service
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private string GetFNOLData(XmlElement objParamDom)
        {
            XMLHTTP objXMLHTTPPost = null;
            XmlDocument xmlDocFNOLClaimXML = null;
            XmlDocument xmlDocReturnFNOL = null;
            string strFNOLServiceURL = string.Empty,
             strResponseText = string.Empty,
             strFNOL2APDXSLPath = string.Empty, strDocumentPath = string.Empty;
            Common.WriteLog objWriteLog = new Common.WriteLog();

            try
            {
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessorMgr.CData.cs - GetFNOLData", "Process started.", "");
                objMDomUtils = new MDomUtils();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                xmlDocFNOLClaimXML = new XmlDocument();
                xmlDocReturnFNOL = new XmlDocument();

                objMLAPDEcadAccessorMgr.InitializeGlobals();
                strFNOLServiceURL = objMLAPDEcadAccessorMgr.GetConfig("FNOLService/URL");

                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("FNOL URL - ", strFNOLServiceURL), "");

                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessorMgr.CData.cs - GetFNOLData", string.Concat("Calling FNOL Service =", strFNOLServiceURL), "");

                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);
                strFNOL2APDXSLPath = string.Concat(strDocumentPath, "\\", objMLAPDEcadAccessorMgr.GetConfig("FNOLService/APDAssignmentXSL"));

                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("APDAssignmentXSL Path - ", strFNOL2APDXSLPath), "");

                //post the xml to the webserver
                objXMLHTTPPost = new XMLHTTP();
                objXMLHTTPPost.open("POST", strFNOLServiceURL, false);
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Performing HTTPPost Operation to get the claim details."), "");
                objXMLHTTPPost.send(objParamDom.FirstChild.OuterXml);
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "HTTPPost completed."), "");
                strResponseText = (objXMLHTTPPost.responseText).Trim();
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "HTTPPost completed and retrieve data.", "HTTP Status Code - ", objXMLHTTPPost.status), "");

                //Decode the HTML special characters.
                strResponseText = HttpUtility.HtmlDecode(strResponseText);
                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Response from FNOL aspx page."), strResponseText);
                strResponseText = strResponseText.Replace("&", "&amp;");
                strResponseText = strResponseText.Replace("&lt;", "<");
                strResponseText = strResponseText.Replace("&gt;", ">");

                objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Decode response xml data.."), strResponseText);

                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessorMgr.CData.cs - GetFNOLData", string.Concat("Response from FNOL service = ", strResponseText), "");
                if (objXMLHTTPPost.status != 200) //the response was not ok. raise an error.
                    throw new Exception(string.Concat(EventCodes.eInvalidHTTPPostResponse, "Response Text: ", strResponseText, " Status Code: ", objXMLHTTPPost.status));

                if (objParamDom.FirstChild.Name == "RetrieveFNOL")
                {
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "RetrieveFNOL Started..."), strResponseText);
                    xmlDocFNOLClaimXML.LoadXml(strResponseText);
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "RetrieveFNOL XSLT Transformation Started..."), strResponseText);
                    xmlDocReturnFNOL = objMDomUtils.TransformDomAsDom(ref xmlDocFNOLClaimXML, strFNOL2APDXSLPath);
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "RetrieveFNOL Completed, loading the data to XML Document"), "");
                    strResponseText = xmlDocReturnFNOL.OuterXml;
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Result of transformation"), strResponseText);

                    //Decode the HTML special characters.
                    strResponseText = HttpUtility.HtmlDecode(strResponseText);
                    strResponseText = strResponseText.Replace("&", "&amp;");
                    strResponseText = strResponseText.Replace("&lt;", "<");
                    strResponseText = strResponseText.Replace("&gt;", ">");
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Retreive claim Details Completed."), strResponseText);
                }

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Process completed."), strResponseText);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CData.cs - GetFNOLData", string.Concat("Server Name - ", Environment.MachineName, "Error Detail ", ex.Message), strResponseText);
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objXMLHTTPPost = null;
                xmlDocFNOLClaimXML = null;
                xmlDocReturnFNOL = null;
                xmlDocFNOLClaimXML = null;
                xmlDocReturnFNOL = null;
            }
            return strResponseText;
        }


        ///// <summary>
        ///// Convert the file from BMP to JPG format
        ///// </summary>
        ///// <param name="strFileName"></param>
        ///// <returns></returns>
        private string GetHQShopData(XmlDocument objParamDom)
        {

            string strResponseText = string.Empty;
            Common.WriteLog objWriteLog = new Common.WriteLog();
            HQProviderSearch.ProviderSearchServiceClient objHQShop = null;

            try
            {

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - GetHQShopData", string.Concat("Server Name - ", Environment.MachineName, " - Process started."), objParamDom.OuterXml);

               objHQShop = new HQProviderSearch.ProviderSearchServiceClient();

               if (isExtensiveDebug)
                   objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetHQShopData", string.Concat("Server Name - ", Environment.MachineName, " - HQProviderSearch object created."), "");


               objHQShop.ClientCredentials.UserName.UserName = Convert.ToString(ConfigurationManager.AppSettings["HQProviderUserID"]);
               objHQShop.ClientCredentials.UserName.Password = Convert.ToString(ConfigurationManager.AppSettings["HQProviderPassword"]);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetHQShopData", string.Concat("Server Name - ", Environment.MachineName, " - HQProviderSearch ClientCredentials passed wiht username :", objHQShop.ClientCredentials.UserName.UserName), "");


                strResponseText = objHQShop.GetProviderSearchResult(objParamDom.OuterXml);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - GetHQShopData", string.Concat("Server Name - ", Environment.MachineName, " - HQProviderSearch called GetProviderSearchResult."), strResponseText);

            }

            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CData.cs - GetHQShopData", string.Concat("Server Name - ", Environment.MachineName, "Error Detail ", ex.Message), strResponseText);
                throw ex;
            }

           

            return strResponseText;

        }

        ///// <summary>
        ///// Convert the file from BMP to JPG format
        ///// </summary>
        ///// <param name="strFileName"></param>
        ///// <returns></returns>
        private void GetNewHQShopStandard(ref XmlDocument objParamDom, ref XmlElement objRootElement, string strProcName, string strEntity, string strApplicationName)
        {

            string strResponseText = string.Empty;
            int intSimilarMatchCount = -1;
            Common.WriteLog objWriteLog = new Common.WriteLog();
            HQAddrSTD.RequestAddressSummary reqAddSummary = null;
            HQAddrSTD.ResponseAddressSummary[] resAddSummary = null;
            HQAddrSTD.AddressStandardizedServiceClient addStandardizedService = null;

            try
            {

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CData.cs - GetNewHQShopStandard", string.Concat("Server Name - ", Environment.MachineName, " - Process started."), objParamDom.OuterXml);

                reqAddSummary = new HQAddrSTD.RequestAddressSummary();

                if (!string.IsNullOrEmpty(objRootElement.Attributes[1].Value))
                    reqAddSummary.Street = objRootElement.Attributes[1].Value;//"187 blueberry road"; //"";
                else
                    reqAddSummary.Street = objRootElement.Attributes[1].Value;

                reqAddSummary.ZipCode = objRootElement.Attributes[5].Value;
                reqAddSummary.City = objRootElement.Attributes[3].Value;
                reqAddSummary.State = objRootElement.Attributes[4].Value;
                reqAddSummary.Country = "USA" ;
                reqAddSummary.MaxOfSuggestions = 5;

                addStandardizedService = new HQAddrSTD.AddressStandardizedServiceClient();



                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUG", "EcadAccessor.CData.cs - GetNewHQShopStandard", string.Concat("Server Name - ", Environment.MachineName, " - ShopSimilarMatch object created."), "");


                addStandardizedService.ClientCredentials.UserName.UserName = Convert.ToString(ConfigurationManager.AppSettings["HQProviderUserID"]);
                addStandardizedService.ClientCredentials.UserName.Password = Convert.ToString(ConfigurationManager.AppSettings["HQProviderPassword"]);

                resAddSummary = addStandardizedService.GetAddressSummary(reqAddSummary);
                var jsonResponse = new JavaScriptSerializer().Serialize(resAddSummary);

                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - GetAddressSummary Request", string.Concat("Server Name - ", Environment.MachineName, " - ShopSimilarMatch calling GetAddressSummaryRequest : "), objRootElement.OuterXml);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CData.cs - GetAddressSummary Response", string.Concat("Server Name - ", Environment.MachineName, " - ShopSimilarMatch called GetAddressSummaryResult : "),jsonResponse);

                // put the code here
                if (resAddSummary != null && resAddSummary.Length > 0 && resAddSummary[0].Status.ToUpper() == "VALID")
                {
                        objRootElement.Attributes[5].Value = resAddSummary[0].ZipCode;
                        objRootElement.Attributes[4].Value = resAddSummary[0].State;
                        objRootElement.Attributes[1].Value = resAddSummary[0].Street;
                        objRootElement.Attributes[3].Value = resAddSummary[0].City;

                    //Execute database procedure to retrieve requested data
                    ExecuteSPXML(objParamDom, ref objRootElement, strProcName, strEntity, strApplicationName);

                    intSimilarMatchCount = objParamDom.SelectNodes("Root/Shop").Count;

                    if (intSimilarMatchCount == 0)
                       objParamDom.DocumentElement.SetAttribute("validshop", "YES");
                }
                else
                {
                    ExecuteSPXML(objParamDom, ref objRootElement, strProcName, strEntity, strApplicationName);

                    intSimilarMatchCount = objParamDom.SelectNodes("Root/Shop").Count;

                    if (intSimilarMatchCount == 0)
                       objParamDom.DocumentElement.SetAttribute("validshop", "NO");
                }

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CData.cs - GetNewHQShopStandard", string.Concat("Server Name - ", Environment.MachineName, " - ShopSimilarMatch called GetAddressSummaryResult : "), objParamDom.OuterXml);

            }

            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CData.cs - GetNewHQShopStandard", string.Concat("Server Name - ", Environment.MachineName, "Error Detail ", ex.Message), ex.StackTrace);
                throw ex;
            }           
        }

        ///// <summary>
        ///// Convert the file from BMP to JPG format
        ///// </summary>
        ///// <param name="strFileName"></param>
        ///// <returns></returns>
        //private bool ConvertBmp2Jpg(string strFileName)
        //{
        //    FileSystemObject objFile = null;
        //    bool blnSuccess = false;
        //    string strJPGFile = string.Empty,
        //        strConverted = string.Empty;

        //    try
        //    {
        //        objFile = new FileSystemObject();
        //        if (objFile.FileExists(strFileName))
        //        {
        //            imgMagick = new MagickImage();
        //            strJPGFile = objFile.BuildPath(objFile.GetParentFolderName(strFileName), string.Concat(objFile.GetBaseName(strFileName), ".jpg"));
        //            strConverted = Convert.ToString(imgMagick.Convert(new object[] { "-quality=90", strFileName, strJPGFile }));

        //            if (objFile.FileExists(strJPGFile))
        //                blnSuccess = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        objFile = null;
        //        imgMagick = null;
        //    }

        //    return blnSuccess;
        //}
        #endregion
    }
}
