﻿using System;
using System.Xml;
using ADODB;
using Scripting;
using MSXML2;
using Lynx.APD.Component.Library.PartnerDataMgr;
using DataAccess = Lynx.APD.Component.Library.APDDataAccessor;
using System.Text.RegularExpressions;
using LAPDCommServer;
using ReportUtils;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    /// Component EcadAccessor : Class Module CWebAssignment
    /// 
    /// This class module processes web assignments from the Claim Point Web
    /// Assignment website.  It was originally written to be private and called
    /// from other modules of the application only.  It has since been exposed
    /// to the world so as to be called from PartnerData.
    /// 
    /// </summary>
    class CWebAssignment
    {
        #region Global Declarations
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CWebAssignment.");
        private string mstrPartner = string.Empty;
        MLAPDEcadAccessorMgr objMLAPDEcadAccessorMgr = null;
        MRecordsetUtils objMRecordsetUtils = null;
        MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        MEscapeUtilities objMEscapeUtilities = null;
        Common.MDomUtils objMDomUtils = null;
        LAPDCommServer.CCommServerClass objCommSvr = null;
        //CCommServer objCommSvr = null;
        clsReport objReportUtil = null;
        lynxReceive.Loss objGlaxisProxy = null;
        DataAccess.CDataAccessor objcDataAccessor = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);
        #endregion

        #region Enumerators
        //Internal error codes for this module.
        private enum EventCodes : ulong
        {
            eProcedureLevelDoesntExist = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CWebAssignment_CLS_ErrorCodes,
            eNoRecordsetReturned,
            eBadDataReturned,
            eUserIdDoesNotExist,
            eUserIdDoesNotMatch,
            eLoginAlreadyUsed
        }
        #endregion

        #region Structures

        public struct typProcLevel
        {
            public int intLevelID;
            public string strName;
        }
        public struct typProc
        {
            public int intID;
            public int intLevelID;
            public string strName;
        }

        public typProcLevel[] marrProcLevelList;
        public typProc[] marrProcList;

        #endregion

        #region Public Functions

        /// <summary>
        ///  Processes a web assignment from the Claim Point Web Assignment module. 
        ///  It expects an XML document with the form:
        /// 
        ///  <WebAssignment>
        ///      <Claim> ... </Claim>
        ///      <Vehicle> ... </Vehicle>
        ///      <Involved> ... </Involved>
        ///  </WebAssignment>
        /// 
        ///  Multiple Vehicle and Involved elements are possible.
        ///  Each element has multiple child elements as passed from Claim Point.
        /// 
        ///  Calls ApdLoadAssignment below to insert the claim into APD.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="strDataSourceCode"></param>
        /// <returns></returns>
        public long DoAssignment(XmlDocument objParamDom, string strDataSourceCode)
        {
            //Objects that need clean-up           
            XmlDocument xmlDocumentReturn = null;
            //Primitives that don't
            long lngUID = 0;
            bool blnRaiseToCaller;
            string strUID = string.Empty,
              strXferXslName = string.Empty,
              strXferXslVersion = string.Empty,
              strAppend = string.Empty, strDocumentPath = string.Empty;
            int result;
            long lngLynxID = 0;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - DoAssignment", "Claim assignment process started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMDomUtils = new Common.MDomUtils();

                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //This boolean is used in the error handler to determine whether to
                //raise errors to the calling process or not.  Once we have stored
                //the data in the partner database, assume that the call is a success
                //as far as the caller is concerned.
                blnRaiseToCaller = true;

                // Determine where the assignment is coming from.
                if (strDataSourceCode == "CP" || strDataSourceCode == "APD")
                {
                    // Try to get a LynxID from the xml.
                    strUID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/LynxID", false);

                    if (int.TryParse(strUID.Trim(), out result))
                        lngUID = Convert.ToInt64(strUID);
                    else
                        lngUID = 0;

                    //If not LynxID exists, create a new one.
                    if (Convert.ToBoolean(lngUID == 0))
                        lngUID = GetFnolUID();
                }
                else
                    lngUID = Convert.ToInt64(objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/LynxID", true));

                //Store the assignment in the partner database
                StoreAssignmentXml(lngUID, objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DataSource", false), objParamDom.OuterXml);

                //Ok, we made it into partner, so from the caller's standpoint
                //assume that the transaction is good and dont raise.
                blnRaiseToCaller = false;

                xmlDocumentReturn = new XmlDocument();
                //Get some DOM objects to work with.               
                strXferXslName = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" + strDataSourceCode + "']/@Xsl");
                strXferXslVersion = objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" + strDataSourceCode + "']/@XslVersion");

                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);
                //Load up the assignment transfer style sheet.
                strAppend = string.Concat(strDocumentPath, "\\", strXferXslName, strXferXslVersion, ".xsl");
                //objMDomUtils.LoadXmlFile(ref xmlDocumentXslTrans, ref strAppend, PROC_NAME, "Style Sheet");             

                xmlDocumentReturn = objMDomUtils.TransformDomAsDom(ref objParamDom, strAppend);
                //Call sister function to do the actual load.
                lngLynxID = ApdLoadAssignment(ref xmlDocumentReturn, lngUID);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - DoAssignment", "DoAssignment process completed", string.Concat("LynxID =", lngLynxID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                xmlDocumentReturn = null;
            }
            return lngLynxID;
        }

        /// <summary>
        /// Procedure : ApdLoadAssignmentExternal
        /// DateTime  : 7/6/2005 13:31
        /// Author    : CSR0901
        /// Purpose   : Added this to get around memory access error when ApdLoadAssignment is called
        ///             from another COM+ component.  The memeory access issue arises because MSXML
        ///             interfaces are not marshalled accross process boundaries.
        /// 
        /// Parameters: strParamXml - an XML string that can be used to created the MSXML DOMDocument
        ///             required by ApdLoadAssignment.
        /// 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="strPartner"></param>
        /// <param name="lngUID"></param>
        /// <returns></returns>
        public long ApdLoadAssignmentExternal(ref string strParamXML, ref string strPartner, long lngUID = 0)
        {
            XmlDocument objDom = null;
            long lngReturnID = 0;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignmentExternal", "ApdLoadAssignmentExternal process started", "");
                objDom = new XmlDocument();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new Common.MDomUtils();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                // Load the passed XML string in to a DOM object.
                objMDomUtils.LoadXml(ref objDom, ref strParamXML, "", "ApdLoadAssignmentExternal");

                // Save passed partner for later use.
                mstrPartner = strPartner;
                lngReturnID = ApdLoadAssignment(ref objDom);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignmentExternal", "ApdLoadAssignmentExternal process completed", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            // Pass the locally created DOM object to ApdLoadAssignmentExternal.
            return lngReturnID;
        }

        /// <summary>
        ///  Processes a web assignment in an attribute-centric format.
        ///  It expects an XML document with the form:
        /// 
        ///  <WebAssignment>
        ///     <Claim ... >
        ///        <Vehicle ... >
        ///           <Involved ... />
        ///        </Vehicle>
        ///     </Claim>
        ///  </WebAssignment>
        /// 
        ///  Multiple Vehicle and Involved elements are possible.
        ///  Each element has multiple attributes as defined by the APD DB SPs.
        /// </summary>
        ///  Returns the LYNX ID of the new claim.
        ///  <param name="objParamDom"></param>
        /// <param name="lngUID"></param>
        /// <returns></returns>
        public long ApdLoadAssignment(ref XmlDocument objParamDom, long lngUID = 0)
        {
            //Objects that need clean-up
            XmlNodeList objElementList = null;
            XmlElement objxmlElement = null;
            XmlElement xmlElemAppend = null;
            //Primitives that don't
            Boolean blnClaimDataIncluded,
              blnRollback = false,
              blnInTrans = false;
            long lngUserID = 0;
            string strCarrierRepEmail = string.Empty,
                strElement = string.Empty, tempBoolvalue = string.Empty, strApplication = string.Empty;
            int intLevel, intIndex;
            SqlConnection sqlConnection = null;
            SqlTransaction transaction = null;
            APDDataAccessor.CDataAccessor cobjAPDDataAccessor = null;
            Common.WriteLog objWriteLog = null;
            int intRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "ApdLoadAssignment process completed", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr.InitializeGlobals();

                tempBoolvalue = objParamDom.SelectSingleNode("/WebAssignment/Claim/@NewClaimFlag").InnerText;

                if (tempBoolvalue.Trim() == "1")
                    blnClaimDataIncluded = true;
                else
                    blnClaimDataIncluded = false;

                // HACK: we will not maintain a list of users for GRANGE.  We will use one user whose email addrress is in the config file.
                if ((mstrPartner.Trim().Length) > 0)
                {
                    strCarrierRepEmail = objMLAPDPartnerDataMgr.GetConfig(string.Concat("PartnerSettings/", mstrPartner, "/AssignmentUser/@email"));

                    // Check whether CarrierRepLogin attribute in passed xml is populated.
                    if (0 == ((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepLogin", true)).Trim().Length))    // If not, populate with value from config file.
                        objParamDom.SelectSingleNode("//@CarrierRepLogin").InnerText = strCarrierRepEmail;

                    // Check whether CarrierRepLogin attribute in passed xml is populated.
                    if (0 == ((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepEmailAddress", true)).Trim().Length))     // If not, populate with value from config file.
                        objParamDom.SelectSingleNode("//@CarrierRepEmailAddress").InnerText = strCarrierRepEmail;
                }

                //Get the entered user detail and add it to the param dom.
                if (blnClaimDataIncluded)
                    lngUserID = FindOrAddUser(ref objParamDom);

                //Get a new LynxID for the assignment if none was passed in.
                if (lngUID == 0)
                    lngUID = GetFnolUID();

                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);
                //Remove all blank child attributes in the passed XML.
                //This way all blank attributes will be replaced by the default values.
                objxmlElement = objParamDom.DocumentElement;
                objMDomUtils.RemoveAllChildBlankAttributes(ref objxmlElement);

                //This boolean is used to determine whether we should rollback when an error occurs.
                blnRollback = true;
                blnInTrans = true;

                //Start a transaction.//TODO Begin Transactions
                //objMLAPDEcadAccessorMgr.g_objDataAccessor.BeginTransaction();

                //Find all elements within the passed document below the root.
                //Each one corresponds to a procedure level.
                objElementList = objxmlElement.SelectNodes("//WebAssignment//*");

                try
                {
                    cobjAPDDataAccessor = new DataAccess.CDataAccessor();
                    sqlConnection = new SqlConnection(cobjAPDDataAccessor.GetConnectionString(strApplication));
                    sqlConnection.Open();

                    transaction = sqlConnection.BeginTransaction();
                    //Loop through and process each of them.
                    foreach (XmlElement objElement in objElementList)
                    {
                        xmlElemAppend = objElement;
                        //The current procedure level name.
                        strElement = xmlElemAppend.Name;

                        //Add the LynxID attribute to every element.
                        objMDomUtils.AddAttribute(ref xmlElemAppend, "LynxID", lngUID.ToString());

                        //Add the FNOLUserID and CarrierRepUserID attribute to every element, if not already present.
                        if (objElement.SelectSingleNode("@FNOLUserID") == null)
                            objMDomUtils.AddAttribute(ref xmlElemAppend, "FNOLUserID", lngUserID.ToString());

                        //Add the FNOLUserID attribute to every element.
                        if (objElement.SelectSingleNode("@CarrierRepUserID") == null)
                            objMDomUtils.AddAttribute(ref xmlElemAppend, "CarrierRepUserID", lngUserID.ToString());

                        objMDomUtils.RemoveAllChildBlankAttributes(ref xmlElemAppend);
                        // The current procedure level (index).
                        // intLevel = LevelFromName(strElement)

                        //Commit the transaction before the last call to uspFnolLoadClaim.
                        //This is to fix the transaction deadlock problems we are having.
                        if (strElement == "BeginInsert")
                        {
                            if (blnInTrans == true)
                            {
                                blnInTrans = false; //TODO Need to Commit the transactions

                                //if (isDebug)
                                //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Transaction going to commit if BeginInsert is True.LynxID - ", lngUID), "");

                                transaction.Commit();
                                objWriteLog.LogEvent("COM Conversion", "COMMIT", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Transaction Commited for the LynxID - ", lngUID), "");
                                //objMLAPDEcadAccessorMgr.g_objDataAccessor.CommitTransaction();
                            }
                        }

                        switch (strElement)
                        {
                            case "Claim":
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Clear the FNOL tables using FNOLClearLoadTablesSP.LynxID - ", lngUID), "");

                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLClearLoadTablesSP"), SetFNOLClearLoadTables(lngUID), strApplication);

                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Claim Details using FNOLLoadClaimSP.LynxID - ", lngUID), "");
                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimSP"), SetFNOLLoadClaimSPParam(xmlElemAppend), strApplication);
                                break;
                            case "Coverage":
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Coverage Details using FNOLLoadClaimCoverageSP.LynxID - ", lngUID), "");

                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimCoverageSP"), SetFNOLLoadClaimCoverage(xmlElemAppend), strApplication);
                                break;
                            case "Vehicle":
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Vehicle Details using FNOLLoadClaimVehicleSP.LynxID -  ", lngUID), "");

                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimVehicleSP"), SetFNOLLoadClaimVehicle(xmlElemAppend), strApplication);
                                break;
                            case "Involved":
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Involved Details using FNOLLoadInvolvedSP.LynxID -  ", lngUID), "");

                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadInvolvedSP"), SetFNOLLoadInvolved(xmlElemAppend), strApplication);
                                break;
                            case "Property":
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Property Details using FNOLLoadPropertySP.LynxID -  ", lngUID), "");

                                cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadPropertySP"), SetFNOLLoadProperty(xmlElemAppend), strApplication);
                                break;
                            case "BeginInsert":
                                //Add the ClaimDataIncluded attribute.
                                xmlElemAppend = objElement;
                                if (objElement.SelectSingleNode("@NewClaimFlag") == null)
                                    objMDomUtils.AddAttribute(ref xmlElemAppend, "NewClaimFlag", (blnClaimDataIncluded ? "1" : "0"));
                                bool isProcessed = false;
                                int iRetry = 0;
                                while (iRetry < 6 && !isProcessed)
                                {
                                    try
                                    {
                                        cobjAPDDataAccessor.ExecuteSpNpColClaimInsert(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/FNOLInsClaimSP"), SetFNOLInsClaim(xmlElemAppend), strApplication);
                                        isProcessed = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        switch (iRetry)
                                        {
                                            case 0:
                                                objWriteLog.Send(ex, string.Concat("WARNING: FNOLInsClaim - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                                objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("FNOLInsClaim - 1st Attempt failed.  Reason: ", ex.Message), "");
                                                break;
                                            case 5:
                                                //5th attempt failure, notify as Error
                                                objWriteLog.Send(ex, string.Concat("ERROR: FNOLInsClaim - 5th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                                objWriteLog.LogEvent("COMConversion", "ERROR", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("SERIOUS - FNOLInsClaim - 5th Attempt failed. Reason: ", ex.Message), "");
                                                throw new Exception("An unexpected error has caused a delay in processing this claim.  LYNX Services have been notified and it will be processed shortly.");
                                                break;
                                            default:
                                                objWriteLog.LogEvent("COMConversion", "WARNING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("FNOLInsClaim -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                                break;
                                        }
                                        isProcessed = false;
                                    }
                                    if (isProcessed)
                                        break;
                                    System.Threading.Thread.Sleep(intRetryTimeOut);
                                    iRetry++;
                                }
                                break;
                        }
                    }

                    //if (isDebug)
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Inserting Claim Details in main table using FNOLInsClaimSP.LynxID -  ", lngUID), "");

                    //Automatic shop assignment is not critical to the web assignment because
                    //the claims reps can pick it up later.  Disable rollbacks and commit.
                    blnRollback = false;

                    //Commit if the transaction still exists.               
                    if (blnInTrans)
                    {
                        transaction.Commit();
                        objWriteLog.LogEvent("COM Conversion", "COMMIT", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Transaction Commited for the LynxID - ", lngUID), "");
                    }
                }
                catch (Exception ex)
                {
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", ex.Message, objParamDom.OuterXml);
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Claim Web Assignment XML for failed LynxID - ", lngUID), objParamDom.OuterXml);
                    if (blnRollback)
                        cobjAPDDataAccessor.RollbackTransaction(ref sqlConnection, ref transaction);
                    throw ex;
                }
                finally
                {
                    if (blnRollback)
                    {
                        cobjAPDDataAccessor.RollbackTransaction(ref sqlConnection, ref transaction);
                        objWriteLog.LogEvent("COM Conversion", "ROLLBACK", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Transaction get Rollback for the LynxId -", lngUID), "");
                    }
                    sqlConnection.Close();
                    transaction.Dispose();
                }

                //Should we send an assignment through CCC to Autoverse to Mitchell for our Auto Techs?
                if (IsAutoTechAssignment(ref objParamDom))
                {
                    //if (isDebug)
                    //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", string.Concat("Send Shop Assignment. LynxID - ", lngUID), "");

                    //Allow an override from the config file.
                    if (objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignAutoTech/@Enabled") == "True")
                    {
                        //Make the assignment through CCC to AutoVerse to Mitchell.
                        SendShopAssignments(objParamDom, lngUID, true);
                    }
                    else
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "Auto-Tech Auto Assignment - disabled in config.xml", objParamDom.OuterXml);
                }
                //Should we call CommServer to send a program shop assignment?
                //HACK:  The AssignmentAtSelectionFlag is currently an attribute of the Insurance Company.  This will now need to be
                // resolved based on the InsuranceCompany and AssignmentType.  The 'or' condition below must then be removed.
                else if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentAtSelectionFlag", true) == "1" ||
                      objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentDescription", true) == "Mobile Electronics Assignment")
                {
                    //Allow an override from the config file.
                    if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentDescription", true) == "Mobile Electronics Assignment")
                    {
                        //ME assignment to FCS
                        if (objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignME/@Enabled") == "True")
                        {
                            //Make the assignments, one per vehicle.
                            SendShopAssignments(objParamDom, lngUID, false);
                        }
                        else
                            objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "ME Auto Assignment - disabled in config.xml", objParamDom.OuterXml);
                    }
                    else if (objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignShop/@Enabled") == "True")
                    {
                        //Make the assignments, one per vehicle.
                        SendShopAssignments(objParamDom, lngUID, false);
                    }
                    else
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "Shop Auto Assignment - disabled in config.xml", objParamDom.OuterXml);
                }
                else
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "Automatic Assignment not required.", objParamDom.OuterXml);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "Process completed.", objParamDom.OuterXml);

                //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "END", string.Concat("EcadAccessor.CWebAssignment.cs - ApdLoadAssignment", "Server Name - ", Environment.MachineName), string.Concat("LYNX ID:", Convert.ToString(lngUID)), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objElementList = null;
                objxmlElement = null;
                xmlElemAppend = null;
                sqlConnection = null;
                transaction = null;
            }
            //Hand back the new LynxID.          
            return lngUID;
        }

        /// <summary>
        /// Load the Parameters to the FNOLLoadIns 
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <returns></returns>
        private Hashtable SetFNOLInsClaim(XmlElement xmlElement)
        {
            Hashtable htParams = null;
            try
            {
                htParams = new Hashtable();
                if (htParams != null)
                {
                    if (xmlElement.HasAttribute("LynxID"))
                        htParams.Add("@LynxID", xmlElement.Attributes["LynxID"].Value);
                    if (xmlElement.HasAttribute("NewClaimFlag"))
                        htParams.Add("@NewClaimFlag", xmlElement.Attributes["NewClaimFlag"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htParams;
        }

        /// <summary>
        /// load the parameters to the FNOlLoadProperty
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <returns></returns>
        private Hashtable SetFNOLLoadProperty(XmlElement xmlElement)
        {
            Hashtable htParams = null;
            try
            {
                if (htParams != null)
                {
                    if (xmlElement.HasAttribute("LynxID"))
                        htParams.Add("@LynxID", xmlElement.Attributes["LynxID"].Value);
                    if (xmlElement.HasAttribute("PropertyNumber"))
                        htParams.Add("@PropertyNumber", xmlElement.Attributes["PropertyNumber"].Value);
                    if (xmlElement.HasAttribute("ContactAddress1"))
                        htParams.Add("@ContactAddress1", xmlElement.Attributes["ContactAddress1"].Value);
                    if (xmlElement.HasAttribute("ContactAddress2"))
                        htParams.Add("@ContactAddress2", xmlElement.Attributes["ContactAddress2"].Value);
                    if (xmlElement.HasAttribute("ContactAddressCity"))
                        htParams.Add("@ContactAddressCity", xmlElement.Attributes["ContactAddressCity"].Value);
                    if (xmlElement.HasAttribute("ContactAddressState"))
                        htParams.Add("@ContactAddressState", xmlElement.Attributes["ContactAddressState"].Value);
                    if (xmlElement.HasAttribute("ContactAddressZip"))
                        htParams.Add("@ContactAddressZip", xmlElement.Attributes["ContactAddressZip"].Value);
                    if (xmlElement.HasAttribute("ContactNameFirst"))
                        htParams.Add("@ContactNameFirst", xmlElement.Attributes["ContactNameFirst"].Value);
                    if (xmlElement.HasAttribute("ContactNameLast"))
                        htParams.Add("@ContactNameLast", xmlElement.Attributes["ContactNameLast"].Value);
                    if (xmlElement.HasAttribute("ContactNameTitle"))
                        htParams.Add("@ContactNameTitle", xmlElement.Attributes["ContactNameTitle"].Value);
                    if (xmlElement.HasAttribute("ContactPhone"))
                        htParams.Add("@ContactPhone", xmlElement.Attributes["ContactPhone"].Value);
                    if (xmlElement.HasAttribute("ContactPhoneExt"))
                        htParams.Add("@ContactPhoneExt", xmlElement.Attributes["ContactPhoneExt"].Value);
                    if (xmlElement.HasAttribute("DamageAmount"))
                        htParams.Add("@DamageAmount", xmlElement.Attributes["DamageAmount"].Value);
                    if (xmlElement.HasAttribute("DamageDescription"))
                        htParams.Add("@DamageDescription", xmlElement.Attributes["DamageDescription"].Value);
                    if (xmlElement.HasAttribute("OwnerAddress1"))
                        htParams.Add("@OwnerAddress1", xmlElement.Attributes["OwnerAddress1"].Value);
                    if (xmlElement.HasAttribute("OwnerAddress2"))
                        htParams.Add("@OwnerAddress2", xmlElement.Attributes["OwnerAddress2"].Value);
                    if (xmlElement.HasAttribute("OwnerAddressCity"))
                        htParams.Add("@OwnerAddressCity", xmlElement.Attributes["OwnerAddressCity"].Value);
                    if (xmlElement.HasAttribute("OwnerAddressState"))
                        htParams.Add("@OwnerAddressState", xmlElement.Attributes["OwnerAddressState"].Value);
                    if (xmlElement.HasAttribute("OwnerAddressZip"))
                        htParams.Add("@OwnerAddressZip", xmlElement.Attributes["OwnerAddressZip"].Value);
                    if (xmlElement.HasAttribute("OwnerBusinessName"))
                        htParams.Add("@OwnerBusinessName", xmlElement.Attributes["OwnerBusinessName"].Value);
                    if (xmlElement.HasAttribute("OwnerNameFirst"))
                        htParams.Add("@OwnerNameFirst", xmlElement.Attributes["OwnerNameFirst"].Value);
                    if (xmlElement.HasAttribute("OwnerNameLast"))
                        htParams.Add("@OwnerNameLast", xmlElement.Attributes["OwnerNameLast"].Value);
                    if (xmlElement.HasAttribute("OwnerNameTitle"))
                        htParams.Add("@OwnerNameTitle", xmlElement.Attributes["OwnerNameTitle"].Value);
                    if (xmlElement.HasAttribute("OwnerPhone"))
                        htParams.Add("@OwnerPhone", xmlElement.Attributes["OwnerPhone"].Value);
                    if (xmlElement.HasAttribute("OwnerPhoneExt"))
                        htParams.Add("@OwnerPhoneExt", xmlElement.Attributes["OwnerPhoneExt"].Value);
                    if (xmlElement.HasAttribute("OwnerTaxId"))
                        htParams.Add("@OwnerTaxId", xmlElement.Attributes["OwnerTaxId"].Value);
                    if (xmlElement.HasAttribute("PropertyAddress1"))
                        htParams.Add("@PropertyAddress1", xmlElement.Attributes["PropertyAddress1"].Value);
                    if (xmlElement.HasAttribute("PropertyAddress2"))
                        htParams.Add("@PropertyAddress2", xmlElement.Attributes["PropertyAddress2"].Value);
                    if (xmlElement.HasAttribute("PropertyAddressCity"))
                        htParams.Add("@PropertyAddressCity", xmlElement.Attributes["PropertyAddressCity"].Value);
                    if (xmlElement.HasAttribute("PropertyAddressState"))
                        htParams.Add("@PropertyAddressState", xmlElement.Attributes["PropertyAddressState"].Value);
                    if (xmlElement.HasAttribute("PropertyAddressZip"))
                        htParams.Add("@PropertyAddressZip", xmlElement.Attributes["PropertyAddressZip"].Value);
                    if (xmlElement.HasAttribute("PropertyDescription"))
                        htParams.Add("@PropertyDescription", xmlElement.Attributes["PropertyDescription"].Value);
                    if (xmlElement.HasAttribute("PropertyName"))
                        htParams.Add("@PropertyName", xmlElement.Attributes["PropertyName"].Value);
                    if (xmlElement.HasAttribute("Remarks"))
                        htParams.Add("@Remarks", xmlElement.Attributes["Remarks"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htParams;
        }

        /// <summary>
        /// Load the parameters to the FNOLLoadInvolved
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <returns></returns>
        private Hashtable SetFNOLLoadInvolved(XmlElement xmlElement)
        {
            Hashtable htParams = null;
            try
            {
                htParams = new Hashtable();
                if (htParams != null)
                {
                    if (xmlElement.HasAttribute("LynxID"))
                        htParams.Add("@LynxID", xmlElement.Attributes["LynxID"].Value);
                    if (xmlElement.HasAttribute("VehicleNumber"))
                        htParams.Add("@VehicleNumber", xmlElement.Attributes["VehicleNumber"].Value);
                    if (xmlElement.HasAttribute("Address1"))
                        htParams.Add("@Address1", xmlElement.Attributes["Address1"].Value);
                    if (xmlElement.HasAttribute("Address2"))
                        htParams.Add("@Address2", xmlElement.Attributes["Address2"].Value);
                    if (xmlElement.HasAttribute("AddressCity"))
                        htParams.Add("@AddressCity", xmlElement.Attributes["AddressCity"].Value);
                    if (xmlElement.HasAttribute("AddressState"))
                        htParams.Add("@AddressState", xmlElement.Attributes["AddressState"].Value);
                    if (xmlElement.HasAttribute("AddressZip"))
                        htParams.Add("@AddressZip", xmlElement.Attributes["AddressZip"].Value);
                    if (xmlElement.HasAttribute("Age"))
                        htParams.Add("@Age", xmlElement.Attributes["Age"].Value);
                    if (xmlElement.HasAttribute("AttorneyAddress1"))
                        htParams.Add("@AttorneyAddress1", xmlElement.Attributes["AttorneyAddress1"].Value);
                    if (xmlElement.HasAttribute("AttorneyAddress2"))
                        htParams.Add("@AttorneyAddress2", xmlElement.Attributes["AttorneyAddress2"].Value);
                    if (xmlElement.HasAttribute("AttorneyAddressCity"))
                        htParams.Add("@AttorneyAddressCity", xmlElement.Attributes["AttorneyAddressCity"].Value);
                    if (xmlElement.HasAttribute("AttorneyAddressState"))
                        htParams.Add("@AttorneyAddressState", xmlElement.Attributes["AttorneyAddressState"].Value);
                    if (xmlElement.HasAttribute("AttorneyAddressZip"))
                        htParams.Add("@AttorneyAddressZip", xmlElement.Attributes["AttorneyAddressZip"].Value);
                    if (xmlElement.HasAttribute("AttorneyName"))
                        htParams.Add("@AttorneyName", xmlElement.Attributes["AttorneyName"].Value);
                    if (xmlElement.HasAttribute("AttorneyPhone"))
                        htParams.Add("@AttorneyPhone", xmlElement.Attributes["AttorneyPhone"].Value);
                    if (xmlElement.HasAttribute("AttorneyPhoneExt"))
                        htParams.Add("@AttorneyPhoneExt", xmlElement.Attributes["AttorneyPhoneExt"].Value);
                    if (xmlElement.HasAttribute("BestPhoneCode"))
                        htParams.Add("@BestPhoneCode", xmlElement.Attributes["BestPhoneCode"].Value);
                    if (xmlElement.HasAttribute("BestTimeToCall"))
                        htParams.Add("@BestTimeToCall", xmlElement.Attributes["BestTimeToCall"].Value);
                    if (xmlElement.HasAttribute("BusinessName"))
                        htParams.Add("@BusinessName", xmlElement.Attributes["BusinessName"].Value);
                    if (xmlElement.HasAttribute("DateOfBirth"))
                        htParams.Add("@DateOfBirth", xmlElement.Attributes["DateOfBirth"].Value);
                    if (xmlElement.HasAttribute("DoctorAddress1"))
                        htParams.Add("@DoctorAddress1", xmlElement.Attributes["DoctorAddress1"].Value);
                    if (xmlElement.HasAttribute("DoctorAddress2"))
                        htParams.Add("@DoctorAddress2", xmlElement.Attributes["DoctorAddress2"].Value);
                    if (xmlElement.HasAttribute("DoctorAddressCity"))
                        htParams.Add("@DoctorAddressCity", xmlElement.Attributes["DoctorAddressCity"].Value);
                    if (xmlElement.HasAttribute("DoctorAddressState"))
                        htParams.Add("@DoctorAddressState", xmlElement.Attributes["DoctorAddressState"].Value);
                    if (xmlElement.HasAttribute("DoctorAddressZip"))
                        htParams.Add("@DoctorAddressZip", xmlElement.Attributes["DoctorAddressZip"].Value);
                    if (xmlElement.HasAttribute("DoctorName"))
                        htParams.Add("@DoctorName", xmlElement.Attributes["DoctorName"].Value);
                    if (xmlElement.HasAttribute("DoctorPhone"))
                        htParams.Add("@DoctorPhone", xmlElement.Attributes["DoctorPhone"].Value);
                    if (xmlElement.HasAttribute("DoctorPhoneExt"))
                        htParams.Add("@DoctorPhoneExt", xmlElement.Attributes["DoctorPhoneExt"].Value);
                    if (xmlElement.HasAttribute("DriverLicenseNumber"))
                        htParams.Add("@DriverLicenseNumber", xmlElement.Attributes["DriverLicenseNumber"].Value);
                    if (xmlElement.HasAttribute("DriverLicenseState"))
                        htParams.Add("@DriverLicenseState", xmlElement.Attributes["DriverLicenseState"].Value);
                    if (xmlElement.HasAttribute("EmployerAddress1"))
                        htParams.Add("@EmployerAddress1", xmlElement.Attributes["EmployerAddress1"].Value);
                    if (xmlElement.HasAttribute("EmployerAddress2"))
                        htParams.Add("@EmployerAddress2", xmlElement.Attributes["EmployerAddress2"].Value);
                    if (xmlElement.HasAttribute("EmployerAddressCity"))
                        htParams.Add("@EmployerAddressCity", xmlElement.Attributes["EmployerAddressCity"].Value);
                    if (xmlElement.HasAttribute("EmployerAddressState"))
                        htParams.Add("@EmployerAddressState", xmlElement.Attributes["EmployerAddressState"].Value);
                    if (xmlElement.HasAttribute("EmployerAddressZip"))
                        htParams.Add("@EmployerAddressZip", xmlElement.Attributes["EmployerAddressZip"].Value);
                    if (xmlElement.HasAttribute("EmployerName"))
                        htParams.Add("@EmployerName", xmlElement.Attributes["EmployerName"].Value);
                    if (xmlElement.HasAttribute("EmployerPhone"))
                        htParams.Add("@EmployerPhone", xmlElement.Attributes["EmployerPhone"].Value);
                    if (xmlElement.HasAttribute("EmployerPhoneExt"))
                        htParams.Add("@EmployerPhoneExt", xmlElement.Attributes["EmployerPhoneExt"].Value);
                    if (xmlElement.HasAttribute("Gender"))
                        htParams.Add("@Gender", xmlElement.Attributes["Gender"].Value);
                    if (xmlElement.HasAttribute("Injured"))
                        htParams.Add("@Injured", xmlElement.Attributes["Injured"].Value);
                    if (xmlElement.HasAttribute("InjuryDescription"))
                        htParams.Add("@InjuryDescription", xmlElement.Attributes["InjuryDescription"].Value);
                    if (xmlElement.HasAttribute("InjuryTypeId"))
                        htParams.Add("@InjuryTypeId", xmlElement.Attributes["InjuryTypeId"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypeClaimant"))
                        htParams.Add("@InvolvedTypeClaimant", xmlElement.Attributes["InvolvedTypeClaimant"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypeDriver"))
                        htParams.Add("@InvolvedTypeDriver", xmlElement.Attributes["InvolvedTypeDriver"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypeInsured"))
                        htParams.Add("@InvolvedTypeInsured", xmlElement.Attributes["InvolvedTypeInsured"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypeOwner"))
                        htParams.Add("@InvolvedTypeOwner", xmlElement.Attributes["InvolvedTypeOwner"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypePassenger"))
                        htParams.Add("@InvolvedTypePassenger", xmlElement.Attributes["InvolvedTypePassenger"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypePedestrian"))
                        htParams.Add("@InvolvedTypePedestrian", xmlElement.Attributes["InvolvedTypePedestrian"].Value);
                    if (xmlElement.HasAttribute("InvolvedTypeWitness"))
                        htParams.Add("@InvolvedTypeWitness", xmlElement.Attributes["InvolvedTypeWitness"].Value);
                    if (xmlElement.HasAttribute("WitnessLocation"))
                        htParams.Add("@WitnessLocation", xmlElement.Attributes["WitnessLocation"].Value);
                    if (xmlElement.HasAttribute("LocationInVehicleId"))
                        htParams.Add("@LocationInVehicleId", xmlElement.Attributes["LocationInVehicleId"].Value);
                    if (xmlElement.HasAttribute("NameFirst"))
                        htParams.Add("@NameFirst", xmlElement.Attributes["NameFirst"].Value);
                    if (xmlElement.HasAttribute("NameLast"))
                        htParams.Add("@NameLast", xmlElement.Attributes["NameLast"].Value);
                    if (xmlElement.HasAttribute("NameTitle"))
                        htParams.Add("@NameTitle", xmlElement.Attributes["NameTitle"].Value);
                    if (xmlElement.HasAttribute("PhoneAlternate"))
                        htParams.Add("@PhoneAlternate", xmlElement.Attributes["PhoneAlternate"].Value);
                    if (xmlElement.HasAttribute("PhoneAlternateExt"))
                        htParams.Add("@PhoneAlternateExt", xmlElement.Attributes["PhoneAlternateExt"].Value);
                    if (xmlElement.HasAttribute("PhoneDay"))
                        htParams.Add("@PhoneDay", xmlElement.Attributes["PhoneDay"].Value);
                    if (xmlElement.HasAttribute("PhoneDayExt"))
                        htParams.Add("@PhoneDayExt", xmlElement.Attributes["PhoneDayExt"].Value);
                    if (xmlElement.HasAttribute("PhoneNight"))
                        htParams.Add("@PhoneNight", xmlElement.Attributes["PhoneNight"].Value);
                    if (xmlElement.HasAttribute("PhoneNightExt"))
                        htParams.Add("@PhoneNightExt", xmlElement.Attributes["PhoneNightExt"].Value);
                    if (xmlElement.HasAttribute("SeatBelt"))
                        htParams.Add("@SeatBelt", xmlElement.Attributes["SeatBelt"].Value);
                    if (xmlElement.HasAttribute("TaxID"))
                        htParams.Add("@TaxID", xmlElement.Attributes["TaxID"].Value);
                    if (xmlElement.HasAttribute("ThirdPartyInsuranceName"))
                        htParams.Add("@ThirdPartyInsuranceName", xmlElement.Attributes["ThirdPartyInsuranceName"].Value);
                    if (xmlElement.HasAttribute("ThirdPartyInsurancePhone"))
                        htParams.Add("@ThirdPartyInsurancePhone", xmlElement.Attributes["ThirdPartyInsurancePhone"].Value);
                    if (xmlElement.HasAttribute("ThirdPartyInsurancePhoneExt"))
                        htParams.Add("@ThirdPartyInsurancePhoneExt", xmlElement.Attributes["ThirdPartyInsurancePhoneExt"].Value);
                    if (xmlElement.HasAttribute("Violation"))
                        htParams.Add("@Violation", xmlElement.Attributes["Violation"].Value);
                    if (xmlElement.HasAttribute("ViolationDescription"))
                        htParams.Add("@ViolationDescription", xmlElement.Attributes["ViolationDescription"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htParams;
        }

        /// <summary>
        /// Load the parameters to the LoadClaimVehicle
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <returns></returns>
        private Hashtable SetFNOLLoadClaimVehicle(XmlElement xmlElement)
        {
            Hashtable htParams = null;
            try
            {
                htParams = new Hashtable();

                if (htParams != null)
                {
                    if (xmlElement.HasAttribute("LynxID"))
                        htParams.Add("@LynxID", xmlElement.Attributes["LynxID"].Value);
                    if (xmlElement.HasAttribute("VehicleNumber"))
                        htParams.Add("@VehicleNumber", xmlElement.Attributes["VehicleNumber"].Value);
                    if (xmlElement.HasAttribute("AirBagDriverFront"))
                        htParams.Add("@AirBagDriverFront", xmlElement.Attributes["AirBagDriverFront"].Value);
                    if (xmlElement.HasAttribute("AirBagDriverSide"))
                        htParams.Add("@AirBagDriverSide", xmlElement.Attributes["AirBagDriverSide"].Value);
                    if (xmlElement.HasAttribute("AirBagHeadliner"))
                        htParams.Add("@AirBagHeadliner", xmlElement.Attributes["AirBagHeadliner"].Value);
                    if (xmlElement.HasAttribute("AirBagPassengerFront"))
                        htParams.Add("@AirBagPassengerFront", xmlElement.Attributes["AirBagPassengerFront"].Value);
                    if (xmlElement.HasAttribute("AirBagPassengerSide"))
                        htParams.Add("@AirBagPassengerSide", xmlElement.Attributes["AirBagPassengerSide"].Value);
                    if (xmlElement.HasAttribute("AssignmentTypeID"))
                        htParams.Add("@AssignmentTypeID", xmlElement.Attributes["AssignmentTypeID"].Value);
                    if (xmlElement.HasAttribute("BodyStyle"))
                        htParams.Add("@BodyStyle", xmlElement.Attributes["BodyStyle"].Value);
                    if (xmlElement.HasAttribute("ClientCoverageTypeID"))
                        htParams.Add("@ClientCoverageTypeID", xmlElement.Attributes["ClientCoverageTypeID"].Value);
                    if (xmlElement.HasAttribute("Color"))
                        htParams.Add("@Color", xmlElement.Attributes["Color"].Value);
                    if (xmlElement.HasAttribute("ContactAddress1"))
                        htParams.Add("@ContactAddress1", xmlElement.Attributes["ContactAddress1"].Value);
                    if (xmlElement.HasAttribute("ContactAddress2"))
                        htParams.Add("@ContactAddress2", xmlElement.Attributes["ContactAddress2"].Value);
                    if (xmlElement.HasAttribute("ContactAddressCity"))
                        htParams.Add("@ContactAddressCity", xmlElement.Attributes["ContactAddressCity"].Value);
                    if (xmlElement.HasAttribute("ContactAddressState"))
                        htParams.Add("@ContactAddressState", xmlElement.Attributes["ContactAddressState"].Value);
                    if (xmlElement.HasAttribute("ContactAddressZip"))
                        htParams.Add("@ContactAddressZip", xmlElement.Attributes["ContactAddressZip"].Value);
                    if (xmlElement.HasAttribute("ContactAltPhone"))
                        htParams.Add("@ContactAltPhone", xmlElement.Attributes["ContactAltPhone"].Value);
                    if (xmlElement.HasAttribute("ContactAltPhoneExt"))
                        htParams.Add("@ContactAltPhoneExt", xmlElement.Attributes["ContactAltPhoneExt"].Value);
                    if (xmlElement.HasAttribute("ContactBestPhoneCD"))
                        htParams.Add("@ContactBestPhoneCD", xmlElement.Attributes["ContactBestPhoneCD"].Value);
                    if (xmlElement.HasAttribute("ContactNameFirst"))
                        htParams.Add("@ContactNameFirst", xmlElement.Attributes["ContactNameFirst"].Value);
                    if (xmlElement.HasAttribute("ContactNameLast"))
                        htParams.Add("@ContactNameLast", xmlElement.Attributes["ContactNameLast"].Value);
                    if (xmlElement.HasAttribute("ContactNameTitle"))
                        htParams.Add("@ContactNameTitle", xmlElement.Attributes["ContactNameTitle"].Value);
                    if (xmlElement.HasAttribute("ContactNightPhone"))
                        htParams.Add("@ContactNightPhone", xmlElement.Attributes["ContactNightPhone"].Value);
                    if (xmlElement.HasAttribute("ContactNightPhoneExt"))
                        htParams.Add("@ContactNightPhoneExt", xmlElement.Attributes["ContactNightPhoneExt"].Value);
                    if (xmlElement.HasAttribute("ContactPhone"))
                        htParams.Add("@ContactPhone", xmlElement.Attributes["ContactPhone"].Value);
                    if (xmlElement.HasAttribute("ContactPhoneExt"))
                        htParams.Add("@ContactPhoneExt", xmlElement.Attributes["ContactPhoneExt"].Value);
                    if (xmlElement.HasAttribute("CoverageProfileCD"))
                        htParams.Add("@CoverageProfileCD", xmlElement.Attributes["CoverageProfileCD"].Value);
                    if (xmlElement.HasAttribute("Drivable"))
                        htParams.Add("@Drivable", xmlElement.Attributes["Drivable"].Value);
                    if (xmlElement.HasAttribute("ExposureCD"))
                        htParams.Add("@ExposureCD", xmlElement.Attributes["ExposureCD"].Value);
                    if (xmlElement.HasAttribute("GlassDamageFlag"))
                        htParams.Add("@GlassDamageFlag", xmlElement.Attributes["GlassDamageFlag"].Value);
                    if (xmlElement.HasAttribute("ImpactLocations"))
                        htParams.Add("@ImpactLocations", xmlElement.Attributes["ImpactLocations"].Value);
                    if (xmlElement.HasAttribute("ImpactSpeed"))
                        htParams.Add("@ImpactSpeed", xmlElement.Attributes["ImpactSpeed"].Value);
                    if (xmlElement.HasAttribute("LicensePlateNumber"))
                        htParams.Add("@LicensePlateNumber", xmlElement.Attributes["LicensePlateNumber"].Value);
                    if (xmlElement.HasAttribute("LicensePlateState"))
                        htParams.Add("@LicensePlateState", xmlElement.Attributes["LicensePlateState"].Value);
                    if (xmlElement.HasAttribute("LocationAddress1"))
                        htParams.Add("@LocationAddress1", xmlElement.Attributes["LocationAddress1"].Value);
                    if (xmlElement.HasAttribute("LocationAddress2"))
                        htParams.Add("@LocationAddress2", xmlElement.Attributes["LocationAddress2"].Value);
                    if (xmlElement.HasAttribute("LocationAddressCity"))
                        htParams.Add("@LocationAddressCity", xmlElement.Attributes["LocationAddressCity"].Value);
                    if (xmlElement.HasAttribute("LocationName"))
                        htParams.Add("@LocationName", xmlElement.Attributes["LocationName"].Value);
                    if (xmlElement.HasAttribute("LocationPhone"))
                        htParams.Add("@LocationPhone", xmlElement.Attributes["LocationPhone"].Value);
                    if (xmlElement.HasAttribute("LocationAddressState"))
                        htParams.Add("@LocationAddressState", xmlElement.Attributes["LocationAddressState"].Value);
                    if (xmlElement.HasAttribute("LocationAddressZip"))
                        htParams.Add("@LocationAddressZip", xmlElement.Attributes["LocationAddressZip"].Value);
                    if (xmlElement.HasAttribute("Make"))
                        htParams.Add("@Make", xmlElement.Attributes["Make"].Value);
                    if (xmlElement.HasAttribute("Mileage"))
                        htParams.Add("@Mileage", xmlElement.Attributes["Mileage"].Value);
                    if (xmlElement.HasAttribute("Model"))
                        htParams.Add("@Model", xmlElement.Attributes["Model"].Value);
                    if (xmlElement.HasAttribute("NADAId"))
                        htParams.Add("@NADAId", xmlElement.Attributes["NADAId"].Value);
                    if (xmlElement.HasAttribute("PermissionToDrive"))
                        htParams.Add("@PermissionToDrive", xmlElement.Attributes["PermissionToDrive"].Value);
                    if (xmlElement.HasAttribute("PhysicalDamageFlag"))
                        htParams.Add("@PhysicalDamageFlag", xmlElement.Attributes["PhysicalDamageFlag"].Value);
                    if (xmlElement.HasAttribute("PostedSpeed"))
                        htParams.Add("@PostedSpeed", xmlElement.Attributes["PostedSpeed"].Value);
                    if (xmlElement.HasAttribute("PriorDamage"))
                        htParams.Add("@PriorDamage", xmlElement.Attributes["PriorDamage"].Value);
                    if (xmlElement.HasAttribute("PriorityFlag"))
                        htParams.Add("@PriorityFlag", xmlElement.Attributes["PriorityFlag"].Value);
                    if (xmlElement.HasAttribute("Remarks"))
                        htParams.Add("@Remarks", xmlElement.Attributes["Remarks"].Value);
                    if (xmlElement.HasAttribute("RentalDaysAuthorized"))
                        htParams.Add("@RentalDaysAuthorized", xmlElement.Attributes["RentalDaysAuthorized"].Value);
                    if (xmlElement.HasAttribute("RentalInstructions"))
                        htParams.Add("@RentalInstructions", xmlElement.Attributes["RentalInstructions"].Value);
                    if (xmlElement.HasAttribute("RepairLocationCity"))
                        htParams.Add("@RepairLocationCity", xmlElement.Attributes["RepairLocationCity"].Value);
                    if (xmlElement.HasAttribute("RepairLocationState"))
                        htParams.Add("@RepairLocationState", xmlElement.Attributes["RepairLocationState"].Value);
                    if (xmlElement.HasAttribute("RepairLocationCounty"))
                        htParams.Add("@RepairLocationCounty", xmlElement.Attributes["RepairLocationCounty"].Value);
                    if (xmlElement.HasAttribute("SelectedShopRank"))
                        htParams.Add("@SelectedShopRank", xmlElement.Attributes["SelectedShopRank"].Value);
                    if (xmlElement.HasAttribute("SelectedShopScore"))
                        htParams.Add("@SelectedShopScore", xmlElement.Attributes["SelectedShopScore"].Value);
                    if (xmlElement.HasAttribute("ShopLocationID"))
                        htParams.Add("@ShopLocationID", xmlElement.Attributes["ShopLocationID"].Value);
                    if (xmlElement.HasAttribute("ShopRemarks"))
                        htParams.Add("@ShopRemarks", xmlElement.Attributes["ShopRemarks"].Value);
                    if (xmlElement.HasAttribute("ShopSearchLogID"))
                        htParams.Add("@ShopSearchLogID", xmlElement.Attributes["ShopSearchLogID"].Value);
                    if (xmlElement.HasAttribute("TitleName"))
                        htParams.Add("@TitleName", xmlElement.Attributes["TitleName"].Value);
                    if (xmlElement.HasAttribute("TitleState"))
                        htParams.Add("@TitleState", xmlElement.Attributes["TitleState"].Value);
                    if (xmlElement.HasAttribute("TitleStatus"))
                        htParams.Add("@TitleStatus", xmlElement.Attributes["TitleStatus"].Value);
                    if (xmlElement.HasAttribute("VehicleYear"))
                        htParams.Add("@VehicleYear", xmlElement.Attributes["VehicleYear"].Value);
                    if (xmlElement.HasAttribute("VIN"))
                        htParams.Add("@Vin", xmlElement.Attributes["VIN"].Value);
                    if (xmlElement.HasAttribute("SourceApplicationPassthruDataVeh"))
                        htParams.Add("@SourceApplicationPassthruDataVeh", xmlElement.Attributes["SourceApplicationPassthruDataVeh"].Value);
                    if (xmlElement.HasAttribute("PrefMethodUpd"))
                        htParams.Add("@PrefMethodUpd", xmlElement.Attributes["PrefMethodUpd"].Value);
                    if (xmlElement.HasAttribute("CellPhoneCarrier"))
                        htParams.Add("@CellPhoneCarrier", xmlElement.Attributes["CellPhoneCarrier"].Value);
                    if (xmlElement.HasAttribute("ContactCellPhone"))
                        htParams.Add("@ContactCellPhone", xmlElement.Attributes["ContactCellPhone"].Value);
                    if (xmlElement.HasAttribute("ContactEmailAddress"))
                        htParams.Add("@ContactEmailAddress", xmlElement.Attributes["ContactEmailAddress"].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htParams;
        }

        /// <summary>
        /// Load the parameters for the LoadClaimCoverage
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <returns></returns>
        private Hashtable SetFNOLLoadClaimCoverage(XmlElement xmlElement)
        {
            Hashtable htParams = null;
            try
            {
                htParams = new Hashtable();
                if (xmlElement.HasAttribute("LynxID"))
                    htParams.Add("@LynxID", xmlElement.Attributes["LynxID"].Value);
                if (xmlElement.HasAttribute("ClientCode"))
                    htParams.Add("@ClientCode", xmlElement.Attributes["ClientCode"].Value);
                if (xmlElement.HasAttribute("ClientCoverageTypeID"))
                    htParams.Add("@ClientCoverageTypeID", xmlElement.Attributes["ClientCoverageTypeID"].Value);
                if (xmlElement.HasAttribute("CoverageTypeCD"))
                    htParams.Add("@CoverageTypeCD", xmlElement.Attributes["CoverageTypeCD"].Value);
                if (xmlElement.HasAttribute("DeductibleAmt"))
                    htParams.Add("@DeductibleAmt", xmlElement.Attributes["DeductibleAmt"].Value);
                if (xmlElement.HasAttribute("LimitAmt"))
                    htParams.Add("@LimitAmt", xmlElement.Attributes["LimitAmt"].Value);
                if (xmlElement.HasAttribute("LimitDailyAmt"))
                    htParams.Add("@LimitDailyAmt", xmlElement.Attributes["LimitDailyAmt"].Value);
                if (xmlElement.HasAttribute("MaximumDays"))
                    htParams.Add("@MaximumDays", xmlElement.Attributes["MaximumDays"].Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htParams;
        }

        /// <summary>
        /// Load the parameters to the FnolClearLoadTables
        /// </summary>
        /// <param name="LynxID"></param>
        /// <returns></returns>
        private Hashtable SetFNOLClearLoadTables(long LynxID)
        {
            Hashtable htFnolLoadClaim = null;
            try
            {
                htFnolLoadClaim = new Hashtable();
                htFnolLoadClaim.Add("@LynxID", LynxID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htFnolLoadClaim;
        }

        /// <summary>
        /// Load the parameters for uspFNOLLoadClaim SP
        /// </summary>
        /// <param name="xmlElementParam"></param>
        /// <returns></returns>
        private Hashtable SetFNOLLoadClaimSPParam(XmlElement xmlElementParam)
        {
            Hashtable htFNOLParams = null;
            try
            {
                htFNOLParams = new Hashtable();

                htFNOLParams.Add("@LynxID", xmlElementParam.Attributes["LynxID"].Value);
                if (xmlElementParam.HasAttribute("AdvanceAmount"))
                    htFNOLParams.Add("@AdvanceAmount", xmlElementParam.Attributes["AdvanceAmount"].Value);
                if (xmlElementParam.HasAttribute("AgentName"))
                    htFNOLParams.Add("@AgentName", xmlElementParam.Attributes["AgentName"].Value);
                if (xmlElementParam.HasAttribute("AgentPhone"))
                    htFNOLParams.Add("@AgentPhone", xmlElementParam.Attributes["AgentPhone"].Value);
                if (xmlElementParam.HasAttribute("AgentExt"))
                    htFNOLParams.Add("@AgentExt", xmlElementParam.Attributes["AgentExt"].Value);
                if (xmlElementParam.HasAttribute("CallerAddress1"))
                    htFNOLParams.Add("@CallerAddress1", xmlElementParam.Attributes["CallerAddress1"].Value);
                if (xmlElementParam.HasAttribute("CallerAddress2"))
                    htFNOLParams.Add("@CallerAddress2", xmlElementParam.Attributes["CallerAddress2"].Value);
                if (xmlElementParam.HasAttribute("CallerAddressCity"))
                    htFNOLParams.Add("@CallerAddressCity", xmlElementParam.Attributes["CallerAddressCity"].Value);
                if (xmlElementParam.HasAttribute("CallerAddressState"))
                    htFNOLParams.Add("@CallerAddressState", xmlElementParam.Attributes["CallerAddressState"].Value);
                if (xmlElementParam.HasAttribute("CallerAddressZip"))
                    htFNOLParams.Add("@CallerAddressZip", xmlElementParam.Attributes["CallerAddressZip"].Value);
                if (xmlElementParam.HasAttribute("CallerBestPhoneCode"))
                    htFNOLParams.Add("@CallerBestPhoneCode", xmlElementParam.Attributes["CallerBestPhoneCode"].Value);
                if (xmlElementParam.HasAttribute("CallerBestTimeToCall"))
                    htFNOLParams.Add("@CallerBestTimeToCall", xmlElementParam.Attributes["CallerBestTimeToCall"].Value);
                if (xmlElementParam.HasAttribute("CallerNameFirst"))
                    htFNOLParams.Add("@CallerNameFirst", xmlElementParam.Attributes["CallerNameFirst"].Value);
                if (xmlElementParam.HasAttribute("CallerNameLast"))
                    htFNOLParams.Add("@CallerNameLast", xmlElementParam.Attributes["CallerNameLast"].Value);
                if (xmlElementParam.HasAttribute("CallerNameTitle"))
                    htFNOLParams.Add("@CallerNameTitle", xmlElementParam.Attributes["CallerNameTitle"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneAlternate"))
                    htFNOLParams.Add("@CallerPhoneAlternate", xmlElementParam.Attributes["CallerPhoneAlternate"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneAlternateExt"))
                    htFNOLParams.Add("@CallerPhoneAlternateExt", xmlElementParam.Attributes["CallerPhoneAlternateExt"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneDay"))
                    htFNOLParams.Add("@CallerPhoneDay", xmlElementParam.Attributes["CallerPhoneDay"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneDayExt"))
                    htFNOLParams.Add("@CallerPhoneDayExt", xmlElementParam.Attributes["CallerPhoneDayExt"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneNight"))
                    htFNOLParams.Add("@CallerPhoneNight", xmlElementParam.Attributes["CallerPhoneNight"].Value);
                if (xmlElementParam.HasAttribute("CallerPhoneNightExt"))
                    htFNOLParams.Add("@CallerPhoneNightExt", xmlElementParam.Attributes["CallerPhoneNightExt"].Value);
                if (xmlElementParam.HasAttribute("CallerRelationToInsuredID"))
                    htFNOLParams.Add("@CallerRelationToInsuredID", xmlElementParam.Attributes["CallerRelationToInsuredID"].Value);
                if (xmlElementParam.HasAttribute("CarrierRepUserID"))
                    htFNOLParams.Add("@CarrierRepUserID", xmlElementParam.Attributes["CarrierRepUserID"].Value);
                if (xmlElementParam.HasAttribute("ContactAddress1"))
                    htFNOLParams.Add("@ContactAddress1", xmlElementParam.Attributes["ContactAddress1"].Value);
                if (xmlElementParam.HasAttribute("ContactAddress2"))
                    htFNOLParams.Add("@ContactAddress2", xmlElementParam.Attributes["ContactAddress2"].Value);
                if (xmlElementParam.HasAttribute("ContactAddressCity"))
                    htFNOLParams.Add("@ContactAddressCity", xmlElementParam.Attributes["ContactAddressCity"].Value);
                if (xmlElementParam.HasAttribute("ContactAddressState"))
                    htFNOLParams.Add("@ContactAddressState", xmlElementParam.Attributes["ContactAddressState"].Value);
                if (xmlElementParam.HasAttribute("ContactAddressZip"))
                    htFNOLParams.Add("@ContactAddressZip", xmlElementParam.Attributes["ContactAddressZip"].Value);
                if (xmlElementParam.HasAttribute("ContactBestPhoneCode"))
                    htFNOLParams.Add("@ContactBestPhoneCode", xmlElementParam.Attributes["ContactBestPhoneCode"].Value);
                if (xmlElementParam.HasAttribute("ContactBestTimeToCall"))
                    htFNOLParams.Add("@ContactBestTimeToCall", xmlElementParam.Attributes["ContactBestTimeToCall"].Value);
                if (xmlElementParam.HasAttribute("ContactEmailAddress"))
                    htFNOLParams.Add("@ContactEmailAddress", xmlElementParam.Attributes["ContactEmailAddress"].Value);
                if (xmlElementParam.HasAttribute("ContactNameFirst"))
                    htFNOLParams.Add("@ContactNameFirst", xmlElementParam.Attributes["ContactNameFirst"].Value);
                if (xmlElementParam.HasAttribute("ContactNameLast"))
                    htFNOLParams.Add("@ContactNameLast", xmlElementParam.Attributes["ContactNameLast"].Value);
                if (xmlElementParam.HasAttribute("ContactNameTitle"))
                    htFNOLParams.Add("@ContactNameTitle", xmlElementParam.Attributes["ContactNameTitle"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneAlternate"))
                    htFNOLParams.Add("@ContactPhoneAlternate", xmlElementParam.Attributes["ContactPhoneAlternate"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneAlternateExt"))
                    htFNOLParams.Add("@ContactPhoneAlternateExt", xmlElementParam.Attributes["ContactPhoneAlternateExt"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneDay"))
                    htFNOLParams.Add("@ContactPhoneDay", xmlElementParam.Attributes["ContactPhoneDay"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneDayExt"))
                    htFNOLParams.Add("@ContactPhoneDayExt", xmlElementParam.Attributes["ContactPhoneDayExt"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneNight"))
                    htFNOLParams.Add("@ContactPhoneNight", xmlElementParam.Attributes["ContactPhoneNight"].Value);
                if (xmlElementParam.HasAttribute("ContactPhoneNightExt"))
                    htFNOLParams.Add("@ContactPhoneNightExt", xmlElementParam.Attributes["ContactPhoneNightExt"].Value);
                if (xmlElementParam.HasAttribute("ContactRelationToInsuredID"))
                    htFNOLParams.Add("@ContactRelationToInsuredID", xmlElementParam.Attributes["ContactRelationToInsuredID"].Value);
                if (xmlElementParam.HasAttribute("CoverageClaimNumber"))
                    htFNOLParams.Add("@CoverageClaimNumber", xmlElementParam.Attributes["CoverageClaimNumber"].Value);
                if (xmlElementParam.HasAttribute("DemoFlag"))
                    htFNOLParams.Add("@DemoFlag", xmlElementParam.Attributes["DemoFlag"].Value);
                if (xmlElementParam.HasAttribute("FNOLUserID"))
                    htFNOLParams.Add("@FNOLUserID", xmlElementParam.Attributes["FNOLUserID"].Value);
                if (xmlElementParam.HasAttribute("InsuranceCompanyID"))
                    htFNOLParams.Add("@InsuranceCompanyID", xmlElementParam.Attributes["InsuranceCompanyID"].Value);
                if (xmlElementParam.HasAttribute("IntakeSeconds"))
                    htFNOLParams.Add("@IntakeSeconds", xmlElementParam.Attributes["IntakeSeconds"].Value);
                if (xmlElementParam.HasAttribute("LienHolderName"))
                    htFNOLParams.Add("@LienHolderName", xmlElementParam.Attributes["LienHolderName"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAddress1"))
                    htFNOLParams.Add("@LienHolderAddress1", xmlElementParam.Attributes["LienHolderAddress1"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAddress2"))
                    htFNOLParams.Add("@LienHolderAddress2", xmlElementParam.Attributes["LienHolderAddress2"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAddressCity"))
                    htFNOLParams.Add("@LienHolderAddressCity", xmlElementParam.Attributes["LienHolderAddressCity"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAddressState"))
                    htFNOLParams.Add("@LienHolderAddressState", xmlElementParam.Attributes["LienHolderAddressState"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAddressZip"))
                    htFNOLParams.Add("@LienHolderAddressZip", xmlElementParam.Attributes["LienHolderAddressZip"].Value);
                if (xmlElementParam.HasAttribute("LienHolderPhone"))
                    htFNOLParams.Add("@LienHolderPhone", xmlElementParam.Attributes["LienHolderPhone"].Value);
                if (xmlElementParam.HasAttribute("LienHolderFax"))
                    htFNOLParams.Add("@LienHolderFax", xmlElementParam.Attributes["LienHolderFax"].Value);
                if (xmlElementParam.HasAttribute("LienHolderEmail"))
                    htFNOLParams.Add("@LienHolderEmail", xmlElementParam.Attributes["LienHolderEmail"].Value);
                if (xmlElementParam.HasAttribute("LienHolderContactName"))
                    htFNOLParams.Add("@LienHolderContactName", xmlElementParam.Attributes["LienHolderContactName"].Value);
                if (xmlElementParam.HasAttribute("LienHolderPayoffAmount"))
                    htFNOLParams.Add("@LienHolderPayoffAmount", xmlElementParam.Attributes["LienHolderPayoffAmount"].Value);
                if (xmlElementParam.HasAttribute("LienHolderPayoffExpirationDate"))
                    htFNOLParams.Add("@LienHolderPayoffExpirationDate", xmlElementParam.Attributes["LienHolderPayoffExpirationDate"].Value);
                if (xmlElementParam.HasAttribute("LienHolderAccountNumber"))
                    htFNOLParams.Add("@LienHolderAccountNumber", xmlElementParam.Attributes["LienHolderAccountNumber"].Value);
                if (xmlElementParam.HasAttribute("LoGAmount"))
                    htFNOLParams.Add("@LoGAmount", xmlElementParam.Attributes["LoGAmount"].Value);
                if (xmlElementParam.HasAttribute("LossDate"))
                    htFNOLParams.Add("@LossDate", xmlElementParam.Attributes["LossDate"].Value);
                if (xmlElementParam.HasAttribute("LossAddressCity"))
                    htFNOLParams.Add("@LossAddressCity", xmlElementParam.Attributes["LossAddressCity"].Value);
                if (xmlElementParam.HasAttribute("LossAddressCounty"))
                    htFNOLParams.Add("@LossAddressCounty", xmlElementParam.Attributes["LossAddressCounty"].Value);
                if (xmlElementParam.HasAttribute("LossAddressState"))
                    htFNOLParams.Add("@LossAddressState", xmlElementParam.Attributes["LossAddressState"].Value);
                if (xmlElementParam.HasAttribute("LossAddressStreet"))
                    htFNOLParams.Add("@LossAddressStreet", xmlElementParam.Attributes["LossAddressStreet"].Value);
                if (xmlElementParam.HasAttribute("LossAddressZip"))
                    htFNOLParams.Add("@LossAddressZip", xmlElementParam.Attributes["LossAddressZip"].Value);
                if (xmlElementParam.HasAttribute("LossDescription"))
                    htFNOLParams.Add("@LossDescription", xmlElementParam.Attributes["LossDescription"].Value);
                if (xmlElementParam.HasAttribute("LossTime"))
                    htFNOLParams.Add("@LossTime", xmlElementParam.Attributes["LossTime"].Value);
                if (xmlElementParam.HasAttribute("LossTypeLevel1ID"))
                    htFNOLParams.Add("@LossTypeLevel1ID", xmlElementParam.Attributes["LossTypeLevel1ID"].Value);
                if (xmlElementParam.HasAttribute("LossTypeLevel2ID"))
                    htFNOLParams.Add("@LossTypeLevel2ID", xmlElementParam.Attributes["LossTypeLevel2ID"].Value);
                if (xmlElementParam.HasAttribute("LossTypeLevel3ID"))
                    htFNOLParams.Add("@LossTypeLevel3ID", xmlElementParam.Attributes["LossTypeLevel3ID"].Value);
                if (xmlElementParam.HasAttribute("NoticeDate"))
                    htFNOLParams.Add("@NoticeDate", xmlElementParam.Attributes["NoticeDate"].Value);
                if (xmlElementParam.HasAttribute("NoticeMethodID"))
                    htFNOLParams.Add("@NoticeMethodID", xmlElementParam.Attributes["NoticeMethodID"].Value);
                if (xmlElementParam.HasAttribute("PoliceDepartmentName"))
                    htFNOLParams.Add("@PoliceDepartmentName", xmlElementParam.Attributes["PoliceDepartmentName"].Value);
                if (xmlElementParam.HasAttribute("PolicyNumber"))
                    htFNOLParams.Add("@PolicyNumber", xmlElementParam.Attributes["PolicyNumber"].Value);
                if (xmlElementParam.HasAttribute("Remarks"))
                    htFNOLParams.Add("@Remarks", xmlElementParam.Attributes["Remarks"].Value);
                if (xmlElementParam.HasAttribute("RoadLocationID"))
                    htFNOLParams.Add("@RoadLocationID", xmlElementParam.Attributes["RoadLocationID"].Value);
                if (xmlElementParam.HasAttribute("RoadTypeID"))
                    htFNOLParams.Add("@RoadTypeID", xmlElementParam.Attributes["RoadTypeID"].Value);
                if (xmlElementParam.HasAttribute("SalvageName"))
                    htFNOLParams.Add("@SalvageName", xmlElementParam.Attributes["SalvageName"].Value);
                if (xmlElementParam.HasAttribute("SalvageAddress1"))
                    htFNOLParams.Add("@SalvageAddress1", xmlElementParam.Attributes["SalvageAddress1"].Value);
                if (xmlElementParam.HasAttribute("SalvageAddress2"))
                    htFNOLParams.Add("@SalvageAddress2", xmlElementParam.Attributes["SalvageAddress2"].Value);
                if (xmlElementParam.HasAttribute("SalvageAddressCity"))
                    htFNOLParams.Add("@SalvageAddressCity", xmlElementParam.Attributes["SalvageAddressCity"].Value);
                if (xmlElementParam.HasAttribute("SalvageAddressState"))
                    htFNOLParams.Add("@SalvageAddressState", xmlElementParam.Attributes["SalvageAddressState"].Value);
                if (xmlElementParam.HasAttribute("SalvageAddressZip"))
                    htFNOLParams.Add("@SalvageAddressZip", xmlElementParam.Attributes["SalvageAddressZip"].Value);
                if (xmlElementParam.HasAttribute("SalvagePhone"))
                    htFNOLParams.Add("@SalvagePhone", xmlElementParam.Attributes["SalvagePhone"].Value);
                if (xmlElementParam.HasAttribute("SalvageFax"))
                    htFNOLParams.Add("@SalvageFax", xmlElementParam.Attributes["SalvageFax"].Value);
                if (xmlElementParam.HasAttribute("SalvageEmail"))
                    htFNOLParams.Add("@SalvageEmail", xmlElementParam.Attributes["SalvageEmail"].Value);
                if (xmlElementParam.HasAttribute("SalvageContactName"))
                    htFNOLParams.Add("@SalvageContactName", xmlElementParam.Attributes["SalvageContactName"].Value);
                if (xmlElementParam.HasAttribute("SalvageControlNumber"))
                    htFNOLParams.Add("@SalvageControlNumber", xmlElementParam.Attributes["SalvageControlNumber"].Value);
                if (xmlElementParam.HasAttribute("SettlementDate"))
                    htFNOLParams.Add("@SettlementDate", xmlElementParam.Attributes["SettlementDate"].Value);
                if (xmlElementParam.HasAttribute("SettlementAmount"))
                    htFNOLParams.Add("@SettlementAmount", xmlElementParam.Attributes["SettlementAmount"].Value);
                if (xmlElementParam.HasAttribute("SourceApplicationCD"))
                    htFNOLParams.Add("@SourceApplicationCD", xmlElementParam.Attributes["SourceApplicationCD"].Value);
                if (xmlElementParam.HasAttribute("SourceApplicationPassThruData"))
                    htFNOLParams.Add("@SourceApplicationPassThruData", xmlElementParam.Attributes["SourceApplicationPassThruData"].Value);
                if (xmlElementParam.HasAttribute("TripPurpose"))
                    htFNOLParams.Add("@TripPurpose", xmlElementParam.Attributes["TripPurpose"].Value);
                if (xmlElementParam.HasAttribute("WeatherConditionID"))
                    htFNOLParams.Add("@WeatherConditionID", xmlElementParam.Attributes["WeatherConditionID"].Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return htFNOLParams;
        }

        /// <summary>
        /// Generate and save the web assignment report.
        /// Returns the document location of the generated report
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="lngLynxID"></param>
        /// <returns></returns>
        public string SaveWAReport(ref XmlDocument objParamDom, long lngLynxID)
        {
            XmlDocument objWARptConfigDOM = null;
            XmlDocument objUserClaimDOM = null;
            Recordset objRS = null;
            Stream objPDFReport = null;
            XmlElement objFirstVehicleNode = null;
            XmlElement objClaimAspectOwner = null;

            string strCreatedUserID = string.Empty,
             strDocLocation = string.Empty,
             strLoadUserClaim = string.Empty,
             strAssignmentTypeID = string.Empty,
             strOwnerNameFirst = string.Empty,
             strOwnerNameLast = string.Empty,
             strOwnerPhone = string.Empty,
             strOwnerEmailAddress = string.Empty,
             strRSFileName = string.Empty,
             strReportFileName = string.Empty,
             strFileName = string.Empty,
             strFilePath = string.Empty,
             strSqlNow = string.Empty,
              strWAFirstVehicleNumber = string.Empty,
             strWAFirstVehicleVIN = string.Empty,
             strWAInsuranceCompanyID = string.Empty,
             strCurServiceChannelCD = string.Empty,
             strPertainsTo = string.Empty, strDocumentPath = string.Empty,
             strExposure = string.Empty;
            bool blnRaiseToCaller = true;
            int iFailOverCount = 0;
            string strAppend = string.Empty,
                strXmlAppend = string.Empty;
            Hashtable hashTable = null;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                //if (isExtensiveDebug)
                //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "START", string.Concat("EcadAccessor.CWebAssignment.cs - SaveWAReport", "Server Name - ", Environment.MachineName), string.Concat("LYNXID:", Convert.ToString(lngLynxID)), objParamDom.OuterXml);

                objReportUtil = new clsReport();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
                //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);

                if (lngLynxID > 0)
                {
                    //Get the assignment type id from the web assignment
                    strAssignmentTypeID = (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/AssignmentTypeID", true));
                    strWAFirstVehicleNumber = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VehicleNumber", false);
                    strWAFirstVehicleVIN = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VIN", false);
                    strWAInsuranceCompanyID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/InsuranceCompanyID", true);

                    objWARptConfigDOM = new XmlDocument();
                    objUserClaimDOM = new XmlDocument();

                    //Load the Web assignment report config file
                    strAppend = string.Concat(strDocumentPath, "\\WA ReportMap", ".xml");
                    objMDomUtils.LoadXmlFile(ref objWARptConfigDOM, ref strAppend, "", "Web Assignment Report Config");

                    //Get the RS template filename
                    //        strRSFileName = GetChildNodeText(objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" & strAssignmentTypeID & "']/@RS", True)
                    //we are going to use the crystal ttx input format instead of rs. RS is creating multi-step errors when setting the data.
                    // we will build the recordset schema on-the-fly from the ttx.
                    strRSFileName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@TTX", true);

                    //Get the report file name
                    strReportFileName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@RPT", true);

                    //Get the LoadUserClaim setting
                    strLoadUserClaim = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@loadClaimUser", true);

                    objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                    if (strLoadUserClaim == "1")
                    {
                        //Need to find out the claim owner of this web assignment
                        hashTable = new Hashtable();
                        hashTable.Add("@LynxID", lngLynxID);
                        hashTable.Add("@InsuranceCompanyID", strWAInsuranceCompanyID);

                        strXmlAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetVehicleListSP"), hashTable, "APD");

                        objMDomUtils.LoadXml(ref objUserClaimDOM, ref strXmlAppend, "UserClaimDetail", "User Claim Detail");

                        if (strWAFirstVehicleVIN != string.Empty)
                            //choosing the vehicle by VIN will be more accurate than the vehicle number.
                            objFirstVehicleNode = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objUserClaimDOM, "/Root/Vehicle[@VIN='" + strWAFirstVehicleVIN + "']"); //Todo:Verify
                        else
                            // Theoritically the vehicle number passed in will match the claims vehicle number.
                            objFirstVehicleNode = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objUserClaimDOM, "/Root/Vehicle[@VehicleNumber='" + strWAFirstVehicleNumber + "']");

                        if (objFirstVehicleNode != null)
                        {
                            strCurServiceChannelCD = objFirstVehicleNode.GetAttribute("CurrentServiceChannelCD");
                            objClaimAspectOwner = (XmlElement)objFirstVehicleNode.SelectSingleNode("ClaimAspectOwner");
                            if (objClaimAspectOwner != null)
                            {
                                //Extract the Entity Owner Information based on the service channel
                                switch (strCurServiceChannelCD)
                                {
                                    case "PS":
                                    case "ME":
                                    case "TL":
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("UserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("UserNameLast");
                                        strOwnerPhone = string.Concat(objClaimAspectOwner.GetAttribute("UserPhoneAreaCode"), "-",
                                                         objClaimAspectOwner.GetAttribute("UserPhoneExchangeNumber"), "-",
                                                         objClaimAspectOwner.GetAttribute("UserPhoneUnitNumber"));
                                        if (objClaimAspectOwner.GetAttribute("UserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("UserExtensionNumber"));

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("UserEmail");
                                        break;
                                    case "DA":
                                    case "DR":
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("AnalystUserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("AnalystUserNameLast");
                                        strOwnerPhone = string.Concat(objClaimAspectOwner.GetAttribute("AnalystUserPhoneAreaCode"), "-", objClaimAspectOwner.GetAttribute("AnalystUserPhoneExchangeNumber"), "-", objClaimAspectOwner.GetAttribute("AnalystUserPhoneUnitNumber"));
                                        if (objClaimAspectOwner.GetAttribute("AnalystUserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("AnalystUserExtensionNumber"));

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("AnalystUserEmail");
                                        break;
                                    default:
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("SupportUserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("SupportUserNameLast");
                                        strOwnerPhone = string.Concat(objClaimAspectOwner.GetAttribute("SupportUserPhoneAreaCode"), "-", objClaimAspectOwner.GetAttribute("SupportUserPhoneExchangeNumber"), "-", objClaimAspectOwner.GetAttribute("SupportUserPhoneUnitNumber"));
                                        if (objClaimAspectOwner.GetAttribute("SupportUserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("SupportUserExtensionNumber"));

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("SupportUserEmail");
                                        break;
                                }
                            }
                        }

                        objFirstVehicleNode = null;
                    }
                    //Transform the Web Assignment XML to Recordset                    
                    objRS = LoadWAXML2RS(ref objParamDom, lngLynxID, strRSFileName, strOwnerNameFirst, strOwnerNameLast, strOwnerPhone, strOwnerEmailAddress);

                    //check if the recordset is valid
                    if (objRS != null)
                        if (objRS.Fields.Count == 0)
                            objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CWebAssignment.cs - SaveWAReport", string.Concat(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset.ToString(), "SaveWAReport", "Invalid recordset returned while converting Web Assignment to Recordset"), "");


                    //Generate the report file
                    if (objReportUtil != null && objRS != null)
                    {
                        iFailOverCount = 1;
                        //Randomize();                        
                        objPDFReport = objReportUtil.getWAReport(objRS, string.Concat(strDocumentPath, "\\", strReportFileName), "PDF");
                        if (objPDFReport != null)
                        {
                            strFileName = BuildFileName("WA", lngLynxID);
                            if (strFileName != string.Empty)
                            {
                                strFilePath = string.Concat((objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory")).Replace("\\\\", "\\"), strFileName.Replace("\\\\", "\\"));

                                objPDFReport.SaveToFile(strFilePath, SaveOptionsEnum.adSaveCreateNotExist);
                                if (isExtensiveDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SaveWAReport", string.Concat("File saved on the following path =", strFilePath), "");

                                strSqlNow = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

                                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                                // This hack added because VehicleNumber is not part of the FNOL Assignment XML.  This report must be switched to
                                // be produced from APD's standard (post XSLT) XML.
                                if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DataSource") == "FNOL Assignment")
                                {
                                    strExposure = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Vehicle/ExposureCD");
                                    strPertainsTo = string.Concat("Veh", strExposure == "1" ? "1" : "2");
                                }
                                else
                                    strPertainsTo = string.Concat("Veh", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VehicleNumber", true));

                                // Get the created user ID.
                                if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/DataSource", true) == "FNOL Assignment")
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/CarrierRepUserID");

                                if (strCreatedUserID == string.Empty)
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/FNOLUserID", true);
                                else
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/CarrierRepUserID", true);

                                //Attach the document to the first vehicle in the web assignment
                                hashTable = new Hashtable();
                                hashTable.Add("@NTUserID", "SYSTEM");
                                hashTable.Add("@UserID", strCreatedUserID);
                                hashTable.Add("@PertainsTo", strPertainsTo);
                                hashTable.Add("@LynxID", lngLynxID);
                                hashTable.Add("@NotifyEvent", 0);
                                hashTable.Add("@DirectionalCD", "I");
                                hashTable.Add("@CreatedDate", strSqlNow);
                                hashTable.Add("@ImageDate", strSqlNow);
                                hashTable.Add("@ImageLocation", strFileName);
                                hashTable.Add("@DocumentSource", "ClaimPoint");
                                hashTable.Add("@DocumentType", "Client Assignment");
                                hashTable.Add("@ImageType", "PDF");
                                objcDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("PartnerSettings/InsertSP"), hashTable, "APD");
                            }
                        }
                    }
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - SaveWAReport", "Process completed.", "");

               //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "END", string.Concat("EcadAccessor.CWebAssignment.cs - SaveWAReport", "Server Name - ", Environment.MachineName), string.Concat("LYNXID:", Convert.ToString(lngLynxID)), objParamDom.OuterXml);

            
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - SaveWAReport", string.Concat(ex.Message, "Stack Trace: ", ex.StackTrace), objParamDom.OuterXml);
            }
            finally
            {
                objWriteLog = null;
                objWARptConfigDOM = null;
                objUserClaimDOM = null;
                objRS = null;
                objPDFReport = null;
                objFirstVehicleNode = null;
                objClaimAspectOwner = null;
                if (objReportUtil != null)
                    Marshal.ReleaseComObject(objReportUtil);
                objReportUtil = null;
                hashTable = null;
            }
            //Add file name to xml as a root level attribute.
            return strFileName;
        }

        #endregion

        #region Private Functions
        // Create and Destruct globals
        private void Class_Initialize()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns True if we should send an assignment through
        /// CCC to Autoverse to Mitchell for our Auto Techs?
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private bool IsAutoTechAssignment(ref XmlDocument objParamDom)
        {
            int intAssignmentType;
            bool blnAssignmentType = false;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - IsAutoTechAssignment", "Process completed.", "");
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Extract the assignment type.
                intAssignmentType = Convert.ToInt32(objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Vehicle/@AssignmentTypeID", true));

                //Temporary hack - match assignment type up to config settings.
                blnAssignmentType = Convert.ToBoolean("True" == objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignAutoTech/AssignmentType[@ID='" + intAssignmentType + "']/@Enabled"));
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - IsAutoTechAssignment", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return blnAssignmentType;
        }

        /// <summary>
        ///  Get a new LynxID for the assignment.
        /// </summary>
        /// <returns></returns>
        private long GetFnolUID()
        {
            string PROC_NAME = string.Empty;
            string strFNOLUID = string.Empty, strApplication = string.Empty;
            Hashtable htparams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - GetFnolUID", "Process completed.", "");
                PROC_NAME = string.Concat(MODULE_NAME, "GetFnolUID: ");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Get the procedure level list from the APD database.
                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eFNOL_Recordset);

                //strFNOLUID = Convert.ToString(objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetLynxIdSP").ToString(), null));
                strFNOLUID = Convert.ToString(objcDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetLynxIdSP"), htparams, strApplication));

                //if (isExtensiveDebug)
                //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "END", string.Concat("EcadAccessor.CWebAssignment.cs - GetFnolUID", "Server Name - ", Environment.MachineName), string.Concat("Process completed. ID =", strFNOLUID), "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                htparams = null;
            }
            return Convert.ToInt32(strFNOLUID);
        }

        /// <summary>
        /// Store the assignment in the partner database
        /// </summary>
        /// <param name="lngUID"></param>
        /// <param name="strDataSource"></param>
        /// <param name="strXml"></param>
        private void StoreAssignmentXml(long lngUID, string strDataSource, string strXml)
        {
            string strApplication = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - StoreAssignmentXml", "Process completed.", "");

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                htParams = new Hashtable();
                objcDataAccessor = new DataAccess.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Get the procedure level list from the APD database.
                strApplication = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);

                htParams.Add("@LynxID", lngUID);
                htParams.Add("@DataSource", strDataSource);
                htParams.Add("@XML", strXml);

                objcDataAccessor.ExecuteSp(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/StoreAssignmentXmlSP"), htParams, strApplication); //Partner
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - StoreAssignmentXml", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                htParams = null;
            }
        }

        /// <summary>
        /// Gets the procedure level list for the default Client Access insurance company.
        /// </summary>
        private void GetProcedureLevelList()
        {
            string strName = string.Empty,
                strDebug = string.Empty;
            int intIndex = 0;
            int intID;
            typProc tProc;
            Recordset rsRecords = null;
            Hashtable htParams = null;
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMRecordsetUtils = new MRecordsetUtils();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();
                htParams = new Hashtable();
                objMLAPDEcadAccessorMgr.InitializeGlobals();

                rsRecords = new Recordset();
                rsRecords = objcDataAccessor.OpenRecordsetSp(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/ProcedureLevelListSP"), "APD");
                // rsRecords = objMLAPDEcadAccessorMgr.g_objDataAccessor.OpenRecordsetSp(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/ProcedureLevelListSP"), null);

                rsRecords.MoveFirst();
                //  ReDim marrProcList(RS.RecordCount)
                intIndex = 0;

                while (!rsRecords.EOF)
                {
                    intID = Convert.ToInt32(rsRecords.Fields["ProcedureID"]);
                    tProc.intID = intID;
                    tProc.intLevelID = Convert.ToInt32(rsRecords.Fields["ProcedureLevelID"]);
                    tProc.strName = (rsRecords.Fields["ProcName"]).ToString();
                    marrProcList[intIndex] = tProc;
                    rsRecords.MoveNext();
                    intIndex = intIndex + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (rsRecords != null)
                {
                    if (rsRecords.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        rsRecords.Close();
                    rsRecords = null;
                    htParams = null;
                }
            }
        }

        /// <summary>
        /// SendShopAssignments sends shop assignments automatically.
        /// 
        /// The purpose of this method is to make the assignment so that our claims
        /// reps do not have to.  This class used to be rather independant of the XML
        /// passed in, but this method breaks that and ties this class to Vehicle.
        /// 
        /// This method loops through the vehicles and calls an APD procedure to get the
        /// AssignmentID for any shop assignment.
        /// 
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="lngLynxID"></param>
        /// <param name="blnStaffAppraiserAssignment"></param>
        public void SendShopAssignments(XmlDocument objDom, long lngLynxID, bool blnStaffAppraiserAssignment)
        {
            string strApplicaiton = string.Empty;
            //Objects that need clean-up
            XmlNodeList objVehList = null;
            //Primitives that don't
            string strAssignmentID = string.Empty;
            int intVehNum = 0;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Process started.", "");
                //Create an instance of CommServer.
                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Calling COMM Server to assign shop.", "");

                objCommSvr = new CCommServerClass();
                objcDataAccessor = new DataAccess.CDataAccessor();

                //Select and loop through all the "valid" vehicle elements (These will have an AssignmentTypeID attribute).
                objVehList = objDom.SelectNodes("//Vehicle[@AssignmentTypeID]");
                strApplicaiton = objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                foreach (XmlElement objVeh in objVehList)
                {
                    intVehNum = Convert.ToInt32(objVeh.GetAttribute("VehicleNumber"));
                    htParams = new Hashtable();
                    htParams.Add("@LynxID", lngLynxID);
                    htParams.Add("@VehicleNumber", intVehNum);
                    htParams.Add("@AssignmentSuffix", DBNull.Value);

                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Executing SP to get the assignment ID.", string.Concat("Vars: StoredProc = ", objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/SPMap/AssignmentDecodeSP"), " LynxID = ", lngLynxID, "VehicleNumber = ", intVehNum, "AssignmentSuffix = "));
                    strAssignmentID = Convert.ToString(objcDataAccessor.ExecuteSpNamedParams(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/SPMap/AssignmentDecodeSP"), htParams, strApplicaiton)); //APD

                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Executing SP to get the assignment ID.", string.Concat("Vars: StoredProc = ", objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/SPMap/AssignmentDecodeSP"), "Result: AssignmentID = ", strAssignmentID));
                    //  Skip the assignment process if no assignment record was found for the LynxID-VehicleNumber
                    if (strAssignmentID != string.Empty && strAssignmentID != "0" && strAssignmentID != null)
                    {
                        htParams = new Hashtable();
                        htParams.Add("@AssignmentID", strAssignmentID);
                        objcDataAccessor.ExecuteSp(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AssignShopSP"), htParams, strApplicaiton); //APD Application
                        //if (isDebug)
                        {
                            //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Exeuting SP.", string.Concat("Vars: StoredProc = ", objMLAPDEcadAccessorMgr.GetConfig("WorkFlow/SysUserID"), "Assignment ID =", strAssignmentID));
                            //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Calling AssignShop from COMMServer COM.", "");
                        }
                        //Now send the assignment, electronic and/or fax.
                        objCommSvr.AssignShop(Convert.ToInt32(strAssignmentID), Convert.ToInt32(objMLAPDEcadAccessorMgr.GetConfig("WorkFlow/SysUserID")), blnStaffAppraiserAssignment);
                    }
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - SendShopAssignments", "Process completed.", "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objVehList = null;
                htParams = null;
                if (objCommSvr != null)
                    Marshal.ReleaseComObject(objCommSvr);
                objCommSvr = null;
            }
        }

        /// <summary>
        /// Queries APD for user information based upon the login ID.
        /// Adds that user to APD if it was not found in APD.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        internal long FindOrAddUser(ref XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;
            //Objects that will require clean-up         
            //Primiatives that don't require clean-up.
            bool blnExternalUser;

            string strUserID = string.Empty, strPassedUserID = string.Empty, strOtherUserID = string.Empty,
             strLogin = string.Empty,
             strEmail = string.Empty,
             strApplicationCD = string.Empty,
             strApplicationID = string.Empty,
             strInsuranceCoID = string.Empty;
            long lngUserId = 0;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - FindOrAddUser", "Process started.", "");
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                strApplicationCD = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationCD", true);
                strApplicationID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationID", true);
                strInsuranceCoID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@InsuranceCompanyID", true);

                //Pull user info from the assignment DOM.
                strUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepUserID", false);
                strLogin = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepLogin", true);
                strEmail = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepEmailAddress", true);

                strPassedUserID = strUserID;
                //Passed a valid user ID?  --> not '0', '-1', or ''
                if (IsValidUserID(strPassedUserID))
                {
                    //Find user ID in DB for any application.
                    strUserID = SessionGetUserID(string.Empty, strPassedUserID, string.Empty, "ALL");
                    // If a user ID is passed, that user record must exist, otherwise raise an error.
                    if (!IsValidUserID(strUserID))
                        throw new Exception(string.Concat(EventCodes.eUserIdDoesNotExist.ToString(), "User Validation", string.Concat("User ", strEmail, " does not exist for APD.")));
                }
                else
                {
                    //Were not passed a valid user ID...
                    //Find login in DB for passed application.
                    strOtherUserID = SessionGetUserID(strLogin, string.Empty, string.Empty, strApplicationCD);

                    //User already exists in passed application?
                    if (IsValidUserID(strOtherUserID))
                        strUserID = strOtherUserID;
                    else
                    {//Did not exist in passed application.
                        // Find userid by email
                        strUserID = SessionGetUserID(string.Empty, string.Empty, strEmail, "ALL");

                        // UserID found for passed email address?  Add to the other (CP,SG) application.
                        if (IsValidUserID(strUserID))
                        {
                            // User exists but not for the passed application.  Add new application record for existing user record.
                            AddUserToApplication(strUserID, strApplicationID, strLogin);
                        }
                        else
                        {
                            //Otherwise create new user.
                            strUserID = InsertToUserTable(strEmail, ref objParamDom);
                            //Add application record for passed application.
                            AddUserToApplication(strUserID, strApplicationID, strLogin);
                        }
                    }
                }
                if (IsValidUserID(strUserID))
                    lngUserId = Convert.ToInt64(strUserID);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - FindOrAddUser", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return lngUserId;
        }

        /// <summary>
        /// Checks validity of passed user ID
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private bool IsValidUserID(string strUserID)
        {
            bool retIsValidUserID = false;
            try
            {
                //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - IsValidUserID", "Process started.", "");

                retIsValidUserID = Convert.ToBoolean(strUserID != string.Empty && strUserID != "0" && strUserID != "-1");

                //if (isDebug)
                //DevwsAPDFoundation.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - IsValidUserID", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //DevwsAPDFoundation.Dispose();
            }
            return retIsValidUserID;
        }

        /// <summary>
        /// Gets the APD user id for a user.
        /// </summary>
        /// <param name="strLoginID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strEmail"></param>
        /// <param name="strApplicationCD"></param>
        /// <returns></returns>
        private string SessionGetUserID(string strLoginID, string strUserID, string strEmail, string strApplicationCD)
        {
            XmlDocument objResults = null;
            string strAppend = string.Empty, strReturn = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - SessionGetUserID", "Process started.", "");
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Get the session user detail for this user.
                if (strUserID != string.Empty)
                {
                    htParams = new Hashtable();
                    htParams.Add("@ApplicationCD", strApplicationCD);
                    htParams.Add("@UserID", strUserID);
                    strAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), htParams, "APD");
                    objMDomUtils.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
                else if (strEmail != string.Empty)
                {
                    htParams = new Hashtable();
                    htParams.Add("@ApplicationCD", strApplicationCD);
                    htParams.Add("@Email", strEmail);
                    strAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), htParams, "APD");
                    objMDomUtils.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
                else
                {
                    htParams = new Hashtable();
                    htParams.Add("@ApplicationCD", strApplicationCD);
                    htParams.Add("@Login", strLoginID);
                    strAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), htParams, "APD");
                    objMDomUtils.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
                strReturn = objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/User/@UserID");
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - SessionGetUserID", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                htParams = null;
            }
            //Get the user ID from the returned XML
            return strReturn;
        }

        /// <summary>
        /// Gets the user table id for a user.
        /// </summary>
        /// <param name="strEmail"></param>
        /// <param name="strApplicationCD"></param>
        /// <param name="strInsuranceCoID"></param>
        /// <returns></returns>
        private string AdmGetUserID(string strEmail, string strApplicationCD, string strInsuranceCoID)
        {
            XmlDocument objResults = null;
            string strXmlAppend = string.Empty, strReturn = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - AdmGetUserID", "Process started.", "");
                objResults = new XmlDocument();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();
                htParams = new Hashtable();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Get the Admin user detail for this user.
                htParams.Add("@EmailAddress", strEmail);
                htParams.Add("@ApplicationCD", strApplicationCD);
                htParams.Add("@InsuranceCompanyID", strInsuranceCoID);

                strXmlAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AdmGetUserDetailSP"), htParams, "APD");
                htParams = null;

                objMDomUtils.LoadXml(ref objResults, ref strXmlAppend, "AdmGetUserID", "Adm User Detail");
                strReturn = objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/@UserID");
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - AdmGetUserID", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                htParams = null;
            }
            //Get the user ID from the returned XML
            return strReturn;
        }

        /// <summary>
        ///  Adds a user to the user table.
        /// </summary>
        /// <param name="strEmail"></param>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private string InsertToUserTable(string strEmail, ref XmlDocument objParamDom)
        {
            XmlDocument objResults = null;
            string strOfficeID = string.Empty,
                strOfficeCode = string.Empty,
                strApplicationCD = string.Empty,
                strParamXmlAppend = string.Empty;
            Hashtable htParams = null;
            Common.WriteLog objWriteLog = null;
            //Insert the Admin user detail for this user.

            // FNOL Office IDs can be resolved from the database.  Need to ensure any other users of this code that wish to do the
            // same include InsuranceCompanyID and ClientOfficeID values in their xml.  Currently the only other user of this code
            // is Scene Genesis -- their OfficeID is pulled from the config file.
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - InsertToUserTable", "Process started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();
                htParams = new Hashtable();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                strApplicationCD = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationCD");
                if (strApplicationCD != "FNOL" && strApplicationCD != "CCAV")
                    strOfficeID = objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/SceneGenesis/Carrier/@OfficeID");
                else
                {
                    strOfficeCode = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ClientOfficeID", true);
                    strOfficeID = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/ClientOfficeID/Client[@Code='" + strOfficeCode + "']/@OfficeID");
                }

                htParams.Add("@OfficeID", strOfficeID);
                htParams.Add("@InsuranceCompanyID", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@InsuranceCompanyID"));
                htParams.Add("@ClientOfficeID", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ClientOfficeID"));
                htParams.Add("@SupervisorUserID", "");
                htParams.Add("@SupervisorFlag", "0");
                htParams.Add("@AssignmentBeginDate", "");
                htParams.Add("@AssignmentEndDate", "");
                htParams.Add("@EmailAddress", strEmail);
                htParams.Add("@FaxAreaCode", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Substring(0, 3));
                htParams.Add("@FaxExchangeNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Substring(4, 3));
                htParams.Add("@FaxUnitNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax").Substring((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Length - 4, 4)));
                htParams.Add("@FaxExtensionNumber", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFaxExt"));
                htParams.Add("@NameFirst", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameFirst"));
                htParams.Add("@NameLast", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameLast"));
                htParams.Add("@NameTitle", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameTitle"));
                htParams.Add("@PhoneAreaCode", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring(0, 3));
                htParams.Add("@PhoneExchangeNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring(4, 3));
                htParams.Add("@PhoneExtensionNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDayExtn")).Substring(0, 5));
                htParams.Add("@PhoneUnitNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Length - 4, 4));
                htParams.Add("@WorkdayList", "");
                htParams.Add("@LicenseAssignStList", "");
                htParams.Add("@RoleList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserRoleList"));
                htParams.Add("@ProfileList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserProfileList"));
                htParams.Add("@PermissionList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserPermissionList"));
                htParams.Add("@SysLastUserID", 0);

                strParamXmlAppend = objcDataAccessor.ExecuteSpNamedParamsXML(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AdmInsUserDetailSP"), htParams, "APD");

                objMDomUtils.LoadXml(ref objResults, ref strParamXmlAppend, "InsertToUserTable", "Adm Ins User Detail");
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - InsertToUserTable", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                htParams = null;
            }
            //Extract and return the newly created user ID.
            return objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/User/@UserID");
        }

        /// <summary>
        ///  Adds a user to the user table.
        /// </summary>
        /// <param name="strUserID"></param>
        /// <param name="strApplicationID"></param>
        /// <param name="strLogin"></param>
        private void AddUserToApplication(string strUserID, string strApplicationID, string strLogin)
        {
            Hashtable htparams = null;
            SqlConnection sqlConnection = null;
            SqlTransaction transaction = null;
            Common.WriteLog objWriteLog = null;
            bool isCommit = false;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - AddUserToApplication", "Process started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objcDataAccessor = new DataAccess.CDataAccessor();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                sqlConnection = new SqlConnection(objcDataAccessor.GetConnectionString("APD"));
                sqlConnection.Open();
                transaction = sqlConnection.BeginTransaction();

                htparams = new Hashtable();
                htparams.Add("@UserID", strUserID);
                htparams.Add("@ApplicationID", strApplicationID);
                htparams.Add("@LogonId", strLogin);
                htparams.Add("@PasswordHint", "");
                htparams.Add("@SysLastUserID", 0);
                objcDataAccessor.ExecuteSpNamedParamsXML(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AdmInsUserAppDetailSP"), htparams, "APD");

                htparams = new Hashtable();
                htparams.Add("@UserID", strUserID);
                htparams.Add("@ApplicationID", strApplicationID);
                htparams.Add("@SysLastUserID", 0);
                //Activate the user for the application.
                objcDataAccessor.ExecuteSpNamedParamsXML(ref sqlConnection, ref transaction, objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AdmUserSetStatusSP"), htparams, "APD");
                isCommit = true;
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - AddUserToApplication", "Process completed.", "");
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (isCommit)
                    transaction.Commit();
                htparams = null;
                sqlConnection.Close();
                transaction = null;
            }
        }

        /// <summary>
        /// LoadRSXml
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="lngLynxID"></param>
        /// <param name="strRSFileName"></param>
        /// <param name="strOwnerNameFirst"></param>
        /// <param name="strOwnerNameLast"></param>
        /// <param name="strOwnerPhone"></param>
        /// <param name="strOwnerEmailAddress"></param>
        /// <returns></returns>
        private Recordset LoadWAXML2RS(ref XmlDocument objParamDom, long lngLynxID, string strRSFileName, string strOwnerNameFirst, string strOwnerNameLast, string strOwnerPhone, string strOwnerEmailAddress)
        {
            string PROC_NAME = string.Empty, strDocumentPath = string.Empty;
            Recordset objRS = null;
            XmlNode objClaimNode = null;
            XmlNodeList objVehiclesNodeList = null;
            Dictionary objFieldsDef = null;
            TextStream oTTXFile = null;
            FileSystemObject oFSO = null;
            string sLine = string.Empty,
                sFldName = string.Empty,
                sFldType = string.Empty;
            string[] aData = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", "Process started.", "");
                PROC_NAME = string.Concat(MODULE_NAME, "LoadWAXML2RS: ");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Generate the recordset format from the crystal reports input ttx format
                //ttx file is a tab delimited file
                objRS = new Recordset();
                objFieldsDef = new Dictionary();
                oFSO = new FileSystemObject();

                objFieldsDef.RemoveAll();
                objFieldsDef.CompareMode.Equals(StringComparison.CurrentCultureIgnoreCase);

                oTTXFile = oFSO.OpenTextFile(string.Concat(strDocumentPath, "\\", strRSFileName), IOMode.ForReading, false);
                while (!oTTXFile.AtEndOfStream)
                {
                    sLine = (oTTXFile.ReadLine().Trim());
                    //comments start with ;
                    if (sLine.Substring(0, 1) != ";")
                    {
                        //current line is not a comment
                        //Field Name<tab>data type<tab>data length (for string)<tab>sample text
                        aData = Regex.Split(sLine, VBA.Constants.vbTab);
                        sFldName = aData[0];
                        sFldType = aData[1];
                        //default field type is string and is 255 chars long
                        switch (sFldType)
                        {
                            case "memo":
                                objRS.Fields.Append(sFldName, DataTypeEnum.adVarChar, 10000);
                                break;
                            default:
                                objRS.Fields.Append(sFldName, DataTypeEnum.adVarChar, 255);  //Crystal 8.5 string type limit is 255
                                break;
                        }
                        objFieldsDef.Add(sFldName, sFldName);
                    }
                }
                objRS.Open();
                oFSO = null;
                oTTXFile = null;

                //Get the claim node from the web assignment
                objClaimNode = objMLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "/WebAssignment/Claim", true);

                //Get the vehicle list from the web assignment
                objVehiclesNodeList = objParamDom.SelectNodes("/WebAssignment/Vehicle");
                if (objVehiclesNodeList == null)
                    objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", string.Concat(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML.ToString(), "/WebAssignment/Vehicle", "WebAssignment is missing a 'Vehicle' child node."), "");
                else
                {
                    //We will loop thru the vehicle list and merge the vehicle and claim data
                    if (objVehiclesNodeList.Count > 0)
                    {
                        foreach (XmlNode objVehicleNodeforlists in objVehiclesNodeList)
                        {
                            objRS.AddNew();

                            //if (isDebug)
                            //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", "Populating claim data.", "");
                            //First load the claim information
                            foreach (XmlNode objChildNodeforclaim in objClaimNode.ChildNodes)
                            {
                                if (objFieldsDef.Exists(string.Concat("Claim_", objChildNodeforclaim.Name)))
                                    objRS.Fields[string.Concat("Claim_", objChildNodeforclaim.Name)].Value = objChildNodeforclaim.InnerText;
                            }
                            //if (isDebug)
                            //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", "Populating vehicle data.", "");

                            //Load the vehicle information
                            foreach (XmlNode objChildNodeforVehicle in objVehicleNodeforlists.ChildNodes)
                            {
                                if (objFieldsDef.Exists(string.Concat("Vehicle_", objChildNodeforVehicle.Name)))
                                    objRS.Fields[string.Concat("Vehicle_", objChildNodeforVehicle.Name)].Value = objChildNodeforVehicle.InnerText;
                            }

                            //if (isDebug)
                            //DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", "Populating claim header data.", "");
                            //stuff in the LynxID
                            if (objFieldsDef.Exists("Claim_LynxID"))
                                objRS.Fields["Claim_LynxID"].Value = (lngLynxID).ToString();

                            //stuff in the owner information
                            if (objFieldsDef.Exists("Claim_OwnerNameFirst"))
                                objRS.Fields["Claim_OwnerNameFirst"].Value = strOwnerNameFirst;

                            if (objFieldsDef.Exists("Claim_OwnerNameLast"))
                                objRS.Fields["Claim_OwnerNameLast"].Value = strOwnerNameLast;

                            if (objFieldsDef.Exists("Claim_OwnerPhone"))
                                objRS.Fields["Claim_OwnerPhone"].Value = strOwnerPhone;

                            if (objFieldsDef.Exists("Claim_OwnerEmailAddress"))
                                objRS.Fields["Claim_OwnerEmailAddress"].Value = strOwnerEmailAddress;

                            objRS.Update();
                        }
                    }
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", "Process completed.", "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "EcadAccessor.CWebAssignment.cs - LoadWAXML2RS", ex.Message, objParamDom.OuterXml);
                objRS = null;
                return objRS;
            }
            finally
            {
                objWriteLog = null;
                oFSO = null;
                oTTXFile = null;
                objClaimNode = null;
                objVehiclesNodeList = null;
                objFieldsDef = null;
            }
            //Return the recordset
            return objRS;
        }

        /// <summary>
        /// Build name foramt for the files
        /// </summary>
        /// <param name="strEntity"></param>
        /// <param name="lngLynxID"></param>
        /// <returns></returns>
        private string BuildFileName(string strEntity, long lngLynxID)
        {
            int intImageCount = 0;
            string strImageFolder = string.Empty,
             strLynxID = string.Empty,
             strDateTime = string.Empty;
            try
            {

                strLynxID = Right(Convert.ToString(lngLynxID), 4);
                for (intImageCount = 0; intImageCount < 4; intImageCount++)
                {
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\", Mid(strLynxID, intImageCount, 1) });
                }
                strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\" });

                //Use the current date time to make the file name unique.
                strDateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Put it all together now...
            return string.Concat(strImageFolder, strEntity, strDateTime, "LYNXID", lngLynxID.ToString(), "Doc.pdf");
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string strRight = string.Empty;
            try
            {
                if (param != string.Empty)
                    strRight = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strRight;
        }

        /// <summary>
        /// use to get the Mid character
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex, int length)
        {
            string strMid = string.Empty;
            try
            {
                if (param != string.Empty)
                    strMid = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strMid;
        }

        /// <summary>
        /// DoGlassSubmission
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        internal string DoGlassSubmission(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;

            // Objects that must be destroyed.
            XSLTemplate40 objXSLT = null;
            Guid objGuid;
            IXSLProcessor objXSLProc = null;
            XmlElement objElem = null;
            IXMLDOMDocument2 objXSLdoc = null;
            // Primitives.
            string xslName = string.Empty,
             strDate = string.Empty,
             strTime = string.Empty,
             strAGCXML = string.Empty,
             strAGCEnvironmentCode = string.Empty,
             strRetVal = string.Empty,
             strGuid = string.Empty,
             strInsCoID = string.Empty,
             strAgcID = string.Empty,
             strSourceXML = string.Empty,
             strLossType = string.Empty;
            long lngErrNum = 0;
            string strErrNum = string.Empty,
             strSource = string.Empty,
             strDescription = string.Empty, strDocumentPath = string.Empty;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CWebAssignment.cs - DoGlassSubmission", "Process started.", "");
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                strDocumentPath = Convert.ToString(ConfigurationManager.AppSettings["LAPDEcadAccessorMgr"]);

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                strInsCoID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//InsuranceCompanyID", true);

                // Retreive values that will be plugged into the XSLT as parameters
                strDate = (DateTime.Now.ToString("mm/dd/yyyy"));
                strTime = (DateTime.Now.ToString("hh:MM:ss"));
                strAGCEnvironmentCode = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/@EnvironmentCode");
                strAgcID = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/Insurance/Company[@ID='" + strInsCoID + "']/@AgcID");

                // Resolve the path and name of the XSL used to transform the Web Assignment XML.
                xslName = string.Concat(strDocumentPath, "\\APDtoAGC", strInsCoID, ".xsl");

                //Generate guid
                objGuid = new Guid();
                strGuid = Convert.ToString(objGuid).Trim();
                strGuid = strGuid.Substring(2, strGuid.IndexOf("}", 1) - 2);

                // Load the XSL into a Free Threaded DOM.
                objXSLdoc.async = false;
                objXSLdoc.load(xslName);

                // Load the XSL document into the XSL Template object.
                objXSLT.stylesheet = objXSLdoc;

                // Create an XSL processor from the XSL Template object.
                objXSLProc = objXSLT.createProcessor();

                // Load the source XML into the XSL processor.
                objXSLProc.input = objParamDom;

                // Populate the XSL parameters.
                objXSLProc.addParameter("TransactionDate", strDate);
                objXSLProc.addParameter("TransactionTime", strTime);
                objXSLProc.addParameter("EnvironmentID", strAGCEnvironmentCode);
                objXSLProc.addParameter("TransactionID", strGuid);
                objXSLProc.addParameter("AgcID", strAgcID);

                // Perform the XSL transformation.
                objXSLProc.transform();

                // Retrieve the transormed XML from the XSL processor.
                strAGCXML = Convert.ToString(objXSLProc.output);

                // Transferring this to GO wil be done through a com+ call rather than the normal post to glaxis.  Chris Billick
                // from Glaxis will provide the compoent/proxy info.

                // Grab data from config and build xml parameter required by Glaxis.
                strLossType = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/Insurance/Company[@ID='" + strInsCoID + "']");
                strSourceXML = string.Concat("<Source><Id>", strAgcID, "</Id><Name>", strLossType, "</Name></Source>");
                // Make Glaxis call.
                objGlaxisProxy = new lynxReceive.Loss();
                strRetVal = objGlaxisProxy.SaveLoss(strAGCXML, strSourceXML);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CWebAssignment.cs - DoGlassSubmission", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }

            return strRetVal;
        }
        #endregion
    }
}
