﻿using System;
using System.Xml;
using System.IO;
using ReportUtils;
using System.Configuration;
using System.Drawing;
using System.Runtime.InteropServices.ComTypes;
using Common = Lynx.APD.Component.Library.Common;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class CDocument
    {
        #region Global Declaration
        MLAPDEcadAccessorMgr mobjEcadAccessorMgr = null;
        MBase64Utils mobjMBase64Utils = null;
        Common.MDomUtils mobjMDomUtils = null;
        clsReport objReportUtils = null;
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);
        #endregion

        #region Enumerators
        //Internal error codes for this class.
        public enum EventCodes : ulong
        {
            eReturnedStreamIsBlank = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CDocument_CLS_ErrorCodes
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Use to get the byte data from the ReportUtils for ClaimPoint report.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetDocument(string strParamXML)
        {
            //Object declarations
            XmlDocument objParamDom = null;
            ADODB.Stream objAdoStream = null;
            XmlNode objxmlnode = null;
            XmlNode objxmlDocumentRoot = null;
            XmlNode objxmlheight = null;
            XmlNode objxmlwidth = null;
            byte[] thumbArr = null;
            //Variable declarations
            string strFilePath = string.Empty;
            string strDocumentRoot = string.Empty;
            string strGetDocument = string.Empty;
            string strWidth = string.Empty;
            string strHeight = string.Empty;
            string strArchivedFlag = string.Empty;
            string strReportsPath = string.Empty, strBase64 = string.Empty;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetDocument", "GetDocument process started.", strParamXML);

                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                mobjMBase64Utils = new MBase64Utils();
                mobjMDomUtils = new Common.MDomUtils();
                mobjEcadAccessorMgr.InitializeGlobals();

                //Check passed parameters.
                mobjEcadAccessorMgr.g_objEvents.Assert(Convert.ToBoolean((strParamXML.Length) > 0), "Nothing passed for strParamXML.");

                if (!string.IsNullOrEmpty(strParamXML.Trim()))
                {
                    if (!strParamXML.Contains("Document") && !strParamXML.Contains("Report") && strParamXML.Contains("Thumbnail"))
                        strParamXML = string.Concat("<Thumbnail ImageLocation='", strParamXML, "' />");
                }

                //Initialize DOM objects
                objParamDom = new XmlDocument();
                mobjMDomUtils.LoadXml(ref objParamDom, ref strParamXML, "", "");

                //Entity Specific Code - select on the entity
                switch (objParamDom.DocumentElement.Name)
                {
                    case "Document":
                        //Get the file path from the parameter
                        objxmlnode = (XmlNode)(objParamDom.DocumentElement.SelectSingleNode("@ImageLocation"));
                        strFilePath = mobjMDomUtils.GetChildNodeText(ref objxmlnode, "//@ImageLocation", true);
                        //Get the document root from the config file.
                        //Get the Archivedflag for the current document,

                        objxmlnode = (XmlNode)(objParamDom.DocumentElement.SelectSingleNode("@Archived"));
                        if (objxmlnode != null)
                        {
                            strArchivedFlag = objxmlnode.InnerText;
                            if (strArchivedFlag == "1")
                                strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootArchiveDirectory");
                            else
                                strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                        }
                        else
                        {
                            strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                        }
                        //Combine and escape out the double slashes.
                        strFilePath = string.Concat(strDocumentRoot, strFilePath).Replace("\\\\", "\\");
                        //Return the results.
                        thumbArr = mobjMBase64Utils.ReadBinaryDataFromFile(strFilePath);
                        strBase64 = Convert.ToBase64String(thumbArr);
                        break;

                    case "Report":
                        //Create the report utilities object.
                        //if (isDebug)
                        // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetDocument", "Creating ReportUtils object.", "");

                        objReportUtils = new clsReport();
                        strReportsPath = Convert.ToString(ConfigurationManager.AppSettings["EcadAccessorReports"]);
                        //if (isDebug)
                        // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetDocument", "Generating report", string.Concat("Vars: ", "ReportFilename =", strReportsPath, "/", objParamDom.SelectSingleNode("/Report/@sReportFileName").InnerText, " StoredProc = ", objParamDom.SelectSingleNode("/Report/@sStoredProc").InnerText, " ReportParams =", objParamDom.SelectSingleNode("/Report/@sReportParams").InnerText
                        // , " ReportStaticParam =", objParamDom.SelectSingleNode("/Report/@sReportStaticParams").InnerText, "ReportFormat =", objParamDom.SelectSingleNode("/Report/@sReportFormat").InnerText));

                        objAdoStream = objReportUtils.getReport(string.Concat(strReportsPath, "/", objParamDom.SelectSingleNode("/Report/@sReportFileName").InnerText),
                  objParamDom.SelectSingleNode("/Report/@sStoredProc").InnerText,
                  objParamDom.SelectSingleNode("/Report/@sReportParams").InnerText,
                  objParamDom.SelectSingleNode("/Report/@sReportStaticParams").InnerText,
                  objParamDom.SelectSingleNode("/Report/@sReportFormat").InnerText);
                        //if (isDebug)
                        // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetDocument", string.Concat("Report generated and the size is = ", objAdoStream.Size), "");
                        //Read all the data from the stream.
                        thumbArr = (byte[])objAdoStream.Read();
                        strBase64 = Convert.ToBase64String(thumbArr);
                        objAdoStream.Close();
                        objAdoStream = null;

                        //Clean up and go home.
                        objReportUtils = null;
                        break;

                    case "Thumbnail":
                        ////Get the file path from the parameter
                        objxmlDocumentRoot = objParamDom.DocumentElement.SelectSingleNode("@ImageLocation");
                        strFilePath = mobjMDomUtils.GetChildNodeText(ref objxmlDocumentRoot, "//@ImageLocation", true);
                        ////Get the document root from the config file.
                        ////Get Archived for current document.
                        objxmlDocumentRoot = objParamDom.DocumentElement.SelectSingleNode("@Archived");

                        if (objxmlDocumentRoot != null)
                        {
                            strArchivedFlag = objxmlDocumentRoot.InnerText;
                            if (strArchivedFlag == "1")
                                strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootArchiveDirectory");
                            else
                                strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                        }
                        else
                        {
                            strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                        }
                        ////Combine and escape out the double slashes.
                        strFilePath = string.Concat(strDocumentRoot, strFilePath).Replace("\\\\", "\\");

                        ////Get the thumbnail height and width from the param XML.
                        ////If not passed, pull the default from the config file.
                        objxmlheight = objParamDom.DocumentElement.SelectSingleNode("@Height");

                        if (objxmlheight != null)
                            strHeight = mobjMDomUtils.GetChildNodeText(ref objxmlheight, "//@Height", false);
                        objxmlwidth = objParamDom.DocumentElement.SelectSingleNode("@Width");

                        if (objxmlwidth != null)
                            strWidth = mobjMDomUtils.GetChildNodeText(ref objxmlwidth, "//@Width", false);
                        if (strHeight == "")
                            strHeight = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Height");
                        if (strWidth == "")
                            strWidth = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Width");

                        Image.GetThumbnailImageAbort myThumbNail = new Image.GetThumbnailImageAbort(ThumbnailCallBack);
                        Bitmap myBitmap = new Bitmap(strFilePath);
                        Image mythumnail = myBitmap.GetThumbnailImage(Convert.ToInt32(strWidth), Convert.ToInt32(strHeight), myThumbNail, IntPtr.Zero);
                        using (MemoryStream ms = new MemoryStream())
                        {

                            mythumnail.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            thumbArr = ms.ToArray();
                            //Read all the data from the stream.
                            //strBase64 = string.Concat("|||", Convert.ToBase64String(thumbArr), "|||");
                            strBase64 = Convert.ToBase64String(thumbArr);
                        }
                        break;
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CDocument.cs - GetDocument", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                if (objReportUtils != null)
                    Marshal.ReleaseComObject(objReportUtils);
                objReportUtils = null;
                objParamDom = null;
                thumbArr = null;
            }
            return strBase64;
        }

        /// <summary>
        /// Use to get the report document for Claim Submission
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetReportDocument(string strParamXML)
        {
            ADODB.Stream objAdoStream = null;
            string strReportsPath = string.Empty;
            XmlDocument objParamDom = null;
            string strrptDo = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetReportDocument", "Process started.", "");
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                mobjMBase64Utils = new MBase64Utils();
                mobjMDomUtils = new Common.MDomUtils();

                objParamDom = new XmlDocument();
                mobjMDomUtils.LoadXml(ref objParamDom, ref strParamXML, "", "");

                //if (isDebug)
                // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetReportDocument", "Creating ReportUtils object.", "");
                objReportUtils = new clsReport();
                //Call it to stream the report file.
                strReportsPath = Convert.ToString(ConfigurationManager.AppSettings["EcadAccessorReports"]);
                //if (isDebug)
                // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetReportDocument", "Generating report", string.Concat("Vars: ", "ReportFilename =", strReportsPath, "/", objParamDom.SelectSingleNode("/Report/@sReportFileName").InnerText, " StoredProc = ", objParamDom.SelectSingleNode("/Report/@sStoredProc").InnerText, " ReportParams =", objParamDom.SelectSingleNode("/Report/@sReportParams").InnerText
                //, " ReportStaticParam =", objParamDom.SelectSingleNode("/Report/@sReportStaticParams").InnerText, "ReportFormat =", objParamDom.SelectSingleNode("/Report/@sReportFormat").InnerText));


                objAdoStream = objReportUtils.getReport(string.Concat(strReportsPath, "/", objParamDom.SelectSingleNode("/Report/@sReportFileName").InnerText),
          objParamDom.SelectSingleNode("/Report/@sStoredProc").InnerText,
          objParamDom.SelectSingleNode("/Report/@sReportParams").InnerText,
          objParamDom.SelectSingleNode("/Report/@sReportStaticParams").InnerText,
          objParamDom.SelectSingleNode("/Report/@sReportFormat").InnerText);
                //if (isDebug)
                // DevwsAPDFoundation.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetReportDocument", string.Concat("Report generated and the size is = ", objAdoStream.Size), "");
                IStream iStream = (IStream)objAdoStream;
                byte[] byteArray = new byte[objAdoStream.Size];
                IntPtr ptrCharsRead = IntPtr.Zero;
                iStream.Read(byteArray, objAdoStream.Size, ptrCharsRead);
                strrptDo = Convert.ToBase64String(byteArray);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CDocument.cs - GetReportDocument", "Process Completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objAdoStream = null;
                objParamDom = null;
                objReportUtils = null;
            }
            return strrptDo;
        }

        /// <summary>
        ///  Creating thumbanil for APD
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width)
        {
            string strBase64 = string.Empty, strDocumentPath = string.Empty, strDocumentRoot = string.Empty;
            byte[] thumbArr = null;
            Common.WriteLog objWriteLog = null;
            Image.GetThumbnailImageAbort myThumbNail = null;
            Bitmap myBitmap = null;
            Image myimgthumnail = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", "");
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();

                if (!strParamXML.Contains("pgw.local"))
                {
                    strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                    strDocumentPath = string.Concat(strDocumentRoot, strParamXML).Replace("\\\\", "\\");
                }
                else
                    strDocumentPath = strParamXML;


                if (string.IsNullOrEmpty(Height.Trim()))
                    Height = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Height");

                if (string.IsNullOrEmpty(Width.Trim()))
                    Width = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Width");

                //if (isDebug)
                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("Vars:", "Location Xml =", strDocumentPath, " Image Height = ", Height, " Image Width = ", Width));


                myThumbNail = new Image.GetThumbnailImageAbort(ThumbnailCallBack);
                myBitmap = new Bitmap(strDocumentPath);
                myimgthumnail = myBitmap.GetThumbnailImage(Convert.ToInt32(Width), Convert.ToInt32(Height), myThumbNail, IntPtr.Zero);

                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("myBitmap.GetThumbnailImage completed"));

                using (MemoryStream ms = new MemoryStream())
                {
                    myimgthumnail.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("myimgthumnail.Save to Memory stream"));

                    thumbArr = ms.ToArray();
                    strBase64 = Convert.ToBase64String(thumbArr);

                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("Memory stream to Convert.ToBase64String"));

                }


                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (myBitmap != null)
                {
                    myBitmap.Dispose();
                    myBitmap = null;
                }

                if (myimgthumnail != null)
                {
                    myimgthumnail.Dispose();
                    myimgthumnail = null;
                }

                myThumbNail = null;
                objWriteLog = null;
                thumbArr = null;
            }
            return strBase64;
        }


        /// <summary>
        ///  Creating thumbanil for APD
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <param name="Archived"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width, string Archived)
        {
            string strBase64 = string.Empty, strDocumentPath = string.Empty, strArchived = string.Empty, strDocumentRoot = string.Empty;
            byte[] thumbArr = null;
            Common.WriteLog objWriteLog = null;
            Image.GetThumbnailImageAbort myThumbNail = null;
            Bitmap myBitmap = null;
            Image myimgthumnail = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", "");
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();

                strArchived = Archived;

                if (!strParamXML.ToLower().Contains("pgw.local") && !strParamXML.ToLower().Contains("apddocarchive"))
                {
                    if (isExtensiveDebug)
                        objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "checking contains PGW.local.", "");
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("Vars:", "Location Xml =", strDocumentPath, " Image Height = ", Height, " Image Width = ", Width, " Archived= ", Archived));


                    if (strArchived != null)
                        if (strArchived == "1")
                            strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootArchiveDirectory");
                        else
                            strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                    else
                        strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");

                    strDocumentPath = string.Concat(strDocumentRoot, strParamXML).Replace("\\\\", "\\");
                }
                else
                    strDocumentPath = strParamXML;

                if (string.IsNullOrEmpty(Height.Trim()))
                    Height = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Height");

                if (string.IsNullOrEmpty(Width.Trim()))
                    Width = mobjEcadAccessorMgr.GetConfig("ClaimPoint/Thumbnail/@Width");
                if (isDebug)
                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("Vars:", "Location Xml =", strDocumentPath, " Image Height = ", Height, " Image Width = ", Width, " Archived= ", strArchived));


                
                myThumbNail = new Image.GetThumbnailImageAbort(ThumbnailCallBack);
                myBitmap = new Bitmap(strDocumentPath);
                myimgthumnail = myBitmap.GetThumbnailImage(Convert.ToInt32(Width), Convert.ToInt32(Height), myThumbNail, IntPtr.Zero);
            
                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("myBitmap.GetThumbnailImage completed"));

                using (MemoryStream ms = new MemoryStream())
                {
                    myimgthumnail.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("myimgthumnail.Save to Memory stream"));
                    thumbArr = ms.ToArray();
                    strBase64 = Convert.ToBase64String(thumbArr);
                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process started.", string.Concat("Memory stream to Convert.ToBase64String"));

                }


                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CDocument.cs - GetAPDThumbnail", "Process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (myBitmap != null)
                {
                    myBitmap.Dispose();
                    myBitmap = null;
                }

                if (myimgthumnail != null)
                {
                    myimgthumnail.Dispose();
                    myimgthumnail = null;
                }

                myThumbNail = null;
                objWriteLog = null;
                thumbArr = null;
            }
            return strBase64;
        }

 

        public bool ThumbnailCallBack()
        {
            return false;
        }

        /// <summary>
        /// GetDocumentXML
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetDocumentXML(string strParamXML)
        {
            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objResultDom = null;
            XmlNode objxmlnode = null;
            //Variable declarations
            string strFilePath = string.Empty;
            string strDocumentRoot = string.Empty;
            string strGetDocumentXML = string.Empty;
            string strArchivedFlag = string.Empty;
            Common.WriteLog objWriteLog = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.CDocument.cs - GetDocumentXML", "Process started.", "");
                objResultDom = new XmlDocument();
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                mobjMDomUtils = new Common.MDomUtils();

                mobjEcadAccessorMgr.InitializeGlobals();
                //Check passed parameters.
                mobjEcadAccessorMgr.g_objEvents.Assert(Convert.ToBoolean((strParamXML.Length) > 0), "Nothing passed for strParamXML.");

                //   Initialize DOM objects.
                objParamDom = new XmlDocument();
                objResultDom = new XmlDocument();
                //   Loads and validates the xml.
                mobjMDomUtils.LoadXml(ref objParamDom, ref strParamXML, "", "");
                // Get the file path from the parameter
                objxmlnode = objParamDom.DocumentElement.SelectSingleNode("@ImageLocation");
                strFilePath = Convert.ToString(mobjMDomUtils.GetChildNode(ref objxmlnode, "@ImageLocation", true));
                // Get Archived for current document

                objxmlnode = objParamDom.DocumentElement.SelectSingleNode("@Archived");
                //Get the document root from the config file.
                if (objxmlnode != null)
                {
                    strArchivedFlag = objxmlnode.InnerText;
                    if (strArchivedFlag == "1")
                        strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootArchiveDirectory");
                    else
                        strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");
                }
                else
                    strDocumentRoot = mobjEcadAccessorMgr.GetConfig("Document/RootDirectory");

                // Combine and escape out the double slashes.
                strFilePath = string.Concat(strDocumentRoot, strFilePath).Replace("\\", @"\");
                // Load up the xml file from disk.
                mobjMDomUtils.LoadXmlFile(ref objResultDom, ref strFilePath, "", "Document");
                //Now return the escaped XML.
                strGetDocumentXML = Convert.ToString(objResultDom.OuterXml);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.CDocument.cs - GetDocumentXML", "Process completed.", strGetDocumentXML);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                objParamDom = null;
                objResultDom = null;
                objxmlnode = null;
                objResultDom = null;
            }
            return strGetDocumentXML;
        }
        #endregion

        #region Private Methods
        private void Class_Terminate()
        {
            mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
            mobjEcadAccessorMgr.TerminateGlobals();
        }
        #endregion
    }
}

