﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using Lynx.APD.Component.Library.Common;
using System.Configuration;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class MBase64Utils
    {
        #region Global Declarations
        //private PartnerDataMgr.MLAPDPartnerDataMgr mlapdPartnerDataMgr = new PartnerDataMgr.MLAPDPartnerDataMgr();
        MLAPDEcadAccessorMgr mLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();       
        bool isExtensiveDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["ExtensiveDebug"]);
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        #endregion

        #region Enumerators
        private enum EventCodes : ulong
        {
            eFileDoesNotExist = 0x80065000 + 0x400,
            eHexDataInvalid
        }
        #endregion

        #region Public Functions
        public byte[] ReadBinaryDataFromFile(string strFileName)
        {
            long lngLen;
            byte[] arrBytes = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - ReadBinaryDataFromFile", "ReadBinaryDataFromFile process started.", string.Concat("Vars: Filename =", strFileName));

                mLAPDEcadAccessorMgr.InitializeGlobals();
                lngLen = strFileName.Length;

                if (lngLen < 1)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - ReadBinaryDataFromFile", "File name is empty.", "");

                if (!StringExtensions.IsNullOrWhiteSpace(strFileName))
                    arrBytes = File.ReadAllBytes(strFileName);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - ReadBinaryDataFromFile", "ReadBinaryDataFromFile process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return arrBytes;
        }

        public void WriteBinaryDataToFile(string strFileName, ref byte[] arrBuffer)
        {
            long lngLen;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - WriteBinaryDataToFile", "WriteBinaryDataToFile process started.", string.Concat("Vars: Filename =", strFileName));

                if (StringExtensions.IsNullOrWhiteSpace(strFileName))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - WriteBinaryDataToFile", "File Name param was blank.", "");

                File.WriteAllBytes(strFileName, arrBuffer);
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - WriteBinaryDataToFile", "WriteBinaryDataToFile process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        public object ReadBinaryDataFromDomElement(ref XmlElement objElement)
        {
            object varType = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - ReadBinaryDataFromDomElement", "ReadBinaryDataFromDomElement process started.", "");

                varType = objElement.GetType();
                objElement.GetType().Equals("bin.base64");

                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - ReadBinaryDataFromDomElement", "ReadBinaryDataFromDomElement process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return objElement.InnerText;
        }

        public void WriteBinaryDataToDomElement(ref XmlElement objElement, ref object varBuffer)
        {
            object varType;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - WriteBinaryDataToDomElement", "WriteBinaryDataToDomElement process started.", "");

                varType = objElement.GetType();
                objElement.GetType().Equals("bin.base64");
                objElement.Value = varBuffer.ToString();

                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - WriteBinaryDataToDomElement", "WriteBinaryDataToDomElement process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        public string ReadTextDataFromFile(string strFileName)
        {
            string readedContent = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - ReadTextDataFromFile", "ReadTextDataFromFile process started.", string.Concat("Vars:", strFileName));

                if (StringExtensions.IsNullOrWhiteSpace(strFileName))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - ReadTextDataFromFile", "File Name param was blank.", "");

                using (StreamReader srFileReader = new StreamReader(strFileName))
                {
                    readedContent = srFileReader.ReadToEnd();
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - ReadTextDataFromFile", "ReadTextDataFromFile process started.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return readedContent;
        }

        public void WriteTextDataToFile(string strFileName, string strData)
        {
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - WriteTextDataToFile", "WriteTextDataToFile process started.", string.Concat("Vars:", strFileName));

                if (StringExtensions.IsNullOrWhiteSpace(strFileName))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - WriteTextDataToFile", "File Name param was blank.", "");

                using (StreamWriter swcontentWriter = new StreamWriter(strFileName, true))
                {
                    swcontentWriter.WriteLine(strData);
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - WriteTextDataToFile", "WriteTextDataToFile process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        public string ReadTextFromCDATA(ref XmlCDataSection objCdata)
        {
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - ReadTextFromCDATA", "WriteTextDataToFile process started.", "");

                if (objCdata == null)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - ReadTextFromCDATA", string.Concat("DOM node param was Nothing."), "");

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - ReadTextFromCDATA", "WriteTextDataToFile process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return objCdata.Data;
        }

        public void WriteTextToCDATA(ref XmlCDataSection objCdata, ref string strData)
        {
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - WriteTextToCDATA", "WriteTextToCDATA process started.", "");

                if (objCdata == null)
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - WriteTextToCDATA", "DOM node param was Nothing.", "");

                objCdata.Data = strData;
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - WriteTextToCDATA", "WriteTextToCDATA process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
        }

        public long DecodeHexBin(string HexString, ref byte[] buffer)
        {
            string hexValue = string.Empty,
                HexStringToDecode = string.Empty;
            int intValue = 0,
                ptr = 0,
                i = 0,
                bufferSize = 0;
            double dblValue;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - DecodeHexBin", "DecodeHexBin process started.", "");

                HexStringToDecode = HexString.Trim();

                if (ValidateHexString(HexStringToDecode))
                    objWriteLog.LogEvent("COM Conversion", "VALIDATE", "EcadAccessor.MBaseUtils.cs - DecodeHexBin", "Hex encoded data contains invalid character.", "");

                bufferSize = Convert.ToInt32(HexString.Length / 2);
                buffer = new byte[bufferSize];

                //Now walk through and perform decoding
                for (i = 0; i <= HexString.Length - 1; i += 2)
                {
                    hexValue = string.Concat("&H", HexString.Substring(i, 2), "&");
                    dblValue = Microsoft.VisualBasic.Conversion.Val(hexValue);
                    buffer[ptr] = Convert.ToByte(dblValue);
                    ptr = ptr + 1;
                }
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - DecodeHexBin", "DecodeHexBin process completed.", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objWriteLog = null;
            }
            return bufferSize;
        }

        public string EncodeHexBin(long bufferSize, ref byte[] buffer)
        {
            string hexValue = string.Empty;
            string HexString = string.Empty;
            string strTempFile = string.Empty;
            int intValue = 0;
            long ptr;
            ADODB.Stream objStream = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objWriteLog = new WriteLog();
                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.MBaseUtils.cs - EncodeHexBin", "EncodeHexBin process strated.", "");

                objStream.Type = ADODB.StreamTypeEnum.adTypeText;
                objStream.Open();
                for (ptr = 0; ptr <= bufferSize - 1; ptr++)
                {
                    intValue = buffer[ptr];
                    if (intValue <= 15)
                        hexValue = "0" + int.Parse(intValue.ToString(), NumberStyles.HexNumber);
                    else
                        hexValue = int.Parse(intValue.ToString(), NumberStyles.HexNumber).ToString();

                    objStream.WriteText(hexValue, ADODB.StreamWriteEnum.adWriteChar);
                }
                objStream.Position = 0;

                if (isExtensiveDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.MBaseUtils.cs - EncodeHexBin", "EncodeHexBin process completed.", "");
                return objStream.ReadText((int)ADODB.StreamReadEnum.adReadAll);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //DevwsAPDFoundation.Dispose();
                objStream = null;
            }
        }
        #endregion

        #region Private Functions
        private bool ValidateHexString(string HexString)
        {
            Int32 ptr = 0,
                intAscVal = 0;
            bool blnOk = true;
            try
            {
                for (ptr = 1; ptr <= HexString.Length; ptr++)
                {
                    intAscVal = Convert.ToInt32(Encoding.ASCII.GetBytes(HexString.Substring(ptr, 1)));
                    if ((intAscVal < 48 || intAscVal > 70 || (intAscVal > 57 && intAscVal < 65)))
                    {
                        // Not a valid Hex Character
                        // Seems its guilty afterall
                        blnOk = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnOk;
        }
        #endregion
    }
}
