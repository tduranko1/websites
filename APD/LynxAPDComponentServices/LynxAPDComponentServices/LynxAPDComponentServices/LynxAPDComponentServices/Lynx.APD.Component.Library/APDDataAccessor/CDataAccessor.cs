﻿using System;
using System.Collections;
using System.Data.SqlClient;
using ADODB;
using System.Data;
using System.Xml;
using System.Configuration;
using Lynx.APD.Component.Library.Common;
using System.Text;

namespace Lynx.APD.Component.Library.APDDataAccessor
{
    /// <summary>
    /// As such, Class CDataAccessor has bloomed into a massive collection of methods
    /// with some confusion and overlap of functionality.  This was done intentionally
    /// for backwards compatability purposes - breaking DataAccessor would break every
    /// single component in our apps.  So deal.
    /// </summary>
    public class CDataAccessor
    {
        #region Variables
        string MODULE_NAME = string.Concat("DataAccess.", "CDataAccessor.");        
        string mstrFpCmdText = string.Empty,
               msConnectString = string.Empty;       
        int iRetryTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorRetryTime"]);
        int iRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);

        //Retry parameters when database connections fail.
        private const long MaxFailoverAttempts = 20;
        private const long FailoverSleepPeriod = 500;
               
        //Do we want to send errors to mEvents for logging and notification?
        //This defaults to True, but in special situations can be disabled
        //through the NotifyEvents property.
        private bool m_blnNotifyEvents = true;

        //The config file path passed to InitEvents
        private string m_strConfigFilePath = string.Empty;       
        #endregion

        #region ENUM
        private enum ErrorEnum : uint
        {
            //These event codes map to specific events within error.xml.
            eInvalidParameter = 0x80061000 + 50,
            eInvalidParameter_StringLength,
            eInvalidParameter_NonNumeric,
            eInvalidParameter_OutOfRange,
            eInvalidParameterType,
            eMaintenanceMode,
            //These event codes will map to the default.
            eConnectionStringNotSet = 0x80061000 + 0x100,
            eBlankUpdateGram,
            eWrongDataProvider,
            eParameterMissingFromCollection,
            eConnectionOpenError,
            eConnectionInTransaction,
            eCannotNestTrans,
            eNoTransExists,
            eTransactionLeftOpen
        }
        #endregion

        #region Properties       

        /// <summary>
        /// Do we want to send errors to mEvents for logging and notification?
        /// This defaults to True, but in special situations can be disabled.
        /// </summary>
        public bool NotifyEvents
        {
            get
            {
                return m_blnNotifyEvents;
            }
            set
            {
                m_blnNotifyEvents = value;
            }
        }
        #endregion

        #region Common Methods

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region UpdatedMethods for Execute StoredProcedure

        /// <summary>
        /// Transalate Type to change the data type.
        /// </summary>
        /// <param name="columnType"></param>
        /// <returns></returns>
        public ADODB.DataTypeEnum TranslateType(Type columnType)
        {
            switch (columnType.UnderlyingSystemType.ToString())
            {
                case "System.Boolean":
                    return ADODB.DataTypeEnum.adBoolean;

                case "System.Byte":
                    return ADODB.DataTypeEnum.adUnsignedTinyInt;

                case "System.Char":
                    return ADODB.DataTypeEnum.adChar;

                case "System.DateTime":
                    return ADODB.DataTypeEnum.adDate;

                case "System.Decimal":
                    return ADODB.DataTypeEnum.adCurrency;

                case "System.Double":
                    return ADODB.DataTypeEnum.adDouble;

                case "System.Int16":
                    return ADODB.DataTypeEnum.adSmallInt;

                case "System.Int32":
                    return ADODB.DataTypeEnum.adInteger;

                case "System.Int64":
                    return ADODB.DataTypeEnum.adBigInt;

                case "System.SByte":
                    return ADODB.DataTypeEnum.adTinyInt;

                case "System.Single":
                    return ADODB.DataTypeEnum.adSingle;

                case "System.UInt16":
                    return ADODB.DataTypeEnum.adUnsignedSmallInt;

                case "System.UInt32":
                    return ADODB.DataTypeEnum.adUnsignedInt;

                case "System.UInt64":
                    return ADODB.DataTypeEnum.adUnsignedBigInt;

                case "System.Guid":
                    return ADODB.DataTypeEnum.adBSTR;

                case "System.String":
                default:
                    return ADODB.DataTypeEnum.adVarChar;
            }
        }

        /// <summary>
        /// OpenRecordSP and returns the RS data using hashtable
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSp(string strProcedureName, Hashtable htParams, string strApplication)
        {
            Recordset recordSet = null;
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            DataColumnCollection inColumns = null;
            SqlDataAdapter dataAdadpter = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                recordSet = new Recordset();
                dataTable = new DataTable();
                strLogParameters = new StringBuilder();

                recordSet.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                ADODB.Fields resultFields = recordSet.Fields;

                strConnectionString = GetConnectionString(strApplication);

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            if (htParams != null)
                            {
                                foreach (DictionaryEntry entry in htParams)
                                {
                                    com.Parameters.AddWithValue(Convert.ToString(entry.Key), entry.Value);
                                    strLogParameters.Append(string.Concat(" ", Convert.ToString(entry.Key), ": ", Convert.ToString(entry.Value), ","));
                                }
                            }
                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);
                            inColumns = dataTable.Columns;

                            foreach (DataColumn inColumn in inColumns)
                                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable : ADODB.FieldAttributeEnum.adFldUnspecified, null);

                            recordSet.Open(System.Reflection.Missing.Value
                            , System.Reflection.Missing.Value
                            , ADODB.CursorTypeEnum.adOpenStatic
                            , ADODB.LockTypeEnum.adLockOptimistic, 0);
                            object temp = "";
                            foreach (DataRow drRow in dataTable.Rows)
                            {
                                recordSet.AddNew(System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value);

                                for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                                {

                                    if (inColumns[columnIndex].DataType == typeof(System.Guid))
                                    {
                                        resultFields[columnIndex].Value = "{" + drRow[columnIndex] + "}";
                                    }
                                    else
                                    {
                                        resultFields[columnIndex].Value = drRow[columnIndex];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetSp", ex.Message, string.Concat("Error occur in OpenRecordsetSp. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                dataTable = null;
                inColumns = null;
                dataAdadpter = null;
                objWriteLog = null;
                strLogParameters = null;
            }
            return recordSet;
        }

        /// <summary>
        /// Return the partner rows in DataSet 
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public DataSet OpenDataSetSP(string strProcedureName, Hashtable htParams, string strApplication)
        {
            DataSet dtPartnerRecord = null;
            SqlDataAdapter daPartnerReader = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                dtPartnerRecord = new DataSet();
                daPartnerReader = new SqlDataAdapter();

                using (SqlConnection sqlConnection = new SqlConnection(GetConnectionString(strApplication)))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(strProcedureName, sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = GetConnectionTimeOut();
                        sqlConnection.Open();

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                sqlCommand.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }
                        daPartnerReader.SelectCommand = sqlCommand;
                        daPartnerReader.Fill(dtPartnerRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenDataSetSP", ex.Message, string.Concat("Error occur in OpenDataSetSP. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            return dtPartnerRecord;
        }

        /// <summary>
        /// OpenRecordSP and returns the RS data using ADODB Connection
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSp(string strProcedureName, string strApplication)
        {
            Recordset recordSet = null;
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            DataColumnCollection inColumns = null;
            SqlDataAdapter dataAdadpter = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                recordSet = new Recordset();
                dataTable = new DataTable();

                recordSet.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                ADODB.Fields resultFields = recordSet.Fields;

                strConnectionString = GetConnectionString(strApplication);

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);
                            inColumns = dataTable.Columns;

                            foreach (DataColumn inColumn in inColumns)
                                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable : ADODB.FieldAttributeEnum.adFldUnspecified, null);

                            recordSet.Open(System.Reflection.Missing.Value
                            , System.Reflection.Missing.Value
                            , ADODB.CursorTypeEnum.adOpenStatic
                            , ADODB.LockTypeEnum.adLockOptimistic, 0);
                            object temp = "";
                            foreach (DataRow drRow in dataTable.Rows)
                            {
                                recordSet.AddNew(System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value);

                                for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                                {

                                    if (inColumns[columnIndex].DataType == typeof(System.Guid))
                                    {
                                        resultFields[columnIndex].Value = "{" + drRow[columnIndex] + "}";
                                    }
                                    else
                                    {
                                        resultFields[columnIndex].Value = drRow[columnIndex];
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetSp", ex.Message, string.Concat("Error occur in OpenRecordsetSp without htparams. SP name - ", strProcedureName));
                throw ex;
            }
            finally
            {
                dataAdadpter = null;
                inColumns = null;
                dataTable = null;
                objWriteLog = null;
            }
            return recordSet;
        }

        /// <summary>
        /// Use to get the session value from DB
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string GetSessionValue(string strProcedureName, Hashtable htParams, string strApplication)
        {
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            SqlDataAdapter dataAdadpter = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strLogParameters = new StringBuilder();

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            dataTable = new DataTable();
                            if (htParams != null)
                            {
                                foreach (DictionaryEntry entry in htParams)
                                {
                                    com.Parameters.AddWithValue(Convert.ToString(entry.Key), entry.Value);
                                    strLogParameters.Append(string.Concat(" ", Convert.ToString(entry.Key), ": ", Convert.ToString(entry.Value), ","));
                                }
                            }

                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);

                            if (dataTable.Rows.Count > 0)
                                return dataTable.Rows[0]["SessionValue"].ToString();
                            else
                                return "";
                        }
                        else
                        {
                            throw new Exception("Connection Failed");
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - GetSessionValue", ex.Message, string.Concat("Error occur in GetSessionValue. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }

        /// <summary>
        /// OpenRecordsetSpNoReturn
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSpNoReturn(string strProcedureName, Hashtable htParams, string strApplication)
        {
            Recordset recordSet = null;
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            DataColumnCollection inColumns = null;
            SqlDataAdapter dataAdadpter = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                recordSet = new Recordset();
                dataTable = new DataTable();
                strLogParameters = new StringBuilder();

                recordSet.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                ADODB.Fields resultFields = recordSet.Fields;

                strConnectionString = GetConnectionString(strApplication);

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            if (htParams != null)
                            {
                                foreach (DictionaryEntry entry in htParams)
                                {
                                    com.Parameters.AddWithValue(Convert.ToString(entry.Key), entry.Value);
                                    strLogParameters.Append(string.Concat(" ", Convert.ToString(entry.Key), ": ", Convert.ToString(entry.Value), ","));
                                }
                            }

                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);
                            inColumns = dataTable.Columns;

                            foreach (DataColumn inColumn in inColumns)
                                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable : ADODB.FieldAttributeEnum.adFldUnspecified, null);

                            recordSet.Open(System.Reflection.Missing.Value
                            , System.Reflection.Missing.Value
                            , ADODB.CursorTypeEnum.adOpenStatic
                            , ADODB.LockTypeEnum.adLockOptimistic, 0);

                            foreach (DataRow drRow in dataTable.Rows)
                            {
                                recordSet.AddNew(System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value);

                                for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                                    resultFields[columnIndex].Value = drRow[columnIndex];
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetSpNoReturn", ex.Message, string.Concat("Error occur in OpenRecordsetSpNoReturn. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                dataTable = null;
                dataAdadpter = null;
                inColumns = null;
                strLogParameters = null;
                objWriteLog = null;
            }
            return recordSet;
        }

        /// <summary>
        /// OpenRecordsetNamedParams
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetNamedParams(string strProcedureName, Hashtable htParams, string strApplication)
        {
            Recordset recordSet = null;
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            DataColumnCollection inColumns = null;
            SqlDataAdapter dataAdadpter = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                recordSet = new Recordset();
                dataTable = new DataTable();
                strLogParameters = new StringBuilder();

                recordSet.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                ADODB.Fields resultFields = recordSet.Fields;

                strConnectionString = GetConnectionString(strApplication);

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            if (htParams != null)
                            {
                                foreach (DictionaryEntry entry in htParams)
                                {
                                    com.Parameters.AddWithValue(Convert.ToString(entry.Key), entry.Value);
                                    strLogParameters.Append(string.Concat(" ", Convert.ToString(entry.Key), ": ", Convert.ToString(entry.Value), ","));
                                }
                            }

                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);
                            inColumns = dataTable.Columns;

                            foreach (DataColumn inColumn in inColumns)
                                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable : ADODB.FieldAttributeEnum.adFldUnspecified, null);

                            recordSet.Open(System.Reflection.Missing.Value
                            , System.Reflection.Missing.Value
                            , ADODB.CursorTypeEnum.adOpenStatic
                            , ADODB.LockTypeEnum.adLockOptimistic, 0);

                            foreach (DataRow drRow in dataTable.Rows)
                            {
                                recordSet.AddNew(System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value);

                                for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                                    resultFields[columnIndex].Value = drRow[columnIndex];
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetNamedParams", ex.Message, string.Concat("Error occur in OpenRecordsetNamedParams. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                dataAdadpter = null;
                dataTable = null;
                inColumns = null;
                objWriteLog = null;
                strLogParameters = null;
            }
            return recordSet;
        }

        /// <summary>
        /// OpenRecordsetSpNpCol
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSpNpCol(string strProcedureName, Hashtable htParams, string strApplication)
        {
            Recordset recordSet = null;
            string strConnectionString = string.Empty, strUserName = string.Empty, strPassword = string.Empty;
            DataTable dataTable = null;
            DataColumnCollection inColumns = null;
            SqlDataAdapter dataAdadpter = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                recordSet = new Recordset();
                dataTable = new DataTable();
                strLogParameters = new StringBuilder();

                recordSet.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                ADODB.Fields resultFields = recordSet.Fields;

                strConnectionString = GetConnectionString(strApplication);

                using (SqlConnection conn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand com = new SqlCommand(strProcedureName, conn))
                    {
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandTimeout = GetConnectionTimeOut();
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            if (htParams != null)
                            {
                                foreach (DictionaryEntry entry in htParams)
                                {
                                    com.Parameters.AddWithValue(Convert.ToString(entry.Key), entry.Value);
                                    strLogParameters.Append(string.Concat(" ", Convert.ToString(entry.Key), ": ", Convert.ToString(entry.Value), ","));
                                }
                            }

                            dataAdadpter = new SqlDataAdapter();
                            dataAdadpter.SelectCommand = com;
                            dataAdadpter.Fill(dataTable);
                            inColumns = dataTable.Columns;

                            foreach (DataColumn inColumn in inColumns)
                                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable : ADODB.FieldAttributeEnum.adFldUnspecified, null);

                            recordSet.Open(System.Reflection.Missing.Value
                            , System.Reflection.Missing.Value
                            , ADODB.CursorTypeEnum.adOpenStatic
                            , ADODB.LockTypeEnum.adLockOptimistic, 0);

                            foreach (DataRow drRow in dataTable.Rows)
                            {
                                recordSet.AddNew(System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value);

                                for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                                    resultFields[columnIndex].Value = drRow[columnIndex];
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetSpNpCol", ex.Message, string.Concat("Error occur in OpenRecordsetSpNpCol. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                dataTable = null;
                inColumns = null;
                dataAdadpter = null;
                objWriteLog = null;
                strLogParameters = null;
            }
            return recordSet;
        }

        /// <summary>
        /// OpenRecordsetAsClientSideXML
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string OpenRecordsetAsClientSideXML(string strProcedureName, Hashtable htParams, string strApplication)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ExecuteSpNpColXML:");
            string returnValue = string.Empty,
        strWSProcName = string.Empty,
        strConnectionString = string.Empty,
        strReturnValue = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                xmlDocument = new XmlDocument();
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(strWSProcName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = GetConnectionTimeOut();

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry De in htParams)
                            {
                                cmd.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                            }
                        }

                        sqlConn.Open();
                        xmlDocument.Load(cmd.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnValue = xmlDocument.OuterXml;
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetAsClientSideXML", ex.Message, string.Concat("Error occur in OpenRecordsetAsClientSideXML. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                xmlDocument = null;
                strLogParameters = null;
            }
            return strReturnValue;
        }

        /// <summary>
        /// ExecuteSpNamedParams
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSpNamedParams(string strProcedureName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }
                        sqlConnection.Open();

                        bool isProcessed = false;
                        int iRetry = 0;
                        while (iRetry < 10 && !isProcessed)
                        {
                            try
                            {  
                                if(isDebug)
                                    objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", "", string.Concat("ExecuteSpNamedParams. SP name - ", strProcedureName, " Parameters: ", strLogParameters));

                                command.ExecuteNonQuery();
                                isProcessed = true;
                            }
                            catch (Exception ex)
                            {
                                switch (iRetry)
                                {
                                    case 0:
                                        //objWriteLog.Send(ex, string.Concat("WARNING: ExecuteSpNamedParams - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", string.Concat("ExecuteSpNamedParams - 1st Attempt failed.  Reason: ", ex.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(ex, string.Concat("ERROR: ExecuteSpNamedParams - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", string.Concat("SERIOUS - ExecuteSpNamedParams - 5th Attempt failed. Reason: ", ex.Message), "");
                                        throw new Exception(ex.Message);
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", string.Concat("ExecuteSpNamedParams -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                        break;
                                }
                                isProcessed = false;
                            }
                            if (isProcessed)
                                break;
                            System.Threading.Thread.Sleep(iRetryTimeOut);
                            iRetry++;
                        }
                        //command.ExecuteNonQuery();
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", ex.Message, string.Concat("Error occur in ExecuteSpNamedParams. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSpNamedParams for Payment execution. Use for the Acoouting.
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSpNamedParams(ref SqlConnection sqlConnection, ref SqlTransaction sqlTransaction, string strProcedureName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                {
                    SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                    sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = GetConnectionTimeOut();
                    command.Transaction = sqlTransaction;
                    command.Parameters.Add(sqlReturnParm);

                    if (htParams != null)
                    {
                        foreach (DictionaryEntry dictionaryEntry in htParams)
                        {
                            command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                            strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                        }
                    }
                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                    lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                }

            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParams", ex.Message, string.Concat("Error occur in ExecuteSpNamedParams with Begin and Commit. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSp
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSp(string strProcedureName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strConnectionString = string.Empty, strWSProcName = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }

                        sqlConnection.Open();
                        bool isProcessed = false;
                        int iRetry = 0;

                        while (iRetry < 10 && !isProcessed)
                        {
                            try
                            {
                                command.ExecuteNonQuery();
                                isProcessed = true;
                            }
                            catch (Exception ex)
                            {
                                switch (iRetry)
                                {
                                    case 0:
                                        //objWriteLog.Send(ex, string.Concat("WARNING: ExecuteSp - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSp", string.Concat("ExecuteSp - 1st Attempt failed.  Reason: ", ex.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(ex, string.Concat("ERROR: ExecuteSp - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSp", string.Concat("SERIOUS - ExecuteSp - 5th Attempt failed. Reason: ", ex.Message), "");
                                        throw new Exception(ex.Message);
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSp", string.Concat("ExecuteSp -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                        break;
                                }
                                isProcessed = false;
                            }
                            if (isProcessed)
                                break;
                            System.Threading.Thread.Sleep(iRetryTimeOut);
                            iRetry++;
                        }
                        //command.ExecuteNonQuery();
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSp", ex.Message, string.Concat("Error occur in ExecuteSp. SP name - ", strProcedureName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSpNpColXML
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="colparams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSpNpColXML(string strProcName, Hashtable colparams, string strApplication)
        {
            string returnValue = string.Empty,
                strWSProcName = string.Empty,
                strConnectionString = string.Empty,
            strReturnValue = string.Empty;
            StringBuilder strLogParameters = null;
            XmlDocument xmlDocument = new XmlDocument();
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(strWSProcName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = GetConnectionTimeOut();
                        if (colparams != null)
                        {
                            foreach (DictionaryEntry De in colparams)
                            {
                                cmd.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                            }
                        }

                        sqlConn.Open();

                        if (strWSProcName == "uspClaimCarrierUpdDetail")
                            cmd.ExecuteNonQuery();
                        else
                            xmlDocument.Load(cmd.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnValue = xmlDocument.OuterXml;
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNpColXML", ex.Message, string.Concat("SPName: ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                xmlDocument = null;
                objWriteLog = null;
                strLogParameters = null;
            }
            return strReturnValue;
        }

        /// <summary>
        /// ExecuteSpNpColXML
        /// </summary>
        /// <param name="sqlConn"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="strProcName"></param>
        /// <param name="colparams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSpNpColXML(ref SqlConnection sqlConn, ref SqlTransaction sqlTransaction, string strProcName, Hashtable colparams, string strApplication)
        {
            string returnValue = string.Empty,
                strWSProcName = string.Empty,
                strConnectionString = string.Empty,
            strReturnValue = string.Empty;
            XmlDocument xmlDocument = new XmlDocument();
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(strWSProcName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = GetConnectionTimeOut();
                        cmd.Transaction = sqlTransaction;

                        if (colparams != null)
                        {
                            foreach (DictionaryEntry De in colparams)
                            {
                                cmd.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                            }
                        }

                        xmlDocument.Load(cmd.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnValue = xmlDocument.OuterXml;
                    }
                }
                catch (Exception ex)
                {
                    sqlTransaction.Rollback();
                    throw ex;
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNpColXML", ex.Message, string.Concat("Error occur in ExecuteSpNpColXML with Begin and Commit. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                xmlDocument = null;
                strLogParameters = null;
            }
            return strReturnValue;
        }

        /// <summary>
        /// ExecuteSpNpColXMLClaimUserDetailUpdate
        /// </summary>
        /// <param name="sqlConn"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="strProcName"></param>
        /// <param name="colparams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSpNpColXMLClaimUserDetailUpdate(ref SqlConnection sqlConn, ref SqlTransaction sqlTransaction, string strProcName, Hashtable colparams, string strApplication)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ExecuteSpNpColXMLClaimUserDetailUpdate:");
            string returnValue = string.Empty,
                strWSProcName = string.Empty,
                strConnectionString = string.Empty,
            strReturnValue = string.Empty;
            XmlDocument xmlDocument = new XmlDocument();
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(strWSProcName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = GetConnectionTimeOut();
                        cmd.Transaction = sqlTransaction;

                        if (colparams != null)
                        {
                            foreach (DictionaryEntry De in colparams)
                            {
                                if (Convert.ToString(De.Key) == "@OfficeID" && (Convert.ToString(De.Value) == "" || De.Value == null))
                                    cmd.Parameters.AddWithValue(Convert.ToString(De.Key), System.Data.SqlTypes.SqlInt32.Null);
                                else if (Convert.ToString(De.Key) == "@SupervisorUserID" && (Convert.ToString(De.Value) == "" || De.Value == null))
                                    cmd.Parameters.AddWithValue(Convert.ToString(De.Key), System.Data.SqlTypes.SqlInt32.Null);
                                else
                                    cmd.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);

                                strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                            }
                        }

                        xmlDocument.Load(cmd.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnValue = xmlDocument.OuterXml;
                    }
                }
                catch (SqlException ex)
                {
                    //sqlTransaction.Rollback();
                    throw ex;
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNpColXMLClaimUserDetailUpdate", ex.Message, string.Concat("Error occur in ExecuteSpNpColXMLClaimUserDetailUpdate with Begin and Commit. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strReturnValue;
        }

        /// <summary>
        /// ExecuteSpNamedParamsXML
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSpNamedParamsXML(string strProcName, Hashtable htParams, string strApplication)
        {
            string strReturnXML = string.Empty, strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                xmlDocument = new XmlDocument();
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(strWSProcName, sqlConn))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = GetConnectionTimeOut();
                        if (htParams != null)
                        {
                            foreach (DictionaryEntry De in htParams)
                            {
                                sqlCommand.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                            }
                        }

                        sqlConn.Open();
                        bool isProcessed = false;
                        int iRetry = 0;
                        while (iRetry < 10 && !isProcessed)
                        {
                            try
                            {
                                xmlDocument.Load(sqlCommand.ExecuteXmlReader());
                                isProcessed = true;
                            }
                            catch (Exception ex)
                            {
                                switch (iRetry)
                                {
                                    case 0:
                                        //objWriteLog.Send(ex, string.Concat("WARNING: ExecuteSpNamedParamsXML - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", string.Concat("ExecuteSpNamedParamsXML - 1st Attempt failed.  Reason: ", ex.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(ex, string.Concat("ERROR: ExecuteSpNamedParamsXML - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", string.Concat("SERIOUS - ExecuteSpNamedParamsXML - 5th Attempt failed. Reason: ", ex.Message), "");
                                        throw new Exception(ex.Message);
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", string.Concat("ExecuteSpNamedParamsXML -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                        break;
                                }
                                isProcessed = false;
                            }
                            if (isProcessed)
                                break;
                            System.Threading.Thread.Sleep(iRetryTimeOut);
                            iRetry++;
                        }
                        //xmlDocument.Load(sqlCommand.ExecuteXmlReader());
                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnXML = xmlDocument.OuterXml;
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in ExecuteSpNamedParamsXML. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
                xmlDocument = null;
            }
            return strReturnXML;
        }

        /// <summary>
        /// ExecuteSpNamedParamsXML
        /// </summary>
        /// <param name="sqlConn"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="strProcName"></param>
        /// <param name="colparams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSpNamedParamsXML(ref SqlConnection sqlConn, ref SqlTransaction sqlTransaction, string strProcName, Hashtable colparams, string strApplication)
        {
            string strReturnXML = string.Empty, strWSProcName = string.Empty,
               strConnectionString = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                xmlDocument = new XmlDocument();
                strLogParameters = new StringBuilder();

                using (SqlCommand sqlCommand = new SqlCommand(strWSProcName, sqlConn))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = GetConnectionTimeOut();
                    sqlCommand.Transaction = sqlTransaction;

                    if (colparams != null)
                    {
                        foreach (DictionaryEntry De in colparams)
                        {
                            sqlCommand.Parameters.AddWithValue(Convert.ToString(De.Key), De.Value);
                            strLogParameters.Append(string.Concat(" ", Convert.ToString(De.Key), ": ", Convert.ToString(De.Value), ","));
                        }
                    }

                    //sqlConn.Open();
                    xmlDocument.Load(sqlCommand.ExecuteXmlReader());

                    if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                        strReturnXML = xmlDocument.OuterXml;

                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in ExecuteSpNamedParamsXML with Begin and Commit. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                xmlDocument = null;
                strLogParameters = null;
            }
            return strReturnXML;
        }

        /// <summary>
        /// ExecuteSpNpCol
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSpNpCol(string strProcName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }

                        sqlConnection.Open();

                        bool isPartnerProcessed = false;
                        int isPartnerRetry = 0;
                        while (isPartnerRetry < 10 && !isPartnerProcessed)
                        {
                            try
                            {
                                command.ExecuteNonQuery();
                                isPartnerProcessed = true;
                            }
                            catch (Exception ex)
                            {
                                switch (isPartnerRetry)
                                {
                                    case 0:
                                        // objWriteLog.Send(ex, string.Concat("WARNING: DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", string.Concat("DataAccessor.ExecuteSpNpCol - ", strProcName), string.Concat(strProcName, " - 1st Attempt failed.  Reason: ", ex.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(ex, string.Concat("ERROR: ", strProcName, " - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", string.Concat("DataAccessor.ExecuteSpNpCol - ", strProcName), string.Concat("SERIOUS -", strProcName, " - 5th Attempt failed. Reason: ", ex.Message), "");
                                        throw new Exception("An unexpected error has caused a delay in executing the SP.");
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", string.Concat("DataAccessor.ExecuteSpNpCol - ", strProcName), string.Concat(strProcName, " -  ", isPartnerRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                        break;
                                }
                                isPartnerProcessed = false;
                            }
                            if (isPartnerProcessed)
                                break;
                            System.Threading.Thread.Sleep(iRetryTimeOut);
                            isPartnerRetry++;
                        }
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in ExecuteSpNpCol. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSpNpCol
        /// </summary>
        /// <param name="sqlConn"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="strProcName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSpNpCol(ref SqlConnection sqlConn, ref SqlTransaction sqlTransaction, string strProcName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                try
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConn))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Transaction = sqlTransaction;
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }
                        sqlConn.Open();
                        bool isPartnerProcessed = false;
                        int isPartnerRetry = 0;
                        while (isPartnerRetry < 10 && !isPartnerProcessed)
                        {
                            try
                            {
                                command.ExecuteNonQuery();
                                isPartnerProcessed = true;
                            }
                            catch (Exception ex)
                            {
                                switch (isPartnerRetry)
                                {
                                    case 0:
                                        // objWriteLog.Send(ex, string.Concat("WARNING: DocumentUpload - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "WARNING", string.Concat("DataAccessor.ExecuteSpNpCol(SQLTransaction) -", strProcName), string.Concat(strProcName, " - 1st Attempt failed.  Reason: ", ex.Message), "");
                                        break;
                                    case 10:
                                        //5th attempt failure, notify as Error
                                        objWriteLog.Send(ex, string.Concat("ERROR: ", strProcName, " - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                        objWriteLog.LogEvent("COMConversion", "ERROR", string.Concat("DataAccessor.ExecuteSpNpCol(SQLTransaction) -", strProcName), string.Concat("SERIOUS -", strProcName, " - 5th Attempt failed. Reason: ", ex.Message), "");
                                        throw new Exception("An unexpected error has caused a delay in executing the SP.");
                                        break;
                                    default:
                                        objWriteLog.LogEvent("COMConversion", "WARNING", string.Concat("DataAccessor.ExecuteSpNpCol.cs -", strProcName), string.Concat(strProcName, " -  ", isPartnerRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                        break;
                                }
                                isPartnerProcessed = false;
                            }
                            if (isPartnerProcessed)
                                break;
                            System.Threading.Thread.Sleep(iRetryTimeOut);
                            isPartnerRetry++;
                        }
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
                catch (SqlException ex)
                {
                    sqlTransaction.Rollback();
                    throw ex;
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in ExecuteSpNpCol with Begin and Commit. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSession
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string ExecuteSession(string strProcedureName, Hashtable htParams, string strApplication)
        {
            string strReturn = string.Empty;
            string strConnectionString = string.Empty, strWSProcName = string.Empty;
            SqlDataReader sqlDataReader = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }

                        sqlConnection.Open();
                        sqlDataReader = command.ExecuteReader();
                        sqlDataReader.Read();
                        strReturn = sqlDataReader[0].ToString();
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in ExecuteSession. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                sqlDataReader = null;
                strLogParameters = null;
            }
            return strReturn;
        }

        /// <summary>
        /// SessionExists
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public bool SessionExists(string strProcedureName, Hashtable htParams, string strApplication)
        {
            string strReturn = string.Empty;
            string strConnectionString = string.Empty, strWSProcName = string.Empty;
            SqlDataReader sqlDataReader = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }

                        sqlConnection.Open();
                        sqlDataReader = command.ExecuteReader();
                        return sqlDataReader.HasRows;
                    }
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - SessionExists", ex.Message, string.Concat("Error occur in SessionExists. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }


        /// <summary>
        /// ExecuteSpNpCol execute for Claimpoint Submission
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTrans"></param>
        /// <param name="strProcName"></param>
        /// <param name="htParams"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public long ExecuteSpNpColClaimInsert(ref SqlConnection sqlConnection, ref SqlTransaction sqlTrans, string strProcName, Hashtable htParams, string strApplication)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
                strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();
                try
                {
                    // objWriteLog = new WriteLog();
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandTimeout = GetConnectionTimeOut();
                        command.Parameters.Add(sqlReturnParm);
                        command.Transaction = sqlTrans;

                        if (htParams != null)
                        {
                            foreach (DictionaryEntry dictionaryEntry in htParams)
                            {
                                command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                                strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                            }
                        }

                        command.ExecuteNonQuery();
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }

                }
                catch (SqlException ex)
                {
                    //sqlTrans.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSpNpColClaimInsert", ex.Message, string.Concat("Error occur in ExecuteSpNpColClaimInsert with Begin and Commit. SP name - ", strWSProcName, "Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ExecuteSP
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTrans"></param>
        /// <param name="strProcName"></param>
        /// <param name="htParams"></param>
        /// <returns></returns>
        public long ExecuteSP(ref SqlConnection sqlConnection, ref SqlTransaction sqlTrans, string strProcName, Hashtable htParams)
        {
            long lngSPreturnID = 0;
            string strWSProcName = string.Empty,
               strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                {
                    SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                    sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = GetConnectionTimeOut();
                    command.Parameters.Add(sqlReturnParm);
                    command.Transaction = sqlTrans;

                    if (htParams != null)
                    {
                        foreach (DictionaryEntry dictionaryEntry in htParams)
                        {
                            command.Parameters.AddWithValue(Convert.ToString(dictionaryEntry.Key), dictionaryEntry.Value);
                            strLogParameters.Append(string.Concat(" ", Convert.ToString(dictionaryEntry.Key), ": ", Convert.ToString(dictionaryEntry.Value), ","));
                        }
                    }
                    bool isProcessed = false;
                    int iRetry = 0;
                    while (iRetry < 10 && !isProcessed)
                    {
                        try
                        {
                            command.ExecuteNonQuery();
                            isProcessed = true;
                        }
                        catch (Exception ex)
                        {
                            switch (iRetry)
                            {
                                case 0:
                                    //objWriteLog.Send(ex, string.Concat("WARNING: ExecuteSP - 1st Attempt failed.  Reason: ", ex.Message), "Warning: LynxAPDComponentServices");
                                    objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSP", string.Concat("ExecuteSP - 1st Attempt failed.  Reason: ", ex.Message), "");
                                    break;
                                case 10:
                                    //5th attempt failure, notify as Error
                                    objWriteLog.Send(ex, string.Concat("ERROR: ExecuteSP - 10th Attempt failed.  Reason: ", ex.Message), "Error: LynxAPDComponentServices");
                                    objWriteLog.LogEvent("COMConversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSP", string.Concat("SERIOUS - ExecuteSP - 5th Attempt failed. Reason: ", ex.Message), "");
                                    throw new Exception(ex.Message);
                                    break;
                                default:
                                    objWriteLog.LogEvent("COMConversion", "WARNING", "DataAccessor.CDataAccessor.cs - ExecuteSP", string.Concat("ExecuteSP -  ", iRetry, " Attempt failed.", "Reason: ", ex.Message), "");
                                    break;
                            }
                            isProcessed = false;
                        }
                        if (isProcessed)
                            break;
                        System.Threading.Thread.Sleep(iRetryTimeOut);
                        iRetry++;
                    }
                    //command.ExecuteNonQuery();
                    lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                }
            }
            catch (SqlException ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ExecuteSP", ex.Message, string.Concat("Error occur in ExecuteSP with Begin and Commit. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSPreturnID;
        }

        /// <summary>
        /// ChangeSPXMLtoWSXML
        /// </summary>
        /// <param name="strSPName"></param>
        /// <returns></returns>
        private string ChangeSPXMLtoWSXML(string strSPName)
        {
            string strReturnSPName = string.Empty;

            try
            {
                if (!StringExtensions.IsNullOrWhiteSpace(strSPName))
                {
                    if (!strSPName.EndsWith("WSXML"))
                    {
                        if (strSPName.EndsWith("XML"))
                            strReturnSPName = strSPName.Replace("XML", "WSXML");
                        else if (strSPName == "uspAdmUserApplicationInsDetail")
                            strReturnSPName = "uspAdmUserApplicationInsDetailWSXML";
                        else if (strSPName == "uspAdmUserSetStatus")
                            strReturnSPName = "uspAdmUserSetStatusWSXML";
                        else if (strSPName == "uspAdmUserUpdDetail")
                            strReturnSPName = "uspAdmUserUpdDetailWSXML";
                        else if (strSPName == "uspAdmUserApplicationUpdDetail")
                            strReturnSPName = "uspAdmUserApplicationUpdDetailWSXML";
                        else if (strSPName == "uspAdmUserInsDetail")
                            strReturnSPName = "uspAdmUserInsDetailWSXML";
                        else if (strSPName == "uspClientVerifyAssignmentTypeXml")
                            strReturnSPName = "uspClientVerifyAssignmentTypeWSXml";
                        else
                            strReturnSPName = strSPName;
                    }
                    else
                    {
                        strReturnSPName = strSPName;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnSPName;
        }

        #endregion

        #region Function call from COMServer dll

        /// <summary>
        /// AssignShop Function call from COMServer dll
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngAssignmentID"></param>
        /// <param name="strApplication"></param>
        /// <returns></returns>
        public string AssignShopExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, string strApplication)
        {
            string strAssignShop = string.Empty;
            string strConnectionString = string.Empty;
            string strWSProcName = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                xmlDocument = new XmlDocument();
                strConnectionString = GetConnectionString(strApplication);
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                strLogParameters = new StringBuilder();

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@AssignmentID", lngAssignmentID);

                        strLogParameters.Append(string.Concat("@AssignmentID", Convert.ToString(lngAssignmentID)));
                        sqlConnection.Open();
                        xmlDocument.Load(command.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strAssignShop = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - AssignShopExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in AssignShopExecuteSpNamedParamsXML. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strAssignShop;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngLynxID"></param>
        /// <returns></returns>
        public string AnalystLogonIDExecuteSpNamedParamsXML(string strProcName, long lngLynxID)
        {
            string strEntityOwnersXML = string.Empty;
            string strConnectionString = string.Empty;
            string strWSProcName = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                xmlDocument = new XmlDocument();
                strLogParameters = new StringBuilder();

                strConnectionString = GetConnectionString("APD");
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@LynxID", lngLynxID);
                        strLogParameters.Append(string.Concat("@LynxID", Convert.ToString(lngLynxID)));
                        sqlConnection.Open();
                        xmlDocument.Load(command.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strEntityOwnersXML = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - AnalystLogonIDExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in AnalystLogonIDExecuteSpNamedParamsXML. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strEntityOwnersXML;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngAssignmentID"></param>
        /// <param name="blnWarrantyAssignment"></param>
        /// <returns></returns>
        public string FaxTemplateExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, bool blnWarrantyAssignment)
        {
            string strFaxTemplateXML = string.Empty;
            string strConnectionString = string.Empty;
            string strWSProcName = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                xmlDocument = new XmlDocument();
                strLogParameters = new StringBuilder();

                strConnectionString = GetConnectionString("APD");
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@AssignmentID", lngAssignmentID);
                        command.Parameters.AddWithValue("@WarrantyFlag", blnWarrantyAssignment);

                        strLogParameters.Append(string.Concat("@AssignmentID", Convert.ToString(lngAssignmentID)));
                        strLogParameters.Append(string.Concat("@WarrantyFlag", Convert.ToString(blnWarrantyAssignment)));

                        sqlConnection.Open();
                        xmlDocument.Load(command.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strFaxTemplateXML = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - FaxTemplateExecuteSpNamedParamsXML", ex.Message, string.Concat("Error occur in FaxTemplateExecuteSpNamedParamsXML. SP name - ", strWSProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strFaxTemplateXML;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="strPPGAssignment"></param>
        public void TransmissionPartnerExecuteSp(string strProcName, string strPPGAssignment)
        {
            long lngSPreturnID = 0;
            string strConnectionString = string.Empty, strWSProcName = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                strConnectionString = GetConnectionString("Partner");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@APDRequestXML", strPPGAssignment);

                        strLogParameters.Append(string.Concat("@APDRequestXML", Convert.ToString(strPPGAssignment)));

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - TransmissionPartnerExecuteSp", ex.Message, string.Concat("Error occur in TransmissionPartnerExecuteSp. SP name - ", strProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngUserID"></param>
        /// <param name="strPertainsTo"></param>
        /// <param name="lngLynID"></param>
        /// <param name="intNotifyEvent"></param>
        /// <param name="strDirectionalCD"></param>
        /// <param name="strSqlNow"></param>
        /// <param name="strImageLocation"></param>
        /// <param name="strDocumentSource"></param>
        /// <param name="strDocumentType"></param>
        /// <param name="strImageType"></param>
        /// <param name="strClaimAspectServiceChannelID"></param>
        /// <param name="blnWarrantyFlag"></param>
        public void DocumentInsertExecuteSpNamedParams(string strProcName, long lngUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strClaimAspectServiceChannelID, bool blnWarrantyFlag)
        {
            long lngSendWorkFlowEvent = 0;
            string strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                strConnectionString = GetConnectionString("Partner");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@UserID", lngUserID);
                        command.Parameters.AddWithValue("@PertainsTo", strPertainsTo);
                        command.Parameters.AddWithValue("@LynxID", lngLynID);
                        command.Parameters.AddWithValue("@NotifyEvent", intNotifyEvent);
                        command.Parameters.AddWithValue("@DirectionalCD", strDirectionalCD);
                        command.Parameters.AddWithValue("@CreatedDate", strSqlNow);
                        command.Parameters.AddWithValue("@ImageDate", strSqlNow);
                        command.Parameters.AddWithValue("@ImageLocation", strImageLocation);
                        command.Parameters.AddWithValue("@DocumentSource", strDocumentSource);
                        command.Parameters.AddWithValue("@DocumentType", strDocumentType);
                        command.Parameters.AddWithValue("@ImageType", strImageType);
                        command.Parameters.AddWithValue("@ClaimAspectServiceChannelID", strClaimAspectServiceChannelID);
                        command.Parameters.AddWithValue("@WarrantyFlag", blnWarrantyFlag);

                        strLogParameters.Append(string.Concat("@UserID", Convert.ToString(lngUserID)));
                        strLogParameters.Append(string.Concat("@PertainsTo", Convert.ToString(strPertainsTo)));
                        strLogParameters.Append(string.Concat("@LynxID", Convert.ToString(lngLynID)));
                        strLogParameters.Append(string.Concat("@NotifyEvent", Convert.ToString(intNotifyEvent)));
                        strLogParameters.Append(string.Concat("@DirectionalCD", Convert.ToString(strDirectionalCD)));
                        strLogParameters.Append(string.Concat("@CreatedDate", Convert.ToString(strSqlNow)));
                        strLogParameters.Append(string.Concat("@ImageDate", Convert.ToString(strSqlNow)));
                        strLogParameters.Append(string.Concat("@ImageLocation", Convert.ToString(strImageLocation)));
                        strLogParameters.Append(string.Concat("@DocumentSource", Convert.ToString(strDocumentSource)));
                        strLogParameters.Append(string.Concat("@DocumentType", Convert.ToString(strDocumentType)));
                        strLogParameters.Append(string.Concat("@ImageType", Convert.ToString(strImageType)));
                        strLogParameters.Append(string.Concat("@ClaimAspectServiceChannelID", Convert.ToString(strClaimAspectServiceChannelID)));
                        strLogParameters.Append(string.Concat("@WarrantyFlag", Convert.ToString(blnWarrantyFlag)));

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSendWorkFlowEvent = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - DocumentInsertExecuteSpNamedParams", ex.Message, string.Concat("Error occur in DocumentInsertExecuteSpNamedParams. SP name - ", strProcName, " Parameters: ", strLogParameters));
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngAssignmentID"></param>
        /// <param name="strAssignmentCode"></param>
        public void WarrantyAssignmentExecuteSpNamedParams(string strProcName, long lngAssignmentID, string strAssignmentCode)
        {
            long lngSendWorkFlowEvent = 0;
            string strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                strConnectionString = GetConnectionString("APD");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@AssignmentID", lngAssignmentID);
                        command.Parameters.AddWithValue("@StatusCD", strAssignmentCode);

                        strLogParameters.Append(string.Concat("@AssignmentID ", lngAssignmentID));
                        strLogParameters.Append(string.Concat("@StatusCD", strAssignmentCode));

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSendWorkFlowEvent = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - WarrantyAssignmentExecuteSpNamedParams", string.Concat("Error occur in WarrantyAssignmentExecuteSpNamedParams. SP name - ", strProcName, " Parameters: ", strLogParameters), ex.Message);
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="strEventID"></param>
        /// <param name="strLynxID"></param>
        /// <param name="strPertainsTo"></param>
        /// <param name="strKey"></param>
        /// <param name="strDescription"></param>
        /// <param name="intUserID"></param>
        /// <param name="strServiceChannelCD"></param>
        /// <returns></returns>
        public long SendWorkFlowEventExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strKey, string strDescription, int intUserID, string strServiceChannelCD)
        {
            long lngSendWorkFlowEvent = 0;
            string strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                strConnectionString = GetConnectionString("APD");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@EventID", strEventID);
                        command.Parameters.AddWithValue("@LynxID", strLynxID);
                        command.Parameters.AddWithValue("@PertainsTo", strPertainsTo);
                        command.Parameters.AddWithValue("@Key", strKey);
                        command.Parameters.AddWithValue("@Description", strDescription);
                        command.Parameters.AddWithValue("@UserID", intUserID);
                        command.Parameters.AddWithValue("@ServiceChannelCD", strServiceChannelCD);

                        strLogParameters.Append(string.Concat("@EventID", strEventID));
                        strLogParameters.Append(string.Concat("@LynxID", strLynxID));
                        strLogParameters.Append(string.Concat("@PertainsTo", strPertainsTo));
                        strLogParameters.Append(string.Concat("@Key", strKey));
                        strLogParameters.Append(string.Concat("@Description", strDescription));
                        strLogParameters.Append(string.Concat("@UserID", intUserID));
                        strLogParameters.Append(string.Concat("@ServiceChannelCD", strServiceChannelCD));


                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSendWorkFlowEvent = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - SendWorkFlowEventExecuteSpNamedParams", string.Concat("Error occur in SendWorkFlowEventExecuteSpNamedParams. SP name - ", strProcName, " Parameters: ", strLogParameters), ex.Message);
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return lngSendWorkFlowEvent;
        }
        #endregion

        #region Function call from DocumentMgr
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="lngLynxID"></param>
        /// <param name="strDocumentType"></param>
        /// <returns></returns>
        public string ClaimInfoExecuteSpNamedParamsXML(string strProcName, long lngLynxID, string strDocumentType)
        {
            string strClaimInfo = string.Empty;
            string strConnectionString = string.Empty;
            string strWSProcName = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                xmlDocument = new XmlDocument();
                strConnectionString = GetConnectionString("APD");
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@LynxID", lngLynxID);
                        command.Parameters.AddWithValue("@DocumentTypeCD", strDocumentType);

                        strLogParameters.Append(string.Concat("@LynxID", lngLynxID));
                        strLogParameters.Append(string.Concat("@DocumentTypeCD", strDocumentType));

                        sqlConnection.Open();
                        xmlDocument.Load(command.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strClaimInfo = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - ClaimInfoExecuteSpNamedParamsXML", string.Concat("Error occur in ClaimInfoExecuteSpNamedParamsXML. SP name - ", strProcName, " Parameters: ", strLogParameters), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strClaimInfo;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="strUserID"></param>
        /// <param name="strCreatedUserID"></param>
        /// <param name="strPertainsTo"></param>
        /// <param name="lngLynID"></param>
        /// <param name="intNotifyEvent"></param>
        /// <param name="strDirectionalCD"></param>
        /// <param name="strSqlNow"></param>
        /// <param name="strImageLocation"></param>
        /// <param name="strDocumentSource"></param>
        /// <param name="strDocumentType"></param>
        /// <param name="strImageType"></param>
        /// <param name="strSupplementSeqNumber"></param>
        public void DocInsertExecuteSpNamedParams(string strProcName, string strUserID, string strCreatedUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strSupplementSeqNumber = "")
        {
            long lngSendWorkFlowEvent = 0;
            string strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();

                strConnectionString = GetConnectionString("Partner");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@NTUserID", strUserID);
                        command.Parameters.AddWithValue("@CreatedUserID", strCreatedUserID);
                        command.Parameters.AddWithValue("@PertainsTo", strPertainsTo);
                        command.Parameters.AddWithValue("@LynxID", lngLynID);
                        command.Parameters.AddWithValue("@NotifyEvent", intNotifyEvent);
                        command.Parameters.AddWithValue("@DirectionalCD", strDirectionalCD);
                        command.Parameters.AddWithValue("@CreatedDate", strSqlNow);
                        command.Parameters.AddWithValue("@ImageDate", strSqlNow);
                        command.Parameters.AddWithValue("@ImageLocation", strImageLocation);
                        command.Parameters.AddWithValue("@DocumentSource", strDocumentSource);
                        command.Parameters.AddWithValue("@DocumentType", strDocumentType);
                        command.Parameters.AddWithValue("@ImageType", strImageType);
                        command.Parameters.AddWithValue("@SupplementSeqNumber", strSupplementSeqNumber);

                        strLogParameters.Append(string.Concat("@NTUserID", strUserID));
                        strLogParameters.Append(string.Concat("@CreatedUserID", strCreatedUserID));
                        strLogParameters.Append(string.Concat("@PertainsTo", strPertainsTo));
                        strLogParameters.Append(string.Concat("@LynxID", lngLynID));
                        strLogParameters.Append(string.Concat("@NotifyEvent", intNotifyEvent));
                        strLogParameters.Append(string.Concat("@DirectionalCD", strDirectionalCD));
                        strLogParameters.Append(string.Concat("@CreatedDate", strSqlNow));
                        strLogParameters.Append(string.Concat("@ImageDate", strSqlNow));
                        strLogParameters.Append(string.Concat("@ImageLocation", strImageLocation));
                        strLogParameters.Append(string.Concat("@DocumentSource", strDocumentSource));
                        strLogParameters.Append(string.Concat("@DocumentType", strDocumentType));
                        strLogParameters.Append(string.Concat("@ImageType", strImageType));
                        strLogParameters.Append(string.Concat("@SupplementSeqNumber", strSupplementSeqNumber));

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSendWorkFlowEvent = Convert.ToInt64(sqlReturnParm.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - DocInsertExecuteSpNamedParams", string.Concat("Error occur in DocInsertExecuteSpNamedParams. SP name - ", strProcName, " Parameters: ", strLogParameters), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="strDocumentID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strEstimateChanges"></param>
        /// <returns></returns>
        public string GrabVehicleExecuteSpNamedParamsXML(string strProcName, string strDocumentID, string strUserID, string strEstimateChanges)
        {
            string strGrabVehicleInfo = string.Empty;
            string strConnectionString = string.Empty;
            string strWSProcName = string.Empty;
            XmlDocument xmlDocument = null;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                xmlDocument = new XmlDocument();
                strConnectionString = GetConnectionString("APD");
                strWSProcName = ChangeSPXMLtoWSXML(strProcName);
                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strWSProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@DocumentID", strDocumentID);
                        command.Parameters.AddWithValue("@UserID", strUserID);
                        command.Parameters.AddWithValue("@EstimateChanges", strEstimateChanges);

                        strLogParameters.Append(string.Concat("@DocumentID", strDocumentID));
                        strLogParameters.Append(string.Concat("@UserID", strUserID));
                        strLogParameters.Append(string.Concat("@EstimateChanges", strEstimateChanges));

                        sqlConnection.Open();
                        xmlDocument.Load(command.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strGrabVehicleInfo = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - GrabVehicleExecuteSpNamedParamsXML", string.Concat("Error occur in GrabVehicleExecuteSpNamedParamsXML. SP name - ", strProcName, " Parameters: ", strLogParameters), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
            return strGrabVehicleInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="strEventID"></param>
        /// <param name="strLynxID"></param>
        /// <param name="strPertainsTo"></param>
        /// <param name="strDescription"></param>
        /// <param name="strUserID"></param>
        public void LogTheFaxExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strDescription, string strUserID)
        {
            string strConnectionString = string.Empty;
            StringBuilder strLogParameters = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                strLogParameters = new StringBuilder();
                strConnectionString = GetConnectionString("APD");

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@return", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.ReturnValue;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@EventID", strEventID);
                        command.Parameters.AddWithValue("@LynxID", strLynxID);
                        command.Parameters.AddWithValue("@PertainsTo", strPertainsTo);
                        command.Parameters.AddWithValue("@Description", strDescription);
                        command.Parameters.AddWithValue("@UserID", strUserID);

                        strLogParameters.Append(string.Concat("@EventID", strEventID));
                        strLogParameters.Append(string.Concat("@LynxID", strLynxID));
                        strLogParameters.Append(string.Concat("@PertainsTo", strPertainsTo));
                        strLogParameters.Append(string.Concat("@Description", strDescription));
                        strLogParameters.Append(string.Concat("@UserID", strUserID));

                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - LogTheFaxExecuteSpNamedParams", string.Concat("Error occur in LogTheFaxExecuteSpNamedParams. SP name - ", strProcName, " Parameters: ", strLogParameters), "");
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                strLogParameters = null;
            }
        }
        #endregion

        #region Function call for SiteUtilities
        /// <summary>
        /// OpenRecordsetAsClientSideXML
        /// </summary>
        /// <param name="strProcedureName"></param>
        /// <param name="strParam1"></param>
        /// <param name="strParam2"></param>
        /// <returns></returns>
        public string OpenRecordsetAsClientSideXML(string strProcedureName, string strParam1, string strParam2)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ExecuteSpNpColXML:");
            string returnValue = string.Empty,
        strWSProcName = string.Empty,
        strConnectionString = string.Empty,
        strReturnValue = string.Empty;
            XmlDocument xmlDocument = null;
            WriteLog objWriteLog = new WriteLog();
            try
            {
                xmlDocument = new XmlDocument();
                strConnectionString = GetConnectionString("APD");
                strWSProcName = ChangeSPXMLtoWSXML(strProcedureName);

                using (SqlConnection sqlConn = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(strWSProcName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = GetConnectionTimeOut();

                        cmd.Parameters.AddWithValue("", strParam1);
                        cmd.Parameters.AddWithValue("", strParam2);

                        sqlConn.Open();
                        xmlDocument.Load(cmd.ExecuteXmlReader());

                        if (xmlDocument != null && !StringExtensions.IsNullOrWhiteSpace(xmlDocument.OuterXml))
                            strReturnValue = xmlDocument.OuterXml;
                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "DataAccessor.CDataAccessor.cs - OpenRecordsetAsClientSideXML", string.Concat("Error occur in OpenRecordsetAsClientSideXML. SP name - ", strProcedureName), ex.Message);
                throw ex;
            }
            finally
            {
                objWriteLog = null;
                xmlDocument = null;
            }
            return strReturnValue;
        } //TODO the functions is using for SiteUtilites COM dll. Need to get the parameter name.
        #endregion

        #region Public Methods    

        /// <summary>
        /// Get the Connection Time Out
        /// </summary>
        /// <returns></returns>
        private int GetConnectionTimeOut()
        {
            int strConnectionTimeOut;
            try
            {
                strConnectionTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectionTimeOut"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strConnectionTimeOut;
        }

        /// <summary>
        /// Get the user name from Config
        /// </summary>
        /// <param name="ApplicationName"></param>
        /// <returns></returns>
        private string GetUserName(string ApplicationName)
        {
            string strReturnUserName = string.Empty;
            try
            {
                if (ApplicationName == "APD" || ApplicationName == "Partner")
                    strReturnUserName = ConfigurationManager.AppSettings["ADODBConnection_UserName"];
                else if (ApplicationName == "FNOL")
                    strReturnUserName = ConfigurationManager.AppSettings["ADODBFNOLConnection_UserName"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnUserName;
        }

        /// <summary>
        /// Get password from Config
        /// </summary>
        /// <param name="ApplicationName"></param>
        /// <returns></returns>
        private string GetPassword(string ApplicationName)
        {
            string strReturnPassword = string.Empty;
            try
            {
                if (ApplicationName == "APD" || ApplicationName == "Partner")
                    strReturnPassword = ConfigurationManager.AppSettings["ADODBConnection_Password"];
                else if (ApplicationName == "FNOL")
                    strReturnPassword = ConfigurationManager.AppSettings["ADODBFNOLConnection_Password"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnPassword;
        }

        /// <summary>
        /// Get ADODB connection string
        /// </summary>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        public string GetADODBConnectionString(string ConnectionType)
        {
            string returnConnectionString = string.Empty;
            try
            {
                if (ConnectionType == "APD")
                {
                    switch (PGW.Business.Environment.getEnvironment())
                    {
                        case "DEV":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_APD_DEV"];
                            break;
                        case "STG":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_APD_STG"];
                            break;
                        case "PRD":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_APD_PROD"];
                            break;
                        case "DR":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_APD_DR"];
                            break;
                        default:
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_APD_DEV"];
                            break;
                    }
                }
                else if (ConnectionType == "Partner")
                {
                    switch (PGW.Business.Environment.getEnvironment())
                    {
                        case "DEV":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_Partner_DEV"];
                            break;
                        case "STG":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_Partner_STG"];
                            break;
                        case "PRD":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_Partner_PROD"];
                            break;
                        case "DR":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_Partner_DR"];
                            break;
                        default:
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_Partner_DEV"];
                            break;
                    }

                }
                else if (ConnectionType == "FNOL")
                {
                    switch (PGW.Business.Environment.getEnvironment())
                    {
                        case "DEV":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_FNOL_DEV"];
                            break;
                        case "STG":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_FNOL_STG"];
                            break;
                        case "PRD":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_FNOL_PROD"];
                            break;
                        case "DR":
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_FNOL_DR"];
                            break;
                        default:
                            returnConnectionString = ConfigurationManager.AppSettings["ADODBConnectionString_FNOL_DEV"];
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnConnectionString;
        }

        /// <summary>
        /// Get DB connection string from Config
        /// </summary>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        public string GetConnectionString(string ConnectionType)
        {
            string returnConnectionString = string.Empty;

            if (ConnectionType == "APD")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                }
            }
            else if (ConnectionType == "Partner")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DR"];
                        break;
                     default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "FNOL")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "Ingres")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-PROD"];
                        break;                    
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DR"];
                        break;
                     default:
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                }
            }

            return returnConnectionString;
        }

        /// <summary>
        /// Begin Transactions
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTrans"></param>
        public void BeginTransaction(ref SqlConnection sqlConnection, ref SqlTransaction sqlTrans)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "BeginTransaction: ");
            try
            {
                if (sqlConnection != null)
                    sqlTrans = sqlConnection.BeginTransaction();
                else
                    throw new Exception(string.Concat(PROC_NAME, " - Connection passed as null."));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Commit Transactions
        /// </summary>
        /// <param name="sqlConnecction"></param>
        /// <param name="sqlTrans"></param>
        public void CommitTransaction(ref SqlConnection sqlConnecction, ref SqlTransaction sqlTrans)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CommitTransaction: ");
            try
            {
                if (sqlConnecction != null)
                    sqlTrans.Commit();
                else
                    throw new Exception(string.Concat(PROC_NAME, "- Connection passed as null."));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Roll back transactions
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTrans"></param>
        public void RollbackTransaction(ref SqlConnection sqlConnection, ref SqlTransaction sqlTrans)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "RollbackTransaction: ");
            try
            {
                if (sqlConnection != null)
                    sqlTrans.Rollback();
                else
                    throw new Exception(string.Concat(PROC_NAME, "- Connection passed as null."));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public Methods

        #region Private Helper Functions
               

        /// <summary>
        /// Returns true if InitEvents was called with a valid config.xml path.
        /// </summary>
        /// <returns></returns>
        private bool HasConfigurationInfo()
        {
            bool retHasConfigurationInfo = false;
            try
            {
                retHasConfigurationInfo = Convert.ToBoolean(m_strConfigFilePath != string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retHasConfigurationInfo;
        }
             
   
        #endregion private methods
    }
}
