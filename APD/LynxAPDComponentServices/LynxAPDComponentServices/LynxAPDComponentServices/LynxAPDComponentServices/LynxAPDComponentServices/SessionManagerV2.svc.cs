﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Session = Lynx.APD.Component.Library.SessionManagerV2;
using ADODB;
using LynxAPDComponentServices.Common;
using System.Xml;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SessionManagerV2" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class SessionManagerV2 : ISessionManagerV2
    {

        public string GetDateTime()
        {
            try
            {
                return string.Concat(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), " - Working");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateSession(string strUserID, string strWebRoot = "")
        {
            Session.CSession objSession = null;
            string returnSession = string.Empty;
            string strSessionKey = string.Empty;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "START", "SessionManagerV2.cs - CreateSession", string.Concat("Function Parameters: strUserID -", strUserID, " strWebRoot -", strWebRoot, "strWebRoot -", strWebRoot), "");
                returnSession = objSession.CreateSession(strUserID, strWebRoot);
                strSessionKey = "{" + returnSession + "}";
                objWriteLog.LogEvent("COM Conversion", "END", "SessionManagerV2.cs - CreateSession", string.Concat("Function Parameters: strUserID -", strUserID, " strWebRoot -", strWebRoot, "strWebRoot -", strWebRoot), "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "SessionManagerV2.cs - CreateSession", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strUserID -" + strUserID + " strWebRoot -" + strWebRoot + "strWebRoot -" + strWebRoot);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
            return returnSession;
        }

        public void UpdateSession(string strSessionKey, string strVariableName, string varValue, string strWebRoot = "")
        {
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "START", "SessionManagerV2.cs - UpdateSession", string.Concat("Function Parameters: strSessionKey -", strSessionKey, " strVariableName -", strVariableName, "varValue -", varValue, "strWebRoot -", strWebRoot), "");
                objSession.UpdateSession(strSessionKey, strVariableName, varValue, strWebRoot);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "SessionManagerV2.cs - UpdateSession", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strSessionKey -" + strSessionKey + " strVariableName -" + strVariableName + "varValue -" + varValue + "strWebRoot -" + strWebRoot);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
        }

        public string GetValue(string strSessionKey, string strVariableName, string strWebRoot = "")
        {
            string strReturn = string.Empty;
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {

                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "START", "SessionManagerV2.cs - GetValue", string.Concat("Function Parameters: strSessionKey -", strSessionKey, " strVariableName -", strVariableName, "strWebRoot -", strWebRoot), "");
                strReturn = objSession.GetValue(strSessionKey, strVariableName, strWebRoot);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "SessionManagerV2.cs - GetValue", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strSessionKey -" + strSessionKey + " strVariableName -" + strVariableName);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
            return strReturn;
        }

        public bool Exists(string strSessionKey, string strVariableName, string strWebRoot = "")
        {
            bool blnReturn = false;
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "START", "SessionManagerV2.cs - Exists", string.Concat("Function Parameters: strSessionKey -", strSessionKey, " strVariableName -", strVariableName, "strWebRoot -", strWebRoot), "");
                blnReturn = objSession.Exists(strSessionKey, strVariableName, strWebRoot);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "SessionManagerV2.cs - Exists", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strSessionKey -" + strSessionKey + " strVariableName -" + strVariableName);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
            return blnReturn;
        }

        public void XslInitSession(string strXslSessionKey, string strXslWindowID)
        {
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "START", "SessionManagerV2.cs - XslInitSession", string.Concat("Function Parameters: strXslSessionKey -", strXslSessionKey, "strXslWindowID -", strXslWindowID), "");
                objSession.XslInitSession(strXslSessionKey, strXslWindowID);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "SessionManagerV2.cs - XslInitSession", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strXslSessionKey -" + strXslSessionKey + "strXslWindowID -" + strXslWindowID);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
        }

        public string XslGetSession(string strVariableName)
        {
            string strReturn = string.Empty;
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "Debugging", "SessionManagerV2.cs - XslGetSession", string.Concat("Function Parameters: strVariableName -", strVariableName), "");
                strReturn = objSession.XslGetSession(strVariableName);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "SessionManagerV2.cs - XslGetSession", string.Concat(ex.Message, "-", ex.Source), "Function Parameters: strVariableName -" + strVariableName);
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
            return strReturn;
        }

        public void XslUpdateSession(string strVariableName, string strValue)
        {
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "Debugging", "SessionManagerV2.cs - XslUpdateSession", string.Concat("Function Parameters: strVariableName -", strVariableName, "strValue -", strValue), "");
                objSession.XslUpdateSession(strVariableName, strVariableName);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "SessionManagerV2.cs - XslUpdateSession", string.Concat(ex.Message, "-", ex.Source), string.Concat("Function Parameters: strVariableName -", strVariableName, "strValue -", strValue));
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
        }

        public void XslUpdateSessionNodeList(string strVariableName, ref XmlNodeList objNodeList)
        {
            Session.CSession objSession = null;
            Common.WriteLog objWriteLog = null;
            try
            {
                objSession = new Session.CSession();
                objWriteLog = new WriteLog();
                objWriteLog.LogEvent("COM Conversion", "Debugging", "SessionManagerV2.cs - XslUpdateSessionNodeList", string.Concat("Function Parameters: strVariableName -", strVariableName), "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "SessionManagerV2.cs - XslUpdateSessionNodeList", string.Concat(ex.Message, "-", ex.Source), string.Concat("Function Parameters: strVariableName -", strVariableName));
            }
            finally
            {
                objSession = null;
                objWriteLog = null;
            }
        }

    }
}
