﻿using System;
using System.ServiceModel;
using LAPDECAD = Lynx.APD.Component.Library.EcadAccessor;
using LAPDEcadMgr = Lynx.APD.Component.Library.EcadAccessorMgr;
using LynxAPDComponentServices.Common;
using System.Configuration;
using ApdDB = Lynx.APD.Component.Library.APDDataAccessor;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename"  command on the "Refactor" menu to change the class name "EcadAccessor" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class EcadAccessor : IEcadAccessor
    {
        Common.WriteLog objWriteLog = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);

        /// <summary>
        /// CheckService use to monitor the EcadAccessor WCF service
        /// </summary>
        /// <returns></returns>
        public string CheckService()
        {            
            ApdDB.CDataAccessor objcDataAccessor = null;
            string strReturn = string.Empty, strChecks = string.Empty;
            try
            {               
                objcDataAccessor = new ApdDB.CDataAccessor();
                strReturn = objcDataAccessor.ExecuteSpNpColXML("uspECADGetInsuranceCoListXML", null, "APD");
                if (string.IsNullOrEmpty(strReturn.Trim()))
                    strChecks = "Failed";
                else
                    strChecks = "Success";
            }
            catch (Exception ex)
            {
                throw ex;
                return "Failed";
            }
            finally
            {
                objcDataAccessor = null;
            }
            return strChecks;
        }

        /// <summary>
        /// GetDateTime 
        /// </summary>
        /// <returns></returns>
        public string GetDateTime()
        {
            try
            {
                return string.Concat(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), " Working Good.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetXML function is used to get the data from DB and return as the XML format.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>     
        public string GetXML(string strParamXML)
        {
            string strGetXML = string.Empty;
            LAPDECAD.CData cData = null;
            try
            {
                objWriteLog = new WriteLog();
                cData = new LAPDECAD.CData();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetXML", "Process started", strParamXML);

                strGetXML = cData.GetXML(strParamXML);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetXML", "Proccess completed", strParamXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetXML ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML);
            }
            finally
            {
                objWriteLog = null;
                cData = null;
            }
            return strGetXML;
        }

        /// <summary>
        /// PutXML is used to save the data inti DB it returns the integer value
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public long PutXML(string strParamXML)
        {
            long lngPutXML = 0;
            LAPDECAD.CData cData = null;
            try
            {
                objWriteLog = new WriteLog();
                cData = new LAPDECAD.CData();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - PutXML", "Process started", strParamXML);
                lngPutXML = cData.PutXML(strParamXML);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - PutXML", "Process completed", strParamXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - PutXML ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML);
            }
            finally
            {
                objWriteLog = null;
                cData = null;
            }
            return lngPutXML;
        }

        /// <summary>
        /// Retrieves an APD document in binary form of a byte array.
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetDocument(string strParamXML)
        {
            string objGetDocument = null;
            LAPDECAD.CDocument cDocuemnt = null;
            try
            {
                cDocuemnt = new LAPDECAD.CDocument();
                objWriteLog = new WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetDocument", "Process started", strParamXML);
                objGetDocument = cDocuemnt.GetDocument(strParamXML);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetDocument", "Process completed", strParamXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetDocument ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML);
            }
            finally
            {
                objWriteLog = null;
                cDocuemnt = null;
            }
            return objGetDocument;
        }

        /// <summary>
        /// Retrieves an XML APD document as a string.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetDocumentXML(string strParamXML)
        {
            string strGetDocumentXML = string.Empty;
            LAPDECAD.CDocument cDocument = null;
            try
            {
                cDocument = new LAPDECAD.CDocument();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetDocumentXML", "Process started", strParamXML);
                strGetDocumentXML = cDocument.GetDocumentXML(strParamXML);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetDocumentXML", "Process completed", strParamXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetDocumentXML ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML); ;
            }
            finally
            {
                objWriteLog = null;
                cDocument = null;
            }
            return strGetDocumentXML;
        }

        /// <summary>
        /// Get Thumbnail for APD
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width)
        {
            string strReturn = string.Empty;
            LAPDECAD.CDocument cDocument = null;
            try
            {
                cDocument = new LAPDECAD.CDocument();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetAPDThumbnail", "Process started", string.Concat("Document Location =", strParamXML, "Height = ", Height, "Width = ", Width));
                strReturn = cDocument.GetAPDThumbnail(strParamXML, Height, Width);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetAPDThumbnail", "Process completed", string.Concat("Document Location =", strParamXML, "Height = ", Height, "Width = ", Width));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetAPDThumbnail ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Document Location =", strParamXML, "Height = ", Height, "Width = ", Width));
            }
            finally
            {
                cDocument = null;
                objWriteLog = null;
            }
            return strReturn;
        }


        /// <summary>
        /// Get Thumbnail for APD
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <param name="Archived"></param>
        /// <returns></returns>
        public string GetAPDThumbnail(string strParamXML, string Height, string Width, string Archived)
        {
            string strReturn = string.Empty;
            LAPDECAD.CDocument cDocument = null;
            try
            {
                cDocument = new LAPDECAD.CDocument();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetAPDThumbnail", "Process started", string.Concat("Document Location =", strParamXML, "Height = ", Height, "Width = ", Width, "Archived = ", Archived));
                strReturn = cDocument.GetAPDThumbnail(strParamXML, Height, Width, Archived);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetAPDThumbnail", "Process completed", string.Concat("Document Location =", strParamXML, "Height = ", Height, "Archived = ", Archived));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetAPDThumbnail ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Document Location =", strParamXML, "Height = ", Height, "Width = ", Width, "Archived = ", Archived));
            }
            finally
            {
                cDocument = null;
                objWriteLog = null;
            }
            return strReturn;
        }

   

        /// <summary>
        /// Get the report document for the ClaimPoint submission report.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetReportDocument(string strParamXML)
        {
            string rptDocument = string.Empty;
            LAPDECAD.CDocument cDocument = null;
            try
            {
                cDocument = new LAPDECAD.CDocument();
                objWriteLog = new WriteLog();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - GetReportDocument", "Process started", strParamXML);
                rptDocument = cDocument.GetReportDocument(strParamXML);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - GetReportDocument", "Process completed", strParamXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - GetReportDocument ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML);
            }
            finally
            {
                cDocument = null;
                objWriteLog = null;
            }
            return rptDocument;
        }

        /// <summary>
        /// Submitting the claims
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string ClaimSubmissionGetXML(string strParamXML)
        {
            string strGetXML = string.Empty;
            LAPDECAD.CData cData = null;
            try
            {
                objWriteLog = new WriteLog();
                cData = new LAPDECAD.CData();

              // if (isDebug)
                //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "START", string.Concat("EcadAccessor.cs - ClaimSubmissionGetXML", "Server Name - ", Environment.MachineName), "Process started", strParamXML);

                strGetXML = cData.GetXML(strParamXML);

                //if (isDebug)
                //Debug script for time out issue on claim submission
                objWriteLog.LogEvent("COM Conversion", "END", string.Concat("EcadAccessor.cs - ClaimSubmissionGetXML", "Server Name - ", Environment.MachineName), "Proccess completed", strGetXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - ClaimSubmissionGetXML ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), strParamXML);
            }
            finally
            {
                objWriteLog = null;
                cData = null;
            }
            return strGetXML;
        }
        
        /// <summary>
        /// Generate Client Assignment report which is failed in ClaimPoint App
        /// </summary>
        /// <param name="strReportXML"></param>
        /// <param name="strLynxID"></param>
        /// <returns></returns>
        public string SaveWAReport(string strReportXML, string strLynxID)
        {
            string strFilePath = string.Empty;         
            LAPDECAD.CData cData = null;
            try
            {
                objWriteLog = new WriteLog();
                cData = new LAPDECAD.CData();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - SaveWAReport", "Process started", string.Concat("Report Data: ", strReportXML, " LynxID: ", strLynxID));

                strFilePath = cData.SaveWAReport(strReportXML, Convert.ToInt64(strLynxID));
                
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - SaveWAReport", "Proccess completed", string.Concat("Report Data: ", strReportXML, " LynxID: ", strLynxID));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - SaveWAReport ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Report Data: ", strReportXML, " LynxID: ", strLynxID));
            }
            finally
            {
                objWriteLog = null;
                cData = null;
            }         
            return strFilePath;
        }


        /// <summary>
        /// SendShopAssignments
        /// </summary>
        /// <param name="strReportXML"></param>
        /// <param name="strLynxID"></param>
        public bool SendShopAssignments(string StrXML, string strLynxID, bool blnStaffAppraiserAssignment)
        {
            string strFilePath = string.Empty;
            LAPDECAD.CData cData = null; 
            bool isSuccess=false;
            try
            {
                objWriteLog = new WriteLog();
                cData = new LAPDECAD.CData();
               

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "EcadAccessor.cs - SendShopAssignments", "Process started", string.Concat("Report Data: ", StrXML, " LynxID: ", strLynxID));

                isSuccess = cData.SendShopAssignments(StrXML, Convert.ToInt64(strLynxID), blnStaffAppraiserAssignment);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "EcadAccessor.cs - SendShopAssignments", "Proccess completed", string.Concat("Report Data: ", StrXML, " LynxID: ", strLynxID));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("EcadAccessor.cs - SendShopAssignments ", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Report Data: ", StrXML, " LynxID: ", strLynxID));
            }
            finally
            {
                objWriteLog = null;
                cData = null;
            }

            return isSuccess;
            
        }

    }
}
