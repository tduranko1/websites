﻿using System;
using System.ServiceModel;
using AccountingLib = Lynx.APD.Component.Library.Accounting;
using System.Configuration;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Accounting" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class Accounting : IAccounting
    {
        #region Declarations
        Common.WriteLog objWriteLog = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["AccoutingDebug"]);
        #endregion

        /// <summary>
        /// Generate Invoice
        /// </summary>
        /// <param name="lngInsuranceCompanyID"></param>
        /// <param name="lngClaimAspectID"></param>
        /// <param name="lngUserID"></param>
        /// <returns></returns>
        public long BillAndCreateInvoice(long lngInsuranceCompanyID, long lngClaimAspectID, long lngUserID)
        {
            long lngReturnValue = 0;
            AccountingLib.CBilling objCBilling = null;
            try
            {
                objCBilling = new AccountingLib.CBilling();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accounting.svc.cs - BillAndCreateInvoice", string.Concat("InsuranceCompanyID:", lngInsuranceCompanyID), string.Concat("ClaimAspectID:", lngClaimAspectID, "UserID:", lngUserID));

                lngReturnValue = objCBilling.BillAndCreateInvoice(lngInsuranceCompanyID, lngClaimAspectID, lngUserID);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accounting.svc.cs - BillAndCreateInvoice", "Funciton processed", string.Concat("Return Data: ", lngReturnValue));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accounting.svc.cs - BillAndCreateInvoice", string.Concat("Exception:", ex.Message), string.Concat("Function Parameters: lngInsuranceCompanyID -", lngInsuranceCompanyID, ", lngClaimAspectID -", lngClaimAspectID, ", lngUserID -", lngUserID));
            }
            finally
            {
                objCBilling = null;
                objWriteLog = null;
            }
            return lngReturnValue;
        }

        /// <summary>
        /// Release payment
        /// </summary>
        /// <param name="strReleaseFundsXml"></param>
        /// <returns></returns>
        public int ReleaseFunds(string strReleaseFundsXml)
        {
            int intReturnValue = 0;
            AccountingLib.CBilling objCBilling = null;
            try
            {
                objCBilling = new AccountingLib.CBilling();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accounting.svc.cs - ReleaseFunds", "ReleaseFundsFunction:", string.Concat("InputXml:", strReleaseFundsXml));
                intReturnValue = objCBilling.ReleaseFunds(strReleaseFundsXml);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accounting.svc.cs - ReleaseFunds", "ReleaseFundsFunction:", string.Concat("Return Data:", intReturnValue));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accounting.svc.cs - ReleaseFunds", string.Concat("Exception:", ex.Message), string.Concat("Function Parameters: strReleaseFundsXml -", strReleaseFundsXml));
            }
            finally
            {
                objCBilling = null;
                objWriteLog = null;
            }
            return intReturnValue;
        }

        /// <summary>
        /// ReviseInvoice
        /// </summary>
        /// <param name="strReviseInvoiceXml"></param>
        /// <returns></returns>
        public int ReviseInvoice(string strReviseInvoiceXml)
        {
            int intReturnValue = 0;
            AccountingLib.CBilling objCBilling = null;
            try
            {
                objWriteLog = new Common.WriteLog();
                objCBilling = new AccountingLib.CBilling();
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "Accounting.svc.cs - ReviseInvoice", "ReviseInvoiceFunction:", string.Concat("InputXml:", strReviseInvoiceXml));
                intReturnValue = objCBilling.ReviseInvoice(strReviseInvoiceXml);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "Accounting.svc.cs - ReviseInvoice", "ReviseInvoiceFunction:", string.Concat("Return Data:", intReturnValue));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accounting.svc.cs - ReviseInvoice", string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("InputXml:", strReviseInvoiceXml));
            }
            finally
            {
                objCBilling = null;
                objWriteLog = null;
            }
            return intReturnValue;
        }

        public string TestIngress(long lDispatchNo, string sWebUidNo, long lUserIdNo, int iUserTypeNo, string sEnvId, string sStylesheet = "")
        {
            AccountingLib.CBilling objCBilling = null;
            string strReturn = string.Empty;
            try
            {
                objCBilling = new AccountingLib.CBilling();
                strReturn = objCBilling.TestIngres(453697577, "dfde7b9e49af4273befaa04d79b340e6**91", 112469, 2, "AGC_WEB_P", "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "Accounting.svc.cs - ReviseInvoice", string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), "");

            }
            return strReturn;

        }
    }
}
