﻿using System;
using System.ServiceModel;
using PartnerMgr = Lynx.APD.Component.Library.PartnerDataMgr;
using LynxAPDComponentServices.Common;
using System.Configuration;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PartnerDataMgr" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class PartnerDataMgr : IPartnerDataMgr
    {
        #region Declarations
        PartnerMgr.CExecute objCExecute = null;
        PartnerMgr.CPartnerDataXfer objPartnerXfer = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        #endregion

        public string GetDateTime()
        {
            try
            {
                return string.Concat(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), " - Working");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values.
        /// </summary>
        /// <returns></returns>
        public string FixUnprocessedPartnerData(string strData, string strSessionKey, string strWindowID, string strUserID, string strFix)
        {
            objCExecute = new PartnerMgr.CExecute();
            string strFixUnProcessed = string.Empty, strErrorXml = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerDatamgr.cs - FixUnprocessedPartnerData", string.Concat("Function Parameters: strData -", strData, "strSessionKey -", strSessionKey, "strWindowID -", strWindowID, "strUserID -", strUserID, "strFix -", strFix), "");

                if (strFix == "fix")
                    strFixUnProcessed = objCExecute.FixUnprocessedPartnerData(strData, strSessionKey, strWindowID, strUserID);
                else
                    strFixUnProcessed = objCExecute.GetUnprocessedPartnerData(strSessionKey, strWindowID, strUserID, strErrorXml);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerDatamgr.cs - FixUnprocessedPartnerData", string.Concat("Function Parameters: strData -", strData, "strSessionKey -", strSessionKey, "strWindowID -", strWindowID, "strUserID -", strUserID, "strFix -", strFix), "");
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerDataMgr.cs - FixUnprocessedPartnerData", ex.Message, string.Concat("Function Parameters: strData -", strData, "strSessionKey -", strSessionKey, "strWindowID -", strWindowID, "strUserID -", strUserID, "strFix -", strFix));
            }
            finally
            {
                objCExecute = null;
                objWriteLog = null;
            }
            return strFixUnProcessed;
        }

        /// <summary>
        /// Procedure : GetUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values. 
        /// </summary>
        /// <returns></returns>
        public string GetUnprocessedPartnerData(string strSessionKey, string strWindowID, string strUserID)
        {
            objCExecute = new PartnerMgr.CExecute();
            string strGetUnprocessedPartnerData = string.Empty, strErrorXml = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                strGetUnprocessedPartnerData = objCExecute.GetUnprocessedPartnerData(strSessionKey, strWindowID, strUserID, strErrorXml);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - FixUnprocessedPartnerData", ex.Message, "Function Parameters: " + "strSessionKey -" + strSessionKey + "strWindowID -" + strWindowID + "strUserID -" + strUserID);
            }
            finally
            {
                objCExecute = null;
                objWriteLog = null;
            }
            return strGetUnprocessedPartnerData;
        }

        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values. 
        /// </summary>
        /// <returns></returns>
        public string PullUnprocessedPartnerData(string strSessionKey, string strWindowID, string strUserID)
        {
            objCExecute = new PartnerMgr.CExecute();
            string strGetUnprocessedPartnerData = string.Empty, strErrorXml = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                strGetUnprocessedPartnerData = objCExecute.GetUnprocessedPartnerData(strSessionKey, strWindowID, strUserID, strErrorXml);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - FixUnprocessedPartnerData", ex.Message, "Function Parameters: " + "strSessionKey -" + strSessionKey + "strWindowID -" + strWindowID + "strUserID -" + strUserID);
            }
            finally
            {
                objCExecute = null;
                objWriteLog = null;
            }
            return strGetUnprocessedPartnerData;
        }

        /// <summary>
        /// Syntax:      object.InitiatePartnerXmlTransaction strXmlDoc
        /// Parameters:  strXmlDoc = Xml document received from calling application
        /// Purpose:     Receive an Xml from an internal application and then kick off
        /// a transaction with an external (partner) application.
        /// Returns:     Blank or any response XML.
        /// </summary>
        /// <returns></returns>
        public string InitiatePartnerXmlTransaction(string strXMLDoc)
        {
            objCExecute = new PartnerMgr.CExecute();
            string strInitiatePartnerXmlTrans = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                strInitiatePartnerXmlTrans = objCExecute.InitiatePartnerXmlTransaction(strXMLDoc);
            }
            catch (Exception ex)
            {

                objWriteLog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - InitiatePartnerXmlTransaction", ex.Message, "Function Parameters: strXMLDoc -" + strXMLDoc);
            }
            finally
            {
                objCExecute = null;
                objWriteLog = null;
            }
            return strInitiatePartnerXmlTrans;

        }

        /// <summary>
        /// This function processes an Xml document sent from outside of PPG,
        /// Specifically from one of our partners (CCC, ADP, SG, etc.)
        /// </summary>
        /// <returns></returns>
        public string ConsumePartnerXmlTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)
        {
            objCExecute = new PartnerMgr.CExecute();
            string strConsumePartnerXmlTransaction = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                strConsumePartnerXmlTransaction = objCExecute.ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, blnReturnXml);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - ConsumePartnerXmlTransaction", ex.Message, "Function Parameters: strXMLDoc -" + strXMLDoc + "strTradingPartner -" + strTradingPartner + "blnReturnXml -" + blnReturnXml);
            }
            finally
            {
                objCExecute = null;
                objWriteLog = null;
            }
            return strConsumePartnerXmlTransaction;
        }

        /// <summary>
        /// Insert Data to APD using PartnerDataXfer.cs
        /// </summary>
        /// <param name="strXMLDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <param name="lngAPDDocumentID"></param>
        /// <returns></returns>
        public string AttachEstimateToAPDDocument(string strXMLDoc, string strTradingPartner, long lngAPDDocumentID)
        {
            PartnerMgr.SharedData objShared = null;
            string strEstimateToAPDDocument = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                objShared = new PartnerMgr.SharedData();
                objPartnerXfer = new PartnerMgr.CPartnerDataXfer(objShared);
                strEstimateToAPDDocument = objPartnerXfer.AttachEstimateToAPDDocument(strXMLDoc, strTradingPartner, lngAPDDocumentID);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - AttachEstimateToAPDDocument", ex.Message, "Function Parameters: strXMLDoc -" + strXMLDoc + "strTradingPartner -" + strTradingPartner + "lngAPDDocumentID -" + lngAPDDocumentID);
            }
            finally
            {
                objPartnerXfer = null;
                objWriteLog = null;
            }
            return strEstimateToAPDDocument;
        }

    }
}
