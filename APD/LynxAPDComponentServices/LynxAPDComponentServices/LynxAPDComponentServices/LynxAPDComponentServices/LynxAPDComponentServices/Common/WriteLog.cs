﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace LynxAPDComponentServices.Common
{
    public class WriteLog
    {
        //lynxdataserviceLogging.APDService DevwsAPDFoundation = null;

        /// <summary>
        /// Write log functionality
        /// </summary>
        /// <param name="strEventType"></param>
        /// <param name="strEventStatus"></param>
        /// <param name="strEventDescription"></param>
        /// <param name="strEventDetailedDescritpion"></param>
        /// <param name="strEventXML"></param>
        public void LogEvent(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        {
            string strConnectionString = string.Empty, strProcName = string.Empty, strError = string.Empty;
            long lngSPreturnID = 0;
            try
            {
                strConnectionString = GetConnectionString("APD");
                strProcName = "uspAPDEventInsLogEntry";

                using (SqlConnection sqlConnection = new SqlConnection(strConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(strProcName, sqlConnection))
                    {
                        SqlParameter sqlReturnParm = new SqlParameter("@iRecID", SqlDbType.BigInt);
                        sqlReturnParm.Direction = ParameterDirection.Output;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(sqlReturnParm);
                        command.Parameters.AddWithValue("@vEventType", strEventType);
                        command.Parameters.AddWithValue("@vEventStatus", strEventStatus);
                        command.Parameters.AddWithValue("@vEventDescription", strEventDescription);
                        command.Parameters.AddWithValue("@vEventDetailedDescription", strEventDetailedDescritpion);
                        command.Parameters.AddWithValue("@vEventXML", strEventXML);
                        
                        sqlConnection.Open();
                        command.ExecuteNonQuery();
                        lngSPreturnID = Convert.ToInt64(sqlReturnParm.Value);

                        if (lngSPreturnID < 0)
                            throw new Exception("LynxAPDComponent Service Failed: Event logging failed to insert a row (LogEvent)...");
                    }
                }

                //DevwsAPDFoundation = new lynxdataserviceLogging.APDService();
                //DevwsAPDFoundation.LogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
            }
            catch (Exception ex)
            {                
                Send(ex, string.Concat("EventDescription: ", strEventDescription, " EventDetailedDescription: ", strEventDetailedDescritpion, " EventXML: ", string.Concat("<![CDATA[", strEventXML, "]]>")));
            }            
        }

        /// <summary>
        /// Get DB connection string from Config
        /// </summary>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        public string GetConnectionString(string ConnectionType)
        {
            string returnConnectionString = string.Empty;

            if (ConnectionType == "APD")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_DEV"];
                        break;
                }
            }
            else if (ConnectionType == "Partner")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectString_Partner_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "FNOL")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["ConnectStringFNOL_DEV"];
                        break;
                }

            }
            else if (ConnectionType == "Ingres")
            {
                switch (PGW.Business.Environment.getEnvironment())
                {
                    case "DEV":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                    case "STG":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-STG"];
                        break;
                    case "PRD":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-PROD"];
                        break;
                    case "DR":
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DR"];
                        break;
                    default:
                        returnConnectionString = ConfigurationManager.AppSettings["IngresConnection-DEV"];
                        break;
                }
            }

            return returnConnectionString;
        }

        public string From
        {
            get;
            set;
        }

        public string To
        {
            get;
            set;
        }

        public string Subject
        {
            get;
            set;
        }

        public string Body
        {
            get;
            set;
        }

        public string CC
        {
            get;
            set;
        }

        public string BCC
        {
            get;
            set;
        }

        /// <summary>
        /// Get smtp server details
        /// </summary>
        /// <returns></returns>
        public string GetHostName()
        {
            string HostName = string.Empty,
                environment = string.Empty;

            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        environment = "DevSmtpServer";
                        break;
                    case "STG":
                        environment = "StgSmtpServer";
                        break;
                    case "PRD":
                        environment = "PrdSmtpServer";
                        break;
                    case "PROD":
                        environment = "PrdSmtpServer";
                        break;
                    case "DR":
                        environment = "DrSmtpServer";
                        break;
                    default:
                        environment = "DevSmtpServer";
                        break;
                }

                HostName = Convert.ToString(ConfigurationManager.AppSettings[environment]);

            }
            catch (Exception ex)
            {

            }
            return HostName;
        }

        private string GetFromMailID()
        {
            string From = string.Empty,
                   ReturnFromID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        From = "DEVExceptionFrom";
                        break;
                    case "STG":
                        From = "STGExceptionFrom";
                        break;
                    case "PRD":
                        From = "PRDExceptionFrom";
                        break;
                    case "DR":
                        From = "DRExceptionFrom";
                        break;
                    default:
                        From = "DEVExceptionFrom";
                        break;
                }
                ReturnFromID = Convert.ToString(ConfigurationManager.AppSettings[From]);
                //ReturnFromID = ConfigurationManager.AppSettings[From];
            }
            catch (Exception ex)
            {

            }
            return ReturnFromID;
        }

        private string GetToMailID()
        {
            string To = string.Empty,
                  ReturnToID = string.Empty;
            try
            {
                switch (PGW.Business.Environment.getEnvironment().ToUpper())
                {
                    case "DEV":
                        To = "DEVExceptionTo";
                        break;
                    case "STG":
                        To = "STGExceptionTo";
                        break;
                    case "PRD":
                        To = "PRDExceptionTo";
                        break;
                    case "DR":
                        To = "DRExceptionTo";
                        break;
                    default:
                        To = "DEVExceptionTo";
                        break;
                }
                ReturnToID = Convert.ToString(ConfigurationManager.AppSettings[To]);
                //ReturnToID = ConfigurationManager.AppSettings[To];
            }
            catch (Exception ex)
            {

            }
            return ReturnToID;
        }

        public void Send(Exception exMessage, string strFunationName)
        {
            MailMessage mail = null;
            SmtpClient mailSender = null;
            string mailHostName = null, strFrom = string.Empty, strTo = string.Empty, strCC = string.Empty, strBCC = string.Empty;
            try
            {
                strFrom = GetFromMailID();
                strTo = GetToMailID();
                mail = new MailMessage(strFrom, strTo);
                mail.Subject = "Error: LynxAPDComponentServices";
                mail.Body = GetMailbodyInhtml(exMessage, strFunationName);
                mail.IsBodyHtml = true;
                mailHostName = GetHostName();

                if (!string.IsNullOrEmpty(strCC))
                    mail.CC.Add(strCC);
                if (!string.IsNullOrEmpty(strBCC))
                    mail.Bcc.Add(strBCC);

                mailSender = new SmtpClient(mailHostName);
                mailSender.Send(mail);
            }
            catch (Exception ex)
            {

            }
        }

        public string GetMailbodyInhtml(Exception exMessage, string strFunationName)
        {
            StringBuilder sbMailFormat = null;

            try
            {
                sbMailFormat = new StringBuilder();

                sbMailFormat.Append("<span style='font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append(exMessage);
                sbMailFormat.Append("</span>");

                //Environment 
                sbMailFormat.Append("<table>");
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:15%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Environment");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(PGW.Business.Environment.getEnvironment().ToUpper());
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //DateTime 

                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Date&Time");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(Convert.ToString(DateTime.Now));
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Program Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Parameters");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>");
                sbMailFormat.Append(strFunationName);
                //Me.Session("WebUserID"))
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Machine Name
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Machine Name");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(Environment.MachineName);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");

                //Error Message
                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");
                sbMailFormat.Append("<td style='width:10%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>");
                sbMailFormat.Append("Error Details");
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("<td style='width:90%; background-color:#fffacd;bold;font-family:Verdana;font-size:8pt;color:black;'>");
                sbMailFormat.Append(exMessage.StackTrace);
                sbMailFormat.Append("</td>");
                sbMailFormat.Append("</tr>");



                sbMailFormat.Append("<tr style='vertical-align:top;text-align:left;'>");

                sbMailFormat.Append("</tr>");

                sbMailFormat.Append("</table>");
            }
            catch (Exception ex)
            {
            }

            return sbMailFormat.ToString();
        }
    }
}