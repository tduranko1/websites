﻿using System;
using System.ServiceModel;
using PartnerDataLib = Lynx.APD.Component.Library.PartnerData;
using PartnerHyper = Lynx.APD.Component.Library.HyperQuest;
using Logging = Lynx.APD.Component.Library.Common;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PartnerData" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class PartnerData : IPartnerData
    {

        #region Declarations
        Common.WriteLog objWriteLog = null;
        bool isDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerDebug"]);
        #endregion

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        /// 
        public long SendXmlDoc(string strXmlDoc, string strTradingPartner)
        {
            long returnXMLDoc = 0;
            //PartnerData. objPartnerData = new PartnerData();
            PartnerDataLib.CExecute objPartnerData = new PartnerDataLib.CExecute();
            objWriteLog = new Common.WriteLog();
            try
            {
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - SendXmlDoc", string.Concat("Started at ", DateTime.Now.ToString(), " Server: ", System.Environment.MachineName), string.Concat(string.Concat("strTradingPartner: ", strTradingPartner, " strXmlDoc: ", strXmlDoc), ""));
                returnXMLDoc = objPartnerData.SendXmlDoc(strXmlDoc, strTradingPartner);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerData.svc.cs - SendXmlDoc", string.Concat("Completed at ", DateTime.Now.ToString(), " Server: ", System.Environment.MachineName), string.Concat(string.Concat("strTradingPartner: ", strTradingPartner, " strXmlDoc: ", strXmlDoc), ""));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("PartnerData.svc.cs - SendXmlDoc", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Transaction XML: ", strXmlDoc, " TransactionType: ", strTradingPartner));
            }
            finally
            {
                objPartnerData = null;
                objWriteLog = null;
            }
            return returnXMLDoc;
        }


        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public string SendXmlRequest(string strXmlDoc, string strTradingPartner)
        {
            string strSendXmlRequest = string.Empty;
            PartnerDataLib.CExecute objPartnerData = new PartnerDataLib.CExecute();
            objWriteLog = new Common.WriteLog();
            try
            {
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - SendXmlRequest", "Process started", string.Concat("TransactionType: ", strTradingPartner, " Transaction XML: ", strXmlDoc));
                strSendXmlRequest = objPartnerData.SendXmlRequest(strXmlDoc, strTradingPartner);
                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerData.svc.cs - SendXmlRequest", "Process completed", string.Concat("TransactionType: ", strTradingPartner, " Transaction XML: ", strXmlDoc));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("PartnerData.svc.cs - SendXmlRequest", "Server Name - ", Environment.MachineName), string.Concat("Exception:", ex.Message, "StackTrace:", ex.StackTrace), string.Concat("Transaction XML -", strXmlDoc, " TransactionType -", strTradingPartner));
            }
            finally
            {
                objPartnerData = null;
                objWriteLog = null;
            }
            return strSendXmlRequest;
        }


        public string GetDateTime()
        {
            try
            {
                return string.Concat(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), " Working Good.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checking NADA Service
        /// </summary>
        /// <param name="strVIN"></param>
        /// <returns></returns>
        public string CallNADA(string strVIN)
        {
            string strReturn = string.Empty;
            Lynx.APD.Component.Library.PartnerDataMgr.NadaWrapper objNADA = null;
            objWriteLog = new Common.WriteLog();
            Hashtable htParam = null;
            try
            {

                objNADA = new Lynx.APD.Component.Library.PartnerDataMgr.NadaWrapper();
                htParam = new Hashtable();
                htParam = objNADA.GetVIN(strVIN);
                if (htParam != null)
                {
                    if (htParam.Count > 0)
                    {
                        strReturn = string.Concat("Year - ", (string)htParam["Year"], " Make - ", (string)htParam["Make"], " Model - ", (string)htParam["Model"]);

                    }
                }
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerData.svc.cs - CallNADA", ex.Message, string.Concat("strVIN  -", strVIN));
            }
            return strReturn;
        }

        /// <summary>
        /// Process the document from HyperQuest to APD
        /// </summary>
        /// <param name="strxml"></param>
        /// <returns></returns>
        public string ProcessHqRequest(string strxml)
        {
            XmlDocument xmlReturn = new XmlDocument();
            PartnerHyper.IncomingHyper objIncomingHyper = null;
            try
            {
                objIncomingHyper = new PartnerHyper.IncomingHyper();
                objWriteLog = new Common.WriteLog();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - ProcessHqRequest", "Process started", string.Concat("XML Data: ", strxml));

                xmlReturn = objIncomingHyper.ProcessHyperIncoming(strxml);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerData.svc.cs - ProcessHqRequest", "Process Completed", string.Concat("XML Data: ", strxml));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("PartnerData.svc.cs - ProcessHqRequest", "Server Name - ", Environment.MachineName), string.Concat("Exception: ", ex.Message, "InnerException -", ex.InnerException, "StackTrace:", ex.StackTrace), string.Concat("XML Data: ", strxml));
            }
            finally
            {
                objWriteLog = null;
            }
            return xmlReturn.OuterXml;
        }

        /// <summary>
        /// Process HQ Assignments
        /// </summary>
        /// <param name="AssignmentID"></param>
        /// <returns></returns>
        public bool ProcessHQAssignment(string AssignmentID)
        {
            bool blnReturn = false;
            PartnerHyper.IncomingHyper objIncomingHyper = null;
            objWriteLog = new Common.WriteLog();
            try
            {
                objIncomingHyper = new PartnerHyper.IncomingHyper();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - ProcessHqRequest", "Process started", string.Concat("AssignmentID: ", AssignmentID));

                blnReturn = objIncomingHyper.ProcessHQAssignment(AssignmentID);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "END", "PartnerData.svc.cs - ProcessHqRequest", "Process Completed", string.Concat("AssignmentID: ", AssignmentID));
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", string.Concat("PartnerData.svc.cs - ProcessHqRequest", "Server Name - ", Environment.MachineName), string.Concat("Exception: ", ex.Message, "InnerException -", ex.InnerException, "StackTrace:", ex.StackTrace), string.Concat("AssignmentID: ", AssignmentID));
            }
            return blnReturn;
        }

        /// <summary>
        /// Use the write the logs from Ebiz servers
        /// </summary>
        /// <param name="strEventType"></param>
        /// <param name="strEventStatus"></param>
        /// <param name="strEventDescription"></param>
        /// <param name="strEventDetailedDescritpion"></param>
        /// <param name="strEventXML"></param>
        /// <returns></returns>
        public long WriteLog(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML)
        {
            Logging.WriteLog objWriteLog = null;
            long lngReturn = 0;

            try
            {
                objWriteLog = new Logging.WriteLog();
                objWriteLog.LogEvent(strEventType, strEventStatus, strEventDescription, strEventDetailedDescritpion, strEventXML);
            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerData.svc.cs - SendXmlDoc", ex.Message, "");
            }
            return lngReturn;
        }


        public string SendWorkflowNotification(string PassthruData, string EventID, string Description)
        {
            XmlDocument xmlReturn = new XmlDocument();

            PartnerHyper.IncomingHyper objIncomingHyper = null;

            try
            {
                objWriteLog = new Common.WriteLog();
                objIncomingHyper = new PartnerHyper.IncomingHyper();

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - SendWorkflowNotification", "Process started", "");

                xmlReturn = objIncomingHyper.SendWorkflowNotification(PassthruData, EventID, Description);

                if (isDebug)
                    objWriteLog.LogEvent("COM Conversion", "START", "PartnerData.svc.cs - SendWorkflowNotification", "Process completed", "");

            }
            catch (Exception ex)
            {
                objWriteLog.LogEvent("COM Conversion", "ERROR", "PartnerData.svc.cs - SendWorkflowNotification", ex.Message, "");
            }

            return xmlReturn.OuterXml;
        }
    }
}
