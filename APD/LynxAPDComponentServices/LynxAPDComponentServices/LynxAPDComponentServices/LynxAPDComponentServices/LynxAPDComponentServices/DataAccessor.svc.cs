﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DataAccess = Lynx.APD.Component.Library;
using LynxAPDComponentServices.Common;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataAccessor" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class DataAccessor : IDataAccessor
    {
        #region Declarations
        DataAccess.APDDataAccessor.CDataAccessor objDataAccess = null;
        #endregion

        #region COMM Server
        public string AssignShopExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, string strApplication)
        {
            string strAssignShop = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strAssignShop = objDataAccess.AssignShopExecuteSpNamedParamsXML(strProcName, lngAssignmentID, strApplication);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strAssignShop;
        }

        public string AnalystLogonIDExecuteSpNamedParamsXML(string strProcName, long lngLynxID)
        {
            string strAnalystLogonIDXML = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strAnalystLogonIDXML = objDataAccess.AnalystLogonIDExecuteSpNamedParamsXML(strProcName, lngLynxID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strAnalystLogonIDXML;
        }

        public string FaxTemplateExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, bool blnWarrantyAssignment)
        {
            string strFaxTemplateXML = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strFaxTemplateXML = objDataAccess.FaxTemplateExecuteSpNamedParamsXML(strProcName, lngAssignmentID, blnWarrantyAssignment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strFaxTemplateXML;
        }

        public void TransmissionPartnerExecuteSp(string strProcName, string strPPGAssignment)
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.TransmissionPartnerExecuteSp(strProcName, strPPGAssignment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DocumentInsertExecuteSpNamedParams(string strProcName, long lngUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strClaimAspectServiceChannelID, bool blnWarrantyFlag)
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.DocumentInsertExecuteSpNamedParams(strProcName, lngUserID, strPertainsTo, lngLynID, intNotifyEvent, strDirectionalCD, strSqlNow, strImageLocation, strDocumentSource, strDocumentType, strImageType, strClaimAspectServiceChannelID, blnWarrantyFlag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WarrantyAssignmentExecuteSpNamedParams(string strProcName, long lngAssignmentID, string strAssignmentCode)
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.WarrantyAssignmentExecuteSpNamedParams(strProcName, lngAssignmentID, strAssignmentCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendWorkFlowEventExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strKey, string strDescription, int intUserID, string strServiceChannelCD)
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.SendWorkFlowEventExecuteSpNamedParams(strProcName, strEventID, strLynxID, strPertainsTo, strKey, strDescription, intUserID, strServiceChannelCD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DocumentMgr

        public string ExecuteDocumentMgr(string strProcName, string strParams, string strIsXml)
        {
            string strReturn = string.Empty;
            Common.WriteLog objWriteLog = new WriteLog();
            try
            {
                objWriteLog.LogEvent("COM Conversion", "DEBUGGING", "DataAccessor.cs - ExecuteDocumentMgr", "Process started", string.Concat("SPName: ", strProcName, "strParams:", strParams, "strIsXML", strIsXml));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }

        public string ClaimInfoExecuteSpNamedParamsXML(string strProcName, long lngLynxID, string strDocumentType)
        {
            string strClaimInfo = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strClaimInfo = objDataAccess.ClaimInfoExecuteSpNamedParamsXML(strProcName, lngLynxID, strDocumentType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strClaimInfo;
        }

        public void DocInsertExecuteSpNamedParams(string strProcName, string strUserID, string strCreatedUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strSupplementSeqNumber = "")
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.DocInsertExecuteSpNamedParams(strProcName, strUserID, strCreatedUserID, strPertainsTo, lngLynID, intNotifyEvent, strDirectionalCD, strSqlNow, strImageLocation, strDocumentSource, strDocumentType, strImageType, strSupplementSeqNumber = "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GrabVehicleExecuteSpNamedParamsXML(string strProcName, string strDocumentID, string strUserID, string strEstimateChanges)
        {
            string strVehicleInfo = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strVehicleInfo = objDataAccess.GrabVehicleExecuteSpNamedParamsXML(strProcName, strDocumentID, strUserID, strEstimateChanges);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strVehicleInfo;
        }

        public void LogTheFaxExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strDescription, string strUserID)
        {
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                objDataAccess.LogTheFaxExecuteSpNamedParams(strProcName, strEventID, strLynxID, strPertainsTo, strDescription, strUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SiteUtilities
        public string OpenRecordsetAsClientSideXML(string strProcedureName, string strEntity, string strUserID)
        {
            string strReturn = string.Empty;
            try
            {
                objDataAccess = new DataAccess.APDDataAccessor.CDataAccessor();
                strReturn = objDataAccess.OpenRecordsetAsClientSideXML(strProcedureName, strEntity, strUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }
        #endregion

    }
}
