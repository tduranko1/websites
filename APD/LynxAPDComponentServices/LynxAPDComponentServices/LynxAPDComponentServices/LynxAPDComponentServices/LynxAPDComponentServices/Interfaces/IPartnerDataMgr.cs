﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPartnerDataMgr" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IPartnerDataMgr
    {
        [OperationContract]
        string GetDateTime();

        [OperationContract]
        string FixUnprocessedPartnerData(string strData, string strSessionKey, string strWindowID, string strUserID, string strFix);

        [OperationContract]
        string GetUnprocessedPartnerData(string strSessionKey, string strWindowID, string strUserID);

        [OperationContract]
        string PullUnprocessedPartnerData(string strSessionKey, string strWindowID, string strUserID);

        [OperationContract]
        string InitiatePartnerXmlTransaction(string strXMLDoc);

        [OperationContract]
        string ConsumePartnerXmlTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml);

        [OperationContract]
        string AttachEstimateToAPDDocument(string strXMLDoc, string strTradingPartner, long lngAPDDocumentID);
    }
}
