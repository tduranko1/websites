﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.ServiceModel.Web;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPartnerData" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IPartnerData
    {
        [OperationContract]
        long SendXmlDoc(string strXmlDoc, string strTradingPartner);

        [OperationContract]
        string SendXmlRequest(string strXmlDoc, string strTradingPartner);

        [OperationContract]
        string GetDateTime();

        [OperationContract]
        string CallNADA(string strVIN);

        [OperationContract]
        string ProcessHqRequest(string strxml);

        [OperationContract]
        long WriteLog(string strEventType, string strEventStatus, string strEventDescription, string strEventDetailedDescritpion, string strEventXML);

        [OperationContract]
        bool ProcessHQAssignment(string AssignmentID);

        [OperationContract]
        string SendWorkflowNotification(string PassthruData, string EventID, string Description);
    }
}
