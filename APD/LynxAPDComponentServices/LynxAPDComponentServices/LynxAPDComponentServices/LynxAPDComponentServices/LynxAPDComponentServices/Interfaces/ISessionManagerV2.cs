﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISessionManagerV2" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface ISessionManagerV2
    {
        [OperationContract]
        string GetDateTime();

        [OperationContract]
        string CreateSession(string strUserID, string strWebRoot = "");

        [OperationContract]
        void UpdateSession(string strSessionKey, string strVariableName, string varValue, string strWebRoot = "");

        [OperationContract]
        string GetValue(string strSessionKey, string strVariableName, string strWebRoot = "");

        [OperationContract]
        bool Exists(string strSessionKey, string strVariableName, string strWebRoot = "");

        [OperationContract]
        void XslInitSession(string strXslSessionKey, string strXslWindowID);

        [OperationContract]
        string XslGetSession(string strVariableName);

        [OperationContract]
        void XslUpdateSession(string strVariableName, string strValue);

       
    }
}
