﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEcadAccessor" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IEcadAccessor
    {
        [OperationContract]
        string CheckService();
    
        [OperationContract]
        string GetDateTime();

        [OperationContract]
        string GetXML(string strParamXML);

        [OperationContract]
        long PutXML(string strParamXML);

        [OperationContract]
        string GetDocument(string strParamXML);

        [OperationContract]
        string GetDocumentXML(string strParamXML);

        [OperationContract]
        string GetAPDThumbnail(string strParamXML, string Height, string Width);

        [OperationContract]
        string GetReportDocument(string strParamXML);

        [OperationContract]
        string ClaimSubmissionGetXML(string strParamXML);

        [OperationContract]
        string SaveWAReport(string strReportXML, string strLynxID);
    }
}
