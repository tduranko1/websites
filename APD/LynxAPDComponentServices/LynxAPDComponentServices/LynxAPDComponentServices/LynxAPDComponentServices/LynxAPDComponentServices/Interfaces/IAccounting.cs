﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAccounting" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IAccounting
    {
        [OperationContract]
        long BillAndCreateInvoice(long lngInsuranceCompanyID, long lngClaimAspectID, long lngUserID);

        [OperationContract]
        int ReleaseFunds(string strReleaseFundsXml);

        [OperationContract]
        int ReviseInvoice(string strReviseInvoiceXml);

        [OperationContract]
        string TestIngress(long lDispatchNo, string sWebUidNo, long lUserIdNo, int iUserTypeNo, string sEnvId, string sStylesheet = "");
    }
}
