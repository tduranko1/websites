﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Lynx.APD.Component.Library;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDataAccessor" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IDataAccessor
    {
        [OperationContract]
        string AssignShopExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, string strApplication);

        [OperationContract]
        string AnalystLogonIDExecuteSpNamedParamsXML(string strProcName, long lngLynxID);

        [OperationContract]
        string FaxTemplateExecuteSpNamedParamsXML(string strProcName, long lngAssignmentID, bool blnWarrantyAssignment);

        [OperationContract]
        void TransmissionPartnerExecuteSp(string strProcName, string strPPGAssignment);

        [OperationContract]
        void DocumentInsertExecuteSpNamedParams(string strProcName, long lngUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strClaimAspectServiceChannelID, bool blnWarrantyFlag);

        [OperationContract]
        void WarrantyAssignmentExecuteSpNamedParams(string strProcName, long lngAssignmentID, string strAssignmentCode);

        [OperationContract]
        void SendWorkFlowEventExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strKey, string strDescription, int intUserID, string strServiceChannelCD);

        [OperationContract]
        string ClaimInfoExecuteSpNamedParamsXML(string strProcName, long lngLynxID, string strDocumentType);

        [OperationContract]
        void DocInsertExecuteSpNamedParams(string strProcName, string strUserID, string strCreatedUserID, string strPertainsTo, long lngLynID, int intNotifyEvent, string strDirectionalCD, string strSqlNow, string strImageLocation, string strDocumentSource, string strDocumentType, string strImageType, string strSupplementSeqNumber = "");

        [OperationContract]
        string GrabVehicleExecuteSpNamedParamsXML(string strProcName, string strDocumentID, string strUserID, string strEstimateChanges);

        [OperationContract]
        void LogTheFaxExecuteSpNamedParams(string strProcName, string strEventID, string strLynxID, string strPertainsTo, string strDescription, string strUserID);

        [OperationContract]
        string OpenRecordsetAsClientSideXML(string strProcedureName, string strEntity, string strUserID);

        [OperationContract]
        string ExecuteDocumentMgr(string strProcName, string strParams, string strIsXml);
    }
}
