﻿<%@ Page Title="Monitors" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Monitors.aspx.cs" Inherits="APDMonitor1.Monitors" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h3><b><%: Title %>.</b></h3>
        <p class="lead">This page is a collection of Process Monitors that will show the most recient transactions happening in APD.</p>
    </div>

    <!------------------------>
    <!-- APD Claims Section -->
    <!------------------------>
    <div>
        <!-- Inbound Hyperquest to APD Integrations -->
        <table width="100%" align="left">
        <tr>
            <td>
                <h2>
                    <asp:Image ID="Image1" runat="server" ImageUrl="/APDMonitor/Images/Inbound.png" />
                    &nbsp;APD Claims</h2>
            </td>
        </tr>
          <tr>
            <td>
              <asp:Button Text="Claimpoint Claims" BorderStyle="None" ID="btnClaimpointClaims" CssClass="Initial" runat="server" OnClick="ClaimView_Tab1_Click" />
              <asp:Button Text="FNOL Claims" BorderStyle="None" ID="btnFNOLClaims" CssClass="Initial" runat="server" OnClick="FNOLView_Tab1_Click" />
              <asp:Button Text="Web Serv Claims" BorderStyle="None" ID="btnWebServClaims" CssClass="Initial" runat="server" OnClick="btnWebServClaims_Click" />

                <asp:MultiView ID="Claim_View1" runat="server">
                <asp:View ID="ClaimView_Tab1" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Claimpoint Claims: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbClaimpointClaimsRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvClaimpointClaims" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Claimpoint_Claims" GridLines="Horizontal" DataKeyNames="LynxID">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" HeaderStyle-Width="40px" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" HeaderStyle-Width="160px" />
                                    <asp:BoundField DataField="ClientClaimNumber" HeaderText="ClientClaimNumber" SortExpression="ClientClaimNumber" />
                                    <asp:BoundField DataField="LossDescription" HeaderText="LossDescription" SortExpression="LossDescription" />
                                    <asp:BoundField DataField="ClientName" HeaderText="ClientName" SortExpression="ClientName" HeaderStyle-Width="50px" ReadOnly="True" />
                                    <asp:BoundField DataField="LossState" HeaderText="LossState" SortExpression="LossState" HeaderStyle-Width="120px" />
                                    <asp:BoundField DataField="LossDate" HeaderText="LossDate" SortExpression="LossDate" HeaderStyle-Width="120px" />
                                    <asp:BoundField DataField="IntakeFinishDate" HeaderText="IntakeFinishDate" SortExpression="IntakeFinishDate" HeaderStyle-Width="120px" />
                                    <asp:BoundField DataField="SysLastUpdatedDate" HeaderText="SysLastUpdatedDate" SortExpression="SysLastUpdatedDate" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_Claimpoint_Claims" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT DISTINCT TOP 10 
	c.LynxID
	, i.Name
	, c.ClientClaimNumber
	, c.LossDescription
	, u.NameFirst + ' ' + u.NameLast  'ClientName'
	, c.LossState
	, c.LossDate
	, c.IntakeFinishDate
                , c.SysLastUpdatedDate
FROM
	utb_claim c WITH(NOLOCK)
	INNER JOIN utb_claim_aspect ca WITH(NOLOCK)
		ON ca.lynxid = c.lynxid
	INNER JOIN utb_insurance i WITH(NOLOCK)
		ON i.InsuranceCompanyID = c.InsuranceCompanyID
	INNER JOIN utb_user u WITH(NOLOCK)
		ON u.UserID = c.CarrierRepUserID
WHERE
	ca.SourceApplicationID = 2  --4 fnol 7 ws
ORDER BY
	c.SysLastUpdatedDate DESC">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>
                <asp:View ID="ClaimView_Tab2" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 FNOL Claims: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbFNOLClaimsRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvFNOLClaims" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_FNOL_Claims" GridLines="Horizontal" DataKeyNames="LynxID">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                    <asp:BoundField DataField="ClientClaimNumber" HeaderText="ClientClaimNumber" SortExpression="ClientClaimNumber" />
                                    <asp:BoundField DataField="LossDescription" HeaderText="LossDescription" SortExpression="LossDescription" />
                                    <asp:BoundField DataField="ClientName" HeaderText="ClientName" SortExpression="ClientName" ReadOnly="True" />
                                    <asp:BoundField DataField="LossState" HeaderText="LossState" SortExpression="LossState" />
                                    <asp:BoundField DataField="LossDate" HeaderText="LossDate" SortExpression="LossDate" />
                                    <asp:BoundField DataField="IntakeFinishDate" HeaderText="IntakeFinishDate" SortExpression="IntakeFinishDate" />
                                    <asp:BoundField DataField="SysLastUpdatedDate" HeaderText="SysLastUpdatedDate" SortExpression="SysLastUpdatedDate" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_FNOL_Claims" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT DISTINCT TOP 10
	c.LynxID
	, i.Name
	, c.ClientClaimNumber
	, c.LossDescription
	, u.NameFirst + ' ' + u.NameLast 'ClientName'
	, c.LossState
	, c.LossDate
	, c.IntakeFinishDate
                , c.SysLastUpdatedDate
FROM
	utb_claim c WITH(NOLOCK)
	INNER JOIN utb_claim_aspect ca WITH(NOLOCK)
		ON ca.lynxid = c.lynxid
	INNER JOIN utb_insurance i WITH(NOLOCK)
		ON i.InsuranceCompanyID = c.InsuranceCompanyID
	INNER JOIN utb_user u WITH(NOLOCK)
		ON u.UserID = c.CarrierRepUserID
WHERE
	ca.SourceApplicationID = 4  --4 fnol 7 ws
ORDER BY
	c.SysLastUpdatedDate DESC" >
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>
<asp:View ID="View6" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 WebServ Claims: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbWebServClaimsRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvWebServClaims" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_WebServ_Claims" GridLines="Horizontal" DataKeyNames="LynxID">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                    <asp:BoundField DataField="ClientClaimNumber" HeaderText="ClientClaimNumber" SortExpression="ClientClaimNumber" />
                                    <asp:BoundField DataField="LossDescription" HeaderText="LossDescription" SortExpression="LossDescription" />
                                    <asp:BoundField DataField="ClientName" HeaderText="ClientName" SortExpression="ClientName" ReadOnly="True" />
                                    <asp:BoundField DataField="LossState" HeaderText="LossState" SortExpression="LossState" />
                                    <asp:BoundField DataField="LossDate" HeaderText="LossDate" SortExpression="LossDate" />
                                    <asp:BoundField DataField="IntakeFinishDate" HeaderText="IntakeFinishDate" SortExpression="IntakeFinishDate" />
                                    <asp:BoundField DataField="SysLastUpdatedDate" HeaderText="SysLastUpdatedDate" SortExpression="SysLastUpdatedDate" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_WebServ_Claims" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT DISTINCT TOP 10
	c.LynxID
	, i.Name
	, c.ClientClaimNumber
	, c.LossDescription
	, u.NameFirst + ' ' + u.NameLast  'ClientName'
	, c.LossState
	, c.LossDate
	, c.IntakeFinishDate
                , c.SysLastUpdatedDate
FROM
	utb_claim c WITH(NOLOCK)
	INNER JOIN utb_claim_aspect ca WITH(NOLOCK)
		ON ca.lynxid = c.lynxid
	INNER JOIN utb_insurance i WITH(NOLOCK)
		ON i.InsuranceCompanyID = c.InsuranceCompanyID
	INNER JOIN utb_user u WITH(NOLOCK)
		ON u.UserID = c.CarrierRepUserID
WHERE
	ca.SourceApplicationID = 7  --4 fnol 7 ws
ORDER BY
	c.SysLastUpdatedDate DESC">
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>
              </asp:MultiView>
            </td>
          </tr>
        </table>
    </div>    

    <!-------------------------------------------->
    <!-- APD to Hyperquest Integrations Section -->
    <!-------------------------------------------->
    <div>
        <!-- APD to Hyperquest Integrations -->

        <table width="100%" align="left">
        <tr>
            <td>
                <h2>
                    <asp:Image ID="imgOutbound" runat="server" ImageUrl="/APDMonitor/Images/Outbound.png" />
                    &nbsp;APD to Hyperquest Integrations</h2>
            </td>
        </tr>

          <tr>
            <td>
              <asp:Button Text="Processed Jobs" BorderStyle="None" ID="btnProcessedJobs" CssClass="Initial" runat="server" OnClick="Processed_Jobs_Click" />
              <asp:Button Text="Unprocessed Jobs" BorderStyle="None" ID="btnUnProcessedJobs" CssClass="Initial" runat="server" OnClick="UnProcessed_Jobs_Click" />
              <asp:Button Text="Failed Jobs" BorderStyle="None" ID="btnFailedJobs" CssClass="Initial" runat="server" OnClick="Failed_Jobs_Click" />
              <asp:MultiView ID="MainView" runat="server">
                <asp:View ID="View1" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Processed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbJobsProcessedRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvHQJobMonitor_Processed" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Database_Processed" GridLines="Horizontal">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="InscCompID" HeaderText="InscCompID" SortExpression="InscCompID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="ReceivedDate" SortExpression="ReceivedDate" />
                                    <asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" />
                                    <asp:BoundField DataField="DocumentSource" HeaderText="DocumentSource" SortExpression="DocumentSource" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                    <asp:BoundField DataField="JobStatusDetails" HeaderText="JobStatusDetails" SortExpression="JobStatusDetails" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_Database_Processed" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT TOP 10
	                            InscCompID
	                            , LynxID
	                            , ClaimNumber
	                            , ReceivedDate
	                            , DocumentTypeName
	                            , DocumentSource
                                            , JobStatus
	                            , JobStatusDetails
                            FROM
	                            utb_hyperquestsvc_jobs WITH(NOLOCK)
                            WHERE
	                            UPPER(JobStatus) = 'PROCESSED'
                            ORDER BY
	                            SysLastUpdatedDate DESC">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>
                <asp:View ID="View2" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Unprocessed Jobs Waiting: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbJobsWaitingRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvHQJobMonitor_Unprocessed" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Database_Unprocessed" GridLines="Horizontal">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="InscCompID" HeaderText="InscCompID" SortExpression="InscCompID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="ReceivedDate" SortExpression="ReceivedDate" />
                                    <asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" />
                                    <asp:BoundField DataField="DocumentSource" HeaderText="DocumentSource" SortExpression="DocumentSource" />
                                    <asp:BoundField DataField="JobStatusDetails" HeaderText="JobStatusDetails" SortExpression="JobStatusDetails" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_Database_Unprocessed" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="IF EXISTS(
                            SELECT TOP 10
	                            ISNULL(InscCompID,'')
	                            , ISNULL(LynxID,'')
	                            , ISNULL(ClaimNumber,'')
	                            , ISNULL(ReceivedDate,'')
	                            , ISNULL(DocumentTypeName,'')
	                            , ISNULL(DocumentSource,'')
                                            , ISNULL(JobStatus , '')
	                            , ISNULL(JobStatusDetails,'')
                            FROM
	                            utb_hyperquestsvc_jobs WITH(NOLOCK)
                            WHERE
	                            UPPER(JobStatus) = 'UNPROCESSED'
                            )
                            BEGIN
	                            SELECT TOP 10
		                            InscCompID
		                            , LynxID
		                            , ClaimNumber
		                            , ReceivedDate
		                            , DocumentTypeName
		                            , DocumentSource
		                            , JobStatusDetails
	                            FROM
		                            utb_hyperquestsvc_jobs
	                            WHERE
		                            UPPER(JobStatus) = 'UNPROCESSED'
	                            ORDER BY
		                            SysLastUpdatedDate DESC
                            END
                            ELSE
                            BEGIN
	                            SELECT 
		                            '' InscCompID
		                            , '' LynxID
		                            , '' ClaimNumber
		                            , '' ReceivedDate
		                            , '' DocumentTypeName
		                            , '' DocumentSource
                                                            , '' JobStatus
		                            , '' JobStatusDetails
                            END">
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>

                <asp:View ID="View3" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Failed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbJobsFailedRefresh" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvHQJobMonitor_Failed" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Database_Failed" GridLines="Horizontal" OnRowDataBound="gvHQJobMonitor_Failed_RowDataBound" >
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="InscCompID" HeaderText="InscCompID" SortExpression="InscCompID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="ReceivedDate" SortExpression="ReceivedDate" />
                                    <asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" />
                                    <asp:BoundField DataField="DocumentSource" HeaderText="DocumentSource" SortExpression="DocumentSource" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                    <asp:BoundField DataField="ErrorMessages" HeaderText="ErrorMessages" SortExpression="ErrorMessages" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_Database_Failed" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="IF EXISTS(
                            SELECT TOP 10
	                            ISNULL(InscCompID,'')
	                            , ISNULL(LynxID,'')
	                            , ISNULL(ClaimNumber,'')
	                            , ISNULL(ReceivedDate,'')
	                            , ISNULL(DocumentTypeName,'')
	                            , ISNULL(DocumentSource,'')
                                            , ISNULL(JobStatus,'')
                                            , ISNULL(ErrorMessages,'')
                            FROM
	                            utb_hyperquestsvc_jobs WITH(NOLOCK)
                            WHERE
	                            UPPER(JobStatus) = 'FAILED'
                            )
                            BEGIN
	                            SELECT TOP 10
		                            InscCompID
		                            , LynxID
		                            , ClaimNumber
		                            , ReceivedDate
		                            , DocumentTypeName
		                            , DocumentSource
                                                            , JobStatus
                                                            , ErrorMessages
	                            FROM
		                            utb_hyperquestsvc_jobs
	                            WHERE
		                            UPPER(JobStatus) = 'FAILED'
	                            ORDER BY
		                            SysLastUpdatedDate DESC
                            END
                            ELSE
                            BEGIN
	                            SELECT 
		                            '' InscCompID
		                            , '' LynxID
		                            , '' ClaimNumber
		                            , '' ReceivedDate
		                            , '' DocumentTypeName
		                            , '' DocumentSource
                                                            , '' JobStatus
                                                            , '' ErrorMessages
                            END">
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>
              </asp:MultiView>
            </td>
          </tr>
        </table>
    </div>

    <!------------------------------------------->
    <!-- Outbound Electronic Documents Section -->
    <!------------------------------------------->
    <div>
        <!-- Outbound Electronic Documents Integrations -->
        <table width="100%" align="left">
        <tr>
            <td>
                <h2>
                    <asp:Image ID="Image3" runat="server" ImageUrl="/APDMonitor/Images/Outbound.png" />
                    &nbsp;Outbound Electronic Documents Integrations</h2>
            </td>
        </tr>

          <tr>
            <td>
              <asp:Button Text="Processed Jobs" BorderStyle="None" ID="btnOutboundElecDocs_Processed" CssClass="Initial" runat="server" OnClick="Outbound_Elec_Processed_Jobs_Click" />
              <asp:Button Text="Unprocessed Jobs" BorderStyle="None" ID="btnOutboundElecDocs_Unprocessed" CssClass="Initial" runat="server" OnClick="Outbound_Elec_UnProcessed_Jobs_Click" />
              <asp:Button Text="Failed Jobs" BorderStyle="None" ID="btnOutboundElecDocs_Failed" CssClass="Initial" runat="server" OnClick="Outbound_Elec_Failed_Jobs_Click" />
              <asp:MultiView ID="OutboundElecDocs" runat="server">
                <asp:View ID="View4" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Processed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton1" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvOutboundElecDocs_Processed" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_OutboundElecDocs_Processed" GridLines="Horizontal">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="EventName" HeaderText="EventName" SortExpression="EventName" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                    <asp:BoundField DataField="SysLastUpdatedDate" HeaderText="SysLastUpdatedDate" SortExpression="SysLastUpdatedDate" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_OutboundElecDocs_Processed" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT TOP 10
	                            i.name Name
	                            , eoq.LynxID LynxID
	                            , eoq.EventName EventName
	                            , eoq.Status Status
	                            , eoq.SysLastUpdatedDate SysLastUpdatedDate
                            FROM 
	                            utb_elec_outbound_queue eoq WITH(NOLOCK)
	                            INNER JOIN utb_insurance i WITH(NOLOCK)
	                            ON i.InsuranceCompanyID = eoq.InsuranceComapnyID
                            WHERE
	                            Status = 'Processed'
                            ORDER BY
	                            eoq.SysLastUpdatedDate DESC">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>
                <asp:View ID="View5" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Unprocessed Jobs Waiting: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton2" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Database_Unprocessed" GridLines="Horizontal">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="InscCompID" HeaderText="InscCompID" SortExpression="InscCompID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="ReceivedDate" SortExpression="ReceivedDate" />
                                    <asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" />
                                    <asp:BoundField DataField="DocumentSource" HeaderText="DocumentSource" SortExpression="DocumentSource" />
                                    <asp:BoundField DataField="JobStatusDetails" HeaderText="JobStatusDetails" SortExpression="JobStatusDetails" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="IF EXISTS(
                            SELECT TOP 10
	                            ISNULL(InscCompID,'')
	                            , ISNULL(LynxID,'')
	                            , ISNULL(ClaimNumber,'')
	                            , ISNULL(ReceivedDate,'')
	                            , ISNULL(DocumentTypeName,'')
	                            , ISNULL(DocumentSource,'')
                                            , ISNULL(JobStatus , '')
	                            , ISNULL(JobStatusDetails,'')
                            FROM
	                            utb_hyperquestsvc_jobs WITH(NOLOCK)
                            WHERE
	                            UPPER(JobStatus) = 'UNPROCESSED'
                            )
                            BEGIN
	                            SELECT TOP 10
		                            InscCompID
		                            , LynxID
		                            , ClaimNumber
		                            , ReceivedDate
		                            , DocumentTypeName
		                            , DocumentSource
		                            , JobStatusDetails
	                            FROM
		                            utb_hyperquestsvc_jobs
	                            WHERE
		                            UPPER(JobStatus) = 'UNPROCESSED'
	                            ORDER BY
		                            SysLastUpdatedDate DESC
                            END
                            ELSE
                            BEGIN
	                            SELECT 
		                            '' InscCompID
		                            , '' LynxID
		                            , '' ClaimNumber
		                            , '' ReceivedDate
		                            , '' DocumentTypeName
		                            , '' DocumentSource
                                                            , '' JobStatus
		                            , '' JobStatusDetails
                            END">
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>

                <asp:View ID="View7" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 Failed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton3" runat="server">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_Database_Failed" GridLines="Horizontal" OnRowDataBound="gvHQJobMonitor_Failed_RowDataBound" >
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="InscCompID" HeaderText="InscCompID" SortExpression="InscCompID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="ReceivedDate" SortExpression="ReceivedDate" />
                                    <asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" />
                                    <asp:BoundField DataField="DocumentSource" HeaderText="DocumentSource" SortExpression="DocumentSource" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                    <asp:BoundField DataField="ErrorMessages" HeaderText="ErrorMessages" SortExpression="ErrorMessages" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="IF EXISTS(
                            SELECT TOP 10
	                            ISNULL(InscCompID,'')
	                            , ISNULL(LynxID,'')
	                            , ISNULL(ClaimNumber,'')
	                            , ISNULL(ReceivedDate,'')
	                            , ISNULL(DocumentTypeName,'')
	                            , ISNULL(DocumentSource,'')
                                            , ISNULL(JobStatus,'')
                                            , ISNULL(ErrorMessages,'')
                            FROM
	                            utb_hyperquestsvc_jobs WITH(NOLOCK)
                            WHERE
	                            UPPER(JobStatus) = 'FAILED'
                            )
                            BEGIN
	                            SELECT TOP 10
		                            InscCompID
		                            , LynxID
		                            , ClaimNumber
		                            , ReceivedDate
		                            , DocumentTypeName
		                            , DocumentSource
                                                            , JobStatus
                                                            , ErrorMessages
	                            FROM
		                            utb_hyperquestsvc_jobs
	                            WHERE
		                            UPPER(JobStatus) = 'FAILED'
	                            ORDER BY
		                            SysLastUpdatedDate DESC
                            END
                            ELSE
                            BEGIN
	                            SELECT 
		                            '' InscCompID
		                            , '' LynxID
		                            , '' ClaimNumber
		                            , '' ReceivedDate
		                            , '' DocumentTypeName
		                            , '' DocumentSource
                                                            , '' JobStatus
                                                            , '' ErrorMessages
                            END">
                            </asp:SqlDataSource>
                        </p>
                      </td>
                    </tr>
                  </table>
                </asp:View>
              </asp:MultiView>
            </td>
          </tr>
        </table>
    </div>

    <!---------------------------->
    <!-- APD PostOffice Section -->
    <!---------------------------->
    <div>
        <!-- APD PostOffice Integrations -->
        <table width="100%" align="left">
        <tr>
            <td>
                <h2>
                    <asp:Image ID="Image2" runat="server" ImageUrl="/APDMonitor/Images/Inbound.png" />
                    &nbsp;APD PostOffice</h2>
            </td>
        </tr>
          <tr>
            <td>
              <asp:Button Text="Processing Jobs" BorderStyle="None" ID="btnAPDPostOfficeJobs_Processed" CssClass="Initial" runat="server" OnClick="APDPostOffice_Processed_Click" />
              <asp:Button Text="Complete Jobs" BorderStyle="None" ID="btnAPDPostOfficeJobs_Complete" CssClass="Initial" runat="server" OnClick="APDPostOffice_Complete_Click" />
              <asp:Button Text="Failed Jobs" BorderStyle="None" ID="btnAPDPostOfficeJobs_Failed" CssClass="Initial" runat="server" OnClick="APDPostOffice_Failed_Click" />

                <asp:MultiView ID="APDPostOfficeMultiView" runat="server">
                <asp:View ID="APDPostOfficeJobs_View" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 APD PostOffice Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkAPDPostOffice_Refresh" runat="server" OnClick="lnkAPDPostOffice_Refresh_Click">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvAPDPostOfficeJobs" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_APDPostOfficeJobs" GridLines="Horizontal" DataKeyNames="JobID,StepID" OnRowDataBound="gvAPDPostOfficeJobs_Processed_RowDataBound">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="JobID" HeaderText="JobID" SortExpression="JobID" ReadOnly="True" />
                                    <asp:BoundField DataField="StepID" HeaderText="StepID" SortExpression="StepID" ReadOnly="True" />
                                    <asp:BoundField DataField="Method" HeaderText="Method" SortExpression="Method" />
                                    <asp:BoundField DataField="ClientID" HeaderText="ClientID" SortExpression="ClientID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" ReadOnly="True" />
                                    <asp:BoundField DataField="JobSubmitted" HeaderText="JobSubmitted" SortExpression="JobSubmitted" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_APDPostOfficeJobs" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT TOP 10
	JobID
	, StepID
	, Method
--	, COALESE(0,'', ClientID)	AS ClientID
	, ClientID
	, ISNULL(CONVERT(VARCHAR(50),LynxID),'')	AS LynxID
	, ISNULL(ClaimNumber,'')	AS ClaimNumber 
                , JobSubmitted  
	, JobStatus
FROM 
	utb_postoffice_jobs
ORDER BY
	JobSubmitted DESC">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>

                <!-- APD PostOffice Complete -->
                <asp:View ID="APDPostOfficeJobs_Complete_View" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 APD PostOffice Completed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkAPDPostOfficeComplete_Refresh" runat="server" OnClick="lnkAPDPostOfficeComplete_Refresh_Click">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvAPDPostOfficeJobs_Complete" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_APDPostOfficeJobs_Complete" GridLines="Horizontal" DataKeyNames="JobID,StepID">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="JobID" HeaderText="JobID" SortExpression="JobID" ReadOnly="True" />
                                    <asp:BoundField DataField="StepID" HeaderText="StepID" SortExpression="StepID" ReadOnly="True" />
                                    <asp:BoundField DataField="Method" HeaderText="Method" SortExpression="Method" />
                                    <asp:BoundField DataField="ClientID" HeaderText="ClientID" SortExpression="ClientID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" ReadOnly="True" />
                                    <asp:BoundField DataField="JobSubmitted" HeaderText="JobSubmitted" SortExpression="JobSubmitted" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_APDPostOfficeJobs_Complete" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT TOP 10
	JobID
	, StepID
	, Method
--	, COALESE(0,'', ClientID)	AS ClientID
	, ClientID
	, ISNULL(CONVERT(VARCHAR(50),LynxID),'')	AS LynxID
	, ISNULL(ClaimNumber,'')	AS ClaimNumber 
                , JobSubmitted  
	, JobStatus
FROM 
	utb_postoffice_jobs
WHERE
                UPPER(JobStatus) = 'COMPLETED'
ORDER BY
	JobSubmitted DESC">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>

                <!-- APD PostOffice Failed -->
                <asp:View ID="APDPostOfficeJobs_Failed_View" runat="server">
                  <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                    <tr>
                      <td>
                        <h3>
                            Last<span> 10 APD PostOffice Failed Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkAPDPostOfficeFailed_Refresh" runat="server" OnClick="lnkAPDPostOfficeFailed_Refresh_Click">Refresh</asp:LinkButton>
                            </span>
                        </h3>
                        
                        <p>
                            <asp:GridView ID="gvAPDPostOfficeJobs_Failed" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="SqlDataSource_APDPostOfficeJobs_Failed" GridLines="Horizontal" DataKeyNames="JobID,StepID" OnRowDataBound="gvAPDPostOfficeJobs_Failed_RowDataBound" >
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:BoundField DataField="JobID" HeaderText="JobID" SortExpression="JobID" ReadOnly="True" />
                                    <asp:BoundField DataField="StepID" HeaderText="StepID" SortExpression="StepID" ReadOnly="True" />
                                    <asp:BoundField DataField="Method" HeaderText="Method" SortExpression="Method" />
                                    <asp:BoundField DataField="ClientID" HeaderText="ClientID" SortExpression="ClientID" />
                                    <asp:BoundField DataField="LynxID" HeaderText="LynxID" SortExpression="LynxID" ReadOnly="True" />
                                    <asp:BoundField DataField="ClaimNumber" HeaderText="ClaimNumber" SortExpression="ClaimNumber" ReadOnly="True" />
                                    <asp:BoundField DataField="JobSubmitted" HeaderText="JobSubmitted" SortExpression="JobSubmitted" />
                                    <asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource_APDPostOfficeJobs_Failed" runat="server" ConnectionString="<%$ ConnectionStrings:stg_udb_apd_ConnectionString %>" 
                            SelectCommand="SELECT
	JobID
	, StepID
	, Method
--	, COALESE(0,'', ClientID)	AS ClientID
	, ClientID
	, ISNULL(CONVERT(VARCHAR(50),LynxID),'')	AS LynxID
	, ISNULL(ClaimNumber,'')	AS ClaimNumber
                , JobSubmitted  
	, JobStatus
FROM 
	utb_postoffice_jobs
WHERE
                UPPER(JobStatus) = 'FAILED'
ORDER BY
	JobSubmitted DESC ">
                            </asp:SqlDataSource>
                        </p>

                      </td>
                    </tr>
                  </table>
                </asp:View>


              </asp:MultiView>
            </td>
          </tr>
        </table>
    </div>    

</asp:Content>
