﻿function GetMMDDYY(ISODate) {
    var date, year, month, dt;

    try {
        date = new Date(ISODate);
        year = date.getFullYear();
        month = date.getMonth() + 1;
        dt = date.getDate();
    } catch (e) {

    }
    return month + '/' + dt + '/' + year;
}

function formatAMPM(dt) {
    var date = new Date(dt);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var milliseconds = date.getMilliseconds();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ':' + milliseconds + ' ' + ampm;
    return strTime;
}

function GetDateTime(dt) {
    return GetMMDDYY(dt) + " " + formatAMPM(dt);
}