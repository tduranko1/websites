﻿var APDWSURL;
var InvoiceData;

$(document).ready(function () {
    LoadWSAPIURL();
    GetInvoiceAJAX();
    GetInvoiceDetailsClick();
    AddPayeeClick();
    ShowInvoicesClick();
});

function LoadWSAPIURL()
{
    try {
        APDWSURL = $("#MainContent_hf_WSAPIServerURL").val();
    } catch (e) {
        console.log(e);
    }
}

function ShowInvoicesClick()
{
    try {
        $(document).on("click", "#inp_btn_ShowInvoiceDetails", function () {
            alert("HI");
            GetInvoiceAJAX();
        });
        
    } catch (e) {
    
    }
}

function GetInvoiceAJAX() {
    try {
        $.ajax({
            //url: APDWSURL + "APDWSApi/ClaimDetailsJSON?LynxId=" + $("#txtLynxID").val(),
            url: APDWSURL + "APDWSApiFinancial/GetAllInvoicesJSON?LYNXID=" + $("#txtLynxID").val() + "&INVTYPE=A",
            type: "GET",
            beforeSend: function () {

            },
            dataType: "JSON",
            success: function (result) {
                //console.log(result);
                $("#hf_InvoiceData").val(JSON.stringify(result.Root));
                InvoiceData=JSON.parse($("#hf_InvoiceData").val());
                renderInvoiceList(result.Root);
            }
        });

    } catch (e) {
        console.log(e);
    }
}

function renderInvoiceList(Root) {
    var lynxID;
    var tableInvoiceList = $("#tb_InvoiceList");
    try {
        lynxID = Root['@LynxID'];
        var InvoiceList = Root.Invoice;

        $.each(InvoiceList, function (key, value) {
            //console.log(value);
            $(tableInvoiceList).append(renderInvoiceListItems(value, lynxID));
        });

    } catch (e) {
        console.log(e);
    }
}

function renderInvoiceListItems(invoiceItem, lynxID) {
    var tr = $("<tr>");
    var td;
    var input;
    var invoiceID = invoiceItem['@InvoiceID'];

    try {



        //Edit and Delete Buttons
        td = $("<td>");
        input = $("<i>", { id: "edit_" + invoiceID, class: "fas fa-edit", style: "color: cornflowerblue" });
        $(td).append(input);
        $(tr).append(td);

        td = $("<td>");
        input = $("<i>", { id: "delete_" + invoiceID, class: "fas fa-trash", style: "color: #f70d1a" });
        $(td).append(input);
        $(tr).append(td);

        //Invoice ID
        td = $("<td>");
        input = $("<a>", { id: "a_invoice_" + invoiceID }).html(invoiceID);
        $(td).append(input);
        $(tr).append(td);

        //LynxID
        td = $("<td>");
        $(td).append(lynxID);
        $(tr).append(td);

        //Description
        td = $("<td>");

        $(td).append(invoiceItem['@Description']);
        $(tr).append(td);

        //DispatchNumber
        td = $("<td>");

        $(td).append(invoiceItem['@DispatchNumber']);
        $(tr).append(td);

        //Item Type
        td = $("<td>");
        $(td).append(GetItemType(invoiceItem['@ItemTypeCD']));
        $(tr).append(td);

        //Amount
        td = $("<td>");
        $(td).append(invoiceItem['@Amount']);
        $(tr).append(td);

        //Entered
        td = $("<td>");
        $(td).append(GetMMDDYY(invoiceItem['@EntryDate']));
        $(tr).append(td);

        //Status
        td = $("<td>");
        $(td).append(invoiceItem['@StatusCD']);
        $(tr).append(td);

        //Status Date
        td = $("<td>");
        $(td).append(GetMMDDYY(invoiceItem['@StatusDate']));
        $(tr).append(td);

        //SysLastUpdatedDate
        td = $("<td>");
        $(td).append(GetDateTime(invoiceItem['@SysLastUpdatedDate']));
        $(tr).append(td);
    } catch (e) {
        console.log(e);
    }

    return tr;
}





function getInvoiceDetails(InvoiceID,InvoiceRoot) {
    var Invoice;
    try {
        //console.log(JSON.stringify(InvoiceList));
        $.each(InvoiceRoot.Invoice, function (key, value) {
            if(value["@InvoiceID"]==InvoiceID)
            {
                Invoice = value;
            }
        });        

    } catch (e) {
        console.log(e);
    }
    return Invoice;
}

function GetInvoiceDetailsClick()
{
    var Invoice;
    //var InvoiceRoot;
    try {
        

        $(document).on("click", "a[id^=\"a_invoice\"]", function () {
            //InvoiceRoot = JSON.parse($("#hf_InvoiceData").val());
            Invoice = getInvoiceDetails($(this).attr("id").replace("a_invoice_", ""), InvoiceData);
            renderInvoiceDetails(Invoice, InvoiceData);
        });

    } catch (e) {
        console.log(e);
    }
}

function renderInvoiceDetails(Invoice,InvoiceRoot) {

    try {
        //console.log(Invoice);
        //console.log(InvoiceRoot);
        //alert(GetFeeCategory(Invoice["@FeeCategoryCD"]));
        $("#inp_txt_category").val(GetFeeCategory(Invoice["@FeeCategoryCD"]));
        $("#inp_txt_status").val(Invoice["@StatusCD"]);
        $("#inp_txt_billing_user").val(Invoice["@RecordingUserName"]);
        $("#inp_txt_appears_invoice").val(Invoice["@ItemizeFlag"]==1?true:false);
        $("#inp_txt_bill_entry_date").val(GetDateTime(Invoice["@EntryDate"]));
        $("#inp_txt_invoice_method").val(GetInvoiceMethod(InvoiceRoot["@InvoiceMethodCD"]));
        $("#inp_txt_pickup_date").val(GetDateTime(Invoice["@StatusDate"]));
        $("#inp_txt_invoice_model").val(GetInvoiceModel(Invoice["@Model"]));
        $("#inp_txt_gen_invoice").val((InvoiceRoot["@InvoiceMethodCD"] == "P" && Invoice["@Model"] == "C") ? true : false);
        $("#inp_txt_amount").val(Invoice["@Amount"]);
        
    } catch (e) {

    }
}

function AddPayeeClick()
{
    try {
        var dialog;
        dialog = $("#dialog-add-payment").dialog({
            autoOpen: false,
            height: 450,
            width: 800,
            modal: true,
            buttons: {
                "Add Payment":addPayment,
                    "Payment History":paymentHistory,
                    Cancel: function () {
                        dialog.dialog("close");
                    }
            },
            open:function()
            {
                var Payee = InvoiceData.Payee;
                //console.log(Payee);

                //LoadPaymentTypeDD
                LoadPaymentTypeDD();

                if(typeof(Payee)!=undefined){
                    $("#inp_addPay_Amount").val(InvoiceData.ClaimAspect[1]["@LatestEstimateTotal"]);
                    $("#inp_addPay_Payee").val(Payee["@PayeeName"]);
                    $("#inp_addPay_TotalTax").val(InvoiceData.ClaimAspect[1]["@LatestEstimateTotalTax"]);
                    $("#inp_addPay_PayeeType").val(GetPayeeTypeCD(Payee["@PayeeTypeCD"]));
                    $("#inp_addPay_Deductible").val(InvoiceData.ClaimAspect[1]["@LatestEstimateDeductible"]);
                    $("#inp_addPay_Address1").val(Payee["@PayeeAddress1"]);
                    $("#sel_addPay_PaymentType").val();
                    $("#inp_addPay_Address2").val(Payee["@PayeeAddress2"]);
                    $("#inp_addPay_City").val(Payee["@PayeeAddressCity"]);
                    $("#inp_addPay_State").val(Payee["@PayeeAddressState"]);
                    $("#inp_addPay_Zip").val(Payee["@PayeeAddressZip"]);

                }
            },
            close: function () {
                //allFields.removeClass("ui-state-error");
            }
        });

        $("#inp_btn_AddPay").on("click", function () {
            dialog.dialog("open");
        });

       
    } catch (e) {
        console.log(e);
    }
    
}

function addPayment()
{
    try {
        
    } catch (e) {
        console.log(e);
    }
}

function paymentHistory()
{
    try {
       // console.log();
    } catch (e) {
        console.log(e);
    }
}

function LoadPaymentTypeDD()
{
    var ddItem;
    try {
        $.each(InvoiceData.Reference, function (key, value) {
            //console.log(value);
            if (value["@List"] == "ItemTypeCD") {
                //console.log(value);
                ddItem = $("<option>", { "value": value["@ReferenceId"] }).html(value["@Name"]);;
                //$(ddItem).html(value["@Name"]);
                $("#sel_addPay_PaymentType").append(ddItem);
            }
        });
    } catch (e) {

    }
}



