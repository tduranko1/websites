﻿<%@ Page Title="Restart Services" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RestartServices.aspx.cs" Inherits="APDMonitor1.RestartServices" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 80%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>

    <div class="jumbotron">
        <h3><b>Restart Services in APD Monitor 
            <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label></b></h3>
        <p class="lead">APD Monitor is the a web site that shows details about the different parts of the APD Process.&nbsp; It also provides UP/DOWN status of each of the components involved in the APD Process as well as here we can restart our services if service is DOWN. </p>
    </div>
    <table>
        <tr>
            <th>Component</th>
            <th>Service Name</th>
            <th>Service Status</th>
            <th>Restart the service</th>
        </tr>
        <%--<tr>
            <td>Claims Today</td>
            <td>PGWClaimPointFoundation</td>
            <td>
                <asp:Label ID="lblClaims" Text="Running" runat="server" /></td>
            <td>
                <asp:Button ID="btnClaims" Text="Restart" runat="server" OnClick="lbtnClaims_Click" CssClass="btn btn-primary btn-xs" />
            </td>
        </tr>--%>
        <tr>
            <td>Shop Assign Today</td>
            <td>LYNXChoiceShopService</td>
            <td>
                <asp:Label ID="lblShopAssign" Text="Running1" runat="server" /></td>
            <td>
                <asp:Button ID="btnShopAssign" Text="Restart" runat="server" OnClick="lbtnShopAssign_Click" CssClass="btn btn-primary btn-xs" />
            </td>
        </tr>
        <tr>
            <td>APD to HQ Today</td>
            <td>LYNXHyperquestService</td>
            <td>
                <asp:Label ID="lblAPDtoHQ" Text="Running" runat="server" /></td>
            <td>
                <asp:Button ID="btnAPDtoHQ" Text="Restart" runat="server" OnClick="lbtnAPDtoHQ_Click" CssClass="btn btn-primary btn-xs" />
            </td>
        </tr>
        <%--<tr>
            <td>HQ to APD Today</td>
            <td>PartnerService</td>
            <td>
                <asp:Label ID="lblHQtoAPD" Text="Running" runat="server" /></td>
            <td>
                <asp:Button ID="btHQtoAPD" Text="Restart" runat="server" OnClick="lbtHQtoAPD_Click" CssClass="btn btn-primary btn-xs" />
            </td>
        </tr>--%>
        <tr>
            <td>Doc Proc Queue Today</td>
            <td>DocProcQueuePickup</td>
            <td>
                <asp:Label ID="lblDocProc" Text="Running" runat="server" /></td>
            <td>
                <asp:Button ID="btnDocProc" Text="Restart" runat="server" OnClick="lbtnDocProc_Click" CssClass="btn btn-primary btn-xs" />
        </tr>
        <tr>
            <td>Outbound Electronic Today</td>
            <td>EMSHelper</td>
            <td>
                <asp:Label ID="lblOutBound" Text="Running" runat="server" /></td>
            <td>
                <asp:Button ID="btnOutBound" Text="Restart" runat="server" OnClick="lbtnOutBound_Click" CssClass="btn btn-primary btn-xs" />
            </td>
        </tr>
    </table>

</asp:Content>
