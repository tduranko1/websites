﻿<%@ Page Title="Invoice" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="APDMonitor1.Invoice" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="CSS/jquery-ui-1.12.1/jquery-ui.min.css">
    <script src="CSS/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="Scripts/CommonScripts/CommonHelper.js"></script>
    <script src="Scripts/CommonScripts/InvoiceHelper.js"></script>
    <script src="Scripts/CommonScripts/Invoice.js"></script>

    <link href="CSS/fontawesome-5.15.3/css/all.css" rel="stylesheet">
    <link href="CSS/Common/Invoice.css" rel="stylesheet" />
    <style>
        .ui-widget-overlay {
            background: #AAA url(images/ui-bg_flat_0_aaaaaa_40x100.png) 50% 50% repeat-x;
            opacity: .30;
            filter: Alpha(Opacity=30);
        }
    </style>
    <div>
        <h4>Actions :</h4>
        <div id="div_header" style="margin-top: 10px;">
            <label>Lynx ID:</label>
            <input type="text" id="txtLynxID" value="1850978" />
            <input class="btn btn-success btn-sm" type="button" id="inp_btn_ShowInvoiceDetails" value="Show Invoice Details" />
            <input class="btn btn-info btn-sm" type="button" id="inp_btn_AddPay" value="Add Pay" />
            <input class="btn btn-info btn-sm" type="button" id="inp_btn_GenInvoice" value="Generate Invoice" />
        </div>
        <table id="tb_InvoiceMain">
            <tr>
                <td>
                    <h4>Invoice List :</h4>
                    <div id="div_InvoiceList">
                        <table id="tb_InvoiceList" class="datatable">
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Invoice ID</th>
                                <th>Lynx ID</th>
                                <th>Description</th>
                                <th>Dispatch No.</th>
                                <th>Item Type</th>
                                <th>Amount</th>
                                <th>Entered</th>
                                <th>Status</th>
                                <th>Status Date</th>
                                <th>SysLastUpdatedDate</th>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Invoice Details :</h4>
                    <table id="tb_InvoiceDetails" class="datatable">
                        <tr>
                            <td>
                                <label>Category</label></td>
                            <td>
                                <input id="inp_txt_category" type="text" readonly="readonly" /></td>
                            <td>
                                <label>Status</label></td>
                            <td>
                                <input id="inp_txt_status" type="text" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label>Billing User</label></td>
                            <td>
                                <input id="inp_txt_billing_user" type="text" readonly="readonly" /></td>
                            <td>
                                <label>Appears Invoice</label></td>
                            <td>
                                <input id="inp_txt_appears_invoice" type="text" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label>Bill Entry Date</label></td>
                            <td>
                                <input id="inp_txt_bill_entry_date" type="text" readonly="readonly" /></td>
                            <td>
                                <label>Invoice Method</label></td>
                            <td>
                                <input id="inp_txt_invoice_method" type="text" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label>Pickup Date</label></td>
                            <td>
                                <input id="inp_txt_pickup_date" type="text" readonly="readonly" /></td>
                            <td>
                                <label>Invoice Model</label></td>
                            <td>
                                <input id="inp_txt_invoice_model" type="text" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label></label>
                            </td>
                            <td></td>
                            <td>
                                <label>Generate Invoice</label></td>
                            <td>
                                <input id="inp_txt_gen_invoice" type="text" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label></label>
                            </td>
                            <td></td>
                            <td>
                                <label>Amount</label></td>
                            <td>
                                <input id="inp_txt_amount" type="text" readonly="readonly" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <%--Add Payee Modal--%>
        <div id="dialog-add-payment" title="Add Payment">

            <table id="tb_Add_Payment">
                <tr>
                    <td>
                        <label>Amount</label>
                    </td>
                    <td><input id="inp_addPay_Amount" type="text" /></td>
                    <td>
                        <label>Payee</label>
                    </td>
                    <td><input id="inp_addPay_Payee" type="text" /></td>
                </tr>

                <tr>
                    <td>
                        <label>Total Tax</label>
                    </td>
                    <td><input id="inp_addPay_TotalTax" type="text" /></td>
                    <td>
                        <label>Payee Type</label>
                    </td>
                    <td><input id="inp_addPay_PayeeType" type="text" /></td>
                </tr>

                <tr>
                    <td>
                        <label>Deductible</label>
                    </td>
                    <td><input id="inp_addPay_Deductible" type="text" /></td>
                    <td>
                        <label>Address 1</label>
                    </td>
                    <td><input id="inp_addPay_Address1" type="text" /></td>
                </tr>

                <tr>
                    <td>
                        <label>Payment Type</label>
                    </td>
                    <td><select id="sel_addPay_PaymentType"></select></td>
                    <td>
                        <label>Address 2</label>
                    </td>
                    <td><input id="inp_addPay_Address2" type="text" /></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <label>City</label>
                    </td>
                    <td><input id="inp_addPay_City" type="text" /></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <label>State</label>
                    </td>
                    <td><input id="inp_addPay_State" type="text" /></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <label>Zip</label>
                    </td>
                    <td><input id="inp_addPay_Zip" type="text" /></td>
                </tr>
                <tr>
                    <td><label>Description :</label></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <textarea id="txt_area_addPay_Description" rows="4" cols="100"></textarea>
                    </td>
                </tr>


            </table>

            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position: absolute; top: -1000px">
        </div>



    </div>
    <input id="hf_WSAPIServerURL" type="hidden" runat="server" />
    <input id="hf_InvoiceData" type="hidden" />
</asp:Content>
