﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="APDMonitor1.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h3><b><%: Title %>.</b></h3>
        <p class="lead">The APD Monitor web site can be used to assist with verifying that APD is up and running and Claims and Job are processing successfully in all area's of APD including the 
        Document Processor, Claims coming in from APD ClaimPoint, FNOL, Web Service Assignments.  It will also provide you details about APD data being processed over to Hyperquest
        and any error information related to the APD to HQ process.
        </p>
    </div>
</asp:Content>
