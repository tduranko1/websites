﻿function GetFeeCategory(FeeCategoryCD) {
    try {
        switch (FeeCategoryCD) {
            case 'H':
                return "Handling Fee";
                break;
            case 'A':
                return "Additional Fee";
                break;
            default:
                return "";
                break;
        }
    } catch (e) {

    }

}

function GetInvoiceMethod(InvoiceMethodCD) {
    switch (InvoiceMethodCD) {
        case 'P':
            return "Paper";
            break;
        case 'F':
            return "File";
            break;
        default:
            break;
    }
}

function GetInvoiceModel(InvoiceMethodCD) {
    switch (InvoiceMethodCD) {
        case 'C':
            return "Per Claim";
            break;
        case 'B':
            return "Bulk";
            break;
        default:
            break;
    }
}

function GetItemType(ItemCD) {
    try {
        switch (ItemCD) {
            case 'F':
                return "Fee";
                break;

            case 'I':
                return "Indemnity";
                break;
        }
    } catch (e) {
        console.log(e);
    }

}

function GetPayeeTypeCD(PayeeCD)
{
    try {
        switch (PayeeCD) {
            case 'S':
                return "Shop";
                break;
            case 'A':
                return "Appraiser";
                break;
            case 'I':
                return "Involved";
                break;
            case 'V':
                return "Vendor";
                break;
            case 'O':
                return "Other";
                break;
            default:
                return "";
                break;
        }

    } catch (e) {
        console.log(e);
    }
}