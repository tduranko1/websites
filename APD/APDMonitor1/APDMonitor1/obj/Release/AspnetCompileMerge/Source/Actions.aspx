﻿<%@ Page Title="Actions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Actions.aspx.cs" Inherits="APDMonitor1.Actions" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h3><b><%: Title %>.</b></h3>
        <p class="lead">The APD Actions are functions that can be performed against APD.</p>
    </div>

    <table style="width: 100%">
        <tr>
            <td style="width: 273px; vertical-align: top;">
                <!-- Claim Details -->
                <h4><b>Action:</b></h4>
                <p>
                    <asp:Label runat="server">LynxID: </asp:Label>
                    <asp:TextBox ID="txtLynxID" runat="server" placeholder="1850978">1850978</asp:TextBox>
                </p>
                <hr />

                <!-- Action Items -->
                <asp:LinkButton ID="btInvoiceDetails" runat="server" OnClick="btInvoiceDetails_Click">Invoice Details</asp:LinkButton>
                <br />
                <asp:LinkButton ID="btInvoiceEdit" runat="server" OnClick="btInvoiceEdit_Click">Invoice Edit</asp:LinkButton>
                <br />
                <asp:LinkButton ID="btInvoiceAdd" runat="server" OnClick="btInvoiceAdd_Click">Invoice Add</asp:LinkButton>

            </td>
            <td style="width: 806px; vertical-align: top;">
                <h4>
                    <asp:Label ID="lblDisplay" runat="server" Text=""></asp:Label>
                </h4>
                <!-- Invoices -->
                <asp:GridView ID="gvFinancialInvoices" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowEditing="gvFinancialInvoices_RowEditing" OnRowDeleting="gvFinancialInvoices_RowDeleting" DataKeyNames="InvoiceID"  >
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteImageUrl="~/Images/delete.gif" ButtonType="Image" DeleteText="" />

                        <asp:TemplateField HeaderText="Inv ID">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblInvoiceID" runat="server" Text='<%# Bind("InvoiceID") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="LynxID">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLynxID" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblLynxID" runat="server" Text='<%# Bind("LynxID") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Disp Num">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDispatchNumber" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblDispatchNumber" runat="server" Text='<%# Bind("DispatchNumber") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Item Type">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtItemTypeCD" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblItemTypeCD" runat="server" Text='<%# Bind("ItemTypeCD") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entered">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEntryDate" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblEntryDate" runat="server" Text='<%# Bind("EntryDate") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtStatusCD" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblStatusCD" runat="server" Text='<%# Bind("StatusCD") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtStatusDate" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("StatusDate") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SysLastUpdatedDate">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSysLastUpdatedDate" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate> 
                                <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox> 
                            </FooterTemplate> 
                            <ItemTemplate> 
                                <asp:Label ID="lblSysLastUpdatedDate" runat="server" Text='<%# Bind("SysLastUpdatedDate") %>'></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <SortedAscendingCellStyle BackColor="#F4F4FD" />
                    <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                    <SortedDescendingCellStyle BackColor="#D8D8F0" />
                    <SortedDescendingHeaderStyle BackColor="#3E3277" />
               </asp:GridView>

                <br />
                <asp:Literal runat="server" id="txtDeleteConfirm" EnableViewState="false" />
                <asp:Button runat="server" ID="btnDelYes" Text="Yes" Visible="false" OnClick="btnDelYes_Click" />
                <asp:Button runat="server" ID="btnDelNo" Text="No" Visible="false" OnClick="btnDelNo_Click" />
                <br />
                <asp:ListBox runat="server" ID="lbDebug" />
            </td>
        </tr>
    </table>
</asp:Content>
