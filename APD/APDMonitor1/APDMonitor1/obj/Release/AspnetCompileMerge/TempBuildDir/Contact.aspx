﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="APDMonitor1.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h3><b><%: Title %>.</b></h3>
    </div>
    <address>
        <strong>Support:</strong>   <a href="mailto:itsupport@solera.com">itsupport@solera.com</a><br />
    </address>
</asp:Content>
