﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="APDMonitor1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<style>
    /*.blink_me {
            animation: blinker 1s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }*/

    /* The Modal (background) */
    .modal .modal-processing {
        position: fixed; /* Stay in place */
        z-index: 1047; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-processing {
        position: fixed; /* Stay in place */
        z-index: 1057; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        display: none;
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        border: 1px solid #888;
        width: 100%;
    }

    .modal-content-processing {
        margin: auto;
        width: 60%;
    }
</style>
<script type="text/javascript">

        $('#modal1').on('hidden.bs.modal', function (event) {
            $('#modal1').empty();
        });
       
        function ShowPopup() {
            $("#MyPopup").modal("show");
        }
        
</script>

<div class="jumbotron">
<h3><b>APDMonitor -
            <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label></b></h3>
<p class="lead">APDMonitor is the a web site that shows details about the different parts of the APD Process.&nbsp; It also provides UP/DOWN status of each of the components involved in the APD Process as well as performance information related to how long transactions are taking to process as compared to a base line measurement. </p>
</div>

<!-- Server UP/DOWN Status -->
<div class="row">
<div class="col-md-8">
<h3>Health Monitor/Statistics </h3>
<p>
Below are the current components invoived in the APD Process including the UP/DOWN status and performance statistics:
</p>
<p>
<asp:GridView ID="gvServerStatus_MainView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_MainView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="ServerName" HeaderText="ServerName" SortExpression="ServerName" ItemStyle-Width="250" />
<asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="50" ItemStyle-Font-Bold="true" />
<asp:BoundField DataField="LastRun" HeaderText="LastRun" ReadOnly="True" SortExpression="LastRun" ItemStyle-Width="150" />
<asp:BoundField DataField="Performance" HeaderText="Performance" SortExpression="Performance" ItemStyle-Width="50" ItemStyle-Font-Bold="true" ReadOnly="True" />
<asp:BoundField DataField="Baseline" HeaderText="Baseline" ReadOnly="True" SortExpression="Baseline" ItemStyle-Width="50" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
<asp:LinkButton ID="lbStatusRefresh" runat="server" OnClick="lbStatusRefresh_Click">Refresh</asp:LinkButton>

<%--<asp:SqlDataSource ID="SQLDS_ServerStatus" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
SubName AS 'ServerName'
, Value AS 'Status'
, '' AS 'LastRun'
, '' AS 'Performance' 
, '' AS 'Baseline' 
FROM utb_app_variable 
WHERE Name = 'Sys_Status' AND Description = 'Application Status - APD Production' "></asp:SqlDataSource>--%>
</p>
</div>
<div class="col-md-4">
<h3>
<asp:Button ID="btnRestartServices" Text="Restart Services" runat="server"
CssClass="btn btn-primary btn-xs nav navbar-nav navbar-right" OnClick="ShowPopup" />
</h3>
</div>
</div>

<%--<hr style="height: 2px; border-width: 0; color: gray; background-color: gray" />--%>

<div class="row">
<!-- Claim Count -->
<div class="col-md-4">
<h3>Claims Today</h3>
<p>
Count of claims by company today:   
</p>
<p>
<asp:GridView ID="gvServerStatus_ClaimCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_ClaimCountView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Insc Company" HeaderText="Insc Company" SortExpression="Insc Company" ItemStyle-Width="200" />
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="100" ItemStyle-Font-Bold="true" />
<asp:BoundField DataField="Claim Count" HeaderText="Claim Count" ReadOnly="True" SortExpression="Claim Count" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
</p>
<table style="width: 100%">
<tr>
<td style="width: 250px">&nbsp;</td>
<td>
<asp:DetailsView ID="dvTotalClaims" runat="server" Height="10px" Width="120px" Font-Bold="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>
</td>
</tr>
</table>

<%--<asp:SqlDataSource ID="SQLDS_TotalClaimCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	COUNT(c.InsuranceCompanyID) 'Total Claim Count'
FROM
	utb_claim c
	INNER JOIN utb_insurance i
		ON i.InsuranceCompanyID = c.InsuranceCompanyID
	INNER JOIN utb_claim_aspect ca
		ON ca.LynxID = c.LynxID
        AND ca.ClaimAspectTypeID = 0
	INNER JOIN utb_application a
		ON a.ApplicationID = ca.SourceApplicationID
WHERE 
	IntakeFinishDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
"></asp:SqlDataSource>--%>
<asp:LinkButton ID="lbClaimCountRefresh" runat="server" OnClick="lbClaimCountRefresh_Click">Refresh</asp:LinkButton>

<%--<asp:SqlDataSource ID="SQLDS_ClaimCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	i.Name 'Insc Company'
	, a.Name
	, COUNT(c.InsuranceCompanyID) 'Claim Count'
FROM
	utb_claim c
	INNER JOIN utb_insurance i
		ON i.InsuranceCompanyID = c.InsuranceCompanyID
	INNER JOIN utb_claim_aspect ca
		ON ca.LynxID = c.LynxID
                                AND ca.ClaimAspectTypeID = 0
	INNER JOIN utb_application a
		ON a.ApplicationID = ca.SourceApplicationID
WHERE 
	IntakeFinishDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
GROUP BY
	i.Name
	, a.Name
ORDER BY
	i.Name
	, a.Name
"></asp:SqlDataSource>--%>
</div>

<!-- Shop Assignments -->
<div class="col-md-4">
<h3>Shop Assign Today</h3>
<p>
Shop Assignment Transactions today:
</p>
<p>
<asp:GridView ID="gvServerStatus_ShopAssignCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_ShopAssignCountView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="200" />
<asp:BoundField DataField="ECnt" HeaderText="Processed Count" SortExpression="ECnt" ItemStyle-Width="100" ItemStyle-Font-Bold="true" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>

</p>
<table style="width: 100%">
<tr>
<td style="width: 150px">&nbsp;</td>
<td>
<asp:DetailsView ID="dvAssignmentTotal" runat="server" Height="10px" Width="150px" Font-Bold="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>
</td>
</tr>
</table>

<%--<asp:SqlDataSource ID="SQLDS_TotalAssignmentCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	COUNT(cm.Name) 'Total Assignment Count'
FROM
	utb_assignment a WITH(NOLOCK)
	INNER JOIN utb_communication_method cm WITH(NOLOCK)
		ON cm.CommunicationMethodID = a.CommunicationMethodID
WHERE
	a.AssignmentDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
"></asp:SqlDataSource>--%>

<asp:LinkButton ID="lbShopAssignCount" runat="server" OnClick="lbShopAssignCount_Click">Refresh</asp:LinkButton>

<%--<asp:SqlDataSource ID="SQLDS_ShopAssignCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT TOP 50 
	cm.Name
	, COUNT(cm.Name) ECnt
FROM
	utb_assignment a WITH(NOLOCK)
	INNER JOIN utb_communication_method cm WITH(NOLOCK)
		ON cm.CommunicationMethodID = a.CommunicationMethodID
WHERE
	a.AssignmentDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
GROUP BY
	cm.Name
ORDER BY
	cm.Name

"></asp:SqlDataSource>--%>
</div>
</div>

<hr style="height: 2px; border-width: 0; color: gray; background-color: gray" />

<div class="row">
<!-- APD to HQ Count -->
<div class="col-md-4">
<h3>APD to HQ Today</h3>
<p>
Transactions APD to HQ today:
</p>
<p>
<asp:GridView ID="gvServerStatus_APDtoHQCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_APDtoHQCountView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="200" />
<asp:BoundField DataField="DocumentTypeName" HeaderText="DocumentTypeName" SortExpression="DocumentTypeName" ItemStyle-Width="100" ItemStyle-Font-Bold="true" />
<asp:BoundField DataField="JobStatus" HeaderText="JobStatus" SortExpression="JobStatus" />
<asp:BoundField DataField="Processed Count" HeaderText="Processed Count" ReadOnly="True" SortExpression="Processed Count" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
</p>

<table style="width: 100%">
<tr>
<td style="width: 210px">&nbsp;</td>
<td>
<asp:DetailsView ID="dvAPDtoHQTotal" runat="server" Height="10px" Width="160px" Font-Bold="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>
</td>
</tr>
</table>

<%-- <asp:SqlDataSource ID="SQLDS_TotalAPDtoHQCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	COUNT(hj.JobStatus) 'Total APD to HQ Count'
FROM
	utb_hyperquestsvc_jobs hj
	INNER JOIN utb_insurance i
		ON i.InsuranceCompanyID = hj.InscCompID
WHERE 
	ReceivedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
	AND hj.JobStatus = 'Processed'
"></asp:SqlDataSource>--%>

<asp:LinkButton ID="lbAPDtoHQCount" runat="server" OnClick="lbAPDtoHQCount_Click">Refresh</asp:LinkButton>

<%--<asp:SqlDataSource ID="SQLDS_APD2HQCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	i.Name
	, hj.DocumentTypeName
	, hj.JobStatus
	, COUNT(hj.InscCompID) 'Processed Count'
FROM
	utb_hyperquestsvc_jobs hj
	INNER JOIN utb_insurance i
		ON i.InsuranceCompanyID = hj.InscCompID
WHERE 
	ReceivedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
	AND hj.JobStatus = 'Processed'
GROUP BY
	i.Name
	, hj.DocumentTypeName
	, hj.JobStatus
ORDER BY
	i.Name
	, hj.DocumentTypeName
	, hj.JobStatus

"></asp:SqlDataSource>--%>
</div>

<!-- HQ to APD -->
<div class="col-md-4">
<h3>HQ to APD Today</h3>
<p>
Transactions HQ to APD today:
</p>
<p>
<asp:GridView ID="gvServerStatus_HQtoAPDCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_HQtoAPDCountView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="200" />
<asp:BoundField DataField="ECnt" HeaderText="Processed Count" SortExpression="ECnt" ItemStyle-Width="100" ItemStyle-Font-Bold="true" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
<asp:LinkButton ID="lbHQtoAPDCount" runat="server" OnClick="lbHQtoAPDCount_Click">Refresh</asp:LinkButton>
</p>
<table style="width: 100%">
<tr>
<td style="width: 210px">&nbsp;</td>
<td>
<asp:DetailsView ID="dvHQtoAPDTotal" runat="server" Height="10px" Width="160px" Font-Bold="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>
</td>
</tr>
</table>

<%--<asp:SqlDataSource ID="SQLDS_TotalHQtoAPDCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	COUNT(hl.EventID) 'Total HQ to APD Count'
FROM 
	utb_history_log hl WITH(NOLOCK)
	INNER JOIN utb_event e WITH(NOLOCK)
		ON hl.EventID = e.EventID
WHERE
	hl.EventID IN (95,96,107,108,109,110)
	AND hl.CompletedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
"></asp:SqlDataSource>

            <asp:SqlDataSource ID="SQLDS_HQ2APDCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT
	e.Name
	, COUNT(hl.EventID) ECnt
FROM 
	utb_history_log hl WITH(NOLOCK)
	INNER JOIN utb_event e WITH(NOLOCK)
		ON hl.EventID = e.EventID
WHERE
	hl.EventID IN (95,96,107,108,109,110)
	AND hl.CompletedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
GROUP BY
	e.Name
ORDER BY
	e.Name

"></asp:SqlDataSource>--%>
</div>
</div>

<hr style="height: 2px; border-width: 0; color: gray; background-color: gray" />

<div class="row">
<!-- Document Processor Queue Count -->
<div class="col-md-4">
<h3>Doc Proc Queue Today</h3>
<p>
Jobs processed today:
                Service Status:
                <asp:Label ID="lblDocProcService" runat="server" Text=""></asp:Label>
</p>
<p>
<asp:GridView ID="gvServerStatus_DocProcQueueCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_DocProcQueueCount_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Job Count" HeaderText="Job Count" SortExpression="Job Count" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
<asp:Label ID="lblAlert" runat="server" Font-Bold="True" ForeColor="Red" Text="Alert!!!"></asp:Label>
</p>
</div>

<div class="col-md-4">
<!-- NADA Status -->
<h3>NADA Status</h3>
<p>
NADA Ping/Token Status:
</p>
<p>
<asp:GridView ID="gvNADAStatus" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="NADAStatus" HeaderText="NADA Status" SortExpression="NADAStatus" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>
<asp:Label ID="lblNADAAlert" runat="server" Font-Bold="True" ForeColor="Red" Text="Alert!!!"></asp:Label>
</p>
</div>
</div>

<hr style="height: 2px; border-width: 0; color: gray; background-color: gray" />

<div class="row">
<!-- Electronic Outbound -->
<div class="col-md-4">
<h3>Outbound Electronic Today</h3>
<p>
Outbound Electronic Documents today:
</p>
<p>
<asp:GridView ID="gvServerStatus_OutboundElecDocumentsCountView" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvServerStatus_OutboundElecDocumentsCountView_RowDataBound">
<AlternatingRowStyle BackColor="#F7F7F7" />
<Columns>
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="200" />
<asp:BoundField DataField="EventName" HeaderText="EventName" SortExpression="EventName" ItemStyle-Width="200" />
<asp:BoundField DataField="ECnt" HeaderText="Processed Count" SortExpression="ECnt" ItemStyle-Width="100" ItemStyle-Font-Bold="true" />
</Columns>
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<SortedAscendingCellStyle BackColor="#F4F4FD" />
<SortedAscendingHeaderStyle BackColor="#5A4C9D" />
<SortedDescendingCellStyle BackColor="#D8D8F0" />
<SortedDescendingHeaderStyle BackColor="#3E3277" />
</asp:GridView>

</p>

<table style="width: 100%">
<tr>
<td style="width: 210px">&nbsp;</td>
<td>
<asp:DetailsView ID="dvBundleTotal" runat="server" Height="10px" Width="160px" Font-Bold="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
<AlternatingRowStyle BackColor="#F7F7F7" />
<EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>
</td>
</tr>
</table>

<%--<asp:SqlDataSource ID="SQLDS_TotalBundleCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
    COUNT(eoq.LynxID) 'Total Elec Bundles Count'
FROM 
	utb_elec_outbound_queue eoq WITH(NOLOCK)
	INNER JOIN utb_insurance i WITH(NOLOCK)
	ON i.InsuranceCompanyID = eoq.InsuranceComapnyID
WHERE
  	eoq.SysLastUpdatedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
	AND Status = 'Processed'
"></asp:SqlDataSource>--%>

<asp:LinkButton ID="lbOutElecDocCount" runat="server" OnClick="lbOutElecDocCount_Click">Refresh</asp:LinkButton>

<%--<asp:SqlDataSource ID="SQLDS_OutElecDocsCount" runat="server" ConnectionString="<%$ ConnectionStrings:wsAPDUser_udb_apd_stg_ConnectionString %>" SelectCommand="SELECT 
	i.Name Name
	, eoq.EventName EventName
	, COUNT(eoq.LynxID) ECnt
FROM 
	utb_elec_outbound_queue eoq WITH(NOLOCK)
	INNER JOIN utb_insurance i WITH(NOLOCK)
	ON i.InsuranceCompanyID = eoq.InsuranceComapnyID
WHERE
  	eoq.SysLastUpdatedDate BETWEEN DATEADD(DAY, DATEDIFF(DAY, 0, CURRENT_TIMESTAMP),0) AND CURRENT_TIMESTAMP
	AND Status = 'Processed'
GROUP BY
	i.Name
	, eoq.EventName
ORDER BY
	i.Name

"></asp:SqlDataSource>--%>
</div>
</div>

<div id="MyPopup" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<h4>Use a local account to log in.</h4>
</div>

<div class="modal-body">
<div class="row form-group">
<div class="col-md-4">
<asp:Label ID="lblUserID" CssClass="lblClass" runat="server">User ID <span style="color:red;">  *</span></asp:Label>
</div>
<div class="col-md-8">
<asp:TextBox ID="txtUserID" CssClass="form-control" runat="server" MaxLength="12"></asp:TextBox>
<asp:RequiredFieldValidator ID="rfvUserID" ErrorMessage="This field is required !" ControlToValidate="txtUserID"
CssClass="text-danger" ValidationGroup="vguser" runat="server" />
</div>
</div>
<div class="row form-group">
<div class="col-md-4">
<asp:Label ID="lblPassword" CssClass="lblClass" runat="server">Password <span style="color:red;">  *</span></asp:Label>
</div>
<div class="col-md-8">
<asp:TextBox ID="txtPassword" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
<asp:RequiredFieldValidator ID="rfvPassword" ErrorMessage="This field is required !" ControlToValidate="txtPassword"
CssClass="text-danger" ValidationGroup="vguser" runat="server" />
</div>
</div>
</div>
<div class="modal-footer">
<asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" ValidationGroup="vguser" OnClick="btnSubmit_Click" />
<asp:Button ID="btnClose" runat="server" Text="Cancel" data-dismiss="modal" CssClass="btn btn-danger" OnClick="btnClose_Click" />
</div>
</div>
</div>
</div>
</asp:Content>

