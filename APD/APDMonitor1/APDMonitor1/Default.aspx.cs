﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Configuration;
using System.ServiceProcess;
using APDMonitor1.Common;
using System.DirectoryServices.AccountManagement;
using System.Runtime.InteropServices;
using System.DirectoryServices;

namespace APDMonitor1
{
    public partial class _Default : Page
    {
        string sDatabaseServerStatus = string.Empty;
        string sDocProcPath = string.Empty;
        Int32 iDocProcCaution = 0;
        Int32 iDocProcCritical = 0;
        string sNADAPingStatus = string.Empty;
        bool isValidUser = false;
        string strusername = string.Empty;
        string strpassword = string.Empty;

        NADAWrapperWS.Service wsNADAWrapper = new NADAWrapperWS.Service();

        Default csDefault = new Default();

        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    ClearContorls();
                }
                //- App Variables 
                //string[] filePaths = Directory.GetFiles(Server.MapPath("~/JobXML/"));
                AppSettingsReader appSettingsReader = new AppSettingsReader();
                lblVersion.Text = appSettingsReader.GetValue("Version", typeof(string)).ToString();
                sDocProcPath = appSettingsReader.GetValue("DOCProcQueuePath", typeof(string)).ToString();
                iDocProcCaution = Int32.Parse(appSettingsReader.GetValue("DocProcCaution", typeof(string)).ToString());
                iDocProcCritical = Int32.Parse(appSettingsReader.GetValue("DocProcCritical", typeof(string)).ToString());

                //- Run Individual Server Tests
                //sDatabaseServerStatus = ServerStatus();
                //string sServer2Status = APDWPRD1601();

                //- Document Processor Queue Count 
                Int32 iDocProcQueueCnt = DocProcQueueCount(sDocProcPath);
                iDocProcQueueCnt = iDocProcQueueCnt - 1;

                DataTable DTDocProcQueueData = new DataTable();
                DataColumn DCDocProcQueueData = new DataColumn();
                DTDocProcQueueData.Columns.Add(new DataColumn("Job Count", typeof(string)));

                DataRow DRDocProcQueueData = DTDocProcQueueData.NewRow();
                DTDocProcQueueData.Rows.Add(DRDocProcQueueData);

                if (!(DTDocProcQueueData == null))
                {
                    //-- Check if Service is running
                    lblDocProcService.Text = ServiceStatus("TermService");

                    DRDocProcQueueData[0] = iDocProcQueueCnt;
                    gvServerStatus_DocProcQueueCountView.DataSource = DTDocProcQueueData;
                    gvServerStatus_DocProcQueueCountView.DataBind();
                    gvServerStatus_DocProcQueueCountView.Columns[0].Visible = true;

                    gvServerStatus_DocProcQueueCountView.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Transparent;
                    lblAlert.Text = "";
                    lblAlert.ForeColor = System.Drawing.Color.Transparent;
                    lblAlert.Visible = false;

                    //-- Check DocProc Threshold
                    if (iDocProcQueueCnt > iDocProcCaution)
                    {
                        gvServerStatus_DocProcQueueCountView.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Yellow;
                        lblAlert.Text = "Caution!!!";
                        lblAlert.ForeColor = System.Drawing.Color.Yellow;
                        lblAlert.Visible = true;
                    }
                    if (iDocProcQueueCnt > iDocProcCritical)
                    {
                        gvServerStatus_DocProcQueueCountView.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Red;
                        lblAlert.Text = "Alert!!!";
                        lblAlert.ForeColor = System.Drawing.Color.Red;
                        lblAlert.Visible = true;
                    }

                }
                else
                {
                    DRDocProcQueueData[0] = "No Jobs in Queue...";
                }

                if (sDatabaseServerStatus == "UP")
                {

                    //foreach (GridViewRow row in gvServerStatus_MainView.Rows)
                    //{
                    //    string lbl_AdNo = (row.FindControl("lbl_AdNo") as Label).Text;
                    //    if (!string.IsNullOrEmpty(txtRegNo.Text) && this.txtRegNo.Text.Trim() == lbl_AdNo)
                    //    {
                    //        txtRegNo.Text = this.txtRegNo.Text.Trim();
                    //        (row.FindControl("lbl_Status") as Label).Text = "Present";
                    //        isRecordExists = true;
                    //    }
                    //}
                }
                else
                {

                }

                //-- HQ to APD Status
                //DataTable DTHQ2APDStatusData = new DataTable();
                //DataColumn DCHQ2APDStatusData = new DataColumn();
                //DTHQ2APDStatusData.Columns.Add(new DataColumn("HQ2APDStatus", typeof(string)));
                //DTHQ2APDStatusData.Columns.Add(new DataColumn("HQ2APDStatus_Count", typeof(string)));

                //DataRow DRHQ2APDStatusData = DTHQ2APDStatusData.NewRow();
                //DTHQ2APDStatusData.Rows.Add(DRHQ2APDStatusData);

                //if (!(DTHQ2APDStatusData == null))
                //{
                //DRDocProcQueueData[0] = iDocProcQueueCnt;
                //gvServerStatus_HQtoAPDCountView.DataSource = SQLDS_HQ2APDCount;
                //gvServerStatus_HQtoAPDCountView.DataBind();
                //gvServerStatus_HQtoAPDCountView.Columns[0].Visible = true;

                //HQCL-3311 - APD Monitor inline query to SP functionality - Start
                GetServerStatus();
                GetTotoalClaimCountDetails();
                GetShopAssignCountCountDetails();
                GetAPDtoHQCountDetails();
                GetHQtoAPDCountDetails();
                GetOutboundElecDocumentsCountDetails();
                //HQCL-3311 - APD Monitor inline query to SP functionality - End

                //}
                //else
                //{
                //    DRHQ2APDStatusData[0] = "No Jobs in Queue...";
                //}

                //-- NADA Status
                DataTable DTNADAStatusData = new DataTable();
                DataColumn DCNADAStatusData = new DataColumn();
                DTNADAStatusData.Columns.Add(new DataColumn("NADAStatus", typeof(string)));

                DataRow DRNADAStatusData = DTNADAStatusData.NewRow();
                DTNADAStatusData.Rows.Add(DRNADAStatusData);

                sNADAPingStatus = NADAPingMonitor();
                DRNADAStatusData[0] = sNADAPingStatus;

                if (!(DTNADAStatusData == null))
                {
                    gvNADAStatus.DataSource = DTNADAStatusData;
                    gvNADAStatus.DataBind();
                    gvNADAStatus.Columns[0].Visible = true;

                    if (sNADAPingStatus.ToUpper() == "UP")
                    {
                        gvNADAStatus.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Empty;
                        lblNADAAlert.Text = "";
                        lblNADAAlert.ForeColor = System.Drawing.Color.Blue;
                        lblNADAAlert.Visible = true;
                    }
                    else
                    {
                        gvNADAStatus.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Red;
                        lblNADAAlert.Text = "Alert!!!";
                        lblNADAAlert.ForeColor = System.Drawing.Color.Red;
                        lblNADAAlert.Visible = true;
                    }
                }
                else
                {
                    DRNADAStatusData[0] = "N/A";
                    gvNADAStatus.Columns[0].ItemStyle.BackColor = System.Drawing.Color.Red;
                    lblNADAAlert.Text = "Alert!!!";
                    lblNADAAlert.ForeColor = System.Drawing.Color.Red;
                    lblNADAAlert.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvServerStatus_ShopAssignCountView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
            }
        }

        protected void gvServerStatus_MainView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                //GridView gvServerStatus = (GridView) (sender);
                //if (gvServerStatus)
                //{
                //    (HiddenField)e.Row.FindControl("hidDatabaseServerStatus").Value = sDatabaseServerStatus;
                //}

                //- Performance
                e.Row.Cells[2].Text = DateTime.Now.ToString();
                //e.Row.Cells[3].Text = HttpUtility.HtmlDecode(ServerPerformance(e.Row.Cells[0].Text));

                //- Test Status Type
                if (e.Row.Cells[0].Text.Contains("ECAD"))
                {
                    e.Row.Cells[1].Text = HttpUtility.HtmlDecode(ServerStatusWCF(e.Row.Cells[0].Text));
                    e.Row.Cells[3].Text = "";
                }
                else
                {
                    e.Row.Cells[1].Text = HttpUtility.HtmlDecode(ServerStatus(e.Row.Cells[0].Text));
                }

                if (e.Row.Cells[0].Text.Contains("APD Foundation WS"))
                {
                    e.Row.Cells[1].Text = HttpUtility.HtmlDecode(ServerStatusWS(e.Row.Cells[0].Text));
                    e.Row.Cells[3].Text = "";
                }


                if ((e.Row.Cells[1].Text != "UP"))
                {
                    e.Row.Cells[1].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[1].Text = ("<blink>" + (e.Row.Cells[1].Text.ToString() + "</blink>"));
                    e.Row.Cells[1].Style.Add("color", "red");
                }
                else
                {
                    e.Row.Cells[1].ForeColor = System.Drawing.Color.Blue;
                    e.Row.Cells[1].Style.Add("text-decoration", "None");
                }
            }
        }

        protected void gvServerStatus_OutboundElecDocumentsCountView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
        }

        protected void gvServerStatus_HQtoAPDCountView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
        }

        protected void gvServerStatus_ClaimCountView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
        }

        protected void gvServerStatus_APDtoHQCountView_RowDataBound (object sender, GridViewRowEventArgs e)
        {
        }

        protected void gvServerStatus_DocProcQueueCount_RowDataBound (object sender, GridViewRowEventArgs e)
        {
            //Int32 iDocProcQueueCnt = DocProcQueueCount("\\\\fileprd01\\Intra\\apddocuments\\Prd\\DocProcessor\\Queue\\");
        }

        protected string ServerStatus (string sServerName)
        {
            //- Run Individual Server Tests
            string[] aServerName = sServerName.Split(',');
            string html = new WebClient().DownloadString("http://" + aServerName[1] + "/HealthCheckMonitor/Status_Server.asp");
            return html;

            //return "UP";
        }

        protected string ServerPerformance (string sServerName)
        {
            //- Run Individual Server Tests
            string[] aServerName = sServerName.Split(',');
            string html = new WebClient().DownloadString("http://" + aServerName[1] + "/HealthCheckMonitor/Status_Server_Performance.asp");

            //- Update Perf Statistics


            return html;
        }

        protected string ServerStatusWCF (string sServerName)
        {
            //- Run Individual Server Tests
            string[] aServerName = sServerName.Split(',');
            string html = new WebClient().DownloadString("http://" + aServerName[1] + "/HealthCheckMonitor/Status_EcadAccessor_service.asp");

            //- Update Perf Statistics


            return html;
        }

        protected string ServerStatusWS (string sServerName)
        {
            string html = "DOWN";
            try
            {
                //- Run Individual Server Tests
                string[] aServerName = sServerName.Split(',');
                html = new WebClient().DownloadString("http://" + aServerName[1] + "/HealthCheckMonitor/Status_APDFoundation_Service.asp");

                //- Update Perf Statistics
            }
            catch (Exception oExcept)
            {
                html = "DOWN";
            }

            return html;
        }

        protected Int32 DocProcQueueCount (string sServerNamePath)
        {
            Int32 iDocJobCount = 0;

            var txtFiles = Directory.EnumerateFiles(sServerNamePath, "*.xml");
            iDocJobCount = txtFiles.Count();

            return iDocJobCount;
        }

        //protected string APDWPRD1601()
        //{
        //    //- Run Individual Server Tests


        //    return "UP";
        //}


        //HQCL-3311 - APD Monitor - inline query to SP functionality - Start
        public void GetServerStatus ()
        {
            DataTable dtServerStatus = new DataTable();
            try
            {
                dtServerStatus = csDefault.GetServerStatus();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            gvServerStatus_MainView.DataSource = dtServerStatus;
            gvServerStatus_MainView.DataBind();
            gvServerStatus_MainView.Visible = true;
        }

        protected void lbStatusRefresh_Click (object sender, EventArgs e)
        {
            GetServerStatus();
        }

        public void GetTotoalClaimCountDetails ()
        {
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = csDefault.GetTotoalClaimCountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            dvTotalClaims.DataSource = dsResult.Tables[0];
            dvTotalClaims.DataBind();
            dvTotalClaims.Visible = true;

            gvServerStatus_ClaimCountView.DataSource = dsResult.Tables[1];
            gvServerStatus_ClaimCountView.DataBind();
            gvServerStatus_ClaimCountView.Visible = true;
        }
        protected void lbClaimCountRefresh_Click (object sender, EventArgs e)
        {
            GetTotoalClaimCountDetails();
        }

        public void GetAPDtoHQCountDetails ()
        {
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = csDefault.GetAPDtoHQCountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            dvAPDtoHQTotal.DataSource = dsResult.Tables[0];
            dvAPDtoHQTotal.DataBind();
            dvAPDtoHQTotal.Visible = true;

            gvServerStatus_APDtoHQCountView.DataSource = dsResult.Tables[1];
            gvServerStatus_APDtoHQCountView.DataBind();
            gvServerStatus_APDtoHQCountView.Visible = true;
        }

        protected void lbAPDtoHQCount_Click (object sender, EventArgs e)
        {
            GetAPDtoHQCountDetails();
        }

        public void GetHQtoAPDCountDetails ()
        {
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = csDefault.GetHQtoAPDCountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            dvHQtoAPDTotal.DataSource = dsResult.Tables[0];
            dvHQtoAPDTotal.DataBind();
            dvHQtoAPDTotal.Visible = true;

            gvServerStatus_HQtoAPDCountView.DataSource = dsResult.Tables[1];
            gvServerStatus_HQtoAPDCountView.DataBind();
            gvServerStatus_HQtoAPDCountView.Visible = true;
        }

        protected void lbHQtoAPDCount_Click (object sender, EventArgs e)
        {
            GetHQtoAPDCountDetails();
        }
        public void GetOutboundElecDocumentsCountDetails ()
        {
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = csDefault.GetOutboundElecDocumentsCountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            dvBundleTotal.DataSource = dsResult.Tables[0];
            dvBundleTotal.DataBind();
            dvBundleTotal.Visible = true;

            gvServerStatus_OutboundElecDocumentsCountView.DataSource = dsResult.Tables[1];
            gvServerStatus_OutboundElecDocumentsCountView.DataBind();
            gvServerStatus_OutboundElecDocumentsCountView.Visible = true;
        }
        protected void lbOutElecDocCount_Click (object sender, EventArgs e)
        {
            //SQLDS_APD2HQCount.DataBind();
            //gvServerStatus_APDtoHQCountView.DataBind();
            //gvServerStatus_APDtoHQCountView.Visible = true;
            GetOutboundElecDocumentsCountDetails();
        }

        public void GetShopAssignCountCountDetails ()
        {
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = csDefault.GetShopAssignCountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            dvAssignmentTotal.DataSource = dsResult.Tables[0];
            dvAssignmentTotal.DataBind();
            dvAssignmentTotal.Visible = true;

            gvServerStatus_ShopAssignCountView.DataSource = dsResult.Tables[1];
            gvServerStatus_ShopAssignCountView.DataBind();
            gvServerStatus_ShopAssignCountView.Visible = true;
        }

        protected void lbShopAssignCount_Click (object sender, EventArgs e)
        {
            GetShopAssignCountCountDetails();
        }
        //HQCL-3311 - APD Monitor - inline query to SP functionality - End
        protected string NADAPingMonitor ()
        {
            string sNADAStatus = "DOWN";

            try
            {
                //-- Call Ping test from NADA Web Service Wrapper
                sNADAStatus = wsNADAWrapper.Ping();

                if (sNADAStatus.ToUpper() == "TRUE")
                {
                    sNADAStatus = "UP";
                }
                else
                {
                    sNADAStatus = "DOWN";
                }
            }
            catch
            {
                sNADAStatus = "DOWN";
            }

            return sNADAStatus;
        }

        protected string ServiceStatus (string sServiceName)
        {
            ServiceController sc = new ServiceController(sServiceName);

            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    return "Running";
                case ServiceControllerStatus.Stopped:
                    return "Stopped";
                case ServiceControllerStatus.Paused:
                    return "Paused";
                case ServiceControllerStatus.StopPending:
                    return "Stopping";
                case ServiceControllerStatus.StartPending:
                    return "Starting";
                default:
                    return "Status Changing";
            }
        }


        /// <summary>
        /// check user role if valid or not HQCL-2134 Allow admin to restart the services 
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private bool CheckUserRole (string strUserID)
        {
            string strReturnValue = string.Empty;
            string strAdmin = ConfigurationManager.AppSettings["Admin"];
            string strSupervisor = ConfigurationManager.AppSettings["Supervisor"];
            DataTable dtResult = new DataTable();
            bool blExists = false;
            try
            {
                dtResult = csDefault.GetUserDetails(strUserID);
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    blExists = dtResult.AsEnumerable().Any(row => strAdmin == row.Field<String>("Name") || strSupervisor == row.Field<String>("Name"));
                }
                return blExists;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnRestartServices_Click (object sender, EventArgs e)
        {
            //ClearContorls();
        }

        /// <summary>
        /// show modal popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowPopup (object sender, EventArgs e)
        {
            ClearContorls();
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('');", true);
        }

        /// <summary>
        /// user authentication using active directory
        /// </summary>
        /// <returns></returns>
        private bool userAuthenticate ()
        {
            strusername = txtUserID.Text;
            strpassword = txtPassword.Text;

            bool result = false;

            using (DirectoryEntry entry = new DirectoryEntry())
            {
                entry.Username = strusername;
                entry.Password = strpassword;

                DirectorySearcher searcher = new DirectorySearcher(entry);

                searcher.Filter = "(objectclass=user)";

                try
                {
                    searcher.FindOne();
                    result= true;
                }
                catch (COMException ex)
                {
                    result = false;
                    if (ex.ErrorCode == -2147023570)
                    {
                        // Login or password is incorrect
                        //throw ex;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The username or password is incorrect !')", true);
                    }
                    //throw ex;
                }

                return result;
            }
            //try
            //{                
            //    using (var context = new PrincipalContext(ContextType.Domain, Environment.UserDomainName, strusername + "@" + Environment.UserDomainName, strpassword))
            //    {
            //        //Username and password for authentication.
            //        return context.ValidateCredentials(strusername, strpassword);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error !')", true);
            //}
        }

        protected void btnSubmit_Click (object sender, EventArgs e)
        {
            try
            {
                isValidUser = userAuthenticate();
                if (isValidUser)
                {
                    if (CheckUserRole(txtUserID.Text))
                    {
                        ClearContorls();
                        Response.Redirect("RestartServices.aspx", false);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You are not a valid user!')", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You are not a valid user!')", true);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        protected void btnClose_Click (object sender, EventArgs e)
        {
            ClearContorls();
        }

        /// <summary>
        /// clear input values
        /// </summary>
        private void ClearContorls ()
        {
            txtUserID.Text = string.Empty;
            txtPassword.Text = string.Empty;
        }
    }
}