﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace APDMonitor1
{
    public partial class Monitors : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //-- Main Jobs --//
                btnProcessedJobs.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                //Tab4.CssClass = "Clicked";
                //HQIncoming_View1.ActiveViewIndex = 0;

                //-- APD ClaimPoint Jobs --//
                btnClaimpointClaims.CssClass = "Clicked";
                Claim_View1.ActiveViewIndex = 0;

                //btnIncomingDocuments.CssClass = "Clicked";
                //Documents_View1.ActiveViewIndex = 0;

                //-- APD PostOffice Jobs --//
                btnAPDPostOfficeJobs_Processed.CssClass = "Clicked";
                APDPostOfficeMultiView.ActiveViewIndex = 0;

                //-- APD PostOffice Jobs --//
                btnAPDPostOfficeJobs_Processed.CssClass = "Clicked";
                APDPostOfficeMultiView.ActiveViewIndex = 0;
            }
        }

        #region APD Claims Code
        protected void lbClaimpointClaimsRefresh_Click(object sender, EventArgs e)
        {
            btnClaimpointClaims.CssClass = "Clicked";
            btnFNOLClaims.CssClass = "Initial";
            btnWebServClaims.CssClass = "Initial";
            Claim_View1.ActiveViewIndex = 0;
            SqlDataSource_Claimpoint_Claims.DataBind();
            gvClaimpointClaims.DataBind();
            gvClaimpointClaims.Visible = true;
        }

        protected void ClaimView_Tab1_Click(object sender, EventArgs e)
        {
            btnClaimpointClaims.CssClass = "Clicked";
            btnFNOLClaims.CssClass = "Initial";
            btnWebServClaims.CssClass = "Initial";
            Claim_View1.ActiveViewIndex = 0;
            SqlDataSource_Claimpoint_Claims.DataBind();
            gvClaimpointClaims.DataBind();
            gvClaimpointClaims.Visible = true;
        }

        protected void FNOLView_Tab1_Click(object sender, EventArgs e)
        {
            btnClaimpointClaims.CssClass = "Initial";
            btnFNOLClaims.CssClass = "Clicked";
            btnWebServClaims.CssClass = "Initial";
            Claim_View1.ActiveViewIndex = 1;
            SqlDataSource_FNOL_Claims.DataBind();
            gvFNOLClaims.DataBind();
            gvFNOLClaims.Visible = true;
        }

        protected void btnWebServClaims_Click(object sender, EventArgs e)
        {
            btnClaimpointClaims.CssClass = "Initial";
            btnFNOLClaims.CssClass = "Initial";
            btnWebServClaims.CssClass = "Clicked";
            Claim_View1.ActiveViewIndex = 2;
            SqlDataSource_WebServ_Claims.DataBind();
            gvWebServClaims.DataBind();
            gvWebServClaims.Visible = true;
        }
        #endregion

        #region APD to Hyperquest Integrations Code
        protected void lbJobsWaitingRefresh_Click(object sender, EventArgs e)
        {
            gvHQJobMonitor_Unprocessed.DataBind();
            gvHQJobMonitor_Processed.DataBind();
        }

        protected void Processed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Clicked";
            btnUnProcessedJobs.CssClass = "Initial";
            btnFailedJobs.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            SqlDataSource_Database_Unprocessed.DataBind();
            gvHQJobMonitor_Unprocessed.DataBind();
            gvHQJobMonitor_Unprocessed.Visible = true;
        }

        protected void UnProcessed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Initial";
            btnUnProcessedJobs.CssClass = "Clicked";
            btnFailedJobs.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
            SqlDataSource_Database_Processed.DataBind();
            gvHQJobMonitor_Processed.DataBind();
            gvHQJobMonitor_Processed.Visible = true;
        }

        protected void Failed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Initial";
            btnUnProcessedJobs.CssClass = "Initial";
            btnFailedJobs.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
            SqlDataSource_Database_Failed.DataBind();
            gvHQJobMonitor_Failed.DataBind();
            gvHQJobMonitor_Failed.Visible = true;
        }


        //private void gvHQJobMonitor_Failed_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if ((e.Row.RowType == DataControlRowType.DataRow))
        //    {
        //        // fetch grid control using findcontrol method
        //        if ((e.Row.Cells[6].Text == "FAILED"))
        //        {
        //            e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
        //            e.Row.Cells[6].Text = ("<blink>"
        //                        + (e.Row.Cells[5].Text.ToString() + "</blink>"));
        //            e.Row.Cells[8].ForeColor = System.Drawing.Color.Red;
        //            e.Row.Cells[6].Style.Add("text-decoration", "blink");
        //            e.Row.Cells[6].Style.Add("color", "red");
        //            // change accoridng to u
        //        }
        //        else {
        //            e.Row.Cells[6].ForeColor = System.Drawing.Color.Blue;
        //            e.Row.Cells[8].ForeColor = System.Drawing.Color.Blue;
        //            e.Row.Cells[6].Style.Add("text-decoration", "None");
        //            // change accoridng to u
        //        }

        //    }

        //}

        protected void gvHQJobMonitor_Failed_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                // fetch grid control using findcontrol method
                if ((e.Row.Cells[6].Text == "FAILED"))
                {
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[6].Text = ("<blink>"
                                + (e.Row.Cells[6].Text.ToString() + "</blink>"));
                    e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[6].Style.Add("text-decoration", "blink");
                    e.Row.Cells[6].Style.Add("color", "red");
                    // change accoridng to u
                }
                else {
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Blue;
                    e.Row.Cells[7].ForeColor = System.Drawing.Color.Blue;
                    e.Row.Cells[6].Style.Add("text-decoration", "None");
                    // change accoridng to u
                }

            }
        }



        #endregion

        #region APD PostOffice Code
        protected void btnAPDPostOfficeJobs_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Clicked";
            btnAPDPostOfficeJobs_Complete.CssClass = "Initial";
            btnAPDPostOfficeJobs_Failed.CssClass = "Initial";
            //APDPostOfficeJobs_View.ActiveViewIndex = 0;
            SqlDataSource_APDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.Visible = true;
        }

        protected void APDPostOffice_Processed_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Clicked";
            btnAPDPostOfficeJobs_Complete.CssClass = "Initial";
            btnAPDPostOfficeJobs_Failed.CssClass = "Initial";
            APDPostOfficeMultiView.ActiveViewIndex = 0;
            SqlDataSource_APDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.Visible = true;
        }

        protected void APDPostOffice_Complete_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Initial";
            btnAPDPostOfficeJobs_Complete.CssClass = "Clicked";
            btnAPDPostOfficeJobs_Failed.CssClass = "Initial";
            APDPostOfficeMultiView.ActiveViewIndex = 1;
            SqlDataSource_APDPostOfficeJobs_Complete.DataBind();
            gvAPDPostOfficeJobs_Complete.DataBind();
            gvAPDPostOfficeJobs_Complete.Visible = true;
        }

        protected void APDPostOffice_Failed_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Initial";
            btnAPDPostOfficeJobs_Complete.CssClass = "Initial";
            btnAPDPostOfficeJobs_Failed.CssClass = "Clicked";
            APDPostOfficeMultiView.ActiveViewIndex = 2;
            SqlDataSource_APDPostOfficeJobs_Failed.DataBind();
            gvAPDPostOfficeJobs_Failed.DataBind();
            gvAPDPostOfficeJobs_Failed.Visible = true;
        }

        //private void gvAPDPostOfficeJobs_Failed_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if ((e.Row.RowType == DataControlRowType.DataRow))
        //    {
                // fetch grid control using findcontrol method
                //if ((e.Row.Cells[6].Text == "FAILED"))
                //{
                //    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                //    e.Row.Cells[6].Text = ("<blink>"
                //                + (e.Row.Cells[5].Text.ToString() + "</blink>"));
                //    e.Row.Cells[8].ForeColor = System.Drawing.Color.Red;
                //    e.Row.Cells[6].Style.Add("text-decoration", "blink");
                //    e.Row.Cells[6].Style.Add("color", "red");
                //    // change accoridng to u
                //}
                //else {
                //    e.Row.Cells[6].ForeColor = System.Drawing.Color.Blue;
                //    e.Row.Cells[8].ForeColor = System.Drawing.Color.Blue;
                //    e.Row.Cells[6].Style.Add("text-decoration", "None");
                //    // change accoridng to u
                //}

//            }

//        }


        protected void gvAPDPostOfficeJobs_Failed_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                //fetch grid control using findcontrol method
                if ((e.Row.Cells[7].Text.ToUpper() == "FAILED"))
                    {
                        e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[7].Text = ("<blink>"
                                    + (e.Row.Cells[7].Text.ToString() + "</blink>"));
                        //e.Row.Cells[8].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[7].Style.Add("text-decoration", "blink");
                        e.Row.Cells[7].Style.Add("color", "red");
                        e.Row.Cells[7].Font.Bold = true;
                    // change accoridng to u
                }
                else {
                        e.Row.Cells[7].ForeColor = System.Drawing.Color.Blue;
                        //e.Row.Cells[8].ForeColor = System.Drawing.Color.Blue;
                        e.Row.Cells[7].Style.Add("text-decoration", "None");
                        e.Row.Cells[7].Font.Bold = false;
                    // change accoridng to u
                }
            }
        }

        protected void gvAPDPostOfficeJobs_Processed_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                //fetch grid control using findcontrol method
                switch ((e.Row.Cells[7].Text.ToUpper()))
                {
                    case "QUEUED":
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                        //e.Row.Cells[7].ForeColor = System.Drawing.Color.Green;
                        //e.Row.Cells[7].Text = ("<blink>"
                        //            + (e.Row.Cells[7].Text.ToString() + "</blink>"));
                        //e.Row.Cells[8].ForeColor = System.Drawing.Color.Red;
                        //e.Row.Cells[7].Style.Add("text-decoration", "blink");
                        //e.Row.Cells[7].Style.Add("color", "red");
                        //e.Row.Cells[7].Font.Bold = true;
                        // change accoridng to u

                        break;
                    case "PROCESSING":
                        e.Row.BackColor = System.Drawing.Color.LightBlue;
                        break;
                    default:
                        e.Row.BackColor = System.Drawing.Color.Transparent;

                        break;
                }

                //if ((e.Row.Cells[7].Text.ToUpper() == "QUEUED"))
                //{
                //    e.Row.BackColor = System.Drawing.Color.LightGreen;

                //}
                //else {
                //    e.Row.BackColor = System.Drawing.Color.Transparent;
                //    //e.Row.Cells[7].ForeColor = System.Drawing.Color.Blue;
                //    //e.Row.Cells[8].ForeColor = System.Drawing.Color.Blue;
                //    //e.Row.Cells[7].Style.Add("text-decoration", "None");
                //    //e.Row.Cells[7].Font.Bold = false;
                //    // change accoridng to u
                //}
            }
        }

        #endregion

        protected void lnkAPDPostOffice_Refresh_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Clicked";
            btnAPDPostOfficeJobs_Complete.CssClass = "Initial";
            btnAPDPostOfficeJobs_Failed.CssClass = "Initial";
            APDPostOfficeMultiView.ActiveViewIndex = 0;
            SqlDataSource_APDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.DataBind();
            gvAPDPostOfficeJobs.Visible = true;
        }

        protected void lnkAPDPostOfficeComplete_Refresh_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Initial";
            btnAPDPostOfficeJobs_Complete.CssClass = "Clicked";
            btnAPDPostOfficeJobs_Failed.CssClass = "Initial";
            APDPostOfficeMultiView.ActiveViewIndex = 1;
            SqlDataSource_APDPostOfficeJobs_Complete.DataBind();
            gvAPDPostOfficeJobs_Complete.DataBind();
            gvAPDPostOfficeJobs_Complete.Visible = true;
        }

        protected void lnkAPDPostOfficeFailed_Refresh_Click(object sender, EventArgs e)
        {
            btnAPDPostOfficeJobs_Processed.CssClass = "Initial";
            btnAPDPostOfficeJobs_Complete.CssClass = "Initial";
            btnAPDPostOfficeJobs_Failed.CssClass = "Clicked";
            APDPostOfficeMultiView.ActiveViewIndex = 2;
            SqlDataSource_APDPostOfficeJobs_Failed.DataBind();
            gvAPDPostOfficeJobs_Failed.DataBind();
            gvAPDPostOfficeJobs_Failed.Visible = true;
        }

        //-- Outbound_Electronic Bound Documents
        protected void Outbound_Elec_Processed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Clicked";
            btnUnProcessedJobs.CssClass = "Initial";
            btnFailedJobs.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            SqlDataSource_Database_Unprocessed.DataBind();
            gvHQJobMonitor_Unprocessed.DataBind();
            gvHQJobMonitor_Unprocessed.Visible = true;
        }

        protected void Outbound_Elec_UnProcessed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Initial";
            btnUnProcessedJobs.CssClass = "Clicked";
            btnFailedJobs.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
            SqlDataSource_Database_Processed.DataBind();
            gvHQJobMonitor_Processed.DataBind();
            gvHQJobMonitor_Processed.Visible = true;
        }

        protected void Outbound_Elec_Failed_Jobs_Click(object sender, EventArgs e)
        {
            btnProcessedJobs.CssClass = "Initial";
            btnUnProcessedJobs.CssClass = "Initial";
            btnFailedJobs.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
            SqlDataSource_Database_Failed.DataBind();
            gvHQJobMonitor_Failed.DataBind();
            gvHQJobMonitor_Failed.Visible = true;
        }

    }
}