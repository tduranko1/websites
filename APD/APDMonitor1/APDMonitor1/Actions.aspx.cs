﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;

namespace APDMonitor1
{
    public partial class Actions : Page
    {
        DataTable DTFinancialInvoices = new DataTable();
        DataColumn DCFinancialInvoices = new DataColumn();
        DataRow DRFinancialInvoices = null;
        AppSettingsReader appSettingsReader = new AppSettingsReader();

        int iSelInvoiceID;
        string sSelDispatchNumber = string.Empty;
        int iSelUserID = 0;
        string sSelSysLastUpdatedDate = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtDeleteConfirm.Text = "";
            if (!IsPostBack)
            {
                //-- DEBUGGING
                lbDebug.Items.Add("Initial Page Load");

                iSelInvoiceID = 0;
                iSelUserID = 5832;

                //-- DEBUGGING
                lbDebug.Items.Add(">>> Default User: " + iSelUserID.ToString());
            }

            //-- DEBUGGING
            lbDebug.Items.Add(">>> Page Reloaded");
        }

        protected void InvoiceDetails(string sLynxID, string sInvType, string sAction)
        {
            //string sLynxID = string.Empty;
            string sInvoiceID = string.Empty;

            if (sAction=="VIEW")
            {
                //-- DEBUGGING
                lbDebug.Items.Add(">>> Invoice Details VIEW");

                lblDisplay.Text = "Invoice Details:";
                ViewState["SelLynxID"] = sLynxID;
                ViewState["SelInvType"] = sInvType;

                GetInvoices(sLynxID, sInvType);
            } // If End
        } // Function End

        protected void btInvoiceDetails_Click(object sender, EventArgs e)
        {
            InvoiceDetails(txtLynxID.Text, "A", "VIEW");
        }

        protected void btFinancialInvoices_Click(object sender, EventArgs e)
        {
            Response.Write("here");
        }

        protected void btInvoiceEdit_Click(object sender, EventArgs e)
        {
            InvoiceDetails(txtLynxID.Text, "A", "EDIT");
        }

        protected void btInvoiceAdd_Click(object sender, EventArgs e)
        {
            //-- Var Declarations
            int iInscCompanyID = 0;

            //-- Get ClaimAspectID and InsuranceCompanyID for LynxID
            GetClaimDetails(txtLynxID.Text);

            //iInscCompanyID = uspGetInscCompanyByLynxID @iLynxID = txtLynxID.Text;

            //-- Get list of available fee's

            //-- Get list of services

            //-- Get Service Channel

            //-- Get ClaimAspectID of the Vehicle

        }

        protected void gvFinancialInvoices_RowEditing(object sender, EventArgs e)
        {
            Response.Write("here");
        }

        protected void btnDelete_Click()
        {
            Response.Write("here");
        }

        protected void gvFinancialInvoices_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //ContactTableAdapter contact = new ContactTableAdapter();
            iSelInvoiceID = Convert.ToInt32(gvFinancialInvoices.DataKeys[e.RowIndex].Values[0].ToString());

            GridViewRow row = gvFinancialInvoices.Rows[e.RowIndex];

            Label DispatchNumber = (Label)row.FindControl("lblDispatchNumber");
            sSelDispatchNumber = DispatchNumber.Text;

            //-- DEBUGGING
            lbDebug.Items.Add(">>> GridView RowDeleteing DispatchNum: " + sSelDispatchNumber);

            Label SysLastUpdatedDate = (Label)row.FindControl("lblSysLastUpdatedDate");
            sSelSysLastUpdatedDate = SysLastUpdatedDate.Text;

            //-- Get logged in UserID
            //iSelUserID//iUserID

            DeleteInvoice(iSelInvoiceID, sSelDispatchNumber, 5832, sSelSysLastUpdatedDate);
            //contact.Delete(id);
            //FillGrid();
        }

        protected void DeleteInvoice(int iInvoiceID, string sSelDispatchNumber, int iSelUserID, string sSelSysLastUpdatedDate)
        {
            txtDeleteConfirm.Text = "Are you sure you want to delete Dispatch Number: " + iInvoiceID.ToString() + "?";
            btnDelYes.Visible = true;
            btnDelNo.Visible = true;

            //-- Set the global vars
            ViewState["SelInvoiceID"] = iInvoiceID;
            ViewState["SelDispatchNumber"] = sSelDispatchNumber;
            ViewState["SelUserID"] = iSelUserID;
            ViewState["SelSysLastUpdatedDate"] = sSelSysLastUpdatedDate;

            //-- DEBUGGING
            lbDebug.Items.Add(">>> Delete Confirmation: " + string.Format("SelInvoiceID: {0}, SelDispatchNumber: {1}, SelUserID: {2}, SelSysLastUpdatedDate: {3} ", iInvoiceID.ToString(), sSelDispatchNumber, iSelUserID.ToString(), sSelSysLastUpdatedDate));
        }

        protected void btnDelYes_Click(object sender, EventArgs e)
        {
            txtDeleteConfirm.Text = "";
            btnDelYes.Visible = false;
            btnDelNo.Visible = false;

            //-- Call Update code to set invoice to disabled
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(appSettingsReader.GetValue("WSAPIServerURL", typeof(string)).ToString());
                var responseTask = client.GetAsync("APDWSApiFinancial/DelInvoice?InvoiceID=" + ViewState["SelInvoiceID"] + "&DispatchNumber=" + ViewState["SelDispatchNumber"] + "&UserID=" + ViewState["SelUserID"] + "&SysLastUpdatedDate=" + ViewState["SelSysLastUpdatedDate"]);
                responseTask.Wait();

                //-- DEBUGGING
                lbDebug.Items.Add(">>> Delete YES: " + string.Format("SelInvoiceID: {0}, SelDispatchNumber: {1}, SelUserID: {2}, SelSysLastUpdatedDate: {3} ", ViewState["SelInvoiceID"], ViewState["SelDispatchNumber"], ViewState["SelUserID"], ViewState["SelSysLastUpdatedDate"]));

                var result = responseTask.Result;

                //-- DEBUGGING
                lbDebug.Items.Add("<<< Response " + result.IsSuccessStatusCode);

                if (result.IsSuccessStatusCode)
                {
                    //-- Refresh Gridview
                    InvoiceDetails(txtLynxID.Text, "A", "VIEW");
                    //GetInvoices(ViewState["SelLynxID"].ToString(), ViewState["SelInvType"].ToString());
                }
            }
        }

        protected void btnDelNo_Click(object sender, EventArgs e)
        {
            txtDeleteConfirm.Text = "";
            btnDelYes.Visible = false;
            btnDelNo.Visible = false;
        }

        protected void GetInvoices(string sLynxID, string sInvType)
        {
            XmlDocument XMLReturn = new XmlDocument();
            XmlNodeList oXMLNodeList = null;
            APDFoundation APDFoundation = new APDFoundation();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(appSettingsReader.GetValue("WSAPIServerURL", typeof(string)).ToString());
                //HTTP GET

                // JSON Response
                //var responseTask = client.GetAsync("APDWSApiFinancial/GetAllInvoices?LynxID=1850978&InvType=F&type=json");

                // XML Response
                var responseTask = client.GetAsync("APDWSApiFinancial/GetAllInvoices?LynxID=" + sLynxID + "&InvType=" + sInvType + "&type=xml");
                responseTask.Wait();

                var result = responseTask.Result;

                //-- DEBUGGING
                lbDebug.Items.Add("WSApi_Load: " + result.RequestMessage.RequestUri.AbsoluteUri);

                if (result.IsSuccessStatusCode)
                {
                    string sXMLReturn = string.Empty;

                    sXMLReturn = result.Content.ReadAsStringAsync().Result;
                    //sXMLReturn = sXMLReturn.Replace('<ArrayOfInvoiceItems xmlns:i="http://www.w3.org/2001/XMLSchema-instance\" xmlns="http://schemas.datacontract.org/2004/07/APDWSApi.Models\"', "");
                    XMLReturn.LoadXml(sXMLReturn);

                    oXMLNodeList = XMLReturn.SelectNodes("ArrayOfInvoiceItems/InvoiceItems");

                    if (oXMLNodeList.Count > 0)
                    {
                        DTFinancialInvoices.Columns.Add(new DataColumn("InvoiceID", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("LynxID", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("Description", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("DispatchNumber", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("ItemTypeCD", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("Amount", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("EntryDate", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("StatusCD", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("StatusDate", typeof(string)));
                        DTFinancialInvoices.Columns.Add(new DataColumn("SysLastUpdatedDate", typeof(string)));

                        //-- Return existing Invoices
                        foreach (XmlNode oXMLNode in oXMLNodeList)
                        {
                            //-- Create and Populate the DataTable
                            //DataRow DRFinancialInvoices = DTFinancialInvoices.NewRow();
                            DRFinancialInvoices = DTFinancialInvoices.NewRow();
                            DRFinancialInvoices["LynxID"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "LynxID");
                            DRFinancialInvoices["InvoiceID"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "InvoiceID");
                            DRFinancialInvoices["Description"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "Description");
                            DRFinancialInvoices["DispatchNumber"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "DispatchNumber");
                            //DRFinancialInvoices["EntryDate"] = string.Format("mm/dd/yyyy", APDFoundation.GetXMLSingleNodeValue(oXMLNode, "EntryDate"));
                            DateTime dtEntryDate = DateTime.Parse(APDFoundation.GetXMLSingleNodeValue(oXMLNode, "EntryDate"));
                            DRFinancialInvoices["EntryDate"] = dtEntryDate.Date.ToString("MM/dd/yyyy");
                            DRFinancialInvoices["StatusCD"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "StatusCD");
                            DateTime dtStatusDate = DateTime.Parse(APDFoundation.GetXMLSingleNodeValue(oXMLNode, "StatusDate"));
                            DRFinancialInvoices["StatusDate"] = dtStatusDate.Date.ToString("MM/dd/yyyy");
                            string dtSysLastUpdatedDate = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "SysLastUpdatedDate");
                            DRFinancialInvoices["SysLastUpdatedDate"] = dtSysLastUpdatedDate;

                            //DateTime dt2 = DateTime.ParseExact(APDFoundation.GetXMLSingleNodeValue(oXMLNode, "StatusDate"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            //DRFinancialInvoices["StatusDate"] = dt2.ToString("mm/dd/yyyy");


                            string sItemTypeCD = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "ItemTypeCD");
                            if (sItemTypeCD.ToUpper() == "F")
                            {
                                DRFinancialInvoices["ItemTypeCD"] = "Fee";
                            }

                            if (sItemTypeCD.ToUpper() == "I")
                            {
                                DRFinancialInvoices["ItemTypeCD"] = "Indemnity";
                            }

                            DRFinancialInvoices["Amount"] = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "Amount");

                            //sLynxID = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "LynxID");
                            //sInvoiceID = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "InvoiceID");
                            //Response.Write(sLynxID + " - " + sInvoiceID + "<br/");

                            DTFinancialInvoices.Rows.Add(DRFinancialInvoices);
                        };

                        if (oXMLNodeList.Count > 0)
                        {
                            gvFinancialInvoices.DataSource = DTFinancialInvoices;
                            gvFinancialInvoices.DataBind();
                        }
                        else
                        {
                            DTFinancialInvoices.Rows.Add(DTFinancialInvoices.NewRow());
                            gvFinancialInvoices.DataSource = DTFinancialInvoices;
                            gvFinancialInvoices.DataBind();

                            int TotalColumns = gvFinancialInvoices.Rows[0].Cells.Count;
                            gvFinancialInvoices.Rows[0].Cells.Clear();
                            gvFinancialInvoices.Rows[0].Cells.Add(new TableCell());
                            gvFinancialInvoices.Rows[0].Cells[0].ColumnSpan = TotalColumns;
                            gvFinancialInvoices.Rows[0].Cells[0].Text = "No Invoices Found";
                        }

                        //-- Bind and Show the Coverage GridView
                        //gvFinancialInvoices.DataSource = DTFinancialInvoices;
                        //gvFinancialInvoices.DataBind();
                        //gvFinancialInvoices.Visible = true;

                        gvFinancialInvoices.Visible = true;
                    } // If End
                } // If End
            } // Using End
        }

        protected void GetClaimDetails(string sLynxID)
        {
            XmlDocument XMLReturn = new XmlDocument();
            XmlNodeList oXMLNodeList = null;
            APDFoundation APDFoundation = new APDFoundation();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(appSettingsReader.GetValue("WSAPIServerURL", typeof(string)).ToString());
                //var responseTask = client.GetAsync("APDWSApiFinancial/GetAllInvoices?LynxID=1850978&InvType=F&type=json");

                // XML Response
                var responseTask = client.GetAsync("APDWSApi/ClaimDetails?LynxID=" + sLynxID);
                responseTask.Wait();

                HttpResponseMessage result = new HttpResponseMessage();

                result = responseTask.Result;

                //result.Content.Headers(XML, Encoding.UTF8, "application/xml");

                //-- DEBUGGING
                lbDebug.Items.Add("WSApi_Load: " + result.RequestMessage.RequestUri.AbsoluteUri);

                if (result.IsSuccessStatusCode)
                {
                    string sXMLReturn = string.Empty;

                    sXMLReturn = result.Content.ReadAsStringAsync().Result;
                    //sXMLReturn = sXMLReturn.Replace('<ArrayOfInvoiceItems xmlns:i="http://www.w3.org/2001/XMLSchema-instance\" xmlns="http://schemas.datacontract.org/2004/07/APDWSApi.Models\"', "");

                    //sXMLReturn = sXMLReturn.Replace("<xml version=\"1.0\">", "");

                    //var Content = new StringContent(sXMLReturn, Encoding.UTF8, "application/xml");

                    XElement incomingXml = XElement.Parse(sXMLReturn);



                    XMLReturn.LoadXml(sXMLReturn);


                    oXMLNodeList = XMLReturn.SelectNodes("Claim/Owner");

                    if (oXMLNodeList.Count > 0)
                    {
                        //-- Return existing Invoices
                        foreach (XmlNode oXMLNode in oXMLNodeList)
                        {
                            string sOwnerName = APDFoundation.GetXMLSingleNodeValue(oXMLNode, "OwnerUserFirstName");
                        }
                    }
                }
            }
        }

            //protected void grdContact_RowDataBound(object sender, GridViewRowEventArgs e)
            //{

            //    //ContactTypeTableAdapter contactType = new ContactTypeTableAdapter();
            //    //DataTable contactTypes = contactType.GetData();
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        Label lblType = (Label)e.Row.FindControl("lblType");
            //        if (lblType != null)
            //        {
            //            int typeId = Convert.ToInt32(lblType.Text);
            //            //lblType.Text = (string)DRFinancialInvoices.GetTypeById(typeId);
            //        }
            //        DropDownList cmbType = (DropDownList)e.Row.FindControl("cmbType");
            //        if (cmbType != null)
            //        {
            //            //cmbType.DataSource = contactTypes;
            //            cmbType.DataTextField = "TypeName";
            //            cmbType.DataValueField = "Id";
            //            cmbType.DataBind();
            //            cmbType.SelectedValue =
            //            gvFinancialInvoices.DataKeys[e.Row.RowIndex].Values[1].ToString();
            //        }
            //    }
            //    if (e.Row.RowType == DataControlRowType.Footer)
            //    {
            //        DropDownList cmbNewType = (DropDownList)e.Row.FindControl("cmbNewType");
            //        cmbNewType.DataSource = gvFinancialInvoices;
            //        cmbNewType.DataBind();
            //    }
            //}
        }
    }