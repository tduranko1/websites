﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(APDMonitor1.Startup))]
namespace APDMonitor1
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
