﻿
using System.Configuration;

namespace APDMonitor1.Common
{
    public class ApplicationConfiguration
    {
        public static string GetDatabaseConnectionString()
        {
            string connectionString = string.Empty;

            connectionString = ConfigurationManager.ConnectionStrings["wsAPDUser_udb_apd_stg_ConnectionString"].ToString();

            return connectionString;
        }
    }
}