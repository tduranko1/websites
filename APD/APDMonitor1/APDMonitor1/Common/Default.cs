﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace APDMonitor1.Common
{
    public class Default
    {
        //HQCL-3311 - APD Monitor - inline query to SP functionality - Start
        public DataTable GetServerStatus()
        {
            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataTable dtResult = new DataTable();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetServerStatus");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dtResult);
                return dtResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }

        public DataSet GetTotoalClaimCountDetails()
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetTotalClaimCountDetails");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dsResult);
                return dsResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }

        public DataSet GetShopAssignCountDetails()
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetShopAssignCountDetails");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dsResult);
                return dsResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }

        public DataSet GetAPDtoHQCountDetails()
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetAPDtoHQCountDetails");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dsResult);
                return dsResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }

        public DataSet GetHQtoAPDCountDetails()
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetHQtoAPDCountDetails");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dsResult);
                return dsResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }

        public DataSet GetOutboundElecDocumentsCountDetails()
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd = new SqlCommand("uspGetOutboundElecDocumentsCountDetails");

                adapter.SelectCommand = cmd;
                adapter.SelectCommand.Connection = sqlconn;
                adapter.Fill(dsResult);
                return dsResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }
        public DataTable GetUserDetails(string strUserID)
        {

            SqlConnection sqlconn = new SqlConnection();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataSet dsResult = new DataSet();
            DataTable dtResult = new DataTable();

            try
            {
                sqlconn.ConnectionString = ApplicationConfiguration.GetDatabaseConnectionString();
                cmd.CommandText = "uspGetUserRoleDetail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LogonID", strUserID);
                adapter = new SqlDataAdapter(cmd);
                cmd.Connection = sqlconn;
                dtResult = new DataTable();
                adapter.Fill(dtResult);
                return dtResult;
            }
            catch (Exception oExcept)
            {
                throw oExcept;
            }
            finally
            {
                cmd = null;
                sqlconn.Close();
            }
        }
        //HQCL-3311 - APD Monitor - inline query to SP functionality - End
    }
}