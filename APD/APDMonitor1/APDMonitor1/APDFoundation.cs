﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Xml;

namespace APDMonitor1
{
    public class APDFoundation
    {
        public XmlDocument ExecuteSpAsXML(string StoredProcedure, string Parameters)
        {
            SqlConnection Conn = new SqlConnection();
            SqlCommand cmdSelect = new SqlCommand();
            AppSettingsReader appSettingsReader = new AppSettingsReader();

            try
            {
                // -------------------------------------------
                //  Open connection to DB and run stored Proc
                // -------------------------------------------
                cmdSelect.CommandType = CommandType.Text;
                cmdSelect.CommandText = ("EXEC "
                            + (StoredProcedure + (" " + Parameters)));
                cmdSelect.Connection = Conn;
                Conn.ConnectionString = appSettingsReader.GetValue("ConnectString", typeof(string)).ToString();

                Conn.Open();
                // -------------------------------------------
                //  Execute the SQL
                // -------------------------------------------
                XmlReader XmlReturnReader;
                XmlReturnReader = cmdSelect.ExecuteXmlReader();
                XmlReturnReader.Read();
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(XmlReturnReader);
                return xdoc;
            }
            catch (Exception oExcept)
            {
                XmlDocument XMLErrorDoc = new XmlDocument();
                System.Diagnostics.StackFrame FunctionName = new System.Diagnostics.StackFrame();
                XMLErrorDoc.LoadXml(oExcept.Message);
                //string sError = "";
                //XMLErrorDoc.LoadXml(("<ErrorHandler><Error>ERROR: Executing stored procedure: "
                //                + (StoredProcedure + (" ("
                //                + (FunctionName.GetMethod.FunctionName + (")...  "
                //                + (oExcept.Message + "</Error></ErrorHandler>")))))));
                //sError = string.Format("Error: {0}", ("WebSerices Failed: Executing store procedure: "
                //                + (StoredProcedure + (" ("
                //                + (FunctionName.GetMethod.Name + (")...  " + oExcept.Message))))));
                //LogEvent("WSServerSideProcessor", "ERROR", ("Failed executing stored procedure: " + StoredProcedure), sError, (" StoredProc: "
                //                + (StoredProcedure + (" Params: " + Parameters))));
                return XMLErrorDoc;
            }
            finally
            {
                cmdSelect = null;
                Conn.Close();
            }

        }

        public string LocalLogEvent(string sProcessingServer, string sEventTransactionID, string sEventType, string sEventSeq, string sEventStatus, string sEventDescription, string sEventDetailedDescription, string sEventXml)
        {
            try
            {
                string sReturn = string.Empty;

                //-------------------------------//
                // Database access
                //-------------------------------//  
                string sStoredProcedure = "uspAPDPostOfficeEventInsLogEntry";
                string sParams = "@vProcessingServer = '" + sProcessingServer + "', @vEventTransactionID = '" + sEventTransactionID + "', @vEventType = '" + sEventType + "', @iEventSeq = " + sEventSeq + ", @vEventStatus = '" + sEventStatus + "', @vEventDescription = '" + sEventDescription + "', @vEventDetailedDescription = '" + sEventDetailedDescription + "', @vEventXML = '" + sEventXml + "', @vRecID = ''";

                //_events.Trace("<<<BaseJob>>>Logging Local Event: " + DateTime.Now + " - " + sStoredProcedure + " " + sParams);

                // -------------------------------
                //  Call WS and Process Request
                // -------------------------------
                //sReturn = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams);

                // -------------------------------------------
                //  Check response for errors
                // -------------------------------------------
                if (sReturn == "0")
                {
                    return sReturn;
                }
                else {
                    throw new System.Exception(string.Format("Error: {0}", "WebSerices Failed: APDPostOffice event logging failed to insert a row (LogEvent)...  " + sReturn));
                }
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string sError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                sError = string.Format("Error: {0}", ("WebServices Failed: APDPostOffice event logging failed to insert a row (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                //wsAPDFoundation.LogEvent("APDPostOffice", "ERROR", "Error occurred...", sError, "");

                // SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
                return sError;
            }
            finally
            {
            }
        }

        public string GetXMLNodeAttributeValue(XmlNode XMLnode, string strAttributes)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.Attributes[strAttributes] == null))
                    {
                        strRetVal = XMLnode.Attributes[strAttributes].InnerText;
                    }

                }

                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("GetXMLNodeAttrbuteValue Failed: Failed to parse XMLNode (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Error: " + strError, "");
                return strError;
            }
        }

        public string GetXMLSingleNodeValue(XmlNode XMLnode, string strNode)
        {
            try
            {
                string strRetVal = "";
                if (!(XMLnode == null))
                {
                    if (!(XMLnode.SelectSingleNode(strNode) == null))
                    {
                        strRetVal = XMLnode.SelectSingleNode(strNode).InnerText;
                    }

                }

                return strRetVal;
            }
            catch (Exception oExcept)
            {
                // ------------------------------
                //  Email Notify of the Error
                // ------------------------------
                string strError = "";
                string sBody = "";
                StackFrame FunctionName = new StackFrame();
                strError = string.Format("Error: {0}", ("GetXMLSingleNodeValue Failed: Failed to parse XMLNode (" + (FunctionName.GetMethod().Name + ")...  " + oExcept.ToString())));

                //LocalLogEvent(PROCESSINGSERVER, strEventTransactionID, "ERROR", intEventSeq.ToString(), DateTime.Now + " - Error occurred...", "Details in DetailedDescription", "Error: " + strError, "");
                return sBody;
            }
        }

        //-- End of Function
    }
}