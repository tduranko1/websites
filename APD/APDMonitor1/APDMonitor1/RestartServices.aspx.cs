﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Management;
using System.ServiceProcess;
using Microsoft.Web.Administration;
using System.Configuration;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace APDMonitor1
{
    public partial class RestartServices : System.Web.UI.Page
    {
        //Server/Machine name declarations
        public string strHYAPDMachine = string.Empty;
        //public string strAPDMachine = string.Empty;
        //public string strEBIZMachine = string.Empty;

        //Window services
        //public string strClaimServiceName = "PGWClaimPointFoundation";
        //public string strShopAssignServiceName = "LYNXChoiceShopService";
        //public string strAPDtoHQServiceName = "LYNXHyperquestService";
        //public string strHQtoAPDServiceName = "PartnerService";
        public string strDocProcServiceName = "DocProcQueuePickup";
        public string strOutBoundServiceName = "EMSHelper";

        //Web services
        //public string strClaimsTodayServiceURL = string.Empty;
        //public string strHQtoAPDServiceURL = string.Empty;

        //Impersonate user
        //// obtains user token
        // [DllImport("advapi32.dll", SetLastError = true)]
        //public static extern bool LogonUser (string pszUsername, string pszDomain, string pszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        //// closes open handes returned by LogonUser
        //[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        //public extern static bool CloseHandle (IntPtr handle);

        //int LogonType = 9; //LogonType.NewCredentials
        //int LogonProvider = 3;//LogonProvider.WinNT50

        protected void Page_Load (object sender, EventArgs e)
        {
            GetAppSettingValues();

            //GetServiceStatus(strShopAssignServiceName, strAPDMachine, btnShopAssign, lblShopAssign);
            //GetServiceStatus(strAPDtoHQServiceName, strAPDMachine, btnAPDtoHQ, lblAPDtoHQ);
            GetServiceStatus(strDocProcServiceName, strHYAPDMachine, btnDocProc, lblDocProc);
            GetServiceStatus(strOutBoundServiceName, strHYAPDMachine, btnOutBound, lblOutBound);

            //RestartWebService();
        }

        private void GetAppSettingValues ()
        {
            try
            {
                AppSettingsReader appSettingsReader = new AppSettingsReader();

                strHYAPDMachine = appSettingsReader.GetValue("HYAPDMachine", typeof(string)).ToString();
                
                //strAPDMachine = appSettingsReader.GetValue("APDMachine", typeof(string)).ToString();
                //strEBIZMachine = appSettingsReader.GetValue("EBIZMachine", typeof(string)).ToString();
                //strClaimsTodayServiceURL = appSettingsReader.GetValue("ClaimsTodayServiceURL", typeof(string)).ToString();
                //strHQtoAPDServiceURL = appSettingsReader.GetValue("HQtoAPDServiceURL", typeof(string)).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetServiceStatus (string strServiceName, string strMachineName, Button btnName, Label lblName)
        {
            //IntPtr tokenHandle = IntPtr.Zero;
            try
            {
                // if (impersonateValidUser(ConfigurationManager.AppSettings["APDImpName"], ConfigurationManager.AppSettings["APDImpDomain"], ConfigurationManager.AppSettings["APDImpPass"]))
                // {
                // WindowsIdentity newId = new WindowsIdentity(tokenHandle);

                // using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                //{
                ServiceController sc = new ServiceController(strServiceName, strMachineName);
                //Check the service status
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    btnName.Enabled = true;
                    lblName.Text = "Down";
                    lblName.Font.Bold = true;
                }
                else
                {
                    btnName.Enabled = false;
                    lblName.Text = "Running";
                    lblName.Font.Bold = false;
                }
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RestartWindowService (string strServiceName, string strMachineName, Button btnName, Label lblName)
        {
            // IntPtr tokenHandle = IntPtr.Zero;
            try
            {
                // if (LogonUser(ConfigurationManager.AppSettings["APDImpName"], ConfigurationManager.AppSettings["APDImpDomain"], ConfigurationManager.AppSettings["APDImpPass"], LogonType, LogonProvider, ref tokenHandle))
                //  {
                //     WindowsIdentity newId = new WindowsIdentity(tokenHandle);

                //  using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                //  {
                ServiceController sc = new ServiceController(strServiceName, strMachineName);
                //Start the service if the service is stopped/down.
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    sc.Start();
                    btnName.Enabled = false;
                    lblName.Text = "Running";
                    lblName.Font.Bold = false;
                }
                // }
                //  CloseHandle(tokenHandle);
                // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void lbtnShopAssign_Click (object sender, EventArgs e)
        {
            //RestartWindowService(strShopAssignServiceName, strAPDMachine, btnShopAssign, lblShopAssign);
        }

        protected void lbtnAPDtoHQ_Click (object sender, EventArgs e)
        {
            //RestartWindowService(strAPDtoHQServiceName, strAPDMachine, btnAPDtoHQ, lblAPDtoHQ);
        }

        protected void lbtnDocProc_Click (object sender, EventArgs e)
        {
            RestartWindowService(strDocProcServiceName, strHYAPDMachine, btnDocProc, lblDocProc);
        }

        protected void lbtnOutBound_Click (object sender, EventArgs e)
        {
            RestartWindowService(strOutBoundServiceName, strHYAPDMachine, btnOutBound, lblOutBound);
        }
    }
}
