<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strHTML

	On Error Resume Next

	strUserID = GetSession("UserID")
	'UpdateSession "LynxID", ""

	'Create and initialize DataPresenter.CExecute object.
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckErr()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
	CheckErr()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspAPDRAAUserPhaseGetListWSXML", "AdvWorkQueueView.xsl",strUserID )
	CheckErr()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0

	Sub CheckErr()
		If Err.Number <> 0 Then
			ErrorHandler()
			Response.End
		End If
	End Sub

%>