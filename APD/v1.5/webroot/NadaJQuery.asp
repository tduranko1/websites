<%@ Language=VBScript %>
<% Option Explicit %>

<%


	Dim xmlhttp  
	'Dim strHtmlValue
    Dim DataToSend
    Dim strHtmlValue
    'Dim xmlStatus
    dim objFSO, objFile, strFilePath,postUrl   
    Dim strYear, strMake, strModel, strVin
    Dim strVehID, strRegionID
    Dim strMode,strServiceURL
    Dim oExe
     Dim iLoop
    'strFilePath = "C:\IntranetSites\APD\webroot\test1.txt"
    'Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    'Set objFile=objFSO.OpenTextFile(strFilePath, 8, true)  
       
    on error resume next
    
   strMode = request.QueryString("mode")
   
   Set oExe = CreateObject("DataPresenter.CExecute")
   oExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH") & "..\config\config.xml"
   strServiceURL = oExe.Setting("NADAWebservice/URL")
   
    if strMode = "getMake" then 
        'Dim strYear 
        strYear = request.Form("year")         
        DataToSend="intYear=" & strYear        
        postUrl = strServiceURL & "GetMakesStringArray" 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send DataToSend
        'If xmlhttp status is 0, connection to PGW NADA webservice failed
        'looping 5 times to connect to PGW nada service     
       
        For iLoop = 0 to 5
            if xmlhttp.status = 0 then 
                set xmlhttp = nothing
                Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                
                xmlhttp.Open "POST",postUrl,false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend     
            else
                exit for
            end if      
        next
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
    end if 
    
    if strMode = "getModel" then 
        
        strYear = request.Form("year") 
        strMake = request.Form("make") 
        DataToSend="intYear=" & strYear & "&intMakeCode=" & strMake
        postUrl = strServiceURL & "GetModelsStringArray"         
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
    
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send DataToSend
        'If xmlhttp status is 0, connection to PGW NADA webservice failed
        'looping 5 times to connect to PGW nada service     
      
        For iLoop = 0 to 5
            if xmlhttp.status = 0 then 
                set xmlhttp = nothing
                Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                
                xmlhttp.Open "POST",postUrl,false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend     
            else
                exit for
            end if      
        next
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
    end if 
    
    if strMode = "getBody" then 
        strYear = request.Form("year") 
        strMake = request.Form("make") 
        strModel = request.Form("model")
        DataToSend="intYear=" & strYear & "&intMakeCode=" & strMake & "&intSeriesCode=" & strModel
        postUrl = strServiceURL & "GetBodiesStringArray" 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
    
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send DataToSend
        'If xmlhttp status is 0, connection to PGW NADA webservice failed
        'looping 5 times to connect to PGW nada service     

        For iLoop = 0 to 5
            if xmlhttp.status = 0 then 
                set xmlhttp = nothing
                Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                
                xmlhttp.Open "POST",postUrl,false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend     
            else
                exit for
            end if      
        next
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
    end if 
    
    if strMode = "getVinInfo" then 
        strVin = request.Form("Vin") 
        DataToSend="strVin=" & strVin 
        postUrl = strServiceURL & "VinLookupArray" 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
    
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send DataToSend
        
        'If xmlhttp status is 0, connection to PGW NADA webservice failed
        'looping 5 times to connect to PGW nada service     

        For iLoop = 0 to 5
            if xmlhttp.status = 0 then 
                set xmlhttp = nothing
                Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                
                xmlhttp.Open "POST",postUrl,false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend     
            else
                exit for
            end if      
        next
    
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
    end if 
    
    if strMode = "getAccessory" then   'strVehID, strRegionID
        strVehID = request.Form("VehID") 
        strRegionID = request.Form("Region") 
        DataToSend="intVehicleID=" & strVehID & "&strVin=" & "&intRegion=" & strRegionID 
        postUrl = strServiceURL & "GetAccessoriesList" 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
    
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send DataToSend
        
        
        'If xmlhttp status is 0, connection to PGW NADA webservice failed
        'looping 5 times to connect to PGW nada service     
       
        For iLoop = 0 to 5
            if xmlhttp.status = 0 then 
                set xmlhttp = nothing
                Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
                
                xmlhttp.Open "POST",postUrl,false
                xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
                xmlhttp.send DataToSend     
            else
                exit for
            end if      
        next
    
    
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
    end if 
    
    'response.Write xmlhttp.responseText
    'objFile.WriteLine("<br>Error" &  err.Description & "<br>")
     'objFile.WriteLine("<br>return value" &  xmlhttp.responseText & "<br>")
    'response.Write xmlhttp.status
     
    ' objFile.Close
    'Set objFile = nothing
    'Set objFSO = nothing  
    
    if len(strHtmlValue) > 0 then
    
     Dim xmlDoc
     set xmlDoc = createObject("MSXML2.DOMDocument")
     xmlDoc.async = False     
     xmlDoc.loadXML(strHtmlValue)
    
     response.Write replace(replace(replace(xmlDoc.getElementsByTagName("string").item(0).childNodes(0).text,"&amp;","&"),"&gt;",">"),"&lt;","<")
      'if err.number > 0 then
        'response.Write err.Description & "error"
      'end if 
     end if

%>