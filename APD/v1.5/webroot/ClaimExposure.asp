<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strLynxID
	Dim strUserID
  Dim strMode
	Dim strInsuranceCompanyID
	Dim strHTML
  Dim strClaimAspectID
  Dim strClaimStatus
  Dim strAssignableUserXML
  Dim sApplication
  Dim strActiveUserXML

    On Error Resume Next

	sApplication = "APD"
    strLynxID = Request("LynxID")
   
	If strLynxID = "" Then
		strLynxID = GetSession("LynxID")
	Else
		UpdateSession "LynxID", strLynxID
	End If

	strUserID = GetSession("UserID")
    strMode = request.QueryString("mode")

    if strMode = "" then strMode = "close"

	CheckError()
  
    strClaimAspectID = Request("ClaimAspectID")
    if strClaimAspectID = "" then
        strClaimAspectID = GetSession("Claim_ClaimAspectID")
        If strClaimAspectID = "" then
            err.raise vbObjectError + 1000, "ClaimExposure.asp", "Invalid ClaimAspectID or no ClaimAspectID was passed in."
            Call CheckError()
        response.end
        end if
    end if

	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	objExe.AddXslParam "mode", strMode
	CheckError()

	objExe.AddXslParam "LynxID", strLynxID
	CheckError()

	objExe.AddXslParam "UserID", strUserID
	CheckError()

'	objExe.AddXslParam "ClaimStatus", strClaimStatus
'	CheckError()

	objExe.AddXslParam "ClaimAspectID", strClaimAspectID
	CheckError()

    if strMode = "open" then
      'Get the user assignable list
'      strAssignableUserXML = objExe.ExecuteSpAsXML( "uspClaimReopenAssignGetListXML", "", strLynxID, strUserID, sApplication)
'      if strAssignableUserXML <> "" then
'        Dim objXML
'        set objXML = createObject("MSXML2.DOMDocument")
'        CheckError()
'        
'        objXML.loadXML strAssignableUserXML
'        Dim strOwner
'        
'        Dim strSubordinatesUserNameList
'        Dim strSubordinatesUserIDList
'        Dim nde
'        
'        if objXML.parseError.errorCode <> 0 then
'            err.raise vbObjectError + 1000, "ClaimExposure.asp", objXML.parseError.reason
'            Call CheckError()
'            response.end
'        end if
'        
'        set nde = objXML.selectSingleNode("/Root")
'        if isobject(nde) then
'          strClaimStatus = nde.attributes(2).value
'          CheckError()
'        	objExe.AddXslParam "ClaimStatus", strClaimStatus
'        	CheckError()
'        end if

'        set nde = objXML.selectSingleNode("/Root/Owner")
'        if isobject(nde) then
'          strOwner = nde.attributes(0).value + "|" + nde.attributes(1).value + "|" + nde.attributes(2).value + "|" + nde.attributes(3).value
'        	objExe.AddXslParam "ClaimOwner", strOwner
'        	CheckError()
'        end if
'        
'        Dim nodes
'        set nodes = objXML.selectNodes("/Root/Supervisor/Subordinates")
'        if isobject(nodes) then
'          strSubordinatesUserNameList = ""
'          strSubordinatesUserIDList = ""
'          for each nde in nodes
'            strSubordinatesUserNameList = strSubordinatesUserNameList + nde.attributes(2).value + ", " + nde.attributes(1).value + "|"
'            strSubordinatesUserIDList = strSubordinatesUserIDList + nde.attributes(0).value + "|"
'          next
          
'        	objExe.AddXslParam "SubordinateNames", strSubordinatesUserNameList
'        	CheckError()
         
'        	objExe.AddXslParam "SubordinateIDs", strSubordinatesUserIDList
'        	CheckError()
'        end if
'      end if     
	    ' Get all closed exposures and transform to HTML.
    	strHTML = objExe.ExecuteSpAsXML( "uspClaimExposureGetListXML", "ClaimExposure.xsl", strLynxID, 1)
      strActiveUserXML = objExe.ExecuteSpAsXML( "uspActiveUserGetListXML", "", "APD")
      
    else
	    ' Get all open exposures and transform to HTML.
    	strHTML = objExe.ExecuteSpAsXML( "uspClaimExposureGetListXML", "ClaimExposure.xsl", strLynxID, 0)
    end if

	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML
  
  if strActiveUserXML <> "" then
    Response.write "<xml id=""xmlActiveUsers"" name=""xmlActiveUsers"" ondatasetcomplete=""initActiveUsers()"">"
    Response.write strActiveUserXML
    Response.write "</xml>"
  end if

	On Error GoTo 0
%>

