<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strHTML
	Dim strClaimNumber
	Dim strLynxID
	Dim strLastName
	Dim strFirstName
  Dim strInsuranceCompanyID
  ' the next two params are for ECAD extension only
	Dim strInvolvedTypeCD
  Dim strCarrierContactNameLast

	On Error Resume Next

	strUserID = GetSession("UserID")

	strClaimNumber = Request("Claim")
	strLynxID = Request("LynxID")
	strLastName = Request("LastName")
	strFirstName = Request("FirstName")
	strInsuranceCompanyID = Request("InsuranceCompanyID")
  strInvolvedTypeCD = ""
  strCarrierContactNameLast = ""
	CheckErr()

	'Create and initialize DataPresenter.CExecute object.
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckErr()
  
  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckErr()
  
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimSearchXML", "ClaimSearchResults.xsl", strClaimNumber, strLynxID, strLastName, strInsuranceCompanyID )
	CheckErr()
  
	Set objExe = Nothing
  
  Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0

	Sub CheckErr()
		If Err.Number <> 0 Then
			ErrorHandler()
			Response.End
		End If
	End Sub

%>
