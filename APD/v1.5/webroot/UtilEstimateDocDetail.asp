<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->


<%
	On Error Resume Next

	Dim objExe, strHTML, strDocumentID, strClaimAspectID, strEventUtilEstimateUpdate, strUserID
  
  Set objExe = InitializeSession()
  
	strDocumentID = Request("DocumentID")
	strClaimAspectID = Request("ClaimAspectID")

	'Create and initialize DataPresenter if not already existing
  if objExe is nothing then
	  Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()
	end if

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
  CheckError()

  strEventUtilEstimateUpdate = objExe.Setting("WorkFlow/EventCodes/Code[@name='EstimateDataUpdated']")
  CheckError()

  strUserID = GetSession("UserID")

  objExe.AddXslParam "DocumentID", strDocumentID
  objExe.AddXslParam "ClaimAspectID", strClaimAspectID
  objExe.AddXslParam "LynxID", Request("LynxID")
  objExe.AddXslParam "UserId", strUserID
  objExe.AddXslParam "EventUtilEstimateUpdate", strEventUtilEstimateUpdate
  objExe.AddXslParam "VehicleNumber", Request("VehicleNumber")
 
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspEstimateQuickGetDetailXML", "UtilEstimateDocDetail.xsl", strDocumentID, strClaimAspectID )
	CheckError()


	Set objExe = Nothing

	Response.Write strHTML

	On Error GoTo 0
%>
