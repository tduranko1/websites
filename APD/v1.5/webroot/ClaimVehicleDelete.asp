<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strLynxID
	Dim strHTML

    On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = GetSession("LynxID")

	CheckError()

	'Create and initialize DataPresenter
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	CheckError()

	objExe.AddXslParam "VehContext", GetSession("VehContext")
	CheckError()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimVehicleDeleteGetDetailXML", "ClaimVehicleDelete.xsl", strLynxID )
	CheckError()

	Set objExe = nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>

