<!-- #include file="errorhandler.asp" -->
<%
    Response.Expires = -1
    on error resume next
    dim xmlDoc, xslDoc, docPath, strHTML
    Dim fso

    docPath = request.queryString("docPath")

    if docPath <> "" then
        Set fso = CreateObject("Scripting.FileSystemObject")
        Call CheckError()

        if not fso.FileExists(docPath) then
            response.write "File not found.<br><br>Requested file: " + docPath
            response.end
        end if

        if LCase(fso.GetExtensionName(docPath)) <> "xml" then
            response.write "Specified File name is not an XML file. This web page view XML files only."
            response.end
        end if

        set fso = nothing

        set xmlDoc = CreateObject("MSXML2.DOMDocument")
        Call CheckError()
        set xslDoc = CreateObject("MSXML2.DOMDocument")

        xmlDoc.Load docPath

        if xmlDoc.parseError.errorCode <> 0 then
            err.raise vbObjectError + 1000, "viewXML.asp", xmlDoc.parseError.reason
            call cleanupObjects
            Call CheckError()
            response.end
        end if

        xslDoc.Load server.mapPath("/xsl/ViewXML.xsl")
        if xslDoc.parseError.errorCode <> 0 then
            err.raise vbObjectError + 1000, "viewXML.asp", xmlDoc.parseError.reason
            call cleanupObjects
            call CheckError()
            response.end
        end if

        strHTML = xmlDoc.transformNode(xslDoc)

        response.write strHTML

        call cleanupObjects

    end if

    sub cleanupObjects
        set xmlDoc = nothing
        set xslDoc = nothing
    end sub
%>
