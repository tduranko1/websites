<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
    'On Error Resume Next
    Dim objExe
    Dim strUserID


    Dim strHTML
    Dim ClaimAspectID
    Dim InsuranceCompanyID
    
    strUserID = GetSession("UserID")
    ClaimAspectID = Request("ClaimAspectID")
    InsuranceCompanyID = Request("InsuranceCompanyID")

    Set objExe = CreateObject("DataPresenter.CExecute")
    Call CheckError

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
    Call CheckError

    strHTML = objExe.ExecuteSpAsXML( "uspMETotalsGetDetailsXML", "METotals.xsl", ClaimAspectID, InsuranceCompanyID)

    Response.Write strHTML

    Call CheckError

    Set objExe = Nothing
%>

