<%
'On Error Resume Next
    Dim strPath

    strPath = CStr(Request.QueryString("docPath"))
    'strPath = Cstr("\\pgw.local\dfs\intra\apddocuments\DEV\Storage\1\0\9\8\DU20121011035122LYNXID1601098VDoc0001.ZIP")

    If strPath = "" Then
        Response.Clear
        Response.Write("No file specified.")
        Response.End
    ElseIf InStr(strPath, "..") > 0 Then
        Response.Clear
        Response.Write("Illegal folder location.")
        Response.End
    ElseIf Len(strPath) > 1024 Then
        Response.Clear
        Response.Write("Folder path too long.")
        Response.End
    Else
        Call DownloadFile(strPath)
    End If

Private Sub DownloadFile(file)
    '--declare variables
    Dim objFSO
    Dim objFile
    Dim objStream
    Dim strFileName
 
    '-- create FSO object to check if file exists and get properties
     Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    
    '-- check, if the file exists
    If objFSO.FileExists(file) Then 
        set objFile = objFSO.GetFile(file)
        strFileName = objFile.Name
        set objFile = nothing

        '-- clear the response, and then set the appropriate headers
        Set objStream = Server.CreateObject("ADODB.Stream")
        objStream.Open
        '-- set as binary
        objStream.Type = 1            
        '-- load into the stream the file
        objStream.LoadFromFile(file)        
        '-- send the stream in the response
        'Response.Clear
        Response.ContentType = "application/octet-stream"
        Response.AddHeader "Content-Disposition", "inline;filename=" & strFileName
        'Response.AddHeader "Content-Length", objFile.Size 
        Response.BinaryWrite objStream.Read       
        Response.Flush
	'response.End
        objStream.Close
        Set objStream = Nothing
    Else 
        Response.Clear
        Response.Write("No such file exists.")
        Response.End
    End If
        Set objFSO = Nothing
End Sub          

%>
