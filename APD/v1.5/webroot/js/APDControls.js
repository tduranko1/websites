var __blnPageInitialized = false;
var __objAsyncHTTP = null;
var __fnAsyncHTTPCallbackSuccess = null;
var __fnAsyncHTTPCallbackFailure = null;

document.attachEvent("onreadystatechange", initBaseAPDControls);

String.prototype.Trim = trim_string;

var arrAPDControls = new Array();
function initBaseAPDControls() {
    if (document.readyState == "complete") {
        document.detachEvent("onreadystatechange", initBaseAPDControls);
        initAPDControlObjectHierarchy(document);
        checkPageReady();
    }
}

function trim_string() {
    var ichar, icount;
    var strValue = this;
    ichar = strValue.length - 1;
    icount = -1;
    while (strValue.charAt(ichar) == ' ' && ichar > icount)
        --ichar;
    if (ichar != (strValue.length - 1))
        strValue = strValue.slice(0, ichar + 1);
    ichar = 0;
    icount = strValue.length - 1;
    while (strValue.charAt(ichar) == ' ' && ichar < icount)
        ++ichar;
    if (ichar != 0)
        strValue = strValue.slice(ichar, strValue.length);
    return strValue;
}

String.prototype.toProperCase = function () {
    return this.toLowerCase().replace(/^(.)|\s(.)/g,
        function ($1) { return $1.toUpperCase(); });
}


// Displays external and internal event messages in a popup window.
// This version is called for errors occurring server side.
function ServerEvent() {
    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "EventPopup.asp";

    top.showModalDialog(sQuery, top, sDimensions + sSettings);
}

// Displays external and internal event messages in a popup window.
// This version is called for errors occurring client side.
function ClientEvent(nNumber, sDescription, sSource) {
    if ((String(sSource) == "undefined") || (sSource == null))
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "../EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);

    top.showModalDialog(sQuery, top, sDimensions + sSettings);
}

function ClientError(sDescription) { ClientEvent(0, sDescription); }
function ClientWarning(sDescription) { ClientEvent(1, sDescription); }
function ClientInfo(sDescription) { ClientEvent(2, sDescription); }

function onAPDControlWarning(strMessage) {
    ClientWarning(strMessage);
}

function on2APDControlError(strMessage) {
    ClientError(strMessage);
}

function YesNoMessage(sTitle, sMessage) {
    var strReturn = sTitle + "|" + sMessage;
    var iDialogHeight;

    // retrieve optional argument which specifies Dialog Height or default to 125px.
    iDialogHeight = arguments.length > 2 ? arguments[2] : 125;

    strReturn = window.showModalDialog("/js/YesNoDialog2.htm", strReturn, "dialogHeight: " + iDialogHeight + "px; dialogWidth: 450px; center: Yes; help: No; resizable: yes; status: No; unadorned: Yes");
    return strReturn;
}

function XMLSave(sXMLRequest) {
    var retObj = {
        code: null,
        xml: null
    }
    var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
    if (objXMLHTTP) {
        objXMLHTTP.open("POST", "/rs/APDSave.asp", false);
        objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        objXMLHTTP.send(sXMLRequest);
        //if (objXMLHTTP.responseXML.xml != "")
        //  return objXMLHTTP.responseXML;
        //else
        //  return objXMLHTTP.responseText;
        if (objXMLHTTP.responseXML.xml != "") {
            var oRetXML = objXMLHTTP.responseXML;
            //alert(oRetXML.xml);
            var oRetCode = oRetXML.documentElement.selectSingleNode("/Root/@returnCode");
            if (oRetCode && oRetCode.value != "0") {
                var strUserInfo = "";
                var aUserInfos = new Array();
                var oErrorNodes = oRetXML.documentElement.selectNodes("/Root/StoredProc[@returnCode != 0]");
                if (oErrorNodes.length > 0) {
                    for (var i = 0; i < oErrorNodes.length; i++) {
                        //strUserInfo = oErrorNodes[i].selectSingleNode("error/user").text;
                        //alert(oErrorNodes[i].selectSingleNode("error/advanced").text);
                        var objUserInfo = {
                            code: oErrorNodes[i].selectSingleNode("@returnCode").value,
                            userText: oErrorNodes[i].selectSingleNode("error/user").text,
                            advanced: oErrorNodes[i].selectSingleNode("error/advanced").text
                        }

                        aUserInfos.push(objUserInfo);
                    }
                } else {
                    var oErrorNode = oRetXML.documentElement.selectSingleNode("//error");
                    if (oErrorNode) {
                        strUserInfo = oErrorNode.selectSingleNode("user").text;
                        var objUserInfo = {
                            code: 1,
                            userText: (strUserInfo != "" ? strUserInfo : "A system error has occurred."),
                            advanced: oErrorNode.selectSingleNode("advanced").text
                        }

                        aUserInfos.push(objUserInfo);
                    }
                }
                //alert("User information count: " + aUserInfos.length);
                var sURL = "/userMessage.htm";
                var sFeatures = "dialogHeight:200px;dialogWidth:424px;center:yes;help:no;resizable:no;scroll:no;status:no"
                window.showModalDialog(sURL, aUserInfos, sFeatures);
                retObj.code = 1;
            } else {
                retObj.code = 0;
                retObj.xml = objXMLHTTP.responseXML;
            }
        }
    }

    return retObj;
}

function makeXMLSaveString(aRequests) {
    var sRet = '<Root>';
    if (typeof (aRequests) == "object") {
        for (var i = 0; i < aRequests.length; i++) {
            sRet += '<StoredProc name="' + aRequests[i].procName + '" execType="' + aRequests[i].method + '">' +
                    '<Data>' +
                    '\<\!\[CDATA\[' + aRequests[i].data + '\]\]\>' +
                    '</Data>' +
                    '</StoredProc>\n';
        }
    }
    sRet += '</Root>';

    return sRet;
}

function formatDateTime(str) { //str can be any JS recognizable date string (UTC, GMT, etc)
    var sRet = "";
    if (str != "") {
        var dt = new Date(str);
        var dtNow = new Date();
        if (dt != dtNow) { // this cannot happen.
            var iMonths = dt.getMonth() + 1;
            var iDate = dt.getDate();
            var iFullYear = dt.getFullYear();
            var iHrs = dt.getHours();
            var iMin = dt.getMinutes();
            var iSec = dt.getSeconds();

            sRet = (iMonths < 10 ? "0" + iMonths : iMonths) + "/" +
                   (iDate < 10 ? "0" + iDate : iDate) + "/" +
                   (iFullYear) + " " +
                   (iHrs < 10 ? "0" + iHrs : iHrs) + ":" +
                   (iMin < 10 ? "0" + iMin : iMin) + ":" +
                   (iSec < 10 ? "0" + iSec : iSec);
        }
    }
    return sRet;
}

function formatDateTimeAMPM(str) { //str can be any JS recognizable date string (UTC, GMT, etc)
    var sRet = "";
    if (str != "") {
        var dt = new Date(str);
        var dtNow = new Date();
        if (dt != dtNow) { // this cannot happen.
            var iMonths = dt.getMonth() + 1;
            var iDate = dt.getDate();
            var iFullYear = dt.getFullYear();
            var iHrs = dt.getHours();
            var iMin = dt.getMinutes();
            var iSec = dt.getSeconds();
            var sAMPM = "AM";

            if (iHrs > 12) {
                iHrs -= 12;
                sAMPM = "PM";
            }

            sRet = (iMonths < 10 ? "0" + iMonths : iMonths) + "/" +
                   (iDate < 10 ? "0" + iDate : iDate) + "/" +
                   (iFullYear) + " " +
                   (iHrs < 10 ? "0" + iHrs : iHrs) + ":" +
                   (iMin < 10 ? "0" + iMin : iMin) + ":" +
                   (iSec < 10 ? "0" + iSec : iSec) + " " +
                   sAMPM;
        }
    }
    return sRet;
}

function extractSQLDateTime(strDate) {
    var strConv =
      strDate.substr(5, 2) + "-"
      + strDate.substr(8, 2) + "-"
      + strDate.substr(0, 4) + " "
      + strDate.substr(11, 8);
    return strConv;
}

function formatSQLDateTime(str) { // SQL formatted date string ex: 1900-01-01T00:00:00
    if (str != "")
        return formatDateTime(extractSQLDateTime(str));
    else
        return "";
}

function formatSQLDateTimeAMPM(str) { // SQL formatted date string ex: 1900-01-01T00:00:00
    if (str != "")
        return formatDateTimeAMPM(extractSQLDateTime(str));
    else
        return "";
}

function safe_Execute(sEleID, sMethod) {
    var objEle = document.all[sEleID];
    if (objEle) {
        if ((objEle.className != "") || (objEle.style.behavior != ""))
            eval(sEleID + "." + sMethod);
        else {
        }
    }
}

function checkEMail() {
    obj = event.srcElement;
    if (obj && obj.value != "") {
        if (!isEmail(obj.value)) {
            event.returnValue = false;
            ClientWarning("Invalid E-Mail specified.<br>Example: johndoe@server.com<br><br>Please try again");
            obj.setFocus();
        }
    }
}

function checkURL() {
    obj = event.srcElement;
    if (obj && obj.value != "") {
        if (!isURL(obj.value)) {
            event.returnValue = false;
            ClientWarning("Invalid URL/Web address specified.<br>Example: http://www.lynxservices.com or www.lynxservices.com<br><br>Please try again");
            obj.setFocus();
        }
    }
}

function isURL(strURL) {
    if ((typeof (strURL) == "string") && (strURL != ""))
        strURL = strURL.Trim();

    return ((strURL.search(/^(((http:\/\/)|(www\.)))[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}$/) == 0) ||
            (strURL.search(/^[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}$/) == 0))
}

function isEmail(strEmail) {
    if (typeof (strEmail) == "string" && strEmail != "")
        strEmail = strEmail.Trim();

    //this is more standard email validation. Eliminates all invalid characters based on the RFC specification.
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(strEmail);
}

function concatString(str1, str2, strDelim) {
    if (str1 != "" && str2 != "")
        return str1 + strDelim + str2;
    else
        return str1 + str2;
}

function onClientsideError(sMsg, sUrl, sLine) {
    var sEnv = top.window.document.title;
    var sCallStack = "";
    var sCaller = "";
    var sFnName = "";
    var oCaller = null;
    var iCount = 0;
    oCaller = onClientsideError.caller;
    var sFirstCaller = "<unknown>";
    var bDevelopment = (sEnv.indexOf("Development") != -1);
    var aArgs;

    //Generate the call stack
    while (oCaller != null && iCount < 100) {
        sCaller = oCaller.toString();
        if (sCaller.indexOf("function") != -1) {

            sFnName = sCaller.substring(sCaller.indexOf("function") + 8, sCaller.indexOf("{"));
            if (iCount == 0)
                sFirstCaller = sFnName.Trim();

            //get the argument list for the function in context.
            aArgs = new Array();
            for (var i = 0; i < oCaller.arguments.length; i++) {
                if (typeof (oCaller.arguments[i]) == "string")
                    aArgs.push("\"" + oCaller.arguments[i] + "\"");
                else
                    aArgs.push(oCaller.arguments[i]);
            }

            sCallStack = sFnName + (aArgs.length > 0 ? "; Arguments: " + aArgs.join(", ") : "") + (bDevelopment ? "<br>" : "\n") + sCallStack;
        }
        oCaller = oCaller.caller;
        iCount++;
    }

    if (bDevelopment) {
        var sMessage = "An unhandled client-side error has occured in function \"" + sFirstCaller + "\".<br><br>" +
                       "Developers Note: This error could have occurred in the script defined in the URL mentioned below or in the included script files.<br><br>" +
                       "Error: " + sMsg + "<br>" +
                       "Line: " + sLine + "<br>" +
                       "URL: " + sUrl + "<br>" +
                       "Call Stack: <br>" + sCallStack;
        ClientWarning(sMessage);
    } else {
        var sMessage = "An unhandled client-side error has occured in function \"" + sFirstCaller + "\".\n\n" +
                       "Developers Note: This error could have occurred in the script defined in the URL mentioned below or in the included script files.\n\n" +
                       "Error: " + sMsg + "\n" +
                       "Function: " + sFirstCaller + "\n" +
                       "Line: " + sLine + "\n" +
                       "URL: " + sUrl + "\n" +
                       "Call Stack: \n" + sCallStack;
        ClientError(sMessage);
    }

    if (typeof (onClientsideError.caller.onErrorCallback) == "function")
        onClientsideError.caller.onErrorCallback();

    return true;
}
window.onerror = onClientsideError;

/*------------------------------------------------------------------------------------*/
/* makeHighlight
     Description: Image opacity filter changer
     Parameters:
      cur - object
      which - no opacity=0; with opacity=1
      level - opcacity level= 100:no opacity, 20:20% opacity
     Usage:
        60% opacity = makeHighlight(this,1,60)
      No opacity = makeHighlight(this,0,100)
*/
function makeHighlight(cur, which, intensity) {
    if (which == 0)
        cur.filters.alpha.opacity = intensity;
    else
        cur.filters.alpha.opacity = intensity;
}


//This function will find for any APD custom controls and initialize them.
//Do not use this function if the page has APDTab. APDTab will take care of
//initializing the active tab controls.
function initAPDControls() {
    initAPDControlObjectHierarchy(document);
}

function initAPDControlObjectHierarchy(objNode) {
    var sTagName = "";
    var sParentTagName = "";
    if (objNode) {
        var oChildNodes = objNode.childNodes;
        for (var i = 0; i < oChildNodes.length; i++) {
            sTagName = "";
            sParentTagName = "";
            try {
                sTagName = oChildNodes[i].tagName;
                sParentTagName = oChildNodes[i].parentElement.tagName;;
            } catch (e) { }
            if ((sTagName != undefined) && (sTagName.substr(0, 3).toLowerCase() == "apd")) {
                if (((sParentTagName != undefined) && (sParentTagName.substr(0, 3).toLowerCase() != "apd")) || (sParentTagName.toLowerCase() == "apdradiogroup")) {
                    if (oChildNodes[i].id != "") {
                        //initialize the control
                        applyAPDBehavior(oChildNodes[i]);
                    }
                }
            }

            if ((sParentTagName != undefined) && (sParentTagName.substr(0, 3).toLowerCase() != "apd")) {
                if (oChildNodes[i].childNodes.length > 0) {
                    initAPDControlObjectHierarchy(oChildNodes[i]);
                }
            }
        }
    }
}

function applyAPDBehavior(objControl) {
    var i = 0;
    var iLoopMax = 1000;
    var iLoopCount = 0;
    if (objControl) {
        iLoopCount = iLoopMax;
        var sTag = "";
        var sParentTag = "";
        try {
            sTag = objControl.tagName.toLowerCase();
            sParentTag = objControl.parentElement.tagName.toLowerCase();

            if (sTag == "apdtabgroup") {
                try {
                    var blnSuppressInit = false;// (objControl.getAttribute("autoInit") == "false");

                    if (!blnSuppressInit)
                        setBehaviour(objControl, sTag);
                    //window.setTimeout(objControl.id + ".className='" + sTag + "'", 10);

                    //make sure the control was initialized
                    /*while ((objControl.readyState != "complete") && (iLoopCount > 0)){
                       JSPause(5);
                       iLoopCount--;
                    }*/

                    //if (objControl.readyState != "complete") alert(objControl.id + " is not ready yet.");
                } catch (e) { }
            } else {
                //objControl.className = sTag.toLowerCase();
                setBehaviour(objControl, sTag);
                //make sure the control was initialized
                /*while ((objControl.readyState != "complete") && (iLoopCount > 0)){
                   JSPause(5);
                   iLoopCount--;
                }*/
                //objControl.value = iLoopMax - iLoopCount;
                //if (objControl.readyState != "complete") alert(objControl.id + " is not ready yet.");
            }
        } catch (e) { }
    }
}

function setBehaviour(objControl, strClassName) {
    if (objControl) {
        arrAPDControls.push(objControl);
        window.setTimeout(objControl.id + ".className='" + strClassName + "'", 50);
        //alert(objControl.id + ", -->" + strClassName + "<--");
    }
}

var iTest = 10;
function isPageReady() {
    //if (iTest > 0){
    //   alert(arrAPDControls.length);iTest--;
    //}
    for (var i = 0; i < arrAPDControls.length; i++) {
        if (arrAPDControls[i].readyState != "complete") {
            /*if (iCount > 0){
               alert(arrAPDControls[i].id);
            }*/
            return false;
        } else {
            try {
                if (arrAPDControls[i].ready == false || arrAPDControls[i].ready == undefined) {
                    /*if (iCount > 0){
                       alert(arrAPDControls[i].id);
                    }*/
                    return false;
                }
            } catch (e) { }
        }
    }
    return true;
}
var iCount = 10;
function checkPageReady() {
    if (iCount < 0) return;
    var bReady = isPageReady();
    if (bReady == true) {
        __blnPageInitialized = true;
        //document.body.fireEvent("onlayoutcomplete");
        if (typeof (__pageInit) == "function") {
            __pageInit();
        }
    } else {
        iCount--;
        window.setTimeout("checkPageReady()", 100);
        //alert("controls not ready");
    }
}

function getData(oDiv) {
    var strRet = "";
    if (typeof (oDiv) == "string")
        oDiv = document.all[oDiv];

    if (oDiv) {
        var aObj = oDiv.all;

        for (var i = aObj.length - 1; i >= 0; i--) {
            sTag = aObj[i].tagName.toLowerCase();
            if (sTag == "apdradiogroup" ||
                sTag == "apdcheckbox" ||
                sTag == "apdcustomselect" ||
                sTag == "apdinput" ||
                sTag == "apdinputcurrency" ||
                sTag == "apdinputdate" ||
                sTag == "apdinputnumeric" ||
                sTag == "apdinputphone" ||
                sTag == "apdtextarea" ||
                sTag == "input" ||
                sTag == "textarea") {
                sName = aObj[i].name;
                //alert(sName);
                if (sName.length > 0) {
                    switch (sTag) {
                        case "textarea":
                            strRet += sName + "=" + escape(aObj[i].innerText) + "&";
                            break;
                        case "apdinputphone":
                            strRet += sName + "AreaCode=" + aObj[i].areaCode + "&" +
                                      sName + "Exchange=" + aObj[i].phoneExchange + "&" +
                                      sName + "Number=" + aObj[i].phoneNumber + "&" +
                                      sName + "Extension=" + aObj[i].phoneExtension + "&" +
                                      sName + "AreaCodeNumber=" + aObj[i].areaCode + "&" +
                                      sName + "ExchangeNumber=" + aObj[i].phoneExchange + "&" +
                                      sName + "UnitNumber=" + aObj[i].phoneNumber + "&" +
                                      sName + "ExtensionNumber=" + aObj[i].phoneExtension + "&";
                            break;
                        default:
                            strRet += sName + "=" + escape((aObj[i].value != null ? aObj[i].value : "")) + "&";
                    }
                }
            }
        }
    }
    return strRet;
}

function getDataXML(oDiv, strRootNodeName, blnReturnAsObject, blnTypeAttribute, blnIncludeMask) {
    var objXML = document.createElement("XML");
    var objRoot;
    var strFriendlyName = "";

    if (strRootNodeName == "") strRootNodeName = "Root";
    objXML.loadXML("<" + strRootNodeName + "/>");

    if (objXML.parseError.errorCode != 0) {
        ClientWarning("Unable to create a XML document. Reason: " + objXML.parseError.reason);
        return "";
    }

    objRoot = objXML.selectSingleNode("/" + strRootNodeName);

    var strName = "";
    var strValue = "";
    if (typeof (oDiv) == "string")
        oDiv = document.all[oDiv];

    if (oDiv) {
        var aObj = oDiv.all;
        var blnPhone = false;
        var obj = null;
        var strSymbol = "";

        for (var i = aObj.length - 1; i >= 0; i--) {
            obj = null;
            sTag = aObj[i].tagName.toLowerCase();
            if (sTag == "apdradiogroup" ||
                sTag == "apdcheckbox" ||
                sTag == "apdcustomselect" ||
                sTag == "apdinput" ||
                sTag == "apdinputcurrency" ||
                sTag == "apdinputdate" ||
                sTag == "apdinputnumeric" ||
                sTag == "apdinputphone" ||
                sTag == "apdtextarea" ||
                sTag == "input" ||
                sTag == "textarea") {
                strName = aObj[i].name;
                if (strName == "") strName = aObj[i].name;
                //if (strName == "") strName = aObj[i].uniqueID;
                if (strName.length > 0) {
                    switch (sTag) {
                        case "textarea":
                            strValue = aObj[i].innerText;
                            break;
                        case "apdinputcurrency":
                            strSymbol = (aObj[i].symbol == null ? "$" : aObj[i].symbol);
                            if (aObj[i].value != "") {
                                if (aObj[i].value < 0)
                                    strValue = (blnIncludeMask == true ? strSymbol : "") + "(" + (aObj[i].value.replace(/[-]/g, "")) + ")";
                                else
                                    strValue = (blnIncludeMask == true ? strSymbol : "") + "" + aObj[i].value;
                            } else
                                strValue = "";
                            break;
                        case "apdinputphone":
                            strValue = aObj[i].value;
                            if (blnIncludeMask == true) {
                                if (strValue != "") {
                                    if (strValue.length == 10)
                                        strValue = "(" + strValue.substr(0, 3) + ") " + strValue.substr(3, 3) + " " + strValue.substr(6);
                                    else if (strValue.length > 10)
                                        strValue = "(" + strValue.substr(0, 3) + ") " + strValue.substr(3, 3) + " " + strValue.substr(6, 4) + " x " + strValue.substr(10);
                                }
                            }
                            break;
                        default:
                            strValue = (aObj[i].value != null ? aObj[i].value : "");
                    }
                    switch (sTag) {
                        case "apdcheckbox":
                            strFriendlyName = aObj[i].caption;
                            break;
                        default:
                            strFriendlyName = aObj[i].getAttribute("friendlyName");
                    }
                    if (blnTypeAttribute == true) {
                        objRoot.setAttribute(strName, strValue)
                        if (strFriendlyName && strFriendlyName != "") objRoot.setAttribute(strName + "__friendlyName", strFriendlyName);
                    } else {
                        obj = objXML.createElement(strName);
                        if (strFriendlyName && strFriendlyName != "") obj.setAttribute("friendlyName", strFriendlyName);
                        obj.appendChild(objXML.createCDATASection(strValue));
                        objRoot.appendChild(obj);
                    }
                }
            }
        }
    }
    if (blnReturnAsObject == true)
        return objXML;
    else
        return objXML.xml;
}

function resetControlBorders(oDiv) {
    var strRet = "";
    if (typeof (oDiv) == "string")
        oDiv = document.all[oDiv];

    if (oDiv) {
        var aObj = oDiv.all;

        for (var i = aObj.length - 1; i >= 0; i--) {
            sTag = aObj[i].tagName.toLowerCase();
            if (sTag == "apdradiogroup" ||
                sTag == "apdcheckbox" ||
                sTag == "apdcustomselect" ||
                sTag == "apdinput" ||
                sTag == "apdinputcurrency" ||
                sTag == "apdinputdate" ||
                sTag == "apdinputnumeric" ||
                sTag == "apdinputphone" ||
                sTag == "apdtextarea") {
                //if (sName.length > 0){
                aObj[i].contentSaved();
                //}
            }
        }
    }
}

//this stuff is to allow events to happen to paint the status
//bar during CRUD actions
function ShowSB40() {
    top.setSB(40, top.sb);
}
function ShowSB80() {
    top.setSB(80, top.sb);
}
function ShowSB100() {
    top.setSB(100, top.sb);
}

function XMLServerExecute(sASPPage, sXMLRequest) {
    if (sASPPage != "" && sASPPage.toLowerCase().indexOf(".asp") == -1) {
        ClientWarning("Invalid remote page specified"); // need to change this to ClientError.
        return;
    }
    var retObj = {
        code: null,
        xml: null
    }
    //alert(sASPPage + "\n" + sXMLRequest);
    //return;
    var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
    if (objXMLHTTP) {
        objXMLHTTP.open("POST", "/rs/" + sASPPage, false);
        objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        objXMLHTTP.send(sXMLRequest);

        //alert(objXMLHTTP.responseXML.xml);
        /*if (objXMLHTTP.responseXML.xml != ""){
          alert("XML:\n" + objXMLHTTP.responseXML);
          window.clipboardData.setData("Text", objXMLHTTP.responseXML.xml);
        } else {
          alert("Text:\n" + objXMLHTTP.responseText);
          window.clipboardData.setData("Text", objXMLHTTP.responseText);
        }*/
        try {
            if (objXMLHTTP.responseXML.xml != "") {
                var oRetXML = objXMLHTTP.responseXML;
                var oRetCode = oRetXML.documentElement.selectSingleNode("/Root/@returnCode");
                var oRoot = oRetXML.documentElement
                if (oRetCode && oRetCode.value != "0") {
                    var strUserInfo = "";
                    var aUserInfos = new Array();
                    var objUserInfo = {
                        code: oRetCode.value,
                        userText: oRoot.selectSingleNode("/Root/error/user").text,
                        advanced: oRoot.selectSingleNode("/Root/error/advanced").text
                    }

                    aUserInfos.push(objUserInfo);
                    //alert("User information count: " + aUserInfos.length);
                    var sURL = "/userMessage.htm";
                    var sFeatures = "dialogHeight:200px;dialogWidth:424px;center:yes;help:no;resizable:no;scroll:no;status:no"
                    window.showModalDialog(sURL, aUserInfos, sFeatures);
                    retObj.code = 1;
                } else {
                    retObj.code = 0;
                    retObj.xml = objXMLHTTP.responseXML;
                    retObj.text = objXMLHTTP.responseText;
                }
            } else {
                /*var oError = objXMLHTTP.responseXML.selectSingleNode("//Error");
                alert(objXMLHTTP.responseXML.xml);
                if (oError){
                   //there is an error in the system. The web page encapsulated the error message.
                   //Extract the message and display it to the user
                   alert(oError);
                   var strMessage = oError.text;
                   var iPosExtMsg = strMessage.indexOf("External Message");
                   var strMessage = strMessage.substring(iPosExtMsg + "External Message".length + 8, strMessage.indexOf("<|", iPosExtMsg));
                   ClientWarning(strMessage);
                   alert("here");
                   retObj.code = 1;
                   retObj.xml = objXMLHTTP.responseXML;
                   retObj.text = objXMLHTTP.responseText;
                } else {*/
                retObj.code = 0;

                try {
                    var oXML = new ActiveXObject("MSXML2.DOMDocument");
                    oXML.async = false;
                    oXML.loadXML(objXMLHTTP.responseText);

                    if (oXML.parseError.errorCode == 0) {
                        retObj.xml = oXML;
                    } else {
                        retObj.errorXML = oXML.parseError.reason;
                    }
                } catch (e) { }

                retObj.text = objXMLHTTP.responseText;
                //}
            }
        } catch (e) { alert(e.description) }
    }

    return retObj;
}

function showSaveAs(sFileName, sFilter) {
    try {
        var sFilePath = "";
        var oXL = new ActiveXObject("Excel.Application");
        if (oXL) {
            sFileName2 = oXL.GetSaveAsFilename(sFileName, sFilter, 0);
        } else {
            ClientError("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
            return;
        }
        if (sFileName2 == false) {
            return "";
        }

        sFilePath = sFileName2;
    } catch (e) {
        ClientError("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
        return "";
    } finally {
        if (oXL)
            oXL.Quit();
    }
    return sFilePath;
}

/*
  sURL  - fully qualified URL of the web file. ex: http://server/path/filename.ext
  sDest - local path where the file will be copied to.
  bOverwrite - Boolean indicating to overwrite the destination file, if exists.
*/
function CopyFileFromWeb(sURL, sDest, bOverwrite) {
    try {
        var oStream = new ActiveXObject("ADODB.Stream");
        var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP")
        if (xmlhttp && oStream) {
            xmlhttp.Open("GET", sURL, false);
            xmlhttp.Send();

            oStream.Open();
            oStream.Charset = "UTF-8";
            oStream.Type = 1;
            oStream.Write(xmlhttp.responseBody);

            if (bOverwrite == true)
                oStream.SaveToFile(sDest, 2);
            else
                oStream.SaveToFile(sDest, 1);
            oStream.Close();
        }
    } catch (e) {
        if (typeof (oStream) != "object")
            ClientError("MS ADO is not properly installed on clients computer. Please contact administrator");
        else
            ClientError(e.description);
    }
}

function docNoBkSpace() {
    if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))) { //alt+left arrow, alt+ right arrow
        event.returnValue = false;
        return;
    }
    if ((event.altKey) && (event.keyCode == 36)) {
        event.returnValue = false;
        return;
    }

    if (event.keyCode == 8) {
        var srcTagName;
        srcTagName = event.srcElement.tagName;
        switch (srcTagName) {
            case 'INPUT':
                if (event.srcElement.readOnly == true ||
          			event.srcElement.type == 'checkbox' ||
          			event.srcElement.type == 'radio' ||
          			event.srcElement.type == 'button')
                    event.returnValue = false;
                break;
            case 'TEXTAREA':
                if (event.srcElement.readOnly == true)
                    event.returnValue = false;
                break;
            default: event.returnValue = false;
        }
    }
}

//this will attach the docNoBkSpace function to the document which will alter all documents that include this script file.
document.onkeydown = docNoBkSpace;


//Finds the horizontal position of the obj in the window
function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft;
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;

    return curleft;
}

//Finds the vertical position of the obj in the window
function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop;
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;

    return curtop;
}

//Retrieves the browser window width and height
function getWindowSize(axis) {
    var myWidth = 0;
    var myHeight = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    }
    else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    }
    else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    if (axis == "width")
        return myWidth;
    else
        return myHeight;
}

//Retrieves the browser X and Y scroll
function getScrollXY(axis) {
    var scrOfX = 0;
    var scrOfY = 0;
    if (typeof (window.pageYOffset) == 'number') {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    }
    else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    }
    else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    if (axis == "x")
        return scrOfX;
    else
        return scrOfY;

    //return [ scrOfX, scrOfY ];
}

function JSPause(numberMillis) {
    //var dialogScript = 'window.setTimeout(' + ' function () { window.close(); }, ' + numberMillis + ');';
    //var result = window.showModalDialog('javascript:document.writeln(' +'"<script>' + dialogScript + '<' + '/script>")');
}

function AsyncXMLSave(sXMLRequest, fnCallbackSuccess, fnCallbackFailure) {
    var retObj = {
        code: null,
        xml: null
    }

    __objAsyncHTTP = new ActiveXObject("MSXML2.XMLHTTP");
    if (__objAsyncHTTP) {
        __objAsyncHTTP.open("POST", "/rs/APDSave.asp", true);
        __objAsyncHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        __objAsyncHTTP.onreadystatechange = handleAsyncXMLSaveReadyState;
        __fnAsyncHTTPCallbackSuccess = fnCallbackSuccess;
        __fnAsyncHTTPCallbackFailure = fnCallbackFailure;
        __objAsyncHTTP.send(sXMLRequest);
    }
    return retObj;
}

function handleAsyncXMLSaveReadyState() {
    if (__objAsyncHTTP) {
        if (__objAsyncHTTP.readyState == 4) {
            var retObj = handleAsyncXMLSaveResponse(__objAsyncHTTP);
            if (retObj && retObj.code == 0) {
                if (typeof (__fnAsyncHTTPCallbackSuccess) == "function")
                    __fnAsyncHTTPCallbackSuccess(retObj);
            } else {
                if (typeof (__fnAsyncHTTPCallbackFailure) == "function")
                    __fnAsyncHTTPCallbackFailure(retObj);
            }
            __objAsyncHTTP = null;
            __fnAsyncHTTPCallbackSuccess = null;
            __fnAsyncHTTPCallbackFailure = null;
        }
    } else {
        alert("__objAsyncHTTP is null");
    }
}

function handleAsyncXMLSaveResponse(objXMLHTTP) {
    var retObj = {
        code: null,
        xml: null
    }
    if (objXMLHTTP.responseXML.xml != "") {
        var oRetXML = objXMLHTTP.responseXML;
        var oRetCode = oRetXML.documentElement.selectSingleNode("/Root/@returnCode");
        if (oRetCode && oRetCode.value != "0") {
            var strUserInfo = "";
            var aUserInfos = new Array();
            var oErrorNodes = oRetXML.documentElement.selectNodes("/Root/StoredProc[@returnCode != 0]");
            if (oErrorNodes.length > 0) {
                for (var i = 0; i < oErrorNodes.length; i++) {
                    var objUserInfo = {
                        code: oErrorNodes[i].selectSingleNode("@returnCode").value,
                        userText: oErrorNodes[i].selectSingleNode("error/user").text,
                        advanced: oErrorNodes[i].selectSingleNode("error/advanced").text
                    }

                    aUserInfos.push(objUserInfo);
                }
            } else {
                var oErrorNode = oRetXML.documentElement.selectSingleNode("/Root/error");
                if (oErrorNode) {
                    //strUserInfo = oErrorNode.text;
                    var objUserInfo = {
                        code: 1,
                        userText: oErrorNodes[i].selectSingleNode("user").text,
                        advanced: oErrorNodes[i].selectSingleNode("advanced").text
                    }

                    aUserInfos.push(objUserInfo);
                }
            }
            //alert("User information count: " + aUserInfos.length);
            var sURL = "/userMessage.htm";
            var sFeatures = "dialogHeight:200px;dialogWidth:424px;center:yes;help:no;resizable:no;scroll:no;status:no"
            window.showModalDialog(sURL, aUserInfos, sFeatures);
            retObj.code = 1;
        } else {
            retObj.code = 0;
            retObj.xml = objXMLHTTP.responseXML;
        }
    }

    return retObj;
}
//Common logging functionality
function logEvent(sEventType, sEventStatus, sEventDescription, sEventDetailedDescription, sEventXML) {
    // debugger;
    var sProc, sRequest;
    try {
        sRequest = "EventType=" + escape(sEventType) +
                    "&EventStatus=" + escape(sEventStatus) +
                    "&EventDescription=" + escape(sEventDescription) +
                    "&EventDetailedDescription=" + escape(sEventDetailedDescription) +
                    "&EventXML=" + escape(sEventXML) +
                    "&SysLastUserID=" + vUserID;

        sProc = "uspApdEventlogInsDetail";


        var aRequests = new Array();
        aRequests.push({
            procName: sProc,
            method: "executespnp",
            data: sRequest
        });
        var objRet = XMLSave(makeXMLSaveString(aRequests));
    }
    catch (e) {
    }
}