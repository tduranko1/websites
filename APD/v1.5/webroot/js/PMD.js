String.prototype.Trim = trim_string;

function trim_string() {
   var ichar, icount;
   var strValue = this;
   ichar = strValue.length - 1;
   icount = -1;
   while (strValue.charAt(ichar)==' ' && ichar > icount)
      --ichar;
   if (ichar!=(strValue.length-1))
      strValue = strValue.slice(0,ichar+1);
   ichar = 0;
   icount = strValue.length - 1;
   while (strValue.charAt(ichar)==' ' && ichar < icount)
      ++ichar;
   if (ichar!=0)
      strValue = strValue.slice(ichar,strValue.length);
   return strValue;
}

//Auto-resize table layout
function resizeScrollTable(oElement)  
{
  var head = oElement.firstChild;
  var body = oElement.lastChild;
  var scrollBarWidth = body.offsetWidth - body.clientWidth;
  var headCells = head.firstChild.rows[0].cells;
  
  headCells[headCells.length - 1].style.width = scrollBarWidth;
}

function YesNoMessage(sTitle, sMessage)
{
	var strReturn = sTitle + "|" + sMessage;
	var iDialogHeight;

	// retrieve optional argument which specifies Dialog Height or default to 125px.
	iDialogHeight = arguments.length > 2 ? arguments[2] : 125;

	strReturn = window.showModalDialog("/js/YesNoDialog.htm",strReturn,"dialogHeight: " + iDialogHeight + "px; dialogWidth: 450px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
	return strReturn;
}

//Takes a string and removes the leading and trailing spaces and also any extra spaces in the string. IE only.
function remove_XS_whitespace(item)
{
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
  for (index = 0; index < item_length; index++)
  {
    if (item.value.charAt(index) != ' ')
    {
      tmp += item.value.charAt(index);
    }
    else
    {
      if (tmp.length > 0)
      {
        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1)
        {
          tmp += item.value.charAt(index);
        }
      }
    }
  }
  item.value = tmp;
}

var saveBG;
function GridMouseOver(oObject)
{
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
	oObject.style.cursor='hand';
}

function GridMouseOut(oObject)
{
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
}  


//init the table selections, must be last
function callListSearch()
{
  parent.document.frames["ifrmPMDShopSearch"].frameElement.src = "PMDCycleTime.asp?ShopLocationID="+gsShopLocationID+"&BeginDate="+txtBeginDate.value+"&EndDate="+txtEndDate.value+"&AssignmentCode="+txtAssignmentCode.value;
}


function callRefresh(){
  window.navigate("PMDShopSearch.asp");
}

  
// Displays external and internal event messages in a popup window.
// This version is called for errors occurring server side.
function ServerEvent()
{
    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "/EventPopup.asp";

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

// Displays external and internal event messages in a popup window.
// This version is called for errors occurring client side.
function ClientEvent( nNumber, sDescription, sSource )
{
	if ( ( String( sSource ) == "undefined" ) || ( sSource == null ) )
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "/EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

function ClientError( sDescription ) { ClientEvent( 0, sDescription ); }
function ClientWarning( sDescription ) { ClientEvent( 1, sDescription ); }
function ClientInfo( sDescription ) { ClientEvent( 2, sDescription ); }


function LeapYear(intYear) {
    if ( (intYear % 100 == 0) && ( ( (intYear % 400 == 0) || ((intYear % 4) == 0) ) ) )
        return true;
    return false;
}

function chkdate(objName){
	var strDatestyle = "US"; //United States date style
    //var strDatestyle = "EU";  //European date style
    var strDate;
    var strDateArray;
    var strDay;
    var strMonth;
    var strYear;
    var intday;
    var intMonth;
    var intYear;
    var booFound = false;
    var datefield = objName;
    var strSeparatorArray = new Array("-"," ","/",".");
    var strSeparator = "/";
    var intElementNr;
    var err = 0;
    var strMonthArray = new Array(12);

    strMonthArray[0] = "Jan";
    strMonthArray[1] = "Feb";
    strMonthArray[2] = "Mar";
    strMonthArray[3] = "Apr";
    strMonthArray[4] = "May";
    strMonthArray[5] = "Jun";
    strMonthArray[6] = "Jul";
    strMonthArray[7] = "Aug";
    strMonthArray[8] = "Sep";
    strMonthArray[9] = "Oct";
    strMonthArray[10] = "Nov";
    strMonthArray[11] = "Dec";

    strDate = datefield.value;
    if (strDate.length < 1)
        return true;

	  for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
        if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
            strDateArray = strDate.split(strSeparatorArray[intElementNr]);
            if (strDateArray.length != 3) {
                err = 1;
                return false;
            } else {
                strDay = strDateArray[0];
                strMonth = strDateArray[1];
                strYear = strDateArray[2];
            }
            booFound = true;
        }
    }

    if (booFound == false) {
        if (strDate.length>5) {
            strDay = strDate.substr(0, 2);
            strMonth = strDate.substr(2, 2);
            strYear = strDate.substr(4);
        }
    }

		if (strYear.length > 4){ err = 4; return false;}

    if (strYear.length == 2) {
        strYear = '20' + strYear;
    }

    // US style
    if (strDatestyle == "US") {
        strTemp = strDay;
        strDay = strMonth;
        strMonth = strTemp;
    }
    intday = parseInt(strDay, 10);
    if (isNaN(intday)) {
        err = 2;
        return false;
    }
    intMonth = parseInt(strMonth, 10);
    if (isNaN(intMonth)) {
        for (i = 0;i<12;i++) {
            if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
                intMonth = i+1;
                strMonth = strMonthArray[i];
                i = 12;
            }
        }
        if (isNaN(intMonth)) {
            err = 3;
            return false;
        }
    }
    intYear = parseInt(strYear, 10);
    if (isNaN(intYear)) {
        err = 4;
        return false;
    }

		if (intYear < 1900){
        err = 4;
        return false;
    }

    if (intMonth>12 || intMonth<1) {
        err = 5;
        return false;
    }
    if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
        err = 6;
        return false;
    }
    if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
        err = 7;
        return false;
    }
    if (intMonth == 2) {
        if (intday < 1) {
            err = 8;
            return false;
        }
        if (LeapYear(intYear) == true) {
            if (intday > 29) {
                err = 9;
                return false;
            }
        }
        else {
            if (intday > 28) {
                err = 10;
                return false;
            }
        }
    }

    // Add leading zeros for month and/or day to
    // keep consistant with msjs-client-library.js.
    strDate = ( (intMonth<10) ? "0" + intMonth : intMonth ) + strSeparator;
    strDate += ( (intday<10) ? "0" + intday : intday ) + strSeparator + strYear;
    datefield.value = strDate;

    return true;
}

function GetUTCDate(){
	var s = "";
	var d = new Date();

	/* Get the full 4-digit year from the date object*/
	yyyy = d.getUTCFullYear();

	/*Get the month and be sure it is zero filled if need be*/
	mm =(d.getUTCMonth() + 1)
	if (mm < 10)
	{
		mm = '0' + mm
	}

	/*Get the day of the month and be sure it is zero filled if need be*/
	dd =(d.getUTCDate())
	if (dd < 10)
	{
		dd = '0' + dd
	}

	/* Now get the hours and be sure they are zero filled as needed*/
	hh = d.getHours();
	if (hh < 10)
	{
		hh = '0' + hh
	}

	/* Get the minutes and be sure they are zero filled as needed*/
	min = d.getMinutes();
	if (min < 10)
	{
		min = '0' + min
	}

	/* Get the seconds and be sure they are zero filled as needed*/
	ss = d.getSeconds();
	if (ss < 10)
	{
		ss = '0' + ss
	}

	/* Get the milliseconds and be sure they are zero filled as needed*/
	ms = d.getMilliseconds();
	if (ms < 10)
	{
		ms = '0' + ms
	}

	s += yyyy + '-' + mm + '-' + dd + 'T' + hh + ':' + min + ':' + ss + '.' + ms;
	return(s);
}
