var saveBG;
function GridMouseOver(oObject, crsrChange)
{
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
  if (crsrChange != 0)
  	oObject.style.cursor='hand';
}

function GridMouseOut(oObject)
{
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
} 

var lastSelRow;
var lastSelCellHTML;
function setGridSelectFont(oObject)
{
  if (lastSelRow != undefined)
  {
    lastSelRow.cells[1].style.fontWeight = "normal";
    lastSelRow.cells[1].innerHTML = lastSelCellHTML;
  }
  lastSelRow = oObject;
  lastSelCellHTML = oObject.cells[1].innerHTML;
  oObject.cells[1].style.fontWeight = "bold";
  oObject.cells[1].innerHTML = "<img src='/images/gridArrowL.gif' width='7' height='6' border='0'>" + oObject.cells[1].innerText + "<img src='/images/gridArrowR.gif' width='7' height='6' border='0'>";
}
