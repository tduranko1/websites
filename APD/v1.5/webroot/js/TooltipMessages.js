	messages= new Array()
	
	// Write your descriptions in here.
	
	messages[0]="Enter the Full Name";
	messages[1]="Enter a physical Address - PO Box is not allowed here";
	messages[2]="Enter an address -- PO Box or Rte # is allowed here";
	messages[3]="Enter the City";
	messages[4]="Select a State, Territory or Province from the list";
	messages[5]="Enter a numeric value for the PHONE Area Code, Exchange, Number and the Ext.: ex. 941 479 5720 12345";
	messages[6]="Enter a numeric value for the CELL Area Code, Exchange, Number and the Ext.: ex. 941 479 5720 12345";
	messages[7]="Enter a numeric value for the FAX Area Code, Exchange, Number and the Ext.: ex. 941 479 5720 12345";
	messages[8]="Enter a numeric value for the PAGER Area Code, Exchange, Number and the Ext.: ex. 941 479 5720 12345";
	messages[9]="Select a Preferred Contact Method";
	
	messages[500]="Check this box if the Electronic Funds Transfer account has been signed";
	messages[501]="Enter the Electronic Funds Transfer account number";
	messages[502]="Select the Electronic Funds Transfer account type";
	messages[503]="Enter the Electronic Funds Transfer account bank routing number";
	messages[504]="Enter the Electronic Funds Transfer account effective date -- (MM/DD/YYYY)";

	if (document.attachEvent)
		document.attachEvent("onclick", top.hideAllMenuScriptlets);
	else
		document.onclick = top.hideAllMenuScriptlets;
