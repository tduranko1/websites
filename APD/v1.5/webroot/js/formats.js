//round to 2 decimal places
function formatCurrency(dValue){
  var dRet = 0;
  if (isNaN(dValue) == false && dValue > 0){
      dRet = Math.round(dValue * 10000) / 10000;
  }
  return dRet;
}

function formatNumberPadded(sVal, sPadChar, iLen){
   if (sPadChar == "") sPadChar = "0";
   if (sVal != ""){
      if (typeof(sVal) != "string")
         sVal = sVal.toString();
      
      var sFormat = charFill(sPadChar, iLen);
      return formatNumber(sVal, sFormat);
   } else
   	return "";
}

function formatNumber(sVal, sFormat){
   if (sFormat != ""){
      if (typeof(sVal) != "string")
         sVal = sVal.toString();
         
      var sValLength = sVal.length;
      var sFormatLength = sFormat.length;
      var sRet = sFormat;
      
      if (sFormatLength > sValLength)
         sRet = sFormat.substr(0, sFormatLength - sValLength) + sVal;
      else
         sRet = sVal;
      
      return sRet;
      
   } else 
      return sVal;
}

function charFill(sChar, iLen){
   var sRet = "";
   if (isNaN(iLen)) iLen = 0;
   
   for (var i = iLen; i > 0; i--)
      sRet += sChar;
   
   return sRet;
}

function formatCurrencyString(sVal){
   strRet = "";
   sVal = String(sVal);
   if (isNaN(sVal) == false && sVal != ""){
      sVal = String(Math.round(parseFloat(sVal) * 1000) / 1000);
      if (sVal.indexOf(".") == -1) {
         strRet = sVal + ".00";
      } else {
         var strParts = sVal.split(".");
         strRet = strParts[0];
         if (strParts.length > 1) {
            var strCents = strParts[1].substr(0, 4);
            try {
            if (strCents.length < 4){
               strCents += charFill("0", 4 - strCents.length);
            }
            strCents = Math.round(parseFloat(strCents)/100) + "";
            strCents = strCents + charFill("0", 2 - strCents.length);
            } catch (e) {alert(e.description)}
            strRet += "." + strCents;
         }
      }
   }
   return strRet;
}

function formatPhone(sVal){
   var strRet = "";
   if (typeof(sVal) == "string"){
      switch (sVal.length){
         case 10: strRet = "(" + sVal.substr(0, 3) + ") " + sVal.substr(3, 3) + "-" + sVal.substr(6, 4); break;
         case 7: strRet = sVal.substr(0, 3) + "-" + sVal.substr(3, 4); break;
         default: strRet = sVal;
      }
      if (sVal.length > 10) strRet = "(" + sVal.substr(0, 3) + ") " + sVal.substr(3, 3) + "-" + sVal.substr(6, 4) + "x" + sVal.substr(10);
   }
   return strRet;
}