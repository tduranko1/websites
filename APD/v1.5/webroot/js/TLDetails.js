﻿var strClaimAspectServiceChannelID = "";
var strUserID = "";
var strSCSatus = "";
var xmlTaskDetail = null;
var blnTLEdit = false;
var blnLoading = false;
var strLienHolderID = "";
var strParentLienHolderID = "";
var strSalvageVendorID = "";
var strChildLienHolderID = "";
var strNewParentLienHolderID = "";
var blnHasLH = false;
var blnSupervisor = false;
var gblnValidZip = true;        //used for zip...
var gblnResetLH = false;            // ...code validation

var aTLDeliverablesWithLH = [{ "Name": "Letter Of Guarantee", "ID": "57" },
                                { "Name": "Title", "ID": "54" },
                                { "Name": "Title Copy", "ID": "56" },
                                { "Name": "Lien Holder POA", "ID": "58" },
                                { "Name": "Vehicle Owner POA", "ID": "55"}];
var aTLDeliverablesWithoutLH = [{ "Name": "Vehicle Owner POA", "ID": "55" },
                                   { "Name": "Title", "ID": "54"}];

//alert(top.blnSupervisorFlag);

$.ready(initPage());

function initPage() {
   try {
      if (parent.strCurrentClaimAspectServiceChannelID)
         strClaimAspectServiceChannelID = parent.strCurrentClaimAspectServiceChannelID;
      if (parent.gsUserID)
         strUserID = parent.gsUserID;

      if (top.blnSupervisorFlag)
         blnSupervisor = top.blnSupervisorFlag == "1";
   } catch (e) { }

   if (strClaimAspectServiceChannelID != "") {
      $.get('tldetails.asp?ClaimAspectServiceChannelID=' + strClaimAspectServiceChannelID, function(dataRet) {
         xmlTaskDetail = dataRet;
         strSCSatus = $(xmlTaskDetail).find("Root").attr("ServiceChannelStatus");
         btnShowStatus.CCDisabled = false;
         btnShowSettlement.CCDisabled = false;
         blnHasLH = parseInt($(xmlTaskDetail).find("LienHolder").attr("LienHolderID"), 10) > 0;
         //btnShowLH.CCDisabled = !blnHasLH;
         btnShowLH.CCDisabled = false;
         btnShowSalvage.CCDisabled = false;
         btnShowTitle.CCDisabled = false;

         if (blnHasLH) {
            showLHDeliverables();
         } else {
            //populate the non-LH deliverables
            for (var i = 0; i < aTLDeliverablesWithoutLH.length; i++) {
               $("#divDeliverableName" + i).text(aTLDeliverablesWithoutLH[i].Name);
               var strDeliverableStatus = getDeliverableStatus(aTLDeliverablesWithoutLH[i].ID);
               switch (strDeliverableStatus) {
                  case "unknown":
                     $("#imgDelieverable" + i).attr("src", "images/content_noaction_red.gif");
                     $("#divDeliverableStatus" + i).text("Pending");
                     break;
                  default:
                     $("#imgDelieverable" + i).attr("src", "images/content_action_green.gif");
                     $("#divDeliverableStatus" + i).text("Rcvd " + strDeliverableStatus);
               }

            }
         }


         showStatus();

         if (parent.stat1) {
            parent.stat1.Hide();
         }
      });
   }

}

function showLHDeliverables() {
   //populate the LH deliverables
   for (var i = 0; i < aTLDeliverablesWithLH.length; i++) {
      $("#divDeliverableName" + i).text(aTLDeliverablesWithLH[i].Name);
      var strDeliverableStatus = getDeliverableStatus(aTLDeliverablesWithLH[i].ID);
      switch (strDeliverableStatus) {
         case "unknown":
            $("#imgDelieverable" + i).attr("src", "images/content_noaction_red.gif");
            $("#divDeliverableStatus" + i).text("Pending");
            break;
         default:
            $("#imgDelieverable" + i).attr("src", "images/content_action_green.gif");
            $("#divDeliverableStatus" + i).text("Rcvd " + strDeliverableStatus);
      }

   }

}

function getDeliverableStatus(strDocumentTypeID) {
   var obj = xmlTaskDetail.selectSingleNode("//Document[@DocumentTypeID='" + strDocumentTypeID + "']");
   if (obj) {
      return obj.getAttribute("CreatedDate");
   } else { return "unknown"; }
}

function hideAllPopup() {
   $("#divStatus").removeClass().addClass("hidden");
   $("#divSettlement").removeClass().addClass("hidden");
   $("#divLienHolder").removeClass().addClass("hidden");
   $("#divSalvage").removeClass().addClass("hidden");
   $("#divTitle").removeClass().addClass("hidden");
}

function showStatus(strFrom) {
   hideAllPopup();
   switch (strFrom) {
      case "Settlement":
         //reset Settlement Data
         break;
     case "LienHolder":
         //reset Lien Holder Data
         resetLienHolder();
         if (strLienHolderID == -1) strLienHolderID = "";
         chkSameAsGeneral.value = 0;
         gblnResetLH = false;           //reset ResetLH value; used for zip code validation
         break;
      case "Salvage":
         //reset Salvage Data
         chkSameAsGeneralSalvage.value = 0;
         break;
   }
   $("#divCurStatus").text("Not Implemented.");
   $("#divPOStatus").text(($(xmlTaskDetail).find("TL").attr("PayoffAmountConfirmedFlag") == "1" ? "Confirmed " + $(xmlTaskDetail).find("TL").attr("PayoffAmountConfirmedDate") : "Not confirmed."));
   $("#divTitleStatus").text(($(xmlTaskDetail).find("Title").attr("TitleNameConfirmFlag") == "1" ? "Confirmed " + $(xmlTaskDetail).find("Title").attr("TitleNameConfirmDate") : "Not confirmed."));
   $("#divStatus").removeClass().addClass("visible");
   blnTLEdit = false;
}

function showSettlement() {
   hideAllPopup();
   if (xmlTaskDetail) {
      var xmlSettlement = $(xmlTaskDetail).find("TL");
      if (xmlSettlement) {
         blnTLEdit = false;
         disableSettlement(false);
         txtSettlementAmount.value = xmlSettlement.attr("SettlementAmount");
         txtPayoffAmount.value = xmlSettlement.attr("PayoffAmount");
         txtAdvanceAmount.value = xmlSettlement.attr("AdvanceAmount");
         txtLoGAmount.value = xmlSettlement.attr("LetterofGauranteeAmount");
         txtPayoffExpDt.value = xmlSettlement.attr("PayoffExpirationDate");
         txtLoanAccountNumber.value = xmlSettlement.attr("LienHolderAccountNumber");
         var blnDisabled = (strSCSatus != "Active" || xmlSettlement.attr("PayoffAmountConfirmedFlag") == "1")
         if (xmlSettlement.attr("PayoffAmountConfirmedFlag") == "1") {
            $("#spnSettlementConfirm").text(" Confirmed on " + xmlSettlement.attr("PayoffAmountConfirmedDate"));
         }
         if (blnSupervisor == true) blnDisabled = false;
         disableSettlement(blnDisabled);
         if (blnDisabled == false) txtSettlementAmount.setFocus();
         if (btnShowLH.CCDisabled == true) {
            txtPayoffAmount.CCDisabled = true;
            txtLoGAmount.CCDisabled = true;
            txtPayoffExpDt.CCDisabled = true;
         }
         blnTLEdit = true;
      }

      $("#divSettlement").removeClass().addClass("visible");
   }
}

function disableSettlement(blnDisabled) {
   disableControls(divSettlement.getElementsByTagName("APDInputCurrency"), blnDisabled);
   disableControls(divSettlement.getElementsByTagName("APDInputDate"), blnDisabled);
   disableControls(divSettlement.getElementsByTagName("APDButton"), blnDisabled);
   disableControls(divSettlement.getElementsByTagName("APDInput"), blnDisabled);
}

function showLienHolder() {
   hideAllPopup();
   $("#divLienHolder").removeClass().addClass("visible");
   disableLienHolder(false);
   blnLoading = true;
   if (blnHasLH == true || strLienHolderID == -1) {
      if (xmlTaskDetail) {
         //alert(xmlTaskDetail.xml);
         var xmlLH = $(xmlTaskDetail).find("LienHolder");
         if (xmlLH) {
            strLienHolderID = xmlLH.attr("LienHolderID");
            strParentLienHolderID = xmlLH.attr("ParentLienHolderID");
            txtLHName.value = xmlLH.attr("Name");
            txtLHOfficeName.value = xmlLH.attr("OfficeName");
            txtLHAddress1.value = xmlLH.attr("Address1");
            txtLHAddress2.value = xmlLH.attr("Address2");
            txtLHAddressCity.value = xmlLH.attr("AddressCity");
            txtLHAddressState.value = xmlLH.attr("AddressState");
            txtLHAddressZip.value = xmlLH.attr("AddressZip");
            txtLHAddressZip.contentSaved();
            txtLHPhone.value = xmlLH.attr("Phone");
            txtLHPhone.contentSaved();
            txtLHFax.value = xmlLH.attr("Fax");
            txtLHContactName.value = xmlLH.attr("ContactName");
            txtLHEmailAddress.value = xmlLH.attr("EmailAddress");
            txtLHFederalTaxID.value = xmlLH.attr("FedTaxID");
            txtLHFederalTaxID.contentSaved();
            selLHBusinessType.value = xmlLH.attr("BusinessTypeCD");

            txtLHBillingName.value = xmlLH.attr("BillingName");
            txtLHBillingAddress1.value = xmlLH.attr("BillingAddress1");
            txtLHBillingAddress2.value = xmlLH.attr("BillingAddress2");
            txtLHBillingAddressCity.value = xmlLH.attr("BillingAddressCity");
            txtLHBillingAddressState.value = xmlLH.attr("BillingAddressState");
            txtLHBillingAddressZip.value = xmlLH.attr("BillingAddressZip");
            txtLHBillingPhone.value = xmlLH.attr("BillingPhone");
            txtLHBillingFax.value = xmlLH.attr("BillingFax");
            txtLHEFTAccountNumber.value = xmlLH.attr("EFTAccountNumber");
            txtLHEFTRoutingNumber.value = xmlLH.attr("EFTRoutingNumber");
            selEFTAccountType.value = xmlLH.attr("EFTAccountTypeCD");
            txtLHEFTEffectiveDate.value = xmlLH.attr("EFTEffectiveDate");
            chkEFTContractSigned.value = xmlLH.attr("EFTContractSignedFlag");
         }
      }
   } else {
      var strRet = YesNoMessage("Confirm", "There is no Lien Holder associated with this claim.\n\nDo you want to add a Lien Holder?");
      if (strRet == "Yes") {
         strLienHolderID = -1;
         resetLienHolder();
      } else {
         showStatus();
         return;
      }
   }
   var blnDisabled = (strSCSatus != "Active");
   disableLienHolder(blnDisabled);
   if (chkEFTContractSigned.value == 1) {
      chkEFTContractSigned.CCDisabled = true;
      disableEFT();
   }
   showLHGeneral();
   blnLoading = false;
}

function resetLienHolder() {

   gblnResetLH = true;      //reset values, no need to validate zip

   strParentLienHolderID = "";
   txtLHName.value = "";
   txtLHOfficeName.value = "";
   txtLHAddress1.value = "";
   txtLHAddress2.value = "";
   txtLHAddressCity.value = "";
   txtLHAddressState.value = "";
   txtLHAddressZip.value = "";
   txtLHAddressZip.contentSaved();
   txtLHPhone.value = "";
   txtLHPhone.contentSaved();
   txtLHFax.value = "";
   txtLHContactName.value = "";
   txtLHEmailAddress.value = "";
   txtLHFederalTaxID.value = "";
   txtLHFederalTaxID.contentSaved();
   selLHBusinessType.value = "";

   txtLHBillingName.value = "";
   txtLHBillingAddress1.value = "";
   txtLHBillingAddress2.value = "";
   txtLHBillingAddressCity.value = "";
   txtLHBillingAddressState.value = "";
   txtLHBillingAddressZip.value = "";
   txtLHBillingPhone.value = "";
   txtLHBillingFax.value = "";
   txtLHEFTAccountNumber.value = "";
   txtLHEFTRoutingNumber.value = "";
   selEFTAccountType.value = "";
   txtLHEFTEffectiveDate.value = "";
   chkEFTContractSigned.value = "";
}

function disableLienHolder(blnDisabled) {
   disableControls(divLHGeneral.getElementsByTagName("APDInput"), blnDisabled);
   disableControls(divLHGeneral.getElementsByTagName("APDCustomSelect"), blnDisabled);
   disableControls(divLHGeneral.getElementsByTagName("APDButton"), blnDisabled);
   disableControls(divLHBilling.getElementsByTagName("APDInput"), blnDisabled);
   disableControls(divLHBilling.getElementsByTagName("APDInputDate"), blnDisabled);
   disableControls(divLHBilling.getElementsByTagName("APDCustomSelect"), blnDisabled);
   disableControls(divLHBilling.getElementsByTagName("APDButton"), blnDisabled);
}

function disableEFT() {
   var blnDisabled = (chkEFTContractSigned.value == "1");
   txtLHEFTAccountNumber.CCDisabled = blnDisabled;
   txtLHEFTRoutingNumber.CCDisabled = blnDisabled;
   selEFTAccountType.CCDisabled = blnDisabled;
   txtLHEFTEffectiveDate.CCDisabled = blnDisabled;
}

function disableControls(controls, blnDisabled) {
   for (var i = 0; i < controls.length; i++) {
      controls[i].CCDisabled = blnDisabled;
   }
}

function showSalvage() {
   hideAllPopup();
   if (xmlTaskDetail) {
      var xmlSalvage = $(xmlTaskDetail).find("Salvage");
      if (xmlSalvage) {
         var xmlTL = $(xmlTaskDetail).find("TL");
         if (xmlTL) {
            txtSalvageControlNumber.value = xmlTL.attr("SalvageControlNumber");
         }
         strSalvageVendorID = xmlSalvage.attr("SalvageVendorID");
         txtSalvageName.value = xmlSalvage.attr("Name");
         txtSalvageOfficeName.value = xmlSalvage.attr("OfficeName");
         txtSalvageAddress1.value = xmlSalvage.attr("Address1");
         txtSalvageAddress2.value = xmlSalvage.attr("Address2");
         txtSalvageAddressCity.value = xmlSalvage.attr("AddressCity");
         txtSalvageAddressState.value = xmlSalvage.attr("AddressState");
         txtSalvageAddressZip.value = xmlSalvage.attr("AddressZip");
         txtSalvagePhone.value = xmlSalvage.attr("Phone");
         txtSalvageFax.value = xmlSalvage.attr("Fax");
         txtSalvageContactName.value = xmlSalvage.attr("ContactName");
         txtSalvageEmailAddress.value = xmlSalvage.attr("EmailAddress");
         txtSalvageWebsite.value = xmlSalvage.attr("WebSiteAddress");

         txtSalvageMailingAddress1.value = xmlSalvage.attr("MailingAddress1");
         txtSalvageMailingAddress2.value = xmlSalvage.attr("MailingAddress2");
         txtSalvageMailingAddressCity.value = xmlSalvage.attr("MailingAddressCity");
         txtSalvageMailingAddressState.value = xmlSalvage.attr("MailingAddressState");
         txtSalvageMailingAddressZip.value = xmlSalvage.attr("MailingAddressZip");
      }
   }

   $("#divSalvage").removeClass().addClass("visible");
   showSalvageGeneral();
}

function updateLoG() {
   if (blnTLEdit == false) return;
   if (txtSettlementAmount.value != "" && txtPayoffAmount.value != "") {
      var settlementAmount = parseFloat(txtSettlementAmount.value);
      var payoffAmount = parseFloat(txtPayoffAmount.value);
      txtLoGAmount.value = (settlementAmount < payoffAmount ? txtSettlementAmount.value : txtPayoffAmount.value);
   }
}

function saveSettlement(strFrom) {
   //validate data
   if (blnHasLH) {
      if (txtPayoffAmount.value == "" || txtPayoffExpDt.value == "") {
         ClientWarning("Payoff Amount and Expiration date is required when a Lien Holder is involved.");
         return;
      }
   }

   //save settlement data
   var sProc = "uspTLDataUpdate";
   sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                    "&SettlementAmount=" + txtSettlementAmount.value +
                    "&PayoffAmount=" + txtPayoffAmount.value +
                    "&PayoffExpirationDate=" + txtPayoffExpDt.value +
                    "&AdvanceAmount=" + txtAdvanceAmount.value +
                    "&LetterOfGuaranteeAmount=" + txtLoGAmount.value +
                    "&LienHolderAccountNumber=" + txtLoanAccountNumber.value +
                    "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   //alert(sXMLRequest); return;
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         //save the changes to the internal
         var xmlSettlement = $(xmlTaskDetail).find("TL");
         if (xmlSettlement) {
            xmlSettlement.attr("SettlementAmount", txtSettlementAmount.value);
            xmlSettlement.attr("PayoffAmount", txtPayoffAmount.value);
            xmlSettlement.attr("AdvanceAmount", txtAdvanceAmount.value);
            xmlSettlement.attr("LetterofGauranteeAmount", txtLoGAmount.value);
            xmlSettlement.attr("PayoffExpirationDate", txtPayoffExpDt.value);
            xmlSettlement.attr("LienHolderAccountNumber", txtLoanAccountNumber.value);
         }

         if (strFrom != "confirm") {
            //go back to status page
            showStatus();
         }
         return true;
      } else
         return false;
   }
   return false;
}

function confirmSettlement() {
   var xmlSettlement = $(xmlTaskDetail).find("TL");
   if (xmlSettlement) {
      if (xmlSettlement.attr("SettlementAmount") != txtSettlementAmount.value ||
                xmlSettlement.attr("PayoffAmount") != txtPayoffAmount.value ||
                xmlSettlement.attr("AdvanceAmount") != txtAdvanceAmount.value ||
                xmlSettlement.attr("LetterofGauranteeAmount") != txtLoGAmount.value ||
                xmlSettlement.attr("PayoffExpirationDate") != txtPayoffExpDt.value ||
                xmlSettlement.attr("LienHolderAccountNumber") != txtLoanAccountNumber.value) {
         //data was changed. save them
         if (saveSettlement("confirm") == false) return;
      }

   }

   if (blnHasLH) {
      if (txtPayoffAmount.value == "" || txtPayoffExpDt.value == "") {
         ClientWarning("Payoff Amount and Expiration date is required when a Lien Holder is involved.");
         return;
      }
   }

   var sProc = "uspTLConfirmData";
   sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                    "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });

   var sXMLRequest = makeXMLSaveString(aRequests);
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         var xmlSettlement = $(xmlTaskDetail).find("TL");
         if (xmlSettlement) {
            var dt = new Date();
            var strDate = formatDateTime(dt.toLocaleString());
            xmlSettlement.attr("PayoffAmountConfirmedFlag", 1);
            xmlSettlement.attr("PayoffAmountConfirmedDate", strDate);
            btnSettlementConfirm.CCDisabled = true;
            $("#spnSettlementConfirm").text("Confirmed on " + strDate);
            showStatus();
         }
      }
   }
}

function showLHGeneral() {
   $("#divLHBilling").removeClass().addClass("hidden");
   $("#divLHGeneral").removeClass().addClass("visible");
}

function showLHBilling() {
   $("#divLHBilling").removeClass().addClass("visible");
   $("#divLHGeneral").removeClass().addClass("hidden");
}

function checkLienHolder() {

   if (blnLoading == true) return;
   var sProc = "uspFindLienHolderByZipPhone";
   var strPhone = txtLHPhone.value;

   if (txtLHAddressZip.value == "" || txtLHPhone.value == "") return;

   sRequest = "ZipCode=" + txtLHAddressZip.value +
                    "&PhoneAreaCode=" + txtLHPhone.areaCode +
                    "&PhoneExchangeNumber=" + txtLHPhone.phoneExchange +
                    "&PhoneUnitNumber=" + txtLHPhone.phoneNumber;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNpAsXML",
      data: sRequest
   });

   var sXMLRequest = makeXMLSaveString(aRequests);
   AsyncXMLSave(sXMLRequest, getLienHolderLookupSuccess, getLienHolderLookupFailure);

   gblnValidZip = validateLHZipCode(txtLHAddressZip.value.Trim(), txtLHAddressCity.value.Trim(), txtLHAddressState.value.Trim(), true)
   txtLHAddressZip.focus();
}

function getLienHolderLookupSuccess(objXML) {

   var objResult = objXML.xml.selectSingleNode("//Result/Root");
   var objLH = objResult.selectSingleNode("LienHolder");
   
   if (objLH) {
      if (strLienHolderID != objLH.getAttribute("LienHolderID")) {
         var strMessage = "";
         strMessage += objLH.getAttribute("Name") + "\n" +
                             "Office Name: " + objLH.getAttribute("OfficeName") + "\n" +
                             objLH.getAttribute("Address1") + "\n" +
                             objLH.getAttribute("AddressCity") + ", " + objLH.getAttribute("AddressState") + " " + objLH.getAttribute("AddressZip") + "\n" +
                             "Phone: " + formatPhone(objLH.getAttribute("Phone"));
         var strRet = YesNoMessage("Confirm", "System found another Lien Holder with the same zip code and phone number.\n\n" + strMessage +
                                                    "\n\nDo you want to assign this Lien Holder to the claim?");
         switch (strRet) {
            case "Yes":
               if (blnHasLH == false) {
                  var objRoot = xmlTaskDetail.selectSingleNode("/Root");
                  if (objRoot) {
                     objRoot.appendChild(xmlTaskDetail.createElement("LienHolder"));
                  }
               }
               var xmlLH = xmlTaskDetail.selectSingleNode("//LienHolder");
               if (xmlLH) {
                  //copy the attributes
                  var objLHAttr = objLH.attributes;
                  for (var i = 0; i < objLHAttr.length - 1; i++) {
                     xmlLH.setAttribute(objLHAttr[i].nodeName, objLHAttr[i].text);
                  }
                  showLienHolder();
               }
            default:
               txtLHPhone.resetValue();
               txtLHAddressZip.resetValue();
         }
      }
   }
}

function getLienHolderLookupFailure() {
}

function checkFedTaxID() {
   if (blnLoading == true) return;
   var strFedTaxID = txtLHFederalTaxID.value.Trim();
   var sProc = "uspFindLienHolderByFedTaxID";

   sRequest = "FedTaxID=" + strFedTaxID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNpAsXML",
      data: sRequest
   });

   var sXMLRequest = makeXMLSaveString(aRequests);
   AsyncXMLSave(sXMLRequest, getLienHolderFedTaxIDLookupSuccess, getLienHolderLookupFailure);
}

function getLienHolderFedTaxIDLookupSuccess(objXML) {
   strChildLienHolderID = "";
   strNewParentLienHolderID = "";
   var xmlLH = $(xmlTaskDetail).find("LienHolder");
   var objResult = objXML.xml.selectSingleNode("//Result/Root");
   //get the parent Lien Holder
   var objParentLH = objResult.selectSingleNode("//LienHolder[@LienHolderID!='" + strLienHolderID + "' and @ParentLienHolderID != '']");
   if (objParentLH) {
      if (strLienHolderID != objParentLH.getAttribute("ParentLienHolderID")) {
         //set the current Lien Holder as a child to the parent
         if (xmlLH) {
            xmlLH.attr("ParentLienHolderID", objParentLH.getAttribute("ParentLienHolderID"));
            strNewParentLienHolderID = objParentLH.getAttribute("ParentLienHolderID");
            return;
         }
      }
   }
   var objLH = objResult.selectNodes("LienHolder[@LienHolderID != '" + strLienHolderID + "']");
   if (objLH && objLH.length > 0) {
      var strMessage = "";
      var strCurrentLienHolder = "";

      //if (blnHasLH == true) {
      //   xmlLH.attr("Name") + " (" + xmlLH.attr("Address1") + ", " + xmlLH.attr("AddressCity") + ", " + xmlLH.attr("AddressState") + " " + xmlLH.attr("AddressZip") + ")";
      //} else {
      strCurrentLienHolder = txtLHName.value + " (" + txtLHAddress1.value + ", " + txtLHAddressCity.value + ", " + txtLHAddressState.value + " " + txtLHAddressZip.value + ")";
      //}
      var strDupLienHolder = objLH[0].getAttribute("Name") + " (" + objLH[0].getAttribute("Address1") + ", " + objLH[0].getAttribute("AddressCity") + ", " + objLH[0].getAttribute("AddressState") + " " + objLH[0].getAttribute("AddressZip") + ")";

      strMessage += objLH[0].getAttribute("Name") + "\n" +
                          "Office Name: " + objLH[0].getAttribute("OfficeName") + "\n" +
                          objLH[0].getAttribute("Address1") + "\n" +
                          objLH[0].getAttribute("AddressCity") + ", " + objLH[0].getAttribute("AddressState") + " " + objLH[0].getAttribute("AddressZip") + "\n" +
                          "Phone: " + formatPhone(objLH[0].getAttribute("Phone"));
      var strRet = YesNoMessage("Confirm", "System found another Lien Holder with the same Federal Tax ID.\n\n" + strMessage +
                                                 "\n\nClick Yes if you would like to make " + strCurrentLienHolder + " as a parent.\n" +
                                                 "Click No if you would like to make " + strCurrentLienHolder + " as a child of " + strDupLienHolder + ".");
      switch (strRet) {
         case "Yes":
            if (xmlLH) {
               xmlLH.attr("ParentLienHolderID", "");
               strParentLienHolderID = "";
               strChildLienHolderID = objLH[0].getAttribute("LienHolderID");
               txtLHFederalTaxID.contentSaved();
            }
            break;
         case "No":
            blnLoading == true;
            if (xmlLH) {
               //copy the attributes
               xmlLH.attr("ParentLienHolderID", objLH[0].getAttribute("LienHolderID"));
               strNewParentLienHolderID = objLH[0].getAttribute("LienHolderID");
               strChildLienHolderID = "";
            }
            break;

         default:
            blnLoading = true;
            txtLHFederalTaxID.value = "";

      }
      blnLoading = false;
   }
}

function saveLienHolder() {

   //validations
   if (txtLHName.value.Trim() == "") {
      ClientWarning("Lien Holder Name is required.");
      return;
   }
   if (txtLHAddress1.value.Trim() == "") {
      ClientWarning("Lien Holder General Address 1 is required.");
      return;
   }
   if (txtLHAddressCity.value.Trim() == "") {
      ClientWarning("Lien Holder General Address City is required.");
      return;
   }
   if (txtLHAddressState.value.Trim() == "") {
      ClientWarning("Lien Holder General Address State is required.");
      return;
   }

   if (txtLHAddressZip.value.Trim() == "") {
      ClientWarning("Lien Holder General Address Zip is required.");
      return;
  } else {
      //validate general zip code against melissa data
      gblnValidZip = validateLHZipCode(txtLHAddressZip.value.Trim(), txtLHAddressCity.value.Trim(), txtLHAddressState.value.Trim(), false)

      if (!gblnValidZip) {
          alert("Invalid General city, state, zip code entered; please correct city, state, and zip code before saving.");
          return;
      }

      //validate billing city, state, zip
      if (txtLHBillingAddressZip.value.Trim() != "") {
          gblnValidZip = validateLHZipCode(txtLHBillingAddressZip.value.Trim(), txtLHBillingAddressCity.value.Trim(), txtLHBillingAddressState.value.Trim(), false)

          if (!gblnValidZip) {
              alert("Invalid Billing city, state, zip code entered; please correct city, state, and zip code before saving.");
              return;
          }
      }
   }
   if (txtLHPhone.value.Trim() == "") {
      ClientWarning("Lien Holder General Phone Number is required.");
      return;
   }
   if (txtLHContactName.value.Trim() == "") {
      ClientWarning("Lien Holder Contact Name is required.");
      return;
   }
   if (txtLHBillingName.value.Trim() == "") {
      ClientWarning("Lien Holder Billing Name is required.");
      return;
   }
   if (txtLHEmailAddress.value.Trim() != "") {
      if (!isEmail(txtLHEmailAddress.value)) {
         ClientWarning("Lien Holder Email address is not valid format.");
         return;
      }
   }
   var strFedTaxID = txtLHFederalTaxID.value + "";
   if (strFedTaxID.Trim() != "") {
      if (strFedTaxID.length != 9) {
         ClientWarning("Lien Holder Federal Tax ID must be 9 characters in length.");
         return;
      }
   }

   if (chkEFTContractSigned.value == 1) {
      if (txtLHEFTAccountNumber.value.Trim() == "") {
         ClientWarning("Lien Holder EFT Account Number is required.");
         return;
      }
      if (txtLHEFTRoutingNumber.value.Trim() == "") {
         ClientWarning("Lien Holder EFT Routing Number is required.");
         return;
      }
      if (selEFTAccountType.selectedIndex == -1) {
         ClientWarning("Lien Holder EFT Account Type is required.");
         return;
      }
      if (txtLHEFTEffectiveDate.value == "") {
         ClientWarning("Lien Holder EFT Effective date is required.");
         return;
      }
   }

   //save data
   var sProc = "uspTLLienHolderUpdDetail";

   sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                     "&LienHolderID=" + strLienHolderID +
                     "&ParentLienHolderID=" + (strNewParentLienHolderID != "" ? strNewParentLienHolderID : strParentLienHolderID) +
                     "&ChildLienHolderID=" + strChildLienHolderID +
                     "&BusinessTypeCD=" + selLHBusinessType.value +
                     "&Name=" + escape(txtLHName.value) +
                     "&Address1=" + escape(txtLHAddress1.value) +
                     "&Address2=" + escape(txtLHAddress2.value) +
                     "&AddressCity=" + escape(txtLHAddressCity.value) +
                     "&AddressState=" + txtLHAddressState.value +
                     "&AddressZip=" + txtLHAddressZip.value +
                     "&ContactName=" + escape(txtLHContactName.value) +
                     "&EmailAddress=" + escape(txtLHEmailAddress.value) +
                     "&FaxAreaCode=" + txtLHFax.areaCode +
                     "&FaxExchangeNumber=" + txtLHFax.phoneExchange +
                     "&FaxUnitNumber=" + txtLHFax.phoneNumber +
                     "&FedTaxId=" + escape(txtLHFederalTaxID.value) +
                     "&OfficeName=" + escape(txtLHOfficeName.value) +
                     "&PhoneAreaCode=" + txtLHPhone.areaCode +
                     "&PhoneExchangeNumber=" + txtLHPhone.phoneExchange +
                     "&PhoneUnitNumber=" + txtLHPhone.phoneNumber +
                     "&PhoneExtensionNumber=" + txtLHPhone.phoneExtension +
                     "&BillingName=" + escape(txtLHBillingName.value) +
                     "&BillingAddress1=" + escape(txtLHBillingAddress1.value) +
                     "&BillingAddress2=" + escape(txtLHBillingAddress2.value) +
                     "&BillingAddressCity=" + escape(txtLHBillingAddressCity.value) +
                     "&BillingAddressState=" + txtLHBillingAddressState.value +
                     "&BillingAddressZip=" + txtLHBillingAddressZip.value +
                     "&BillingFaxAreaCode=" + txtLHBillingFax.areaCode +
                     "&BillingFaxExchangeNumber=" + txtLHBillingFax.phoneExchange +
                     "&BillingFaxUnitNumber=" + txtLHBillingFax.phoneNumber +
                     "&BillingPhoneAreaCode=" + txtLHBillingPhone.areaCode +
                     "&BillingPhoneExchangeNumber=" + txtLHBillingPhone.phoneExchange +
                     "&BillingPhoneUnitNumber=" + txtLHBillingPhone.phoneNumber +
                     "&BillingPhoneExtensionNumber=" + txtLHBillingPhone.phoneExtension +
                     "&EFTAccountNumber=" + escape(txtLHEFTAccountNumber.value) +
                     "&EFTAccountTypeCD=" + selEFTAccountType.value +
                     "&EFTContractSignedFlag=" + chkEFTContractSigned.value +
                     "&EFTEffectiveDate=" + txtLHEFTEffectiveDate.value +
                     "&EFTRoutingNumber=" + escape(txtLHEFTRoutingNumber.value) +
                     "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });

   var sXMLRequest = makeXMLSaveString(aRequests);
   //alert(sXMLRequest); return;
   var objRet = XMLSave(sXMLRequest);
   if (objRet && objRet.code == 0) {
      if (blnHasLH == false) {
         var objRoot = xmlTaskDetail.selectSingleNode("/Root");
         if (objRoot) {
            objRoot.appendChild(xmlTaskDetail.createElement("LienHolder"));
         }
      }
      var xmlLH = $(xmlTaskDetail).find("LienHolder");
      if (xmlLH) {
         strChildLienHolderID = "";
         xmlLH.attr("ParentLienHolderID", strParentLienHolderID);
         xmlLH.attr("Name", txtLHName.value);
         xmlLH.attr("OfficeName", txtLHOfficeName.value);
         xmlLH.attr("Address1", txtLHAddress1.value);
         xmlLH.attr("Address2", txtLHAddress2.value);
         xmlLH.attr("AddressCity", txtLHAddressCity.value);
         xmlLH.attr("AddressState", txtLHAddressState.value);
         xmlLH.attr("AddressZip", txtLHAddressZip.value);
         xmlLH.attr("Phone", txtLHPhone.value);
         xmlLH.attr("Fax", txtLHFax.value);
         xmlLH.attr("ContactName", txtLHContactName.value);
         xmlLH.attr("EmailAddress", txtLHEmailAddress.value);
         xmlLH.attr("FedTaxID", txtLHFederalTaxID.value);
         xmlLH.attr("BusinessTypeCD", selLHBusinessType.value);

         xmlLH.attr("BillingName", txtLHBillingName.value);
         xmlLH.attr("BillingAddress1", txtLHBillingAddress1.value);
         xmlLH.attr("BillingAddress2", txtLHBillingAddress2.value);
         xmlLH.attr("BillingAddressCity", txtLHBillingAddressCity.value);
         xmlLH.attr("BillingAddressState", txtLHBillingAddressState.value);
         xmlLH.attr("BillingAddressZip", txtLHBillingAddressZip.value);
         xmlLH.attr("BillingPhone", txtLHBillingPhone.value);
         xmlLH.attr("BillingFax", txtLHBillingFax.value);
         xmlLH.attr("EFTAccountNumber", txtLHEFTAccountNumber.value);
         xmlLH.attr("EFTRoutingNumber", txtLHEFTRoutingNumber.value);
         xmlLH.attr("EFTAccountTypeCD", selEFTAccountType.value);
         xmlLH.attr("EFTEffectiveDate", txtLHEFTEffectiveDate.value);
         xmlLH.attr("EFTContractSignedFlag", chkEFTContractSigned.value);

         if (blnHasLH == false) {
            //need to refresh the status page
            showLHDeliverables();
         }
         blnHasLH = true;
      }

      chkSameAsGeneral.value = 0;

      //go back to status page
      showStatus();
   }

}

function updateBillingAddress() {
   txtLHBillingName.value = txtLHName.value;
   txtLHBillingAddress1.value = txtLHAddress1.value;
   txtLHBillingAddress2.value = txtLHAddress2.value;
   txtLHBillingAddressCity.value = txtLHAddressCity.value;
   txtLHBillingAddressState.value = txtLHAddressState.value;
   txtLHBillingAddressZip.value = txtLHAddressZip.value;
   txtLHBillingPhone.value = txtLHPhone.value;
   txtLHBillingFax.value = txtLHFax.value;
}

function showSalvageGeneral() {
   $("#divSalvageGeneral").removeClass().addClass("visible");
   $("#divSalvageMailing").removeClass().addClass("hidden");
}

function showSalvageMailing() {
   $("#divSalvageGeneral").removeClass().addClass("hidden");
   $("#divSalvageMailing").removeClass().addClass("visible");
}

function updateSalvageMailingAddress() {
   txtSalvageMailingAddress1.value = txtSalvageAddress1.value;
   txtSalvageMailingAddress2.value = txtSalvageAddress2.value;
   txtSalvageMailingAddressCity.value = txtSalvageAddressCity.value;
   txtSalvageMailingAddressState.value = txtSalvageAddressState.value;
   txtSalvageMailingAddressZip.value = txtSalvageAddressZip.value;
}

function saveSalvage() {
   //validations
   if (txtSalvageName.value.Trim() == "") {
      ClientWarning("Salvage Name is required.");
      return;
   }
   if (txtSalvageControlNumber.value.Trim() == "") {
      ClientWarning("Salvage Control # is required.");
      return;
   }
   if (txtSalvageMailingAddress1.value.Trim() == "") {
      ClientWarning("Salvage Mailing Address 1 is required.");
      return;
   }
   if (txtSalvageMailingAddressCity.value.Trim() == "") {
      ClientWarning("Salvage Mailing Address City is required.");
      return;
   }
   if (txtSalvageMailingAddressState.value.Trim() == "") {
      ClientWarning("Salvage Mailing Address State is required.");
      return;
   }
   if (txtSalvageMailingAddressZip.value.Trim() == "") {
      ClientWarning("Salvage Mailing Address Zip is required.");
      return;
   }
   if (txtSalvageEmailAddress.value.Trim() != "") {
      if (!isEmail(txtSalvageEmailAddress.value)) {
         ClientWarning("Salvage Email address is not valid format.");
         return;
      }
   }

   //save data
   var sProc = "uspTLSalvageVendorUpdDetail";
   sRequest = "SalvageVendorID=" + strSalvageVendorID +
                    "&Name=" + escape(txtSalvageName.value) +
                    "&Address1=" + escape(txtSalvageAddress1.value) +
                    "&Address2=" + escape(txtSalvageAddress2.value) +
                    "&AddressCity=" + escape(txtSalvageAddressCity.value) +
                    "&AddressState=" + escape(txtSalvageAddressState.value) +
                    "&AddressZip=" + txtSalvageAddressZip.value +
                    "&ContactName=" + escape(txtSalvageContactName.value) +
                    "&EmailAddress=" + escape(txtSalvageEmailAddress.value) +
                    "&FaxAreaCode=" + txtSalvageFax.areaCode +
                    "&FaxExchangeNumber=" + txtSalvageFax.phoneExchange +
                    "&FaxUnitNumber=" + txtSalvageFax.phoneNumber +
                    "&MailingAddress1=" + escape(txtSalvageMailingAddress1.value) +
                    "&MailingAddress2=" + escape(txtSalvageMailingAddress2.value) +
                    "&MailingAddressCity=" + escape(txtSalvageMailingAddressCity.value) +
                    "&MailingAddressState=" + escape(txtSalvageMailingAddressState.value) +
                    "&MailingAddressZip=" + txtSalvageMailingAddressZip.value +
                    "&OfficeName=" + escape(txtSalvageOfficeName.value) +
                    "&PhoneAreaCode=" + txtSalvagePhone.areaCode +
                    "&PhoneExchangeNumber=" + txtSalvagePhone.phoneExchange +
                    "&PhoneUnitNumber=" + txtSalvagePhone.phoneNumber +
                    "&PhoneExtensionNumber=" + txtSalvagePhone.phoneExtension +
                    "&WebsiteAddress=" + escape(txtSalvageWebsite.value) +
                    "&UserID=" + strUserID +
                    "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                    "&SalvageControlNumber=" + escape(txtSalvageControlNumber.value);
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         var xmlSalvage = $(xmlTaskDetail).find("Salvage");
         if (xmlSalvage) {
            var xmlTL = $(xmlTaskDetail).find("TL");
            if (xmlTL) {
               xmlTL.attr("SalvageControlNumber", txtSalvageControlNumber.value);
            }
            xmlSalvage.attr("Name", txtSalvageName.value);
            xmlSalvage.attr("OfficeName", txtSalvageOfficeName.value);
            xmlSalvage.attr("Address1", txtSalvageAddress1.value);
            xmlSalvage.attr("Address2", txtSalvageAddress2.value);
            xmlSalvage.attr("AddressCity", txtSalvageAddressCity.value);
            xmlSalvage.attr("AddressState", txtSalvageAddressState.value);
            xmlSalvage.attr("AddressZip", txtSalvageAddressZip.value);
            xmlSalvage.attr("Phone", txtSalvagePhone.value);
            xmlSalvage.attr("Fax", txtSalvageFax.value);
            xmlSalvage.attr("ContactName", txtSalvageContactName.value);
            xmlSalvage.attr("EmailAddress", txtSalvageEmailAddress.value);
            xmlSalvage.attr("WebSiteAddress", txtSalvageWebsite.value);

            xmlSalvage.attr("MailingAddress1", txtSalvageMailingAddress1.value);
            xmlSalvage.attr("MailingAddress2", txtSalvageMailingAddress2.value);
            xmlSalvage.attr("MailingAddressCity", txtSalvageMailingAddressCity.value);
            xmlSalvage.attr("MailingAddressState", txtSalvageMailingAddressState.value);
            xmlSalvage.attr("MailingAddressZip", txtSalvageMailingAddressZip.value);
         }

         chkSameAsGeneralSalvage.value = 0;
         showStatus();
      }
   }
}

// for billing address, only need to validate zip
function validateLHBillingZipCode(pstrZip, pstrCity, pstrState, pblnDisplayAlerts) {

    if (gblnResetLH != true)
        validateLHZipCode(txtLHBillingAddressZip.value.Trim(), txtLHBillingAddressCity.value.Trim(), txtLHBillingAddressState.value.Trim(), true)
}


// Validate city, state, and zip code
function validateLHZipCode(pstrZip, pstrCity, pstrState, pblnDisplayAlerts) {

    var sProc = "uspValidateZipCodeXML";
    var strZipCities = "";

    sRequest = "ZipCode=" + pstrZip +
                    "&City=" + pstrCity +
                    "&State=" + pstrState;
    var aRequests = new Array();
    aRequests.push({ procName: sProc,
        method: "ExecuteSpNpAsXML",
        data: sRequest
    });

    var sXMLRequest = makeXMLSaveString(aRequests);
    var objXML = XMLSave(sXMLRequest);

    if (objXML && objXML.code == 0) {

        var objResult = objXML.xml.selectSingleNode("//Result/Root");
        var objMelissaData = objResult.selectNodes("Melissa")

        if (objMelissaData.length == 0) {
            if (pblnDisplayAlerts)
                alert("Invalid zip code entered - please enter a valid zip code.");
            return false;
        } else {
            for (var i = 0; i <= objMelissaData.length - 1; i++) {
                //check if city/state match for entered zip
                if (pstrCity.Trim().toUpperCase() == objMelissaData[i].getAttribute("MelCity").Trim().toUpperCase() && pstrState.Trim().toUpperCase() == objMelissaData[i].getAttribute("MelState").Trim().toUpperCase())
                    return true;

                //add to message of valid cities                    
                if (strZipCities == "")
                    strZipCities = objMelissaData[i].getAttribute("MelCity").Trim() + " " + objMelissaData[i].getAttribute("MelState").Trim();
                else
                    strZipCities = strZipCities.Trim() + ", " + objMelissaData[i].getAttribute("MelCity").Trim() + " " + objMelissaData[i].getAttribute("MelState").Trim();
            }
            //no match for city/state/zip found; display msg and return false
            if (pblnDisplayAlerts)
                alert("Invalid city/state for zip code " + pstrZip + "; Valid cities/states are: " + strZipCities);
            return false;
        }
    } else {
        // no or bad data returned from xml; return false
        return false;
    }

}


function showTitle() {
   hideAllPopup();
   if (xmlTaskDetail) {
      var xmlTitle = $(xmlTaskDetail).find("Title");
      if (xmlTitle) {
         disableTitle(false);
         txtTitleNames.value = xmlTitle.attr("TitleName");
         txtTitleState.value = xmlTitle.attr("TitleState");
         txtTitleStatus.value = xmlTitle.attr("TitleStatus");

         var blnDisabled = (strSCSatus != "Active" || xmlTitle.attr("TitleNameConfirmFlag") == "1")
         if (xmlTitle.attr("TitleNameConfirmFlag") == "1") {
            $("#spnTitleConfirm").text(" Confirmed on " + xmlTitle.attr("TitleNameConfirmDate"));
         }
         if (blnSupervisor == true) blnDisabled = false;
         disableTitle(blnDisabled);

         if (blnDisabled == false)
            setTitleConfirmDisabled();

         if (blnDisabled == false) txtTitleNames.setFocus();
      }

      $("#divTitle").removeClass().addClass("visible");
   }
}

function setTitleConfirmDisabled() {
   btnTitleConfirm.CCDisabled = (txtTitleNames.value == "" || txtTitleState.value == "");
}

function disableTitle(blnDisabled) {
   disableControls(divTitle.getElementsByTagName("APDInput"), blnDisabled);
   disableControls(divTitle.getElementsByTagName("APDCustomSelect"), blnDisabled);
   disableControls(divTitle.getElementsByTagName("APDButton"), blnDisabled);
}

function saveTitle(strFrom) {
   if (txtTitleNames.value.Trim() == "") {
      alert("Title name(s) is required.");
      return;
   }
   if (txtTitleState.value.Trim() == "") {
      alert("Title State is required.");
      return;
   }

   //save settlement data
   var sProc = "uspTLTitleDataUpdate";
   sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                    "&TitleName=" + escape(txtTitleNames.value) +
                    "&TitleState=" + escape(txtTitleState.value) +
                    "&TitleStatus=" + (txtTitleStatus.selectedIndex != -1 ? txtTitleStatus.value : "") +
                    "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         var xmlTitle = $(xmlTaskDetail).find("Title");
         if (xmlTitle) {
            xmlTitle.attr("TitleName", txtTitleNames.value);
            xmlTitle.attr("TitleState", txtTitleState.value);
            xmlTitle.attr("TitleStatus", (txtTitleStatus.selectedIndex != -1 ? txtTitleStatus.value : ""));
         }

         if (strFrom != "confirm") {
            //go back to status page
            showStatus();
         }
         return true;
      }
      return false;
   }
   return false;
}

function confirmTitle() {
   var xmlTitle = $(xmlTaskDetail).find("Title");
   if (xmlTitle) {
      if (xmlTitle.attr("TitleName") != txtTitleNames.value ||
                xmlTitle.attr("TitleState") != txtTitleState.value) {
         //data has changed. save changes.
         if (saveTitle("confirm") == false) return;
      }
   }
   //save settlement data
   var sProc = "uspTLConfirmTitleData";
   sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                    "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         var xmlTitle = $(xmlTaskDetail).find("Title");
         if (xmlTitle) {
            var dt = new Date();
            var strDate = formatDateTime(dt.toLocaleString());
            xmlTitle.attr("TitleNameConfirmDate", strDate);
            xmlTitle.attr("TitleNameConfirmFlag", 1);
            $("#spnTitleConfirm").text("Confirmed on " + strDate);
            btnTitleConfirm.CCDisabled = true;
         }

         //go back to status page
         showStatus();
      }
   }
}
