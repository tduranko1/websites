
// Document / Imaging module popup code
function ShowImagePopup( strPath )
{
    var settingsXML='scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
    var settings='scrollbars=yes,location=no,directories=no,status=no,menubar=yes,toolbar=no,resizable=yes';

    strPath = strPath.toUpperCase();
    //alert(strPath);

    if ( ( strPath.indexOf("HTTPS:") == -1 ) && ( strPath.indexOf(".XML") == -1 ) )
    {
        try
        {
            var wsh = new ActiveXObject( "WSCript.shell" );
            wsh.Run( "Kodakimg " + strPath );
            wsh.AppActivate( "Imaging" )
        }
        catch(e)
        {
            window.open( strPath, 'docview', settings );
        }
    }
    else if ( strPath.indexOf(".XML") != -1 )
    {
        window.open( "/viewXML.asp?docPath=" + strPath, "", settingsXML );
    }
    else
    {
        window.open( strPath, 'docview', settings );
    }
}
