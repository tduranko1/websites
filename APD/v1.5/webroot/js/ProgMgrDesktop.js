if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;



//Auto-resize table layout
function resizeScrollTable(oElement)
{
  var head = oElement.firstChild;
  var body = oElement.lastChild;
  var scrollBarWidth = body.offsetWidth - body.clientWidth;
  var headCells = head.firstChild.rows[0].cells;

  headCells[headCells.length - 1].style.width = scrollBarWidth;
}

//Takes a string and removes the leading and trailing spaces and also any extra spaces in the string. IE only.
function remove_XS_whitespace(item)
{
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
  for (index = 0; index < item_length; index++)
  {
    if (item.value.charAt(index) != ' ')
    {
      tmp += item.value.charAt(index);
    }
    else
    {
      if (tmp.length > 0)
      {
        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1)
        {
          tmp += item.value.charAt(index);
        }
      }
    }
  }
  item.value = tmp;
}

var saveBG;
function GridMouseOver(oObject)
{
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
	oObject.style.cursor='hand';
}

function GridMouseOut(oObject)
{
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
}


//update the selected elements from the table row
function GridSelect(oRow)
{

}

//init the table selections, must be last
function callListSearch()
{
  parent.document.frames["ifrmPMDShopSearch"].frameElement.src = "PMDCycleTime.asp?ShopLocationID="+gsShopLocationID+"&BeginDate="+txtBeginDate.value+"&EndDate="+txtEndDate.value+"&AssignmentCode="+txtAssignmentCode.value;
}


function callRefresh(){
  window.navigate("PMDShopSearch.asp");
}


// Displays external and internal event messages in a popup window.
// This version is called for errors occurring server side.
function ServerEvent()
{
    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "EventPopup.asp";

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

// Displays external and internal event messages in a popup window.
// This version is called for errors occurring client side.
function ClientEvent( nNumber, sDescription, sSource )
{
    if ( ( String( sSource ) == "undefined" ) || ( sSource == null ) )
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

function ClientError( sDescription ) { ClientEvent( 0, sDescription ); }
function ClientWarning( sDescription ) { ClientEvent( 1, sDescription ); }
function ClientInfo( sDescription ) { ClientEvent( 2, sDescription ); }
