
var dom = (document.getElementsByTagName) ? true : false;
var ie5 = (document.getElementsByTagName && document.all) ? true : false;
var arrowUp, arrowDown;

if (ie5 || dom)
	initSortTable();

function initSortTable() {
	arrowUp = document.createElement("SPAN");
	var tn = document.createTextNode("5");
	arrowUp.appendChild(tn);
	arrowUp.className = "arrow";

	arrowDown = document.createElement("SPAN");
	var tn = document.createTextNode("6");
	arrowDown.appendChild(tn);
	arrowDown.className = "arrow";
}

function sortTable(tableNode, nCol, bDesc, sType) {
	var tBody = tableNode.tBodies[0];
	var trs = tBody.rows;
	var a = new Array();
  var d = new Array();

  // Body attributes used to specify customized behavior.
  var dr = tBody.getAttribute("dataRows");
  var color1 = tBody.getAttribute("bgColor1");
  var color2 = tBody.getAttribute("bgColor2");

  dr = ( dr == null ) ? 1 : Number(dr)

  var di = 0;
  var ai = 0;
  var iLength = trs.length;
  for (var i=0; i < iLength; i=i+dr)
  {
    a[ai++] = trs[i];
    for (var j=1; j<dr; j++)
        d[di++] = trs[i+j];
	}

	a.sort(compareByColumn(nCol,bDesc,sType));
  var iLength = a.length;
  for (var i=0; i < iLength; i++)
  {
    // Reset alternating background colors
    if ( color1 != null && color2 != null )
        a[i].setAttribute("bgColor", ((i&1)>0) ? color1 : color2 );

		tBody.appendChild(a[i]);

    // Carry along special data rows attached to their parent rows (i.e. notes).
    var idx = a[i].getAttribute("idx");
    var jLength = d.length;
    for (var j=0; j < jLength; j++) {
        if ( d[j].getAttribute("idx") == idx ) {
            tBody.appendChild(d[j]);
        }
    }
	}
}

function CaseInsensitiveString(s) {
	return String(s).toUpperCase();
}

function parseDate(s) {
	return Date.parse(s.replace(/\-/g, '/'));
}

function imageSourceName(td)
{
    return String(td.getElementsByTagName("img").item(0).getAttribute("src"));
}

function TimeDiff(s)
{
	var num = parseInt(s);
	var mux = 1; 
	if (s.indexOf("hrs") != -1) mux = 60;
	if (s.indexOf("days") != -1) mux = 1440;
	
	//alert(s+":"+num+":"+mux+":"+(num*mux));
	return (num * mux);
}

/* alternative to number function
 * This one is slower but can handle non numerical characters in
 * the string allow strings like the follow (as well as a lot more)
 * to be used:
 *    "1,000,000"
 *    "1 000 000"
 *    "100cm"
 */

function toNumber(s) {
    return Number(s.replace(/[^0-9\.]/g, ""));
}

function compareByColumn(nCol, bDescending, sType) {
	var c = nCol;
	var d = bDescending;

	var fTypeCast = String;

	if (sType == "Number")
		fTypeCast = Number;
	else if (sType == "Date")
		fTypeCast = parseDate;
	else if (sType == "CaseInsensitiveString")
		fTypeCast = CaseInsensitiveString;
	else if (sType == "TimeD")
		fTypeCast = TimeDiff;
  else if (sType == "ImageSourceFileName")
  {
    return function (n1, n2) 
		{
			if (imageSourceName(n1.cells[c]) < imageSourceName(n2.cells[c]))
			    return d ? -1 : +1;
			if (imageSourceName(n1.cells[c]) > imageSourceName(n2.cells[c]))
			    return d ? +1 : -1;
			return 0;
    };
  }

	return function (n1, n2) {
		if (fTypeCast(getInnerText(n1.cells[c])) < fTypeCast(getInnerText(n2.cells[c])))
			return d ? -1 : +1;
		if (fTypeCast(getInnerText(n1.cells[c])) > fTypeCast(getInnerText(n2.cells[c])))
			return d ? +1 : -1;
		return 0;
	};
}


function sortColumn(e, sDataTbl) {

	var tmp, el, tHeadParent;

	if (ie5)
		tmp = e.srcElement;
	else if (dom)
		tmp = e.target;

	tHeadParent = getParent(tmp, "TBODY");
	el = getParent(tmp, "TD");
	if (tHeadParent == null || el == null)
		return;

  var colIndex = el.getAttribute("sIndex");
  if (colIndex == 99)
    return;
  

	if (el != null) 
	{
		var p = el.parentNode;

		if (el._descending)	// catch the null
			el._descending = false;
		else
			el._descending = true;

		if (tHeadParent.arrow != null) 
		{
			if (tHeadParent.arrow.parentNode != el) 
			{
				tHeadParent.arrow.parentNode._descending = null;	//reset sort order
			}
			tHeadParent.arrow.parentNode.removeChild(tHeadParent.arrow);
		}

		if (el._descending)
			tHeadParent.arrow = arrowDown.cloneNode(true);
		else
			tHeadParent.arrow = arrowUp.cloneNode(true);

		el.appendChild(tHeadParent.arrow);

    // get the index of the td
    var iLength = p.cells.length;
		for (var i=0; i < iLength; i++)
		{
			if (p.cells[i] == el)
      {
    		if (colIndex != null)
        {
          i = colIndex;
          break;
        }
        else break;
      }
		}

		//var table = getParent(el, "TABLE");
		var table = document.getElementById(sDataTbl);
		// can't fail

		sortTable(table,i,el._descending, el.getAttribute("type"));
	}
}


function getInnerText(el) {
	if (ie5) return el.innerText;	//Not needed but it is faster

	var str = "";
  var iLength = el.childNodes.length;
	for (var i=0; i < iLength; i++) {
		switch (el.childNodes.item(i).nodeType) {
			case 1: //ELEMENT_NODE
				str += getInnerText(el.childNodes.item(i));
				break;
			case 3:	//TEXT_NODE
				str += el.childNodes.item(i).nodeValue;
				break;
		}

	}

	return str;
}

function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}
