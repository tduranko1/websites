// PMDNavigate.js
//
// Functions for navigating to claims
// i.e. from claim data to vehicle to property etc.
//
// These methods assume the caller is catching exceptions.
//


// Returns a string value that will not throw.
function StrVal( sSrc )
{
    if ( String( sSrc ) == "undefined" )
        return "[undefined]";
    else if ( sSrc == null )
        return "[null]";
    return sSrc;
}


function NavToClaim(sLynxID){
  try{
    var sWinName = "APDClaimFrame_" + sLynxID;
    var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
    var sUrl = "../ApdSelect.asp?Entity=vehicle&Context=1&ClaimView=cond&LynxID=" + sLynxID;

    var oWin = window.open( sUrl, sWinName, sSettings );
    oWin.focus();
  }
  catch ( e ){
    throw "PMDNavigate.js::NavToClaim( sEntity='" + StrVal( sEntity )
        + "', sLynxID='" + StrVal( sLynxID )
        + "', sContext='" + StrVal( sContext )
        + "' ) Error: " + e.message;
  }
}

function NavToClaim2(sLynxID, sVehicleNum){
  try{
    var sWinName = "APDClaimFrame_" + sLynxID;
    var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
    var sUrl = "../ApdSelect.asp?Entity=vehicle&Context=" + sVehicleNum + "&ClaimView=cond&LynxID=" + sLynxID;

    var oWin = window.open( sUrl, sWinName, sSettings );
    oWin.focus();
  }
  catch ( e ){
    throw "PMDNavigate.js::NavToClaim2( sEntity='" + StrVal( sEntity )
        + "', sLynxID='" + StrVal( sLynxID )
        + "', sContext='" + StrVal( sContext )
        + "' ) Error: " + e.message;
  }
}

function NavToPMDDetail(sShopID){
  try{
    var sWinName = "PMDFrame_" + sShopID;
    var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
    var sUrl = "PMD.asp?ShopID=" + sShopID;

    var oWin = window.open( sUrl, sWinName, sSettings );
    oWin.focus();
  }
  catch ( e ){
    throw "PMDNavigate.js::NavToPMDDetail( sShopID='" + StrVal( sShopID ) + "' ) Error: " + e.message;
  }
}


function NavToShop(sSearchType, sEntityID, sShopBusinessID){
  // The parameter 'sShopBusinessID' is the Business (utb_shop) to which the Shop (utb_shop_location) belongs.  This is only needed for 
  // SearchType 'S'.  This is so the Business ID can be used in the window name rather than the ShopID.  This will prevent opening multiple 
  // sibling shops in different windows and then being able to change tabs in those windows possibly resulting in multiple windows with 
  // the same shop.  
  try{
      var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
      var sUrl = "SMT.asp?SearchType=" + sSearchType + "&EntityID=" + sEntityID;
      var sWinName;
      
      switch (sSearchType){
        case "S":
          sWinName = "SMTFrame_B" + sShopBusinessID;
          break;
        case "B":
          sWinName = "SMTFrame_B" + sEntityID;
          break;
        case "D":
          sWinName = "SMTFrame_D" + sEntityID;
          break;
        case "BusinessWiz":
        case "ShopWiz":
        case "DealerWiz":  
          ShowWizard(sSearchType);
          return;
          break;        
        default:
          throw "Invalid value was passed for sSearchType.  Value must be one of the following: 'S', 'B', or 'D'.";
      }
      
      if (sUrl != ""){      
        var oWin = window.open( sUrl, sWinName, sSettings );
        oWin.oCallingWin = top.window.self;  // send the new window a handle to this window.
        oWin.focus();
        //alert(oWin.oCallingWin.name);      
      }
  }
  catch ( e ){
    throw "PMDNavigate.js::NavToShop( sSearchType='" + StrVal( sSearchType ) 
                                     + "', sEntityID='" + StrVal( sEntityID )
                                     + "', sShopBusinessID='" + StrVal( sShopBusinessID )
                                     + "' ) Error: " + e.message;
  }
}
  
  
function ShowWizard(sWizard){
  var sRetVal, aRet, sUrl, sSettings;
  
  switch (sWizard){
    case "BusinessWiz":
      sUrl = "SMTWizard.asp?mode=businfo";
      break;
    case "ShopWiz":
      sUrl = "SMTWizard.asp?mode=shop&BusinessID=" + top.gsBusinessInfoID;
      break;
    case "DealerWiz":
      sUrl = "SMTDealerDetailLevel.asp?mode=wizard";
      break;
  }
  
  if (sWizard == "BusinessWiz" || sWizard == "ShopWiz")
    sSettings = "dialogHeight:580px; dialogWidth:740px; resizable:no; status:no; help:no; center:yes;";
  else
    sSettings = "dialogHeight:580px; dialogWidth:765px; resizable:no; status:no; help:no; center:yes;";
                  
  sRetVal =  window.showModalDialog(sUrl, "", sSettings);
  
  switch(sWizard){
    case "BusinessWiz":
    case "ShopWiz":
      if (sRetVal == "NONE" || sRetVal.indexOf("|") == -1)
        return;
    
      aRet = sRetVal.split("|");
      if (aRet[0] != "")
        NavToShop("B", aRet[0]);
      else
        NavToShop("S", aRet[1], top.gsBusinessInfoID);      
      
      break;
      
    case "DealerWiz":
      if (sRetVal == "NONE")
        return
      else
        NavToShop("D", sRetVal);
      break;
        
  }
  return;
}

function NavToAssignment(sShopLocationID, sAssignmentID, sAssignmentDate){
  try{
    var sWinName = "APDClaimFrame_" + sAssignmentID;
    var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
    var sUrl = "PMD.asp?ShopID=" + sShopLocationID + "&AssignmentID=" + sAssignmentID + "&BeginDate=" + sAssignmentDate;
    var oWin = window.open( sUrl, sWinName, sSettings );
    oWin.focus();
  }
  catch ( e ){
    throw "PMDNavigate.js::NavToAssignment( sShopLocationID='" + StrVal( sShopLocationID )
        + "', sAssignmentID='" + StrVal( sAssignmentID )
        + "', sAssignmentDate='" + StrVal( sAssignmentDate )
        + "' ) Error: " + e.message;
  }  
}