
/********************************************************************************
* Function:  GetRequestStringFromHTMLForm                                       *
* Purpose:   Generates a string of name/value pairs from elements on Form sName *
* Parameters:   sName - The name of the form whose elements to get              *
*               bGetMultiples - (false)  Retrieves only elements with unique    *
*                                        names                                  *
*                               (true)   Retrieves all elements, including      *
*                                        elements with duplicate names          *
*********************************************************************************/                                
function GetRequestStringFromHTMLForm(sName, bGetMultiples)
{
	var oForm = document.all.namedItem(sName);
	var sRequest = "";

  // "Input" elements
  {
  	var elms = oForm.getElementsByTagName("INPUT");
    var iLength = elms.length;
  	for(var i = 0; i < iLength; i++) {
  		if (elms[i].type != "button" &&
  		    elms[i].type != "file" &&		 
  		    elms[i].type != "image" &&
  		    elms[i].type != "reset" &&
  		    elms[i].type != "submit" &&
  		    elms[i].name != "")	{
  		  if (bGetMultiples == false && oForm.namedItem(elms[i].name).length > 1) {
  		    continue;
  		  }
  		  
  		  if (elms[i].type == "checkbox") {
    			sRequest += elms[i].name + "=";
    			if (elms[i].checked == true) {
    		    sRequest += "1&";
    			}
    			else {
    			  sRequest += "0&";
    			}
    		}
    		else {
  			  sRequest += elms[i].name + "=" + elms[i].value + "&";
  			}
  		}
  	}
  }

  // "Select" elements
  // Note we build an array of names here instead of using the namedItem(elmName).length as above
  // because length gives us the number of option elements in the select instead of the number of
  // duplicately named elements.  The helper function GetArrayElementCount in utilities.js is used
  // to get the count
  {
    var aElmNames = new Array();
    var elms = oForm.getElementsByTagName("SELECT");
    var iLength = elms.length;
    for (var i = 0; i < iLength; i++) {
      aElmNames[i] = elms[i].name;
    }
    var iLength = aElmNames.length;
    for (var i = 0; i < iLength; i++) {  
  	  if (bGetMultiples == false && GetArrayElementCount(aElmNames, aElmNames[i]) > 1) {
  	    continue;
  	  }
  
  		sRequest += aElmNames[i] + "=" + oForm.namedItem(aElmNames[i]).value + "&";
    }
  }

  // "TextArea" elements
  {
  	var elms = oForm.getElementsByTagName("TEXTAREA");
    var iLength = elms.length;
  	for(var i = 0; i < iLength; i++) {
  		if (elms[i].name != "")	{
  		  if (bGetMultiples == false && oForm.namedItem(elms[i].name).length > 1) {
  		    continue;
  		  }
  		
  			sRequest += elms[i].name + "=" + elms[i].value + "&";
  		}
  	}
  }

  if (sRequest.length > 0) {
    // Remove the final "&"
    sRequest = sRequest.substring(0, sRequest.length - 1)
  }
  
	return sRequest;
}

/************************************************************************************
* Function:  GetValueFromElement                                                *
* Purpose: Returns a string representing the value of sAttribute from element(s)    * 
*          named sName.  All values will be returned from duplicately named         *
*          elements as a comma delimited list.  The function returns the values of  *
*          Checkbox and radio elements only if their "checked" property is true.    *
* Parameters:   sName - The name of the element(s) whose values to get              *
* Parameters:   sAttribute (optional) - The name of the attribute to get            *
*               (defaults to value)                                                 *
************************************************************************************/                                
function GetValueFromElement(sName, sAttribute)
{
	var sValueString = "";
  if (sAttribute==null) {
    // Np atttribute parameter sent, set default
    sAttribute = "value";
  }
  
	var elms = document.getElementsByName(sName);
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++) {
		if (elms[i].type != "button" &&
		    elms[i].type != "file" &&		 
		    elms[i].type != "image" &&
		    elms[i].type != "reset" &&
		    elms[i].type != "submit")	{
		  if (elms[i].type == "checkbox" ||
		      elms[i].type == "radio") {
  			if (elms[i].checked == true) {
  		    sValueString += elms[i].getAttribute(sAttribute) + ",";
  			}
  		}
  		else {
 		    sValueString += elms[i].getAttribute(sAttribute) + ",";
			}
		}
	}
  
  if (sValueString.length > 0) {
    // Remove the final ","
    sValueString = sValueString.substring(0, sValueString.length - 1)
  }
  
	return sValueString;
}

/*********************************************************************************
* Function:  GetValueFromElementWithAttribute                                    *
* Purpose: Returns a string representing the value of sAttribute from element(s) * 
*          named sName whose sSearchAttribute value matches sSearchValue.  All   *
*          values will be returned from multiple elements meeting this criteria  *
*          as a comma delimited list.                                            *
* Parameters: sName - The name of the element(s) whose attributes to search      *
*             sSearchAttribute - The name of the attribute to use for the search *
*             sSearchValue - The value to search for                             *
*             sAttribute - The attribute whose value(s) to return                *
**********************************************************************************/       
function GetValueFromElementWithAttribute(sName, sSearchAttribute, sSearchValue, sAttribute)
{
	var sValueString = "";
  if (sAttribute==null) {
    // Np attribute parameter sent, set default
    sAttribute = "value";
  }
  
	var elms = document.getElementsByName(sName);
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++) {
    if (elms[i].getAttribute(sSearchAttribute) == sSearchValue) {
      sValueString += elms[i].getAttribute(sAttribute) + ",";
		}
	}
  
  if (sValueString.length > 0) {
    // Remove the final ","
    sValueString = sValueString.substring(0, sValueString.length - 1)
  }
  
	return sValueString;
}
