
//**************************************************************************
//**        Estimate Details
//*************************************************************************

  function rowMouseOver(tblObject)
  {
		if (gsDetailEditableFlag == 0 || tblObject.deleteDoc == "true" || tblObject.newRowItem == true) return;
  	if (editablecell == null)
		{
			saveBG = tblObject.style.backgroundColor;
  		tblObject.style.backgroundColor="#FFEEBB";
  		tblObject.style.color = "#000066";
	   	tblObject.style.cursor='default';
		}
  }

  
  function rowMouseOut(tblObject)
  {
		if (gsDetailEditableFlag == 0 || tblObject.deleteDoc == "true" || tblObject.newRowItem == true) return;
  	if (editablecell == null)
  	{
  		tblObject.style.backgroundColor=saveBG;
  		tblObject.style.color = "#000000";
  	}
  }


	var editbg;
	function EditableCellOver(obj)
	{
		if (gsDetailEditableFlag == 0 || obj.parentElement.deleteDoc == "true") return;
		if (gsCRUD.indexOf("U") == -1 ) return;
    if (editablecell == null)
		{
			editbg = obj.style.backgroundColor;
			obj.style.backgroundColor = "#DDDDDD";
		}
		//saveBorder = obj.style.border;
		//obj.style.border = "1px solid Black";
	}


	function EditableCellOut(obj)
	{
		if (gsDetailEditableFlag == 0 || obj.parentElement.deleteDoc == "true") return;
		if (gsCRUD.indexOf("U") == -1 ) return;
		if (editablecell == null)
			obj.style.backgroundColor = editbg;
		//obj.style.border = saveBorder;
	}


	function EditableCellChange(obj)
	{
		if (gsDetailEditableFlag == 0) return;
    obj.parentElement.style.border = "1px solid #800000";
  	obj.parentElement.dirty = true;
    EditableCellClose(obj);
	}

  
	function EditableCellBlur(obj)
	{
		if (gsDetailEditableFlag == 0) return;
    EditableCellClose(obj);
	}

  
	function EditableCellKeyPress(obj)
	{
    if (event.keyCode == 13 || event.keyCode == 27)
  	{
			EditableCellClose(obj);
    }
    else
    {
			editablecell.style.border = "1px solid #800000";
    	editablecell.dirty = true;
      SetDirtyFlag(true);
    }
	}


  function EditableCellCheckClose(obj)
  {
    if (event.toElement.parentElement == editablecell)
      return;
    else
      EditableCellClose();
  }

  
	function EditableCellClose(obj)
	{
    if (editablecell == null) return;

    var obj = editablecell.firstChild;
    if (editablecell.lastChild.type == "checkbox")
      var cbObj = editablecell.lastChild;
    var objRow = editablecell.parentElement;
		var tblIndex = editablecell.IDIndex;
    var dbsave;

  	if (obj.type == "text")
  	{
  		editablecell.innerHTML = ""
  		if (editablecell.dataType == 'Hours')
      {
        var sHrs = detailsFormatHoursStr(obj.value);
        if (sHrs == " ") sHrs = 0;
        if (cbObj.checked == true)
        {
          editablecell.innerText = "Incl.";
          if (editablecell.xmlAttribute == "LaborHoursAgreed")
            objRow.cells[47].innerText = "1";
          else if (editablecell.xmlAttribute == "RefinishLaborHoursAgreed")
            objRow.cells[48].innerText = "1";
          dbsave = sHrs;
        }
        else
        {
          editablecell.innerText = sHrs;
          if (editablecell.xmlAttribute == "LaborHoursAgreed")
            objRow.cells[47].innerText = "0";
          else if (editablecell.xmlAttribute == "RefinishLaborHoursAgreed")
            objRow.cells[48].innerText = "0";
          dbsave = editablecell.innerText;
        }
  		}
      else if (editablecell.dataType == 'Money')
      {
        var sMny = detailsFormatCurrencyStr(obj.value);
        if (sMny == " ") sMny = 0;
        if (cbObj.checked == true)
        {
          editablecell.innerText = "Incl.";
          objRow.cells[46].innerText = "1";
          dbsave = sMny;
        }
        else
        {
          editablecell.innerText = sMny;
          objRow.cells[46].innerText = "0";
          dbsave = editablecell.innerText;
        }
  		}
      else if (editablecell.dataType == 'Qty')
      {
        if (obj.value.indexOf('.') == 0 || obj.value == '')
        {
          editablecell.innerText = ' ';
          dbsave = '0';
        }
        else if (obj.value.indexOf('.') != -1)
        {
          editablecell.innerText = obj.value.substring(0, obj.value.indexOf('.'));
          dbsave = editablecell.innerText;
        }
        else
        {
          editablecell.innerText = obj.value;
          dbsave = obj.value;
        }
      }
  		else
      {
        editablecell.innerText = obj.value;
        dbsave = obj.value;
      }

  		if (tblIndex)
  			objRow.cells[parseInt(tblIndex)].innerText = dbsave;
  	}
  	else if (obj.type == "select-one")
  	{
      if (tblIndex)
  			objRow.cells[parseInt(tblIndex)].innerText = obj.value;
  		//get text to place back in the cell
  		var strValue = obj.options[obj.selectedIndex].value;
  		var strValuePopup = obj.options[obj.selectedIndex].innerText;
  		var objName = obj.name;
  		//clear out control
  		editablecell.innerHTML = "";
      if (escape(strValue) == '%A0' || strValue == '')
      {
    		var newSelValue = "&nbsp;";
      }
      else
      {
    		var newSelValue = "<SPAN style='cursor:hand' onMouseOver='popUpDynMsg(this.lastChild.innerText)' onMouseOut='popOut()'>";
    		newSelValue += strValue;
    		newSelValue += "<SPAN style='display:none'>-</SPAN>";
    		newSelValue += "<SPAN style='display:none'>"+strValuePopup+"</SPAN>";
    		newSelValue += "</SPAN>";
      }
      editablecell.innerHTML = newSelValue;
  	}
  	else
  	{
  		dbsave = obj.value;
      var cellXmlAttrib = editablecell.xmlAttribute;
      if (cellXmlAttrib == 'LynxComment')
      {
        if (obj.value.length < 4)
      		editablecell.innerHTML = "<DIV unselectable='on' class='autoflowDiv' style='width:20px; height:26px; overflow:hidden;'>"+obj.value+"</DIV>";
        else
      		editablecell.innerHTML = "<DIV unselectable='on' class='autoflowDiv' style='width:20px; height:26px; overflow:hidden;' onMouseOver='popUpDynMsg(this.innerText)' onMouseOut='popOut()'>"+obj.value+"</DIV>";

    		if (tblIndex)
    			objRow.cells[parseInt(tblIndex)].innerText = dbsave;
      }
      else if (cellXmlAttrib == 'Description')
      {
        if (obj.value.length < 30)
      		editablecell.innerHTML = "<DIV unselectable='on' class='autoflowDiv' style='height:26px;'>"+obj.value+"</DIV>";
        else
      		editablecell.innerHTML = "<DIV unselectable='on' class='autoflowDiv' style='height:26px;' onMouseOver='popUpDynMsg(this.innerText)' onMouseOut='popOut()'>"+obj.value+"</DIV>";

    		if (tblIndex)
    			objRow.cells[parseInt(tblIndex)].innerText = dbsave;
      }
  	}

    editablecell.onclick =  new Function("", "this.focus()");
    editablecell.style.backgroundColor = editbg;
  	objRow.style.backgroundColor = editbg;
  	objRow.dirty = true;
    SetDirtyFlag(true);
    //gbDirtyFlag = true;
    gsUpdSummFromDetails = true;
  	editablecell = null;
    resizeScrollTable(document.getElementById("EstScrollTable"));
	}


	function EditableCellDblClick(obj, size, lines, UItype, DBtype, xmlNode)
	{
		if (gsDetailEditableFlag == 0 || obj.parentElement.deleteDoc == "true") return;
		if (gsCRUD.indexOf("U") == -1 ) return;
		if (editablecell != null)
		{
			EditableCellClose(editablecell.firstChild);
		}
		var oRow = obj.parentElement;
  	var objValue = obj.innerText.Trim();
    if (editablecell == null)
		{
			editablecell = obj;
			var sHTML = "";
			if (UItype == "Money")
			{
        if (objValue == "Incl.")
        {
          var isChecked = true;
          objValue = oRow.cells[39].innerText;
        }
        sHTML = "<input type='Text' id='editableInput' class='InputField' size='"+size+"' maxlength='"+size+"' onbeforedeactivate='EditableCellCheckClose(this)' onkeypress='NumbersOnly(event); ";
				sHTML += "EditableCellKeyPress(this);' Scale='2' Precision='9' dbtype='money' value='"+objValue+"' cbType='true' >";
				sHTML += "<br>";
        if (isChecked == true)
				  sHTML += "Incl.<input type='checkbox' CHECKED id='editableCkBox' style='cursor:default' onClick='EditableCellChange(this)'>";
        else
  				sHTML += "Incl.<input type='checkbox' id='editableCkBox' style='cursor:default' onClick='EditableCellChange(this)'>";
      }

			else if (UItype == "Hours")
			{
        if (objValue == "Incl.")
        {
          var isChecked = true;
          if (editablecell.xmlAttribute == "LaborHoursAgreed")
            objValue = oRow.cells[40].innerText;
          else if (editablecell.xmlAttribute == "RefinishLaborHoursAgreed")
            objValue = oRow.cells[41].innerText;
        }
        sHTML = "<input type='Text' id='editableInput' class='InputField' size='"+size+"' maxlength='"+size+"' onbeforedeactivate='EditableCellCheckClose(this)' onkeypress='NumbersOnly(event); ";
				sHTML += "EditableCellKeyPress(this);' Scale='1' Precision='5' dbtype='decimal' value='"+objValue+"' cbType='true'>";
				sHTML += "<br>";
        if (isChecked == true)
				  sHTML += "Incl.<input type='checkbox' CHECKED id='editableCkBox' style='cursor:default' onClick='EditableCellChange(this)'>";
        else
  				sHTML += "Incl.<input type='checkbox' id='editableCkBox' style='cursor:default' onClick='EditableCellChange(this)'>";
			}

			else if (UItype == "Qty")
			{
				sHTML = "<input type='Text' id='editableInput' class='InputField' size='"+size+"' maxlength='"+size+"' onkeypress='NumbersOnly(event); ";
				sHTML +="EditableCellKeyPress(this);' Scale='0' Precision='5' dbtype='decimal' value='"+objValue+"' onblur='EditableCellBlur(this)'>";
			}

			else if (UItype == "Dropdown")
			{
        var selCode = objValue.substring(0, objValue.indexOf('-'));
        sHTML = GetSelectInfo(xmlNode, selCode);
			}

			else
			{
				if (UItype == null && (lines != null && lines > 1))
					sHTML = "<textarea id='editableInput'  style='width:200px' cols='"+size+"' onkeypress='EditableCellKeyPress(this)' onfocusout='EditableCellClose(this)'>"+objValue+"</textarea>"
				else
					sHTML = GetSelectInfo(UItype, obj);
			}

			obj.onclick = '';
      obj.innerHTML= sHTML;
      if (oRow.newRowItem == true)
        oRow.scrollIntoView(true);
			objInput = document.getElementById("editableInput");
			//some controls don't handle select
			try { objInput.select(); }catch(e) {};
      objInput.focus();
		  resizeScrollTable(document.getElementById("EstScrollTable"));
		}
	}

  
  function GetSelectInfo(xmlNodeName, selCode)
  {
   	var strXPath = '//Reference[@List="'+xmlNodeName+'"]';
    var objNodeList = eval("xml"+xmlNodeName+".documentElement.selectNodes('"+ strXPath +"')");
  
  	var strRet = "<select id='editableInput' name='"+xmlNodeName+"' onchange='EditableCellChange(this)' onkeypress='EditableCellKeyPress(this)' onfocusout='EditableCellClose(this)'>"
  
  	if ( objNodeList.length )
    {
  		strRet += "<option value='' ";
  		if (selCode == '') strRet += "SELECTED ";
  		strRet += "></option>";
  	  var objNode;
      var nLength = objNodeList.length;
  	  for (n=0; n < nLength; n++ )
  	  {
        objNode = objNodeList.nextNode();
    		strRet += "<option value='"+objNode.selectSingleNode("@ReferenceID").text+"' ";
    		if (selCode == objNode.selectSingleNode("@ReferenceID").text) strRet += "SELECTED ";
    		strRet += ">";
    		strRet += objNode.selectSingleNode("@Name").text + "</option>";
  	  }
  	}
  	strRet += "</select>";
  	return strRet;
  }


	function MarkDelCellDblClick(rowIndex)
  {
    if (gsCRUD.indexOf("D") == -1 ) return;
    var objRow = tblEstimateDetails.rows[rowIndex];
		if (objRow.deleteDoc == true)
    {
  		objRow.deleteDoc = false;
  		objRow.bgColor = objRow.savebgColor;
  		objRow.dirty = false;
      //gbDirtyFlag = false;
    }
    else
    {
  		objRow.deleteDoc = true;
  		objRow.savebgColor = objRow.bgColor;
  		objRow.bgColor = '#FFCC99';
  		objRow.dirty = true;
      //gbDirtyFlag = true;
      gsUpdSummFromDetails = true;
      SetDirtyFlag(true);

    }
    if (oPopup.isOpen)
      oPopup.hide();
  }
  

  function delNewRow(rowIndex)
  {
    var tblRows = tblEstimateDetails.rows;
    var numRows = tblRows.length;
    for(var i=rowIndex+1; i<numRows; i++)
    {
      if (tblRows[i].cells[18].innerText == "Ins")
      {
        var newDetailNumber = tblRows[i].DetailNumber -1;
        tblRows[i].DetailNumber = newDetailNumber;
        tblRows[i].firstChild.innerText = newDetailNumber;
      }
    }
    tblEstimateDetails.deleteRow(rowIndex);
    if (oPopup.isOpen)
      oPopup.hide();
  }


  function insNewRow()
  {
    var tblRows = tblEstimateDetails.rows;
    var lastRow = tblRows[tblRows.length-1];
    var newDetailNumber = Number(lastRow.DetailNumber)+1;
    var oNewRow = tblEstimateDetails.insertRow(-1);
    var oNewCell = null;
    var iLength = lastRow.cells.length;
    for (i = 0; i < iLength; i++)
    {
      oNewCell = oNewRow.insertCell();
      oNewCell.unselectable = "on";
      oNewCell.className="GridTypeTD";
      oNewCell.onclick = new Function("", "this.focus()");
      if (i < 18)
        oNewCell.innerHTML = "&nbsp;";
      if (i > 17)
        oNewCell.style.display = "none";
    }
    oNewRow.unselectable = "on";
    oNewRow.newRowItem = true;
    oNewRow.oncontextmenu = new Function("", "callContextMenu(event.x-1,event.y-1,this)");
    oNewRow.onmouseover = new Function("", "rowMouseOver(this)");
    oNewRow.onmouseout = new Function("", "rowMouseOut(this)");
    oNewRow.height = "30";
    oNewRow.dirty = "false";
    oNewRow.bgColor= "#CCCC99";
    oNewRow.DetailNumber = newDetailNumber;
    oNewRow.style.padding = "2px";
    oNewRow.deleteDoc = "false";

    for (x = 11; x < 18; x++)
    {
      oNewRow.cells[x].style.cursor = "text";
      oNewRow.cells[x].onmouseover = new Function("", "EditableCellOver(this)");
      oNewRow.cells[x].onmouseout = new Function("", "EditableCellOut(this)");
      oNewRow.cells[x].nowrap = "no";
    }

    oNewRow.cells[0].innerText = newDetailNumber;

    oNewRow.cells[1].style.cursor = "text";
    oNewRow.cells[1].onmouseover = new Function("", "EditableCellOver(this)");
    oNewRow.cells[1].onmouseout = new Function("", "EditableCellOut(this)");
    oNewRow.cells[1].nowrap = "no";
    oNewRow.cells[1].xmlAttribute = "OperationCd";
    oNewRow.cells[1].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Dropdown','','OperationType')");
    oNewRow.cells[1].style.textAlign = "center";
    oNewRow.cells[1].IDIndex = "19";
    oNewRow.cells[1].style.border = "1px solid gray";

    oNewRow.cells[2].style.cursor = "text";
    oNewRow.cells[2].onmouseover = new Function("", "EditableCellOver(this)");
    oNewRow.cells[2].onmouseout = new Function("", "EditableCellOut(this)");
    oNewRow.cells[2].nowrap = "no";
    oNewRow.cells[2].xmlAttribute = "PartTypeAgreedCD";
    oNewRow.cells[2].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Dropdown','','PartType')");
    oNewRow.cells[2].style.textAlign = "center";
    oNewRow.cells[2].IDIndex = "43";
    oNewRow.cells[2].style.border = "1px solid gray";
    
    oNewRow.cells[3].xmlAttribute = "Description";
    oNewRow.cells[3].style.cursor = "text";
    oNewRow.cells[3].onmouseover = new Function("", "EditableCellOver(this)");
    oNewRow.cells[3].onmouseout = new Function("", "EditableCellOut(this)");
    oNewRow.cells[3].ondblclick = new Function("", "EditableCellDblClick(this,26,3)");
    oNewRow.cells[3].style.textAlign = "left";
    oNewRow.cells[3].IDIndex = "21";
    oNewRow.cells[3].style.border = "1px solid gray";

    oNewRow.cells[10].style.backgroundColor = "#EFEFEF";

    oNewRow.cells[11].xmlAttribute = "APDPartCategoryAgreedCD";
    oNewRow.cells[11].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Dropdown','','APDPartCategory')");
    oNewRow.cells[11].style.textAlign = "center";
    oNewRow.cells[11].IDIndex = "37";
    oNewRow.cells[11].style.border = "1px solid gray";

    oNewRow.cells[12].xmlAttribute = "QuantityAgreed";
    oNewRow.cells[12].dataType = "Qty";
    oNewRow.cells[12].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Qty')");
    oNewRow.cells[12].style.textAlign = "center";
    oNewRow.cells[12].IDIndex = "38";
    oNewRow.cells[12].style.border = "1px solid gray";

    oNewRow.cells[13].xmlAttribute = "PriceAgreed";
    oNewRow.cells[13].dataType = "Money";
    oNewRow.cells[13].ondblclick = new Function("", "EditableCellDblClick(this,10,1,'Money')");
    oNewRow.cells[13].style.textAlign = "right";
    oNewRow.cells[13].IDIndex = "39";
    oNewRow.cells[13].style.border = "1px solid gray";

    oNewRow.cells[14].xmlAttribute = "LaborHoursAgreed";
    oNewRow.cells[14].dataType = "Hours";
    oNewRow.cells[14].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Hours')");
    oNewRow.cells[14].style.textAlign = "right";
    oNewRow.cells[14].IDIndex = "40";
    oNewRow.cells[14].style.border = "1px solid gray";

    oNewRow.cells[15].xmlAttribute = "RefinishLaborHoursAgreed";
    oNewRow.cells[15].dataType = "Hours";
    oNewRow.cells[15].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Hours')");
    oNewRow.cells[15].style.textAlign = "right";
    oNewRow.cells[15].IDIndex = "41";
    oNewRow.cells[15].style.border = "1px solid gray";

    oNewRow.cells[16].xmlAttribute = "LaborTypeAgreedCD";
    oNewRow.cells[16].ondblclick = new Function("", "EditableCellDblClick(this,5,1,'Dropdown','','LaborType')");
    oNewRow.cells[16].style.textAlign = "center";
    oNewRow.cells[16].IDIndex = "42";
    oNewRow.cells[16].style.border = "1px solid gray";

    oNewRow.cells[17].xmlAttribute = "LynxComment";
    oNewRow.cells[17].ondblclick = new Function("", "EditableCellDblClick(this,26,3)");
    oNewRow.cells[17].style.textAlign = "left";
    oNewRow.cells[17].style.border = "1px solid gray";

    oNewRow.cells[18].xmlAttribute = "Comment";
    oNewRow.cells[18].innerText = "Ins"; //mark this row as inserted

    oNewRow.cells[24].xmlAttribute = "PriceIncludedOriginalFlag";
    oNewRow.cells[24].innerText = "0";
    oNewRow.cells[26].xmlAttribute = "PriceChangedFlag";
    oNewRow.cells[26].innerText = "0";
    oNewRow.cells[28].xmlAttribute = "LaborHoursIncludedOriginalFlag";
    oNewRow.cells[28].innerText = "0";
    oNewRow.cells[30].xmlAttribute = "LaborHoursChangedFlag";
    oNewRow.cells[30].innerText = "0";
    oNewRow.cells[32].xmlAttribute = "RefinishLaborIncludedOriginalFlag";
    oNewRow.cells[32].innerText = "0";
    oNewRow.cells[34].xmlAttribute = "RefinishLaborHoursChangedFlag";
    oNewRow.cells[34].innerText = "0";
    oNewRow.cells[34].xmlAttribute = "RefinishLaborHoursChangedFlag";
    oNewRow.cells[34].innerText = "0";

    oNewRow.cells[44].xmlAttribute = "SysLastUpdatedDate";
    oNewRow.cells[44].innerText = new Date();

    oNewRow.cells[45].xmlAttribute = "ManualLineFlag";
    oNewRow.cells[45].innerText = "0";
    
    oNewRow.cells[46].xmlAttribute = "PriceIncludedAgreedFlag";
    oNewRow.cells[46].innerText = "0";

    oNewRow.cells[47].xmlAttribute = "LaborHoursIncludedAgreedFlag";
    oNewRow.cells[47].innerText = "0";

    oNewRow.cells[48].xmlAttribute = "RefinishLaborIncludedAgreedFlag";
    oNewRow.cells[48].innerText = "0";

    oNewRow.scrollIntoView(true);
    if (oPopup.isOpen)
      oPopup.hide();
  }


  function SendAuditFax()
  {
    //btn_SendAuditFax.CCDisabled = true;
    var sName;
    var oDiv;
    var tblObj;
    var idxLength;
    var sAuditChangesIns = "";
    var sAuditChangesDel = "";
    var sAuditChangesUpd = "";
    var sAuditChanges = "";
    var cLength;
    var oRow;
    
    //Gather changes from the Details screen
    sName = "content12";
    oDiv = document.getElementById(sName);
    tblObj = document.getElementById("tbl"+oDiv.name);
    idxLength = tblObj.rows.length;

    for (var idx=0; idx < idxLength; idx++)
    {
      if (tblObj.rows[idx].newRowItem && tblObj.rows[idx].newRowItem == true)
      {
        if (tblObj.rows[idx].cells[3].dirty == true)
          sAuditChangesIns += "Description: " + tblObj.rows[idx].cells[3].innerText + "\n";
        if (tblObj.rows[idx].cells[12].dirty == true)
          sAuditChangesIns += "Quantity: " + tblObj.rows[idx].cells[12].innerText + "\n";
        if (tblObj.rows[idx].cells[13].dirty == true)
          sAuditChangesIns += "Price: " + tblObj.rows[idx].cells[13].innerText + "\n";
        if (tblObj.rows[idx].cells[14].dirty == true)
          sAuditChangesIns += "Labor Hours: " + tblObj.rows[idx].cells[14].innerText + "\n";
        if (tblObj.rows[idx].cells[15].dirty == true)
          sAuditChangesIns += "Refinish Labor Hours: " + tblObj.rows[idx].cells[15].innerText + "\n";
        if (tblObj.rows[idx].cells[16].dirty == true)
          sAuditChangesIns += "Labor Type: " + tblObj.rows[idx].cells[16].innerText + "\n";
        if (tblObj.rows[idx].cells[17].innerText != "")
          sAuditChangesIns += "Comments: " + tblObj.rows[idx].cells[17].innerText + "\n";

        if (sAuditChangesIns != "")
        {
          sAuditChanges += tblObj.rows[idx].DetailNumber + "~Inserted.\n";
          sAuditChanges += sAuditChangesIns;
          sAuditChanges += "|";
        }
      }

      else if (tblObj.rows[idx].deleteDoc && tblObj.rows[idx].deleteDoc == true)
      {
        if (tblObj.rows[idx].cells[17].innerText != "")
          sAuditChangesDel += "Comments: " + tblObj.rows[idx].cells[17].innerText + "\n";

        if (sAuditChangesDel != "")
        {
          sAuditChanges += tblObj.rows[idx].DetailNumber + "~Deleted.\n";
          sAuditChanges += sAuditChangesDel;
          sAuditChanges += "|";
        }
      }

      else if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
      {
        if (tblObj.rows[idx].cells[12].dirty == true)
          sAuditChangesUpd = "Quantity: changed from " + tblObj.rows[idx].cells[49].innerText + " to " + tblObj.rows[idx].cells[12].innerText + "\n";
        if (tblObj.rows[idx].cells[13].dirty == true)
          sAuditChangesUpd += "Price: changed from " + tblObj.rows[idx].cells[50].innerText + " to " + tblObj.rows[idx].cells[13].innerText + "\n";
        if (tblObj.rows[idx].cells[14].dirty == true)
          sAuditChangesUpd += "Labor Hours: changed from " + tblObj.rows[idx].cells[51].innerText + " to " + tblObj.rows[idx].cells[14].innerText + "\n";
        if (tblObj.rows[idx].cells[15].dirty == true)
          sAuditChangesUpd += "Refinish Labor Hours: changed from " + tblObj.rows[idx].cells[52].innerText + " to " + tblObj.rows[idx].cells[15].innerText + "\n";
        if (tblObj.rows[idx].cells[16].dirty == true)
          sAuditChangesUpd += "Labor Type: changed from " + tblObj.rows[idx].cells[53].innerText + " to " + tblObj.rows[idx].cells[16].innerText + "\n";
        if (tblObj.rows[idx].cells[17].innerText != "")
          sAuditChangesUpd += "Comments: " + tblObj.rows[idx].cells[17].innerText + "\n";
          
        if (sAuditChangesUpd != "")
        {
          sAuditChanges += tblObj.rows[idx].DetailNumber + "~Updated.\n";
          sAuditChanges += sAuditChangesUpd;
          sAuditChanges += "|";
        }
      }
    }
    

    //Gather changes from the Summary screen
    //Parts line items
    if (document.getElementById("tblrow3").dirty && document.getElementById("tblrow3").dirty == true)
      sAuditChanges += getPartsUpdates("tblrow3", "Parts w/ Markup");

    if (document.getElementById("tblrow4").dirty && document.getElementById("tblrow4").dirty == true)
      sAuditChanges += getPartsUpdates("tblrow4", "Parts w/ Discount");
      
    //Supplies line items
    if (document.getElementById("tblrow8").dirty && document.getElementById("tblrow8").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow8", "Body Supplies");

    if (document.getElementById("tblrow9").dirty && document.getElementById("tblrow9").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow9", "Paint Supplies");
  
    //Labor line items
    if (document.getElementById("tblrow12").dirty && document.getElementById("tblrow12").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow12", "Body Labor");

    if (document.getElementById("tblrow13").dirty && document.getElementById("tblrow13").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow13", "Mechanical Labor");

    if (document.getElementById("tblrow14").dirty && document.getElementById("tblrow14").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow14", "Frame Labor");

    if (document.getElementById("tblrow15").dirty && document.getElementById("tblrow15").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow15", "Paint Labor");

    if (document.getElementById("tblrow16").dirty && document.getElementById("tblrow16").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow16", "Refinish Labor");

    if (document.getElementById("tblrow17").dirty && document.getElementById("tblrow17").dirty == true)
      sAuditChanges += getSuppliesUpdates("tblrow17", "Other Labor");

    //Additional Charges line items
    var sAddCharges;
    var oTblRowId;
    var iRowMax;

    iRowMax = 22;
    for (var iTblRow=19; iTblRow < iRowMax; iTblRow++)
    {
      oTblRowId = "tblrow" + iTblRow;
      if (document.getElementById(oTblRowId).dirty && document.getElementById(oTblRowId).dirty == true)
      {
        sAddCharges = getAddChargesUpdates(oTblRowId, "Additional Charges");
        if (sAddCharges != 0)
          sAuditChanges += sAddCharges;
        else
          return;
      }
    }
    
    //Taxes line items
    iRowMax = 29;
    var iTaxesChanged = false;
    for (var iTblRow=25; iTblRow < iRowMax; iTblRow++)
    {
      oTblRowId = "tblrow" + iTblRow;
      if (document.getElementById(oTblRowId).dirty && document.getElementById(oTblRowId).dirty == true)
        iTaxesChanged = true;
    }
    if (iTaxesChanged == true)
       sAuditChanges += "Taxes~These updates may have changed tax amounts.|";
    

    //Adjustments line items
    if (document.getElementById("tblrow32").dirty && document.getElementById("tblrow32").dirty == true)
      sAuditChanges += getAdjustmentUpdates("tblrow32", "Deductible");

    if (document.getElementById("tblrow33").dirty && document.getElementById("tblrow33").dirty == true)
      sAuditChanges += getAdjustmentUpdates("tblrow33", "Appearance/Allowance");

    if (document.getElementById("tblrow34").dirty && document.getElementById("tblrow34").dirty == true)
      sAuditChanges += getAdjustmentUpdates("tblrow34", "Betterment");

    if (document.getElementById("tblrow38").dirty && document.getElementById("tblrow38").dirty == true)
      sAuditChanges += getAdjustmentUpdates("tblrow38", "Other Adjustments");

    if (sAuditChanges != "")
    {
      //confirm the request
      if (YesNoMessage("Confirm Send Fax", "Do you want to fax the shop the changes made?\nChanges will be saved before sent.") != "Yes") {
        btn_SendAuditFax.CCDisabled = false;
        return;
      }

      frmFaxAuditChanges.DocumentID.value = gsDocumentID;
      frmFaxAuditChanges.UserID.value = gsUserID;
      frmFaxAuditChanges.EstimateChanges.value = sAuditChanges.replace(/\'/g, "''");// replace single quote to double single quote.
      frmFaxAuditChanges.submit();
      SaveAll();
    }
    else
    {
      ClientWarning("There are no changes in the Details to be faxed to the shop.<br>Please review it and it try again.");
    }
    btn_SendAuditFax.CCDisabled = false;
  }


  //Get changes made to updatable parts items
  function getPartsUpdates(oTblRow, lineItemDesc)
  {
    var oDiv = document.getElementById(oTblRow);
  	var elms = oDiv.getElementsByTagName("INPUT");
    var iLength = elms.length;
    var sAuditChanges = "";
    var sInitialTax;
    var sNewTax;
    
    if (elms[17].value == 1) sInitialTax = "Taxable";
    else  sInitialTax = "Non-Taxable";
    if (elms[7].value == 1) sNewTax = "Taxable";
    else  sNewTax = "Non-Taxable";
    
    sAuditChanges += "Changed from " + escape(elms[15].value) + "%/" + escape(elms[16].value) + " " + sInitialTax; 
    sAuditChanges += " to " + escape(elms[4].value) + "%/" + escape(elms[5].value) + " " + sNewTax;

    if (elms[8].dirty && elms[8].dirty == true)
      sAuditChanges += "\nComments: " + elms[8].value;

    if (sAuditChanges != "")
      return lineItemDesc + "~" + sAuditChanges + "|";
  }


  //Get changes made to updatable supplies items
  function getSuppliesUpdates(oTblRow, lineItemDesc)
  {
    var oDiv = document.getElementById(oTblRow);
  	var elms = oDiv.getElementsByTagName("INPUT");
    var iLength = elms.length;
    var sAuditChanges = "";
    var sInitialTax;
    var sNewTax;
    
    if (elms[17].value == 1) sInitialTax = "Taxable";
    else  sInitialTax = "Non-Taxable";
    if (elms[7].value == 1) sNewTax = "Taxable";
    else  sNewTax = "Non-Taxable";
    
    sAuditChanges += "Changed from " + escape(elms[15].value) + "/" + escape(elms[16].value) + " " + sInitialTax; 
    sAuditChanges += " to " + escape(elms[4].value) + "%/" + escape(elms[5].value) + " " + sNewTax;

    if (elms[8].dirty && elms[8].dirty == true)
      sAuditChanges += "\nComments: " + elms[8].value;

    if (sAuditChanges != "")
      return lineItemDesc + "~" + sAuditChanges + "|";
  }


  //Get changes made to updatable additional charges items
  function getAddChargesUpdates(oTblRow, lineItemDesc)
  {
    var oDiv = document.getElementById(oTblRow);
  	var elms = oDiv.getElementsByTagName("INPUT");
    var iLength = elms.length;
    var sAuditChanges = "";
    var sInitialTax;
    var sNewTax;
    var sAddChargeName;

    //Verify a Charge Type is selected for changes made.
    if (elms[0].value == "")
    {
      ClientWarning("Changes made to row " + (Number(oTblRow.substring(oTblRow.length-2, oTblRow.length)) - 18 ) + " of Additional Charges require a Type to be selected from the drop-down before continuing.<br>Please review it and it try again.");
      return 0;
    }

    var sRow = "Row " + (Number(oTblRow.substring(oTblRow.length-2, oTblRow.length)) - 18 );

    if (elms[0].value == "") sAddChargeName = "none";
    if (elms[0].value == 15) sAddChargeName = "Storage";
    if (elms[0].value == 16) sAddChargeName = "Sublet";
    if (elms[0].value == 17) sAddChargeName = "Towing";
    if (elms[0].value == 18) sAddChargeName = "Other";

    if (elms[17].value == 1) sInitialTax = "Taxable";
    else  sInitialTax = "Non-Taxable";
    if (elms[6].value == 1) sNewTax = "Taxable";
    else  sNewTax = "Non-Taxable";
    
    sAuditChanges += sRow + " changed from " + escape(elms[15].value) + "/" + escape(elms[16].value) + " " + sInitialTax;
    sAuditChanges += " to " + sAddChargeName  + "/" + escape(elms[4].value) + " " + sNewTax;

    if (elms[7].dirty && elms[7].dirty == true)
      sAuditChanges += "\nComments: " + elms[7].value;

    if (sAuditChanges != "")
      return lineItemDesc + "~" + sAuditChanges + "|";
  }


  //Get changes made to updatable taxes items
  function getTaxesUpdates(oTblRow, lineItemDesc)
  {
    var oDiv = document.getElementById(oTblRow);
  	var elms = oDiv.getElementsByTagName("INPUT");
    var iLength = elms.length;
    var sAuditChanges = "";
    var sTaxTypeName;
    var sTaxTotalName;

    //Verify a Tax Type and Total is selected for changes made.
    if (elms[0].value == "" || elms[1].value == "")
    {
      ClientWarning("Changes made to row " + (Number(oTblRow.substring(oTblRow.length-2, oTblRow.length)) - 24 ) + " of Taxes require a Tax Type and a Total to be selected from the drop-down before continuing.<br>Please review it and it try again.");
      return 0;
    }

    var sRow = "Row " + (Number(oTblRow.substring(oTblRow.length-2, oTblRow.length)) - 24 );

    if (elms[0].value == "") sTaxTypeName = "blank";
    if (elms[0].value == 21) sTaxTypeName = "County";
    if (elms[0].value == 22) sTaxTypeName = "Municipal";
    if (elms[0].value == 23) sTaxTypeName = "Sales";
    if (elms[0].value == 24) sTaxTypeName = "Other";

    if (elms[1].value == "") sTaxTotalName = "blank";
    if (elms[1].value == "AC") sTaxTotalName = "Add Chrg";
    if (elms[1].value == "LB") sTaxTotalName = "Labor";
    if (elms[1].value == "MT") sTaxTotalName = "P&M";
    if (elms[1].value == "PT") sTaxTotalName = "Parts";
    if (elms[1].value == "SP") sTaxTotalName = "Supplies";
    if (elms[1].value == "ST") sTaxTotalName = "Subtotal";
    if (elms[1].value == "OT") sTaxTotalName = "Other";

    sAuditChanges += sRow + " changed from " + escape(elms[15].value) + "/" + escape(elms[16].value) + " " + escape(elms[18].value) + "@" + escape(elms[17].value) + "%=" + escape(elms[19].value);
    sAuditChanges += " to " + sTaxTypeName  + "/" + sTaxTotalName  + " " + escape(elms[6].value) + "@" + escape(elms[5].value) + "%=" + escape(elms[7].value);

    if (elms[8].dirty && elms[8].dirty == true)
      sAuditChanges += "\nComments: " + elms[8].value;

    if (sAuditChanges != "")
      return lineItemDesc + "~" + sAuditChanges + "|";
  }


  //Get changes made to updatable adjustment items
  function getAdjustmentUpdates(oTblRow, lineItemDesc)
  {
    var oDiv = document.getElementById(oTblRow);
  	var elms = oDiv.getElementsByTagName("INPUT");
    var iLength = elms.length;
    var sAuditChanges = "";
    
    sAuditChanges += "Changed from " + escape(elms[15].value); 
    sAuditChanges += " to " + escape(elms[2].value) ;

    if (elms[4].dirty && elms[4].dirty == true)
      sAuditChanges += "\nComments: " + elms[4].value;
      
    if (sAuditChanges != "")
      return lineItemDesc + "~" + sAuditChanges + "|";
  }


  function getOriginalVal(row, attrib)
  {
    var cLength = row.cells.length;
    for (var x=0; x < cLength; x++)
  	{
      if (row.cells[x].xmlAttribute == attrib)
      var retVal = row.cells[x].innerText;
    }
    return retVal;
  }

  function details_ADS_Pressed()
  {
    var sRequest;
    var sName = "content12";

    var sAction = "Delete";
    var sProc = "uspEstimateDetailDelDetail";

    var oDiv = document.getElementById(sName)
    var tblObj = document.getElementById("tbl"+oDiv.name);
    var idxLength = tblObj.rows.length;

    for (var idx=0; idx < idxLength; idx++)
    {
      if (tblObj.rows[idx].deleteDoc && tblObj.rows[idx].deleteDoc == true)
      {
        sRequest = "DetailNumber="+tblObj.rows[idx].DetailNumber+"&";
        sRequest += "DocumentID="+gsDocumentID+"&";
        sRequest += "UserID="+gsUserID+"&";
        sRequest += "SysLastUpdatedDate="+tblObj.rows[idx].cells[44].innerText;
        
        // // alert("Details Delete sRequest = "+sRequest);

        //var sProc = oDiv.getAttribute(sAction);
        var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
        retArray = ValidateRS(coObj);
        if (retArray[1] == 1)
        {
          tblObj.rows[idx].deleteDoc = false;
          tblObj.rows[idx].dirty = false;
        }
        else
        {
          ClientWarning("Error saving row ("+tblObj.rows[idx].DetailNumber+") in the Estimate Details. (Delete)");
          return;
        }
      }
    }

    sAction = "Update";
    sProc = "uspEstimateDetailUpdDetail";

    for (var idx=0; idx < idxLength; idx++)
    {
      if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
      {
        sRequest = "DetailNumber="+tblObj.rows[idx].DetailNumber+"&";
        sRequest += "Comment="+escape(tblObj.rows[idx].cells[18].innerText)+"&";
        sRequest += "OperationCd="+escape(tblObj.rows[idx].cells[19].innerText)+"&";
        sRequest += "PartTypeOriginalCD="+tblObj.rows[idx].cells[20].innerText+"&";
        sRequest += "Description="+escape(tblObj.rows[idx].cells[21].innerText)+"&";
        sRequest += "APDPartCategoryOriginalCD="+tblObj.rows[idx].cells[22].innerText+"&";
        sRequest += "QuantityOriginal="+tblObj.rows[idx].cells[23].innerText+"&";
        sRequest += "PriceIncludedOriginalFlag="+tblObj.rows[idx].cells[24].innerText+"&";
        sRequest += "PriceOriginal="+tblObj.rows[idx].cells[25].innerText+"&";
        sRequest += "PriceChangedFlag="+tblObj.rows[idx].cells[26].innerText+"&";
        sRequest += "DBPriceOriginal="+tblObj.rows[idx].cells[27].innerText+"&";
        sRequest += "LaborHoursIncludedOriginalFlag="+tblObj.rows[idx].cells[28].innerText+"&";
        sRequest += "LaborHoursOriginal="+tblObj.rows[idx].cells[29].innerText+"&";
        sRequest += "LaborHoursChangedFlag="+tblObj.rows[idx].cells[30].innerText+"&";
        sRequest += "DBLaborHoursOriginal="+tblObj.rows[idx].cells[31].innerText+"&";
        sRequest += "RefinishLaborIncludedOriginalFlag="+tblObj.rows[idx].cells[32].innerText+"&";
        sRequest += "RefinishLaborHoursOriginal="+tblObj.rows[idx].cells[33].innerText+"&";
        sRequest += "RefinishLaborHoursChangedFlag="+tblObj.rows[idx].cells[34].innerText+"&";
        sRequest += "DBRefinishLaborHoursOriginal="+tblObj.rows[idx].cells[35].innerText+"&";
        sRequest += "LaborTypeOriginalCD="+tblObj.rows[idx].cells[36].innerText+"&";
        sRequest += "APDPartCategoryAgreedCD="+tblObj.rows[idx].cells[37].innerText+"&";
        sRequest += "QuantityAgreed="+tblObj.rows[idx].cells[38].innerText+"&";
        sRequest += "PriceAgreed="+tblObj.rows[idx].cells[39].innerText+"&";
        sRequest += "LaborHoursAgreed="+tblObj.rows[idx].cells[40].innerText+"&";
        sRequest += "RefinishLaborHoursAgreed="+tblObj.rows[idx].cells[41].innerText+"&";
        sRequest += "LaborTypeAgreedCD="+tblObj.rows[idx].cells[42].innerText+"&";
        sRequest += "PartTypeAgreedCD="+tblObj.rows[idx].cells[43].innerText+"&";
        sRequest += "LynxComment="+escape(tblObj.rows[idx].cells[17].innerText)+"&";
        sRequest += "DocumentID="+gsDocumentID+"&";
        sRequest += "UserID="+gsUserID+"&";
        sRequest += "UserName=''&";
        sRequest += "ManualLineFlag="+tblObj.rows[idx].cells[45].innerText+"&";
        sRequest += "PriceIncludedAgreedFlag="+tblObj.rows[idx].cells[46].innerText+"&";
        sRequest += "LaborHoursIncludedAgreedFlag="+tblObj.rows[idx].cells[47].innerText+"&";
        sRequest += "RefinishLaborIncludedAgreedFlag="+tblObj.rows[idx].cells[48].innerText+"&";
        sRequest += "SysLastUpdatedDate="+tblObj.rows[idx].cells[44].innerText;
        
        //alert("Details Update sRequest = "+sRequest);
        //var sProc = oDiv.getAttribute(sAction);
        //var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );

        var coObj = RSExecute("/rs/RSADSAction.asp", "RadExecute", sProc, sRequest );
        retArray = ValidateRS(coObj);
        if (retArray[1] == 1)
        {
          //alert(retArray[0]);
          //update the last Updated date...
          var objXML = new ActiveXObject("Microsoft.XMLDOM");
          objXML.loadXML(retArray[0]);
               
          var rootNode = objXML.documentElement.selectSingleNode("/Root");
          var EstimateDetailNode = rootNode.selectSingleNode("@SysLastUpdatedDate");
          if (EstimateDetailNode)
          {
            tblObj.rows[idx].cells[44].innerText = EstimateDetailNode.nodeValue;
            tblObj.rows[idx].dirty = false;
            oRow = tblObj.rows[idx];
            //clear the cell borders
            var xLength = oRow.cells.length;
            for (var x=0; x < xLength; x++)
          	{
              if (oRow.cells[x].dirty == true)
              {
                oRow.cells[x].style.borderLeft = '1px solid #FFF0F5';
                oRow.cells[x].style.borderTop = '1px solid #FFF0F5';
                oRow.cells[x].style.borderRight = '1px solid #BBBBBB';
                oRow.cells[x].style.borderBottom = '1px solid #BBBBBB';
              }
            }
          }
        }
        else
        {
          ClientWarning("Error saving row ("+tblObj.rows[idx].DetailNumber+") in the Estimate Details. (Update)");
          return;
        }
      }
    }
  }

  
  //format each numeric input field to currency
  function detailsFormatFields()
  {
		var tblObj = document.getElementById("tblEstimateDetails");
		var oRow;
    var idxLength = tblObj.rows.length;
    for (var idx=0; idx < idxLength; idx++)
  	{
  		oRow = tblObj.rows[idx];
      var xLength = oRow.cells.length;
      if (oRow.cells[19].innerText == 'COM' && oRow.cells[18].innerText != 'Ins')
      {
        for (var x=0; x < xLength; x++)
      	{
          // Original columns
          if (x == 2 || x == 4 || x == 5 || x == 6 || x == 7 || x == 8 || x == 9)
            oRow.cells[x].innerText = " ";
          // Agreed columns
          if (x == 11 || x == 12 || x == 13 || x == 14 || x == 15 || x == 16)
          {
            oRow.cells[x].innerText = " ";
            oRow.cells[x].ondblclick = "";
            oRow.cells[x].onmouseover = "";
            oRow.cells[x].onmouseout = "";
            oRow.cells[x].style.cursor = "default";
          }
        }
      }
      else
      {
        for (var x=0; x < xLength; x++)
      	{
          var changedFlag = false;
          if (x == 6 || x == 13)
          {
            if (oRow.cells[x].innerText.indexOf('*') != -1)
              changedFlag = true;
            if (!isNaN(oRow.cells[x].firstChild.nodeValue))
              oRow.cells[x].firstChild.nodeValue = detailsFormatCurrencyStr(oRow.cells[x].firstChild.nodeValue, changedFlag);
          }
  
          if (x == 7 || x == 8 || x == 14 || x == 15)
          {
            if (oRow.cells[x].innerText.indexOf('*') != -1)
              changedFlag = true;
            if (!isNaN(oRow.cells[x].firstChild.nodeValue))
              oRow.cells[x].firstChild.nodeValue = detailsFormatHoursStr(oRow.cells[x].firstChild.nodeValue, changedFlag);
          }
        }
      }

      if (oRow.cells[5].innerText == '0')
        oRow.cells[5].innerText = ' ';
      if (oRow.cells[12].innerText == '0')
        oRow.cells[12].innerText = ' ';
    }
  }

  //currency formating
  function detailsFormatCurrencyStr(X, changedFlag)
  {
    if (X == '.0' && changedFlag == true)
      return X = '0.0';
    if (isNaN(X) || X == '.' || X == '0' || X == '.0' || X == ' ')
      return X = ' ';
    else
      return X = formatcurrency(X);
  }


  //hours formating
  function detailsFormatHoursStr(X, changedFlag)
  {
    if (X == '.0' && changedFlag == true)
      return X = '0.0';
    if (isNaN(X) || X == '.' || X == '0' || X == '.0' || X == ' ')
      return X = ' ';
    else
      return X = formathours(X);
  }

  
  function resizeScrollTable(oElement)
  {
  
  	var head = oElement.firstChild;
  	var headTable = head.firstChild;
  	var body = oElement.lastChild;
  	var bodyTable = body.firstChild;
  
  	body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);
  
  	var scrollBarWidth = body.offsetWidth - body.clientWidth;
  
  	// set width of the table in the head
  	headTable.style.width = Math.max(0, Math.max(bodyTable.offsetWidth + scrollBarWidth, oElement.clientWidth));
  
  	// go through each cell in the head and resize
  	var headCells = headTable.rows[0].cells;
  	if (bodyTable.rows.length > 0)
  	{
  		var bodyCells = bodyTable.rows[0].cells;
      var iLength = bodyCells.length;
  		for (var i = 0; i < iLength; i++)
  		{
  			if (bodyCells[i].style.display != "none")
  			{
  				if (i != (headCells.length-1))
  				{
  					var iOff = 0;
  					if (i ==0) iOff = 2;
  					headCells[i].style.width = bodyCells[i].offsetWidth + iOff;
  				}
  				else
  				{
  					headCells[i].style.width = bodyCells[i].offsetWidth + scrollBarWidth;
  				 	bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 ) ;
  				}
  			}
  		}
  	}
  }


  function callContextMenu(x,y,obj)
  {
		if (gsDetailEditableFlag == 0) return;
		if (gsCRUD.indexOf("U") == -1 ) return;
    var rowIndex = obj.sectionRowIndex
    var contextHtml="";
    contextHtml+='<TABLE unselectable="on" STYLE="border:1pt solid #666666" BGCOLOR="#F9F8F7" WIDTH="140" HEIGHT="44" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    if (obj.newRowItem == true)
      contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i0" ONMOUSEOVER="document.all.i0.style.background=\'#B6BDD2\';document.all.i0.style.border=\'1pt solid #0A246A\';document.all.i0.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i0.style.background=\'#F9F8F7\';document.all.i0.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.delNewRow(' +rowIndex+ ');">&nbsp;<IMG SRC="/images/EstMarkDel.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;Delete Line</TD></TR>';
    else
      contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i0" ONMOUSEOVER="document.all.i0.style.background=\'#B6BDD2\';document.all.i0.style.border=\'1pt solid #0A246A\';document.all.i0.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i0.style.background=\'#F9F8F7\';document.all.i0.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.MarkDelCellDblClick(' +rowIndex+ ');">&nbsp;<IMG SRC="/images/EstMarkDel.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;Mark for Deletion</TD></TR>';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i1" ONMOUSEOVER="document.all.i1.style.background=\'#B6BDD2\';document.all.i1.style.border=\'1pt solid #0A246A\';document.all.i1.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i1.style.background=\'#F9F8F7\';document.all.i1.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.insNewRow();">&nbsp;<IMG SRC="/images/EstNewItem.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;Insert New Item</TD></TR>';
    contextHtml+='</TABLE>';
  
    var oPopupBody = oPopup.document.body;
    oPopupBody.innerHTML = contextHtml;
    oPopup.show(x, y, 140, 44, document.body);
  }

  
  // Calculate the totals in Details and update the Summary screen
  function getDetailsTotals()
  {
    var sName = "content12";
    var oDiv = document.getElementById(sName)
    var tblObj = document.getElementById("tbl"+oDiv.name);
    var oRow;
    var PAT_total = 0;
    var PAN_total = 0;
    var POT_total = 0;
    var PON_total = 0;
    var STG_total = 0;
    var SUB_total = 0;
    var TOW_total = 0;
    var OTH_total = 0;
    var Paint_Hrs_total = 0;
    var Body_Hrs_total = 0;
    var Frame_Hrs_total = 0;
    var Mechanical_Hrs_total = 0;
    
    var idxLength = tblObj.rows.length;
    for (var idx=0; idx < idxLength; idx++)
    {
      oRow = tblObj.rows[idx];
      var oRowDeleted = false;

      if (oRow.deleteDoc && (oRow.deleteDoc == 'true' || oRow.deleteDoc == true))
        oRowDeleted = true;
      
      if (oRowDeleted == false)
      {  
        if (oRow.cells[46].innerText != 1) //check for the PriceIncludedAgreedFlag
        {
          if (oRow.cells[37].innerText == 'PAT')  // let's add the Parts Taxable - (1* -Price is ext amount)
            PAT_total = PAT_total + (1 * oRow.cells[39].innerText);
          
          if (oRow.cells[37].innerText == 'PAN')  // let's add the Parts Non-Taxable - (1* -Price is ext amount)
            PAN_total = PAN_total + (1 * oRow.cells[39].innerText);
          
          if (oRow.cells[37].innerText == 'POT')  // let's add the Parts Other Taxable - (1* -Price is ext amount)
            POT_total = POT_total + (1 * oRow.cells[39].innerText);
          
          if (oRow.cells[37].innerText == 'PON')  // let's add the Parts Other Non-Taxable - (1* -Price is ext amount)
            PON_total = PON_total + (1 * oRow.cells[39].innerText);
        }
        
        if (oRow.cells[37].innerText == 'STG')  // let's add the AC Storage
          STG_total = STG_total + (oRow.cells[39].innerText * oRow.cells[38].innerText);
        
        if (oRow.cells[37].innerText == 'SUB')  // let's add the AC Sublet
          SUB_total = SUB_total + (oRow.cells[39].innerText * oRow.cells[38].innerText);
        
        if (oRow.cells[37].innerText == 'TOW')  // let's add the AC Towing
          TOW_total = TOW_total + (oRow.cells[39].innerText * oRow.cells[38].innerText);
        
        if (oRow.cells[37].innerText == 'OTH')  // let's add the AC Other
          OTH_total = OTH_total + (oRow.cells[39].innerText * oRow.cells[38].innerText);
        
        if (oRow.cells[47].innerText != 1) //check for the LaborHoursIncludedAgreedFlag
        {
          if (oRow.cells[42].innerText == 'B')    // let's add the Body Labor Hours
            Body_Hrs_total = Body_Hrs_total + Number(oRow.cells[40].innerText);
          
          if (oRow.cells[42].innerText == 'M')    // let's add the Mechanical Labor Hours
            Mechanical_Hrs_total = Mechanical_Hrs_total + Number(oRow.cells[40].innerText);
          
          if (oRow.cells[42].innerText == 'F')    // let's add the Frame Labor Hours
            Frame_Hrs_total = Frame_Hrs_total + Number(oRow.cells[40].innerText);
        }
  
        if (oRow.cells[48].innerText != 1) //check for the RefinishLaborIncludedAgreedFlag
        {
          Paint_Hrs_total = Paint_Hrs_total + Number(oRow.cells[41].innerText);    // let's add the Paint Labor Hours
        }
      }
    }

    //Format total results in currency/hour
    PAT_total = formatcurrency(PAT_total);
    PAN_total = formatcurrency(PAN_total);
    POT_total = formatcurrency(POT_total);
    PON_total = formatcurrency(PON_total);
    STG_total = formatcurrency(STG_total);
    SUB_total = formatcurrency(SUB_total);
    TOW_total = formatcurrency(TOW_total);
    OTH_total = formatcurrency(OTH_total);
    Paint_Hrs_total = formathours(Paint_Hrs_total);
    Body_Hrs_total = formathours(Body_Hrs_total);
    Frame_Hrs_total = formathours(Frame_Hrs_total);
    Mechanical_Hrs_total = formathours(Mechanical_Hrs_total);

    //now update the totals in Summary
    updTotalsSummary("txtAgreedUnitAmt_1", PAT_total);
    updTotalsSummary("txtAgreedUnitAmt_2", PAN_total);
    updTotalsSummary("txtAgreedUnitAmt_5", POT_total);
    updTotalsSummary("txtAgreedUnitAmt_6", PON_total);
    updTotalsSummary("txtAgreedHrs_8", Body_Hrs_total);
    updTotalsSummary("txtAgreedHrs_9", Paint_Hrs_total);
    updTotalsSummary("txtAgreedHrs_12", Body_Hrs_total);
    updTotalsSummary("txtAgreedHrs_13", Mechanical_Hrs_total);
    updTotalsSummary("txtAgreedHrs_14", Frame_Hrs_total);
    updTotalsSummary("txtAgreedHrs_15", Paint_Hrs_total);
    
    //now update the Additional Charges in Summary
    populateAddChg(OTH_total, 18); //Other
    populateAddChg(STG_total, 15); //Storage
    populateAddChg(SUB_total, 16); //Sublet
    populateAddChg(TOW_total, 17); //Towing

    //now calculate/update the totals in Summary
    calcSpanRows("taxableTotals1");
    calcSpanRows("taxableTotals2");
    calcSpanRows("taxableTotals3");
    calcSpanRows("taxableTotals4");
    calcTaxableTotal(); //calculate taxable totals before updating tax rows
    calcSpanRows("nontaxableTotals");
    getAgreedTotals();

    gsUpdSummFromDetails = false; //reset flag to update Summary
    gsSummaryDirtyFlag = true;
    gsUpdSummFromDetails = false;
    SetDirtyFlag(true);
  }

  
  // update the totals in Summary
  function updTotalsSummary(obj, sValue)
  {
    var objFld = document.getElementById(obj);
    var objRow;
    if (objFld.value != sValue)
    {
      objFld.value = sValue;
      objFld.style.borderColor = "#805050";
      objRow = objFld.parentElement.parentElement;
      objRow.dirty = true;
    }
  }


  // Populates the Additional Charges in Summary with new totals from Details
  function populateAddChg(newTotal, selValueReq)
  {
    var selFound = false;
    var objRow;
    for (var i=19; i < 23; i++) // estimate summary row IDs for Additional Charges 19-22
    {
      var selObj = document.getElementById("selEstimateSummaryTypeID_"+i);
  		var selOptionValue = selObj.options[selObj.selectedIndex].value;
      if (selOptionValue == selValueReq)
      {
        var rplFld = document.getElementById("txtAgreedUnitAmt_"+i);
        if (rplFld.value != newTotal)
        {
          rplFld.value = detailsFormatCurrencyStr(newTotal);
          rplFld.style.borderColor = "#805050";
          objRow = selObj.parentElement.parentElement;
      		objRow.dirty = true;
        }
        selFound = true;
      }
      if (selFound == true) break;
    }
    
    //if select not found and value not equeal to zero, look for an empty select and populate it
    if (selFound == false && newTotal != "0.00")
    {
      for (var i=19; i < 23; i++) // estimate summary row IDs for Additional Charges
      {
        var selObj = document.getElementById("selEstimateSummaryTypeID_"+i);
      	var selIndex = selObj.getAttribute("selectedIndex");
        if (selIndex == 0) //index for an empty select
        {
          int_optionselect(selObj, null, null, selValueReq); //use internal function in CoolSelect to change the select to the request item	
          var rplFld = document.getElementById("txtAgreedUnitAmt_"+i);
          rplFld.value = detailsFormatCurrencyStr(newTotal);
          rplFld.style.borderColor = "#805050";
          objRow = selObj.parentElement.parentElement;
      		objRow.dirty = true;
          selFound = true;
        }
        if (selFound == true) break;
      }
    }
  }
  

  // Update extended amounts in Summary for each row of the called span
  function calcSpanRows(sSpan)
  {
    var oRow = "";
    var oSpan = document.getElementById(sSpan);
    var trs = oSpan.getElementsByTagName("TR");
    var zLength = trs.length;
    for (var z=0; z < zLength; z++)
    {
      oRow = trs[z].getAttribute("rowID");
      if (oRow)
        calcRow(trs[z]);
    }
  }
