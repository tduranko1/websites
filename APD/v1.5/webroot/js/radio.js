
function Radio(layername,length,defaultValue) 
{
	this.layer = layername+"Div";
	this.imgNames = layername+"Img";
	this.inputbox = layername;
	this.length = length
	this.change = RadioChange
	this.value = (defaultValue)? defaultValue : "undefined"

	var imgObj;
	for (var i=0; i < length; i++)
	{
		imgObj = document.all[this.imgNames+i];//document.getElementById(this.imgNames+i);
		imgObj.onmouseover = RadioHover;
		imgObj.onmouseout = RadioHoverOut;
		imgObj.radio = this
		imgObj.index = i;
		//imgObj.state = 0;
		
	}
 
}

function RadioChange(index,value) 
{
  var objLayer = document.all[this.layer];
  if (objLayer)
    if (objLayer.disabled) return;

  var imgObj1;

	for (var i=0; i<this.length; i++) 
	{ 
		changeImage(this.layer,this.imgNames+i,'radio0')
		imgObj1 = document.all[this.imgNames+i];//document.getElementById(this.imgNames+i);
		imgObj1.state = 0;

	  if (index != -1)
	  {
			if (i == index)
			{
				imgObj1.state = 1;
				if (value == "")
				{
					value = imgObj1.nextSibling.data
					this.value = value.substr(1);
				}
			}
		}
		else
		{
		  if (imgObj1.nextSibling.data.substr(1) == value)
		  { 
				imgObj1.state = 1; 
				index = i; 
				
			}
		}
	}
	
	if (index != -1) 
	{
		this.value = value
		eval("document.all." + this.inputbox + ".value = '"+ value + "'");
		changeImage(this.layer,this.imgNames+index,'radio1')
	}
}

function RadioHover()
{
//  if (event.srcElement.parentElement.parentElement.disabled) return;
  var objLayer = document.all[this.radio.layer];
  if (objLayer)
    if (objLayer.disabled) return;

  changeImage(this.radio.layer,this.radio.imgNames+this.index,'radiohover'+this.state)
}
function RadioHoverOut()
{
  //if (event.srcElement.parentElement.parentElement.disabled) return;
  var objLayer = document.all[this.radio.layer];
  if (objLayer)
    if (objLayer.disabled) return;

	changeImage(this.radio.layer,this.radio.imgNames+this.index,'radio'+this.state)
}

// parameters : rbname, vertOrhorz,defaultvalue, value1, value2,.....valueN
function AddRadio()
{
	var args=AddRadio.arguments;
	var rbname = args[0];
	var dir = args[1];  //0 = horz , 1 = vert
	var defaultvalue = args[2];
	
	var sdir = "&nbsp;&nbsp;";
	if (dir == 1) sdir = "<br>";
	
	var simage = "";
	var defstate = "";
	var docstr = "<div unselectable='on' id='"+rbname+"Div'>";
	for(i=0; i < (args.length - 3); i++)
	{
		simage = "/images/radiounsel.png";
		defstate = "0";
		if (args[i+3] == defaultvalue)
		{
			simage = "/images/radiosel.png";
			defstate = "1";
		}
		docstr += "<a unselectable='on' href='#' onClick='RB"+rbname+".change("+i+",\""+args[i+3]+"\")' onFocus='if(this.blur)this.blur()'><img unselectable='on' name='"+rbname+"Img"+i+"' src='"+simage+"' width=13 height=13 border='0' state='"+defstate+"'> "+args[i+3]+"</a>";
		if (i < (args.length-4)) docstr += sdir;
	}
	docstr +="<input type='hidden' name='" + rbname + "' value='" + defaultvalue + "' />";
	docstr += "</div>";

	document.write(docstr)
	eval("RB"+rbname+" = new Radio('"+rbname+"',"+(args.length-3)+",'"+defaultvalue+"');");
}

preload('radio0','/images/radiounsel.png')
preload('radio1','/images/radiosel.png')
preload('radiohover0','/images/radiohover.png')
preload('radiohover1','/images/radioselhv.png')
	