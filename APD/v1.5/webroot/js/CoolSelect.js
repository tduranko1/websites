/*	SelectBox Object
	Description: 
		DHTML object simulating the standard selectbox.  This is used in place of the standard one because
		of z-ordering and display problems when using hide/show divs
	Usage:
*/

// Global variables for style and events

// ** these are in the css for APD **
// var overOptionCss = "background: highlight; color: highlighttext";
// var sizedBorderCss = "2 inset buttonhighlight";

var overOptionCss = "background : url(../images/input_bkgd.png) repeat;\
                     color: black; border-top: 1 solid #CCCCCC; border-bottom: 1 solid #CCCCCC;\
          					 padding-left: 3px; padding-right: 0px; padding-bottom: 0px; padding-top: 0px";
var sizedBorderCss = "0px none buttonhighlight";
var globalSelect;	//This is used when calling an unnamed selectbox with onclick="this.PROPERTY"

/*------------------------------------------------------------------------------------*/
/*	initSelectBoxes - Method 
	Description:
		Called on the load event of a page to initialize all select boxes on the page
	Parameters:
		None
	Return: 
		None
*/	

function initSelectBoxes()
{
	var spans = document.all.tags("SPAN");
	var selects = new Array();
	var index = 0;

	//scan all Spans tags for class name
  var iLength = spans.length;
	for (var i=0; i < iLength; i++)
	{
		if (spans[i].className == "select")
			selects[index++] = spans[i];
	}
	// for each select initialize each individual one
  var jLength = selects.length;
	for (var j=0; j < jLength; j++)
	{
		initSelectBox(selects[j]);
	}
}

/*------------------------------------------------------------------------------------*/
/*	initSelectBox - Method 
	Description:
	Parameters:
	Return: 
		None
*/

function initSelectBox(el)
{
	copySelected(el);

	var size = el.getAttribute("size");

	// These two lines combined with execution in optionClick() allow you to write:
	//		onchange="alert(this.options[this.selectedIndex].value)"
	el.options = el.children[1].children;
	el.selectedIndex = findSelected(el);	//Set the index now!
	
	// Some methods that are supported on the real SELECT box
	el.remove = new Function("i", "int_remove(this,i)");
	el.item   = new Function("i", "return this.options[i]");
	el.add    = new Function("e", "i", "int_add(this, e, i)");
	el.removeall = new Function("int_removeall(this)");
	el.optionselect = new Function("e","i","int_optionselect(this, e, i)");
	//el.children[0].onkeydown = new Function("alert(event.keyCode)");

// The real select box let you have lot of options with the same NAME. In that case the item
// needs two arguments. When using DIVs you can't have two with the same NAME (or ID) and
// then there is no need for the second argument

  if ( el.options != null && el.options[el.selectedIndex] != null )
      el.options[el.selectedIndex].selected = true;

	dropdown = el.children[1];

	// set the size, number of displayed listed item
	if (size != null)
	{
		if (size > 1)
		{
			el.size = size;
			dropdown.style.zIndex = 0;
			initSized(el);
		}
		else
		{
			el.size = 1;
			//dropdown.style.zIndex = 99;
			if (dropdown.offsetHeight > 120)
			{
				dropdown.style.height = "120";
				dropdown.style.overflow = "auto";
			}
		}
	}
	// show select item
	highlightSelected(el,true);
}

/*------------------------------------------------------------------------------------*/
/*	int_optionselect - Method 
	Description:
		Interal function to select the request item		
		Parameters e and indx are optional but need at least one
		The external method wraps the this pointer to this function
		
	Parameters:
		el - element, selectbox reference
		e  - (optional) text of the item
		indx - (optional) index of the option in selectbox
		val = 9optional) option value
	Return: 
		None
*/	

function int_optionselect(el, e, indx, val)
{
	try {
    var i
  	var bFound = false;
  
  	if ((e == null || e == "") && indx == null && (val == null || val == "")) return;
  
  	// if we have text then walk the list to find the index
  	if (e != null & e != "")
  	{
        var iLength = el.options.length;
    		for(i=0; i < iLength; i++)
    		{
    			if (el.options[i].innerText == e){ bFound = true; break; }
    		}
  		if (bFound == false) i = 0;
  	}
  	else if (val != null || val != "")
  	{
        var iLength = el.options.length;
    		for(i=0; i < iLength; i++)
    		{
    			if (el.options[i].value == val){ bFound = true; break; }
    		}
  		if (bFound == false) i = 0;
  	}
  	else i = indx;
  
  	// reset current selection highlight
  	oldSelected = el.options[findSelected(el)];
  	oldSelected.style.cssText = "";
  	oldSelected.removeAttribute("selected");
  	highlightSelected(oldSelected.parentElement.parentElement, 0);
  	// set new selection stuff
  	el.options[i].setAttribute("selected", 1);
  	el.selectedIndex = findSelected(el);
  	//el.selectedIndex = i;
  
  	// save styles for replacing back, can be changed outside 
  	if (el.options[i].backupCss != null)
  			el.options[i].style.cssText = el.options[i].backupCss;
  
  	copySelected(el);
  	highlightSelected(el, true);
  
  	selectBox = el;
  	if (selectBox.onchange != null)
  	{	// This executes the onchange when you chnage the option
  		if (selectBox.id != "")
  		{		// For this to work you need to replace this with an ID or name
  			eval(selectBox.onchange.replace(/this/g, selectBox.id));
  		}
  		else
  		{
  			globalSelect = selectBox;
  			eval(selectBox.onchange.replace(/this/g, "globalSelect"));
  		}
  	}
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	int_remove - Method 
	Description:
		Remove item interal function		
	Parameters:
		el - element reference
		i - index of item in list
	Return: 
		None
*/	

function int_remove(el,i)
{
  try {
  	if (el.options[i] != null)
  		el.options[i].outerHTML = "";
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	int_removeall - Method 
	Description:
		Interal function for removing all items from the select (can not remove the last)		
	Parameters:
		el - element reference
	Return: 
		None
*/	

function int_removeall(el)
{
	//can not remove the last one!
    //var optCount = el.options.length;
	//for(var i=0; i < optCount; i++)
  try {
      for(var i=el.options.length; i >= 0; i--)
  	{
  		//can not remove the last one, need a place holder for adding options
  		//when you want to totally replace all options you call removeall
  		//add your options and then remove the place holder 'remove(0)'
  		if (i == (el.options.length -1) && el.options[i] != null)
  			el.options[i].outerHTML = "<div></div>";
  		else
  			int_remove(el,i);
  	}
  } catch (e) {}  
}

/*------------------------------------------------------------------------------------*/
/*	int_add - Method 
	Description:
		Interal function to  Add an item to the list		
	Parameters:
		el - element reference
		e - object that can be the text and the value of an item
			e.text = display text
			e.value = selected value
		i - index (optional) index to insert new item otherwise at end
	Return: 
		None
*/	

function int_add(el, e, i) 
{
  try {
  	// build the HTML for the DIV that is the item	
  	var html = "<div class='option' noWrap";
  	if (e.value != null)
  		html += " value='" + e.value + "'";
  
  // no styles
  //	if (e.style.cssText != null)
  //		html += " style='" + e.style.cssText + "'";
  
  	html += ">";
  	if (e.text != null)
  		html += e.text;
  	html += "</div>"
  
  	// if not told where to place then check if we have items and place last
  	if ((i == null) || (i >= el.options.length))
  	{
  		if (el.options.length > 0)
  			i = el.options.length-1;
  		else
  			i = 0;
  	}
  	// insert HTML
  	el.options[i].insertAdjacentHTML("AfterEnd", html);
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	initSized - Method 
	Description:
		If we have a 'sized' list then we have to set the styles for height and borders etc.	
	Parameters:
		el - element
	Return: 
		None
*/	

function initSized(el)
{
  try {
  	// height 
  	var h = 0;
  	// make sure do not take up space
  	el.children[0].style.display = "none";
  
  	dropdown = el.children[1];
  	dropdown.style.visibility = "visible";
  
  	if (dropdown.children.length > el.size)
  	{
  		dropdown.style.overflow = "auto";
  		// get total height
      var iLength = el.size;
  		for (var i=0; i<iLength; i++)
  		{
  			h += dropdown.children[i].offsetHeight;
  		}
  		// add border 
  		if (dropdown.style.borderWidth != null)
  		{
  			dropdown.style.pixelHeight = h + 4; //2 * parseInt(dropdown.style.borderWidth);
  		}
  		else
  			dropdown.style.height = h;
  	}
  	// set style and height
  	dropdown.style.border = sizedBorderCss;
  	el.style.height = dropdown.style.pixelHeight;
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	copySelected - Method 
	Description:
		Copy the select items text to the displayed text line (selectbox text)		
	Parameters:
		el - element
	Return: 
		None
*/	

function copySelected(el)
{
  try {
  	//get select index of sb
  	var selectedIndex = findSelected(el);
  	// get references to the cell and the Div that is the display
  	selectedCell = el.children[0].rows[0].cells[0];
  	selectedDiv  = 	el.children[1].children[selectedIndex];
  	
    if ( selectedCell != null && selectedDiv != null )
    {
        // update the display
  			selectedCell.innerHTML = selectedDiv.outerHTML;
  			// update input box that holds the select items value
        var objInput = el.children.hiddenInput;
        if ( objInput != null ){
            objInput.value = selectedDiv.value;
        }
    }
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	findSelected - Method 
	Description:
		This function returns the first selected option and resets the rest
	Parameters:
		el - element
	Return: 
		selected - index of selected item
*/	

function findSelected(el)
{
  try {
  	var selected = null;
  	// tc used to ignore if no items
  	try
  	{
  		ec = el.children[1].children;	//the table is the first child
  	}
  	catch(e){ return 0; } 
  
  	var ecl = ec.length;
  	// walk the list and find the select attribute
  	for (var i=0; i<ecl; i++)
  	{
  		if (ec[i].getAttribute("selected") != null)
  		{
  			if (selected == null)
  			{	// Found first selected
  				selected = i;
  			}
  			else
  				ec[i].removeAttribute("selected");	//only one selected allowed
  		}
  	}
  	if (selected == null)
  		selected = 0;	//When starting this is the most logic start value if none is present
  
  	return selected;
  } catch (e) {}
}


/*------------------------------------------------------------------------------------*/
/*	findSelected - Method 
	Description:
		This function returns the text value of the state value(selected)
	Parameters:
		el - element
	Return: 
		selected - text of selected item
*/	

function findSelectedStateText(el,StValue)
{
  	// if we have text then walk the list to find the text 
  	if (StValue != null & StValue != "")
  	{
        var iLength = el.options.length;
    	for(i=0; i < iLength; i++)
    	{
    		if (el.options[i].value == StValue){ return el.options[i].innerText; break;}
		}
   	}
    return 0;
}



/*------------------------------------------------------------------------------------*/
/*	toggleDropDown - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function toggleDropDown(el, DDStyle)
{
  if (el.disabled) return;

	if (el.size == 1)
	{
		dropDown = el.children[1];

		if (dropDown.style.visibility == "")
			dropDown.style.visibility = "hidden";

		if (dropDown.style.visibility == "hidden")
			showDropDown(dropDown, DDStyle);
		else
			hideDropDown(dropDown);
	}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function optionClick()
{
  try {
  	el = getRealS(window.event.srcElement, "className", "option");
  
  	if (el.className == "option")
  	{
  		dropdown  = el.parentElement;
  		selectBox = dropdown.parentElement;
  
  		oldSelected = dropdown.children[findSelected(selectBox)]
  
  		if(oldSelected != el)
  		{
  			oldSelected.removeAttribute("selected");
  			el.setAttribute("selected", 1);
  			selectBox.selectedIndex = findSelected(selectBox);
  		}
  
  		if (selectBox.onchange != null)
  		{	// This executes the onchange when you chnage the option
  			if (selectBox.id != "")
  			{		// For this to work you need to replace this with an ID or name
  				eval(selectBox.onchange.replace(/this/g, selectBox.id));
  			}
  			else
  			{
  				globalSelect = selectBox;
  				eval(selectBox.onchange.replace(/this/g, "globalSelect"));
  			}
  		}
  
  		if (el.backupCss != null)
  			el.style.cssText = el.backupCss;
  		copySelected(selectBox);
  		toggleDropDown(selectBox);
  		highlightSelected(selectBox, true);
  
      var objInput = selectBox.lastChild;
      //set the control dirty
      objInput.dirty = true;
      //set the controls to BlurStyle.
      BlurStyle(selectBox.firstChild);
  	}
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function selHover(el, inout)
{
	if (inout) el.className = "selButtonHover"
	else el.className = "selButton"
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function selMouseDown(el)
{
	el.className = "selButtonMouseDown"
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function optionOver()
{
  try {
  	var toEl = getRealS(window.event.toElement, "className", "option");
  	var fromEl = getRealS(window.event.fromElement, "className", "option");
  	if (toEl == fromEl) return;
  	var el = toEl;
  
  	if (el.className == "option")
  	{
  		if (el.backupCss == null)
  			el.backupCss = el.style.cssText;
  		highlightSelected(el.parentElement.parentElement, false);
  		el.style.cssText = el.backupCss + "; " + overOptionCss;
  		this.highlighted = true;
  	}
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function optionOut()
{
  try {
  	var toEl = getRealS(window.event.toElement, "className", "option");
  	var fromEl = getRealS(window.event.fromElement, "className", "option");
  
  	if (fromEl == fromEl.parentElement.children[findSelected(fromEl.parentElement.parentElement)])
  	{
  		if (toEl == null)
  			return;
  		if (toEl.className != "option")
  			return;
  	}
  
  	if (toEl != null)
  	{
  		if (toEl.className != "option")
  		{
  			if (fromEl.className == "option")
  				highlightSelected(fromEl.parentElement.parentElement, true);
  		}
  	}
  
  	if (toEl == fromEl) return;
  	var el = fromEl;
  
  	if (el.className == "option")
  	{
  		if (el.backupCss != null)
  			el.style.cssText = el.backupCss;
  	}
  } catch (e) {}

}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function highlightSelected(el,add)
{
  try {
  	var selectedIndex = findSelected(el);
  
  	selected = el.children[1].children[selectedIndex];
  
      if ( String( selected ) != "undefined" )
      {
          if (add)
          {
              if (selected.backupCss == null)
                  selected.backupCss = selected.style.cssText;
              selected.style.cssText = selected.backupCss + "; " + overOptionCss;
          }
          else if (!add)
          {
              if (selected.backupCss != null)
                  selected.style.cssText = selected.backupCss;
          }
      }
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function hideShownDropDowns()
{
	try {
    var el = getRealS(window.event.srcElement, "className", "select");
  
  	var spans = document.all.tags("SPAN");
  	var selects = new Array();
  	var index = 0;
  
    var iLength = spans.length;
  	for (var i=0; i<iLength; i++)
  	{
  		if ((spans[i].className == "select") && (spans[i] != el))
  		{
  			dropdown = spans[i].children[1];
  			if ((spans[i].size == 1) && (dropdown.style.visibility == "visible"))
  				selects[index++] = dropdown;
  		}
  	}
  
    var jLength = selects.length;
  	for (var j=0; j<jLength; j++)
  	{
  		hideDropDown(selects[j]);
  	}
  } catch (e) {}

}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function hideDropDown(el)
{
  if (typeof(fade) == "function")
  {
		fade(el, false,null, null, el);
  }
	else
  {
		el.style.visibility = "hidden";
		el.style.display = "none";
  }
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function showDropDown(el, DDStyle)
{
	if (typeof(fade) == "function")
  {
		fade(el, true, null, null, el);
  }
	else if (typeof(swipe) == "function")
  {
		swipe(el, DDStyle);  //DRLDRLDRL 8 for up
		el.style.display = "inline";
  }
	else
	{
		el.style.visibility = "visible";
		el.style.display = "inline";
	}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function FocusStyle(obj)
{
  try {
  	var objInput = obj.parentElement.lastChild;
    if (objInput.getAttribute("dirty") == true) {
  	  obj.style.background='#FFFFFF';
    }
    else {
      obj.style.borderRight = '1px solid #808080';
      obj.style.borderBottom = '1px solid #808080';
  	  obj.style.borderLeft = '1px solid #D3D3D3';
    	obj.style.borderTop='1px solid #D3D3D3';
  	  obj.style.background='#FFFFFF';
    }
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function BlurStyle(obj)
{
  try {
  	var objInput = obj.parentElement.lastChild;
    if (objInput.getAttribute("dirty") == true) {
  	  obj.style.background='#FFFFFF';
    }
    else {
      obj.style.borderColor='#CCCCCC';
  	  obj.style.background='url(../images/input_bkgd.png) repeat';
    }
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function SelKeyDown(el, evt)
{
  try {
    // if tab key, hide the drop-down select
  	if (evt.keyCode == 9) hideDropDown(el.children[1]); 
  
    if (evt.keyCode >= 37 && evt.keyCode <= 40)  //down key
  	{
  		var currentindex = findSelected(el);
  		currentindex++;
  		try { //added this line to prevent error when no items are in the list and user uses key to navigate in the list.
        if (currentindex >= el.options.length) 
    			currentindex = 0
      } catch (e) {}
  
  		int_optionselect(el, null, currentindex)
  	}
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

//call addSelect('select4',2,fnOnChange,2,'Personal','Work')
// select id , direction, function_name_onChange, SelectedIndex, size, zindex, options, option_value1, option_value2, ..... , width (optional)
function addSelect()
{
  try {
  	var args=addSelect.arguments;
  	var selectname = args[0];
  	var selectdir = args[1];
  	var fnOnChange = args[2];
  	var selectindexp = args[3];
  	var selectsize = args[4];
  	var zindex = args[5];
  	var optionscount = args[6];
  	var str = "";
      var sHeight = "";
  	
  	var strTabIndex = "";
  	if (args.length > (8+optionscount))
  		strTabIndex = " tabindex='" + args[8+optionscount] + "' ";
  
  	var arrowtype = "6";
  	if (selectdir == 8) arrowtype = "5";
  
  	var sZindex = "z-index:" + String(10000+zindex) + ";";
  
  	//get the width from the options
  	var selWidth = 90;
      var selHeight = 0;
  	if (args.length > (7+optionscount))
  	{
  		if (args[7+optionscount] != null && args[7+optionscount] != "")
  			selWidth = args[7+optionscount];
  		if (args[8+optionscount] != null && args[8+optionscount] != "")
  			selHeight = args[8+optionscount];
      else
      {
      	for(i=0; i<optionscount; i++)
  	    {
  		    if (((args[7+i].length * 6)+20) > selWidth) selWidth = (args[7+i].length * 6)+20;
      	}
  		}
  	}
    else
    {
  		for(i=0; i<optionscount; i++)
  		{
  			if (((args[7+i].length * 6)+20) > selWidth) selWidth = (args[7+i].length * 6)+20;
  		}
  	}
      
      if (selHeight > 0)
          sHeight = sHeight = "height:"+selHeight+"px;";
  	str = "<SPAN unselectable='on' class='select' style='width:"+selWidth+"px;' id='"+selectname+"' onchange='"+fnOnChange+"(this)' selectedIndex='"+selectindexp+"' size='"+selectsize+"' style='position: relative; top: 0;"+sZindex+"'>";
  	str +="<TABLE class='selectTable' cellspacing='0' cellpadding='0' onactivate='FocusStyle(this)' ondeactivate='BlurStyle(this); hideDropDown(this.parentElement.children[1]);' onClick='toggleDropDown(this.parentElement, 2)' onkeydown='SelKeyDown(this.parentElement,event)'>";
  	str +="<TR style='height:8px;'>";
  	str +="<TD unselectable='on' class='selected'  >	&nbsp;</TD>";
  	str +="<TD unselectable='on' align='CENTER' valign='MIDDLE' class='selButton' onmouseover='selHover(this,1)' onmouseout='selHover(this,0)' onmousedown='selMouseDown(this)' onmouseup='selHover(this,0)' ><SPAN style='position: relative; left: 1; top: -2; width: 100%;'><A HREF='#'"+strTabIndex+">"+arrowtype+"</a></SPAN>";
  	str +="</TD></TR></TABLE>";
  	str +="<DIV unselectable='on' class='dropDownNO' style='" + sZindex + sHeight + "' onClick='optionClick()' onMouseOver='optionOver()' onMouseOut='optionOut()'>";
  
  	var strSel = ""
  	for(i=0; i<optionscount; i++)
  	{
  		if (i == selectindexp ) strSel = "selected";
  		else strSel = "";
  		str +="<DIV unselectable='on' class='option' value='"+(i+1)+"' "+strSel+" nowrap>"+args[7+i]+"</DIV>";
  	}
  	str +="</DIV></SPAN>";
  
      document.write(str);
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	
function addSelectCustomValues()
{
  try {
    var args=addSelectCustomValues.arguments;
    var selectname = args[0];
    var selectdir = args[1];
    var fnOnChange = args[2];
    var selectval = args[3];
    var selectsize = args[4];
    var zindex = args[5];
    var optionscount = args[6];
		var crud = args[7];
		
    var str = "";

    var arrowtype = "6";
    if (selectdir == 8) arrowtype = "5";
		if (crud.indexOf("U") == -1)
			arrowtype = "";

    var sZindex = "z-index:" + String(10000+zindex) + ";";

		var strTabIndex = "";
		if (args.length > (10+(optionscount*2)))
			strTabIndex = " tabindex='" + args[10+(optionscount*2)] + "' ";

    //get the width from the options
    var selWidth = 90;
    if (args.length > (8+(optionscount*2)))
    {
			if (args[8+(optionscount*2)] != null && args[8+(optionscount*2)] != "" && args[8+(optionscount*2)] != 0)
				selWidth = args[8+optionscount*2];
    }

    if (selWidth == 90)
    {
        for(i=0; i< optionscount; i++)
        {
            if (((args[8+(i*2)].length * 6)+20) > selWidth) selWidth = (args[8+(i*2)].length * 6)+20;
        }
    }

		var sHeight = "";
		if (args.length > (9+(optionscount*2)))
		{
		   sHeight = args[9+(optionscount*2)];
		}
		if (sHeight != "")
	    sHeight = "height:"+sHeight+"px;";

    str = "<SPAN  class='select' style='width:"+selWidth+"px;' id='"+selectname+"' ";
		
		if (crud.indexOf("U") > 0)
			str += "onchange='"+fnOnChange+"(this)' ";
			
		str += "selectedIndex='0' size='"+selectsize+"' style='position: relative; top: 0;"+sZindex+"'>";
    str +="<TABLE class='selectTable' cellspacing='0' cellpadding='0' onactivate='FocusStyle(this)' ondeactivate='BlurStyle(this);'";
		
		if (crud.indexOf("U") > 0)
			str += " onClick='toggleDropDown(this.parentElement, 2)' onkeydown='SelKeyDown(this.parentElement,event)'";
			
    str +="><TR style='height:8px;'>";
    str +="<TD class='selected'  >  &nbsp;</TD>";
    str +="<TD align='CENTER' valign='MIDDLE' class='selButton' ";

		if (crud.indexOf("U") > 0)
			str += "onmouseover='selHover(this,1)' onmouseout='selHover(this,0)' onmousedown='selMouseDown(this)' onmouseup='selHover(this,0)'";
			
		str += " ><SPAN style='position: relative; left: 1; top: -2; width: 100%;'><A HREF='#'"+strTabIndex+">"+arrowtype+"</a></SPAN>";
    str +="</TD></TR></TABLE>";
    str +="<DIV  class='dropDown' style='" + sZindex + sHeight + "' ";
		
		if (crud.indexOf("U") > 0)
			str += "onClick='optionClick()' onMouseOver='optionOver()' onMouseOut='optionOut()'";
		str += " id='selectOptionDiv'>";

    var strSel = ""
    for(i=0; i<optionscount; i++)
    {
        if ( args[8+(i*2)] == selectval ) strSel = "selected";
        else strSel = "";
        str +="<DIV class='option' value='"+args[8+(i*2)]+"' "+strSel+" nowrap>"+args[9+(i*2)]+"</DIV>";
    }
    str +="</DIV></SPAN>";

		if (crud.indexOf("R") == -1)
			str ="<input type='password' value='xxxx' readonly class='InputReadonlyField' size='6'/>";
		
    document.write(str);
  } catch (e) {}
}

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function getRealS(el, type, value)
{
  try {
  	temp = el;
  	while ((temp != null) && (temp.tagName != "BODY"))
  	{
  		if (eval("temp." + type) == value)
  		{
  			el = temp;
  			return el;
  		}
  		temp = temp.parentElement;
  	}
  	return el;
  } catch (e) {}
}

	//window.onload = initSelectBoxes;
	document.onclick = hideShownDropDowns;

/*------------------------------------------------------------------------------------*/
/*	 - Method 
	Description:
	Parameters:
	Return: 
		None
*/	

function replaceSelectListContents( objSel, objNodeList, selIndex, nameAtt, valAtt )
{
  try {
    var strOptions = "";
    if ( objNodeList.length )
    {
        var n;
        var nLength = objNodeList.length;
        for ( n = 0; n < nLength; n++ )
        {
            var objNode = objNodeList.nextNode();
            var strSel = ( n == selIndex ) ? " selected" : "";
            strOptions += "<div class='option' value='"
                + objNode.selectSingleNode( valAtt ).text + "'" + strSel + " nowrap>"
                + objNode.selectSingleNode( nameAtt ).text + "</div>";
        }

        // Set the innards of the status select.
        objSel.all.selectOptionDiv.innerHTML = strOptions;
        //objSel.optionselect( '', 0 );
				var i = 0;
				objSel.options[i].setAttribute("selected", 1);
				objSel.selectedIndex = findSelected(objSel);
				copySelected(objSel);
				highlightSelected(objSel, true);


    }
    // Clear combo box - something wrong with database?!
    else
    {
        objSel.all.selectOptionDiv.innerHTML = "&nbsp;";
        objSel.children[0].rows[0].cells[0].innerHTML = "&nbsp;";
        var objInput = objSel.children.hiddenInput;
        if ( objInput != null )
            objInput.value = "";
    }
  } catch (e) {}
}


