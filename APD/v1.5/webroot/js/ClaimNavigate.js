// ClaimNavigate.js
//
// Functions for navigating around claims
// i.e. from claim data to vehicle to property etc.
//
// These methods assume the caller is catching exceptions.
//

// Returns a string value that will not throw.
function StrVal( sSrc )
{
    if ( String( sSrc ) == "undefined" )
        return "[undefined]";
    else if ( sSrc == null )
        return "[null]";
    return sSrc;
}

// Returns a URL to navigate to based upon the parameters.
function NavBuildUrl( sEntity, sLynxID, sContext, sWindowID )
{
    try
    {
        switch( sEntity.toLowerCase() )
        {
            case "claim":
                if (top.sClaimView == "cond")
                    return "ClaimXP.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID;
                else if (top.sClaimView == "condnew")
                     return "ClaimXPNew.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID;
                else
                    return "Claim.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID;
            case "vehicle":
                if (top.sClaimView == "cond")
                    return "ClaimXP.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID + "&VehNum=" + sContext;
                else if (top.sClaimView == "condnew")
                    return "ClaimXPnew.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID + "&VehNum=" + sContext;
                else
                    return "ClaimVehicleList.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID + "&VehContext=" + sContext;
            case "property":
                return "ClaimPropertyList.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID + "&PropContext=" + sContext;
            case "document":
                return "ClaimDocument.asp?WindowID=" + sWindowID + "&LynxID=" + sLynxID;
            default:
                throw "Bad entity passed!";
        }
    }
    catch ( e )
    {
        throw "ClaimNavigate.js::NavBuildUrl( sEntity='" + StrVal( sEntity )
            + "', sLynxID='" + StrVal( sLynxID )
            + "', sContext='" + StrVal( sContext )
            + "', sWindowID='" + StrVal( sWindowID )
            + "' ) Error: " + e.message;
    }
}

// Opens a new claim window with the passed lynx id and url.
// Note that the window name will prevent multiple windows
// pointing to the same claim.
function NavOpenWindow( sEntity, sLynxID, sContext )
{
    try
    {
        NavOpenWindowSettings( NavOpenWindowUrl( sEntity, sLynxID, sContext ) );
    }
    catch ( e )
    {
        throw "ClaimNavigate.js::NavOpenWindow( sEntity='" + StrVal( sEntity )
            + "', sLynxID='" + StrVal( sLynxID )
            + "', sContext='" + StrVal( sContext )
            + "' ) Error: " + e.message;
    }
}

// Splits out some of the functionality for NavOpenWindow.
function NavOpenWindowUrl( sEntity, sLynxID, sContext )
{
    return sLynxID + "||ApdSelect.asp?Entity=" + sEntity.toLowerCase() + "&LynxID=" + sLynxID + "&Context=" + sContext;
}

// Splits out some of the functionality for NavOpenWindow.
function NavOpenWindowSettings( sUrl )
{
    var ary = sUrl.split("||");
    var sWinName = "APDClaimFrame_" + ary[0];
    var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
    var oWin = window.top.open( ary[1], sWinName, sSettings );
    oWin.focus();
}

