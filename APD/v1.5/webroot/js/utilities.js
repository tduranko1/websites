/**********************************************************************
* Function:  Recordset2Array                                          *
* Purpose:   Converts a recordset object into a two-dimensional array *
* Parameters:   oRS - The recordset object                            *
**********************************************************************/                          
function Recordset2Array(rst) 
 {
     var numOfFields, i, j
     var aRS;
     
     // Creates a new array to hold the RS
     aRS = new Array();
     
     // Adds a first child array with all the field names
     aRS[0] = new Array();        
     numOfFields = rst.Fields.Count;
     
     for (i=0; i<numOfFields; i++)
         aRS[0][i] = rst.Fields.Item(i).Name;
 
     // Adds as many child arrays as needed to hold all the records 
     i = 1;
 
     while (!rst.EOF) {
         aRS[i] = new Array();
         
         for (j=0; j<numOfFields; j++) 
             aRS[i][j] = String(rst.Fields.Item(j).Value);
         
         // Next record and next item in the array
         rst.MoveNext();
         i++;
     }
 
     return aRS;
 }
 
/***********************************************************************
* Function:  GetArrayElementCount                                      *
* Purpose:   Return the number of elements in aArray with value sValue *
* Parameters:   aArray - The array to check                            *
*               sValue - The value to count                            *
***********************************************************************/                          
function GetArrayElementCount(aArray, sValue) 
{
  var nCount = 0;
  
  for (var i = 0; i< aArray.length; i++) {
    if (aArray[i] == sValue) {
      nCount++;
    }
  }
  
  return nCount;
}

/***********************************************************************
* Function:  trim_string                                               *
* Purpose:   Trims whitespace from the beginning and end of a string.  *
*            The function is implemented as a prototype of the String  *
*            class.                                                    *
***********************************************************************/                          
String.prototype.Trim = trim_string;
function trim_string() 
{
  var ichar, icount;
  var strValue = this;
  ichar = strValue.length - 1;
  icount = -1;
  while (strValue.charAt(ichar)==' ' && ichar > icount)
    --ichar;
  if (ichar!=(strValue.length-1))
    strValue = strValue.slice(0,ichar+1);
  ichar = 0;
  icount = strValue.length - 1;
  while (strValue.charAt(ichar)==' ' && ichar < icount)
    ++ichar;
  if (ichar!=0)
    strValue = strValue.slice(ichar,strValue.length);
  return strValue;
}
