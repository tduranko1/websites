
// Displays external and internal event messages in a popup window.
// This version is called for errors occurring server side.
function ServerEvent()
{
    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "EventPopup.asp";

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

// Displays external and internal event messages in a popup window.
// This version is called for errors occurring client side.
function ClientEvent( nNumber, sDescription, sSource )
{
    if ( ( String( sSource ) == "undefined" ) || ( sSource == null ) )
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "/EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);

    top.showModalDialog( sQuery, top, sDimensions + sSettings );
}

function ClientError( sDescription ) { ClientEvent( 0, sDescription ); }
function ClientWarning( sDescription ) { ClientEvent( 1, sDescription ); }
function ClientInfo( sDescription ) { ClientEvent( 2, sDescription ); }

// Generic routine for handling results from our remote script calls.
function ValidateRS( oReturn )
{
    var aReturn = new Array()

    aReturn[0] = oReturn.message;
    aReturn[1] = null;

    if ( oReturn.status == -1 )
        ServerEvent();
    else
    {
        aReturn = oReturn.return_value.split("||");

        if ( aReturn[1] == 0 )
        {
            ServerEvent();
            aReturn[1] = 0;
        }
    }

    return aReturn;
}

function FormatPhone(obj)
{
	var sValue = String(obj.value);
	if (sValue == "() -") return;

	var re = /^\((\d{3})\) (\d{3})-(\d{4})/;
	var ret = sValue.match(re);

	if (ret == null || ret[1] == null || ret[2] == null || ret[3] == null)
	{
        ClientWarning("Bad Phone Number Format!  Example: (941) 479-0000");
		return;
	}

	obj.value = "(" + ret[1]+ ") " + ret[2] + "-" + ret[3];
}

function NumbersOnly(event)
{
  var bOK = false;
	if ((event.keyCode >= 45 && event.keyCode <= 57) && (event.keyCode != 47))  bOK = true;

  try {
      if (event.srcElement.allowDecimal != undefined && event.keyCode == 46)
      {
        if (event.srcElement.allowDecimal == true)
          bOK = true;
        else
          bOK = false;
      }
      if (event.srcElement.allowMinus != undefined && event.keyCode == 45)
      {
        if (event.srcElement.allowMinus == true)
          bOK = true;
        else
          bOK = false;
      }
  }
  catch(e) {
      bOK = false;
  }

  if (event.keyCode == 13) bOK = true;  //pass thru an <Enter> key
  if (event.shiftKey == true || bOK == false )
    event.returnValue = false;
}

function ExpandMe()
{
	var orgSize = this.getAttribute("InitSize");
	var max = this.getAttribute("maxlength");

	if((this.value == null ) || (this.value == "" )) this.size = orgSize;
	if((this.value.length >= orgSize)&&(this.value.length <= max)) this.size = this.value.length + 1;
	else this.size = orgSize;
}

function ExpandSize()
{
	if (this.value.length > this.getAttribute('InitSize')) this.size=this.value.length + 1;
}

function ExpandReset()
{
	this.size=this.getAttribute('InitSize');
}

function ignoreSpaces(string)
{
	var temp = "";
	string = '' + string;
	splitstring = string.split(" ");
  var iLength = splitstring.length;
	for(i = 0; i < iLength; i++)
		temp += splitstring[i];
	return temp;
}

function CheckDate(objName)
{
    var datefield = objName;
		if (objName.value == "") return false;
    if (objName.value.length < 6 || chkdate(objName) == false )
    {
        datefield.select();
        ClientWarning("The entered date is invalid.<br>Please enter a valid date in \"mm/dd/yyyy\" format.");
        datefield.focus();
        return false;
    }
    else return true;
}

function DTUpdate(obj)
{
	var sParent = obj.getAttribute("DTParent");
	var oDate = document.getElementById("ctrDate"+sParent);
	var oTime = document.getElementById("ctrTime"+sParent);

	document.getElementById(sParent).value = oDate.value + " " + oTime.value;
}

function chkdate(objName) {
  //if the passed in object does not exist in the document
  if (!objName) return false;
  var sDate = objName.value;

  var dt = new Date(sDate);

  if (isNaN(dt)) return false; //the value in the control is not a valid date.

  var iMonth, iDate, iYear;
  var sMonth, sDate;
  var strSeparator = "/";

  //get the year portion of the typed value
  for (var i = sDate.length - 1; i >= 0; i--){
    if (isNaN(parseInt(sDate.substr(i, 1))))
      break;
  }
  iYearTyped = parseInt(sDate.substr(i+1, sDate.length), 10);

  // avoid 3 digit year which is a valid javascript year
  if ((iYearTyped + "").length == 3) {
    iYearTyped *= 10;
    dt.setFullYear(iYearTyped);
  }
    
  //if the year portion started like 0*, then we guess that the year was in the current century.
  if (iYearTyped < 10) {
    dtNow = new Date();
    var iCentury = parseInt(String(dtNow.getFullYear()).substr(0, 2), 10) * 100;
    dt.setFullYear(iCentury + iYearTyped);
  }

  /*if ((dt.getFullYear() + "").length > 4) 
    dt.setFullYear((dt.getFullYear() + "").substr(0, 4));*/
  
  iMonth = dt.getMonth();
  iDate = dt.getDate();
  iYear = dt.getFullYear();

  iMonth++; //getMonth() is a zero based month value.

  sMonth = (iMonth < 10 ? "0" + iMonth : iMonth); //pad the single digit month with a zero
  sDate = (iDate < 10 ? "0" + iDate : iDate); //pad the single digit date with a zero

  //this is hardcoded to be in mm/dd/yyyy format which is not flexible. need to revisit.
  objName.value = sMonth + strSeparator + sDate + strSeparator + iYear;

  return true;

}

function LeapYear(intYear) {
    if ( (intYear % 100 == 0) && ( ( (intYear % 400 == 0) || ((intYear % 4) == 0) ) ) )
        return true;
    return false;
}

function doDateCheck(from, to) {
    if (Date.parse(from.value) <= Date.parse(to.value)) {
        ClientInfo("The dates are valid.");
    }
    else {
        if (from.value == "" || to.value == "")
            ClientWarning("Both dates must be entered.");
        else
            ClientWarning("To date must occur after the from date.");
   }
}

function CheckTime(obj)
{
	// Checks if time is in HH:MM:SS AM/PM format.
	// The seconds and AM/PM are optional.
	var timeStr = String(obj.value);
	if (timeStr == "") return true;

	var TArray = timeStr.split(" ");
	if (TArray[2])
		timeStr = TArray[1] + " " + TArray[2];

	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|Am|aM|PM|pm|Pm|pM))?$/;

	var matchArray = timeStr.match(timePat);
	if (matchArray == null)
	{
		obj.select();
        ClientWarning("Time is not in a valid format.<br>Please enter a valid time in the format HH:MM:SS A/PM");
		obj.focus();
		return false;
	}

	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];

	if (second=="") second = null;
	if (ampm=="") ampm = null;

	if (hour < 0  || hour > 23)
	{
		obj.select();
        ClientWarning("Hour must be between 1 and 12 (or 0 and 23 for military time).");
		obj.focus();
		return false;
	}

	if (hour <= 12 && ampm == null)
	{
		if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time"))
		{
			obj.select();
            ClientWarning("You must specify AM or PM.");
			obj.focus();
			return false;
        }
	}

	if  (hour > 12 && ampm != null)
	{
		obj.select();
        ClientWarning("You can't specify AM or PM for military time.");
		obj.focus();
		return false;
	}

	if (minute<0 || minute > 59)
	{
		obj.select();
        ClientWarning("Minute must be between 0 and 59.");
		obj.focus();
		return false;
	}

	if (second != null && (second < 0 || second > 59))
	{
		obj.select();
        ClientWarning("Second must be between 0 and 59.");
		obj.focus();
		return false;
	}
	return true;
}

function CheckMilitaryTime()
{
	// Checks if time is valid Military time (no colon(:))

	var obj = eval('document.all.' + event.srcElement.id);

	var timeStr = String(obj.value);
	if (timeStr == "") return true;

	var nums = '0123456789';
	var len = timeStr.length;

	if (len != 4){
        ClientWarning("This is not a valid Military time.  Must be in the form 'hhmm'.");
		obj.select();
		return false;
	}

	for (i=0;i<len;i++){
		if (nums.indexOf(timeStr.charAt(i)) == -1){
            ClientWarning('Military times must contain only numbers.');
			obj.select();
			return false;
		}
	}

	var mins = timeStr.substr(len-2);
	var hrs = timeStr.substr(0, len-2);
    if (hrs < 0  || hrs > 24)
	{
        ClientWarning("Hour must be between 0 and 24 for military time");
		obj.select();
		return false;
	}

	if (mins < 0 || mins > 59)
	{
        ClientWarning("Minute must be between 0 and 59.");
		obj.select();
		return false;
	}

	if (hrs == 24 && mins != 0)
	{
        ClientWarning("2400 is the maximum value allowed for military time.");
		obj.select();
		return false;
	}

	return true;
}

function SSNCheck(obj)
{
	var ssn = obj.value;
	if (ssn == "") return;

	var matchArr = ssn.match(/^(\d{3})-?\d{2}-?\d{4}$/);
	var numDashes = ssn.split('-').length - 1;

	if (matchArr == null || numDashes == 1)
	{
        ClientWarning('Invalid SSN. Must be 9 digits or in the form NNN-NN-NNNN.');
//		msg = "does not appear to be valid";
    ChangeInputBorder(obj)
		return;
	}
	else
	if (parseInt(matchArr[1],10)==0)
	{
        ClientWarning("Invalid SSN: SSN's can't start with 000.");
//		msg = "does not appear to be valid";
    ChangeInputBorder(obj)
		return;
	}
	else
	{
		return;
	}
//  ClientWarning(ssn + "\r\n\r\n" + msg + " Social Security Number.");
}

//These functions require RemoteScripting

function IsValidZip(obj)
{
    var sType = obj.getAttribute("type");
    if (sType == "password") return;

	if (obj.value.length == 0)
		return;

	if (obj.value.length != 5)
	{
		obj.style.borderColor = "#7f0000";
        ClientWarning("The entered zipcode length invalid.  Please restrict to 5 characters exactly.");
		obj.select();
		return;
	}

	var co = RSExecute("/rs/RSValidateZip.asp", "IsValidZip", obj.value);
	if (co.status == 0)
    {
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else
			if (listArray[0] == 0){
                ClientWarning("The entered zipcode could not be found in our database.  Please enter a valid Zipcode.");
				obj.select();
				return;
			}
    }
    else ServerEvent();
}

function ValidateFedTaxID(obj)
{
	if (obj.value.length == 0)
		return;

	var sShopId = "";
	if (arguments.length > 1)
		sShopId = arguments[1].value;

	var co = RSExecute("/rs/RSShopMaint.asp", "IsDuplicateFedTaxID", obj.value, sShopId);
	if (co.status == 0)
    {
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else
			if (listArray[0] == 0){
                ClientWarning("The SSN/EIN you entered already exists within the APD system.  Please enter a different SSN/EIN.");
				obj.select();
				return;
			}
    }
    else ServerEvent();
}

function ValidateZip(obj, sStateBox, sCityBox )
{
	if (obj.value == "") return;

	if (obj.value.length != 5 )
	{
		obj.style.borderColor = "#7f0000";
		return;
	}

	var StateObj = document.getElementById(sStateBox);
	var CityObj = document.getElementById(sCityBox);

    RSExecute("/rs/RSValidateZip.asp", "ZipToState", obj.value, RSrefreshPage, RSerrHandler, StateObj);
    RSExecute("/rs/RSValidateZip.asp", "ZipToCity", obj.value, RSrefreshPage, RSerrHandler, CityObj);
}


function InvolvedValidateZip(obj, sStateBox, sCityBox )
{
	if (obj.value == "") return;

	if (obj.value.length != 5 )
	{
		obj.style.borderColor = "#7f0000";
		return;
	}

	var StateObj = document.getElementById(sStateBox);
	var CityObj = document.getElementById(sCityBox);

   var co =  RSExecute("/rs/RSValidateZip.asp", "ZipToState", obj.value, RSrefreshPage, RSerrHandler, StateObj);
   co.wait();
   co=  RSExecute("/rs/RSValidateZip.asp", "ZipToCity", obj.value, RSrefreshPage, RSerrHandler, CityObj);
   co.wait();
}



function ValidateZipToCounty(obj, sCountyBox)
{
	if (obj.value == "") return;

	if (obj.value.length != 5 )
	{
		obj.style.borderColor = "#7f0000";
		return;
	}

	var CountyObj = document.getElementById(sCountyBox);
    RSExecute("/rs/RSValidateZip.asp", "ZipToCounty", obj.value, RSrefreshPage, RSerrHandler, CountyObj);
}

function RSrefreshPage(co)
{
	if (co.status == 0)
    {
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else if (co.context.value == "" || co.context.value == " " || co.context.value.indexOf("?") != -1)
			co.context.value = listArray[0];
    }
    else ServerEvent();
}

function RSerrHandler(co)
{
	if (co.status != 0)
	{
  	co.context.value = co.message;
  }
}

//this changes the border for the input field with the focus
function InitFields()
{
  flds= document.getElementsByTagName("input");
  var fldType, sDBType;
  var initsize;
  var xLength = flds.length;
  for (var x=0; x < xLength; x++)
  {
    fldType = flds[x].getAttribute("type").toLowerCase();
    if (fldType != "button" && fldType != "submit")
    {
      flds[x].onactivate = InputFocus;
      flds[x].ondeactivate = InputBlur;
      flds[x].onkeydown = InputChange;
      flds[x].oncut = onCutPasteSetDirty;
      flds[x].onpaste = onCutPasteSetDirty;
      flds[x].ondrop = cancelEvent;
      flds[x].dirty = false;
      sDBType = flds[x].getAttribute("dbtype");
      if (sDBType)
      {
        sDBType = sDBType.toLowerCase();
        if (sDBType == "decimal" || sDBType == "money")
        flds[x].onbeforedeactivate = checkDecimal;
      }
      
      initsize = flds[x].getAttribute("InitSize");
      if (initsize != null)
      {
        flds[x].onkeypress=ExpandMe;
        flds[x].onfocus=ExpandSize;
        flds[x].onblur=ExpandReset;
      }
    }
  }

  flds= document.getElementsByTagName("textarea");
  var xLength = flds.length;
  for (var x=0; x < xLength; x++)
  {
    flds[x].onactivate = InputFocus;
    flds[x].ondeactivate = InputBlur;
    flds[x].onkeydown = InputChange;
    flds[x].oncut = onCutPasteSetDirty;
    flds[x].onpaste = onTextAreaPaste;
    flds[x].dirty = false;
    
    initsize = flds[x].getAttribute("InitSize");
    if (initsize != null)
    {
      flds[x].onkeypress=ExpandMe;
      flds[x].onfocus=ExpandSize;
      flds[x].onblur=ExpandReset;
    }
  }
}

function cancelEvent(){
  var sDBtype = event.srcElement.getAttribute("dbType");
  if (sDBtype != "varchar" && sDBtype != "char" && sDBtype != "datetime")
    event.returnValue=false;
}

function checkDecimal(){
  try {
    sDBType = this.getAttribute("dbtype");
  	iPrecision = parseInt(this.getAttribute("Precision"), 10);
  	iScale = parseInt(this.getAttribute("Scale"), 10);
    sValue = this.value;
    if (sDBType == "decimal" || sDBType == "money") {
      if (sValue.indexOf(".") == -1) {
        if (sValue.length > (iPrecision - iScale)) {
          var sExpectedFormat = "";
          var iLength = iPrecision - iScale;
          for (var i = 0; i < iLength; i++)
            sExpectedFormat += "#";
          sExpectedFormat += ".";
          for (var i = 0; i < iScale; i++)
            sExpectedFormat += "#";
          ClientWarning("Numeric data entered exceeds the maximum length of the field. Expecting a numeric value of the format : " + sExpectedFormat);
          event.returnValue = false;
          this.select();
        }
      }
      else {
        var aTmp = sValue.split(".");
        if ((aTmp[0].length + aTmp[1].length > iPrecision) || (aTmp[1].length > iScale)) {
          var sExpectedFormat = "";
          var iLength = iPrecision - iScale;
          for (var i = 0; i < iLength; i++)
            sExpectedFormat += "#";
          sExpectedFormat += ".";
          for (var i = 0; i < iScale; i++)
            sExpectedFormat += "#";
          ClientWarning("Numeric data entered is not of expected format. Expecting a numeric value of the format : " + sExpectedFormat);
          event.returnValue = false;
          this.select();
        }
      }
    }
  } catch(e) {}
}

// Overrid to clip pastes to maxlen of textarea.
function onTextAreaPaste()
{
    var vMaxLen = parseInt(this.getAttribute("Maxlength"));
    this.value += window.clipboardData.getData("Text");

    if ( this.value.length > vMaxLen )
        this.value = this.value.substr( 0, vMaxLen );

    event.returnValue = false;

    onCutPasteSetDirty();
}

var gbDirtyFlag = false;

function InputChange(obj)
{
    if (obj == null) obj = this;
    if (obj.readOnly) return;

    if (event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 39)
        if (CheckInputDataType(event, obj) == false) return;

    // Ctrl+C causes the control dirty. defect #719
    if ((event.keyCode == 67 || event.keyCode == 99) && (event.ctrlKey || event.ctrlLeft)) return;

    if (!( obj.dirty && obj.dirty == true ))
    {
        var sBadCodes = ",9,13,16,17,18,19,33,34,35,36,37,38,39,40,45,92,93,113,114,115,116,117,118,119,120,121,122,123,145,";
        if (sBadCodes.indexOf(","+String(event.keyCode)+",") != -1) return;
    }

    obj.style.borderColor='#805050';
    obj.dirty = true;

    gbDirtyFlag = true;
    if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = true;
}

function InputFocus(obj)
{
	if (obj == null) obj = this;
	if (obj.getAttribute("readonly") == 0)
	{
		if (obj.dirty == false)
		{
			obj.style.borderBottom = '1px solid #808080';
			obj.style.borderRight = '1px solid #808080';
			obj.style.borderLeft = '1px solid #D3D3D3';
			obj.style.borderTop='1px solid #D3D3D3';
		}
		obj.style.background='#FFFFFF';
	}
}

function InputBlur(obj)
{
	if (obj == null) obj = this;
	if (obj.dirty == false && obj.getAttribute("readonly") == 0)
  {
		obj.style.borderColor='#CCCCCC';
	  obj.style.background='url(../images/input_bkgd.png) repeat';
  }
}

function CheckInputDataType(event, obj)
{
//	alert(event.keyCode);
  // check maxlength for textarea
	if (obj.tagName == "TEXTAREA")
	{
        if (obj.value.length >= parseInt(obj.getAttribute("Maxlength")))
		{
			event.returnValue = false;
			return false;
		}
	}
	else
	{
		var sDBtype = obj.getAttribute("dbtype");
		var iPrecision = 0;
		var iScale = 0;
		var floatMaxValue = 0.0;

		// build max value for type
		if (sDBtype != null && sDBtype != "" && sDBtype != "varchar" && sDBtype != "char" && sDBtype != "datetime")
		{
			var bOK = false;
			var bNumKeys = false;
			// normal numbers
			if (event.keyCode >= 45 && event.keyCode <= 57) bOK = true;
			// num keypad with '.'
			if (event.keyCode >= 96 && event.keyCode <= 105)
			{
				bOK = true;
  		 	bNumKeys = true;
			}

			// normal '-' '.' 
			if (event.keyCode == 189 || event.keyCode == 190) bOK = true;
			// NumKeys '-' '.' 
			if (event.keyCode == 109 || event.keyCode == 110) bOK = true;
      //Home and End Keys were not working for tinyint fields. - defect #897.
      if ((event.keyCode == 35) || (event.keyCode == 36)) bOK = true;

			if (event.shiftKey == true || bOK == false )
			{
				event.returnValue = false;
				return false;
			}

			iPrecision = parseInt(obj.getAttribute("Precision"));
			iScale = parseInt(obj.getAttribute("Scale"));

			var sPr = "";
      var iLength = iPrecision-iScale;
			for(var i=0; i < iLength; i++)	sPr += "9";

			iScale = parseInt(obj.getAttribute("Scale"));
			var sSc = "";
			for(var i=0; i < iScale; i++)	sSc += "9";
			floatMaxValue = parseFloat(sPr+"."+sSc);

			// have to do this because 'fromCharCode' does not decode numpad numbers
			var sNewNumber = "";
			if (bNumKeys == false)
				sNewNumber = String.fromCharCode(event.keyCode);
			else
				sNewNumber = String(event.keyCode - 96);

			// special case fot tinyint
			if (sDBtype == "tinyint")
			{
				if (parseInt(obj.value + sNewNumber) > 256)
				{
					event.returnValue = false;
					//THIS HAS TO REMAIN AS AN ALERT BECAUSE IT NEEDS TO BE MODAL TO STOP
					//OTHER EVENTS FROM PASSING UNDERNEATH
          alert("Maximum numeric value exceeded!");
					return false;
				}
			}
      // special case for decimals
      else if (sDBtype == "decimal" || sDBtype == "money")
      {
        var strValue = obj.value;
        if (event.keyCode == 190 || event.keyCode == 110)
        {
          if (strValue.indexOf(".") != -1)
          {
    				event.returnValue = false;
    				return false;
          }
        }
        else
        {
          if (!isNaN(sNewNumber)){ // this allow the entry of keys like HOME, END to pass thru and do the actual check if the keycode was numeric.
            if (strValue.indexOf(".") == -1) {
              if (strValue.length >= (iPrecision - iScale)) {
        				event.returnValue = false;
        				return false;
              }
            }
            else {
              if (strValue.length > iPrecision) {
        				event.returnValue = false;
        				return false;
              }
            }
          }
        }
      }
      // all others use the Precision and Scale values
			else if (parseFloat(obj.value + sNewNumber) > floatMaxValue)
			{
				//if (obj.value.length >= (iPrecision-iScale)) obj.select();

				event.returnValue = false;
        //ClientWarning("Maximum numeric value exceeded!");
				return false;
			}
		}
	}

	return true;
}

function GetRequestString(sName)
{
	var oDiv = document.getElementById(sName)

	var elms = oDiv.getElementsByTagName("INPUT");
	var sRequest = ""
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++)
	{
		if (elms[i].type != "button" && elms[i].name != "")
			sRequest += elms[i].name + "=" + escape(elms[i].value) + "&";
	}

	elms = oDiv.getElementsByTagName("TEXTAREA");
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++)
	{
		if (elms[i].name != "")
			sRequest += elms[i].name + "=" + escape(elms[i].value) + "&";
	}

	var sContextID = oDiv.getAttribute("ContextID");
	var sContextName = oDiv.getAttribute("ContextName");
	if (sContextName)
		sRequest += sContextName + "ID=" + sContextID + "&";

	var sLastUpdatedDate = "";
	if (oDiv.getAttribute("LastUpdatedDate"))
		sLastUpdatedDate = oDiv.getAttribute("LastUpdatedDate");
	sRequest += "SysLastUpdatedDate=" + sLastUpdatedDate + "&";

	if (sContextName)
		sRequest += sContextName + "SysLastUpdatedDate=" + sLastUpdatedDate + "&";

  return sRequest;
}


//this stuff is to allow events to happen to paint the status
//bar during CRUD actions
function ShowSB40()
{
	top.setSB(40,top.sb);
}
function ShowSB100()
{
	top.setSB(100,top.sb);
}
function ShowSB80()
{
	top.setSB(80,top.sb);
}


function DoCrudAction( oDiv, sAction, sRequest )
{
	var retArray = new Array;

	try
	{
		setTimeout(ShowSB40,1);

		var sProc = oDiv.getAttribute(sAction);
		var coObj = RSExecute("/rs/RSADSAction.asp", "RadExecute", sProc, sRequest );

		retArray = ValidateRS( coObj );

		setTimeout(ShowSB80,2);
		if ( retArray[1] == 1 )
		{
			gbDirtyFlag = false;
			if (typeof(parent.gbDirtyFlag)=='boolean')
				parent.gbDirtyFlag = false;

			resetControlBorders(oDiv);
		}
		setTimeout(ShowSB100,300);
		return retArray;
	}
	catch(e)
	{
		retArray[0] = e.message;
		retArray[1] = 0;
		setTimeout(showSB100,1);
		return retArray;
	}
}
// resets the border colors on control on the div passed in
function resetControlBorders(oDiv)
{
	var elms = oDiv.getElementsByTagName("INPUT");
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++)
	{
		if ((elms[i].type != "button") && (elms[i].readOnly == true) && (elms[i].style.borderColor == '#805050'))
		{
      elms[i].style.borderWidth = '0px';
			elms[i].dirty = false;
		}
		if ((elms[i].type != "button") && (elms[i].readOnly != true))
		{
			elms[i].style.borderColor = '#CCCCCC';
      elms[i].style.backgroundImage = "url(\/images\/input_bkgd.png)";
			elms[i].dirty = false;
		}
	}

	elms = oDiv.getElementsByTagName("TEXTAREA");
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++)
	{
		if (elms[i].readOnly != true)
		{
  		elms[i].style.borderColor = '#CCCCCC';
      elms[i].style.backgroundImage = "url(\/images\/input_bkgd.png)";
  		elms[i].dirty = false;
    }
	}

	elms = oDiv.getElementsByTagName("DIV");
  var iLength = elms.length;
	for(var i = 0; i < iLength; i++)
	{
		if (elms[i].className == "autoflowDiv")
		{
      if (elms[i].style.borderColor == '#805050')
      {
        elms[i].style.borderWidth = '0px';
    		elms[i].dirty = false;
      }
    }
	}

	resetSelectBoxBorder(oDiv);
}

function resetSelectBoxBorder(oDiv)
{
	var spans = oDiv.getElementsByTagName("SPAN");
	var selects = new Array();
	var index = 0;

	//scan all Spans tags for class name
  var iLength = spans.length;
	for (var i=0; i<iLength; i++)
	{
		if (spans[i].className == "select")
			selects[index++] = spans[i];
	}
	// for each select initialize each individual one
  var jLength = selects.length;
	for (var j=0; j<jLength; j++)
	{
		selects[j].children[0].style.borderColor='#CCCCCC';
    selects[j].children[0].style.backgroundImage = "url(\/images\/input_bkgd.png)";
	}
}

var inCrudDiv = false;
function ADS_Buttons(sName)
{
	var oDiv = document.getElementById(sName)

	// check for mis-use by not placing a CRUD attribute
	try
	{
		var sCRUD = oDiv.getAttribute("CRUD");
		var sVisible = "";
		var lngLeft = oDiv.offsetWidth - 135;
		var lngTop = -22;
        var bRead = false;
        var bCreate = false;
        var bUpdate = false;
        var bDelete = false;

        bRead   = (sCRUD.indexOf("R") != -1);
        bCreate = (sCRUD.indexOf("C") != -1);
        bUpdate = (sCRUD.indexOf("U") != -1);
        bDelete = (sCRUD.indexOf("D") != -1);

        /*alert( "Name: " + sName + "\n" +
               "Read: " + bRead + "\n" +
               "Create: " + bCreate + "\n" +
               "Update: " + bUpdate + "\n" +
               "Delete: " + bDelete );*/

		var sDiv = "<DIV id='crudDiv"+sName+"' onmouseover='inCrudDiv=true;' onmouseout='inCrudDiv=false;' style='position:absolute; visibility:hidden; top:"+lngTop+"px; left:" + lngLeft + "px;'>";
		sDiv += "<TABLE><TR><TD nowrap>";

		sVisible = "visibility:hidden;";
		//if (sCRUD.indexOf("C") != -1 && oDiv.getAttribute("Many") != null)
		if ((bRead && bCreate && bUpdate) && oDiv.getAttribute("Many") != null)
			sVisible =  "";
		sDiv += "<IMG src='/images/but_ADD_norm.png' style='cursor:hand; "+sVisible+"' id='"+sName+"_ADD' name='"+sName+"_ADD' onmousedown='changeImage(null,this.name,\"buttonAddDown\")' onmouseup='changeImage(null,this.name,\"buttonAddOver\")' onmouseover='changeImage(null,this.name,\"buttonAddOver\")' onmouseout='changeImage(null,this.name,\"buttonAdd\")' onclick='ADS_Pressed(\"Insert\",\""+sName+"\");' ondblclick=''>";
		sDiv += "<IMG src='/images/spacer.gif' alt='' width='4' height='1' border='0'>";

		sVisible = "visibility:hidden;";
		//if (sCRUD.indexOf("D") != -1 && oDiv.getAttribute("Many") != null)
		if ((bRead && bDelete) && oDiv.getAttribute("Many") != null)
			sVisible =  "";
		sDiv += "<IMG src='/images/but_DEL_norm.png' style='cursor:hand; "+sVisible+"' id='"+sName+"_DEL' name='"+sName+"_DEL' onmousedown='changeImage(null,this.name,\"buttonDelDown\")' onmouseup='changeImage(null,this.name,\"buttonDelOver\")' onmouseover='changeImage(null,this.name,\"buttonDelOver\")' onmouseout='changeImage(null,this.name,\"buttonDel\")' onclick='ADS_Pressed(\"Delete\",\""+sName+"\");' ondblclick=''>";
		sDiv += "<IMG src='/images/spacer.gif' alt='' width='4' height='1' border='0'>";

		sVisible = "visibility:hidden;";
		//if (sCRUD.indexOf("U") != -1)
		if (bRead && bUpdate)
			sVisible =  "";
		sDiv += "<IMG src='/images/but_SAVE_norm.png' style='cursor:hand; "+sVisible+"' id='"+sName+"_SAV' name='"+sName+"_SAV' onmousedown='changeImage(null,this.name,\"buttonSaveDown\")' onmouseup='changeImage(null,this.name,\"buttonSaveOver\")' onmouseover='changeImage(null,this.name,\"buttonSaveOver\")' onmouseout='changeImage(null,this.name,\"buttonSave\")' onclick='ADS_Pressed(\"Update\",\""+sName+"\");' ondblclick=''>";
		sDiv += "<IMG src='/images/spacer.gif' alt='' width='4' height='1' border='0'>";
		sDiv += "</TD></TR></TABLE></DIV>"

		document.write(sDiv);
        //alert(sName + "\n\n" + sDiv);
	}
	catch(e) {inCrudDiv = false;}

}

function disableADS(objDivWithButtons, bDisable){
  if (objDivWithButtons){
    var divADS = document.all["crudDiv" + objDivWithButtons.id];
    if (divADS) {
      var aImgs = divADS.getElementsByTagName("IMG");
      var iLength = aImgs.length;
      for (var i = 0; i < iLength; i++)
        aImgs[i].disabled = bDisable;
    }
  }
}

	var divWithButtons = null;
	var inDivOnFocusIn = false;
// Show the ADS buttons
function DivOnFocusIn(obj, bForceFlag)
{
	if (inDivOnFocusIn == true) return;
	inDivOnFocusIn = true;

	if (!bForceFlag) bForceFlag = false;

	var sCrudDiv = "crudDiv"+obj.id
	var oObj = document.getElementById(sCrudDiv);

	// check to make sure div as a CRUD div if not, a mis-use condition
	try
	{
		oObj.style.visibility="inherit";

		if (divWithButtons && inCrudDiv == false && (bForceFlag == true || obj != divWithButtons))
		{
			oObj = document.getElementById("crudDiv"+divWithButtons.id);
			if (oObj)
				oObj.style.visibility="hidden";

			if (gbDirtyFlag == true )
			{
				//var sSave = confirm("The Information in " + divWithButtons.name + " has Changed!  Do you want to Save?");
				var sSave = YesNoMessage("Need To Save", "The information in " + divWithButtons.name + " has changed!  Do you want to save?");
				if (sSave == "Yes")
					ADS_Pressed("Update", divWithButtons.id);
				else
				{
					gbDirtyFlag = false;
					if (typeof(parent.gbDirtyFlag)=='boolean')
						parent.gbDirtyFlag = false;

					gbInsertMode = false;

					var selObj = null;
					if (divWithButtons.Many == "true")
					{
						selObj = document.getElementById("sel"+divWithButtons.name)
						if (selObj)
						{
							if (selObj.options.length > 0)
								selObj.optionselect(null,0);
							sName = divWithButtons.id;
							selObj = document.getElementById(sName + "Sel");
							if (selObj)
							{
								selObj.style.borderColor = "#cccccc";
								if (divWithButtons.getAttribute("CRUD").indexOf("C") != -1)
									eval(sName+"_ADD.style.visibility='inherit'");
								if (divWithButtons.getAttribute("CRUD").indexOf("D") != -1)
									eval(sName+"_DEL.style.visibility='inherit'");
								if (divWithButtons.getAttribute("CRUD").indexOf("U") != -1)
									eval(sName+"_SAV.style.visibility='inherit'");
							}
						}
					}
				}
			}
		}

		divWithButtons = obj;
		inDivOnFocusIn = false;
	 	inCrudDiv = false;
	}
	catch(e)
	{
		inDivOnFocusIn = false;
	 	inCrudDiv = false;
	}
}

function MainTabChange()
{
	//check main page
	if (gbDirtyFlag == true)
	{
		if (divWithButtons)
			DivOnFocusIn(divWithButtons, true);
	}
	//check child frames
  var idxLength = document.frames.length;
	for (var idx=0; idx < idxLength; idx++)
	{
		try{ //some of the frames may not include formvalid.js
			document.frames[idx].MainTabChange();
		}catch(e){}
	}

}


	var gbInsertMode = false;
	var onTabChangeBubble = new Function();
function tabChange()
{
	//var sType = this.relatedTab.content.name;
	var sName = this.relatedTab.content.id;
	var oDiv = this.relatedTab.content;

	if (gbDirtyFlag == true)
		DivOnFocusIn(this.activeTab.content);

	if (divWithButtons && inCrudDiv == false)
	{
		oObj = document.getElementById("crudDiv"+divWithButtons.id);
		if (oObj)
				oObj.style.visibility="hidden";
	}
	divWithButtons = this.activeTab.content;

	if (gbDirtyFlag == false)
	{
		var sCrudDiv = "crudDiv"+sName;
		var oObj = document.getElementById(sCrudDiv);
		if (oObj)
			oObj.style.visibility="hidden";
		sCrudDiv = "crudDiv"+this.activeTab.content.id;
		oObj = document.getElementById(sCrudDiv);
		if (oObj)
			oObj.style.visibility="inherit";
		//this.activeTab.content.all[5].focus();
	}

	if (this.activeTab.content.Many == "true" && parseInt(this.activeTab.content.getAttribute("Count")) == 0)
	{
		sName = this.activeTab.content.id;
		//if (this.activeTab.content.getAttribute("CRUD").indexOf("C") != -1)
        //display add button only when the user can read, create and update.
		if (this.activeTab.content.getAttribute("CRUD").indexOf("C") != -1 &&
            this.activeTab.content.getAttribute("CRUD").indexOf("R") != -1 &&
            this.activeTab.content.getAttribute("CRUD").indexOf("U") != -1)
			eval(sName+"_ADD.style.visibility='inherit'");
		eval(sName+"_DEL.style.visibility='hidden'");
		eval(sName+"_SAV.style.visibility='hidden'");
	}

	onTabChangeBubble(this);
}


function UpdateTabTextCount(oDiv)
{
	var sDivNum = oDiv.id
	sDivNum = sDivNum.substr(sDivNum.length-2);
	var oTabSpan = document.getElementById("tab"+sDivNum)
	var sStr = oTabSpan.innerText;
	sStr = sStr.substr(0, sStr.indexOf("(")) + "(" + oDiv.Count + ")";
	oTabSpan.innerText = sStr;
}

function onBCPC(selobj)
{
	var selValue = selobj.options[selobj.selectedIndex].value;
	var sTextName = "txt"+selobj.id;
	var oText = document.getElementById(sTextName);
	oText.value = selValue;

	gbDirtyFlag = true;
	if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = true;
	selobj.children[0].style.borderColor='#805050';
}

//shows/hides injury fields in Involved
function onInjC(selobj)
{
	var selValue = selobj.options[selobj.selectedIndex].value;
    var sTextName = selobj.id;
    sTextName = sTextName.slice(3);
	var oText = document.getElementById(sTextName);
	oText.value = selValue;
  ShowHideInjuryFlds();

  gbDirtyFlag = true;
	if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = true;
	selobj.children[0].style.borderColor='#805050';
}

// Generic combo on select
function onSelectChange(selobj)
{
	gbDirtyFlag = true;
	if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = true;
	selobj.children[0].style.borderColor='#805050';
}

function CalculateAge(objDate, objAge)
{
	if (objAge.value != "") return;

    if (objDate.value == "")
	{
		objAge.value = "";
		return;
	}

  if (CheckDate(objDate) == false)
	{
    ChangeInputBorder(objDate)
		return;
	}

	var myDate = new Date(objDate.value);
  objAge.value = Math.round((new Date() - myDate) / 24 / 60 / 60 / 1000 / 365.25);

  if (objAge.value <= 0)
    objAge.value = '';
}



function ValidateEndDate(obj,StartDate,EndDate)
{
   var currentDate= new Date(); 
   
   var SDateObj = document.getElementById('txtRepairStartDate');
   
   if (SDateObj.value == '' && obj.value == '') return true;
   if (SDateObj.value != '' && obj.value == '') return true; 
    
	 if (SDateObj.value == '' && obj.value != '') 
	  {
	   ClientWarning("Please Enter Start Date"); 
	   SDateObj.select();
	   SDateObj.focus(); 
       return false;
	  }  
     else
     if ( Date.parse(EndDate.value) >= Date.parse(currentDate) )
      { 
        ClientWarning("Date value cannot be future date");
		obj.select();
     	obj.focus();	
        return false;
      }
     else
     if (Date.parse(StartDate.value) >= Date.parse(EndDate.value))
     {
       ClientWarning("End Date must be greater than Start Date");
	   obj.select();
	   obj.focus();
       return false;
     }
	 else
	   return true; 
}

  

function LeapYear(year)
{
  if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
    return true;
  else
    return false;
}

function ChangeInputBorder(obj)
{
		obj.style.borderColor = "#7f0000";
}

function ShowHideTxtArea(id, visibility, obj)
{
	div = document.getElementById(id);
	if (obj) div.children[0].value = obj.value;
	div.style.visibility = visibility;
}

function UTCConvertDate(vDate)
{
	if (vDate == "") return "";
	var vYear = vDate.substr(0,4);
  var vMonth = vDate.substr(5,2);
  var vDay = vDate.substr(8,2);
  vDate = vMonth + '/' + vDay  + '/' + vYear
  return vDate;
}

function UTCConvertTime(vDate)
{
    if (vDate == "") return "";
		var ampm = 'AM'
    var hh = vDate.substr(11,2)
    var mm = vDate.substr(14,2)
    var ss = vDate.substr(17,2)
    if (hh > 12)
    {
      hh = hh-12;
      ampm = 'PM';
    }
    vDate = hh + ':' + mm  + ':' + ss  + ' ' + ampm;
    return vDate;
}

function removeSpaces(strIn)
{
	/*var stemp = "";
	var k = 0;
        var idxLength = strIn.length;
	for(var idx=0; idx < idxLength; idx++)
	{
		if (strIn[idx] != " "){ stemp[k] = strIn[idx];
		k++; }
	}
	
    */	
    /* stemp = strIn.replace(/\s+/g,""); return stemp; */
    while (strIn.substring(0,1) == ' ')  strIn = strIn.substring(1, strIn.length);
    while (strIn.substring(strIn.length-1,strIn.length) == ' ')  strIn = strIn.substring(0, strIn.length-1);
    return strIn; 
}

function YesNoMessage(sTitle, sMessage)
{
	var strReturn = sTitle + "|" + sMessage;
	var iDialogHeight;

	// retrieve optional argument which specifies Dialog Height or default to 125px.
	iDialogHeight = arguments.length > 2 ? arguments[2] : 125;

	strReturn = window.showModalDialog("/js/YesNoDialog.htm",strReturn,"dialogHeight: " + iDialogHeight + "px; dialogWidth: 450px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
	return strReturn;
}

function ValidatePhone(obj) {
	var sValue = String(obj.value);

	if ( sValue == "() -" || sValue == "" || obj.type == 'password') return;

	//retrieve all numeric characters found
	sValue = parseNumber(sValue);

	//this will check if numeric characters were found
	if ((sValue.length == 0) && (obj.value.length > 0)) {
        ClientWarning("Invalid characters were found in the phone number. Only numeric characters are allowed.");
		obj.select();
		obj.focus();
		return;
	}

	//if any part of phone number specified, then all the sections must be completed.
	if ((sValue.length > 0) && (sValue.length < 10)) {
        ClientWarning("Phone number must have an area code, an exchange and a number in the format (xxx) xxx-xxxx.");
		obj.select();
		obj.focus();
		return;
	}

	//this expression will extract the first 3 chars for area code, next 3 for exchange and the last 4 for phone number.
	//if the length of the phone number entered is greater than 10 (3+3+4) the remaining are truncated.
	var re = /^(\d{3})(\d{3})(\d{4})/;
	var ret = sValue.match(re);
	//ret can never be null. We would have reached this LOC only after passing the above check of length > 0 and < 10.

	//check if the area code is valid
	if (!isAreaCode(ret[1])) {
        ClientWarning("Invalid area code specified.");
		obj.select();
		obj.focus();
		return;
	}

	if (!isPhoneExchange(ret[2])) {
        ClientWarning("Invalid exchange specified.");
		obj.select();
		obj.focus();
		return;
	}

	//format the phone number and refresh the control.
	obj.value = "(" + ret[1]+ ") " + ret[2] + "-" + ret[3];
}

//checks if the input is an area code
function isAreaCode(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if (!(sVal.length == 3)) return false;
	//area code cannot start with 0 or 1
	if (parseInt(sVal.substr(0,1)) < 2) return false;

	return true;
}

//checks if the input is a phone exchange
function isPhoneExchange(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 3)) return false;
	//if (sValue == "000") return false;

	return true;
}

//checks if the input is a phone number
function isPhoneNumber(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 4)) return false;

	return true;
}

//Check Area code
var bInCheckAreaCode = false;
function checkAreaCode(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;
  if (obj.dirty == false) return;

  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isAreaCode(sVal) && !(bInCheckAreaCode)) {
    event.returnValue = false;
    bInCheckAreaCode = true;
    ClientWarning("Invalid phone area code specified.");
		if (obj !== objTo)
      obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckAreaCode = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Exchange
var bInCheckPhoneExchange = false;
function checkPhoneExchange(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  if (obj.dirty == false) return;
  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isPhoneExchange(sVal) && !(bInCheckPhoneExchange)) {
    event.returnValue = false;
    bInCheckPhoneExchange = true;
    ClientWarning("Invalid phone exchange specified.");
    obj.select();
    obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneExchange = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Number
var bInCheckPhoneNumber = false;
function checkPhoneNumber(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  if (obj.dirty == false) return;
  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (obj.type == "password") return;
	if (sVal.length > 0)
	if (!isPhoneNumber(sVal) && !(bInCheckPhoneNumber)) {
    event.returnValue = false;
    bInCheckPhoneNumber = true;
    ClientWarning("Invalid phone number specified.");
    obj.select();
    obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneNumber = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//check if the input param is numeric
function isNumeric(sValue) {
    return !isNaN(sValue);
}

//Parse and return only numeric characters found as a string.
function parseNumber(sValue) {
	var strRet = "";
  var iLength = sValue.length;
	for (var i=0; i < iLength; i++)
		if (!isNaN(parseInt(sValue.substr(i, 1))))
			strRet += sValue.substr(i, 1);
	return strRet;
}

//Takes a string and removes the leading and trailing spaces and also any extra spaces in the string. IE only.
function remove_XS_whitespace(item)
{
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
  for (index = 0; index < item_length; index++)
  {
    if (item.value.charAt(index) != ' ')
    {
      tmp += item.value.charAt(index);
    }
    else
    {
      if (tmp.length > 0)
      {
        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1)
        {
          tmp += item.value.charAt(index);
        }
      }
    }
  }
  item.value = tmp;
}

//function that will find missing phone sections and focus
function validatePhoneSections(objAreaCode, objExchange, objUnit) {
    var srcElementID = '';
    srcElementID = document.activeElement.id;
		if (objAreaCode.type == "password") return true;

    if (objAreaCode.dirty == false && objExchange.dirty == false && objUnit.dirty == false) return true;
    /*if (objAreaCode.id == srcElementID)
        if (objAreaCode.value.length > 0)
            if (!isAreaCode(objAreaCode.value)){
                objAreaCode.onblur();
                return false;
            }
    if (objExchange.id == srcElementID)
        if (objExchange.value.length > 0)
            if (!isPhoneExchange(objExchange.value)){
                objExchange.onblur();
                return false;
            }
    if (objUnit.id == srcElementID)
        if (objUnit.value.length > 0)
            if (!isPhoneNumber(objUnit.value)){
                objUnit.onblur();
                return false;
            }*/

    if ((objAreaCode.value.length == 0) || (objExchange.value.length == 0) || (objUnit.value.length == 0)) {
        //all the sections are empty - this is OK.
        if ((objAreaCode.value.length == 0) && (objExchange.value.length == 0) && (objUnit.value.length == 0)) return true;

        if (isAreaCode(objAreaCode.value) && isPhoneExchange(objExchange.value) && isPhoneNumber(objUnit.value)) return true;
        //some sections are empty - not OK.
        ClientWarning("Some sections of the phone number are missing.  Please enter a valid area code, exchange and phone number.");
        if ((objAreaCode.value.length == 0) || (!isAreaCode(objAreaCode.value))) {
          try {
            objAreaCode.focus();
            objAreaCode.select();
          } catch (e) {}
          return false;
        }
        if ((objExchange.value.length == 0) || (!isPhoneExchange(objExchange.value))) {
          try {
            objExchange.focus();
            objExchange.select();
          } catch (e) {}
          return false;
        }
        if ((objUnit.value.length == 0) || (!isPhoneNumber(objUnit.value))) {
          try {
            objUnit.focus();
            objUnit.select();
          } catch (e) {}
          return false;
        }
        //this line will never be executed. Just in case!!.
        //objAreaCode.focus();
        //objAreaCode.select();
        return false;
    }
    else {
        if (!isAreaCode(objAreaCode.value)) {
            ClientWarning("Invalid phone area code specified.");
            try {
              objAreaCode.focus();
              objAreaCode.select();
            } catch (e) {}
            return false;
        }
        if (!isPhoneExchange(objExchange.value)) {
            ClientWarning("Invalid phone exchange specified.");
            try {
              objExchange.focus();
              objExchange.select();
            } catch (e) {}
            return false;
        }
        if (!isPhoneNumber(objUnit.value)) {
            ClientWarning("Invalid phone number specified.");
            try {
              objUnit.focus();
              objUnit.select();
            } catch (e) {}
            return false;
        }
    }
    return true;
}

// Function that will prevent page to navigate one level back in history when backspace is pressed in a
// read-only field. This function must be called in body's onkeyDown event.
function docNoBkSpace()  {
    if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))){ //alt+left arrow, alt+ right arrow
        event.returnValue = false;
        return;
    }
    if ((event.altKey) && (event.keyCode == 36)){
        event.returnValue = false;
        return;
    }

    if (event.keyCode == 8) {
    	var srcTagName;
    	srcTagName = event.srcElement.tagName;

        switch (srcTagName) {
            case 'INPUT':
        		if (event.srcElement.readOnly == true ||
        			event.srcElement.type == 'checkbox' ||
        			event.srcElement.type == 'radio' ||
        			event.srcElement.type == 'button')
        		    event.returnValue = false;
        		break;
            case 'TEXTAREA':
        		if (event.srcElement.readOnly == true)
        			event.returnValue = false;
                break;
            default:  event.returnValue = false;
        }
    }
/*	if ((srcTagName == 'INPUT') || (srcTagName == 'TEXTAREA')) {
		if (event.srcElement.readOnly == true) {
			if (event.keyCode == 8) //backspace
				event.returnValue = false;
		}
	}*/
}

//this will attach the docNoBkSpace function to the document which will alter all documents that include this script file.
document.onkeydown = docNoBkSpace;

//process the clipboard with Numbers only data and reset clipboard with the processed value.
function onBeforePasteNumbersOnly(){
    var sNewString = '';
    sNewString = window.clipboardData.getData("Text");
    if (sNewString != null) {
        sNewString = parseNumbersOnly(sNewString);
        window.clipboardData.setData("Text", sNewString);
    }
}
//allow paste if the clipboard has Numeric data
function onPasteNumbersOnly() {
    var sNewString = '';
    sNewString = window.clipboardData.getData("Text");
    if (sNewString != null){
        if ((isNumeric(sNewString) == false) || (sNewString == '')) {
            ClientWarning("Clipboard has invalid characters or is empty. Cannot paste these into this edit field.");
            event.returnValue = false;
        }
        else {
            event.srcElement.value = ''; //this line prevents pasting in between controls' value.
            event.returnValue = true;
            setControlDirty(event.srcElement);
        }
    }
}

function setControlDirty(obj){
	obj.style.borderColor='#805050';
	obj.dirty = true;
	gbDirtyFlag = true;

	if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = true;
}

function onCutPasteSetDirty(){
    var obj = event.srcElement;
    setControlDirty(obj);
}

//this function simulates the NumbersOnly functionality. This function is intended to be used by the onpaste event.
function parseNumbersOnly(sValue){
	var strRet = "";
    var iChrCode;
    var iNxtChrCode;
    var bAllowDecimal = false;
    var sdbType = "";
    var iPrecision = 0;
    var iScale = 0;
    var bDecimal = false;

    bAllowDecimal = event.srcElement.getAttribute("allowDecimal");
    sdbType = event.srcElement.getAttribute("dbtype");
    iPrecision = parseInt(event.srcElement.getAttribute("Precision"));
    iScale = parseInt(event.srcElement.getAttribute("Scale"));
    var iLength = sValue.length;
  	for (var i=0; i < iLength; i++){
        iChrCode = sValue.charCodeAt(i);

        if (bDecimal == true) {
            //checks if the decimal scale does not exceed.
            if ((strRet.split(".")[1].length) >= iScale)
                return strRet;
        }
        if ((strRet.indexOf(".") == -1) && (strRet.length >= (iPrecision-iScale))) {
            if (iChrCode >= 48 && iChrCode <= 57 && (iScale > 0) && (sdbType == "money")) {
                strRet += ".";
                bDecimal = true;
            }
        }

        if (iChrCode >= 48 && iChrCode <= 57)
            strRet += sValue.substr(i, 1);
        else {
            if (i < sValue.length)
                iNxtChrCode = sValue.charCodeAt(i+1);
            else
                iNxtChrCode = -1;
            switch (iChrCode){
                case 45:// "-"
                    break;
                case 46:// "."
                        //for shop maintenance screens we don't have dbtype, precision and scale.
                        if ((bAllowDecimal == true) && (strRet.indexOf('.') == -1)){ // a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if ((sdbType == "money") && (iScale > 0) && (strRet.indexOf('.') == -1)) {// a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if (sdbType == "tinyint") {
                            //rounds to the nearest integer and returns
                            if (iNxtChrCode >= 53 && iNxtChrCode <= 57)
                                strRet = (parseInt(strRet) + 1).toString();
                            return strRet;
                        }
                        if (sdbType == "varchar")
                            continue;
                    break;
                case 47:// "/"
                    break;
                default:
                    if (sdbType != "varchar")
                        return strRet;
            }//switch
        }//else
    }//for
	return strRet;
}

//this function will attach onpaste and onbeforepaste events to all INPUTs who have NumbersOnly function attached
//to the onkeypress event.
//Call this function from a page's onLoad event.
function onpasteAttachEvents(){
    var oObjInputs = null;
    var strOnKeyPress = '';
    oObjInputs = document.all.tags('INPUT');
    var iLength = oObjInputs.length;
    for (var i = 0; i < iLength; i++) {
        try {
            strOnKeyPress = '';
            strOnKeyPress = oObjInputs[i].onkeypress;
            if (strOnKeyPress != null) {
                if (strOnKeyPress.toString().toUpperCase().indexOf('NUMBERSONLY') > 0){
                    oObjInputs[i].attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
                    //remove the generic paste event which was set in the InitFields().
                    oObjInputs[i].onpaste = new Function("");
                    //attach the more specific onpaste function.
                    oObjInputs[i].attachEvent('onpaste', onPasteNumbersOnly);
                }
            }
        }
        catch (e) {continue;}
    }
}

//this function will attach onpaste and onbeforepaste events to the object specified.
function attachPasteEvents(obj){
    if (obj) {
        obj.attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
        //remove the generic paste event which was set in the InitFields().
        obj.onpaste = new Function("");
        //attach the more specific onpaste function.
        obj.attachEvent('onpaste', onPasteNumbersOnly);
    }
}

function getClaimVehicles(obj){
    if (obj.value != ""){
        var co = RSExecute("/rs/RSGetVehicles.asp", "getVehicles", obj.value);
    	if (co.status == 0)
        {
            sTblData = co.return_value;
            var listArray = sTblData.split("||");
            if ( listArray[1] == "0" )
                ServerEvent();
            else
                return listArray[0];
        }
        else ServerEvent();
    }
}

//Function to handle area code splits.
var AreaCodeSplitsFlagged = new Array();
function validateAreaCodeSplit(objAreaCode, objExchange){
  var sAreaCode, sExchange, sTmp;
  sAreaCode = objAreaCode.value;
  sExchange = objExchange.value;
  sTmp = sAreaCode + "|" + sExchange + "|";

  //Check to see if the area code + exchange was already notified.
  var iLength = AreaCodeSplitsFlagged.length;
  for (var i = 0; i < iLength; i++){
    if (AreaCodeSplitsFlagged[i].indexOf(sTmp) != -1){
      //this area code and exchange was already notified to the user. Silently update the area code.
      var aTmp = AreaCodeSplitsFlagged[i].split("|");
      if (aTmp.length == 3){
        objAreaCode.value = aTmp[2];
        return true;
      }
    }
  }
  // this seems to be a new area code + exchange, check for the area code split
  var coObj = RSExecute("/rs/RSValidateAreaCode.asp", "IsValidAreaCode", sAreaCode, sExchange);
  retArray = ValidateRS( coObj );
  if (retArray[1] == 1){
    var retVal = retArray[0].split(",");
    if (retVal[0] == "false"){
      //area code + exchange is scheduled for a split. Notify user.
      ClientWarning("Phone number starting with " + sAreaCode + " " + sExchange + " is scheduled for a change to the new area code " + retVal[1] + ".<br>" +
                    "The system will automatically change all occurances of " + sAreaCode + " " + sExchange + " to " + retVal[1] + " " + sExchange);
      //update the area code field to the new one.
      objAreaCode.value = retVal[1];
      //save this in an array so that this combination will not be notified again.
      AreaCodeSplitsFlagged.push(sAreaCode + "|" + sExchange + "|" + retVal[1]);
    }
  }
  return true;
}


  //generic currency formating x.00
  function formatcurrency(X)
  {
    var roundNum = Math.round(X*100)/100; //round to 2 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.00';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 3);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  //generic hours formating x.0
  function formathours(X)
  {
    var roundNum = Math.round(X*10)/10; //round to 1 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 2);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  //generic percentage formating x.000
  function formatpercentage(X)
  {
    var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.000';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '00';
    else if (strValue.indexOf('.') == strValue.length - 3)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 6);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  function phoneAutoTab(obj, objNext, objAttrib){
    if (obj) {
      if (obj.value.length == parseInt(obj.getAttribute("maxlength"))){
        switch (objAttrib) {
          case "A":
            if (!isAreaCode(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;
          case "E":
            if (!isPhoneExchange(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;
          case "U":
            if (!isPhoneNumber(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;          
        }
        if ((obj.defaultValue == "") || 
            (obj.getAttribute("prevValue") != "" && obj.value != obj.defaultValue && obj.getAttribute("prevValue") != obj.value) //obj.defaultValue != "" && 
           ){
          if (objNext){
            obj.setAttribute("defaultValue", obj.value, 0); 
            objNext.focus();
            objNext.select();
            //setControlDirty(objNext);
            return;
          }
        }
        if (obj.value == obj.defaultValue)
          obj.setAttribute("defaultValue", "-1", 0);
        if (obj.value == obj.getAttribute("prevValue"))
          obj.setAttribute("prevValue", "-1", 0);        
      }
    }
  }

  function isURL(strURL) {
      return ((strURL.search(/^(((http:\/\/)|(www\.)))[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}[ ]{0,}$/) == 0) ||
              (strURL.search(/^[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}[ ]{0,}$/) == 0))
  }
  
  function isEmail(strEmail) {
      return (strEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}[ ]{0,}$/) == 0)
  }  
  
  function checkEMail(obj){
    if (obj.value != ""){
      if (!isEmail(obj.value)) {
        event.returnValue = false;
        ClientWarning("Invalid E-Mail specified.<br>Example: johndoe@server.com<br><br>Please try again");
        try {
          obj.focus();
          obj.select();
        } catch (e) { }
      }
    }
  }
  
  function checkURL(obj){
    if (obj.value != ""){
      if (!isURL(obj.value)) {
        event.returnValue = false;
        ClientWarning("Invalid Internet website address (URL) specified.<br>Example: http://www.yahoo.com<br><br>Please try again");
        try {
          obj.focus();
          obj.select();
        } catch (e) { }
      }
    }
  }


/*------------------------------------------------------------------------------------*/
/*	makeHighlight
	  Description: Image opacity filter changer
	  Parameters:
      cur - object
      which - no opacity=0; with opacity=1
      level - opcacity level= 100:no opacity, 20:20% opacity
	  Usage: 
		  60% opacity = makeHighlight(this,1,60)
      No opacity = makeHighlight(this,0,100)
*/
function makeHighlight(cur,which,intensity)
{
  if (which==0)
    cur.filters.alpha.opacity=intensity;
  else
    cur.filters.alpha.opacity=intensity;
}


