
//**************************************************************************
//**        Estimate Summary
//*************************************************************************

  // set page to original mode when the Orig Moade icon is pressed
  function forceOriginalMode()
  {
    if (gbDirtyFlag == true )
    {
      var obj = document.getElementById('content13');
      var sSave = YesNoMessage("Need To Save", "The information in " + obj.name + " has changed!  Do you want to save?");
      if (sSave == "Yes")
      	summary_ADS_Pressed();
    }
    gsOriginalMode = true;
    setOriginalMode();
  }

  
  //calculate Original column Totals
  function getOriginalTotals()
  {
    txtOriginalExtendedAmt_7.value = formatcurrency(Number(txtOriginalExtendedAmt_1.value) + Number(txtOriginalExtendedAmt_2.value) + Number(txtOriginalExtendedAmt_3.value) + Number(txtOriginalExtendedAmt_4.value) + Number(txtOriginalExtendedAmt_5.value) + Number(txtOriginalExtendedAmt_6.value));
    txtOriginalExtendedAmt_10.value = formatcurrency(Number(txtOriginalExtendedAmt_8.value) + Number(txtOriginalExtendedAmt_9.value));
    txtOriginalExtendedAmt_11.value = formatcurrency(Number(txtOriginalExtendedAmt_7.value) + Number(txtOriginalExtendedAmt_10.value));
    txtOriginalExtendedAmt_18.value = formatcurrency(Number(txtOriginalExtendedAmt_12.value) + Number(txtOriginalExtendedAmt_13.value) + Number(txtOriginalExtendedAmt_14.value) + Number(txtOriginalExtendedAmt_15.value) + Number(txtOriginalExtendedAmt_16.value) + Number(txtOriginalExtendedAmt_17.value));
    txtOriginalExtendedAmt_23.value = formatcurrency(Number(txtOriginalExtendedAmt_19.value) + Number(txtOriginalExtendedAmt_20.value) + Number(txtOriginalExtendedAmt_21.value) + Number(txtOriginalExtendedAmt_22.value));
    txtOriginalExtendedAmt_24.value = formatcurrency(Number(txtOriginalExtendedAmt_11.value) + Number(txtOriginalExtendedAmt_18.value) + Number(txtOriginalExtendedAmt_23.value));
    txtOriginalExtendedAmt_29.value = formatcurrency(Number(txtOriginalExtendedAmt_25.value) + Number(txtOriginalExtendedAmt_26.value) + Number(txtOriginalExtendedAmt_27.value) + Number(txtOriginalExtendedAmt_28.value));
    txtOriginalExtendedAmt_35.value = formatcurrency(Number(txtOriginalExtendedAmt_32.value) + Number(txtOriginalExtendedAmt_33.value) + Number(txtOriginalExtendedAmt_34.value) + Number(txtOriginalExtendedAmt_38.value));

    //if (txtOriginalUnitAmt_30.value > 0)
    //  txtOriginalExtendedAmt_31.value = formatcurrency(Number(txtOriginalUnitAmt_30.value));
    //else
      txtOriginalExtendedAmt_31.value = formatcurrency(Number(txtOriginalExtendedAmt_24.value) + Number(txtOriginalExtendedAmt_29.value));

    txtOriginalExtendedAmt_36.value = formatcurrency(Number(txtOriginalExtendedAmt_31.value) - Number(txtOriginalExtendedAmt_35.value));
  }

  
  //calculate Agreed column Totals
  function getAgreedTotals()
  {
    var objRow;
    if ( txtAgreedExtendedAmt_7.value != formatcurrency(Number(txtAgreedExtendedAmt_1.value) + Number(txtAgreedExtendedAmt_2.value) + Number(txtAgreedExtendedAmt_3.value) + Number(txtAgreedExtendedAmt_4.value) + Number(txtAgreedExtendedAmt_5.value) + Number(txtAgreedExtendedAmt_6.value)) )
    {
      txtAgreedExtendedAmt_7.value = formatcurrency(Number(txtAgreedExtendedAmt_1.value) + Number(txtAgreedExtendedAmt_2.value) + Number(txtAgreedExtendedAmt_3.value) + Number(txtAgreedExtendedAmt_4.value) + Number(txtAgreedExtendedAmt_5.value) + Number(txtAgreedExtendedAmt_6.value));
      objRow = txtAgreedExtendedAmt_7.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_10.value != formatcurrency(Number(txtAgreedExtendedAmt_8.value) + Number(txtAgreedExtendedAmt_9.value)) )
    {
      txtAgreedExtendedAmt_10.value = formatcurrency(Number(txtAgreedExtendedAmt_8.value) + Number(txtAgreedExtendedAmt_9.value));
      objRow = txtAgreedExtendedAmt_10.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_11.value != formatcurrency(Number(txtAgreedExtendedAmt_7.value) + Number(txtAgreedExtendedAmt_10.value)) )
    {
      txtAgreedExtendedAmt_11.value = formatcurrency(Number(txtAgreedExtendedAmt_7.value) + Number(txtAgreedExtendedAmt_10.value));
      objRow = txtAgreedExtendedAmt_11.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_18.value != formatcurrency(Number(txtAgreedExtendedAmt_12.value) + Number(txtAgreedExtendedAmt_13.value) + Number(txtAgreedExtendedAmt_14.value) + Number(txtAgreedExtendedAmt_15.value) + Number(txtAgreedExtendedAmt_16.value) + Number(txtAgreedExtendedAmt_17.value)) )
    {
      txtAgreedExtendedAmt_18.value = formatcurrency(Number(txtAgreedExtendedAmt_12.value) + Number(txtAgreedExtendedAmt_13.value) + Number(txtAgreedExtendedAmt_14.value) + Number(txtAgreedExtendedAmt_15.value) + Number(txtAgreedExtendedAmt_16.value) + Number(txtAgreedExtendedAmt_17.value));
      objRow = txtAgreedExtendedAmt_18.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_23.value != formatcurrency(Number(txtAgreedExtendedAmt_19.value) + Number(txtAgreedExtendedAmt_20.value) + Number(txtAgreedExtendedAmt_21.value) + Number(txtAgreedExtendedAmt_22.value)) )
    {
      txtAgreedExtendedAmt_23.value = formatcurrency(Number(txtAgreedExtendedAmt_19.value) + Number(txtAgreedExtendedAmt_20.value) + Number(txtAgreedExtendedAmt_21.value) + Number(txtAgreedExtendedAmt_22.value));
      objRow = txtAgreedExtendedAmt_23.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_24.value != formatcurrency(Number(txtAgreedExtendedAmt_11.value) + Number(txtAgreedExtendedAmt_18.value) + Number(txtAgreedExtendedAmt_23.value)) )
    {
      txtAgreedExtendedAmt_24.value = formatcurrency(Number(txtAgreedExtendedAmt_11.value) + Number(txtAgreedExtendedAmt_18.value) + Number(txtAgreedExtendedAmt_23.value));
      objRow = txtAgreedExtendedAmt_24.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_29.value != formatcurrency(Number(txtAgreedExtendedAmt_25.value) + Number(txtAgreedExtendedAmt_26.value) + Number(txtAgreedExtendedAmt_27.value) + Number(txtAgreedExtendedAmt_28.value)) )
    {
      txtAgreedExtendedAmt_29.value = formatcurrency(Number(txtAgreedExtendedAmt_25.value) + Number(txtAgreedExtendedAmt_26.value) + Number(txtAgreedExtendedAmt_27.value) + Number(txtAgreedExtendedAmt_28.value));
      objRow = txtAgreedExtendedAmt_29.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_35.value != formatcurrency(Number(txtAgreedExtendedAmt_32.value) + Number(txtAgreedExtendedAmt_33.value) + Number(txtAgreedExtendedAmt_34.value) + Number(txtAgreedExtendedAmt_38.value)) )
    {
      txtAgreedExtendedAmt_35.value = formatcurrency(Number(txtAgreedExtendedAmt_32.value) + Number(txtAgreedExtendedAmt_33.value) + Number(txtAgreedExtendedAmt_34.value) + Number(txtAgreedExtendedAmt_38.value));
      objRow = txtAgreedExtendedAmt_35.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_31.value != formatcurrency(Number(txtAgreedExtendedAmt_24.value) + Number(txtAgreedExtendedAmt_29.value)) )
    {
      txtAgreedExtendedAmt_31.value = formatcurrency(Number(txtAgreedExtendedAmt_24.value) + Number(txtAgreedExtendedAmt_29.value));
      objRow = txtAgreedExtendedAmt_31.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if ( txtAgreedExtendedAmt_36.value != formatcurrency(Number(txtAgreedExtendedAmt_31.value) - Number(txtAgreedExtendedAmt_35.value)) )
    {
      txtAgreedExtendedAmt_36.value = formatcurrency(Number(txtAgreedExtendedAmt_31.value) - Number(txtAgreedExtendedAmt_35.value));
      objRow = txtAgreedExtendedAmt_36.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }
  }

  
  //calculate across when pageinit
  function initCalcRows()
  {
  	var oDiv = document.getElementById('taxableTotals');
    var flds = oDiv.getElementsByTagName("INPUT");
    var objRow;
    var oRowCnt;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      objRow = flds[x].parentElement.parentElement;
      if ((objRow.id) && (objRow.id != oRowCnt))
      {
        oRowCnt = objRow.id;
        calcRow(objRow);
      }
    }

  	oDiv = document.getElementById('nontaxableTotals');
    flds = oDiv.getElementsByTagName("INPUT");
    objRow = "";
    oRowCnt = "";
    xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      objRow = flds[x].parentElement.parentElement;
      if ((objRow.id) && (objRow.id != oRowCnt))
      {
        oRowCnt = objRow.id;
        calcRow(objRow);
      }
    }
  }


  //Put inputs fields in Original screen mode
  function setOriginalMode()
  {
    gsUpdOrigAndAgrd = true;
    document.all["SetOriginalModeIcon"].style.display = "none";
    document.all["SetDupFieldsIcon"].style.display = "none";
    //change attributes in INPUT tags in each of these spans
  	setFldsOrigMode1('taxableTotals1');
  	setFldsOrigMode1('taxableTotals2');
  	setFldsOrigMode1('taxableTotals3');
  	setFldsOrigMode1('taxableTotals4');
  	setFldsOrigMode2('nontaxableTotals');
  }

  
  //Put inputs fields in Original screen mode
  function setFldsOrigMode1(sSpan)
  {
  	var oDiv1 = document.getElementById(sSpan);
    var flds = oDiv1.getElementsByTagName("INPUT");
    var objRow;
    var cRow;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {

      if ((flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalHrs' || flds[x].name == 'OriginalUnitAmt') && (flds[x].type != 'hidden'))
      {
        objRow = flds[x].parentElement.parentElement;
        cRow = objRow.getAttribute("rowID");
        if (objRow.id)
        {
          if (cRow == 1 || cRow == 2 || cRow == 5 || cRow == 6 || cRow == 17 || cRow == 19 || cRow == 20 || cRow == 21 || cRow == 22)
          {
            flds[x+3].className = "inputDimmed";
            flds[x+3].readOnly = true;
            flds[x+3].disabled = true;
            flds[x+3].onChange = '';
          }
          else
          {
            flds[x+4].className = "inputDimmed";
            flds[x+4].readOnly = true;
            flds[x+4].disabled = true;
            flds[x+4].onChange = '';
          }
          flds[x].className = "inputField";
          flds[x].readOnly = false;
          flds[x].disabled = false;
          flds[x].systemfield = false;
        }
      }

      if (flds[x].name == 'Comments')
      {
        flds[x].className = "InputReadonlyField";
        flds[x].readOnly = true;
        flds[x].disabled = true;
      }
    }
  }

  
  //Put inputs fields in Original screen mode
  function setFldsOrigMode2(sSpan)
  {
  	var oDiv1 = document.getElementById(sSpan);
    var flds = oDiv1.getElementsByTagName("INPUT");
    var objRow;
    var cRow;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      if ((flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalUnitAmt') && (flds[x].className != 'inputNoBkgrd'))
      {
        objRow = flds[x].parentElement.parentElement;
        cRow = objRow.getAttribute("rowID");
        if (objRow.id)
        {
          if (cRow == 25 || cRow == 26 || cRow == 27 || cRow == 28)
          {
            flds[x+3].className = "inputDimmed";
            flds[x+3].readOnly = true;
            flds[x+3].disabled = true;
            flds[x+3].onChange = '';
          }
          else
          {
            flds[x+2].className = "inputDimmed";
            flds[x+2].readOnly = true;
            flds[x+2].disabled = true;
            flds[x+2].onChange = '';
          }

          flds[x].className = "inputField";
          flds[x].readOnly = false;
          flds[x].disabled = false;
          flds[x].systemfield = false;
        }
      }

      if (flds[x].name == 'Comments')
      {
        flds[x].className = "InputReadonlyField";
        flds[x].readOnly = true;
        flds[x].disabled = true;
      }
    }
  }

  
  //Put inputs fields in Agreed screen mode
  function setAgreedMode()
  {
  	var oDiv1 = document.getElementById('taxableTotals');
    var flds = oDiv1.getElementsByTagName("INPUT");
    var sFldID;
    var objRow;
    var cRow;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      sFldID = flds[x].id;
      if (flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalHrs' || flds[x].name == 'OriginalUnitAmt')
      {
        flds[x].className = "inputDimmed";
        flds[x].readOnly = true;
        flds[x].systemfield = true;
        flds[x].disabled = true;
      }

      objRow = flds[x].parentElement.parentElement;
      cRow = objRow.getAttribute("rowID");
      if (objRow.id)
      {
        if ((flds[x].name == 'AgreedUnitAmt') && (cRow == 1 || cRow == 2 || cRow == 5 || cRow == 6))
        {
          flds[x].className = "inputDimmed";
          flds[x].readOnly = true;
          flds[x].systemfield = true;
          flds[x].disabled = true;
        }

        if ((flds[x].name == 'AgreedHrs') && (cRow == 8 || cRow == 9))
        {
          flds[x].className = "inputDimmed";
          flds[x].readOnly = true;
          flds[x].systemfield = true;
          flds[x].disabled = true;
        }

        if ((flds[x].name == 'AgreedHrs') && (cRow == 12 || cRow == 13 || cRow == 14 || cRow == 15 || cRow == 16 || cRow == 17))
        {
          flds[x].className = "inputDimmed";
          flds[x].readOnly = true;
          flds[x].systemfield = true;
          flds[x].disabled = true;
        }

      }
    }

  	oDiv1 = document.getElementById('nontaxableTotals');
    flds = oDiv1.getElementsByTagName("INPUT");
    sFldID = "";
    xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      sFldID = flds[x].id;
      if ((flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalHrs' || flds[x].name == 'OriginalUnitAmt') && (flds[x].className != 'inputNoBkgrd'))
      {
        flds[x].className = "inputDimmed";
        flds[x].readOnly = true;
        flds[x].systemfield = true;
        flds[x].disabled = true;
      }
    }
  }


  //Put inputs fields in Agreed screen mode
  function setAgreedModeDetailNonEdit()
  {
  	var oDiv1 = document.getElementById('taxableTotals');
    var flds = oDiv1.getElementsByTagName("INPUT");
    var sFldID;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      sFldID = flds[x].id;
      if (flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalHrs' || flds[x].name == 'OriginalUnitAmt')
      {
        flds[x].className = "inputDimmed";
        flds[x].readOnly = true;
        flds[x].systemfield = true;
        flds[x].disabled = true;
      }
    }

  	oDiv1 = document.getElementById('nontaxableTotals');
    flds = oDiv1.getElementsByTagName("INPUT");
    sFldID = "";
    xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      sFldID = flds[x].id;
      if ((flds[x].name == 'OriginalPct' || flds[x].name == 'OriginalHrs' || flds[x].name == 'OriginalUnitAmt') && (flds[x].className != 'inputNoBkgrd'))
      {
        flds[x].className = "inputDimmed";
        flds[x].readOnly = true;
        flds[x].systemfield = true;
        flds[x].disabled = true;
      }
    }
  }

  //On Change, check if value is negative and if not, change it to negative
  function chkNegative(fld)
  {
    if (fld.value <= 0)
      return;
    else
      return fld.value = (fld.value)*(-1);
  }

  
  //On Tab out of each field, format it and update the caculations on the page
  function updField(fld1)
  {
    var objRow = fld1.parentElement.parentElement;
		objRow.dirty = true;
    SetDirtyFlag(true);
    //gbDirtyFlag = true;
    
    if (fld1.name != 'Comments') //if a Commnent, just set the dirty flag
    {
      var objRowMakeDirty = document.getElementById('tblrow7');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow10');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow11');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow18');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow23');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow24');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow29');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow31');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow35');
  		objRowMakeDirty.dirty = true;
      objRowMakeDirty = document.getElementById('tblrow36');
  		objRowMakeDirty.dirty = true;
      SetDirtyFlag(true);
  
      if (fld1.dbtype == "money")
        FormatCurrencyObj(fld1);
      else if ((fld1.dbtype == "decimal") && (fld1.Scale == "1"))
        FormatHoursObj(fld1);
      else if ((fld1.dbtype == "decimal") && (fld1.Scale == "5"))
        FormatPercentageObj(fld1);

      calcRow(objRow);
      calcTaxableTotal();
      if (gsVANFlag != 1)
        getOriginalTotals();
      getAgreedTotals();

    	//if (typeof(parent.gbDirtyFlag)=='boolean')
    	//	parent.gbDirtyFlag = false;
    }
  }

  
  // Does the calculation across depending on the calculation type for that row
  function calcRow(obj)
  {
    var cType = obj.getAttribute("calcType");
    var cRow = obj.getAttribute("rowID");
    var setDirty = false;

    if (cType == 1)
    {
      if (gsVANFlag != 1)
      {
        var obj1 = document.getElementById('txtOriginalUnitAmt_'+cRow);
        var obj2 = document.getElementById('txtOriginalExtendedAmt_'+cRow);
        obj2.value = formatcurrency(obj1.value);
      }
      if (gsOriginalMode == false || gsUpdOrigAndAgrd == true)
      {
        var obj3 = document.getElementById('txtAgreedUnitAmt_'+cRow);
        var obj4 = document.getElementById('txtAgreedExtendedAmt_'+cRow);
        if (obj4.value != formatcurrency(obj3.value))
        {
          obj4.value = formatcurrency(obj3.value);
          setDirty = true;
        }
      }
    }

    else if (cType == 2)
    {
      if (gsVANFlag != 1)
      {
        var obj1 = document.getElementById('txtOriginalPct_'+cRow);
        var obj2 = document.getElementById('txtOriginalUnitAmt_'+cRow);
        var obj3 = document.getElementById('txtOriginalExtendedAmt_'+cRow);
        obj3.value = formatcurrency(obj2.value * (obj1.value / 100));
      }
      if (gsOriginalMode == false || gsUpdOrigAndAgrd == true)
      {
        var obj4 = document.getElementById('txtAgreedPct_'+cRow);
        var obj5 = document.getElementById('txtAgreedUnitAmt_'+cRow);
        var obj6 = document.getElementById('txtAgreedExtendedAmt_'+cRow);
        if (obj6.value != formatcurrency(obj5.value * (obj4.value / 100)))
        {
          obj6.value = formatcurrency(obj5.value * (obj4.value / 100));
          setDirty = true;
        }
      }
    }

    else if (cType == 3)
    {
      if (gsVANFlag != 1)
      {
        var obj1 = document.getElementById('txtOriginalHrs_'+cRow);
        var obj2 = document.getElementById('txtOriginalUnitAmt_'+cRow);
        var obj3 = document.getElementById('txtOriginalExtendedAmt_'+cRow);
        obj3.value = formatcurrency(obj2.value * obj1.value);
      }
      if (gsOriginalMode == false || gsUpdOrigAndAgrd == true)
      {
        var obj4 = document.getElementById('txtAgreedHrs_'+cRow);
        var obj5 = document.getElementById('txtAgreedUnitAmt_'+cRow);
        var obj6 = document.getElementById('txtAgreedExtendedAmt_'+cRow);
        if (obj6.value != formatcurrency(obj5.value * obj4.value))
        {
          obj6.value = formatcurrency(obj5.value * obj4.value);
          setDirty = true;
        }
      }
    }

    else if (cType == 4)
    {
      if (gsVANFlag != 1)
      {
        var obj1 = document.getElementById('txtOriginalPct_'+cRow);
        var obj2 = document.getElementById('txtOriginalUnitAmt_'+cRow);
        var obj3 = document.getElementById('txtOriginalExtendedAmt_'+cRow);
        obj3.value = formatcurrency(-1*(obj2.value * (obj1.value / 100)));
      }
      if (gsOriginalMode == false || gsUpdOrigAndAgrd == true)
      {
        var obj4 = document.getElementById('txtAgreedPct_'+cRow);
        var obj5 = document.getElementById('txtAgreedUnitAmt_'+cRow);
        var obj6 = document.getElementById('txtAgreedExtendedAmt_'+cRow);
        if (obj6.value != formatcurrency(-1*(obj5.value * (obj4.value / 100))))
        {
          obj6.value = formatcurrency(-1*(obj5.value * (obj4.value / 100)));
          setDirty = true;
        }
      }
    }
    if (setDirty == true)
    {
      obj.dirty = true;
      SetDirtyFlag(true);
    }
  }

  
  //overide the onChange checkbox function in checkbox.js
  function CheckBoxChange(obj) 
  {
    if (obj.parentElement.readOnly == true  || obj.parentElement.disabled == true)
      return;
    var sObj = obj.parentElement.id;
    if (sObj.indexOf('Agreed') != -1  && gsOriginalMode == true)
      return;
    if (sObj.indexOf('Original') != -1  && gsOriginalMode != true)
      return;
    
    var cbValue = obj.value;
    var objImg = obj.firstChild;
    if (cbValue == obj.falseValue)
    {
      objImg.src = "/images/cbhoverck.png";
      obj.value = obj.trueValue;
    }
    else
    {
      objImg.src = "/images/cbhover.png";
      obj.value = obj.falseValue;
    }
    obj.parentElement.lastChild.value = obj.value;
    obj.parentElement.lastChild.Dirty = true;
    
    //this will automatically make the tab, on which this check box lives, as the active one.
    if (document.activeElement == obj)
        obj.focus();
    else
        obj.parentElement.parentElement.focus();
  
//  	if (typeof(gbDirtyFlag)=='boolean')
//  		gbDirtyFlag = true;
//  	if (typeof(parent.gbDirtyFlag)=='boolean')
//  		parent.gbDirtyFlag = true;
  
    var objRow = obj.parentElement.parentElement.parentElement; //sets the TR dirty attribute
    objRow.dirty = true;
    SetDirtyFlag(true);
//  	gbDirtyFlag = true;

    calcTaxableTotal(); //this recalcs the taxable totals
    if (gsVANFlag != 1)
      getOriginalTotals(); //this recalcs the Original totals
    getAgreedTotals(); //this recalcs the Agreed totals
  }


  // Custom Tax Type onSelectChangge combo
  function onTaxTypeChange(selobj,showDirty)//showDirty=1 shows the tax totals selects dirty
  {
    var objRow = selobj.parentElement.parentElement;
    var rowNum = objRow.rowID;
    var origUnitFld = document.getElementById('txtOriginalUnitAmt_'+rowNum);
    var agrdUnitFld = document.getElementById('txtAgreedUnitAmt_'+rowNum);
    var selCode = selobj.options[selobj.selectedIndex].value;
    var setDirty = false;
    
    if (selCode == 'PT')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxablePartsOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxablePartsAgreed.value)
      {
        agrdUnitFld.value = txtTaxablePartsAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'SP')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxableSuppliesOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxableSuppliesAgreed.value)
      {
        agrdUnitFld.value = txtTaxableSuppliesAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'MT')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxablePartsAndMaterialsOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxablePartsAndMaterialsAgreed.value)
      {
        agrdUnitFld.value = txtTaxablePartsAndMaterialsAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'LB')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxableLaborOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxableLaborAgreed.value)
      {
        agrdUnitFld.value = txtTaxableLaborAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'AC')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxableAddChrgOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxableAddChrgAgreed.value)
      {
        agrdUnitFld.value = txtTaxableAddChrgAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'ST')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = txtTaxableSubTotalOriginal.value;
      if (origUnitFld.className == "inputField")
        rstTaxEditField(origUnitFld,false);
      if (agrdUnitFld.value != txtTaxableSubTotalAgreed.value)
      {
        agrdUnitFld.value = txtTaxableSubTotalAgreed.value;
        setDirty = true;
      }
      if (agrdUnitFld.className == "inputField")
        rstTaxEditField(agrdUnitFld,false);
    }

    if (selCode == 'OT')
    {
      if (gsVANFlag != 1 && gsOriginalMode == true)
        rstTaxEditField(origUnitFld,true);
      if (gsOriginalMode == false)
        rstTaxEditField(agrdUnitFld,true);
    }

    if (selCode == '')
    {
      if (gsVANFlag != 1)
        origUnitFld.value = '0.00';
      agrdUnitFld.value = '0.00';
    }

    if ( setDirty == true || (gsPageInitFlag != true) && ((gsOriginalMode == true && origUnitFld.value != 0) || (gsOriginalMode != true && agrdUnitFld.value != 0)) )
    {
      objRow.dirty = true;
      SetDirtyFlag(true);
      //gbDirtyFlag = true;
    }

    if (gsPageInitFlag == false && showDirty != 0)
    {
//    	if (typeof(parent.gbDirtyFlag) == "boolean")
//        parent.gbDirtyFlag = true;
    	selobj.children[0].style.borderColor='#805050';
    }

    calcRow(objRow);
    if (gsVANFlag != 1)
      getOriginalTotals(); //this recalcs the Original totals
    getAgreedTotals(); //this recalcs the Agreed totals
  }


  //Resets the editable field for taxes rows
  function rstTaxEditField(obj,chngflag)
  {
    if (chngflag == false)
    {
      obj.className = "inputNoBkgrd";
      obj.readOnly = true;
      obj.systemfield = true;
      obj.tabIndex = '-1';
    }

    if (chngflag == true)
    {
      obj.className = "inputField";
      obj.readOnly = false;
      obj.systemfield = false;
      obj.style.textAlign = 'right';
      obj.tabIndex = '0';
    }
  }


  //Calculate Taxable Totals
  function calcTaxableTotal()
  {
  	var retArray;
    var sValues;
    var originalTotalVal;
    var agreedTotalVal;
    var objRow;
    var obj;

  	retArray = addTaxAmounts('taxableTotals1');
    sValues = retArray.split("|");
    originalTotalVal = sValues[0];
    agreedTotalVal = sValues[1];
    if (gsVANFlag != 1)
      txtTaxablePartsOriginal.value = formatcurrency(originalTotalVal);
    obj = document.getElementById("txtTaxablePartsAgreed");
    if (obj.value != formatcurrency(agreedTotalVal))
    {
      txtTaxablePartsAgreed.value = formatcurrency(agreedTotalVal);
      objRow = obj.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

  	retArray = addTaxAmounts('taxableTotals2');
    sValues = retArray.split("|");
    originalTotalVal = sValues[0];
    agreedTotalVal = sValues[1];

    if (gsVANFlag != 1)
      txtTaxableSuppliesOriginal.value = formatcurrency(originalTotalVal);
    obj = document.getElementById("txtTaxableSuppliesAgreed");
    if (obj.value != formatcurrency(agreedTotalVal))
    {
      txtTaxableSuppliesAgreed.value = formatcurrency(agreedTotalVal);
      objRow = obj.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    txtTaxablePartsAndMaterialsOriginal.value = formatcurrency(Number(txtTaxablePartsOriginal.value) + Number(txtTaxableSuppliesOriginal.value));
    txtTaxablePartsAndMaterialsAgreed.value = formatcurrency(Number(txtTaxablePartsAgreed.value) + Number(txtTaxableSuppliesAgreed.value));

  	retArray = addTaxAmounts('taxableTotals3');
    sValues = retArray.split("|");
    originalTotalVal = sValues[0];
    agreedTotalVal = sValues[1];
    if (gsVANFlag != 1)
      txtTaxableLaborOriginal.value = formatcurrency(originalTotalVal);
    obj = document.getElementById("txtTaxableLaborAgreed");
    if (obj.value != formatcurrency(agreedTotalVal))
    {
      txtTaxableLaborAgreed.value = formatcurrency(agreedTotalVal);
      objRow = obj.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

  	retArray = addTaxAmounts('taxableTotals4');
    sValues = retArray.split("|");
    originalTotalVal = sValues[0];
    agreedTotalVal = sValues[1];
    if (gsVANFlag != 1)
      txtTaxableAddChrgOriginal.value = formatcurrency(originalTotalVal);
    obj = document.getElementById("txtTaxableAddChrgAgreed");
    if (obj.value != formatcurrency(agreedTotalVal))
    {
      txtTaxableAddChrgAgreed.value = formatcurrency(agreedTotalVal);
      objRow = obj.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    if (gsVANFlag != 1)
      txtTaxableSubTotalOriginal.value = formatcurrency(Number(txtTaxablePartsAndMaterialsOriginal.value) + Number(txtTaxableLaborOriginal.value) + Number(txtTaxableAddChrgOriginal.value));
    obj = document.getElementById("txtTaxableSubTotalAgreed");
    if ( obj.value != formatcurrency(Number(txtTaxablePartsAndMaterialsAgreed.value) + Number(txtTaxableLaborAgreed.value) + Number(txtTaxableAddChrgAgreed.value)) )
    {
      txtTaxableSubTotalAgreed.value = formatcurrency(Number(txtTaxablePartsAndMaterialsAgreed.value) + Number(txtTaxableLaborAgreed.value) + Number(txtTaxableAddChrgAgreed.value));
      objRow = obj.parentElement.parentElement;
   		objRow.dirty = true;
      SetDirtyFlag(true);
    }

    onTaxTypeChange(selTaxTypeCD_25,0);
    onTaxTypeChange(selTaxTypeCD_26,0);
    onTaxTypeChange(selTaxTypeCD_27,0);
    onTaxTypeChange(selTaxTypeCD_28,0);
  }


  //Generic function to add taxable amounts
  function addTaxAmounts(sSpan)
  {
  	originalTotalVal = 0;
  	agreedTotalVal = 0;
  	oDiv = document.getElementById(sSpan);
    trs = oDiv.getElementsByTagName("TR");
    var oRow;
   	var elms;
    var zLength;
    var obj;
    var xLength = trs.length;
    for (var x=0; x < xLength; x++)
    {
      oRow = trs[x].getAttribute("rowID");
      if (oRow)
      {
      	elms = trs[x].getElementsByTagName("INPUT");
        zLength = elms.length;
        for (var z=0; z < zLength; z++)
        {
          obj = elms[z];
          if (gsVANFlag != 1)
          {
            if ((obj.name == 'OriginalExtendedAmt') && (elms[z+1].value == 1))
              originalTotalVal = originalTotalVal + Number(obj.value);
          }
          if ((obj.name == 'AgreedExtendedAmt') && (elms[z+1].value == 1))
            agreedTotalVal = agreedTotalVal + Number(obj.value);
        }
      }
    }
    return taxTotals = originalTotalVal+ "|" +agreedTotalVal;
  }

  
  //format each numeric input field
  function formatFields()
  {
    var flds= content13.getElementsByTagName("input");
    var obj;
    var xLength = flds.length;
    for (var x=0; x < xLength; x++)
    {
      obj = flds[x];
      if ((flds[x].dbtype != "varchar") && (!isNaN(flds[x].value)))
      {
        if (flds[x].dbtype == "money")
          flds[x].value = formatcurrency(flds[x].value);

        else if ((flds[x].dbtype == "decimal") && (flds[x].Scale == "1"))
          flds[x].value = formathours(flds[x].value);

        else if ((flds[x].dbtype == "decimal") && (flds[x].Scale == "5"))
          flds[x].value = formatpercentage(flds[x].value);
      }
    }
  }

  
  //currency formating
  function FormatCurrencyObj(obj)
  {
    X = parseFloat(obj.value);
    if (isNaN(X) || X == '.')
      return obj.value = '0.00'
    else
    {
      var currNum = formatcurrency(X);
      obj.value = currNum;
    }
  }
  
  function formatcurrency(X)
  {
    var roundNum = Math.round(X*100)/100; //round to 2 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.00';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 3);
    
    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  
  //hours formating
  function FormatHoursObj(obj)
  {
    X = obj.value;
    if (isNaN(X) || X == '.')
      return obj.value = '0.0'
    else
    {
      var hrsNum = formathours(X);
      obj.value = hrsNum;
    }
  }
  
  function formathours(X)
  {
    var roundNum = Math.round(X*10)/10; //round to 1 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 2);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  
  //percentage formating
  function FormatPercentageObj(obj)
  {
    X = obj.value;
    if (isNaN(X) || X == '.')
      return obj.value = '0.000'
    else
    {
      var pctNum = formatpercentage(X);
      obj.value = pctNum;
    }
  }
  
  function formatpercentage(X)
  {
    var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.000';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '00';
    else if (strValue.indexOf('.') == strValue.length - 3)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 6);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  
  // Summary ADS function
  function summary_ADS_Pressed()
  {
    //if ((gsOriginalMode == true) && (txtOriginalExtendedAmt_30.value != 0 && txtOriginalExtendedAmt_24.value != 0))
    //{
    //  ClientWarning("Please check the Original values entered. Your Subtotal is $"+txtOriginalExtendedAmt_24.value+" and your Contract Price is $"+txtOriginalExtendedAmt_30.value+".<br>" +
    //                "If entering an estimate with a Contract Price total, the Subtotal amount must equal zero.");
    //  return;
    //}

    //if ((gsOriginalMode == false) && (txtAgreedExtendedAmt_30.value != 0 && txtAgreedExtendedAmt_24.value != 0))
    //{
    //  ClientWarning("Please check the Agreed values entered. Your Subtotal is $"+txtAgreedExtendedAmt_24.value+" and your Contract Price is $"+txtAgreedExtendedAmt_30.value+".<br>" +
    //                "An estimate with a Contract Price total must have the Subtotal equal to zero.");
    //  return;
    //}

    
    if (gsOriginalMode == true) //if in original mode copy original values to agreed
    {
      var sConfirm = YesNoMessage("Overwrite Agreed Values?",
                                  "Press YES to overwrite the Agreed values with the Original values and save.\n" +
                                  "Press NO to save the Original values without overwriting the Agreed values.");
      if (sConfirm == "Yes")
      {
      	copyFldsWithCkBx('taxableTotals1'); //overwrite fields in this span
      	copyFldsWithCkBx('taxableTotals2'); //overwrite fields in this span
      	copyFldsWithCkBx('taxableTotals3'); //overwrite fields in this span
      	copyFldsWithCkBx('taxableTotals4'); //overwrite fields in this span
      	copyFldsWoCkBx('nontaxableTotals'); //overwrite fields in this span
        gsUpdOrigAndAgrd = false;
      }
    }

    if (gsVANFlag != 1)
      getOriginalTotals();
    getAgreedTotals();
    formatFields();

		var sRequest;
	  var sAction;
    var sProc;
    
    var sName = "content13";
    var oDiv = document.getElementById(sName)
    
	  sAction = "Update";
    sProc = "uspEstimateSummaryUpdDetail";
    var tblObj = document.getElementById("tbl"+oDiv.name);

    // check to see if the Additional Charges and Taxes rows drop-downs are properly filled
    if (gsOriginalMode != true)
    {
      var sACPosition = "";
      var sTXPosition = "";
      var cRow;
      var objRow;
      var oDivRow;
    	var elms;
      var sAcTypeIdDirty = false;
      var sAcIdDirty = false;
      var sTxTypeIdDirty = false;
      var sTxIdDirty = false;
      var idxLength = tblObj.rows.length;
      for (var idx=0; idx < idxLength; idx++)
    	{
        cRow = tblObj.rows[idx].getAttribute("rowID");
        // check the Additional Charges rows
    		if ((cRow == 19 || cRow == 20 || cRow == 21 || cRow == 22) && (tblObj.rows[idx].dirty == true))
        {
          objRow = tblObj.rows[idx];
          oDivRow = document.getElementById(objRow.id);
        	elms = oDivRow.getElementsByTagName("INPUT");
          sAcTypeIdDirty = false;
          sAcIdDirty = false;
          var iLength = elms.length;
        	for(var i = 0; i < iLength; i++)
        	{
        		if (elms[i].name.indexOf('EstimateSummaryTypeID') != -1 && elms[i].value == '')
              sAcTypeIdDirty = true;
        		if (elms[i].name.indexOf('EstimateSummaryID') != -1 && elms[i].value == 0)
              sAcIdDirty = true;
          }

          if (sAcTypeIdDirty == true && sAcIdDirty == true)
          {
            if (sACPosition != '') sACPosition += ", ";
            if (cRow == 19) sACPosition += "first ";
            if (cRow == 20) sACPosition += "second ";
            if (cRow == 21) sACPosition += "third ";
            if (cRow == 22) sACPosition += "fourth ";
          }
        }

        // check the Taxes rows
    		if ((cRow == 25 || cRow == 26 || cRow == 27 || cRow == 28) && (tblObj.rows[idx].dirty == true))
        {
          objRow = tblObj.rows[idx];
          oDivRow = document.getElementById(objRow.id);
        	elms = oDivRow.getElementsByTagName("INPUT");
          sTxTypeIdDirty = false;
          sTxIdDirty = false;
          var iLength = elms.length;
        	for(var i = 0; i < iLength; i++)
        	{
        		if (elms[i].name.indexOf('EstimateSummaryTypeID') != -1 && elms[i].value == '')
              sTxTypeIdDirty = true;
        		if (elms[i].name.indexOf('TaxTypeCD') != -1 && elms[i].value == '')
              sTxIdDirty = true;
          }

          if (sTxTypeIdDirty == true || sTxIdDirty == true)
          {
            if (sTXPosition != '') sTXPosition += ", ";
            if (cRow == 25) sTXPosition += "first ";
            if (cRow == 26) sTXPosition += "second ";
            if (cRow == 27) sTXPosition += "third ";
            if (cRow == 28) sTXPosition += "fourth ";
          }
        }
      }
      
      // Show Additional Charges message
      if (sACPosition != '')
      {
        var sACConfirm = YesNoMessage("Changes to the " + sACPosition + " row(s) of Additional Charges.",
                                    "Press YES to specify a Charge Type before saving.\n" +
                                    "Press NO to ignore the changes and continue without saving.");
        if (sACConfirm == "Yes")
        {
          inADS_Pressed = false;
          return;
        }
      }

      // Show Taxes message
      if (sTXPosition != '')
      {
        var sTXConfirm = YesNoMessage("Changes to the " + sTXPosition + " row(s) of Taxes.",
                                    "Press YES to correct the Taxes before saving.\n" +
                                    "Press NO to ignore the changes and continue without saving.");
        if (sTXConfirm == "Yes")
        {
          inADS_Pressed = false;
          return;
        }
      }
    }
    
    var objRow;
    var cRow;
    var sRequest;
    var idxLength = tblObj.rows.length;
  	for (var idx=0; idx < idxLength; idx++)
  	{
      if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
      {
        objRow = tblObj.rows[idx];
        cRow = objRow.getAttribute("rowID");
        if (objRow.id)
        {
          sRequest = GetRequestString(objRow.id);

          sRequest += "DocumentID=" + gsDocumentID + "&";
          sRequest += "UserID=" + gsUserID + "&";
          sRequest += "EstimateSummaryType=&"; //optional param that the proc requires
          sRequest += "EstimateSummaryTypeCD=&"; //optional param that the proc requires
          sRequest += "UserName="; //optional param that the proc requires
          
          if (cRow != 19 || cRow != 20 || cRow != 21 || cRow != 22)
          {
            sRequest = sRequest.replace('EstimateSummaryTypeID_'+cRow, 'EstimateSummaryTypeID');
          }
  
          if (cRow != 24 || cRow != 31 || cRow != 36)
          {
            sRequest = sRequest.replace('OriginalTaxableFlag'+cRow, 'OriginalTaxableFlag');
            sRequest = sRequest.replace('AgreedTaxableFlag'+cRow, 'AgreedTaxableFlag');
          }
  
          if (cRow != 25 || cRow != 26 || cRow != 27 || cRow != 28)
          {
            sRequest = sRequest.replace('TaxTypeCD_'+cRow, 'TaxTypeCD');
          }

          // // alert("Summary Update sRequest = "+ sRequest);

          // var sProc = oDiv.getAttribute(sAction);

    			var coObj = RSExecute("/rs/RSADSAction.asp", "RadExecute", sProc, sRequest );
      		retArray = ValidateRS( coObj );
  
      		//setTimeout(ShowSB80,2);
      		if (retArray[1] == 1)
      		{
            //update the last Updated date...
            var objXML = new ActiveXObject("Microsoft.XMLDOM");
            objXML.loadXML(retArray[0]);
                 
            var rootNode = objXML.documentElement.selectSingleNode("/Root");
            var EstimateSummaryNode = rootNode.selectSingleNode("@SysLastUpdatedDate");
            if (EstimateSummaryNode)
            {
              var flds= objRow.getElementsByTagName("input");
              var obj;
              var xLength = flds.length;
              for (var x=0; x < xLength;x++)
              {
                obj = flds[x];
                if (obj.name == "SysLastUpdatedDate")
                  obj.value = EstimateSummaryNode.nodeValue;
              }
            }
  
      			//if (typeof(parent.gbDirtyFlag)=='boolean')
      			//	parent.gbDirtyFlag = false;
      
            if (cRow != 24 || cRow != 31 || cRow != 36)
            {
        			resetControlBorders(objRow);
            }
      		}
      		else
          {
            //if there is a problem saving...
            if (cRow < 8)
              var subMsg = "Parts Costs";
            if (cRow > 7 && cRow < 11)
              var subMsg = "Supply Costs";
            if (cRow == 11)
              var subMsg = "Parts and Supplies Total";
            if (cRow > 11 && cRow < 19)
              var subMsg = "Labor Costs";
            if (cRow > 18 && cRow < 24)
              var subMsg = "Additional Charges";
            if (cRow == 24)
              var subMsg = "Subtotal";
            if (cRow > 24 && cRow < 30)
              var subMsg = "Taxes";
            if (cRow == 30)
              var subMsg = "Contract Price";
            if (cRow == 31)
              var subMsg = "Total Cost of Repair";
            if ((cRow > 31 && cRow < 36) || (cRow == 38))
              var subMsg = "Adjustments";
            if (cRow == 36)
              var subMsg = "Net Total";
            
            ClientWarning("Error saving " + subMsg + " row(s) in Estimate Totals.");
            return;
          }
          //setTimeout(ShowSB100,300);
        }
      }

    //clear dirty flags upon successful saves.
    SetDirtyFlag(false);
    //gbDirtyFlag = false;
    gsSummaryDirtyFlag = false;
    }

 		if (gsOriginalMode == true)
    {
      var showTotals = 1; //Flag to go to totals tab onLoad
      //alert("Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" +gsDocumentID+ "&showTotals=" +showTotals);
      //parent.frames["ifrmEstimateSummary"].frameElement.src = "Estimates.asp?DocumentID=" +gsDocumentID+ "&showTotals=" +showTotals;
      //location.href = "Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" +gsDocumentID+ "&showTotals=" +showTotals;
    }
  }

  // Copies Original values to Agreed
  function dupFields()
  {
  	copyFldsWithCkBx('taxableTotals1'); //overwrite fields in this span
  	copyFldsWithCkBx('taxableTotals2'); //overwrite fields in this span
  	copyFldsWithCkBx('taxableTotals3'); //overwrite fields in this span
  	copyFldsWithCkBx('taxableTotals4'); //overwrite fields in this span
  	copyFldsWoCkBx('nontaxableTotals'); //overwrite fields in this span
    calcTaxableTotal();
    getAgreedTotals();
  }


  // Overwrites Agreed values for sections with checkboxes
  function copyFldsWithCkBx(sSpan)
  {
    var oRow;
    var cType;
    var flds;
    var oSpan = document.getElementById(sSpan);
    var trs = oSpan.getElementsByTagName("TR");
    var zLength = trs.length;
    for (var z=0; z < zLength; z++)
    {
      oRow = trs[z].getAttribute("rowID");
      cType = trs[z].getAttribute("calcType");
      if (oRow)
      {
        var sTypeId = false;
        var sId = false;
        flds= trs[z].getElementsByTagName("INPUT");
        var xLength = flds.length;
        for (var x=0; x < xLength; x++)
        {
          if (cType == 1)
          {
            if (flds[x].name == 'OriginalUnitAmt')
              flds[x+3].value = flds[x].value;
            
            if (flds[x].name == 'OriginalExtendedAmt')
              flds[x+3].value = flds[x].value;
    
            if (flds[x].name.indexOf('OriginalTaxableFlag') != -1)
            {
              flds[x+3].value = flds[x].value;
              var objImg = document.getElementById("AgreedTaxableFlag"+oRow+"Img");
              if (objImg)
              {
                if (flds[x+3].value == 1)
                  objImg.src = "/images/cbcheck.png";
                else
                  objImg.src = "/images/cbuncheck.png";
              }
            }
          }
          else if (cType == 2 || cType == 4)
          {
            if (flds[x].name == 'OriginalPct')
              flds[x+4].value = flds[x].value;
            
            if (flds[x].name == 'OriginalUnitAmt')
              flds[x+4].value = flds[x].value;
            
            if (flds[x].name == 'OriginalExtendedAmt')
              flds[x+4].value = flds[x].value;
    
            if (flds[x].name.indexOf('OriginalTaxableFlag') != -1)
            {
              flds[x+4].value = flds[x].value;
              var objImg = document.getElementById("AgreedTaxableFlag"+oRow+"Img");
              if (objImg)
              {
                if (flds[x+4].value == 1)
                  objImg.src = "/images/cbcheck.png";
                else
                  objImg.src = "/images/cbuncheck.png";
              }
            }
          }
          else if (cType == 3)
          {
            if (flds[x].name == 'OriginalHrs')
              flds[x+4].value = flds[x].value;
            
            if (flds[x].name == 'OriginalUnitAmt')
              flds[x+4].value = flds[x].value;
            
            if (flds[x].name == 'OriginalExtendedAmt')
              flds[x+4].value = flds[x].value;
    
            if (flds[x].name.indexOf('OriginalTaxableFlag') != -1)
            {
              flds[x+4].value = flds[x].value;
              var objImg = document.getElementById("AgreedTaxableFlag"+oRow+"Img");
              if (objImg)
              {
                if (flds[x+4].value == 1)
                  objImg.src = "/images/cbcheck.png";
                else
                  objImg.src = "/images/cbuncheck.png";
              }
            }
          }

          if (oRow == 19 || oRow == 20 || oRow == 21 || oRow == 22)
          {
            if (flds[x].name.indexOf('EstimateSummaryTypeID') != -1 && flds[x].value == '')
              sTypeId = true;
            if (flds[x].name.indexOf('EstimateSummaryID') != -1 && flds[x].value == 0)
              sId = true;
           }
        }
        
        if (sTypeId != true && sId != true)
        {
          trs[z].dirty = true;
          //gbDirtyFlag = true;
        }
        calcRow(trs[z]);
      }
    }
  }


  // Overwrites Agreed values for sections without checkboxes
  function copyFldsWoCkBx(sSpan)
  {
    var oRow;
    var cType;
    var flds;
    var sTypeId = false;
    var sId = false;
    var oSpan = document.getElementById(sSpan);
    var trs = oSpan.getElementsByTagName("TR");
    var zLength = trs.length;
    for (var z=0; z < zLength; z++)
    {
      oRow = trs[z].getAttribute("rowID");
      cType = trs[z].getAttribute("calcType");
      if (oRow)
      {
        sTypeId = false;
        sId = false;
        flds= trs[z].getElementsByTagName("INPUT");
        var xLength = trs.length;
        for (var x=0; x < flds.length;x++)
        {
          if (cType == 1)
          {
            if (flds[x].name == 'OriginalUnitAmt')
              flds[x+2].value = flds[x].value;
            
            if (flds[x].name == 'OriginalExtendedAmt')
              flds[x+2].value = flds[x].value;
          }
          else if (cType == 2)
          {
            if (flds[x].name == 'OriginalPct')
              flds[x+3].value = flds[x].value;
            
            if (oRow == 25 || oRow == 26 || oRow == 27 || oRow == 28)
            {
              if (flds[x].name == 'OriginalUnitAmt')
                flds[x+3].value = flds[x].value;
            }
          }

          if (oRow == 25 || oRow == 26 || oRow == 27 || oRow == 28)
          {
            if (flds[x].name.indexOf('EstimateSummaryTypeID') != -1 && flds[x].value == '')
              sTypeId = true;
            if (flds[x].name.indexOf('EstimateSummaryID') != -1 && flds[x].value == 0)
              sId = true;
           }
        }
        if (sTypeId != true && sId != true)
        {
          trs[z].dirty = true;
          //gbDirtyFlag = true;
        }
        calcRow(trs[z]);
      }
    }
  }

  
  // Local function to gather all elements in the page and build Request String
  function GetRequestString(sName)
  {
  	var oDiv = document.getElementById(sName)
  	var elms = oDiv.getElementsByTagName("INPUT");
    var cRow = oDiv.getAttribute("rowID");
  	var sRequest = "";
  	var strName;
  	var sFld;

    sRequest += "Row=" + cRow + "&";
    var iLength = elms.length;
  	for(var i = 0; i < iLength; i++)
  	{
  		if (elms[i].type != "button" && elms[i].name != "")
      {
        strName = elms[i].name;
        if ((cRow != 24 || cRow != 31 || cRow != 36) && (strName.indexOf('TaxableFlag') != -1))
        {
          if (strName.indexOf('OriginalTaxableFlag') != -1)
        		sRequest += "OriginalTaxableFlag" + "=" + escape(elms[i].value) + "&";

          else if (strName.indexOf('AgreedTaxableFlag') != -1)
        		sRequest += "AgreedTaxableFlag" + "=" + escape(elms[i].value) + "&";
        }
        else
         sRequest += elms[i].name + "=" + escape(elms[i].value) + "&";
    	}
  	}
  
  	elms = oDiv.getElementsByTagName("TEXTAREA");
    var iLength = elms.length;
  	for(var i = 0; i < iLength; i++)
  	{
  		if (elms[i].name != "")
  			sRequest += elms[i].name + "=" + escape(elms[i].value) + "&";
  	}
  
  	var sContextID = oDiv.getAttribute("ContextID");
  	var sContextName = oDiv.getAttribute("ContextName");
  	if (sContextName)
  		sRequest += sContextName + "ID=" + sContextID + "&";
  
  	var sLastUpdatedDate = "";
  	if (oDiv.getAttribute("LastUpdatedDate"))
  		sLastUpdatedDate = oDiv.getAttribute("LastUpdatedDate");
  
  	if (sContextName)
  		sRequest += sContextName + "SysLastUpdatedDate=" + sLastUpdatedDate + "&";
  
    return sRequest;
  }


  // Popup to expand the Comments fields
  function showComments(fld)
  {
    if (fld.value == '') return;
    var oPopupBody = oPopup.document.body;
    oPopupBody.style.backgroundColor = "#FFFFFF";
    oPopupBody.style.background = "url(../images/textarea_bkgd.png) repeat-x";
    oPopupBody.style.border = "1px solid #999999";
    oPopupBody.innerHTML = "<div style='font-family:Verdana,Arial; font-size:10px; padding:2px;'>"+ fld.value +"</div>";
    oPopup.show(8, 12, 184, 114, fld);
  }
