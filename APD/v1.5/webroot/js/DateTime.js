/*---------------------------------------------------------*
 * DateTime.js
 *
 * JavaScript utilities for formatting date and time
 * based upon passed format strings.  To be used in
 * parsing user input for constriction to standards.
 *
 *---------------------------------------------------------*/

// Creates a date/time string compatible with sql:dateTime from the current date and time.
function formatSQLNow()
{
    var varDate = new Date();
    return formatDate( varDate, "yyyy-mm-dd" ) + "T" + formatTime( varDate, "HH:mm:ss" );
}

// Creates a date/time string compatible with sql:dateTime.
function formatSQLDateTime( varDate )
{
    return formatDate( varDate, "yyyy-mm-dd" ) + "T" + formatTime( varDate, "HH:mm:ss" );
}

// Creates a JavaScript Date object from a sql:dateTime string.
// i.e. "2002-01-29T11:31:06.583"
function extractSQLDateTime( strDate )
{
   var strConv =
        strDate.substr( 5, 2 ) + "-"
      + strDate.substr( 8, 2 ) + "-"
      + strDate.substr( 0, 4 ) + " "
      + strDate.substr( 11, 8 );
   return strConv;
}

function formatTime(varDate, strFormat)
{
    var time = varDate;  // BUGBUG need conversion from varDate

    var result = "";
    for (var i=0; i<strFormat.length; i++)
    {
        switch (strFormat.charAt(i))
        {
            case "H":
                if (strFormat.charAt(i+1) == "H") //HH
                {
                    i++;
                    result += (time.getHours()<10 ? "0" : "") + (time.getHours());
                }
                else // H
                    result += time.getHours();
                break;
            case "h":
                var hours = time.getHours() % 12;
                if (hours == 0) hours = 12;
                if (strFormat.charAt(i+1) == "h")  // hh
                {
                    i++;
                    result += (hours<10 ? "0" : "") + hours;
                }
                else // h
                    result += hours;
                break;
            case "m":
                if (strFormat.charAt(i+1) == "m") // mm
                {
                    i++;
                    result += (time.getMinutes()<10 ? "0" : "") + (time.getMinutes());
                }
                else // m
                    result += time.getMinutes();
                break;
            case "s":
                if (strFormat.charAt(i+1) == "s")  // ss
                {
                    i++;
                    result += (time.getSeconds()<10 ? "0" : "") + (time.getSeconds());
                }
                else // s
                    result += time.getSeconds();
                break;
            case "t":
                if (strFormat.charAt(i+1) == "t")  // tt
                {
                    i++;
                    result += time.getHours() < 12 ? "AM" : "PM";
                }
                else // t
                    result += time.getHours() < 12 ? "A" : "P";
                break;
            case "'":
                i++;
                while (strFormat.charAt(i) != "'" && i < strFormat.length)
                    result += strFormat.charAt(i++);
                // Handle quoted quotes ('') in the literal
                if (strFormat.charAt(i+1) == "'")
                    result += "'";
                break;
            default:
                result += strFormat.charAt(i);
        }
    }

    return result;
}

function formatDate(varDate, strFormat, varDestLocale)
{
    var date = varDate;  // BUGBUG need conversion from varDate

    var result = "";
    for (var i=0; i<strFormat.length; i++)
    {
        switch (strFormat.charAt(i))
        {
            case "d":
                if (strFormat.charAt(i+1) == "d")
                {
                    i++;
                    if (strFormat.charAt(i+1) == "d")
                    {
                        i++;
                        if (strFormat.charAt(i+1) == "d") //dddd
                        {
                            i++;
                            var longMonthName = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
                            result += longMonthName[date.getDay()];
                        }
                        else //ddd
                        {
                            var shortMonthName = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
                            result += shortMonthName[date.getDay()];
                        }
                    }
                    else //dd
                        result += (date.getDate()<10 ? "0" : "") + (date.getDate());
                    }
                else //dd
                    result += date.getDate();
                break;
            case "m":
                if (strFormat.charAt(i+1) == "m")
                {
                    i++;
                    if (strFormat.charAt(i+1) == "m")
                    {
                        i++;
                        if (strFormat.charAt(i+1) == "m") //mmmm
                        {
                            i++;
                            var longMonthName = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
                            result += longMonthName[date.getMonth()];
                        }
                        else    //mmm
                        {
                            var shortMonthName = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                            result += shortMonthName[date.getMonth()];
                        }
                    }
                    else //mm
                        result += (date.getMonth()<9 ? "0" : "") + (date.getMonth() + 1);
                }
                else //m
                    result += date.getMonth() + 1;
                break;
            case "y":
                if (strFormat.charAt(i+1) == "y")
                {
                    i++;
                    if (strFormat.charAt(i+1) == "y" && strFormat.charAt(i+2) == "y") // yyyy
                    {
                        i += 2;
                        result += date.getFullYear();
                    }
                    else //yy
                        result += (date.getFullYear() + "").substr(2,2);
                }
                else //y
                    result += date.getFullYear() % 100;
                break;
            case "'":
                i++;
                while (strFormat.charAt(i) != "'" && i < strFormat.length)
                    result += strFormat.charAt(i++);
                // Handle quoted quotes ('') in the literal
                if (strFormat.charAt(i+1) == "'")
                    result += "'";
                break;
            default:
                result += strFormat.charAt(i);
        }
    }

    return result;
}

