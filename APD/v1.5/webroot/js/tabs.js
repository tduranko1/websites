/*
	APD Select
	Tab In Tab code
	User Interface Control for Tabs

	The code can handle N number of Tabs, but the CSS (tabs.css) needs
	to reflect the number of styling. ex. tab1, tab2 .... tabN+1	
*/
var ua = navigator.userAgent;
var WIN = (navigator.platform=="Win32") ? true : false;
var getID = document.getElementById ? true : false;
var IE5_0, IE5,IE6,IE5_5
var browserName = navigator.appName.substring(0,navigator.appName.indexOf(" ") > 4 ? navigator.appName.indexOf(" ") : navigator.appName.length);

IE6 = (ua.indexOf("MSIE 6") > 0) ? true : false;
IE5_0 = (ua.indexOf("MSIE 5.0") > 0) ? true : false;
IE5_5 = (ua.indexOf("MSIE 5.5") > 0) ? true : false;
IE5 = (ua.indexOf("MSIE 5") > 0  || IE6 || IE5_5) ? true : false;


var tabs, tabArray;
var tabsIntabs;
var bTabInTab = false;
var bgActiveTab = "";
var bLastActiveTabInTab = 0;

function tabInit(flagTabInTab, bgColorContent1) 
{
	bTabInTab = flagTabInTab;
	if (bgColorContent1 != null) bgActiveTab = bgColorContent1;
	
	tabsIntabs = objectGetElementsByClass("DIV","apdtabs");
	
	if (tabsIntabs == null || tabsIntabs.length == 0) return;
	
  var xLength = tabsIntabs.length;
	for (var x = 0;x < xLength; x++)
	{
		tabs= document.all['tabs'+(x+1)];//document.getElementById('tabs'+(x+1));
		tabs.getElementsByClass= objectGetElementsByClass;
		tabArray= tabs.getElementsByClass("SPAN", "tab"+(x+1));
		
		if (tabArray == null || tabArray.length == 0) return;
	
		tabArray.isImg= tabArray[0].tagName.toLowerCase() == "img" ? true : false;
		IE5_0 = (IE5 && !IE5_5 && !IE6) ? true : false;

		var iLength = tabArray.length;
    for(var i = 0; i < iLength; i++)
		{
			tabArray[i].content=document.all["content"+(x+1)+(i+1)] ;//document.getElementById("content"+(x+1)+(i+1));
		
			// deal with any img tab.
			if(tabArray[i].tagName.toLowerCase() == "img")
			{
				tabArray[i].setAttribute("normalsrc", tabArray[i].src);
				tabArray[i].hover= new Image();
				tabArray[i].hover.src= tabArray[i].getAttribute("hoversrc");
				tabArray[i].active= new Image();
				tabArray[i].active.src= tabArray[i].getAttribute("activesrc");
			}
		
			tabArray[i].onmouseover= hoverTab;
			tabArray[i].onmouseout= hoverOff;
			tabArray[i].onmousedown= depressTab;
			tabArray[i].depressTab= depressTab;
			tabArray[i].tabindex = x + 1;
			tabArray[i].tabnum = i + 1;
		}
		tabs.onchange= new Function();
		tabs.onBeforechange= new Function("return true;");
	
		// Switch to a tab in the search string.
//		var q = String(window.location.search);
//		if(q && q.indexOf("tab"+(x+1)+"=") > 0)
//			switchTabs(q.substring(q.indexOf("tab"+(x+1)+"=")+4,q.indexOf("tab"+(x+1)+"=")+8))

		if(IE5_5 && !IE6 && !tabArray.isImg) tabs.style.top=(tabs.offsetTop+2)+"px";	
		if(IE5 && !IE5_5 && !IE6 && !tabArray.isImg)
		{
			tabs.style.top= (tabs.offsetTop-1)+"px";
		}
		
		tabsIntabs[x].tabs = tabs;
		tabsIntabs[x].tabArray = tabArray;
		tabsIntabs[x].activeTab = null;
		tabsIntabs[x].relatedTab = null;
		
		// depress tab 1.
		if (flagTabInTab == false)
			tabArray[0].depressTab();
		else if (x ==0) tabArray[0].depressTab();
			
	}
}	
	
function switchTabs(tab,e,hash)
{
	try
	{
		document.all[tab].depressTab(e); //document.getElementById(tab).depressTab(e);
	}
	catch(exception){}
	
	if(hash && document.getElementById(hash.substring(1)))
		scrollToElement(hash);
	else window.scrollTo(0,0);
}

function scrollToElement(hash)
{
	try
	{
		if(IE5) document.getElementById(hash.substring(1)).scrollIntoView();
	}
	catch(ex){}
}

function hoverTab() 
{
	if(tabsIntabs[this.tabindex-1].activeTab==this) return;
	this.className= "tabHover"+this.tabindex;
	this.src= this.getAttribute("hoversrc");
}

function hoverOff() 
{
	if(tabsIntabs[this.tabindex-1].activeTab==this) return;
	this.className= "tab"+this.tabindex;
	this.src= this.getAttribute("normalsrc");
}

function depressTab(e) 
{
	// check if need to not change tabs, validation of fields
	if (tabsIntabs[bLastActiveTabInTab].tabs.onBeforechange(tabsIntabs[bLastActiveTabInTab].activeTab, this) == false) return;

	var oActiveTab = tabsIntabs[this.tabindex-1].activeTab;
	var oRelatedTab = tabsIntabs[this.tabindex-1].relatedTab;
	
	if (oActiveTab == this) return;
	bLastActiveTabInTab = this.tabindex-1;
	
	try
	{
		if (bTabInTab == true && this.tabindex == 1)
		{
            var xLength = tabsIntabs.length;
			for (var x = 1;x < xLength; x++)
			{
				var tabA = tabsIntabs[x].tabArray;
                var iLength = tabA.length;
				for(var i = 0;i < iLength; i++)
				{
					resetTab(tabA[i]);
				}
			}
		}
	}
	catch(ex){}

	tabsIntabs[this.tabindex-1].relatedTab= oActiveTab;
	this.className= "tab"+this.tabindex;
	this.className= "tabActive"+this.tabindex;

	if (bgActiveTab != "")
	{
		this.style.backgroundColor = bgActiveTab;
		this.content.style.backgroundColor = bgActiveTab;
		}

	this.src= this.getAttribute("activesrc");

	if(oActiveTab) resetTab(tabsIntabs[this.tabindex-1].activeTab);

	tabsIntabs[this.tabindex-1].activeTab= this;
	tabsIntabs[this.tabindex-1].tabs.onchange(e);
    setVisible(this.content, true);
	
	if (bTabInTab == true && this.tabindex == 1 && this.tabnum > 1)
	{
		var idx
		if (tabsIntabs[1].activeTab)
			idx = tabsIntabs[1].activeTab.tabnum - 1;
		else
			idx = 0;
		
		tabsIntabs[1].activeTab = tabsIntabs[1].tabArray[idx];
		tabsIntabs[1].tabArray[idx].className= "tab"+tabsIntabs[1].tabArray[idx].tabindex;
		tabsIntabs[1].tabArray[idx].className= "tabActive"+tabsIntabs[1].tabArray[idx].tabindex;
		tabsIntabs[1].tabArray[idx].src= tabsIntabs[1].tabArray[idx].getAttribute("activesrc");
        setVisible(tabsIntabs[1].tabArray[idx].content, true);
	}

}

function resetTab(tab) 
{
	if (bgActiveTab != "")
	{
		tab.style.backgroundColor = "";
		tab.content.style.backgroundColor = "";
		}


	tab.className= "tab"+tab.tabindex;
	tab.className= "tabHover"+this.tabindex;
	tab.className= "tab"+tab.tabindex;

	//alert(tab.className);
	tab.src= tab.getAttribute("normalsrc");

	setVisible(tab.content,false);
}

function objectGetElementsByClass(tagName, className)
{
	var collection;
	var returnedCollection = new Array(0);
	var objRef = this;
	if (this.tagName == undefined)
		objRef = document;

	
	if(document.all && tagName == "*") collection = document.all;
	else collection = objRef.getElementsByTagName(tagName);
	
  var iLength = collection.length;
	for(var i = 0, counter = 0; i < iLength; i++)
	{
		if(collection[i].className == className)
		{
			returnedCollection[counter] = collection[i];
			counter++;
		}
	}
	return returnedCollection;
}

function setVisible(obj, bool)
{
    if(bool == false) obj.style.visibility = 'hidden';
    else obj.style.visibility = 'visible';
}
