var wizard;
var wizpages;

function wizardInit(bLoadAll)
{
	wizard = objectGetElementByClass("div","wizard");
	wizpages = objectGetElementsByClass("span","wiz1");

	if (wizpages.length > 1) // if no more than 1 then don't need a wizard
	{
		wizard.currentpage = 0;
		wizard.nextpage = 0;
		wizard.prevpage = 0;
		wizard.wizpages = wizpages;
		
		if (wizard.getAttribute("ifrwidth"))
			wizard.wizpagewidth = wizard.getAttribute("ifrwidth");
		else
			wizard.wizpagewidth = wizard.offsetwidth-4;
		
		if (wizard.getAttribute("ifrheight"))	
			wizard.wizpageheight = wizard.getAttribute("ifrheight");
		else
			wizard.wizpageheight = wizard.offsetHeight-70; //if you change the size of the head/foot then this will have to be corrected
			
		wizard.fnNext = WizardDefaultNext;
		wizard.fnBack = WizardDefaultBack;
		wizard.fnFinish = WizardDefaultFinish;
		wizard.fnCancel = WizardDefaultCancel;

		wizard.title = wizard.getAttribute("title");

		var strHTML = "<div id='wizheader' class='wizheader'></div>";
		strHTML += "<div id='wizframecontainer' class='wizframes' style='width:"+ wizard.wizpagewidth +"px; height:"+ wizard.wizpageheight +"px;' >";

		for(var x=0;x < wizpages.length; x++)
		{
			wizpages[x].pagenumber = x + 1;
			wizpages[x].urlpage = wizpages[x].getAttribute("wizpage");
			wizpages[x].title = wizpages[x].innerText; 
			
			strHTML += "<div id='wizPageDiv" + (x+1) + "' class='wizIfrPage' >";
			if (bLoadAll == true)
				strHTML += "<iframe src='" + wizpages[x].urlpage + "' id='wizIframe" + (x+1) + "' class='wizIframe' style='width:99%; height:99%' allowtransparency='true' ></iframe>"
			else
				strHTML += "<iframe src='../blank.asp' id='wizIframe" + (x+1) + "' class='wizIframe' style='width:99%; height:99%' allowtransparency='true' ></iframe>"
			strHTML += "</div>";
		}
		
		strHTML += "</div>";
		strHTML += "<div id='wizfooter' class='wizfooter' style='position:absolute;top:"+ (parseInt(wizard.offsetHeight)-27) + "px;' >"
		strHTML += "<table width='95%' align='center'><tr><td width='40%'><span id='wizfooterlabel' class='wizfooterlabel'>"+wizard.title+"</span></td>";
		strHTML += "<td><input type='Button' name='Cancel' value='Cancel' class='formbutton' title='Cancel' onclick='wizard.fnCancel()'></td>";
		strHTML += "<td><input type='Button' name='Back' value='< Back' disabled class='formbutton' title='< Back' onclick='wizard.fnBack()'></td>";
		strHTML += "<td><input type='Button' name='Next' value='Next >' class='formbutton' title='Next >' onclick='wizard.fnNext()'></td>";				
		strHTML += "<td><input type='Button' name='wizButtonFinish' value='Finish' disabled class='formbutton' name='Finish' title='Finish' onclick='wizard.fnFinish()'></td>";
		strHTML += "</tr></table></div>";
		
		wizard.innerHTML = strHTML;
		WizardChangePage(1);
	}
}

function WizardChangePage(num)
{
	if (wizard.currentpage > 0)
	{
		setVisible(document.getElementById("wizPageDiv"+wizard.currentpage), false);
		wizard.prevpage = wizard.currentpage;
	}

	var ifrObj = document.getElementById("wizIframe"+num);
	if (ifrObj)
	{
		if (ifrObj.src == "../blank.asp")
			ifrObj.src = wizpages[num-1].urlpage;
		setVisible(ifrObj.parentElement, true);
		wizard.currentpage = num;
		var lobj = document.getElementById("wizheader");
		lobj.innerText = "Step " + num + " of " + wizpages.length + "  -  " + String(wizpages[num-1].title)
	}

}


function WizardDefaultNext()
{
	Back.disabled = false;
	if (wizard.currentpage < wizpages.length)
	{
		var num = wizard.currentpage + 1;
		WizardChangePage(num);
		if (num == wizpages.length){
			wizButtonFinish.disabled = false;
			Next.disabled = true;
		}
	}
}

function WizardDefaultBack()
{
	Next.disabled = false;
	wizButtonFinish.disabled = true;
	
	if (wizard.currentpage > 1)
	{
		var num = wizard.currentpage - 1;
		WizardChangePage(num);
		if (num == 1)
			Back.disabled = true;
	}
	
	
			

}

function WizardDefaultFinish()
{
	alert("Need to override Finish");
}

function WizardDefaultCancel()
{
	alert("Need to override Cancel");
}


function objectGetElementByClass(tagName, className)
{
	var divs = document.all.tags("DIV");
	var index = 0;

	//scan all Divs tags for class name
	for (var i=0; i<divs.length; i++)
	{
		if (divs[i].className == "wizard")
			return divs[i];
	}
}


function objectGetElementsByClass(tagName, className)
{
	var collection;
	var returnedCollection = new Array(0);
	
	if(document.all && tagName == "*") collection = document.all;
	else collection = document.getElementsByTagName(tagName);
	
	for(var i = 0, counter = 0; i < collection.length; i++)
	{
		if(collection[i].className == className)
		{
			returnedCollection[counter] = collection[i];
			counter++;
		}
	}
	return returnedCollection;
}

function setVisible(obj, bool)
{
    if(bool == false)
		{
			obj.style.visibility = 'hidden';
			obj.style.display = 'none';
			obj.style.zindex = 1;
		}
    else
		{
			obj.style.visibility = 'visible';
			obj.style.display = 'inline';
			obj.style.zindex = 1000;
		}
}

