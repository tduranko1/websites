/*	CheckBox Object
	
	Description: 
		Image based javascript object that simulates the standard HTML checkbox that can
		use any value (string or number) for the true/false values. 	All mouse actions 
		are ecapsulated in the object to manage state and the true/false values.  A hidden
		input box is used to hold the selected value so the object can used in a form based
		request page.
		
	Usage:
		Create the object and Html inserts by using the Method 'AddCheckBox'
		Any key press will case the value to toggle.
		

   	$History: checkbox.js $ 
 * 
 * *****************  Version 1  *****************
 * User: Dprice       Date: 11/24/04   Time: 9:01a
 * Created in $/websites/apd/v1.4/WEBROOT/js
 * 
 * *****************  Version 2  *****************
 * User: Mkotipalli   Date: 8/25/04    Time: 4:32p
 * Updated in $/websites/apd/v1.3/webroot/js
 * Defect #2068: set focus to the check box object too.
 * 
 * *****************  Version 1  *****************
 * User: Jcarpenter   Date: 11/14/03   Time: 8:25a
 * Created in $/websites/apd/v1.3/webroot/js
 * 
 * 
 * *****************  Version 36  *****************
 * User: Rvishegu     Date: 10/16/02   Time: 4:09p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * the focus thing caused a problem in Vehicle Damage List. Fixed it.
 * 
 * *****************  Version 35  *****************
 * User: Jrodrigues   Date: 10/14/02   Time: 1:39p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * Set focus to parentElement to buble up the SAVE button.
 * 
 * *****************  Version 34  *****************
 * User: Rvishegu     Date: 8/29/02    Time: 5:02p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * missed out the javaScript equality check "==".
 * 
 * *****************  Version 33  *****************
 * User: Rvishegu     Date: 8/29/02    Time: 4:56p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * Part of defect #699.
 * when using the keyPress on the CheckBox it works fine. But when we
 * control something else (like changing the checkbox enable something
 * else on the page) on the page it failed. Fixed this.
 * 
 * *****************  Version 32  *****************
 * User: Rvishegu     Date: 8/29/02    Time: 4:44p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * modified the way the inner Anchor tag works. When clicked the href="#"
 * caused the content in IFrame to jump up. To avoid this, I removed the
 * href and added a onKeyPress event to handle <Enter> and <Space Bar>.
 * Also, for the hand cursor added this in the style to the Anchor tag.
 * 
 * *****************  Version 31  *****************
 * User: Rvishegu     Date: 8/26/02    Time: 12:40p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * more enhancements (defect #699). added feature to disable the visual
 * effects when the control is disabled.
 * 
 * *****************  Version 30  *****************
 * User: Rvishegu     Date: 8/23/02    Time: 8:28a
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * more enhancements related to defect #699
 * 
 * *****************  Version 29  *****************
 * User: Rvishegu     Date: 8/22/02    Time: 11:09a
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * defect #699 left out bug fixes. When the check box is readonly and the
 * default to True is false, the control shows checked. Fixed this
 * problem. This is part of the #699 optimizing issue of the check boxes.
 * 
 * *****************  Version 28  *****************
 * User: Rvishegu     Date: 8/22/02    Time: 11:00a
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * defect #699 left out bug fixes. When the check box is readonly and the
 * default to True is false, the control shows checked. Fixed this
 * problem. This is part of the #699 optimizing issue of the check boxes.
 * 
 * *****************  Version 27  *****************
 * User: Rvishegu     Date: 8/18/02    Time: 2:35p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * defect #699 fixes - optimize the check box control.
 * 
 * *****************  Version 26  *****************
 * User: Dlowery      Date: 8/05/02    Time: 5:05p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * More optim,,s,,s,s,s,,sss
 * 
 * *****************  Version 24  *****************
 * User: Dlowery      Date: 8/05/02    Time: 2:02p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * 
 * *****************  Version 23  *****************
 * User: Rvishegu     Date: 7/26/02    Time: 1:57p
 * Updated in $/websites/apd/v1.1.0/webroot/js
 * cosmetic fixes (due to defect #589) when the asp page is not from the
 * root folder. The images (hover, hover-out) were missing due to the
 * relative image path. When clicking on the checkbox, the an error
 * message indicating the hidden input box was not found (user maintanence
 * uses a form to wrap all the input controls). Fixed this by finding the
 * input box using document.all....
 * 
 * *****************  Version 21  *****************
 * User: Dlowery      Date: 6/27/02    Time: 3:04p
 * Updated in $/websites/apd/v1.0.0/webroot/js
 * 
 * *****************  Version 20  *****************
 * User: Dlowery      Date: 5/31/02    Time: 4:11p
 * Updated in $/websites/apd/v1.0.0/webroot/js
 * 
 * *****************  Version 19  *****************
 * User: Jrodrigues   Date: 5/31/02    Time: 4:13p
 * Updated in $/websites/apd/v1.0.0/webroot/js
 * Added readonly param to CheckBox
 * 
 * *****************  Version 18  *****************
 * User: Dlowery      Date: 5/14/02    Time: 2:41p
 * Updated in $/websites/apd/v1.0.0/webroot/js
*/
/*------------------------------------------------------------------------------------*/
/*	CheckBox - Method 

	Description:
		used to Initalize the checkbox object, setting custom properties and hooking events
		
	Parameters:
		layername 		- user name for checkbox
		truevalue 		- value that the object sets for a checked condition
		falsevalue 		- value that the object sets for a unchecked condition
		defaultToTrue - default value to set during init.

	Return: 
		None

*/
/*function CheckBox(layername,trueValue,falseValue,defaultToTrue, readonly) 
{
	this.layer = layername + "Div";														// containing DIV tag
	this.imgName = layername + "Img";													// image the contains the checked/uncheck images
	this.inputbox = layername;																// attached inputbox to hold the value for form tags
	this.trueValue = trueValue;																// the actual true value
	this.falseValue = falseValue;															// the actual false value
	this.state = (defaultToTrue) ? 1 : 0;											// current state of the object
	this.value = (this.state) ? this.trueValue : this.falseValue;// the current value for the state
	this.change = CheckBoxChange;															// change method interface				
	
	// hook mouse and keyboard events
	var cbImg = document.all[this.imgName]; //document.getElementById(this.imgName);
	var oLayer = document.all[this.layer]; //document.getElementById(this.layer);
	var oLayerChild = oLayer.firstChild;
		
  if (readonly == null || readonly == false)
  {
  	cbImg.onmouseover = CheckBoxHover;									// hoover effect
		oLayerChild.onfocus = CheckBoxFocus;
		oLayerChild.onblur = CheckBoxBlur;
	  cbImg.onmouseout = CheckBoxHoverOut;
  }

	cbImg.checkbox = this;															// the image has a reference to the parent object
	oLayer.onkeypress = this.change;
	oLayer.cbObj = this;																	// the Div has a reference to the parent object
	
}*/

/*------------------------------------------------------------------------------------*/
/*	CheckBoxChange - Method 

	Description:
		Handles the mouse click and key press event, and manual state changes to 
		change the state of the object (true/false)
		
	Parameters:
		state - optional - the state to set the object to
	Return: 
		None

*/	
function CheckBoxChange(obj) 
{
    if (obj.parentElement.readOnly == true  || obj.parentElement.disabled == true)
        return;

    var cbValue = obj.value;
    var objImg = obj.firstChild;
    if (cbValue == obj.falseValue) {
        objImg.src = "/images/cbhoverck.png";
        obj.value = obj.trueValue;
    }
    else {
        objImg.src = "/images/cbhover.png";
        obj.value = obj.falseValue;
    }
    obj.parentElement.lastChild.value = obj.value;
    
    //this will automatically make the tab, on which this check box lives, as the active one.
    if (document.activeElement == obj)
        obj.focus();
    else {
        try {
            obj.parentElement.parentElement.focus();
			obj.focus();
        } catch (e) {}
    }

	if (typeof(gbDirtyFlag)=='boolean')
		gbDirtyFlag = true;
	if (typeof(parent.gbDirtyFlag)=='boolean')
		parent.gbDirtyFlag = true;
}

function CheckBoxKeyPress(obj){
    var iKeyCode = window.event.keyCode;
    
    if ((iKeyCode == 13) || (iKeyCode == 32)){ //<Enter> and <Space Bar>
        CheckBoxChange(obj);
        obj.parentElement.fireEvent("onclick"); //this will help when we programatically control something else on the page.
    }
}

/*------------------------------------------------------------------------------------*/
/*	CheckBoxHover - Method 

	Description:
		Mouse event handler for the mouseover effect
		
	Parameters:
		None

	Return: 
		None

*/	
function CheckBoxHover(obj)
{
    var objParent = obj.parentElement;
    if (objParent.parentElement.readOnly == true || objParent.parentElement.disabled == true)
        return;

    if (objParent.value == objParent.falseValue)
        obj.src = "/images/cbhover.png";
    else 
        obj.src = "/images/cbhoverck.png";
}


function CheckBoxFocus(obj)
{
    CheckBoxHover(obj.firstChild);
}


/*------------------------------------------------------------------------------------*/
/*	CheckBoxHoverOut - Method 

	Description:
		Mouse event handler for the mouseover effect		
		
	Parameters:
		None

	Return: 
		None

*/	
function CheckBoxHoverOut(obj)
{
    var objParent = obj.parentElement;
    if (objParent.parentElement.readOnly == true  || objParent.parentElement.disabled == true)
        return;
    if (objParent.value == objParent.falseValue)
        obj.src = "/images/cbuncheck.png";
    else
        obj.src = "/images/cbcheck.png";
}

function CheckBoxBlur(obj)
{
    CheckBoxHoverOut(obj.firstChild);

}

/*------------------------------------------------------------------------------------*/
/*	AddCheckBox - Method 

	Description:
		Used to create a checkbox in a HTML page. This writes the HTML structures that make up
		the checkbox object and places a initialization call to create the object interfaces
		
	Parameters:
		cbname 				- name of the object to be referenced by, this is also the name of the inputbox
		truevalue			- custom value for the true state, can be string or number
	 	falsevalue		-	custom value for the false state, can be string or number
		defaultToTrue	- default value to used to set the inital state
	Return: 
		None

*/
function AddCheckBox(cbname, truevalue, falsevalue, defaultToTrue, idnumber, readonly, tabindex, returnObj)
{
    try
    {
        var str = "";
        var imgname = "/images/cbuncheck.png";

        var selectval = falsevalue;
        if (defaultToTrue == true)
        {
            imgname = "/images/cbcheck.png";
            selectval = truevalue;
        }

        if (idnumber)
            cbname += idnumber;

        str = "<div unselectable='on' id='"+cbname+"Div'>";
        
        str +="<a unselectable='on' ";
        
        
        if ((readonly == null || readonly == false)) {
            if (tabindex != null)
                str +="tabindex='" + tabindex + "' ";
        }
        else
            str +="tabindex='-1' ";
        
        str += "trueValue='" + truevalue + "' falseValue='" + falsevalue + "' readOnly='" + readonly + "' value='" + selectval + "' ";
        
        if ((readonly == null || readonly == false)) {
            str += " style='cursor:hand' onClick='CheckBoxChange(this)' ";
            str += "onfocus='CheckBoxFocus(this)' onblur='CheckBoxBlur(this)' onkeypress='CheckBoxKeyPress(this);' ";
        }
        else {
            if (selectval == truevalue)
                imgname = "/images/cbcheckro.png";
            else
                imgname = "/images/cbreadonly.png";
        }
        
        str += "><img unselectable='on' id='"+cbname+"Img' src='"+imgname+"' width=13 height=13 border='0' ";
        if ((readonly == null || readonly == false))
            str += "onmouseover='CheckBoxHover(this)' onmouseout='CheckBoxHoverOut(this)' ";
        str += "></a>";
        str +="<input type='hidden' name='" + cbname + "' id='" + cbname  + "' value='" + selectval + "' />";
        
        str +="</div>";
        
        if (returnObj)
            return str;
        else
            document.write(str);

    }
    catch(e){ 
        if (returnObj)
            return e.message;
        else
            document.write(e.message);
   }
}

function CheckBoxGetValue(obj){
    var retVal = null;
    if (obj) {
        var objTag = obj.tagName;
        switch(objTag){
            case "DIV":
                retVal = obj.lastChild.value;
                break;
            case "A":
                retVal = obj.parentElement.lastChild.value;
                break;
            default:
                retVal = obj.getAttribute("value");
        }
    }
    return retVal;
}
	
// preload the images so the swapping is faster
preload('checkbox0','/images/cbuncheck.png')
preload('checkbox1','/images/cbcheck.png')
preload('checkboxhover0','/images/cbhover.png')
preload('checkboxhover1','/images/cbhoverck.png')
	
	
	
	
/*------------------------------------------------------------------------------------*/
/*	 - Method 

	Description:
		
		
	Parameters:

	Return: 
		None

*/	