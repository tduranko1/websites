﻿//-------------------------------------//
// Web Service Call - Globals
//-------------------------------------//
var webserUrl = "APDFoundationWrapper.asmx";
//var webserUrl = "http://dapd.pgw.local/APDFoundationWrapper.asmx";
//-------------------------------------//
// Web Service Call - ExecuteSQL
//-------------------------------------//
$(document).ready(function () {
    WSExecuteSQL = function (storedProc, params) {
        if (!storedProc || storedProc === "") {
            alert("Internal Error. No storedProc Name specified.\\r\\n" + 'System Communications Error');
        }
        //alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                                <soap:Body> \
                                    <ExecuteSQL xmlns="http://tempuri.org/"> \
                                    <strStoredProcedure>' + storedProc + '</strStoredProcedure> \
                                    </ExecuteSQL> \
                                </soap:Body> \
                                </soap:Envelope>';


       // alert(soapRequest);

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            data: soapRequest,
            contentType: "text/xml; charset=\"utf-8\"",
            success: WSExecuteSQLSuccessOccur,
            error: WSExecuteSQLErrorOccur
        });
    };
});

function WSExecuteSQLSuccessOccur(data, status, req) {
    if (status == "WSExecuteSQLSuccessOccur")
        alert(req.responseText);
}

function WSExecuteSQLErrorOccur(data, status, req) {
    alert(req.responseText + "1111111 " + status + "2222");
}


//-------------------------------------//
// Web Service Call - ExecuteSpAsXML
//-------------------------------------//
var returnData;

$(document).ready(function () {
    WSExecuteSpAsXML = function (storedProc, params) {
        if (!storedProc || storedProc === "") {
            alert("Internal Error. No storedProc Name specified.\\r\\n" + 'System Communications Error');
        }
        //alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                                <soap:Body> \
                                    <ExecuteSpAsXML xmlns="http://tempuri.org/"> \
                                    <StoredProcedure>' + storedProc + ' </StoredProcedure> \
                                    <Parameters>' + params + ' </Parameters> \
                                    </ExecuteSpAsXML> \
                                </soap:Body> \
                                </soap:Envelope>';
        //alert(soapRequest);

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            cache: false,
            data: soapRequest,
            async: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: SuccessOccur, 
            error: ErrorOccur 
        });

        return returnData;
    };
});

function SuccessOccur(data, status, req) {
    returnData = data;
    //alert("Success");
}

function ErrorOccur(data, status, req) {
    alert("failed");
}

//-------------------------------------//
// Web Service Call - ExecuteSp
//-------------------------------------//
$(document).ready(function () {
    WSExecuteSp = function (storedProc, params) {
        if (!storedProc || storedProc === "") {
            alert("Internal Error. No storedProc Name specified.\\r\\n" + 'System Communications Error');
        }
        //alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                                <soap:Body> \
                                    <ExecuteSp xmlns="http://tempuri.org/"> \
                                    <StoredProcedure>' + storedProc + ' </StoredProcedure> \
                                    <Parameters>' + params + ' </Parameters> \
                                    </ExecuteSp> \
                                </soap:Body> \
                                </soap:Envelope>';
        //alert(soapRequest);

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            cache: false,
            data: soapRequest,
            async: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: ExecuteSpSuccessOccur,
            error: ExecuteSpErrorOccur
        });

        return returnData;
    };
});

function ExecuteSpSuccessOccur(data, status, req) {
    returnData = $(data).find("WSExecuteSpResult").text();;
}

function ExecuteSpErrorOccur(data, status, req) {
    alert("failed");
}

//-------------------------------------//
// Web Service Call - XslGetSession
//-------------------------------------//
$(document).ready(function () {
    XslGetSession = function (SessionKey, SessionVariable) {
        
        if (!SessionKey || SessionKey === "") {
            alert("Internal Error. No SessionKey Name specified.\\r\\n" + 'System Communications Error');
        }
        //alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                              <soap:Body> \
                                <XslGetSession xmlns="http://tempuri.org/"> \
                                  <SessionKey>' + SessionKey + ' </SessionKey> \
                                  <SessionVariable>' + SessionVariable + ' </SessionVariable> \
                                </XslGetSession> \
                              </soap:Body> \
                            </soap:Envelope>';
       

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            cache: false,
            data: soapRequest,
            async: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: SessionSuccess,
            error: Sessionfailure
        });

        return returnData;
    };
});

function SessionSuccess(data, status, req, xml, xmlHttpRequest, responseXML) {
       returnData = $(data).find("XslGetSessionResult").text();
   
}

function Sessionfailure(data, status, req) {
    alert("SessionFailed");
}

//-------------------------------------//
// Web Service Call - UodateTabSession
//-------------------------------------//
$(document).ready(function () {
    UpdateSessionVar = function (SessionKey, SessionVariable, SessionValue) {
        if (!SessionKey || SessionKey === "") {
            alert("Internal Error. No SessionKey Name specified.\\r\\n" + 'System Communications Error');
        }
         var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                              <soap:Body> \
                                <UpdateSessionVar xmlns="http://tempuri.org/"> \
                                  <SessionID>' + SessionKey + '</SessionID> \
                                  <SessionVariable>' + SessionVariable + ' </SessionVariable> \
                                  <SessionValue>' + SessionValue + '</SessionValue> \
                                </UpdateSessionVar> \
                              </soap:Body> \
                            </soap:Envelope>';

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            cache: false,
            data: soapRequest,
            async: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: SessionSuccess,
            error: updSessionfailure
        });

        return returnData;
    };
});

function updSessionSuccess(data, status, req, xml, xmlHttpRequest, responseXML) {
    returnData = data

}

function updSessionfailure(data, status, req) {
    alert("updSessionFailed");
}
