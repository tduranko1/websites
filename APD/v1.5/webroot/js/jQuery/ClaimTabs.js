﻿//-------------------------------------------//
// Handle the screen Div changes each time
// the tabs are clicked
//-------------------------------------------//

//-- Globals -->
var oWindowHandler;
var isgbDirty = false;
var TabDirty;


function MenuTab(evt, currentClass, tabName, tabContainer, subTabContainer, subTab, tabInnerText) {

    var ct;
    var strsessionkey;

    //To get the default menu from Session based on session key 
    strsessionkey = getUrlParameter("SessionKey");
    strsessionkey = strsessionkey.replace("{", "").replace("}", "");

    var tabInnerHTML = '';
    if (evt != '')
        tabInnerHTML = evt.srcElement.innerText;

    if (tabInnerHTML === '')
        tabInnerHTML = tabInnerText;

    udpateTabSession(strsessionkey, currentClass, tabName, tabContainer, subTabContainer, subTab, tabInnerHTML)

    ////var tablinks = getElementsByClassName(currentClass);
    var tablinks = $("." + currentClass);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    if (tabContainer) {
        //// tabcontent = getElementsByClassName(tabContainer);
        tabcontent = $("." + tabContainer);
        for (ct = 0; ct < tabcontent.length; ct++) {
            tabcontent[ct].style.display = "none";
        }
    }

    if (evt != '')
        evt.srcElement.className += " active";
    else {
        if (tabInnerText != '') {
            var obj = $('button:contains(' + tabInnerText + ')');
            if (obj[0] != null)
                obj[0].className += " active";
        }
    }

    ////document.getElementById(tabName).style.display = "block";
    $("#" + tabName).removeAttr("style");
    $("#" + tabName).attr("style", "display: block;");

    if (subTab && subTab != 'undefined') {

        ////var subTabcontent = getElementsByClassName(subTab);
        var subTabcontent = $("." + subTab);
        for (ct = 0; ct < subTabcontent.length; ct++) {
            if (subTabcontent[ct].style.display === 'block') {
                subTabcontent[ct].style.display = 'none';
            }
        }
    }
    if (subTabContainer && subTabContainer != 'undefined') {
        ////document.getElementById(subTabContainer).style.display = "block";
        $("#" + subTabContainer).css("display", "block");

        //// var vehDetails = document.getElementById(subTabContainer).parentNode.children[0];
        var vehDetails = $("#" + subTabContainer).parent().children(":first-child");
        if (vehDetails.length > 0) {
            for (ct = 0; ct < vehDetails.children('button').length; ct++) {
                vehDetails.children(0)[ct].className = vehDetails.children(0)[ct].className.replace(" active", "");
            }
            vehDetails.children(0)[0].className += " active";
        }
    }
    showbuttonTabs(tabName);
    SetDirty();
    ActiveAPDTabAttributes(tabName);

    //Choice shop validation message purpose var obj = $("#" + sender.id.replace(senderclientID, "hidInvolvedSysLastUpdatedDate"));
    var assinmentTypetext = ($('#' + tabName).find("[id$='txtInitialAssignmentType']")).attr('value');
    if (assinmentTypetext == "Choice Shop Assignment")
        alert("Please note this vehicle is assigned to a Choice Shop and is being processed in Hyperquest.\nPlease do not attempt to work this vehicle in APD.");
}

$(document).ready(function () {
    SessionPageLoad();

    //this is called for set Dirty 
    $("input,select,textarea").change(function (e) {
        var flag = true;
        var obj = e.target;
        while (flag) {
            if (obj.parentNode.tagName.toLowerCase() === 'div') {
                var classnames = obj.parentNode.className;
                if (classnames != '' && (classnames.indexOf('tabVehDetcontent') >= 0 || classnames.indexOf('TabClaimContent') >= 0 || classnames.indexOf('tabAssignmentcontent') >= 0 || classnames.indexOf('tabVehcontent') >= 0)) {
                    TabDirty = obj.parentNode.id;
                    flag = false;
                }
                else {
                    obj = obj.parentNode;
                }
            }
            else
                obj = obj.parentNode;
        }
        isgbDirty = true;
    });

    $(document).mouseup(function (e) {
        if (isgbDirty) {
            var container = $("#" + TabDirty);
            var dateContainer = $("#ui-datepicker-div");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if (!dateContainer.is(e.target) && dateContainer.has(e.target).length === 0)
                    SetDirty();
            }
        }
    });

    //disable text box in the warranty tab 
    $(".tabWarAssignmentcontent input[type='text'],[id$=btnWarSendAssignment],[id$=btnWarCancelAssignment],[id$=ddlWarDisposition]").attr("disabled", "disabled");

    //adding placeholders
    $("[id$=txtWarShopPhone],[id$=txtWarShopFax]").attr("placeholder", "(   )   -    ");
    $("[id$=txtWarCurAssgDate],[id$=txtWarPrevAssgDate],[id$=txtWarRepairStartDate],[id$=txtWarRepairEndDate]").attr("placeholder", "  /  /    ");

    $("[id$=btnVehicleLocationSave_Click]").on("click", function () {
        var sphone = $("#" + this.id.replace("btnVehicleLocationSave_Click", "txtLocationPhone")).val();
        var sZip = $("#" + this.id.replace("btnVehicleLocationSave_Click", "txtLocationZip")).val();
        if (sphone.replace(/[^0-9]/g, "").length == 10 || sphone == "") { }
        else {
            alert("Please check the phone number");
            return false;
        }
        if (sZip.replace(/[^0-9]/g, "").length == 5 || sZip == "") { }
        else {
            alert("Please check the zip code");
            return false;
        }
    });

    $("[id$=btnVehicleContactSave]").on("click", function () {
        var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var sEmail = $("#" + this.id.replace("btnVehicleContactSave", "txtContactEmailAddress")).val();
        var sZip = $("#" + this.id.replace("btnVehicleContactSave", "txtContactZip")).val();
        if (regEx.test(sEmail) || sEmail == "") {}
        else{
         alert("Email id not valid");
            return false;
        }
        if (sZip.replace(/[^0-9]/g, "").length == 5 || sZip == "") { }
        else {
            alert("Please check the zip code");
            return false;
        }
    });

    $("[id$=btnSaveInvolved]").on("click", function () {
        var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var sEmail = $("#" + this.id.replace("btnSaveInvolved", "txtInvolvedEmailAddress")).val();
        var sZip = $("#" + this.id.replace("btnSaveInvolved", "txtInvolvedAddressZip")).val();
        var dphone = $("#" + this.id.replace("btnSaveInvolved", "txtInvolvedPhoneDay")).val();
        var nphone = $("#" + this.id.replace("btnSaveInvolved", "txtInvolvedPhoneNight")).val();
        var aphone = $("#" + this.id.replace("btnSaveInvolved", "txtAlt")).val();
        if (!regEx.test(sEmail)) {
            if (sEmail != "") {
                alert("Email id not valid");
                return false;
            }
        }
        if (dphone.replace(/[^0-9]/g, "").length == 10 || dphone == "") { }
        else {
            alert("Please check the day phone number");
            return false;
        }
        if (nphone.replace(/[^0-9]/g, "").length == 10 || nphone == "") { }
        else {
            alert("Please check the night phone number");
            return false;
        }
        if (aphone.replace(/[^0-9]/g, "").length == 10 || aphone == "") { }
        else {
            alert("Please check the alternate phone number");
            return false;
        }
        if (sZip.replace(/[^0-9]/g, "").length == 5 || sZip == "") { }
        else {
            alert("Please check the zip code");
            return false;
        }
    });
    //Adding app_variable code for rentalMgt button
    var strFunctionName = "GetApplicationVar";
    var strDataToSend = "?VariableName=RentalMgt_Flag";
    RentalMgtHttpGet(strFunctionName, strDataToSend);


});

function RentalMgtHttpGet(strFunctionName, strDataToSend) {
    if (typeof window.parent.GetApdWebServiceURL == 'function' || typeof window.parent.GetApdWebServiceURL == 'object')
        var strServiceURL = window.parent.GetApdWebServiceURL();
    if (strServiceURL != "" && strServiceURL != null && strServiceURL != undefined) {
        var getURL = strServiceURL + "/" + strFunctionName + strDataToSend;
        var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
        //dataToSend = "StoredProcedure=uspWorkflowSendShopAssignWSXML" + "&Parameters=" + strAssignmentID;
        objXMLHTTP.open("GET", getURL, false);
        objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        objXMLHTTP.send();

        if (objXMLHTTP.responseXML.text == "True" || objXMLHTTP.responseXML.text == "TRUE")
            $("[id$=btnVehRental]").show();
        else
            $("[id$=btnVehRental]").hide();
    }
}

function ActiveAPDTabAttributes(tabName) {
    
    var objTab;
    var tabNumber = "01";
    var sCurServiceChannelCD = "";
    var sCurClaimAspectServiceChannelID = "";
    var sCurrentClaimAspectID = "";
    var sReopenExpCRUD = $("#hidReOpenExpCRUD").val();
    var sClaimStatus = $("#hidClaimStatus").val();
    var sReassignExpCRUD = $("#hidReassignExpCRUD").val();
    var sCancelExpCRUD = $("#hidCancelExpCRUD").val();
    var sVoidExpCRUD = $("#hidVoidExpCRUD").val();
    var blnPSExists = 0;

    if (tabName.replace(/\D/g, "") != "") {
        objTab = $('#' + tabName);
        if (objTab && objTab.find($('[id^=RepeaterVehdetTab_ctl]'))[0] != undefined && objTab.find($('[id^=RepeaterVehdetTab_ctl]'))[0] != null) {
            tabNumber = objTab.find($('[id^=RepeaterVehdetTab_ctl]'))[0].id.replace(/\D/g, "");
        }
        sCurrentClaimAspectID = $("#RepeaterVehdetTab_ctl" + tabNumber + "_hidVehicleClaimAspectID").val();
        sCurServiceChannelCD = $("#RepeaterVehdetTab_ctl" + tabNumber + "_hidServiceChannelCD").val();
        sCurClaimAspectServiceChannelID = $("#RepeaterVehdetTab_ctl" + tabNumber + "_hidClaimAspectServiceChannelID").val();

        if (sCurServiceChannelCD == "PS" && sCurClaimAspectServiceChannelID != "") {
            blnPSExists = 1;
        }

        if (typeof window.parent.SetCurrentClaimAspectId == 'function' || typeof window.parent.SetCurrentClaimAspectId == 'object') {
            window.parent.SetCurrentClaimAspectId(sCurrentClaimAspectID);
        }

        if (typeof window.parent.VehiclesMenu == 'function' || typeof window.parent.VehiclesMenu == 'object') {
            window.parent.VehiclesMenu(false, 1); // Complete Service Channel
            window.parent.VehiclesMenu(false, 2); // Cancel Service Channel
            window.parent.VehiclesMenu((sReopenExpCRUD.indexOf("U") != -1), 3); // Reactivate Service Channel
            window.parent.VehiclesMenu((sReopenExpCRUD.indexOf("U") != -1 && blnPSExists == 1), 4); // Active Warr

            if (sClaimStatus == "Open") {
                window.parent.VehiclesMenu((sReassignExpCRUD.indexOf("U") != -1), 6);  //Reassign vehicle
                window.parent.VehiclesMenu((sCancelExpCRUD.indexOf("U") != -1), 7); //Cancel vehicle
                if (window.parent.blnSupervisorFlag == "1") {
                    window.parent.VehiclesMenu((sVoidExpCRUD.indexOf("U") != -1), 8); //Void vehicle
                } else
                    window.parent.VehiclesMenu(false, 8);
            } else {
                window.parent.VehiclesMenu(false, 7); // Cancel
                window.parent.VehiclesMenu(false, 8); // Void
            }
        }
    }
}

function SetDirty() {
    if (isgbDirty) {
        var btnDirty = $("#" + TabDirty + " input[type='submit'][value='Save']");

        if (confirm("Do you want to save changes?")) {
            if (btnDirty != null) {
                $("#" + btnDirty.attr('id')).trigger("click");
                isgbDirty = false;
            }
        }
        else {
            isgbDirty = false;
            // location.reload();
            window.location.href = window.location.href;
        }
    }
    return false;
}


function showbuttonTabs(tabName) {

    var tab = tabName.split("_");

    if (tabName.indexOf("divDescription") >= 0) {
        if (tab.length >= 2) {
            $("#divVehInvolved_" + tab[1]).css("display", "none");
            $("#divVehSave_" + tab[1]).css("display", "block");
        }
    }
    else if (tabName.indexOf("divInvolved") >= 0) {
        if (tab.length >= 2) {
            $("#divVehSave_" + tab[1]).css("display", "none");
            $("#divVehInvolved_" + tab[1]).css("display", "block");

        }
    }
    else if (tabName.indexOf("divMainAssignment") >= 0) {
        if (tab.length >= 2) {
            $("#divVehSave_" + tab[1]).css("display", "none");
            $("#divVehInvolved_" + tab[1]).css("display", "none");

        }
    }
    else {
        $("#divVehSave_" + tab[1]).css("display", "none");
        $("#divVehInvolved_" + tab[1]).css("display", "none");
    }
}

function udpateTabSession(strsessionkey, currentClass, tabName, tabContainer, subTabContainer, subTab, tabInnerText) {
    var strRet;
    if (tabContainer == 'TabClaimContent')
        strRet = UpdateSessionVar(strsessionkey, 'ClaimTab', 'currentClass=' + currentClass + ';tabName=' + tabName + ';tabContainer=' + tabContainer + ';tabInnerText=' + tabInnerText);
    else
        strRet = UpdateSessionVar(strsessionkey, 'ClaimVehTab', 'currentClass=' + currentClass + ';tabName=' + tabName + ';tabContainer=' + tabContainer + ';subTabContainer=' + subTabContainer + ';subTab=' + subTab + ';tabInnerText=' + tabInnerText);
}

function SessionPageLoad() {
    var strSessionKey = getUrlParameter('SessionKey');
    strSessionKey = strSessionKey.replace("{", "").replace("}", "");

    GetTabsession(strSessionKey, 'ClaimTab')
    GetTabsession(strSessionKey, 'ClaimVehTab')

    SetMask()

}

function SetMask() {
    //Claim tab
    $("#txtLossDate").datepicker();
    $("#txtIntakeFinishDate").datepicker();

    $("#txtLossDate").mask("00/00/0000 00:00:00");
    $("#txtIntakeFinishDate").mask("00/00/0000 00:00:00");

    //Insured Tab
    $("#txtDay").mask("(000)000-0000");
    if ($("#txtDay").val() == '(')
        $("#txtDay").val('');
    $("#txtNight").mask("(000)000-0000");
    if ($("#txtNight").val() == '(')
        $("#txtNight").val('');
    $("#txtAlternate").mask("(000)000-0000");
    if ($("#txtAlternate").val() == '(')
        $("#txtAlternate").val('');


    //Carrier Tab
    $("#txtCarrierPhone").mask("(000)000-0000");
    if ($("#txtCarrierPhone").val() == '(')
        $("#txtCarrierPhone").val('');
    $("#txtCarrierFax").mask("(000)000-0000");
    if ($("#txtCarrierFax").val() == '(')
        $("#txtCarrierFax").val('');

    //Vehicle Location Tab
    $("[id$=txtLocationPhone]").mask("(000)000-0000");
    if ($("[id$=txtLocationPhone]").val() == '(')
        $("[id$=txtLocationPhone]").val('');

    //Inolved Tab
    $("[id$=txtInvolvedBirthDate]").datepicker();

    $("[id$=txtInvolvedBirthDate]").mask("00/00/0000");
    $("[id$=txtInvolvedPhoneDay]").mask("(000)000-0000");
    if ($("[id$=txtInvolvedPhoneDay]").val() == '(')
        $("[id$=txtInvolvedPhoneDay]").val('');
    $("[id$=txtInvolvedPhoneNight]").mask("(000)000-0000");
    if ($("[id$=txtInvolvedPhoneNight]").val() == '(')
        $("[id$=txtInvolvedPhoneNight]").val('');
    $("[id$=txtAlt]").mask("(000)000-0000");
    if ($("[id$=txtAlt]").val() == '(')
        $("[id$=txtAlt]").val('');

    //Contact Tab
    $("[id$=txtContactDay]").mask("(000)000-0000");
    if ($("[id$=txtContactDay]").val() == '(')
        $("[id$=txtContactDay]").val('');
    $("[id$=txtContactNight]").mask("(000)000-0000");
    if ($("[id$=txtContactNight]").val() == '(')
        $("[id$=txtContactNight]").val('');
    $("[id$=txtContactAlternate]").mask("(000)000-0000");
    if ($("[id$=txtContactAlternate]").val() == '(')
        $("[id$=txtContactAlternate]").val('');
    $("[id$=txtContactCell]").mask("(000)000-0000");
    if ($("[id$=txtContactCell]").val() == '(')
        $("[id$=txtContactCell]").val('');


    //Assignment Tab
    $("[id$=txtAssignmentDate]").datepicker();
    $("[id$=txtAssignmentPrevDate]").datepicker();

    $("[id$=txtInspectionDate]").datepicker();
    $("[id$=txtWorkStartDate]").datepicker();
    $("[id$=txtOriginalEstimateDate]").datepicker();
    $("[id$=txtWorkEndDate]").datepicker();
    $("[id$=txtFinalEstDate]").datepicker();
    $("[id$=txtCODate]").datepicker();
    $("[id$=txtWarRepairStartDate]").datepicker();
    $("[id$=txtWarRepairEndDate]").datepicker();

    $("[id$=txtShopAssignmentContactPhone]").mask("(000)000-0000");
    if ($("[id$=txtShopAssignmentContactPhone]").val() == '(')
        $("[id$=txtShopAssignmentContactPhone]").val('');
    $("[id$=txtShopAssignmentContactFax]").mask("(000)000-0000");
    if ($("[id$=txtShopAssignmentContactFax]").val() == '(')
        $("[id$=txtShopAssignmentContactFax]").val('');

    $("[id$=txtAssignmentPrevDate]").mask("00/00/0000");
    $("[id$=txtAssignmentDate]").mask("00/00/0000");

    $("[id$=txtInspectionDate]").mask("00/00/0000");
    $("[id$=txtWorkStartDate]").mask("00/00/0000");
    $("[id$=txtOriginalEstimateDate]").mask("00/00/0000");
    $("[id$=txtWorkEndDate]").mask("00/00/0000");
    $("[id$=txtFinalEstDate]").mask("00/00/0000");
    $("[id$=txtCODate]").mask("00/00/0000");
}


function GetTabsession(SessionKey, SessionVariable) {
    var strSessionval = '';
    strSessionval = XslGetSession(SessionKey, SessionVariable);
    if (strSessionval != '') {

        var arrSessionvalues = strSessionval.split(';');
        var strcurrentClass = '', strtabName = '', strtabContainer = '', strsubTabContainer = '', strsubTab = '', tabInnerText = '';

        for (cnt = 0; cnt < arrSessionvalues.length; cnt++) {

            arrParam = arrSessionvalues[cnt].split('=');
            switch (arrParam[0]) {
                case 'currentClass':
                    strcurrentClass = arrParam[1];
                    break;
                case 'tabName':
                    strtabName = arrParam[1];
                    break;
                case 'tabContainer':
                    strtabContainer = arrParam[1];
                    break;
                case 'subTabContainer':
                    strsubTabContainer = arrParam[1];
                    break;
                case 'subTab':
                    strsubTab = arrParam[1];
                    break;
                case 'tabInnerText':
                    tabInnerText = arrParam[1];
                    break;
            }
        }

        if (SessionVariable == 'ClaimTab')
            MenuTab('', strcurrentClass, strtabName, strtabContainer, '', '', tabInnerText)
        else
            MenuTab('', strcurrentClass, strtabName, strtabContainer, strsubTabContainer, strsubTab, tabInnerText)
    }
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

//-----------------------------------//
// Add Coverage Description Changed
//-----------------------------------//
$(function () {
    $("#ddlCarrierOffice").change(function () {

        //-- Carrier Office Changed - Get Users --//
        //alert($("#ddlCarrierOffice option:selected").text());
        //alert($("#ddlCarrierOffice option:selected").val());
        //alert($("#hidInsuranceCompanyID").val());

        //-- Clear Carrier Rep Users Dropdown --//
        $('#ddlCarrierRep').empty();

        //-- Call Web Service and get Carrier Rep Users --//
        var params = "@InsuranceCompanyID=" + $("#hidInsuranceCompanyID").val() + ", @OfficeID=" + $("#ddlCarrierOffice option:selected").val();
        var xmlReturnData = XslGetSession("uspClaimCarrierRepGetListByOfficeWSXML", params);

        //alert((new XMLSerializer()).serializeToString(xmlReturnData));
        //alert($(xmlReturnData).find('OfficeUser').attr('NameLast'));

        $(xmlReturnData).find('OfficeUser').each(function () {
            var attribData = $(this);
            var carrierRepName = attribData.attr('NameLast') + ", " + attribData.attr('NameFirst');
            $('#ddlCarrierRep').append($("<option     />").val(attribData.attr('UserID')).text(carrierRepName));
        })
    })
})

//-----------------------------------//
// CarrierRep dialog
//-----------------------------------//
$(function () {
    $("#diaCarrierRep").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 370,
        height: 100,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnEditCarrier").on("click", function () {
        $("#diaCarrierRep").dialog("open");
    });

    $("#btnCarrierRepCancel").on("click", function () {
        $("#diaCarrierRep").dialog("close");
    });

});

//-----------------------------------//
// New Service Channel - dialog
//-----------------------------------//
$(function () {
    $("#diaNewServiceChannel").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 370,
        height: 100,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnNewServiceChannel").on("click", function () {
        $("#diaNewServiceChannel").dialog("open");
    });

    $("#btnCarrierRepCancel").on("click", function () {
        $("#diaNewServiceChannel").dialog("close");
    });

});

//-----------------------------------//
// Modal Block - dialog
//-----------------------------------//
$(function () {
    $("#diaModal").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    //$("#diaModal").focus(function () {
    //    alert("here");
    //    $("#diaModal").dialog("close");
    //});

    $("#btnLaunch").on("click", function () {
        $("#diaModal").dialog("open");
        oWindowHandler = window.open("AddCoverage.aspx?WindowID=45", "", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=750px,height=480px,left = 490,top=300,center=yes;");


        setTimeout(function () {
            if (oWindowHandler && !oWindowHandler.closed) {  //checks to see if window is open
                //winPop.focus();
            } else {
                $("#diaModal").dialog("close");
                setInterval(setTimeout, 3000);
            }
        }, 3000
        );


        return false;
    });

    //$("#chkDialogWindow").on("click", function () {
    //alert("incode");

    //if (oWindowHandler && !oWindowHandler.closed) {  //checks to see if window is open
    //    alert("Opened");
    //    //winPop.focus();
    //} else {
    //    alert("Closed");
    //}
    //});

});

//-----------------------------------//
// Dialog Window 
//-----------------------------------//
//function OpenDialogWindow(strWebPageWinType, strPage, strParams, bEnabled, strScreen) {
function OpenDialogWindow(strWebPageWinType, strPage, strParams) {
    //alert(strWebPageWinType);
    //alert(strPage);
    //alert(strParams);
    strRetVal = window.open(strWebPageWinType, strPage, strParams);
    LoadModalDiv();

    // strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
        return false;

    //window.location = 'ClaimXP.aspx';
}

//Javascript dialog for NADA button added below code 
// NADA module popup code

var strRetVal;

function callNada(strVin, strMake, strModel, strVehicleYear, strBodyStyle, strMileage, sZIP) {
    ////  if (gsVehicleCRUD.indexOf("U") == -1 || gsVehicleCRUD.indexOf("R") == -1) return;
    var myreq = "";
    var strNada = "";
    var arrRetVal;
    var arrNameValue;
    var control;
    myreq = "&strVIN=" + strVin;
    if (strNada != "null")
        myreq += "&strVehId=" + strNada +
                  "&strMake=" + strMake +
                  "&strModel=" + strModel +
                  "&strYear=" + strVehicleYear +
                  "&strBody=" + strBodyStyle +
                  "&txtMileage=" + strMileage +
                  "&strReadOnly=" + 0 +
                  "&strZip=" + sZIP;

    // The object "myObject" is sent to the modal window.
    strRetVal = window.open("VehicleEvaluator.asp?WindowID=" + 0 + myreq, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=750px,height=500px,left = 150,top= 80,center=yes;");
    LoadModalDiv();

    // strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
        return false;

    arrRetVal = strRetVal.split('||');
    var iLength = arrRetVal.length;
    for (var j = 0; j < iLength; j++) {
        arrNameValue = arrRetVal[j].split('--');

        switch (arrNameValue[0]) {
            case 'year':
                VehicleYear.value = arrNameValue[1];
                break;
            case 'make':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);  // 15 character limit imposed by database
                Make.value = arrNameValue[1];
                break;
            case 'model':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);
                Model.value = arrNameValue[1];
                break;
            case 'body':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);
                BodyStyle.value = arrNameValue[1];
                break;
            case 'vin':
                if (arrNameValue[1] != "")
                    VIN.value = arrNameValue[1];
                break;
            case 'value':
                BookValueAmt.value = arrNameValue[1].substr(1, arrNameValue[1].length);
                break;
            case 'miles':
                Mileage.value = arrNameValue[1];
                break;
        }
    }
}

function refreshParent() {
    window.opener.location.reload(true);
    window.close();
}

//Load Parent window div for gray out
function LoadModalDiv() {
    var bcgDiv = document.getElementById("divBackground");
    bcgDiv.style.display = "block";
}

//Unload Popup 
function HideModalDiv() {
    var bcgDiv = document.getElementById("divBackground");
    bcgDiv.style.display = "none";
}
function OnUnload() {
    if (false == strRetVal.closed) {
        strRetVal.close();

    }
}
//window.onunload = OnUnload;

function AddInvolved(sender, senderclientID) {
    //reset text --Add new -- for ddlInvolved
    var obj = $("#" + sender.id.replace(senderclientID, "ddlInvolved"));
    if (obj != null)
        obj.val("-1");

    ClearObj(sender, senderclientID, "txtInvolvedNameTitle")
    ClearObj(sender, senderclientID, "txtInvolvedNameFirst")
    ClearObj(sender, senderclientID, "txtInvolvedNameLast")
    ClearObj(sender, senderclientID, "txtInvolvedBusinessName")

    //ddlInvolvedBusinessType   need to implement reset logic

    ClearObj(sender, senderclientID, "txtInvolvedAddress1")
    ClearObj(sender, senderclientID, "txtInvolvedAddress2")
    ClearObj(sender, senderclientID, "txtInvolvedAddressCity")

    //ddlInvolvedState need to implement reset logic

    ClearObj(sender, senderclientID, "txtInvolvedAddressZip")
    ClearObj(sender, senderclientID, "txtInvolvedEmailAddress")

    //cbInsured
    obj = $("#" + sender.id.replace(senderclientID, "cbInsured"));
    if (obj != null)
        obj[0].checked = false;

    //cbClaimant
    obj = $("#" + sender.id.replace(senderclientID, "cbClaimant"));
    if (obj != null)
        obj[0].checked = false;

    //cbOwner
    obj = $("#" + sender.id.replace(senderclientID, "cbOwner"));
    if (obj != null)
        obj[0].checked = false;

    //cbDriver
    obj = $("#" + sender.id.replace(senderclientID, "cbDriver"));
    if (obj != null)
        obj[0].checked = false;

    //cbPassenger
    obj = $("#" + sender.id.replace(senderclientID, "cbPassenger"));
    if (obj != null)
        obj[0].checked = false;

    //ddlInvolvedGender need to implement reset logic

    ClearObj(sender, senderclientID, "txtInvolvedFedTaxId")
    ClearObj(sender, senderclientID, "txtInvolvedBirthDate")
    ClearObj(sender, senderclientID, "txtInvolvedAge")

    //ddlBestNumberCall need to implement reset logic

    ClearObj(sender, senderclientID, "txtInvolvedBestTimeToCall")
    ClearObj(sender, senderclientID, "txtInvolvedPhoneDay")
    ClearObj(sender, senderclientID, "txtInvolvedPhoneNight")
    ClearObj(sender, senderclientID, "txtAlt")
}

function ClearObj(sender, senderclientID, ControlId) {
    obj = $("#" + sender.id.replace(senderclientID, ControlId));
    if (obj != null)
        obj[0].value = "";
}


function GetInvolvedData(sender, senderclientID, strinvolvedID) {
    //-- Call Web Service and get Carrier Rep Users --//
    //populate the data from uspVehicleInvolvedGetDetailXML params [InvolvedID, InsuranceCompanyID]
    var params = "@InsuranceCompanyID=" + $("#hidInsuranceCompanyID").val() + ", @InvolvedID=" + strinvolvedID;
    var xmlReturnData = WSExecuteSpAsXML("uspVehicleInvolvedGetDetailWSXML", params);

    var obj = $("#" + sender.id.replace(senderclientID, "hidInvolvedSysLastUpdatedDate"));
    if (obj != null) {
        obj[0].value = $(xmlReturnData).find('Involved').attr('SysLastUpdatedDate');
        obj[0].defaultValue = $(xmlReturnData).find('Involved').attr('SysLastUpdatedDate');
    }


    var obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedNameTitle"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('NameTitle');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedNameFirst"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('NameFirst');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedNameLast"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('NameLast');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedBusinessName"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('BusinessName');

    //ddlInvolvedBusinessType   need to implement reset logic
    var obj = $("#" + sender.id.replace(senderclientID, "ddlInvolvedBusinessType"));
    if (obj != null)
        obj.val($(xmlReturnData).find('Involved').attr('BusinessTypeCD'));

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedAddress1"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('Address1');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedAddress2"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('Address2');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedAddressCity"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('AddressCity');

    //ddlInvolvedState need to implement reset logic
    var obj = $("#" + sender.id.replace(senderclientID, "ddlInvolvedState"));
    if (obj != null)
        obj.val($(xmlReturnData).find('Involved').attr('AddressState'));

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedAddressZip"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('AddressZip');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedEmailAddress"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('EmailAddress');

    //intialize the checkbox values 
    obj = $("#" + sender.id.replace(senderclientID, "cbInsured"));
    if (obj != null)
        obj[0].checked = false;

    //intialize the checkbox values 
    obj = $("#" + sender.id.replace(senderclientID, "cbClaimant"));
    if (obj != null)
        obj[0].checked = false;

    obj = $("#" + sender.id.replace(senderclientID, "cbOwner"));
    if (obj != null)
        obj[0].checked = false;

    obj = $("#" + sender.id.replace(senderclientID, "cbDriver"));
    if (obj != null)
        obj[0].checked = false;

    obj = $("#" + sender.id.replace(senderclientID, "cbPassenger"));
    if (obj != null)
        obj[0].checked = false;


    //cbInsured
    $(xmlReturnData).find('InvolvedType').each(function () {
        var attribData = $(this);
        var InvolvedTypename = attribData.attr('InvolvedTypeName')
        var chkboxID = '';

        switch (InvolvedTypename) {
            case 'Insured':
                chkboxID = 'cbInsured';
                break;
            case 'Claimant':
                chkboxID = 'cbClaimant';
                break;
            case 'Owner':
                chkboxID = 'cbOwner';
                break;
            case 'Driver':
                chkboxID = 'cbDriver';
                break;
            case 'Passenger':
                chkboxID = 'cbPassenger';
                break;
        }

        if (chkboxID != "") {
            obj = $("#" + sender.id.replace(senderclientID, chkboxID));
            if (obj != null)
                obj[0].checked = true;
        }

    })

    //ddlInvolvedGender need to implement reset logic
    //ddlInvolvedGender
    var obj = $("#" + sender.id.replace(senderclientID, "ddlInvolvedGender"));
    if (obj != null)
        obj.val($(xmlReturnData).find('Involved').attr('GenderCD'));

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedFedTaxId"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('FedTaxId');

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedBirthDate"));
    if (obj != null) {
        var datestring = $(xmlReturnData).find('Involved').attr('BirthDate');
        if ($.trim(datestring) != "") {
            if (datestring.indexOf("T") != -1)
                datestring = datestring.split("T");
            else if (datestring.indexOf(" ") != -1)
                datestring = datestring.split(" ");

            datestring = datestring[0].split("-");
            datestring = datestring[1] + "/" + datestring[2] + "/" + datestring[0]
        }
        obj[0].value = datestring;
    }
    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedAge"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('Age');

    //ddlBestNumberCall need to implement reset logic
    obj = $("#" + sender.id.replace(senderclientID, "ddlBestNumberCall"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('BestContactPhoneCD');


    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedBestTimeToCall"));
    if (obj != null)
        obj[0].value = $(xmlReturnData).find('Involved').attr('BestContactTime');


    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedPhoneDay"));
    if (obj != null) {
        obj[0].value = "";
        if (/^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('DayAreaCode')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('DayExchangeNumber')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('DayUnitNumber')) != false)
            obj[0].value = "(" + $(xmlReturnData).find('Involved').attr('DayAreaCode') + ") " + $(xmlReturnData).find('Involved').attr('DayExchangeNumber') + " - " + $(xmlReturnData).find('Involved').attr('DayUnitNumber'); // + " x " + $(xmlReturnData).find('Involved').attr('DayExtensionNumber');
    }

    obj = $("#" + sender.id.replace(senderclientID, "txtInvolvedPhoneNight"));
    if (obj != null) {
        obj[0].value = "";
        if (/^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('NightAreaCode')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('NightExchangeNumber')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('NightUnitNumber')) != false)
            obj[0].value = "(" + $(xmlReturnData).find('Involved').attr('NightAreaCode') + ") " + $(xmlReturnData).find('Involved').attr('NightExchangeNumber') + " - " + $(xmlReturnData).find('Involved').attr('NightUnitNumber'); // + " x " + $(xmlReturnData).find('Involved').attr('NightExtensionNumber');
    }

    obj = $("#" + sender.id.replace(senderclientID, "txtAlt"));
    if (obj != null) {
        obj[0].value = "";
        if (/^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('AlternateAreaCode')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('AlternateExchangeNumber')) != false && /^[0-9]*$/.test($(xmlReturnData).find('Involved').attr('AlternateUnitNumber')) != false)
            obj[0].value = "(" + $(xmlReturnData).find('Involved').attr('AlternateAreaCode') + ") " + $(xmlReturnData).find('Involved').attr('AlternateExchangeNumber') + " - " + $(xmlReturnData).find('Involved').attr('AlternateUnitNumber'); // + " x " + $(xmlReturnData).find('Involved').attr('AlternateExtensionNumber');
    }
}

function selectInvolved(sender, senderclientID) {
    var obj = $("#" + sender.id);
    if (obj[0].value == -1) {
        AddInvolved(sender, senderclientID);
    }
    else {
        GetInvolvedData(sender, senderclientID, obj[0].value);
    }
}



function SaveInvolved(sender, senderclientID) {
    var obj;
    var bInvolvedSelected = false;

    obj = $("#" + sender.id.replace(senderclientID, "cbInsured"));
    if (obj != null) {
        if (obj[0].checked)
            bInvolvedSelected = true;
    }
    obj = $("#" + sender.id.replace(senderclientID, "cbClaimant"));
    if (obj != null) {
        if (obj[0].checked)
            bInvolvedSelected = true;
    }
    obj = $("#" + sender.id.replace(senderclientID, "cbOwner"));
    if (obj != null) {
        if (obj[0].checked)
            bInvolvedSelected = true;
    }
    obj = $("#" + sender.id.replace(senderclientID, "cbDriver"));
    if (obj != null) {
        if (obj[0].checked)
            bInvolvedSelected = true;
    }
    obj = $("#" + sender.id.replace(senderclientID, "cbPassenger"));
    if (obj != null) {
        if (obj[0].checked)
            bInvolvedSelected = true;
    }
    if (bInvolvedSelected == false) {
        alert("Please select an Involved type and try again.");
    }

    return bInvolvedSelected;
}

function delInvolved(sender, senderclientID) {

    var involvedname;
    obj = $("#" + sender.id.replace(senderclientID, "ddlInvolved"));
    involvedname = $("#" + sender.id.replace(senderclientID, "ddlInvolved") + " option:selected").text();

    if (obj[0].value == -1 || obj[0].value <= 0) {

        return false;
    }

    if (involvedname.indexOf("Insured") != -1) {
        alert("Can not delete the Insured Involved!");
        return false;
    }

    var strRet = confirm("Confirm Delete", "Do you want to delete the selected Involved?");
    switch (strRet) {
        case true:
            //This will execute server side onclick event
            return true;
            break;
        case false:
            //execution stops client side itself
            return false;
            break;
        default:
            //execution stops client side itself
            return false;
            break;
    }
}

function showAssignmentProfile(sender, senderclientID) {

    var strInsuranceCompanyID, strInsuranceCompanyName, strState, strCity, strCounty, strShopLocationID, strServiceChannelCD;

    strInsuranceCompanyID = $("#hidInsuranceCompanyID").val()
    obj = $("#" + sender.id.replace(senderclientID, "hidInsuranceCompanyName"));
    if (obj != null)
        strInsuranceCompanyName = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidShopLocationID"));
    if (obj != null)
        strShopLocationID = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidShopLocationCity"));
    if (obj != null)
        strCity = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidShopLocationCounty"));
    if (obj != null)
        strCounty = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidShopLocationState"));
    if (obj != null)
        strState = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidServiceChannelCD"));
    if (obj != null)
        strServiceChannelCD = obj[0].value;


    winException = window.open("exceptionreferenceNew.htm?InsuranceCompanyID=" + strInsuranceCompanyID + "&InsuranceCompanyName=" + strInsuranceCompanyName + "&State=" + strState + "&County=" + strCounty + "&City=" + strCity + "&ShopLocationID=" + strShopLocationID + "&ServiceChannelCD=" + strServiceChannelCD, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=265px,height=740px,center=yes;");
    // winException.CharacterData = args;

}

function viewAssignmentHistory(sender, senderclientID) {
    obj = $("#" + sender.id.replace(senderclientID, "divAssignmentHistory"));
    if (obj != null)
        obj.dialog();
    // LoadModalDiv();
    return false;
}

function closeAssignmentHistory(sender, senderclientID) {
    obj = $("#" + sender.id.replace(senderclientID, "divAssignmentHistory"));
    if (obj != null) {
        obj.dialog('close');
        //HideModalDiv();
    }

    return false;
}

function updateVIN(sender, senderclientID) {
    var objVINEst = $("#" + sender.id.replace(senderclientID, "txtVINEst"));
    var objVIN = $("#" + sender.id.replace(senderclientID, "txtVIN"));
    if (objVINEst != null && objVIN != null) {
        objVIN[0].value = objVINEst[0].value;
    }
    return false;
}

function AddAssignmentRemarks(sender, senderclientID) {
    obj = $("#" + sender.id.replace(senderclientID, "divAssignmentHistory"));
    if (obj != null)
        obj.dialog();
    // divMaskAll.style.display = "inline";
    return false;
}

function openMoreRemarks(sender, senderclientID) {

    var strClaimAspectServiceChannelID, strInsuranceCompanyID, strParentRemarksID;

    var strInsuranceCompanyID = $("#hidInsuranceCompanyID").val();

    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelID"));
    if (obj != null)
        strClaimAspectServiceChannelID = obj[0].value;

    strParentRemarksID = sender.id.replace(senderclientID, "txtAssignmentRemarks");

    var strRetVal = window.open("AssignmentRemarks.aspx?InsuranceCompanyID=" + strInsuranceCompanyID + "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + "&RemarksTextID=" + strParentRemarksID, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=480px,height=220px,left = 490,top=300,center=yes;");
    LoadModalDiv();

    // if (strRetVal == null || strRetVal == 'cancel')
    return false;

}
function AddConcession(sender, senderclientID) {

    var strClaimAspectServiceChannelID = "", strUserID = "";

    strUserID = $("#hidUserID").val()


    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelID"));
    if (obj != null)
        strClaimAspectServiceChannelID = obj[0].value;


    var strRetVal = window.open("AssignmentConcession.aspx?ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + "&UserID=" + strUserID, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=480px,height=220px,left = 490,top=300,center=yes;"); LoadModalDiv();

    // if (strRetVal == null || strRetVal == 'cancel')
    return false;

}

function EditConcession(sender, senderclientID) {

    var strRetVal = "", strClaimAspectServiceChannelID = "", strClaimAspectServiceChannelConcessionID = "", strClaimAspectID = "", strUserID = "", strInsuranceCompanyID = "";

    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelID"));
    if (obj != null)
        strClaimAspectServiceChannelID = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelConcessionID"));
    if (obj != null)
        strClaimAspectServiceChannelConcessionID = obj[0].value;

    //hidClaimAspectID
    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectID"));
    if (obj != null)
        strClaimAspectID = obj[0].value;


    strUserID = $("#hidUserID").val()
    strInsuranceCompanyID = $("#hidInsuranceCompanyID").val();

    strRetVal = window.open("AssignmentConcession.aspx?ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + "&ClaimAspectServiceChannelConcessionID=" + strClaimAspectServiceChannelConcessionID + "&ClaimAspectID=" + strClaimAspectID + "&InsuranceCompanyID=" + strInsuranceCompanyID + "&UserID=" + strUserID, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=480px,height=220px,left = 490,top=300,center=yes;");
    LoadModalDiv();

    // if (strRetVal == null || strRetVal == 'cancel')
    return false;

}

function setEditConcession(sender, senderclientID, sClaimAspectServiceChannelConcessionID) {
    var objSender = sender;
    for (var i = 0; objSender.previousSibling != null; i++) {
        objSender = objSender.previousSibling;
    }
    for (var i = 0; objSender != null; i++) {
        $(objSender.nextSibling).css("background", "");
        if (i % 2 != 1) { $(objSender.nextSibling).css("background", "#E7E7FF"); }
        objSender = objSender.nextSibling;
    }
    $(sender).css("cssText", "background: #ffff00 !important;");

    var obj = $("#" + senderclientID + "_hidClaimAspectServiceChannelConcessionID");
    if (obj != null) {
        obj[0].value = sClaimAspectServiceChannelConcessionID;
        obj[0].defaultValue = sClaimAspectServiceChannelConcessionID;
    }
    return false;

}

function dblClickEditConcession(sender, senderclientID, strClaimAspectServiceChannelConcessionID, strClaimAspectServiceChannelID, strClaimAspectID, strUserID, strInsuranceCompanyID) {
    var strRetVal = window.open("AssignmentConcession.aspx?ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + "&ClaimAspectServiceChannelConcessionID=" + strClaimAspectServiceChannelConcessionID + "&ClaimAspectID=" + strClaimAspectID + "&InsuranceCompanyID=" + strInsuranceCompanyID + "&UserID=" + strUserID, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=480px,height=220px,left = 490,top=300,center=yes;");
    LoadModalDiv();
    return false;
}

function doConcessionDelete(sender, senderclientID) {
    var strClaimAspectServiceChannelConcessionID;
    var obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelConcessionID"));
    if (obj != null) {

        strClaimAspectServiceChannelConcessionID = obj[0].value;
    }

    if (strClaimAspectServiceChannelConcessionID == "")
        alert("Please select concession!!!");
    else {
        var strRet = confirm("Confirm Delete", "Do you want to delete the selected Concession?");
        switch (strRet) {
            case true:
                //This will execute server side onclick event
                return true;
                break;
            case false:
                //execution stops client side itself
                return false;
                break;
            default:
                //execution stops client side itself
                return false;
                break;
        }
    }
}

function togglePinned(sender, senderclientID) {

    var strRetVal = "", strWindowID = "", strLynxID = "", strClaimAspectID = "", strInsuranceCompanyID = "", strClaimAspectServiceChannelID = "", strVehNum = "";


    strWindowID = $("#hidWindowID").val();
    strInsuranceCompanyID = $("#hidInsuranceCompanyID").val();
    strLynxID = $("#hidLynxID").val();

    //hidClaimAspectID
    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectID"));
    if (obj != null)
        strClaimAspectID = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectServiceChannelID"));
    if (obj != null)
        strClaimAspectServiceChannelID = obj[0].value;

    obj = $("#" + sender.id.replace(senderclientID, "hidVehNumber"));
    if (obj != null)
        strVehNum = obj[0].value;

    var sURL = "Documents.asp?WindowID=" + strWindowID +
               "&InsuranceID=" + strInsuranceCompanyID +
               "&LynxID=" + strLynxID +
               "&ClaimAspectID=" + strClaimAspectID +
               "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
               "&EntityName=Vehicle" +
               "&EntityNumber=" + strVehNum +
               "&viewMode=2";

    strRetVal = window.open(sURL, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800px,height=600px,left = 50,top=50,center=yes;");

    return false;

    //if (curDocShown)
    //    sURL += "&selectThumbnail=" + curDocShown.getAttribute("DocumentID");


    //if (newWin != null)
    //    newWin.close();
    //newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
    //newWin.opener = window.self;

}

function displayImage(strimagelocation, strBundleDocumentName, IsArchived) {
    var strURL = "", strRetVal = "";
   

    if (strBundleDocumentName != "Estimate EMS") {
        strURL = "EstimateDocView.asp?docPath=" + strimagelocation
        strRetVal = window.open(strURL, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800px,height=600px,left = 50,top=50,center=yes;");
    }
    else {
        alert("This file type cannot be displayed in APD.");
    }

    return false;

}

function shareDocument(sender, senderclientID, strdocumentID) {

    var stApprovedFlag = false;
    var sProc = "uspShareDocument";;
    var obj;

    obj = $("#" + sender.id);
    if (obj != null)
        stApprovedFlag = obj[0].checked;

    var params = "@DocumentID=" + strdocumentID + ",@ApprovedFlag=" + stApprovedFlag;

    var sReturnData = WSExecuteSp(sProc, params);

    return false;

}

function ShowEstimate(docId, showTotals) {
    var strWindowID = "", strLynxID = "";

    if ($("#hidWindowID") != null)
        strWindowID = $("#hidWindowID").val()

    if ($("#hidLynxID") != null)
        strLynxID = $("#hidLynxID").val();

    var sURL = "Estimates.asp?WindowID=" + strWindowID + "&DocumentID=" + docId + "&showTotals=" + showTotals;
    var winName = "EstimateFull_" + strLynxID;
    // strRetVal = window.open(sURL, "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800px,height=600px,left = 50,top=50,center=yes;");
    var sWinEstimates = window.open(sURL, winName, "Height=523px, Width=728px, Top=90px, Left=40px, resizable=Yes, status=No, menubar=No, titlebar=No");
    parent.gAryOpenedWindows.push(sWinEstimates);
    sWinEstimates.focus();
    return false;
}


function ShowShortEstimate(strDocumentID, strImageLocation, strClaimAspectID, strLynxID, strInsuranceCompanyID, strUserID) {
    if (strDocumentID != "") {
        var sURL = "DocumentEstDetails.asp?EstimateUpd=1&ClaimAspectID=" + strClaimAspectID + "&DocumentID=" + strDocumentID + "&UserID=" + strUserID + "&ImageLocation=" + escape(strImageLocation) + "&LynxId=" + strLynxID + "&InsuranceCompanyID=" + strInsuranceCompanyID;
        var strReturn = window.open(sURL, "", "dialogHeight:608px; dialogWidth:860px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
    }
}

function showWarrantyDetails(sender, SendeID, AssignmentID, ShopName, ShopContact, ShopPhone, ShopFax, AssignmentStatus, FaxStatus, ElecStatus, Disposition, WorkStatus, AssignmentDate, Remarks,
    WarrantyStartDate, WarrantyStartConfirmFlag, WarrantyEndDate, WarrantyEndConfirmFlag) {

    var obj;
    //Row highlighter
    var objSender = sender;
    for (var i = 0; objSender.previousSibling != null; i++) {
        objSender = objSender.previousSibling;
    }
    for (var i = 0; objSender != null; i++) {
        $(objSender.nextSibling).css("background", "");
        if (i % 2 != 1) { $(objSender.nextSibling).css("background", "#E7E7FF"); }
        objSender = objSender.nextSibling;
    }
    $(sender).css("cssText", "background: #ffff00 !important;");

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarShopName"));
    if (obj != null)
        obj[0].value = ShopName;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarContactName"));
    if (obj != null)
        obj[0].value = ShopContact;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarMSA"));
    if (obj != null)
        obj[0].value = "";

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarShopPhone"));
    if (obj != null)
        obj[0].value = ShopPhone;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarRepairStartDate"));
    if (obj != null)
        obj[0].value = WarrantyStartDate;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarShopFax"));
    if (obj != null)
        obj[0].value = ShopFax;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarRepairEndDate"));
    if (obj != null)
        obj[0].value = WarrantyEndDate;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarFaxStatus"));
    if (obj != null)
        obj[0].value = FaxStatus;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarElecStatus"));
    if (obj != null)
        obj[0].value = ElecStatus;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarCurAssgDate"));
    if (obj != null)
        obj[0].value = AssignmentDate;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarPrevAssgDate"));
    if (obj != null)
        obj[0].value = "";

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarWorkStatus"));
    if (obj != null)
        obj[0].value = WorkStatus;

    obj = $("#" + SendeID.replace("gvWarAssignment", "txtWarRemarks"));
    if (obj != null)
        obj[0].value = Remarks;

    obj = $("#" + SendeID.replace("gvWarAssignment", "hidWarAssignmentID"));
    if (obj != null)
        obj[0].value = AssignmentID;

    $("[id$=txtWarShopPhone]").mask("(000)000-0000");
    $("[id$=txtWarShopPhone]").mask("(999)999-9999");
    $("[id$=txtWarShopFax]").mask("(000)000-0000");
    $("[id$=txtWarShopFax]").mask("(999)999-9999");

    $("[id$=txtWarCurAssgDate]").mask("00/00/0000");
    $("[id$=txtWarCurAssgDate]").mask("99/99/9999");
    $("[id$=txtWarPrevAssgDate]").mask("00/00/0000");
    $("[id$=txtWarPrevAssgDate]").mask("99/99/9999");
    $("[id$=txtWarRepairStartDate]").mask("00/00/0000");
    $("[id$=txtWarRepairStartDate]").mask("99/99/9999");
    $("[id$=txtWarRepairEndDate]").mask("00/00/0000");
    $("[id$=txtWarRepairEndDate]").mask("99/99/9999");


    $("#" + SendeID.replace("gvWarAssignment", "btnWarSendAssignment")).removeAttr('disabled', 'disabled');
    $("#" + SendeID.replace("gvWarAssignment", "btnWarCancelAssignment")).removeAttr('disabled', 'disabled');
    $("#" + SendeID.replace("gvWarAssignment", "ddlWarDisposition")).removeAttr('disabled', 'disabled');
    return false;
}

function sendWarrantyAssignment(sender, senderclientID) {

    var strAssignmentID = "", strUserID = "";

    var obj;

    strUserID = $("#hidUserID").val()

    obj = $("#" + sender.id.replace(senderclientID, "hidWarAssignmentID"));
    if (obj != null)
        strAssignmentID = obj[0].value;

    sRequest = "<Root><Assignment assignmentID=\"" + strAssignmentID + "\" userID=\"" + strUserID + "\" staffAppraiser=\"false\" warrantyAssignment=\"true\"/></Root>";
    var objRet = XMLServerExecute("AssignShop.asp", sRequest);
    if (objRet.code == 0) {
        var strRet;
        if (objRet.xml) {
            var oResult = objRet.xml.selectSingleNode("/Root/Success");
            if (oResult) {
                var strStatus = oResult.text;
                if (strStatus.substring(0, 1) == "F") ClientInfo("Electronic assignment to shop failed.");
                if (strStatus.substring(1, 1) == "F") ClientInfo("Fax assignment to shop failed.");

                //document.location.reload();
                window.location.href = "/ClaimVehicleWarranty.htm";
            } else {
                ClientWarning("1: Assignment Failed");
            }
        } else {
            ClientWarning("2: Assignment failed.");
        }
    } else {
        ClientWarning("3: Assignment Failed.");
    }

    return false;
}

function selectShopWarranty(sender, senderclientID) {
    var sShopRemarks = escape($("#" + sender.id.replace(senderclientID, "txtWarRemarks")).val());
    var sZIP = sCTY = sST = "";
    var strLocationPhone = $("#" + sender.id.replace(senderclientID, "txtWarShopPhone")).val();
    var sAC = strLocationPhone.substring(1, 4);
    var sExchange = strLocationPhone.substring(5, 8);
    var obj;

    obj = $("#" + sender.id.replace(senderclientID, "txtLocationZip"))
    if (obj != null)
        sZip = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "txtLocationCity"))
    if (obj != null)
        var sCity = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "ddlLocationState"))
    if (obj != null)
        var sState = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "hidUserID"))
    if (obj != null)
        var sUserID = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "hidLynxID"))
    if (obj != null)
        var sLynxID = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "hidVehNumber"))
    if (obj != null)
        var sVehNum = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "hidVehicleClaimAspectID"))
    if (obj != null)
        var sClaimAspectID = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "hidInsuranceCompanyID"))
    if (obj != null)
        var sInsuranceCompanyID = obj.val();

    obj = $("#" + sender.id.replace(senderclientID, "txtLocationName"))
    if (obj != null)
        var sLocationName = obj.val();

    //if ((parent.lCTY).Trim() != "") {
    //    sZIP = parent.lZIP; sCTY = parent.lCTY; sST = parent.lST;
    //} else if ((parent.lZIP).Trim() != "") {
    //    sZIP = parent.lZIP; sCTY = parent.lCTY; sST = parent.lST;
    //}

    var sAssignmentTo = "SHOP";


    var strURL = "ShopLookup.asp?AreaCode=" + sAC +
                    "&ExchangeNumber=" + sExchange + "&ZipCode=" + (isNaN(sZip) ? "" : sZip) + "&City=" + (sCity == "" ? "" : sCity) + "&State=" + sState +
                    "&UserID=" + sUserID + "&LynxID=" + sLynxID + "&VehicleNumber=" + sVehNum +
                    "&ClaimAspectID=" + sClaimAspectID + "&InsuranceCompanyID=" + sInsuranceCompanyID +
                    "&LocationName=" + sLocationName + "&AssignmentTo=" + sAssignmentTo +
                    "&getUser=false" + "&Warranty=1&ShopRemarks=" + sShopRemarks;

    var strWindow = window.open(strURL, "ModalPopUp", "titlebar=no,status=no,location=no,toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=no,width=720px,height=400px,left = 120,top=100,center=yes;")
    $(strWindow).on("beforeunload", function () {
        activateReload();
    });
    return false;
}

function activateReload() {
    setTimeout(function () {
        window.location.href = window.location.href;
    }, 1);
}

//-----------------------------------//
//Coverage popup for ADD/Edit and delete
//-----------------------------------//

function setCoverageValue(sender, senderClientID, sSelectedClaimCoverageID, sCoverageSysLastUpdatedDate, sSelectedClientCoverageTypeID) {
    var objSender = sender;
    for (var i = 0; objSender.previousSibling != null; i++) {
        objSender = objSender.previousSibling;
    }
    for (var i = 0; objSender != null; i++) {
        $(objSender.nextSibling).css("background", "");
        if (i % 2 != 1) { $(objSender.nextSibling).css("background", "#E7E7FF"); }
        objSender = objSender.nextSibling;
    }
    $(sender).css("cssText", "background: #ffff00 !important;");
    var obj = $("#hidSelectedClaimCoverageID");
    if (obj != null) {
        obj[0].value = sSelectedClaimCoverageID;
        obj[0].defaultValue = sSelectedClaimCoverageID;
    }
    var obj = $("#hidSelectedClientCoverageTypeID");
    if (obj != null) {
        obj[0].value = sSelectedClientCoverageTypeID;
        obj[0].defaultValue = sSelectedClientCoverageTypeID;
    }
    var obj = $("#hidCoverageSysLastUpdatedDate");
    if (obj != null) {
        obj[0].value = sCoverageSysLastUpdatedDate;
        obj[0].defaultValue = sCoverageSysLastUpdatedDate;
    }
}

function dblClicksetCoverageValue(sender, senderCleintID, sSelectedClaimCoverageID, sClientCoverageTypeID, sCoverageSysLastUpdatedDate, sDescription, sCoverageTypeCD, sAddtlCoverageFlag, sDeductibleAmt, sLimitAmt, sMaximumDays, sLimitDailyAmt) {
    var slynxID = $('#hidLynxID').val();
    var sInsCompID = $('#hidInsuranceCompanyID').val();
    var sUserID = $('#hidUserID').val();
    var strParams = '?LynxID=' + slynxID + '&InscCompID=' + sInsCompID + '&UserID=' + sUserID + '&SelectedClaimCoverageID=' + sSelectedClaimCoverageID + '&CoverageSysLastUpdatedDate=' + sCoverageSysLastUpdatedDate + '&ClientCoverageTypeID=' + sClientCoverageTypeID + '&Description=' + sDescription + '&ClientCode=' + sCoverageTypeCD + '&Additional=' + sAddtlCoverageFlag + '&Deductable=' + sDeductibleAmt + '&Limit=' + sLimitAmt + '&MaxDays=' + sMaximumDays + '&DayLimit=' + sLimitDailyAmt;
    doSavCoverage("SavCoverage.aspx", strParams, "ModalPopUp", "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=350px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;");
}

function doSavCoverage(strPage, strParams, strWindowStyle, strWindowParams) {
    var strCRUD = $("#hidClaimCRUD").val();
    //var strCoverageID = parseUrlParameter(strParams, 'Params');
    var strCoverageSelected = strWindowParams //$("#hidCoverageSelected").val();

    if (strCRUD.indexOf("U") == -1) {
        ClientWarning("You do not have permission to edit coverage. Please contact your supervisor.");
        return;
    }

    //if (strParams.length == 0) {
    //    //-- No row was selected --//
    //    ClientWarning("Please select the coverage description from the list.");
    //    return false;
    //}

    OpenDialogWindow(strPage + strParams, strWindowStyle, strWindowParams);
}
function doAddCoverage(strPage, strParams, strWindowStyle, strWindowParams) {
    var strCRUD = $("#hidClaimCRUD").val();
    if (strCRUD.indexOf("C") == -1) {
        ClientWarning("You do not have permission to add coverage. Please contact your supervisor.");
        return;
    }

    OpenDialogWindow(strPage + strParams, strWindowStyle, strWindowParams);
}

function doDelCoverage(strPage, strParams, strWindowStyle, strWindowParams) {
    var strCRUD = $("#hidClaimCRUD").val();
    var SelectedClaimCoverageID = $("#hidSelectedClientCoverageTypeID").val();
    var SelectedClientCoverageTypeID = $("#hidSelectedClaimCoverageID").val();
    var CoverageSysLastUpdatedDate = $("#hidCoverageSysLastUpdatedDate").val();
    if (strCRUD.indexOf("D") == -1) {
        alert("You do not have permission to delete coverage. Please contact your supervisor.");
        return;
    }

    if ((SelectedClaimCoverageID == 0 || SelectedClaimCoverageID == "") && (SelectedClientCoverageTypeID == 0 || SelectedClientCoverageTypeID == "")) {
        alert("Please select the coverage description from the list.");
        return false;
    }
    else {
        OpenDialogWindow(strPage + strParams + '&SelectedClaimCoverageID=' + SelectedClaimCoverageID + '&SelectedClientCoverageTypeID=' + SelectedClientCoverageTypeID + '&CoverageSysLastUpdatedDate=' + CoverageSysLastUpdatedDate, strWindowStyle, strWindowParams);
    }
}

var currentImpactSender;
var currentImpactSenderClientId = "";
var xmlReturnDataImpact;

function doImpact(sender, senderClientId, strPage, strDamage, strWindowStyle, strWindowParams) {
    currentImpactSender = sender;
    currentImpactSenderClientId = senderClientId;
    var strImpactDetails = $("#" + sender.id.replace(senderClientId, "txtImpact")).val() + "|" + $("#" + sender.id.replace(senderClientId, "txtPriorImpact")).val() + "|" + strDamage;
    OpenDialogWindow(strPage + "?" + strImpactDetails, strWindowStyle, strWindowParams);
    return false;
}

function getImpactData(strReturn) {
    HideModalDiv();
    if (strReturn && strReturn != "") {
        var sArray = strReturn.split("|");
        ShowImageImpact(sArray[0], sArray[1]);
    }
}

function ShowImageImpact(sAreaImpact, sPrevAreaImpact) {
    $("#" + currentImpactSender.id.replace(currentImpactSenderClientId, "txtImpact")).val(sAreaImpact.substr(0, (sAreaImpact.length - 2)));
    $("#" + currentImpactSender.id.replace(currentImpactSenderClientId, "txtPriorImpact")).val(sPrevAreaImpact.substr(0, (sPrevAreaImpact.length - 2)));
}

function getImpactNodes(sender, senderClientId) {
    var finalImpactPoints = "";
    var finalPriorImpactPoints = "";
    var strName = "";
    var strXpath = "";
    var sImpactPoints = $("#" + sender.id.replace(senderClientId, "txtImpact")).val();
    var sPriorImpactPoints = $("#" + sender.id.replace(senderClientId, "txtPriorImpact")).val();
    var sCliamAspectId = $("#" + sender.id.replace(senderClientId, "hidVehicleClaimAspectID")).val();
    sImpactPoints = sImpactPoints.split(",");
    sPriorImpactPoints = sPriorImpactPoints.split(",");
    var params = "@ClaimAspectId=" + sCliamAspectId + ",@InsuranceCompanyID=" + $("#hidInsuranceCompanyID").val();
    xmlReturnDataImpact = WSExecuteSpAsXML("uspClaimVehicleGetDetailWSXML", params);

    for (var impactCount = 0; impactCount < sImpactPoints.length; impactCount++) {

        if (($.trim(sImpactPoints[impactCount])).indexOf("(Primary)") != -1) {
            strName = $.trim($.trim(sImpactPoints[impactCount]).replace("(Primary)", ""))
        }
        else {
            strName = $.trim(sImpactPoints[impactCount]);
        }
        strXpath = '//Reference[@List="Impact"][@Name="' + strName + '"]/@ReferenceID';
        if (finalImpactPoints == "")
            finalImpactPoints = xmlReturnDataImpact.documentElement.selectSingleNode(strXpath).value;
        else
            finalImpactPoints = finalImpactPoints + "," + xmlReturnDataImpact.documentElement.selectSingleNode(strXpath).value;
    }

    for (var priorImpactCount = 0; priorImpactCount < sPriorImpactPoints.length; priorImpactCount++) {
        strName = $.trim(sPriorImpactPoints[priorImpactCount]);
        strXpath = '//Reference[@List="Impact"][@Name="' + strName + '"]/@ReferenceID';
        if (finalPriorImpactPoints == "" || finalImpactPoints == undefined)
            finalPriorImpactPoints = xmlReturnDataImpact.documentElement.selectSingleNode(strXpath).value;
        else
            finalPriorImpactPoints = finalPriorImpactPoints + "," + xmlReturnDataImpact.documentElement.selectSingleNode(strXpath).value;
    }
    obj = $("#" + sender.id.replace(senderClientId, "hidImpactPoints"));
    if (obj != null) {
        obj[0].value = finalImpactPoints;
    }

    obj = $("#" + sender.id.replace(senderClientId, "hidPriorImpactPoints"));
    if (obj != null) {
        obj[0].value = finalPriorImpactPoints;
    }
}


function callRental(sender, senderclientID) {

    var strPage = "RentalCoverage.Aspx";

    var slynxID = $('#hidLynxID').val();
    var sInsCompID = $('#hidInsuranceCompanyID').val();
    var sUserID = $('#hidUserID').val();
    var strClaimAspectID = "";

    obj = $("#" + sender.id.replace(senderclientID, "hidClaimAspectID"));
    if (obj != null)
        strClaimAspectID = obj[0].value;

    var strQuery = "LynxID=" + slynxID + "&InscCompID=" + sInsCompID + "&ClaimAspectID=" + strClaimAspectID + "&UserID=" + sUserID;

    OpenDialogWindow(strPage + "?" + strQuery, "", "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=1050px,height=600px,left = 100,top=10,center=1,help=0,scroll=0,status=0;");
    // strRetVal = window.showModalDialog("RentalMangement.htm","dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;" );

    return false;
}


//-----------------------------------//
// Save Assignment Validation
//-----------------------------------//

var repairReasonFlag = false;

function saveAssignment(sender, senderID) {
    var strServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"))[0].value;
    var oRepairStartDate = $("#" + sender.id.replace(senderID, "txtWorkStartDate"));
    var oRepairEndDate = $("#" + sender.id.replace(senderID, "txtWorkEndDate"));
    var strRepairStartDate = $("#" + sender.id.replace(senderID, "hidWorkStartDate"))[0].value;
    var strRepairEndDate = $("#" + sender.id.replace(senderID, "hidWorkEndDate"))[0].value;
    var strLossDate = $("#hidLoss").val();
    var strhidval = sender.id.replace(senderID, "hidRepairScheduleChangeReason");
    var strRepairScheduleChangeReason = $("#" + sender.id.replace(senderID, "hidRepairScheduleChangeReason"))[0].value;
    var strClaimAspectServiceChannelID = $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID"))[0].value;
    var strInsurenceCompanyID = $("#" + sender.id.replace(senderID, "hidInsuranceCompanyID"))[0].value;
    var strRepairEndDate = "";

    if (strServiceChannelCD == "GL" ||
        strServiceChannelCD == "ME" ||
        strServiceChannelCD == "CS" ||
        strServiceChannelCD == "PS") {
        if (oRepairStartDate[0].value == "") {
            alert("Repair start date not specified. Please try again.");
            return false;
        }
        if (oRepairStartDate[0].value != "" && oRepairEndDate[0].value != "") {
            var dtStart = new Date(oRepairStartDate[0].value);
            var dtEnd = new Date(oRepairEndDate[0].value);
            if (dtEnd < dtStart) {
                alert("Repair end date cannot be before the repair start date. Please try again.");
                return false;
            }
        }

        if (isDate(oRepairStartDate[0].value) == true || isDate(oRepairEndDate[0].value) == true) {
            var dtOldStart = new Date(strRepairStartDate);
            var dtOldEnd = new Date(strRepairEndDate);
            var dtNewStart = new Date(oRepairStartDate[0].value);
            var dtNewEnd = new Date(oRepairEndDate[0].value);
            var dtLoss = new Date(strLossDate);

            if (dtNewStart && (dtNewStart.setHours(23, 59, 59) < dtLoss)) {
                alert("Repair start date cannot be before the loss date.");
                return false;
            }
            if (dtNewEnd && (dtNewEnd.setHours(23, 59, 59) < dtLoss)) {
                alert("Repair end date cannot be before the loss date.");
                return false;
            }

            //if ((strRepairStartDate != "" && oRepairStartDate[0].value != dtOldStart.toString()) ||
            //(strRepairEndDate != "" && oRepairEndDate[0].value != dtOldEnd.toString())) {

            $("#curClaimAspectServiceChannelID")[0].value = $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID")).val();
            $("#curRepeaterID")[0].value = sender.id.replace(senderID, "");

            params = "@ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + ",@InsuranceCompanyID=" + strInsurenceCompanyID;
            xmlReturnData = WSExecuteSpAsXML("uspVehicleAssignGetDetailwsXML", params);
            strRepairEndDate = $(xmlReturnData).find('ServiceChannel').attr('WorkEndDate');

            if ($.trim(strRepairEndDate) != "") {
                if (strRepairEndDate.indexOf("T") != -1)
                    strRepairEndDate = strRepairEndDate.split("T");
                else if (strRepairEndDate.indexOf(" ") != -1)
                    strRepairEndDate = strRepairEndDate.split(" ");

                strRepairEndDate = strRepairEndDate[0].split("-");
                strRepairEndDate = strRepairEndDate[1] + "/" + strRepairEndDate[2] + "/" + strRepairEndDate[0]
            }

            if (strRepairEndDate == "" || strRepairEndDate == $("#" + sender.id.replace(senderID, "txtWorkEndDate")).val()) {
                return;
            }
            else {
                if (repairReasonFlag == false) {
                    var newOption;
                    isgbDirty = false;
                    params = "@ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;
                    xmlReturnData = WSExecuteSpAsXML("uspRepairSchReasonGetListWSXML", params);
                    $("#ddlRepairReason option").remove();
                    newOption = $('<option value=""></option>');
                    $('#ddlRepairReason').append(newOption);
                    $(xmlReturnData).find('Reason').each(function () {
                        var attribData = $(this);
                        var iReasonID = attribData.attr('ReasonID');
                        var iReasonName = attribData.attr('Name');
                        newOption = $('<option value="' + iReasonID + '">' + iReasonName + '</option>');
                        $('#ddlRepairReason').append(newOption);
                    });
                    $("#diaRepairChangeReason").dialog("open");
                    return false;
                }
                return;
            }
        }
    }
}

function isDate(value) {
    switch (typeof value) {
        case 'number':
            return true;
        case 'string':
            return !isNaN(Date.parse(value));
        case 'object':
            if (value instanceof Date) {
                return !isNaN(value.getTime());
            }
        default:
            return false;
    }
}


//dialog ok click reason

function doOk(sender, senderID) {
    var oDDLRepairReason = $("#" + sender.id.replace(senderID, "ddlRepairReason"));
    var oTXTOther = $("#" + sender.id.replace(senderID, "txtOther"));
    var obj;
    // var hidRepairScheduleChangeReason = $("#" + sender.id.replace(senderID, "hidRepairScheduleChangeReason"));
    // var hidRepairScheduleChangeReasonID = $("#" + sender.id.replace(senderID, "hidRepairScheduleChangeReasonID"));
    var btnSaveAssignment = $("#" + sender.id.replace(senderID, "btnSaveAssignment"));
    var params;
    var xmlReturnData;
    var strClaimAspectServiceChannelID = $("#curClaimAspectServiceChannelID").val();
    var sRet;
    var strRepeaterVal = $("#curRepeaterID").val();
    //var oDDLRepairReason = $("#ddlRepairReason");
    //var oTXTOther = $("#txtOther");

    if (oDDLRepairReason[0].selectedIndex == 0) {
        alert("Please select a reason from the list.");
        return;
    }

    if (oDDLRepairReason[0].value == "6") {
        if (oTXTOther[0].value == "") {
            alert("Please describe the Other reason and try again.");
            if (oTXTOther[0].disabled == false)
                $("#txtOther").focus();
            return;
        }
        //sRet = oDDLRepairReason[0].value + "|" + oTXTOther[0].value;
    } else {
        //sRet = oDDLRepairReason[0].value + "|" + oDDLRepairReason[0].selectedOptions[0].text;
    }
    //hidRepairScheduleChangeReason.value = oTXTOther[0].value;
    obj = $("#" + strRepeaterVal + "hidRepairScheduleChangeReasonID");
    if (obj != null) {
        obj[0].value = oDDLRepairReason.val();
        obj[0].defaultValue = oDDLRepairReason.val();
    }
    params = "@ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;
    xmlReturnData = WSExecuteSpAsXML("uspRepairSchReasonGetListWSXML", params);

    $(xmlReturnData).find('Reason').each(function () {
        var attribData = $(this);
        var iReasonID = attribData.attr('ReasonID');
        var chkboxID = '';
        if (iReasonID == oDDLRepairReason.val()) {
            obj = $("#" + strRepeaterVal + "hidRepairScheduleChangeReason");
            if (obj != null) {
                obj[0].value = attribData.attr('Name');
                obj[0].defaultValue = attribData.attr('Name');
            }
        }
    });


    $("#diaRepairChangeReason").dialog("close");
    repairReasonFlag = true;
    $("#" + strRepeaterVal + "btnSaveAssignment").trigger("click");
    return;
}

function checkOther(ddlID) {
    if ($("#" + ddlID.id).val() == "6")
        $("#txtOther").removeAttr("disabled", "disabled");
    else
        $("#txtOther").attr("disabled", "disabled");
}
function SendAssignment(sender, senderID, resendFlag, assignmentFlag) {
    var gsVehicleAssignmentSentToShopCD = '25';
    var gsAppraiserAssignmentSentEventID = '75';

    var strServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"))[0].value;
    var strAssignmentID = $("#" + sender.id.replace(senderID, "hidAssignmentID")).val();
    var strUserId = $("#hidUserID").val();
    var strClaimAspectServiceChannelID = $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID")).val();
    var varVehNum = $("#" + sender.id.replace(senderID, "hidVehNumber")).val();

    var gbCEIFlag = false;
    var gsAssignmentID1 = 0;
    var EffDedSent = $("#" + sender.id.replace(senderID, "txtAssignmentDeductSent")).val();
    if (parseFloat(EffDedSent) > 1000) {
        if (!confirm("Confirm Effective Deductible - Effective Deductible Sent value is greater than $1000. Is this value correct?")) {
            return false;
        }
    }


    if (resendFlag == 0) {
        if (assignmentFlag == 'LDAU')
            strAssignmentID = gsAssignmentID1;
        var params = "@AssignmentID=" + strAssignmentID;
        var xmlReturnData = XslGetSession("uspWorkflowAssignShop", params);
    }

    if (gbCEIFlag == true) {

        var params = "@EventID=" + gsVehicleAssignmentSentToShopCD +
                  "&@ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                  "&@Description=" + "C-PROGRAM SHOP(NP): Assignment for Vehicle " + varVehNum + " was sent to  (Shop ID " + $("#" + sender.id.replace(senderID, "hidShopID")).val() + ")" +
                  "&@ConditionValue=CEI" +
                  "&@UserID=" + strUserId;
        var xmlReturnData = XslGetSession("uspWorkflowNotifyEvent", params);

        window.location.href = window.location.href;
    } else {
        // executing the remote script in a separate thread so that the user can do other things while the assignment is being sent.
        //window.setTimeout('RSExecute( "/rs/RSAssignShop.asp", "AssignShop", gsAssignmentID, gsUserID, false, showFaxResults, showFaxErrors, null )', 100);

        var strStaffAppraiser = "false";
        if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR" || (strServiceChannelCD == "RRP" && assignmentFlag == "LDAU")) {

            strStaffAppraiser = "true";
        }

        if (assignmentFlag == "LDAU")
            strAssignmentID = gsAssignmentID1;

        sRequest = "<Root><Assignment assignmentID=\"" + strAssignmentID + "\" userID=\"" + strUserId + "\" staffAppraiser=\"" + strStaffAppraiser + "\" hqAssignment=\"" + "true" + "\" /></Root>";


        var objRet = XMLServerExecute("AssignShop.asp", sRequest);


        if (objRet.code == 0) {
            var strRet;

            if (objRet.xml) {
                var oError = objRet.xml.selectSingleNode("//Error");

                if (oError) {

                    var strMessage = oError.text;

                    var iPosExtMsg = strMessage.indexOf("External Message");
                    var strMessage = strMessage.substring(iPosExtMsg + "External Message".length + 8, strMessage.indexOf("<|", iPosExtMsg));
                    ClientWarning(strMessage);
                    return;
                } else {

                    var oResult = objRet.xml.selectSingleNode("/Root/Success");
                    if (oResult) {
                        var strStatus = oResult.text;
                        var strVanResult = strStatus.substring(0, 1);
                        var strFaxResult = strStatus.substring(1, 1);

                        if (strServiceChannelCD == "RRP" && assignmentFlag == "LDAU") {

                            if (strVanResult == "F") ClientInfo("Estimate Preload failed.");
                            if (strFaxResult == "F") ClientInfo("Fax assignment to shop failed.");
                        } else

                            if (strVanResult == "F") ClientInfo("Electronc assignment to shop failed.");
                        if (strFaxResult == "F") ClientInfo("Fax assignment to shop failed.");

                        if (strServiceChannelCD == "ME" || strServiceChannelCD == "GL" || strServiceChannelCD == "DA") {

                            var params = "@EventID=" + gsAppraiserAssignmentSentEventID +
                                "&@ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                                "&@Description=" + "Appriaser Assignment for Vehicle " + varVehNum + " was sent to " + txtAssignedTo.value +
                                "&@UserID=" + varUserID;
                            var xmlReturnData = XslGetSession("uspWorkflowNotifyEvent", params);

                        }

                        window.setTimeout("refreshPage()", 1500);
                        return false;
                    } else {
                        ClientWarning("Assignment Failed. Code 003");
                    }
                }
            } else {
                ClientWarning("Assignment failed. Code 002");
            }
        } else {
            ClientWarning("Assignment Failed. Code 001");
        }
    }
    window.setTimeout("refreshPage()", 1500);
    return false;
}
function refreshPage() {
    window.location.href = window.location.href;
}
/*
//-----------------------------------//
//Coverage popup for ADD/Edit and delete
//-----------------------------------//
//$(function () {
//    $("#diaAddCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#divRentalMax").hide();
//    $("#divRentalLimit").hide();

//    $("#btnAddCoverage").on("click", function () {
//        $("#diaAddCoverage").dialog("open");
//    });

//    $("#btnCoverageAddCancel").on("click", function () {
//        $("#diaAddCoverage").dialog("close");
//    });
//});

//-----------------------------------//
// Del Coverage 
//-----------------------------------//
//$(function () {
//    $("#diaDelCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#btnDelCoverage").on("click", function () {
//        $("#diaDelCoverage").dialog("open");
//    });

//    $("#btnCoverageDelCancel").on("click", function () {
//        $("#diaDelCoverage").dialog("close");
//    });
//});

//Edit coverage
//$(function () {
//    $("#diaEditCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#btnSavCoverage").on("click", function () {
//        $("#diaEditCoverage").dialog("open");
//    });

//    $("#btnEditCoverageCancel").on("click", function () {
//        $("#diaEditCoverage").dialog("close");
//    });
//});

//-----------------------------------//
// Add Coverage Description Changed
//-----------------------------------//
//$(function () {
//    $("#ddlCoverageDescription").change(function () {
//        var aCoverageClientCode = $("#ddlCoverageDescription").val().toUpperCase().split("|");
//        $("#HIDCoverageParams").val($("#ddlCoverageDescription").val());
//        $("#txtCoverageClientCode").val(aCoverageClientCode[0]).val().toUpperCase();
//        $("#txtCoverageType").val(aCoverageClientCode[0]).val().toUpperCase();
//        //$("#HIDCoverageTypeID").val(aCoverageClientCode[2]).val();

//        //alert(aCoverageClientCode[0]);
//        //alert(aCoverageClientCode[1]);
//        //alert(aCoverageClientCode[2]);
//        //alert(aCoverageClientCode[3]);

//        //-- Additional Flag --//
//        switch (aCoverageClientCode[3]) {
//            case "0":
//                $("#txtCoverageAdditionalFlag").val("No");
//                break;
//            case "1":
//                $("#txtCoverageAdditionalFlag").val("Yes");
//                break;
//            default:
//                $("#txtCoverageAdditionalFlag").val("No");
//        }

//        //-- Rental Area --//
//        if (aCoverageClientCode[0] == 'RENT') {
//            $("#divRentalMax").show();
//            $("#divRentalLimit").show();
//        } else {
//            $("#divRentalMax").hide();
//            $("#divRentalLimit").hide();
//        }

//    })
//})
//-----------------------------------//
// NADA - dialog
//-----------------------------------//
//$(function () {
//    $("#diaNADA").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        width: 840,
//        height: 460,
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#imgNADA").on("click", function () {
//        $("#diaNADA").dialog("open");
//    });

//    $("#imgNADACancel").on("click", function () {
//        $("#diaNADA").dialog("close");
//    });
//});

/*
 //OLD JS without JQuery

 //-------------------------------------------//
 // Get all the elements that have a class 
 // of Claim and Vehicle
 //-------------------------------------------//
 function getElementsByClassName(className) {
     var found = [];
     var elements = document.getElementsByTagName("*");
     for (var i = 0; i < elements.length; i++) {
         var names = elements[i].className.split(' ');
         for (var j = 0; j < names.length; j++) {
             if (names[j] == className) found.push(elements[i]);
         }
     }
     return found;
 }

 //-------------------------------------------//
 // Handle the screen Div changes each time
 // the tabs are clicked
 //-------------------------------------------//
 function MenuTab(evt, currentClass, tabName, tabContainer, subTabContainer, subTab) {
     var ct;

     var tablinks = getElementsByClassName(currentClass);
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }

     if (tabContainer) {
         tabcontent = getElementsByClassName(tabContainer);
         for (ct = 0; ct < tabcontent.length; ct++) {
             tabcontent[ct].style.display = "none";
         }
     }
     evt.srcElement.className += " active";

     document.getElementById(tabName).style.display = "block";
      if (subTab) {

         var subTabcontent = getElementsByClassName(subTab);

         for (ct = 0; ct < subTabcontent.length; ct++) {
             if (subTabcontent[ct].style.display === 'block') {
                 subTabcontent[ct].style.display = 'none';
             }
         }
     }
     if (subTabContainer) {
         document.getElementById(subTabContainer).style.display = "block";
         var vehDetails = document.getElementById(subTabContainer).parentNode.children[0];
         for (ct = 0; ct < vehDetails.children.length; ct++) {
             vehDetails.children[ct].className = vehDetails.children[ct].className.replace(" active", "");
         }
         vehDetails.children[0].className += " active";
     }
 }
*/