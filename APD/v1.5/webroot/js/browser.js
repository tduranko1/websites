
function lib_bwcheck()
{ //Browsercheck (needed)
	this.ver = navigator.appVersion; this.agent=navigator.userAgent
	this.dom = document.getElementById ? 1 : 0
	this.ie5 = (this.ver.indexOf("MSIE 5")>-1 && this.dom) ? 1 : 0;
	this.ie6 = (this.ver.indexOf("MSIE 6")>-1 && this.dom) ? 1 : 0;
	this.ie4= (document.all && !this.dom) ? 1 : 0;
	this.ie7=(this.ver.indexOf("MSIE 7")>-1 && this.dom && !this.opera5)?1:0;
	this.ie8=(this.ver.indexOf("MSIE 8")>-1 && this.dom && !this.opera5)?1:0;
	this.ie9=(this.ver.indexOf("MSIE 9")>-1 && this.dom && !this.opera5)?1:0;
	this.ie=this.ie4||this.ie5||this.ie6||this.ie7||this.ie8||this.ie9;
	this.mac = this.agent.indexOf("Mac")>-1
	this.opera5 = this.agent.indexOf("Opera 5")>-1
	this.ns6 = (this.dom && parseInt(this.ver) >= 5) ?1:0;
	this.ns4 = (document.layers && !this.dom)?1:0;
	this.bw = (this.ie6 || this.ie5 || this.ie4 || this.ns4 || this.ns6 || this.opera5 || this.dom)
	return this
}
var bw=new lib_bwcheck()

function checkBrowserSecuritySetting(){
  //check for Java execution enabled - used by Remote scripting
  if (!window.clientInformation.javaEnabled()) {
    if (typeof(ClientError2) == "function")
      ClientEvent2(50, "The security settings on this machine prevents execution of Java applets [Remote scripting]. This is required for APD LYNX Select. Please check browser security settings.");
    else
      alert("The security settings on this machine prevents execution of Java applets [Remote scripting]. This is required for APD LYNX Select. Please check browser security settings.");
    window.navigate("blank.asp");
    return;
  }
    
  //check for Initialize and script ActiveX not marked as safe
  var WshShell = null;
  try {
    WshShell = new ActiveXObject( "WScript.shell" );
  } catch (e) {}
    finally {
      if(!WshShell) {
        if (typeof(ClientError2) == "function")
          ClientEvent2(50, "The security settings on this machine prevents scripting of safe ActiveX components [using javascript:ActiveXObject()]. This is required for APD LYNX Select. Please check browser security settings.");
          //ClientWarning("User does not have permission to run Scripting ActiveX not marked as safe permission required. Please check the security setting.");
        else
          alert("The security settings on this machine prevents scripting of safe ActiveX components [using javascript:ActiveXObject()]. This is required for APD LYNX Select. Please check browser security settings.");

        window.navigate("blank.asp");        
        return;
      }
    }
}

function noPluginRights(){
  if (typeof(ClientError2) == "function")
    ClientEvent2(50, "The security settings on this machine prevents execution of ActiveX components or plugins [Photo Preview OCX]. This is required for APD LYNX Select. Please check browser security settings.");
  else
    alert("The security settings on this machine prevents execution of ActiveX components or plugins [Photo Preview OCX]. This is required for APD LYNX Select. Please check browser security settings.");
  //ClientWarning("User does not have permission to run ActiveX components or plugins with the current security settings. Please check the security setting.");
  window.navigate("blank.asp");        
}

function noPluginRights2(){
  if (typeof(ClientError2) == "function")
    ClientEvent2(50, "The security settings on this machine prevents downloading of ActiveX component or plugins [AltenaTif viewer]. This is required for APD LYNX Select. Please check browser security settings.");
  else
    alert("The security settings on this machine prevents downloading of ActiveX component or plugins [AltenaTif viewer]. This is required for APD LYNX Select. Please check browser security settings.");
  //ClientWarning("User does not have permission to download unsigned ActiveX components or plugins with the current security settings. Please check the security setting.");
  window.navigate("blank.asp");        
}

function ClientEvent2( nNumber, sDescription, sSource )
{
    if ( ( String( sSource ) == "undefined" ) || ( sSource == null ) )
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);
  //alert(sQuery);
    window.showModalDialog( sQuery, top, sDimensions + sSettings );
}

function ClientError2( sDescription ) { ClientEvent2( 0, sDescription ); }

checkBrowserSecuritySetting();