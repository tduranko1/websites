arrMonths = [
   "January", "February", "March", "April",
   "May", "June", "July", "August",
   "September", "October", "November", "December"];

var oPopup = window.createPopup();
var oPopBody = oPopup.document.body;
var uniqueID = 1;
var bCalInitialized = false;
var eleCurrent = null;

//this is the actual HTML stuff that will go into the popup window.
oPopBody.innerHTML =
  '<table border=0 class=WholeCalendar_' + uniqueID + '> ' +
  '  <tr style="height:18px;">                     ' +
  '      <td class=Title_' + uniqueID + ' style="width:70%;"></td>    ' +
  '      <td class=Title_' + uniqueID + ' > </td> ' +
  '  </tr>                                         ' +
  '  <tr valign="top"> <td colspan=3>                           ' +
  '    <table class=CalTable_' + uniqueID + ' cellspacing=0 border=0> ' +
  '      <tr><td class=DayTitle_' + uniqueID + ' style="color:#FF0000">S</td>' +
  '          <td class=DayTitle_' + uniqueID + '>M</td>' +
  '          <td class=DayTitle_' + uniqueID + '>T</td>' +
  '          <td class=DayTitle_' + uniqueID + '>W</td>' +
  '          <td class=DayTitle_' + uniqueID + '>T</td>' +
  '          <td class=DayTitle_' + uniqueID + '>F</td>' +
  '          <td class=DayTitle_' + uniqueID + '>S</td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>' +
  '    </table> ' +
  '    <table class=CalTable_' + uniqueID + ' cellpadding=0 cellspacing=0 border=0 style="border-collapse:collapse;border-top:1px solid #C0C0C0;padding-top:3px">  ' +
  '       <colgroup> ' +
  '         <col > ' +
  '         <col width="18px"> ' +
  '         <col width="10px"> ' +
  '         <col width="8px"> ' +
  '         <col width="18px"> ' +
  '         <col width="10px"> ' +
  '         <col width="8px"> ' +
  '         <col width="18px"> ' +
  '         <col width="10px"> ' +
  '         <col width="8px"> ' +
  '         <col width="21px"> ' +
  '         <col width="10px"> ' +
  '       </colgroup>' +
  '       <tr> ' +
  '         <td style="cursor:default;text-align:left;padding-left:5px" class="Day_' + uniqueID + '"><strong>Time</strong></td> ' +
  '         <td><div class="Day_' + uniqueID + '" style="border:1px solid #C0C0C0;width:100%;height:14px;cursor:default;"></div></td> ' +
  '         <td> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">5</div> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">6</div> ' +
  '         </td> ' +
  '         <td align="center"><strong>:</strong></td>' +
  '         <td><div class="Day_' + uniqueID + '" style="border:1px solid #C0C0C0;width:100%;height:14px;cursor:default;"></div></td> ' +
  '         <td> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">5</div> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">6</div> ' +
  '         </td> ' +
  '         <td align="center"><strong>:</strong></td>' +
  '         <td><div class="Day_' + uniqueID + '" style="border:1px solid #C0C0C0;width:100%;height:14px;cursor:default;"></div></td> ' +
  '         <td> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">5</div> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">6</div> ' +
  '         </td> ' +
  '         <td align="center">&nbsp;</td>' +
  '         <td><div class="Day_' + uniqueID + '" style="border:1px solid #C0C0C0;width:100%;height:14px;cursor:default;"></div></td> ' +
  '         <td> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">5</div> ' +
  '           <div style="height:3px;width:3px;font-family:Marlett;font-size:10px;cursor:hand">6</div> ' +
  '         </td> ' +
  '       </tr> ' +
  '    </table> ' +
  '    <table class=CalTable_' + uniqueID + ' cellpadding=0 cellspacing=0 border=0 style="border-collapse:collapse;border-top:1px solid #C0C0C0;margin-top:2px;padding-top:1px">  ' +
  '       <tr> ' +
  '         <td class="Day_' + uniqueID + '" style="width:50%;text-align:left;padding-left:5px"><strong>Today</strong></td> '+
  '         <td class="Day_' + uniqueID + '" style="cursor:default;">&nbsp;</td> '+
  '         <td class="Day_' + uniqueID + '" style="width:50%;text-align:right;padding-right:5px"><strong>Close</strong></td> '+
  '       </tr> ' +
  '    </table> ' +
  '  </tr>      ' +
  '</table>     ' +
  '<div id="divMonths" class="divMonths" style="position:absolute;top:16px;left:2px;display:none;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3);">' +
    '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">' +
      '<tr>' +
        '<td>Jan</td>'+
        '<td>Feb</td>'+
        '<td>Mar</td>'+
        '<td>Apr</td>'+
        '<td>May</td>'+
        '<td>Jun</td>'+
      '</tr>' +
      '<tr>' +
        '<td>Jul</td>'+
        '<td>Aug</td>'+
        '<td>Sep</td>'+
        '<td>Oct</td>'+
        '<td>Nov</td>'+
        '<td>Dec</td>'+
      '</tr>' +
    '</table>' +
  '</div>' +
  '<div id="divYears" class="divMonths" style="position:absolute;top:16px;left:5px;display:none;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3);">' +
    '<table border="0" cellspacing="0" cellpadding="3" style="border-collapse:collapse;">' +
      '<tr>' +
        '<td style="font-family:Marlett;width:5px;padding:0px;">3</td>' +
        '<td>1900</td>'+
        '<td>1900</td>'+
        '<td>1900</td>'+
        '<td>1900</td>'+
        '<td>1900</td>'+
        '<td style="font-family:Marlett;width:5px;padding:0px;">4</td>'+
      '</tr>' +
    '</table>' +
  '</div>';

  //this line caused error in Estimate Summary page cos so many calculations are going on and this file would not have loaded fully
  //window.setTimeout("InitCalendar()", 1);

  //moving this after the declaration of the InitCalendar() function


//attach the events to elements
function InitCalendar() {
  if (bCalInitialized) return;
  fnCreateStyleSheet(oPopup.document.createStyleSheet());
  var objTmp = oPopBody.firstChild.firstChild.firstChild.firstChild
  objTmp.onclick = fnMonthsClick;
  objTmp.onselectstart = fnSelectStart;
  objTmp.nextSibling.attachEvent("onclick", fnYearsClick);
  objTmp.nextSibling.attachEvent("onselectstart", fnSelectStart);

  oPopBody.firstChild.firstChild.lastChild.attachEvent("onclick", fnMonthsHide);
  oPopBody.attachEvent("onkeypress", fnCalKeyDown);
  oPopBody.tblTime = oPopBody.firstChild.firstChild.lastChild.firstChild.firstChild.nextSibling;
  oPopBody.tblToday = oPopBody.firstChild.firstChild.lastChild.firstChild.lastChild;
  oPopBody.tblToday.cells[0].onmouseover = function (){if (oPopBody.tblToday) {oPopBody.tblToday.cells[0].style.color = "#B8860B"}};
  oPopBody.tblToday.cells[0].onmouseout = function (){if (oPopBody.tblToday) {oPopBody.tblToday.cells[0].style.color = "#000000"}};
  oPopBody.tblToday.cells[0].attachEvent("onclick", fnTodayClick);
  oPopBody.tblToday.cells[2].onmouseover = function (){if (oPopBody.tblToday) {oPopBody.tblToday.cells[2].style.color = "#B8860B"}};
  oPopBody.tblToday.cells[2].onmouseout = function (){if (oPopBody.tblToday) {oPopBody.tblToday.cells[2].style.color = "#000000"}};
  oPopBody.tblToday.cells[2].attachEvent("onclick", fnCloseClick);
  //tblToday.cells[0].onmouseout = function () {this.style.color = "black"};
  var tblCal = oPopBody.firstChild.firstChild.lastChild.firstChild.firstChild;
  var tblCalCells = tblCal.cells;
  for(var i = 1; i < 7; i++)
    tblCalCells[i].attachEvent("onselectstart", fnSelectStart);
  //for(var i = 7; i < tblCalCells.length; i++) {
  for(var i = tblCalCells.length - 1; i > 6 ; i--) {
    tblCalCells[i].onmouseover = fnCalMouseOver;
    tblCalCells[i].onmouseout = fnCalMouseOut;
    tblCalCells[i].onclick = fnCalClick;
    tblCalCells[i].onselectstart = fnSelectStart;
  }
  fnInitMonths();
  fnInitYears();
  fnInitTime();
  bCalInitialized = true;
}

InitCalendar();
//this is the stylesheet of the calendar control
function fnCreateStyleSheet(ss)
{
  ss.addRule(   'SELECT',
      'font-family      : Tahoma ;'+
      'font-size        : 11px    ;'
    )
  ss.addRule(   '.WholeCalendar_' + uniqueID,
      'border           : 1px solid #c0c0c0;'+
      'border-collapse  : collapse         ;'+
      'background-color : #FFFFFF          ;'+
      'cursor           : default          ;'+
      'width            : 100%             ;'+
      'height           : 100%             ;'
    )

  ss.addRule(   '.Title_' + uniqueID,
      'color            : #FFFFFF  ;'+
      'font-family      : Tahoma   ;'+
      'font-size        : 11px     ;'+
      'font-weight      : bold     ;'+
      'text-align       : center   ;'+
      'cursor           : hand     ;'+
      'background-color : #1E90FF  ;'
    )

  ss.addRule(   '.DateControls_' + uniqueID,
      'text-align : right ;'
    )

  ss.addRule(   '.CalTable_' + uniqueID,
      'width  : 100%          ;'+
      'table-align: fixed'
    )

  ss.addRule(   '.DayTitle_' + uniqueID,
      'background-color    : lightgrey ;'+
      'color               : black     ;'+
      'font-family         : Tahoma    ;'+
      'font-size           : 9px       ;'+
      'font-weight         : bold      ;'+
      'text-align          : center    ;'+
      'border              : 1px solid #c0c0c0;'+
      'height              : 1         ;'
    )

  ss.addRule(   '.OffDay_' + uniqueID,
      'color               : #c0c0c0                ;'+
      'font-family         : Tahoma, Verdana, Arial ;'+
      'font-size           : 11px                   ;'+
      'font-weight         : normal                 ;'+
      'text-align          : center                 ;'+
      'vertical-align      : middle                 ;'+
      'border              : 1px solid #FFFFFF      ;'+
      'cursor              : hand                   ;'
    )

  ss.addRule(   '.Day_' + uniqueID,
      'color               : #000000                ;'+
      'font-family         : Tahoma, Verdana, Arial ;'+
      'font-size           : 11px                   ;'+
      'font-weight         : normal                 ;'+
      'text-align          : center                 ;'+
      'vertical-align      : middle                 ;'+
      'border              : 1px solid #FFFFFF      ;'+
      'cursor              : hand                   ;'
    )

  ss.addRule(   '.DayWeekend_' + uniqueID,
      'color               : #FF0000                ;'+
      'font-family         : Tahoma, Verdana, Arial ;'+
      'font-size           : 11px                   ;'+
      'font-weight         : normal                 ;'+
      'text-align          : center                 ;'+
      'vertical-align      : middle                 ;'+
      'border              : 1px solid #FFFFFF      ;'+
      'cursor              : hand                   ;'
    )

  ss.addRule(   '.DaySelected_' + uniqueID,
      //'background-image    : url("../images/input_bkgd.png");' +
      'FILTER              : FILTER: progid:DXImageTransform.Microsoft.Gradient(gradientType=0,startColorStr=#F4F2EF,endColorStr=#FFFFFF);' +
      'color               : #000000                        ;'+
      'font-family         : Tahoma, Verdana, Arial         ;'+
      'font-size           : 11px                           ;'+
      'text-align          : center                         ;'+
      'vertical-align      : middle                         ;'+
      'border              : 1px solid #c0c0c0              ;'+
      'cursor              : hand                           ;'
    )
  ss.addRule(   '.divMonths td',
      'background-color    : #FFFFFF                ;' +
      'color               : #000000                ;'+
      'font-family         : Tahoma, Verdana, Arial ;'+
      'font-size           : 11px                   ;'+
      'text-align          : center                 ;'+
      'vertical-align      : middle                 ;'+
      'border              : 1px solid #c0c0c0      ;'+
      'padding             : 3px                    ;' +
      'cursor              : hand                   ;'
    )
}

//initialize the Months drop down div
function fnInitMonths(){
  var objDivMonths = oPopBody.document.all["divMonths"];
  var tbl = objDivMonths.firstChild;
  var tblCells = tbl.cells;
  //for (var i = 0; i < tblCells.length; i++) {
  for (var i = tblCells.length - 1; i >= 0 ; i--) {
    /*tblCells[i].onmouseover = new Function("", "fnMonthsMouseOver(this)");
    tblCells[i].onmouseout = new Function ("", "fnMonthsMouseOut(this)");
    tblCells[i].onclick = new Function ("", "fnMonthClick(this)");
    tblCells[i].attachEvent("onselectstart", fnSelectStart);*/

    tblCells[i].onmouseover = fnMonthsMouseOver;
    tblCells[i].onmouseout = fnMonthsMouseOut;
    tblCells[i].onclick = fnMonthClick;
    tblCells[i].onselectstart = fnSelectStart;
  }
}

//initialize the Years drop down div
function fnInitYears(){
  var objDivMonths = oPopBody.document.all["divYears"];
  var tbl = objDivMonths.firstChild;
  var tblCells = tbl.cells;

/*  for (var i = 1; i < tblCells.length - 1; i++) {
    tblCells[i].onmouseover = new Function("", "fnMonthsMouseOver(this)");
    tblCells[i].onmouseout = new Function ("", "fnMonthsMouseOut(this)");
    tblCells[i].onclick = new Function ("", "fnYearClick(this)");
    tblCells[i].attachEvent("onselectstart", fnSelectStart);
  }
  tblCells[0].attachEvent("onselectstart", fnSelectStart);
  tblCells[0].onmouseover = new Function ("", "fnYearsMouseOver(this)");
  tblCells[0].onmouseout = new Function ("", "fnYearsMouseOut(this)");
  tblCells[0].onclick = new Function ("", "fnYearsPrevClick(this)");
  tblCells[0].onmousedown = new Function ("", "fnYearsMouseDown(this, -1)");
  tblCells[0].onmouseup = new Function ("", "fnYearsMouseUp(this)");
  tblCells[6].attachEvent("onselectstart", fnSelectStart);
  tblCells[6].onmouseover = new Function ("", "fnYearsMouseOver(this)");
  tblCells[6].onmouseout = new Function ("", "fnYearsMouseOut(this)");
  tblCells[6].onclick = new Function ("", "fnYearsNextClick(this)");
  tblCells[6].onmousedown = new Function ("", "fnYearsMouseDown(this, 1)");
  tblCells[6].onmouseup = new Function ("", "fnYearsMouseUp(this)");*/
  for (var i = 1; i < tblCells.length - 1; i++) {
    tblCells[i].onmouseover = fnMonthsMouseOver;
    tblCells[i].onmouseout = fnMonthsMouseOut;
    tblCells[i].onclick = fnYearClick;
    tblCells[i].onselectstart = fnSelectStart;
  }
  tblCells[0].onselectstart = tblCells[6].onselectstart = fnSelectStart;
  tblCells[0].onmouseover = tblCells[6].onmouseover = fnYearsMouseOver;
  tblCells[0].onmouseout = tblCells[6].onmouseout = fnYearsMouseOut;
  tblCells[0].onclick = fnYearsPrevClick;
  tblCells[0].onmousedown = new Function ("", "fnYearsMouseDown(this, -1)");
  tblCells[0].onmouseup = tblCells[6].onmouseup = fnYearsMouseUp;

  tblCells[6].onclick = fnYearsNextClick;
  tblCells[6].onmousedown = new Function ("", "fnYearsMouseDown(this, 1)");
}

function fnInitTime(){
  var tblTime = oPopBody.tblTime;
  var tblTimeCell2 = tblTime.cells[2].firstChild;
  var tblTimeCell2Next = tblTimeCell2.nextSibling;

  var tblTimeCell5 = tblTime.cells[5].firstChild;
  var tblTimeCell5Next = tblTimeCell5.nextSibling;

  var tblTimeCell8 = tblTime.cells[8].firstChild;
  var tblTimeCell8Next = tblTimeCell8.nextSibling;

  var tblTimeCell11 = tblTime.cells[11].firstChild;
  var tblTimeCell11Next = tblTimeCell11.nextSibling;

  tblTimeCell2.onmouseover = tblTimeCell2Next.onmouseover = tblTimeCell5.onmouseover = tblTimeCell5Next.onmouseover = tblTimeCell8.onmouseover = tblTimeCell8Next.onmouseover = tblTimeCell11.onmouseover = tblTimeCell11Next.onmouseover = fnYearsMouseOver;
  tblTimeCell2.onmouseout = tblTimeCell2Next.onmouseout = tblTimeCell5.onmouseout = tblTimeCell5Next.onmouseout = tblTimeCell8.onmouseout = tblTimeCell8Next.onmouseout = tblTimeCell11.onmouseout = tblTimeCell11Next.onmouseout = fnYearsMouseOut;

  tblTimeCell2.onclick = new Function ("", "fnTimeHoursInc(this, 1)");
  tblTimeCell2.onmousedown = new Function ("", "fnTimeHoursMouseDown(this, 1)");
  tblTimeCell2.onmouseup = new Function ("", "fnTimeMouseUp(this, 1)");
  tblTimeCell2Next.onclick = new Function ("", "fnTimeHoursInc(this, -1)");
  tblTimeCell2Next.onmousedown = new Function ("", "fnTimeHoursMouseDown(this, -1)");
  tblTimeCell2Next.onmouseup = new Function ("", "fnTimeMouseUp(this, -1)");

  tblTimeCell5.onclick = new Function ("", "fnTimeMinInc(this, 1)");
  tblTimeCell5.onmousedown = new Function ("", "fnTimeMinMouseDown(this, 1)");
  tblTimeCell5.onmouseup = new Function ("", "fnTimeMouseUp(this, 1)");
  tblTimeCell5Next.onclick = new Function ("", "fnTimeMinInc(this, -1)");
  tblTimeCell5Next.onmousedown = new Function ("", "fnTimeMinMouseDown(this, -1)");
  tblTimeCell5Next.onmouseup = new Function ("", "fnTimeMouseUp(this, -1)");

  tblTimeCell8.onclick = new Function ("", "fnTimeSecInc(this, 1)");
  tblTimeCell8.onmousedown = new Function ("", "fnTimeSecMouseDown(this, 1)");
  tblTimeCell8.onmouseup = new Function ("", "fnTimeMouseUp(this, 1)");
  tblTimeCell8Next.onclick = new Function ("", "fnTimeSecInc(this, -1)");
  tblTimeCell8Next.onmousedown = new Function ("", "fnTimeSecMouseDown(this, -1)");
  tblTimeCell8Next.onmouseup = new Function ("", "fnTimeMouseUp(this, -1)");

  tblTimeCell11.onclick = new Function ("", "fnTimeAMPMToggle(this, 1)");
  tblTimeCell11Next.onclick = new Function ("", "fnTimeAMPMToggle(this, -1)");

  var tblCells = tblTime.cells;
  //for (var i = 0; i < tblCells.length; i++) {
  for (var i = tblCells.length - 1; i >= 0; i--) {
    tblCells[i].onselectstart = fnSelectStart;
    if (tblCells[i].children.length > 0) {
      //for (var j = 0; j < tblCells[i].children.length; j++)
      for (var j = tblCells[i].children.length - 1; j >= 0; j--)
        tblCells[i].children[j].onselectstart = fnSelectStart;
    }
  }
}

//Display the calendar.
function ShowCalendar(eleID){
   if (oPopup.isOpen) return;

  var iX = iY = 0;
  var iHeight = 0;
  var dt = null;
  var bTimeEmpty = true;
  var TextID = eleID.children[2];
  var TimeID = eleID.children[3];
  var ImgID = eleID.children[1];
  eleCurrent = eleID;
  if (TextID){
    //if (!TextID.isContentEditable || TextID.disabled) return;

    var txtID = TextID.getAttribute("id");

    if (eleID.type == "datetime" || eleID.type == "time"){
      var objTime = TimeID;
      if (objTime != null) {
        oPopBody.objTime = objTime;
        dt = new Date(TextID.value + " " + objTime.value);
        bTimeEmpty = false;
      }
    }
    else {
      oPopBody.objTime = null;
      dt = new Date(TextID.value);
    }

    oPopBody.dateControl = TextID;
    var bFutureDate = TextID.getAttribute("futureDate");
    if (bFutureDate == null || bFutureDate == "false" || bFutureDate == false)
      oPopBody.futureDate = false;
    else
      oPopBody.futureDate = true;
  }
  else {
    dt = new Date();
    oPopBody.dateControl = null;
    oPopBody.objTime = null;
    oPopBody.futureDate = true;
  }

  var dt1 = new Date();
  oPopBody.currentYear = dt1.getFullYear();

  if (isNaN(dt)) dt = new Date();

  if (ImgID) {
    iX = -190 + ImgID.offsetWidth;
    iY = ImgID.offsetHeight + 1;
  }

  var iHours = dt.getHours();
  var iMin = dt.getMinutes();
  var iSec = dt.getSeconds();
  var sAMPM = "AM";
  //window.status = dt.toString();
  fnFillCalendar(dt);
  //fnInitYears();

  if (!oPopBody.objTime)
    oPopBody.firstChild.firstChild.lastChild.firstChild.firstChild.nextSibling.style.display = "none";
  else {
    //window.status = bTimeEmpty;
    if (bTimeEmpty){
      iHours = iMin = iSec = 0;
      sAMPM = "AM";
    }
    if (iHours == 0 && iMin == 0 && iSec == 0) sAMPM = "AM";
    if (iHours > 11) {
      sAMPM = "PM";
      if (iHours > 12)
        iHours -= 12;
    }

    var tblTime = oPopBody.tblTime;
    tblTime.style.display = "inline";


    tblTime.cells[1].firstChild.innerText = (iHours < 10 ? "0" + iHours : iHours);
    tblTime.cells[4].firstChild.innerText = (iMin < 10 ? "0" + iMin : iMin);
    tblTime.cells[7].firstChild.innerText = (iSec < 10 ? "0" + iSec : iSec);
    tblTime.cells[10].firstChild.innerText = sAMPM;
  }
  if (oPopBody.objTime)
    iHeight = 183; //165
  else
    iHeight = 159; //139

  oPopBody.tblToday.cells[0].fireEvent("onmouseout");

  if (ImgID)
    oPopup.show(iX, iY, 190, iHeight, ImgID);
  else
    oPopup.show(iX, iY, 190, iHeight, document);

  //if (!bTimeEmpty)
  //  updateTimeControl();
}

//update the calendar control with the month specified
function fnFillCalendar(dt) {
  //window.status = dt.toString();
  if (oPopBody.selectedCell){
    fnCalMouseOut(oPopBody.selectedCell);
  }
  oPopBody.Year = dt.getFullYear();
  oPopBody.Month = dt.getMonth();
  oPopBody.Date = dt.getDate();

  oPopBody.firstChild.cells[0].innerText = arrMonths[dt.getMonth()];
  oPopBody.firstChild.cells[1].innerText = dt.getFullYear();
  var tblCal = oPopBody.firstChild.firstChild.lastChild.firstChild.firstChild;
  var curMon = dt.getMonth();
  var curDate = dt.getDate();
  
  dt.setDate(1);
  var startCell = 7 + dt.getDay();

  var dtNow = new Date();
  dt.setHours(0, 0, 0);
  //dt.setMinutes(0);
  //dt.setSeconds(0);

  //reset the calendar
  oPopBody.selectedCell = null;
  for (var i = 7; i < tblCal.cells.length; i++) {
     var curCell = tblCal.cells[i];
    curCell.innerText = " ";
    curCell.style.border = "1px solid #FFFFFF";
    //tblCal.cells[i].style.backgroundImage = "";
    curCell.style.backgroundColor = "#FFFFFF";
    curCell.style.filter = "";
    curCell.style.cursor = "default";
    curCell.selected = false;
    curCell.enabled = true;
    curCell.style.color = "";
    curCell.className = "Day_1";
  }

  //update with appropriate calendar dates.
  for(var i = startCell; i < tblCal.cells.length; i++) {
    var iDate = dt.getDate();
    var curCell = tblCal.cells[i];
    curCell.innerText = iDate;
    if (i % 7 == 0)
      curCell.className = "DayWeekend_1";

    if (!oPopBody.futureDate) {
      if (dt > dtNow){}
      else if (iDate == curDate) {
        curCell.selected = true;
        fnCalMouseOver(tblCal.cells[i]);
        oPopBody.selectedCell = curCell;
      }
    }
    else if (iDate == curDate) {
      curCell.selected = true;
      fnCalMouseOver(tblCal.cells[i]);
      oPopBody.selectedCell = curCell;
    }

    if (!oPopBody.futureDate) {
      if (dt > dtNow) {
        curCell.enabled = false;
        curCell.style.color = "#DCDCDC";
      }
    }
    dt.setDate(dt.getDate() + 1);
    if (dt.getMonth() != curMon) break;
  }

  fnMonthsHide();
}

function fnCalMouseOver(obj) {
  if (!obj) obj = this;
  if (obj.getAttribute("enabled") == false) return;
  if (oPopBody.selectedCell && obj.innerText == " ") {
    fnSetStyleCalMouseOver(oPopBody.selectedCell);
    return;
  }

  if (oPopBody.selectedCell){
    fnSetStyleCalMouseOut(oPopBody.selectedCell);
  }

  if (obj.innerText != " ") {
    fnSetStyleCalMouseOver(obj);
  }
}

function fnCalMouseOut(obj) {
  if (!obj) obj = this;
  if (oPopBody.selectedCell){
    fnSetStyleCalMouseOver(oPopBody.selectedCell);
  }
  if (obj != oPopBody.selectedCell) {
    fnSetStyleCalMouseOut(obj);
  }
}

function fnCalKeyDown(obj) {
  if (oPopup.isOpen){
  }
}

function fnSetStyleCalMouseOver(obj) {
  obj.style.border = "1px solid #c0c0c0";
  //obj.style.backgroundImage = "url(\/images\/input_bkgd.png)";
  obj.style.filter = "FILTER: progid:DXImageTransform.Microsoft.Gradient(gradientType=0,startColorStr=#F4F2EF,endColorStr=#FFFFFF);";
}

function fnSetStyleCalMouseOut(obj) {
  obj.style.border = "1px solid #FFFFFF";
  //obj.style.backgroundImage = "";
  obj.style.filter = "";
}

function fnCalClick() {
  var obj = this;
  if (obj.innerText == " " ) return;
  if (obj.getAttribute("enabled") == false) return;

  //if (oPopBody.selectedCell) {
  //  window.status = oPopBody.selectedCell.innerText;
  //  fnCalMouseOut(oPopBody.selectedCell);
  //}
  //oPopBody.selectedCell = obj;
  //fnCalMouseOut(obj);
  fnCalMouseOver(obj);
  oPopBody.selectedCell = obj;

  if (oPopBody.dateControl) {
    var iMon = oPopBody.Month;
    var iDate = parseInt(obj.innerText, 10);
    var iYear = oPopBody.Year;
    iMon++;

    fnUpdateDate(iMon, iDate, iYear);
    updateTimeControl();
    //oPopup.hide();
  }
}

function fnUpdateDate(iMonth, iDate, iYear){
  if (oPopBody.dateControl) {

    var sMon = (iMonth < 10 ? "0" + iMonth : iMonth);
    var sDate = (iDate < 10 ? "0" + iDate : iDate);
    var dtNew = new Date(sMon + "/" + sDate + "/" + iYear);
    var dtNow = new Date();

    if (!oPopBody.futureDate)
      if (dtNew > dtNow)
        return;

    oPopBody.dateControl.value = sMon + "/" + sDate + "/" + iYear;
    oPopBody.dateControl.fireEvent("onchange");
    updateTimeControl();
    try {
      setControlDirty(oPopBody.dateControl);
    } catch(e) {}
  }
}

function fnMonthsClick(){
  var objDivMonths = oPopBody.document.all["divMonths"];
  if (objDivMonths.style.display == "none")
    objDivMonths.style.display = "inline";
  else
    objDivMonths.style.display = "none";
  var objDivYears = oPopBody.document.all["divYears"];
  objDivYears.style.display = "none";
}

function fnYearsClick(obj){
  var objDivMonths = oPopBody.document.all["divMonths"];
  objDivMonths.style.display = "none";
  var objDivYears = oPopBody.document.all["divYears"];

  if (objDivYears.style.display == "none")
    objDivYears.style.display = "inline";
  else
    objDivYears.style.display = "none";
  resetYears();
}

function resetYears() {
  var objDivMonths = oPopBody.document.all["divYears"];
  var tbl = objDivMonths.firstChild;
  var curYr = oPopBody.Year;
  if (!oPopBody.futureDate) {
    if ((curYr + 2) >= oPopBody.currentYear) {
      curYr = oPopBody.currentYear
      curYr = curYr - 2;
    }
  }
  tbl.cells[1].innerText = curYr - 2;
  tbl.cells[2].innerText = curYr - 1;
  tbl.cells[3].innerText = curYr;
  tbl.cells[4].innerText = curYr + 1;
  tbl.cells[5].innerText = curYr + 2;
}

function fnMonthsHide(){
  var objDivMonths = oPopBody.document.all["divMonths"];
  objDivMonths.style.display = "none";
  var objDivMonths = oPopBody.document.all["divYears"];
  objDivMonths.style.display = "none";
}

function fnMonthsMouseOver(obj){
  if (!obj) obj = this;
  obj.style.backgroundColor = "#FFD700";
}

function fnMonthsMouseOut(obj){
  if (!obj) obj = this;
  obj.style.backgroundColor = "#FFFFFF";
}

function fnYearsMouseOver(){
  var obj = this;
  obj.style.backgroundColor = "#B8860B";
  obj.style.color = "#FFFFFF";
}

function fnYearsMouseOut(){
  var obj = this;
  bTimeMouseDown = false;
  bYearsMouseDown = false;
  if (oPopBody.timer > 0)
    window.clearInterval(oPopBody.timer);
  obj.style.backgroundColor = "#FFFFFF";
  obj.style.color = "#000000";
}

function fnYearsPrevClick(){
  var obj = this;
  var objDivMonths = oPopBody.document.all["divYears"];
  var tbl = objDivMonths.firstChild;
  var curYr = parseInt(tbl.cells[1].innerText, 10) - 3;
  tbl.cells[1].innerText = curYr - 2;
  tbl.cells[2].innerText = curYr - 1;
  tbl.cells[3].innerText = curYr;
  tbl.cells[4].innerText = curYr + 1;
  tbl.cells[5].innerText = curYr + 2;
}

function fnYearsNextClick(){
  var obj = this;
  var objDivMonths = oPopBody.document.all["divYears"];
  var tbl = objDivMonths.firstChild;
  var curYr = parseInt(tbl.cells[5].innerText, 10) + 3;
  if (!oPopBody.futureDate)
    if ((curYr + 2) >= oPopBody.currentYear)
      curYr = oPopBody.currentYear - 2;
  tbl.cells[1].innerText = curYr - 2;
  tbl.cells[2].innerText = curYr - 1;
  tbl.cells[3].innerText = curYr;
  tbl.cells[4].innerText = curYr + 1;
  tbl.cells[5].innerText = curYr + 2;
}

function fnMonthClick(){
  var obj = this;
  var objDivMonths = oPopBody.document.all["divMonths"];
  objDivMonths.style.display = "none";
  fnMonthsMouseOut(obj);
  var tbl = objDivMonths.firstChild;
  var tblCells = tbl.cells;
  //for (var i = 0; i < tbl.cells.length; i++){
  for (var i = tblCells.length - 1; i >= 0 ; i--){
    if (tblCells[i] == obj){
      var curYr = oPopBody.Year;
      var curDate = oPopBody.Date;

      var dtNow = new Date();
      var newDt = new Date(curYr, i, curDate);
      if (curDate != newDt.getDate()){
        curDate = 1;
        newDt = new Date(curYr, i, curDate);
        oPopBody.Date = curDate;
      }

      oPopBody.Month = i;

      fnFillCalendar(newDt);
      i++;
      fnUpdateDate(i, curDate, curYr);
      break;
    }
  }
}

function fnYearClick() {
  var obj = this;
  fnMonthsMouseOut(obj);
  curYr = parseInt(obj.innerText, 10);

  var curMon = oPopBody.Month;
  var curDate = oPopBody.Date;

  oPopBody.Year = curYr;

  var newDt = new Date(curYr, curMon, curDate);
  fnFillCalendar(newDt);
  curMon++;
  fnUpdateDate(curMon, curDate, curYr);
}

function fnTimeHoursInc(obj, incDirection){
  var tblTime = oPopBody.tblTime;
  var objHours = tblTime.cells[1].firstChild;
  var iHours = parseInt(objHours.innerHTML, 10);
  var sAMPM = tblTime.cells[10].firstChild.innerText;
  iHours = (isNaN(iHours) ? 0 : iHours);
  if (incDirection > 0)
    iHours++;
  else
    iHours--;

  if (iHours == 12 && sAMPM == "AM")
    iHours = 0;
  else if (iHours > 12) {
    if (sAMPM == "AM")
      iHours = 0;
    else
      iHours = 1;
  }
  else if (iHours == 0){
    if (sAMPM == "PM")
      iHours = 12;
  }
  else if (iHours < 0 && sAMPM == "AM"){
    iHours = 11;
  }
  else if (iHours < 0){
    iHours = 12;
  }


  if (iHours < 10)
    objHours.innerText = "0" + iHours;
  else
    objHours.innerText = iHours;

  updateTimeControl();
}

function fnTimeMinInc(obj, incDirection){
  var tblTime = oPopBody.tblTime;
  var objMin = tblTime.cells[4].firstChild;
  var iMin = parseInt(objMin.innerHTML, 10);
  iMin = (isNaN(iMin) ? 0 : iMin);
  if (incDirection > 0)
    iMin++;
  else
    iMin--;

  if (iMin > 59) iMin = 0;
  if (iMin < 0 ) iMin = 59;


  if (iMin < 10)
    objMin.innerText = "0" + iMin;
  else
    objMin.innerText = iMin;

  updateTimeControl();
}

function fnTimeSecInc(obj, incDirection){
  var tblTime = oPopBody.tblTime;
  var objSec = tblTime.cells[7].firstChild;
  var iSec = parseInt(objSec.innerHTML, 10);
  iSec = (isNaN(iSec) ? 0 : iSec);
  if (incDirection > 0)
    iSec++;
  else
    iSec--;

  if (iSec > 59) iSec = 0;
  if (iSec < 0 ) iSec = 59;


  if (iSec < 10)
    objSec.innerText = "0" + iSec;
  else
    objSec.innerText = iSec;

  updateTimeControl();
}

function fnTimeAMPMToggle(obj, incDirection){
  var tblTime = oPopBody.tblTime;
  var objAMPM = obj.parentElement.previousSibling.firstChild;
  var sAMPM = objAMPM.innerText;

  if (sAMPM == "" || sAMPM == "PM")
    sAMPM = "AM";
  else
    sAMPM = "PM";

  objAMPM.innerText = sAMPM;
  var iHours = parseInt(tblTime.cells[1].firstChild.innerText, 10);
  if (sAMPM == "PM" && iHours == 0)
    tblTime.cells[1].firstChild.innerText = "12";

  updateTimeControl();
}

function updateTimeControl(){
  if (oPopBody.objTime){
    var tblTime = oPopBody.tblTime;
    var sTime = "";
    var iHr = parseInt(tblTime.cells[1].firstChild.innerText,10);
    var iMin = parseInt(tblTime.cells[4].firstChild.innerText,10);
    var iSec = parseInt(tblTime.cells[7].firstChild.innerText,10);
    var sAMPM = tblTime.cells[10].firstChild.innerText;

    iHr = (isNaN(iHr) ? "00" : ((iHr < 10) ? "0" + iHr : iHr) );
    iMin = (isNaN(iMin) ? "00" : ((iMin < 10) ? "0" + iMin : iMin) );
    iSec = (isNaN(iSec) ? "00" : ((iSec < 10) ? "0" + iSec : iSec) );
    sAMPM = (sAMPM == "PM" ? "PM" : "AM" );

    sTime = iHr + ":" + iMin + ":" + iSec + " " + sAMPM;

    if (!oPopBody.futureDate) {
      if (oPopBody.dateControl.value != "") {
        var dt = new Date(oPopBody.dateControl.value);
        sAMPM = (sAMPM == "PM" ? "PM" : "AM" );
        dt.setHours(((sAMPM == "PM" && iHr < 12) ? iHr + 12 : iHr), iMin, iSec);
        var dtNow = new Date();
        //window.status = dt.toString() + "; " + dtNow.toString();
        if (dt > dtNow)
          return;
      }
    }
    if (oPopBody.objTime.value != sTime) {
      var i = parseInt(iHr, 10);
      if (sAMPM == "PM" && i < 12)
         i += 12;

      var sTmp = (i < 10 ? "0" + i : i) + ":" + iMin + ":" + iSec;
      oPopBody.objTime.value = sTmp;
      oPopBody.objTime.fireEvent("onchange");
      try {
        setControlDirty(oPopBody.objTime);
      } catch(e) {}
    }
  }
}


var bTimeMouseDown = false;
function fnTimeHoursMouseDown(obj, incDirection){
  bTimeMouseDown = true;
  oPopBody.timer = window.setInterval("fnTimeHoursInc(" + null + ", " + incDirection + ")", 250);
}

function fnTimeMinMouseDown(obj, incDirection){
  bTimeMouseDown = true;
  oPopBody.timer = window.setInterval("fnTimeMinInc(" + null + ", " + incDirection + ")", 250);
}

function fnTimeSecMouseDown(obj, incDirection){
  bTimeMouseDown = true;
  oPopBody.timer = window.setInterval("fnTimeSecInc(" + null + ", " + incDirection + ")", 250);
}

function fnTimeMouseUp(obj, incDirection){
  bTimeMouseDown = false;
  if (oPopBody.timer > 0)
    window.clearInterval(oPopBody.timer);
}

var bYearsMouseDown = false;
function fnYearsMouseDown(obj, iDir){
  bYearsMouseDown = true;
  if (iDir == 1)
    oPopBody.timer = window.setInterval("fnYearsNextClick(" + null + ")", 250);
  else
    oPopBody.timer = window.setInterval("fnYearsPrevClick(" + null + ")", 250);
}

function fnYearsMouseUp(obj){
  bYearsMouseDown = false;
  if (oPopBody.timer > 0)
    window.clearInterval(oPopBody.timer);
}

function fnTodayClick(){
  var dtNow = new Date();
  fnUpdateDate((dtNow.getMonth() + 1), dtNow.getDate(), dtNow.getFullYear());
  if (oPopBody.objTime){
    var tblTime = oPopBody.tblTime;
    var iHours = dtNow.getHours();
    var iMin = dtNow.getMinutes();
    var iSec = dtNow.getSeconds();
    var sAMPM = "AM";

    if (iHours == 0 && iMin == 0 && iSec == 0) sAMPM = "AM";
    if (iHours > 12) {
      sAMPM = "PM";
      iHours -= 12;
    }

    tblTime.cells[1].firstChild.innerText = (iHours < 10 ? "0" + iHours : iHours);
    tblTime.cells[4].firstChild.innerText = (iMin < 10 ? "0" + iMin : iMin);
    tblTime.cells[7].firstChild.innerText = (iSec < 10 ? "0" + iSec : iSec);
    tblTime.cells[10].firstChild.innerText = sAMPM;
    updateTimeControl();
  }
  oPopup.hide();
   if (eleCurrent) 
    eleCurrent.setFocus();
}

function fnCloseClick(){
   oPopBody.tblToday.cells[2].fireEvent("onmouseout");
   if (oPopup.isOpen)
      oPopup.hide();
   if (eleCurrent) 
    eleCurrent.setFocus();
}

function fnSelectStart(){
  return false;
}

