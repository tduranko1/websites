/*****************************************************************************************************
  Name:     ZipCodeUtils.js
  Author:   Ramesh Vishegu
  Date:     07/30/2003
  Purpose:  Javascript version of the RSValidateZip. Trying to remove all remote script calls and use
            the new XMLHTTPPost method for better performance.
            
  Dependent files: (must be included in the web page using this file)
    * APDControls.js
    
  History:  
  Date        Developer   Description
  7/30/2003   RV          Initial Conversion
  
******************************************************************************************************/

function IsValidZip(ctlZip) {
  //make sure that APDControls.js was included. If it was included, XMLSave function would be defined.
  if (typeof(XMLSave) != "function")
    return;

  if (ctlZip) {
    var sZipCode = ctlZip.value;
    var aRequests = new Array();
    aRequests.push( { procName : "uspSMTValidateInfo",
                      method   : "ExecuteSpNpAsRS",
                      data     : "FedTaxId=&ZipCode='" + sZipCode + "'&ShopId="
                    }
                  );
                  
    var objRet = XMLSave(makeXMLSaveString(aRequests));
    
    if (objRet && objRet.code == 0) {
      var objRetXML = objRet.xml;
      if (objRetXML) {
        oResultNode = objRetXML.documentElement.selectSingleNode("//SQLResultCol[@name='ValidZipCode']");
        if (oResultNode) {
            if (oResultNode == "1")
              return true;
            else
              return false;
        }
      }
    }
  }
}

function ValidateZip(ctlZip, ctlCity, ctlState) {
  //make sure that APDControls.js was included. If it was included, XMLSave function would be defined.
  if (typeof(XMLSave) != "function")
    return;

  if (ctlZip) {
    var sZipCode = ctlZip.value;
    if (sZipCode == "") return;
    if (sZipCode.length != 5) {
      if (typeof(ClientWarning) == "function")
        ClientWarning("The entered zipcode length is invalid.  Please restrict to 5 characters.");
      else
        alert("The entered zipcode length is invalid.  Please restrict to 5 characters.");
      try {
        ctlZip.setFocus();
      } catch (e) {}
    }
  }

  if (ctlState && ctlState.value != "" && ctlCity && ctlCity.value != "")
    return;

  if (ctlZip) {
    var aRequests = new Array();
    aRequests.push( { procName : "uspZipCodeSearchXML",
                      method   : "ExecuteSpNpAsXML",
                      data     : "ZipCode=" + sZipCode + ""
                    }
                  );
                  
    var objRet = XMLSave(makeXMLSaveString(aRequests));
    
    if (objRet && objRet.code == 0) {
      var objRetXML = objRet.xml;
      if (objRetXML) {
        oZipInfoNode = objRetXML.documentElement.selectSingleNode("//ZipInfo");
        if (oZipInfoNode) {
          if (ctlState && (ctlState.value == "" || ctlState.value == null))
            ctlState.value = oZipInfoNode.getAttribute("State").trim();

          if (ctlCity && ctlCity.value == "")
            ctlCity.value = oZipInfoNode.getAttribute("City").trim();
        }
      }
    }
  }
}

function ValidateZipToCounty(ctlZip, ctlCounty) {
  //make sure that APDControls.js was included. If it was included, XMLSave function would be defined.
  if (typeof(XMLSave) != "function")
    return;

  if (ctlZip) {
    var sZipCode = ctlZip.value;
    var aRequests = new Array();
    aRequests.push( { procName : "uspZipCodeSearchXML",
                      method   : "ExecuteSpNpAsXML",
                      data     : "ZipCode=" + sZipCode + ""
                    }
                  );
                  
    var objRet = XMLSave(makeXMLSaveString(aRequests));
    
    if (objRet && objRet.code == 0) {
      var objRetXML = objRet.xml;
      if (objRetXML) {
        oZipInfoNode = objRetXML.documentElement.selectSingleNode("//ZipInfo");
        if (oZipInfoNode) {
          if (ctlCounty && ctlCounty.value == "")
            ctlCity.value = oZipInfoNode.getAttribute("County").trim();
        }
      }
    }
  }
}

function ValidateZipToCity(ctlZip, ctlCity) {
  //make sure that APDControls.js was included. If it was included, XMLSave function would be defined.
  if (typeof(XMLSave) != "function")
    return;

  if (ctlZip) {
    var sZipCode = ctlZip.value;
    var aRequests = new Array();
    aRequests.push( { procName : "uspZipCodeSearchXML",
                      method   : "ExecuteSpNpAsXML",
                      data     : "ZipCode=" + sZipCode + ""
                    }
                  );
                  
    var objRet = XMLSave(makeXMLSaveString(aRequests));
    
    if (objRet && objRet.code == 0) {
      var objRetXML = objRet.xml;
      if (objRetXML) {
        oZipInfoNode = objRetXML.documentElement.selectSingleNode("//ZipInfo");
        if (oZipInfoNode) {
          if (ctlCity && ctlCity.value == "")
            ctlCity.value = oZipInfoNode.getAttribute("City").trim();
        }
      }
    }
  }
}