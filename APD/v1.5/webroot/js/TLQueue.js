var data;
var strCurTask = "";
var strScriptSource = "";
var QueueTask = { "LHInitialContact": "94", "LHPOFollowup": "98", "LHLoGFollowup":"97", "VOInitialContact": "93", "VOFollowup": "96" };

var strUserID = "";
var strTaskID = "";
var strChecklistID = "";
var strLynxID = "";
var strVehNum = "";
var iTimeLeft = 0;
var iTimeTaskSlideExpiration = 10 * 60; // slide the expiration every 10 minutes
var iTimer = null;
var iTimePageRefresh = null;
var blnRefreshGrid = false;
var arrConfirmations = null;
var arrRadioConfirmations = null;
var intEscalate = 0;
var strNotes = "";
var blnTLEdit = false;
var strGreetingTimeOfDay = "morning";
var strBringInView = "";
var intScrollTop = -1;

var strQueueUser = "0";

try { strUserID = top.gsUserID; strQueueUser = parent.gsQueueUser } catch (e) { }
if (strUserID == "") top.navigate("index.asp");

var strChecklistID = "";

$.ready(
   jqReady()
);

function jqReady() {
   initQueue();
}

function __pageInit() {
   if (blnRefreshGrid == true) grdTLQueue.refresh();
}

function initQueue(){
	$("#divProgress").modal({ escClose: false });
	$.get('tlqueue.asp', function(dataRet) {
	   xmlData.loadXML(dataRet.xml);
	   massageQueueData();
	   try { grdTLQueue.refresh(); } catch (e) { blnRefreshGrid = true; }
	   hideModal();
	});
}

function showTask(strDiv) {
	 if (strDiv != "" && strDiv != undefined) {
		  if (strDiv.substr(0, 1) != "#") strDiv = "#" + strDiv;
		  $(strDiv).modal({ escClose: false });
		  strCurTask = strDiv;
	 } else {
		  if (strCurTask != "") {
				$(strCurTask).modal({ escClose: false });
		  }
	 }
}

function showEdit(strDiv) {
	if (strDiv.substr(0, 1) != "#") strDiv = "#" + strDiv;
	$(strDiv).modal({ escClose: false});
}

function updateScript(strDiv) {
  var strSource = "";

  if (strDiv != "" && strDiv != undefined) {
     if (strDiv.substr(0, 1) != "#") strDiv = "#" + strDiv;
     strSource = strDiv;
     strScriptSource = strDiv;
  } else {
     if (strScriptSource != "") {
        strSource = strScriptSource;
     }
  }
  if (strSource != "") {
     $("#divLHInitialContact").removeClass().addClass(strSource == "#divLHInitialContact" ? "visible" : "hidden");
     $("#divLHFollowup").removeClass().addClass(strSource == "#divLHFollowup" ? "visible" : "hidden");
     $("#divVOInitialContact").removeClass().addClass(strSource == "#divVOInitialContact" ? "visible" : "hidden");
     $("#divVOFollowup").removeClass().addClass(strSource == "#divVOFollowup" ? "visible" : "hidden");
  }
}

function hideModal(){
	$.modal.close();
}

function massageQueueData() {
   try {
      var objChecklists = xmlData.selectNodes("/Root/TLQueue");
      var objRoot = xmlData.selectSingleNode("/Root");
      var strTaskID;
      var strDueDate;
      if (strQueueUser == "1") {
         //queue users dont have to see the locked ones. So, delete them. these will appear after they expire.
         for (var i = objChecklists.length - 1; i >= 0; i--) {
            if (objChecklists[i].getAttribute("Locked") == "1") {
               objRoot.removeChild(objChecklists[i]);
            }
         }
      }
      for (var i = 0; i < objChecklists.length; i++) {
         objChecklists[i].setAttribute("Indicator", objChecklists[i].getAttribute("Locked") == "1" ? "/images/lock.gif" : "/images/spacer.gif");
         strTaskID = objChecklists[i].getAttribute("TaskID");
         strDueDate = objChecklists[i].getAttribute("DueDate");
         if (strTaskID == QueueTask.LHInitialContact || strTaskID == QueueTask.LHPOFollowup || strTaskID == QueueTask.LHLoGFollowup) {
            objChecklists[i].setAttribute("LHVOName", objChecklists[i].getAttribute("LHName"));
         } else {
            objChecklists[i].setAttribute("LHVOName", objChecklists[i].getAttribute("VOName"));
         }
         if (strDueDate != "") {
            var dt = new Date(strDueDate);
            objChecklists[i].setAttribute("TaskDue", dt.getTime());
         }
      }
      $("#spnTotalCount").html(objChecklists.length);
      $("#spnLocked").html(xmlData.selectNodes("/Root/TLQueue[@Locked='1']").length);

      if (objChecklists.length == 0) {
          //xmlData.loadXML("<Root/>");
         //try { grdTLQueue.refresh(); } catch (e) { }
         startPageRefresh();
      }
   } catch (e) {
      alert(e.description);
   }
}

function getNextTask(sChecklistID) {
	//get the next available task
   $("#divProgress").modal({ escClose: false });
   if (iTimer > 0) { window.clearInterval(iTimer); iTimer = null }
   $.get('tlnexttask.asp?UserID=' + strUserID + (sChecklistID != undefined ? "&ChecklistID=" + sChecklistID : ""), function(dataRet) {
       initEdits();
       hideModal();
       //alert(dataRet.xml);
       xmlTaskData.loadXML(dataRet.xml);
       if (xmlTaskData.selectSingleNode("//Error")) {
            //decode the error message.
           var strText = xmlTaskData.selectSingleNode("//Error").text.split("Raw Description:");
           strText = strText[1].substr(strText[1].indexOf(":") + 1);
           strText = strText.substr(0, strText.indexOf("ADO"));
           alert(strText); 
           return;
       }
       extractQueue();
       //alert(xml2json(xmlTaskData.documentElement));
       eval("var dataJSON = {" + xml2json(xmlTaskData.documentElement) + "}");
       data = dataJSON;
       populateTaskDetail();
       btnCommit.CCDisabled = false;
       btnClaimDetails.CCDisabled = false;
   });

}

function extractQueue() {
	var objQueue = xmlTaskData.selectNodes("//TLQueue");
	if (objQueue.length > 0) {
	   xmlData.loadXML("<Root/>");
	   for (var i = 0; i < objQueue.length; i++) {
	      xmlData.documentElement.appendChild(objQueue[i]);
	   }
	   massageQueueData();
	   try { grdTLQueue.refresh(); } catch (e) { }
	} else {
	   xmlData.loadXML("<Root/>");
	   grdTLQueue.refresh();
	   startPageRefresh();
	}
}

function populateTaskDetail() {
   strTaskID = "";
   strChecklistID = "";
   var strTaskDiv = "";
  
	var objCL = xmlTaskData.selectSingleNode("/Root/Checklist");
	if (objCL) {
	   var datetoday = new Date();
	   var thehour = datetoday.getHours();
	   if (thehour > 18)
	      strGreetingTimeOfDay = "evening";
	   else if (thehour > 12)
	      strGreetingTimeOfDay = "afternoon";
	   else
	      strGreetingTimeOfDay = "day";
	   
	   strLynxID = objCL.getAttribute("LynxID");
	   strVehNum = objCL.getAttribute("ClaimAspectNumber");
	   strTaskID = objCL.getAttribute("TaskID");
	   strChecklistID = objCL.getAttribute("ChecklistID");
	   
	   $("#spnTaskID").text(strLynxID + "-" + strVehNum + " [" + strChecklistID + "]");
	   switch (strTaskID) {
	      case QueueTask.LHInitialContact: strTaskDiv = "divLHInitialContact";
	         showLHInitialContact();
	         break;
	      case QueueTask.LHPOFollowup: strTaskDiv = "divLHFollowup";
	         showLHFollowup();
	         break;
	      case QueueTask.LHLoGFollowup: strTaskDiv = "divLHFollowup";
	         showLHFollowup();
	         break;
	      case QueueTask.VOInitialContact: strTaskDiv = "divVOInitialContact";
	         showVOInitialContact();
	         break;
	      case QueueTask.VOFollowup: strTaskDiv = "divVOFollowup";
	         showVOFollowup();
	         break;
	   }
	   iTimeLeft = iTimeTaskSlideExpiration;
	   startCountDown();
	   stopPageRefresh();
	} else {
	   alert("No Tasks available.");
	}
}

function startCountDown() {
   iTimer = window.setInterval("doCountDown()", 1000);
}

function doCountDown() {
   if (iTimeLeft > 0) {
      iTimeLeft--;
   } else {
      window.clearInterval(iTimer);
      iTimer = null;
      slideExpiration();
      iTimeLeft = iTimeTaskSlideExpiration;
      startCountDown();
   }
}

function formatTime(iSeconds) {
   if (iSeconds > 60) {
      var iMin = Math.floor(iSeconds / 60);
      var iSec = iSeconds - (iMin * 60);
      return iMin + " minute" + (iMin > 1 ? "s" : "") + (iSec > 0 ? " and " + iSec + " seconds." : "");
   } else {
      return iSeconds + " seconds";
   }
}

function showLHInitialContact(){
	var strLHName = "";
	var strVOName = "";
	var strContactName = "";
	var strContactPhone = "";
	var strRepName = "";
	var strInsCoName = "";
	strRepName = data.Root.LynxRepName;
	strInsCoName = data.Root.Checklist.Insurance.Name;
	strVOName = (data.Root.Checklist.VehicleOwner.NameBusiness != "" ? data.Root.Checklist.VehicleOwner.NameBusiness : data.Root.Checklist.VehicleOwner.NameFirst + " " + data.Root.Checklist.VehicleOwner.NameLast);
	divTaskName.innerText = data.Root.Checklist.TaskName;
	if (data.Root.Checklist.TaskID == QueueTask.LHInitialContact) {
		strContactName = data.Root.Checklist.LienHolder.Name;
		strContactPhone = data.Root.Checklist.LienHolder.Phone;
	} else {
	   strContactName = strVOName;  
		strContactPhone = data.Root.Checklist.VehicleOwner.Phone;
	}

	var strVehicle = data.Root.Checklist.Vehicle.Year + " " + data.Root.Checklist.Vehicle.Make + " " + data.Root.Checklist.Vehicle.Model + " [VIN: " + data.Root.Checklist.Vehicle.Vin + "]";
	var strLienHolder = data.Root.Checklist.LienHolder.Name + " (" + data.Root.Checklist.LienHolder.AddressCity + ", " + data.Root.Checklist.LienHolder.AddressState + ")";
	var strLienHolderPayoffAmount = "$ " + data.Root.Checklist.TL.PayoffAmount;
	divName.innerText = strContactName;
	divPhone.innerText = formatPhone(strContactPhone);
	showTask("divTaskDetail");
	
	$("#divLHInitialContact .LYNXRep").each(function(index){$(this).text(strRepName)});
	$("#divLHInitialContact .InsCoName").each(function(index){$(this).text(strInsCoName)});
	$("#divLHInitialContact .VOName").each(function(index) { $(this).text(strVOName) });
	$("#divLHInitialContact .VONameConfirm").each(function (index) { $(this).text(strVOName) });
	$("#divLHInitialContact .VOVehicleConfirm").each(function (index) { $(this).text(strVehicle) });
	$("#divLHInitialContact .LHInfoConfirm").each(function (index) { $(this).text(strLienHolder) });
	$("#divLHInitialContact .LHPayoffAmountConfirm").each(function(index) { $(this).text(strLienHolderPayoffAmount) });
	$("#divLHInitialContact .LoGAmount").each(function(index) { $(this).text("$ " + data.Root.Checklist.TL.LetterofGauranteeAmount) });
	$("#divLHInitialContact .ClaimOwnerName").each(function(index) { $(this).text(data.Root.Checklist.ClaimOwnerName) });
	$("#divLHInitialContact .ClaimOwnerPhone").each(function(index) { $(this).text(formatPhone(data.Root.Checklist.ClaimOwnerPhone)) });
	$("#divLHInitialContact .ClaimOwnerEmail").each(function(index) { $(this).text(data.Root.Checklist.ClaimOwnerEmail) });
	$("#divLHInitialContact .LynxID").each(function(index) { $(this).text(data.Root.Checklist.LynxID) });
	$("#divLHInitialContact .greetingTimeOfDay").each(function(index) { $(this).text(strGreetingTimeOfDay) });
	updateScript("#divLHInitialContact");
}

function showLHFollowup() {
	var strLHName = "";
	var strVOName = "";
	var strContactName = "";
	var strContactPhone = "";
	var strRepName = "";
	var strInsCoName = "";
	strRepName = data.Root.LynxRepName;
	strInsCoName = data.Root.Checklist.Insurance.Name;
	strVOName = (data.Root.Checklist.VehicleOwner.NameBusiness != "" ? data.Root.Checklist.VehicleOwner.NameBusiness : data.Root.Checklist.VehicleOwner.NameFirst + " " + data.Root.Checklist.VehicleOwner.NameLast);
	divTaskName.innerText = data.Root.Checklist.TaskName;
	var strLastContactDate = data.Root.Checklist.lastCalledOn;
	var blnLienHolder = typeof(data.Root.Checklist.LienHolder) == "object";
	if (blnLienHolder) {
		strContactName = data.Root.Checklist.LienHolder.Name;
		strContactPhone = data.Root.Checklist.LienHolder.Phone;
	} else {
	strContactName = strVOName;  
		strContactPhone = data.Root.Checklist.VehicleOwner.Phone;
	}

	divName.innerText = strContactName;
	divPhone.innerText = formatPhone(strContactPhone);
	showTask("divTaskDetail");

	$("#divLHFollowup .LYNXRep").each(function (index) { $(this).text(strRepName) });
	$("#divLHFollowup .InsCoName").each(function (index) { $(this).text(strInsCoName) });
	$("#divLHFollowup .VOName").each(function (index) { $(this).text(strVOName) });
	$("#divLHFollowup .LastContactDate").each(function(index) { $(this).text(strLastContactDate) });

	if (strTaskID == QueueTask.LHLoGFollowup) {
		$("#divLHFollowup .LoGTitle").removeClass("hidden");
		$("#divLHFollowup .ReqLoG").removeClass("hidden");
		$("#divLHFollowup .PayoffProcessing").removeClass("hidden");
		$("#divLHFollowup .PoATitle").addClass("hidden");
		$("#divLHFollowup .ReqPoA").addClass("hidden");
		$("#divLHFollowup .SalvageProcessing").addClass("hidden");
	} else {
		$("#divLHFollowup .LoGTitle").addClass("hidden");
		$("#divLHFollowup .ReqLoG").addClass("hidden");
		$("#divLHFollowup .PayoffProcessing").addClass("hidden");
		$("#divLHFollowup .PoATitle").removeClass("hidden");
		$("#divLHFollowup .ReqPoA").removeClass("hidden");
		$("#divLHFollowup .SalvageProcessing").removeClass("hidden");
	}

	updateScript("#divLHFollowup");
}

function showVOInitialContact() {
	var strLHName = "";
	var strVOName = "";
	var strContactName = "";
	var strContactPhone = "";
	var strRepName = "";
	var strInsCoName = "";
	strRepName = data.Root.LynxRepName;
	strInsCoName = data.Root.Checklist.Insurance.Name;
	strVOName = (data.Root.Checklist.VehicleOwner.NameBusiness != "" && data.Root.Checklist.VehicleOwner.NameBusiness != undefined ? data.Root.Checklist.VehicleOwner.NameBusiness : data.Root.Checklist.VehicleOwner.NameFirst + " " + data.Root.Checklist.VehicleOwner.NameLast);
	divTaskName.innerText = data.Root.Checklist.TaskName;
	var strLastContactDate = data.Root.Checklist.lastCalledOn;
	var blnLienHolder = typeof (data.Root.Checklist.LienHolder) == "object";
	var strLienHolderPayoffAmount = "$ " + data.Root.Checklist.TL.PayoffAmount;
	
	strContactName = strVOName;  
	strContactPhone = data.Root.Checklist.VehicleOwner.Phone;

	var strVehicle = data.Root.Checklist.Vehicle.Year + " " + data.Root.Checklist.Vehicle.Make + " " + data.Root.Checklist.Vehicle.Model + " [VIN: " + data.Root.Checklist.Vehicle.Vin + "]";
	var strLienHolder;
	if (blnLienHolder) {
	   strLienHolder = data.Root.Checklist.LienHolder.Name + " (" + data.Root.Checklist.LienHolder.AddressCity + ", " + data.Root.Checklist.LienHolder.AddressState + ")";
	   $("#tr3LHInfo").removeClass("hidden");
	   $("#tr3LHAdvanceAmount").removeClass("hidden");
	   $("#tr3ReqTitle").addClass("hidden");
	} else {
	   $("#tr3LHInfo").addClass("hidden");
	   $("#tr3LHAdvanceAmount").addClass("hidden");
	   $("#tr3ReqTitle").removeClass("hidden");
	}
	divName.innerText = strContactName;
	divPhone.innerText = formatPhone(strContactPhone);
	showTask("divTaskDetail");
	
	$("#divVOInitialContact .LYNXRep").each(function (index) { $(this).text(strRepName) });
	$("#divVOInitialContact .InsCoName").each(function (index) { $(this).text(strInsCoName) });
	$("#divVOInitialContact .VOName").each(function (index) { $(this).text(strVOName) });
	$("#divVOInitialContact .VONameConfirm").each(function (index) { $(this).text(strVOName) });
	$("#divVOInitialContact .VOVehicleConfirm").each(function (index) { $(this).text(strVehicle) });
	$("#divVOInitialContact .LHInfoConfirm").each(function(index) { $(this).text(strLienHolder) });
	$("#divLHInitialContact .LHPayoffAmountConfirm").each(function(index) { $(this).text(strLienHolderPayoffAmount) });
	$("#divVOInitialContact .VOSettlementAmountConfirm").each(function (index) { $(this).text("$ " + data.Root.Checklist.TL.SettlementAmount) });
	$("#divVOInitialContact .AdvanceAmountConfirm").each(function(index) { $(this).text("$ " + data.Root.Checklist.TL.AdvanceAmount) });
	$("#divVOInitialContact .VOEmailAddress").each(function(index) { $(this).text(data.Root.Checklist.VehicleOwner.EmailAddress) });
	$("#divVOInitialContact .ClaimOwnerName").each(function(index) { $(this).text(data.Root.Checklist.ClaimOwnerName) });
	$("#divVOInitialContact .ClaimOwnerPhone").each(function(index) { $(this).text(formatPhone(data.Root.Checklist.ClaimOwnerPhone)) });
	$("#divVOInitialContact .ClaimOwnerEmail").each(function(index) { $(this).text(data.Root.Checklist.ClaimOwnerEmail) });
	$("#divVOInitialContact .LynxID").each(function(index) { $(this).text(data.Root.Checklist.LynxID) });
	$("#divVOInitialContact .greetingTimeOfDay").each(function(index) { $(this).text(strGreetingTimeOfDay) });

	$("#divVIInitialContactProvideEmail").removeClass().addClass(data.Root.Checklist.VehicleOwner.EmailAddress == "" ? "visible" : "hidden");
   $("#divVOInitialContactNoEmail").removeClass().addClass(data.Root.Checklist.VehicleOwner.EmailAddress == "" ? "visible" : "hidden");

	updateScript("#divVOInitialContact");
}

function showVOFollowup() {
	var strLHName = "";
	var strVOName = "";
	var strContactName = "";
	var strContactPhone = "";
	var strRepName = "";
	var strInsCoName = "";
	strRepName = data.Root.LynxRepName;
	strInsCoName = data.Root.Checklist.Insurance.Name;
	strVOName = (data.Root.Checklist.VehicleOwner.NameBusiness != "" ? data.Root.Checklist.VehicleOwner.NameBusiness : data.Root.Checklist.VehicleOwner.NameFirst + " " + data.Root.Checklist.VehicleOwner.NameLast);
	divTaskName.innerText = data.Root.Checklist.TaskName;
	var strLastContactDate = data.Root.Checklist.lastCalledOn;
	var blnLienHolder = typeof (data.Root.Checklist.LienHolder) == "object";
	if (blnLienHolder) {
		strContactName = data.Root.Checklist.LienHolder.Name;
		strContactPhone = data.Root.Checklist.LienHolder.Phone;
	} else {
	   strContactName = strVOName;  
		strContactPhone = data.Root.Checklist.VehicleOwner.Phone;
	}

	divName.innerText = strContactName;
	divPhone.innerText = formatPhone(strContactPhone);
	showTask("divTaskDetail");

	var blnPoAExists = (xmlTaskData.selectSingleNode("/Root/Checklist/Document[@DocumentTypeID = '54']") != null);
	var blnTitleExists = (xmlTaskData.selectSingleNode("/Root/Checklist/Document[@DocumentTypeID = '53']") != null);

	$("#divVOFollowup .LYNXRep").each(function (index) { $(this).text(strRepName) });
	$("#divVOFollowup .InsCoName").each(function (index) { $(this).text(strInsCoName) });
	$("#divVOFollowup .VOName").each(function (index) { $(this).text(strVOName) });
	$("#divVOFollowup .LastContactDate").each(function (index) { $(this).text(strLastContactDate) });

	if (blnLienHolder) {
	   $("#tr4ReqTitle").addClass("hidden");
	} else {
	   $("#tr4ReqTitle").removeClass("hidden");
	}

	if (blnPoAExists) {
		$("#tr4ReqPoA").addClass("hidden");
	} else {
		$("#tr4ReqPoA").removeClass("hidden");
	}

	if (blnTitleExists) {
		$("#tr4ReqTitle").addClass("hidden");
	} else {
		$("#tr4ReqPoA").removeClass("hidden");
   }

	updateScript("#divVOFollowup");
}

function initEdits() {
   //reset Vehicle Owner edit
   txtVOFirstName.value = "";
   txtVOLastName.value = "";
   txtVOBusinessName.value = "";
   txtVOAddress1.value = "";
   txtVOAddress2.value = "";
   txtVOAddressCity.value = "";
   txtVOAddressState.value = "";
   txtVOAddressZip.value = "";
   txtVOPhone.value = "";
   txtVOAltPhone.value = "";
   txtVOEmailAddress.value = "";
   
   //reset Vehicle information
   txtVehicleYear.value = "";
   txtVehicleMake.value = "";
   txtVehicleModel.value = "";
   txtVehicleVIN.value = "";

   //reset Lien Holder edit
   txtLHName.value = "";
   txtLHAddress1.value = "";
   txtLHAddress2.value = "";
   txtLHAddressCity.value = "";
   txtLHAddressState.value = "";
   txtLHAddressZip.value = "";
   txtLHFedTaxID.value = "";
   txtLHEmailAddress.value = "";

   chkSameAsGeneral.value = 1;
   txtLHBillingName.value = "";
   txtLHBillingAddress1.value = "";
   txtLHBillingAddress2.value = "";
   txtLHBillingAddressCity.value = "";
   txtLHBillingAddressState.value = "";
   txtLHBillingAddressZip.value = "";
   chkSameAsGeneral.value = 0;

   txtLHEFTAccountNumber.value = "";
   txtLHEFTRoutingNumber.value = "";

   //reset Total Loss edit
   txtTLPayoff.value = "";
   txtTLSettlement.value = "";
   txtTLAdvance.value = "";
   txtTLLoG.value = "";
   txtLHLienHolderAccountNumber.value = "";

   chk1VOOwner.value = 0;
   chk1VOVehicle.value = 0;
   chk1LHInfo.value = 0;
   chk1LHPayoff.value = 0;
   chk1ReqPoA.value = 0;
   chk1ReqTitle.value = 0;

   chk3VOOwner.value = 0;
   chk3VOVehicle.value = 0;
   chk3VOSettlementAmt.value = 0;
   chk3LHInfo.value = 0;
   chk3AdvanceAmount.value = 0;
   //chk3RentalExpiration.value = 0;
   chk3ReqPoA.value = 0;
   chk3ReqTitle.value = 0;

   chk4ReqPoA.value = 0;
   chk4ReqTitle.value = 0;

   selCallType.value = "Contacted";
   txtCallNotes.value = "";
   chkEscalate.value = 0;
}

function xml2json(obj){
	if (obj){
		var strRet = obj.nodeName + " : {" ;
		var attribs = obj.attributes;
		for (var i = 0; i < attribs.length; i++){
			strRet += attribs[i].nodeName + " : \"" + attribs[i].text.replace(/[\"]/g, "") + "\"";
			strRet += (i < attribs.length - 1 ? ", " : "");
		}
		
		for (var i = 0; i < obj.childNodes.length; i++){
			strRet += (i < obj.childNodes.length ? ", " : "");
			strRet += xml2json(obj.childNodes[i]);
		}
		
		strRet += "}";
		
		return strRet;
	}
}

/*function formatPhone(str) {
	if (str != "")
		return "(" + str.substr(0,3) + ") " + str.substr(3, 3) + " " + str.substr(6,4);
	else
		return str;
}*/

function getAttribute(strXPath, strAttrib){
	var strRet = "";
	var obj = xmlTaskData.selectSingleNode(strXPath);
	if (obj)
		strRet = obj.getAttribute(strAttrib);
	return strRet;
}

function ReshowTask() {
   switch (strScriptSource) {
      case "#divLHInitialContact":
         showLHInitialContact();
         break;
      case "#divLHFollowup":
         showLHFollowup();
         break;
      case "#divVOInitialContact":
         showVOInitialContact();
         break;
      case "#divVOFollowup":
         showVOFollowup();
         break;
   }
}

function editOwner() {
   saveConfirmations();
	hideModal();
	showEdit("divVOEdit");
	txtVOFirstName.value = data.Root.Checklist.VehicleOwner.NameFirst;
	txtVOLastName.value = data.Root.Checklist.VehicleOwner.NameLast;
	txtVOBusinessName.value = data.Root.Checklist.VehicleOwner.NameBusiness;
	txtVOAddress1.value = data.Root.Checklist.VehicleOwner.Address1;
	txtVOAddress2.value = data.Root.Checklist.VehicleOwner.Address2;
	txtVOAddressCity.value = data.Root.Checklist.VehicleOwner.AddressCity;
	txtVOAddressState.value = data.Root.Checklist.VehicleOwner.AddressState;
	txtVOAddressZip.value = data.Root.Checklist.VehicleOwner.AddressZip;
	txtVOPhone.value = data.Root.Checklist.VehicleOwner.Phone;
	txtVOAltPhone.value = data.Root.Checklist.VehicleOwner.AltPhone;
	txtVOEmailAddress.value = data.Root.Checklist.VehicleOwner.EmailAddress;
	txtVOFirstName.setFocus();
}

function updateOwner() {
	 data.Root.Checklist.VehicleOwner.NameFirst = txtVOFirstName.value;
	 data.Root.Checklist.VehicleOwner.NameLast = txtVOLastName.value;
	 data.Root.Checklist.VehicleOwner.NameBusiness = txtVOBusinessName.value;
    data.Root.Checklist.VehicleOwner.Address1 = txtVOAddress1.value;
	 data.Root.Checklist.VehicleOwner.Address2 = txtVOAddress2.value;
	 data.Root.Checklist.VehicleOwner.AddressCity = txtVOAddressCity.value;
	 data.Root.Checklist.VehicleOwner.AddressState = txtVOAddressState.value;
	 data.Root.Checklist.VehicleOwner.AddressZip = txtVOAddressZip.value;
	 data.Root.Checklist.VehicleOwner.Phone = txtVOPhone.value;
	 data.Root.Checklist.VehicleOwner.AltPhone = txtVOAltPhone.value;
	 data.Root.Checklist.VehicleOwner.EmailAddress = txtVOEmailAddress.value;

	 hideModal();
	 ReshowTask();
	 restoreConfirmations();
	 /*if (intScrollTop > 0) {
	    divScript.scrollTop = intScrollTop;
	 }
	 strBringInView = "";
	 intScrollTop = -1;*/
}

function cancelEdit() {
	hideModal();
	ReshowTask();
	restoreConfirmations();
}

function editVehicle() {
   saveConfirmations();
	hideModal();
	showEdit("divVehicleEdit");
	txtVehicleYear.value = data.Root.Checklist.Vehicle.Year;
	txtVehicleMake.value = data.Root.Checklist.Vehicle.Make;
	txtVehicleModel.value = data.Root.Checklist.Vehicle.Model;
	txtVehicleVIN.value = data.Root.Checklist.Vehicle.Vin;
}

function updateVehicle() {
	 data.Root.Checklist.Vehicle.Year = txtVehicleYear.value;
	 data.Root.Checklist.Vehicle.Make = txtVehicleMake.value;
	 data.Root.Checklist.Vehicle.Model = txtVehicleModel.value;
	 data.Root.Checklist.Vehicle.Vin = txtVehicleVIN.value;
	 hideModal();
	 ReshowTask();
	 restoreConfirmations();
}

function editLienHolder() {
    saveConfirmations();
	 hideModal();
	 showEdit("divLienHolderEdit");
	 txtLHName.value = data.Root.Checklist.LienHolder.Name;
	 txtLHAddress1.value = data.Root.Checklist.LienHolder.Address1;
	 txtLHAddress2.value = data.Root.Checklist.LienHolder.Address2;
	 txtLHAddressCity.value = data.Root.Checklist.LienHolder.AddressCity;
	 txtLHAddressState.value = data.Root.Checklist.LienHolder.AddressState;
	 txtLHAddressZip.value = data.Root.Checklist.LienHolder.AddressZip;
	 txtLHFedTaxID.value = data.Root.Checklist.LienHolder.FedTaxID;
	 txtLHEmailAddress.value = data.Root.Checklist.LienHolder.EmailAddress;

	 chkSameAsGeneral.value == "1";
	 txtLHBillingName.value = data.Root.Checklist.LienHolder.BillingName;
	 txtLHBillingAddress1.value = data.Root.Checklist.LienHolder.BillingAddress1;
	 txtLHBillingAddress2.value = data.Root.Checklist.LienHolder.BillingAddress2;
	 txtLHBillingAddressCity.value = data.Root.Checklist.LienHolder.BillingAddressCity;
	 txtLHBillingAddressState.value = data.Root.Checklist.LienHolder.BillingAddressState;
	 txtLHBillingAddressZip.value = data.Root.Checklist.LienHolder.BillingAddressZip;

	 txtLHEFTAccountNumber.value = data.Root.Checklist.LienHolder.EFTAccountNumber;
	 txtLHEFTRoutingNumber.value = data.Root.Checklist.LienHolder.EFTRoutingNumber;
}

function updateLienHolder() {
	 data.Root.Checklist.LienHolder.Name = txtLHName.value;
	 data.Root.Checklist.LienHolder.Address1 = txtLHAddress1.value;
	 data.Root.Checklist.LienHolder.Address2 = txtLHAddress2.value;
	 data.Root.Checklist.LienHolder.AddressCity = txtLHAddressCity.value;
	 data.Root.Checklist.LienHolder.AddressState = txtLHAddressState.value;
	 data.Root.Checklist.LienHolder.AddressZip = txtLHAddressZip.value;
	 data.Root.Checklist.LienHolder.FedTaxID = txtLHFedTaxID.value;
	 data.Root.Checklist.LienHolder.EmailAddress = txtLHEmailAddress.value;

	 data.Root.Checklist.LienHolder.BillingName = txtLHBillingName.value;
	 data.Root.Checklist.LienHolder.BillingAddress1 = txtLHBillingAddress1.value;
	 data.Root.Checklist.LienHolder.BillingAddress2 = txtLHBillingAddress2.value;
	 data.Root.Checklist.LienHolder.BillingAddressCity = txtLHBillingAddressCity.value;
	 data.Root.Checklist.LienHolder.BillingAddressState = txtLHBillingAddressState.value;
	 data.Root.Checklist.LienHolder.BillingAddressZip = txtLHBillingAddressZip.value;

	 data.Root.Checklist.LienHolder.EFTAccountNumber = txtLHEFTAccountNumber.value;
	 data.Root.Checklist.LienHolder.EFTRoutingNumber = txtLHEFTRoutingNumber.value;

	 hideModal();
	 ReshowTask();
	 restoreConfirmations();
}

function editTL() {
    saveConfirmations();
    hideModal();
    showEdit("divTLEdit");
    txtTLPayoff.value = data.Root.Checklist.TL.PayoffAmount;
    txtTLSettlement.value = data.Root.Checklist.TL.SettlementAmount;
	 txtTLAdvance.value = data.Root.Checklist.TL.AdvanceAmount;
	 txtTLLoG.value = data.Root.Checklist.TL.LetterofGauranteeAmount;
	 txtLHLienHolderAccountNumber.value = data.Root.Checklist.TL.LienHolderAccountNumber;
	 txtTLPayoff.setFocus();
    blnTLEdit = true;
}

function updateTL() {
   var blnLienHolder = typeof (data.Root.Checklist.LienHolder) == "object";
   if (blnLienHolder) {
      var settlementAmount = parseFloat(txtTLSettlement.value);
      var payoffAmount = parseFloat(txtTLPayoff.value);
      var LoGAmount = parseFloat(txtTLPayoff.value);

      if (LoGAmount > payoffAmount && LoGAmount > settlementAmount) {
         var strRet = YesNoMessage("Confirm", "Letter of Guarantee amount is not equal to the lesser of the Settlement and Payoff Amount.\n\n" +
                              "Do you want the system to update the Letter of Guarantee Amount to the lesser of Settlement and Payoff Amount?");
         switch (strRet) {
            case "Yes":
               updateLoG();
               break;
            case "No":
               break;
            default:
               break;
         }
      }
   }
   data.Root.Checklist.TL.PayoffAmount = txtTLPayoff.value;
   data.Root.Checklist.TL.SettlementAmount = txtTLSettlement.value;
   data.Root.Checklist.TL.AdvanceAmount = txtTLAdvance.value;
   data.Root.Checklist.TL.LetterofGauranteeAmount = txtTLLoG.value;
   data.Root.Checklist.TL.LeinHolderAccountNumber = txtLHLienHolderAccountNumber.value;
   blnTLEdit = false;
   hideModal();
   ReshowTask();
   restoreConfirmations();}

function updateLoG() {
   if (blnTLEdit == false) return;
   if (txtTLSettlement.value != "" && txtTLPayoff.value != "") {
      var settlementAmount = parseFloat(txtTLSettlement.value);
      var payoffAmount = parseFloat(txtTLPayoff.value);
      txtTLLoG.value = (settlementAmount < payoffAmount ? txtTLSettlement.value : txtTLPayoff.value);
   }
}

function getEditedData() {
   var strRet = "";
   var strEdit = getEditedVOData();
   strRet += (strEdit != "" ? strEdit + "\n" : "");

   strEdit = getEditedVehicleData();
   strRet += (strEdit != "" ? strEdit + "\n" : "");

   strEdit = getEditedLienHolderData();
   strRet += (strEdit != "" ? strEdit + "\n" : "");

   strEdit = getEditedTLData();
   strRet += (strEdit != "" ? strEdit + "\n" : "");

   return strRet;
}

function getEditedVOData() {
   var strRet = "";
   var objVO = xmlTaskData.selectSingleNode("/Root/Checklist/VehicleOwner");
   if (objVO){
      if (data.Root.Checklist.VehicleOwner.NameLast != objVO.getAttribute("NameLast")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Last Name: " + data.Root.Checklist.VehicleOwner.NameLast + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.NameFirst != objVO.getAttribute("NameFirst")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "First Name: " + data.Root.Checklist.VehicleOwner.NameFirst + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.NameBusiness != objVO.getAttribute("NameBusiness")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Business Name: " + data.Root.Checklist.VehicleOwner.NameBusiness + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.Address1 != objVO.getAttribute("Address1")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Address 1: " + data.Root.Checklist.VehicleOwner.Address1 + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.Address2 != objVO.getAttribute("Address2")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Address 2: " + data.Root.Checklist.VehicleOwner.Address2 + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.AddressCity != objVO.getAttribute("AddressCity")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Address City: " + data.Root.Checklist.VehicleOwner.AddressCity + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.AddressState != objVO.getAttribute("AddressState")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Address State: " + data.Root.Checklist.VehicleOwner.AddressState + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.AddressZip != objVO.getAttribute("AddressZip")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Address Zip: " + data.Root.Checklist.VehicleOwner.AddressZip + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.Phone != objVO.getAttribute("Phone")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Phone: " + formatPhone(data.Root.Checklist.VehicleOwner.Phone) + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.AltPhone != objVO.getAttribute("AltPhone").Trim()) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "Alt. Phone: " + formatPhone(data.Root.Checklist.VehicleOwner.AltPhone) + "\n";
      }
      if (data.Root.Checklist.VehicleOwner.EmailAddress != objVO.getAttribute("EmailAddress")) {
         strRet += (strRet == "" ? "Vehicle Owner Update:\n" : "") + "EmailAddress: " + data.Root.Checklist.VehicleOwner.EmailAddress + "\n";
      }
   }
   return strRet;
}

function getEditedVehicleData() {
   var strRet = "";
   var obj = xmlTaskData.selectSingleNode("/Root/Checklist/Vehicle");
   if (obj) {
      if (data.Root.Checklist.Vehicle.Year != obj.getAttribute("Year")) {
         strRet += (strRet == "" ? "Vehicle Update:\n" : "") + "Year: " + data.Root.Checklist.Vehicle.Year + "\n";
      }
      if (data.Root.Checklist.Vehicle.Make != obj.getAttribute("Make")) {
         strRet += (strRet == "" ? "Vehicle Update:\n" : "") + "Make: " + data.Root.Checklist.Vehicle.Make + "\n";
      }
      if (data.Root.Checklist.Vehicle.Model != obj.getAttribute("Model")) {
         strRet += (strRet == "" ? "Vehicle Update:\n" : "") + "Model: " + data.Root.Checklist.Vehicle.Model + "\n";
      }
      if (data.Root.Checklist.Vehicle.Vin != obj.getAttribute("Vin")) {
         strRet += (strRet == "" ? "Vehicle Update:\n" : "") + "Vin: " + data.Root.Checklist.Vehicle.Vin + "\n";
      }
   }
   return strRet;
}

function getEditedLienHolderData() {
   var strRet = "";
   var obj = xmlTaskData.selectSingleNode("/Root/Checklist/LienHolder");
   if (obj) {
      //alert(obj.xml);
      if (data.Root.Checklist.LienHolder.Name != obj.getAttribute("Name")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Name: " + data.Root.Checklist.LienHolder.Name + "\n";
      }
      if (data.Root.Checklist.LienHolder.Address1 != obj.getAttribute("Address1")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Address1: " + data.Root.Checklist.LienHolder.Address1 + "\n";
      }
      if (data.Root.Checklist.LienHolder.Address2 != obj.getAttribute("Address2")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Address2: " + data.Root.Checklist.LienHolder.Address2 + "\n";
      }
      if (data.Root.Checklist.LienHolder.AddressCity != obj.getAttribute("AddressCity")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "City: " + data.Root.Checklist.LienHolder.AddressCity + "\n";
      }
      if (data.Root.Checklist.LienHolder.AddressState != obj.getAttribute("AddressState")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "State: " + data.Root.Checklist.LienHolder.AddressState + "\n";
      }
      if (data.Root.Checklist.LienHolder.AddressZip != obj.getAttribute("AddressZip")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Zip: " + data.Root.Checklist.LienHolder.AddressZip + "\n";
      }
      if (data.Root.Checklist.LienHolder.FedTaxID != obj.getAttribute("FedTaxID")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "FedTaxID: " + data.Root.Checklist.LienHolder.FedTaxID + "\n";
      }

      if (data.Root.Checklist.LienHolder.BillingName != obj.getAttribute("BillingName")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing Name: " + data.Root.Checklist.LienHolder.BillingName + "\n";
      }
      if (data.Root.Checklist.LienHolder.BillingAddress1 != obj.getAttribute("BillingAddress1")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing Address1: " + data.Root.Checklist.LienHolder.BillingAddress1 + "\n";
      }
      if (data.Root.Checklist.LienHolder.BillingAddress2 != obj.getAttribute("BillingAddress2")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing Address2: " + data.Root.Checklist.LienHolder.BillingAddress2 + "\n";
      }
      if (data.Root.Checklist.LienHolder.BillingAddressCity != obj.getAttribute("BillingAddressCity")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing City: " + data.Root.Checklist.LienHolder.BillingAddressCity + "\n";
      }
      if (data.Root.Checklist.LienHolder.BillingAddressState != obj.getAttribute("BillingAddressState")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing State: " + data.Root.Checklist.LienHolder.BillingAddressState + "\n";
      }
      if (data.Root.Checklist.LienHolder.BillingAddressZip != obj.getAttribute("BillingAddressZip")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "Billing Zip: " + data.Root.Checklist.LienHolder.BillingAddressZip + "\n";
      }
      if (data.Root.Checklist.LienHolder.EFTAccountNumber != obj.getAttribute("EFTAccountNumber")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "EFT Account Number: " + data.Root.Checklist.LienHolder.EFTAccountNumber + "\n";
      }
      if (data.Root.Checklist.LienHolder.EFTRoutingNumber != obj.getAttribute("EFTRoutingNumber")) {
         strRet += (strRet == "" ? "Lien Holder Update:\n" : "") + "EFT Routing Number: " + data.Root.Checklist.LienHolder.EFTRoutingNumber + "\n";
      }
   }
   return strRet;
}

function getEditedTLData() {
   var strRet = "";
   var obj = xmlTaskData.selectSingleNode("/Root/Checklist/TL");
   if (obj) {
      if (data.Root.Checklist.TL.PayoffAmount != obj.getAttribute("PayoffAmount")) {
         strRet += (strRet == "" ? "Total Loss Data:\n" : "") + "Payoff Amount: " + data.Root.Checklist.TL.PayoffAmount + "\n";
      }
      if (data.Root.Checklist.TL.SettlementAmount != obj.getAttribute("SettlementAmount")) {
         strRet += (strRet == "" ? "Total Loss Data:\n" : "") + "Settlement Amount: " + data.Root.Checklist.TL.SettlementAmount + "\n";
      }
      if (data.Root.Checklist.TL.AdvanceAmount != obj.getAttribute("AdvanceAmount")) {
         strRet += (strRet == "" ? "Total Loss Data:\n" : "") + "Advance Amount: " + data.Root.Checklist.TL.AdvanceAmount + "\n";
      }
      if (data.Root.Checklist.TL.LetterofGauranteeAmount != obj.getAttribute("LetterofGauranteeAmount")) {
         strRet += (strRet == "" ? "Total Loss Data:\n" : "") + "Letter of Guarantee Amount: " + data.Root.Checklist.TL.LetterofGauranteeAmount + "\n";
      }
      if (data.Root.Checklist.TL.LeinHolderAccountNumber != obj.getAttribute("LeinHolderAccountNumber")) {
         strRet += (strRet == "" ? "Total Loss Data:\n" : "") + "Loan Account #: " + data.Root.Checklist.TL.LeinHolderAccountNumber + "\n";
      }
   }	      
   return strRet;
}

function saveConfirmations() {
   arrConfirmations = new Array();
   arrRadioConfirmations = new Array();
   strSource = strScriptSource;
   var strConfirmations = "";
   if (strSource.indexOf("#") != -1) strSource = strSource.substring(1);
   var obj = document.getElementById(strSource);
   if (obj) {
      var objChk = obj.getElementsByTagName("APDCheckBox");
      for (var i = 0; i < objChk.length; i++) {
         arrConfirmations.push(objChk[i].value);
      }
      var objChk = obj.getElementsByTagName("APDRadio");
      for (var i = 0; i < objChk.length; i++) {
         arrRadioConfirmations.push(objChk[i].value);
      }
   }
   intEscalate = chkEscalate.value;
   strNotes = txtCallNotes.value;
   intScrollTop = divScript.scrollTop;
}

function restoreConfirmations() {
   if (arrConfirmations !== null) {
      strSource = strScriptSource;
      var strConfirmations = "";
      if (strSource.indexOf("#") != -1) strSource = strSource.substring(1);
      var obj = document.getElementById(strSource);
      if (obj) {
         var objChk = obj.getElementsByTagName("APDCheckBox");
         for (var i = 0; i < objChk.length; i++) {
            objChk[i].value = arrConfirmations[i];
         }
         var objChk = obj.getElementsByTagName("APDRadio");
         for (var i = 0; i < objChk.length; i++) {
            objChk[i].value = arrRadioConfirmations[i];
            if (typeof (objChk[i].onchange) == "function") {
               //alert("trigger");
               window.setTimeout(objChk[i].id + ".onchange()", 100);
            }
         }
      }
   }
   chkEscalate.value = intEscalate;
   txtCallNotes.value = strNotes;
   if (intScrollTop > 0) {
      divScript.scrollTop = intScrollTop;
      intScrollTop = -1;
   }
}

function validateConfirmations() {
   if (selCallType.selectedIndex == -1) {
      alert("Please select an item from the Call Resolution.");
      selCallType.focus();
      return false;
   }
   if (txtCallNotes.value == "") {
      alert("Enter the notes for this Call Resolution.");
      txtCallNotes.setFocus();
      return false;
   }

   var strSource;

   strSource = strScriptSource;
   var strConfirmations = "";
   if (strSource.indexOf("#") != -1) strSource = strSource.substring(1);
   var obj = document.getElementById(strSource);

   if (selCallType.value == "NoContact") {
      if (obj) {
         var objChk = obj.getElementsByTagName("APDCheckBox");
         for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].value == "1") {
               alert("Cannot have items confirmed when the Call Resolution is \"No Contact\"");
               return false;
            }
         }
      }
   } else {
      if (obj) {
         var objChk = obj.getElementsByTagName("APDCheckBox");
         for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].value != "1") {
               if (objChk[i].parentElement.parentElement.className != "hidden") {
                  alert(objChk[i].label + " needs to be confirmed. If this information was read out to the callee please mark the item as checked.");
                  objChk[i].setFocus();
                  return false;
               }
            }
         }
         if (strTaskID == "93") {
            if (rbPoAYesNo.value == null) {
               alert("Please select Yes or No Power Of Attorney question.");
               return false;
            }
            if (rbKeys.value == null) {
               alert("Please select a value for Key(s) to vehicle question.");
               return false;
            }
         }
      }
   }
   return true;
}

function getUserConfirmations() {
   var strSource;

   strSource = strScriptSource;
   var strConfirmations = "";
   if (selCallType.value != "NoContact") {
      if (strSource.indexOf("#") != -1) strSource = strSource.substring(1);
      var obj = document.getElementById(strSource);
      if (obj) {
         var objChk = obj.getElementsByTagName("APDCheckBox");
         for (var i = 0; i < objChk.length; i++) {
            if (objChk[i].parentElement.parentElement.className != "hidden") {
               strConfirmations += objChk[i].label + (objChk[i].value == "1" ? " Confirmed.\n" : " Not Confirmed.\n");
            }
         }
      }
      if (strTaskID == "93") {
         strConfirmations += (rbPoAYesNo.value == 1 ?
            "Vehicle Owner confirmed receipt of Power of Attorney document.\n" :
            "Vehicle owner requires a Power of Attorney document sent to his " + (data.Root.Checklist.VehicleOwner.EmailAddress == "" ? "mailing address" : "email address") + ".\n");
         strConfirmations += (rbKeys.value == 1 ?
            "Vehicle Owner does not have the key(s) to the vehicle" : "Vehicle Owner will send the key(s) with the POA") + ".\n";
      }
   } else {
      strConfirmations = "No Contact.";
   }
   return strConfirmations;
}

function commitTask() {
   if (validateConfirmations() == false) return;
   //return;
   //alert(getEditedData());alert(getUserConfirmations()); return;
   //alert(rbPoAYesNo.value);return;

   btnCommit.CCDisabled = true;
   btnClaimDetails.CCDisabled = true;

   window.clearInterval(iTimer);
   iTimer = null;
   
   var sProc = "uspTLTaskComplete";
   sRequest = "CheckListID=" + strChecklistID +
              "&StatusCD=" + (selCallType.value == "Contacted" ? "S" : "F") +
              "&EscalateFlag=" + chkEscalate.value +
              "&TaskNotes=" + escape(divTaskName.innerText +
                                       (selCallType.value == "Contacted" ? " task was completed.\n" : " task was delayed.\n") + 
                                       txtCallNotes.value + "\nConfirmations:\n" + getUserConfirmations()) +
              "&EditNotes=" + escape(getEditedData()) +
              "&UserID=" + strUserID;
   //alert(sRequest);
   //return;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   //alert(sXMLRequest); return;
   var objRet = XMLSave(sXMLRequest);
   if (objRet) {
      if (objRet.code == 0) {
         //alert("Task was committed successfully.");
         $.gritter.add({ title: "Task Commit", text: "Task was committed successfully.", time: 2000 }); 
         if (chkFireMode.value == "1") {
            getNextTask();
         } else {
            hideModal();
            startPageRefresh();
         }
         
      } else {
         $.gritter.add({ title: "Task Commit", text: "An error occurred while commiting the task.", time: 2000 });
         alert("Error commiting the task.");
      }
   }
}

function showClaimHistory() {
   NavOpenWindow("claim", strLynxID, strVehNum);
}

function unlockTask() {
   var sProc = "uspTLTaskExpire";
   sRequest = "CheckListID=" + strChecklistID +
              "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   //alert(sXMLRequest); return;
   var objRet = XMLSave(sXMLRequest);
}

function slideExpiration() {
   var sProc = "uspTLTaskSlideExpiration";
   sRequest = "CheckListID=" + strChecklistID +
              "&UserID=" + strUserID;
   var aRequests = new Array();
   aRequests.push({ procName: sProc,
      method: "ExecuteSpNp",
      data: sRequest
   });
   var sXMLRequest = makeXMLSaveString(aRequests);
   //alert(sXMLRequest); return;
   XMLSave(sXMLRequest);
}

function startPageRefresh() {
   window.setTimeout("reloadQueue()", 1000 * 60 * 60); //reload the queue every hour when no task is attended to
}

function stopPageRefresh() {
   iTimePageRefresh = null;;
}

function reloadQueue() {
   initQueue();
}

function updateBillingAddress() {
   var blnChecked = chkSameAsGeneral.value == "1";

   txtLHBillingName.CCDisabled = false;
   txtLHBillingAddress1.CCDisabled = false;
   txtLHBillingAddress2.CCDisabled = false;
   txtLHBillingAddressCity.CCDisabled = false;
   txtLHBillingAddressState.CCDisabled = false;
   txtLHBillingAddressZip.CCDisabled = false;

   if (blnChecked) {
      txtLHBillingName.value = txtLHName.value;
      txtLHBillingAddress1.value = txtLHAddress1.value;
      txtLHBillingAddress2.value = txtLHAddress2.value;
      txtLHBillingAddressCity.value = txtLHAddressCity.value;
      txtLHBillingAddressState.value = txtLHAddressState.value;
      txtLHBillingAddressZip.value = txtLHAddressZip.value;
   }

   txtLHBillingName.CCDisabled = blnChecked;
   txtLHBillingAddress1.CCDisabled = blnChecked;
   txtLHBillingAddress2.CCDisabled = blnChecked;
   txtLHBillingAddressCity.CCDisabled = blnChecked;
   txtLHBillingAddressState.CCDisabled = blnChecked;
   txtLHBillingAddressZip.CCDisabled = blnChecked;

}

function showPoAScript() {
   $("#divVOInitialContactPoAYes").removeClass().addClass(rbPoAYesNo.value == "1" ? "visible" : "hidden");
   $("#divVOInitialContactPoANo").removeClass().addClass(rbPoAYesNo.value == "0" ? "visible" : "hidden");
}

function doShowTask() {
   //alert("here");
   if (strQueueUser != "1") {
      var objSelRow = grdTLQueue.selectedRow;
      if (objSelRow.objXML) {
         var strChecklistID = objSelRow.objXML.getAttribute("ID");
         getNextTask(strChecklistID);
      }
   }
}