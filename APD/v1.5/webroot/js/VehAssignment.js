// Send/Resend assignment
function SendAssignment(resendFlag, assignmentFlag) {

    if (parent.stat1) {
        parent.stat1.Show("Please wait...");
    }

    window.setTimeout("SendAssignment2(" + resendFlag + ",'" + assignmentFlag + "')", 100);
}

function SendAssignment2(resendFlag, assignmentFlag) {

    var sProc, sRequest;
    disableButtons(true);

    // Check for changes in Shop Remarks
    if (txtAssignmentRemarks.isDirty) {

        ClientWarning("Assignment data has changed. Please save the data and try again.");
        disableButtons(false);
        return;
        //saveData();
    }


    if (parseFloat(txtEffDedSent.value) > 1000) {
        if (YesNoMessage("Confirm Effective Deductible", "Effective Deductible Sent value is greater than $1000. Is this value correct?") != "Yes") {
            disableButtons(false);
            return;
        }
    }

    if (resendFlag == 0) {
        sProc = "uspWorkflowAssignShop";
        if (assignmentFlag == 'LDAU')
            gsAssignmentID = gsAssignmentID1;
        sRequest = "AssignmentID=" + gsAssignmentID;



        var aRequests = new Array();
        aRequests.push({ procName: sProc,
            method: "executespnp",
            data: sRequest
        }
                    );
        var objRet = XMLSave(makeXMLSaveString(aRequests));
    }


    if (gbCEIFlag == true) {
        sProc = "uspWorkflowNotifyEvent";

        sRequest = "EventID=" + gsVehicleAssignmentSentToShopCD +
                  "&ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                  "&Description=" + "C-PROGRAM SHOP(NP): Assignment for Vehicle " + gsVehNum + " was sent to " + gsShopName + " (Shop ID " + gsShopId + ")" +
                  "&ConditionValue=CEI" +
                  "&UserID=" + gsUserID;



        var aRequests = new Array();
        aRequests.push({ procName: sProc,
            method: "executespnp",
            data: sRequest
        }
                    );
        var objRet = XMLSave(makeXMLSaveString(aRequests));

        disableButtons(false);
        document.location.reload();
    } else {
        // executing the remote script in a separate thread so that the user can do other things while the assignment is being sent.
        //window.setTimeout('RSExecute( "/rs/RSAssignShop.asp", "AssignShop", gsAssignmentID, gsUserID, false, showFaxResults, showFaxErrors, null )', 100);

        var strStaffAppraiser = "false";
        if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR" || (strServiceChannelCD == "RRP" && assignmentFlag == "LDAU")) {

            strStaffAppraiser = "true";
        }

        if (assignmentFlag == "LDAU")
            gsAssignmentID = gsAssignmentID1;

        sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + "\" userID=\"" + gsUserID + "\" staffAppraiser=\"" + strStaffAppraiser + "\" hqAssignment=\"" + "true" + "\" /></Root>";

        //alert("calling com..\n" + sRequest);

        var objRet = XMLServerExecute("AssignShop.asp", sRequest);


        if (objRet.code == 0) {
            var strRet;

            if (objRet.xml) {
                var oError = objRet.xml.selectSingleNode("//Error");

                if (oError) {

                    var strMessage = oError.text;

                    var iPosExtMsg = strMessage.indexOf("External Message");
                    var strMessage = strMessage.substring(iPosExtMsg + "External Message".length + 8, strMessage.indexOf("<|", iPosExtMsg));
                    ClientWarning(strMessage);

                    disableButtons(false);
                    if (parent.stat1)
                        parent.stat1.Hide();
                    return;
                } else {

                    var oResult = objRet.xml.selectSingleNode("/Root/Success");
                    if (oResult) {
                        var strStatus = oResult.text;
                        var strVanResult = strStatus.substring(0, 1);
                        var strFaxResult = strStatus.substring(1, 1);

                        if (strServiceChannelCD == "RRP" && assignmentFlag == "LDAU") {

                            if (strVanResult == "F") ClientInfo("Estimate Preload failed.");
                            if (strFaxResult == "F") ClientInfo("Fax assignment to shop failed.");
                        } else

                            if (strVanResult == "F") ClientInfo("Electronc assignment to shop failed.");
                        if (strFaxResult == "F") ClientInfo("Fax assignment to shop failed.");

                        if (strServiceChannelCD == "ME" ||
                      strServiceChannelCD == "GL" ||
                      strServiceChannelCD == "DA") {
                            var sProc, sRequest;


                            sProc = "uspWorkflowNotifyEvent";

                            sRequest = "EventID=" + gsAppraiserAssignmentSentEventID +
                                "&ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                                "&Description=" + "Appriaser Assignment for Vehicle " + gsVehNum + " was sent to " + txtAssignedTo.value +
                                "&UserID=" + gsUserID;

                            var aRequests = new Array();
                            aRequests.push({ procName: sProc,
                                method: "executespnp",
                                data: sRequest
                            }
                                 );
                            var objRet = XMLSave(makeXMLSaveString(aRequests));
                        }

                        if (typeof (parent.refreshVehicle) == "function") {

                            parent.refreshVehicle();
                            return;
                        } else
                            document.location.reload();
                    } else {
                        ClientWarning("Assignment Failed. Code 003");
                    }
                }
            } else {
                ClientWarning("Assignment failed. Code 002");
            }
        } else {
            ClientWarning("Assignment Failed. Code 001");
        }
    }
    document.location.reload();

}

// Check results from AssignShop
function showFaxResults(co) {
    var retArray = ValidateRS(co);
    if (retArray[1] == 1) {
        var sResult = retArray[0];
        var sVanResult = sResult.charAt(0);
        var sFaxResult = sResult.charAt(1);
        if (sVanResult == "F") ClientInfo("Electronic assignment to shop failed.");
        if (sFaxResult == "F") ClientInfo("Fax assignment to shop failed.");
        //parent.onShopAssignment();
        document.location.reload();
    }
    disableButtons(false);
}

// Show errors from AssignShop
function showFaxErrors(co) {
    var msg = "The following error occurred during the '" + co.context + "' remote script call: '"
        + co.message + "'/n  The raw data returned by the remote method call is: " + co.data;
    ClientEvent(51, msg, "VehicleShopAssignment.xsl showFaxErrors()");
}

// Cancel Assignment to shop
function CancelAssignedShop() {
    var sProc, sRequest;
    disableButtons(true);
    // Check for changes in Shop Remarks
    if (txtAssignmentRemarks.isDirty) {
        ClientWarning("Assignment data has changed. Please save the data and try again.");
        disableButtons(false);
        return;
        //saveData();
    }
    //gsShopId = "";
    sShopRemarks = "";
    sProc = "uspWorkFlowSelectShop";

    if (gsClaimAspectServiceChannelID != "") {
        //do not send the vehicle number or the LynxID when the claimAspectID is available
        sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID + "&SelectOperationCD=C&UserId=" + gsUserID + "&AssignmentRemarks=" + sShopRemarks + "&NotifyEvent=1";
        sRequest += (gsLDAUAppraiserTypeCD == "S" ? "&ShopLocationID=" + gsShopId : "&AppraiserID=");
        //var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", "uspWorkFlowSelectShop", "LynxId=&VehicleNumber=&ClaimAspectID=" + gsClaimAspectID + "&SelectOperationCD=C&UserId="+gsUserID+"&ShopLocationId="+gsShopId+"&AssignmentRemarks="+sShopRemarks+"&NotifyEvent=1");
    } else {
        //when claimAspectID is not available then send in the LynxID and the vehicle Number.
        sRequest = "LynxId=" + gsLynxID + "&VehicleNumber=" + gsVehNum + "&ClaimAspectID=&SelectOperationCD=C&UserId=" + gsUserID + "&AssignmentRemarks=" + sShopRemarks + "&NotifyEvent=1"
        sRequest += (gsLDAUAppraiserTypeCD == "S" ? "&ShopLocationID=" + gsShopId : "&AppraiserID=");
        //var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", "uspWorkFlowSelectShop", "LynxId="+gsLynxID+"&VehicleNumber=" + gsVehNum + "&ClaimAspectID=&SelectOperationCD=C&UserId="+gsUserID+"&ShopLocationId="+gsShopId+"&AssignmentRemarks="+sShopRemarks+"&NotifyEvent=1");
    }

    var aRequests = new Array();
    aRequests.push({ procName: sProc,
        method: "executespnp",
        data: sRequest
    }
                  );
    //alert(makeXMLSaveString(aRequests));
    //return;
    var objRet = XMLSave(makeXMLSaveString(aRequests));

    //var retArray = ValidateRS( co );
    //alert(AssignmentTo.value); return;
    // we don't want to send cancel electronic notification for Mobile Electronics (IA)
    if ((strServiceChannelCD != 'ME') && (objRet && objRet.code == 0) && (gsVanId == 12 || gsVanId == 13 || gsVanId == 14)) {
        //RSExecute( "/rs/RSAssignShop.asp", "AssignShop", gsAssignmentID, gsUserID, false, showFaxResults, showFaxErrors, null );
        sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + "\" userID=\"" + gsUserID + "\" staffAppraiser=\"false\" hqAssignment=\"true\" /></Root>";
        //alert("calling com..\n" + sRequest);
        var objRet = XMLServerExecute("AssignShop.asp", sRequest);
        if (objRet.code == 0) {
            var strRet;
            if (objRet.xml) {
                //alert(objRet.xml.xml);
                var oResult = objRet.xml.selectSingleNode("/Root/Success");
                if (oResult) {
                    var strStatus = oResult.text;

                    if (strStatus.substring(0, 1) == "F") ClientInfo("Electronic cancel assignment to shop failed.");
                    if (strStatus.substring(1, 1) == "F") ClientInfo("Fax cancel assignment to shop failed.");

                    document.location.reload();
                } else {
                    ClientWarning("Assignment Cancel Failed");
                }
            } else {
                ClientWarning("Assignment Cancel failed.");
            }
        } else {
            ClientWarning("Assignment Cancel Failed.");
        }
    } else
        document.location.reload();
    //parent.onShopAssignment();
    //disableButtons(false);
}

// Generic routine for handling results from our remote script calls.
function ValidateRS(oReturn) {
    var aReturn = new Array()

    aReturn[0] = oReturn.message;
    aReturn[1] = null;

    if (oReturn.status == -1)
        ServerEvent();
    else {
        aReturn = oReturn.return_value.split("||");

        if (aReturn[1] == 0) {
            ServerEvent();
            aReturn[1] = 0;
        }
    }

    return aReturn;
}

function submit2Glass() {
    if (xmlGlass) {
        //alert(xmlGlass.xml);
        //send the glass claim to AGC
        var objRet = XMLServerExecute("../FNOLAssignment.asp", xmlGlass.xml);
        if (objRet.code == 0) {
            var strRet;
            var oXMLRet = objRet.xml;
            if (oXMLRet) {
                if (oXMLRet.selectSingleNode("//Error") || oXMLRet.selectSingleNode("//error")) {
                    var strReason = "";
                    var objReason = oXMLRet.selectSingleNode("//description");
                    if (objReason) {
                        strReason = objReason.text;
                    }
                    ClientWarning("Glass Assignment failed." + (strReason != "" ? "<br/><br/>Reason: " + strReason : ""));
                } else {
                    //alert(oXMLRet.xml);
                    var oMsg = oXMLRet.selectSingleNode("//records/record/@msg_text");
                    var oRefNumber = oXMLRet.selectSingleNode("//records/record/@loss_no");

                    if (oRefNumber) {
                        var sRefNumber = oRefNumber.text;
                        sRefNumber = sRefNumber.substring(1);

                        if (typeof (parent.setReferenceNumber) == "function") {
                            parent.setReferenceNumber(sRefNumber);
                        }

                        //now select the shop inorder to create the assignment record in APD
                        var strAssignmentID = execSelectShop(gsClaimAspectServiceChannelID, gsUserID, gsGLID, "");

                        if (strAssignmentID != "") {
                            //now update the assignment record with the reference id

                            var sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                                        "&AssignmentID=" + strAssignmentID +
                                        "&ReferenceID=" + escape(sRefNumber);

                            //alert(sRequest); return;

                            sProc = "uspAssignmentReferenceIDUpd";

                            var aRequests = new Array();
                            aRequests.push({ procName: sProc,
                                method: "executespnp",
                                data: sRequest
                            }
                                        );
                            //alert(makeXMLSaveString(aRequests));
                            //return;
                            var objRet = XMLSave(makeXMLSaveString(aRequests));
                            if (objRet.code == 0 && objRet.xml) {
                                document.location.reload();
                            }
                        }
                    }
                }

            } else {
                ClientWarning("Glass Assignment failed.");
            }
        } else {
            ClientWarning("Glass Assignment Failed.");
        }
    }

    if (parent.stat1) {
        parent.stat1.Hide();
    }
}

function execSelectShop(strClaimAspectServiceChannelID, strUserID, strAppraiserID, strRemarks) {
    var sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                   "&SelectOperationCD=S&UserId=" + strUserID +
                   "&AppraiserID=" + strAppraiserID +
                   "&AssignmentRemarks=" + strRemarks +
                   "&NotifyEvent=1";

    //alert(sRequest); return;

    sProc = "uspWorkFlowSelectShop";

    var aRequests = new Array();
    aRequests.push({ procName: sProc,
        method: "executespnp",
        data: sRequest
    }
                   );
    //alert(makeXMLSaveString(aRequests));
    //return;
    var objRet = XMLSave(makeXMLSaveString(aRequests));
    if (objRet.code == 0 && objRet.xml) {
        var oRetXML = objRet.xml;
        //alert(oRetXML.xml);
        var oResult = oRetXML.selectSingleNode("//Result");
        if (oResult) {
            strAssignmentID = oResult.text;
            //alert(gsAssignmentID);

            //now assignment LDAU
            sProc = "uspWorkflowAssignShop";

            sRequest = "AssignmentID=" + strAssignmentID;

            var aRequests = new Array();
            aRequests.push({ procName: sProc,
                method: "executespnp",
                data: sRequest
            }
                         );
            //alert(sRequest);
            //alert(makeXMLSaveString(aRequests));
            //return;
            var objRet = XMLSave(makeXMLSaveString(aRequests));
            if (objRet.code == 0) {
                return strAssignmentID;
            } else
                return "";
        }

    } else
        return "";
}
  