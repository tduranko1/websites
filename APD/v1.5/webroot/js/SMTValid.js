
var gbDirtyFlag = false;		


// Generic routine for handling results from our remote script calls.
function ValidateRS( oReturn )
{
    var aReturn = new Array()

    aReturn[0] = oReturn.message;
    aReturn[1] = null;

    if ( oReturn.status == -1 )
        ServerEvent();
    else
    {
        aReturn = oReturn.return_value.split("||");

        if ( aReturn[1] == 0 )
        {
            ServerEvent();
            aReturn[1] = 0;
        }
    }

    return aReturn;
}


function NumbersOnly(event)
{
  var bOK = false;
	if ((event.keyCode >= 45 && event.keyCode <= 57) && (event.keyCode != 47))  bOK = true;

    try {
        if (event.srcElement.allowDecimal != "undefined")
        {
        if (event.srcElement.allowDecimal == true){
            if (event.keyCode == 46) bOK = true;
        }
        }
        else {
	        if (event.keyCode == 46) bOK = false;
        }
    }
    catch(e) {
        bOK = false;
    }
    if (event.keyCode == 13) bOK = true;  //pass thru an <Enter> key
	if (event.shiftKey == true || bOK == false )
		event.returnValue = false;
}

function CheckDate(objName)
{
    var datefield = objName;
		if (objName.value == "") return false;
    if (objName.value.length < 6 || chkdate(objName) == false )
    {
        datefield.select();
        parent.ClientWarning("The entered date is invalid.<br>Please enter a valid date in \"mm/dd/yyyy\" format.");
        datefield.focus();
        return false;
    }
    else return true;
}


function chkdate(objName) {
  //if the passed in object does not exist in the document
  if (!objName) return false;
  var sDate = objName.value;
  
  var dt = new Date(sDate);
  
  if (isNaN(dt)) return false; //the value in the control is not a valid date.
  
  var iMonth, iDate, iYear;
  var sMonth, sDate;
  var strSeparator = "/";
  
  //get the year portion of the typed value
  for (var i = sDate.length - 1; i >= 0; i--){
    if (isNaN(parseInt(sDate.substr(i, 1)))) 
      break;
  }
  iYearTyped = parseInt(sDate.substr(i+1, sDate.length), 10);

  //if the year portion started like 0*, then we guess that the year was in the current century.
  if (iYearTyped < 10) {
    dtNow = new Date();
    var iCentury = parseInt(String(dtNow.getFullYear()).substr(0, 2), 10) * 100;
    dt.setFullYear(iCentury + iYearTyped);
  }
  
  iMonth = dt.getMonth();
  iDate = dt.getDate();
  iYear = dt.getFullYear();
  
  iMonth++; //getMonth() is a zero based month value.
  
  sMonth = (iMonth < 10 ? "0" + iMonth : iMonth); //pad the single digit month with a zero
  sDate = (iDate < 10 ? "0" + iDate : iDate); //pad the single digit date with a zero
  
  //this is hardcoded to be in mm/dd/yyyy format which is not flexible. need to revisit.
  objName.value = sMonth + strSeparator + sDate + strSeparator + iYear;
  
  return true;
  
}

function LeapYear(intYear) {
    if ( (intYear % 100 == 0) && ( ( (intYear % 400 == 0) || ((intYear % 4) == 0) ) ) )
        return true;
    return false;
}

function CheckTime(obj)
{
	// Checks if time is in HH:MM:SS AM/PM format.
	// The seconds and AM/PM are optional.
	var timeStr = String(obj.value);
	if (timeStr == "") return true;

	var TArray = timeStr.split(" ");
	if (TArray[2])
		timeStr = TArray[1] + " " + TArray[2];

	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|Am|aM|PM|pm|Pm|pM))?$/;

	var matchArray = timeStr.match(timePat);
	if (matchArray == null)
	{
		obj.select();
        parent.ClientWarning("Time is not in a valid format.<br>Please enter a valid time in the format HH:MM:SS A/PM");
		obj.focus();
		return false;
	}

	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];

	if (second=="") second = null;
	if (ampm=="") ampm = null;

	if (hour < 0  || hour > 23)
	{
		obj.select();
        parent.ClientWarning("Hour must be between 1 and 12 (or 0 and 23 for military time).");
		obj.focus();
		return false;
	}

	if (hour <= 12 && ampm == null)
	{
		if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time"))
		{
			obj.select();
            parent.ClientWarning("You must specify AM or PM.");
			obj.focus();
			return false;
        }
	}

	if  (hour > 12 && ampm != null)
	{
		obj.select();
        parent.ClientWarning("You can't specify AM or PM for military time.");
		obj.focus();
		return false;
	}

	if (minute<0 || minute > 59)
	{
		obj.select();
        parent.ClientWarning("Minute must be between 0 and 59.");
		obj.focus();
		return false;
	}

	if (second != null && (second < 0 || second > 59))
	{
		obj.select();
        parent.ClientWarning("Second must be between 0 and 59.");
		obj.focus();
		return false;
	}
	return true;
}

function CheckMilitaryTime()
{
	// Checks if time is valid Military time (no colon(:))

	var obj = eval('document.all.' + event.srcElement.id);

	var timeStr = String(obj.value);
	if (timeStr == "") return true;

	var nums = '0123456789';
	var len = timeStr.length;

	if (len != 4){
        parent.ClientWarning("This is not a valid Military time.  Must be in the form 'hhmm'.");
		obj.select();
		return false;
	}

	for (i=0;i<len;i++){
		if (nums.indexOf(timeStr.charAt(i)) == -1){
            parent.ClientWarning('Military times must contain only numbers.');
			obj.select();
			return false;
		}
	}

	var mins = timeStr.substr(len-2);
	var hrs = timeStr.substr(0, len-2);
    if (hrs < 0  || hrs > 24)
	{
        parent.ClientWarning("Hour must be between 0 and 24 for military time");
		obj.select();
		return false;
	}

	if (mins < 0 || mins > 59)
	{
        parent.ClientWarning("Minute must be between 0 and 59.");
		obj.select();
		return false;
	}

	if (hrs == 24 && mins != 0)
	{
        parent.ClientWarning("2400 is the maximum value allowed for military time.");
		obj.select();
		return false;
	}

	return true;
}


//These functions require RemoteScripting

function IsValidZip(obj)
{
    var sType = obj.getAttribute("type");
    if (sType == "password") return;

	if (obj.value.length == 0)
		return;

	if (obj.value.length != 5)
	{
		parent.ClientWarning("The entered zipcode length invalid.  Please restrict to 5 characters exactly.");
		obj.select();
		return;
	}

	var co = RSExecute("/rs/RSValidateZip.asp", "IsValidZip", obj.value);
	if (co.status == 0)
    {
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else
			if (listArray[0] == 0){
                parent.ClientWarning("The entered zipcode could not be found in our database.  Please enter a valid Zipcode.");
				obj.select();
				return;
			}
    }
    else ServerEvent();
}

function ValidateFedTaxID(obj){
  var sFedTaxID = obj.value;
  
  if (sFedTaxID.length == 0)
		return true;
  
  if (sFedTaxID.length != 9){
    parent.ClientWarning("The SSN/EIN, if entered, must be exactly 9 numeric characters.")
    obj.select();
    return false;
  }
  
  var sBusinessInfoID;
  switch (gsMode){
    case "wizard":
      sBusinessInfoID = "";
      break;
    case "merge":
      sBusinessInfoID = frmShop.txtShopID.value;
      break;
    default:
      sBusinessInfoID = top.gsBusinessInfoID;
      break;
  }
  
  var co = RSExecute("/rs/RSShopMaint.asp", "IsDuplicateFedTaxID", sFedTaxID, sBusinessInfoID);
	if (co.status == 0){
    sTblData = co.return_value;
    var listArray = sTblData.split("||");
    if ( listArray[1] == "0" )
      ServerEvent();
    else{
      if (listArray[0] == 0){
        var dlgFeatures = "dialogHeight:285px;dialogWidth:780px;center:yes;help:no;resizable:no;scroll:no;status:no;";
        var retVal = window.showModalDialog("SMTFedTaxID.asp?FedTaxID=" + sFedTaxID + "&BusinessInfoID=" + sBusinessInfoID + "&mode=" + gsMode, "", dlgFeatures);
        
        if (!isNaN(retVal) && retVal != "0"){
          if (gsMode == "wizard"){
            top.gsFramePageFile = "SMTDetailLevel.asp";
            top.gsPageFile = "SMTBusinessInfo.asp";
            top.gsCancelSearchType = "B";
            top.gsBusinessInfoID = retVal;
            top.gsShopID = "";
            parent.fnCancel();        
          }
          else{
            if (top.gsFramePageFile == "")
              window.navigate ("SMTDetailLevel.asp?SearchType=B&BusinessInfoID=" + retVal);
            else
              parent.navigate ("SMTDetailLevel.asp?SearchType=B&BusinessInfoID=" + retVal);
            return;
          }
        }
        
        obj.select();
				return false;
			}
      else
        return true;
    }
  }
  else
    ServerEvent();
}


function ValidateZip(obj, sStateBox, sCityBox )
{
	if (obj.value == "") return;

	if (obj.value.length != 5 )
	{
		obj.style.borderColor = "#7f0000";
		return;
	}

	var StateObj = document.getElementById(sStateBox);
	var CityObj = document.getElementById(sCityBox);

    RSExecute("/rs/RSValidateZip.asp", "ZipToState", obj.value, RSrefreshPage, RSerrHandler, StateObj);
    RSExecute("/rs/RSValidateZip.asp", "ZipToCity", obj.value, RSrefreshPage, RSerrHandler, CityObj);
}

function ValidateZipToCounty(obj, sCountyBox)
{
	if (obj.value == "") return;

	if (obj.value.length != 5 )
	{
		obj.style.borderColor = "#7f0000";
		return;
	}

	var CountyObj = document.getElementById(sCountyBox);
    RSExecute("/rs/RSValidateZip.asp", "ZipToCounty", obj.value, RSrefreshPage, RSerrHandler, CountyObj);
}

function RSrefreshPage(co)
{
	if (co.status == 0)
    {
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else if (co.context.value == "" || co.context.value == " " || co.context.value.indexOf("?") != -1)
			co.context.value = listArray[0];
    }
    else ServerEvent();
}

function RSerrHandler(co)
{
	if (co.status != 0)
	{
  	co.context.value = co.message;
  }
}


// Overrid to clip pastes to maxlen of textarea.
function onTextAreaPaste()
{
    var vMaxLen = parseInt(this.getAttribute("Maxlength"));
    this.value += window.clipboardData.getData("Text");

    if ( this.value.length > vMaxLen )
        this.value = this.value.substr( 0, vMaxLen );

    event.returnValue = false;

    onCutPasteSetDirty();
}



function LeapYear(year)
{
  if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
    return true;
  else
    return false;
}

function ChangeInputBorder(obj)
{
		obj.style.borderColor = "#7f0000";
}

function UTCConvertDate(vDate)
{
	if (vDate == "") return "";
	var vYear = vDate.substr(0,4);
  var vMonth = vDate.substr(5,2);
  var vDay = vDate.substr(8,2);
  vDate = vMonth + '/' + vDay  + '/' + vYear
  return vDate;
}

function UTCConvertTime(vDate)
{
    if (vDate == "") return "";
		var ampm = 'AM'
    var hh = vDate.substr(11,2)
    var mm = vDate.substr(14,2)
    var ss = vDate.substr(17,2)
    if (hh > 12)
    {
      hh = hh-12;
      ampm = 'PM';
    }
    vDate = hh + ':' + mm  + ':' + ss  + ' ' + ampm;
    return vDate;
}

function ValidatePhone(obj) {
	var sValue = String(obj.value);

	if ( sValue == "() -" || sValue == "" || obj.type == 'password') return;

	//retrieve all numeric characters found
	sValue = parseNumber(sValue);

	//this will check if numeric characters were found
	if ((sValue.length == 0) && (obj.value.length > 0)) {
        parent.ClientWarning("Invalid characters were found in the phone number. Only numeric characters are allowed.");
		obj.select();
		obj.focus();
		return;
	}

	//if any part of phone number specified, then all the sections must be completed.
	if ((sValue.length > 0) && (sValue.length < 10)) {
        parent.ClientWarning("Phone number must have an area code, an exchange and a number in the format (xxx) xxx-xxxx.");
		obj.select();
		obj.focus();
		return;
	}

	//this expression will extract the first 3 chars for area code, next 3 for exchange and the last 4 for phone number.
	//if the length of the phone number entered is greater than 10 (3+3+4) the remaining are truncated.
	var re = /^(\d{3})(\d{3})(\d{4})/;
	var ret = sValue.match(re);
	//ret can never be null. We would have reached this LOC only after passing the above check of length > 0 and < 10.

	//check if the area code is valid
	if (!isAreaCode(ret[1])) {
        parent.ClientWarning("Invalid area code specified.");
		obj.select();
		obj.focus();
		return;
	}

	if (!isPhoneExchange(ret[2])) {
        parent.ClientWarning("Invalid exchange specified.");
		obj.select();
		obj.focus();
		return;
	}

	//format the phone number and refresh the control.
	obj.value = "(" + ret[1]+ ") " + ret[2] + "-" + ret[3];
}

//checks if the input is an area code
function isAreaCode(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if (!(sVal.length == 3)) return false;
	//area code cannot start with 0 or 1
	if (parseInt(sVal.substr(0,1)) < 2) return false;

	return true;
}

//checks if the input is a phone exchange
function isPhoneExchange(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 3)) return false;
	//if (sValue == "000") return false;

	return true;
}

//checks if the input is a phone number
function isPhoneNumber(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 4)) return false;

	return true;
}

function phoneAutoTab(obj, objNext, objAttrib){
    if (obj) {
      if (obj.value.length == parseInt(obj.getAttribute("maxlength"))){
        switch (objAttrib) {
          case "A":
            if (!isAreaCode(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;
          case "E":
            if (!isPhoneExchange(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;
          case "U":
            if (!isPhoneNumber(obj.value)){
              obj.fireEvent("onbeforedeactivate");
              return;
            }
            break;          
        }
        if ((obj.defaultValue == "") || 
            (obj.getAttribute("prevValue") != "" && obj.value != obj.defaultValue && obj.getAttribute("prevValue") != obj.value) //obj.defaultValue != "" && 
           ){
          if (objNext){
            obj.setAttribute("defaultValue", obj.value, 0); 
            objNext.focus();
            objNext.select();
            //setControlDirty(objNext);
            SetDirty();
            return;
          }
        }
        if (obj.value == obj.defaultValue)
          obj.setAttribute("defaultValue", "-1", 0);
        if (obj.value == obj.getAttribute("prevValue"))
          obj.setAttribute("prevValue", "-1", 0);        
      }
    }
  }

//Check Area code
var bInCheckAreaCode = false;
function checkAreaCode(obj) {
  var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;
  
  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isAreaCode(sVal) && !(bInCheckAreaCode)) {

    event.returnValue = false;
    bInCheckAreaCode = true;
    parent.ClientWarning("Invalid phone area code specified.");
		if (obj !== objTo)
      obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckAreaCode = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Exchange
var bInCheckPhoneExchange = false;
function checkPhoneExchange(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isPhoneExchange(sVal) && !(bInCheckPhoneExchange)) {
    event.returnValue = false;
    bInCheckPhoneExchange = true;
    parent.ClientWarning("Invalid phone exchange specified.");
		obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneExchange = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Number
var bInCheckPhoneNumber = false;
function checkPhoneNumber(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (obj.type == "password") return;
	if (sVal.length > 0)
	if (!isPhoneNumber(sVal) && !(bInCheckPhoneNumber)) {
    event.returnValue = false;
    bInCheckPhoneNumber = true;
    parent.ClientWarning("Invalid phone number specified.");
		obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneNumber = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}


//function that will find missing phone sections and focus
function validatePhoneSections(objAreaCode, objExchange, objUnit) {
	var srcElementID = '';
    srcElementID = document.activeElement.id;
		if (objAreaCode.type == "password") return true;

    if (objAreaCode.id == srcElementID)
        if (objAreaCode.value.length > 0)
            if (!isAreaCode(objAreaCode.value)){
                objAreaCode.onblur();
                return false;
            }
		
    if (objExchange.id == srcElementID)
        if (objExchange.value.length > 0)
            if (!isPhoneExchange(objExchange.value)){
                objExchange.onblur();
                return false;
            }
    if (objUnit.id == srcElementID)
        if (objUnit.value.length > 0)
            if (!isPhoneNumber(objUnit.value)){
                objUnit.onblur();
                return false;
            }
    
  	if ((objAreaCode.value.length == 0) || (objExchange.value.length == 0) || (objUnit.value.length == 0)) {
	
        //all the sections are empty - this is OK.
      if ((objAreaCode.value.length == 0) && (objExchange.value.length == 0) && (objUnit.value.length == 0)) return true;
        
        if (isAreaCode(objAreaCode.value) && isPhoneExchange(objExchange.value) && isPhoneNumber(objUnit.value)) return true;
        //some sections are empty - not OK.
        parent.ClientWarning("Some sections of the phone number are missing.  Please enter a valid area code, exchange and phone number.");
        if ((objAreaCode.value.length == 0) || (!isAreaCode(objAreaCode.value))) {
            objAreaCode.focus();
            objAreaCode.select();
            return false;
        }
		if ((objExchange.value.length == 0) || (!isPhoneExchange(objExchange.value))) {
            objExchange.focus();
            objExchange.select();
            return false;
        }
        if ((objUnit.value.length == 0) || (!isPhoneNumber(objUnit.value))) {
            objUnit.focus();
            objUnit.select();
            return false;
        }
        //this line will never be executed. Just in case!!.
        //objAreaCode.focus();
        //objAreaCode.select();
        return false;
    }
    else {
		if (!isAreaCode(objAreaCode.value)) {
            parent.ClientWarning("Invalid phone area code specified.");
            objAreaCode.focus();
            objAreaCode.select();
            return false;
        }
		if (!isPhoneExchange(objExchange.value)) {
            parent.ClientWarning("Invalid phone exchange specified.");
            objExchange.focus();
            objExchange.select();
            return false;
        }
        if (!isPhoneNumber(objUnit.value)) {
            parent.ClientWarning("Invalid phone number specified.");
            objUnit.focus();
            objUnit.select();
            return false;
        }
    }
    return true;
}


function isNumeric(sValue) {
	/*for (var i=0; i < sValue.length; i++){
        if (sValue.substr(i,1) == '.')
            continue;
		if (isNaN(parseInt(sValue.substr(i, 1))))
			return false;
    }

	return true;*/
    return !isNaN(sValue);
}

//process the clipboard with Numbers only data and reset clipboard with the processed value.
function onBeforePasteNumbersOnly(){
    var sNewString = '';
    sNewString = window.clipboardData.getData("Text");
    if (sNewString != null) {
        sNewString = parseNumbersOnly(sNewString);
        window.clipboardData.setData("Text", sNewString);
    }    
    
}
//allow paste if the clipboard has Numeric data
function onPasteNumbersOnly() {
    var sNewString = '';
    sNewString = window.clipboardData.getData("Text");
    
    if (sNewString != null){
        if ((isNumeric(sNewString) == false) || (sNewString == '')) {
            parent.ClientWarning("Clipboard has invalid characters or is empty. Cannot paste these into this edit field.");
            event.returnValue = false;
        }
        else {
            event.srcElement.value = ''; //this line prevents pasting in between controls' value.
            event.returnValue = true;
            //setControlDirty(event.srcElement);
            SetDirty();
        }
    }    
}

//Parse and return only numeric characters found as a string.
function parseNumber(sValue) {
	var strRet = "";
	for (var i=0; i < sValue.length; i++)
		if (!isNaN(parseInt(sValue.substr(i, 1))))
			strRet += sValue.substr(i, 1);
	return strRet;
}

//this function simulates the NumbersOnly functionality. This function is intended to be used by the onpaste event.
function parseNumbersOnly(sValue){
	var strRet = "";
  var iChrCode;
  var iNxtChrCode;
  var bAllowDecimal = false;
  var sdbType = "";
  var iPrecision = 0;
  var iScale = 0;
  var bDecimal = false;

  bAllowDecimal = event.srcElement.getAttribute("allowDecimal");
  sdbType = event.srcElement.getAttribute("dbtype");
  iPrecision = parseInt(event.srcElement.getAttribute("Precision"));
  iScale = parseInt(event.srcElement.getAttribute("Scale"));
	for (var i=0; i < sValue.length; i++){
        iChrCode = sValue.charCodeAt(i);

        if (bDecimal == true) {
            //checks if the decimal scale does not exceed.
            if ((strRet.split(".")[1].length) >= iScale)
                return strRet;
        }
        if ((strRet.indexOf(".") == -1) && (strRet.length >= (iPrecision-iScale))) {
            if (iChrCode >= 48 && iChrCode <= 57 && (iScale > 0) && (sdbType == "money")) {
                strRet += ".";
                bDecimal = true;
            }
        }

        if (iChrCode >= 48 && iChrCode <= 57)
            strRet += sValue.substr(i, 1);
        else {
            if (i < sValue.length)
                iNxtChrCode = sValue.charCodeAt(i+1);
            else
                iNxtChrCode = -1;
            switch (iChrCode){
                case 45:// "-"
                    break;
                case 46:// "."
                        //for shop maintenance screens we don't have dbtype, precision and scale.
                        if ((bAllowDecimal == true) && (strRet.indexOf('.') == -1)){ // a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if ((sdbType == "money") && (iScale > 0) && (strRet.indexOf('.') == -1)) {// a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if (sdbType == "tinyint") {
                            //rounds to the nearest integer and returns
                            if (iNxtChrCode >= 53 && iNxtChrCode <= 57)
                                strRet = (parseInt(strRet) + 1).toString();
                            return strRet;
                        }
                        if (sdbType == "varchar")
                            continue;
                    break;
                case 47:// "/"
                    break;
                default:
                    if (sdbType != "varchar")
                        return strRet;
            }//switch
        }//else
    }//for
	return strRet;
}

//this function will attach onpaste and onbeforepaste events to all INPUTs who have NumbersOnly function attached
//to the onkeypress event.
//Call this function from a page's onLoad event.
function onpasteAttachEvents(){
    var oObjInputs = null;
    var strOnKeyPress = '';
    oObjInputs = document.all.tags('INPUT');
    for (var i = 0; i < oObjInputs.length; i++) {
        try {
            strOnKeyPress = '';
            strOnKeyPress = oObjInputs[i].onkeypress;
            if (strOnKeyPress != null) {
                if (strOnKeyPress.toString().toUpperCase().indexOf('NUMBERSONLY') > 0){
                    oObjInputs[i].attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
                    //remove the generic paste event which was set in the InitFields().
                    oObjInputs[i].onpaste = new Function("");
                    //attach the more specific onpaste function.
                    oObjInputs[i].attachEvent('onpaste', onPasteNumbersOnly);
                }
            }
        }
        catch (e) {continue;}
    }
}

//this function will attach onpaste and onbeforepaste events to the object specified.
function attachPasteEvents(obj){
    if (obj) {
        obj.attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
        //remove the generic paste event which was set in the InitFields().
        obj.onpaste = new Function("");
        //attach the more specific onpaste function.
        obj.attachEvent('onpaste', onPasteNumbersOnly);
    }
}

//Function to handle area code splits.
var AreaCodeSplitsFlagged = new Array();
function validateAreaCodeSplit(objAreaCode, objExchange){
  var sAreaCode, sExchange, sTmp;
  sAreaCode = objAreaCode.value;
  sExchange = objExchange.value;
  sTmp = sAreaCode + "|" + sExchange + "|";
  
  //Check to see if the area code + exchange was already notified.
  for (var i = 0; i < AreaCodeSplitsFlagged.length; i++){
    if (AreaCodeSplitsFlagged[i].indexOf(sTmp) != -1){
      //this area code and exchange was already notified to the user. Silently update the area code.
      var aTmp = AreaCodeSplitsFlagged[i].split("|");
      if (aTmp.length == 3){
        objAreaCode.value = aTmp[2];
        return true;
      }
    }
  }
  // this seems to be a new area code + exchange, check for the area code split
  var coObj = RSExecute("/rs/RSValidateAreaCode.asp", "IsValidAreaCode", sAreaCode, sExchange);
  retArray = ValidateRS( coObj );
  if (retArray[1] == 1){
    var retVal = retArray[0].split(",");
    if (retVal[0] == "false"){
      //area code + exchange is scheduled for a split. Notify user.
      parent.ClientWarning("Phone number starting with " + sAreaCode + " " + sExchange + " is scheduled for a change to the new area code " + retVal[1] + ".<br>" +
                    "The system will automatically change all occurances of " + sAreaCode + " " + sExchange + " to " + retVal[1] + " " + sExchange);
      //update the area code field to the new one.
      objAreaCode.value = retVal[1];
      //save this in an array so that this combination will not be notified again.
      AreaCodeSplitsFlagged.push(sAreaCode + "|" + sExchange + "|" + retVal[1]);
    }
  }
  return true;
}


  //generic currency formating x.00
  function formatcurrency(X)
  {
    var roundNum = Math.round(X*100)/100; //round to 2 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.00';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 3);
    
    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  //generic hours formating x.0
  function formathours(X)
  {
    var roundNum = Math.round(X*10)/10; //round to 1 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 2);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }
  
  //generic percentage formating x.000
  function formatpercentage(X)
  {
    var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.000';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '00';
    else if (strValue.indexOf('.') == strValue.length - 3)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 6);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  function YesNoMessage(sTitle, sMessage)
{
	var strReturn = sTitle + "|" + sMessage;
	var iDialogHeight;

	// retrieve optional argument which specifies Dialog Height or default to 125px.
	iDialogHeight = arguments.length > 2 ? arguments[2] : 125;

	strReturn = window.showModalDialog("/js/YesNoDialog.htm",strReturn,"dialogHeight: " + iDialogHeight + "px; dialogWidth: 450px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
	return strReturn;
}

  function isURL(strURL) {
      return ((strURL.search(/^(((http:\/\/)|(www\.)))[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}[ ]{0,}$/) == 0) ||
              (strURL.search(/^[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{3}[ ]{0,}$/) == 0))
  }
  
  function isEmail(strEmail) {
      return (strEmail.search(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) == 0);
  }  
  
  function checkEMail(obj){
    if (obj.value != ""){
      if (!isEmail(obj.value)) {
        event.returnValue = false;
        parent.ClientWarning("Invalid E-Mail specified.<br>Example: johndoe@server.com<br><br>Please try again");
        obj.focus();
        obj.select();
      }
    }
  }
  
  function checkURL(obj){
    if (obj.value != ""){
      if (!isURL(obj.value)) {
        event.returnValue = false;
        parent.ClientWarning("Invalid Internet website address (URL) specified.<br>Example: http://www.yahoo.com<br><br>Please try again");
        obj.focus();
        obj.select();
      }
    }
  }