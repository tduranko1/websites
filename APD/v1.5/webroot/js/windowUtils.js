var iSubWindow_1Top = 26;
var iSubWindow_regX = 790;
var iSubWindow_regW = 242;
var iSubWindow_regH = 320;
var iSubWindow_maxX = 4; // 166; //256;
var iSubWindow_maxW = 842 + 166 - iSubWindow_maxX; // 842 //752;
var iSubWindow_minH = 23;
var iSubWindow_colH = 635;
var iSubWinStartIndex = 900;
var iSubWindow_padding = 3;
var gsSubWindows = new Array();

function createSubWindow(sWinName, bInitialDisplay, sCaption, sIcon, iAutoRefresh) {
  try {
    if (sWinName != "") {
      var oFrame = document.createElement("IFRAME");
      var iIndex = iSubWinStartIndex + gsSubWindows.length;
      var iTop;
      //if (gsSubWindows.length == 1)
      //  iTop = iSubWindow_1Top * (gsSubWindows.length + 1) + iSubWindow_padding;
      //else 
      if (gsSubWindows.length > 0)
        iTop = iSubWindow_1Top * (gsSubWindows.length + 1) + (1 * gsSubWindows.length);
      else
        iTop = iSubWindow_1Top;
      oFrame.name = oFrame.id = sWinName;
      oFrame.style.cssText = "z-index:" + iIndex + ";position:absolute;top:" + iTop + ";left:" + iSubWindow_regX + ";height:" + iSubWindow_minH + ";width:" + iSubWindow_regW + ";border:0px;display:inline"; //( bInitialDisplay == false ? "display:none" : "display:inline")
      document.body.appendChild(oFrame);
      oFrame.src = "/APDWindow.asp?winName=" + sWinName + "&caption=" + sCaption + "&icon=" + sIcon + "&autoRefresh=" + iAutoRefresh;
      gsSubWindows.push(oFrame);
    }
  } catch (e) {
    alert(e.description)
  }
}

function window_maximize(sWinName) {
  try {
    if (sWinName != "") {
      //make sure that the subwindow exists in our collection
      var bSubWindowFound = false;
      var iSubWindowIndex = null;
      var iNextTop = iSubWindow_1Top;
      for (var i = 0; i < gsSubWindows.length; i++) {
        if (gsSubWindows[i].name.toLowerCase() == sWinName.toLowerCase()){
          bSubWindowFound = true;
          iSubWindowIndex = i;
          break;
        }
      }
      if (bSubWindowFound) {
        var iWinShown = 0;
        for (var i = 0; i < gsSubWindows.length; i++) {
          if (gsSubWindows[i].style.display == "inline")
            iWinShown++;
        }
        //var iSubWinHeight = iSubWindow_colH - ((iSubWindow_minH + iSubWindow_padding) * (gsSubWindows.length - 1));
        var iSubWinHeight = iSubWindow_colH - ((iSubWindow_minH + iSubWindow_padding) * (iWinShown - 1));
        var iSubWinTop = 0;
        if (iSubWindowIndex > 0)
          iSubWinTop = iSubWindow_1Top * (iSubWindowIndex + 1) + iSubWindow_padding;
        else
          iSubWinTop = iSubWindow_1Top * (iSubWindowIndex + 1);
          
        //minimize all the sub windows first
        for (var i = 0; i < iSubWindowIndex; i++) {
          iTop = iSubWindow_1Top + ((iSubWindow_minH + iSubWindow_padding ) * i);
          gsSubWindows[i].style.top = iTop;
          gsSubWindows[i].style.left = iSubWindow_regX;
          gsSubWindows[i].style.height = iSubWindow_minH;
          gsSubWindows[i].style.width = iSubWindow_regW;
          iNextTop = iTop + iSubWindow_minH + iSubWindow_padding;
        }
        
        gsSubWindows[iSubWindowIndex].style.top = iNextTop;
        gsSubWindows[iSubWindowIndex].style.left = iSubWindow_maxX;
        gsSubWindows[iSubWindowIndex].style.width = iSubWindow_maxW;
        gsSubWindows[iSubWindowIndex].style.height = iSubWinHeight + 3;
        
        iNextTop += iSubWinHeight + iSubWindow_padding;
        
        for (var i = iSubWindowIndex + 1; i < gsSubWindows.length; i++) {
          gsSubWindows[i].style.top = iNextTop + iSubWindow_padding;
          gsSubWindows[i].style.left = iSubWindow_regX;
          gsSubWindows[i].style.height = iSubWindow_minH;
          gsSubWindows[i].style.width = iSubWindow_regW;
          iNextTop += iSubWindow_minH + iSubWindow_padding;
        }
        
      }
    }
  } catch (e) {
    alert(e.description)
  }
}

function window_columnMax(sWinName) {
  try {
    if (sWinName != "") {
      //make sure that the subwindow exists in our collection
      var bSubWindowFound = false;
      var iSubWindowIndex = null;
      var iNextTop = iSubWindow_1Top;
      for (var i = 0; i < gsSubWindows.length; i++) {
        if (gsSubWindows[i].name.toLowerCase() == sWinName.toLowerCase()){
          bSubWindowFound = true;
          iSubWindowIndex = i;
          break;
        }
      }
      if (bSubWindowFound) {
        var iWinShown = 0;
        for (var i = 0; i < gsSubWindows.length; i++) {
          if (gsSubWindows[i].style.display == "inline")
            iWinShown++;
        }

        //var iSubWinHeight = iSubWindow_colH - ((iSubWindow_minH + iSubWindow_padding) * (gsSubWindows.length - 1));
        var iSubWinHeight = iSubWindow_colH - ((iSubWindow_minH + iSubWindow_padding) * (iWinShown - 1));
        var iSubWinTop = 0;
        if (iSubWindowIndex > 0)
          iSubWinTop = iSubWindow_1Top * (iSubWindowIndex + 1) + iSubWindow_padding;
        else
          iSubWinTop = iSubWindow_1Top * (iSubWindowIndex + 1);
          
        //minimize all the sub windows first
        for (var i = 0; i < iSubWindowIndex; i++) {
          iTop = iSubWindow_1Top + ((iSubWindow_minH + iSubWindow_padding ) * i);
          gsSubWindows[i].style.top = iTop;
          gsSubWindows[i].style.left = iSubWindow_regX;
          gsSubWindows[i].style.height = iSubWindow_minH;
          gsSubWindows[i].style.width = iSubWindow_regW;
          iNextTop = iTop + iSubWindow_minH + iSubWindow_padding;
        }
        
        gsSubWindows[iSubWindowIndex].style.top = iNextTop;
        gsSubWindows[iSubWindowIndex].style.left = iSubWindow_regX;
        gsSubWindows[iSubWindowIndex].style.width = iSubWindow_regW;
        gsSubWindows[iSubWindowIndex].style.height = iSubWinHeight + 3;
        
        iNextTop += iSubWinHeight + iSubWindow_padding;
        
        for (var i = iSubWindowIndex + 1; i < gsSubWindows.length; i++) {
          gsSubWindows[i].style.top = iNextTop + iSubWindow_padding;
          gsSubWindows[i].style.left = iSubWindow_regX;
          gsSubWindows[i].style.height = iSubWindow_minH;
          gsSubWindows[i].style.width = iSubWindow_regW;
          iNextTop += iSubWindow_minH + iSubWindow_padding;
        }
        
      }
    }
  } catch (e) {
    alert(e.description)
  }
}

function window_columnEqual() {
  try {
    var iWinShown = 0;
    for (var i = 0; i < gsSubWindows.length; i++) {
      if (gsSubWindows[i].style.display == "inline")
        iWinShown++;
    }

    //var iEqualWinHeight = (iSubWindow_colH - (iSubWindow_padding * gsSubWindows.length)) / gsSubWindows.length;
    if (iWinShown <= 0) return;
    
    var iEqualWinHeight = (iSubWindow_colH - (iSubWindow_padding * (iWinShown - 1))) / iWinShown;
    var iNextTop = iSubWindow_1Top;
    for (var i = 0; i < gsSubWindows.length; i++) {
      gsSubWindows[i].style.top = iNextTop;
      gsSubWindows[i].style.left = iSubWindow_regX;
      gsSubWindows[i].style.height = iEqualWinHeight;
      gsSubWindows[i].style.width = iSubWindow_regW;
      iNextTop += iEqualWinHeight + iSubWindow_padding;
    }
  } catch (e) {
    alert(e.description);
  }
}
