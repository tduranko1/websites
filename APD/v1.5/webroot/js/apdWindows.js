/*===========================================================*

    Lynx Select Window Object

 *===========================================================*/
var oWin=new Array;
oWin.zIndex=10;
oWin.dragobj=-1;
oWin.resizeobj=-1;
oWin.zIndex=100

oWin.bordercolor="#067baf"; //Remember that you have to change the images as well if you change this color
oWin.bgcolor="FFFAEB"; //Default background color
oWin.bgcoloron="FFFAEB";  //The "active" background color
oWin.bottomh=3; //The height of the bottom "border"
oWin.headh=37; //The height of the head "border"
oWin.bordersize=2; //The left and right bordersize
oWin.buttonsw=39; //The width of the buttons image
oWin.resizeh=9; //The width of the resize img
oWin.resizew=15; //The height of the resize img
oWin.starty=5; //If you have a header or something on the page that you want the setWindows and the dragdrop to care about set it here.
oWin.defwidth=400; //Default width for the windows if nothing is spesified
oWin.defheight=200; //Default height for the windows if nothing is spesified
oWin.between=15; //This variable controls how much space there will be between the windows when you use setWindows

// Constant positioning vars
oWin.maxX = 6; // 166; //256;
oWin.maxW = 842 + 166 - oWin.maxX; // 842; //752;
oWin.colH = 610;
oWin.regX = 766;
oWin.regW = 242;
oWin.regH = 320;
oWin.minH = 18;

//End Variables to set ********************************************************************

function lib_doc_size()
{ //Page positions - needed!
	this.x = 0;
	this.x2 = bw.ie && document.body.offsetWidth-20||innerWidth||0;
	this.y=0;
	this.y2 = bw.ie && document.body.offsetHeight-5||innerHeight||0;
	if(!this.x2||!this.y2) return message('Document has no width or height')
	this.x50 = this.x2/2;
	this.y50 = this.y2/2;
	this.x10 = (this.x2*10)/100;
	this.y10 = (this.y2*10)/100
	this.ytop = 140*100/this.y2
	this.avail = (this.y2*(100-this.ytop))/100
	this.origy = this.y2
	return this;
}

function lib_moveIt(x,y)
{
	this.x=x;
	this.y=y;
	this.css.left=x;
	this.css.top=y
}

function lib_moveBy(x,y)
{
	this.moveIt(this.x+x,this.y+y)
}

function lib_showIt()
{
	this.css.visibility="visible"
}

function lib_hideIt()
{
	this.css.visibility="hidden"
}

function lib_bg(color)
{
	this.css.backgroundColor=color
}

function lib_clipTo(t,r,b,l,setwidth)
{
	if(t<0)t=0;
	if(r<0)r=0;
	if(b<0)b=0;
	if(b<0)b=0
	this.ct=t;
	this.cr=r;
	this.cb=b;
	this.cl=l
	this.css.clip="rect("+t+","+r+","+b+","+l+")";
	if(setwidth)
	{
		this.css.width=r;
		this.css.height=b;
		this.w=r;
		this.h=b
	}
}

function lib_writeIt(text,startHTML,endHTML)
{
	this.evnt.innerHTML=text
}

//Default lib functions
function lib_obj(obj,nest,dnest,ddnest,num)
{
	this.evnt = bw.dom && document.getElementById(obj)||bw.ie4 && document.all[obj]

	if(!this.evnt) return lib_message('The layer does not exist ('+obj+') - Exiting script')
	this.css = this.evnt.style
	this.ref = document
	this.moveIt = lib_moveIt;
	this.moveBy=lib_moveBy;
	this.showIt=lib_showIt;
	this.hideIt=lib_hideIt;
	this.bg=lib_bg;
	this.num=num;
	this.writeIt=lib_writeIt;
	this.clipTo=lib_clipTo;
	this.obj = obj + "Object";
	eval(this.obj + "=this")
	return this
}
/*****************************************************************************
Creating windows
*****************************************************************************/
function create_window(i,colY,regY,minY)
{
    w=oWin.defwidth;
    h=oWin.defheight
    bg=oWin.bgcolor;
    bga=oWin.bgcoloron

	oWin[i]=new lib_obj('divWin'+i,"","","",i)
	oWin[i].oWindow=new lib_obj('divWindow'+i,'divWin'+i)
    oWin[i].oWindow.moveIt(oWin.bordersize-1,oWin.headh)
	oWin[i].oText=new lib_obj('divWinText'+i,'divWin'+i,'divWindow'+i)
	oWin[i].oHead=new lib_obj('divWinHead'+i,'divWin'+i)
	oWin[i].oToolBar = new lib_obj('divWinToolBar'+i,'divWin'+i)
	oWin[i].oButtons=new lib_obj('divWinButtons'+i,'divWin'+i)

    oWin[i].oWindow.css.overflow="auto";
	oWin[i].css.overflow="hidden"
    oWin[i].oText.css.overflow="hidden"
    oWin[i].oToolBar.css.overflow="hidden"

	oWin[i].defbg=bg;
	oWin[i].defbga=bga
	oWin[i].bg(oWin.bordercolor);
	oWin[i].oWindow.bg(oWin[i].defbg)

    oWin[i].colY=colY;
    oWin[i].regY=regY;
    oWin[i].minY=minY;

    oWin[i].me=i;
    oWin[i].resize=win_resize;
    oWin[i].change_state=win_change_state;
    oWin[i].on_change_state=win_on_change_state;
	oWin[i].addZ=win_addZ;

    oWin[i].mouse_focus=0;
    oWin[i].mouse_out=win_mouse_out;
    oWin[i].mouse_in=win_mouse_in;

    // Toolbar event handlers
    oWin[i].toolbar_add = win_not_implemented;
    oWin[i].toolbar_open = win_not_implemented;
    oWin[i].toolbar_show_all = win_not_implemented;
    oWin[i].toolbar_search = win_not_implemented;
    oWin[i].toolbar_refresh_callback = win_not_implemented;
    oWin[i].toolbar_refresh = win_on_refresh;

    return oWin[i];
}

/*****************************************************************************
Window functions
*****************************************************************************/

function win_not_implemented()
{
    alert( "That functionality has not been implemented yet." );
}

function win_change_state( newstate )
{
    if ( this.state != newstate )
    {
        switch ( newstate )
        {
            case "max" :
                do_minimize( oWin[ 1 - this.me ] );
                do_maximize( this );
                break;
            case "col" :
                do_minimize( oWin[ 1 - this.me ] );
                do_columnize( this );
                break;
            case "reg" :
                do_regwin( oWin[ 1 - this.me ] );
                do_regwin( this );
                break;
            case "min" :
                do_columnize( oWin[ 1 - this.me ] );
                do_minimize( this );
                break;
        }
    }
}

function win_mouse_out()
{
    this.mouse_focus=0;
}

function win_on_refresh()
{
    this.toolbar_refresh_callback( false );
}

function win_mouse_in()
{
    this.mouse_focus=1;
}

function do_maximize( win )
{
    win.resize( oWin.maxW, oWin.colH );
    win.moveIt( oWin.maxX, win.colY );
    win.on_change_state( "max" );
}

function do_columnize( win )
{
    win.resize( oWin.regW, oWin.colH );
    win.moveIt( oWin.regX, win.colY );
    win.on_change_state( "col" );
}

function do_regwin( win )
{
    win.resize( oWin.regW, oWin.regH );
    win.moveIt( oWin.regX, win.regY );
    win.on_change_state( "reg" );
}

function do_minimize( win )
{
    win.resize( oWin.regW, oWin.minH );
    win.moveIt( oWin.regX, win.minY );
    win.on_change_state( "min" );
}

function win_on_change_state( state )
{
    this.state=state;
}

function win_resize(w,h)
{
    this.oButtons.moveIt(w-oWin.buttonsw,0);
    this.oWindow.clipTo(0,w-oWin.bordersize*2,h-oWin.bottomh-oWin.headh,0,1);
    this.clipTo(0,w,h,0,1);
    this.oHead.clipTo(0,w,oWin.headh,0,1);
    this.oToolBar.clipTo(0,w-5,20,0,1);
    this.oText.moveIt(0,0);
}

function win_addZ()
{
	oWin.zIndex++;
	this.css.zIndex=oWin.zIndex
}

function win_init()
{
}

/*****************************************************************************
Adding window to winpage!
*****************************************************************************/

var lastx,lasty,lastw,lasth,winNum;

function addWindow(heading,timage)
{
    winNum=oWin.length;
    wins=winNum+1;
    var str = '<div id="divWin'+winNum+'" class="clWin">\n'
        +'<div class="clLogo"><img alt="" src="'+timage+'" width="19" height="18" border="0" align="top"/></div>\n'

        +'<div id="divWinHead'+winNum+'" class="clWinHead">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+heading+'</div>\n'

        +'<div id="divWinButtons'+winNum+'" class="clWinButtons">\n'
        +'<img border="0" src="images/window_max.gif" onClick="oWin['+winNum+'].change_state(\'max\'); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Maximize"/>'
        +'<img border="0" src="images/window_col.gif" onClick="oWin['+winNum+'].change_state(\'col\'); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Columnize"/>'
        +'<img border="0" src="images/window_reg.gif" onClick="oWin['+winNum+'].change_state(\'reg\'); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Normal"/>'
        +'</div>\n'

        +'<div id="divWinToolBar'+winNum+'" class="clWinToolBar">';
    document.write(str);
}

function addButton( fn, img, moverimg, alt )
{
    var str =
         '<img border="0" src="images/' + img
        +'" onClick="oWin['+winNum+'].' + fn + '(); return false;"'
        +' style="cursor:hand; FILTER:alpha(opacity=60)"'
        +' onMouseOut="makeHighlight(this,1,60)"'
        +' onMouseOver="makeHighlight(this,0,100)" alt="' + alt + '">'
        //+'\'" width="' + width + '" height="' + height + '" alt="' + alt + '">'
        +'<img src="images/spacer.gif" width="4" height="2" border="0">';
    return str;
}

function addDefaultButtons()
{
    var str =
          addButton( 'toolbar_add'     , 'smnew2.gif'   , 'smnew2_on.gif'   , 'Add' )
        + addButton( 'toolbar_open'    , 'smnew.gif'    , 'smnew_on.gif'    , 'Open' )
        + addButton( 'toolbar_refresh' , 'smrefresh.gif', 'smrefresh_on.gif', 'Refresh' )
        + addButton( 'toolbar_show_all', 'smsend.gif'   , 'smsend_on.gif'   , 'Show All' )
        + addButton( 'toolbar_search'  , 'smsearch.gif' , 'smsearch_on.gif' , 'Search' );
    document.write(str);
}

function addWindowBody(content)
{
    var str = '</div>\n'
        +'<div id="divWindow'+winNum+'" class="clWindow" onMouseOut="oWin['+winNum+'].mouse_out()" onMouseOver="oWin['+winNum+'].mouse_in()">\n'
        +'<div id="divWinText'+winNum+'" class="clText"><span id="spanWinText'+winNum+'">\n';
    str += content;
    document.write(str);
}

function endWindow(colY,regY,minY,initState)
{
    var str = '\n</span></div></div></div>\n';
    document.write(str);
    var newWin = create_window( winNum, colY, regY, minY );

    switch ( initState ) {
        case "max" : do_maximize( newWin ); break;
        case "col" : do_columnize( newWin ); break;
        case "reg" : do_regwin( newWin ); break;
        case "min" : do_minimize( newWin ); break;
    }

    newWin.showIt();

    return newWin;
}

// Clears the inner text window.
function clearWindow( num )
{
    var objSpanWinText = document.getElementById( 'spanWinText' + num );
    objSpanWinText.innerHTML = '';
}

