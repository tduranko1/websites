if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;


// Function that will prevent page to navigate one level back in history when backspace is pressed in a
// read-only field. This function must be called in body's onkeyDown event.
function docNoBkSpace()  {
    if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))){ //alt+left arrow, alt+ right arrow
        event.returnValue = false;
        return;
    }
    if ((event.altKey) && (event.keyCode == 36)){
        event.returnValue = false;
        return;
    }

    if (event.keyCode == 8) {
    	var srcTagName;
    	srcTagName = event.srcElement.tagName;

        switch (srcTagName) {
            case 'INPUT':
        		if (event.srcElement.readOnly == true ||
        			event.srcElement.type == 'checkbox' ||
        			event.srcElement.type == 'radio' ||
        			event.srcElement.type == 'button')
        		    event.returnValue = false;
        		break;
            case 'TEXTAREA':
        		if (event.srcElement.readOnly == true)
        			event.returnValue = false;
                break;
            default:  event.returnValue = false;
        }
    }
}

//this will attach the docNoBkSpace function to the document which will alter all documents that include this script file.
document.onkeydown = docNoBkSpace;