<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- NoteDetails.asp
	This file handles both the Insert and Update for Notes.
	It is used in a popup dialog window.
-->

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
	On Error Resume Next

    Dim objExe
    Response.Write GetHtml()
    Set objExe = Nothing

    Call KillSessionObject
    Call CheckError

    Function GetHtml()

        'Extract the query data.
        Dim BeforeXML, Metadata, Reference, PertainsTo, Action, strResult, DocumentID, LynxID
        Dim strXML, objXML

        'BeforeXML = Request("BeforeXML")
        DocumentID = Request("DocumentID")
        Action = Request("Action")
        PertainsTo = GetSession("NotePertainsTo")
        Reference = Application.Contents("NoteReference")
        Metadata = GetSession("NoteMetadata")
        LynxID = GetSession("LynxID")
        
        'response.write LynxID & "<br/>"
        'response.write ClaimAspectID & "<br/>"

        'Pass in a blank note so the template will trigger a blank page layout.
        'If BeforeXML = "" Then BeforeXML = "<Note/>"

        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, GetSession("UserID")

        objExe.AddXslParam "WindowID", sWindowID
        objExe.AddXslParam "LynxID", LynxID

        If DocumentID = vbnullstring Then
            objExe.AddXslParam "RawContext", GetSession("Context")

            'strResult = "<Root><Note/>" & CStr(Reference) & CStr(PertainsTo) & CStr(Metadata) & "</Root>"
            strResult = objExe.ExecuteSpAsXML( "uspNoteGetDetailXML", "NoteReference.xsl", LynxID )

            strResult = objExe.TransformXML( CStr(strResult), "NoteDetails.xsl" )
        Else
            strResult = objExe.ExecuteSpAsXML( "uspNoteGetDetailXML", "NoteDetails.xsl", LynxID, DocumentID )
        End If

        GetHtml = strResult

    End Function
%>
