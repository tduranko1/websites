
<%
  response.expires = -1
  
  On Error Resume Next
  lsPageName = "FNOLAssignment.asp"

  Dim lsStrXML, lsRetVal
  dim loAPD, lsExternalEventMsg
  dim sDocLocation, sVehListXML, sTemp

	Dim mexMonikerString , loStrXML , strResult         
	mexMonikerString = GetWebServiceURL()
	set loAPD = GetObject(mexMonikerString)

  'set loAPD = Server.CreateObject("LAPDEcadAccessorMgr.CData")
  
  If Err.Number <> 0 Then
    response.write "<Error>Error creating 'LAPDEcadAccessor.CData' object</Error>"
    response.end
  End If
  
  set oXML = Server.CreateObject("MSXML2.DOMDocument")
  oXML.async = false
  oXML.load(request)  

  if oXML.parseError.errorCode <> 0 then
    response.write "<Error><![CData[Error parsing the XML request at Line #: " & CStr(oXML.parseError.line) & " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error>"
    response.end
  end if
 
  lsRetVal = submitData(oXML.xml)

  if instr(1, lsRetVal, "<Error>") > 0 then
    response.write lsRetVal
  else
    if instr(1, oXML.xml, "<WebAssignment ") > 0  then
      'sTemp = split(lsRetVal, chr(255))
      
      'sDocLocation = sTemp(0)
      'sVehListXML = sTemp(1)
    
      'response.write "<Root>" & _
      '                "<Document><![CDATA[" & sDocLocation & "]]></Document>" & _
      '                sVehListXML & _
      '                "</Root>"
      
      response.write lsRetVal
    else
      response.write lsRetVal
    end if    
  end if
 
  set loAPD = Nothing

function submitData(sXML)
  on error resume next
  dim lsRet
  Dim xmlhttp  
  Dim postUrl, DataToSend
  Dim strHtmlValue
  Dim oExe
  'Dim sXML1
  'sXML1 =  "<NADAValuation LossDate='01/15/2012' Region='' LossState='TX' Year='' Make='' Model='' Body='' VIN='1G2NE52F52C197193' Mileage='' Zip='' NADAVehicleId='' Action='LookupVIN' ReadOnly='true' />"
  '"<NADAValuation LossDate="02/26/2012" Region="" LossState="TX" Year="" Make="" Model="" Body="" VIN="JTMZD33V965025673" Mileage="" Zip="" NADAVehicleId="" Action="LookupVIN" ReadOnly="true"/>"
 
   If instr(sXML,"NADAValuation") > 0 then
    'To get Nada Vehicle Values  
    Set oExe = CreateObject("DataPresenter.CExecute")
    oExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH") & "..\config\config.xml"
    postUrl = oExe.Setting("NADAWebservice/URL") & "GetFNOLXML" 

    Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
    DataToSend="FNOLXml=" & sXML   
    xmlhttp.Open "POST",postUrl,false
    xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
    xmlhttp.send DataToSend
    
    'writeFile(xmlhttp.responseText)
    'writeFile("--------------------------------")
    strHtmlValue = xmlhttp.responseText
    set xmlhttp = nothing 

    if not inStr(strHtmlValue,"Exception") > 0 then
      Dim xmlDoc
      set xmlDoc = createObject("MSXML2.DOMDocument")
      xmlDoc.async = False
      xmlDoc.loadXML(strHtmlValue)      
      lsRet = replace(replace(replace(xmlDoc.getElementsByTagName("string").item(0).childNodes(0).text,"&amp;","&"),"&gt;",">"),"&lt;","<") 
    else
        response.write "<Error>" & server.HTMLEncode(strHtmlValue) & " </Error>"
    end if 
      
   else   
   'Calling component 	
	lsRet = CallSoapWCF(sXML)	
   end if 
  
  
  if err.number <> 0 then
    lsRet = "<Error><![CDATA[An Error occurred. Error Source: " & err.source & "; Description: " & err.description & "]]></Error>"
  end if
  submitData = lsRet
end function

'For testing 
function writeFile(value)
dim objFSO, objFile, strFilePath
strFilePath = "C:\IntranetSites\APD\webroot\testFnol.txt"
     
Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Set objFile=objFSO.OpenTextFile(strFilePath, 8, true)

objFile.WriteLine(value)
objFile.Close

Set objFile = nothing
Set objFSO = nothing  

end function

function CallSoapWCF(claimXML)

'/* Call the web service */
Dim xmlHttp, xmlResponse, postUrl
Dim dataToSend
Dim sXmlCall
Dim docXML
Dim NodeList
Dim strXmlArray
Dim strReturnXML, strResult

sXmlCall = claimXML
Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
postUrl = GetSoapURL()
dataToSend = "" & _
                "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">" & _   
                   "<soapenv:Body>" & _
                   "<GetXML xmlns=""http://LynxAPDComponentServices""><strParamXML>" & Server.HTMLEncode(sXmlCall) & "</strParamXML></GetXML>" &_                                 
                   "</soapenv:Body>" & _
                "</soapenv:Envelope>"

Call xmlHttp.Open("POST", postUrl, False, "", "")
xmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
xmlHttp.setRequestHeader "SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetXML"
xmlHttp.setRequestHeader "Content-length", len(dataToSend)
xmlHttp.send dataToSend

Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
docXML.Async = False
docXML.LoadXML HTMLDecode(xmlHttp.responseText)
strReturnXML = docXML.xml

 strResult = Split(strReturnXML,"<GetXMLResult>")(1)

xmlResponse = Split(strResult, "</GetXMLResult>")(0)

CallSoapWCF = xmlResponse

end function

function GetSoapURL()

Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"       
        case "STG"
        strmexMonikerString = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"        
        case "PRD"
        strmexMonikerString = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"       
    End Select
    
   GetSoapURL = strmexMonikerString


end function

Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")		
		returnValue = Replace(returnValue, "&amp;", "&")
                
                HtmlDecode = returnValue
End Function

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
%>
