<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- CheckListSnoozeHistory.asp
	This file displays the checklist history for a task item.
	It is used in a popup dialog window.
-->

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
    On Error Resume Next

    Dim objExe

    'Extract the query data.
    Dim TaskID, strResult

    TaskID = Request("TaskID")

    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey

    objExe.Trace "TaskID='" & TaskID & "'", "CheckListSnoozeHistory.asp"

    strResult = objExe.ExecuteSpAsXML( "dbo.uspDiaryGetHistoryXML", "CheckListSnoozeHistory.xsl", TaskID )

    Response.write strResult

    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

%>
