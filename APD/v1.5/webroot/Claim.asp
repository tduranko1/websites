<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
    
        Dim strLynxID
        Dim strUserID
        Dim strInsuranceCompanyID
        Dim strClaim_ClaimAspectID

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        strUserID = GetSession("UserID")

        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        ' Place claim information in session manager.
        StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey
        
        strClaim_ClaimAspectID = GetSession("Claim_ClaimAspectID")

        ' Claim has been touched!
        objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strClaim_ClaimAspectID, strUserID

        strInsuranceCompanyID = GetSession("InsuranceCompanyID")

        Dim strImageRootDir
        strImageRootDir = objExe.Setting("Document/RootDirectory")

        Dim strIMPSEmailBCC
        strIMPSEmailBCC = objExe.Setting("IMPSEmail/BCC")
		
        objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "IMPSEmailBCC", strIMPSEmailBCC

        ' Get claim XML and transform to HTML.
        'Response.Write objExe.ExecuteSpAsXML( "uspClaimGetDetailXML", "Claim.xsl", strLynxID, strInsuranceCompanyID )
        Response.Write objExe.ExecuteSpAsXML( "uspClaimCondGetDetailXML", "Claim.xsl", strLynxID, strInsuranceCompanyID )

    End Sub
%>
