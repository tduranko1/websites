<%@ Language=VBScript %>
<%
    Option Explicit
    Response.Expires = -1
%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->
<%
    On Error Resume Next

    Dim objExe
    Dim strUserID,  strLynxID,  strEntity,  strClaimAspectID
                
    strUserID = GetSession("UserID")
    strLynxID = GetSession("LynxID")
    strEntity = Request("Entity")
    strClaimAspectID = request("ClaimAspectID")
            
    'Create and initialize DataPresenter
    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
    
    objExe.AddXslParam "LynxId", strLynxID
    objExe.AddXslParam "Entity", strEntity
    objExe.AddXslParam "ClaimAspectID", strClaimAspectID
    objExe.AddXslParam "IntervalID", request("IntervalID")
    objExe.AddXslParam "PaymentID", request("PaymentID")
    objExe.AddXslParam "InvoiceID", request("InvoiceID")

    'Get XML and transform to HTML.
    Response.Write objExe.ExecuteSpAsXML( "uspInvoiceGetDetailXML", "PaymentAdd.xsl", strLynxID, strUserID)

    Set objExe = Nothing
    
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0
    
%>
