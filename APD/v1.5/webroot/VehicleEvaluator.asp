<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expiresabsolute = Now() - 1 %>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%

  Dim strPeriod, strRegion, strRegionDesc, strYear, strMake, strModel,  strBody, strVIN, strReadOnly
  Dim strVehId, strAction, strZip, strDBDate, strMileage, strLynxID, strHTML
  Dim oExe,strServiceURL
  
  strPeriod     = Request("strPeriod")
  strRegion     = Request("strRegion")
  strRegionDesc = Request("strRegionDesc")
  strYear       = Request("strYear")
	strMake       = Request("strMake")
	strModel      = Request("strModel")
	strBody       = Request("strBody")
	strVIN        = Request("strVIN")
	strVehId      = Request("strVehId")
	strAction     = Request("strAction")
  strZip        = Request("strZip")
  strDBDate     = Request("strDBDate")
	strMileage	  = Request("txtMileage")
  strReadOnly   = Request("strReadOnly")
	  
  On Error Resume Next
  
	strLynxID = Request("LynxID")
	If strLynxID = "" Then
		strLynxID = GetSession("LynxID")
	Else
		UpdateSession "LynxID", strLynxID
	End If	
    
    
	Dim objNada
	Set objNada = CreateObject("NadaWrapper.CNadaWrapper")
   
  ' strMileage passed here instead of GetAllAsXML call to avoid breaking compatibility.
	objNada.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), strLynxID
	CheckError() 
	       
	on error resume next 
	Dim xmlhttp  
	Dim strHtmlValue
    Dim DataToSend
    Dim RegionCookie
    
    RegionCookie = request.Cookies("NADARegionList")
    
    DataToSend="strTokenId=1&strRegion=" & strRegion & "&strRegionDesc=" & strRegionDesc & "&strYear=" & strYear & "&strMake=" & strMake & "&strModel=" & strModel & _
                    "&strBody=" & strBody & "&strMileage=" & strMileage & "&strVIN=" & strVIN & "&strVehId=" & strVehId & "&strAction=" & strAction & _
                    "&strZip=" & strZip & "&strReadOnly=" & strReadOnly & "&strRegionList=" &  replace(RegionCookie,"&","amp;")
       
    'strHTML = objNada.GetAllAsXML( strRegion, strRegionDesc, strYear, strMake, strModel, strBody, strMileage, _
	                               'strVIN, strVehId, strAction, strZip, strReadOnly, strDBDate, strPeriod)
    'response.Write strHtml
    'response.Write err.Description
    'response.End
    
    Set oExe = CreateObject("DataPresenter.CExecute")
   
    oExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH") & "..\config\config.xml"
   'Get the PGW NADA wenservice link from Config file 
    strServiceURL = oExe.Setting("NADAWebservice/URL")
   
    Dim postUrl
    on error resume next      
    dim objFSO, objFile, strFilePath
     
    postUrl = strServiceURL & "GetAllAsXML"     
    Dim strBody2 
    'strBody2 = DataToSend
    Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")    
    xmlhttp.Open "POST","http://sapd.pgw.local/nadaservice/service.asmx/GetAllAsXML",false   'postUrl
    xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
    xmlhttp.send DataToSend

    'If xmlhttp status is 0, connection to PGW NADA webservice failed
    'looping 5 times to connect to PGW nada service     
    Dim iLoop
    For iLoop = 0 to 5
        if xmlhttp.status = 0 then 
            set xmlhttp = nothing
            Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
            
            xmlhttp.Open "POST",postUrl,false
            xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
            xmlhttp.send DataToSend     
        else
            exit for
        end if      
    next
     
    'If still couldn't establish connection with PGW NADA serice, try again latter
    if xmlhttp.status = 0 then 
        'xmlhttp.send DataToSend
        
        response.Write "Error connecting to webservice, Please try again<br>"      
        response.Write xmlhttp.statusText
        'call SendEmail("NADATesting",strBody2) 
        set xmlhttp = nothing        
        response.End
    End if
     
    'xmlhttp responseText contains raw http return values from PGW Nada service
    strHtmlValue = xmlhttp.responseText
   
    set xmlhttp = nothing 
    
    'Parsing the http response 
      Dim strTxtValue 	
      Dim xmlDoc
      set xmlDoc = createObject("MSXML2.DOMDocument")
      xmlDoc.async = False
      
      xmlDoc.loadXML(strHtmlValue)
     
      response.Write replace(replace(replace(xmlDoc.getElementsByTagName("string").item(0).childNodes(0).text,"&amp;","&"),"&gt;",">"),"&lt;","<")
 	                         
     Set objNada = nothing
    
    Call KillSessionObject

	'Response.Write  strHTML
%>
