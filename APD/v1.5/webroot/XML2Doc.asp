<!-- #include file="errorhandler.asp" -->
<%
    Response.Expires = -1
    On Error Resume Next
    dim xmlDoc, docPath
    dim pgCount
    dim pgContent
    pgContent = ""
    docPath = request.QueryString("docPath")
    if docPath <> "" then
        Dim fso, tfolder, tName, strFileName, strFile
        Set fso = CreateObject("Scripting.FileSystemObject")
        Call CheckError()

        if not fso.FileExists(docPath) then
            response.write "File not found."
            response.end
        end if

        if LCase(fso.GetExtensionName(docPath)) <> "xml" then
            response.write "Specified File name is not an XML file. This web page can be used to convert XML file to MS Word Document."
            response.end
        end if

        set xmlDoc = CreateObject("MSXML2.DOMDocument")

        xmlDoc.Load docPath

        if xmlDoc.parseError.errorCode <> 0 then
            err.raise vbObjectError + 1000, "xml2doc.asp", xmlDoc.parseError.reason
            call cleanupObjects
            call CheckError
            response.end
        end if

        Set objNodeList = xmlDoc.documentElement.selectSingleNode("/PrintImage")
        if isObject(objNodeList) then
            pgCount = 1
            for each objChildNode in objNodeList.childNodes
                if (objChildNode.childNodes(0).nodeType = 4) then 'CDATA
                    pgContent = pgContent + trimContent(objChildNode.childNodes(0).nodeTypedValue)
                    if pgCount < objNodeList.childNodes.length then pgContent = pgContent + Chr(12)
                    pgCount = pgCount + 1
                end if
            next
        end if
        'response.write pgContent
        set objNodeList = nothing
        call cleanupObjects

        'get the file name from the doc path and append to system temp path
        Set tfolder = fso.GetSpecialFolder(2)
        strFile = fso.GetBaseName(docPath) + ".doc"
        strFileName = tfolder.Path + "\" + strFile
        set tfolder = nothing

        'Generate a word document in the temp path using the docPath's file name
        dim objWordApp, objWordDoc
        set objWordApp = createObject("Word.Application")
        Call CheckError
        objWordApp.DisplayAlerts = 0 'wdAlertsNone
        objWordApp.Visible = False
        if isObject(objWordApp) then
            set objWordDoc = objWordApp.Documents.Add
            if isObject(objWordDoc) then
                objWordApp.ActiveDocument.Content.InsertAfter pgContent
                Set myRange = objWordApp.ActiveDocument.Range(0, objWordApp.ActiveDocument.Paragraphs(objWordApp.ActiveDocument.Paragraphs.Count).Range.End)
                With myRange
                    .Font.Name = "Courier New"
                    .Font.Size = "10"
                End With
                with objWordApp.ActiveDocument.PageSetup
                    .LeftMargin = objWordApp.InchesToPoints(1)
                    .RightMargin = objWordApp.InchesToPoints(0.5)
                    .TopMargin = objWordApp.InchesToPoints(1)
                    .BottomMargin = objWordApp.InchesToPoints(0.5)
                end with
                objWordApp.ActiveDocument.SaveAs strFileName
            end if
        end if
        objWordApp.Quit 0
        set objWordDoc = nothing
        set objWordApp = nothing

       'output the doc
        response.addHeader "Content-Type", "application/octet-stream"
        response.addHeader "Content-Transfer-Encoding", "base64"
        response.addHeader "Content-Disposition", "attachment; filename=""" + strFile + """"
        dim adoStream
        set adoStream = CreateObject("ADODB.Stream")
        Call CheckError
        adoStream.Open
        adoStream.Type = 1 'binary
        adoStream.LoadFromFile strFileName
        adoStream.Position = 0
        response.binaryWrite adoStream.Read() 'readall
        set adoStream = nothing
        'Document has been sent. Delete it now.
        fso.DeleteFile strFileName
        set fso = nothing

    end if

    sub cleanupObjects
        set xmlDoc = nothing
        'set xslDoc = nothing
    end sub

    function trimContent(strData)
        dim strRet, arrStr, posStart, posEnd
        strRet = ""
        strData = replace(strData, vbTab, "")
        if instr(strData, vbcrlf) then
            arrStr = split(strData, vbcrlf)
        else
            if instr(strData, vbcr) then
                arrStr = split(strData, vbcr)
            else
                arrStr = split(strData, vblf)
            end if
        end if

        for i = LBound(arrStr) to UBound(arrStr)
            if trim(arrStr(i)) <> "" then
                posStart = i
                exit for
            end if
        next

        for i = UBound(arrStr) to LBound(arrStr) step -1
            if trim(arrStr(i)) <> "" then
                posEnd = i
                exit for
            end if
        next
        strRet = ""
        for i = posStart to posEnd
            strRet = strRet + arrStr(i)
            if i < posEnd then strRet = strRet + vbcrlf
        next

        trimContent = strRet
    end function

%>

