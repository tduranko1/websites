<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	On Error Resume Next

	Dim strLynxID, strUserID, strInvolvedID, strVehNum
	Dim strInsuranceCompanyID, strPageReadOnly
	Dim objExe, strHTML

	strLynxID = GetSession("LynxID")
	strUserID = GetSession("UserID")
	strVehNum = GetSession("VehicleNumber")
	strInvolvedID = Request("InvolvedID")
  strPageReadOnly = Request("pageReadOnly")
	'strLynxID = 2600
	'strInvolvedID = 27

	CheckError()

	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	CheckError()

	'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
	objExe.AddXslParam "LynxId", strLynxID
  objExe.AddXslParam "VehicleNumber", strVehNum
  objExe.AddXslParam "pageReadOnly", strPageReadOnly	
	CheckError()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspVehicleInvolvedGetDetailXML", "VehicleInvolved.xsl", strInvolvedID, strInsuranceCompanyID )
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>
