<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->


<%
	On Error Resume Next

	Dim objExe, strHTML, strFromDate, strToDate, strNotReviewedOnlyFlag
  
  Set objExe = InitializeSession()
  
	strFromDate = Request("FromDate")
	strToDate = Request("ToDate")
	strNotReviewedOnlyFlag = Request("NotReviewedOnlyFlag")

  if strFromDate <> null then
     strFromDate = "0"
  end if
  
  if strToDate <> null then  
     strToDate = "0"
  end if
  

	'Create and initialize DataPresenter if not already existing
  if objExe is nothing then
	  Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()

  	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
 	  CheckError()
	end if

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspUtilClaimListXML", "UtilEstimateClaimList.xsl", strFromDate, strToDate, strNotReviewedOnlyFlag )
	CheckError()


	Set objExe = Nothing

	Response.Write strHTML

	On Error GoTo 0
%>
