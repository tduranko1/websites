<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	On Error Resume Next

	Dim strLynxID, strUserID, strVehNum
	Dim strInsuranceCompanyID, strPageReadOnly
	Dim objExe, strHTML
'   Dim strClaimAspectID
   Dim strEvtVehicleAssignmentSentToShop, strEvtAppraiserAssignmentSent
   Dim strClaimAspectServiceChannelID
   ' Dim StrAssignmentSequenceNumber
  
	strLynxID = GetSession("LynxID")
	strUserID = GetSession("UserID")
	
	'strVehNum = GetSession("VehicleNumber")
	CheckError()

'	strClaimAspectID = Request("ClaimAspectID")
   strInsuranceCompanyID = Request("InsuranceCompanyID")
   strPageReadOnly = Request("pageReadOnly")
   strClaimAspectServiceChannelID = Request("ClaimAspectServiceChannelID")
 
  ' StrAssignmentSequenceNumber = 1'Request("AssignmentSequenceNumber")
  
'	If strClaimAspectID = "" Then
'		strClaimAspectID = GetSession("ClaimAspectID")
'	Else
'		UpdateSession "ClaimAspectID", strClaimAspectID
'	End If
'	CheckError()
  
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()	
	
	
    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	  CheckError()
  
    strEvtVehicleAssignmentSentToShop = objExe.Setting("WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
    CheckError()

    strEvtAppraiserAssignmentSent = objExe.Setting("WorkFlow/EventCodes/Code[@name='AppraiserAssignmentSent']")
    CheckError()
       
    objExe.AddXslParam "EventVehicleAssignmentSentToShopID", strEvtVehicleAssignmentSentToShop
    CheckError()

    objExe.AddXslParam "EventAppraiserAssignmentSent", strEvtAppraiserAssignmentSent
    CheckError()

    objExe.AddXslParam "LynxID", strLynxID
    CheckError()
    
   ' objExe.AddXslParam "AssignmentSequenceNumber", Request ("StrAssignmentSequenceNumber")
    'CheckError()
    
    objExe.AddXslParam "pageReadOnly", strPageReadOnly
    CheckError() 
    
    
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspVehicleAssignGetDetailXML", "VehicleShopAssignment.xsl", strClaimAspectServiceChannelID,strInsuranceCompanyID )
	CheckError()

	Set objExe = Nothing
   
	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>
