<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        Dim strLynxID
        Dim strVehContext
        Dim strClaimAspectID
        Dim strInsuranceCompanyID
        Dim strClaim_ClaimAspectID

        strUserID = GetSession("UserID")

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        strVehContext = Request("VehContext")
        UpdateSession "VehContext", strVehContext

        strClaimAspectID = Request("ClaimAspectID")
        UpdateSession "ClaimAspectID", strClaimAspectID

        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        ' Place claim information in session manager.
        StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey

        strClaim_ClaimAspectID = GetSession("Claim_ClaimAspectID")
        
        ' Claim has been touched!
        objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strClaim_ClaimAspectID, strUserID

        'moved this line here because the Insurance company id for the claim in context will be correctly set only after the call to uspSessionGetClaimDetailXML
        strInsuranceCompanyID = GetSession("InsuranceCompanyID")

        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        objExe.AddXslParam "VehContext", strVehContext
        objExe.AddXslParam "ClaimAspectID", strClaimAspectID

        ' Get claim XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspClaimVehicleGetListXML", "ClaimVehicleList.xsl", strLynxID, strInsuranceCompanyID )

    End Sub
%>
