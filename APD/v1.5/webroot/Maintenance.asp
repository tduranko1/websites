<%@ LANGUAGE="VBSCRIPT"%>
<html>
<head>
<title>APD Maintenance Mode</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000">
	<table width="100%">
		<tr>
			<td>
				<br>
				<br>
				<div align="center">
					<img src="/images/apdlogo.gif"><br><br>
					<h1>Sorry!</h1>
					<h3>
						<p>This site is currently under maintenance.<p>
						<p>Contact your IS manager for details.<p>
						<p>We will be up again ASAP!<p>
					</h3>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
