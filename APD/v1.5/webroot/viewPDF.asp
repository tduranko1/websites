<html>
    <head>
    <title>APD PDF Viewer</title>
        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        <style>
            .myPage {
                width:660px;
                border: 1px solid #000000;
                background-color: #FFF8DC;
                padding:15px;
                filter: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=5);
            }
            .pageContent {
                width:640px;
                background-color: #FFFFFF;
                padding-left:10px;
                padding-right:10px;
            }
            .pagePrint {
                width:100%;
                background-color: #FFFFFF;
            }
            pre {
                font-family: Courier New;
                font-size: 10pt;
            }
            .toolbarButton {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border:0px;
            }
            .toolbarButtonOver {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border-top:1px solid #D3D3D3;
              border-left:1px solid #D3D3D3;
              border-bottom:1px solid #696969;
              border-right:1px solid #696969;
            }
        
            .toolbarButtonOut {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border:0px;
            }

        </style>

    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

    <script language="javascript">
        var zoomVal = 90;
        var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
        var newWin = null;
        var sDoc = "<%=replace(request.QueryString("docPath"), "\", "\\")%>";
        
        function zoomIn(){
            if (zoomVal < 100) {
                zoomVal += 10;
                pdf1.setZoom(zoomVal);
                pdf1.focus();
            }
        }

        function zoomOut(){
            if (zoomVal > 20) { //limit this to a 40% zoomout.
                zoomVal -= 10;
                pdf1.setZoom(zoomVal);
                pdf1.focus();
            }
        }
    
        function closeMe(){
            if (window.self == window.top) {
                window.returnValue = true;
                close();
            }
            else {
                window.self.frameElement.style.display = "none";
            }
        }
        
        function initPage(){
            if (document.readyState == "complete") {
                try {
	
                    pdf1.setShowToolbar(false);
                    pdf1.setPageMode("Page");
                    //btnPrint.style.display = "none";
                    enableControls();
                }
                catch(e) {
                    console.error("Tring again :", e);
                    enableControls();
                }
            }
            try 
            {
                if (window.self == window.top) {
                  //btnSaveLocal.parentElement.parentElement.style.height = "18px";
                  btnSaveLocal.style.display = "inline";
                  btnClose.style.display = "inline";
                  btnPin.firstChild.src = "images/pin.gif";
                } else {
                  //btnSaveLocal.parentElement.parentElement.style.height = "0px";
                  btnSaveLocal.style.display = "none";
                  btnClose.style.display = "none";
                }
                //btnClose.disabled = false;
                btnClose.style.cursor = 'hand';
            } catch(ex) {
                    console.error("Tring again :", ex);
                    enableControls();
            }
          window.setTimeout("zoomIn()", 250);
        }


        function reloadFrame() {
          try {
             if (window.opener) {	 
               var bObj = window.opener.document.getElementById('btnPin');
               if (bObj == null) return;
               var objImg = bObj.firstChild;
               objImg.src="/images/pinned.gif";
             }
          } catch (e) {}
        }

        function enableControls(){
          var objs = toolbar.getElementsByTagName("BUTTON");
          for (var i = 0; i < objs.length; i++) {
            objs[i].disabled = false;
          }
        }

        function goToPage(val){
            switch(val){
                case 1:
                    pdf1.gotoFirstPage();
                    break;
                case 2:
                    pdf1.gotoPreviousPage();
                    break;
                case 3:
                    pdf1.gotoNextPage();
                    break;
                case 4:
                    pdf1.gotoLastPage();
                    break;
            }
            pdf1.focus();
        }

        function tb_onmouseover(obj){
            obj.className = "toolbarButtonOver";
        }
        
        function tb_onmouseout(obj){
            obj.className = "toolbarButtonOut";
        }
        
        function SaveLocal(){
          var sFeatures = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;dialogHeight:245px;dialogWidth:400px;"
          var fso = new ActiveXObject("Scripting.FileSystemObject");
          //var sPath = fso.GetAbsolutePathName(sDoc);
          var sFileName = fso.GetFileName(sDoc);
          //var sFilePath = window.showModalDialog("/SaveAsDlg.htm", sFileName, sFeatures);
          try {
            var oXL = new ActiveXObject("Excel.Application");
            if (oXL) {
              sFileName2 = oXL.GetSaveAsFilename(sFileName, "PDF Files (*.pdf),*.pdf", 0);
            } else {
              ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
              return;
            }
            if (sFileName2 == false) {
              return;
            }
              
           sFilePath = sFileName2;
          } catch (e) {
            ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
            return;
          } finally {
            if (oXL)
              oXL.Quit();
          }

          if (sFilePath) {
            if (fso.FileExists(sFilePath)) {
              var sReturn = YesNoMessage("Destination File Exists", "A file already exists at the destination folder with the same name. Do you want to overwrite?");
              if (sReturn == "Yes"){
                var f = fso.GetFile(sFilePath);
                if (f && (f.attributes && 1)) { // if the file is read-only
                  f.attributes = 0; // set attribute to normal.
                }
                try {
                  fso.CopyFile(sDoc, sFilePath, true);
                } catch (e) {
                  ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
                }
              }
            } else {
              try {
                fso.CopyFile(sDoc, sFilePath, false);
              } catch (e) {
                ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
              }                  
            }
          }
        }
        

       function togglePinned() {
    		 var objImg = btnPin.firstChild;
    
    		 if (objImg.src.indexOf("/images/pinned.gif") != -1) {
     		  var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
    	    var parentLoc = window.parent.location.href;
      		var sURL = window.location.href;
     		  if (parentLoc.toLowerCase().indexOf("DocumentEstDetails.asp") != -1) {
               newWin = window.showModalDialog(sURL, null, sDlgProp + sDlgSize);
     		  }
      		 else {
       		   objImg.src = "/images/pin.gif";
        	   btnPin.title = "Detach Photo Viewer";
        	   newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
       		   newWin.opener = window.self;
     		  }
    	  }	else {
     		   objImg.src = "/images/pinned.gif";
     		   btnPin.title = "Close Detached Photo Viewer";
    		   if (newWin != null)
      		    newWin.close();
    		   window.close();
    		 }
       }
    
        function printDoc() {
          try {
            pdf1.Print();
          } catch (e) {
                ClientWarning("An error occured while printing the pdf file: " + sDoc);
          }
        }
    </script>
    </head>
    <body topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0" style="overflow:hidden;border:1px solid #000000;" onreadystatechange="initPage()"  onunload="reloadFrame();">
      <table border="0" cellspacing="0" cellpadding="0" style="width:100%; height:100%">
        <tr style="height:21px">
          <td>
                <div id="toolbar" name="toolbar" style="margin:2px;width:100%;background-color:#FFFFFF">
                    <table border="0" cellspacing="0" cellpadding="0" style="width:100%;border:0px;border-bottom: 1px solid #696969">
                    <tr>
                    <td width="*">
                    <button hidefocus="true" id="btnPin" name="btnPin" tabIndex = "-1" tabStop="false" onclick="togglePinned()" title="Unpin/Pin" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/pinned.gif" width="18" height="18" border="0" style="cursor:hand; background-color:#FFFFFF;"  /></button>
                    <button hidefocus="true" id="btnSaveLocal" tabIndex="-1" tabStop="false" onclick="SaveLocal()" title="Save a local copy" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/SaveCopy.gif" width="17" height="14" border="0" style="cursor:hand;background-color:#FFFFFF"/></button>
                    <button disabled hidefocus="true" id="btnPrint" tabIndex="-1" tabStop="false" onclick="printDoc()" title="Print Document" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/print.gif"/ width="18" height="18" border="0" style="cursor:hand;background-color:#FFFFFF"></button>
                    <img src="/images/spacer.gif" width="4px"/>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomIn()" title="Zoom In" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomin.gif"/ width="18" height="18" border="0" style="cursor:hand;background-color:#FFFFFF"></button>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomOut()" title="Zoom Out" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomout.gif"/ width="18" height="18" border="0" style="cursor:hand;background-color:#FFFFFF"></button>
                    <img src="/images/spacer.gif" width="4px"/>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(1)" title="First Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;&lt;</button>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(2)" title="Previous Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;</button>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(3)" title="Next Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;</button>
                    <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(4)" title="Last Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;&gt;</button>
                    &nbsp;&nbsp;&nbsp;
                    <span id="pgNo" name="pgNo"></span>
                    </td>
                    <td align="right" valign="middle" style="width:24px" >
                    <button disabled hidefocus="true" id="btnClose" tabIndex="-1" tabStop="false" onclick="closeMe()" title="Close Window" class="toolbarButton"><img src="/images/close.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></button>
                    </td>
                    </tr> 
                    </table> 
                    <!-- <img src="/images/spacer.gif" style="width:100%;height:0px;border-top:1px solid #696969;border-bottom:1px solid #D3D3D3;"> -->
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <object id="pdf1" name="pdf1" classid="CLSID:CA8A9780-280D-11CF-A24D-44455354000012" width="100%" height="100%">
                    <param name="src" value="getFile.asp?doc=<%=request.QueryString("docPath")%>">
                    <!-- if the above object failed, then try to embed the document.-->
                    <embed id="pdf1" name="pdf1" src="getFile.asp?doc=<%=request.QueryString("docPath")%>" width="100%" height="100%"></embed>
                    <!-- if there was no viewer to support both the embed as well as the object tags, then display message
                         so that user can install the adobe viewer --> 
                    <center>
                        You do not have a PDF viewer installed on your computer.<br>
                        <a href="install/pdf/">Click here to download and install Adobe PDF Reader.</a>
                    </center>
                </object>                
                
                <script>
                    
                    window.setTimeout("initPage()", 2000);
                </script>
              </td>
            </tr>
          </table>
    </body>
</html>

