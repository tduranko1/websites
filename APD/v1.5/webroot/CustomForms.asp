<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
    
        Dim strLynxID
        Dim strUserID
        Dim strVehNum
        Dim strInsuranceCompanyID 

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        strUserID = Request("UserID")
        strVehNum = Request("VehNum")
        strInsuranceCompanyID = Request("InsuranceCompanyID")

        'Set objExe = CreateObject("DataPresenter.CExecute")

        'objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'objExe.AddXslParam "InsCoId", strInsuranceCompanyID
        'objExe.AddXslParam "LynxId", strLynxID
        'objExe.AddXslParam "UserId", strUserID
        'objExe.AddXslParam "VehNum", strVehNum

        ' Get claim XML and transform to HTML.
        'Response.Write objExe.ExecuteSpAsXML( "uspCustomFormsGetListXML", "CustomForms.xsl", strLynxID, strUserID)
        
        '------------------------------------'
		' APD.NET Local Params
		'------------------------------------'
        dim sParams
        sParams = "LynxId:" & strLynxID
        sParams = sParams + "|UserId:" & strUserID
        sParams = sParams + "|VehNum:" & strVehNum
        sParams = sParams + "|InsCoId:" & strInsuranceCompanyID

		'------------------------------------'
		' APD.NET Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
        'response.write("sASPXSessionKey" & " - " & sASPXSessionKey & "<br/>")
        'response.write("sASPXWindowID" & " - " & sASPXWindowID & "<br/>")
        'response.write("strUserID" & " - " & strUserID & "<br/>")
        'response.write("strLynxID" & " - " & strLynxID & "<br/>")
        'response.write("strVehNum" & " - " & strVehNum & "<br/>")
        'response.write("strInsuranceCompanyID" & " - " & strInsuranceCompanyID & "<br/>")
		'response.end

		'------------------------------------'
		' APD.NET Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("CustomForms.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & SASPXWindowID & "&Params=" & sParams)
    End Sub
%>
