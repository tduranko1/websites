<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
        Dim oExe, strUploadLink, sUserID
        
        sUserID = Request( "UserId" )
        Set oExe = CreateObject("DataPresenter.CExecute")
        oExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, sUserID
        strUploadLink = oExe.Setting("PursuitAudit/@url")
        set oExe = nothing

%>
<html xmlns:IE>
<head>
	<title>Expanded Comments</title>
   <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
   <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
   <SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>
   <script language="JavaScript">
      var strFormID, strSupplementFormID, strUserID, strLynxID, strClaimAspectID, strServiceChannelCD, strProc, strPDFPath;
      var blnDataReady = false;
      var uploadLink = "<%=strUploadLink %>";

      function setFormParam(obj) {
         if (obj) {
            strFormID = obj.FormID;
            strSupplementFormID = obj.SupplementFormID;
            strUserID = obj.UserID;
            strLynxID = obj.LynxID;
            strClaimAspectID = obj.ClaimAspectID;
            strServiceChannelCD = obj.ServiceChannelCD;
            strProc = obj.SP;
            strPDFPath = obj.PDFform;

            UserName.value = parent.strUserName;
            UserPhone.value = parent.strUserPhone;
            UserEmail.value = parent.strUserEmail;

            getData();
         }
      }

      function getData() {
         var sProc = strProc;
         var sRequest = "ClaimAspectID=" + strClaimAspectID +
                        "&UserID=" + strUserID;

         var sMethod = "ExecuteSpNpAsXML";

         //alert(sProc); return;

         var aRequests = new Array();
         aRequests.push({ procName: sProc,
            method: sMethod,
            data: sRequest
         }
                       );
         var sXMLRequest = makeXMLSaveString(aRequests);
         //alert(sXMLRequest); return;
         var objRet = XMLSave(sXMLRequest);

         if (objRet && objRet.code == 0) {
            blnDataReady = true;
            var strProcResultXML = objRet.xml.selectSingleNode("//Result/Root").xml;
            xmlData.loadXML(strProcResultXML);
         }
      }

      function getFormData(str) {
         if (str != "") {
            ShopFaxNumber.value = formatPhone(txtFax.value);
            var objXML = document.createElement("XML");
            var objRoot;
            objXML.loadXML("<CustomForm><Form/></CustomForm>");
            objRoot = objXML.selectSingleNode("//Form");
            objRoot.setAttribute("formfile", strPDFPath);
            objRoot.setAttribute("xslfdf", strPDFPath.substr(0, strPDFPath.length - 3) + "xsl");
            var objData = getDataXML(divData, "Data", true, false, true);
            if (objData) {
               objRoot.appendChild(objData.firstChild);
            }
            //alert(objXML.xml);
            return objXML;
         }
      }

      function initFormData() {
         if (blnDataReady == false) return;
         var objRoot = xmlData.selectSingleNode("/Root");

         LynxId.value = objRoot.getAttribute("LynxID");
         RequestDate.value = objRoot.getAttribute("ReportDate");
         ClaimNumber.value = objRoot.getAttribute("ClaimNumber");
         LossDate.value = objRoot.getAttribute("DateOfLoss");
         Party.value = objRoot.getAttribute("Party");
         Deductible.value = objRoot.getAttribute("Deductible");
         VehicleOwnerName.value = objRoot.getAttribute("VehicleOwnerName");
         VehicleOwnerAddress.value = objRoot.getAttribute("VehicleOwnerAddress");
         VehicleOwnerAddressCity.value = objRoot.getAttribute("VehicleOwnerAddressCity");
         VehicleOwnerAddressZip.value = objRoot.getAttribute("VehicleOwnerAddressZip");
         VehicleOwnerAddressState.value = objRoot.getAttribute("VehicleOwnerAddressState");
         VehicleOwnerPhone.value = formatPhone(objRoot.getAttribute("VehicleOwnerPhone"));
         VehicleTag.value = objRoot.getAttribute("VehicleTag");
         VehicleYear.value = objRoot.getAttribute("VehicleYear");
         VehicleMake.value = objRoot.getAttribute("VehicleMake");
         VehicleModel.value = objRoot.getAttribute("VehicleModel");
         VIN.value = objRoot.getAttribute("VehicleVIN");
         Mileage.value = objRoot.getAttribute("VehicleMileage");
         ShopName.value = objRoot.getAttribute("RepairLocationName");
         ShopAddress.value = objRoot.getAttribute("RepairLocationAddress");
         ShopAddressCity.value = objRoot.getAttribute("RepairLocationCity");
         ShopAddressState.value = objRoot.getAttribute("RepairLocationState");
         ShopAddressZip.value = objRoot.getAttribute("RepairLocationZip");
         ShopPhoneNumber.value = formatPhone(objRoot.getAttribute("RepairLocationPhone"));
         ShopFaxNumber.value = objRoot.getAttribute("RepairLocationFax");
         txtEmailAddress.value = objRoot.getAttribute("RepairLocationEmail");
         InspectionDate.value = objRoot.getAttribute("RepairInspectionDate");
         txtFax.value = objRoot.getAttribute("RepairLocationFax");
         ShopEmailAddress.value = objRoot.getAttribute("RepairLocationEmail");
         
         ClaimOwnerName.value = objRoot.getAttribute("ClaimRepName");
         ClaimOwnerTitle.value = objRoot.getAttribute("ClaimRepTitle");
         ClaimOwnerPhone.value = objRoot.getAttribute("ClaimRepPhone");
         ClaimOwnerEmail.value = objRoot.getAttribute("ClaimRepEmail");

         uploadLink += strLynxID + "-" + strClaimAspectID + "-" + objRoot.getAttribute("InsuranceCompanyID");
         UploadLink.value = uploadLink;

         try {
            if (txtEmailAddress.value != "") {
               rbSendViaEmail.value = "1";
            } else {
               rbSendViaFax.value = "2";
            }
         } catch (e) { }



         txtShopName.innerText = objRoot.getAttribute("RepairLocationName");
         setDestinations();
      }

      function setDestinations() {
         try {
            if (typeof (parent.clearDestinations) == "function") {
               parent.clearDestinations();
            }

            if (rbSendViaEmail.value == "1") {
               if (typeof (parent.addEmailDestination) == "function") {
                  parent.addEmailDestination(UserEmail.value, txtEmailAddress.value, "Audit Assignment", "Please see attached assignment.");
               }
            }

            if (rbSendViaFax.value == "2") {
               if (typeof (parent.addFaxDestination) == "function") {
                  parent.addFaxDestination("APD FAX: Pursuit Audit Shop Fax to " + ShopName.value, txtFax.value);
               }
            }
         } catch (e) { alert(e.description) } 
      }

      function validateFormData() {
         if (rbSendViaEmail.value == "1") {
            var strEmailAddress = txtEmailAddress.value.Trim();
            if (strEmailAddress == "") {
               ClientWarning("Please enter the Shop Email Address.");
               txtEmailAddress.setFocus();
               return false;
            }

            if (strEmailAddress.indexOf(",") > 0) {
               var emailAddresses = strEmailAddress.split(",");
               var invalid = false;
               var invalidEmail = "";
               for (var i = 0; i < emailAddresses.length; i++) {
                  if (!isEmail(emailAddresses[i].Trim())) {
                     invalid = true;
                     invalidEmail += emailAddresses[i];
                  }
               }
               if (invalid) {
                  ClientWarning("The following email addresses are invalid.\n\n" + invalidEmail);
                  txtEmailAddress.setFocus();
                  return false;
               }
            } else {
               if (!isEmail(strEmailAddress)) {
                  ClientWarning("Invalid Email format for Shop Email Address.");
                  txtEmailAddress.setFocus();
                  return false;
               }
            }
         }
         if (rbSendViaFax.value == "2" && txtFax.value == "") {
            ClientWarning("Please enter the Shop Fax Number.");
            txtFax.setFocus();
            return false;
         }
      }

   </script>
</head>
<body unselectable="on" style="margin:0px;padding:0px">
   <div id="divData" style="padding:5px;">
      <table border="0" cellpadding="3" cellspacing="0">
         <tr>
            <td colspan="2">Pursuit Audit Shop Assignment will be automatically sent to <span id="txtShopName">shop</span> at:</td>
            <!--<td>
               <IE:APDInputPhone id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="1" onChange="setDestinations()">
               </IE:APDInputPhone>
            </td>-->
         </tr>
         <tr>
            <td colspan="2">Send via:</td>
         </tr>
         <tr>
            <td colspan="2"/>
         </tr>
         <tr>
            <td>
               <IE:APDRadioGroup id="rbSendVia" name="rbSendVia" canDirty="false">
                  <IE:APDRadio id="rbSendViaEmail" name="rbSendViaEmail" caption="Email" value="99" groupName="rbSendVia" checkedValue="1" uncheckedValue="99" CCTabIndex="13" onChange="setDestinations()" />
               </IE:APDRadioGroup>
            </td>
            <td>
               <IE:APDInput id="txtEmailAddress" name="txtEmailAddress" value="" size="75" maxLength="100" width=""  required="true" canDirty="false" CCDisabled="false" CCTabIndex="s" onChange="setDestinations()" />
            </td>
         </tr>
         <tr>
            <td>
               <IE:APDRadio id="rbSendViaFax" name="rbSendViaFax" caption="Fax" value="99" groupName="rbSendVia" checkedValue="2" uncheckedValue="99" CCTabIndex="13" onChange="setDestinations()" />
            </td>
            <td>
               <IE:APDInputPhone id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="1" onChange="setDestinations()">
               </IE:APDInputPhone>
            </td>
         </tr>
      </table>
      <input type="hidden" name="RequestDate" id="RequestDate" friendlyName="Request Date"/>
      <input type="hidden" name="LynxId" id="LynxId" friendlyName="LYNX ID"/>
      <input type="hidden" name="ClaimNumber" id="ClaimNumber" friendlyName="Claim Number"/>
      <input type="hidden" name="LossDate" id="LossDate" friendlyName="Loss Date"/>
      <input type="hidden" name="Party" id="Party" friendlyName="Party"/>
      <input type="hidden" name="Deductible" id="Deductible" friendlyName="Deductible"/>
      <input type="hidden" name="VehicleOwnerName" id="VehicleOwnerName" friendlyName="Vehicle Owner Name"/>
      <input type="hidden" name="VehicleOwnerAddress" id="VehicleOwnerAddress" friendlyName="Vehicle Owner Address"/>
      <input type="hidden" name="VehicleOwnerAddressCity" id="VehicleOwnerAddressCity" friendlyName="Vehicle Owner City"/>
      <input type="hidden" name="VehicleOwnerAddressZip" id="VehicleOwnerAddressZip" friendlyName="Vehicle Owner Zip"/>
      <input type="hidden" name="VehicleOwnerAddressState" id="VehicleOwnerAddressState" friendlyName="Vehicle Owner State"/>
      <input type="hidden" name="VehicleOwnerPhone" id="VehicleOwnerPhone" friendlyName="Vehicle Owner Phone"/>
      <input type="hidden" name="VehicleOwnerEmail" id="VehicleOwnerEmail" friendlyName="Vehicle Owner Email"/>
      <input type="hidden" name="VehicleTag" id="VehicleTag" friendlyName="Vehicle Tag"/>
      <input type="hidden" name="VehicleYear" id="VehicleYear" friendlyName="Vehicle Year"/>
      <input type="hidden" name="VehicleMake" id="VehicleMake" friendlyName="Vehicle Make"/>
      <input type="hidden" name="VehicleModel" id="VehicleModel" friendlyName="Vehicle Model"/>
      <input type="hidden" name="VIN" id="VIN" friendlyName="Vehicle VIN"/>
      <input type="hidden" name="Mileage" id="Mileage" friendlyName="Vehicle Mileage"/>
      <input type="hidden" name="ShopName" id="ShopName" friendlyName="Shop Name"/>
      <input type="hidden" name="ShopAddress" id="ShopAddress" friendlyName="Shop Address"/>
      <input type="hidden" name="ShopAddressCity" id="ShopAddressCity" friendlyName="Shop Address City"/>
      <input type="hidden" name="ShopAddressState" id="ShopAddressState" friendlyName="Shop Address Stae"/>
      <input type="hidden" name="ShopAddressZip" id="ShopAddressZip" friendlyName="Shop Address Zip"/>
      <input type="hidden" name="ShopPhoneNumber" id="ShopPhoneNumber" friendlyName="Shop Phone Number"/>
      <input type="hidden" name="ShopFaxNumber" id="ShopFaxNumber" friendlyName="Shop Fax Number"/>
      <input type="hidden" name="ShopEmailAddress" id="ShopEmailAddress" friendlyName="Shop Email Address"/>
      <input type="hidden" name="InspectionDate" id="InspectionDate" friendlyName="Inspection Date"/>
      <input type="hidden" name="UploadLink" id="UploadLink" friendlyName="Upload Link"/>
      <input type="hidden" name="ClaimOwnerName" id="ClaimOwnerName" friendlyName="Claim Owner Name"/>
      <input type="hidden" name="ClaimOwnerTitle" id="ClaimOwnerTitle" friendlyName="Claim Owner Title"/>
      <input type="hidden" name="ClaimOwnerPhone" id="ClaimOwnerPhone" friendlyName="Claim Owner Phone"/>
      <input type="hidden" name="ClaimOwnerEmail" id="ClaimOwnerEmail" friendlyName="Claim Owner Email"/>
      <input type="hidden" name="UserName" id="UserName" friendlyName="User Name"/>
      <input type="hidden" name="UserPhone" id="UserPhone" friendlyName="User Phone"/>
      <input type="hidden" name="UserEmail" id="UserEmail" friendlyName="User Email"/>
      <input type="hidden" name="LossState" id="LossState" friendlyName="Loss State"/>
      <div class="TDLabel" style="text-align:center;padding:30px;font-weight:bold;">
         No user data required on this form.
      </div>
   </div>
   
   <xml id="xmlData" ondatasetcomplete="initFormData()"><Root/></xml>
</body>
</html>
