<html xmlns:IE>
<head>
	<title>Expanded Comments</title>
   <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
   <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
   <script language="JavaScript">
      var strFormID, strSupplementFormID, strUserID, strLynxID, strClaimAspectID, strServiceChannelCD, strProc, strPDFPath;
      var blnDataReady = false;
      
      function setFormParam(obj){
         if (obj){
            strFormID = obj.FormID;
            strSupplementFormID = obj.SupplementFormID;
            strUserID = obj.UserID;
            strLynxID = obj.LynxID;
            strClaimAspectID = obj.ClaimAspectID;
            strServiceChannelCD = obj.ServiceChannelCD;
            strProc = obj.SP;
            strPDFPath = obj.PDFform;
            
            UserName.value = parent.strUserName;
            UserPhone.value = parent.strUserPhone;
            UserEmail.value = parent.strUserEmail;
            
			//alert(obj.SP);
            getData();
         }
      }

      function getData(){
         var sProc = strProc;
         var sRequest = "ClaimAspectID=" + strClaimAspectID + 
                        "&UserID=" + strUserID;

         var sMethod = "ExecuteSpNpAsXML";
         var aRequests = new Array();
         aRequests.push( { procName : sProc,
                           method   : sMethod,
                           data     : sRequest }
                       );
         var sXMLRequest = makeXMLSaveString(aRequests);
         //alert(sXMLRequest); return;
         var objRet = XMLSave(sXMLRequest);
       
         if (objRet && objRet.code == 0){
            blnDataReady = true;
            var strProcResultXML = objRet.xml.selectSingleNode("//Result/Root").xml;
            xmlData.loadXML(strProcResultXML);
         }
      }
            
      function getFormData(str){
         if (str != ""){
             var objXML = document.createElement("XML");
             var objRoot;
             objXML.loadXML("<CustomForm><Form/></CustomForm>");
             objRoot = objXML.selectSingleNode("//Form");
             objRoot.setAttribute("formfile", strPDFPath);
             objRoot.setAttribute("xslfdf", strPDFPath.substr(0, strPDFPath.length - 3) + "xsl");
             var objData = getDataXML(divData, "Data", true, false, true);
             if (objData){
                objRoot.appendChild(objData.firstChild);
             }
             //alert(objXML.xml);
             return objXML;
         }
      }
      
      function initFormData(){
         if (blnDataReady == false) return;
         var objRoot = xmlData.selectSingleNode("/Root");

         LYNXID.value = strLynxID;
         ClaimNumber.value = objRoot.getAttribute("ClaimNumber");
         CarrierRepLName.value = objRoot.getAttribute("CarrierRepLName");
         CarrierRepFName.value = objRoot.getAttribute("CarrierRepFName");
         InsuredLName.value = objRoot.getAttribute("InsuredLName");
         InsuredFName.value = objRoot.getAttribute("InsuredFName");
         InsuredBName.value = objRoot.getAttribute("InsuredBName");
         OwnerLName.value = objRoot.getAttribute("OwnerLName");
         OwnerFName.value = objRoot.getAttribute("OwnerFName");
         OwnerBName.value = objRoot.getAttribute("OwnerBName");
         OwnerZip.value = objRoot.getAttribute("OwnerZip");
         OwnerState.value = objRoot.getAttribute("OwnerState");
         OwnerDayPhone.value = objRoot.getAttribute("OwnerDayPhone");
         OwnerDayPhoneExt.value = objRoot.getAttribute("OwnerDayPhoneExt");
         OwnerNightPhone.value = objRoot.getAttribute("OwnerNightPhone");
         OwnerNightPhoneExt.value = objRoot.getAttribute("OwnerNightPhoneExt");
         OwnerAltPhone.value = objRoot.getAttribute("OwnerAltPhone");
         OwnerAltPhoneExt.value = objRoot.getAttribute("OwnerAltPhoneExt");
         VehicleLicenseState.value = objRoot.getAttribute("VehicleLicenseState");
         VehicleYear.value = objRoot.getAttribute("VehicleYear");
         VehicleMake.value = objRoot.getAttribute("VehicleMake");
         VehicleModel.value = objRoot.getAttribute("VehicleModel");
         txtFax.value = objRoot.getAttribute("ShopFaxNumber");
         ShopFaxNumber.value = objRoot.getAttribute("ShopFaxNumber");
         ShopName.value = objRoot.getAttribute("ShopName");
         UserName.value = objRoot.getAttribute("UserName");
         UserPhone.value = objRoot.getAttribute("UserPhone");
         UserEmail.value = objRoot.getAttribute("UserEmail");
         LossState.value = objRoot.getAttribute("LossState");
         LossDate.value = objRoot.getAttribute("LossDate");
		 
		 VehicleTag.value = objRoot.getAttribute("VehicleTag");
         VIN.value = objRoot.getAttribute("VehicleVIN");
         InsuredPhone.value = objRoot.getAttribute("InsuredPhone");
         VehicleMileage.value = objRoot.getAttribute("VehicleMileage");

		 //alert(objRoot.xml);
		 
         txtShopName.innerText = objRoot.getAttribute("ShopName");
         setDestinations();
      }
      
      function setDestinations(){
         if (ShopFaxNumber.value != ""){
            try {
            if (typeof(parent.clearDestinations) == "function"){
               parent.clearDestinations();
            }
            if (typeof(parent.addFaxDestination) == "function"){
               parent.addFaxDestination("APD FAX: BCIF Request to " + ShopName.value, txtFax.value);
            }
            } catch (e) {alert(e.description)}
         }
      }
      
      function validateFormData(){
         if (txtFax.value == ""){
            ClientWarning("Please enter the Shop Fax Number.");
            txtFax.setFocus();
            return false;
         }
      }

   </script>
</head>
<body unselectable="on" style="margin:0px;padding:0px">
   <div id="divData" style="padding:5px;">
      <table border="0" cellpadding="3" cellspacing="0">
         <tr>
            <td>BCIF Request will automatically faxed to the <span id="txtShopName">shop</span> at:</td>
            <td>
               <IE:APDInputPhone id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="1" onChange="setDestinations()">
                  <xsl:attribute name="value"><xsl:value-of select="/Root/@ShopFaxNumber"/></xsl:attribute>
               </IE:APDInputPhone>
            </td>
         </tr>
      </table>
      <input type="hidden" name="LYNXID" id="LYNXID" friendlyName="LYNX ID"/>
      <input type="hidden" name="ClaimNumber" id="ClaimNumber" friendlyName="Claim Number"/>
      <input type="hidden" name="CarrierRepLName" id="CarrierRepLName" friendlyName="Carrier Rep Last Name"/>
      <input type="hidden" name="CarrierRepFName" id="CarrierRepFName" friendlyName="Carrier Rep First Name"/>
      <input type="hidden" name="InsuredLName" id="InsuredLName" friendlyName="Insured Last Name"/>
      <input type="hidden" name="InsuredFName" id="InsuredFName" friendlyName="Insured First Name"/>
      <input type="hidden" name="InsuredBName" id="InsuredBName" friendlyName="Insured Business Name"/>
      <input type="hidden" name="OwnerLName" id="OwnerLName" friendlyName="Owner Last Name"/>
      <input type="hidden" name="OwnerFName" id="OwnerFName" friendlyName="Owner First Name"/>
      <input type="hidden" name="OwnerBName" id="OwnerBName" friendlyName="Owner Business Name"/>
      <input type="hidden" name="OwnerZip" id="OwnerZip" friendlyName="Owner Zip"/>
      <input type="hidden" name="OwnerState" id="OwnerState" friendlyName="Owner State"/>
      <input type="hidden" name="OwnerDayPhone" id="OwnerDayPhone" friendlyName="Owner Day Phone"/>
      <input type="hidden" name="OwnerDayPhoneExt" id="OwnerDayPhoneExt" friendlyName="Owner Day Phone Extn"/>
      <input type="hidden" name="OwnerNightPhone" id="OwnerNightPhone" friendlyName="Owner Night Phone"/>
      <input type="hidden" name="OwnerNightPhoneExt" id="OwnerNightPhoneExt" friendlyName="Owner Night Phone Extn"/>
      <input type="hidden" name="OwnerAltPhone" id="OwnerAltPhone" friendlyName="Owner Alt Phone"/>
      <input type="hidden" name="OwnerAltPhoneExt" id="OwnerAltPhoneExt" friendlyName="Owner Alt Phone Extn"/>
      <input type="hidden" name="VehicleLicenseState" id="VehicleLicenseState" friendlyName="Vehicle License State"/>
      <input type="hidden" name="VehicleYear" id="VehicleYear" friendlyName="Vehicle Year"/>
      <input type="hidden" name="VehicleMake" id="VehicleMake" friendlyName="Vehicle Make"/>
      <input type="hidden" name="VehicleModel" id="VehicleModel" friendlyName="Vehicle Model"/>
      <input type="hidden" name="ShopFaxNumber" id="ShopFaxNumber" friendlyName="Shop Fax Number"/>
      <input type="hidden" name="ShopName" id="ShopName" friendlyName="Shop Name"/>
      <input type="hidden" name="UserName" id="UserName" friendlyName="User Name"/>
      <input type="hidden" name="UserPhone" id="UserPhone" friendlyName="User Phone"/>
      <input type="hidden" name="UserEmail" id="UserEmail" friendlyName="User Email"/>
      <input type="hidden" name="LossState" id="LossState" friendlyName="Loss State"/>
      <input type="hidden" name="LossDate" id="LossDate" friendlyName="Loss Date"/>

      <input type="hidden" name="VehicleTag" id="VehicleTag" friendlyName="Vehicle Tag"/>
	  <input type="hidden" name="VIN" id="VIN" friendlyName="Vehicle VIN"/>
      <input type="hidden" name="InsuredPhone" id="InsuredPhone" friendlyName="Insured Day Phone"/>
      <input type="hidden" name="VehicleOwnerName" id="VehicleOwnerName" friendlyName="Vehicle Owner Name"/>
      <input type="hidden" name="VehicleMileage" id="VehicleMileage" friendlyName="Vehicle Mileage"/>

      <div class="TDLabel" style="text-align:center;padding:30px;font-weight:bold;">
         No user data required on this form.
      </div>
   </div>
   
   <xml id="xmlData" ondatasetcomplete="initFormData()"><Root/></xml>
</body>
</html>
