<html xmlns:IE>
<head>
	<title>Expanded Comments</title>
   <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
   <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
   <script language="JavaScript">
       var strFormID, strSupplementFormID, strUserID, strLynxID, strClaimAspectID, strServiceChannelCD, strProc, strPDFPath;
       var blnDataReady = false;

       function setFormParam(obj) {
           if (obj) {
               strFormID = obj.FormID;
               strSupplementFormID = obj.SupplementFormID;
               strUserID = obj.UserID;
               strLynxID = obj.LynxID;
               strClaimAspectID = obj.ClaimAspectID;
               strServiceChannelCD = obj.ServiceChannelCD;
               strProc = obj.SP;
               strPDFPath = obj.PDFform;

               UserName.value = parent.strUserName;
               UserPhone.value = parent.strUserPhone;
               UserEmail.value = parent.strUserEmail;

               getData();
           }
       }

       function getData() {
           var sProc = strProc;
           var sRequest = "ClaimAspectID=" + strClaimAspectID +
                        "&UserID=" + strUserID;

           var sMethod = "ExecuteSpNpAsXML";
           //alert(strProc);
           var aRequests = new Array();
           aRequests.push({ procName: sProc,
               method: sMethod,
               data: sRequest
           }
                       );
           var sXMLRequest = makeXMLSaveString(aRequests);
           //alert(sXMLRequest); return;
           var objRet = XMLSave(sXMLRequest);

           if (objRet && objRet.code == 0) {
               blnDataReady = true;
               var strProcResultXML = objRet.xml.selectSingleNode("//Result/Root").xml;
               xmlData.loadXML(strProcResultXML);
               //window.clipboardData.setData('text', xmlData.xml);
           }
       }

       function getFormData(str) {
           if (str != "") {
               var objXML = document.createElement("XML");
               var objRoot;
               objXML.loadXML("<CustomForm><Form/></CustomForm>");
               objRoot = objXML.selectSingleNode("//Form");
               objRoot.setAttribute("formfile", strPDFPath);
               objRoot.setAttribute("xslfdf", strPDFPath.substr(0, strPDFPath.length - 3) + "xsl");
               var objData = getDataXML(divData, "Data", true, false, true);
               if (objData) {
                   objRoot.appendChild(objData.firstChild);
               }
               //alert(objXML.xml);
               //window.clipboardData.setData('text', objXML.xml);

               return objXML;
           }
       }

       function initFormData() {
           if (blnDataReady == false) return;
           var objRoot = xmlData.selectSingleNode("/Root");

           ClaimNumber.value = objRoot.getAttribute("ClaimNumber");
           LynxID.value = objRoot.getAttribute("LynxID");
           CarrierRepLName.value = objRoot.getAttribute("CarrierRepLName");
           CarrierRepFName.value = objRoot.getAttribute("CarrierRepFName");
           AuditorName.value = objRoot.getAttribute("AuditorName");
           AuditorTitle.value = objRoot.getAttribute("AuditorTitle");
           AuditorPhone.value = objRoot.getAttribute("AuditorPhone");
           AuditorEmail.value = objRoot.getAttribute("AuditorEmail");    
           InsuredLName.value = objRoot.getAttribute("InsuredLName");
           InsuredFName.value = objRoot.getAttribute("InsuredFName");
           InsuredBName.value = objRoot.getAttribute("InsuredBName");
           OwnerLName.value = objRoot.getAttribute("OwnerLName");
           OwnerFName.value = objRoot.getAttribute("OwnerFName");
           OwnerBName.value = objRoot.getAttribute("OwnerBName");
           OwnerZip.value = objRoot.getAttribute("OwnerZip");
           OwnerState.value = objRoot.getAttribute("OwnerState");
           OwnerDayPhone.value = objRoot.getAttribute("OwnerDayPhone");
           OwnerDayPhoneExt.value = objRoot.getAttribute("OwnerDayPhoneExt");
           OwnerNightPhone.value = objRoot.getAttribute("OwnerNightPhone");
           OwnerNightPhoneExt.value = objRoot.getAttribute("OwnerNightPhoneExt");
           OwnerAltPhone.value = objRoot.getAttribute("OwnerAltPhone");
           OwnerAltPhoneExt.value = objRoot.getAttribute("OwnerAltPhoneExt");
           VehicleLicenseState.value = objRoot.getAttribute("VehicleLicenseState");
           VehicleYear.value = objRoot.getAttribute("VehicleYear");
           VehicleMake.value = objRoot.getAttribute("VehicleMake");
           VehicleModel.value = objRoot.getAttribute("VehicleModel");
           VehicleVIN.value = objRoot.getAttribute("VehicleVIN");
           VehicleBodyStyle.value = objRoot.getAttribute("VehicleBodyStyle");
           VehicleColor.value = objRoot.getAttribute("VehicleColor");
           VehicleMileage.value = objRoot.getAttribute("VehicleMileage");
           txtFax.value = objRoot.getAttribute("ShopFaxNumber");
           ShopFaxNumber.value = objRoot.getAttribute("ShopFaxNumber");
           txtEmailAddress.value = objRoot.getAttribute("CommunicationAddress");
           ShopName.value = objRoot.getAttribute("ShopName");
           UserName.value = objRoot.getAttribute("UserName");
           UserPhone.value = objRoot.getAttribute("UserPhone");
           UserEmail.value = objRoot.getAttribute("UserEmail");
           LossState.value = objRoot.getAttribute("LossState");
           LossDate.value = objRoot.getAttribute("LossDate");
           ShopFax.value = objRoot.getAttribute("ShopFax");
           ShopPhone.value = objRoot.getAttribute("ShopPhone");
           ShopEmail.value = objRoot.getAttribute("ShopEmail");
           Carrier.value = objRoot.getAttribute("Carrier");
           ClaimRepPhone.value = objRoot.getAttribute("CarrierRepPhone");
           
           rbSendViaFax.value = "2";

           txtShopName.innerText = objRoot.getAttribute("ShopName");
           setDestinations();
       }

       function setDestinations() {
           try {
               if (typeof (parent.clearDestinations) == "function") {
                   parent.clearDestinations();
               }

               if (rbSendViaEmail.value == "1") {
                   if (typeof (parent.addEmailDestination) == "function") {
                       parent.addEmailDestination(UserEmail.value, txtEmailAddress.value, "Total Loss Vehicle Vehicle Condition Report Request :" + ClaimNumber.value + ":Claim #:" + "/Insured:" + InsuredLName.value + InsuredFName.value + "/Loss Date:" + LossDate.value + "/LynxID:" + LynxID.value, "Please see attached assignment.");
                   }
               }

               if (rbSendViaFax.value == "2") {
                   if (typeof (parent.addFaxDestination) == "function") {
                       parent.addFaxDestination("APD FAX: Vehicle Condition Report Request Fax to " + ShopName.value, txtFax.value);
                   }
               }
           } catch (e) { alert(e.description) }
       }

       function validateFormData() {
           if (rbSendViaEmail.value == "1") {
               var strEmailAddress = txtEmailAddress.value.Trim();
               if (strEmailAddress == "") {
                   ClientWarning("Please enter the Shop Email Address.");
                   txtEmailAddress.setFocus();
                   return false;
               }

               if (strEmailAddress.indexOf(",") > 0) {
                   var emailAddresses = strEmailAddress.split(",");
                   var invalid = false;
                   var invalidEmail = "";
                   for (var i = 0; i < emailAddresses.length; i++) {
                       if (!isEmail(emailAddresses[i].Trim())) {
                           invalid = true;
                           invalidEmail += emailAddresses[i];
                       }
                   }
                   if (invalid) {
                       ClientWarning("The following email addresses are invalid.\n\n" + invalidEmail);
                       txtEmailAddress.setFocus();
                       return false;
                   }
               } else {
                   if (!isEmail(strEmailAddress)) {
                       ClientWarning("Invalid Email format for Shop Email Address.");
                       txtEmailAddress.setFocus();
                       return false;
                   }
               }
           }
           if (rbSendViaFax.value == "2" && txtFax.value == "") {
               ClientWarning("Please enter the Shop Fax Number.");
               txtFax.setFocus();
               return false;
           }
       }

   </script>
</head>
<body unselectable="on" style="margin:0px;padding:0px">
   <div id="divData" style="padding:5px;">
      <table border="0" cellpadding="3" cellspacing="0">
         <tr>
            <td colspan="2">Autosource Condition Book will be automatically sent to <span id="txtShopName">shop</span> at:</td>
            <!--<td>
               <IE:APDInputPhone id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="1" onChange="setDestinations()">
               </IE:APDInputPhone>
            </td>-->
         </tr>
         <tr>
            <td colspan="2">Send via:</td>
         </tr>
         <tr>
            <td colspan="2"/>
         </tr>
         <tr>
            <td>
               <IE:APDRadioGroup id="rbSendVia" name="rbSendVia" canDirty="false">
                  <IE:APDRadio id="rbSendViaEmail" name="rbSendViaEmail" caption="Email" value="99" groupName="rbSendVia" checkedValue="1" uncheckedValue="99" CCTabIndex="13" onChange="setDestinations()" />
               </IE:APDRadioGroup>
            </td>
            <td>
               <IE:APDInput id="txtEmailAddress" name="txtEmailAddress" value="" size="75" maxLength="100" width=""  required="true" canDirty="false" CCDisabled="false" CCTabIndex="s" onChange="setDestinations()" />
            </td>
         </tr>
         <tr>
            <td>
               <IE:APDRadio id="rbSendViaFax" name="rbSendViaFax" caption="Fax" value="99" groupName="rbSendVia" checkedValue="2" uncheckedValue="99" CCTabIndex="13" onChange="setDestinations()" />
            </td>
            <td>
               <IE:APDInputPhone id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="1" onChange="setDestinations()">
               </IE:APDInputPhone>
            </td>
         </tr>
      </table>

        <input type="hidden" name="LynxID" id="LynxID" friendlyname="Lynx ID" />       
        <input type="hidden" name="ShopFax" id="ShopFax" friendlyname="Shop Fax" />       
        <input type="hidden" name="ShopPhone" id="ShopPhone" friendlyname="Shop Phone" />       
        <input type="hidden" name="ShopEmail" id="ShopEmail" friendlyname="Shop Email" />       

        <input type="hidden" name="OwnerName" id="OwnerName" friendlyname="Owner Name" />       
        <input type="hidden" name="ClaimRepName" id="ClaimRepName" friendlyname="Claim Representative" />
        <input type="hidden" name="ClaimRepPhone" id="ClaimRepPhone" friendlyname="Claim Phone" />
        <input type="hidden" name="ClaimRepEmail" id="ClaimRepEmail" friendlyname="Claim Email" />

        <input type="hidden" name="VehicleVIN" id="VehicleVIN" friendlyName="Vehicle VIN"/>
        <input type="hidden" name="VehicleBodyStyle" id="VehicleBodyStyle" friendlyName="Vehicle Body Style"/>
        <input type="hidden" name="VehicleColor" id="VehicleColor" friendlyName="Vehicle Color"/>
        <input type="hidden" name="VehicleMileage" id="VehicleMileage" friendlyName="Vehicle Mileage"/>

        <!-- New hidden controls are added by glsd451-->
        <input type="hidden" name="Carrier" id="Carrier" friendlyname="Carrier" />

      <input type="hidden" name="ClaimNumber" id="ClaimNumber" friendlyName="Claim Number"/>
      <input type="hidden" name="CarrierRepLName" id="CarrierRepLName" friendlyName="Carrier Rep Last Name"/>
      <input type="hidden" name="CarrierRepFName" id="CarrierRepFName" friendlyName="Carrier Rep First Name"/>
      <input type="hidden" name="CarrierRepPhone" id="CarrierRepPhone" friendlyName="Carrier Rep Phone"/>
       <input type="hidden" name="AuditorName" id="AuditorName" friendlyName="Auditor Name"/>
        <input type="hidden" name="AuditorTitle" id="AuditorTitle" friendlyName="Auditor Title"/>
        <input type="hidden" name="AuditorPhone" id="AuditorPhone" friendlyName="Auditor Phone"/>
         <input type="hidden" name="AuditorEmail" id="AuditorEmail" friendlyName="Auditor Email"/>     
      <input type="hidden" name="InsuredLName" id="InsuredLName" friendlyName="Insured Last Name"/>
      <input type="hidden" name="InsuredFName" id="InsuredFName" friendlyName="Insured First Name"/>
      <input type="hidden" name="InsuredBName" id="InsuredBName" friendlyName="Insured Business Name"/>
      <input type="hidden" name="OwnerLName" id="OwnerLName" friendlyName="Owner Last Name"/>
      <input type="hidden" name="OwnerFName" id="OwnerFName" friendlyName="Owner First Name"/>
      <input type="hidden" name="OwnerBName" id="OwnerBName" friendlyName="Owner Business Name"/>
      <input type="hidden" name="OwnerZip" id="OwnerZip" friendlyName="Owner Zip"/>
      <input type="hidden" name="OwnerState" id="OwnerState" friendlyName="Owner State"/>
      <input type="hidden" name="OwnerDayPhone" id="OwnerDayPhone" friendlyName="Owner Day Phone"/>
      <input type="hidden" name="OwnerDayPhoneExt" id="OwnerDayPhoneExt" friendlyName="Owner Day Phone Extn"/>
      <input type="hidden" name="OwnerNightPhone" id="OwnerNightPhone" friendlyName="Owner Night Phone"/>
      <input type="hidden" name="OwnerNightPhoneExt" id="OwnerNightPhoneExt" friendlyName="Owner Night Phone Extn"/>
      <input type="hidden" name="OwnerAltPhone" id="OwnerAltPhone" friendlyName="Owner Alt Phone"/>
      <input type="hidden" name="OwnerAltPhoneExt" id="OwnerAltPhoneExt" friendlyName="Owner Alt Phone Extn"/>
      <input type="hidden" name="VehicleLicenseState" id="VehicleLicenseState" friendlyName="Vehicle License State"/>
      <input type="hidden" name="VehicleYear" id="VehicleYear" friendlyName="Vehicle Year"/>
      <input type="hidden" name="VehicleMake" id="VehicleMake" friendlyName="Vehicle Make"/>
      <input type="hidden" name="VehicleModel" id="VehicleModel" friendlyName="Vehicle Model"/>
     <input type="hidden" name="CommunicationAddress" id="CommunicationAddress" friendlyName="Communication Address"/>
      <input type="hidden" name="ShopFaxNumber" id="ShopFaxNumber" friendlyName="Shop Fax Number"/>
      <input type="hidden" name="ShopName" id="ShopName" friendlyName="Shop Name"/>
      <input type="hidden" name="UserName" id="UserName" friendlyName="User Name"/>
      <input type="hidden" name="UserPhone" id="UserPhone" friendlyName="User Phone"/>
      <input type="hidden" name="UserEmail" id="UserEmail" friendlyName="User Email"/>
      <input type="hidden" name="LossState" id="LossState" friendlyName="Loss State"/>
      <input type="hidden" name="LossDate" id="LossDate" friendlyName="Loss Date"/>
      <div class="TDLabel" style="text-align:center;padding:30px;font-weight:bold;">
         No user data required on this form.
      </div>
   </div>
   
   <xml id="xmlData" ondatasetcomplete="initFormData()"><Root/></xml>
</body>
</html>
