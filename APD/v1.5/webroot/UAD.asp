<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
    Dim strHTML, objExe

	On Error Resume Next

    Set objExe = InitializeSession()
    CheckError()

    objExe.AddXslParam "Environment", objExe.mToolkit.mEvents.mSettings.Environment
    CheckError()

	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSessionGetUserDetailXML", "UAD.xsl", LCase(strCsrID))
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

    On Error GoTo 0
%>

