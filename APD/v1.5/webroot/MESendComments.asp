<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%

    On Error Resume Next

    if Request("lynxid") <> "" then
      Dim objExe
      dim sClaimView
      Call GetHtml
      Set objExe = Nothing
      Call KillSessionObject
      Call CheckError
    end if

    On Error GoTo 0

    Sub GetHtml()
      on error resume next
      dim iLynxID, strInsuranceCompanyID
      dim strUserDetailXML, strEntityOwnerXML, strClaimSearchXML
      dim oXML, oUserNode, oClaimNode, oVehNode, oRefNode
      dim strRepNameFirst, strRepNameLast, strRepOfficeName, strRepPhoneDay, strRepEmailAddress, strInsCompanyName
      dim strClaimRepEmail, strClaimRepNameFirst, strClaimRepNameLast, strClaimDetail

      strInsuranceCompanyID = 185 'AllState Insurance
      iLynxID = Request("lynxid")

      Set objExe = InitializeSession()

      set oXML = Server.CreateObject("MSXML2.DOMDocument")
      oXML.async = false

      'check the validity of the LynxID
      strClaimSearchXML = objExe.ExecuteSpNpAsXML( "uspClaimSearchXML", "lynxid=" & iLynxID)

      oXML.loadXML strClaimSearchXML

      set oClaimNode = oXML.selectSingleNode("/Root/Claim[@LynxId='" & iLynxID & "']")
      if oClaimNode is nothing then
        writeError "Invalid LYNX ID. Please try again."
      end if

      set oClaimNode = nothing

      'Get the claim information
      strClaimDetailXML = objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", iLynxID)

      oXML.loadXML strClaimDetailXML

      set oClaimNode = oXML.selectSingleNode("/Root/Claim")
      set oRefNode = oXML.selectSingleNode("/Root/Reference[@Name='Vehicle 1']")

      If Not oClaimNode is Nothing And Not oRefnode Is Nothing Then

					Dim strClaimAspectID, strNoteTypeID, strStatusID, strNote, strEventID, strUserID, strChannelID

					strNoteTypeID = objExe.Setting( "ClaimPoint/CarrierComment/NoteTypeID" )
					strEventID = objExe.Setting( "ClaimPoint/CarrierComment/EventID" )
					strNote = Request( "comments" )
					strStatusID = oRefNode.getAttribute( "StatusID" )
					strClaimAspectID = oRefNode.getAttribute( "ReferenceID" )
					strUserID = GetSession( "UserID" )
					strChannelID = oRefNode.getAttribute( "PrimaryClaimAspectServiceChannelID" )

					objExe.ExecuteSpNp "uspNoteInsDetail", _
                  "ClaimAspectID=" + strClaimAspectID + _
                  "&ClaimAspectServiceChannelID=" + strChannelID + _
                  "&NoteTypeID=" + strNoteTypeID + _
                  "&StatusID=" + strStatusID + _
                  "&Note=" + escape(strNote) + _
                  "&UserID=" + strUserID

					objExe.ExecuteSpNp "uspWorkflowNotifyEvent", _
                  "ClaimAspectID=" + strClaimAspectID + _
                  "&ClaimAspectServiceChannelID=" + strChannelID + _
                  "&EventID=" + strEventID + _
                  "&LynxID=" + iLynxID + _
                  "&UserID=" + strUserID

			Else
        writeError "Invalid Claim - was missing 'Vehicle 1' claim aspect.  Please try again."
			End If

      set oXML = nothing

      Response.write "<script>top.doClose()</script>"
    End Sub

sub writeError(strError)
%>
  <script language="javascript">
    var strError = "<%=strError%>";
    top.displayMessage(strError);
  </script>
<%
  Response.end
end sub

%>

<html xmlns:IE>
  <head>
    <TITLE>Send Comments</TITLE>

    <LINK rel="stylesheet" href="/css/APDControls.css" type="text/css"/>

    <STYLE type="text/css">
        A {color:black; text-decoration:none;}
        .nowrap {text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:100%;cursor:default;}
    </STYLE>

    <SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
    <SCRIPT language="JavaScript">
        function doSend(){
          if (txtLynxID.value == ""){
            ClientWarning("Please enter a value for LYNX Id and try again.");
            return;
          }

          if (txtComments.value == ""){
            ClientWarning("Please enter a value for Comments and try again.");
            return;
          }

          btnSend.CCDisabled = true;
          btnClose.CCDisabled = true;
          frmComments.lynxid.value = txtLynxID.value;
          frmComments.comments.value = txtComments.value;
          frmComments.submit();
        }

        function doClose(){
          window.close();
        }

        function displayMessage(sMsg){
          ClientWarning(sMsg);
          btnSend.CCDisabled = false;
          btnClose.CCDisabled = false;
        }
    </SCRIPT>

  </head>
  <body style="padding:5px;">
    <form name="frmComments" id="frmComments" method="post" action="MESendComments.asp" target="iFrm">
    <table border="0" cellpadding="2" cellspacing="0">
      <colgroup>
        <col width="75" style="font-weight:bold"/>
        <col width="*"/>
      </colgroup>
      <!-- <tr>
        <td colspan="2" style="font:10pt Tahoma;color:#0000FF;font-weight:bold">Send Comments</td>
      </tr> -->
      <tr>
        <td>LYNX ID:</td>
        <td>
          <IE:APDInputNumeric id="txtLynxID" name="txtLynxID" precision="8"
							scale="0" neg="false" int="true" required="true" canDirty="false" CCDisabled="false"
							CCTabIndex="1" onChange="" value="<%= Request.QueryString("initlynxid") %>" />
        </td>
      </tr>
      <tr>
        <td valign="top">Comments:</td>
        <td>
          <IE:APDTextArea id="txtComments" name="txtComments" maxLength="450" width="305" height="75" required="true" canDirty="false" CCDisabled="false" CCTabIndex="2" onChange="" />
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">
          <IE:APDButton id="btnSend" name="btnSend" value="Send" width="75" CCDisabled="false" CCTabIndex="3" onButtonClick="doSend()"/>
          <IE:APDButton id="btnClose" name="btnClose" value="Close" width="75" CCDisabled="false" CCTabIndex="4" onButtonClick="doClose()"/>
        </td>
      </tr>
    </table>
    <input type="hidden" name="lynxid" id="lynxid"/>
    <input type="hidden" name="comments" id="comments"/>
    </form>

    <iframe id="iFrm" name="iFrm" style="height:100;width:100;display:none"/>
  </body>
</html>
