<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!--<% Response.Buffer = True %>-->
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError
    
    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        Dim strLynxID
        Dim strClaimAspectID
        Dim strInsuranceCompanyID
        Dim IsEmailFlag

        strUserID = GetSession("UserID")
        strInsuranceCompanyID = Request("InsuranceCompanyID")

        strLynxID = request("LynxID")
        If Trim(strLynxID) = vbnullstring Then
            strLynxID = GetSession("LynxID")
        End If

        strClaimAspectID = Request("ClaimAspectID")
        
        if strClaimAspectID = "" then
          response.write "Missing ClaimAspectID in the URL. Please contact IT."
          response.end
        end if
        
        if strLynxID = "" then
          response.write "Missing LynxID in the URL. Please contact IT."
          response.end
        end if

        'Create and initialize DataPresenter
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'Read Config info
        IsEmailFlag = objExe.Setting("ClaimPoint/Customizations/InsuranceCompany[@ID ='" & strInsuranceCompanyID & "']/@RepairReferral")
        
        ' vehicle has been touched!
        objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strClaimAspectID, strUserID

        ' Place claim information in session manager.
        'StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey

        Dim strImageRootDir
        strImageRootDir = objExe.Setting("Document/RootDirectory")

        ' 14Jan2016 - TVD - Added Archive root here
        Dim strImageRootArchiveDir 
        strImageRootArchiveDir = objExe.Setting("Document/RootArchiveDirectory")

        objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "ImageRootArchiveDir", strImageRootArchiveDir
        objExe.AddXslParam "VehClaimAspectID", strClaimAspectID
        objExe.AddXslParam "IsCellEmailFlag", Lcase(IsEmailFlag)
                
        'Get XML and transform to HTML.
        Response.Write  objExe.ExecuteSpAsXML( "uspClaimVehicleGetDetailXML", "ClaimVehicleCond.xsl", strClaimAspectID, strInsuranceCompanyID )      

    End Sub
%>
