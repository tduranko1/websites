<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%

'  On Error Resume Next
  Dim objExe, strUserID, strUserDetailXML, strReinspectionXML, strXML
  Dim oXML, oUserNode, oReINode, oReinspection
  Dim strRepNameFirst, strRepNameLast, strRepName, strLynxID, strVehicleNum, strShopName, strShopCity
  Dim strDocumentID, strProgramManagerName, strProgramManagerEmailAddress
  dim strFunction, strProc
  dim blnEmailSent, strErrorText, strEmailText, strEmailSubject
  dim strEmailFrom
  
  strFunction = request("fn")
  strDocumentID = Request("DocumentID")
  strUserID = Request("UserID")
  strComments = Request("Comments")
  strEmailFrom = "no-reply-lynx@lynxservices.com"
  strErrorText = ""
  
  if strFunction = "CancelVehicle" or strFunction = "VoidVehicle" then
    strLynxID = request("LynxID")
    strVehicleNum = request("VehicleNum")
    strRepName = request("RequestedBy")
    strShopName = request("ShopName")
    strShopCity = request("ShopCity")
    strProgramManagerEmailAddress = request("ProgramManagerEmailAddress")
    strEmailText = "Reinspection has been cancelled "
    if strFunction = "CancelVehicle" then
      strEmailSubject = "APD - Reinspection cancelled"
      strEmailText = strEmailText & "due to cancellation of the vehicle."
    else
      strEmailSubject = "APD - Reinspection cancel - Vehicle Voided"
      strEmailText = strEmailText & "due to voiding of the vehicle."
    end if
  else
    Set objExe = InitializeSession()
    if strFunction = "request" then
      strProc = "uspRequestReinspection"
      strXML = objExe.ExecuteSpAsXML(strProc, "", strDocumentID, strComments, strUserID)
      strEmailText = "A reinspection has been requested."
      strEmailSubject = "APD - New Reinspection requested"
    else
      strProc = "uspCancelReinspection"
      strXML = objExe.ExecuteSpAsXML(strProc, "", strDocumentID, strUserID)
      strEmailText = "Reinspection has been cancelled."
      strEmailSubject = "APD - Reinspection cancelled"
    end if
    
    set oXML = Server.CreateObject("MSXML2.DOMDocument")
    oXML.async = false
    oXML.loadXML strXML
    
    set oReinspection = oXML.selectSingleNode("/Root/Reinspection")
    if not oReinspection is nothing then
      strLynxID = oReinspection.getAttribute("LynxID")
      strVehicleNum = oReinspection.getAttribute("ClaimAspectNumber")
      strRepName = oReinspection.getAttribute("UserNameLast") & ", " & oReinspection.getAttribute("UserNameFirst")
      strShopName = oReinspection.getAttribute("ShopName")
      strShopCity = oReinspection.getAttribute("ShopCity")
      strProgramManagerEmailAddress = oReinspection.getAttribute("ProgramManagerEmailAddress")
    end if
  end if
  
  if strProgramManagerEmailAddress <> "" then
    strEmailSubject = strEmailSubject & " - Lynx ID:" & strLynxID & "-" & strVehicleNum
    strEmailText = strEmailText & vbcrlf & "Reinspection Details:" & vbcrlf & _
                   "LYNX ID:      " & strLynxID & "-" & strVehicleNum & vbcrlf
    if strFunction = "request" then
      strEmailText = strEmailText & "Requested By: " & strRepName & vbcrlf
    else
      strEmailText = strEmailText & "Cancelled By: " & strRepName & vbcrlf
    end if
    strEmailText = strEmailText & _
                   "Shop Name:    " & strShopName & vbcrlf & _
                   "Shop City:    " & strShopCity
    'strProgramManagerEmailAddress = "rvishegu@lynxservices.com"
    SendMail strProgramManagerEmailAddress, strEmailFrom, strEmailSubject, strEmailText
  else
    strErrorText = "Missing email address for the Shop Program Manager. Please contact IT."
  end if
  
  
%>

<script language="javascript">
  <%if strFunction = "CancelVehicle" or strFunction = "VoidVehicle" then%>
    <% if strErrorText = "" then%>
      if (typeof(parent.doOK) == "function")
        parent.doOK();
    <%else%>
    parent.ClientWarning("<%=strErrorText%>");
    <%end if%>
  <%else%>
    <% if strErrorText = "" then%>
    if (parent.btnReqReinspection){
      var obj = parent.btnReqReinspection;
      if (obj.value.indexOf("Cancel") != -1){     
        obj.value = "Request Reinspection";
        parent.bReinspectionRequested = false;
        if (typeof(parent.disableVehCurrentAssignment) == "function")
          parent.disableVehCurrentAssignment(false);
        parent.ClientWarning("Reinspection has been cancelled and an email was sent to Program Manager.");
      } else {
        obj.value = "Cancel Reinspection";
        parent.bReinspectionRequested = true;
        if (typeof(parent.disableVehCurrentAssignment) == "function")
          parent.disableVehCurrentAssignment(true);
        parent.ClientWarning("Reinspection has been requested and an email was sent to Program Manager.");
      }
  
      if (typeof(parent.disableControls) == "function")
        parent.disableControls(false);
    }
    <%else%>
    parent.ClientWarning("<%=strErrorText%>");
    <%end if%>
  <%end if%>
</script>

<%

Sub SendMail(sTo, sFrom, sSubject, sBody)

On Error Resume Next

	Dim iMsg, iConf, Flds
  dim sSMTPServer
  
  sSMTPServer = "sftmfnolprdweb1"

	Set iMsg = Server.CreateObject("CDO.Message")

	Set iConf = Server.CreateObject("CDO.Configuration")

	Set Flds = iConf.Fields

	With Flds
	  .Item("http://schemas.microsoft.com/cdo/configuration/sendusing")					    = 2 ' cdoSendUsingPort
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")				    = sSMTPServer
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10 ' quick timeout
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")		  = 0
	  .Update
	End With

	With iMsg
	  Set .Configuration = iConf
	      .To			=	sTo
	      .From			=	sFrom
	      .Subject		=	sSubject
	      .TextBody		=	sBody
	      .Send
	End With

	If err.number <> 0 Then
		Err.Clear
		'Response.Write "E-mail Failure. Close the window and Try again." & "<BR>"
		'Response.Write sSMTPServer
		'Response.end
	End If

	Set Flds = Nothing
	Set iMsg = Nothing
	Set iConf = Nothing

End Sub
%>