<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- ClaimReassignmentDialog.asp
	This file handles the page for Claim Reasignment.
	It is used in a popup dialog window.
-->

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->
<%
	On Error Resume Next

	'Extract the query data.
	Dim strResult, objExe

	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
	CheckError()

	objExe.AddXslParam "LynxId", GetSession("LynxID")
	CheckError()

	'Get Organizational Chart Necessary for reassignment and transform XML
	strResult = objExe.ExecuteSpAsXML("dbo.uspOrgChartGetListXML", "ClaimReassignmentDialog.xsl", GetSession("UserID"))
    CheckError()

	Set objExe = Nothing

    Call KillSessionObject

 	Response.Write strResult

	On Error GoTo 0
%>
