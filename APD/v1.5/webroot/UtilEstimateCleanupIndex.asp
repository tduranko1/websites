<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
    On Error Resume Next

    Dim objExe, strPageName, strWinName
    Call GetHtml
    Set objExe = Nothing
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Set objExe = InitializeSession()

        strPageName = "UtilEstimateCleanup.asp"
        strWinName = "APDUtilEstimate_" & objExe.mToolkit.mEvents.mSettings.Environment

    End Sub
%>

<html>
<head>
	<title>APD Estimate Cleanup Utility Auto Start</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script language="javascript" type="text/javascript">

		function NewWindow()
		{
      var settings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
			var win = window.open( '<%= strPageName %>', '<%= strWinName %>', settings );
		}

	</script>

</head>
<body bgcolor="#FFFFFF" text="#000000" onload=NewWindow()>

</body>
</html>
