<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strHTML
	Dim strLynxID

	On Error Resume Next

	strLynxID = GetSession("LynxID")
	strUserID = GetSession("UserID")

    Call CheckError
    Call KillSessionObject
%>


<HTML>
<HEAD>
  <title>Claim Close</title>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>


<!-- for radio buttons -->
<STYLE type="text/css">
A
{color:black; text-decoration:none;}

SELECT
{
  background : url(../images/input_bkgd.png) no-repeat;
  background : #White;
  font-family : Tahoma, Arial, Helvetica, sans-serif;
  font-size : 11px;
}
</STYLE>

<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
	<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,46);
		  event.returnValue=false;
		  };
    </script>
<SCRIPT language="JavaScript">

var gsLynxID = "<%=strLynxID%>";
var gsUserID = "<%=strUserID%>";

function CloseClaim()
{
    var coObj = RSExecute("/rs/RSCloseClaim.asp", "CloseClaim", gsLynxID, gsUserID );
	if (coObj.status == -1)
	{
        ServerEvent();
	}
	else
	{
        var retArray = coObj.return_value.split("||");
        if (retArray[1] != "0")
        {
            window.returnValue = "Close";
            window.close();
        }
        else
            ServerEvent();
	}

}

function CancelCloseClaim()
{
    window.returnValue = "Cancel";
    window.close();
}

function InitPage()
{
	if (gsLynxID == "")
	{
		alert("Invalid LynxID");
		window.close;
	}
}

</SCRIPT>

</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="background:transparent;border:0px" onload="InitPage();">

<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<DIV align="center" style="position:absolute;background:#CCCCCC;left:0px;top:0px;width:294px"><b>
	Close Claim LynxID - <script>document.write(gsLynxID)</script></b>
</DIV>
<BR>
<DIV align="center">
<P><strong><font color="#880000" size="3">
Are you sure you want to Close this Claim?
</font></strong>
</P>

</DIV>

<BR/><BR/>
<P align="right">
	<input type="button" class="formbutton" onclick="CloseClaim();" value="Close Claim"/>&#xa0;&#xa0;
    <input type="button" class="formbutton" onclick="CancelCloseClaim();" value="Cancel"/>

</P>



</BODY>
</HTML>
