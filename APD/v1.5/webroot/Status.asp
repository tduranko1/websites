<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- Status.asp
    Puts up useful status information like version numbers.
-->

<!-- #include file="errorhandler.asp" -->
<%
    Dim objExe, objRS

    On Error Resume Next

    StatusInfo
    CheckError

    Set objExe = Nothing
    Set objRS = Nothing

    On Error GoTo 0

    'Builds HTML from status and version info from the app.
    Function StatusInfo()
        On Error Goto 0

        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")

        Response.Write "<html><head><title>APD Status</title></head><body>"
        Response.Write "<table cellpadding='3' border='0' cellspacing='1'>"
        Response.Write "<col bgcolor='#BBBBBB'/>"
        Response.Write "<col bgcolor='#DDDDDD'/>"
        Response.Write "<col bgcolor='#EEEEEE'/>"

        ' CONFIGURATION

        Response.Write "<tr bgcolor='#999999'><th align='left'>Configuration</th><th/></tr>"

        Response.Write "<tr><td>Application</td><td>" & objExe.mToolkit.mEvents.mSettings.Application & "</td></tr>"
        Response.Write "<tr><td>Environment</td><td>" & objExe.mToolkit.mEvents.mSettings.Environment & "</td></tr>"
        Response.Write "<tr><td>Instance</td><td>"    & objExe.mToolkit.mEvents.mSettings.Instance    & "</td></tr>"

        ' APD DATA STORE

        DataStore "APD DB", ""

        ' PARTNER DATA STORE

        DataStore "Partner DB", "PartnerSettings/"

        ' WEB SERVER

        Response.Write "<tr bgcolor='#999999'><th align='left'>Web</th><th/></tr>"

        Response.Write "<tr><td>Version</td><td>" & objExe.mToolkit.mEvents.mSettings.GetRawSetting("/Root/Application/@version") & "</td></tr>"
        Response.Write "<tr><td>Server</td><td>" & objExe.mToolkit.mEvents.mSettings.Machine     & "</td></tr>"
        Response.Write "<tr><td>Site</td><td>" & Request.ServerVariables("SERVER_NAME") & "</td></tr>"
        Response.Write "<tr><td>Client IP</td><td>" & Request.ServerVariables("REMOTE_ADDR") & "</td></tr>"

        Response.Write "</table></body></html>"

    End Function

    'Retrieves database version info based upon the passed connect config.
    Function DataStore( sName, sConnect )

        Dim strResult

        Response.Write "<tr bgcolor='#999999'><th align='left'>" & sName & "</th><th/></tr>"

        objExe.ConnectionStringParent = sConnect
        Set objRS = objExe.ExecuteSpAsRS("uspSysVersionGetList")
        If objRS.EOF = True Then
            Response.Write "<tr><td>Error retrieving version through uspSysVersionGetList</td></tr>"
        Else
            Response.Write "<tr><td>Version</td><td>" & objRS.Fields.Item(0) & "</td></tr>"
        End If

        Dim strConnect, intIdx, intEnd
        strConnect = objExe.Setting(sConnect & "ConnectionStringXml")

        intIdx = InStr( strConnect, "data source=" ) + Len( "data source=" )
        intEnd = InStr( Mid( strConnect, intIdx ), "," )
        strResult = Mid( strConnect, intIdx, intEnd - 1 )

        Response.Write "<tr><td>Server</td><td>" & strResult  & "</td></tr>"

        intIdx = InStr( strConnect, "initial catalog=" ) + Len( "initial catalog=" )
        intEnd = InStr( Mid( strConnect, intIdx ), ";" )
        strResult = Mid( strConnect, intIdx, intEnd - 1 )

        Response.Write "<tr><td>Database</td><td>" & strResult  & "</td></tr>"
        
        set objRS = nothing
    End Function
%>
