<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe, objDom
    Call GetHtml
    Set objExe = Nothing
    Set objDom = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strLynxID
        Dim strUserID
        Dim strInsuranceCompanyID
        Dim strHTML
        Dim strReloadTab
        Dim strClaim_ClaimAspectID

        strLynxID = Request("LynxID")
        strUserID = GetSession("UserID")
      	strReloadTab = Request("ReloadTab")

        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If


        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        ' Place claim information in session manager.
        Set objDom = StuffClaimSessionVars( objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey )

        strClaim_ClaimAspectID = GetSession("Claim_ClaimAspectID")
        
        ' Claim has been touched!
        objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strClaim_ClaimAspectID, strUserID

        'moved this line here because the Insurance company id for the claim in context will be correctly set only after the call to uspSessionGetClaimDetailXML
        strInsuranceCompanyID = GetSession("InsuranceCompanyID")

        Dim strImageRootDir
		Dim strImageRootArchiveDir

		' 14Jan2016 - TVD - Added Archive root here
        strImageRootArchiveDir = objExe.Setting("Document/RootArchiveDirectory")
        strImageRootDir = objExe.Setting("Document/RootDirectory")

        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "ImageRootArchiveDir", strImageRootArchiveDir
        objExe.AddXslParam "InsuranceCompanyName", objDom.selectSingleNode("//Claim/@InsuranceCompanyName").Text
        objExe.AddXslParam "ClientClaimNumber", objDom.selectSingleNode("//Claim/@ClientClaimNumber").Text
        objExe.AddXslParam "InsuredNameFirst", objDom.selectSingleNode("//Claim/@InsuredNameFirst").Text
        objExe.AddXslParam "InsuredNameLast", objDom.selectSingleNode("//Claim/@InsuredNameLast").Text
        objExe.AddXslParam "LossDate", objDom.selectSingleNode("//Claim/@LossDate").Text
        objExe.AddXslParam "ReloadTab", strReloadTab

        ' Get claim XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspDocumentGetDetailXML", "ClaimDocument.xsl", strLynxID, "", "", "", strInsuranceCompanyID, strUserID )
    End Sub
%>


