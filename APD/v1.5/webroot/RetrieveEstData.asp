<%@ Language=VBScript %>
<% Option Explicit %>
<!-- #include file="errorhandler.asp" -->

<%
  On Error Resume Next

  Dim sLynxID, oApd, var, errMsgNum
  Dim sSupplementNo

  sLynxID = Request.Form("LynxID")
  sSupplementNo = Request.Form("SupplementNo")
  if Request.Form("EstimatingPackage") = "Pathways" then
    Set oApd = CreateObject("DocumentMgr.CPathwaysEMS")
    CheckError()

    var = oApd.GetEstimateTotals(sLynxID, sSupplementNo)
  
    CheckError()
  else
    Set oApd = CreateObject("DocumentMgr.CMitchellEMS")
    CheckError()

    var = oApd.GetEstimateTotals(sLynxID)
  
    CheckError()
  end if
  


  errMsgNum = Err.Number

    'GrossTotal                 var(0)
    'GrossBetterment            var(1)
    'GrossDeductible            var(2)
    'OtherAdjustments           var(3)
    'NetTotal                   var(4)
    'GrossRelatedPriorDamage    var(5)
    'GrossUnrelatedPriorDamage  var(6)
    'GrossAppearanceAllowance   var(7)

  Set oApd = Nothing

	On Error GoTo 0
%>

<html>
<head>
	<title>APD Estimate Data Retrieval</title>

<script language="JavaScript">

  var sMsg = "<%= errMsgNum %>";
  var sGrossTotal = "<%= var(0) %>";
  if (sMsg == 0 && sGrossTotal != "")
  {
    parent.document.getElementById("txtRepairTotalAmt").value = "<%= var(0) %>";
    parent.document.getElementById("txtBettermentAmt").value = "<%= var(1) %>";
    parent.document.getElementById("txtDeductibleAmt").value = "<%= var(2) %>";
    parent.document.getElementById("txtOtherAdjustmentAmt").value = "<%= var(3) %>";

    var loNetTotalAmt = parent.document.getElementById("txtNetTotalAmt");
    loNetTotalAmt.CCDisabled = false;
    loNetTotalAmt.value = "<%= var(4) %>";
    loNetTotalAmt.CCDisabled = true;
  }
  else if (sGrossTotal == "")
  {
    parent.dsplRetrieveDataMsg("EMS data was not found.");
  }
  else if (sMsg != 0)
  {
    parent.dsplRetrieveDataMsg(sMsg);
  }
  
</script>

</head>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" tabindex="-1">

</body>
</html>
