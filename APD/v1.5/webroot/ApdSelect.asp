<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%

    On Error Resume Next

    Dim objExe
    dim sClaimView
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
      dim iLynxID

        Set objExe = InitializeSession()
        
        sClaimView = Request("ClaimView")
        if sClaimView = "" then
          sClaimView = Request.cookies("ClaimView")
        end if
        
         'hardcode to disable expanded view.
        'sClaimView = "cond"

        sEntity = LCase( Request("Entity" ) )
        'sEntity = "claimnew"
         'if sEntity = "claimnew" then
          sClaimView = "condnew"     'Change condnew to cond for old APD screen
       ' end if
        
        
        objExe.AddXslParam "Environment", objExe.mToolkit.mEvents.mSettings.Environment
        objExe.AddXslParam "LynxID", Request("LynxID" )
        objExe.AddXslParam "Entity", LCase( Request("Entity" ) )
        objExe.AddXslParam "Context", Request("Context")
        objExe.AddXslParam "EntityContext", Request("Context")
        objExe.AddXslParam "ClaimView", sClaimView
        objExe.AddXslParam "VehNum", Request("VehNum")
        
        'Get ApdSelect XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspSessionGetUserDetailXML", "ApdSelect.xsl", LCase(strCsrID))
        iLynxID = CLng(Request("LynxID"))
        if iLynxID > 0 then
          Response.Write objExe.ExecuteSpAsXML( "uspEntityOwnerRepGetListXML", "EntityOwners.xsl", Request("LynxID"))
        end if
    End Sub
    
 
%>