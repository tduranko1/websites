<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
    Dim objExe
    Dim strUserID
    Dim strHTML
    dim sUserList, bIncludeSubordinates, aUsers, sUser
  
    On Error Resume Next
    strUserID = GetSession("UserID")
  
    Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
    CheckError()

    strHTML = objExe.ExecuteSpAsXML("uspAdmUserGetListXML", "rptUserList.xsl")
    CheckError()

    Call KillSessionObject
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html XMLNS:IE>
<head>
    <title>APD Claim System Profile</title>
    <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css">
    <style>
      IE\:APDCheckBox {
        behavior: url(/behaviors/APDCheckBox.htc);
      }
    </style>
    <!-- <SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT> -->
    <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
    </script>
    <script language="javascript">
        var strHoldHTML;
        function initPage() {
          form1.btnShow.disabled = false;
          if (form1.btnPrint)
            form1.btnPrint.disabled = false;
        }
        
        function showReport(){
          if (tblUsers){
            var tblUsersRows = tblUsers.rows;
            var rowLength = tblUsersRows.length;
            var sUsersList = "";
            var sIncludeSubordinates = chkPrintSubordinates.value
            for(var i = 0; i < rowLength; i++) {
              var chk = tblUsers.rows[i].cells[0].firstChild;
              if (chk.value == 1) {
                sSupervisor = tblUsers.rows[i].getAttribute("SupervisorFlag");
                sUserID = tblUsers.rows[i].getAttribute("UserID")
                sUsersList += sUserID + "|" + sSupervisor + ",";
                if (sSupervisor == "1" && sIncludeSubordinates == "1"){
                  sUsersList += getSubordinates(sUserID);
                }
              }
            }
            //remove the last comma
            sUsersList = sUsersList.substring(0, sUsersList.length - 1);
            
            if (sIncludeSubordinates == "1") {
              //remove duplicates users due to include subordinates
              aUsersList = sUsersList.split(",");
              for (var i = 0; i < aUsersList.length; i++){
                for (var j = i+1; j < aUsersList.length; j++) {
                  if (aUsersList[i] == aUsersList[j])
                    aUsersList.splice(j, 1);
                }
              }
              sUsersList = aUsersList.join(",");
            }
            
            //alert(sUsersList);
            form1.txtUserList.value = sUsersList;
            //form1.txtSubordinates.value = chkPrintSubordinates.value;
            
            //alert(chkPrintSubordinates.value);
            
            if (sUsersList.length > 0)
              form1.submit();
          }
        }
        
        function getSubordinates(sUserID){
          var retStr = "";
          for (var i = 0; i < aUserList.length; i++) {
            if (aUserList[i][0] == sUserID) {
              retStr += aUserList[i][1];
              var aList = aUserList[i][1].split(",");
              for (var j = 0; j < aList.length; j++) {
                if (aList[j].split("|")[1] == "1")
                  retStr += getSubordinates(aList[j].split("|")[0]);
              }
              return retStr;
            }
          }
        }

        function printPage(){
          //event.srcElement.style.display = "none";
            var oObj = new Object;
            oObj.strTitle = document.title;
            oObj.strReport = rptData.innerHTML;
            window.showModalDialog('printReport.asp', oObj, 'dialogHeight:0px;dialogWidth:0px;dialogTop:0px;dialogLeft:0px;center:no;status:no;resizable:yes;dialogHide:yes');
          //event.srcElement.style.display = "inline";
        }

        function resetDirty(){
            if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = false;
        }
		
		if (document.attachEvent)
      		document.attachEvent("onclick", top.hideAllMenuScriptlets);
    	else
     		document.onclick = top.hideAllMenuScriptlets;
		
    </script>
</head>

<body class="bodyAPDSub" onload="initPage();" style="background-color:#FFFFFF;overflow:hidden" tabIndex="-1" onbeforedeactivate="resetDirty()">
<span id="spanCriteria">
<form method="post" action="clmRepProfileVerify.asp" id="form1" name="form1">
<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
  <colgroup>
    <col width="350px"/>
    <col width="*"/>
  </colgroup>
    <tr>
        <td colspan="2"><div style="font:bolder 12pt Tahoma;">Claim Representative Profile Verification Report</div><br>
        Select User:
        </td>
    </tr>
    <tr valign="bottom">
        <td>
          <div style="height:100px;width:345px;overflow:auto;background-color:#FFFFFF;padding:5px;border:1px solid #A9A9A9;align:left;">
          <%Response.Write strHTML%>
          </div>
        </td>
        <td>
          <div style="padding-left:10px">
            <IE:APDCheckBox id="chkPrintSubordinates" name="chkPrintSubordinates" caption="Include subordinates" alignment="1" CCTabIndex="2" /><br/><br/>
            <input name="btnShow" id="btnShow" type="button" class="formButton" disabled value="Show Report" onclick="showReport()"/>
<%    if request.form("txtPost") <> "" then %>
            <input name="btnPrint" id="btnPrint" type="button" class="formButton" disabled value="Print Report" onclick="printPage()"/>
<%    end if %>
          </div>
          
        </td>
    </tr>
</table>
<input type="hidden" id="txtUserList" name="txtUserList">
<input type="hidden" id="txtSubordinates" name="txtSubordinates">
<input type="hidden" id="txtPost" name="txtPost" value="post">
</form>
<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
    <tr>
      <td colspan="2">
        <div name="rptData" id="rptData" style="width:100%;height:380px;overflow:auto;">
<%
    if request.form("txtPost") <> "" then
      sUserList = request.form("txtUserList")
      if (sUserList <> "") then
        dim oUser, sUserDetail
        dim xmlDoc, xmlDoc2, iCount, ndeUser
        dim xslDoc
        dim iUsersCount
        set xmlDoc = CreateObject("Msxml2.DOMDocument")
        xmlDoc.async = false

        set xslDoc = CreateObject("Msxml2.DOMDocument")
        xslDoc.async = false
        xslDoc.load server.mappath("/xsl/rptClaimRepProfileVerify.xsl")

        aUsers = split(sUserList, ",")
        iUsersCount = UBound(aUsers) + 1
        iCount = 0
        for each oUser in aUsers
          iCount = iCount + 1
          sUser = split(oUser, "|")(0)
          strHTML = ""
          if (isNumeric(sUser)) then
            sUserDetail = objExe.ExecuteSpAsXML("uspAdmUserGetDetailXML", "", sUser)
            if sUserDetail <> "" then
                xmlDoc.loadXML sUserDetail
                strHTML = xmlDoc.transformNode(xslDoc)
                response.write "<script language='javascript'>top.setSB(" + CStr(CInt((iCount / iUsersCount) * 100)) + ", top.sb);</script>" + vbcrlf
                response.write strHTML
            end if
          end if
        next
        'response.write xslDoc.parseError.reason
      end if
    end if

    Set objExe = Nothing
%>        
        </div>
      </td>
    </tr>
</table>
</span>
  
</body>
</html>
