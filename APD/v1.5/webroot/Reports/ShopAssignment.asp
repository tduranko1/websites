<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../rs/RSGetVehicles.asp" -->

<%
    dim bShowReport
    bShowReport = false

    if (request.form("txtPost") <> "") and (request.form("lynxID") <> "") then
    'if request.form("frmDate") <> "" or request.form("toDate") <> "" then
        Dim strUserID
        dim strQuery
        Dim strHTML

        On Error Resume Next

        strUserID = GetSession("UserID")

        Dim objExe
        Set objExe = CreateObject("DataPresenter.CExecute")
        CheckError()

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
        CheckError()

'        'response.write "'" & request.form("frmDate") & "', '" & request.form("toDate") & "'"
'        'response.end
        ' Get claim XML and transform to HTML.
        strQuery = ""
        'strQuery = request.form("lynxID") + ", 1, NULL, 0, ''"
        if request.form("selVehicleNumber") = "" then
            strQuery = request.form("lynxID")
        else
            strQuery = request.form("lynxID") + ", " + request.form("selVehicleNumber")
        end if
        strHTML = objExe.ExecuteSpStrAsXML( "uspRptShopAssignmentGetListXML", "rptShopAssignment.xsl", strQuery)
        CheckError()

        Set objExe = Nothing

        Call KillSessionObject

        if strHTML <> "" then
            bShowReport = true
        end if

        'response.write "Output"
        'Response.Write strHTML

        On Error GoTo 0
    end if
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Shop Assignment - Report</title>
    <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css">
    <LINK rel="stylesheet" href="/css/apdmain.css" type="text/css">
    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
    <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
    <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
    </script>
    <script language="javascript">
        var strHoldHTML;
        function initPage() {
            top.closeClaim();
            document.all.lynxID.focus();
            attachPasteEvents(document.all.lynxID);
        }
        function validateDates(){
            if (document.all.frmDate.value > '')
            if (CheckDate(document.all.frmDate) == false) return;

            if (document.all.toDate.value > '')
            if (CheckDate(document.all.toDate) == false) return;

            form1.submit();
        }
        function getVehicles(obj){
            var sVehicles = "";
            sVehicles = getClaimVehicles(obj);
            if (sVehicles != undefined){
                if (sVehicles.substr(1, 6) == "select")
                    divVehicleList.innerHTML = sVehicles;
            }
        }
        function printPage(){
            var oObj = new Object;
            oObj.strTitle = document.title;
            oObj.strReport = rptText.innerHTML;
            window.showModalDialog('printReport.asp', oObj, 'dialogHeight:0px;dialogWidth:0px;dialogTop:0px;dialogLeft:0px;center:no;status:no;resizable:yes;dialogHide:yes');
        }
        function resetDirty(){
            if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = false;
        }
    </script>
</head>
<body class="bodyAPDSub" onload="initPage();" onbeforedeactivate="resetDirty()">
<span id="spanCriteria">
<form method="post" action="shopAssignment.asp" id="form1" name="form1">
<table>
    <tr>
        <td colspan="2"><div style="font:bolder 12pt Tahoma;">Shop Assignment Report</div><br></td>
    </tr>
<!--    <tr>
        <td>From Date: </td>
        <td><input id="frmDate" name="frmDate" dbtype="datetime" class="InputField" onkeypress="NumbersOnly(event);"></td>
        <td></td>
    </tr>
    <tr>
        <td>To Date: </td>
        <td><input id="toDate" name="toDate" class="InputField" onkeypress="NumbersOnly(event);"></td>
        <td><Input type="button" id="btnLoad" value="Show" class="formbutton" onclick="validateDates()"></td>
    </tr>-->
    <tr>
        <td>Lynx ID: </td>
        <td><input id="lynxID" name="lynxID" class="InputField" onkeypress="NumbersOnly(event);if (event.keyCode == 13) btnLoad.fireEvent('onclick');" value="<%=request.form("lynxID")%>" onBlur="getVehicles(this);"></td>
        <td></td>
    </tr>
    <tr>
        <td>Vehicle Number: </td>
        <td>
            <div id="divVehicleList" name="divVehicleList">
                <select id="selVehicleNumber" name="selVehicleNumber"  onkeypress="if (event.keyCode == 13) btnLoad.fireEvent('onclick')">
                    <option>1</option>
                </select>
            </div>
        </td>
        <td><Input type="submit" id="btnLoad" value="Show" class="formbutton"></td>
    </tr>
</table>
<input type="hidden" id="txtPost" name="txtPost" value="post">
</form>
</span>
<%
    if bShowReport then
        Response.Write strHTML
    end if
%>
</body>
</html>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
