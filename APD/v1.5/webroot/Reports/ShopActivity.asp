<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
    dim bShowReport
    bShowReport = false

    if request.form("txtPost") <> "" then
    'if request.form("frmDate") <> "" or request.form("toDate") <> "" then
        Dim strUserID
        dim strQuery
        Dim strHTML

        On Error Resume Next

        strUserID = GetSession("UserID")

        Dim objExe
        Set objExe = CreateObject("DataPresenter.CExecute")
        CheckError()

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
        CheckError()

        'response.write "'" & request.form("frmDate") & "', '" & request.form("toDate") & "'"
        'response.end
        ' Get claim XML and transform to HTML.
        strQuery = ""
        strQuery = "'" & request.form("frmDate") & "', '" & request.form("toDate") & "'"
        'response.write strQuery
        'response.end
        strHTML = objExe.ExecuteSpStrAsXML( "uspRptShopActivityGetXML", "ShopActivity.xsl", strQuery)
        CheckError()

        Set objExe = Nothing

        Call KillSessionObject
        
        if strHTML <> "" then
            bShowReport = true
        end if

        'Response.Write strHTML

        On Error GoTo 0
    end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Program Shop - Report</title>
    <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css">
    <LINK rel="stylesheet" href="/css/apdmain.css" type="text/css">
    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
    <SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
	<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
	<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
	</script>
    <script language="javascript">
        var strHoldHTML;
        function initPage() {
            document.all.frmDate.focus();
            document.all.frmDate.futureDate = false;
            document.all.toDate.futureDate = false;
        }
        function validateDates(){
            if (document.all.frmDate.value > '')
              if (CheckDate(document.all.frmDate) == false) return;

            if (document.all.toDate.value > '')
              if (CheckDate(document.all.toDate) == false) return;

            form1.submit();
        }
        function printPage(){
            var oObj = new Object;
            oObj.strTitle = document.title;
            oObj.strReport = rptText.innerHTML;
            window.showModalDialog('printReport.asp', oObj, 'dialogHeight:0px;dialogWidth:0px;dialogTop:0px;dialogLeft:0px;center:no;status:no;resizable:yes;dialogHide:yes');
        }
        function resetDirty(){
            if (typeof(parent.gbDirtyFlag) == "boolean") parent.gbDirtyFlag = false;
        }
    </script>
</head>

<body class="bodyAPDSub" onload="initPage();" onbeforedeactivate="resetDirty()">
<span id="spanCriteria">
<form method="post" action="/Reports/ShopActivity.asp" id="form1" name="form1">
<table>
    <tr>
        <td colspan="2"><div style="font:bolder 12pt Tahoma;">Shop Activity Report</div><br>
        Select the dates for the report:
        </td>
    </tr>
    <tr>
        <td>From Date: </td>
        <td>
          <input id="frmDate" name="frmDate" dbtype="datetime" class="InputField" maxLength="10" value="<%=request.form("frmDate")%>" onkeypress="if (event.keyCode == 13) btnLoad.fireEvent('onclick')">
          <img src="/images/calendar.gif" onclick="ShowCalendar(this, frmDate)" align="absmiddle"/>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>To Date: </td>
        <td>
          <input id="toDate" name="toDate" class="InputField" maxLength="10" value="<%=request.form("toDate")%>" onkeypress="if (event.keyCode == 13) btnLoad.fireEvent('onclick')">
          <img src="/images/calendar.gif" onclick="ShowCalendar(this, toDate)" align="absmiddle"/>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td><Input type="button" id="btnLoad" value="Show" class="formbutton" onclick="validateDates()"></td>
    </tr>
</table>
<input type="hidden" id="txtPost" name="txtPost" value="post">
</form>
</span>
<%
    if bShowReport then
        Response.Write strHTML
    end if
%>

</body>
</html>
