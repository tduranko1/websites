
<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/CRDSecurity.vbs" -->
<%
	Dim strUserID
	Dim strLynxID
	Dim strVehNum
	Dim strHTML

	On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = GetSession("LynxID")

	'strLynxID = 2600
	'strVehNum = 1

	'Create and initialize DataPresenter
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	CheckError()
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClientReportsGetListXML", "ProgramMgrReports.xsl", "P")
	CheckError()

	Set objExe = nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>
