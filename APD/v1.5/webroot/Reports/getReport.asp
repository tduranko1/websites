<%
  Dim sQueryString
  sQueryString = "RFN=" & Request("reportFileName") & "&SP=" & Request("storedProc") & _
                 "&P=" & replace(Request("staticParams"), "&", "|") & "&RP=" & replace(Request("reportParams"), "&", "|") & _
                 "&FN=" & Request("friendlyName") & "&RF=" & Request("reportFormat")
%>
  <html xmlns:IE>
  <head>
    <title><%=Request("friendlyName")%></title>
        <style>
            .toolbarButton {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border:0px;
            }
            .toolbarButtonOver {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border-top:1px solid #FFFFFF;
              border-left:1px solid #FFFFFF;
              border-bottom:1px solid #696969;
              border-right:1px solid #696969;
            }
        
            .toolbarButtonOut {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border:0px;
            }
        </style>
    <SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
    <script language="javascript">
      var zoomVal = 60;
      var sFriendlyName = "<%=Request("friendlyName")%>";
      var pdf1 = null;
      var sFormActionView = "/reports/getReportFile.asp?mode=view";
      var sFormActionSave = "/getReportFile.asp?mode=save";
      var bReportPDFLoading = false;
      var sFormat = "<%=Request("reportFormat")%>";
      var sQueryString = "<%=sQueryString%>";
      var sPDFFilter = "PDF Files (*.pdf),*.pdf";
      var sXLSFilter = "MS Excel Files (*.xls),*.xls";
      
      function pageInit(){
        try {
          //bReportPDFLoading = false;
          if (window == window.top)
            btnClose.style.display = "inline";

          //frmGetReport.action = sFormActionView;
          //frmGetReport.submit();
          //bReportPDFLoading = true;
        } catch (e) {
          ClientError(e.description);
        }
      }
      
      function SaveLocal(){
        try {
          var sBaseURL = document.URL;
          sBaseURL = sBaseURL.substr(0, sBaseURL.lastIndexOf("/"));
          
          btnSaveLocal.disabled = true;
          var sFilter;
          switch (sFormat) {
            case "PDF":
              sFilter = sPDFFilter;
              break;
            case "XLS":
              sFilter = sXLSFilter;
              break;
            default:
              sFilter = "All Files (*.*), *.*";
          }
          var sFileDest = showSaveAs(sFriendlyName, sFilter);
          if (sFileDest != "") {
            if (sFileDest.substr(sFileDest.length - 3, 3).toUpperCase() != sFormat)
              sFileDest += "." + sFormat.toLowerCase();
            CopyFileFromWeb(sBaseURL + sFormActionSave + "&" + sQueryString, sFileDest, true);
          }
        } catch (e) {}
        finally {
          btnSaveLocal.disabled = false;
        }
      }

      function printDoc() {
        try {
          if (pdf1)
            pdf1.Print();
        } catch (e) {
          ClientError("An error occured while printing the pdf file.");
        }
      }

      function DefPrintDoc() {
        try {
          if (pdf1)
            pdf1.printAll();
        } catch (e) {
          ClientError("An error occured while printing the pdf file.");
        }
      }

      function zoomIn(){
        if (pdf1)
          if (zoomVal < 600) {
              zoomVal += 50;
              pdf1.setZoom(zoomVal);
              pdf1.focus();
          }
      }

      function zoomOut(){
        if (pdf1)
          if (zoomVal > 20) { 
              zoomVal -= 50;
              pdf1.setZoom(zoomVal);
              pdf1.focus();
          }
      }
      function tb_onmouseover(obj){
          obj.className = "toolbarButtonOver";
      }
      
      function tb_onmouseout(obj){
          obj.className = "toolbarButtonOut";
      }
      function goToPage(val){
        if (pdf1) {
          switch(val){
              case 1:
                  pdf1.gotoFirstPage();
                  break;
              case 2:
                  pdf1.gotoPreviousPage();
                  break;
              case 3:
                  pdf1.gotoNextPage();
                  break;
              case 4:
                  pdf1.gotoLastPage();
                  break;
          }
          pdf1.focus();
        }
      }
      
      function reportReady(){
        try {
          if (iFrmReportPDF.frameElement.readyState == "complete" && bReportPDFLoading == true) {
            pdf1 = iFrmReportPDF.document.body.firstChild;
            pdf1.setShowToolbar(false);
          }
        } catch (e) {
        } finally {
          if (parent.stat1 && iFrmReportPDF.frameElement.readyState == "complete" && frmGetReport.action == sFormActionView) {
            try {
            parent.stat1.Hide();
            } catch (e) {}
          }
        }
      }
      
      function initDoc(){
        if (sFormat == "PDF"){
          try {
            doc.setShowToolbar(false);
            pdf1 = doc;
          } catch (e) {}
        }
      }
      
    </script>
  </head>
  <body style="border:1px solid #C0C0C0;margin:0px;padding:0px;overflow:hidden" onload="pageInit()" onc1ontextmenu="return false" onreadystatechange="window.status=document.readyState">
    <table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
      <tr style="height:21px;">
        <td style="background-color:buttonface">
          <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <colgroup>
              <col width="*"/>
              <col width="21px" style="align:right"/>
            </colgroup>
            <tr>
              <td>
                <button hidefocus="true" id="btnSaveLocal" tabIndex="-1" tabStop="false" onclick="SaveLocal()" title="Save a local copy" class="toolbarButton" onmouseover="tb_onmouseover(this);makeHighlight(this.firstChild,0,100)" onmouseout="tb_onmouseout(this);makeHighlight(this.firstChild,1,60)"><img src="/images/SaveCopy.gif" width="17" height="14" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"/></button>
<%if Request("reportFormat") = "PDF" then%>
                <button hidefocus="true" id="btnDefPrint" tabIndex="-1" tabStop="false" onclick="DefPrintDoc()" title="Print Document to Default Printer" class="toolbarButton" onmouseover="tb_onmouseover(this);makeHighlight(this.firstChild,0,100)" onmouseout="tb_onmouseout(this);makeHighlight(this.firstChild,1,60)"><img src="/images/DefaultPrinter.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <button hidefocus="true" id="btnPrint" tabIndex="-1" tabStop="false" onclick="printDoc()" title="Print Document" class="toolbarButton" onmouseover="tb_onmouseover(this);makeHighlight(this.firstChild,0,100)" onmouseout="tb_onmouseout(this);makeHighlight(this.firstChild,1,60)"><img src="/images/print.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <img src="/images/spacer.gif" width="4px"/>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomIn()" title="Zoom In" class="toolbarButton" onmouseover="tb_onmouseover(this);makeHighlight(this.firstChild,0,100)" onmouseout="tb_onmouseout(this);makeHighlight(this.firstChild,1,60)"><img src="/images/zoomin.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomOut()" title="Zoom Out" class="toolbarButton" onmouseover="tb_onmouseover(this);makeHighlight(this.firstChild,0,100)" onmouseout="tb_onmouseout(this);makeHighlight(this.firstChild,1,60)"><img src="/images/zoomout.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <img src="/images/spacer.gif" width="4px"/>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(1)" title="First Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;&lt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(2)" title="Previous Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(3)" title="Next Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(4)" title="Last Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;&gt;</button>
<%end if%>
              </td>
              <td>
                <button id="btnClose" hidefocus="true" tabIndex="-1" tabStop="false" onclick="window.close()" title="Close Window" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)" style="display:none"><img src="/images/close.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <iframe id="iFrmReportPDF" name="iFrmReportPDF" style="width:100%;height:100%;border:0px;<%if Request("reportFormat") = "PDF" then response.write "display:none"%>"
            <%if Request("reportFormat") <> "PDF" then%>
              src="getReportFile.asp?<%=sQueryString%>"
            <%end if%>
          >
          </iframe>
          <%if Request("reportFormat") = "PDF" then%>
          <object id="doc" name="doc" width="100%" height="100%">
              <param name="src" value="getReportFile.asp?<%=sQueryString%>">
              <!-- if the above object failed, then try to embed the document.-->
              <embed id="doc" name="doc" src="getReportFile.asp?<%=sQueryString%>" width="100%" height="100%"></embed>
              <!-- if there was no viewer to support both the embed as well as the object tags, then display message
                   so that user can install the adobe viewer --> 
              <center>
                  You do not have an appropriate viewer to view "<%=Request("reportFormat")%>" document.<br>
                  Please contact to Help desk.</a>
              </center>
          </object>
          <%end if%>
        </td>
      </tr>
    </table>
    <script>
      if (sFormat == "PDF"){
        window.setTimeout("initDoc()", 100);
        window.setTimeout("initDoc()", 2000);
      }
    </script>
  </body>
  </html>
