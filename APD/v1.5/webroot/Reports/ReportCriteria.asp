<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/CRDSecurity.vbs" -->
<%
	Dim strUserID
	Dim strLynxID
	Dim strVehNum
	Dim strHTML
    Dim strReportID
    Dim strInsuranceCompanyID
    Dim strReportCategory
 
	On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = GetSession("LynxID")
    strReportID = request("reportID")
    strInsuranceCompanyID = request("InsuranceCompanyID")
	strReportCategory = request("ReportCategory")

	'strLynxID = 2600
	'strVehNum = 1

	'Create and initialize DataPresenter
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()
	
    objExe.AddXslParam "ReportCategory" , strReportCategory
	CheckError()
    'response.write "ReportCriteria.asp"
    'response.write strReportID & vbcrlf
    'response.write strInsuranceCompanyID
    'response.end
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspReportCriteriaGetListXML", "ReportCriteria.xsl", strReportID, "", strInsuranceCompanyID)
	CheckError()
  
	Set objExe = nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>
