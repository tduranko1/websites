<%@ Language=VBScript %>
<% Option Explicit %>
<!-- #include file="../errorhandler.asp" -->
<%
    Dim oRpt, oStream
    Dim sFormat

    On Error Resume Next

    sFormat = request.QueryString("RF")
'    response.write sFormat
'    response.end
    
    Call GetReport
    Call CheckError


    On Error Goto 0

    Function GetReport()


        Set oRpt = CreateObject("ReportUtils.clsReport")
  
        'Set oStream = oRpt.getReport( Server.mappath( Request.Form("reportFileName") ), Request.Form("storedProc"), request.form("staticParams"), request.form("reportParams"), request.form("reportFormat") )
        Set oStream = oRpt.getReport( Server.mappath( Request.QueryString("RFN") ), Request.QueryString("SP"), replace(request.QueryString("P"), "|", "&"), replace(request.QueryString("RP"), "|", "&"), request.QueryString("RF"))
  
        If oStream Is Nothing Then
            Err.Raise ServerSideEvent(), "getReportPDF.asp", _
                "Stream returned from ReportUtils.clsReport.getReport() Is Nothing"
        Else
            If ( Request.QueryString("mode")  = "save" ) Then
                'Response.AddHeader "Content-Disposition", "attachment;filename=" & request.QueryString("FN")
            Else
                SELECT CASE sFormat
                  Case "DOC"
                    Response.ContentType = "application/msword"
                  Case "XLS"
                   Response.ContentType = "application/x-msexcel"
                  Case "RTF"
                    Response.ContentType = "application/msword"
                  Case ELSE
                    Response.ContentType = "application/pdf"
                END SELECT
                'Response.AddHeader "Content-Disposition", "inline;filename=" & request.form("friendlyName")
                Response.AddHeader "Content-Disposition", "inline;filename=" & request.QueryString("FN")
            End If

            Response.BinaryWrite oStream.Read()
        End If
  
        Set oRpt = Nothing
        Set oStream = Nothing
    End Function
%>
