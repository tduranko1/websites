<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strLynxID
        Dim strUserID
        Dim strClaimAspectID
        Dim strInsuranceCompanyID
        Dim strHTML

        strUserID = GetSession("UserID")
        strLynxID = GetSession("LynxID")

        strClaimAspectID = Request("ClaimAspectID")
        If strClaimAspectID = "" Then
            strClaimAspectID = GetSession("ClaimAspectID")
        Else
            UpdateSession "ClaimAspectID", strClaimAspectID
        End If

        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'this is done at the property list level. so no need to do again here.
        ' Claim has been touched!
        'objExe.ExecuteSp "uspWorkflowSetClaimAsRead", strLynxID, strUserID

        ' Place claim information in session manager.
        StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey

        Dim strImageRootDir
        strImageRootDir = objExe.Setting("Document/RootDirectory")

        objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "LynxID", strLynxID
        objExe.AddXslParam "ClaimAspectID", strClaimAspectID

        ' Get claim XML and transform to HTML.
        strHTML = objExe.ExecuteSpAsXML( "uspClaimPropertyGetDetailXML", "ClaimProperty.xsl", strClaimAspectID, strInsuranceCompanyID )

        Response.Write strHTML

    End Sub
%>
