<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strLynxID
	Dim strUserID
    Dim strMode
	Dim strInsuranceCompanyID
	Dim strHTML
    Dim strClaimAspectID

    On Error Resume Next

	strLynxID = Request("LynxID")
	If strLynxID = "" Then
		strLynxID = GetSession("LynxID")
	Else
		UpdateSession "LynxID", strLynxID
	End If

	strUserID = GetSession("UserID")
	strInsuranceCompanyID = GetSession("InsuranceCompanyID")

	CheckError()
  
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	On Error GoTo 0
%>


<html>
  <head>

    <title>Claim Cancel</title>
    <link rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
    <link rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

    <style type="text/css">
        A {color:black; text-decoration:none;}
    </style>

    <xsl:variable name="UserId"/>

    <script language="JavaScript" src="js/formvalid.js"></script>
	<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
	<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,38);
		  event.returnValue=false;
		  };	
	</script>
    <script language="JavaScript">
		var sLynxID = "<%=strLynxID%>";
		var sUserID = "<%=strUserID%>";
		var sInsuranceID = "<%=strInsuranceCompanyID%>";
		
		function pageInit(){
			//alert("LynxID: " + sLynxID + "\nUserID: " + sUserID + "\nInsuranceID: " + sInsuranceID);
		}
		
	    function doSave(){
  			var sComments = txtComments.innerText;
  			if (sComments == "") {
  				ClientWarning("APD System requires a non-blank comment inorder to cancel the claim. Please enter the comments and try canceling again.");
  				return;
  			} else {
  				if (sComments.substr(0, 16) != "CLAIM CANCELLED:")
  					sComments = "CLAIM CANCELLED: " + sComments;
  			}
        var sAction = "Update";
        var sRequest = "LynxID=" + sLynxID + "&" +
                       "InsuranceCompanyID=" + sInsuranceID + "&" +
                       "UserID=" + sUserID + "&" +
		                   "Comment=" + escape(sComments);
        var retArray = myDoCrudAction(sRequest);
	      window.returnValue = "OK";
  		  window.close();
	    }
	
	    function myDoCrudAction( sRequest )
	    {
	    	var retArray = new Array;
	    
	    	try
	    	{
	    
	    		var sProc = "uspWorkflowCancelClaim";
	            //alert(sProc + "\n" + sRequest);
	            if (sProc != "") {
	        		var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
	    
	        		retArray = ValidateRS( coObj );
	                
	    		    return retArray;
	            }
	    	}
	    	catch(e)
	    	{
	    		retArray[0] = e.message;
	    		retArray[1] = 0;
	    		return retArray;
	    	}
	    }    
    </script>

  </head>
  <body bgcolor="#FFFAEB" text="#000000" onload="pageInit()" style="margin:5px">
	<table border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td colspan="2" style="font-size:18px;font-weight:bold">Claim Cancel</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>Lynx ID:</td>
			<td><%=strLynxID%></td>
		</tr>
		<tr valign="top">
			<td>Comments:</td>
			<td>
				<textarea name="txtComments" id="txtComments" class="TextAreaField" cols="60" rows="6"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="button" class="formButton" value=" OK " onclick="doSave()"/>
				<input type="button" class="formButton" value="Cancel" onclick="window.returnValue = 'Cancel';window.close()"/>
			</td>
		</tr>
	</table>
  </body>
</html>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
