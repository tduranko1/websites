<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->

<%
  On Error Resume Next

  Dim strInsuranceCoID, strShopTypeCode
  Dim objExe, strHTML

  'Get values from the query sring
  strShopTypeCode = Request("ShopTypeCode")
  strInsuranceCoID = Request("InsuranceCompanyID")
  CheckError()

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
  CheckError()

  'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
  objExe.AddXslParam "UserId", Request("UserID")
  objExe.AddXslParam "LynxId", Request("LynxID")
  objExe.AddXslParam "VehicleNumber", Request("VehicleNumber")
  objExe.AddXslParam "ClaimAspectID", Request("ClaimAspectID")
  objExe.AddXslParam "ClaimAspectServiceChannelID", Request("ClaimAspectServiceChannelID")
  objExe.AddXslParam "ShopRemarks", Request("ShopRemarks")
  objExe.AddXslParam "AssignmentTo", Request("AssignmentTo")
  objExe.AddXslParam "MEChildWin", Request("MEChildWin")
  CheckError()

  'Get XML and transform to HTML.
  If Request("SrhFlag") = "1" Then

    strHTML = objExe.ExecuteSpAsXML( "uspShopSearchbyNameXML", "ShopList.xsl", _
        Request("ShopCity"), Request("ShopName"), Request("ShopState"), _
        strInsuranceCoID, strShopTypeCode, 0, Request("UserID"))

  Else

    strHTML = objExe.ExecuteSpAsXML( "uspShopSearchbyDistanceXML", "ShopList.xsl", _
        Request("ZipCode") , strInsuranceCoID, strShopTypeCode, 0, Request("UserID"))

  End If
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  On Error GoTo 0
%>
