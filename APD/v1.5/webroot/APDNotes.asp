<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
    On Error Resume Next
    Dim objExe

    Call GetNotesList
    Call KillSessionObject
    Call CheckError

    Set objExe = Nothing

    Private Sub GetNotesList()

        Dim strLynxID
        Dim strUserID
        Dim strWindowState
        Dim strHTML
        Dim strShowAllNotes

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        If strLynxID = "" Then strLynxID = 0

        strUserID = Request("UserID")
        If strUserID = "" Then
            strUserID = GetSession("UserID")
        End If

        strWindowState = Request("winState")
        strShowAllNotes = Request("ShowAllNotes")

		'------------------------------------'
		' Local Params
		'------------------------------------'
        dim sParams
        sParams = "winState:" & strWindowState
        sParams = sParams + "|ShowAllNotes:" & strShowAllNotes

		'------------------------------------'
		' OLD COM Method
		'------------------------------------'
        'Set objExe = CreateObject("DataPresenter.CExecute")
        'objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'objExe.AddXslParam "LynxID", strLynxID
        'objExe.AddXslParam "UserID", strUserID
        'objExe.AddXslParam "windowState", strWindowState
        'objExe.AddXslParam "ShowAllNotes", Request("ShowAllNotes")

        'strHTML = objExe.ExecuteSpAsXML( "uspNoteGetDetailXML", "APDNotes.xsl", strLynxID )

        'Response.Write strHTML

		'------------------------------------'
		' Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
		'response.write(sASPXSessionKey & " - " & sASPXWindowID)
		'response.end
		
		'------------------------------------'
		' Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("APDNotes.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & sASPXWindowID & "&Params=" & sParams)
		
        'Extract the reference data from the result.
        'Stuff it into the application object.
        'Replace all this crap later with reference data object (What was that?  Anyone say HACK?).

        Const sStart = "<xml id=""Reference"">"
        Const sEnd = "</xml>"

        Dim iStart, iEnd

        iStart = InStr( 1, strHTML, sStart, 1 )

        If iStart > 1 Then
            iStart = iStart + Len(sStart)
            iEnd = InStr( iStart, strHTML, sEnd, 1 )
            If iEnd > iStart Then
                Application.Contents( "NoteReference" ) = Mid( strHTML, iStart, iEnd - iStart )
            End If
        End If

    End Sub
%>

