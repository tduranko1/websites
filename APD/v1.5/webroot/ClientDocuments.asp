<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
    'This object var declared outside of the function calls
    'so that we can assure object cleanup regardless of error.
    Dim oExe 'As Object
    Dim sLynxID, sUserID, sInsCoID, sAttach, sTemplate
    Dim strImageRootDir, strCCCXSLPath, strEmailSubject, strEmailBody, strDocumentName
    Dim strDocumentNameFormat, strMergeFileFormat
    Dim strDefaultEmailSubject, strDefaultEmailBody, strDefaultDocumentNameFormat, strDefaultMergeFileFormat
    Dim blnNotifySuccess, blnNotifyFailure
    Dim strNotifySuccessSubj, strNotifySuccessBody
    Dim strNotifyFailureSubj, strNotifyFailureBody
    Dim strElectronicBundle
    Dim strAttachToClaim
    Dim oXML
    Dim strServerName

    strServerName = Request.ServerVariables("SERVER_NAME")

    on error Resume Next

    Call ProcessAction
    Set oExe = Nothing
'    Call KillSessionObject

    If Err.Number <> 0 Then
      response.write "Error: " & err.description
'        CloseWindow
    End If

    Call CheckError

    on error GoTo 0

    'Call other functions based upon passed Action.
    Function ProcessAction()
        Dim sAction

        sUserID  = Request( "UserId" )
        sLynxID  = Request( "LynxId" )
        sInsCoID = Request( "InsCoId" )
        sAttach  = Request( "Attach" )
        sAction  = Request( "theAction" )
        sTemplate = Request( "Template" )
        strImageRootDir = Request("ImageRootPath")

        If sAction = "Generate" Then
            GenerateDocument
        ElseIf sAction = "Cancel" Then
            CloseWindow
        Else
            BuildDocumentList
        End If
    End Function

    'Retrieves an XML list of available documents and doc templates.
    'Builds the UI HTML from this XML via XSL style sheet.
    Function BuildDocumentList()
        Dim strJobTransformationFile
        on error resume next

        Set oExe = CreateObject("DataPresenter.CExecute")
        oExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, sUserID
        strImageRootDir = oExe.Setting("Document/RootDirectory")
        blnNotifySuccess = (oExe.Setting("ClientDocument/Default/Notify/Success/@enabled") = "true")
        blnNotifyFailure = (oExe.Setting("ClientDocument/Default/Notify/Failure/@enabled") = "true")

        If blnNotifySuccess Then
          strNotifySuccessSubj = oExe.Setting("ClientDocument/Default/Notify/Success/Subject")
          strNotifySuccessBody = oExe.Setting("ClientDocument/Default/Notify/Success/Body")
        End If

        If blnNotifyFailure Then
          strNotifyFailureSubj = oExe.Setting("ClientDocument/Default/Notify/Failure/Subject")
          strNotifyFailureBody = oExe.Setting("ClientDocument/Default/Notify/Failure/Body")
        End If

        sInsCoID = GetSession("InsuranceCompanyID")

        If strCCCXSLPath <> "" Then
          strCCCXSLPath = server.mappath(strCCCXSLPath)
        End If
        call CheckError
        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        oExe.AddXslParam "InsCoId", GetSession("InsuranceCompanyID")
        oExe.AddXslParam "SrcAppCd", GetSession("SourceApplicationCode")
        oExe.AddXslParam "LynxId", sLynxID
        oExe.AddXslParam "UserId", sUserID
        oExe.AddXslParam "Attach", sAttach
        oExe.AddXslParam "ImageRootDir", strImageRootDir
        oExe.AddXslParam "CCCXSLPath", strCCCXSLPath
        oExe.AddXslParam "VehNum", Request("VehNum")
        oExe.AddXslParam "VehClaimAspectID", Request("VehClaimAspectID")


        'get the default email subject, document name format from the config

            if oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/Email/Subject") <> "" then
              strEmailSubject = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/Email/Subject")
            end if
            err.clear
            strEmailBody = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/Email/Body")
            err.clear
            strDocumentNameFormat = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/FileNameFormat")
            err.clear
            strMergeFileFormat = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/MergeFileNameFormat")
            err.clear

            strJobTransformationFile = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/JobTransformation[@enabled='true']")
            err.clear


        If strEmailSubject <> "" Then
          oExe.AddXslParam "EmailSubject", strEmailSubject
          oExe.AddXslParam "EmailBody", strEmailBody
        Else
          strDefaultEmailSubject = oExe.Setting("ClientDocument/Default/Email/Subject")
          strDefaultEmailBody = oExe.Setting("ClientDocument/Default/Email/Body")
          oExe.AddXslParam "EmailSubject", strDefaultEmailSubject
          oExe.AddXslParam "EmailBody", strDefaultEmailBody
        End If
        If strDocumentNameFormat <> "" Then
          oExe.AddXslParam "DocumentNameFormat", strDocumentNameFormat
        Else
          strDefaultDocumentNameFormat = oExe.Setting("ClientDocument/Default/DocumentNaming/@format")
          oExe.AddXslParam "DocumentNameFormat", strDefaultDocumentNameFormat
        End If
        If strMergeFileFormat <> "" Then
          oExe.AddXslParam "MergeFileFormat", strMergeFileFormat
        Else
          strDefaultMergeFileFormat = oExe.Setting("ClientDocument/Default/DocumentNaming/@mergeformat")
          oExe.AddXslParam "MergeFileFormat", strDefaultMergeFileFormat
        End If

        
        strElectronicBundle = oExe.Setting("ClaimPoint/Customizations/InsuranceCompany/@ElectronicBundle")
        oExe.AddXslParam "IsElectronicBundle", strElectronicBundle
        

        oExe.AddXslParam "NotifySuccessSubject", strNotifySuccessSubj
        oExe.AddXslParam "NotifySuccessBody", strNotifySuccessBody
        oExe.AddXslParam "NotifyFailureSubject", strNotifyFailureSubj
        oExe.AddXslParam "NotifyFailureBody", strNotifyFailureBody
        oExe.AddXslParam "jobTransformationFile", replace(strJobTransformationFile, "\", "\\")

        'Grabs the XML from the database and transforms it.

        Response.Write oExe.ExecuteSpAsXML( _
            "uspDocumentGetDetailXML", _
            "ClientDocuments.xsl", _
            sLynxID, "", "", "A", sInsCoID, sUserID)


        Set oExe = Nothing


    End Function


function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/PartnerDataMgr.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/PartnerDataMgr.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IPartnerDataMgr, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IPartnerDataMgr, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/PartnerDataMgr.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/PartnerDataMgr.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IPartnerDataMgr, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IPartnerDataMgr, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
         strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/PartnerDataMgr.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/PartnerDataMgr.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IPartnerDataMgr, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IPartnerDataMgr, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function


    'Create and submit the document processing job
    Function GenerateDocument()
      Dim objFSO, strDocTemplate

      set objFSO = Server.CreateObject("Scripting.FileSystemObject")
      set oXML = Server.CreateObject("MSXML2.DOMDocument")
      oXML.async = false
      oXML.loadXML request("txtDocumentsXML")

      response.write "0.1....<br/>"
      response.write request("txtDocumentsXML")
      response.write "<br/>"
  
      If oXML.parseError.errorCode <> 0 Then
        handleError "The data sent for processing is not valid. Please contact Helpdesk."
      end if

      Dim strSendVia, strSendValue, strSkipSend, strJobName, strCCAVXSLPath
      Dim strUserEmailAddress, strEmailSubject, strEmailBody, strNOLIncoming
      Dim strPackageType, strPackageTypeValue, strMergedFileName
      Dim strSuccessEmailTo, strSuccessEmailSubject, strSuccessEmailBody
      Dim strFailureEmailTo, strFailureEmailSubject, strFailureEmailBody
      Dim strMessage, strCCCXSLPath, strImageRootDir
      Dim blnAttachToClaim, strFailureEmail, strEmailBCC
      Dim strMessageDocID, strClaimAspectID, strNoteInsSP, strMessageNoteTypeID
      Dim strClaimMessage, oMessageNode, strPostbackWebPage, strPostbackURL, strClaimMessageEscaped
      Dim strVehicleStatusID, strUserID
      Dim oDocument, oDocuments
      Dim strFaxCoversheet
      Dim strMessageFormat
      Dim strDocumentUploadXSL, strDocumentUploadPostBackURL, strDocumentUploadCoversheetXSL
      Dim blnCCAVMessageOnly, strJobTransformationFile
      Dim strFileCopyLocation

      on error resume next

      strJobTransformationFile = getXMLValue("/ClientDocument/@jobTransformationFile")
      response.write "0.....<br/>"
       response.write strJobTransformationFile
             response.write "<br/>"
       

      if strJobTransformationFile <> "" then
         strJobTransformationFile = Server.MapPath(strJobTransformationFile)
         if objFSO.fileExists(strJobTransformationFile) then
            Dim oXSLJob, strNewJobXML
            set oXSLJob = Server.CreateObject("MSXML2.DOMDocument")
            oXSLJob.async = false
            oXSLJob.load strJobTransformationFile           
            If oXSLJob.parseError.errorCode <> 0 Then
               handleError "The client specific job transformation for Document bundling is malformed. Please contact Helpdesk."
            end if
            'response.write "transforming..."
            strNewJobXML = oXML.transformNode(oXSLJob)
            set oXSLJob = nothing

            oXML.loadXML strNewJobXML

            If oXML.parseError.errorCode <> 0 Then
              handleError "The client specific job transformation result is malformed. Please contact Helpdesk."
            end if

         else
            'job transformation file not found.
            'response.write "not transforming..."
            handleError "The client specific job transformation for Document bundling was not found. Please contact Helpdesk."
         end if
      end if
      response.write "01.....<br/>"
      response.write strNewJobXML
      response.write "<br/>"
      'response.end

      strPackageType = getXMLValue("/ClientDocument/Send/PackageType")
      if strPackageType = "" then
        strPackageType = "PDF"
      end if

      strJobName = getXMLValue("/ClientDocument/Send/@jobName")
      strSendVia = getXMLValue("/ClientDocument/Send/@via")
      strSkipSend = getXMLValue("/ClientDocument/Send/@skipSend")

      Set oExe = CreateObject("DataPresenter.CExecute")
      oExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, sUserID
      strImageRootDir = replace(oExe.Setting("Document/RootDirectory"), "\\", "\")

      strCCCXSLPath = oExe.Setting("ClientDocument/Partner/CCC/@XSLFile")

      If strCCCXSLPath <> "" Then
        strCCCXSLPath = server.mappath(strCCCXSLPath)
      End If
      call CheckError

      strDocumentUploadXSL = oExe.Setting("ClientDocument/DocumentUpload/@xsl")
      if strDocumentUploadXSL <> "" then
         strDocumentUploadXSL = Server.MapPath("../install/DocumentMgr/" & strDocumentUploadXSL)
      else
         handleError "Missing APD configuration setting. Please contact Helpdesk."
      end if

      strDocumentUploadPostBackURL = oExe.Setting("ClientDocument/DocumentUpload/@postbackWebPage")
      if strDocumentUploadPostBackURL <> "" then
         strDocumentUploadPostBackURL = "http://" & strServerName & "/" & strDocumentUploadPostBackURL
      else
         handleError "Missing APD configuration setting. Please contact Helpdesk."
      end if

      strDocumentUploadCoversheetXSL = oExe.Setting("ClientDocument/DocumentUpload/@coversheet")
      if strDocumentUploadCoversheetXSL <> "" then
         strDocumentUploadCoversheetXSL = Server.MapPath("../install/DocumentMgr/" & strDocumentUploadCoversheetXSL)
      else
         handleError "Missing APD configuration setting. Please contact Helpdesk."
      end if


      strPackageTypeValue = oExe.Setting("ClientDocument/PackageType/Package[@code='" & strPackageType & "']/@value")
      err.clear
      if strPackageTypeValue = "" then
        strPackageTypeValue = 0
      end if
      blnAttachToClaim = (oExe.Setting("ClientDocument/Default/Notify/@AttachToClaim") = "true")
      strFailureEmail = oExe.Setting("EventHandling/Email")
      strNOLIncoming = oExe.Setting("IMPSEmail/BCC")
      strNoteInsSP = oExe.Setting("ClaimPoint/SPMap/NoteInsDetail")
      strMessageNoteTypeID = oExe.Setting("ClientDocument/MessageNoteType/@id")

      strClaimMessage = getXMLValue("/ClientDocument/Message")
      strMessageFormat = getXMLValue("/ClientDocument/Message/@format")
      strClaimMessageEscaped = getXMLValue("/ClientDocument/MessageEscaped")

      strClaimAspectID = getXMLValue("/ClientDocument/Vehicle/@claimAspectID")
      strVehicleStatusID = getXMLValue("/ClientDocument/Vehicle/@statusID")
      strUserID = getXMLValue("/ClientDocument/Send/User/@id")

      if strClaimMessage <> "" then
        response.write "1.....<br/>"
        if strNoteInsSP <> "" and strMessageNoteTypeID <> "" then
        response.write "2.....<br/>"

          if strClaimAspectID <> "" then
            response.write "3.....<br/>"
            if isNumeric(strClaimAspectID) then
              response.write "4.....<br/>"
              strMessageDocID = oExe.ExecuteSpNp( strNoteInsSP, "ClaimAspectID=" & strClaimAspectID & "&NoteTypeID=" & strMessageNoteTypeID & _
                                                  "&StatusID=" & strVehicleStatusID & "&Note=" & strClaimMessageEscaped & "&UserID=" & strUserID)
              response.write "strMessageDocID=" & strMessageDocID & "<br/>"
              set oMessageNode = oXML.selectSingleNode("/ClientDocument/Message")
              if not oMessageNode is nothing then
                response.write "5.....<br/>"
                oMessageNode.setAttribute "id", strMessageDocID
              end if
            end if
          end if
        else
          handleError "Error retrieving setting to add a note."
        end if
      end if

'      if strSendVia = "CCAV" then
        strCCAVXSLPath = oExe.Setting("PartnerSettings/CCAV/DocumentBundling/@XSL")
        if strCCAVXSLPath <> "" then
          strCCAVXSLPath = Server.MapPath("../install/LAPDPartnerDataMgr/" & strCCAVXSLPath)
        end if

        response.write "<br/>" & oXML.xml & "<br/><br/>"

        if objFSO.FileExists(strCCAVXSLPath) then
          Dim oXSL
          set oXSL = Server.CreateObject("MSXML2.DOMDocument")
          oXSL.async = false
          oXSL.load strCCAVXSLPath

          If oXSL.parseError.errorCode <> 0 Then
            handleError "The Autoverse transformation settings for Document bundling is malformed. Please contact Helpdesk."
          end if

          strDocTemplate = oXML.transformNode(oXSL)

          set oXSL = nothing
        else
          handleError "Missing Partner transformation settings for Document Bundling. Please contact Helpdesk."
        end if
'      end if

      response.write "strNOLIncoming=" & strNOLIncoming & "<br/>"
      response.write "strFailureEmail=" & strFailureEmail & "<br/>"
      response.write "strJobName=" & strJobName & "<br/>"
      response.write "strCCAVXSLPath=" & strCCAVXSLPath & "<br/>" & vbcrlf
      response.write "strDocTemplate=" & strDocTemplate & "<br/>" & vbcrlf
      response.write "strNoteInsSP=" & strNoteInsSP & "<br/>" & vbcrlf

      blnCCAVMessageOnly = false

      if (strSendVia = "CCAV") and (getXMLValue("/ClientDocument/Message") <> "") and (oXML.selectNodes("/ClientDocument/Document").length = 0) then
        'call the Partner Data Mgr to do the stuff
        Dim oPartnerMgr, bSuccess, strNotifyEventSP, strMsgSentEventID, mexMonikerString
        mexMonikerString = GetWebServiceURL() 
        set oPartnerMgr = GetObject(mexMonikerString)
        oPartnerMgr.InitiatePartnerXmlTransaction(strDocTemplate)

        if err.number <> 0 then
          bSuccess = false
        else
          bSuccess = true
        end if

        set oPartnerMgr = nothing

        If bSuccess Then
          blnCCAVMessageOnly = true
          'throw the message sent event
          strNotifyEventSP = oExe.Setting("ShopAssignment/EventSpName")
          strMsgSentEventID =  oExe.Setting("ClientDocument/Event[@name='External Claim Message Sent']/@id")
          oExe.ExecuteSpNp strNotifyEventSP, "EventID=" & strMsgSentEventID & _
                                         "&ClaimAspectID=" & strClaimAspectID & _
                                         "&Description=External Claim Message Sent." & _
                                         "&ConditionValue=" & _
                                         "&UserID=" + strUserID

        %>
          <html><head><SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT><script>                                                                                          ClientWarning("The message was successfully sent."); top.window.close()</script></head></html>
        <%
            response.write "Message sent<br/>"
        Else
            %>
          <html><head><SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT><script>                                                                                          ClientError("Error occurred while sending the message.");</script></head></html>
        <%
            response.write "Error sending message<br/>"
            bSuccess = false
        End If
'      else
      end if
        'use the document bundler.
        Dim objJob, objPkg, strNotesTemplate
        set objJob = Server.CreateObject("LYNXDocSpooler.clsJob")

        Call CheckError

        objJob.newJob(strJobName)

        Call CheckError

        response.write "New Job " & strJobName & " created<br/>"

        Set objPkg = objJob.Package

        Call CheckError
        response.write "New package created<br/>"

        objPkg.PackageType = CInt(strPackageTypeValue)
        if right(strPackageType, 1) = "z" then
          strPackageType = "zip"
        else
          strPackageType = lcase(left(strPackageType, 3))
        end if

        if getXMLValue("/ClientDocument/Send/MergedFileName") <> "" then
          'objPkg.outputFileName = getXMLValue("/ClientDocument/Send/MergedFileName")

			dim strOutputFileName1
			strOutputFileName1 = getXMLValue("/ClientDocument/Send/MergedFileName")

            '-----------------------------------------------
			' 12Feb2013 - TVD - Replace special characters
			' I don't like this fix, but it works
            '-----------------------------------------------
            strOutputFileName1 = replace(strOutputFileName1,chr(58),"")  ' :
            strOutputFileName1 = replace(strOutputFileName1,chr(38),"")  ' &
            strOutputFileName1 = replace(strOutputFileName1,chr(35),"")  ' #
            strOutputFileName1 = replace(strOutputFileName1,chr(91),"")  ' [
            strOutputFileName1 = replace(strOutputFileName1,chr(93),"")  ' ]
            strOutputFileName1 = replace(strOutputFileName1,chr(40),"")  ' (
            strOutputFileName1 = replace(strOutputFileName1,chr(41),"")  ' )
            strOutputFileName1 = replace(strOutputFileName1,chr(36),"")  ' $
            strOutputFileName1 = replace(strOutputFileName1,chr(43),"")  ' +
            strOutputFileName1 = replace(strOutputFileName1,chr(44),"")  ' ,
            strOutputFileName1 = replace(strOutputFileName1,chr(126),"")  ' ~
            strOutputFileName1 = replace(strOutputFileName1,chr(39),"")  ' '
            strOutputFileName1 = replace(strOutputFileName1,chr(34),"")  ' "
            strOutputFileName1 = replace(strOutputFileName1,chr(42),"")  ' *
            strOutputFileName1 = replace(strOutputFileName1,chr(63),"")  ' ?
            strOutputFileName1 = replace(strOutputFileName1,chr(60),"")  ' <
            strOutputFileName1 = replace(strOutputFileName1,chr(62),"")  ' >
            strOutputFileName1 = replace(strOutputFileName1,chr(123),"")  ' {
            strOutputFileName1 = replace(strOutputFileName1,chr(125),"")  ' }
			objPkg.outputFileName = strOutputFileName1

 	    else
          objPkg.outputFileName = "Claim Documents"
        end if

        Dim strNotesMsg
        strNotesMsg = getXMLValue("/ClientDocument/NotesMsg")
        if strNotesMsg <> "" then
          strNotesTemplate = "<ClientDocuments><NotesMsg claimAspectID=""" & strClaimAspectID & """ vehicleStatusID=""" & strVehicleStatusID & """ userID=""" & strUserID & """ noteTypeID=""" & strMessageNoteTypeID & """><![CDATA[" & strNotesMsg & "]]></NotesMsg></ClientDocuments>"
        end if

        response.write "strNotesMsg = " & strNotesMsg & "<br/>"

        set oDocuments = oXML.selectNodes("/ClientDocument/Document")

        'attachments with a message or not
        'job coversheet
         if strDocumentUploadCoversheetXSL <> "" then
            response.write "Done.<br/>Opening job Coversheet XSL File: " & strFaxCoversheet & "<br/>"
            Dim strJobCoversheetXSL, objJobXSL

            set objJobXSL = objFSO.OpenTextFile(strDocumentUploadCoversheetXSL, 1, false)
            Call CheckError

            strJobCoversheetXSL = objJobXSL.ReadAll()

            set objJobXSL = nothing

            'add the coversheet to the job
            objJob.addDocumentXML oXML.xml, strJobCoversheetXSL, false, false, "", "__job_coversheet." & strPackageType, "", strOutputFormat

         end if


 response.write "strSendVia=" & strSendVia & "<br/>"

        'first create the processing package
        if strSkipSend <> "1" then
           select case strSendVia
             case "EML":
               'if blnAttachToClaim then
               '  strEmailBCC = strNOLIncoming
               '  blnAttachToClaim = false ' no need to attach separately. it will get sent to NOL Incoming via BCC
               'else
                 strEmailBCC = ""
               'end if
               if objPkg.addEmail(getXMLValue("/ClientDocument/Send/User/@email"), getXMLValue("/ClientDocument/Send/@value"), "", strEmailBCC, getXMLValue("/ClientDocument/Send/User/@email"), "", getXMLValue("/ClientDocument/Send/EmailSubject"), getXMLValue("/ClientDocument/Send/EmailBody")) then
                 response.write "Email package created<br/>"
               else
                 response.write "Email package NOT created<br/>"
                 bSuccess = false
               end if
               Call CheckError

               if oDocuments.length > 0 then
				   'post back the document processing status - applicable only for existing documents. claim message is not a document.
				   strPostbackWebPage = oExe.Setting("ClientDocument/DocumentStatusUpdate/@postbackWebPage")
				   strPostbackURL = "http://" & strServerName
				   if request.ServerVariables("SERVER_PORT") <> "80" then
				     strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
				   end if
				   'GO Prod hack
        		   strPostbackURL = "http://" & strServerName
        		   strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
				   if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strNotesTemplate) then
				     response.write "HTTP Post package created<br/>"
				   else
				     bSuccess = false
				   end if
               response.write err.number & "<br/>" & err.description & "<br/>"
				   Call CheckError
               end if

             case "FAX":
               response.Write "Fax number: " & getXMLValue("/ClientDocument/Send/@value") & "<br/>"
               if objPkg.addFax(getXMLValue("/ClientDocument/Send/@value"), strJobName) then
                 response.write "Fax package created<br/>"
               else
                 bSuccess = false
               end if

               Call CheckError

               if oDocuments.length > 0 then
   				   'post back the document processing status - applicable only for existing documents. claim message is not a document.
   				   strPostbackWebPage = oExe.Setting("ClientDocument/DocumentStatusUpdate/@postbackWebPage")
   				   strPostbackURL = "http://" & strServerName
   				   if request.ServerVariables("SERVER_PORT") <> "80" then
   					   strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
   				   end if
   				   'GO Prod hack
   				   strPostbackURL = "http://" & strServerName
   				   strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
   				   if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strNotesTemplate) then
   					   response.write "HTTP Post package created<br/>"
   				   else
   					   bSuccess = false
   				   end if
   				   Call CheckError
               end if

			   'add a coversheet
			   response.write "fax coversheet<br/>"
			   if oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/Fax/@coversheet") <> "" then
				   strFaxCoversheet = oExe.Setting("ClientDocument/Insurance[@ID='" & sInsCoID & "']/Fax/@coversheet")
			   end if
			   if strFaxCoversheet = "" then
				   response.write "Getting default fax coversheet<br/>"
				   strFaxCoversheet = oExe.Setting("ClientDocument/Default/Fax/@coversheet")
			   end if
			   err.clear
			   response.write "strFaxCoversheet=" & strFaxCoversheet & "<br/>"

			   if strFaxCoversheet <> "" then
				   strFaxCoversheet = Server.MapPath("../install/LAPDFaxServer/" & strFaxCoversheet)

				   response.write "Done.<br/>Opening Fax Coversheet XSL File: " & strFaxCoversheet & "<br/>"
				   Dim strFaxCoversheetXSL, objFaxXSL

				   set objFaxXSL = objFSO.OpenTextFile(strFaxCoversheet, 1, false)
				   Call CheckError

				   strFaxCoversheetXSL = objFaxXSL.ReadAll()

				   set objFaxXSL = nothing

				   'add the coversheet to the job
				   objJob.addDocumentXML oXML.xml, strFaxCoversheetXSL, false, false, "", "__coversheet." & strPackageType, "", strOutputFormat

			   end if

             case "CCAV":
               if blnCCAVMessageOnly = false then
                  strPostbackWebPage = oExe.Setting("PartnerSettings/CCAV/DocumentBundling/@postbackWebPage")
                  strPostbackURL = "http://" & strServerName
                  if request.ServerVariables("SERVER_PORT") <> "80" then
                    strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
                  end if

                  'hacked for GO prod server.
                  strPostbackURL = "http://" & strServerName
                  strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
                  if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strDocTemplate) then
                    response.write "HTTP Post package created<br/>"
                  else
                    bSuccess = false
                  end if


                  'post back the document processing status
                  strPostbackWebPage = oExe.Setting("ClientDocument/DocumentStatusUpdate/@postbackWebPage")
                  strPostbackURL = "http://" & strServerName
                  if request.ServerVariables("SERVER_PORT") <> "80" then
                    strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
                  end if

                  'Hack for GO Prod
                  strPostbackURL = "http://" & strServerName
                  strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
                  if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strNotesTemplate) then
                    response.write "HTTP Post package created<br/>"
                  else
                    bSuccess = false
                  end if

                  Call CheckError
               end if
              case "FTP":
                  Dim strFTPServer, strConfigPath, strLogon, strPassword, strPath
                  strFTPServer = getXMLValue("/ClientDocument/Send/@FTPServer")
                  if instr(1, strFTPServer, "$CONFIG$:") > 0 then
                     'get the FTP settings from the config file
                     strConfigPath = mid(strFTPServer, 10)
                     strFTPServer = "$INI$" & oExe.Setting(strConfigPath)
                  end if
                  if (getXMLValue("/ClientDocument/Send/@FTPServerPort") <> "" and getXMLValue("/ClientDocument/Send/@FTPServerPort") <> "$EMPTY$") then
                     strFTPServer = strFTPServer + ":" + getXMLValue("/ClientDocument/Send/@FTPServerPort")
                  end if
                  strLogon = getXMLValue("/ClientDocument/Send/@FTPLogonID")
                  strPassword = getXMLValue("/ClientDocument/Send/@FTPPassword")
                  strPath = getXMLValue("/ClientDocument/Send/@FTPPath")
                  if strLogon = "$EMPTY$" then strLogon = "  "
                  if strPassword = "$EMPTY$" then strPassword = "  "
                  if strPath = "$EMPTY$" then strPath = "  "
                  if objPkg.addFTP(strFTPServer, _
                                   strLogon, _
                                   strPassword, _
                                   strPath) then
                     response.write "FTP package created<br/>"
                     err.clear
                  else
                     bSuccess = false
                  end if

                  'response.write "FTP: " & strFTPServer
                  'response.end
                  'Call CheckError

                  if oDocuments.length > 0 then
                     'post back the document processing status - applicable only for existing documents. claim message is not a document.
                     strPostbackWebPage = oExe.Setting("ClientDocument/DocumentStatusUpdate/@postbackWebPage")
                     strPostbackURL = "http://" & strServerName
                     if request.ServerVariables("SERVER_PORT") <> "80" then
                        strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
                     end if
                     'GO Prod hack
                     strPostbackURL = "http://" & strServerName
                     strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
                     if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strNotesTemplate) then
                        response.write "HTTP Post package created<br/>"
                     else
                        bSuccess = false
                     end if
                     Call CheckError
                  end if
                  response.write "FTP package complete<br/>"
              case "File"
                  strFileCopyLocation = getXMLValue("/ClientDocument/Send/@location")
                  if strFileCopyLocation <> "" then
                     if objPkg.addFileCopy(strFileCopyLocation, true) then
                        response.write "File Copy package created<br/>"
                     else
                        bSuccess = false
                     end if
                     Call CheckError
                  end if
           end select
        end if

        strFileCopyLocation = getXMLValue("/ClientDocument/File/@location")
        if strFileCopyLocation <> "" then
            if objPkg.addFileCopy(strFileCopyLocation, true) then
               response.write "File Copy package created<br/>"
            else
               bSuccess = false
            end if
          '  response.Write "Inside File Copy package"
           
            ' Changes Begin- Add HTTP post job for WebService SendToCarrier Update
         
               if oDocuments.length > 0 then
				   'post back the document processing status - applicable only for existing documents. claim message is not a document.
				   strPostbackWebPage = oExe.Setting("ClientDocument/DocumentStatusUpdate/@postbackWebPage")
				   strPostbackURL = "http://" & strServerName
				   if request.ServerVariables("SERVER_PORT") <> "80" then
				     strPostbackURL = strPostbackURL & ":" & request.ServerVariables("SERVER_PORT")
				   end if
				   'GO Prod hack
        		   strPostbackURL = "http://" & strServerName
        		   strPostbackURL = strPostbackURL & "/" & strPostbackWebPage
				   if objPkg.addHTTPPost(strPostbackURL, false, "Document", "", strNotesTemplate) then
				     response.write "HTTP Post package created<br/>"
				   else
				     bSuccess = false
				   end if
                   response.Write "SendToCarrierUpdate HTTP Post Package Added "
               response.write err.number & "<br/>" & err.description & "<br/>"
				   Call CheckError
               end if
               'Changes End

            Call CheckError
        end if

       ' response.Write "OutSide"
       ' response.End

        if blnAttachToClaim then
            'response.write "UserEmailAddress = " & getXMLValue("/ClientDocument/Send/User/@email") & "<br/>"
            'if objPkg.addEmail(getXMLValue("/ClientDocument/Send/User/@email"), strNOLIncoming, "", "", "", "", getXMLValue("/ClientDocument/Send/EmailSubject"), getXMLValue("/ClientDocument/Send/EmailBody")) then
            '  response.write "Email package created<br/>"
            'else
            '  bSuccess = false
            'end if

            'new method - we will post back the documents to APD directly instead of loopback email
            if strDocumentUploadXSL <> "" then
                Dim oXSL2
                set oXSL2 = Server.CreateObject("MSXML2.DOMDocument")
                oXSL2.async = false
                oXSL2.load strDocumentUploadXSL

                If oXSL2.parseError.errorCode <> 0 Then
                  handleError "The Document attach transformation settings for Document bundling is malformed. Please contact Helpdesk."
                end if

                strDocTemplate = oXML.transformNode(oXSL2)

                set oXSL2 = nothing
           
               'add a HTTP post job
               if objPkg.addHTTPPost(strDocumentUploadPostBackURL, true, "File", "", strDocTemplate) then
                 response.write "HTTP Post package created for loop back<br/>"
               else
                 bSuccess = false
               end if
            else
               'old method. Send an email to NOL Incoming.
               if objPkg.addEmail(getXMLValue("/ClientDocument/Send/User/@email"), strNOLIncoming, "", "", "", "", getXMLValue("/ClientDocument/Send/EmailSubject"), getXMLValue("/ClientDocument/Send/EmailBody")) then
                 response.write "Email package created<br/>"
               else
                 bSuccess = false
               end if
            end if
            response.write "Added loop back<br/>"
            err.clear
            Call CheckError
        end if

        response.write "Now processing documents...<br/>"

        'now add the documents for processing
        Dim strFileName, strFileOptions, strFilePath, strXSL, strOutputFormat, strOutputFileName
        Dim blnMerge, blnNoConvert, strFileExt, blnException, strDocumentTypeName, strDocumentEmbeded, strDocumentEmbededText
        Dim strHTMLOptions

        strClaimMessage = getXMLValue("/ClientDocument/MessageHTML")
        if getXMLValue("/ClientDocument/MessageHTML/@merge") = "true" then
          blnMerge = true
        else
          blnMerge = false
        end if
        err.clear

        strMessageFormat = "htm"
        if strClaimMessage <> "" then
          if strMessageFormat = "htm" or strMessageFormat = "html" then
            strClaimMessage = "<html><head></head><body>" & strClaimMessage & "</body></html>"
          end if
          'add the note to the job
          if objJob.addFreeFormText(CStr(strClaimMessage), blnMerge, false, "", "Claim Message.txt", "")  then
            response.write "Added Message<br/>"
          else
            bSuccess = false
          end if
        end if

        response.write "count: " & oDocuments.length & "<br>"
        Dim i

        i = 1

        for each oDocument in oDocuments
            strXSL = ""
            blnNoConvert = false
            strDocumentEmbeded = false
            'response.write i & "  -- " & oDocument.xml & "<br/>"
            strFileName = replace(oDocument.getAttribute("imageLocation"), "\\", "\")
            'response.write "ImageLocation..."
            strFilePath = objFSO.BuildPath(strImageRootDir, strFileName)
            'response.write "strImageRootDir..."
            strOutputFormat = oDocument.getAttribute("outputFormat")
            'response.write "outputFormat..."
            blnException = oDocument.getAttribute("exception")
            'response.write "exception..."
            blnMerge = (oDocument.getAttribute("merge") = "true")
            'response.write "merge..."

            '-----------------------------------------------
			' 12Feb2013 - TVD - Replace special characters
			' I don't like this fix, but it works
            '-----------------------------------------------
			strOutputFileName = oDocument.getAttribute("outputFileName")
            strOutputFileName = replace(strOutputFileName,chr(58),"")  ' :
            strOutputFileName = replace(strOutputFileName,chr(38),"")  ' &
            strOutputFileName = replace(strOutputFileName,chr(35),"")  ' #
            strOutputFileName = replace(strOutputFileName,chr(91),"")  ' [
            strOutputFileName = replace(strOutputFileName,chr(93),"")  ' ]
            strOutputFileName = replace(strOutputFileName,chr(40),"")  ' (
            strOutputFileName = replace(strOutputFileName,chr(41),"")  ' )
            strOutputFileName = replace(strOutputFileName,chr(36),"")  ' $
            strOutputFileName = replace(strOutputFileName,chr(43),"")  ' +
            strOutputFileName = replace(strOutputFileName,chr(44),"")  ' ,
            strOutputFileName = replace(strOutputFileName,chr(126),"")  ' ~
            strOutputFileName = replace(strOutputFileName,chr(39),"")  ' '
            strOutputFileName = replace(strOutputFileName,chr(34),"")  ' "
            strOutputFileName = replace(strOutputFileName,chr(42),"")  ' *
            strOutputFileName = replace(strOutputFileName,chr(63),"")  ' ?
            strOutputFileName = replace(strOutputFileName,chr(60),"")  ' <
            strOutputFileName = replace(strOutputFileName,chr(62),"")  ' >
            strOutputFileName = replace(strOutputFileName,chr(123),"")  ' {
            strOutputFileName = replace(strOutputFileName,chr(125),"")  ' }

			'response.write "outputFileName..."
			strDocumentTypeName = oDocument.getAttribute("documentTypeName")
            'response.write "documentTypeName..."
            if isNull(oDocument.getAttribute("imageEmbedded")) = false then
               strDocumentEmbeded = oDocument.getAttribute("imageEmbedded")
            else
               strDocumentEmbeded = ""
            end if
            if isNull(oDocument.getAttribute("htmlOptions")) = false then
               strHTMLOptions = oDocument.getAttribute("htmlOptions")
            else
               strHTMLOptions = ""
            end if
            err.clear
            'response.write "1..."
            strFileExt = lcase(objFSO.getExtensionName(strFileName))
            'response.write "2..."
            if blnException then
              blnNoConvert = true
            end if
            'response.write "3..."
            Call CheckError
            'response.write "4..."
            if strSendVia = "CCAV" then
              blnMerge = false
            end if
            'response.write "5..."
            response.write "strFileExt = " & strFileExt & "<br/>"
            if strFileExt = "xml" then
              strXSL = strCCCXSLPath
            else
              strXSL = ""
            end if

            response.write "strXSL=" & strXSL & "<br/>"
            response.write "strFileName=" & strFileName & "<br/>"
            response.write "strFilePath=" & strFilePath & "<br/>"
            response.write "Merge=" & CStr(blnMerge) & "<br/>"
            response.write "NoConvert=" & CStr(blnNoConvert) & "<br/>"
            response.write "strDocumentEmbeded=" & CStr(strDocumentEmbeded) & "<br/>"

            if strDocumentEmbeded = "true" then
'               dim objStream ', objStream2
'               set objStream = Server.createObject("ADODB.Stream")
'               objStream.Type = 2 'binary mode
'               objStream.Open
'               objStream.WriteText oDocument.text
'               objStream.Position = 0
'               response.write oDocument.text & "<br/>"
'
'               if objJob.addDocumentEmbedV2(objStream, _
'                                            strOutputFileName, _
'                                            nothing, _
'                                            blnMerge, _
'                                            blnNoConvert, _
'                                            "$DONTMERGEWITHJOBDOCUMENT$", _
'                                            strOutputFileName) then
'                   response.write "Add Document Embedded " & strOutputFileName & "<br/>"
'                 else
'                   bSuccess = false
'               end if
'               set objStream = nothing
               if objJob.addFreeFormText(CStr(oDocument.text), _
                                          blnMerge, _
                                          blnNoConvert, _
                                          strHTMLOptions, _
                                          strOutputFileName) then
                  response.write "Add Document Embedded " & strOutputFileName & "<br/>"
               else
                  bSuccess = false
               end if
            else
               if strXSL <> "" then
                 Dim strXMLFileName
                 Dim strXMLText, strXSLText
                 Dim oTxtFile

                 strXMLFileName = objFSO.GetBaseName(strFilePath)
                 response.write "strXMLFileName=" & strXMLFileName & "<br/>"
                 response.write "strFilePath=" & strFilePath & "<br/>"

                 set oTxtFile = objFSO.OpenTextFile(strFilePath, 1, false)
                 Call CheckError

                 strXMLText = oTxtFile.ReadAll()

                 set oTxtFile = nothing

                 response.write "Done.<br/>Opening XSL File: " & strXSL & "<br/>"

                 set oTxtFile = objFSO.OpenTextFile(strXSL, 1, false)
                 Call CheckError

                 strXSLText = oTxtFile.ReadAll()

                 set oTxtFile = nothing

                 response.write "Done.<br/>"

                 strXSLText = ""

                 objJob.addDocumentXML strXMLText, strXSLText, blnMerge, blnNoConvert, strHTMLOptions, CStr(strOutputFileName), "DocumentID=" & oDocument.getAttribute("id") & "|", strOutputFormat

                 Call CheckError

                 response.write "Done.<br/>"
               else
                 if objJob.addDocumentLinked(CStr(strFilePath), cstr(strXSL), blnMerge, blnNoConvert, strHTMLOptions, cstr(strOutputFileName), "DocumentID=" & oDocument.getAttribute("id") & "|", strOutputFormat) then
                   response.write "Add Document " & strFilePath & "<br/>"
                 else
                   bSuccess = false
                 end if
               end if
            end if
            err.clear
            i = i + 1
        next

        set oDocuments = nothing
        set oDocument = nothing
        set objFSO = nothing
        'response.write "here"

        strSuccessEmailTo = getXMLValue("/ClientDocument/Send/User/@email")
        strSuccessEmailSubject = getXMLValue("/ClientDocument/Send/Success/Subject")
        strSuccessEmailBody = getXMLValue("/ClientDocument/Send/Success/Body")

        strFailureEmailTo = strFailureEmail
        strFailureEmailSubject = getXMLValue("/ClientDocument/Send/Failure/Subject")
        strFailureEmailBody = getXMLValue("/ClientDocument/Send/Failure/Body")

        if strSuccessEmailSubject <> "" and strSuccessEmailTo <> "" then
          if objPkg.addNotifySuccess("", CStr(strSuccessEmailTo), CStr(strSuccessEmailSubject), CStr(strSuccessEmailBody)) then
          else
            bSuccess = false
          end if

          Call CheckError
        end if

        if strFailureEmailSubject <> "" and strFailureEmailTo <> "" then
          if objPkg.addNotifyError("", CStr(strFailureEmailTo), CStr(strFailureEmailSubject), CStr(strFailureEmailBody)) then
          else
            bSuccess = false
          end if

          Call CheckError
        end if

        Call CheckError

        If objJob.submitJob Then

       ' response.End
        %>
          <html><head><SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT><script>                                                                                          ClientWarning("The processing job was successfully submitted."); top.window.close()</script></head></html>
        <%
            response.write "Job submitted<br/>"
        Else
            %>
          <html><head><SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT><script>                                                                                          ClientError("Error occurred while submitting the message/document(s)");</script></head></html>
        <%
            response.write "Error submitting Job<br/>"
            bSuccess = false
        End If

        Call CheckError

        set objPkg = nothing
        set objJob = nothing

'			'no need to do this. The processor will post to APD to add a note when the job is processed.
'        if strNotesMsg <> "" then
'          'add a note to the system that the Documents were sent
'          oExe.ExecuteSpNp strNoteInsSP, "ClaimAspectID=" & strClaimAspectID & "&NoteTypeID=" & strMessageNoteTypeID & _
'                                      "&StatusID=" & getXMLValue("/ClientDocument/Vehicle/@statusID") & "&Note=" & strNotesMsg & "&UserID=" & getXMLValue("/ClientDocument/Send/User/@id")
'        end if
'      end if



      set oXML = nothing
      set oExe = nothing
      Call CheckError


    End Function

    Sub handleError(strMsg)
      showMessage strMsg
      response.end
    End sub

    Function getXMLValue(strXPath)
      dim strRet, oNode
      strRet = ""
      if not oXML is nothing then
        if strXPath <> "" then
          set oNode = oXML.selectSingleNode(strXPath)
          if not oNode is nothing then
            strRet = oNode.text
          end if
        end if
      end if
      getXMLValue = strRet
    End Function


    Sub showMessage(strMsg)
%>
    <html><head><SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT><script>                                                                                    ClientError("<%=strMsg%>");</script></head></html>
<%
    End Sub
%>