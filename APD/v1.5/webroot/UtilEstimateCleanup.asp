<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML xmlns:IE>
<HEAD>
  <title>APD Estimate Cleanup Utility</title>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
  IE\:APDButton {behavior:url("/behaviors/APDButton.htc")}
  IE\:APDInputDate {behavior:url("/behaviors/APDInputDate.htc")}
  IE\:APDCheckBox {behavior:url("/behaviors/APDCheckBox.htc")}
</STYLE>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>

<SCRIPT language="JavaScript">

  var gsFromDate = "";
  var gsToDate = "";
  var gsNotReviewedOnlyFlag = "";
  
  function refreshThisPage()
  {
    gsFromDate = document.getElementById("dtFrom").value;
    gsToDate = document.getElementById("dtTo").value;
    gsNotReviewedOnlyFlag = document.getElementById("NotReviewedOnlyFlag").value;

    if (gsFromDate == '' || gsToDate == '')
    {
      ClientWarning('Please enter the "From" and "To" dates.');
      return;
    }

    var DaysDiff = daysDiff(gsFromDate, gsToDate);
    if (DaysDiff < 0)
    {
      ClientWarning('The "To" date cannot be before the "From" date.');
      return;
    }
    else
    {
      if (DaysDiff > 15)
      {
        var sDatesConfirm = YesNoMessage("Search Warning", "You are searching for " +DaysDiff+ " days which could result in a long list of claims. Press YES to continue, or NO to cancel.");
        if (sDatesConfirm != "Yes")
        return;
      }
      
      parent.document.getElementById("ifrmClaimsList").src = "blank.asp";
      parent.document.getElementById("ifrmEstimateList").src = "blank.asp";
      parent.document.getElementById("ifrmDocViewer").src = "blank.asp";
      parent.document.getElementById("ifrmEstimateDetails").src = "blank.asp";
      document.getElementById("ifrmClaimsList").src = "UtilEstimateClaimList.asp?FromDate=" + gsFromDate + "&ToDate=" + gsToDate + "&NotReviewedOnlyFlag=" + gsNotReviewedOnlyFlag;
    }
  }

  function daysDiff(From, To)
  {
  	var x = new Date (From);
  	var DateFrom = new Date(x.toGMTString());
  	var YearFrom = takeYear(DateFrom);
  	var MonthFrom = DateFrom.getMonth();
  	var DayFrom = DateFrom.getDate();
  	var ModFrom = (Date.UTC(YearFrom,MonthFrom,DayFrom,0,0,0))/86400000;
  	x = new Date (To);
  	var DateTo = new Date(x.toGMTString());
  	var YearTo = takeYear(DateTo);
  	var MonthTo = DateTo.getMonth();
  	var DayTo = DateTo.getDate();
  	var ModTo = (Date.UTC(YearTo,MonthTo,DayTo,0,0,0))/86400000;
  	var DaysDiff = ModTo - ModFrom;
    return DaysDiff;
  }
    
  function takeYear(theDate)
  {
  	x = theDate.getYear();
  	var y = x % 100;
  	y += (y < 38) ? 2000 : 1900;
  	return y;
  }

</SCRIPT>

</HEAD>

<BODY class="bodyAPDSelect" background="/images/fundo_default.gif" unselectable="on">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<div id="PageLayout" style="position: absolute; left: 8px; top: 6px; background-color: #FFFFFF;" unselectable="on">

  <table width="450" height="60" border="0" cellspacing="0" cellpadding="4">
    <tr>
      <td colspan="2" align="center">
        <SPAN style="color: #000066; font-weight: bold; font-size: 12px;">Search For Claims</SPAN>
      </td>
      <td nowrap>
        <IE:APDButton id="cmdGet" name="cmdGet" value="Get Claims" width="100" CCDisabled="false" CCTabIndex="4" onButtonClick="refreshThisPage()" />
      </td>
    </tr>
    <tr>
      <td><strong>From:</strong>
        <IE:APDInputDate id="dtFrom" name="dtFrom" type="date" futureDate="false" required="false" canDirty="false" CCDisabled="false" CCTabIndex="1" onChange="" />
      </td>
      <td><strong>To:</strong>
        <IE:APDInputDate id="dtTo" name="dtTo" type="date" futureDate="false" required="false" canDirty="false" CCDisabled="false" CCTabIndex="2" onChange="" />
      </td>
      <td>
        <IE:APDCheckBox id="NotReviewedOnlyFlag" name="NotReviewedOnlyFlag" caption="Not Reviewed Only" value="0" alignment="2" CCTabIndex="3" />
      </td>
    </tr>
  </table>

<iframe src="blank.asp" id="ifrmClaimsList" scrolling="no" frameborder="0" style="left: 0px; top: 68px; width:450px; height:402px; border:1px; visibility:visible; position:absolute; background:transparent; overflow:hidden;" >
</iframe>

<IFRAME src="blank.asp" id="ifrmEstimateList" scrolling="no" frameborder="0" style="left: 0px; top:478px; width:450px; height:182px; border:1px; visibility:visible; position:absolute; background:transparent; overflow:hidden;" >
</IFRAME>

<IFRAME src="blank.asp" id="ifrmDocViewer" scrolling="no" frameborder="0" style="left: 458px; top: 0px; width:542px; height:502px; border:1px; visibility:visible; position:absolute; background:transparent; overflow:hidden;" >
</IFRAME>

<IFRAME src="blank.asp" id="ifrmEstimateDetails" scrolling="no" frameborder="0" style="left: 458px; top: 510px; width:542px; height:150px; border:1px; visibility:visible; position:absolute; background:transparent; overflow:hidden;" >
</IFRAME>

</BODY>
</HTML>
