<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
    
	On Error Resume Next

  dim strCity, strState, strUserID
  dim objExe
  dim strHTML
     
  ReadSessionKey()
     
  strCity = Request("City")
  strState = Request("State")
  strUserID = Request("UserID")
    
    
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()
    
	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()
    
	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspShopZipByCityStateXML", "CityPrimaryZip.xsl", strCity, strState)
	CheckError()

	Set objExe = Nothing

  Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

