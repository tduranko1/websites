<%@LANGUAGE="VBSCRIPT"%>
<%
    Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
  Dim objData
  dim strHTML

    Dim LoginUserID

    ' Get the logged in user
    LoginUserID = GetSession("UserID")
    CheckError()

    ' Initiate DataAccessor
    set objData = CreateObject("DataPresenter.CExecute")
    CheckError()

    objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH")
    CheckError()

    'Get UserList
    strHTML = objData.ExecuteSpAsXML("uspAdmRoleGetListXML", "RoleList.xsl")

    'Processing Done, destroy DataAccessor
    set objData = Nothing


    Call KillSessionObject

    On Error Goto 0

    response.write strHTML

%>
