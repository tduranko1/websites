<%@LANGUAGE="VBSCRIPT"%>
<%
    Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
  Dim objData
  Dim rsCrud
  dim strHTML

    Dim LoginUserID

    ' Get the logged in user
    LoginUserID = GetSession("UserID")
    CheckError()

    ' Initiate DataAccessor
    set objData = CreateObject("DataPresenter.CExecute")
    CheckError()

    objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH")
    CheckError()


    ' Get CRUD
    set rsCRUD = objData.ExecuteSpAsRS("dbo.uspCRUDGetDetail", "'User Profile'", LoginUserID)
    CheckError()


    ' Check to see if reads are allowed, if not, error out and quit
    if (rsCRUD.Fields.Item("ReadFlag").Value = false) then
        ' User has no permissions
        err.Raise 65535, "Security Violation", "Access Denied to view User Information.  Please see your supervisor."
        CheckError()
    end if


  Dim CrudValue

    ' Now build the CRUD variable attribute based on rsCRUD returned
    CrudValue = ""
    if (rsCRUD.Fields.Item("CreateFlag").Value = true) then
        CrudValue = CrudValue & "C"
    else
        CrudValue = CrudValue & "_"
    end if
    if (rsCRUD.Fields.Item("ReadFlag").Value = true) then
        CrudValue = CrudValue & "R"
    else
        CrudValue = CrudValue & "_"
    end if
    'Just add an underscore for the remaining items to always disable Updates and Deletes
    CrudValue = CrudValue & "__"

    objData.AddXslParam "CRUDInfo", CrudValue
    CheckError()

    'Get UserList
    strHTML = objData.ExecuteSpAsXML("uspAdmUserGetListXML", "UserList.xsl")

    'Processing Done, destroy DataAccessor
    If Not rsCRUD Is Nothing Then
        If rsCRUD.State = 1 
          Then rsCRUD.Close
        end if
        Set rsCRUD = Nothing
    End If
    
    set objData = Nothing

    On Error Goto 0

    Call KillSessionObject

    response.write strHTML

%>
