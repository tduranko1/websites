<%@ Language=VBScript %>
<%response.buffer=true%>
<script id="DebugDirectives" runat="server" language="javascript">
// Set these to true to enable debugging or tracing
@set @debug=false
@set @trace=false
</script>

<% ' VI 6.0 Scripting Object Model Enabled %>
<!--#include file="../_ScriptLibrary/pm.asp"-->
<% if StartPageProcessing() Then Response.End() %>
<FORM name=thisForm METHOD=post>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="datapump.asp" -->

<html>
<head>
<meta name="VI60_DTCScriptingPlatform" content="Server (ASP)">
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<title>ADMIN - Program Sponsors</title>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="STYLESHEET" type="text/css" href="/css/CoolSelect.css"/>
<link REL="stylesheet" HREF="/css/CUSTOM.CSS" TYPE="text/css" /><SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>

<SCRIPT LANGUAGE="VBScript" RUNAT="Server">

Sub AddUpdateDelete(Mode)

	Dim spName , Params, strConn, SysLastUserID,ReturnVal
	'Response.Write Mode	'For debugging!

	SysLastUserID = GetSession("UserID")
    Call KillSesssionObject

	Params = ""
	spName = ""
	' Decide the stored proc name
	If Mode = "Update" Then
		spName = "uspRefCommunicationMethodUpdDetail"	' Update stored Procedure Name
		' Add identity column ONLY for updates
		Params = tbCommunicationMethodID.value & ", "
	ElseIf Mode = "Insert" Then
		spName = "uspRefCommunicationMethodInsDetail"	' Insert stored Procedure Name
	ElseIf Mode = "Delete" Then
		spName = ""									' Placeholder for Delete functionality
	End If

	Params = Params & tbDisplayOrder.value & ", "

	If cbEnabledFlag.getChecked() = true Then
		Params = Params & 1 & ", "
	Else
		Params = Params & 0 & ", "
	End If

	if Mode = "Update" Then
		Params = Params & "'" & tbName.value  &_
		"'" & ", " & SysLastUserID &_
		", " & "'" & tbSysLastUpdatedDate.value & "'"  'needed for updates ONLY!
	ElseIf Mode = "Insert" Then
		Params = Params & "'" & tbName.value  & "'" & ", " &_
		SysLastUserID							'BE CAREFUL!
												' On inserts, the form is blank
												' And the ID is lost
	End If
	'************************************
	'Response.Write spName & " "  & Params
	'Response.End
	'************************************
		
	Set ReturnVal = DataPumpAsRS(spName, Params)
		
	'Update Session Variable for the Shop Just added
	If ReturnVal.Fields.item(0) > "" Then
		
		if Mode = "Update" Then
			lblMessage.setCaption("Updated  ID : " & ReturnVal.Fields.item(0))
		Elseif  Mode = "Insert"   Then
			lblMessage.setCaption("Inserted  ID : " & ReturnVal.Fields.item(0))
		End If
	Else
		Response.Write "Error adding a Shop in procedure name btnSaveShop_onclick() of the ShopAdd.asp page"
	End If

	'Trap for errors
	If err.number = 0 Then
		'Response.Write "Successful"
		If Mode = "Update" then
			Dim ap ' Absoulte position
			ap = Recordset1.absolutePosition
			Recordset1.requery()
			Recordset1.moveAbsolute(ap)
		ElseIf Mode = "Insert" Then
			Recordset1.Requery()
			Recordset1.MoveLast()
		End if
	Else
		errorhandler()
	End If


End Sub
Function FlagTest(EstimateTypeFlag)
	if EstimateTypeFlag = True Then
		FlagTest = "<IMG SRC='/IMAGES/cbcheck.png'/>"
	Else
		FlagTest = "<IMG SRC='/IMAGES/cbUNcheck.png'/>"
	End If
End Function
</SCRIPT>

</head>
<body topMargin="0" leftMargin="0" bottomMargin="0">
<!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=Recordset1 style="LEFT: 0px; WIDTH: 461px; TOP: 0px; HEIGHT: 79px" height=79 
	width=461 classid="clsid:9CF5D7C2-EC10-11D0-9862-0000F8027CA0">
	<PARAM NAME="ExtentX" VALUE="12197">
	<PARAM NAME="ExtentY" VALUE="2090">
	<PARAM NAME="State" VALUE="(TCConn=\qudb_apd\q,TCDBObject=\qStored\sProcedures\q,TCDBObjectName=\quspRefCommunicationMethodGetList\q,TCControlID_Unmatched=\qRecordset1\q,TCPPConn=\qudb_apd\q,RCDBObject=\qRCDBObject\q,TCPPDBObject=\qStored\sProcedures\q,TCPPDBObjectName=\quspRefCommunicationMethodGetList\q,TCSQLStatement_Unmatched=\q\q,TCCursorType=\q3\s-\sStatic\q,TCCursorLocation=\q3\s-\sUse\sclient-side\scursors\q,TCLockType=\q3\s-\sOptimistic\q,TCCacheSize_Unmatched=\q10\q,TCCommTimeout_Unmatched=\q240\q,CCPrepared=1,CCAllRecords=1,TCNRecords_Unmatched=\q10\q,TCODBCSyntax_Unmatched=\q\q,TCHTargetPlatform=\q\q,TCHTargetBrowser_Unmatched=\qServer\s(ASP)\q,TCTargetPlatform=\qInherit\sfrom\spage\q,RCCache=\qRCBookPage\q,CCOpen=1,GCParameters=(Rows=0))">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<!--#INCLUDE FILE="../_ScriptLibrary/Recordset.ASP"-->
<SCRIPT LANGUAGE="JavaScript" RUNAT="server">
function _initRecordset1()
{
	var DBConn = Server.CreateObject('ADODB.Connection');
	DBConn.ConnectionTimeout = Application('udb_apd_ConnectionTimeout');
	DBConn.CommandTimeout = Application('udb_apd_CommandTimeout');
	DBConn.CursorLocation = Application('udb_apd_CursorLocation');
	DBConn.Open(Application('udb_apd_ConnectionString'), Application('udb_apd_RuntimeUserName'), Application('udb_apd_RuntimePassword'));
	var cmdTmp = Server.CreateObject('ADODB.Command');
	var rsTmp = Server.CreateObject('ADODB.Recordset');
	cmdTmp.ActiveConnection = DBConn;
	rsTmp.Source = cmdTmp;
	cmdTmp.CommandType = 4;
	cmdTmp.CommandTimeout = 240;
	cmdTmp.Prepared = true;
	cmdTmp.CommandText = '"uspRefCommunicationMethodGetList"';
	rsTmp.CacheSize = 10;
	rsTmp.CursorType = 3;
	rsTmp.CursorLocation = 3;
	rsTmp.LockType = 3;
	Recordset1.setRecordSource(rsTmp);
	Recordset1.open();
	if (thisPage.getState('pb_Recordset1') != null)
		Recordset1.setBookmark(thisPage.getState('pb_Recordset1'));
}
function _Recordset1_ctor()
{
	CreateRecordset('Recordset1', _initRecordset1, null);
}
function _Recordset1_dtor()
{
	Recordset1._preserveState();
	thisPage.setState('pb_Recordset1', Recordset1.getBookmark());
}
</SCRIPT>

<!--METADATA TYPE="DesignerControl" endspan-->

<!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=PgCntHeadBeg1 style="LEFT: 0px; TOP: 0px" 
classid=clsid:1F56DB70-9821-11D1-B7B7-00C04FD6564F dtcid="1">
							                                      </OBJECT>
-->
<!-- Begin Layout Header -->

<!--METADATA TYPE="DesignerControl" endspan-->
<DIV id="divTooltip"> </DIV>
<table border="0" align="left" cellPadding="5" cellSpacing="0">
<tr>
<td valign="top" rowspan="2" style="WIDTH: 198px" width=1>

<div class="" style="WIDTH: 198px">
<SPAN class=boldtext>All Communication Methods</SPAN>
      <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=Grid1 style="LEFT: 0px; WIDTH: 144px; TOP: 0px; HEIGHT: 147px" height=147 
	width=144 classid="clsid:277FC3F2-E90F-11D0-B767-0000F81E081D">
	<PARAM NAME="_ExtentX" VALUE="3810">
	<PARAM NAME="_ExtentY" VALUE="3889">
	<PARAM NAME="DataConnection" VALUE="">
	<PARAM NAME="SourceType" VALUE="">
	<PARAM NAME="Recordset" VALUE="Recordset1">
	<PARAM NAME="CtrlName" VALUE="Grid1">
	<PARAM NAME="UseAdvancedOnly" VALUE="0">
	<PARAM NAME="AdvAddToStyles" VALUE="-1">
	<PARAM NAME="AdvTableTag" VALUE="">
	<PARAM NAME="AdvHeaderRowTag" VALUE="">
	<PARAM NAME="AdvHeaderCellTag" VALUE="">
	<PARAM NAME="AdvDetailRowTag" VALUE="">
	<PARAM NAME="AdvDetailCellTag" VALUE="">
	<PARAM NAME="ScriptLanguage" VALUE="1">
	<PARAM NAME="ScriptingPlatform" VALUE="0">
	<PARAM NAME="EnableRowNav" VALUE="-1">
	<PARAM NAME="HiliteColor" VALUE="#e6e6fa">
	<PARAM NAME="RecNavBarHasNextButton" VALUE="-1">
	<PARAM NAME="RecNavBarHasPrevButton" VALUE="-1">
	<PARAM NAME="RecNavBarNextText" VALUE=">">
	<PARAM NAME="RecNavBarPrevText" VALUE="<">
	<PARAM NAME="ColumnsNames" VALUE='"DisplayOrder","Name","=FlagTest([EnabledFlag])"'>
	<PARAM NAME="columnIndex" VALUE="0,1,2">
	<PARAM NAME="displayWidth" VALUE="68,68,68">
	<PARAM NAME="Coltype" VALUE="1,1,1">
	<PARAM NAME="formated" VALUE="0,0,0">
	<PARAM NAME="DisplayName" VALUE='"Display<br/>Order","Name","Enabled"'>
	<PARAM NAME="DetailAlignment" VALUE=",,">
	<PARAM NAME="HeaderAlignment" VALUE=",,">
	<PARAM NAME="DetailBackColor" VALUE=",,">
	<PARAM NAME="HeaderBackColor" VALUE=",,">
	<PARAM NAME="HeaderFont" VALUE=",,">
	<PARAM NAME="HeaderFontColor" VALUE=",,">
	<PARAM NAME="HeaderFontSize" VALUE=",,">
	<PARAM NAME="HeaderFontStyle" VALUE=",,">
	<PARAM NAME="DetailFont" VALUE=",,">
	<PARAM NAME="DetailFontColor" VALUE=",,">
	<PARAM NAME="DetailFontSize" VALUE=",,">
	<PARAM NAME="DetailFontStyle" VALUE=",,">
	<PARAM NAME="ColumnCount" VALUE="3">
	<PARAM NAME="CurStyle" VALUE="Basic Navy">
	<PARAM NAME="TitleFont" VALUE="Arial">
	<PARAM NAME="titleFontSize" VALUE="-2">
	<PARAM NAME="TitleFontColor" VALUE="268435455">
	<PARAM NAME="TitleBackColor" VALUE="268435455">
	<PARAM NAME="TitleFontStyle" VALUE="1">
	<PARAM NAME="TitleAlignment" VALUE="0">
	<PARAM NAME="RowFont" VALUE="Arial">
	<PARAM NAME="RowFontColor" VALUE="268435455">
	<PARAM NAME="RowFontStyle" VALUE="0">
	<PARAM NAME="RowFontSize" VALUE="-2">
	<PARAM NAME="RowBackColor" VALUE="16775141">
	<PARAM NAME="RowAlignment" VALUE="0">
	<PARAM NAME="HighlightColor3D" VALUE="268435455">
	<PARAM NAME="ShadowColor3D" VALUE="268435455">
	<PARAM NAME="PageSize" VALUE="10">
	<PARAM NAME="MoveFirstCaption" VALUE="|<">
	<PARAM NAME="MoveLastCaption" VALUE=">|">
	<PARAM NAME="MovePrevCaption" VALUE="<<">
	<PARAM NAME="MoveNextCaption" VALUE=">>">
	<PARAM NAME="BorderSize" VALUE="1">
	<PARAM NAME="BorderColor" VALUE="13421772">
	<PARAM NAME="GridBackColor" VALUE="8421504">
	<PARAM NAME="AltRowBckgnd" VALUE="16777215">
	<PARAM NAME="CellSpacing" VALUE="0">
	<PARAM NAME="WidthSelectionMode" VALUE="1">
	<PARAM NAME="GridWidth" VALUE="144">
	<PARAM NAME="EnablePaging" VALUE="-1">
	<PARAM NAME="ShowStatus" VALUE="0">
	<PARAM NAME="StyleValue" VALUE="447469">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<!--#INCLUDE FILE="../_ScriptLibrary/Button.ASP"-->
<!--#INCLUDE FILE="../_ScriptLibrary/RSNavBar.ASP"-->
<!--#INCLUDE FILE="../_ScriptLibrary/DataGrid.ASP"-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initGrid1()
{
Grid1.pageSize = 10;
Grid1.setDataSource(Recordset1);
Grid1.tableAttributes = ' cellpadding=2  cellspacing=0 bgcolor=Gray border=1 cols=3 rules=ALL';
Grid1.headerAttributes = '   align=Left';
Grid1.headerWidth[0] = '';
Grid1.headerWidth[1] = '';
Grid1.headerWidth[2] = '';
Grid1.headerFormat = '<Font face="Arial" size=-2> <b>';
Grid1.colHeader[0] = '\'Display<br/>Order\'';
Grid1.colHeader[1] = '\'Name\'';
Grid1.colHeader[2] = '\'Enabled\'';
Grid1.rowAttributes[0] = '  bgcolor = #fff7e5 align=Left';
Grid1.rowAttributes[1] = '  bgcolor = White align=Left';
Grid1.rowFormat[0] = ' <Font face="Arial" size=-2 >';
Grid1.colAttributes[0] = ' ';
Grid1.colFormat[0] = '<Font Size=-2 Face="Arial" >';
Grid1.colData[0] = 'Recordset1.fields.getValue(\'DisplayOrder\')';
Grid1.colAttributes[1] = ' ';
Grid1.colFormat[1] = '<Font Size=-2 Face="Arial" >';
Grid1.colData[1] = 'Recordset1.fields.getValue(\'Name\')';
Grid1.colAttributes[2] = ' ';
Grid1.colFormat[2] = '<Font Size=-2 Face="Arial" >';
Grid1.colData[2] = 'FlagTest(Recordset1.fields.getValue(\'EnabledFlag\'))';
Grid1.navbarAlignment = 'Right';
var objPageNavbar = Grid1.showPageNavbar(170,1);
objPageNavbar.getButton(0).value = '|<';
objPageNavbar.getButton(1).value = '<<';
objPageNavbar.getButton(2).value = '>>';
objPageNavbar.getButton(3).value = '>|';
Grid1.hasPageNumber = false;
Grid1.hiliteAttributes = ' bgcolor=#e6e6fa';
var objRecNavbar = Grid1.showRecordNavbar(40,1);
objRecNavbar.getButton(1).value = '<';
objRecNavbar.getButton(2).value = '>';
}
function _Grid1_ctor()
{
	CreateDataGrid('Grid1',_initGrid1);
}
</SCRIPT>

<%	Grid1.display %>


<!--METADATA TYPE="DesignerControl" endspan-->

</div>

<br>


    </td>
    <td valign="top" align="left" style="WIDTH: 577px; HEIGHT: 2px" width="577" height="2">
</td>

  </tr>
 <tr>
    <td width="120" valign="top" style="WIDTH: 120px"><!-- VI6.0LAYOUT = "BKASIA (TLB)"-->
      <!--METADATA TYPE="DesignerControl" startspan
<OBJECT style="LEFT: 0px; TOP: 0px" 
      classid=clsid:1F56DB71-9821-11D1-B7B7-00C04FD6564F dtcid="4">
                                                   </OBJECT>
-->
<!-- End Layout Header -->

<!--METADATA TYPE="DesignerControl" endspan-->
 
      <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=tbSysLastUpdatedDate style="LEFT: 0px; WIDTH: 78px; TOP: 0px; HEIGHT: 19px" 
	height=19 width=78 classid="clsid:B5F0E469-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="2064">
	<PARAM NAME="_ExtentY" VALUE="503">
	<PARAM NAME="id" VALUE="tbSysLastUpdatedDate">
	<PARAM NAME="ControlType" VALUE="0">
	<PARAM NAME="Lines" VALUE="1">
	<PARAM NAME="DataSource" VALUE="Recordset1">
	<PARAM NAME="DataField" VALUE="SysLastUpdatedDate">
	<PARAM NAME="Enabled" VALUE="-1">
	<PARAM NAME="Visible" VALUE="0">
	<PARAM NAME="MaxChars" VALUE="15">
	<PARAM NAME="DisplayWidth" VALUE="13">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<!--#INCLUDE FILE="../_ScriptLibrary/TextBox.ASP"-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _inittbSysLastUpdatedDate()
{
	tbSysLastUpdatedDate.setStyle(TXT_TEXTBOX);
	tbSysLastUpdatedDate.setDataSource(Recordset1);
	tbSysLastUpdatedDate.setDataField('SysLastUpdatedDate');
	tbSysLastUpdatedDate.hide();
	tbSysLastUpdatedDate.setMaxLength(15);
	tbSysLastUpdatedDate.setColumnCount(13);
}
function _tbSysLastUpdatedDate_ctor()
{
	CreateTextbox('tbSysLastUpdatedDate', _inittbSysLastUpdatedDate, null);
}
</script>
<% tbSysLastUpdatedDate.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
 
<div class="">
<table class="fieldlabel" cols="3" border="0" cellPadding="0" cellSpacing="0" valign="top">
<tr>
<th colSpan="6" align="middle" vAlign=top>
            <P align=center>Shop Location -- Communication Methods<BR>
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=btnUpdate style="LEFT: 0px; WIDTH: 67px; TOP: 0px; HEIGHT: 27px" height=27 
	width=67 classid="clsid:B6FC3A14-F837-11D0-9CC8-006008058731" dtcid="31">
	<PARAM NAME="_ExtentX" VALUE="1773">
	<PARAM NAME="_ExtentY" VALUE="714">
	<PARAM NAME="id" VALUE="btnUpdate">
	<PARAM NAME="Caption" VALUE="Update">
	<PARAM NAME="Image" VALUE="">
	<PARAM NAME="AltText" VALUE="">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initbtnUpdate()
{
	btnUpdate.value = 'Update';
	btnUpdate.setStyle(0);
}
function _btnUpdate_ctor()
{
	CreateButton('btnUpdate', _initbtnUpdate, null);
}
</script>
<% btnUpdate.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=btnAdd 
            style="LEFT: 0px; WIDTH: 44px; TOP: 0px; HEIGHT: 27px" height=27 
            width=44 classid=clsid:B6FC3A14-F837-11D0-9CC8-006008058731 
            dtcid="29"><PARAM NAME="_ExtentX" VALUE="1164"><PARAM NAME="_ExtentY" VALUE="714"><PARAM NAME="id" VALUE="btnAdd"><PARAM NAME="Caption" VALUE="Add"><PARAM NAME="Image" VALUE=""><PARAM NAME="AltText" VALUE=""><PARAM NAME="Visible" VALUE="-1"><PARAM NAME="Platform" VALUE="0"><PARAM NAME="LocalPath" VALUE="../">
                          
            </OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initbtnAdd()
{
	btnAdd.value = 'Add';
	btnAdd.setStyle(0);
}
function _btnAdd_ctor()
{
	CreateButton('btnAdd', _initbtnAdd, null);
}
</script>
<% btnAdd.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=btnCanel 
            style="LEFT: 0px; WIDTH: 64px; TOP: 0px; HEIGHT: 27px" height=27 
            width=64 classid=clsid:B6FC3A14-F837-11D0-9CC8-006008058731 
            dtcid="29"><PARAM NAME="_ExtentX" VALUE="1693"><PARAM NAME="_ExtentY" VALUE="714"><PARAM NAME="id" VALUE="btnCanel"><PARAM NAME="Caption" VALUE="Cancel"><PARAM NAME="Image" VALUE=""><PARAM NAME="AltText" VALUE=""><PARAM NAME="Visible" VALUE="-1"><PARAM NAME="Platform" VALUE="0"><PARAM NAME="LocalPath" VALUE="../">
                                    									      </OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initbtnCanel()
{
	btnCanel.value = 'Cancel';
	btnCanel.setStyle(0);
}
function _btnCanel_ctor()
{
	CreateButton('btnCanel', _initbtnCanel, null);
}
</script>
<% btnCanel.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
<!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=lblMessage style="LEFT: 0px; WIDTH: 5px; TOP: 0px; HEIGHT: 18px" height=18 
	width=5 classid="clsid:B5F0E460-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="132">
	<PARAM NAME="_ExtentY" VALUE="476">
	<PARAM NAME="id" VALUE="lblMessage">
	<PARAM NAME="DataSource" VALUE="">
	<PARAM NAME="DataField" VALUE="">
	<PARAM NAME="FontFace" VALUE="Arial">
	<PARAM NAME="FontSize" VALUE="1">
	<PARAM NAME="FontColor" VALUE="Blue">
	<PARAM NAME="FontBold" VALUE="0">
	<PARAM NAME="FontItalic" VALUE="0">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="FormatAsHTML" VALUE="-1">
	<PARAM NAME="Platform" VALUE="256">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<!--#INCLUDE FILE="../_ScriptLibrary/Label.ASP"-->
<FONT FACE="Arial" SIZE="1" COLOR="Blue">
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initlblMessage()
{
	lblMessage.setDataFormatAs('html');
	lblMessage.setCaption('');
}
function _lblMessage_ctor()
{
	CreateLabel('lblMessage', _initlblMessage, null);
}
</script>
<% lblMessage.display %>
</FONT>

<!--METADATA TYPE="DesignerControl" endspan-->
</P>


</th></tr>
        <TR>
          <TD class=fieldlabel>Communication Method ID</TD>
          <TD class=fieldback align=left>
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=tbCommunicationMethodID style="LEFT: 0px; TOP: 0px" classid="clsid:B5F0E469-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="3175">
	<PARAM NAME="_ExtentY" VALUE="503">
	<PARAM NAME="id" VALUE="tbCommunicationMethodID">
	<PARAM NAME="ControlType" VALUE="0">
	<PARAM NAME="Lines" VALUE="3">
	<PARAM NAME="DataSource" VALUE="Recordset1">
	<PARAM NAME="DataField" VALUE="CommunicationMethodID">
	<PARAM NAME="Enabled" VALUE="0">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="MaxChars" VALUE="20">
	<PARAM NAME="DisplayWidth" VALUE="20">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _inittbCommunicationMethodID()
{
	tbCommunicationMethodID.setStyle(TXT_TEXTBOX);
	tbCommunicationMethodID.setDataSource(Recordset1);
	tbCommunicationMethodID.setDataField('CommunicationMethodID');
	tbCommunicationMethodID.disabled = true;
	tbCommunicationMethodID.setMaxLength(20);
	tbCommunicationMethodID.setColumnCount(20);
}
function _tbCommunicationMethodID_ctor()
{
	CreateTextbox('tbCommunicationMethodID', _inittbCommunicationMethodID, null);
}
</script>
<% tbCommunicationMethodID.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
</TD></TR>
        <TR>
           <td class="fieldlabel">Display Order </td>
          <td class="fieldback" align=left>
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=tbDisplayOrder style="LEFT: 0px; WIDTH: 30px; TOP: 0px; HEIGHT: 19px" 
	height=19 width=30 classid="clsid:B5F0E469-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="794">
	<PARAM NAME="_ExtentY" VALUE="503">
	<PARAM NAME="id" VALUE="tbDisplayOrder">
	<PARAM NAME="ControlType" VALUE="0">
	<PARAM NAME="Lines" VALUE="3">
	<PARAM NAME="DataSource" VALUE="Recordset1">
	<PARAM NAME="DataField" VALUE="DisplayOrder">
	<PARAM NAME="Enabled" VALUE="-1">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="MaxChars" VALUE="5">
	<PARAM NAME="DisplayWidth" VALUE="5">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _inittbDisplayOrder()
{
	tbDisplayOrder.setStyle(TXT_TEXTBOX);
	tbDisplayOrder.setDataSource(Recordset1);
	tbDisplayOrder.setDataField('DisplayOrder');
	tbDisplayOrder.setMaxLength(5);
	tbDisplayOrder.setColumnCount(5);
}
function _tbDisplayOrder_ctor()
{
	CreateTextbox('tbDisplayOrder', _inittbDisplayOrder, null);
}
</script>
<% tbDisplayOrder.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
</td>
</TR>
        <TR>
          <td class="fieldlabel">Name</td>
          <td class="fieldback">
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=tbName style="LEFT: 0px; WIDTH: 306px; TOP: 0px; HEIGHT: 19px" height=19 
	width=306 classid="clsid:B5F0E469-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="8096">
	<PARAM NAME="_ExtentY" VALUE="503">
	<PARAM NAME="id" VALUE="tbName">
	<PARAM NAME="ControlType" VALUE="0">
	<PARAM NAME="Lines" VALUE="1">
	<PARAM NAME="DataSource" VALUE="Recordset1">
	<PARAM NAME="DataField" VALUE="Name">
	<PARAM NAME="Enabled" VALUE="-1">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="MaxChars" VALUE="50">
	<PARAM NAME="DisplayWidth" VALUE="51">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _inittbName()
{
	tbName.setStyle(TXT_TEXTBOX);
	tbName.setDataSource(Recordset1);
	tbName.setDataField('Name');
	tbName.setMaxLength(50);
	tbName.setColumnCount(51);
}
function _tbName_ctor()
{
	CreateTextbox('tbName', _inittbName, null);
}
</script>
<% tbName.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
</td></TR>
  <tr>
    <td class="fieldlabel">
	Enabled ?
 </td>
	<td class="fieldback" colspan="2" noWrap align=left>
<!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=cbEnabledFlag style="LEFT: 0px; WIDTH: 29px; TOP: 0px; HEIGHT: 27px" height=27 
	width=29 classid="clsid:B5F0E46C-DC5F-11D0-9846-0000F8027CA0">
	<PARAM NAME="_ExtentX" VALUE="767">
	<PARAM NAME="_ExtentY" VALUE="714">
	<PARAM NAME="id" VALUE="cbEnabledFlag">
	<PARAM NAME="Caption" VALUE="">
	<PARAM NAME="DataSource" VALUE="Recordset1">
	<PARAM NAME="DataField" VALUE="EnabledFlag">
	<PARAM NAME="Enabled" VALUE="-1">
	<PARAM NAME="Visible" VALUE="-1">
	<PARAM NAME="Platform" VALUE="0">
	<PARAM NAME="LocalPath" VALUE="../"></OBJECT>
-->
<!--#INCLUDE FILE="../_ScriptLibrary/CheckBox.ASP"-->
<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
function _initcbEnabledFlag()
{
	cbEnabledFlag.setDataSource(Recordset1);
	cbEnabledFlag.setDataField('EnabledFlag');
}
function _cbEnabledFlag_ctor()
{
	CreateCheckbox('cbEnabledFlag', _initcbEnabledFlag, null);
}
</script>
<% cbEnabledFlag.display %>

<!--METADATA TYPE="DesignerControl" endspan-->
            <!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=FormManager1 style="LEFT: 0px; TOP: 0px" 
            classid=clsid:CEB04D01-0445-11D1-BB81-006097C553C8><PARAM NAME="ExtentX" VALUE="4233"><PARAM NAME="ExtentY" VALUE="609"><PARAM NAME="State" VALUE="(txtName_Unmatched=\qFormManager1\q,txtNewMode_Unmatched=\q\q,grFormMode=(Rows=2,Row1=(txtMode_Unmatched=\qNormal\q),Row2=(txtMode_Unmatched=\qAdd\q)),txtDefaultMode=\qNormal\q,grMasterMode=(Rows=7,Row1=(txtName_Unmatched=\q1\q,txtControl_Unmatched=\qbtnCanel\q,txtProperty_Unmatched=\qhide\q,txtValue_Unmatched=\q()\q),Row2=(txtName_Unmatched=\q1\q,txtControl_Unmatched=\qbtnUpdate\q,txtProperty_Unmatched=\qvalue\q,txtValue_Unmatched=\q'Update'\q),Row3=(txtName_Unmatched=\q1\q,txtControl_Unmatched=\qbtnAdd\q,txtProperty_Unmatched=\qshow\q,txtValue_Unmatched=\q()\q),Row4=(txtName_Unmatched=\q2\q,txtControl_Unmatched=\qbtnCanel\q,txtProperty_Unmatched=\qshow\q,txtValue_Unmatched=\q()\q),Row5=(txtName_Unmatched=\q2\q,txtControl_Unmatched=\qbtnUpdate\q,txtProperty_Unmatched=\qvalue\q,txtValue_Unmatched=\q'Save'\q),Row6=(txtName_Unmatched=\q2\q,txtControl_Unmatched=\qRecordset1\q,txtProperty_Unmatched=\qaddRecord\q,txtValue_Unmatched=\q()\q),Row7=(txtName_Unmatched=\q2\q,txtControl_Unmatched=\qbtnAdd\q,txtProperty_Unmatched=\qhide\q,txtValue_Unmatched=\q()\q)),grTransitions=(Rows=4,Row1=(txtCurrentMode=\qNormal\q,txtObject=\qbtnAdd\q,txtEvent=\qonclick\q,txtNextMode=\qAdd\q),Row2=(txtCurrentMode=\qNormal\q,txtObject=\qbtnUpdate\q,txtEvent=\qonclick\q,txtNextMode=\qNormal\q),Row3=(txtCurrentMode=\qAdd\q,txtObject=\qbtnUpdate\q,txtEvent=\qonclick\q,txtNextMode=\qNormal\q),Row4=(txtCurrentMode=\qAdd\q,txtObject=\qbtnCanel\q,txtEvent=\qonclick\q,txtNextMode=\qNormal\q)),grMasterStep=(Rows=5,Row1=(txtName_Unmatched=\q1\q),Row2=(txtName_Unmatched=\q2\q,txtControl_Unmatched=\q\q,txtAction_Unmatched=\qAddUpdateDelete\q,txtValue_Unmatched=\q(\q\qUpdate\q\q)\q),Row3=(txtName_Unmatched=\q3\q,txtControl_Unmatched=\q\q,txtAction_Unmatched=\qAddUpdateDelete\q,txtValue_Unmatched=\q(\q\qInsert\q\q)\q),Row4=(txtName_Unmatched=\q4\q,txtControl_Unmatched=\qRecordset1\q,txtAction_Unmatched=\qcancelUpdate\q,txtValue_Unmatched=\q()\q),Row5=(txtName_Unmatched=\q4\q,txtControl_Unmatched=\qRecordset1\q,txtAction_Unmatched=\qrequery\q,txtValue_Unmatched=\q()\q)))">
            			  </OBJECT>
-->
<SCRIPT RUNAT=SERVER LANGUAGE="JavaScript">
function _FormManager1_ctor()
{
	thisPage.advise(PAGE_ONINIT, _FormManager1_init);
}
function _FormManager1_init()
{
	if (thisPage.getState("FormManager1_formmode") == null)
		_FormManager1_SetMode("Normal");
	btnAdd.advise("onclick", "_FormManager1_btnAdd_onclick()");
	btnUpdate.advise("onclick", "_FormManager1_btnUpdate_onclick()");
	btnCanel.advise("onclick", "_FormManager1_btnCanel_onclick()");
}
function _FormManager1_SetMode(formmode)
{
	thisPage.setState("FormManager1_formmode", formmode);
	if (formmode == "Normal")
	{
		btnCanel.hide();
		btnUpdate.value = 'Update';
		btnAdd.show();
	}
	if (formmode == "Add")
	{
		btnCanel.show();
		btnUpdate.value = 'Save';
		Recordset1.addRecord();
		btnAdd.hide();
	}
}
function _FormManager1_btnAdd_onclick()
{
	if (thisPage.getState("FormManager1_formmode") == "Normal")
	{
		_FormManager1_SetMode("Add");
	}
	else _FormManager1_SetMode(thisPage.getState("FormManager1_formmode"))
}
function _FormManager1_btnUpdate_onclick()
{
	if (thisPage.getState("FormManager1_formmode") == "Normal")
	{
		AddUpdateDelete("Update");
		_FormManager1_SetMode("Normal");
	}
	else if (thisPage.getState("FormManager1_formmode") == "Add")
	{
		AddUpdateDelete("Insert");
		_FormManager1_SetMode("Normal");
	}
	else _FormManager1_SetMode(thisPage.getState("FormManager1_formmode"))
}
function _FormManager1_btnCanel_onclick()
{
	if (thisPage.getState("FormManager1_formmode") == "Add")
	{
		Recordset1.cancelUpdate();
		Recordset1.requery();
		_FormManager1_SetMode("Normal");
	}
	else _FormManager1_SetMode(thisPage.getState("FormManager1_formmode"))
}
</SCRIPT>


<!--METADATA TYPE="DesignerControl" endspan-->

</td>
	
<tr></tr>
</table></div>
      <!--METADATA TYPE="DesignerControl" startspan
<OBJECT style="LEFT: 0px; TOP: 0px" 
      classid=clsid:1F56DB72-9821-11D1-B7B7-00C04FD6564F dtcid="5">
                                                                                 
                                                         </OBJECT>
-->
<!-- Begin Layout Footer -->

<!--METADATA TYPE="DesignerControl" endspan-->
    </td>
  </tr>
</table>
<!--METADATA TYPE="DesignerControl" startspan
<OBJECT id=LayoutDTC1 style="LEFT: 0px; TOP: 0px" 
classid=clsid:1F56DB73-9821-11D1-B7B7-00C04FD6564F dtcid="7">
                        
</OBJECT>
-->
<!-- End Layout Footer -->

<!--METADATA TYPE="DesignerControl" endspan-->
</body>
<% ' VI 6.0 Scripting Object Model Enabled %>
<% EndPageProcessing() %>
</FORM>
</html>
