<%@LANGUAGE="VBSCRIPT"%>
<%
  Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
  Dim objData
  dim strHTML, strAct, LoginUserID

  strAct = request("act")
  //response.write "<html><head></head><body><h1>" & strAct & "</h1></body></html>"
  //response.end
        
  ' Get the logged in user
  LoginUserID = GetSession("UserID")
  CheckError()

  ' initiate PartnerDataMgr
  set objData = CreateObject("LAPDPartnerDataMgr.CExecute")
  CheckError()
    
  if strAct = "fix" then
    strHTML = objData.FixUnprocessedPartnerData(request("data"), Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, LoginUserID)
  else
    strHTML = objData.GetUnprocessedPartnerData(Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, LoginUserID)
  end if
    
    
  'Processing Done, destroy PartnerDataMgr
  set objData = Nothing

  Call KillSessionObject

  On Error Goto 0

  response.write strHTML
  
%>
