<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
	Dim strLynxID
	Dim strUserID
	Dim strHTML

    On Error Resume Next

	strUserID = GetSession("UserID")

	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	' Place claim information in session manager.
	strHTML = objExe.ExecuteSpAsXML("uspRefNoteTypeGetXML", "refNoteType.xsl")
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>

<script language="javascript">
    var curUser = <%=strUserID%>;
</script>
