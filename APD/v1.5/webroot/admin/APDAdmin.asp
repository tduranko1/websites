<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
    On Error Resume Next

	Dim objData
    Dim rsCRUD
    Dim bReadFlag

    Call GetData

    Set objData = Nothing

    If Not rsCRUD Is Nothing Then
        If rsCRUD.State = 1 Then rsCRUD.Close   ' 1 = adStateOpen
        Set rsCRUD = Nothing
    End If

    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetData()

        ' Initiate DataAccessor
        Set objData = CreateObject("DataPresenter.CExecute")

        objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH")

        ' Get CRUD
        Set rsCRUD = objData.ExecuteSpAsRS( "dbo.uspCRUDGetDetail", "'Reference'", GetSession("UserID") )

        bReadFlag = rsCRUD.Fields.Item("ReadFlag").Value

    End Sub

    ' Check to see if reads are allowed, if not, error out and quit
    If (bReadFlag = False) Then
        ' User has no permissions
%>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
            <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        </head>
        <body class="bodyAPDSub" unselectable="on" bgcolor="#FFFFFF">
        <center>
            <font color="#ff0000"><strong>You do not have sufficient permission to view this page.
            <br/>Please contact administrator for permissions.</strong></font>
        </center>
        </body>
        </html>
<%
        Response.End
    End If



%>
<html>
<head>
	<title>APD Administration</title>

    <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
    <LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>

    <STYLE type="text/css">
        A {color:black; text-decoration:none;}
    </STYLE>

    <SCRIPT language="JavaScript" src="/js/formValid.js"></SCRIPT>

    <SCRIPT language="JavaScript">
        var currentTabRow = 1;
        var curTabID = "";
        var tabArray = null;
        var tabsHIndend = 10;
        var tabsVIndend = 18;

        function InitTabs(){
            //top.closeClaim();
            var tmpTabsArray = document.all["tabsInTabs"].childNodes;
            tabArray = new Array();
            var i;
            for (i = 0; i < tmpTabsArray.length; i++)
                if (tmpTabsArray[i].tagName == "DIV")
                    tabArray.push(tmpTabsArray[i]);

            for (i = tabArray.length - 1; i > -1; i--){
                tabArray[i].style.top = (2 + tabsVIndend * (tabArray.length - i - 1)) + "px";
                tabArray[i].style.left = (3 + tabsHIndend * (i)) + "px";
                tabArray[i].style.visibility = "visible";
            }
            frmAPDAdmin.frameElement.style.top = (4 + tabsVIndend * tabArray.length) + "px";
            frmAPDAdmin.frameElement.style.left = "2px";
            frmAPDAdmin.frameElement.style.height = 510 - (4 + tabsVIndend * tabArray.length) + "px";
            frmAPDAdmin.frameElement.style.width = "99%";
            frmAPDAdmin.frameElement.style.visibility = "visible";

            var firstTab = tabArray[0].getElementsByTagName("SPAN")[1];
            if (firstTab)
                changePage(firstTab);
        }

        function changeTabRow(obj){
            var idx = -1;
            var i;
            var newtabArray = new Array();

            for (i = 0; i < tabArray.length; i++){
                if (tabArray[i] == obj) {
                    idx = i;
                    break;
                }
            }
            if (idx == 0) return;
            if (idx > -1){
                for (i = idx; i < tabArray.length; i++){
                    newtabArray.push(tabArray[i]);
                }
                for (i = 0; i < idx; i++){
                    newtabArray.push(tabArray[i]);
                }
                tabArray = newtabArray;
            }
            for (var i = tabArray.length - 1; i > -1; i--){
                tabArray[i].style.top = (2 + tabsVIndend * (tabArray.length - i - 1)) + "px";
                tabArray[i].style.left = (3 + tabsHIndend * (i)) + "px";
            }
        }

        function hoverTab(obj){
            if (obj.id != curTabID)
                obj.className = "tabhover1";
        }

        function hoverOff(obj){
            if (obj.id != curTabID)
                obj.className = "tab1";
        }

        function resetTabs(){
            var tabs, i, j;
            for (i = 0; i < tabArray.length; i++) {
                tabs = tabArray[i].getElementsByTagName("SPAN");
                for (j = 1; j < tabs.length; j++)
                    tabs[j].className = "tab1";
            }
        }

        function changePage(obj){
            if (curTabID == obj.id) return;
            if (curTabID != ""){
                try {
                    if (frmAPDAdmin.gbDirty) {
                        var oTab = document.all[curTabID];
                        if (oTab) {
                            var sSave = YesNoMessage("Need to Save", "Some Information on the " + oTab.innerText + " tab has changed. \nDo you want to save the changes?.");
                            if (sSave == "Yes"){
                                if (!frmAPDAdmin.btnSave()) return;
                            }
                        }
                    }
                }
                catch (e) {}
            }
            resetTabs();
            obj.className = "tabactive1";
            curTabID = obj.id;

            var pg = obj.getAttribute("tabPage");
            if (pg == null || pg == "")
                pg = "/blank.asp";

            frmAPDAdmin.frameElement.src = pg;
        }

        function checkCorrectPage(sURL){
            if (curTabID){
                var obj = document.all[curTabID];
                var pg = obj.getAttribute("tabPage");
                if (sURL){
                    var sTmp = sURL.toLowerCase();
                    if(sTmp.indexOf(pg.toLowerCase()) == -1){
                        //the tab and the page displayed in the IFrame mismatch
                        resetTabs();
                        var tabs, i, j;
                        for (i = 0; i < tabArray.length; i++) {
                            tabs = tabArray[i].getElementsByTagName("SPAN");
                            for (j = 1; j < tabs.length; j++){
                                var pg1 = tabs[j].getAttribute("tabPage");
                                if (pg1){
                                    pg1 = pg1.toLowerCase();
                                    if(sTmp.indexOf(pg1.toLowerCase()) != -1){
                                        changePage(tabs[j]);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    </SCRIPT>

</head>

<body class="bodyAPDSub" style="background:transparent;overflow:hidden" unselectable="on" onload="InitTabs();" tabIndex="-1"  margin="0" topmargin="0" leftmargin="0" >
<!--
    Instructions on adding a new tab/new tab row
    To add a new tab:
        1. copy a span element (for example: tab11) defined in one of the tab rows and changed attibutes as needed. The tabPage attribute
           defines the page you want to load into the iFrame.
        2. change the id of the tab. This does not affect the tab functioning but it will be a good practice to have
           consequtive tab numbers.
        3. If you want the saving of changes, when you click another tab, to work - define the save functionality routine
           as btnSave(). This function will return false, if you do not want to navigate to the tab being clicked.
    To add a new tab row: (Tab row contains a set of tabs)
        1. copy a DIV section (for example: tab1) and change/remove all the span elements inside as needed.
        Note: the first tab (example: tab10) is a dummy tab. This will give a small indendation on the left
              of the tab row. It is recommended that you copy the "tab10" span element to the new
              tab row and change the id to tab<row number>0.

-->
<DIV unselectable="on" style="position:absolute; z-index:1; top: 0px; left: 0px;" name="tabsInTabs" id="tabsInTabs">
    <DIV id="tabs1" unselectable="on" class="apdtabs" style="position:absolute; visibility: hidden;" onclick="changeTabRow(this)">
        <SPAN id="tab10" class="" unselectable="on" tabPage="" style="width:0px;">&nbsp;</SPAN>
        <SPAN id="tab11" class="tab1" unselectable="on" tabPage="/admin/RefDocumentSource.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Document Source</SPAN>
        <SPAN id="tab12" class="tab1" unselectable="on" tabPage="/admin/RefDocumentType.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Document Type</SPAN>
        <SPAN id="tab13" class="tab1" unselectable="on" tabPage="/admin/RefNoteType.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Note Type</SPAN>
        <SPAN id="tab14" class="tab1" unselectable="on" tabPage="/admin/RefVehicleMake.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Vehicle Makes</SPAN>
        <SPAN id="tab15" class="tab1" unselectable="on" tabPage="/admin/RefInsurance.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Insurance Companies</SPAN>
    </DIV>

    <!-- <DIV id="tabs2" unselectable="on" class="apdtabs" style="position:absolute; visibility: hidden;" onclick="changeTabRow(this)">
        <SPAN id="tab20" class="" unselectable="on" tabPage="" style="width:0px;">&nbsp;</SPAN>
        <SPAN id="tab21" class="tab1" unselectable="on" tabPage="/admin/RefProgramSponsor.asp" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Insurance Companies</SPAN>
        <SPAN id="tab22" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Involved Type</SPAN>
        <SPAN id="tab23" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Loss Type</SPAN>
        <SPAN id="tab24" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Note Type</SPAN>
        <SPAN id="tab25" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Person Location</SPAN>
        <SPAN id="tab26" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Person Relation</SPAN>
        <SPAN id="tab27" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Phone</SPAN>
    </DIV>

    <DIV id="tabs3" unselectable="on" class="apdtabs" style="position:absolute; visibility: hidden;" onclick="changeTabRow(this)">
        <SPAN id="tab30" class="" unselectable="on" tabPage="" style="width:0px;">&nbsp;</SPAN>
        <SPAN id="tab31" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Road Location</SPAN>
        <SPAN id="tab32" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Road Type</SPAN>
        <SPAN id="tab33" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Safety Devices</SPAN>
        <SPAN id="tab34" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">States</SPAN>
        <SPAN id="tab35" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Time Zones</SPAN>
        <SPAN id="tab36" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Weather Condition</SPAN>
        <SPAN id="tab37" class="tab1" unselectable="on" tabPage="" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)" onclick="changePage(this)">Zip Codes</SPAN>
    </DIV> -->

</DIV>
<IFRAME name="frmAPDAdmin" id="frmAPDAdmin" SRC="/blank.asp" style="visibility:hidden;border:1px solid black;position:absolute;width:100%;">
</IFRAME>
</body>
</html>
