<%@LANGUAGE="VBSCRIPT"%>
<%
    Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
    Dim objData
    Dim strHTML

    Dim LoginUserID

    ' Get the logged in user
    LoginUserID = GetSession("UserID")
    CheckError()

    if Request.QueryString.Count = 0 then
        ' No parameters were passed.  Invalid call
        err.Raise 65535, "User Defined Error", "RoleID Paramaeter not passed"
    end if
    CheckError()

    if Not IsNumeric(Request("RoleID")) then
        ' Invalid Role ID
        err.Raise 65535, "User Defined Error", "RoleID Parameter Invalid"
    end if
    CheckError()

    ' Initiate DataPresenter
    set objData = CreateObject("DataPresenter.CExecute")
    CheckError()

    objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH"), sSessionKey, LoginUserID
    CheckError()

    objData.AddXslParam "LoginUserID", LoginUserID
    CheckError()
    
    strHTML = objData.ExecuteSpAsXML("uspAdmRoleGetDetailXML", "RoleDetail.xsl", Request("RoleID"))

    set objData = Nothing

    Call KillSessionObject

    On Error Goto 0

    response.write strHTML
%>
