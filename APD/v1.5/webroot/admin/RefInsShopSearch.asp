<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1
   'Server.scriptTimeout = 60
%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<html xmlns:IE>
<head>
    <LINK rel="stylesheet" href="/css/APDControls.css" type="text/css"/>
    <LINK rel="stylesheet" href="/css/APDGrid.css" type="text/css"/>
    <STYLE type="text/css">
        A {color:black; text-decoration:none;}
    </STYLE>
	 <script type="text/javascript" language="JavaScript"  src="/js/apdcontrols.js"></script> 		
	 <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
    </script>
    <script language="Javascript">
      function pageInit(){
        if (document.all["searchProgress"]) { // coming in after the search.
          if (!(document.all["xmlSearchResult"])) {
            searchProgress.innerText = "Search resulted in 0 or a large number of hits. Please refine your search and try again.";
            searchProgress.style.display = "inline";
            searchProgress.style.color = "#FF0000";
            divResult.firstChild.nextSibling.style.display = "none";
            divResult.firstChild.nextSibling.nextSibling.style.display = "none";
            divResult.firstChild.cells[0].style.display = "none";
            btnNewSearch.onButtonClick = goBack;
          } else {
            searchProgress.style.display = "none";          
          }
        }
      }
      
      function goBack(){
        window.history.back();
      }
    </script>
</head>
<BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden" tabIndex="-1" topmargin="0"  leftmargin="0" onload="pageInit()">

<%
    Dim strInsID
    Dim strUserID
    Dim strHTML

    Dim strShopName, strShopCity, strShopState, strShopTypeCode, strInsuranceCoID
    Dim strUseCEI
    Dim bDoSearch


    On Error Resume Next

    strUserID = GetSession("UserID")

    'Get values from the query sring
    strShopName = Request("ShopName")
    strShopCity = Request("ShopCity")
    strShopState = Request("ShopState")
    strShopTypeCode = Request("ShopTypeCode")
    strInsuranceCoID = Request("InsuranceCompanyID")
    strUseCEI = Request.QueryString("UseCEI")

    if (strInsuranceCoID = "") or (not isNumeric(strInsuranceCoID)) then
        response.write "Insurance ID is required."
        response.end
    end if
    
    if  strShopName = "" and _
        strShopCity = "" and _
        strShopState = "" then
        bDoSearch = false
%>
    <table border="0" cellspacing="0" cellpadding="1" style="width:100%;border-collapse:collapse;border:1px solid gray">
        <tr style="height:18px;">
            <td colspan="3" align="middle" class="TableSortHeader" style="cursor:default" unselectable="on"><strong unselectable="on">Shop Search</strong></td>
        </tr>
        <tr style="height:23px;">
            <td unselectable="on">Name</td>
            <td unselectable="on">
              <IE:APDInput id="txtSearchName" name="txtSearchName" value="" size="40" maxLength="50" canDirty="false" CCTabIndex="26" />
            </td>
            <td unselectable="on"></td>
        </tr>
        <tr style="height:21px;">
            <td unselectable="on">City</td>
            <td unselectable="on">
              <IE:APDInput id="txtSearchCity" name="txtSearchCity" value="" size="32" maxLength="30" canDirty="false" CCTabIndex="27" />
              <!-- <input type="text" name="txtSearchCity" id="txtSearchCity" class="InputField" size="32" maxLength="30"  tabIndex="27"/> -->
            </td>
            <td unselectable="on"></td>
        </tr>
        <tr style="height:21px;">
            <td unselectable="on">State</td>
            <td unselectable="on" name="tdState" id="tdState">
              <IE:APDCustomSelect id="State" name="State" value="" displayCount="9" blankFirst="true" canDirty="false" CCTabIndex="28">
                    <script language="javascript">
                        var sStateCodes = parent.parent.sStateCodes;
                        var sStates = parent.parent.sStates;
                        if (sStateCodes != "" && sStates != "") {
                          var aCodes = sStateCodes.split("|");
                          var aCaptions = sStates.split("|");
                          for (var i = 0; i < aCaptions.length; i++)
                            document.write("<IE:dropDownItem value=\"" + aCodes[i] + "\" style=\"display:none\" >" + aCaptions[i] + "</IE:dropDownItem>");
                        }

                    </script>
                </IE:APDCustomSelect>
            </td>
            <td align="right" unselectable="on">
              <IE:APDButton id="btnSearch1" name="btnSearch1" value="Search" CCTabIndex="29" onButtonClick="parent.btnShopSearchClick()"/>
            </td>
        </tr>
    </table>
    <span id="divSearchStatus" name="divSearchStatus" style="padding:10px;font:11px Tahoma, Arial, Verdana;display:none">Searching. Please wait ...</span>
    <!--<DIV name="divSearchStatus" id="divSearchStatus" style="padding:10px;" unselectable="on">
    </DIV>-->

<%
        'strShopTypeCode = vbNull
    else
        bDoSearch = true
    end if



    if bDoSearch then
%>
    <script language="JavaScript">
        var recCount;
        var curPage;
        var pgCount;
        
        function navMoveFirst(){
          if (tblSearch) {
            tblSearch.firstPage();
            curPage = 1;
            pg.innerText = "Page: " + curPage + " / " + pgCount;
          }
        }

        function navMovePrev(){
          if (tblSearch) {
            if (curPage > 1) {
                tblSearch.previousPage();
                curPage--;
                pg.innerText = "Page: " + curPage + " / " + pgCount;
            }
          }
        }

        function navMoveNext(){
          if (tblSearch) {
            if (curPage < pgCount) {
                tblSearch.nextPage();
                curPage++;
                pg.innerText = "Page: " + curPage + " / " + pgCount;
            }
          }
        }

        function navMoveLast(){
          if (tblSearch) {
            tblSearch.lastPage();
            curPage = pgCount;
            pg.innerText = "Page: " + curPage + " / " + pgCount;
          }
        }
        

        function showResult(){
            var sPageSize = tblSearch.getAttribute("DATAPAGESIZE");
            if (sPageSize != "")
              iPageSize = parseInt(sPageSize, 10);
            
            if (iPageSize < 1)
              iPageSize = 8; //default 8 records at a time

            recCount = xmlSearchResult.XMLDocument.firstChild.childNodes.length;
            curPage = 1;
            pgCount = Math.ceil(recCount / iPageSize);
            if (pgCount == 0) {
                curPage = 0;
                pg.innerText = "";
                tblSearch.style.display = "none";
                disableButtons(true);
            }
            else {
                pg.innerText = "Page: " + curPage + " / " + pgCount;
                disableButtons(false);
            }
        }

        function showProgress(){
            searchProgress.innerText += ".";
        }
        
        function disableButtons(val){
            btnNavFirst.CCDisabled = val;
            btnNavPrev.CCDisabled = val;
            btnNavNext.CCDisabled = val;
            btnNavLast.CCDisabled = val;
        }
        
    </script>
    <span id="searchProgress" name="searchProgress" style="font:11px Tahoma, Arial, Verdana;display:none">Searching. Please wait ...</span>
    <DIV id="divResult" name="divResult">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%;" unselectable="on">
          <colgroup>
              <col width="120px"/>
              <col width="140px"/>
              <col width="*"/>
          </colgroup>
          <tr valign="bottom">
            <td>
              <IE:APDButton id="btnNavFirst" name="btnNavFirst" value="&#x8b;&#x8b;" CCDisabled="true" onButtonClick="navMoveFirst()"/>
              <IE:APDButton id="btnNavPrev" name="btnNavPrev" value="&#x8b;" CCDisabled="true" onButtonClick="navMovePrev()"/>
              <IE:APDButton id="btnNavNext" name="btnNavNext" value="&#x9b;" CCDisabled="true" onButtonClick="navMoveNext()"/>
              <IE:APDButton id="btnNavLast" name="btnNavLast" value="&#x9b;&#x9b;" CCDisabled="true" onButtonClick="navMoveLast()"/>
            </td>
            <td>
              <span id="pg" name="pg" unselectable="on">
              </span>
            </td>
            <td>
              <IE:APDButton id="btnNewSearch" name="btnNewSearch" width="80" value="New Search" onButtonClick="parent.doShopSearch()"/>
            </td>
          </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="2" style="width:100%;" unselectable="on">
            <colgroup>
                <col width="21px"/>
                <col width="21px"/>
                <col width="160px"/>
                <col width="106px"/>
                <col width="*"/>
            </colgroup>
            <tr style="height:21px;cursor:hand;">
                <td unselectable="on" class="TableSortHeader" title="Select/Deselect All current page"><!-- onclick="parent.selectAll(this)"  -->
                        Exc
                </td>
                <td unselectable="on" class="TableSortHeader" title="Select/Deselect All current page"><!-- onclick="parent.selectAll(this)"  -->
                        Inc
                </td>
                <td unselectable="on" class="TableSortHeader">Name</td>
                <td unselectable="on" class="TableSortHeader">City, State</td>
                <td unselectable="on" class="TableSortHeader">Prgm</td>
            </tr>
        </table>
        <DIV unselectable="on" class="autoflowTable" style="width:100%; height:171px;">
            <table border="0" cellspacing="1" cellpadding="2" style="table-layout:fixed;" name="tblSearch" id="tblSearch" datasrc="#xmlSearchResult" DATAPAGESIZE="8">
                <colgroup>
                    <col width="25px"/>
                    <col width="26px"/>
                    <col width="155px"/>
                    <col width="103px"/>
                    <col width="*"/>
                </colgroup>
                <tr style="height:20px">
                    <td unselectable="on" class="GridTypeTD" nowrap="" >
                        <IE:APDCheckBox canDirty="false" onBeforeChange="parent.addExclusion(this); event.returnValue=false"/>
                    </td>
                    <td unselectable="on" class="GridTypeTD" nowrap="" >
                        <IE:APDCheckBox canDirty="false" onBeforeChange="parent.addInclusion(this); event.returnValue=false" />
                    </td>
                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;">&nbsp;<span datafld="Name" DATAFORMATAS="html" style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;"></span></td>
                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;">
                      <span style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;padding:0px;">
                        <span datafld="AddressCity" DATAFORMATAS="html"></span>, <span datafld="AddressState" DATAFORMATAS="html"></span>
                      </span>
                    </td>
                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:center;">&nbsp;<span datafld="CEI_PS" DATAFORMATAS="html" unselectable="on"></span></td>
                    <td style="display:none"><div datafld="EntityID"></div></td>
                </tr>
            </table>
        </DIV>
    </DIV>
<%
        Dim objExe
        Set objExe = CreateObject("DataPresenter.CExecute")
        CheckError()

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
        CheckError()

        if strShopName = "'" then strShopName = ""
        if strShopCity = "'" then strShopCity = ""

        dim rsSearchResult, i

        'set rsSearchResult = objExe.ExecuteSpAsRS("uspAdmShopSearchList", "", strShopState, "", strShopCity, strShopName, "", "", "", "")
        rsSearchResult = objExe.ExecuteSpAsXML("uspSMTEntitySearchGetListXML", "", "S", "", strShopCity, strShopState, "", "", strShopName, "", "", "", "1", "", "", "", strUseCEI)
        CheckError()
        
        response.write "<XML ID='xmlSearchResult' ondatasetcomplete='showResult()' ondataavailable='showProgress()'>" + vbcrlf
        response.write replace(rsSearchResult, "SearchType=""S"">", ">")
        response.write "</XML>" + vbcrlf

        CheckError()

        Set objExe = Nothing

%>
<%
    end if

    Call KillSessionObject

    On Error GoTo 0
%>
</body>
</html>
