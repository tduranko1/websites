<%@LANGUAGE="VBSCRIPT"%>
<%
    Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
    Dim objData
    Dim rsCRUD

    Dim LoginUserID
    Dim bInsertMode
    Dim strHTML
    dim strRequest

    ' Get the logged in user
    LoginUserID = GetSession("UserID")
    if Err.Number <> 0 then
        ErrorHandler()
        Response.End
    end if

    if Request.QueryString.Count = 0 then
        ' No parameters were passed.  Invalid call
        err.Raise 65535, "User Defined Error", "UserID Paramaeter not passed"
        ErrorHandler()
        Response.End
    end if

    if Request("UserID") = "New" then
        bInsertMode = true
    else
        if Not IsNumeric(Request("UserID")) then
            ' Invalid User ID
            err.Raise 65535, "User Defined Error", "UserID Parameter Invalid"
            ErrorHandler()
            Response.End
        end if
        bInsertMode = false
    end if

    ' Initiate DataAccessor
    set objData = CreateObject("DataPresenter.CExecute")
    if Err.Number <> 0 then
        ErrorHandler()
        Response.End
    end if

    objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH"), sSessionKey, LoginUserID
    if Err.Number <> 0 then
        ErrorHandler()
        Response.End
    end if

    objData.AddXslParam "InsertMode", LCase(CStr(bInsertMode))
    CheckError()
    objData.AddXslParam "LoginUserID", LoginUserID
    CheckError()

    if Request("UserID") <> "New" then
	    strHTML = objData.ExecuteSpAsXML("uspWorkAssignmentGetDetailXML", "APDUserDetails.xsl", Request("UserID"))
    else
	    strHTML = objData.ExecuteSpAsXML("uspWorkAssignmentGetDetailXML", "APDUserDetails.xsl", "-1")
    end if

    'Processing Done, destroy DataAccessor
    set objData = Nothing

    Call KillSessionObject

    On Error Goto 0

    response.write strHTML

%>
