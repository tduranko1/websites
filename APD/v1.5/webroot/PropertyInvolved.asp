<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
	Dim strUserID, strLynxID, strInvolvedID, strPropNum
	Dim strInsuranceCompanyID
	Dim strHTML, objExe

	On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = GetSession("LynxID")
	strPropNum = GetSession("PropertyNumber")
	strInvolvedID = Request("InvolvedID")

	'strLynxID = 2600
	'strInvolvedID = 34

	CheckError()

	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	CheckError()

	objExe.Trace "", "PropertyInvolved.asp"

	'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
	objExe.AddXslParam "PropertyNumber", strPropNum
	CheckError()

    objExe.AddXslParam "LynxId", strLynxID
	CheckError()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspPropertyInvolvedGetDetailXML", "PropertyInvolved.xsl", strInvolvedID, strInsuranceCompanyID )
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>
