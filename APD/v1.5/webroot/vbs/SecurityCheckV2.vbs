<%
'=========================================================================
'| SessionManagerV2.vbs
'|
'| This file is included to add a security check to an ASP.
'| It ensures that the user has a valid session within the application.
'|
'| You must include SessionManagerV2.vbs ahead of this file to use it.
'| You must include errorhandler.asp ahead of this file as well.
'=========================================================================

On Error Resume Next

'Make sure the the user has a valid session state with the application.
Call CheckSession()

'Pop up an error dialog if there were problems.
Call CheckError()

On Error GoTo 0

Sub CheckSession()

    'sSessionKey is from SessionManagerV2.vbs.
    'If it does not yet have a value then try to retrieve it.
    If Not HasValue( sSessionKey ) Then
        ReadSessionKey
    End If

    'If we do not have a session key now then there is a problem.
    If Not HasValue( sSessionKey ) Then

        'Set up the error parameters.
        Err.Raise ServerSideEvent() + 51, "SecurityCheckV2.vbs", _
            Request.ServerVariables("LOGON_USER")

    'Since we have a key now we check to see that the key is valid.
    'This ensures that no one can use an old key to hack the app.
    ElseIf Not CBool( SessionExists() ) Then

        'Clear the session key as it is bad.
        sSessionKey = ""
        WriteSessionKey()

        'Set up the error parameters.
        Err.Raise ServerSideEvent() + 50, "SecurityCheckV2.vbs", _
            Request.ServerVariables("LOGON_USER") & "|" & sSessionKey

    End If

End Sub

%>
