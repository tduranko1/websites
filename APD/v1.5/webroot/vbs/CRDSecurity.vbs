<%

dim bViewCRD, sErrMsg
  
  bViewCRD = GetSession("ViewCRD")
  CheckError()
  
  if bViewCRD <> 1 then
  	sErrMsg = "<table border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;'>"
	sErrMsg = sErrMsg & "<tr><td align='center' style='color:red; font-weight:bolder;'>"
    sErrMsg = sErrMsg & "You do not have permission to view this page.  Close the browser and attempt to reenter the"
    sErrMsg = sErrMsg & " application using the desktop icon or contact your supervisor."
	sErrMsg = sErrMsg & "</td></tr></table>"
	
  	response.write sErrMsg
	response.end
end if
  
%>