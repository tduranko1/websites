<%
'=========================================================================
'| SessionManagerV2.vbs
'|
'| This file is included to add session managment tools to an ASP.
'|
'=========================================================================

'------------------------------------------
' Constants
'------------------------------------------
Const gsCookie = "APDSession"

'------------------------------------------
' 'global' Variables
'------------------------------------------
Dim oSession    'non transaction object
Dim sSessionKey 'the session handle
Dim sWindowID   'the window identifier
Dim strCsrID, strApdUserID  'user information

'------------------------------------------
' Methods
'------------------------------------------

'Creates a new session manager object in oSession.
Sub CreateSessionObject()
    On Error Resume Next
    If oSession Is Nothing Then
        Set oSession = CreateObject("SessionManager.CSession")
    End If
    On Error Goto 0

    If oSession Is Nothing Then
        Err.Raise ServerSideEvent(), "SessionManagerV2.vbs", "Could not create SessionManager COM component."
    End If
End Sub

'Kills the session manager object in oSession.
Sub KillSessionObject()
    Set oSession = Nothing
End Sub

'Utility function for checking whether a variant has a value.
Function HasValue( vValue )
    If CBool( IsNull( vValue ) ) Then
        HasValue = False
    ElseIf CBool( Len( vValue ) = 0) Then
        HasValue = False
    Else
        HasValue = True
    End If
End Function

'Creates a new session.  Returns the new session key.
Sub CreateSession( sUserID )
    Call CreateSessionObject

    sSessionKey = oSession.CreateSession( sUserID )

    If Not HasValue( sSessionKey ) Then
        Err.Raise ServerSideEvent(), "SessionManagerV2.vbs CreateSession()", _
            "A blank user SessionKey was returned from SessionMgr.CreateSession() for CSR '" & strCsrID & "' APD user '" & strApdUserID & "'."
    End If
End Sub

'Check to see if the current session exists
Function SessionExists()
    SessionExists = False
    If HasValue( sSessionKey ) Then
        Call CreateSessionObject
        SessionExists = oSession.Exists( sSessionKey )
    End If
End Function

'Updates a session variable value.
Sub UpdateSession( sVariableName, vValue )
    Call ReadSessionKey
    Call CreateSessionObject

    Dim strValue
    strValue = CStr( vValue )
    strValue = Replace( strvalue, "'", "''" ) 'SQL escaping

    oSession.UpdateSession sSessionKey, sVariableName & "-" & sWindowID, strValue
End Sub

'Returns a session variable value.
Function GetSession( sVariableName )
    Call ReadSessionKey
    Call CreateSessionObject

    GetSession = oSession.GetValue( sSessionKey, sVariableName & "-" & sWindowID )
End Function

'Reads the window specific session key cookie (if we don't have it already).
Sub ReadSessionKey()
    Call ReadWindowID
    If Not HasValue( sSessionKey ) Then
        sSessionKey = Request.Cookies(gsCookie)
        If Not HasValue( sSessionKey ) Then
            Err.Raise ServerSideEvent(), "SessionManagerV2.vbs ReadSessionKey()", _
                "A blank user SessionKey was returned from the cookie for CSR '" & strCsrID & "' APD user '" & strApdUserID & "' WindowID '" & sWindowID & "' SessionKey '" & sSessionKey & "' gsCookie '" & gsCookie & "'."
        End If
    End If
End Sub

'Writes the window specific session key cookie.
Sub WriteSessionKey()
    Call ReadWindowID
    Response.Cookies(gsCookie) = sSessionKey
    'set the session cookie to expire 7 days hrs from now
    Response.Cookies(gsCookie).expires = DateAdd("d", 7, now)
End Sub

'Reads the Window ID from the query or form string.
Sub ReadWindowID()
    'Exist already in the var?
    If Not HasValue( sWindowID ) Then
        'Try the query string.
        sWindowID = Request.QueryString("WindowID")
        If Not HasValue( sWindowID ) Then
            'Try the form post.
            sWindowID = Request.Form("WindowID")
            If Not HasValue( sWindowID ) Then
                
                'Strict Method: Enforce presence of WindowID in query string.
                'Err.Raise ServerSideEvent(), "SessionManagerV2.vbs ReadWindowID()", "You must pass or set the WindowID in order to use Session."

                'Alternate Method: Use a default window id for non-claim windows.
                sWindowID = "0"

            End If
        End If
    End If
End Sub

'Creates a new Window ID.
Sub GenerateNewWindowID()
Dim sLynxID, sLynxWindowID

sLynxID = CStr(Request.QueryString( "LynxID" ))
sLynxWindowID = Request.Cookies(sLynxID)("WindowID")


    If Not HasValue( sWindowID ) Then
        sWindowID = Request.Cookies("WindowID")
        If Not HasValue( sWindowID ) Then
            sWindowID = "1"
	    sLynxWindowID= sWindowID
        Else


            If CLng(sWindowID) < 10000 Then
		If Not HasValue(sLynxWindowID) then
                	sWindowID = CStr(CLng(sWindowID) + 1)
			sLynxWindowID = sWindowID
		else
		sLynxWindowID = CStr(CLng(sLynxWindowID))
		end if
		
            Else
                sWindowID = "1"
		sLynxWindowID = sWindowID
            End If

        End If
        Response.Cookies("WindowID") = sWindowID
	sWindowID = sLynxWindowID
	Response.Cookies(sLynxID)("WindowID") = sWindowID
    End If

End Sub

'Initializes session manager for entry into APD application.
'Returns an initialized DataPresenter object for use by the caller.
'Made to be called from 'desktop' level ASPs such as ApdSelect.asp.
Function InitializeSession()

    On Error Goto 0

    'Get current user from Server NT Authentification
    strCsrID = Request.ServerVariables("LOGON_USER")

    'If user id is not returned (SHOULD Never happen but just in case....)
    If strCsrID = "" Then
        Err.Raise ServerSideEvent(), "SessionManagerV2.vbs", _
            "'LOGON_USER' server variable was blank.  This should not happen with IIS authentication."
    End If

    'Strip NT Domain from login
    If InStr( strCsrID, "\" ) Then
        strCsrID = Right(strCsrID, Len(strCsrID)-InStr(strCsrID,"\"))
    End If

    'Create and initialize DataPresenter.CExecute object.
    Dim objExe
    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")

    'We need to get the APD user ID from the NT CSR ID.
    Dim objRS
    Set objRS = objExe.ExecuteSpAsRS( "uspSessionGetUserDetail", strCsrID )

    'Returned recordset should contain the APD user ID.
    If objRS.EOF = False Then
        strApdUserID = objRS.Fields.Item("UserID").Value
    Else
        strApdUserID = ""
    End If
    
    set objRS = nothing

    If Not HasValue( strApdUserID ) Then
        Err.Raise ServerSideEvent() + 52, "SessionManagerV2.vbs", strCsrID
    End If

    'Reset Window ID.
    sWindowID = ""

    'Desktop Window
    If Request.QueryString( "NewSession" ) = "1" Or Request.QueryString( "LynxID" ) = "" Then

        'Use a default window id for non-claim windows.
        sWindowID = "0"

        'If the old session still exists then reuse it.
        sSessionKey = Request.Cookies(gsCookie)

        If Not SessionExists() Then

            'Create a new session in the database from the APD ID.
            Call CreateSession( strApdUserID )
            Call WriteSessionKey 'Save session id for other pages

            'Clear the WindowID counter.
            Response.Cookies("WindowID") = "0"

        End If
        
    'Claim Window.
    Else

        'Generate a new window ID.
        Call GenerateNewWindowID
        
        'Reuse the session created by the desktop.
        Call ReadSessionKey
        
    End If

    'Save user info into session.
    UpdateSession "CSRID", strCsrID

    If HasValue(strApdUserID) Then
		UpdateSession "UserID", strApdUserID
	End If

    'Initialize DataPresenter.CExecute object with new session key.
    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strApdUserID

    'Return the DataPresenter object for caller to use.
    Set InitializeSession = objExe

End Function

%>