<%
'=========================================================================
'| SessionStuffer.vbs
'|
'| This file is used instead of the ClaimSession.XSL to stuff bulk data
'| into Session.  Session management through XSL is rather inefficient, so
'| for the high number of vars getting stuffed here ASP was a better choice.
'|
'=========================================================================

Dim objClaimElement

Function StuffClaimSessionVars( strClaimXML, strSessionKey )
    Dim objSession, objDom

    Set objSession = CreateObject("SessionManager.CSession")
    Set objDom = CreateObject("MSXML2.DOMDocument")

    objDom.loadXml strClaimXML
    Set objClaimElement = objDom.selectSingleNode("//Claim")

    objSession.XslInitSession strSessionKey, sWindowID

    objSession.XslUpdateSession "LynxID"                  , ClaimVal( "@LynxID"                )
'    objSession.XslUpdateSession "OwnerUserID"             , ClaimVal( "@OwnerUserID"           )
'    objSession.XslUpdateSession "OwnerUserNameFirst"      , ClaimVal( "@OwnerUserNameFirst"    )
'    objSession.XslUpdateSession "OwnerUserNameLast"       , ClaimVal( "@OwnerUserNameLast"     )
    objSession.XslUpdateSession "ClaimStatus"             , ClaimVal( "@ClaimStatus"           )
    objSession.XslUpdateSession "RestrictedFlag"          , ClaimVal( "@RestrictedFlag"        )
    objSession.XslUpdateSession "ClaimOpen"               , ClaimVal( "@ClaimOpen"             )
    objSession.XslUpdateSession "InsuranceCompanyName"    , ClaimVal( "@InsuranceCompanyName"  )
    objSession.XslUpdateSession "InsuranceCompanyID"      , ClaimVal( "@InsuranceCompanyID"    )
    objSession.XslUpdateSession "ClientClaimNumber"       , ClaimVal( "@ClientClaimNumber"     )
    objSession.XslUpdateSession "InsuredNameFirst"        , ClaimVal( "@InsuredNameFirst"      )
    objSession.XslUpdateSession "InsuredNameLast"         , ClaimVal( "@InsuredNameLast"       )
    objSession.XslUpdateSession "InsuredBusinessName"     , ClaimVal( "@BusinessName"          )
    objSession.XslUpdateSession "LossDate"                , ClaimVal( "@LossDate"              )
    objSession.XslUpdateSession "Claim_ClaimAspectID"     , ClaimVal( "@ClaimAspectID"         )
    objSession.XslUpdateSession "DemoFlag"                , ClaimVal( "@DemoFlag"              )
    objSession.XslUpdateSession "SourceApplicationCode"   , ClaimVal( "@SourceApplicationCode" )
    objSession.XslUpdateSession "CoverageLimitWarningInd" , ClaimVal( "@CoverageLimitWarningInd" )

    'Pass back in case the caller can use this.
    Set StuffClaimSessionVars = objDom

    Set objClaimElement = Nothing
    Set objDom = Nothing
    Set objSession = Nothing
End Function

Function ClaimVal( strAtt )
    ClaimVal = replace( objClaimElement.selectSingleNode( strAtt ).Text, "'", "''")
End Function

%>











