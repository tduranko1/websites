<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
    'On Error Resume Next
    Dim objExe
    Dim strUserID


    Dim strHTML
    Dim ClaimAspectServiceChannelID
    
    strUserID = GetSession("UserID")
    ClaimAspectServiceChannelID = Request("ClaimAspectServiceChannelID")

    Set objExe = CreateObject("DataPresenter.CExecute")
    Call CheckError

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
    Call CheckError

    strHTML = objExe.ExecuteSpAsXML( "uspRepairSchReasonGetListXML", "RepairChangeReason.xsl", ClaimAspectServiceChannelID)

    Response.Write strHTML

    Call CheckError

    Set objExe = Nothing
%>

