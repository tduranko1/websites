<%@ Language=VBScript %>
<%
    Option Explicit
    Response.Expires = -1
%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strLynxID, strUserID, strVehNum, strDocumentID, strShowTotals, strWindowID

        strUserID = GetSession("UserID")
        strLynxID = GetSession("LynxID")
        strVehNum = GetSession("VehicleNumber")

        strDocumentID = Request("DocumentID")
        strShowTotals = Request("showTotals")
        strWindowID = Request("WindowID")

        'Create and initialize DataPresenter
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID

        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        objExe.AddXslParam "LynxId", strLynxID
        objExe.AddXslParam "VehicleNumber", strVehNum
        objExe.AddXslParam "showTotals", strShowTotals
        objExe.AddXslParam "WindowID", strWindowID
        objExe.AddXslParam "ImageRootDir", objExe.Setting("Document/RootDirectory")

        'Get XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspEstimateGetDetailXML", "Estimates.xsl", strDocumentID, strUserID )

    End Sub
%>
