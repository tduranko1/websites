<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        Dim strLynxID
        Dim strClaimAspectID
        Dim strInsuranceCompanyID

        strUserID = GetSession("UserID")
	
        strLynxID = request("LynxID")
        If Trim(strLynxID) = vbnullstring Then
            strLynxID = GetSession("LynxID")
        End If

        strClaimAspectID = Request("ClaimAspectID")
        If strClaimAspectID = "" Then
            strClaimAspectID = GetSession("ClaimAspectID")
        Else
            UpdateSession "ClaimAspectID", strClaimAspectID
        End If

        'Create and initialize DataPresenter
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        ' vehicle has been touched!
        objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strClaimAspectID, strUserID

        ' Place claim information in session manager.
        StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey

        'moved this line here because the Insurance company id for the claim in context will be correctly set only after the call to uspSessionGetClaimDetailXML
        strInsuranceCompanyID = GetSession("InsuranceCompanyID")

        Dim strImageRootDir
        strImageRootDir = objExe.Setting("Document/RootDirectory")

        objExe.AddXslParam "ImageRootDir", strImageRootDir

        'Get XML and transform to HTML.
        Response.Write  objExe.ExecuteSpAsXML( "uspClaimVehicleGetDetailXML", "ClaimVehicle.xsl", strClaimAspectID, strInsuranceCompanyID )

    End Sub
%>
