<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
  On Error Resume Next

  dim iLynxID, iEntityNumber, iInsuranceID
  dim sClaimAspectID, sEntityName, sClaimAspectServiceChannelID
  dim strUserID, objExe, strImageRootDir, strHTML
  dim iViewMode, iSelectThumbnail

  strUserID = GetSession("UserID")

  iLynxID = request("LynxID")
  iEntityNumber = request("EntityNumber")
  sEntityName = request("EntityName")
  sClaimAspectID = request("ClaimAspectID")
  sClaimAspectServiceChannelID = request("ClaimAspectServiceChannelID")
  iInsuranceID = request("InsuranceID")
  iViewMode = request("viewmode")
  iSelectThumbnail = request("selectthumbnail")

  if (iLynxID <> "" and sClaimAspectID <> "" and iInsuranceID <> "") then

    'Create and initialize DataPresenter
    Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
    CheckError()

    strImageRootDir = objExe.Setting("Document/RootDirectory")
    CheckError()

    objExe.AddXslParam "ImageRootDir", strImageRootDir
    CheckError()

    objExe.AddXslParam "EntityNumber", iEntityNumber
    CheckError()
    objExe.AddXslParam "EntityName", sEntityName
    CheckError()

    objExe.AddXslParam "viewMode", iViewMode
    CheckError()
    objExe.AddXslParam "selectThumbnail", iSelectThumbnail
    CheckError()

    strHTML = objExe.ExecuteSpAsXML( "uspDocumentGetDetailXML", "Documents.xsl", iLynxID, sClaimAspectID, sClaimAspectServiceChannelID, "O", iInsuranceID )
    CheckError()

    Set objExe = Nothing

    Call KillSessionObject

    Response.Write strHTML
  end if

  On Error GoTo 0

%>
