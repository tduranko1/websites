<%
   dim sLynxID, sVehNum, sRet, sProc, sRequest, sClaimAspectID, sInsuranceCompanyID
   dim oVehListXML, objExe, oRoot, oNode, oVehXML
   dim sClaimData, sVehData, sLaborData, sCity, sState, sCounty
   
   on error resume next
   
   sLynxID = Request("l")
   sVehNum = Request("v")
   
   'response.write "Lynx id: " & sLynxID & "<br/>"
   'response.write "Veh Num: " & sVehNum & "<br/>"
   
   if sLynxID <> "" and isNumeric(sLynxID) then
      set objExe = CreateObject("DataPresenter.CExecute")
      objExe.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH")
      
      
      sProc = "uspCPGetAllVehicleListXML"
      sRequest = "LynxID=" & sLynxID
      
      sRet = objExe.ExecuteSpNpAsXML( sProc, sRequest )
      
      if sRet <> "" then
      
         set oVehListXML = CreateObject("MSXML2.DOMDocument")
         oVehListXML.async = false
         oVehListXML.loadXML sRet
         
         set oRoot = oVehListXML.selectSingleNode("/Root")
         
         if not oRoot is nothing then
            sInsuranceCompanyID = oRoot.getAttribute("InsuranceCompanyID")
            set oNode = nothing
            if sVehNum <> "" and isNumeric(sVehNum) then
               set oNode = oVehListXML.selectSingleNode("/Root/Vehicle[@VehicleNumber='" & sVehNum & "']")
            end if
            if oNode is nothing then
               set oNode = oVehListXML.selectSingleNode("/Root/Vehicle")
            end if
            
            if not oNode is nothing then
               sClaimAspectID = oNode.getAttribute("ClaimAspectID")
               sProc = "uspECADClaimCondGetDetailXML"
               sRequest = "LynxID=" & sLynxID & "&InsuranceCompanyID=" & sInsuranceCompanyID
               sClaimData = objExe.ExecuteSpNpAsXML( sProc, sRequest )
               
               sProc = "uspECADClaimVehicleGetDetailXML"
               sRequest = "ClaimAspectID=" & sClaimAspectID & "&InsuranceCompanyID=" & sInsuranceCompanyID
               sVehData = objExe.ExecuteSpNpAsXML( sProc, sRequest )
               
               sCity = ""
               sState = ""
               sCounty = ""
               set oVehXML = CreateObject("MSXML2.DOMDocument")
               oVehXML.async = false
               oVehXML.loadXML sVehData
               
               set oNode = oVehXML.selectSingleNode("//Vehicle/ClaimAspectServiceChannel")
               if isNull(oNode.getAttribute("RepairLocationCity")) = false then sCity = oNode.getAttribute("RepairLocationCity")
               if isNull(oNode.getAttribute("RepairLocationCounty")) = false then sCounty = oNode.getAttribute("RepairLocationCounty")
               if isNull(oNode.getAttribute("RepairLocationState")) = false then sState = oNode.getAttribute("RepairLocationState")
               
               if sState <> "" then
                  sProc = "uspLaborRatesGetXML"
                  sRequest = "StateCode=" & sState & "&City=" & sCity & "&County=" & sCounty & "&InsuranceCompanyID=" & sInsuranceCompanyID
                  sLaborData = objExe.ExecuteSpNpAsXML( sProc, sRequest )
                  
               end if
               
               sRet = "<Root>" & _
                        "<List>" & sRet & "</List>" & _
                        "<Claim>" & sClaimData & "</Claim>" & _
                        "<Vehicle>" & sVehData & "</Vehicle>" & _
                        "<Labor>" & sLaborData & "</Labor>" & _
                      "</Root>"
            else
               sRet = "<Error>Unable to retrive vehicle list</Error>"
            end if
         else
            sRet = "<Error>Unable to retrieve vehicle list</Error>"
         end if
      else
         sRet = "<Error>Invalid LynxID</Error>"
      end if
   else
      sRet = "<Error>No LYNX ID Specified.</Error>"
   end if
   response.write sRet
%>