
<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->
<%
	Dim strUserID
	Dim strDocumentID
	Dim strHTML
    Dim strCaller

	On Error Resume Next

	strUserID = GetSession("UserID")
	strDocumentID = request("DocumentID")	
    strCaller = request("Caller")

	'Create and initialize DataPresenter
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

    objExe.AddXslParam "Caller", strCaller
    CheckError()
  
	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspEstimateAuditGetDetailXML", "EstimateAudit.xsl", strDocumentID )
	CheckError()

	Set objExe = nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>
