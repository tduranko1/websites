<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
   Dim strLynxID
   Dim strUserID
   Dim strMode
   Dim strInsuranceCompanyID
   Dim strHTML
   Dim strClaimAspectID
   Dim strEntity, strEntityInfo
   
   On Error Resume Next
   
   strLynxID = Request("LynxID")
   If strLynxID = "" Then
      strLynxID = GetSession("LynxID")
   Else
      UpdateSession "LynxID", strLynxID
   End If
   
   strUserID = GetSession("UserID")
   strInsuranceCompanyID = GetSession("InsuranceCompanyID")
   strEntity = Request("Entity")
   strEntityInfo = Request("Info")
   strClaimAspectID = Request("ClaimAspectID")
   
   CheckError()
   
   Dim objExe
   Dim strXML, strVehicleNum, strShopName, strShopCity, strProgramManagerEmailAddress, strRepName
   Dim oXML, oReinspection

   Set objExe = CreateObject("DataPresenter.CExecute")
   CheckError()
   
   objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
   CheckError()
   
   strXML = objExe.ExecuteSpAsXML("uspReIInfoGetDetailXML", "", strClaimAspectID)
   set oXML = Server.CreateObject("MSXML2.DOMDocument")
   oXML.async = false
   oXML.loadXML strXML
   
   set oReinspection = oXML.selectSingleNode("/Root/ReIRequest")
   if not oReinspection is nothing then
      strLynxID = oReinspection.getAttribute("LynxID")
      strVehicleNum = oReinspection.getAttribute("ClaimAspectNumber")
      strRepName = oReinspection.getAttribute("RequestedBy")
      strShopName = oReinspection.getAttribute("ShopName")
      strShopCity = oReinspection.getAttribute("ShopCity")
      strProgramManagerEmailAddress = oReinspection.getAttribute("ProgramManagerEmailAddress")
   end if
   
   set objExe = nothing
   set oXML = nothing

	On Error GoTo 0
%>


<html>
  <head>

    <title>Void <%=strEntity%></title>
    <link rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
    <link rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

    <style type="text/css">
        A {color:black; text-decoration:none;}
    </style>

    <xsl:variable name="UserId"/>

   <script language="JavaScript" src="js/apdcontrols.js"></script>
   <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
	<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,38);
		  event.returnValue=false;
		  };	
	</script>
    <script language="JavaScript">
      var sLynxID = "<%=strLynxID%>";
      var sUserID = "<%=strUserID%>";
      var sInsuranceID = "<%=strInsuranceCompanyID%>";
      var sClaimAspectID = "<%=strClaimAspectID%>";
      var sEntity = "<%=strEntity%>";
      var sProgramManagerEmailAddress = escape("<%=strProgramManagerEmailAddress%>");
      var sVehicleNum = "<%=strVehicleNum%>";
      var sRepName = escape("<%=strRepName%>");
      var sShopName = escape("<%=strShopName%>");
      var sShopCity = "<%=strShopCity%>";
		
		function pageInit(){
			//alert("LynxID: " + sLynxID + "\nUserID: " + sUserID + "\nInsuranceID: " + sInsuranceID);
		}
		
   function doSave(){
      var sComments = txtComments.innerText;
      if (sComments == "") {
         ClientWarning("APD System requires a non-blank comment inorder to void the " + sEntity + ". Please enter the comments and try again.");
         return;
      } else {
         if (sComments.substr(0, 14) != (sEntity.toUpperCase() + " VOIDED:"))
            sComments = sEntity.toUpperCase() + " VOIDED: " + sComments;
      }
      var strRet = YesNoMessage("Confirm Void", sEntity + " " + sVehicleNum + " will be voided along with all the service channels regardless of their status.\n\nDo you want to continue?");

      if (strRet != "Yes") return;
      
      var sAction = "Update";
   
      var sRequest = "ClaimAspectID=" + sClaimAspectID + 
                     "&InsuranceCompanyID=" + sInsuranceID + 
                     "&UserID=" + sUserID + 
                     "&Comment=" + escape(sComments);
   
      var sProc = "uspWorkflowVoidEntity";
      //alert(sProc + "\n" + sRequest); return;
      
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "executespnp",
                        data     : sRequest }
                     );
      var objRet = XMLSave(makeXMLSaveString(aRequests));
      if (objRet.code == 0) {
         if (sProgramManagerEmailAddress != ""){
            //active reinspection existed for this vehicle. Notify Program Manager
            frmReI.LynxID.value = sLynxID;
            frmReI.VehicleNum.value = sVehicleNum;
            frmReI.RequestedBy.value = unescape(sRepName);
            frmReI.ShopName.value = unescape(sShopName);
            frmReI.ShopCity.value = sShopCity;
            frmReI.ProgramManagerEmailAddress.value = unescape(sProgramManagerEmailAddress);
            
            frmReI.submit();
   
         } else {
            doOK();
         }
      }
   }
	
      function doOK(){
         window.returnValue = "OK";
         window.close();
      }
    </script>

  </head>
  <body bgcolor="#FFFAEB" text="#000000" onload="pageInit()" style="margin:5px;overflow:hidden">
	<table border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td colspan="2" style="font-size:18px;font-weight:bold">Void <%=strEntity%></td>
		</tr>
		<tr>
			<td>Lynx ID:</td>
			<td><%=strLynxID%></td>
		</tr>
		<tr>
			<td><%=strEntity%>:</td>
			<td><%=strEntityInfo%></td>
		</tr>
		<tr valign="top">
			<td>Comments:</td>
			<td>
				<textarea name="txtComments" id="txtComments" class="TextAreaField" cols="60" rows="6"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="button" class="formButton" value=" OK " onclick="doSave()"/>
				<input type="button" class="formButton" value="Cancel" onclick="window.returnValue = 'Cancel';window.close()"/>
			</td>
		</tr>
	</table>
  <form id="frmReI" name="frmReI" action="/NotifyReinspection.asp" method="post" target="ifrmRetrieveData">
    <input type="hidden" name="LynxID" id="LynxID" value=""/>
    <input type="hidden" name="VehicleNum" id="VehicleNum" value=""/>
    <input type="hidden" name="RequestedBy" id="RequestedBy" value=""/>
    <input type="hidden" name="ShopName" id="ShopName" value=""/>
    <input type="hidden" name="ShopCity" id="ShopCity" value=""/>
    <input type="hidden" name="ProgramManagerEmailAddress" id="ProgramManagerEmailAddress" value=""/>
    <input type="hidden" name="fn" value="VoidVehicle"/>
  </form>
  <IFRAME id="ifrmRetrieveData" name="ifrmRetrieveData" src="blank.asp" frameborder="0" style="display:none; border:0px;" tabindex="-1"></IFRAME>
  </body>
</html>
