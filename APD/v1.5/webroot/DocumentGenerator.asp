<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
    'This object var declared outside of the function calls
    'so that we can assure object cleanup regardless of error.
    Dim oExe 'As Object
    Dim sLynxID, sUserID, sInsCoID, sAttach, sTemplate

    On Error Resume Next

    Call ProcessAction
    Set oExe = Nothing
    Call KillSessionObject

    If Err.Number <> 0 Then
        CloseWindow
    End If
    
    Call CheckError

    On Error GoTo 0

    'Call other functions based upon passed Action.
    Function ProcessAction()
        Dim sAction

        sUserID  = Request( "UserId" )
        sLynxID  = Request( "LynxId" )
        sInsCoID = Request( "InsCoId" )
        sAttach  = Request( "Attach" )
        sAction  = Request( "theAction" )
        sTemplate = Request( "Template" )

        If sAction = "Generate" Then
            GenerateDocument
        ElseIf sAction = "Cancel" Then
            CloseWindow
        Else
            BuildDocumentList
        End If
    End Function

    'Retrieves an XML list of available documents and doc templates.
    'Builds the UI HTML from this XML via XSL style sheet.
    Function BuildDocumentList()

        Set oExe = CreateObject("DataPresenter.CExecute")
        oExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, sUserID

        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        oExe.AddXslParam "InsCoId", GetSession("InsuranceCompanyID")
        oExe.AddXslParam "SrcAppCd", GetSession("SourceApplicationCode")
        oExe.AddXslParam "LynxId", sLynxID
        oExe.AddXslParam "Attach", sAttach

        'Grabs the XML from the database and transforms it.
        Response.Write oExe.ExecuteSpAsXML( _
            "uspDocumentGetDetailXML", _
            "DocumentGenerator.xsl", _
            sLynxID, "", "", "A", sInsCoID)

    End Function

    'Creates the document, sends it on its way.
    'This hard-coded crab should be replaced with something more elegant for real document management.
    Function GenerateDocument()

        Dim sDocument

        Select Case sTemplate

            Case "FolderUpload"

                Set oExe = CreateObject("LAPDPartnerDataMgr.CExecute")

                oExe.InitiatePartnerXmlTransaction _
                    "<FolderUpload Partner='SceneGenesis' InsuranceCompanyID='" & sInsCoID _
                    & "' LynxID='" & sLynxID & "' Attachments='" & sAttach & "' UserID='" & sUserID & "' />"

                CloseWindow

            Case "DeskAuditStatusReport", "DeskAuditStatusSupplement"

                Set oExe = CreateObject("DocumentMgr.CDocMgr")

                sDocument = oExe.GenerateDocument( _
                    "<" & sTemplate & " InsuranceCompanyID='" & sInsCoID _
                    & "' InsuranceCompanyName='" & Replace(GetSession("InsuranceCompanyName"),"&","&amp;") _
                    & "' LynxID='" & sLynxID & "' Attachments='" & sAttach _
                    & "' UserID='" & sUserID & "' NTUserID='" & GetSession("CSRID") _
                    & "' ProtectDocument='False' DocumentType='Closing Report' />")

                OpenDocument sDocument

            Case "EstimateAuditSummary"

                Set oExe = CreateObject("DocumentMgr.CDocMgr")

                sDocument = oExe.GenerateDocument( _
                    "<" & sTemplate & " InsuranceCompanyID='" & sInsCoID _
                    & "' InsuranceCompanyName='" & Replace(GetSession("InsuranceCompanyName"),"&","&amp;") _
                    & "' LynxID='" & sLynxID & "' Attachments='" & sAttach _
                    & "' UserID='" & sUserID & "' NTUserID='" & GetSession("CSRID") _
                    & "' ProtectDocument='True' DocumentType='Form' />")

                OpenDocument sDocument

            Case Else

                Err.Raise ServerSideEvent(), "DocumentGenerator.asp::GenerateDocument()", _
                    "Unknown Document Template: '" & sTemplate & "'"

        End Select

    End Function

    'Closes this dialog window.
    Function CloseWindow()
        Response.Write "<html><head><script>window.close();</script></head></html>"
    End Function

    'Points this window to a document, presumably on the document share.
    Function OpenDocument( sDocument )

        'This version just opens up from the client but within the window.
        'Response.Write "<script language=""VBScript"">window.navigate(""" & sDocument & """)</script>"

        'This version uses the document viewer Ramesh wrote.
        'response.redirect "EstimateDocView.asp?docPath=" & sDocument

        'This version opens it up from a shell outside the window.
        Response.Write "<script language=""JavaScript"">" & vbCrLf
        Response.Write "  var wsh = new ActiveXObject( 'WSCript.shell' );" & vbCrLf
        Response.Write "  wsh.Run( 'winword " & Replace(sDocument,"\","\\") & "' );" & vbCrLf
        Response.Write "  wsh.AppActivate( 'Imaging' );" & vbCrLf
        Response.Write "  window.close();" & vbCrLf
        Response.Write "</script>"

    End Function

%>
