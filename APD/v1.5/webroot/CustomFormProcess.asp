<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->

<%
    On Error Resume Next

    Dim objExe, objStream
    Dim strDocumentID
    Call GetHtml
    Set objExe = Nothing
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
    
      Dim strAction
      Dim strXML
      
      strAction = request("txtAction")
      strXML = request("txtXML")
      
      '        response.write strAction & "<br/>"
      '        response.write strXML & "<br/>"
      
      Set objExe = CreateObject("LAPDCustomForms.CCustomForms")
      if strAction = "" then
         strDocumentID = objExe.processForm(strXML)
%>
   <script language="javascript">
      var strDocumentID = "<%=strDocumentID%>";
      if (strDocumentID != "") {
         alert("Document was successfully added to the claim.");
      } else
         alert("There was an issue saving the form to the claim. Please contact Help Desk.");
      parent.window.close();
   </script>
<%         
      else
         'preview the form
         set objStream = objExe.previewForm(strXML, true)
         Response.ContentType = "application/pdf"
         Response.AddHeader "Content-Disposition", "inline;filename=CustomReport.pdf" 
         Response.BinaryWrite objStream.Read()
         set objStream = nothing
      end if
      
      set objExe = nothing
    End Sub
%>
