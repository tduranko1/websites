<%@ Language=VBScript %>
<% Option Explicit %>

<%
    Dim objExe, objUtils, objRS, strServerName

    On Error Resume Next
    StatusInfo

    Set objRS = Nothing
    Set objExe = Nothing

    On Error GoTo 0

    Function StatusInfo()
        On Error Goto 0


			
		Set objUtils =  CreateObject("SiteUtilities.CSettings")
		strServerName = objUtils.GetServerName
		
		'Response.Write strServerName
	    
		
		Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
		
        Set objRS = objExe.ExecuteSpAsRS("uspSysGetStatus", strServerName)

		If objRS.eof Then
			Response.write "DOWN"
		Else
			Response.write "UP"
		End If
		
        Set objRS = Nothing
        Set objExe = Nothing

    End Function
%>
