<%@ Language=VBScript %>
<%
    Option Explicit
    Response.Expires = -1
%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->
<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        Dim strLynxID
        dim strAction
        dim strClaimAspectID

        strUserID = GetSession("UserID")
        strLynxID = GetSession("LynxID")
        strAction = request("act")        
                
        'Create and initialize DataPresenter
        'Set objExe = CreateObject("DataPresenter.CExecute")

        'objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'objExe.AddXslParam "LynxId", strLynxID

        'Get XML and transform to HTML.
        'Response.Write objExe.ExecuteSpAsXML( "uspJournalGetDetailXML", "JournalDetail.xsl", strLynxID )

        '------------------------------------'
		' APD.NET Local Params
		'------------------------------------'
        dim sParams
        sParams = "LynxId:" & strLynxID
        sParams = sParams + "|UserId:" & strUserID
        sParams = sParams + "|Action:" & strAction

		'------------------------------------'
		' APD.NET Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
        'response.write("sASPXSessionKey" & " - " & sASPXSessionKey & "<br/>")
        'response.write("sASPXWindowID" & " - " & sASPXWindowID & "<br/>")
        'response.write("strUserID" & " - " & strUserID & "<br/>")
        'response.write("strLynxID" & " - " & strLynxID & "<br/>")
        'response.write("strAction" & " - " & strAction & "<br/>")
		'response.end

		'------------------------------------'
		' APD.NET Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("JournalDetails.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & SASPXWindowID & "&Params=" & sParams)
    End Sub
%>
