<%
    Response.Expires = -1

	Function ServerSideEvent()
		ServerSideEvent = &H80070000
	End Function

	'Returns html that will popup the error window.
	Function GetErrorPopupScript()
		GetErrorPopupScript = "<script language='JavaScript'>" _
            & "window.open('\\EventPopup.asp','','width=424,height=200,top=284,left=300,scrollbars=yes,location=no," _
			& "directories=no,status=no,menubar=no,toolbar=no,resizable=yes')</script>"
	End Function

	'Saves error data for later retrieval by EventPopup.asp.
	Sub SaveErrorData( nNumber, sSource, sDescription )
        Response.Cookies("APDError")("Number") = nNumber
        Response.Cookies("APDError")("Source") = Left(sSource,1024)    'Size limit
        Response.Cookies("APDError")("Description") = Left(sDescription,1024) 'Size limit
	End Sub

	'Here we save the error data as a cookie.
	'The preferred method would have been to use session, but what if the error arose from session?
	'And we cannot pass the error description through window.open() because of its size.
	Sub ErrorHandler()
        SaveErrorData Err.Number, Err.Source, Err.Description
		Response.Write GetErrorPopupScript()
	End Sub

	'Generic error checking subroutine for server side VBScript.
	Sub CheckError()
		If Err.Number <> 0 Then
            Call ErrorHandler
			Err.Clear
			Response.End
		End If
	End Sub
%>
