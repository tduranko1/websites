<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
	Dim strLynxID, strVehNum, strUserID
	Dim strHTML

	On Error Resume Next

	strLynxID = GetSession("LynxID")
	strPropNum = GetSession("PropertyNumber")
	if strPropNum = "" then
		strPropNum = Request("PropNum")
		if strPropNum <> "" then
			UpdateSession "PropertyNumber", strPropNum
		end if
	end if

	strUserID = GetSession("UserID")
	CheckError()

	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimPropertyGetDetailXML", "ClaimProperty.xsl", strLynxID, strPropNum, strUserID )
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>
