<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strLynxID
	Dim strInsuranceCompanyID
	Dim strHTML

    On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = GetSession("LynxID")
    strInsuranceCompanyID = GetSession("InsuranceCompanyID")
  
	CheckError()

	'Create and initialize DataPresenter
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	CheckError()

    objExe.AddXslParam "LynxID", strLynxID
	CheckError()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimVehicleGetDetailXML", "ClaimVehicleInsert.xsl", 0, strInsuranceCompanyID )
	CheckError()

	Set objExe = nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>

