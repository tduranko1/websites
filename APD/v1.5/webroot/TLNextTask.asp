<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<%
    On Error Resume Next
    Response.ContentType = "text/xml"

    Dim objExe, strRet, strUserID, strChecklistID
    strUserID = Request("UserID")
    strChecklistID = Request("ChecklistID")
    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
    strRet = objExe.ExecuteSpNpAsXML( "uspTLGetNextTaskXML", "UserID=" & strUserID & "&ChecklistID=" & strChecklistID )
    if err.number = 0 then
      response.write strRet
    else
      response.Write "<Error><![CDATA[" & err.number & ": " & err.Description & "]]></Error>"
    end if
   
    Set objExe = Nothing
%>