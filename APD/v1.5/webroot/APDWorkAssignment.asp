<%@LANGUAGE="VBSCRIPT"%>
<%
    Option Explicit
  On Error Resume Next

  Response.Expires = -1
%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
  Dim objData
  Dim rsCrud
  dim strHTML

    Dim LoginUserID

    ' Get the logged in user
    LoginUserID = GetSession("UserID")
    CheckError()

    ' Initiate DataAccessor
    set objData = CreateObject("DataPresenter.CExecute")
    CheckError()

    objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH"), sSessionKey, LoginUserID
    CheckError()

    'Get UserList
    strHTML = objData.ExecuteSpAsXML("uspAdmUserGetListXML", "APDWorkAssignment.xsl", "APD")

    'Processing Done, destroy DataAccessor
    set objData = Nothing

    Call KillSessionObject

    On Error Goto 0

    response.write strHTML

    sub displayNoAccess()
    %>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to view User Information.
                  <br/><br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
    <%
    end sub

%>
