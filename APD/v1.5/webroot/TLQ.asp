﻿<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1 %>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
   Dim strUserID, strQueueUser
   strUserID = Request("UserID")
   strQueueUser = Request("q")
   if strUserID = "" then
      strUserID = GetSession("UserID")
   end if
%>
<html>
<head>
    <title>Total Loss Queue</title>
    <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
    <script language="javascript" type="text/javascript">
       var gsUserID = "<%=strUserID %>";
       var gsQueueUser = "<%=strQueueUser %>";
       self.resizeTo("1022", "699");
       //alert(gsQueueUser);
    </script>
</head>
<body class="bodyAPDSelect" scroll="no" >
   <iframe src="tlqueue.htm" style="height:100%;width:100%"></iframe>
</body>
</html>
