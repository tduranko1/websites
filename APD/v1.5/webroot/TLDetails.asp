<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<%
    'On Error Resume Next
    Response.ContentType = "text/xml"

    Dim objExe, strRet, strClaimAspectServiceChannelID
    strClaimAspectServiceChannelID = Request("ClaimAspectServiceChannelID")
    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
    strRet = objExe.ExecuteSpNpAsXML( "uspTLGetDetailXML", "ClaimASpectServiceChannelID=" & strClaimAspectServiceChannelID )
    response.write strRet
   
    Set objExe = Nothing
%>