<%
on error goto 0
dim strCity, strState, strRet, blnTransform
dim oXML, oXSL

strCity = UCase(trim(request("q")))
strState = UCase(trim(request("s")))

blnTransform = false

'response.write "q=" & strCity

if strCity <> "" and left(strCity, 1) >= "A" and left(strCity, 1) <= "Z" then

   set oXML = Server.CreateObject("MSXML2.DOMDocument")
   oXML.async = false
   oXML.load Server.Mappath("lookup/cities_" & left(strCity, 1) &".xml")
   
   If oXML.parseError.errorCode <> 0 Then
      response.write oXML.parseError.reason
   End If
   blnTransform = true
   'response.write oXSL.xml
elseif strState <> "" then
   set oXML = Server.CreateObject("MSXML2.DOMDocument")
   oXML.async = false
   oXML.load Server.Mappath("lookup/states.xml")
   
   If oXML.parseError.errorCode <> 0 Then
      response.write oXML.parseError.reason
   End If
   strCity = ""
   
   blnTransform = true
end if

if blnTransform = true then
   Set oRoot = oXML.selectSingleNode("/Root")
   oRoot.setAttribute "city", strCity
   oRoot.setAttribute "state", strState
   
   set oXSL = Server.CreateObject("MSXML2.DOMDocument")
   oXSL.async = False
   oXSL.Load Server.Mappath("xsl/citiesfilter.xsl")
   If oXSL.parseError.errorCode <> 0 Then
      response.write oXSL.parseError.reason
   End If
   
   strRet = oXML.transformNode(oXSL)
end if

response.write strRet

set oXML = nothing
set oXSL = nothing

%>