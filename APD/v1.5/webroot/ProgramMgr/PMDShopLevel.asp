<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->
<%
	Dim strUserID, strDistrictManagerUserID, strProgramManagerUserID, strShopLocationID, strSupervisorFlag

	On Error Resume Next

	strProgramManagerUserID = request("ProgramManagerUserID")
	strDistrictManagerUserID = request("DistrictManagerUserID")
	strShopLocationID = request("ShopLocationID")
	strSupervisorFlag = request("SupervisorFlag")
	
	On Error GoTo 0
%>

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

  <!-- CLIENT SCRIPTS -->
  <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>	<!-- all child pages use this script -->
  <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>

  <!-- Page Specific Scripting -->
  <SCRIPT language="JavaScript">

  var gsShopLocationID = '<%=strShopLocationID%>';
  var gsDistrictManagerUserID = '<%=strDistrictManagerUserID%>';
  var gsProgramManagerUserID = '<%=strProgramManagerUserID%>';
  var gsSupervisorFlag = '<%=strSupervisorFlag%>';
  var gsLynxID = "";
  var gsClaimAspectID = "";
  var gsAssignmentID = "";
  var gsBeginDate = "";
  var gsEndDate = "";
  var gsAssignmentCode = "";
  var gsReferrer;		// this is used to maintain ShopLevel state from lower level pages.
  var gsInsuranceCompanyID;
  var gsPertainsTo;
  var gbDirtyFlag = false;
  var gsPageName;
  var gsImageRootDir;
  var gsImageLocation;


  function TabChange(sPage){
    var obj;
    var qString = "?ShopLocationID="+gsShopLocationID+"&BeginDate="+gsBeginDate+"&EndDate="+gsEndDate+"&AssignmentCode="+gsAssignmentCode;
    var sHREF = sPage + qString;

    tab11.className = "tab1";
    tab12.className = "tab1";
    tab13.className = "tab1";
    tab14.className = "tab1";
    tab15.className = "tab1";
	
  	if (ifrmContent.document.all.txtCallBack){
  		ifrmContent.document.all.txtCallBack.value = sHREF;
  	}	
  	
  	if (CheckDirty()){
    	document.frames["ifrmContent"].frameElement.src = sHREF;
    	obj = document.getElementById("ifrmContent");
    	obj.style.visibility = "visible";
    	obj.style.display = "inline";
  	}
  }

  function MenuCheckDirty(){
    if (gbDirtyFlag) {
		var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to return to the page and save the changes?");
        if (sSave == "Yes") return false;        
        else gbDirtyFlag = false;
    }
    return true;
 }
    
  function CheckDirty(){
	if (gbDirtyFlag) {
		var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to save the changes?");
        if (sSave == "Yes")
            return ifrmContent.IsDirty();
    }
	gbDirtyFlag = false;
    return true;
  }
  
  function ShowEstimateViewer(){
  	var sHREF = "/EstimateDocView.asp?docPath=" + gsImageRootDir + gsImageLocation;
	window.showModelessDialog(sHREF, "", "dialogHeight:355px; dialogWidth:1038px; dialogTop:5px; dialogLeft:5px; resizable:yes; status:no; help:no; center:no;");
  }
  
  function ShowPhotoViewer(){
  	sHREF = "/Documents.asp?LynxID=" + gsLynxID + "&InsuranceID=" + gsInsuranceCompanyID + "&ClaimAspectID=" + gsClaimAspectID;
	window.showModelessDialog(sHREF, "", "dialogHeight:355px; dialogWidth:1038px; dialogTop:5px; dialogLeft:5px; resizable:yes; status:no; help:no; center:no;");
  }

</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="TabChange('PMDCycleTime.asp');" bgcolor="#FFFAEB">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
    <SPAN unselectable="on" id="tab11" page="PMDCycleTime.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Shop Detail</SPAN>
	<SPAN unselectable="on" id="tab12" page="PMDIndemnity.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Indemnity</SPAN>
	<SPAN unselectable="on" id="tab13" page="PMDReinspection.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Reinspection</SPAN>
	<SPAN unselectable="on" id="tab14" page="PMDAudit.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Audit</SPAN>
	<SPAN unselectable="on" id="tab15" page="PMDCSI.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">CSI</SPAN>
  </DIV>

  <DIV unselectable="on" class="content1" id="content11" style="background-color:#FFFAEB; width:995px; height:555px;">
    <IFRAME id="ifrmContent" src="/blank.asp" style='width:970px; height:553px; top:0px; left:0px; border:0px; padding:0px; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' ></IFRAME>
  </DIV>

</BODY>
</HTML>
