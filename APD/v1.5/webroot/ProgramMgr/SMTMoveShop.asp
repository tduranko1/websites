<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
    Dim strHTML, objExe, sUserID
	
	On Error Resume Next

	ReadSessionKey()
	
	sUserID = GetSession("UserID")
	
	'Create and initialize DataPresenter
  	Set objExe = CreateObject("DataPresenter.CExecute")
  	CheckError()

  	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sUserID
  	CheckError()
		
	objExe.AddXslParam "DealerID", request("DealerID")
	CheckError()
	
	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSMTShopSearchReferenceGetListXML", "SMTMoveShopSearch.xsl" )
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

