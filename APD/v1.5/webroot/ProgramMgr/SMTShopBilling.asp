<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strBillingID

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strBillingID = request("BillingID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  select case request.querystring("mode")
    case "wizard"
		objExe.AddXslParam "mode", "wizard"
		CheckError()
  	case "update"
		objExe.ExecuteSpNp "uspSMTShopBillingUpdDetail", request.form
    	CheckError()
		if request("CallBack") <> vbnullstring then
			response.redirect(request("CallBack"))
		else
			Response.Redirect("SMTShopBilling.asp?BillingID=" & strBillingID)
		end if
  end select
    
  objExe.AddXslParam "UserID", strUserID
  CheckError()
	
  strHTML = objExe.ExecuteSpAsXML( "uspSMTShopBillingGetDetailXML", "SMTShopBilling.xsl", strBillingID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
