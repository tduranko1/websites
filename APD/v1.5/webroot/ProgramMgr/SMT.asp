<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
    Dim strHTML, objExe, sShopID

	On Error Resume Next

  Set objExe = InitializeSession()
  CheckError()

  objExe.AddXslParam "Environment", objExe.mToolkit.mEvents.mSettings.Environment
  CheckError()
  
  objExe.AddXslParam "SearchType", request("SearchType")
  CheckError()
  
  objExe.AddXslParam "EntityID", request("EntityID")
  CheckError()
  
  'Get PMD XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSessionGetUserDetailXML", "SMT.xsl", LCase(strCsrID))
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

  Call KillSessionObject

	On Error GoTo 0
%>

