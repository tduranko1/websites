<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  dim objExe, strHTML,  strUserID, lRetVal, strProc, strDocumentID, strSupervisorForm, strImageRootDir, strCreateDocument
  dim objDocMgr, strDocument, strFaxDocument, blnDocFaxed
  
  strUserID = GetSession("UserID")

  'Get values from the query sring
  strDocumentID = Request("DocumentID")
  strSupervisorForm = Request("SupForm")
  strProc = Request("proc")
  strCreateDocument = request("CreateDocument")
  strFaxDocument = request("FaxDocument")

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
  CheckError()

  objExe.AddXslParam "UserID", strUserID
  CheckError()

  if strProc <> vbnullstring then						' some action was initiated from the page
  	lRetVal = objExe.ExecuteSpNp(strProc, Request.form)
    CheckError()
	  
    '------------------------------------------------------------------------------------------
    ' When locking the ReI form create a word doc ReI Report and attach it to the vehicle.
    if strCreateDocument = "true" then
      Set objDocMgr = CreateObject("DocumentMgr.CDocMgr")
      CheckError()
      
      strDocument = objDocMgr.GenerateReinspectionDocument( _
          "<ReinspectionForm DocumentID='" & strDocumentID _
          & "' SupervisorForm='" & strSupervisorForm _
          & "' UserID='" & strUserID & "' NTUserID='" & GetSession("CSRID") _
          & "' ProtectDocument='True' DocumentType='Reinspection Report' NotifyEventDocumentInsert='False' />")
  
      'OpenDocument sDocument
    end if    
    '------------------------------------------------------------------------------------------
    
    if request("CallBack") <> vbnullstring then
		  response.redirect(request("CallBack"))
	  else
		  ' this is to prevent retry/cancel messages when refreshing the page after a post
		  response.redirect("PMDReinspectForm.asp?DocumentID=" & strDocumentID & "&SupForm=" & strSupervisorForm)
	  end if
  end if

  '------------------------------------------------------------------------------------------
  ' Sending the reinspection form to the shop. This will not post the document to claim
  blnDocFaxed = false
  if strFaxDocument = "true" then
    Set objDocMgr = CreateObject("DocumentMgr.CDocMgr")
    CheckError()
    
    blnDocFaxed = objDocMgr.FaxReinspectionDocument( _
        "<ReinspectionForm DocumentID='" & strDocumentID _
        & "' SupervisorForm='" & strSupervisorForm _
        & "' UserID='" & strUserID & "' NTUserID='" & GetSession("CSRID") _
        & "' ProtectDocument='True' DocumentType='Reinspection Report' />")
    
  end if
  '------------------------------------------------------------------------------------------

  strImageRootDir = objExe.Setting("Document/RootDirectory")
  CheckError()

  objExe.AddXslParam "ImageRootDir", strImageRootDir
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDReinspectFormGetDetailXML", "PMDReinspectForm.xsl", strDocumentID, strSupervisorForm, strUserID )
  CheckError()

  Set objExe = Nothing
  set objDocMgr = nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0
  if strFaxDocument = "true" then
    if blnDocFaxed = true then
%>
  <script language="javascript">
    parent.ClientWarning("The Reinspection report has been faxed to the shop.");
  </script>
<%  else%>
  <script language="javascript">
    parent.ClientWarning("Unable to fax the Reinspection report to the shop.");
  </script>
<%  end if%>  
<%end if%>
