<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
  Dim strHTML, objExe, sUserID, sFedTaxID, iBusinessInfoID

	On Error Resume Next

	ReadSessionKey()

	sUserID = GetSession("UserID")
    sFedTaxID = request("FedTaxID")
    iBusinessInfoID = request("BusinessInfoID")
  
	'Create and initialize DataPresenter
    Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sUserID
    CheckError()

    objExe.AddXslParam "mode", request("mode")
    CheckError()
  
	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSMTDupFedTaxIDGetListXML", "SMTFedTaxID.xsl",  sFedTaxID, iBusinessInfoID)
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

