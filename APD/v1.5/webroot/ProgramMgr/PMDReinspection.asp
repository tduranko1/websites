<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->
<%
  On Error Resume Next

  Dim strShopLocationID, strBeginDate, strEndDate, strAssignmentCode
  Dim objExe, strHTML,  strUserID

  strUserID = GetSession("UserID")

  'Get values from the query sring
  strShopLocationID = Request("ShopLocationID")
  strBeginDate = Request("BeginDate")
  strEndDate = Request("EndDate")
  strAssignmentCode = Request("AssignmentCode")
  CheckError()

 'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
  objExe.AddXslParam "LocID", strShopLocationID
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDReinspectionGetDetailXML", "PMDReinspection.xsl", strShopLocationID, strBeginDate, strEndDate, strAssignmentCode, strUserID )

  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0
%>
