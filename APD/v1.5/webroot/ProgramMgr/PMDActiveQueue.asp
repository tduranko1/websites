<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim strProgramManagerUserID, strUserID, strShopName, strShopCity, strShopZip
  Dim objExe, strHTML

  strUserID = GetSession("UserID")

  'Get values from the query sring
  strProgramManagerUserID = Request("ProgramManagerUserID")
  'strProgramManagerUserID = 35          ''''''''''''''''''''''''''''''''' Debug Line ''''''''''''''''''''''

  if strProgramManagerUserID = vbnullstring then		' or this user is a not a district manager or above
  	strProgramManagerUserID = GetSession("UserID")
  end if

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDActiveWorkGetListXML", "PMDActiveQueue.xsl", strProgramManagerUserID )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0

  %>
