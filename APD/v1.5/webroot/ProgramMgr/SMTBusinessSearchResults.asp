<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
  
<%
  On Error Resume Next
   
  Dim objExe, strHTML, strUserID, sPageID
  
  strUserID = GetSession("UserID")
  sPageID = request("PageID")
  
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
  
  dim x, i, strUpdates
  
  strUpdates = ""
  
  if request("mode") = "save" then
    for i = 1 to Request.Form("chkAdd").count
		  x = request.form("chkAdd")(i)
      strUpdates = strUpdates & Request.Form("EntityID")(x) & ","
    next  
    
    if sPageID = "DealerAddBusiness" then
      objExe.ExecuteSp "uspSMTDealerBusinessInsDetail", strUpdates, Request.Form("DealerID"), Request.Form("SysLastUserID")
    else
      objExe.ExecuteSp "uspSMTShopUpdBusinessInfoID", strUpdates, Request.Form("BusinessInfoID"), strUserID
    end if
    
    CheckError()
	      
    'if sPageID = "DealerAddBusiness" then
      if request("CallBack") <> vbnullstring then
        objExe.AddXslParam "CallBack", request("CallBack")
  		end if
    'end if
    
    if sPageID = "BusinessIncludeShop" then
      objExe.AddXslParam "ShopsMoved", 1
      CheckError()
    end if
  end if
   
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  objExe.AddXslParam "PageID", sPageID
  CheckError
   
  dim sSearchType, sEntityID, sAddressCity, sAddressState, sFedTaxID, sName, sPhoneAreaCode, sPhoneExchange
  dim sAddressZip, sPhoneUnit, sProgramFlag, sSpecialtyID, sExclusionEntityID, sExclusionType
  
  sSearchType = request("SearchType")
  
  if request("mode") = "save" then
    sEntityID = ""
  else
    sEntityID = request("EntityID")
  end if
  
  sFedTaxID = request("FedTaxID")
  sAddressZip = request("AddressZip")
  sName = request("Name")
  sAddressCity = request("AddressCity")
  sAddressState = request("AddressState")
  sPhoneAreaCode = request("PhoneAreaCode")
  sPhoneExchange = request("PhoneExchange")
  sPhoneUnit = request("PhoneUnit")
  sProgramFlag = request("ProgramFlag")
  sSpecialtyID = request("SpecialtyID")
  sExclusionEntityID = request("ExclusionEntityID")
  sExclusionType = request("ExclusionType")
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTEntitySearchGetListXML", "SMTBusinessSearchResults.xsl", sSearchType, sEntityID, sAddressCity, sAddressState, sAddressZip, sFedTaxID, sName, sPhoneAreaCode, sPhoneExchange, sPhoneUnit, sProgramFlag, sSpecialtyID, sExclusionEntityID, sExclusionType)
  CheckError()
  
  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
  
  %>
