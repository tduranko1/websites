<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, sHTML, sCertifiedFirstID, sAddressZip

  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey
  CheckError()

  if request.querystring("act") = "save" then
    objExe.ExecuteSpNp "uspSMTShopLoadInsDetail2", request.form & cstr(now())
    CheckError()
    
    objExe.AddXslParam  "Saved", "yes"
    CheckError()
  end if
  
  sCertifiedFirstID = request("CertifiedFirstID")
  sAddressZip = request("AddressZip")
  
  sHTML = objExe.ExecuteSpAsXML( "uspSMTAllInfoGetDetailXML2", "SMTAllInfo.xsl", sCertifiedFirstID, sAddressZip )
  CheckError()

  Set objExe = Nothing

  Response.Write sHTML

  On Error GoTo 0
%>
