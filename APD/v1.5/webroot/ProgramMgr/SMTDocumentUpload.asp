<%@ LANGUAGE=VBScript%>
<% Option Explicit
  Response.Expires = 0 
%>
<!-- #include file="../errorhandler.asp" -->
<%
'  On Error Resume Next
  
  ' define variables and COM objects
  dim oADOStream, oXML, oXML_ECAD, oRoot, oFileDataNode, oCommentsNode
  dim oFile, oFiles
  dim i
  dim sURL


  Dim oApd, sDocId, sShopLocationID, sDocumentType, sSource, sUserID, sCSRNo
  Dim sEffectiveDate, sExpirationDate, sComments
'  response.write "Upload"
'  response.end
  
  ' Instantiate Upload Class
  'Set oApd = Server.CreateObject("LAPDEcadAccessorMgr.CData")
  
  CheckError()
  
'  ' create XMLDOM object and load it from request ASP object
  set oXML = Server.CreateObject("MSXML2.DOMDocument")
  oXML.load(request)
  
  'Get the Basic details
  set oRoot = oXML.selectSingleNode("/root")
  
  if not oRoot is nothing then
    sShopLocationID = oRoot.getAttribute("ShopLocationID")
    sDocumentType = oRoot.getAttribute("DocumentType")
    sUserID = oRoot.getAttribute("UserID")
    sEffectiveDate = oRoot.getAttribute("EffectiveDate")
    sExpirationDate = oRoot.getAttribute("ExpirationDate")
    sComments = oRoot.getAttribute("Comments")
  end if
  
  sCSRNo = request.ServerVariables("AUTH_USER")
  sSource = "User"
  
  ' retrieve XML node with binary content
  set oFiles = oXML.selectNodes("/root/File")
  
  

  sDocId = ""
  for i = 0 to oFiles.length - 1
    ' open stream object and store XML node content into it   
    ' create Stream Object
    set oADOStream = Server.CreateObject("ADODB.Stream")
    oADOStream.Type = 1  ' 1=adTypeBinary 
    oADOStream.open 
    oADOStream.Write oFiles(i).nodeTypedValue
    oADOStream.position = 0
    ' save uploaded file
   set oXML_ECAD = server.createObject("MSXML2.DOMDocument")
    oXML_ECAD.loadXML  "<ShopDocumentUpload>" & _
                            "<UserID>" & sUserID & "</UserID>" & _
                            "<ShopLocationID>" & sShopLocationID & "</ShopLocationID>" & _
                            "<DocumentType>" & sDocumentType & "</DocumentType>" & _
                            "<DocumentSource>" & sSource & "</DocumentSource>" & _
                            "<EffectiveDate>" & sEffectiveDate & "</EffectiveDate>" & _
                            "<ExpirationDate>" & sExpirationDate & "</ExpirationDate>" & _
                            "<Comments/>" & _
                            "<FileName>" & oFiles(i).getAttribute("FileName") & "</FileName>" & _
                            "<FileExtension>" & oFiles(i).getAttribute("FileExt") & "</FileExtension>" & _
                            "<FileLength>" & oFiles(i).getAttribute("FileLength") & "</FileLength>" & _
                            "<FileData/>" & _
                        "</ShopDocumentUpload>"
    
    'set comments
    set oCommentsNode = oXML_ECAD.selectSingleNode("//Comments")
    oCommentsNode.nodeTypedValue = sComments
    
    'set the FileData
    set oFileDataNode = oXML_ECAD.selectSingleNode("//FileData")
    oFileDataNode.dataType = "bin.base64"
    oFileDataNode.nodeTypedValue = oADOStream.Read(-1) ' Read All data
   
   
    oADOStream.close
    set oADOStream = nothing
'    oXML_ECAD.Save "c:\temp\upload\" & oFiles(i).getAttribute("FileName") & ".xml"
    ' upload the data to the LAPDEcadAccessorMgr
	Dim mexMonikerString , loStrXML , strResult         
	mexMonikerString = GetWebServiceURL()

	set loStrXML = GetObject(mexMonikerString)
    strResult = loStrXML.PutXML(oXML_ECAD.xml)

    if sDocId = "" then
      sDocId = cstr(strResult)  'oApd.PutXML(oXML_ECAD.xml)
    else
      sDocId = sDocId & ";" & cstr(strResult)  'oApd.PutXML(oXML_ECAD.xml)
    end if
    set oXML_ECAD = nothing
   
  next
 
  ' destroy COM object   
  set oADOStream = Nothing 
  set oXML = Nothing
  set oAPD = Nothing
 ' write message to browser
  Response.Write sDocId

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
%>
