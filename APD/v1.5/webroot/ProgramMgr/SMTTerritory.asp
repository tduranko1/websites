<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID

  strUserID = GetSession("UserID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTTerritoryGetDetailXML", "SMTTerritory.xsl" )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
