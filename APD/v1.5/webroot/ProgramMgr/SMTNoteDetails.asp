<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- SMTNoteDetails.asp
	This file handles both the Insert and Update for Notes.
	It is used in a popup dialog window.
-->

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<%
	On Error Resume Next

    Dim objExe
    Response.Write GetHtml()
    Set objExe = Nothing

    Call KillSessionObject
    Call CheckError

    Function GetHtml()

        'Extract the query data.
        Dim BeforeXML, Metadata, Reference, Action, strResult, DocumentID, ShopLocationID, UserID

        'BeforeXML = Request("BeforeXML")
        DocumentID = Request("DocumentID")
        Action = Request("Action")
        Reference = Application.Contents("NoteReference")
        Metadata = GetSession("NoteMetadata")
        ShopLocationID = Request("ShopLocationID")
        UserID = GetSession("UserID")
        'Pass in a blank note so the template will trigger a blank page layout.
        'If BeforeXML = "" Then BeforeXML = "<Note/>"

        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, UserID

        objExe.AddXslParam "WindowID", sWindowID

        'Handle Insert, Update and possibly Delete.
        If Action <> "" Then
                    
            Select Case Action
                Case "Insert"
                    objExe.ExecuteSpNp "dbo.uspSMTNoteInsDetail", Request.Form
                Case "Update"
                    objExe.ExecuteSpNp "dbo.uspSMTNoteUpdDetail", Request.Form
            End Select
                        
            strResult = "<html><head><script>dialogArguments.refreshNotesWindow(false, " + ShopLocationID + ", " + UserID + "); window.close();</script></head></html>"
            
        'Transform XML.
        Else
            objExe.AddXslParam "ShopLocationID", ShopLocationID
            If DocumentID = vbnullstring Then
              strResult = "<Root><Note/>" & CStr(Reference) & CStr(Metadata) & "</Root>"
              strResult = objExe.TransformXML( CStr(strResult), "SMTNoteDetails.xsl" )
            Else
              objExe.AddXslParam "DocumentID", DocumentID
              strResult = objExe.ExecuteSpAsXML( "uspSMTNoteGetDetailXML", "SMTNoteDetails.xsl", ShopLocationID, DocumentID )
            End If
        End If

        GetHtml = strResult

    End Function
%>
