<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim strAssignmentID
  Dim objExe, strHTML,  strUserID, strImageRootDir

  strUserID = GetSession("UserID")

  'Get values from the query sring
  strAssignmentID = Request("AssignmentID")
  CheckError()

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  strImageRootDir = objExe.Setting("Document/RootDirectory")
  CheckError()

  objExe.AddXslParam "ImageRootDir", strImageRootDir
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDEstimateGetListXML", "PMDEstimateList.xsl", strAssignmentID, strUserID )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0
%>
