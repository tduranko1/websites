<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next


  Dim objExe, strHTML, strUserID, sAddressZip, strInsuranceCoID,SearchFlag

  SearchFlag = 0	' 0 for Shop Distance Search
  
  'Get values from the query sring

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  
  sAddressZip = request("AddressZip")
  strInsuranceCoID = request("InsuranceCompanies")
  
  strHTML = objExe.ExecuteSpAsXML( "uspShopSearchbyDistanceXML", "SMTEntityDistanceSearchResults.xsl", sAddressZip , strInsuranceCoID, "P", 0, 0, SearchFlag )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0

  %>
