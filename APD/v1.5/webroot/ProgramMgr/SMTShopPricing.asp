<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strShopID

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strShopID = request("ShopID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
  select case Request.QueryString("mode")
  		case "wizard"
			objExe.AddXslParam "mode", "wizard"
			CheckError()
		case "insert"
			objExe.ExecuteSpNp "uspSMTShopPricingInsDetail", request.form
        	CheckError()
			if request("CallBack") <> vbnullstring then
				response.redirect(request("CallBack"))
			else
				Response.Redirect("SMTShopPricing.asp?ShopID=" & strShopID)
			end if
		case "update"
			objExe.ExecuteSpNp "uspSMTShopPricingUpdDetail", request.form
        	CheckError()
			if request("CallBack") <> vbnullstring then
				response.redirect(request("CallBack"))
			else
				Response.Redirect("SMTShopPricing.asp?ShopID=" & strShopID)
			end if
  end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTShopPricingGetDetailXML", "SMTShopPricing.xsl", strShopID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
