<%@ Language=VBScript %>
<% Option Explicit %>
<%  Response.buffer = true
    Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-16">
<TITLE>Shop Maintenance Wizard</TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/wizard.css" type="text/css"/>


<%

dim objExe, rs 
dim strShopID, strParams, strSysLastUserID, strBusinessID

strShopID = ""
strBusinessID = ""

if Request.QueryString("act") = "finish" then
	strParams = Request.Form("txtParms")
	strSysLastUserID = GetSession("UserID")
	strParams = strParams & "SysLastUserID=" & strSysLastUserID
	
	set objExe = createobject("DataPresenter.CExecute")
	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strSysLastUserID
		
	set rs = objExe.ExecuteSpNpAsRS("uspSMTWizard", strParams)
	CheckError()	
	
  if not rs.eof then
    strBusinessID = rs("BusinessInfoID")
    strShopID = rs("ShopID")
  end if
  
  set rs = nothing
  set objExe = nothing
end if

Call KillSessionObject

%>



<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/wizard.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  window.name = "WizWin";
  window.returnValue = "NONE";
	
  //parent.layerEntityData.style.visibility = "hidden";   // get rid of info tab
	var gsNewBusinessID = "<%=strBusinessID%>";
  var gsNewShopID = "<% =strShopID %>";  
  var gsMode = "<%=request.queryString("mode")%>";
  var gsAct = "<%=request.querystring("act")%>";
  
   
  //-----------------------------------------------------------
  if (gsAct == "finish"){  
    /*
    if (gsNewBusinessID != "")
      window.returnValue = "SMTDetailLevel.asp?SearchType=B&BusinessInfoID=" + gsNewBusinessID + "&ShopID=" + gsNewShopID;
    else if (gsNewShopID != "")
    	window.returnValue = "SMTDetailLevel.asp?SearchType=S&ShopID=" + gsNewShopID;      
    */
    
    window.returnValue = gsNewBusinessID + "|" + gsNewShopID;
    window.close();
  }
  
  var gsBusinessID = "<%=request.querystring("BusinessID")%>";
  var iWizPageCount = 5;
	var	gbDirtyFlag = false;  // Do not remove.  Placed here to avoid extra scripting and invalid refs in child pages.
	var curPage = 0;
  var wizFrms = new Array(0,0,0,0,0,0,0,0);
  
  function loadPages(){
		if (gsMode == "shop"){
			curPage = 2;
			wizfooterlabel.innerText = "Add Shop Wizard";
		}
    else
			wizfooterlabel.innerText = "Add Business Wizard";   
               
    fnNext();
  }
    
  function frameLoaded(i){
    wizFrms[i] = 1;
    preFillData(i);
  }
    
  function preFillData(i){
    try{
		  var wizFrame = null;
			var wizFrameSource = null;
      switch(i){
        case 2:
				  wizFrame = wizIframe2.document.all;
					wizFrameSource = wizIframe1.document.all;
          wizFrame.frmPersonnel.txtName1.focus();
            		
					wizFrame.txtPhoneAreaCode1.value = wizFrameSource.txtPhoneAreaCode.value;
          wizFrame.txtPhoneExchangeNumber1.value = wizFrameSource.txtPhoneExchangeNumber.value;
          wizFrame.txtPhoneExtensionNumber1.value = wizFrameSource.txtPhoneExtensionNumber.value;
          wizFrame.txtPhoneUnitNumber1.value = wizFrameSource.txtPhoneUnitNumber.value;
            		
					wizFrame.txtFaxAreaCode1.value = wizFrameSource.txtFaxAreaCode.value;
          wizFrame.txtFaxExchangeNumber1.value = wizFrameSource.txtFaxExchangeNumber.value;
          wizFrame.txtFaxExtensionNumber1.value = wizFrameSource.txtFaxExtensionNumber.value;
          wizFrame.txtFaxUnitNumber1.value = wizFrameSource.txtFaxUnitNumber.value;
          break;
        case 3:
          if (gsMode == "businfo"){
					  wizFrame = wizIframe3.document.all;
						wizFrameSource = wizIframe1.document.all;
            wizFrame.txtName.value = wizFrameSource.txtName.value;
            wizFrame.txtAddress1.value = wizFrameSource.txtAddress1.value;
            wizFrame.txtAddress2.value = wizFrameSource.txtAddress2.value;
            wizFrame.txtAddressCity.value = wizFrameSource.txtAddressCity.value;
            wizFrame.txtAddressZip.value = wizFrameSource.txtAddressZip.value;
            wizFrame.selState.value = wizFrameSource.selState.value;
            wizFrame.txtAddressCounty.value = wizFrameSource.txtAddressCounty.value;

            wizFrame.txtPhoneAreaCode.value = wizFrameSource.txtPhoneAreaCode.value;
            wizFrame.txtPhoneExchangeNumber.value = wizFrameSource.txtPhoneExchangeNumber.value;
            wizFrame.txtPhoneExtensionNumber.value = wizFrameSource.txtPhoneExtensionNumber.value;
            wizFrame.txtPhoneUnitNumber.value = wizFrameSource.txtPhoneUnitNumber.value;

            wizFrame.txtFaxAreaCode.value = wizFrameSource.txtFaxAreaCode.value;
            wizFrame.txtFaxExchangeNumber.value = wizFrameSource.txtFaxExchangeNumber.value;
            wizFrame.txtFaxExtensionNumber.value = wizFrameSource.txtFaxExtensionNumber.value;
            wizFrame.txtFaxUnitNumber.value = wizFrameSource.txtFaxUnitNumber.value;
              
            //program shop flag            
            if (wizFrameSource.rdProgramShop.checked == true){
              wizFrame.rdProgramShop.checked = true;
              wizFrame.txtProgramFlag.value = 1;
            }
                    
            if (wizFrameSource.rdNonProgramShop.checked == true){
              wizFrame.rdNonProgramShop.checked = true;  
              wizFrame.txtProgramFlag.value = 0;      
            }
            
            //referral shop flag
            if (wizFrameSource.rdReferralShop.checked == true){
              wizFrame.rdReferralShop.checked = true;
              wizFrame.txtReferralFlag.value = 1;
            }
                    
            if (wizFrameSource.rdNonReferralShop.checked == true){
              wizFrame.rdNonReferralShop.checked = true;  
              wizFrame.txtReferralFlag.value = 0;      
            }   
            
          }
          break;
        case 4:
          if (gsMode == "businfo"){
						wizFrame = wizIframe4.document.all;
						wizFrameSource = wizIframe2.document.all;
            wizFrame.txtName1.value = wizFrameSource.txtName1.value;
            wizFrame.txtEmailAddress1.value = wizFrameSource.txtEmailAddress1.value;
            wizFrame.txtName2.value = wizFrameSource.txtName2.value;
            wizFrame.txtEmailAddress2.value = wizFrameSource.txtEmailAddress2.value;
                		
            //Contact1
            /*Phone information*/ 
            wizFrame.txtPhoneAreaCode1.value = wizFrameSource.txtPhoneAreaCode1.value;
            wizFrame.txtPhoneExchangeNumber1.value = wizFrameSource.txtPhoneExchangeNumber1.value;
            wizFrame.txtPhoneUnitNumber1.value = wizFrameSource.txtPhoneUnitNumber1.value;
            wizFrame.txtPhoneExtensionNumber1.value = wizFrameSource.txtPhoneExtensionNumber1.value;
            /*Fax information*/ 
            wizFrame.txtFaxAreaCode1.value = wizFrameSource.txtFaxAreaCode1.value;
            wizFrame.txtFaxExchangeNumber1.value = wizFrameSource.txtFaxExchangeNumber1.value;
            wizFrame.txtFaxUnitNumber1.value = wizFrameSource.txtFaxUnitNumber1.value;
            wizFrame.txtFaxExtensionNumber1.value = wizFrameSource.txtFaxExtensionNumber1.value;
            /*Pager information*/ 
            wizFrame.txtPagerAreaCode1.value = wizFrameSource.txtPagerAreaCode1.value;
            wizFrame.txtPagerExchangeNumber1.value = wizFrameSource.txtPagerExchangeNumber1.value;
            wizFrame.txtPagerUnitNumber1.value = wizFrameSource.txtPagerUnitNumber1.value;
            wizFrame.txtPagerExtensionNumber1.value = wizFrameSource.txtPagerExtensionNumber1.value;
            /*Cell information*/ 
            wizFrame.txtCellAreaCode1.value = wizFrameSource.txtCellAreaCode1.value;
            wizFrame.txtCellExchangeNumber1.value = wizFrameSource.txtCellExchangeNumber1.value;
            wizFrame.txtCellUnitNumber1.value = wizFrameSource.txtCellUnitNumber1.value;
            wizFrame.txtCellExtensionNumber1.value = wizFrameSource.txtCellExtensionNumber1.value;
                		
            //Contact2
            /*Phone information*/ 
            wizFrame.txtPhoneAreaCode2.value = wizFrameSource.txtPhoneAreaCode2.value;
            wizFrame.txtPhoneExchangeNumber2.value = wizFrameSource.txtPhoneExchangeNumber2.value;
            wizFrame.txtPhoneUnitNumber2.value = wizFrameSource.txtPhoneUnitNumber2.value;
            wizFrame.txtPhoneExtensionNumber2.value = wizFrameSource.txtPhoneExtensionNumber2.value;
            /*Fax information*/ 
            wizFrame.txtFaxAreaCode2.value = wizFrameSource.txtFaxAreaCode2.value;
            wizFrame.txtFaxExchangeNumber2.value = wizFrameSource.txtFaxExchangeNumber2.value;
            wizFrame.txtFaxUnitNumber2.value = wizFrameSource.txtFaxUnitNumber2.value;
            wizFrame.txtFaxExtensionNumber2.value = wizFrameSource.txtFaxExtensionNumber2.value;
            /*Pager information*/ 
            wizFrame.txtPagerAreaCode2.value = wizFrameSource.txtPagerAreaCode2.value;
            wizFrame.txtPagerExchangeNumber2.value = wizFrameSource.txtPagerExchangeNumber2.value;
            wizFrame.txtPagerUnitNumber2.value = wizFrameSource.txtPagerUnitNumber2.value;
            wizFrame.txtPagerExtensionNumber2.value = wizFrameSource.txtPagerExtensionNumber2.value;
            /*Cell information*/ 
            wizFrame.txtCellAreaCode2.value = wizFrameSource.txtCellAreaCode2.value;
            wizFrame.txtCellExchangeNumber2.value = wizFrameSource.txtCellExchangeNumber2.value;
            wizFrame.txtCellUnitNumber2.value = wizFrameSource.txtCellUnitNumber2.value;
            wizFrame.txtCellExtensionNumber2.value = wizFrameSource.txtCellExtensionNumber2.value;
                		
            /*Preferred Contact Method information*/
            wizFrame.selPreferredContactMethodID1.value = wizFrameSource.selPreferredContactMethodID1.value;
            wizFrame.selPreferredContactMethodID2.value = wizFrameSource.selPreferredContactMethodID2.value;
            wizFrame.cbShopManagerFlag1.checked = wizFrameSource.cbShopManagerFlag1.checked;
            wizFrame.cbShopManagerFlag2.checked = wizFrameSource.cbShopManagerFlag2.checked;
          }
          else{
						wizFrame = wizIframe4.document.all;
						wizFrameSource = wizIframe3.document.all;
            wizFrame.txtEmailAddress1.value = wizFrameSource.txtEmailAddress.value;
            /*Phone information*/ 
            wizFrame.txtPhoneAreaCode1.value = wizFrameSource.txtPhoneAreaCode.value;
            wizFrame.txtPhoneExchangeNumber1.value = wizFrameSource.txtPhoneExchangeNumber.value;
            wizFrame.txtPhoneUnitNumber1.value = wizFrameSource.txtPhoneUnitNumber.value;
						wizFrame.txtPhoneExtensionNumber1.value = wizFrameSource.txtPhoneExtensionNumber.value;
            /*Fax information*/ 
            wizFrame.txtFaxAreaCode1.value = wizFrameSource.txtFaxAreaCode.value;
            wizFrame.txtFaxExchangeNumber1.value = wizFrameSource.txtFaxExchangeNumber.value;
            wizFrame.txtFaxUnitNumber1.value = wizFrameSource.txtFaxUnitNumber.value;
						wizFrame.txtFaxExtensionNumber1.value = wizFrameSource.txtFaxExtensionNumber.value;
          }
          break;                
        case 5:
					wizFrame = wizIframe5.document.all;
					wizFrameSource = wizIframe3.document.all
          wizFrame.txtName.value = wizFrameSource.txtName.value;
          wizFrame.txtAddress1.value = wizFrameSource.txtAddress1.value;
          wizFrame.txtAddress2.value = wizFrameSource.txtAddress2.value;
          wizFrame.txtAddressCity.value = wizFrameSource.txtAddressCity.value;
          wizFrame.txtAddressZip.value = wizFrameSource.txtAddressZip.value;
          wizFrame.selState.value = wizFrameSource.selState.value;
          wizFrame.txtAddressCounty.value = wizFrameSource.txtAddressCounty.value;
            
          /* Contact Owner #1 - Phone information*/ 
          wizFrame.txtPhoneAreaCode.value = wizFrameSource.txtPhoneAreaCode.value;
          wizFrame.txtPhoneExchangeNumber.value = wizFrameSource.txtPhoneExchangeNumber.value;
          wizFrame.txtPhoneUnitNumber.value = wizFrameSource.txtPhoneUnitNumber.value;
          wizFrame.txtPhoneExtensionNumber.value = wizFrameSource.txtPhoneExtensionNumber.value;
          /*Fax information*/ 
          wizFrame.txtFaxAreaCode.value = wizFrameSource.txtFaxAreaCode.value;
          wizFrame.txtFaxExchangeNumber.value = wizFrameSource.txtFaxExchangeNumber.value;
          wizFrame.txtFaxUnitNumber.value = wizFrameSource.txtFaxUnitNumber.value;
          wizFrame.txtFaxExtensionNumber.value = wizFrameSource.txtFaxExtensionNumber.value;
          wizFrame.txtName.focus()
          break;
        case 6:
					wizIframe6.document.all.txtSheetMetal.focus()
					break;
				case 7:
					wizIframe7.document.all.txtDomestic.focus()
					break;    
      }
    }
    catch(e){};
  }
    
  function fnNext(){
    var wizFrame = null;
    
    if ((gsMode == "businfo" && curPage > 0) || (gsMode == "shop" && curPage > 2))
      if (!document.frames["wizIframe" + curPage].ValidatePage()) return false;
    
    if (curPage < iWizPageCount){
      curPage++;
      switch(curPage){
        case 1:
					if (wizFrms[1] == 0){
            wizIframe1.frameElement.src = "SMTBusinessInfo.asp?mode=wizard";
            wizFrms[1] = 1;
          }
          wizIframe1.frameElement.style.display = "inline";
          break;
        case 2:
					wizIframe1.frameElement.style.display = "none";
          if (wizFrms[2] == 0)
            wizIframe2.frameElement.src = "SMTPersonnel.asp?mode=wizard&EntityID=0&EntityType=B";
          
          wizIframe2.frameElement.style.display = "inline";
          break;
        case 3:
					var sEntityType = gsMode == "businfo" ? "B" : "S";
					var sBusInfoID = gsMode == "businfo" ? 0 : gsBusinessID;
					wizIframe2.frameElement.style.display = "none";
          if (wizFrms[3] == 0)
            wizIframe3.frameElement.src = "SMTShopInfo.asp?mode=" + gsMode + "&EntityType=" + sEntityType + "&BusinessInfoID=" + sBusInfoID;
           
          wizIframe3.frameElement.style.display = "inline";
          break;
       case 4:
          wizIframe3.frameElement.style.display = "none";
          if (wizFrms[4] == 0)
             wizIframe4.frameElement.src = "SMTPersonnel.asp?mode=wizard&EntityID=0&EntityType=S";
                 
          wizIframe4.frameElement.style.display = "inline";
          break;
        case 5:
          wizIframe4.frameElement.style.display = "none";
           if (wizFrms[5] == 0)
             wizIframe5.frameElement.src = "SMTShopBilling.asp?mode=wizard";
              
           wizIframe5.frameElement.style.display = "inline";
           break;
      }
    }
    checkNav();
  }
    
  function fnBack(){
    if (curPage > 0){
      curPage--;
      switch(curPage){
        case 1:
          wizIframe2.frameElement.style.display = "none";
          if (wizFrms[1] == 0)
            wizIframe1.frameElement.src = "SMTBusinessInfo.asp?mode=wizard";
                
          wizIframe1.frameElement.style.display = "inline";
          break;
        case 2:
          wizIframe3.frameElement.style.display = "none";
          if (wizFrms[2] == 0)
            wizIframe2.frameElement.src = "SMTPersonnel.asp?mode=wizard&EntityID=0&EntityType=B";
                 
          wizIframe2.frameElement.style.display = "inline";
          break;
        case 3:
          wizIframe4.frameElement.style.display = "none";
          if (wizFrms[3] == 0)
            wizIframe3.frameElement.src = "SMTShopInfo.asp?mode=wizard";
                   
          wizIframe3.frameElement.style.display = "inline";
          break;
        case 4:
          wizIframe5.frameElement.style.display = "none";
          if (wizFrms[4] == 0)
            wizIframe4.frameElement.src = "SMTPersonnel.asp?mode=wizard&EntityID=0&EntityType=S";
                    
          wizIframe4.frameElement.style.display = "inline";
          break;
        case 5:
					wizIframe6.frameElement.style.display = "none";
          if (wizFrms[5] == 0)
            wizIframe5.frameElement.src = "SMTShopBilling.asp?mode=wizard";
                   
          wizIframe5.frameElement.style.display = "inline";
          break;                  
      }
    }
    checkNav();
  }
    
  function fnCancel(){
    window.close();
		//window.navigate(top.CancelURL());
	}
    
  function checkNav(){
		
    if (gsMode == "businfo")
      Back.disabled = !(curPage > 1);
    else
      Back.disabled = !(curPage > 3);
            
    Next.disabled = !(curPage < iWizPageCount);
    wizButtonFinish.disabled = !(curPage == iWizPageCount);
        
    switch(curPage){
      case 1:
        wizheader.innerText = "Step 1 of 5 - Business Information";
        break;
      case 2:
        wizheader.innerText = "Step 2 of 5 - Business Personnel";
        break;
      case 3:
        if (gsMode == "businfo")
          wizheader.innerText = "Step 3 of 5 - Shop Information";
        else
          wizheader.innerText = "Step 1 of 3 - Shop Information";
				break;
      case 4:
        if (gsMode == "businfo")
          wizheader.innerText = "Step 4 of 5 - Shop Personnel";
        else
          wizheader.innerText = "Step 2 of 3 - Shop Personnel";
				break;
      case 5:
        if (gsMode == "businfo")
          wizheader.innerText = "Step 5 of 5 - Shop Payment";
        else
          wizheader.innerText = "Step 3 of 3 - Shop Payment";
				break;
    }
  }
    	
	function fnFinish(){
    
    if (fnNext() == false) return false;
    
    wizButtonFinish.disabled = true;
    
    var sParams = "";
		
		for (i = gsMode == "businfo" ? 1 : 3; i <= iWizPageCount; i++)
			sParams += BuildParams("wizIframe" + i);		
							
		finishForm.txtParms.value = sParams;
		finishForm.action += "?act=finish&mode=" + gsMode;
		finishForm.submit();
  }
	
    function BuildParams(sFrame){
		var sParams = "";
		var elms = eval(sFrame + ".document.all.tags('INPUT')");
		
        for(var i = 0; i < elms.length; i++){
    
            if (elms[i].wizname)
            sParams += elms[i].wizname + "=" + escape(elms[i].value) + "&";
        }
		
		elms = eval(sFrame + ".document.all.tags('SELECT')");
        for(var i = 0; i < elms.length; i++){

            if (elms[i].wizname)
            sParams += elms[i].wizname + "=" + escape(elms[i].value) + "&";
        }
        
        elms = eval(sFrame + ".document.all.tags('TEXTAREA')");
        for(var i = 0; i < elms.length; i++){
            
            if (elms[i].wizname)
                sParams += elms[i].wizname + "=" + escape(elms[i].value) + "&";
        }
        
		return sParams;	
	}
        
</SCRIPT>


</HEAD>

<BODY class="bodyAPDsub" unselectable="on" style="background:transparent;border:0px;margin-top:0;margin-bottom:0;" onLoad="loadPages()"><!-- onLoad="wizardInit("true"); initPage();" -->
	
	<form id="finishForm" name="finishForm" method="post" target="WizWin"><input type="hidden" id="txtParms" name="txtParms"/></form>
  
  <div id='wizheader' class='wizheader' style="width:725px;"></div>
    <div id='wizframecontainer' class='wizframes' style='width:725px; height:475px;' >	
    	<DIV id="shopwizard" class="wizard" title="Shop Add Wizard" style="display:none">
    		<SPAN class="wiz1" wizpage="ShopAdd.asp" >Business Information</SPAN>	
    		<SPAN class="wiz1" wizpage="ShopAddPersonnel.asp">Business Personnel</SPAN>
    		<SPAN class="wiz1" wizpage="LocationAdd.asp">Shop Information</SPAN>
    		<SPAN class="wiz1" wizpage="LocationAddPersonnel.asp">Shop Personnel</SPAN>
    		<SPAN class="wiz1" wizpage="LocationBillingAdd.asp">Shop Payment</SPAN>		
    		<SPAN class="wiz1" wizpage="LocationPricingAdd.asp">Shop Pricing</SPAN>
    		<SPAN class="wiz1" wizpage="LocationPricingOEMDiscountAdd.asp">Shop OEM Discount</SPAN>
    	</DIV>
      
    	<iframe src="../blank.asp" id="wizIframe1" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe2" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe3" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe4" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe5" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe6" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
      <iframe src="../blank.asp" id="wizIframe7" style="width:720px;height:475px;display:none" allowtransparency="true"></iframe>
    </div>
    
  	<div id="wizfooter" class="wizfooter" style="position:absolute;width:725px">
  	<table width="95%" align="center">
      <tr>
        <td width="40%"><span id="wizfooterlabel" class="wizfooterlabel"></span></td>
        <td><input type="Button" name="Cancel" value="Cancel" class="formbutton" title="Cancel" onclick="fnCancel()"></td>
        <td><input type="Button" name="Back" value="< Back" disabled class="formbutton" title="< Back" onclick="fnBack()"></td>
        <td><input type="Button" name="Next" value="Next >" class="formbutton" title="Next >" onclick="fnNext()"></td>	
        <td><input type="Button" name="wizButtonFinish" id="wizButtonFinish" value="Finish" disabled class="formbutton" title="Finish" onclick="fnFinish()"></td>
  	  </tr>
    </table>
  </div>
</BODY>
</HTML>

<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
