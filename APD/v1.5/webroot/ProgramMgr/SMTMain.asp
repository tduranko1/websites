<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
  Dim strHTML, objExe, sUserID, sEntityID, sPageID

	On Error Resume Next

	ReadSessionKey()

	sUserID = GetSession("UserID")
    sEntityID = request("EntityID")
    sPageID = request("PageID")
  
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sUserID
	CheckError()

	objExe.AddXslParam "PageID", sPageID
    CheckError()
  
    if sEntityID <> "" then
        objExe.AddXslParam "EntityID", sEntityID
        CheckError()
    end if
  
	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSMTShopSearchReferenceGetListXML", "SMTMain.xsl",  sPageID)
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

