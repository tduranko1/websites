<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strShopID, mode

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strShopID = request("ShopID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  mode = request("mode")
  select case mode
  		case "businfo", "shop"
			objExe.AddXslParam "mode", mode
			CheckError()
			objExe.AddXslParam "EntityType", request("EntityType")
			CheckError()
			objExe.AddXslParam "BusinessInfoID", request("BusinessInfoID")
			CheckError()
		case "delete"
			objExe.ExecuteSpNp "uspSMTShopInfoDel", request.form
        	CheckError()
        	objExe.AddXslParam "Deleted", "yes"
			CheckError()
			objExe.AddXslParam "BusinessInfoID", request("BusinessInfoID")
			CheckError()
        case "update"
		   	'response.write request("ShopID")
			'response.end
			objExe.ExecuteSpNp "uspSMTShopInfoUpdDetail", request.form
        	CheckError()
			if request("CallBack") <> vbnullstring then
				response.redirect(request("CallBack"))
			else
				Response.Redirect("SMTShopInfo.asp?ShopID=" & strShopID)
			end if
  end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTShopInfoGetDetailXML", "SMTShopInfo.xsl", strShopID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
