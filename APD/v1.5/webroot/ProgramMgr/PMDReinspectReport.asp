<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strDocumentID, strSupervisorForm

  strUserID = GetSession("UserID")

  'Get values from the query sring
  strDocumentID = Request("DocumentID")
  strSupervisorForm = Request("SupForm")

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDReinspectFormGetDetailXML", "PMDReinspectReport.xsl", strDocumentID, strSupervisorForm, 0 )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0

%>
