<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strEntityID, strSearchType, strBusinessInfoID, strShopID, strMode

  On Error Resume Next
  
  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  'strEntityID = request("EntityID")
  strBusinessInfoID = request("BusinessInfoID")
  strShopID = request("ShopID")
  strSearchType = request("SearchType")
  strMode = request("mode")
      
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
  
  objExe.AddXslParam "mode", strMode
  CheckError()
  	
  strHTML = objExe.ExecuteSpAsXML( "uspSMTDetailLevelGetListXML", "SMTDetailLevel.xsl", strBusinessInfoID, strShopID, strSearchType )
  CheckError()

  Set objExe = Nothing
 
  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
