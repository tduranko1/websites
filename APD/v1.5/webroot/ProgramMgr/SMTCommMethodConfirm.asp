<%@ Language=VBScript %>
<% option explicit
   Response.Expires = -1
%>

<!-- #include file="../errorhandler.asp" -->
<%
dim objData, strHTML

set objData = CreateObject("DataPresenter.CExecute")

objData.Initialize Request.ServerVariables.Item("APPL_PHYSICAL_PATH")
CheckError()

strHTML = objData.ExecuteSpAsXML( "uspSMTDupCommInfoGetListXML", "SMTCommMethodConfirm.xsl", request.QueryString("ShopID"), request.QueryString("Method"), request.QueryString("Addr"))

  response.write strHTML
%>
