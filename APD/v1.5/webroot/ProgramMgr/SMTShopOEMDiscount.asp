<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strShopID, strUpdates

  strUserID = GetSession("UserID")
  strUpdates = ""
  
  'Get values from the query sring
  strShopID = request("ShopID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
  select case Request.QueryString("mode")
  	case "wizard"
		  objExe.AddXslParam "mode", "wizard"
      CheckError()
  	case "update"
	 	  dim i, discount
	
  		for i = 1 to Request.Form("VehicleMakeID").count
  			discount = Request.Form("DiscountPct")(i)
  			if discount <> vbnullstring and discount <> "0" then
  				strUpdates = strUpdates & Request.Form("VehicleMakeID")(i) & "," & discount & ";"
          'objExe.ExecuteSp "uspSMTShopOEMDiscountInsDetail", strShopID, Request.Form("VehicleMakeID")(i), discount, strUserID
  				CheckError()
        end if
      next		
      
      objExe.ExecuteSp "uspSMTShopOEMDiscountUpdDetail", strShopID, strUpdates, strUserID
      
      'response.write strUpdates
      'response.end
      
  		if request("CallBack") <> vbnullstring then
  			response.redirect(request("CallBack"))
  		else
  			Response.Redirect("SMTShopOEMDiscount.asp?ShopID=" & strShopID)
  		end if
  end select
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTShopOEMDiscountGetDetailXML", "SMTShopOEMDiscount.xsl", strShopID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
