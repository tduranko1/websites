<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
    On Error Resume Next
    Dim objExe
       
    
    Call GetNotesList
    Call KillSessionObject
    Call CheckError

    Set objExe = Nothing

    Private Sub GetNotesList()

        Dim strShopLocationID
        Dim strUserID
        Dim strWindowState
        Dim strHTML
        Dim strImageRootDir

        strShopLocationID = Request("ShopLocationID")
        
        
        If strShopLocationID = "" Then 
          Response.write "Empty Shop Location ID. Please contact Help desk"
          Response.end
        end if

        strUserID = Request("UserID")
        If strUserID = "" Then
            strUserID = GetSession("UserID")
        End If
        
        strWindowState = Request("winState")
        
        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
        
        strImageRootDir = objExe.Setting("Document/ShopRootDirectory")
        CheckError()
    
        objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "ShopLocationID", strShopLocationID
        objExe.AddXslParam "UserID", strUserID
        objExe.AddXslParam "windowState", strWindowState

        
        
        strHTML = objExe.ExecuteSpAsXML( "uspSMTDocumentGetListXML", "SMTDocuments.xsl", strShopLocationID )

        Response.Write strHTML

    End Sub
%>

