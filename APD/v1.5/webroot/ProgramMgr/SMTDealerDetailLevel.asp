<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
dim sDealerID, sMode

sDealerID = request("DealerID")
sMode = request("mode")

%>

<HEAD>
  <TITLE>Shop Maintenance</TITLE>
  
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>

<!-- Page Specific Scripting -->


<!-- these global variables maintain state information for the various pages loaded into the content frame on this page -->
<!-- javascript variable notes:
  gsPageID - used to determine into what parent page SMTSearch is loaded
  gsPageName

-->
<script language="javascript">

var gbDirtyFlag = false;
var gsMode = "<%=sMode%>";
var gsDealerID = "<%=sDealerID%>";
var gsPageName;
var gsCallBack;
var gsPage;

window.returnValue = "NONE";

// search parameters
var gsID = "";
var gsSSN = "";
var gsName = "";
var gsCity = "";
var gsState = "";
var gsPhoneAreaCode = "";
var gsPhoneExchange = "";
var gsPhoneUnit = "";
var gsScroll;
var gsFramePageFile = "SMTDealerDetailLevel.asp";

function initPage(){
  
	if (gsMode == "wizard"){
		tab12.style.visibility = "hidden";
		tab13.style.visibility = "hidden";
	}
	else{
		top.gsFramePageFile = gsFramePageFile;
		if (top.gbInfo) top.SetInfoHeader("  Dealer ID:", top.gsDealerID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone);
	}
	
	//if (top.gsOrigin == "wiz"){
	//	top.gsOrigin = "";
	//	TabChange(top.gsPageFile);
	//}
	//else
		TabChange(tab11.page);		
}

function MenuCheckDirty(){
    if (gbDirtyFlag) {
		var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to return to the page and save the changes?");
        if (sSave == "Yes") return false;        
        else gbDirtyFlag = false;
    }
    return true;
 }

function CheckDirty(){
    if (gbDirtyFlag) {
		var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to save the changes?");
        if (sSave == "Yes") return ifrmContent.IsDirty();
    }
	gbDirtyFlag = false;
    return true;
}

function TabChange(sPage){
	var obj;
    				
	tab11.className = "tab1";
  tab12.className = "tab1";
	tab13.className = "tab1";
		
	var sHREF = sPage + "?DealerID=" + gsDealerID;
  
  switch(sPage){
    case "SMTMain.asp":
      sHREF += "&PageID=DealerAddBusiness&EntityID=" + gsDealerID;
      btnSave.disabled = "true";
      btnDelete.style.visibility = "hidden";
      break;
    case "SMTDealerBusinessList.asp":
      btnSave.disabled = "true";
      btnDelete.style.visibility = "hidden";
      break;
  }
  gsPage = sPage;
	
	if (ifrmContent.document.all.txtCallBack){
		ifrmContent.document.all.txtCallBack.value = sHREF;
	}
	
	if (CheckDirty()){		
    	document.frames["ifrmContent"].frameElement.src = sHREF;
   		obj = document.getElementById("ifrmContent");
   		obj.style.visibility = "visible";
   		obj.style.display = "inline";
	}
}
	
function IsDirty(){
	if (gbDirtyFlag){ 
		ifrmContent.DirtyPage();
		gbDirtyFlag = false;	
	}
}

if (gsMode != "wizard"){  
  if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;
}


</script>

</HEAD>


<BODY class="bodyAPDSub" unselectable="on" onload="initPage()" bgcolor="#FFFAEB">

  <!-- Tabs -->
  <DIV unselectable="on" class="content1" id="content11" style="position:absolute; left:3px; top:19px; width:747; height:518;">
    <DIV unselectable="on" id="DealerTabs" class="apdtabs" style="position:absolute; left:5; top:4; visibility:visible;">
      <SPAN unselectable="on" id="tab11" page="SMTDealerInfo.asp" class="tabactive1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Dealer</SPAN>
	    <SPAN unselectable="on" id="tab12" page="SMTDealerBusinessList.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Owned Businesses</SPAN>
	    <SPAN unselectable="on" id="tab13" page="SMTMain.asp" class="tab1" onclick="TabChange(this.page)" style="cursor:hand; margin-left:0px;">Add Businesses</SPAN>
    </DIV>
  
    <!-- action buttons -->
    <DIV align="right" style="position:absolute; left:500; top:3; width:235; height:16;">
      <input type="button" id="btnCancel" value="Cancel" class="formButton" style="width:45; visibility:hidden;" onclick="ifrmContent.Cancel()"/>
      <input type="button" id="btnDelete" value="Delete" class="formButton" style="width:45; display:none;" onclick="ifrmContent.Delete()"/>
      <input type="button" id="btnSave" value="Save" class="formButton" style="width:45" onclick="ifrmContent.Save()"/>
    </DIV>
    
    <!-- content frame -->
    <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:24px;">
      <IFRAME id="ifrmContent" src="../blank.asp" style="position:relative; width:720; height:475; border:0px; background:transparent; overflow:hidden" allowtransparency="true" >
      </IFRAME>
    </DIV>
 </DIV>
</BODY>
</HTML>