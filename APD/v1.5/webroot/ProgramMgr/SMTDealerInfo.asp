<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strDealerID

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strDealerID = request("DealerID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
  select case request("mode")
    case "add"
  		dim rs
  		set rs = objExe.ExecuteSpNpAsRS("uspSMTDealerInsDetail", request.form)
  		CheckError()
  		strDealerID = rs("DealerID")
  		objExe.AddXslParam "mode", "edit"
  		CheckError()
	  case "update" 
	    objExe.ExecuteSpNp "uspSMTDealerUpdDetail", request.form
		  CheckError()
  		if request("CallBack") <> vbnullstring then
  			response.redirect(request("CallBack"))
  		else
  			response.redirect "SMTDealerInfo.asp?DealerID=" + request("DealerID")
  		end if
    case "delete"
      objExe.ExecuteSpNp "uspSMTDealerDel", request.form
      CheckError()
      objExe.AddXslParam "Deleted", "yes"
      CheckError()
  end select
    
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTDealerGetDetailXML", "SMTDealerInfo.xsl", strDealerID )
  CheckError()

  If Not RS Is Nothing Then
    If RS.State = 1 Then 
      RS.Close
    end if
    Set RS = Nothing
  End If

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
