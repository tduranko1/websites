<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
    Dim strHTML, objExe

	  On Error Resume Next

    Set objExe = InitializeSession()
    CheckError()

    objExe.AddXslParam "Environment", objExe.mToolkit.mEvents.mSettings.Environment
    CheckError()

    objExe.AddXslParam "ShopID", request("ShopID")
    CheckError()

    objExe.AddXslParam "AssignmentID", request("AssignmentID")
    CheckError()

    objExe.AddXslParam "BeginDate", request("BeginDate")
    CheckError()
    
	  'Get PMD XML and transform to HTML.
	  strHTML = objExe.ExecuteSpAsXML( "uspSessionGetUserDetailXML", "PMD.xsl", LCase(strCsrID))
	  CheckError()

	  Set objExe = Nothing

	  Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>

