<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next


  Dim objExe, strHTML, strUserID

  strUserID = GetSession("UserID")

  'Get values from the query sring

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
  
  dim sShopLoadID, sAddressCity, sAddressState, sAddressZip, sCertifiedFirstID, sFedTaxID, sName, sPhoneAreaCode, sPhoneExchange, sPhoneUnit
  dim sProgramFlag
  
  sShopLoadID = request("ShopLoadID")
  sFedTaxID = request("FedTaxID")
  sName = request("Name")
  sAddressCity = request("AddressCity")
  sAddressState = request("AddressState")
  sAddressZip = request("AddressZip")
  sCertifiedFirstID = request("CertifiedFirstID")
  sPhoneAreaCode = request("PhoneAreaCode")
  sPhoneExchange = request("PhoneExchange")
  sPhoneUnit = request("PhoneUnit")
  sProgramFlag = request("ProgramFlag")
  
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTWebSignupGetListXML", "SMTWebSignupSearchResults.xsl", sShopLoadID, sAddressCity, sAddressState, sAddressZip, sCertifiedFirstID, sFedTaxID, sName, sPhoneAreaCode, sPhoneExchange, sPhoneUnit, sProgramFlag)
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0

  %>
