<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strBusinessInfoID

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strBusinessInfoID = request("BusinessInfoID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
   select case request("mode")
    case "wizard"
		objExe.AddXslParam "mode", "wizard"
		CheckError()
  	case "update" 
	    dim sParms
		objExe.ExecuteSpNp "uspSMTBusinessInfoUpdDetail", request.form
		CheckError()
		if request("CallBack") <> vbnullstring then
			response.redirect(request("CallBack"))
		else
			response.redirect "SMTBusinessInfo.asp?BusinessInfoID=" + request("BusinessInfoID")
		end if
    case "delete"
        objExe.ExecuteSpNp "uspSMTBusinessInfoDel", request.form
        CheckError()
        objExe.AddXslParam "Deleted", "yes"
		CheckError()
    end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTBusinessInfoGetDetailXML", "SMTBusinessInfo.xsl", strBusinessInfoID )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0
%>
