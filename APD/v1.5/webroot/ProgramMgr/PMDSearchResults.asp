<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim strProgramManagerUserID, strShopName, strShopCity, strShopState, strShopZip
  Dim objExe, strHTML

  'Get values from the query sring
  strProgramManagerUserID = Request("ProgramManagerUserID")
  strShopName = CleanDataElementwithMaxLength(Request("ShopName"), 50)
  strShopCity = CleanDataElementwithMaxLength(Request("ShopCity"), 50)
  strShopState = Request("ShopState")
  strShopZip = Request("ShopZip")
  CheckError()


  ReadSessionKey()
  strProgramManagerUserID = GetSession("UserID")

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strProgramManagerUserID
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDShopSearchGetListXML", "PMDSearchResults.xsl", strProgramManagerUserID, strShopName, strShopCity, strShopState, strShopZip )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0

  Function CleanDataElementwithMaxLength(DataElement, maxLen)
    dim idx, sVal
    dim iLen, sRet
    idx = 1
    sRet = ""
    sVal = Replace(DataElement,"""","")
    sVal = Replace(sVal,"<script>","")
    sVal = Replace(sVal,"</script>","")

    iLen = len(sVal)
    for idx = 1 to iLen
        if mid(sVal, idx, 1) = "'" then
            if len(sRet) < maxLen - 1 then
                sRet = sRet + "''"
            else
                exit for
            end if
        else
            if len(sRet) < maxLen then
                sRet = sRet + mid(sVal, idx, 1)
            else
                exit for
            end if
        end if
    next
    CleanDataElementwithMaxLength = sRet
End Function
%>
