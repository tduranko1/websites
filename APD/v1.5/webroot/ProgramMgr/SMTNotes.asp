<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
    On Error Resume Next
    Dim objExe
       
    
    Call GetNotesList
    Call KillSessionObject
    Call CheckError

    Set objExe = Nothing

    Private Sub GetNotesList()

        Dim strShopLocationID
        Dim strUserID
        Dim strWindowState
        Dim strHTML

        strShopLocationID = Request("ShopLocationID")
        
        
        If strShopLocationID = "" Then strShopLocationID = 0

        strUserID = Request("UserID")
        If strUserID = "" Then
            strUserID = GetSession("UserID")
        End If
        
        strWindowState = Request("winState")
        
        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
        
        objExe.AddXslParam "ShopLocationID", strShopLocationID
        
        
        objExe.AddXslParam "UserID", strUserID
        objExe.AddXslParam "windowState", strWindowState

        
        
        strHTML = objExe.ExecuteSpAsXML( "uspSMTNoteGetDetailXML", "SMTNotes.xsl", strShopLocationID )

        Response.Write strHTML

        'Extract the reference data from the result.
        'Stuff it into the application object.
        'Replace all this crap later with reference data object (What was that?  Anyone say HACK?).

        Const sStart = "<xml id=""Reference"">"
        Const sEnd = "</xml>"

        Dim iStart, iEnd

        iStart = InStr( 1, strHTML, sStart, 1 )

        If iStart > 1 Then
            iStart = iStart + Len(sStart)
            iEnd = InStr( iStart, strHTML, sEnd, 1 )
            If iEnd > iStart Then
                Application.Contents( "NoteReference" ) = Mid( strHTML, iStart, iEnd - iStart )
            End If
        End If

    End Sub
%>

