<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML, strUserID, strDealerID, strUpdates

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strDealerID = request("DealerID")(1)
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
  
  dim x, i
  strUpdates = ""
  
  if request("mode") = "save" then
  	for i = 1 to Request.Form("chkBusinessInfo").count
  		x = request.form("chkBusinessInfo")(i)
      strUpdates = strUpdates & Request.Form("BusinessInfoID")(x) & ","
  	next
    
    objExe.ExecuteSp "uspSMTShopDealerDel", strUpdates, Request.Form("DealerID")(x), Request.Form("SysLastUserID")(x)
    CheckError()
      
    if request("CallBack") <> vbnullstring then
      response.redirect(request("CallBack"))
		end if
  end if
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTDealerBusinessGetListXML", "SMTDealerBusinessList.xsl", strDealerID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
