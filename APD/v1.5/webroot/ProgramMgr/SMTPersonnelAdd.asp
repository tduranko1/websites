<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML, strUserID, strEntityType, strEntityID

  strUserID = GetSession("UserID")
   
  'Get values from the query sring
  strEntityType = request("EntityType")
  strEntityID = request("EntityID")
    
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
  select case request("act")
    case "add" 
	    objExe.ExecuteSpNp "uspSMTPersonnelInsDetail", request.form
		  CheckError()
      if request("CallBack") <> vbnullstring then
			  response.redirect(request("CallBack"))
		  else
			  response.redirect "SMTPersonnelAdd.asp?EntityID=" & strEntityID & "&EntityType=" & strEntityType
		  end if
   end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTPersonnelGetDetailXML", "SMTPersonnel.xsl", strEntityID, strEntityType )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
