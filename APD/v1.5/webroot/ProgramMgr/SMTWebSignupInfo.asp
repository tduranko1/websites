<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<%
  Dim strHTML, objExe, sUserID, sShopLoadID, sShopLocationID, sShopID, iDirty

	On Error Resume Next

	ReadSessionKey()

	sUserID = GetSession("UserID")
  sShopLoadID = request("ShopLoadID")
  sShopID = request("ShopID")
  sShopLocationID = request("ShopLocationID")
  iDirty = request("dirty")
  
  
	'Create and initialize DataPresenter
  	Set objExe = CreateObject("DataPresenter.CExecute")
  	CheckError()

  	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sUserID
  	CheckError()
    
    select case request("act") 
      case "save"
        objExe.ExecuteSpNp "uspSMTShopLoadUpdDetail", request.form
        CheckError()
        Response.Redirect("SMTWebSignupInfo.asp?ShoploadID=" & sShopLoadID)
      case "merge"
        if iDirty = 1 then
          objExe.ExecuteSpNp "uspSMTShopLoadUpdDetail", request.form
          CheckError()
        end if
         
        'response.write request("act") & ", " & sShopLoadID & ", " & sShopID & ", " & sShopLocationID & ", " & sUserID
        'response.end
      
        dim oRs 
        set oRs = objExe.ExecuteSpAsRS("uspSMTShopLoadMerge", sShopLoadID, sShopID, sShopLocationID, sUserID)
        CheckError()
               
        
        if not oRs.eof then
          sShopLocationID = cstr(oRs.Fields(2).value)
          sShopID = cstr(oRs.Fields(1).value)
        else
          sShopLocationID = "0"
          sShopID = "0"
        end if
                        
        If Not oRS Is Nothing Then
          If oRS.State = 1 Then oRS.Close
          Set oRS = Nothing
        End If
                      
        if sShopLocationID <> "0" then  
          objExe.AddXslParam "ShopLocationID", sShopLocationID
          CheckError()
          
          objExe.AddXslParam "ShopID", sShopID
          CheckError()
        end if
     
    end select
    		
	  'Get ApdSelect XML and transform to HTML.
	  strHTML = objExe.ExecuteSpAsXML( "uspSMTShopLoadGetDetailXML", "SMTWebSignupInfo.xsl", sShopLoadID )
	  CheckError()

	  Set objExe = Nothing

    Call KillSessionObject

	  Response.Write strHTML

	  On Error GoTo 0
%>

