<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->

<%
  On Error Resume Next

  dim iLynxID, iVehicleNumber, strUserID, objExe, strImageRootDir, strHTML

  ReadSessionKey()
  strUserID = GetSession("UserID")

  iLynxID = request("LynxID")
  iVehicleNumber = request("VehicleNumber")

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  strImageRootDir = objExe.Setting("Document/RootDirectory")
  CheckError()

  objExe.AddXslParam "ImageRootDir", strImageRootDir
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDPhotosGetListXML", "PMDPhotoViewer.xsl", iLynxID, iVehicleNumber )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0

%>

