<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->


<HTML>
<HEAD>
  <TITLE>Web Shop Load</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

  <script language="javascript">
  
    function Validate(){
      if (frmLogin.txtCertifiedFirstID.value == ""){
        alert("You must enter the Certified First ID of the shop for which you are searching.");
        frmLogin.txtCertifiedFirstID.focus();
        return false;
      }
      
      if (frmLogin.txtAddressZip.value == ""){
        alert("You must enter the Zip Code of the shop for which you are searching.");
        frmLogin.txtAddressZip.focus();
        return false;
      } 
      
      return true;
    }
  </script>
</HEAD>

<BODY style="background-color:#FFFAEB;">
<form name="frmLogin" action="SMTAllInfo.asp" method="post" onsubmit="return(Validate())">
  <div style="position:absolute; top:220px; left:340px;">
  <table>
    <tr>
      <td><strong>Certified First ID:</strong></td>
      <td><input type="text" id="txtCertifiedFirstID" name="CertifiedFirstID" value="62156698" maxlength="50"/></td>
    </tr>
    <tr>
      <td><strong>Zip Code:</strong></td>
      <td><input type="text" id="txtAddressZip" name="AddressZip" value="32211" maxlength="8"/></td>
    </tr>
    <tr>
      <td></td>
      <td align="center"><input type="submit" value="Search"/></td>
    </tr>
  </table> 
  </div> 
</form>
</BODY>
</HTML>
