<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strBusinessParentID

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strBusinessParentID = request("BusinessParentID")
  
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
   select case request("mode")
  	case "update" 
		objExe.ExecuteSpNp "uspSMTBusinessParentUpdDetail", request.form
        CheckError()
		if request("CallBack") <> vbnullstring then
				response.redirect(request("CallBack"))
		else
        	response.redirect "SMTBusinessParent.asp?BusinessParentID=" + request("BusinessParentID")
		end if
    case "insert"
        dim rsID, iID
		set rsID = objExe.ExecuteSpNpAsRS ("uspSMTBusinessParentInsDetail", request.form)
        CheckError()
		if request("CallBack") <> vbnullstring then
			response.redirect(request("CallBack"))
		else
			if not rs.eof then
				iID = rsID("BusinessParentID")
			end if
			response.redirect "SMTBusinessParent.asp?BusinessParentID=" & iID
		end if
		set rsID = nothing			
   end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTBusinessParentGetDetailXML", "SMTBusinessParent.xsl", strBusinessParentID )
  CheckError()

  Set objExe = Nothing

  Call KillSessionObject

  Response.Write strHTML

  On Error GoTo 0
%>
