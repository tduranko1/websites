<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML,  strUserID, strShopLocationID, strReport, strArgs

  strUserID = GetSession("UserID")

  'Get values from the query sring
  strShopLocationID = Request("ShopLocationID")
  strReport = Request("Report")
  strArgs = Request("Args")

  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()

  objExe.AddXslParam "Report", strReport
  CheckError()
  objExe.AddXslParam "Args", strArgs
  CheckError()

  strHTML = objExe.ExecuteSpAsXML( "uspPMDReportViewerGetDetailXML", "PMDReportViewer.xsl", strShopLocationID )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  Call KillSessionObject

  On Error GoTo 0

%>
