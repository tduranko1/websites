<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->
<!-- #include file="../vbs/PMDSecurity.vbs" -->
<%
  Dim strHTML, objExe, sUserID

	On Error Resume Next

	sUserID = GetSession("UserID")
	
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, "0", sUserID
  CheckError()

	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspPMDMainGetListXML", "PMDMain.xsl" )
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

  Call KillSessionObject

	On Error GoTo 0
%>

