<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        Dim strLynxID
        Dim strInsuranceCompanyID
        Dim strClaimAspectID

        strLynxID = request("LynxID")
        strInsuranceCompanyID = request("InsuranceCompanyID")
        If request("EstimateUpd") = 1 Then
            strClaimAspectID = request("ClaimAspectID")
        End If

        'Create and initialize DataPresenter
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'Get XML and transform to HTML.
        If request("EstimateUpd") = 1 Then
            Response.Write objExe.ExecuteSpAsXML( "uspDocumentGetDetailXML", "DocumentEstList.xsl", strLynxID, strClaimAspectID, "", "", strInsuranceCompanyID )
        Else
            Response.Write objExe.ExecuteSpAsXML( "uspDocumentGetDetailXML", "DocumentEstList.xsl", strLynxID, "", "", "", strInsuranceCompanyID )
        End If

    End Sub
%>
