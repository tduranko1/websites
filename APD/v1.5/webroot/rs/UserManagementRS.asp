<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../rs/RSutilities.asp"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.RadExecute = DoRadExecute
}
public_description = new Description();

function DoRadExecute(sProcName, sRequest, sLogText, sLastUpdatedUserID)
{
	try
	{
		var objData = new ActiveXObject("DataPresenter.CExecute");
		objData.Initialize(Request.ServerVariables.Item("APPL_PHYSICAL_PATH"));

	  	//var oReturn = objData.ExecuteSpNpAsRS(sProcName, sRequest);
      var sRet = objData.ExecuteSpNpAsXML( sProcName, sRequest );
      
      //insert the security log information.
      if (sLogText != ""){
        var sKeyDesc;
        var sKeyID;
        var iStartIdx;
         
                
        // a role is being maintained          
        if (sRequest.indexOf("RoleID=") > -1){
          sKeyID = sRequest.substr(Number(sRequest.indexOf("RoleID=")) + 7);
          sKeyID = sKeyID.substring(0, sKeyID.indexOf("&")) + " ";
        
          sKeyDesc = sRequest.substr(Number(sRequest.indexOf("&RoleName=")) +10);
          sKeyDesc = sKeyDesc.substring(0, sKeyDesc.indexOf("&")) + " ";
        }
        // a user is being maintained
        else{
          sKeyID = sRequest.substr(Number(sRequest.indexOf("UserID=")) + 7);
          sKeyID = sKeyID.substring(0, sKeyID.indexOf("&")) + " ";
        
          sKeyDesc = sRequest.substr(Number(sRequest.indexOf("&NameFirst=")) + 11);
          sKeyDesc = sKeyDesc.substring(0, sKeyDesc.indexOf("&")) + " ";
          sKeyDesc += sRequest.substr(Number(sRequest.indexOf("&NameLast=")) + 10) 
          sKeyDesc = sKeyDesc.substring(0, sKeyDesc.indexOf("&")) + " ";        
        }        
                      
        var aLogText = sLogText.split("\n");
        for (var i = 0; i < aLogText.length; i++){
          if (aLogText[i] != "")
            objData.ExecuteSp("uspAuditLogInsDetail", "S", "", sKeyDesc, sKeyID, aLogText[i], sLastUpdatedUserID);
        }
      }
      return sRet + "||1";
        //return Recordset2Array(oReturn) + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "UserManagementRS:DoRadExecute()", e.message );
        return e.message + "||0";
	}
}

</SCRIPT>
