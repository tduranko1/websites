
<%@ LANGUAGE=JavaScript %>

<%
  try {
    Response.ContentType = "text/xml";
    var sRequestXML = Request.Form;
    var objReleasePayment, oXML;
    var lngEvent, strRet;
    
    oXML = new ActiveXObject("MSXML2.DOMDocument");
    oXML.async = false;
    oXML.loadXML(sRequestXML);

    if (oXML.parseError.errorCode != 0) {
     Response.Write("<Root><Error><![CDATA[Error parsing the XML request at Line #: " + CStr(oXML.parseError.line) + " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error></Root>");
     Response.End()
    }
   
    oReleasePaymentNode = oXML.selectSingleNode("/ReleasePayment");
      try {
         objReleasePayment = new ActiveXObject("LAPDAccounting.CBilling");
         lngEvent = objReleasePayment.ReleaseFunds( sRequestXML );
         strRet = "<Root><Success><![CDATA[" + lngEvent + "]]></Success></Root>"
      } catch (e) {
         strRet = "<Root><Error><![CDATA[" + e.description + "]]></Error></Root>"
      }
      
      Response.Write(strRet);
    
  } catch (e) {
    var sStrRet = "<Root returnCode=\"1\"><error><user>An internal error has occured while saving the data.</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error></Root>";
    Response.Write(sStrRet);
  }
%>
