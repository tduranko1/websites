
<%@ LANGUAGE=JavaScript %>

<%
  try {
    Response.ContentType = "text/xml";
    var sRequestXML = Request.Form;
    var objAssignment, oXML, oAssignmentNode, oPathwayNode, oPathwayXML;
    var lngEvent, strRet;
    var strAssignmentID, strUserId, blnStaffAppraiser, blnWarrantyAssignment, blnFaxAssignment ,dataToSend, postURL, strPathWay;
    
    oXML = new ActiveXObject("MSXML2.DOMDocument");
    oXML.async = false;
    oXML.loadXML(sRequestXML);

    if (oXML.parseError.errorCode != 0) {
     Response.Write("<Root><Error><![CDATA[Error parsing the XML request at Line #: " + CStr(oXML.parseError.line) + " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error></Root>");
     Response.End()
    }
    
    blnWarrantyAssignment = false
   
    oAssignmentNode = oXML.selectSingleNode("/Root/Assignment");
    if (oAssignmentNode){
      strAssignmentID =  oAssignmentNode.getAttribute("assignmentID");
      strUserId =  oAssignmentNode.getAttribute("userID");
      blnStaffAppraiser = (oAssignmentNode.getAttribute("staffAppraiser") == "true");
      blnWarrantyAssignment = (oAssignmentNode.getAttribute("warrantyAssignment") == "true");
      blnHQAssignment = (oAssignmentNode.getAttribute("hqAssignment") == "true");
    }
	
        if (blnHQAssignment)
	{
		dataToSend = "StoredProcedure=uspWorkflowSendShopAssignWSXML" + "&Parameters=" + strAssignmentID;
		strPathWay = HttpPost("ExecuteSpAsXML", dataToSend)
	}
	
       	
     	 try {
         	objAssignment = new ActiveXObject("LAPDCommServer.CCommServer");
	         lngEvent = objAssignment.AssignShop( strAssignmentID, strUserId, blnStaffAppraiser, blnWarrantyAssignment );
	         strRet = "<Root><Success><![CDATA[" + lngEvent + "]]></Success></Root>"
	      } catch (e) {
	         strRet = "<Root><Error><![CDATA[" + e.description + "]]></Error></Root>"
      		}
		
      
      Response.Write(strRet);
    
  } catch (e) {
    var sStrRet = "<Root returnCode=\"1\"><error><user>An internal error has occured while saving the data.</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error></Root>";
    Response.Write(sStrRet);
  }


function GetWebServiceURL() {

        var strmexMonikerString;
        var objFSO, ts, GetEnvironment;
        objFSO = new ActiveXObject("Scripting.FileSystemObject");
        ts = objFSO.OpenTextFile("C:\\PGW\\Environment\\Config.txt", 1);
        GetEnvironment = ts.ReadLine();

        switch (GetEnvironment) {
            case "DEV":
                strmexMonikerString = "http://dlynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                break;
            case "STG":
                strmexMonikerString = "http://slynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                break;
            case "PRD":
                strmexMonikerString = "http://lynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                break;
        }

        return strmexMonikerString;
    }

function GenerateGUID() { 
        return (new ActiveXObject("Scriptlet.TypeLib") 
                                    .GUID.substr(1, 36)); 
    } 


function HttpPost(strFunctionName, strDataToSend)
{
	var strServiceURL, dataToSend, postURL, strPathWay, dataToSendJob, strJobParams, strGUID, strAssignmentType, strEventID, strDescription, strShop, strDocumentID, strShopLocationID;
	var oPathwayXML, oPathwayNode, oClaimNode, oCoverageNode, oVehicleNode;
	var strLynxID, strClaimNumber, strAssingnmentID, strInsuranceCompany, strClaimAspectID, strDocumentType, strDocumentTypeID, strJobFileName, strVehicleNumber, strServiceChannelCD, strClaimAspectServiceChannelID, strCommunicationMethodID;

	strServiceURL = GetWebServiceURL() 
	postURL = strServiceURL + "/" + strFunctionName;
	var objXMLHTTP =  new ActiveXObject("MSXML2.XMLHTTP");
	//dataToSend = "StoredProcedure=uspWorkflowSendShopAssignWSXML" + "&Parameters=" + strAssignmentID;
	objXMLHTTP.open("POST", postURL , false);
    	objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    	objXMLHTTP.send(strDataToSend);

	oPathwayXML = new ActiveXObject("MSXML2.DOMDocument");
	oPathwayXML.async = false;
	oPathwayXML.loadXML(objXMLHTTP.responseXML.xml);

//Response.Write(objXMLHTTP.responseXML.xml);

	if (oPathwayXML.parseError.errorCode != 0) {
	Response.Write("<Root><Error><![CDATA[Error parsing the XML request at Line #: " + CStr(oPathwayXML.parseError.line) + " at character position: " & CStr(oPathwayXML.parseError.linepos) & ". Reason: " & oPathwayXML.parseError.reason & "]]></Error></Root>");
	Response.End()
	}
	
	oPathwayNode = oPathwayXML.selectSingleNode("/Assignment");
	if (oPathwayNode)
	{
	 strCommunicationMethodID =oPathwayNode.getAttribute("CommunicationMethodID");
	 strPathWay = oPathwayNode.getAttribute("EstimatingPackage");
	 strLynxID = oPathwayNode.getAttribute("LynxId");
	 strAssignmentID = oPathwayNode.getAttribute("AssignmentID");
	 strAssignmentType = oPathwayNode.getAttribute("AssignmentCode"); 
 	 strVehicleNumber  = oPathwayNode.getAttribute("VehicleNumber");
	}


	if (strAssignmentType == "New")
	{
	 strDocumentType = "Shop Assignment";	
	 strDocumentTypeID = "22";
	 strJobFileName = "AssignmentAdd.xsl";	 
	}
	else if (strAssignmentType == "Cancel")
	{
	 strDocumentType = "Cancel Assignment";
	 strDocumentTypeID = "81";
	 strJobFileName = "AssignmentCancel.xsl";	 
	}

	oCoverageNode = oPathwayXML.selectSingleNode("/Assignment/Coverage");
	if (oCoverageNode)
	{
	 strClaimNumber = oCoverageNode.getAttribute("ClaimNumber");
	}

	oClaimNode = oPathwayXML.selectSingleNode("/Assignment/Claim");
	if (oClaimNode)
	{
	 strInsuranceCompany= oClaimNode.getAttribute("InsuranceCompanyID");
	}

	oVehicleNode = oPathwayXML.selectSingleNode("/Assignment/Vehicle");
	if (oVehicleNode)
	{
	 strClaimAspectID = oVehicleNode.getAttribute("ClaimAspectID");
	 strClaimAspectServiceChannelID = oVehicleNode.getAttribute("ClaimAspectServiceChannelID");
	 strServiceChannelCD = oVehicleNode.getAttribute("ServiceChannelCD");
	}

	oShopNode = oPathwayXML.selectSingleNode("/Assignment/Shop");
	if (oShopNode)
	{
	 strShop= oShopNode.getAttribute("ShopName");
	 strShopLocationID = oShopNode.getAttribute("ShopLocationID");
	}

	if (strCommunicationMethodID == "17" || strCommunicationMethodID == "2")
	{
	// Insert Job for the Assignment
	 strGUID = GenerateGUID();	
	 
	 strJobParams = "'" + strGUID + "'" + "," + strInsuranceCompany + "," + strLynxID + "," + "'" + strClaimNumber + "'" + "," + 0 + "," + 0 + "," + strVehicleNumber + "," + strShopLocationID + "," + 0 + "," + "'" + GetDBDateTime() + "'" + "," + strDocumentTypeID + "," + "'" + strDocumentType + "'" + "," + "'User'" + "," + "''" + "," + "''" + "," + strClaimAspectServiceChannelID + "," + strClaimAspectID + "," + "'" + strJobFileName + "'" + "," + "'Unprocessed'" + "," + "'" + "APD Shop Assignment to Hyperquest (MANUAL)" + "'" + "," + 1 + "," + "'" + GetDBDateTime() + "'" + "," + "1" + "," + "''" + "," + "0";
	 
	postURL = strServiceURL + "/" + "ExecuteSp";
	var objJobHTTP =  new ActiveXObject("MSXML2.XMLHTTP");
	dataToSendJob = "StoredProcedure=uspHyperquestInsDAJobDocument" + "&Parameters=" + strJobParams;
	objJobHTTP.open("POST", postURL , false);
    	objJobHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    	objJobHTTP.send(dataToSendJob);
	
	if (strAssignmentType == "New")
	{
		//Trigger workflow to change the Elec status as Enroute

		var dataToSendEvent, strEvevntParams;

		strEventID = "25";
		strShop = strShop.replace(/'/g, "''");
		strShop = strShop.replace(/&/g, "%26");
		strDescription = "PROGRAM SHOP (HQ): Assignment for " + strLynxID + "-" + strVehicleNumber + " was sent to " + strShop;

		strEventParams = parseInt(strEventID) + "," + null + "," + null + "," + parseInt(strLynxID) + "," + "'" + "VEH0" + strVehicleNumber + "'" + "," + null + "," + null + "," + strServiceChannelCD + "," + "'" + strDescription + "'" + "," + null + "," + null + "," + 0 + "," + "''";

		var objEventHTTP =  new ActiveXObject("MSXML2.XMLHTTP");
 		dataToSendEvent = "StoredProcedure=uspWorkflowNotifyEvent" + "&Parameters=" + strEventParams;
		objEventHTTP.open("POST", postURL , false);
    		objEventHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
	    	objEventHTTP.send(dataToSendEvent);
	}
	}
	
//Response.Write(objJobHTTP.responseXML.xml + strGUID + strJobParams);
	

//Response.Write(strJobParams);	

//Response.Write(strPathWay + strLynxID + strAssignmentID + strClaimNumber + strInsuranceCompany);

	return strPathWay;
}

function GetDBDateTime () {
  now = new Date();
  year = "" + now.getFullYear();
  month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
  day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
  hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
  minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
  second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
  return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}
%>
