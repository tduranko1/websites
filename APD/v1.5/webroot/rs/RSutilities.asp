
<SCRIPT RUNAT=SERVER Language=javascript>

function Recordset2Array(rst) 
 {
     var numOfFields, i, j
     var aRS;
     
     // Creates a new array to hold the RS
     aRS = new Array();
     
     // Adds a first child array with all the field names
     aRS[0] = new Array();        
     numOfFields = rst.Fields.Count;
     
     for (i=0; i<numOfFields; i++)
         aRS[0][i] = rst.Fields.Item(i).Name;
 
     // Adds as many child arrays as needed to hold all the records 
     i = 1;
 
     while (!rst.EOF) {
         aRS[i] = new Array();
         
         for (j=0; j<numOfFields; j++) 
             aRS[i][j] = String(rst.Fields.Item(j).Value);
         
         // Next record and next item in the array
         rst.MoveNext();
         i++;
     }
 
     return aRS;
 }
 
 </SCRIPT>