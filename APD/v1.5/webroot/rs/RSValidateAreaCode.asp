<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->
<!--#INCLUDE file="RSUtilities.asp"-->
<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
 	this.IsValidAreaCode = DoIsValidAreaCode;
}
public_description = new Description();

function DoIsValidAreaCode(sAreaCode, sExchange)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var rs = objExe.ExecuteSpAsRS( "uspCheckPhoneNumber", sAreaCode, sExchange);
		return rs.Fields.Item(0) + "," + rs.Fields.Item(2) + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateAreaCode:DoIsValidAreaCode()", e.message );
        return e.message + "||0";
	}
}

</SCRIPT>

