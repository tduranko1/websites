
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.IsValidateLynxID = DoValidateLynxID;
}
public_description = new Description();

function DoValidateLynxID(sLynxID)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
    objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
    var rs = objExe.ExecuteSpAsRS( "uspClaimVerifyLynxID", sLynxID);
    return rs.Fields.Item(0) + "||1";
  }
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateLynxID:DoValidateLynxID()", e.message );
    return e.message + "Foooo||0";
	}
}

</SCRIPT>

