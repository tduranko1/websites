
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.CloseClaim = DoCloseExecute
}
public_description = new Description();

function DoCloseExecute( sLynxID, sUserID )
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		var sRet = objExe.ExecuteSpAsXML( "uspWorkFlowCloseClaim", "", sLynxID, sUserID );
        return sRet + "||1";
	}
	catch(e)
	{
    SaveErrorData( ServerSideEvent(), "RSCloseClaim.asp", e.message );
    return e.message + "||0";
	}
}


</SCRIPT>
