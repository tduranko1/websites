
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.RadExecute = DoRadExecute
    this.JustExecute = DoJustExecute
    this.RetJustExecute = DoRetJustExecute; //used by the shop selection. Assignment ID was returned by the proc.
}
public_description = new Description();

function DoRadExecute( sProc, sRequest )
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		var sRet = objExe.ExecuteSpNpAsXML( sProc, sRequest );
        return sRet + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSADSAction.asp", e.message );
        return e.message + "||0";
	}
}

function DoJustExecute( sProc, sRequest )
{
    try
    {
        var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
        objExe.ExecuteSpNp( sProc, sRequest );
        return "Success executing " + sProc + "||1";
    }
    catch(e)
    {
        SaveErrorData( ServerSideEvent(), "RSADSAction.asp", e.message );
        return e.message + "||0";
    }
}

function DoRetJustExecute( sProc, sRequest )
{
    try
    {
        var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
        var ret = objExe.ExecuteSpNp( sProc, sRequest );
        return ret + "||1";
    }
    catch(e)
    {
        SaveErrorData( ServerSideEvent(), "RSADSAction.asp", e.message );
        return e.message + "||0";
    }
}

</SCRIPT>

