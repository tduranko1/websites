
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>
function Description()
{
  this.AssignShop = DoAssignShop;
}
public_description = new Description();

function DoAssignShop( AssignmentID, UserId, StaffAppraiser ) // need to add ShopRemarks
{
	try
	{
        // This procedure sends a shop assignment for the given Shop Assignment ID.
		var objAssignment = new ActiveXObject("LAPDCommServer.CCommServer");
        var lngEvent = objAssignment.AssignShop( AssignmentID, UserId, StaffAppraiser );
        return lngEvent + "|Success||1";
	}
	catch(e)
	{
        var strMessage = e.message + ": AssignmentID='" + AssignmentID + "' UserID='" + UserId + "'";
        SaveErrorData( ServerSideEvent(), "RSAssignShop:DoAssignShop()", strMessage );
        return lngEvent + "|" + strMessage + "||0";
	}
}

</SCRIPT>
