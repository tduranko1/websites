<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->
<!--#INCLUDE file="RSUtilities.asp"-->
<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
 	this.IsValidComm = DoIsValidComm;
 	this.getShopList = DogetShopList;
}
public_description = new Description();

function DoIsValidComm(iShopID, iCommunicationMethodID, sCommunicationAddress)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var rs = objExe.ExecuteSpAsRS( "uspSMTCheckCommunicationInfo", iShopID, iCommunicationMethodID, sCommunicationAddress);
		return rs.Fields.Item(0) + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateComm:DoIsValidComm()", e.message );
        return e.message + "||0";
	}
}

function DogetShopList(iShopID, iCommunicationMethodID, sCommunicationAddress)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var rs = objExe.ExecuteSpAsRS( "uspSMTDupCommInfoGetListXML", iShopID, iCommunicationMethodID, sCommunicationAddress);
		return rs.Fields.Item(0) + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateComm:DoIsValidComm()", e.message );
        return e.message + "||0";
	}
}

</SCRIPT>

