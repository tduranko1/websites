
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

  function Description()
  {
  	this.IsDuplicateFedTaxID = DoValidateFedTaxID;
    this.GetUserByID = DoGetUserByID;
    this.NotifyProgramManager = DoNotifyProgramManager;
  }
  public_description = new Description();
  
  function DoValidateFedTaxID(sFedTaxID, sShopId){
  	try{
  		var objExe = new ActiveXObject("DataPresenter.CExecute");
      objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
      var rs = objExe.ExecuteSpAsRS( "uspSMTValidateInfo", sFedTaxID, "", sShopId);
  		return rs.Fields.Item(0) + "||1"; 
  	}
  	catch(e){
  		SaveErrorData( ServerSideEvent(), "RSShopMaint:DoValidateFedTaxID()", e.message );
      return e.message + "||0";
  	}
  }
  
  // returns User First and Last Name and Email address
  function DoGetUserByID(sUserID, ApplicationCD){
      
    try{
  		var objExe = new ActiveXObject("DataPresenter.CExecute");
      objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
      var sXML = objExe.ExecuteSpAsXML( "uspAdmUserGetDetailXML", "", sUserID);
  		var objXML = new ActiveXObject("Microsoft.XMLDOM");
      
      objXML.loadXML(sXML);
      
      var oNode = objXML.documentElement.selectSingleNode("/Root/User");
      var sUserEmail = oNode.selectSingleNode("@EmailAddress").text;
      var sUserFirstName = oNode.selectSingleNode("@NameFirst").text;
      var sUserLastName = oNode.selectSingleNode("@NameLast").text;
      
      return sUserEmail + "|" + sUserFirstName + " " + sUserLastName + "||1"; 
    }
  	catch(e)
  	{
  		SaveErrorData( ServerSideEvent(), "RSSendEmail:DoGetUserByID()", e.message );
      return e.message + "||0";
  	}      
  }
  
  // retrieves From and To email addresses based on UserID, builds notification message and sends email
  function DoNotifyProgramManager(iSenderUserID, iProgramManagerUserID, sShopInfo){
    try{
      var sRet;
      var arrRet;
      var sSenderName = "APD User " + iSenderUserID;
      var sSenderEmail;
      var sProgramManagerEmail;
      var sProgramManagerName = "APD User " + iProgramManagerUserID;
      var sSubject = "A new Shop has been assigned to you";
      var sBody;
      var bPMEmailMissing = false;
            
      // get sender name based on UserID
          
      var sRet = DoGetUserByID(iSenderUserID, "APD");
      arrRet = sRet.split("||");
            
      if (arrRet[1] == 1){
        arrRet = arrRet[0].split("|");
        sSenderEmail = arrRet[0];
        if (arrRet[1].replace(/\s*$/, "").length > 0) sSenderName = arrRet[1];
      }
      
      // get Program Manager email address based on UserID
      var sRet = DoGetUserByID(iProgramManagerUserID, "APD");
      arrRet = sRet.split("||");
      if (arrRet[1] == 1){
        arrRet = arrRet[0].split("|");
        sProgramManagerEmail = arrRet[0];
        if (arrRet[1].replace(/\s*$/, "").length > 0) sProgramManagerName = arrRet[1];
      }
        
      var objExe = new ActiveXObject("DataPresenter.CExecute");
      objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
            
      // set default values for From and To email addresses if necessary
      if (objExe.Setting("ShopMaintenance/TestingEmailTo").length > 0)
        sProgramManagerEmail = objExe.Setting("ShopMaintenance/TestingEmailTo");

      if (sProgramManagerEmail.replace(/\s*$/, "").length == 0){
        sProgramManagerEmail = objExe.Setting("ShopMaintenance/DefaultEmailTo");
        bPMEmailMissing = true;
      }
       
      if (sSenderEmail.replace(/\s*$/, "").length == 0)
        sSenderEmail = objExe.Setting("ShopMaintenance/DefaultEmailFrom");
            
      arrRet = sShopInfo.split("||");            
          
      // build email body
      sBody = "<html><head></head><body>";
      sBody += "<table><tr><td colspan='2'>" + sSenderName + " has assigned a new Shop to the managerial domain of " + sProgramManagerName + ".</td></tr>";
      sBody += "<tr><td colspan='2'><img src='/images/spacer.gif' border='0' height='20px' width='5px'/></td></tr>";
      sBody += "<tr><td><strong>Shop ID:</strong></td><td>" + arrRet[0] + "</td></tr>";
      sBody += "<tr><td><strong>Name:</strong></td><td>" + unescape(arrRet[1]) + "</td></tr>";
      sBody += "<tr><td><strong>Address:</strong></td><td>" + unescape(arrRet[2]) + "</td></tr>";
      sBody += "<tr><td><strong>City:</strong></td><td>" + unescape(arrRet[3]) + "</td></tr>";
      sBody += "<tr><td><strong>Phone:</strong></td><td>" + arrRet[4] + "</td></tr>";
      sBody += "<tr><td><strong>Program Manager:</strong></td><td>" + sProgramManagerName + "</td></tr>";

      if (bPMEmailMissing){
        sBody += "<tr><td colspan='2'><img src='/images/spacer.gif' border='0' height='20px' width='5px'/></td></tr>";
        sBody += "<tr><td colspan='2' style='font-size:12'>*You are receiving this message because the APD system does " 
        sBody += "not have an email address for " + sProgramManagerName + ".  Please contact the APD System Administrator to have this rectified</td></tr>";
      }

      sBody += "</table></body></html>";
    
      objExe.mToolkit.mEvents.SendEmail(sSenderEmail, sProgramManagerEmail, sSubject, sBody, true);
      return sProgramManagerName + "||1";
    }
    catch(e){
  		SaveErrorData( ServerSideEvent(), "RSSendEmail:DoNotifyProgramManager()", e.message );
      return e.message + "||0";
  	} 
  }
  

</SCRIPT>

