
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
 	this.GetUserDetails = DoGetUserDetails;
}
public_description = new Description();

function DoGetUserDetails(sLynxID)
{
	try
	{

    var objExe = new ActiveXObject("DataPresenter.CExecute");
    objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
    var rs = objExe.ExecuteSpAsRS( "uspSessionGetUserDetail", "", "", sLynxID);
 		var srs = "";
    srs += rs.Fields.Item(0) + ",";
    srs += rs.Fields.Item(3) + ",";
    srs += rs.Fields.Item(4) + ",";
    srs += rs.Fields.Item(6) + ",";
    srs += rs.Fields.Item(7) + ",";
    srs += rs.Fields.Item(9);
    
    return srs + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "", e.message );
        return e.message + "||0";
	}
}

</SCRIPT>

