
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.BillClaim = DoBillExecute
	this.RemoveBillClaim = DoRemoveBillExecute
	this.GenerateInvoice = DoGenerateInvoice
  this.RequestApproval = DoRequestApproval
  this.NotifyAccounting = DoNotifyAccounting
}
public_description = new Description();

function DoBillExecute( sClaimAspectID, sFeeID, sUserID )
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		var sRet = objExe.ExecuteSp( "uspClaimBillingInsDetail", sClaimAspectID, sFeeID, sUserID, 1  );
		return sRet + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSClaimBill.asp", e.message );
		return e.message + "||0";
	}
}

function DoRemoveBillExecute( sClaimBillingUserID, sUserID, sLastUpdatedDate )
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		var sRet = objExe.ExecuteSp( "uspClaimBillingDel", sClaimBillingUserID, sUserID, sLastUpdatedDate, 1 );
		return sRet + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSClaimBill.asp", e.message );
		return e.message + "||0";
	}
}


function DoGenerateInvoice(sInsuranceCompanyID, sClaimAspectID, sUserID){
  var sRet = "";
	try
	{
		var objExe = new ActiveXObject("LAPDAccounting.CBilling");
		sRet = objExe.BillAndCreateInvoice(sInsuranceCompanyID, sClaimAspectID, sUserID);
		
    if (sRet == "-1")
      return "Irrelevant||-1"
    else
      return "Success||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSClaimBill.asp", e.message );
		return e.message + "||0";
	}
}


function DoRequestApproval(sClaimAspectID, sUserID){
  var sRet = "";
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
    objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		objExe.ExecuteSP("uspInvoiceUpdateStatus", sClaimAspectID, sUserID);
		return "Success||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSClaimBill.asp", e.message );
		return e.message + "||0";
	}
}

// retrieves From and To email addresses based on UserID, builds notification message and sends email
  function DoNotifyAccounting(iSenderUserID, sDispatch){
    try{
      var sRet;
      var arrRet;
      var sSenderName = "APD User " + iSenderUserID;
      var sSenderEmail;
      var sAccountingRep;
      var sSubject = "Dispatched APD invoice items removed";
      var sBody;
            
            
      // Get sender name based on UserID          
      var sRet = DoGetUserByID(iSenderUserID, "APD");
      arrRet = sRet.split("||");
            
      if (arrRet[1] == 1){
        arrRet = arrRet[0].split("|");
        sSenderEmail = arrRet[0];
      }
      
      // Create an instance of DataPresenter to access the config file.        
      var objExe = new ActiveXObject("DataPresenter.CExecute");
      objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
            
      // Set default values for From and To email addresses if necessary
      if (objExe.Setting("Accounting/Invoice/DeleteNotification").length > 0)
        sAccountingRep = objExe.Setting("Accounting/Invoice/DeleteNotification");
             
      if (sSenderEmail.replace(/\s*$/, "").length == 0)
        sSenderEmail = objExe.Setting("ShopMaintenance/DefaultEmailFrom");    // just used the default from shop since it is generic.
                      
      // Build email body
      sBody = "APD invoice items corresponding to Dispatch #" + sDispatch + " have been removed.  Please void this dispatch.  Thank you."
      
      // Send the email.    
      objExe.mToolkit.mEvents.SendEmail(sSenderEmail, sAccountingRep, sSubject, sBody);
      return "sent" + "||1";
    }
    catch(e){
  		SaveErrorData( ServerSideEvent(), "RSClaimBill:DoNotifyAccounting()", e.message );
      return e.message + "||0";
  	} 
  }


</SCRIPT>
