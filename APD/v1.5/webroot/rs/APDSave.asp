<%@ LANGUAGE=JavaScript %>

<%
  try {
    Response.ContentType = "text/xml";
    var sRequestXML = Request.Form;
    var sStrRet = "";
    var oXML = new ActiveXObject("MSXML2.DOMDocument");
    var sProc = sRequest = sExecType = sRequestData = strRequestData = "";
    oXML.async = false;
    oXML.loadXML(sRequestXML);

    var oRequests = oXML.documentElement.selectNodes("/Root/StoredProc");
    var bError = false;

    var objExe = new ActiveXObject("DataPresenter.CExecute");
    objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );

    for (var i = 0; i < oRequests.length; i++) {
      sProc = oRequests[i].selectSingleNode("@name").value;
      sExecType = oRequests[i].selectSingleNode("@execType").value;
      sRequestData = oRequests[i].firstChild.text;

      strRequestData = sRequestData.split("||");
      sRequest = strRequestData[0];

      if (sProc != "config") {
         sStrRet += "<StoredProc name=\"" + sProc + "\"";
   
         switch (sExecType.toLowerCase()) {
           case "executespnpasxml":
             try {
               var sRet = objExe.ExecuteSpNpAsXML( sProc, sRequest );
               sStrRet += " returnCode=\"0\"><Result>" + sRet + "</Result>";
             } catch(e) {
               bError = true;
               var objErrorInfo = getExternalErrorMessage(e.message);
   
               sStrRet += " returnCode=\"" + objErrorInfo.code + "\"><error><user>" + objErrorInfo.description + "</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error>";
             }
             break;
   
           case "executespnpasrs":
             try {
               var sRet = objExe.ExecuteSpNpAsRS( sProc, sRequest );
               sStrRet += " returnCode=\"0\"><Result>" + RS2XML(sRet) + "</Result>";
             } catch(e) {
               bError = true;
               var objErrorInfo = getExternalErrorMessage(e.message);
   
               sStrRet += " returnCode=\"" + objErrorInfo.code + "\"><error><user>" + objErrorInfo.description + "</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error>";
             }
             break;
   
           case "executespnp":
             try {
               var sRet = objExe.ExecuteSpNp( sProc, sRequest );
               sStrRet += " returnCode=\"0\"><Result>" + sRet + "</Result>";
             } catch(e) {
               bError = true;
               var objErrorInfo = getExternalErrorMessage(e.message);
   
               sStrRet += " returnCode=\"" + objErrorInfo.code + "\"><error><user>" + objErrorInfo.description + "</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error>";
             }
             break;
           default:
             bError = true;
             sStrRet += " returnCode=\"1\"><error><user>An APD internal error has occurred.  Contact your IS administrator.</user><advanced><![CDATA[APDSave.asp - [" + sExecType + "] unsupported COM method call or not yet implemented.]]></advanced></error>";
   
             break;
         }

     sStrRet += "</StoredProc>";

     if (strRequestData.length == 2) {

         var sRightSplit = strRequestData[1].split("&");
         var sSession, sWindow, sSessionKey, sWindowID, sClientClaimNumber;

         if (sRightSplit.length == 2) {
             sSession = sRightSplit[0].split("=");
             sWindow = sRightSplit[1].split("=");

             if (sSession.length == 2 && sWindow.length == 2) {
                 sSessionKey = unescape(sSession[1]);
                 sWindowID = unescape(sWindow[1]);

                 var sLeftSplit = strRequestData[0].split("&");

                 for (var i = 0; i < sLeftSplit.length; i++) {
                     if (sLeftSplit[i].indexOf("ClientClaimNumber=") > -1)
                         sClientClaimNumber = sLeftSplit[i].split("=");
                 }

                 if (sSessionKey != "" && sWindowID != "" && sClientClaimNumber.length == 2) {
                     if (sClientClaimNumber[0] != "" && sClientClaimNumber[1] != "") {
                         var objExee = new ActiveXObject("SessionManager.CSession");
                         objExee.UpdateSession(sSessionKey, ("ClientClaimNumber" + "-" + sWindowID), sClientClaimNumber[1]);
                     }
                 }
             }
         }

     }
      }
      
      if (sProc == "config"){
         sStrRet += "<Config><request><![CDATA[" + sRequest + "]]></request>";
         try {
            sStrRet += "<value><![CDATA[" + objExe.Setting(sRequest) + "]]></value>";
         } catch (e) {
            sStrRet += "<error><![CDATA[" + e.description + "]]></error>";
         }
         sStrRet += "</Config>";
      }

      sProc = sExecType = sRequest = "";
    }

    sStrRet = "<Root returnCode=\"" + (bError ? "1" : "0") + "\">" + sStrRet + "</Root>";
    Response.Write(sStrRet);

  } catch (e) {
    sStrRet = "<Root returnCode=\"1\"><error><user>An internal error has occured while saving the data.</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error></Root>";
    Response.Write(sStrRet);
  }


  function RS2XML(rst) {
    var sRet = "";
    var numOfFields = rst.Fields.Count;

    while (!rst.EOF) {
      sRet += "<SQLResultRow>";
      for (var i = 0; i < numOfFields; i++) {
        sRet += "<SQLResultCol name=\"" + rst.Fields.Item(i).Name + "\"><![CDATA[" + rst.Fields.Item(i).Value + "]]></SQLResultCol>";
      }
      sRet += "</SQLResultRow>";
      rst.MoveNext();
    }

    return sRet;
  }

  function getExternalErrorMessage(strError){
    var objRet = {
                    code : null,
                    description: ""
                  }
    if (strError != "") {
      //var iPos = strError.indexOf("External Message: |AE>");
      var iPos = strError.indexOf("External Message:");
      if (iPos > 0) {
        //strRet = strError.substring(iPos, strError.indexOf("<|", iPos));
        strRet = strError.substring(iPos + 17, strError.indexOf("<|", iPos));
        strRet = strRet.replace(/[\n]/g, "");
        strRet = strRet.substr(strRet.indexOf(">") + 1);
        objRet.code = strRet.substr(0, strRet.indexOf(">"));
        objRet.description = strRet.substr(strRet.indexOf(">") + 1);
      }
    }
    return objRet;
  }

%>



