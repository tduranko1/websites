
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.AssignClaim = DoAssignExecute
  this.AssignTask = DoAssignTask
}
public_description = new Description();

function DoAssignExecute( sClaimAspectID, sUserID, sCurrentUserID, bAssignTasks )
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );
		//var sRet = objExe.ExecuteSpAsXML( "uspWorkFlowAssignClaim", "", sLynxID, sCurrentUserID, sUserID );
        var sRet = objExe.ExecuteSp( "uspWorkFlowAssignClaim", sClaimAspectID, sCurrentUserID, sUserID, 0, 1, bAssignTasks);
        return sRet + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSReAssignClaim:DoAssignExecute()", e.message );
        return e.message + "||0";
	}  
}


function DoAssignTask(sCheckListID, sAlarmDate, sAssignedUserID, sDescription, sReassignTaskFlag, sUserID, sSysLastUpdatedDate){
  try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
		objExe.Initialize( Request.ServerVariables.Item("APPL_PHYSICAL_PATH") );		
    var sRet = objExe.ExecuteSp( "uspDiaryUpdDetail", sCheckListID, sAlarmDate, sAssignedUserID, sDescription, sReassignTaskFlag,  sUserID, sSysLastUpdatedDate);
    return sRet + "||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSReAssignClaim:DoAssignTask()", e.message );
        return e.message + "||0";
	}  
}


</SCRIPT>
