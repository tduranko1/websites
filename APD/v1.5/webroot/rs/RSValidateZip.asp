
<%@ LANGUAGE=VBSCRIPT %>

<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.ZipToState = DoGetState;
 	this.ZipToCity = DoGetCity;
 	this.ZipToCounty = DoGetCounty;
 	this.IsValidZip = DoIsValidZip;
}
public_description = new Description();

function DoIsValidZip(sZipCode)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var rs = objExe.ExecuteSpAsRS( "uspSMTValidateInfo", "", sZipCode);
		return rs.Fields.Item(1) + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateZip:DoIsValidZip()", e.message );
        return e.message + "||0";
	}
}

function DoGetState(sZipCode)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var sXML = objExe.ExecuteSpAsXML( "uspZipCodeSearchXML", "", sZipCode);

		var idx = sXML.indexOf("State");
		if (idx != -1)
		{
			var sRet = sXML.substring(idx+7,idx+9);
            return sRet + "||1";
		}
		else
            return "??||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateZip:DoGetState()", e.message );
        return e.message + "||0";
	}
}

function DoGetCity(sZipCode)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var sXML = objExe.ExecuteSpAsXML( "uspZipCodeSearchXML", "", sZipCode);

		var idx = sXML.indexOf("City");
		if (idx != -1)
		{
			var sRet = sXML.substr(idx+6);
			idx = sRet.indexOf("\"");
			sRet = sRet.substring(0, idx);
            return sRet + "||1";
		}
		else
            return "?????||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateZip:DoGetCity()", e.message );
        return e.message + "||0";
	}
}

function DoGetCounty(sCounty)
{
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var sXML = objExe.ExecuteSpAsXML( "uspZipCodeSearchXML", "", sCounty);

		var idx = sXML.indexOf("County");
		if (idx != -1)
		{
			var sRet = sXML.substr(idx+8);
			idx = sRet.indexOf("\"");
			sRet = sRet.substring(0, idx);
            return sRet + "||1";
		}
		else
            return "?????||1";
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSValidateZip:DoGetCountry()", e.message );
        return e.message + "||0";
	}
}

</SCRIPT>

