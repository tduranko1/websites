<% RSDispatch %>
<!--#INCLUDE file="../_SCriptLibrary/RS.ASP"-->
<!--#INCLUDE file="../errorhandler.asp"-->

<SCRIPT RUNAT=SERVER Language=javascript>

function Description()
{
	this.getVehicles = DoGetVehicles;
}
public_description = new Description();

function DoGetVehicles(sLynxID)
{
    var sRet = "";
    var sVehList = "";
	try
	{
		var objExe = new ActiveXObject("DataPresenter.CExecute");
        objExe.Initialize( Request.ServerVariables("APPL_PHYSICAL_PATH") );
        var sVehList = objExe.ExecuteSpStrAsXML( "uspRptShopAssignVehicleListXML", "rptShopAssignmentVehicleList.xsl", sLynxID);

		return sVehList + "||1"; 
	}
	catch(e)
	{
		SaveErrorData( ServerSideEvent(), "RSGetVehicles:DoGetVehicles()", e.message );
        return e.message + "||0";
	}
}

</script>