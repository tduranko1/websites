<%
  response.expires = -1
dim strDocPath, strDocExt
strDocPath = request.QueryString("docPath")
strDocExt = ""
if instrrev(strDocPath, ".") > 0 then
strDocExt = mid(strDocPath, instrrev(strDocPath, "."), len(strDocPath))
strDocExt = LCase(strDocExt)
else
strDocExt = "error"
end if
select case strDocExt
case ".xml":
%>
<!-- #include file="viewXML.asp" -->
<%
        case ".pdf":
%>
<!-- #include file="viewPDF.asp" -->
<%
        case ".tif":
%>
<!-- #include file="viewTIF.asp" -->
<%
        case ".jpg":
%>
<!-- #include file="viewJPG.asp" -->
<%
        case ".gif":
%>
<!-- #include file="viewJPG.asp" -->
<%
        case ".bmp":
%>
<!-- #include file="viewJPG.asp" -->
<%
        case ".doc":
%>
<!-- #include file="viewDOC.asp" -->
<%
        case ".zip":
		response.redirect "viewzip.asp?docpath=" & strDocPath
%>

<%
        case "error":
%>

<html>
  <head>
    <title></title>

    <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
    <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
    <style>
      .toolbarButton {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border:0px;
      }

      .toolbarButtonOver {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border-top:1px solid #D3D3D3;
      border-left:1px solid #D3D3D3;
      border-bottom:1px solid #696969;
      border-right:1px solid #696969;
      }

      .toolbarButtonOut {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border:0px;
      }

      span {
      cursor: default;
      }
    </style>

    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

    <script language="javascript">
        function closeMe() {
            //window.close();
            if (window.self == window.top) {
                window.returnValue = true;
                close();
            }
            else {
                window.self.frameElement.style.display = "none";
            }
        }
        function tb_onmouseover(obj) {
            obj.className = "toolbarButtonOver";
        }

        function tb_onmouseout(obj) {
            obj.className = "toolbarButtonOut";
        }
    </script>
  </head>

  <body topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0" style="overflow:hidden;border:1px solid #000000;">
    <table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
      <tr style="height:24px">
        <td align="right">
          <button id="btnClose" tabIndex="-1" tabStop="false" onclick="closeMe()" title="Close Window" class="toolbarButton">
            <img src="/images/close.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)">
          </button>
        </td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
      <tr>
        <td align="center" >
          <font color="#FF0000">Invalid document location specified or the document does not exist.</font>
        </td>
      </tr>
    </table>
  </body>
</html>

<%
    case else:
%>
<embed src=''<%=request.QueryString("docPath")%>' width="100%" height="100%"/>
<%
    end select
%>