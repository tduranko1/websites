<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim strLynxID, strUserID
  Dim strInsuranceCompanyID
  Dim objExe, strHTML

  'Get the insurance company ID from the session
  strLynxID = GetSession("LynxID")
  strUserID = GetSession("UserID")
  CheckError()

  'Create and initialize DataPresenter
  'Set objExe = CreateObject("DataPresenter.CExecute")
  'CheckError()

  'objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
  'CheckError()

  'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
  'objExe.AddXslParam "LynxID", strLynxID
  'CheckError()

  'strHTML = objExe.ExecuteSpAsXML( "uspClaimHistoryGetDetailXML", "ClaimHistory.xsl", strLynxID, strInsuranceCompanyID )
  'CheckError()

  'Set objExe = Nothing

  'Call KillSessionObject

  'Response.Write strHTML

          '------------------------------------'
		' APD.NET Local Params
		'------------------------------------'
        dim sParams
        sParams = "LynxID:" & strLynxID
        sParams = sParams + "|UserID:" & strUserID

		'------------------------------------'
		' APD.NET Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
        'response.write("sASPXSessionKey" & " - " & sASPXSessionKey & "<br/>")
        'response.write("sASPXWindowID" & " - " & sASPXWindowID & "<br/>")
        'response.write("strUserID" & " - " & strUserID & "<br/>")
        'response.write("strLynxID" & " - " & strLynxID & "<br/>")
        'response.write("strVehNum" & " - " & strVehNum & "<br/>")
        'response.write("strInsuranceCompanyID" & " - " & strInsuranceCompanyID & "<br/>")
		'response.end

		'------------------------------------'
		' APD.NET Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("CLaimHistory.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & SASPXWindowID & "&Params=" & sParams)

  On Error GoTo 0


%>
