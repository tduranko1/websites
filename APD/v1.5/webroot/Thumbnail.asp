<%@ LANGUAGE="VBSCRIPT"%>
<!-- #include file="errorhandler.asp" -->
<%
    Dim objThumb, objStream

    Response.Buffer = True
    
    dim docPath
    dim imgHeight, imgWidth
    dim sExt
    dim objADOStream
    
    imgHeight = request("height")
    imgWidth = request("width")
    
    if imgHeight = "" then imgHeight = 50
    
    if imgWidth = "" then imgWidth = 50
    'imgHeight = 50
    'imgWidth = 50
    
    docPath = request.queryString("doc")
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    Call CheckError()
    
    if fso.FileExists(docPath) then
      sExt = LCase(fso.GetExtensionName(docPath))
      select case sExt
        case "jpg", "gif", "png", "bmp", "emf", "wmf":
          'Set objThumb = CreateObject("ImageUtils.Thumbnail")
          Call CheckError()

	response.cookies("lsdocPath") = docPath
	response.cookies("lsimgHeight") = imgHeight
	response.cookies("lsimgWidth") = imgWidth 

	  Response.Redirect("Thumbnail.aspx")
          'Set objStream = objThumb.GetThumbnail( docPath, imgHeight, imgWidth )
          Call CheckError()
         
          'Response.Clear
          'Response.ContentType="image/png"
          'Response.AddHeader "Content-Disposition", "inline;thumbNail.png"
          'Response.BinaryWrite objStream.Read
          
          objStream.Close
          Set objStream = Nothing
          Set objThumb = Nothing
          
        case else
          set objADOStream = CreateObject("ADODB.Stream")
          objADOStream.charset = "UTF-8"
          objADOStream.type = 1 'Binary
          objADOStream.open 
          select case sExt
            case "tif", "tiff":
              objADOStream.loadFromFile Server.mapPath("/images/TIFImage.gif")
            case "doc":
              objADOStream.loadFromFile Server.mapPath("/images/DOCImage.gif")
            case "pdf":
              objADOStream.loadFromFile Server.mapPath("/images/PDFImage.gif")
            case "xml":
              objADOStream.loadFromFile Server.mapPath("/images/XMLImage.gif")
            case else
              objADOStream.loadFromFile Server.mapPath("/images/unknownImage.gif")
          end select 
          Response.Clear
          Response.ContentType="image/gif"
          Response.AddHeader "Content-Disposition", "inline;thumbNail.gif"
          Response.BinaryWrite objADOStream.Read
          objADOStream.close
          set objADOStream = nothing
      end select
    else
      set objADOStream = CreateObject("ADODB.Stream")
      objADOStream.charset = "UTF-8"
      objADOStream.type = 1 'Binary
      objADOStream.open 
      objADOStream.loadFromFile Server.mapPath("/images/NoImage.gif")
      Response.Clear
      Response.ContentType="image/gif"
      Response.AddHeader "Content-Disposition", "inline;thumbNail.gif"
      Response.BinaryWrite objADOStream.Read
      objADOStream.close
      set objADOStream = nothing
    end if
    
    set fso = nothing
    
    Response.End
%>
