<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- CheckList.asp
	This file handles the Insert, Update and Complete for Diary.
	It is used in a popup dialog window.
-->

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
    On Error Resume Next

    Dim objExe
    
    'Extract the query data.
    Dim Action, Entity, UserID, LynxID, TaskID, TaskCompleted, strResult

    Action = Request("Action")
    Entity = Request("Entity")
    UserID = Request("UserID")
    TaskID = Request("TaskID")
    LynxID = Request("LynxID")
    TaskCompleted = Request("TaskCompleted")
    
    If Not HasValue( LynxID ) Then LynxID = GetSession("LynxID")
    If Not HasValue( UserID ) Then UserID = GetSession("UserID")

    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, UserID

    objExe.Trace "Action='" & Action & "' Entity='" & Entity & "' TaskID='" & TaskID & "' LynxID='" & LynxID & "'", "CheckList.asp"

    'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
    objExe.AddXslParam "LynxId", LynxID
    objExe.AddXslParam "OwnerUserId", GetSession("OwnerUserID")
    objExe.AddXslParam "RawContext", GetSession("Context")
    
    If Entity = "CheckListComplete" Then
      objExe.AddXslParam "TaskCompleted", TaskCompleted
    End If
    strResult = objExe.ExecuteSpAsXML( "uspDiaryGetDetailXML", Entity & ".xsl", TaskID, LynxID, UserID )
    Response.write strResult

    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError
        
%>
