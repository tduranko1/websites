<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strLynxID
	Dim strUserID
	Dim strInvolvedID
	Dim strInsuranceCompanyID
	Dim strHTML

	On Error Resume Next

	strLynxID = GetSession("LynxID")
	strUserID = GetSession("UserID")
	strInvolvedID = Request("InvolvedID")
	CheckError()

	'strLynxID = 2600
	'strInvolvedID = 27

	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	' Add LynxId variable to be stuffed into xsl.
	' UserId and SessionKey were added in Initialize call above.
	objExe.AddXslParam "LynxId", strLynxID
	CheckError()

	' Get claim XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimPedestrianGetDetailXML", "ClaimPedestrian.xsl", strInvolvedID, strInsuranceCompanyID )
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>
