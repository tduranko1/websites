<%@ Language=VBScript %>
<% Option Explicit %>

<html xmlns:IE>
<head>
	<title>APD Document Upload Dialogue Browse</title>

<STYLE type="text/css">
  IE\:APDButton {behavior:url("/behaviors/APDButton.htc")}
  IE\:APDFileOpen {behavior:url("/behaviors/APDFileOpen.htc")}
  Input {
  	font-family: Arial, sans-serif;
  	font-weight: normal;
  	font-size: 8pt;
  	}
</STYLE>

<script language="JavaScript">

  function uploadFile(gsLynxID, lsDocPertainsTo, lsClaimAspectServiceChannelID, lsFileType, lsSupSeqNumber, gsUserID, strWarrantyFlag)
  {
    //document.frmFileUpload.submit();
    doFileUpload(gsLynxID, lsDocPertainsTo, lsClaimAspectServiceChannelID, lsFileType, lsSupSeqNumber, gsUserID, strWarrantyFlag);
  }

  function doFileUpload(gsLynxID, lsDocPertainsTo, lsClaimAspectServiceChannelID, lsFileType, lsSupSeqNumber, gsUserID, strWarrantyFlag)
  {
		var bDebug = false;
    var aFiles = null;
    var sFileList = filUpload.value;
    aFiles = sFileList.split(";");

    //create an XML Document that will contain the individual file
    var oXML = new ActiveXObject("MSXML2.DOMDocument");
    oXML.loadXML('<?xml version="1.0" ?> <root/>');
    oXML.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");

    //add parameters to the root
    oRoot = oXML.documentElement.selectSingleNode("/root");
    if (oRoot) {
      oRoot.setAttribute("LynxID", gsLynxID);
      oRoot.setAttribute("ClaimAspectServiceChannelID", lsClaimAspectServiceChannelID);
      oRoot.setAttribute("PertainsTo", escape(lsDocPertainsTo));
      oRoot.setAttribute("FileType", lsFileType);
      oRoot.setAttribute("SupplementSeqNo", lsSupSeqNumber);
      oRoot.setAttribute("UserID", gsUserID);
      oRoot.setAttribute("DocumentSource", "User");
      oRoot.setAttribute("WarrantyFlag", strWarrantyFlag);
    } else
      alert("Internal Error. No root node found.");

    //create an ADO Stream to load the file
    var oADOStream = new ActiveXObject("ADODB.Stream");
    var oFSO = new ActiveXObject("Scripting.FileSystemObject");
    var sFile = "";

    for (var i = 0; i < aFiles.length; i++)
		{
      sFile = aFiles[i];
      sFile = sFile.replace(/\\/g, "\\\\"); //escape the back-slash
      sFile = sFile.replace(/\"/g, ""); // remove the double quote "

      var oFileNode = oXML.createElement("File");
      var lFileLength = 0;

			if ( !bDebug )
			{
      	var oFile = oFSO.GetFile(sFile);
      	lFileLength = oFile.Size;
      	oFile = null;
			}

      oFileNode.setAttribute("FileName", oFSO.GetBaseName(sFile) + "." + oFSO.GetExtensionName(sFile));
      oFileNode.setAttribute("FileExt", oFSO.GetExtensionName(sFile));
      oFileNode.setAttribute("FileLength", lFileLength);
      oFileNode.dataType = "bin.base64";

			if ( !bDebug )
			{
      	//read the file into the ADO Stream
      	oADOStream.Type = 1; // Binary type
      	oADOStream.Open();
      	oADOStream.LoadFromFile(sFile);

      	//Store this file into the File node
      	oFileNode.nodeTypedValue = oADOStream.Read(-1); // Read All data
      	oADOStream.Close();
			}

      oXML.documentElement.appendChild(oFileNode);
    }

    var oXMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
    oXMLHttp.open("POST","DocumentFileUpload.asp",false);
    oXMLHttp.send(oXML);
    // show server message in message-area
    var sResponse = oXMLHttp.ResponseText;
//    alert(sResponse);
//    window.clipboardData.setData("Text", sResponse);
    var aDocIDs = sResponse.split(";");
    var ifrmDocDetails = parent.document.getElementById("ifrmDocDetails").contentWindow;
    var iDeleteAfter = 0;
    if (parent.cbDelFile)
      iDeleteAfter = parent.cbDelFile.value;

    for (var i = 0; i < aDocIDs.length; i++){
      if (parseInt(aDocIDs[i], 10) > 0) {
        if (iDeleteAfter == 1) {
          sFile = aFiles[i];
          sFile = sFile.replace(/\\/g, "\\\\"); //escape the back-slash
          sFile = sFile.replace(/\"/g, ""); // remove the double quote "
          if (oFSO.FileExists(sFile)){
            var f = oFSO.GetFile(sFile);
            if ((f.attributes && 1) == 1) {
              var iRemoveReadOnly = (f.attributes >> 1);//readonly if bit 1. so right shift once
              f.attributes = (iRemoveReadOnly << 1);//now left shift once to remove set the readonly bit to 0
            }
            f = null;
            oFSO.DeleteFile(sFile);
          }
        }
        ifrmDocDetails.contSaveDoc(aDocIDs[i]);
      }
    }

    ifrmDocDetails.closeMe();
  }

  function checkMultiple(){
    if (filUpload.files.length > 1) {
      if (parent.btnViewDoc) {
        parent.btnViewDoc.CCDisabled = true;
      }
    } else {
      if (parent.btnViewDoc) {
        parent.btnViewDoc.CCDisabled = false;
      }
    }
  }

</script>

</head>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" tabindex="-1" style="overflow:hidden">
  <IE:APDFileOpen id="filUpload" name="filUpload" width="500"
    filterIndex="1" filter="All APD File,*.pdf;*.doc;*.tif;*.bmp;*.jpg;*.gif,Photographs,*.bmp;*.jpg;*.tif;*.gif"
    canDirty="false" CCDisabled="false" dialogTitle="Select Files to Upload" required="false" tabIndex="2" multiple="true"
    onChange="checkMultiple()"
    />

  <!-- <form name="frmFileUpload" method="post" encType="multipart/form-data" action="DocumentFileUpload.asp">

    <input type="file" name="theFile" id="theFile" size="82" tabindex="1"/>

  </form> -->

</body>
</html>
