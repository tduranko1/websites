<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
    <p style="margin:0px;padding:0px">This is to advise that the following rental is being managed for the associated repair assignment below. <xsl:choose>
      <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SourceApplicationID='4'">through our First Notice of Loss Department</xsl:when>
      <xsl:otherwise/>
    </xsl:choose></p>
      <br/>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b><xsl:choose><xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '1'">Insured</xsl:when><xsl:otherwise>Claimant</xsl:otherwise></xsl:choose>:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '3'">
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/Root/Claim/@InsuredName"/>
        </xsl:otherwise>
      </xsl:choose></p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel, ' - ')"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleDrivable = '1'">Drivable</xsl:when>
        <xsl:otherwise>Not Drivable</xsl:otherwise>
      </xsl:choose>
      </p>
      <!--<p style="margin:0px;padding:0px"><b>Coverage:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@CoverageName"/>
      </p>-->
      <p style="margin:0px;padding:0px"><b>Deductible:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@CoverageName)"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Repair Facility:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName"/>
      <xsl:if test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName != ''">
        <xsl:value-of select="concat(' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopPhone)"/>
      </xsl:if>
      </p>
      <!--<p style="margin:0px;padding:0px"><b>Expected Estimate Receipt Date:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="'2-3 business days'"/>
      </p>-->
      <p style="margin:0px;padding:0px"><b>Rental Coverage:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/@RentalDailyAmt != '' or /Root/Claim/@RentalMax != ''">
          <xsl:value-of select="concat(/Root/Claim/@RentalDailyAmt, ' per day, ',  /Root/Claim/@RentalMax, ' limit')"/>
        </xsl:when>
        <xsl:otherwise>No rental stated to manage</xsl:otherwise>
      </xsl:choose>

      </p><br/><br/>
      
      <p style="margin:0px;padding:0px"><b>RENTAL:</b></p>
	
      <xsl:choose>
        <xsl:when test="/Root/Claim/RentalMgt/@RentalAgency != ''">
			  <p style="margin:0px;padding:0px"><b>Rental Vendor:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalAgency"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Reservation/Confirmation #:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@ResConfNumber"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Vehicle Class:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@VehicleClass"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rate Type:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RateTypeCD"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rate:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@Rate"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Tax Rate:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@TaxRate"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Authorized Pickup Date::</b> 
				  <xsl:call-template name="space"/>
				  <xsl:call-template name="formatDate">
					  <xsl:with-param name="DateTimeStr" select="/Root/Claim/RentalMgt/@AuthPickupDate"/>
				  </xsl:call-template>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental Start Date:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:call-template name="formatDate">
					  <xsl:with-param name="DateTimeStr" select="/Root/Claim/RentalMgt/@StartDate"/>
				  </xsl:call-template>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental End Date:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:call-template name="formatDate">
					  <xsl:with-param name="DateTimeStr" select="/Root/Claim/RentalMgt/@EndDate"/>
				  </xsl:call-template>
			  </p>

			  <p style="margin:0px;padding:0px"><b>Rental Phone:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:call-template name="formatPhone">
					  <xsl:with-param name="val" select="/Root/Claim/RentalMgt/@RentalAgencyPhone"/>
				  </xsl:call-template>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental Address:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalAgencyAddress1"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental City:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalAgencyCity"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental State:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalAgencyState"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>Rental Zip:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalAgencyZip"/>
			  </p>
			  <br/>
			  <p style="margin:0px;padding:0px"><b>RENTAL DAYS:</b> 
				  <xsl:call-template name="space"/>
				  <xsl:value-of select="/Root/Claim/RentalMgt/@RentalNoOfDays"/>
			  </p>
			  <p style="margin:0px;padding:0px"><b>RENTAL AMOUNT:</b> 
				  <xsl:call-template name="space"/>
				  $ <xsl:value-of select="/Root/Claim/RentalMgt/@Rental"/> 
			  </p>
			  
        </xsl:when>
        <xsl:otherwise>No rental stated to manage</xsl:otherwise>
      </xsl:choose>	
      
      <br/>
      <br/><p style="margin:0px;padding:0px">Should you have any questions, please contact me.
	  </p>
      <br/>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claim Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>

	<xsl:template name="formatDate">
		<xsl:param name="DateTimeStr" />

		<!-- input format xslt datetime string -->
		<!-- output format mm/dd/yyyy -->

		<xsl:variable name="datestr">
			<xsl:value-of select="substring-before($DateTimeStr,'T')" />
		</xsl:variable>

		<xsl:variable name="mm">
			<xsl:value-of select="substring($datestr,6,2)" />
		</xsl:variable>

		<xsl:variable name="dd">
			<xsl:value-of select="substring($datestr,9,2)" />
		</xsl:variable>

		<xsl:variable name="yyyy">
			<xsl:value-of select="substring($datestr,1,4)" />
		</xsl:variable>

		<xsl:value-of select="concat($mm,'/', $dd, '/', $yyyy)" />
	</xsl:template>
</xsl:stylesheet>