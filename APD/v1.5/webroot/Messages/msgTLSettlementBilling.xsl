<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
      <p style="margin:0px;padding:0px"><b>LYNX ID:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/@LynxID"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Date of Loss:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@LossDate"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Insured:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@InsuredName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel)"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle Owner:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Lien Holder:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@LienHolderName"/>
      </p>
      <br/><p style="margin:0px;padding:0px">Please be advised that the following Lien Holder payoff amount has been confirmed and billed for your remittance to LYNX:</p>
      <br/>
      <p><b>Payoff Amount:</b> <xsl:value-of select="concat(' $ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PayoffAmount)"/></p>
      <p>The following <u>documents</u> are attached for your review:</p>
      <div id="documentList">$DocumentList$</div><br/>
      <p style="margin:0px;padding:0px">Should you have any questions, please contact me.</p>
      <br/>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Operations Support Specialist</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>