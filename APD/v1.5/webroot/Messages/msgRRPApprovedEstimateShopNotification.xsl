<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function removeOffice(s) {
          var strRet = s;
          if (s.indexOf("(") != -1)
            strRet = s.substring(0, s.indexOf("(") - 1);
          else
            strRet = s;
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <!--<p style="margin:0px">-->
      <div id="_msgFaxName" style="font-weight:bold"><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName"/></div>
      <div id="_msgFaxNumber">
        <xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopEmail != ''">
            <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopEmail"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopFax"/>
          </xsl:otherwise>
        </xsl:choose>
      </div>
      <div id="_msgFaxContact"><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopContact"/>,</div><br/>
      Please be advised LYNX Services has completed the estimate review on the assignment requested by:<br/><br/>
      <b><xsl:value-of select="/Root/Claim/@InsuranceCompanyName"/></b><br/>
      <xsl:value-of select="user:toProperCase(concat(user:removeOffice(string(/Root/Claim/@AdjusterFName)), ' ', /Root/Claim/@AdjusterLName))"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterPhone"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterEmail"/><br/><br/>
      <b>Claim Information:</b><br/>
      Claim #: <xsl:value-of select="//Claim/@ClientClaimNumber"/><br/>
      Insured: <xsl:value-of select="//Claim/@InsuredName"/><br/>
      Loss Date: <xsl:value-of select="//Claim/@LossDate"/><br/>
      LYNX ID:
		<xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear != ''">
			<xsl:value-of select="concat(//Root/@LynxID, '-', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAspectNumber)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat(//Root/@LynxID, '-', //Vehicle/@ClaimAspectNumber)"/>
          </xsl:otherwise>
        </xsl:choose>
	  <br/>
	  
	  Vehicle:
        <xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear != ''">
            <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel )"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat(//Vehicle/@VehicleYear, ' ', //Vehicle/@VehicleMake, ' ', //Vehicle/@VehicleModel)"/>
          </xsl:otherwise>
        </xsl:choose>
	  <br/>	
	  <br/>	

      
      The estimate revisions requested in our recent contact with you have been confirmed on the revised estimate you submitted.
      <br/><br/>
      <b>
        <u>Upon receipt of this notification you may contact the vehicle owner to secure permission to proceed with repairs in compliance with the approved estimate.</u>
      </b>
      <br/><br/>

      <b>IMPORTANT NOTICES BELOW - PLEASE READ</b>
      <ul>
          <li>Questions regarding <u>rental cars, coverage or payment(s)</u>, should be directed to the <u>QBE</u> claim adjuster noted above.</li>
          <li>Any questions related to the <u>approved repair estimate</u> should be directed to the <u>LYNX</u> contact shown below.</li>
          <li>All <u>supplemental damages</u> must have approval from <u>LYNX</u> Services prior to additional repairs being started.</li>
          <li>Fax all supplement requests to (239) 479-5943. Please include any <u>relevant invoices</u> with the supplement.<br/>
          <b>NOTE: Lynx ID # <xsl:value-of select="/Root/@LynxID"/>-<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAspectNumber"/> must be prominently noted to reach the proper file handler.</b></li>
          <li>If applicable, you may include any available "Direction To Pay" with your final documents. As payment is the responsibility of the insurance carrier, DTP will be forwarded with our audit report to the client adjuster shown above.</li>
      </ul>
      <p style="margin:0px">Your assistance is greatly appreciated.</p><br/>
      <p style="margin:0px">Thanks,</p><br/>
     <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Material Damage Specialist
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
      <!--</p>-->
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>



