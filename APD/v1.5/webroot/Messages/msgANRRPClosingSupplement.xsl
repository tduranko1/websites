<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function removeOffice(s) {
          var strRet = s;
          if (s.indexOf("(") != -1)
            strRet = s.substring(0, s.indexOf("(") - 1);
          else
            strRet = s;
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(user:removeOffice(string(/Root/Claim/@AdjusterFName)), ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
      <p style="margin:0px">
      Policy Number: <xsl:value-of select="//Claim/@PolicyNumber"/><br/>
      Claim Number: <xsl:value-of select="//Claim/@ClientClaimNumber"/><br/>
      Insured Name: <xsl:value-of select="//Claim/@InsuredName"/><br/>
      Loss Date: <xsl:value-of select="//Claim/@LossDate"/><br/><br/>
      </p>
      <br/>
      <p style="margin:0px">Please be advised the supplement review for this assignment has been <u>completed</u>.
        Please review the uploaded approved supplement to determine the amount you need to pay the vehicle owner and/or shop for the <u>incremental</u> repair cost.
      </p>
      <br/>
      <p style="margin:0px"><b>Auditor Notes:</b> Supplement review results have been detailed in the uploaded "Approved Supplement".</p>
      <br/>
      <p style="margin:0px">The following applicable <u>supplement documents</u> are attached for your review:</p>
      <br/>
      <div id="documentList">$DocumentList$</div><br/>
      
      <p style="margin:0px">Should you have any questions, please contact me.</p>
      <br/>
      <p style="margin:0px">Thank you for the assignment.</p>
      <br/>
      <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claim Representative
		</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>
