<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <DIV style="FONT:10pt Arial">
    <P style="MARGIN: 0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></P><br/>
    <P style="MARGIN: 0px">Please be advised the status of this claim has critically changed as follows:</P>
      <BR/>
      <P style="MARGIN: 0px"><B>[<xsl:call-template name="space"/>] Delayed Repair Start - Parts Unavailability</B></P>
      <P style="MARGIN: 0px"><B>[<xsl:call-template name="space"/>] Delayed Repair Start - Vehicle Owner Availability</B></P>
      <P style="MARGIN: 0px"><B>[<xsl:call-template name="space"/>] Other</B><xsl:call-template name="space"/></P>
      <BR/>
      <P style="MARGIN: 0px"><B>Additional Comments:</B>
      <xsl:call-template name="space"/>
      </P>
      <BR/><P style="MARGIN: 0px">Please contact me with any questions, issues or concerns.</P>
      <BR/>
      <P style="MARGIN: 0px"><B><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claims Representative</B>
      <BR/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </P>
    </DIV>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>