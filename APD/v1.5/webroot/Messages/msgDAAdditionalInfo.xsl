<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px">
      <xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/>
      </p><br/>
      <p style="margin:0px">This is to acknowledge that I have received your new desk audit assignment for:
      </p><br/>
      <p style="margin:0px"><b>Insured/Claimant: </b>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '1'">
          <xsl:value-of select="/Root/Claim/@InsuredName"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
        </xsl:otherwise>
      </xsl:choose>
      </p>
      <br/><p style="margin:0px">In reviewing the file I have determined the
      <u>estimate</u> was not received with the assignment. Please forward the estimate so I may begin the audit.
      </p>
      <br/>
      <p style="margin:0px"><b>Additional Comments:</b></p>
      <br/><p style="margin:0px">Thank you for the assignment, and feel free to contact me with any questions.</p>
      <br/>
      <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Quality Control Representative
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>