<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function removeOffice(s) {
          var strRet = s;
          if (s.indexOf("(") != -1)
            strRet = s.substring(0, s.indexOf("(") - 1);
          else
            strRet = s;
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <!--<p style="margin:0px">-->
      <div id="_msgFaxName" style="font-weight:bold"><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName"/></div>
      <div id="_msgFaxNumber">
        <xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopEmail != ''">
            <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopEmail"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopFax"/>
          </xsl:otherwise>
        </xsl:choose>
      </div>
      <div id="_msgFaxContact"><br/><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopContact"/>,</div><br/>
      Please be advised LYNX Services has reviewed your submitted estimate and completed BCIF report on the assignment requested by:<br/><br/>
      <b><xsl:value-of select="/Root/Claim/@InsuranceCompanyName"/></b><br/>
      <xsl:value-of select="user:toProperCase(concat(user:removeOffice(string(/Root/Claim/@AdjusterFName)), ' ', /Root/Claim/@AdjusterLName))"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterPhone"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterEmail"/><br/><br/>
      <b>Claim Information:</b><br/>
      Claim #: <xsl:value-of select="//Claim/@ClientClaimNumber"/><br/>
      Insured: <xsl:value-of select="//Claim/@InsuredName"/><br/>
      Loss Date: <xsl:value-of select="//Claim/@LossDate"/><br/>
      LYNX ID: 
	  	<xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear != ''">
			<xsl:value-of select="concat(//Root/@LynxID, '-', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAspectNumber)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat(//Root/@LynxID, '-', //Vehicle/@ClaimAspectNumber)"/>
          </xsl:otherwise>
        </xsl:choose>
	  <br/>

      Vehicle: 
        <xsl:choose>
          <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear != ''">
            <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel )"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat(//Vehicle/@VehicleYear, ' ', //Vehicle/@VehicleMake, ' ', //Vehicle/@VehicleModel)"/>
          </xsl:otherwise>
        </xsl:choose>
	  <br/>	
	  <br/>	

	  <p>
		Please be advised the following determination has been reached regarding the total loss status of the vehicle. 
	  </p>

  	  <p>
		_____ <b>Proceed with Repairs</b> - The estimate value <u>has <b>not</b> exceeded</u> the total loss threshold value. 
	  </p>
  	  <p>
		_____ <b>Total Loss</b> - The estimate value <u>has exceeded</u> the total loss threshold value. 
	  </p>

      <b>Note: </b>
	  <p>
		If a total loss, QBE will arrange to have the vehicle timely moved to salvage (21 days free vehicle storage 
		starting from the date the total loss was determined, is provided by LYNXAdvantage shops). Any costs for towing, 
		teardown, or storage on the vehicle will be paid directly by QBE or the salvage provider when the vehicle is picked up. 
	  </p>
	  
      <p style="margin:0px">Should you have any questions, please contact me.</p><br/>
     <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Material Damage Specialist
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
      <!--</p>-->
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>



