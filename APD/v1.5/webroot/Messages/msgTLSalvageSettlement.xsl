<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px">
    <b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageName"/></b><br/>
    <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddress1"/><br/>
    <xsl:if test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddress2 != ''">
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddress2"/><br/>
    </xsl:if>
    <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddressCity, ', ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddressState, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageAddressZip)"/><br/>
    <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageEmailAddress"/><br/>
    </p><br/>
      <p style="margin:0px;padding:0px"><b>Insurance Company:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@InsuranceCompanyName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel)"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Salvage Lot Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageControlNumber"/>
      </p><br/>
      <p style="margin:0px;padding:0px"><b>LYNX ID:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/@LynxID"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Date of Loss:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@LossDate"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Insured:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@InsuredName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle Owner:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Lien Holder:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@LienHolderName"/>
      </p>
      <br/>
      <br/>
      <p> Dear Sirs,</p>
      <p>Please be advised that required total loss settlement documentation has been received on the above listed vehicle. The following owner and applicable lien holder executed documents are enclosed so that you may proceed with the vehicle salvage disposition and recovery to <xsl:value-of select="/Root/Claim/@InsuranceCompanyName"/>.</p>
      <div id="documentList">$DocumentList$</div>
      <p><span style="background-color:#FFFF00">Tracking information and electronic Images of the documents forwarded to you are attached to the electronic copy of this transmittal, for your reference.</span></p>
      <p>Should you have any questions, please contact me.</p>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Operations Support Specialist</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>