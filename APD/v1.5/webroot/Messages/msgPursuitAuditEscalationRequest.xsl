<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function removeOffice(s) {
          var strRet = s;
          strRet = s.substring(0, s.indexOf("(") - 1);
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px">Met Choice, </p><br/>
    <p style="margin:0px;padding:0px">This is an <b>Escalation Request</b> for action needed on the following assignment:</p>
      <br/>
      <p style="margin:0px;padding:0px"><b><u>Metlife Choice Audit</u></b></p>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Insured:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@InsuredName"/></p>
      <p style="margin:0px;padding:0px"><b>Party:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '3'">3rd Party</xsl:when>
        <xsl:otherwise>1st Party</xsl:otherwise>
      </xsl:choose>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle Owner:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '3'">
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/Root/Claim/@InsuredName"/>
        </xsl:otherwise>
      </xsl:choose>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel, ' - ')"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleDrivable = '1'">Drivable</xsl:when>
        <xsl:otherwise>Not Drivable</xsl:otherwise>
      </xsl:choose>
      </p>
      <p style="margin:0px;padding:0px"><b>Deductible:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@CoverageName)"/>
      </p>
      <br/>
      <p style="margin:0px;padding:0px"><b>Shop:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName"/><br/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopAddress, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopCity, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopState, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopZip)"/>
      </p>
      <br/>
      <p style="margin:0px;padding:0px">
      <b>Assignment Date:</b><xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@IntakeFinishDate"/>
      <br/>
      <b>Inspection Date:</b><xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@InspectionDate"/>
      </p>
      <br/>
      <p style="margin:0px;padding:0px"><b>Request Notes:</b>
      <xsl:call-template name="space"/>
      </p>
      <br/><p style="margin:0px;padding:0px">Please contact me if you have any questions regarding this request.</p>
      <br/>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Customer Service Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>






