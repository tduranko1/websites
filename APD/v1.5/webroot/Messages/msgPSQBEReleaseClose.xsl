<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function removeOffice(s) {
          var strRet = s;
          strRet = s.substring(0, s.indexOf("(") - 1);
          return strRet;
        }
        
        function netDifference(finalAmount, origAmount){
          return formatCurrency(finalAmount - origAmount);
        }
        
        function formatCurrency(sVal){
           strRet = "";
           sVal = String(sVal);
           if (isNaN(sVal) == false && sVal != ""){
              sVal = String(Math.round(parseFloat(sVal) * 1000) / 1000);
              if (sVal.indexOf(".") == -1) {
                 strRet = sVal + ".00";
              } else {
                 var strParts = sVal.split(".");
                 strRet = strParts[0];
                 if (strParts.length > 1) {
                    var strCents = strParts[1].substr(0, 4);
                    try {
                    if (strCents.length < 4){
                       strCents += charFill("0", 4 - strCents.length);
                    }
                    strCents = Math.round(parseFloat(strCents)/100) + "";
                    strCents = strCents + charFill("0", 2 - strCents.length);
                    } catch (e) {}
                    strRet += "." + strCents;
                 }
              }
           }
           return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
	 
		<xsl:variable name="NetDifference">
			<xsl:value-of select="user:netDifference(number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount), number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ApprovedNetEstimateAmount))"/>
		</xsl:variable>
  
		<div style="font:10pt Arial">
		<p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(user:removeOffice(string(/Root/Claim/@AdjusterFName)), ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>

		<p style="margin:0px;padding:0px">The net estimate amount to pay is $
		<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@NetEstimateAmount"/>. 
		</p>
		<br/>

		<table border="0" cellpadding="5px" cellspacing="0" style="font:10pt Arial;border-collapse:collapse">
			<tr>
				<td colspan="2"/>
				<td colspan="2" style="text-align:center;border:1px solid #000000;background-color:#C0C0C0;color:#0000ff;">
					Pay this Amount:
				</td>
			</tr>
			<tr style="background-color:#C0C0C0;">
				<td style="text-align:center;border:1px solid #000000;color:#0000ff;">Approved Initial Estimate Net Amount</td>
				<td style="text-align:center;border:1px solid #000000;color:#0000ff;">Final Estimate Net Amount</td>
				<td style="text-align:center;border:1px solid #000000;color:#0000ff;">Approved Initial Estimate Net Amount was paid</td>
				<td style="text-align:center;border:1px solid #000000;color:#0000ff;">Approved Initial Estimate Net Amount was <b><u>NOT</u></b> paid</td>
			</tr>
			
			<tr>
				<td style="border:1px solid #000000">
					<xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ApprovedNetEstimateAmount)"/>
				</td>
				<td style="border:1px solid #000000">
					<xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount)"/>
				</td>
				<td style="border:1px solid #000000">
					<xsl:choose>
						<!-- Final is less than Approved. So refund the difference -->
						<xsl:when test="$NetDifference &lt; 0">
							<span style="color:#FF0000"><b>DO NOT PAY. </b></span>Overage will be refunded
						</xsl:when>
						<!-- Final is same approved -->
						<xsl:when test="$NetDifference = 0">
							$ 0.00
						</xsl:when>
						<!-- Final is greater than Approved -->
						<xsl:when test="$NetDifference &gt; 0">
							<xsl:value-of select="concat('$ ', $NetDifference)"/>
						</xsl:when>
					</xsl:choose>
				</td>
				<td style="border:1px solid #000000">
					<xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount)"/>
				</td>
				
			</tr>			
		</table>

		<br/>
		
		<p style="margin:0px;padding:0px">The following applicable <u>closing documents</u> are included for your review:
		<xsl:call-template name="space"/>
		</p>
	
		<p style="margin:0px;padding:0px">•  Revised/Additional Invoice 
		<xsl:call-template name="space"/>
		</p>
		
		<p style="margin:0px;padding:0px">•  Final Estimate 
		<xsl:call-template name="space"/>
		</p>

		<p style="margin:0px;padding:0px">•  13 Photographs 
		<xsl:call-template name="space"/>
		</p>
		
		<p style="margin:0px;padding:0px">•  Repair Acceptance
		<xsl:call-template name="space"/>
		</p>
		
		<br/>

		<p style="margin:0px;padding:0px">Thank you for the assignment.</p>
		<br/>

		<p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Claim Representative</b>
		<br/>
		<xsl:call-template name="formatPhone">
		<xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
		</xsl:call-template>
		<br/>
		<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
		</p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>