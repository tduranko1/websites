<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
    <p style="margin:0px;padding:0px">This is to acknowledge that I have received your new assignment <xsl:choose>
      <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SourceApplicationID='4'">through our First Notice of Loss Department</xsl:when>
      <xsl:otherwise/>
    </xsl:choose> for:</p>
      <br/>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b><xsl:choose><xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '1'">Insured</xsl:when><xsl:otherwise>Claimant</xsl:otherwise></xsl:choose>:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PartyCD = '3'">
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/Root/Claim/@InsuredName"/>
        </xsl:otherwise>
      </xsl:choose></p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel, ' - ')"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleDrivable = '1'">Drivable</xsl:when>
        <xsl:otherwise>Not Drivable</xsl:otherwise>
      </xsl:choose>
      </p>
      <!--<p style="margin:0px;padding:0px"><b>Coverage:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@CoverageName"/>
      </p>-->
      <p style="margin:0px;padding:0px"><b>Deductible:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@CoverageName)"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Repair Facility:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName"/>
      <xsl:if test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopName != ''">
        <xsl:value-of select="concat(' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ShopPhone)"/>
      </xsl:if>
      </p>
      <p style="margin:0px;padding:0px"><b>Rental Coverage:</b>
      <xsl:call-template name="space"/>
      <xsl:choose>
        <xsl:when test="/Root/Claim/@RentalDailyAmt != '' or /Root/Claim/@RentalMax != ''">
          <xsl:value-of select="concat(/Root/Claim/@RentalDailyAmt, ' per day, ',  /Root/Claim/@RentalMax, ' limit')"/>
        </xsl:when>
        <xsl:otherwise>No rental stated to manage</xsl:otherwise>
      </xsl:choose>

      </p><br/>
      <p style="margin:0px;padding:0px"><b>Additional Comments:</b>
      <xsl:call-template name="space"/>
      <xsl:if test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SourceApplicationID='4'">
        <br/>If this acknowledgement has reached you as the adjuster in charge, and you are not assigned to the claim, please forward this acknowledgement information to the current assigned adjuster, and advise us of the revised contact information.
      </xsl:if>
      </p>
      <br/><p style="margin:0px;padding:0px">Thank you for the assignment, and feel free to contact me with any questions.</p>
      <br/>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claim Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>