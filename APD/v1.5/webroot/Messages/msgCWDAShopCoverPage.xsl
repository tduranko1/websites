<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <!--<p style="margin:0px">-->
      <div id="_msgFaxName" style="font-weight:bold">[Shop]</div>
      <div id="_msgFaxNumber">[Fax Number]</div>
      <div id="_msgFaxContact">[Shop Contact from Agreed Price Contact],</div><br/>
      Please be advised LYNX Services has completed the estimate audit requested by:<br/><br/>
      <b><xsl:value-of select="/Root/Claim/@InsuranceCompanyName"/></b><br/>
      <xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName))"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterPhone"/><br/>
      <xsl:value-of select="/Root/Claim/@AdjusterEmail"/><br/><br/>
      The following document(s) listed below are attached for your review, and detail the results of the audit as discussed in our recent contact with you.<br/>
      <div id="documentList">$DocumentList$</div><br/>
      <b>IMPORTANT NOTICES BELOW - PLEASE READ</b>
      <ul>
          <li>Questions regarding rental cars, coverage or payment(s), should be directed to the insurance carrier claim adjuster noted above.</li>
          <li>Any questions related to the audited repair estimate should be directed to the LYNX contact shown below.</li>
          <li>All supplemental damages must have approval from LYNX Services prior to additional repairs being started.</li>
          <li>Fax all supplement requests to (239) 479-5943. Please include any relevant invoices with the supplement.<br/>
          <b>NOTE: Lynx ID # <xsl:value-of select="/Root/@LynxID"/>-<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAspectNumber"/> must be prominently noted to reach the proper file handler.</b></li>
          <li>If applicable, you may include any available "Direction To Pay" with your final documents. As payment is the responsibility of the insurance carrier, it will be forwarded with our audit report to the client adjuster shown above.</li>
      </ul>
      <p style="margin:0px">Your assistance is greatly appreciated.</p><br/>
      <p style="margin:0px">Thanks,</p><br/>
     <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>
       <!-- 
          12Dec2011 - TVD - Adding Insurance Company ID test to 
          show a different message here 
        -->
       <xsl:choose>
         <xsl:when test="/Root/Claim/@InsuranceCompanyID = '327'">, Material Damage Specialist</xsl:when>
         <xsl:otherwise>, Quality Control Representative</xsl:otherwise>
       </xsl:choose>
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
      <!--</p>-->
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>