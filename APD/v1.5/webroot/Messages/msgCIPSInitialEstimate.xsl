<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function formatCurrency(dValue){
          var dRet = "0.00";
          if (isNaN(dValue) == false && dValue > 0){
              dRet = Math.ceil(dValue * 100) + "";
              dRet = dRet.substr(0, (dRet.length - 2)) + "." + dRet.substr((dRet.length - 2));
          }
          return dRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
    <p style="margin:0px">Please be advised the initial estimate and photos have been received, and are being forwarded to assist in your liability reserve evaluation.</p>
      <br/><p style="margin:0px">The following applicable documents are attached for your review:</p>
      <br/><div id="documentList">$DocumentList$</div>
      <br/>
      <p style="margin:0px">We will proceed to review the photos and estimate, and work with the shop to provide an approved repair estimate payment advice based on established estimating guidelines. 
      </p>
      <br/>
      <p style="margin:0px">Should the estimate value exceed the established total loss 70% threshold value you will be alerted to a <u>potential</u> Total Loss. Other status updates to this claim will be provided should they occur. 
      </p>
      <br/>
      <p style="margin:0px">Please contact me with any questions, issues or concerns. 
      </p>
      <br/>
      <p style="margin:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Material Damage Specialist</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
    <script language="javascript">
      <![CDATA[
        function calcNetAmount(){
          alert("Hello");
        }
      ]]>
    </script>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>