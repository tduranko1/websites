<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function removeOffice(s) {
          var strRet = s;
          strRet = s.substring(0, s.indexOf("(") - 1);
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(user:removeOffice(string(/Root/Claim/@AdjusterFName)), ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
      <p style="margin:0px">
      Policy Number: <xsl:value-of select="//Claim/@PolicyNumber"/><br/>
      Claim Number: <xsl:value-of select="//Claim/@ClientClaimNumber"/><br/>
      Insured Name: <xsl:value-of select="//Claim/@InsuredName"/><br/>
      Loss Date: <xsl:value-of select="//Claim/@LossDate"/><br/><br/>
      </p>
      <br/>
      <p style="margin:0px">Please be advised the supplement audit for this assignment has been <u>completed</u>. The shop
      <xsl:choose>
        <xsl:when test="number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@NetEstimateAmount) &gt; number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OriginalEstimateNetAmount)">AGREED</xsl:when>
        <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@AuditedEstimateAgreedPriceMetCD='Y'">AGREED</xsl:when>
        <xsl:otherwise>NON-AGREED</xsl:otherwise>
      </xsl:choose> revised net supplement amount to pay is $
      <xsl:choose>
        <xsl:when test="number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@NetEstimateAmount) &lt; number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OriginalEstimateNetAmount)">
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@NetEstimateAmount"/>.
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OriginalEstimateNetAmount"/>.
        </xsl:otherwise>
      </xsl:choose>
      </p>
      <br/>
      <p style="margin:0px"><b>Auditor Notes:</b> Audit results have been summarized in the uploaded "Desk Audit Summary Report."</p>
      <br/>
      <p style="margin:0px">The following applicable <u>supplement documents</u> are attached for your review:</p>
      
      <div id="documentList">$DocumentList$</div>
      
      <br/>
      <p style="margin:0px">Should you have any questions, please contact me.</p>
      <br/>
      <p style="margin:0px">Thank you for the assignment.</p>
      <br/>
      <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Material Damage Specialist
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>