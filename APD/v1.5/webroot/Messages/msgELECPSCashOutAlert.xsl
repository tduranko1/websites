<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function removeOffice(s) {
          var strRet = s;
          strRet = s.substring(0, s.indexOf("(") - 1);
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
		<p style="margin:0px;padding:0px">
			<xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/>
		</p><br/>
		<p style="margin:0px;padding:0px">Please be advised that it has come to our attention that the vehicle owner has decided not to have their vehicle repaired at this shop.</p>
		<br/>
		
		<p style="margin:0px;padding:0px">Attached is the <u>audited</u> estimate, plus any available photos for your records. 
			We have proceeded to close and bill this as a <b>Cash Out</b> assignment. A cash out fee will be included in the consolidated invoice detail of your next billing cycle statement.</p>
		<br/>

		<p style="margin:0px;padding:0px">The following applicable <u>cash out documents</u> are attached for your review:
		<xsl:call-template name="space"/>
		</p>
	
		<p style="margin:0px;padding:0px">•  Audited Estimate
		<xsl:call-template name="space"/>
		</p>

				<p style="margin:0px;padding:0px">•  Estimate EMS
		<xsl:call-template name="space"/>
		</p>

		<p style="margin:0px;padding:0px">•  Photographs
		<xsl:call-template name="space"/>
		</p>
		
		<br/>

		<p style="margin:0px;padding:0px">Should you have any questions, please contact me.</p>
		<br/>

		<p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claim Representative</b>
		<br/>
		<xsl:call-template name="formatPhone">
		<xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
		</xsl:call-template>
		<br/>
		<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
		</p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>