<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
      <p style="margin:0px">Please be advised the estimate audit for this assignment has been <u>completed</u>. The net estimate amount to pay is $<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@NetEstimateAmount"/>.</p>
      <br/>
      <p style="margin:0px">This is a <b>possible Total Loss</b>. The Audited Estimate has <u>not been provided</u> to the repair shop, and <u>no attempt</u> has been made to reach an agreed price.</p>
      <br/>
      <p style="margin:0px">The following applicable <u>closing documents</u> are attached for your review:</p>
      <div id="documentList">$DocumentList$</div>
	  <p style="margin:0px"><b>NOTE:</b>  If LYNX is managing an open rental on this claim assignment, the rental is being transferred (in the applicable rental vendor application) back to you for disposition handling.</p><br/> 
      <p style="margin:0px">Thank you for the assignment.</p>
      <br/>
      <p style="margin:0px"><b>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>
        <!-- 
          12Dec2011 - TVD - Adding Insurance Company ID test to 
          show a different message here 
        -->
        <xsl:choose>
          <xsl:when test="/Root/Claim/@InsuranceCompanyID = '327'">, Material Damage Specialist</xsl:when>
          <xsl:otherwise>, Quality Control Representative</xsl:otherwise>
        </xsl:choose>
      </b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>