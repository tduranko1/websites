<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output method="text" omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
<xsl:template match="/">Please contact vehicle owner today to arrange towing with customer.
Vehicle is not drivable. <xsl:choose>
<xsl:when test="/Claim/@rental != ''">
  <xsl:choose>
    <xsl:when test="/Claim/@rental = 'Rental Handled by Client'">Rental authorization is handled by <xsl:value-of select="/Claim/@InsuranceCompanyName"/>.</xsl:when>
    <xsl:when test="/Claim/@rental = 'Rental Not Required'">Rental is not required by <xsl:value-of select="/Claim/@InsuredClaimant"/> on this assignment.</xsl:when>
    <xsl:otherwise>Rental with <xsl:value-of select="/Claim/@rental"/> has been setup.</xsl:otherwise>
  </xsl:choose>
</xsl:when>
<xsl:otherwise>Rental with __________ has been setup.</xsl:otherwise>
</xsl:choose>
Note scheduled date and days to repair on your estimate. If available use LKQ &amp; A/M parts where applicable. Upload estimate &amp; photos within 48 hours.
Thank you for your prompt attention. Have a great day!. If you have any questions or concerns call <xsl:value-of select="/Claim/@ClaimOwnerName"/> at <xsl:value-of select="/Claim/@ClaimOwnerPhone"/>.
</xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>