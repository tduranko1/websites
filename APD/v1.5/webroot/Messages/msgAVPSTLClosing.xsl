<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px;padding:0px">
        <xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/>
      </p>
      <br/>
      <p style="margin:0px">Please be advised this estimate has exceeded the established total loss <xsl:value-of select="concat(/Root/Claim/@TLPercentage, '%')"/> threshold value, and has been determined to be a <b>Total Loss</b>.</p>
      <br/>
      <p style="margin:0px">We have proceeded to close and bill this as a total loss assignment. A total loss fee will be included in the consolidated
      invoice detail of your next billing cycle statement.</p>
      <br/>
      <p style="margin:0px">The following applicable <u>total loss documents</u> are attached for your review:</p>
	  <br/>
      <div id="documentList">$DocumentList$</div><br/>
	    <p style="margin:0px">
        <b>NOTE 1:</b>  If LYNX is managing an open rental on this claim assignment, the rental is being transferred (in the applicable rental vendor application) back to you for disposition handling.</p><br/>
      <p style="margin:0px">
        <b>NOTE 2:</b>  If the shop has paid for towing or prior storage fees, a LYNX invoice has been attached for your payment, so we may reimburse the shop for this expense.
      </p>
      <br/>
      <p style="margin:0px">Thank you for the assignment.</p><br/>
      <p style="margin:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claim Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>