<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function removeOffice(s) {
          var strRet = s;
          strRet = s.substring(0, s.indexOf("(") - 1);
          return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
      <p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p>
      <br/>
      <p style="margin:0px;padding:0px">Please be advised the status of this claim has changed as follows:</p>
		<br/>

		<p style="margin:0px;padding:0px"><b>[  ] Delay in Vehicle Repair</b>
		<xsl:call-template name="space"/>
		</p>

      <p style="margin:0px;padding:0px">
        <b>[  ] Escalation - Please Advise</b>
        <xsl:call-template name="space"/>
      </p>

      <p style="margin:0px;padding:0px"><b>[  ] Other</b>
		<xsl:call-template name="space"/>
		</p>

		<br/>

		<p style="margin:0px;padding:0px"><b>Additional Comments:</b>
		<xsl:call-template name="space"/>
		</p>

		<br/>

		<p style="margin:0px;padding:0px">Please contact me with any questions, issues or concerns.</p>
		<br/>

		<p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Material Damage Specialist</b>
		<br/>
		<xsl:call-template name="formatPhone">
		<xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
		</xsl:call-template>
		<br/>
		<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
		</p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>