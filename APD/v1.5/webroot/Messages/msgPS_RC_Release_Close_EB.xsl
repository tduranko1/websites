<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function netDifference(finalAmount, origAmount){
          return formatCurrency(finalAmount - origAmount);
        }
        
        function formatCurrency(sVal){
           strRet = "";
           sVal = String(sVal);
           if (isNaN(sVal) == false && sVal != ""){
              sVal = String(Math.round(parseFloat(sVal) * 1000) / 1000);
              if (sVal.indexOf(".") == -1) {
                 strRet = sVal + ".00";
              } else {
                 var strParts = sVal.split(".");
                 strRet = strParts[0];
                 if (strParts.length > 1) {
                    var strCents = strParts[1].substr(0, 4);
                    try {
                    if (strCents.length < 4){
                       strCents += charFill("0", 4 - strCents.length);
                    }
                    strCents = Math.round(parseFloat(strCents)/100) + "";
                    strCents = strCents + charFill("0", 2 - strCents.length);
                    } catch (e) {}
                    strRet += "." + strCents;
                 }
              }
           }
           return strRet;
        }
      ]]>
    </msxsl:script>

  <xsl:template match="/">
    <xsl:variable name="NetDifference">
      <xsl:value-of select="user:netDifference(number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount), number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ApprovedNetEstimateAmount))"/>
    </xsl:variable>
    <div style="font:10pt Arial">
      <p style="margin:0px;padding:0px;">
        <xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/>
      </p><br/>
      <p style="margin:0px;padding:0px;">Please be advised that repairs have been <u>completed</u> for this claim.<br/><br/>
         The net estimate amount to pay is $ <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount"/>.
         <!--<xsl:choose>
           <xsl:when test="$NetDifference &lt; 0">DO NOT PAY<br/>Overage will be refunded</xsl:when>
           <xsl:when test="$NetDifference = 0">$ 0.00</xsl:when>
           <xsl:when test="$NetDifference &gt; 0">
             <xsl:value-of select="concat('$ ', $NetDifference)"/>
           </xsl:when>
         </xsl:choose>-->
		 Owner was provided a copy of the estimate.
      </p>
      <br/>
      <p>
      <!--<b>RECONCILIATION</b>
      <table style="font:10pt Arial;">
        <colgroup>
          <col width="90pt" style="text-align:right;padding:0px;padding-right:15px"/>
          <col />
        </colgroup>
        <tr>
          <td>$3000.00</td>
          <td>Final - Net Estimate Amount</td>
        </tr>
        <tr>
          <td style="border: 0px; border-bottom:2px solid #000000">$2300.00</td>
          <td>Initial Approved - Net Estimate Amount Billed</td>
        </tr>
        <tr>
          <td>$700.00</td>
          <td>Final - Supplemental Net Invoice</td>
        </tr>
      </table>-->
      <table border="0" cellpadding="5px" cellspacing="0" style="font:10pt Arial;border-collapse:collapse">
<!--        <colgroup>
          <col width="120pt"/>
          <col width="120pt"/>
          <col width="120pt"/>
          <col width="120pt"/>
        </colgroup>
-->        <tr>
          <td colspan="2"/>
          <td colspan="2" style="text-align:center;border:1px solid #000000;background-color:#C0C0C0;color:#0000ff;">
            Pay this Amount:
          </td>
        </tr>
        <tr style="background-color:#C0C0C0;">
          <td style="text-align:center;border:1px solid #000000;color:#0000ff;">Initial Bill Estimate Net Amount</td>
          <td style="text-align:center;border:1px solid #000000;color:#0000ff;">Final Estimate Net Amount</td>
          <td style="text-align:center;border:1px solid #000000;color:#0000ff;">Initial Bill Estimate Net Amount was paid</td>
          <td style="text-align:center;border:1px solid #000000;color:#0000ff;">Initial Bill Estimate Net Amount was <b><u>NOT</u></b> paid</td>
        </tr>
        <tr>
          <td style="border:1px solid #000000"><xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ApprovedNetEstimateAmount)"/></td>
          <td style="border:1px solid #000000"><xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount)"/></td>
          <td style="border:1px solid #000000">
            <xsl:choose>
              <!-- Final is less than Approved. So refund the difference -->
              <xsl:when test="$NetDifference &lt; 0"><span style="color:#FF0000"><b>DO NOT PAY. </b></span>Overage will be refunded</xsl:when>
              <!-- Final is same approved -->
              <xsl:when test="$NetDifference = 0">$ 0.00</xsl:when>
              <!-- Final is greater than Approved -->
              <xsl:when test="$NetDifference &gt; 0">
                <xsl:value-of select="concat('$ ', $NetDifference)"/>
              </xsl:when>
            </xsl:choose>
          </td>
          <td style="border:1px solid #000000"><xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@FinalNetEstimateAmount)"/></td>
        </tr>
      </table>
      </p><br/>
      <p style="margin:0px;padding:0px;">The following applicable <u>closing documents</u> are included for your review:</p>
      <div id="documentList">$DocumentList$</div>
      <br/>
      <p style="margin:0px;padding:0px;">Thank you for the assignment.</p>
      <br/>
      <p style="margin:0px;padding:0px;">
        <b>
          <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Claims Representative
        </b>
        <br/>
        <xsl:call-template name="formatPhone">
          <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
        </xsl:call-template>
        <br/>
        <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>