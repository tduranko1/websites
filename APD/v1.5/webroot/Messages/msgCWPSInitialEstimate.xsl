<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
        
        function formatCurrency(dValue){
          var dRet = "0.00";
          if (isNaN(dValue) == false && dValue > 0){
              dRet = Math.ceil(dValue * 100) + "";
              dRet = dRet.substr(0, (dRet.length - 2)) + "." + dRet.substr((dRet.length - 2));
          }
          return dRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
    <p style="margin:0px">The initial estimate and photos for this assignment were reviewed and its status is as follows:</p>
      <br/><p style="margin:0px">Status:
      <b>Preliminary - DO NOT PAY</b></p>
      <br/>
      <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed; border:0px">
        <colgroup>
          <col style="width:5mm"/>
          <col style="width:4cm"/>
          <col style="width:5mm"/>
          <col style="width:3cm;text-align:right"/>
          <col style="width:2mm"/>
          <col style="width:4cm"/>
        </colgroup>
        <tr>
          <td colspan="6" style="border:0px"><div style="font:10pt Arial"><b>Approximate Costs of Repairs</b></div></td>
        </tr>
        <tr>
          <td style="border:0px"/>
          <td style="border:0px"><div style="font:10pt Arial">Gross</div></td>
          <td style="border:0px"><div style="font:10pt Arial">$</div></td>
          <td style="border:0px"><div id="divGrossAmt" style="font:10pt Arial;text-align:right"><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@GrossEstimateAmount"/></div></td>
          <td style="border:0px"/>
          <td style="border:0px"/>
        </tr>
        <tr>
          <td style="border:0px"/>
          <td style="border:0px"><div style="font:10pt Arial">Deductible</div></td>
          <td style="border:0px"><div style="font:10pt Arial">$</div></td>
          <td style="border:0px"><div style="font:10pt Arial;text-align:right">(<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible"/>)</div></td>
          <td style="border:0px"/>
          <td style="border:0px"/>
        </tr>
        <tr>
          <td style="border:0px"/>
          <td style="border:0px;border-bottom:2px solid #000000;"><div style="font:10pt Arial">Other *</div></td>
          <td style="border:0px;border-bottom:2px solid #000000;"><div style="font:10pt Arial">$</div></td>
          <td style="border:0px;border-bottom:2px solid #000000;"><div style="font:10pt Arial;text-align:right">(<xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OtherAdjustmentEstimateAmount"/>)</div></td>
          <td style="border:0px"/>
          <td style="border:0px"><div style="font:10pt Arial">* (Betterment, etc.)</div></td>
        </tr>
        <tr>
          <td style="border:0px"/>
          <td style="border:0px"><div style="font:10pt Arial">Net</div></td>
          <td style="border:0px"><div style="font:10pt Arial">$</div></td>
          <td style="border:0px">
            <div style="font:10pt Arial;text-align:right">
            <xsl:variable name="GrossAmt">
              <xsl:choose>
                <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@GrossEstimateAmount != ''"><xsl:value-of select="number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@GrossEstimateAmount)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="number('0.00')"/></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="DeductibleAmt">
              <xsl:choose>
                <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible != ''"><xsl:value-of select="number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@Deductible)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="number('0.00')"/></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:variable name="OtherAmt">
              <xsl:choose>
                <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OtherAdjustmentEstimateAmount != ''"><xsl:value-of select="number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@OtherAdjustmentEstimateAmount)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="number('0.00')"/></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="user:formatCurrency($GrossAmt - $DeductibleAmt - $OtherAmt)"/>
            </div>
          </td>
          <td style="border:0px"/>
          <td style="border:0px"/>
        </tr>
      </table><br/>
      <p style="margin:0px">Shop was contacted and spoke with:
      <xsl:call-template name="space"/></p>
      <br/>
      <p style="margin:0px"><b>Alternate Parts:</b>
      <xsl:call-template name="space"/>
      </p>
      <p style="margin:0px"><b>Shop Estimate Changes Requested:</b>
      <xsl:call-template name="space"/>
      </p>
      <p style="margin:0px"><b>Days to Repair:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@DaysToRepair"/>
      </p>
      <p style="margin:0px"><b>Start Date:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@RepairStartDt"/>
      </p>
      <p style="margin:0px"><b>Estimated Finish Date:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@RepairEndDt"/>
      </p>
      <p style="margin:0px"><b>Additional Comments:</b>
      <xsl:call-template name="space"/>
      </p>
      <br/><p style="margin:0px">The following <u>initial documents</u> are included for your review:</p>
      <div id="documentList">$DocumentList$</div>
      <br/>
      <p style="margin:0px">You will receive critical status updates to this claim, should they occur. Please contact me with any questions, issues or concerns.
      </p>
      <br/>
      <p style="margin:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Quality Control Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
    <script language="javascript">
      <![CDATA[
        function calcNetAmount(){
          alert("Hello");
        }
      ]]>
    </script>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>