<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }

        function netBalance(settlementAmount, payoffAmount, advanceAmount) {
          settlementAmount = isNaN(settlementAmount) == false ? settlementAmount : 0;
          payoffAmount = isNaN(payoffAmount) == false ? payoffAmount : 0;
          advanceAmount = isNaN(advanceAmount) == false ? advanceAmount : 0;

          if (settlementAmount > 0)
             return formatCurrency(settlementAmount - payoffAmount - advanceAmount);
          else
             return formatCurrency(payoffAmount - advanceAmount);
        }

        function formatCurrency(sVal){
           strRet = "";
           sVal = String(sVal);
           if (isNaN(sVal) == false && sVal != ""){
              sVal = String(Math.round(parseFloat(sVal) * 1000) / 1000);
              if (sVal.indexOf(".") == -1) {
                 strRet = sVal + ".00";
              } else {
                 var strParts = sVal.split(".");
                 strRet = strParts[0];
                 if (strParts.length > 1) {
                    var strCents = strParts[1].substr(0, 4);
                    try {
                    if (strCents.length < 4){
                       strCents += charFill("0", 4 - strCents.length);
                    }
                    strCents = Math.round(parseFloat(strCents)/100) + "";
                    strCents = strCents + charFill("0", 2 - strCents.length);
                    } catch (e) {}
                    strRet += "." + strCents;
                 }
              }
           }
           return strRet;
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <xsl:variable name="NetBalance">
      <xsl:value-of select="user:netBalance(number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SettlementAmount), number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PayoffAmount), number(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@AdvanceAmount))"/>
    </xsl:variable>

    <div style="font:10pt Arial">
    <p style="margin:0px;padding:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
      <p style="margin:0px;padding:0px"><b>LYNX ID:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/@LynxID"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Claim Number:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@ClientClaimNumber"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Date of Loss:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@LossDate"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Insured:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/@InsuredName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="concat(/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleYear, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleMake, ' ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@VehicleModel)"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Vehicle Owner:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimantName"/>
      </p>
      <p style="margin:0px;padding:0px"><b>Lien Holder:</b>
      <xsl:call-template name="space"/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@LienHolderName"/>
      </p>
      <br/>
      <p style="margin:0px;padding:0px">Please be advised the above reference total loss assignment has been <u>completed</u>. The following applicable settlement documents have been forwarded to <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SalvageName"/> for vehicle disposition.</p><br/>
      <div id="documentList">$DocumentList$</div>  <br/>
      <p style="margin:0px;padding:0px">Lien Holder and Vehicle Owner settlement reconciliation is as follows:</p>
      <table border="0" cellpadding="2px" cellspacing="0" style="font:10pt Arial;border-collapse:collapse">
        <tr>
          <td><b>Settlement Amount:</b></td>
          <td style="text-align:right;width:0.8in">
            <xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@SettlementAmount)"/>
          </td>
          <td/>
        </tr>
        <tr>
          <td><b>Lien Holder Payoff Amount:</b></td>
          <td style="text-align:right">
            <xsl:choose>
              <xsl:when test="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PayoffAmount != ''">
                <xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@PayoffAmount)"/>
              </xsl:when>
              <xsl:otherwise>$ 0.00</xsl:otherwise>
            </xsl:choose>
          </td>
          <td/>
        </tr>
        <tr style="">
          <td style="border:0px; border-bottom:1px solid #000000;"><b>Advance Amount:</b></td>
          <td style="border:0px; border-bottom:1px solid #000000;text-align:right">
            <xsl:value-of select="concat('$ ', /Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@AdvanceAmount)"/>
          </td>
          <td/>
        </tr>
        <tr>
          <td><b>Balance Due:</b></td>
          <td style="text-align:right"><b><xsl:value-of select="concat('$ ', $NetBalance)"/></b></td>
          <td><b>(Subject to other charges owed by Vehicle Owner)</b></td>
        </tr>
      </table>
      <br/><p style="margin:0px;padding:0px">Should you have any questions, please contact me. Thank you for the assignment.</p>
      <br/>
      <p style="margin:0px;padding:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerName"/>, Operations Support Specialist</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimOwnerEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>