<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
    <div style="font:10pt Arial">
    <p style="margin:0px"><xsl:value-of select="user:toProperCase(concat(/Root/Claim/@AdjusterFName, ' ', /Root/Claim/@AdjusterLName, ', '))"/></p><br/>
    <p style="margin:0px">Please be advised the estimate value has exceeded the established total loss <xsl:value-of select="/Root/Claim/@TLPercentage"/>% threshold value, and is a <u>potential</u> <b> Total Loss</b>. We have proceeded with the vehicle evaluation as follows:</p>
      <br/>
      <p style="margin:0px"><b>[<xsl:call-template name="space"/>] Requested shop complete BCIF and forward to LYNX</b></p>
      <p style="margin:0px"><b>[<xsl:call-template name="space"/>] LYNX will enter BCIF into Valuescope</b> or <b>[<xsl:call-template name="space"/>] Forward to adjuster</b></p>
<b>or</b>

      <p style="margin:0px"><b>[<xsl:call-template name="space"/>] Advise shop the adjuster will arrange for appraisal of vehicle</b></p>

      <br/><p style="margin:0px">Shop will provide 21 days free vehicle storage, starting from the date the total loss was determined. Unless you advise us otherwise, we will proceed to close and bill this assignment as a <b>Total Loss</b>.</p>
      <br/><p style="margin:0px">Should you have any questions, please contact me.</p>
      <br/>
      <p style="margin:0px"><b><xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystName"/>, Quality Control Representative</b>
      <br/>
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="val" select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystPhone"/>
      </xsl:call-template>
      <br/>
      <xsl:value-of select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]/@ClaimAnalystEmail"/>
      </p>
    </div>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>