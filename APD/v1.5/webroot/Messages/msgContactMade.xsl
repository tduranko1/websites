<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                              xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes"/>
    <msxsl:script language="JScript" implements-prefix="user">
      <![CDATA[
        function toProperCase(s)
        {
          return s.toLowerCase().replace(/^(.)|\s(.)/g,
                  function($1) { return $1.toUpperCase(); });
        }
      ]]>
    </msxsl:script>
  <xsl:template match="/">
  <xsl:for-each select="/Root/Claim/Vehicle[@ClaimAspectID = /Root/@ClaimAspectID]">

  <xsl:text>Called </xsl:text>
  <xsl:choose>
     <xsl:when test="@PartyCD='1'"><xsl:value-of select="/Root/Claim/@InsuredName"/></xsl:when>
     <xsl:otherwise><xsl:value-of select="@ClaimantName"/></xsl:otherwise>
  </xsl:choose>
  <xsl:text> and verified assignment process</xsl:text>
  <xsl:if test="@PartyCD='1'">, and $<xsl:value-of select="@Deductible"/> deductible</xsl:if>
  <xsl:if test="/Root/Claim/@RentalDailyAmt != '' or /Root/Claim/@RentalMax != ''"> and rental process</xsl:if>
  <xsl:text>. </xsl:text>
  <xsl:choose>
     <xsl:when test="@PartyCD='1'"><xsl:value-of select="/Root/Claim/@InsuredName"/></xsl:when>
     <xsl:otherwise><xsl:value-of select="@ClaimantName"/></xsl:otherwise>
  </xsl:choose>
  <xsl:text> will</xsl:text>
  <xsl:choose>
  <xsl:when test="@VehicleDrivable='1'"> take vehicle to the shop [when]. <xsl:if test="/Root/Claim/@RentalDailyAmt != '' or /Root/Claim/@RentalMax != ''">Advised that rental reservation to be made for repair days, after vehicle inspection completed, parts ordered <xsl:text disable-output-escaping="yes">&amp;</xsl:text> received and repair dates confirmed.</xsl:if></xsl:when>
  <xsl:otherwise> contact shop and wrecker yard to have vehicle released to body shop for inspection.</xsl:otherwise>
  </xsl:choose>
  
Called shop and verified assignment with [shop contact name] and confirmed estimate and photos to follow.<xsl:if test="/Root/Claim/@InsuranceCompanyID='261'"> Shop will contact customer to schedule inspection and set up repair date.</xsl:if>
  </xsl:for-each>
  </xsl:template>
  <!-- adds a no-break space -->
  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </xsl:template>
  <!-- formats a numeric string to phone -->
  <xsl:template name="formatPhone">
    <xsl:param name="val"/>
    <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), '-', substring($val, 7))"/>
  </xsl:template>
</xsl:stylesheet>