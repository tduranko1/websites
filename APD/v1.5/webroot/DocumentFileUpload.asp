<%@ LANGUAGE=VBScript%>
<% Option Explicit
  Response.Expires = 0
%>
<!-- #include file="errorhandler.asp" -->
<%
'  On Error Resume Next

  ' define variables and COM objects
  dim oADOStream, oXML, oXML_ECAD, oRoot, oFileDataNode
  dim oFile, oFiles
  dim i
  dim sURL


  Dim oApd,sDocId, sLynxID, sPertainsTo, sClaimAspectServiceChannelID, sDocumentType, sSupplementNumber, sSource, sUserID
  Dim strNotifyEvent, strDirectionalCD, strSendToCarrierStatusCD
  dim strMessage, strWarrantyFlag

  ' Instantiate Upload Class
  'Set oApd = Server.CreateObject("LAPDEcadAccessorMgr.CData")

  CheckError()

  ' create XMLDOM object and load it from request ASP object
  set oXML = Server.CreateObject("MSXML2.DOMDocument")
  oXML.async = false
  oXML.load(request)

  if oXML.parseError.errorCode <> 0 then
    response.write "<Error><![CDATA[Error parsing the XML request at Line #: " & CStr(oXML.parseError.line) & " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error>"
    response.end
  end if
  
  'Get the Basic details
  set oRoot = oXML.selectSingleNode("//root")

  if not oRoot is nothing then
     sLynxID = oRoot.getAttribute("LynxID")
     sPertainsTo = oRoot.getAttribute("PertainsTo")
     sClaimAspectServiceChannelID = oRoot.getAttribute("ClaimAspectServiceChannelID")
     sDocumentType = oRoot.getAttribute("FileType")
     sSupplementNumber = oRoot.getAttribute("SupplementSeqNo")
     sUserID = oRoot.getAttribute("UserID")
     sSource = oRoot.getAttribute("DocumentSource")
     strWarrantyFlag = oRoot.getAttribute("WarrantyFlag")
     
     
     if (sSource = "") then sSource = "User"
   
     ' retrieve XML node with binary content
     set oFiles = oXML.selectNodes("/root/File")
   
   
   
     sDocId = ""
     for i = 0 to oFiles.length - 1
       ' open stream object and store XML node content into it
       ' create Stream Object
       set oADOStream = Server.CreateObject("ADODB.Stream")
       oADOStream.Type = 1  ' 1=adTypeBinary
       oADOStream.open
       oADOStream.Write oFiles(i).nodeTypedValue
       oADOStream.position = 0
       
       if oFiles(i).getAttribute("NotifyEvent") = "0" then
          strNotifyEvent = "0"
       else
          strNotifyEvent = "1"
       end if
       
       if oFiles(i).getAttribute("DirectionalCD") = "O" then
          strDirectionalCD = "O"
       else
          strDirectionalCD = "I"
       end if
       if isNull(oFiles(i).getAttribute("SendToCarrierStatusCD")) = false then
          strSendToCarrierStatusCD = oFiles(i).getAttribute("SendToCarrierStatusCD")
          
          if oFiles(i).getAttribute("SendToCarrierStatusCD") = "" then strSendToCarrierStatusCD = "NS"
       end if
   
      ' save uploaded file
       set oXML_ECAD = server.createObject("MSXML2.DOMDocument")
       oXML_ECAD.loadXML  "<DocumentUpload>" & _
                               "<UserID>" & sUserID & "</UserID>" & _
                               "<LynxID>" & sLynxID & "</LynxID>" & _
                               "<PertainsTo>" & sPertainsTo & "</PertainsTo>" & _
                               "<ClaimAspectServiceChannelID>" & sClaimAspectServiceChannelID & "</ClaimAspectServiceChannelID>" & _
                               "<SuppSeqNumber>" & sSupplementNumber & "</SuppSeqNumber>" & _
                               "<DocumentType>" & sDocumentType & "</DocumentType>" & _
                               "<DocumentSource>" & sSource & "</DocumentSource>" & _
                               "<NotifyEvent>" & strNotifyEvent & "</NotifyEvent>" & _
                               "<DirectionalCD>" & strDirectionalCD & "</DirectionalCD>" & _
                               "<SendToCarrierStatusCD>" & strSendToCarrierStatusCD & "</SendToCarrierStatusCD>" & _
                               "<WarrantyFlag>" & strWarrantyFlag & "</WarrantyFlag>" & _
                               "<FileName>" & oFiles(i).getAttribute("FileName") & "</FileName>" & _
                               "<FileExtension>" & oFiles(i).getAttribute("FileExt") & "</FileExtension>" & _
                               "<FileLength>" & oFiles(i).getAttribute("FileLength") & "</FileLength>" & _
                               "<FileData/>" & _
                           "</DocumentUpload>"
   
       'set the FileData
       set oFileDataNode = oXML_ECAD.selectSingleNode("//FileData")
       oFileDataNode.dataType = "bin.base64"
       oFileDataNode.nodeTypedValue = oADOStream.Read(-1) ' Read All data
   
   
       oADOStream.close
       set oADOStream = nothing
   '    oXML_ECAD.Save "c:\temp\upload\" & oFiles(i).getAttribute("FileName") & ".xml"
       ' upload the data to the LAPDEcadAccessorMgr

           

   Dim mexMonikerString , loStrXML , strResult         
	mexMonikerString = GetWebServiceURL()
	set loStrXML = GetObject(mexMonikerString)
    strResult = loStrXML.PutXML(oXML_ECAD.xml)


       if sDocId = "" then
         sDocId = cstr(strResult)         'oApd.PutXML(oXML_ECAD.xml)
       else
         sDocId = sDocId & ";" & cstr(strResult)   'oApd.PutXML(oXML_ECAD.xml)
       end if

       set oXML_ECAD = nothing
     next

   else
      strMessage = "<Error>No Root Node found. "
      if oXML.parseError.errorCode <> 0 then
         strMessage = strMessage & oXML.parseError.reason
      end if
      strMessage = strMessage & "</Error>"
      
      response.write strMessage
   end if
  ' destroy COM object
  set oADOStream = Nothing
  set oXML = Nothing
  set oAPD = Nothing
  ' write message to browser
  Response.Write sDocId
function callAPI(param)
Dim mexMonikerString , loStrXML , strResult         
	mexMonikerString = GetWebServiceURL()
	set loStrXML = GetObject(mexMonikerString)
    strResult = loStrXML.PutXML(param)
end function
Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")
                
                HtmlDecode = returnValue
End Function

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function

%>
