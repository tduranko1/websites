<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
  Dim strHTML, objExe, strUserID, sEntityID, sPageID
  Dim strZipCode, strCity, strState, strLynxID, strVehicleNumber
  Dim strInsuranceCompanyID, strClaimAspectID, strLocationName, strShopRemarks
  Dim strAssignmentTo, strMEChildWin, strGetUser, strClaimAspectServiceChannelID
  Dim strWarranty
  
	On Error Resume Next

  ReadSessionKey()

  strZipCode = Request("ZipCode")
  strCity = Request("City")
  strState = Request("State")
  strUserID = Request("UserID")
  strLynxID = Request("LynxID")
  strVehicleNumber = Request("VehicleNumber")
  strInsuranceCompanyID = Request("InsuranceCompanyID")
  strClaimAspectID = Request("ClaimAspectID")
  strClaimAspectServiceChannelID = Request("ClaimAspectServiceChannelID")
  strLocationName = Request("LocationName")
  strShopRemarks = Request("ShopRemarks")
  strAssignmentTo = Request("AssignmentTo")
  strMEChildWin = Request("MEChildWin")
  strGetUser = Request("getUser")
  strWarranty = Request("Warranty")
  
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()
  
	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

  objExe.AddXslParam "ZipCode", strZipCode
  objExe.AddXslParam "City", strCity
  objExe.AddXslParam "State", strState
  objExe.AddXslParam "UserID", strUserID
  objExe.AddXslParam "LynxID", strLynxID
  objExe.AddXslParam "VehicleNumber", strVehicleNumber
  objExe.AddXslParam "InsuranceCompanyID", strInsuranceCompanyID
  objExe.AddXslParam "ClaimAspectID", strClaimAspectID
  objExe.AddXslParam "ClaimAspectServiceChannelID", strClaimAspectServiceChannelID
  objExe.AddXslParam "LocationName", strLocationName
  objExe.AddXslParam "ShopRemarks", strShopRemarks
  objExe.AddXslParam "AssignmentTo", strAssignmentTo
  objExe.AddXslParam "MEChildWin", strMEChildWin
  objExe.AddXslParam "getUser", strGetUser
  objExe.AddXslParam "Warranty", strWarranty
  
	'Get ApdSelect XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspSMTShopSearchReferenceGetListXML", "ShopLookup.xsl")
	CheckError()

	Set objExe = Nothing

  Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

