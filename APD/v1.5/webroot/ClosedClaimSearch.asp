<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
	Dim strUserID
	Dim strHTML
    Dim strDays

	On Error Resume Next

	strUserID = GetSession("UserID")
    'strUserID = "28"
	CheckErr()

    strDays = request.queryString("Days")

	'Create and initialize DataPresenter.CExecute object.
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckErr()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	CheckErr()

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspCRDClosedClaimGetDetailXML", "ClosedClaimSearch.xsl", strUserID, strDays )
	CheckErr()

	Set objExe = Nothing

	Response.Write strHTML

	On Error GoTo 0

    Call KillSessionObject

	Sub CheckErr()
		If Err.Number <> 0 Then
			ErrorHandler()
			Response.End
		End If
	End Sub

%>


<script type="text/javascript" language=JavaScript1.2  src="webhelp/RoboHelp_CSH.js"></script> 		

<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,7);
		  event.returnValue=false;
		  };	
</script>