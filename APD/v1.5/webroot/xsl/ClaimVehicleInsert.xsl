<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt"
		xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
		xmlns:js="urn:the-xml-files:xslt"
		id="ClaimVehicle">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxID"/>
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/>

<!--<xsl:variable name="LynxID" select="/Root/@LynxID" />-->
<xsl:variable name="VehicleNumber" select="/Root/@VehicleNumber" />
<xsl:variable name="VehicleLastUpdatedDate" select="/Root/Vehicle/@SysLastUpdatedDate"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimVehicleGetDetailXML,ClaimVehicleInsert.xsl,6366,0,145   -->

<HEAD>
<TITLE>Vehicle Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
		A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
		  event.returnValue=false;
		  };	
 </script>

<SCRIPT language="JavaScript">

	var gsLynxID = '<xsl:value-of select="$LynxID"/>';
	var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsVehicleLastUpdatedDate = "<xsl:value-of select="$VehicleLastUpdatedDate"/>";

	var gsSaftyDeviceCount = '<xsl:value-of select="count(/Root/Reference[@List='SafetyDevice'])"/>';
	var gsCRUD = '<xsl:value-of select="$VehicleCRUD"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  
   var gsCoverageCDList = new Array(<xsl:for-each select="/Root/Vehicle/CoverageType">
						  "<xsl:value-of select="@CoverageProfileCD"/>" <xsl:if test="position() != last()">, </xsl:if>
                      </xsl:for-each> );
   var gsCoverageIDList = new Array(<xsl:for-each select="/Root/Vehicle/CoverageType">
						  "<xsl:value-of select="@ClientCoverageTypeID"/>" <xsl:if test="position() != last()">, </xsl:if>
                      </xsl:for-each> );

<![CDATA[

	// Tooltip Variables to set:
	messages= new Array()
	// Write your descriptions in here.
	messages[0]="Double Click to Expand"
	messages[1]="Double Click to Close"

	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png')
	preload('buttonAddDown','/images/but_ADD_down.png')
	preload('buttonAddOver','/images/but_ADD_over.png')
	preload('buttonDel','/images/but_DEL_norm.png')
	preload('buttonDelDown','/images/but_DEL_down.png')
	preload('buttonDelOver','/images/but_DEL_over.png')
	preload('buttonSave','/images/but_SAVE_norm.png')
	preload('buttonSaveDown','/images/but_SAVE_down.png')
	preload('buttonSaveOver','/images/but_SAVE_over.png')

	// Page Initialize
	function PageInit()
	{
    if (gsCRUD.indexOf("C") == -1)
		{
      ClientWarning("You do not have rights to create a new Vehicle.  See your supervisor.");
      document.parentWindow.frameElement.src="blank.asp";
		} else if (gsClaimStatus == "Claim Cancelled" || gsClaimStatus == "Claim Voided") {
      if (gsClaimStatus == "Claim Cancelled")
        var str = "cancelled";
      else
        var str = "voided";

      ClientWarning("You cannot add a new vehicle to this claim because the claim has been " + str + ".");
      document.parentWindow.frameElement.src="blank.asp";
    }

    //if(top.gsInvInsuredCount)
      //if (top.gsInvInsuredCount > 0){
    if (top.giVeh1stPartyCount)
      if (top.giVeh1stPartyCount > 0) {
        for (var i = 0; i < selExposureCD.options.length;  i++){
          if (selExposureCD.options[i].innerText == "1st Party"){
            selExposureCD.remove(i);
            break;
          }
        }
      }

    if (txtDescYear)
      txtDescYear.onbeforedeactivate = CheckVehicleYear;

    document.all.selCoverageProfileCD.attachEvent("onblur",OnSelectCoverageProfile);
	top.HideMainTabs();
	}

  function CheckVehicleYear(){
    if (txtDescYear.value.length > 0 && txtDescYear.value.length != 4) {
      ClientWarning("Vehicle year must be a 4 digit year. Please try again.");
      txtDescYear.focus();
      event.returnValue = false;
      event.cancelBubble = true;
    }
  }

  
  	function OnSelectCoverageProfile()
	{
	try
	{
	 var selCoverageProfileCD = document.getElementById("selCoverageProfileCD");
	 var selectedCoverageTypeCD = selCoverageProfileCD.options[selCoverageProfileCD.selectedIndex].value;
	 var clientCoverageID = document.getElementById("ClientCoverageTypeID");
	  for(var i=0;i<gsCoverageCDList.length;i++)
	  {
	    if (gsCoverageCDList[i] == selectedCoverageTypeCD )
        clientCoverageID.value = gsCoverageIDList[i]; 
	  }
	 }
	 catch(e) {}
	}
	
  
  
	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
  var objDivWithButtons = null;
	// ADS Button pressed
	function ADS_Pressed(sAction, sName)
	{
    if (ExposureCD.value == ""){
      ClientWarning("Party is a required field. Please pick one from the list.");
      return;
    }
    if (!gbDirtyFlag) return; //no change made to the page
		if (inADS_Pressed == true) return;
		inADS_Pressed = true;
    objDivWithButtons = divWithButtons;
    disableADS(objDivWithButtons, true);

		var oDiv = document.getElementById(sName)

		if (sAction == "Update")
		{
      if (txtDescYear.value.length > 0 && txtDescYear.value.length != 4) {
        ClientWarning("Vehicle year must be a 4 digit year. Please try again.");
        return;
      }

   		GetCBvalues('AirBag');

			var sRequest = GetRequestString(sName);

			sRequest += "VehicleSysLastUpdatedDate=" + gsVehicleLastUpdatedDate + "&";
 			//sRequest += "VehicleNumber=" + gsVehNum + "&";
			sRequest += "LynxID=" + gsLynxID + "&";
			sRequest += "UserID=" + gsUserID + "&";
			sRequest += "ImpactPoints="+ strImpactPoints+"&";
			sRequest += "PriorImpactPoints="+strPrevImpactPoints +"&";
			sRequest += "NotifyEvent=1&";

			var retArray = DoCrudAction(oDiv, "Insert", sRequest);

      if (retArray[1] != "0")
			{
				//alert(retArray[0]);
				// take them back to the vehicle page and show the new vehicle

				var objXML = new ActiveXObject("Microsoft.XMLDOM");
				objXML.loadXML(retArray[0]);
				/*var objNodeList = objXML.documentElement.selectNodes("/Root/Vehicle");
				var objNode;
        var nLength = objNodeList.length;
				// get last one added
		    for (var n = 0; n < (nLength - 1); n++)
		    {
					objNode = objNodeList.nextNode();
				}
				//last one
				objNode = objNodeList.nextNode();
				var sVehNum = objNode.selectSingleNode( "@VehicleNumber" ).text;
				var sClaimAspect = objNode.selectSingleNode( "@ClaimAspectID" ).text;*/

        var sClaimAspectID = objXML.documentElement.selectSingleNode("/Root/NewVehicle/@ClaimAspectID").value;
        var sVehNum = objXML.documentElement.selectSingleNode("/Root/NewVehicle/@ClaimAspectNumber").value;
        if (parent.sClaimView == "exp")
				  document.parentWindow.frameElement.src="ClaimVehicleList.asp?LynxID=" + gsLynxID + "&VehContext=" + sVehNum + "&ClaimAspectID=" + sClaimAspectID;
        else
				  top.refreshVehicles(sVehNum);
			}
		}

		inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;
	}

	// builds the multi-checkboxes list for Safety Devices Deployed
  function GetCBvalues(tag)
  {
    var sName = tag;
    var sId = "";
    var node = "";
		var otb = document.getElementById("tblAirBags");

    for (var i = 1; i <= gsSaftyDeviceCount; i++)
    {
      node = i;
      sInputName = sName + node;
	    getValues = document.getElementById(sInputName).value;

      if (getValues == '1')
      {
        if (sId == "") sId += otb.rows[i-1].cells[1].innerText;
        else sId += "," + otb.rows[i-1].cells[1].innerText;
      }
    }
    txtSafetyDevicesDeployed.value = sId;
  }

	function ShowImpact()
	{
		var strReturn = document.getElementById("txtImpactArea").innerText + "|" + document.getElementById("txtPriorImpactArea").innerText;

		strReturn = window.showModalDialog("/DamagePick.htm",strReturn,"dialogHeight: 410px; dialogWidth: 520px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");

		if (strReturn != "")
		{
			var sArray = strReturn.split("|");
			SaveImageImpact(sArray[0], sArray[1]);
		}
	}

	var strImpactPoints = "";
	var strPrevImpactPoints = "";

	function SaveImageImpact(sAreaImpact,sPrevAreaImpact)
	{
		document.getElementById("txtImpactArea").innerText = sAreaImpact;
		document.getElementById("txtPriorImpactArea").innerText = sPrevAreaImpact;
		document.getElementById("txtImpactArea").parentElement.style.border = '1px solid #805050';
		document.getElementById("txtPriorImpactArea").parentElement.style.border = '1px solid #805050';

		var sArray = sAreaImpact.split(",");

		xmlReference.setProperty("SelectionLanguage", "XPath");
		strImpactPoints = ImpactNodes(sArray);
		sArray = sPrevAreaImpact.split(",");
		strPrevImpactPoints = ImpactNodes(sArray);

	}


	function Trim(str)
	{
		while (str.charAt(0) == ' ') str = str.substr(1);
		while (str.charAt(str.length-1) == ' ') str = str.substr(0,str.length-2);
		return str;

	}

	function ImpactNodes(sArray)
	{
		var strPrimaryID = "";
		var strXPath = "";
		var objNode;
		var strIDs = "";
		var strNode="";

		try
		{
			var iLength = sArray.length;
      for (var i=0; i < iLength; i++)
			{
				if (sArray[i] && Trim(sArray[i]) != "")
				{
					if (sArray[i].indexOf("(Primary)") != -1)
					{
            strNode = Trim(sArray[i]);
						var strTmp = strNode.substring(0, strNode.indexOf("(Primary)")-1);
            strXPath = '//Reference[@List="Impact"][@Name="' + strTmp + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strPrimaryID = objNode.value;
					}
					else
					{
            strXPath = '//Reference[@List="Impact"][@Name="' + Trim(sArray[i]) + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strIDs += String(objNode.value) + ",";
					}
				}
			}

			if (strPrimaryID != "")
				strIDs = strPrimaryID + "," + strIDs;
			return strIDs;
		}
		catch(e)
		{
      ClientError( e.message + ": XPath = " + strXPath );
		}
	}

  var objCoverageCodes = new Array();
  function onExposureChange(){
    onSelectChange(selExposureCD);
    var sExposureCD = selExposureCD.options[selExposureCD.selectedIndex].value;
    if (objCoverageCodes.length == 0){
      for(var i = 0; i < selCoverageProfileCD.options.length; i++) {
        var objCoverage = new Object();
        objCoverage.value = selCoverageProfileCD.options[i].value;
        objCoverage.text = selCoverageProfileCD.options[i].innerText;
        objCoverageCodes.push(objCoverage);
      }
    }
      for(var i = selCoverageProfileCD.options.length - 1; i > 0; i--)
        selCoverageProfileCD.remove(i);

    var strOptions = "<div class='option' value='' nowrap></div>";
    switch(sExposureCD) {
      case "1":
        for (var i = 0; i < objCoverageCodes.length; i++){
          if (objCoverageCodes[i].text == "Comprehensive" || objCoverageCodes[i].text == "Collision" || objCoverageCodes[i].text == "Uninsured")
            strOptions += "<div class='option' value='"  + objCoverageCodes[i].value + "' nowrap>" + objCoverageCodes[i].text + "</div>";
        }
        break;
      case "3":
        for (var i = 0; i < objCoverageCodes.length; i++){
          if (objCoverageCodes[i].text == "Liability")
            strOptions += "<div class='option' value='"  + objCoverageCodes[i].value + "' nowrap>" + objCoverageCodes[i].text + "</div>";
        }
        break;
      case "N":
        break;
    }

    selCoverageProfileCD.all.selectOptionDiv.innerHTML = strOptions;

    //select the blank item
    setTimeout("selCoverageProfileCD.optionselect( null, 0 )", 10);
  }

  function checkDirty(){
    if (gbDirtyFlag) {
  		var sSave = YesNoMessage("Need To Save", "Do you want to save the changes for the new vehicle?");
  		if (sSave == "Yes") {
  			ADS_Pressed("Update", "ClaimVehicleInsert");
        if (gbDirtyFlag == true){ // this would have reset to false if the save successful.
          window.event.returnValue = false;
          window.event.cancelBubble = true;
        }
      }
  		else
  		{
  			gbDirtyFlag = false;
  			if (typeof(parent.gbDirtyFlag)=='boolean')
  				parent.gbDirtyFlag = false;
        }
    }
  }


	if (document.attachEvent)
		document.attachEvent("onclick", top.hideAllMenuScriptlets);
	else
		document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSub" onLoad="initSelectBoxes(); InitFields(); PageInit();" style="overflow:hidden;" tabIndex="-1" onbeforeunload="checkDirty()">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
<!-- Impact Type XML Data -->
<xml id="xmlReference"><Root><xsl:copy-of select="/Root/Reference[@List='Impact']"/></Root></xml>

<xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'">
  <xsl:value-of select="js:SetCRUD( '_R__' )"/>
  <xsl:value-of select="js:setPageDisabled()"/>
  <!-- <xsl:value-of select="js:SetCRUD( '_R_' )"/> -->
</xsl:if>

<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">

  <DIV id="DescriptionInfoTab" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 5px; left: 6px;">
    Add New Vehicle - Description</DIV>
  <DIV id="ClaimVehicleInsert" class="SmallGrouping" name="ClaimVehicleInsert" Insert="uspClaimVehicleInsDetail" style="position:absolute; left:3px; top:21px; width:740px; height:490px; z-index:1;" onfocusin="DivOnFocusIn(this)" >
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$VehicleCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Vehicle/@SysLastUpdatedDate"/></xsl:attribute>

		<xsl:for-each select="Vehicle" >
			<xsl:call-template name="DescriptionInfo"/>
		</xsl:for-each>

		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="5px" /></TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center">
					<img src="\images\background_top_flip.gif" width="40%" height="1px" />
					<img src="\images\background_top.gif" width="40%" height="1px" />
				</TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="4px" /></TD>
      </TR>
			<TR>
        <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Location</TD>
			</TR>
		</TABLE>

		<xsl:for-each select="Vehicle" >
			<xsl:call-template name="VehicleLocationInfo"/>
		</xsl:for-each>
		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="5px" /></TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center">
					<img src="\images\background_top_flip.gif" width="40%" height="1px" />
					<img src="\images\background_top.gif" width="40%" height="1px" />
				</TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="4px" /></TD>
      </TR>
			<TR>
        <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Damage</TD>
			</TR>
		</TABLE>

		<xsl:for-each select="/Root" >
			<xsl:call-template name="DamageInfo"/>
		</xsl:for-each>
		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="5px" /></TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center">
					<img src="\images\background_top_flip.gif" width="40%" height="1px" />
					<img src="\images\background_top.gif" width="40%" height="1px" />
				</TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="4px" /></TD>
      </TR>
		</TABLE>

		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
			<TR>
        <TD unselectable="on" class="boldText" width="50%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Speed</TD>
        <TD unselectable="on" class="boldText" width="50%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Rental</TD>
			</TR>
	    <TR>
        <TD>
    		<xsl:for-each select="Vehicle" >
    			<xsl:call-template name="SpeedInfo"/>
    		</xsl:for-each>
        </TD>
        <TD>
    		<xsl:for-each select="Vehicle" >
    			<xsl:call-template name="RentalInfo"/>
    		</xsl:for-each>
        </TD>
      </TR>
    </TABLE>
		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="5px" /></TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center">
					<img src="\images\background_top_flip.gif" width="40%" height="1px" />
					<img src="\images\background_top.gif" width="40%" height="1px" />
				</TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="4px" /></TD>
      </TR>
			<TR>
        <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Claim Rep. Remarks</TD>
			</TR>
		</TABLE>

		<xsl:for-each select="Vehicle" >
			<xsl:call-template name="RemarksToClaimRepInfo"/>
		</xsl:for-each>


		  <!-- Start Property Contact Info -->
		<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="5px" /></TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center">
					<img src="\images\background_top_flip.gif" width="40%" height="1px" />
					<img src="\images\background_top.gif" width="40%" height="1px" />
				</TD>
      </TR>
	    <TR>
        <TD unselectable="on" align="center"><img src="\images\spacer.gif" width="1" height="4px" /></TD>
      </TR>
			<TR>
        <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Contact Info</TD>
			</TR>
		</TABLE>

		<xsl:for-each select="Vehicle/Contact" >
			<xsl:call-template name="VehicleContactInfo"/>
		</xsl:for-each>


    <SCRIPT>ADS_Buttons('ClaimVehicleInsert');</SCRIPT>
  </DIV>


</DIV>

</BODY>
</HTML>
</xsl:template>


	<!-- Gets the Description Info -->
  <xsl:template name="DescriptionInfo" >
	<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD unselectable="on">Year:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescYear',3,'VehicleYear',.,'','',1)"/>
			</TD>
			<TD unselectable="on">Driven w/ Perm.:</TD>
			<TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtPTD" select="@PermissionToDriveCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('PermissionToDriveCD',2,'onSelectChange',string($rtPTD),1,2,/Root/Reference[@List='PermissionToDrive'],'Name','ReferenceID','','','','',6,1)"/>
      </TD>
      <TD unselectable="on">Party:</TD>
      <TD unselectable="on">
        <xsl:variable name="sExposureCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('ExposureCD',2,'onExposureChange',string($sExposureCD),1,3,/Root/Reference[@List='Exposure'],'Name','ReferenceID','','','','',11,1)"/>
      </TD>
			<TD unselectable="on" align="center" class="autoFlowSpanTitle">
        Air Bags Deployed:
			</TD>
		</TR>
		<TR>
			<TD unselectable="on">Make:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescMake',15,'Make',.,'','',2)"/>
			</TD>
			<TD unselectable="on">VIN:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescVIN',17,'VIN',.,'','',7)"/>
			</TD>
      <TD unselectable="on">Coverage:</TD>
      <TD unselectable="on">
        <xsl:variable name="rtCov" select="@CoverageProfileCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('CoverageProfileCD',2,'onSelectChange',string($rtCov),1,2,/Root/Reference[@List='CoverageProfile'],'Name','ReferenceID','','','','',11,1)"/>
		  <INPUT type="hidden" id="ClientCoverageTypeID" name="ClientCoverageTypeID" value="" />
      </TD>
  		<!-- <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on">
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD> -->
			<TD unselectable="on" rowspan="4" valign="top"><xsl:attribute name="nowrap"/>
				<DIV unselectable="on" style="z-index:1; width:160px; height:76px; overflow: auto;">
					<TABLE id='tblAirBags' unselectable="on" border="0" cellspacing="0" cellpadding="1">
						<xsl:for-each select="/Root/Reference[@List='SafetyDevice']">
							<xsl:sort data-type="number" select="@DisplayOrder" order="ascending" />
							<TR>
								<TD unselectable="on"><xsl:value-of select="@Name"/></TD>
								<TD unselectable="on" style="display:none"><xsl:value-of select="@ReferenceID"/></TD>
								<TD unselectable="on">
									<xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AirBag',boolean(/Root/Vehicle/SafetyDevice[@SafetyDeviceID=current()/@ReferenceID]),'1','0',position(),'', 11)"/>
								</TD>
							</TR>
						</xsl:for-each>
					</TABLE>
          <INPUT type="hidden" id="txtSafetyDevicesDeployed" name="SafetyDevicesDeployed" value="" />
				</DIV>
			</TD>
		</TR>
		<TR>
			<TD unselectable="on">Model:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtdescModel',15,'Model',.,'','',3)"/>
			</TD>
			<TD unselectable="on">Odometer Reading:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOdometer',10,'Mileage',.,'','',8)"/>
			</TD>
			<TD unselectable="on" colspan="2" rowspan="4" style="vertical-align:top;padding:0px;padding-top:4px;">
        Assignment Type:<br/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('AssignmentTypeID',2,'onSelectChange',string(@AssignmentTypeID),1,0,/Root/Reference[@List='AssignmentType'],'Name','ReferenceID','','','','',12,1)"/>
      </TD>
		</TR>
		<TR>
			<TD unselectable="on">Body Style:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescBodyStyle',15,'BodyStyle',.,'','',4)"/>
			</TD>
			<TD unselectable="on"><xsl:attribute name="nowrap"/>
        License Plate:
			</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLicensePlate',10,'LicensePlateNumber',.,'','',9)"/>
			</TD>
			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
		</TR>
		<TR>
			<TD unselectable="on">Color:</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescColor',10,'Color',.,'','',5)"/>
			</TD>
			<TD unselectable="on"><xsl:attribute name="nowrap"/>
        License Plate State:
			</TD>
			<TD unselectable="on">
				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLicensePlateState',2,'LicensePlateState',.,'',12,10)"/>
			</TD>
			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
		</TR>
	</TABLE>
  </xsl:template>


	<!-- Gets the Vehicle Location Info -->
  <xsl:template name="VehicleLocationInfo" >
  	<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
  		<TR>
  			<TD unselectable="on">Location:</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocName',22,'LocationName',.,'','',12)"/>
          </TD>
  			<TD unselectable="on">Address:</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocAddress1',22,'LocationAddress1',.,'','',13)"/>
  			</TD>
  			<TD unselectable="on">City:</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocCity',15,'LocationCity',.,'','',15)"/>
  			</TD>
  			<TD unselectable="on">Phone:</TD>
  			<TD unselectable="on"><xsl:attribute name="nowrap"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneAC',1,'LocationAreaCode',.,'',10,18)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneEX',1,'LocationExchangeNumber',.,'',10,19)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneUN',2,'LocationUnitNumber',.,'',10,20)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneXT',3,'LocationExtensionNumber',.,'',10,21)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  			</TD>
  		</TR>
  		<TR>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocAddress2',22,'LocationAddress2',.,'','',14)"/>
  			</TD>
  			<TD unselectable="on">State:</TD>
  			<TD unselectable="on"><xsl:attribute name="nowrap"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocState',2,'LocationState',.,'',12,16)"/>
  				Zip:
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocZip',10,'LocationZip',.,'',10,17)"/>
  				<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtVehLocState', 'txtVehLocCity');"></xsl:text>
  			</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		</TR>
  	</TABLE>
	</xsl:template>

	<!-- Gets the Damage Info -->
  <xsl:template name="DamageInfo" >
			<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on" valign="top">
            Impact<br />Area:
          </TD>
          <TD unselectable="on" valign="top">
            <IMG src="/images/next_button.gif" onClick="ShowImpact()" width="19" height="19" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" align="absmiddle"/>
          </TD>
          <TD unselectable="on">
            <DIV class="autoflowDiv" style="width:252px; height:34px;overflow-x:hidden;overflow-y:auto">
              <SPAN id="txtImpactArea" class="InputReadonlyField" style="width:246px; height:34px">
                <xsl:for-each select="Reference[@List='Impact']">
                  <xsl:variable name="ImpactNode" select="/Root/Vehicle/Impact[@ImpactID=current()/@ReferenceID and @CurrentImpactFlag='1']"/>
                  <xsl:if test="boolean($ImpactNode)">
                    <xsl:value-of select="@Name"/>
                    <xsl:if test="$ImpactNode/@PrimaryImpactFlag='1'">
                      <xsl:text> (Primary)</xsl:text>
                    </xsl:if>
                    <xsl:text>,</xsl:text>
                  </xsl:if>
                </xsl:for-each>
              </SPAN>
            </DIV>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on" valign="top">
            Prior<br />Damage<br />Area:
          </TD>
          <TD unselectable="on">
            <DIV class="autoflowDiv" style="width:252px; height:34px;overflow-x:hidden;overflow-y:auto">
              <SPAN id="txtPriorImpactArea" class="InputReadonlyField" style="width:246px; height:34px">
                <xsl:for-each select="Reference[@List='Impact']">
                  <xsl:if test="boolean(/Root/Vehicle/Impact[@ImpactID=current()/@ReferenceID and @PriorImpactFlag='1'])">
                    <xsl:value-of select="@Name"/>
                      <xsl:text>,</xsl:text>
                  </xsl:if>
                </xsl:for-each>
              </SPAN>
            </DIV>
          </TD>
          <TD unselectable="on" valign="top"><xsl:attribute name="nowrap"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Drivable:
          </TD>
          <TD unselectable="on" valign="top">
            <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('DriveableFlag',boolean(Vehicle[@DriveableFlag='1']),'1','0','','',22)"/>
          </TD>
        </TR>
			</TABLE>
	</xsl:template>

	<!-- Gets the Speed Info -->
  <xsl:template name="SpeedInfo" >
			<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
				<TR>
					<TD unselectable="on" nowrap="">
            Estimated Speed of Vehicle:
					</TD>
					<TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEstimatedSpeed',3,'ImpactSpeed',.,'','',23)"/>
					</TD>
					<TD unselectable="on" nowrap="">
            Posted Speed Limit:
					</TD>
					<TD unselectable="on" >
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPostedSpeed',3,'PostedSpeed',.,'','',24)"/>
					</TD>
				</TR>
			</TABLE>
	</xsl:template>

	<!-- Gets the Rental Info -->
  <xsl:template name="RentalInfo" >
			<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
				<TR>
					<TD unselectable="on" nowrap="">
            Days Authorized:
					</TD>
					<TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRental',4,'RentalDaysAuthorized',.,'','',25)"/>
						<!-- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEstimatedSpeed',3,'ImpactSpeed',.,'','',23)"/> -->
					</TD>
					<TD unselectable="on" nowrap="">
            Instructions:
					</TD>
					<TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtRentalInstructions',29,2,'RentalInstructions',.,'','',26)"/>
						<!-- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPostedSpeed',3,'PostedSpeed',.,'','',24)"/> -->
					</TD>
				</TR>
			</TABLE>
	</xsl:template>
    
	<!-- Gets the Remarks to Claim Rep Info -->
  <xsl:template name="RemarksToClaimRepInfo" >
			<TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0" width="100%">
				<TR>
					<TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtRemarksToRep',0,2,'Remarks',.,'','',27)"/>
					</TD>
				</TR>
			</TABLE>
	</xsl:template>


  <!-- Gets the Contact Info -->
  <xsl:template name="VehicleContactInfo" >
      <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameTitle',2,'NameTitle',.,'',1,90)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameFirst',15,'NameFirst',.,'',1,91)"/>
            <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameLast',15,'NameLast',.,'',1,92)"/>
          </TD>
          <TD  unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtContactRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_2',2,'onSelectChange',number($rtContactRelToIns),1,3,/Root/Reference[@List='ContactRelationToInsured'],'Name','ReferenceID',0,90,'','',99)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on">Address:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress1',30,'Address1',.,'',1,93)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress2',30,'Address2',.,'',1,94)"/>
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Number to Call:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtBCP" select="@BestContactPhoneCD"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',100,1)"/>
          </TD>
          <TD unselectable="on">Day:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayAreaCode',1,'DayAreaCode',.,'',10,102)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExchangeNumber',1,'DayExchangeNumber',.,'',10,103)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayUnitNumber',2,'DayUnitNumber',.,'',10,104)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExtensionNumber',3,'DayExtensionNumber',.,'',10,105)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">City:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactCity',15,'AddressCity',.,'',1,95)"/>
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Time to Call:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactBestContactTime',20,'BestContactTime',.,'',1,101)"/>
          </TD>
          <TD unselectable="on">Night:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightAreaCode',1,'NightAreaCode',.,'',10,106)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExchangeNumber',1,'NightExchangeNumber',.,'',10,107)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightUnitNumber',2,'NightUnitNumber',.,'',10,108)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExtensionNumber',3,'NightExtensionNumber',.,'',10,109)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactState',2,'AddressState',.,'',12,96)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactZip',10,'AddressZip',.,'',10,97)"/>
            <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtContactState', 'txtContactCity');"></xsl:text>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Alt.:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateAreaCode',1,'AlternateAreaCode',.,'',10,110)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExchangeNumber',1,'AlternateExchangeNumber',.,'',10,111)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,112)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,113)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">E-mail:</TD>
          <TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmailAddress',30,'EmailAddress',.,'',10,98)"/><xsl:text disable-output-escaping="yes">onbeforedeactivate="checkEMail(this);"></xsl:text>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
      </TABLE>
  </xsl:template>

</xsl:stylesheet>
