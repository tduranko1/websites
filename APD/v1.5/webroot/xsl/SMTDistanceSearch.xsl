<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTDistanceSearch">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID" select="UserID"/>
<xsl:template match="/Root">

<HTML>

<HEAD>
  <TITLE>Shop Distance Search</TITLE>

  <!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,29);
		  event.returnValue=false;
		  };	
 </script>
 
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsUserID = "<xsl:value-of select='$UserID'/>";
var gsFramePageFile = "SMTDistanceSearch.asp"
var gsPageFile = "SMTDistanceSearch.asp";
var gsSearchingIntervalID;
top.layerEntityData.style.visibility = "hidden";
gsPageID = top.gsPageID;

<![CDATA[
//init the table selections, must be last

function initPage(){
    top.gsLoc = window.location.toString().slice(16); 
    top.gsFramePageFile = gsFramePageFile;
	 
  	if( gsPageID == "SMT" )
      top.SwitchMenuSearchItem("main",3);     
	else if(gsPageID = "WebSignups")
	   top.SwitchMenuSearchItem("web", 3);
	
	
	txtCity.value = top.gsCity;
	selState.value = top.gsState;
    txtZip.value = top.gsZip; 
	selIns.value = top.gsIns;
}


function Search(){
	
     if (selIns.value=="") {
	     top.ClientWarning("Please select Insurance Company");
		 return;
	 }	
	
    strCity = txtCity.value.replace(/\s*$/, "");
    strState = selState.selectedIndex > -1 ? selState.value : "";
	
    var strRetVal;
    SearchIndicator();
    if (txtZip.value == "") { 
      if (strCity == "" || strState == ""){
        alert(" Invalid Search. A ZIP Code or a City and State is required to conduct a Distance code search.");
        return;
      }
      else{
	    var co = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspShopZipByCityStateXML", "ShopCity=" + escape(strCity) + "&ShopState=" + strState);
        if (co.status == 0){    
          var sTblData = co.return_value;
          var listArray = sTblData.split("||");
          if ( listArray[1] == "0" )
            ServerEvent();
          else{
            var objXML = new ActiveXObject("Microsoft.XMLDOM");
            objXML.loadXML(listArray[0]);      
            var oCity = objXML.documentElement.selectNodes("/Root/City");
            if (oCity.length > 1){
              strRetVal = window.showModalDialog("../CityPrimaryZip.asp?City=" +  escape(strCity) + "&State=" + strState + "&UserID=" + gsUserID , "", "dialogHeight:190px; dialogWidth:400px; resizable:no; status:no; help:no; center:yes;");
              if (isNaN(strRetVal)){
                alert("Invalid Search. A ZIP Code or a City and State is required to conduct a Distance search.  No City was selected");
                return;
              }
              txtZip.value = strRetVal;   
            }
            else{
              txtZip.value = objXML.documentElement.selectSingleNode("/Root").getAttribute("PrimaryZip");
            }	
          }  
        }
      }  
    }
	
    btnSearch.disabled = true;
	document.frames["ifrmShopList"].frameElement.style.visibility = "hidden";
	SearchIndicator();
	// save search parameters	
	top.gsCity = txtCity.value;
	top.gsState = selState.value;
    top.gsZip = txtZip.value;
	top.gsIns = selIns.value;
	
	var val;
	var qString = "";
	var aInputs = document.all.tags("INPUT");
    for (var i=0; i < aInputs.length; i++) {
		var sType = aInputs[i].type;
    if (sType == "text" || sType == "hidden"){
		  val = escape(aInputs[i].value.replace(/'/g, "''"));
      qString += aInputs[i].name + "=" + val.substr(0, aInputs[i].maxLength) + "&";
		}
  }
	aInputs = document.all.tags("SELECT");
    for (var i=0; i < aInputs.length; i++) {
		qString += aInputs[i].name + "=" + aInputs[i].value + "&";
	}
	qString = qString.slice(0, qString.length - 1);
	btnClear.disabled = true;
    document.frames["ifrmShopList"].frameElement.src = "SMTEntityDistanceSearchResults.asp?" + qString;
}


function SearchIndicator(){
	lblCount.style.display = "none";
	lblSearching.style.display = "inline";
	lblSearching.innerText = "Searching";
	gsSearchingIntervalID = window.setInterval("if (lblSearching.innerText == 'Searching.......') lblSearching.innerText = 'Searching'; lblSearching.innerText += '.'", 750);
}

function NumbersOnly(event){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}

function ShowCount(count){
	var sCountMsg = count;
	sCountMsg += " Shop";
	if (count == '0' || count > '1') sCountMsg += "s";
	sCountMsg += " Found";
	lblCount.innerText = sCountMsg;
	lblSearching.style.display = "none";
	lblCount.style.display = "inline";
	window.clearInterval(top.gsSearchingIntervalID);
	if (count > 0) document.frames["ifrmShopList"].frameElement.style.visibility = "visible";
	btnClear.disabled = false;
	btnSearch.disabled = false;
  
}

function Clear(){
	txtCity.value = "";
	selState.selectedIndex = 0;
	selIns.selectedIndex = 0;
	txtZip.value = "";
	lblSearching.style.display = "none";
	lblCount.style.display = "none";
	lblSorting.style.display = "none";
}    

if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;


]]>

</SCRIPT>
</HEAD>


<BODY class="bodyAPDSubSub" unselectable="on" style="margin:0px;padding:0px;overflow:hidden;background:transparent;"  onLoad="tabInit(false,'#FFFFFF'); initPage();"  tabIndex="-1"   onkeypress="if (event.keyCode == 13) Search();"> 

<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px;width:100%;height:100%;">
   <xsl:call-template name="SMTDistanceSearch">
   </xsl:call-template>
</DIV>

<IFRAME id="ifrmShopList" src="../blank.asp" allowtransparency="true" style="position:relative; top:120px; border:1px; left:0px; visibility:hidden; background:transparent; overflow:hidden;width:765; height:390;">
</IFRAME>

</BODY>
</HTML>

</xsl:template>


<!-- Gets the Main SMT Search Screen -->
  <xsl:template name="SMTDistanceSearch" >

  <TABLE unselectable="on" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2" width="750px">
  <tr>
    <td valign="top">
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <colgroup>
          <col width="100"/>
          <col width="100%"/>
        </colgroup>

        <tr>
		  <td nowrap="nowrap">
		  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		  </td>
        </tr>
        
		<tr>
          <td nowrap="nowrap">City:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="AddressCity" id="txtCity" size="51" maxLength="30"/>
          </td>
        </tr>
        
		<tr>
          <td nowrap="nowrap">State:</td>
          <td nowrap="nowrap">
            <select name="AddressState" id="selState" class="inputFld" style="width:146;">
              <option value=""></option>
      			  <xsl:for-each select="Reference[@ListName='State']">
      				  <xsl:call-template name="BuildSelectOptions"/>
      			  </xsl:for-each>
            </select>
            <img src="/images/spacer.gif" border="0" height="2" width="45"/>
            Zip:
            <img src="/images/spacer.gif" border="0" height="2" width="10"/>
            <input type="text" class="inputFld" name="AddressZip" id="txtZip" size="6" maxLength="5" onkeypress="NumbersOnly(window.event)"/>
          </td>	  
        </tr>
       
	   <tr>
		  <td unselectable="on" nowrap="nowrap">
          Insurance Cmp: </td>
        <td nowrap="nowrap">
		    <select name="InsuranceCompanies" id="selIns" class="inputFld" style="width:146;">
              <option value=""></option>
      			  <xsl:for-each select="Reference[@ListName='InsuranceCompany']">
      				  <xsl:call-template name="BuildSelectOptions"/>
      			  </xsl:for-each>
            </select>
        </td> 
		</tr>
		 
 	    <tr>
		  <td nowrap="nowrap">
		  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		  </td>
        </tr>
		
		<tr>
		  <td nowrap="nowrap" colspan="2">
		  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		  Program Key:  D = DRP; R = RRP; C = CEI; N/A = Non-Program
		  </td>
        </tr>
        
		   
      </table>
    </td>
   	 
	 <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
   </tr>
 </TABLE>

 <div style="position:absolute; top:90px; left:555px;">
  <input type="button" id="btnClear" value="Clear" class="formButton" onclick="Clear()" style="width:80"/>
  <input type="button" id="btnSearch" value="Search" class="formButton" onclick="Search()" style="width:80"/>
 </div>

 <div style="position:absolute; top:95px; left: 333px; height:12px: width; 80px;">   
  <label id="lblCount" style="color:blue; font-weight:600; display:none"></label>
  <label id="lblSearching" style="color:blue; font-weight:600; display:none"></label>
  <label id="lblSorting" style="color:blue; font-weight:600; display:none"></label>
</div>

</xsl:template>


<xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>
		<xsl:value-of select="@Name"/>
	</option>
</xsl:template>

</xsl:stylesheet>



