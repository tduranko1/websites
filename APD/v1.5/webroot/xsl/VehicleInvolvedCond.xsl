<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="VehicleInvolved">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InvolvedCRUD" select="Involved"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>
<xsl:param name="VehicleNumber"/>
<xsl:param name="ExposureCD"/>
<xsl:param name="pageReadOnly"/>

<xsl:template match="/Root">

<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->

<xsl:variable name="AdjInvolvedCRUD">
<xsl:choose>
  <xsl:when test="contains($InvolvedCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
  <xsl:when test="contains($InvolvedCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
  <xsl:otherwise><xsl:value-of select="$InvolvedCRUD"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="InvolvedID" select="/Root/@InvolvedID" />
<xsl:variable name="InvolvedLastUpdatedDate" select="/Root/Involved/@SysLastUpdatedDate"/>


<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspVehicleInvolvedGetDetailXML,VehicleInvolved.xsl,711,145   -->

<HEAD>
<TITLE>Vehicle Involved</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/zipcodeutils.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

  var gsCRUD = '<xsl:value-of select="$InvolvedCRUD"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
  var gsInvolvedID = '<xsl:value-of select="$InvolvedID"/>';
  var gsInvolvedLastUpdatedDate = "<xsl:value-of select="$InvolvedLastUpdatedDate"/>";
  var gsInvolvedTypesCount = '<xsl:value-of select="count(/Root/Reference[@List='InvolvedType'])"/>';
  //var gsInsured = '<xsl:value-of select="Involved/InvolvedType[@InvolvedTypeName='Insured']/@InvolvedTypeID"/>';
  var gsInsured = '<xsl:value-of select="Reference[@List='InvolvedType' and @Name='Insured']/@ReferenceID"/>';
  var gsClaimant = '<xsl:value-of select="Reference[@List='InvolvedType' and @Name='Claimant']/@ReferenceID"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var bPageReadonly = <xsl:value-of select="$pageReadOnly"/>;
  var posInsured = null;
  var posClaimant = null;
  var gbDirty = false;
<![CDATA[
  //function pageInit(){
  function __pageInit(){
    if (parent.gsParty == "1st Party") {
      if (parent.giInvInsuredCount > 0){
        var obj = document.getElementById("chkInvolved_" + gsInsured);
        if (obj) {
          obj.CCDisabled = true;
        }
      }
    } else if (parent.gsParty == "3rd Party") {
      if (parent.giInvVehicleClaimantCount > 0){
        var obj = document.getElementById("chkInvolved_" + gsClaimant);
        if (obj) {
          obj.CCDisabled = true;
        }
      }
    }
    try {
    NameTitle.setFocus();
    } catch (e) {}
  }
  
  function setDirty(){
    gbDirty = true;
  }
  
  function isDirty(){
    return gbDirty;
  }
  
  function validateData(){
    var bInvolvedSelected = false;
    var objNode = xmlReference.documentElement.selectNodes( "/Root/Reference[@List='InvolvedType']" );
    if (objNode) {
      for (var i = 0; i < objNode.length; i++){
        var InvolvedID = objNode[i].getAttribute("ReferenceID");
        var obj = document.getElementById("chkInvolved_" + InvolvedID);
        if (obj && obj.value == 1)
          bInvolvedSelected = true;
      }
      if (bInvolvedSelected == false){
        ClientWarning("Please select an Involved type and try again.");
      }
    }
    return bInvolvedSelected;
  }
  
  function getSaveData(){
    var sProc, sRequest;
    var InvolvedTypeList;

    sRequest = getData(document.body);
    sRequest += "&UserID=" + gsUserID +
                "&ClaimAspectID=" + parent.gsClaimAspectID;

    //Generate the InvolvedTypeList
    xmlReference.setProperty("SelectionLanguage", "XPath");
    InvolvedTypeList = "";
    var objNode = xmlReference.documentElement.selectNodes( "/Root/Reference[@List='InvolvedType']" );
    if (objNode) {
      for (var i = 0; i < objNode.length; i++){
        var InvolvedID = objNode[i].getAttribute("ReferenceID");
        var obj = document.getElementById("chkInvolved_" + InvolvedID);
        if (obj && obj.value == 1)
          InvolvedTypeList += InvolvedID + ",";
      }
      InvolvedTypeList = InvolvedTypeList.substring(0, InvolvedTypeList.length-1);
    }
    
    sRequest += "&InvolvedTypeList=" + InvolvedTypeList;
    
    if (gsInvolvedID > 0) {
      sProc = "uspCondVehInvolvedUpdDetail";
    } else {
      sProc = "uspCondVehInvolvedInsDetail";
    }
    
    var aRequests = new Array();
    aRequests.push( { procName : sProc,
                      method   : "executespnpasxml",
                      data     : sRequest }
                  );
    return makeXMLSaveString(aRequests);
  }
  
  function resetDirty(){
    gbDirty = false;
    resetControlBorders(document.body);
  }
  
  function CalculateAge(objDate, objAge)
  {
    if (objAge.value != "") return;
  
    if (objDate.value == ""){
  		objAge.value = "";
  		return;
  	}
    
  	var myDate = new Date(objDate.value);
    var iAge = Math.round((new Date() - myDate) / 24 / 60 / 60 / 1000 / 365.25);
    objAge.value = iAge;
    if (objAge.value < 0)
      objAge.value = "";
  }

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSubSub" tabindex="-1" style="margin:0px;padding:0px">
  <xsl:if test="/Root/@InvolvedID &gt; 0">
    <IE:APDInput id="InvolvedID" name="InvolvedID" canDirty="false" style="display:none">  
      <xsl:attribute name="value"><xsl:value-of select="/Root/@InvolvedID"/></xsl:attribute>
    </IE:APDInput>
    <IE:APDInput id="SysLastUpdatedDate" name="SysLastUpdatedDate" canDirty="false" style="display:none">  
      <xsl:attribute name="value"><xsl:value-of select="/Root/Involved/@SysLastUpdatedDate"/></xsl:attribute>
    </IE:APDInput>
  </xsl:if>
  <xsl:variable name="dataDisabled">
    <xsl:choose>
      <xsl:when test="contains($AdjInvolvedCRUD, 'U') = true()">false</xsl:when>
      <xsl:otherwise>true</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <table border="0" cellpadding="1" cellspacing="0" style="table-layout:fixed;border-collapse:collapse;">
    <colgroup>
      <col width="80px"/>
      <col width="260px"/>
      <col width="105px"/>
      <col width="250px"/>
    </colgroup>
    <tr>
      <td>Name:</td>
      <td>
        <table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameTitle', /Root/Involved, 'NameTitle', '25', string($dataDisabled), 105, 'true', 'true', 'setDirty()')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameFirst', /Root/Involved, 'NameFirst', '90', string($dataDisabled), 106, 'true', 'true', 'setDirty()')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameLast', /Root/Involved, 'NameLast', '95', string($dataDisabled), 107, 'true', 'true', 'setDirty()')"/>
            </td>
          </tr>
        </table>
      </td>
      <td>Gender:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('GenderCD', /Root/Involved, 'GenderCD', 'GenderCD', 'false', '6', string($dataDisabled), 129, 'true', '', '75', '', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>Business:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BusinessName', /Root/Involved, 'BusinessName', '225', string($dataDisabled), 108, 'true', 'true', 'setDirty()')"/>
      </td>
      <td>SSN/EIN:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('FedTaxId', /Root/Involved, 'FedTaxId', '', string($dataDisabled), 130, 'true', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>Business Type:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('BusinessTypeCD', /Root/Involved, 'BusinessTypeCD', 'BusinessType', 'false', '6', string($dataDisabled), 109, 'true', '', '100', '', 'true', 'setDirty()')"/>        
      </td>
      <td>DOB:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('BirthDate', /Root/Involved, 'BirthDate', 'date', 'false', string($dataDisabled), 131, 'true', 'true', 'CalculateAge(BirthDate, Age);setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>Address:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address1', /Root/Involved, 'Address1', '225', string($dataDisabled), 110, 'true', 'true', 'setDirty()')"/>
      </td>
      <td>Age:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputNumeric('Age', /Root/Involved, 'Age', 'false', '', string($dataDisabled), 132, 'true', '', '', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address2', /Root/Involved, 'Address2', '225', string($dataDisabled), 111, 'true', 'true', 'setDirty()')"/>
      </td>
      <td>Best Number to Call:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('BestContactPhoneCD', /Root/Involved, 'BestContactPhoneCD', 'BestContactPhoneCD', 'false', '6', string($dataDisabled), 133, 'true', '', '100', '', 'true', 'setDirty()')"/>        
      </td>
    </tr>
    <tr>
      <td>City:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressCity', /Root/Involved, 'AddressCity', '175', string($dataDisabled), 112, 'true', 'true', 'setDirty()')"/>
      </td>
      <td>Best Time to Call:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BestContactTime', /Root/Involved, 'BestContactTime', '225', string($dataDisabled), 134, 'true', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>State:</td>
      <td>
        <table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('AddressState', /Root/Involved, 'AddressState', 'State', 'true', '6', string($dataDisabled), 113, 'true', '', '125', '', 'true', '')"/>
              <!-- <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressState', /Root/Involved, 'AddressState', '35', string($dataDisabled), 113, 'true', 'true', 'setDirty()')"/> -->
            </td>
            <td style="padding:0px;padding-left:10px;">Zip:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressZip', /Root/Involved, 'AddressZip', '50', string($dataDisabled), 114, 'true', 'true', 'ValidateZip(AddressZip, AddressCity, AddressState); setDirty()')"/>
            </td>
          </tr>
        </table>
      </td>
      <td>Day:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Day', /Root/Involved, 'DayAreaCode', 'DayExchangeNumber', 'DayUnitNumber', 'DayExtensionNumber', '', 'true', string($dataDisabled), 135, 'false', 'false', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('EmailAddress', /Root/Involved, 'EmailAddress', '225', string($dataDisabled), 115, 'true', 'true', 'checkEMail();setDirty();')"/>
      </td>
      <td>Night:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Night', /Root/Involved, 'NightAreaCode', 'NightExchangeNumber', 'NightUnitNumber', 'NightExtensionNumber', '', 'true', string($dataDisabled), 136, 'false', 'false', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td>Involved Type:</td>
      <td rowspan="3" valign="top">
        <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed;border-collapse:collapse">
        <colgroup>
          <col width="100px"/>
          <col width="100px"/>
        </colgroup>
        <xsl:for-each select="/Root/Reference[@List='InvolvedType' and not((@Name='Insured' and $ExposureCD='3') or (@Name='Claimant' and $ExposureCD='1'))]">
          <!-- <tr>
            <td> -->
              <xsl:if test="position()=1">
                <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
              </xsl:if>
              <td>
              <IE:APDCheckBox alignment="1" canDirty="true" >
                <xsl:attribute name="id">chkInvolved_<xsl:value-of select="@ReferenceID"/></xsl:attribute>
                <xsl:attribute name="name">chkInvolved_<xsl:value-of select="@ReferenceID"/></xsl:attribute>
                <xsl:attribute name="caption"><xsl:value-of select="@Name"/></xsl:attribute>
                <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() + 115"/></xsl:attribute>
                <xsl:attribute name="count"><xsl:value-of select="count(/Root/Involved/InvolvedType[@InvolvedTypeID = current()/@ReferenceID])"/></xsl:attribute>
                <xsl:if test="count(/Root/Involved/InvolvedType[@InvolvedTypeID = current()/@ReferenceID]) &gt; 0">
                  <xsl:attribute name="value">1</xsl:attribute>
                </xsl:if>
                <xsl:if test="@Name='Insured' and count(/Root/Involved/InvolvedType[@InvolvedTypeID = current()/@ReferenceID]) &gt; 0">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:if test="@Name='Claimant' and count(/Root/Involved/InvolvedType[@InvolvedTypeID = current()/@ReferenceID]) &gt; 0">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:if test="(@Name='Insured' and $ExposureCD='3') or (@Name='Claimant' and $ExposureCD='1') or (@Name='Insured' and $ExposureCD='N') or (@Name='Claimant' and $ExposureCD='N')">
                  <xsl:attribute name="style">display:none</xsl:attribute>
                </xsl:if>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                
              </IE:APDCheckBox>
              </td>
              <xsl:if test="position() mod 2 = 0">
                <xsl:text disable-output-escaping="yes">&lt;</xsl:text>/tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
                <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
              </xsl:if>
              <xsl:if test="position()=last()">
                <xsl:text disable-output-escaping="yes">&lt;</xsl:text>/tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
              </xsl:if>
            <!-- </td>
          </tr> -->
        </xsl:for-each>
        </table>
      </td>
      <td>Alt.:</td>
      <td>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Alternate', /Root/Involved, 'AlternateAreaCode', 'AlternateExchangeNumber', 'AlternateUnitNumber', 'AlternateExtensionNumber', '', 'true', string($dataDisabled), 137, 'false', 'false', 'true', 'setDirty()')"/>
      </td>
    </tr>
    <tr>
      <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>

  <xml id="xmlReference"><Root><xsl:copy-of select="/Root/Reference[@List='InvolvedType']"/></Root></xml>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
