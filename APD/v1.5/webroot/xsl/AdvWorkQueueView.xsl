<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  xmlns:user="http://mycompany.com/mynamespace"
  xmlns:js="urn:the-xml-files:xslt"
  id="AdvWorkQueueView">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library-htc.js"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="WindowID"/>
  <xsl:param name="UserId"/>

  <xsl:template match="/Root">

    <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
    <!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspClaimSearchParmGetDetailXML,ClaimSearch.xsl   -->

    <HTML>
      <HEAD>
        <TITLE>Work Queue View</TITLE>
        <!-- STYLES -->
        <LINK rel="STYLESHEET" href="/css/apdcontrols.css" type="text/css"/>
        <LINK href="/css/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <LINK href="/css/style.css" rel="stylesheet" type="text/css" />
        <LINK href="/css/prettify.css" rel="stylesheet" type="text/css" />

        <LINK rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />

        <!-- for radio buttons -->
        <STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

        <!-- CLIENT SCRIPTS -->
        <SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
        <script src="/js/jquery.multiselect.js" type="text/javascript"></script>
        <script type="text/javascript">


          document.onhelp=function(){
          RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,27);
          event.returnValue=false;
          };
        </script>
        <script type="text/javascript">
          $(document).ready(function () {
          var myselect = $('#my-select');
          $('#my-select').multiselect({
          includeSelectAlloption: true
          });
          });
          function GetSecondaryCheckListUsers() {
          var j = [];
          $('#my-select :selected').each(function (i, selected) {
          j[i] = $(selected).val();
          });
          return j;
          }

        </script>

        <!-- Page Specific Scripting -->
        <SCRIPT language="JavaScript">
          var vWindowID = "<xsl:value-of select="$WindowID"/>";
          var vUserID = "<xsl:value-of select="$UserId"/>";

          <![CDATA[

var sSearchCriteria = "";

function pageInit(){
 

}

function WorkflowPhases() {
}
function SpecificTasks() {
}
function SendToNext(){
       
       var ReturnAdvWorkQueueValue;
       var SecondaryCheckListUserVal = GetSecondaryCheckListUsers();
       var WorkflowPhasesVal = document.getElementById("csWorkflowPhases").value;
       var SpecificTasksVal =  document.getElementById("csSSpecificTasks").value;
       var WorkflowPhasestxt = document.getElementById("csWorkflowPhases").text;
       var SpecificTaskstxt = "0";//document.getElementById("csSSpecificTasks").text;
             
       if(SecondaryCheckListUserVal == "" || SecondaryCheckListUserVal == undefined)
           SecondaryCheckListUserVal = 0;
           
       if(WorkflowPhasesVal == "" || WorkflowPhasesVal == undefined)    
           WorkflowPhasesVal = 0;
           
       if(SpecificTasksVal == "" || SpecificTasksVal == undefined)    
           SpecificTasksVal = 0;
       
       ReturnAdvWorkQueueValue = { SecondaryCheckListUser: SecondaryCheckListUserVal, WorkflowPhases:WorkflowPhasesVal, SpecificTasks:SpecificTasksVal,WorkflowPhasesTest:WorkflowPhasestxt  };
           
       window.returnValue = ReturnAdvWorkQueueValue; 
       window.close();
}


]]>
        </SCRIPT>
      </HEAD>

      <BODY class="bodyAPDSub" unselectable="on" style="border:0px;padding:5px" onload="pageInit()">
        <table border="0" cellpadding="2" cellspacing="0" style="width:100%">
          <colgroup>
            <col width="90"/>
            <col width="200"/>
            <col width="100"/>
            <col width="*"/>
            <col width="50"/>
          </colgroup>
          <tr>
            <td style="font-weight:bold">
              Second:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            </td>
            <td>
              <select id="my-select" multiple="multiple">
                <xsl:for-each select="Users/User">
                  <xsl:sort select="@NameLast"/>
                  <option>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@UserID"/>
                    </xsl:attribute>
                    <xsl:if test="@SupervisorFlag='1'">
                      <xsl:choose>
                        <xsl:when test="count(//User[@SupervisorID = current()/@UserID and @SupervisorFlag = '1']) &gt; 0">
                          <xsl:attribute name="imgSrc">/images/2User.gif</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="imgSrc">/images/1User.gif</xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:if>
                    <xsl:value-of select="concat(@NameLast, ', ' , @NameFirst)"/>
                  </option>
                </xsl:for-each>
              </select>
              <!--<input type="button" id="btnSelected" value="Get Selected" onclick="superViewSelNew();" />-->
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold">
              Workflow Phases
            </td>
            <td>
              <IE:APDCustomSelect id="csWorkflowPhases" name="csWorkflowPhases" overflow="scroll" width="150" maxwidth="235" displayCount="10" blankFirst="false" canDirty="false" ExpRequired="true" CCDisabled="false" onChange="WorkflowPhases()" CCTabIndex="1">
                <xsl:attribute name="value">
                  <xsl:value-of select="$UserId"/>
                </xsl:attribute>
                <xsl:for-each select="WorkFlowUsers/WorkFlowUser">
                  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value">
                      <xsl:value-of select="@UserID"/>
                    </xsl:attribute>
                    <xsl:value-of select="@NameFirst"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td style="font-weight:bold">
              Specific Tasks:
            </td>
            <td>
              <IE:APDCustomSelect id="csSSpecificTasks" name="csSpecificTasks" width="200" size="7" maxwidth="235" displayCount="10" blankFirst="false" canDirty="false" ExpRequired="true" CCDisabled="false" onChange="SpecificTasks()" CCTabIndex="2">
                <xsl:for-each select="Tasks/Task">
		 <xsl:sort select="@Name" data-type="text" order="ascending"/>
                  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value">
                      <xsl:value-of select="@TaskID"/>
                    </xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <IE:APDButton id="btSendToNext" name="btSendToNext" value="Search" CCTabIndex="3" onButtonClick="SendToNext();" style="float: right;"/>
            </td>
          </tr>
        </table>
      </BODY>
    </HTML>
  </xsl:template>

</xsl:stylesheet>
