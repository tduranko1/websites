<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicle">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="RepUserID"/>
<xsl:param name="RepNameFirst"/>
<xsl:param name="RepNameLast"/>
<xsl:param name="RepOfficeName"/>
<xsl:param name="RepPhoneDay"/>
<xsl:param name="RepEmailAddress"/>
<xsl:param name="RepInsCompanyName"/>
<xsl:param name="EventVehicleAssignmentCancelled"/>
<xsl:param name="Debug">0</xsl:param>  <!-- set value 1 to pre-pop fields for testing -->

<xsl:template match="/Root">

<HTML>
<HEAD>
<TITLE>Mobile Electronics</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid2.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
    .nowrap {text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:100%;cursor:default;}

    .div_popup {background-color: #FFFFFF; color: #000000; border: 1px solid #0B198C;}

</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/zipcodeutils.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datetime.js"></SCRIPT>

<SCRIPT language="JavaScript">

var sUserID = "<xsl:value-of select="$RepUserID"/>";
var sClaimAspectID = "";
var sCallerID = "";
var sCallerEmailAddress = "";
var sEventVehicleAssignmentCancelled = "<xsl:value-of select="$EventVehicleAssignmentCancelled"/>";
var sRepNameFirst = escape("<xsl:value-of select="$RepNameFirst"/>");
var sRepNameLast = escape("<xsl:value-of select="$RepNameLast"/>");
var sRepPhoneDay = "<xsl:value-of select="$RepPhoneDay"/>";
var sRepEmailAddress = escape("<xsl:value-of select="$RepEmailAddress"/>");
var sCallerOfficeName = "";
var sCallerInsCompanyName = escape("<xsl:value-of select="@InsuranceCompanyName"/>");
var sInsuranceCompanyID = "<xsl:value-of select="@InsuranceCompanyID"/>";
var sMEAssignmentTypeID = "<xsl:value-of select="/Root/Reference[@List='AssignmentType' and @ServiceChannelName='Mobile Electronics']/@ReferenceID"/>";
var sMEAssignmentTypeName = "<xsl:value-of select="/Root/Reference[@List='AssignmentType' and @ServiceChannelName='Mobile Electronics']/@Name"/>";
var sPartyCD = "1";
var sClientCoverageTypeID = "<xsl:value-of select="CoverageType[@CoverageProfileCD='COMP']/@ClientCoverageTypeID"/>";
var sCoverageProfileCD = "<xsl:value-of select="CoverageType[@CoverageProfileCD='COMP']/@CoverageProfileCD"/>";
var sClientCoverageName = escape("<xsl:value-of select="CoverageType[@CoverageProfileCD='COMP']/@Name"/>");
var sDriveable = "1";
var sAssignmentAtSelectionFlag = "<xsl:value-of select="@AssignmentAtSelectionFlag"/>";
var sDemoFlag = "<xsl:value-of select="@DemoFlag"/>";
var dteStart = new Date();
var bPrimaryChanging = false;
var sCRLF = String.fromCharCode(13,10);
var sUnknownImpactID = "<xsl:value-of select="/Root/Reference[@List='ImpactPoint' and @Name='Unknown']/@ReferenceID"/>";
var sCallerNameFirst, sCallerNameLast;
var gbSubmittingGlass = false;
var gbGlassSubmitted = false;
var sLynxID = "";
var sApdRrep = "";

var cAssign = 1, cGlass = 2, cSummary = 3;
var gnWizardState = 0; // can be cAssign, cGlass", or cSummary

<![CDATA[

  function initPage()
  {
		try {

    	if (document.getElementById("selLossState")){
    	  if (selLossState.Options){
    	    if (selLossState.Options.length == 1)
    	      selLossState.selectedIndex = 0;
    	    if (selAssignedBy && selAssignedBy.Options){
    	      if (selAssignedBy.Options.length == 1)
    	        selAssignedBy.selectedIndex = 0;
    	    }
    	  } else {
    	    window.setTimeout("initPage()", 150);
    	    return;
    	  }
    	}

    	if (stat1) {
    	  stat1.style.top = (document.body.offsetHeight - 100) / 2;
    	  stat1.style.left = (document.body.offsetWidth - 300) / 2;
    	}

      updateGlassScript();
      updateSummaryScript();
      gnWizardState = cAssign;

      // Update the coverage controls displayed.
      onChangeCoverageClientCode();

    } catch (e) { ClientError( "initPage() " + e.description ); }
  }

  function xmlInit()
  {
    xmlAPDScripting.setProperty('SelectionLanguage', 'XPath');
  }

  // Continue button event handler.
  function onContinue()
  {
      if ( gnWizardState == cAssign )
          submitClaim();
      else if ( gnWizardState == cGlass )
      {
          if ( validateGlass() )
              updateGlassClaimToAPD();
      }
      else if ( gnWizardState == cSummary )
          doNewClaim( true );
  }

  // Continue button event handler.
  function onContinueSuccess()
  {
      if ( gnWizardState == cAssign )
      {
          tabClaim.disableControls( true );

          if ( chkGlassDamage.value == 1 )
          {
              gnWizardState = cGlass;
              updateGlassScript();
              setGlassDispatched( true );
              tabGlass.hidden = false;
              tabGrpClaim.SelectTab( 1 );
          }
          else
          {
              showSummaryTab();
          }
      }
      else if ( gnWizardState == cGlass )
      {
          tabGlass.disableControls( true );
          btnSubmit.value = "New Claim";
          btnCancel.disabled = true;
          gnWizardState = cSummary;
          ClientInfo("Assignment successful.  Click 'New Claim' when you are ready to enter a new claim.");
      }
  }

  function showSummaryTab()
  {
      updateSummaryScript();
      gnWizardState = cSummary;
      btnSubmit.value = "New Claim";
      btnCancel.disabled = true;
      tabSummary.hidden = false;
      tabGrpClaim.SelectTab( 2 );
  }

  // Cancel button event handler.
  function onCancel()
  {
      if ( gnWizardState == cAssign )
      {
          doNewClaim( true );
      }
      else if ( gnWizardState == cGlass )
      {
          var sRet = YesNoMessage("Cancel Claim Confirmation", "This Claim has already been entered into APD.  Are you sure you wish to Cancel it?" );
          if (sRet == "Yes")
          {
              var sProcName = "dbo.uspWorkflowNotifyEvent";
              var sRequest = "EventID=" + sEventVehicleAssignmentCancelled +
                         "&ClaimAspectID=" + sClaimAspectID +
                         "&Description=" + escape( "ME assignment for LynxID " + sLynxID + " cancelled." ) +
                         "&UserID=" + sUserID;

              var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProcName, sRequest);
              if ( co.status == -1 )
              {
                  ClientError( "Cancellation event for LynxID " + sLynxID + " failed!" );
              }
              else
              {
                  ClientInfo( sLynxID + " has been cancelled.  You may now begin a new assignment." );
                  doNewClaim( false );
              }
          }
      }
  }

  // Checks the Carrier Claim Number to see if it alredy exists in the system.
  // Will put up a warning message if it does, allowing the user to follow up.
  function duplicateClaimCheck( obj, bGlass )
  {
    try {
      if (stat1)
        stat1.Show("Checking Claim Number...");
      window.setTimeout("duplicateClaimCheck2('" + obj.value + "', " + bGlass + ")", 100);
    } catch (e) {
      ClientError( "duplicateClaimCheck() " + e.description)
    }
  }

  function duplicateClaimCheck2( sClaimNumber, bGlass )
  {
      try
      {
          btnSubmit.CCDisabled = false;
          if ( sClaimNumber.length > 0 )
          {
              var sProcName = "uspClaimSearchXML";
              var sRequest = "ClaimNumber=" + sClaimNumber + "&InsuranceCompanyID=" + sInsuranceCompanyID;
              var oXml = getXml( sProcName, sRequest );

              if ( oXml != null )
              {
                  // A LynxID will be returned if the claim number already exists.
                  var oLynxID = oXml.selectSingleNode( "/Root/Claim/@LynxId" );
                  if ( oLynxID != null )
                  {
                      var sFoundLynxID = oLynxID.value;

                      // Was a duplicate claim found in the system?
                      if ( ( sFoundLynxID != null ) && ( sFoundLynxID.length > 0 ) )
                      {
                          if ( bGlass && ( sLynxID != sFoundLynxID ) )
                          {
                              ClientWarning( "This Claim Number already exists in the APD system for " +
                                  unescape( sCallerInsCompanyName ) + " as LynxID " + sFoundLynxID +
                                  ".  You cannot continue until a Claim Number is entered that does " +
                                  "not already exist in the system." );

                              btnSubmit.CCDisabled = true;
                          }
                      }
                  }
              }
          }
      } catch (e) {
          ClientError( "duplicateClaimCheck2() " + e.description );
      } finally {
          if (stat1) stat1.Hide();
      }
  }

  function getAssignedToInfo()
  {
    if (selAssignedBy.selectedIndex == -1) return;
    sCallerNameFirst = "";
    sCallerNameLast = "";
    sCallerUserCD = selAssignedBy.value.split("|");

    var sCaller = sCallerUserCD[0];
    sCallerID = sCallerUserCD[1];
    var oNode = null;

    if (sCaller == "U"){ //user
      oNode = xmlUserList.selectSingleNode("/Root/Office/OfficeUser[@UserID='" + sCallerID + "']");
      if (oNode) {
        sCallerNameFirst = oNode.getAttribute("NameFirst");
        sCallerNameLast = oNode.getAttribute("NameLast");
        sCallerOfficeName = oNode.parentNode.getAttribute("OfficeName");
        sCallerEmailAddress = oNode.getAttribute("EmailAddress");
      }
    } else { //office
      oNode = xmlUserList.selectSingleNode("//Office[@OfficeID='" + sCallerID + "']");
      if (oNode) {
        sCallerNameFirst = oNode.getAttribute("OfficeName");
        sCallerNameLast = "";
      }
    }
    if (sCallerNameFirst == "" && sCallerNameLast == ""){
      ClientError("Unable to extract the assigned by information")
      return false;
    }
  }

  function setGlassDispatched( bDispatched )
  {
    try {
      txtAppointment.CCDisabled = txtDispatch.CCDisabled = txtShopName.CCDisabled = txtShopPhone.CCDisabled = !bDispatched;
      txtDispatch.required = txtShopName.required = txtShopPhone.required = bDispatched;
      txtNoDispatchReason.CCDisabled = bDispatched;
      txtNoDispatchReason.required = !bDispatched;
    } catch (e) { ClientError( "setGlassDispatched() " + e.description ); }
  }

  function displayAddDamage()
  {
    txtAdditionalDamageComments.CCDisabled = (chkAdditionalDamage.value == 0);
    txtAdditionalDamageComments.required = (chkAdditionalDamage.value == 1);
  }

  function checkVINLength(){
    if (txtVIN) {
      if (txtVIN.value != "" && txtVIN.value.length < 6){
        event.returnValue = false;
        event.cancelBubble = true;
        ClientWarning("Vehicle VIN number must be atleast 6 characters long.");
        txtVIN.setFocus();
      }
    }
  }

  // Updates the glass script text field with the insured name.
  function updateGlassScript()
  {
      var sAssignmentScript = updateAssignmentScript(
          xmlAPDScripting.selectSingleNode("/Root/AllstateME/Claims[@Type='Glass']").text );
      if ( sAssignmentScript != "" )
          txtGlassScript.value = sAssignmentScript;
  }

  // Updates the summary script text field with the insured name.
  function updateSummaryScript()
  {
      var sAssignmentScript = updateAssignmentScript(
          xmlAPDScripting.selectSingleNode("/Root/AllstateME/Claims[@Type='All']").text );
      if ( sAssignmentScript != "" )
          ScriptingAll.innerText = sAssignmentScript;
  }

  function updateAssignmentScript( sAssignmentScript )
  {
      if (txtInsuredLastName.value != "")
          sAssignmentScript = sAssignmentScript.replace(/@Insured@/g, "Mr/Mrs. " + txtInsuredLastName.value);
      else
          sAssignmentScript = sAssignmentScript.replace(/@Insured@/g, txtInsuredBusinessName.value + " or Insured Last Name");

      if ( sApdRrep != "" )
          sAssignmentScript = sAssignmentScript.replace(/@APD Rep First Last@/g, sApdRrep);

      if ( sLynxID != "" )
          sAssignmentScript = sAssignmentScript.replace(/@LYNX ID@/g, sLynxID);

      return sAssignmentScript;
  }

  ///////////////////////////////////////////////////////////////////
  //
  // SUBMIT CLAIM TO APD - opens required service channels, too.
  //
  ///////////////////////////////////////////////////////////////////

  function submitClaim()
  {
    try {
      btnSubmit.CCDisabled = true;
      btnSubmitGlass.CCDisabled = true;
      if (stat1)
        stat1.Show("Submitting claim...");
      window.setTimeout("submitClaim2()", 100);
    } catch (e) {
      ClientError( "submitClaim() " + e.description)
    }
  }

  function submitClaim2()
  {
    try
    {
      var bSuccess = false;

      // Copy claim number from the first page to the second page.
      txtClaimNumber2.value = txtClaimNumber.value;

      if ( validateApdData() && ( ( chkGlassDamage.value == 0 ) || validateGlassData() ) )
      {
        tabGrpClaim.CCDisabled = true;
        stuffWAXMLData("apd");

        var oXML = xmlWebAssignment;
        var oXMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
        oXMLHttp.open("POST","FNOLAssignment.asp",false);
        oXMLHttp.send(oXML);
        var strRet = oXMLHttp.ResponseText;

        if (strRet.indexOf("Error") == -1)
        {
          //assignment success
          var oXML = new ActiveXObject("MSXML2.DOMDocument");
          oXML.async = false;
          oXML.loadXML(strRet);

          if (oXML.parseError.errorCode == 0)
          {
            //no parse error
            var oRoot = oXML.selectSingleNode("/Root");
            if (oRoot)
            {
              try
              {
                var sApdRepPhone = "";
                sLynxID = glsSummaryLynxID.innerText = clmSummaryLynxID.innerText = oRoot.getAttribute("LynxID");
                top.sCommentLynxID = sLynxID;
                var oVeh = oXML.selectSingleNode("/Root/Vehicle[@VehicleNumber='1']");
                sClaimAspectID = oVeh.getAttribute("ClaimAspectID");
                if (oVeh){
                  glsSummaryCreatedDate.innerText = clmSummaryCreatedDate.innerText =
                    formatSQLDateTimeAMPM(oVeh.getAttribute("CreatedDate"));
                  var oVehRep = oXML.selectSingleNode("/Root/Vehicle[@VehicleNumber='1']/ClaimAspectOwner");
                  if (oVehRep){
                    sApdRrep = oVehRep.getAttribute("UserNameFirst") + " " + oVehRep.getAttribute("UserNameLast");
                    glsSummaryClaimsRep.innerText = clmSummaryClaimsRep.innerText = sApdRrep;
                    sApdRepPhone = "(" + oVehRep.getAttribute("UserPhoneAreaCode") + ") " + oVehRep.getAttribute("UserPhoneExchangeNumber") + " " + oVehRep.getAttribute("UserPhoneUnitNumber") + (oVehRep.getAttribute("UserExtensionNumber") != '' ? " x " + oVehRep.getAttribute("UserExtensionNumber") : "");
                    glsSummaryClaimsRepPhone.innerText = clmSummaryClaimsRepPhone.innerText = sApdRepPhone;
                    glsSummaryClaimsRepEmail.innerText = clmSummaryClaimsRepEmail.innerText = oVehRep.getAttribute("UserEmail");
                  }
                }

                bSuccess = true;
                updateSummaryScript();

              } catch (e) {ClientError(e.description)}

            } else {
              ClientError("'/Root/Root' node not found [" + sVehListXML + "]");
            }
          } else {
            ClientError("XML Parse error occurred while loading '" + sVehListXML + "'");
          }
        } else {
          ClientError("An error occurred while submitting the claim. Look at the log file or the server event viewer.");
        }
      }
    } catch (e) {
      ClientError( "submitClaim2() " + e.description );
    } finally {
      if (stat1)
        stat1.Hide();
      tabGrpClaim.CCDisabled = false;
      btnSubmit.CCDisabled = false;
      if (!gbGlassSubmitted)
        btnSubmitGlass.CCDisabled = false;
      if ( bSuccess )
        onContinueSuccess();
    }
  }

  function validateApdData()
  {
    txtDeductible.required = false;
    var bValid = tabClaim.doValidate();
    txtDeductible.required = true;

    if ( bValid == false) {
      ClientWarning("Claim tab has some missing required fields. Please enter data for all required fields and try again.<br/><br/>Fields with a blue border are required fields.");
      return false;
    }

    if (sCallerNameFirst == "" && sCallerNameLast == ""){
      ClientWarning("Please select a Assigned by from the list and try again.");
      return false;
    }

    if (txtInsuredFirstName.value.Trim() == "" && txtInsuredLastName.value.Trim() == "" && txtInsuredBusinessName.value.Trim() == ""){
      ClientWarning("Please enter the Insured First and Last Name or the Insured Business Name.");
      return false;
    }

    if (txtYear.value.toString().trim != "" && txtYear.value.toString().length != 4){
      ClientWarning("Vehicle Year must be 4 digits.<br/><br/>Please try again.");
      return false;
    }

    if (rbgPrimaryContactPhone.value == 2 && txtNightPhone.value == ""){
      ClientWarning("You have selected the second contact phone as the primary and no phone number was provided. Please try again.");
      return false;
    }

    if (rbgPrimaryContactPhone.value == 3 && txtAltPhone.value == ""){
      ClientWarning("You have selected the third contact phone as the primary and no phone number was provided. Please try again.");
      return false;
    }

    var oCompNode = xmlCoveragesEntered.selectSingleNode( "/Root/Coverage[CoverageProfileCD='COMP' and AdditionalCoverageFlag='0']" );
    if ( oCompNode == null ) {
      oCompNode = xmlCoveragesEntered.selectSingleNode( "/Root/Coverage[CoverageProfileCD='COMP']" );
      if ( oCompNode == null ) {
        ClientWarning("You must select at least one comprehensive coverage type.");
      } else {
        ClientWarning("You must select at least one principal comprehensive coverage type.  You have only supplemental comprehensive coverages selected." );
      }
      return false;
    }

    if (!gbSubmittingGlass)
    {
      if (chkGlassDamage.value == 1){
        // do nothing
      } else {
        var sRet = YesNoMessage("No Glass Damage Confirmation", "You have not entered any Glass Damage. Is this correct?");
        if (sRet != "Yes")
            return false;
      }

      if (chkAdditionalDamage.value == 1)
      {
        if (txtAdditionalDamageComments.value.Trim() == "")
        {
            ClientWarning("You have chosen Body Damage and Damage Information has not been entered.<br/><br/>Please enter the Damage Information and try again.");
            return false;
        }
      }
      else
      {
        var sRet = YesNoMessage("No Body Damage Confirmation", "You have not entered any Body Damage. Is this correct?");
        if (sRet != "Yes")
            return false;
      }
    }

    return true;
  }

  ///////////////////////////////////////////////////////////////////
  //
  // SUBMIT CLAIM TO AGC
  //
  ///////////////////////////////////////////////////////////////////

  function submitGlassClaimToAGC(){
    btnSubmit.CCDisabled = true;
    btnSubmitGlass.CCDisabled = true;
    gbSubmittingGlass = true;
    if (stat1)
      stat1.Show("Submitting glass claim...");
    window.setTimeout("submitGlassClaimToAGC2()", 100);
  }

  function submitGlassClaimToAGC2()
  {
    try {

      if ( validateApdData() && validateGlassData() )
      {
        tabGrpClaim.CCDisabled = true;
        stuffWAXMLData("glass");

        var oXML = xmlWebAssignment;
        var oNode = oXML.selectSingleNode("/WebAssignment");
        var oAttribute = oXML.createNode(2, "SubmitGlassClaim", "");
        oAttribute.text = "true";
        oNode.attributes.setNamedItem(oAttribute);

        oNode = oXML.selectSingleNode("/WebAssignment");

        var oXMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
        oXMLHttp.open("POST","FNOLAssignment.asp",false);
        oXMLHttp.send(oXML);
        var strRet = oXMLHttp.ResponseText;

        var oXMLRet = new ActiveXObject("MSXML2.DOMDocument");

        oXMLRet.loadXML(strRet);

        if (strRet.indexOf("<error>") == -1){
          gbGlassSubmitted = true;
          var oMsg = oXMLRet.selectSingleNode("//records/record/@msg_text");
          var oRefNumber = oXMLRet.selectSingleNode("//records/record/@loss_no");
          var sClipBoardPastedMsg = "";
          document.getElementById("RefNumberHolder").value = "";

          if (oRefNumber)
          {
            var sRefNumber = oRefNumber.text;
            document.getElementById("RefNumberHolder").value = sRefNumber.substr(1, sRefNumber.length - 1);

            if (sRefNumber != "")
            {
              // past ref number to clipboard
              var oObj = document.getElementById("RefNumberHolder");
              var rng = oObj.createTextRange();
              rng.execCommand("Copy");
              sClipBoardPastedMsg = "The Reference Number has been copied to the clipboard. You may paste the number in the AGC application."
            }
          }

          if (oMsg)
            ClientInfo(oMsg.text + "<br><br>" + sClipBoardPastedMsg);
          else
            ClientWarning("The Claim Data was successfully sent to AGC.  No reference number was provided by AGC.");
        }
        else{

          gbGlassSubmitted = false;

          var oMsg = oXMLRet.selectSingleNode("//error/description");

          if (oMsg)
            ClientWarning(oMsg.text);
          else
            ClientWarning("The Claim Data was NOT successfully sent to AGC.  Please validate the data and resubmit.");
        }

      }

    } catch (e) {
      ClientError(e.description)
    } finally {
      if (stat1)
        stat1.Hide();
      tabGrpClaim.CCDisabled = false;
      btnSubmit.CCDisabled = false;

      gbSubmittingGlass = false;
      if (gbGlassSubmitted){
        chkGlassDamage.CCDisabled = true;
        chkGlassDamage.title = "Cannot uncheck Glass damage because this claim was submitted to Glass.";
        btnSubmitGlass.CCDisabled = true;
      }
      else
        btnSubmitGlass.CCDisabled = false;
    }
  }

  function validateGlassData()
  {
      if (txtAddress1.value == "")
      {
        ClientWarning("You have not entered an Address.  Please correct and try again.");
        return false;
      }

      if (txtAddressCity.value == "")
      {
        ClientWarning("You have not entered a City.  Please correct and try again.");
        return false;
      }

      if (selAddressState.selectedIndex == -1)
      {
        ClientWarning("You have not selected a State.  Please correct and try again.");
        return false;
      }

      if (txtAddressZip.value == "")
      {
        ClientWarning("You have not entered a ZIP code.  Please correct and try again.");
        return false;
      }

      return true;
  }

  ///////////////////////////////////////////////////////////////////
  //
  // UPDATE GLASS INFO TO APD
  //
  ///////////////////////////////////////////////////////////////////

  function updateGlassClaimToAPD(){
    btnSubmit.CCDisabled = true;
    if (stat1)
      stat1.Show("Updating glass claim...");
    window.setTimeout("updateGlassClaimToAPD2()", 100);
  }

  function updateGlassClaimToAPD2()
  {
    var sError = "";
    var bSuccess = false;

    try {

      if ( validateGlass() )
      {
          var bContinue = true;

          // If the user changed the claim number on the glass tab,
          // then we need to update that information in APD.
          if ( txtClaimNumber2.value != txtClaimNumber.value )
          {
              bContinue = false;

              var sProcedure = "dbo.uspClaimNumberUpdDetail";
              var sRequest = "LynxID=" + sLynxID + "&ClientClaimNumber=" + txtClaimNumber2.value;

              var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProcedure, sRequest);
              if ( co.status == -1 )
                  sError = sProcedure + " " + sRequest;
              else
              {
                  sError = co.return_value.toString();
                  if ( sError.indexOf( "Error" ) == -1 )
                  {
                      bContinue = true;
                      sError = "";
                  }
              }
          }

          if ( bContinue )
          {
              var sRemarks = "";

              if ( rbgGlassDispatched.value == 0 )
                  sRemarks = "Glass Damage indicated but not dispatched. Reason: " +
                      (txtNoDispatchReason.value == "" ? "none." : txtNoDispatchReason.value) + sCRLF;

              sRemarks += "Comments: " + txtComments.value.substr(0, 300) + sCRLF;

              if ( selCompanyCode.selectedIndex != -1 )
                  sRemarks += selCompanyCode.text.substr(0, 50) + sCRLF;

              if ( txtLineNo.value != "" )
                  sRemarks += "Line #:" + ( txtLineNo.value < 10 ? "0" : "" ) + parseInt( txtLineNo.value, 10 ) + sCRLF;

              // Truncate, remove single quotes.
              sRemarks = sRemarks.substr( 0, 1000 );
              sRemarks = sRemarks.replace( /'/g, "" );

              var sProcedure = "dbo.uspAssignmentUpdDetail";
              var sRequest = "ClaimAspectID=" + sClaimAspectID +
                  "&ServiceChannelCD=GL&AltRemarksServiceChannelCD=ME" +
                  "&DispatchedFlag=" + rbgGlassDispatched.value +
                  "&ReferenceId=" + document.getElementById("RefNumberHolder").value +
                  "&DispatchId=" + txtDispatch.value +
                  "&ShopName=" + txtShopName.value +
                  "&ShopPhoneAreaCode=" + txtShopPhone.areaCode +
                  "&ShopPhoneExchangeNumber=" + txtShopPhone.phoneExchange +
                  "&ShopPhoneUnitNumber=" + txtShopPhone.phoneNumber +
                  "&AppointmentDate=" + txtAppointment.value +
                  "&AssignmentRemarks=" + sRemarks +
                  "&UserID=" + sUserID;

              var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProcedure, sRequest);
              if ( co.status == -1 )
                  sError = sProcedure + " " + sRequest;
              else
              {
                  sError = co.return_value.toString();
                  if ( sError.indexOf( "Error" ) == -1 )
                  {
                      bSuccess = true;
                      sError = "";
                  }
              }
          }
       }

    } catch (e) {
        ClientError( "Update glass info to APD failed on: " + e.description );
    } finally {
        if (stat1)
            stat1.Hide();

        tabGrpClaim.CCDisabled = false;
        btnSubmit.CCDisabled = false;

        if ( sError.length > 0 )
            ClientError( sError.substring( 0, 500 ) );
        else if ( bSuccess )
            onContinueSuccess();
    }
  }

  function validateGlass()
  {
    if (rbgGlassDispatched.value == 1)
    {
      // Trim is not working // if (txtDispatch.value.Trim() == ""){
      if (txtDispatch.value == ""){
        ClientWarning("You have chosen Glass Dispatch and a Dispatch number is not specified.<br/><br/>Please enter the data and try again.");
        return false;
      }

      if (txtShopName.value.Trim() == ""){
        ClientWarning("You have chosen Glass Dispatch and a Shop name is not specified.<br/><br/>Please enter the data and try again.");
        return false;
      }
      if (txtShopPhone.value.Trim() == ""){
        ClientWarning("You have chosen Glass Dispatch and a Shop phone number is not specified.<br/><br/>Please enter the data and try again.");
        return false;
      }

      if (txtAppointment.value != "")
      {
        var dtAppointment = txtAppointment.date;
        var dtLossDate = txtLossDate.date;
        if (dtAppointment < dtLossDate){
          ClientWarning("Appointment date cannot be before the Loss Date.<br/><br/>Please enter the data and try again.");
          return false;
        }
      }

      if ( sClaimAspectID == null ) {
        ClientWarning("The system is missing some information vital to updating the glass information to APD.<br/><br/>Please try again.");
        return false;
      }

    } else {
      if (txtNoDispatchReason.value == ""){
        ClientWarning("Please enter a No Glass Dispatched Reason.");
        return false;
      }
    }
    return true;
  }

	///////////////////////////////////////////////////////////////////
	// Quicky params and methods for xml document buildout.
	var oAddDoc = null;
	var oAddParent = null;

	// Prepare the document and parent node vars for calls to AddElement.
	function SetAddDocAndParent( oDoc, oParent )
	{
			oAddDoc = oDoc;
			oAddParent = oParent;
	}

	// Appends an element to the parent node in the document set above.
	function AddElement( sName, sValue )
	{
      var oNode = oAddDoc.createNode( 1, sName, "" );
      oNode.appendChild( oAddDoc.createCDATASection( sValue ) );
      oAddParent.appendChild( oNode );
	}

  // Appends an element to the parent node in the document set above.
  function AddElementNoCdata( sName, sValue )
  {
      var oNode = oAddDoc.createNode( 1, sName, "" );
      oNode.appendChild( oAddDoc.createTextNode( sValue ) );
      oAddParent.appendChild( oNode );
  }
  //
  ///////////////////////////////////////////////////////////////////


  function stuffWAXMLData(sSystem)
	{
  //Gather the remarks to be used in PS and ME assignment.
    var sRemarks = "";
    var sPassThruRemarks = "";
    var sShopRemarks = "";

    if (chkAdditionalDamage.value == 1)
      sPassThruRemarks += "Body Damages: " + txtAdditionalDamageComments.value + sCRLF;
    else
      sRemarks += "No Body Damage reported." + sCRLF;

    sPassThruRemarks += "Comments: " + txtComments.value.substr(0, 300) + sCRLF;

    if (selCompanyCode.selectedIndex != -1)
      sPassThruRemarks += selCompanyCode.text.substr(0, 50) + sCRLF;

    if (txtLineNo.value != "")
      sPassThruRemarks += "Line #:" + (txtLineNo.value < 10 ? "0" : "") + parseInt(txtLineNo.value, 10) + sCRLF;

    sPassThruRemarks = sPassThruRemarks.substr(0, 1000);
    sPassThruRemarks = sPassThruRemarks.replace(/'/g, ""); //remove single quotes. it is causing the AGC to blow up.

    sShopRemarks = sRemarks + sPassThruRemarks;

    sRemarks += sPassThruRemarks;
    sRemarks = sRemarks.substr(0, 1000);
    sShopRemarks = sShopRemarks.substr(0, 1000);

    var oXMLTemplate = new ActiveXObject("MSXML2.DOMDocument");
    oXMLTemplate.async = false;
    oXMLTemplate.loadXML(xmlWebAssignmentTemplate.innerHTML);

    var oXML = xmlWebAssignment;
    var dteEnd = new Date();
    var oNode = null;

    // Reinitialize data island from template.
    oNode = oXML.selectSingleNode("/WebAssignment");
    if (oNode)
      oXML.removeChild(oNode);

    oNode = oXMLTemplate.selectSingleNode("/WebAssignment")
    oXML.appendChild(oNode);

    var oClaimNode = oXML.selectSingleNode("/WebAssignment/Claim");
    var oVehNode = oXML.selectSingleNode("/WebAssignment/Vehicle");

    if (oClaimNode)
		{
			SetAddDocAndParent( oXML, oClaimNode );

      // Add the claim node elements
      AddElement( "FNOLUserID", sUserID );
      AddElement( "InsuranceCompanyID", sInsuranceCompanyID );
      AddElement( "AssignmentAtSelectionFlag", sAssignmentAtSelectionFlag );
      AddElement( "DemoFlag", sDemoFlag );
      AddElement( "AssignmentDescription", sMEAssignmentTypeName );

      // This is Assigned by user's office name. This is plugged into the
			// carrierOfficeName so that the report will be fine.
      AddElement( "CarrierOfficeName", unescape(sCallerOfficeName) );

      // This is Assigned by user's id. This is plugged into the carrierRepUserID
			// so that the report will be fine and the carrier tab
      // on the APD will point to the correct one.
      AddElement( "CarrierRepUserID", sCallerID );
      AddElement( "CarrierRepNameFirst", unescape(sCallerNameFirst) );
      AddElement( "CarrierRepNameLast", unescape(sCallerNameLast) );
      AddElement( "CarrierRepPhoneDay", "" );
      AddElement( "CarrierRepEmailAddress", unescape(sCallerEmailAddress) );

      // Assigned by user's Insurance company, plugged into the CarrierName so that the report will be fine.
      AddElement( "CarrierName", unescape(sCallerInsCompanyName) );
      AddElement( "CoverageClaimNumber", txtClaimNumber.value );
      AddElement( "LossDate", txtLossDate.value );
      AddElement( "LossAddressState", selLossState.value );
      AddElement( "CallerNameFirst", sCallerNameFirst );
      AddElement( "CallerNameLast", sCallerNameLast );
      AddElement( "CallerRelationToInsuredID", selAssignerRelation.value );
      AddElement( "CallerRelationToInsuredIDDescription", selAssignerRelation.text );
      AddElement( "InsuredNameFirst", txtInsuredFirstName.value );
      AddElement( "InsuredNameLast", txtInsuredLastName.value );
      AddElement( "InsuredBusinessName", txtInsuredBusinessName.value );
      AddElement( "ComprehensiveDeductibleAmt", txtDeductible.value );
      AddElement( "LossDescription", txtLossDetails.value + sCRLF + txtAdditionalDamageComments.value );
      AddElement( "TimeStarted", formatDateTime(dteStart) );
      AddElement( "TimeFinished", formatDateTime(dteEnd) );
      AddElement( "IntakeStartSeconds", Math.round(dteStart.valueOf()/1000) );
      AddElement( "IntakeEndSeconds", Math.round(dteEnd.valueOf()/1000) );
      AddElement( "IntakeSeconds", Math.round((dteEnd - dteStart)/1000) );
      AddElement( "Remarks", sRemarks );

      // Copy all coverage nodes under claim.
      var oNodeList = xmlCoveragesEntered.selectNodes( "//Root/Coverage" );
      for (var i = 0; i < oNodeList.length; i++)
          oClaimNode.appendChild( oNodeList[i].cloneNode( true ) );
		}

    if (oVehNode)
		{
			SetAddDocAndParent( oXML, oVehNode );

      // Add the claim node elements
      AddElement( "AssignmentTypeID", sMEAssignmentTypeID );
      AddElement( "ExposureCD", sPartyCD );
      AddElement( "CoverageProfileCD", sCoverageProfileCD );
      AddElement( "OwnerNameFirst", txtInsuredFirstName.value );
      AddElement( "OwnerNameLast", txtInsuredLastName.value );
      AddElement( "OwnerBusinessName", txtInsuredBusinessName.value );
      AddElement( "ContactNameFirst", txtInsuredFirstName.value );
      AddElement( "ContactNameLast", txtInsuredLastName.value );
      AddElement( "VIN", (sSystem == "apd") ? txtVIN.value : "" );

      AddElement( "VehicleYear", txtYear.value );
      AddElement( "Make", txtMake.value );
      AddElement( "Model", txtModel.value );
      AddElement( "Drivable", sDriveable );

      AddElement( "ContactPhone", txtDayPhone.areaCode + txtDayPhone.phoneExchange + txtDayPhone.phoneNumber );
      AddElement( "ContactPhoneExt", txtDayPhone.phoneExtension );
      AddElement( "ContactPhoneSumm", txtDayPhone.areaCode + "-" + txtDayPhone.phoneExchange + "-" +
				txtDayPhone.phoneNumber + "x" + txtDayPhone.phoneExtension );

      AddElement( "ContactNightPhone", "" );
      AddElement( "ContactNightPhoneExt", "" );
      AddElement( "ContactNightPhoneSumm", "" );

      AddElement( "ContactAltPhone", txtAltPhone.areaCode + txtAltPhone.phoneExchange + txtAltPhone.phoneNumber );
      AddElement( "ContactAltPhoneExt", txtAltPhone.phoneExtension );
      AddElement( "ContactAltPhoneSumm", ( txtAltPhone.value == "" ? "" : (
				txtAltPhone.areaCode + "-" + txtAltPhone.phoneExchange + "-" +
				txtAltPhone.phoneNumber + "x" + txtAltPhone.phoneExtension ) ) );
      AddElement( "ContactBestPhoneCD", "D" );

      AddElement( "OwnerPhone", txtDayPhone.areaCode + txtDayPhone.phoneExchange + txtDayPhone.phoneNumber );
      AddElement( "OwnerPhoneExt", txtDayPhone.phoneExtension );
      AddElement( "OwnerNightPhone", "" );
      AddElement( "OwnerNightPhoneExt", "" );
      AddElement( "OwnerAltPhone", txtAltPhone.areaCode + txtAltPhone.phoneExchange + txtAltPhone.phoneNumber );
      AddElement( "OwnerAltPhoneExt", txtAltPhone.phoneExtension );

      var oNode = oXML.createNode(1, "OwnerBestPhoneCD", "");
      if (rbgPrimaryContactPhone.value == 2) //Night Phone
        oNode.appendChild(oXML.createCDATASection("N"));
      else if (rbgPrimaryContactPhone.value == 3) //Alt Phone
        oNode.appendChild(oXML.createCDATASection("A"));
      else
        oNode.appendChild(oXML.createCDATASection("D"));
      oVehNode.appendChild(oNode);

      oNode = oXML.createNode(1, "OwnerPhoneSumm", "");
      if (rbgPrimaryContactPhone.value == 2) //Night Phone
        oNode.appendChild(oXML.createCDATASection(txtNightPhone.areaCode + txtNightPhone.phoneExchange + txtNightPhone.phoneNumber + "x" + txtNightPhone.phoneExtension));
      else if (rbgPrimaryContactPhone.value == 3) //Alt Phone
        oNode.appendChild(oXML.createCDATASection(txtAltPhone.areaCode + txtAltPhone.phoneExchange + txtAltPhone.phoneNumber + "x" + txtAltPhone.phoneExtension));
      else
        oNode.appendChild(oXML.createCDATASection(txtDayPhone.areaCode + txtDayPhone.phoneExchange + txtDayPhone.phoneNumber + "x" + txtDayPhone.phoneExtension));
      oVehNode.appendChild(oNode);

      AddElement( "OwnerAddress1", txtAddress1.value );
      AddElement( "OwnerAddress2", txtAddress2.value );
      AddElement( "OwnerAddressCity", txtAddressCity.value );
      AddElement( "OwnerAddressState", (selAddressState.selectedIndex == -1 ? "" : selAddressState.value) );
      AddElement( "OwnerAddressZip", txtAddressZip.value );
      AddElement( "DeductibleAmt", txtDeductible.value );
      AddElement( "ClientCoverageTypeID", sClientCoverageTypeID );
      AddElement( "ClientCoverageTypeDesc", unescape(sClientCoverageName) );
      AddElement( "ShopRemarks", sShopRemarks );
      AddElement( "SourceApplicationPassThruData", chkGlassDamage.value == 1 ? sPassThruRemarks : "" );

      AddElement( "PhysicalDamageFlag", chkAdditionalDamage.value );
      AddElement( "GlassDamageFlag", chkGlassDamage.value );

      if (oClaimNode)
        oClaimNode.appendChild(oNode.cloneNode(true));
    }
  }

  function formatDateTime(d) {
    var dte = new Date(d);
    var s = "";
    s = (dte.getMonth() + 1) + "/" + dte.getDate() + "/" + dte.getFullYear() + " " +
        dte.getHours() + ":" + (dte.getMinutes() < 10 ? "0" : "" ) + dte.getMinutes() + ":" + (dte.getSeconds() < 10 ? "0" : "") + dte.getSeconds();
    return s;
  }

  function showNADA(){
    alert("Function not yet implemented.");
  }

  function checkPrimary(obj){
    if (bPrimaryChanging) return;
    bPrimaryChanging = true;
    if (obj.id == "chkAltPhPrimary"){
      if (chkContactPhPrimary.value == 1)
        chkContactPhPrimary.value = 0;
    } else {
      if (chkAltPhPrimary.value == 1)
        chkAltPhPrimary.value = 0;
    }
    bPrimaryChanging = false;
  }

  function doNewClaim( bPrompt )
  {
      if ( bPrompt )
      {
          var sRet = YesNoMessage("Confirmation", "Proceed to new claim?");
          if (sRet != "Yes")
              return;
      }
      window.location.reload();
  }

  function getCityState()
  {
    ValidateZip(txtAddressZip, txtAddressCity, selAddressState);
    if (txtAddressCity.value != "" && selAddressState.selectedIndex != -1)
      txtClaimNumber.setFocus();
  }

  ///////////////////////////////////////////////////////////////////
  // Coverage Popup Scripts

  // Add coverage - init and show the dialog.
  function addCoverage()
  {
      txtDeductible.value = txtLimit.value = txtRentalDays.value = txtRentalMaxDays.value = txtRentalMaxVal.value = 0;

      // Clear and refill the coverages combo with only those coverages
      // that have not already been entered.

      selCoverageClientCode.Clear();

      var arCoverages = xmlCoverageTypes.selectNodes( "/Root/CoverageType" );
      var arSelected = xmlCoveragesEntered.selectNodes( "/Root/Coverage" );
      var bHasItems = false;

      if ( arCoverages != null )
      {
          for ( var n = 0; n < arCoverages.length; n++ )
          {
              var sTypeID = arCoverages[n].getAttribute( "ClientCoverageTypeID" );
              var bFound = false;

              if ( arSelected != null )
              {
                  for ( var m = 0; m < arSelected.length; m++ )
                  {
                      var oSelTypeID = arSelected[m].selectSingleNode( "ClientCoverageTypeID" );
                      if ( oSelTypeID )
                      {
                          var sSelTypeID = oSelTypeID.text;
                          if ( sSelTypeID == sTypeID )
                          {
                              bFound = true;
                              break;
                          }
                      }
                  }
              }

              if ( !bFound )
              {
                  selCoverageClientCode.AddItem( sTypeID,
                      arCoverages[n].getAttribute("ClientCode") + ": " +
                      arCoverages[n].getAttribute("Name") );
                  bHasItems = true;
              }

          }
      }

      if ( bHasItems )
      {
          selCoverageClientCode.selectedIndex = 0;
          coveragePopUp.style.display = "inline";
          selCoverageClientCode.setFocus();
      }
      else
      {
          ClientInfo( "You have already added every available coverage type." );
      }
  }

  // Cancel add coverage - reset and hide the dialog.
  function onCoverageCancel()
  {
      selCoverageClientCode.selectedIndex = 0;
      //onChangeCoverageClientCode();
      coveragePopUp.style.display = "none";
  }

  // Called when the coverage code combo box changes.
  // When this happens we need to change the visible controls.
  function onChangeCoverageClientCode()
  {
      // Look for rental in the newly selected code.
      var sTypeID = selCoverageClientCode.value;
      var oTypeNode = xmlCoverageTypes.selectSingleNode(
          "//CoverageType[@ClientCoverageTypeID='" + sTypeID + "']" );
      var sProfileCD = ( oTypeNode != null ) ? oTypeNode.getAttribute( "CoverageProfileCD" ) : "COMP";

      // Found rental, so enable the rental controls.
      if ( sProfileCD == "RENT" )
      {
          divCoverageRental.style.display = "inline";
          divCoverageNormal.style.display = "none";
      }
      // Not rental, so enable the normal controls.
      else
      {
          divCoverageRental.style.display = "none";
          divCoverageNormal.style.display = "inline";
      }
  }

  function missingData( name ) { return "You must enter a " + name + ".<br/><br/>"; }

  // Del coverage - remove a row from the grid.
  function delCoverage()
  {
      var nSel = CoverageGrid.selectedRow.recordNumber;
      if ( nSel == -1 )
          ClientWarning( "You must select a valid coverage for removal." );
      else
      {
          var oRoot = xmlCoveragesEntered.selectSingleNode( "Root" );
          var oCoverage = oRoot.selectSingleNode( "//Coverage[position()=" + ( nSel ) + "]" );
          oRoot.removeChild( oCoverage );

          CoverageGrid.refreshData();
      }
  }

  /*
    Adds a coverage element to the coverage XML island.
    The coverage elements look as follows.

    <Coverage>
      <ClientCoverageTypeID/>
      <DeductibleAmt/>
      <LimitAmt/>
      <LimitDailyAmt/>
      <MaximumDays/>
    </Coverage>
  */
  function onCoverageSave()
  {
      // Look for rental in the newly selected code.
      var sTypeID = selCoverageClientCode.value;
      var oTypeNode = xmlCoverageTypes.selectSingleNode(
          "//CoverageType[@ClientCoverageTypeID='" + sTypeID + "']" );
      var sProfileCD = oTypeNode.getAttribute( "CoverageProfileCD" );
      var sAddCovFlag = oTypeNode.getAttribute( "AdditionalCoverageFlag" );
      var sClientCode = oTypeNode.getAttribute( "ClientCode" );
      var sWarning = "";
      var bRental = ( sProfileCD == "RENT" );

      // Trim down the edit strings.
      var sRentalDays = txtRentalDays.value.Trim();
      var sRentalMaxDays = txtRentalMaxDays.value.Trim();
      var sRentalMaxVal = txtRentalMaxVal.value.Trim();
      var sDeductible = txtDeductible.value.Trim();
      var sLimit = txtLimit.value.Trim();

      // Do some basic validation.
      if ( !sDeductible.length )
          sWarning += missingData( "Deductible" );

      //if ( !bRental && !sLimit.length )
      //    sWarning += missingData( "Limit" );

      if ( sWarning.length )
      {
          ClientWarning( sWarning + "Please enter the data and try again." );
          return;
      }

      // Found rental, so validate the rental controls.
      if ( bRental && !sRentalDays.length && !sRentalMaxDays.length && !sRentalMaxVal.length )
          ClientWarning( "Rental coverage item is missing detail." );

      // Add a new coverage element to the XML document.
      var oRoot = xmlCoveragesEntered.selectSingleNode("/Root");
      var oCoverage = xmlCoveragesEntered.createNode( 1, "Coverage", "" );
      oRoot.appendChild( oCoverage );

      // Set those common values between rental and other.
      SetAddDocAndParent( xmlCoveragesEntered, oCoverage );
      AddElementNoCdata( "ClientCoverageTypeID", sTypeID );
      AddElementNoCdata( "CoverageProfileCD", sProfileCD );
      AddElementNoCdata( "AdditionalCoverageFlag", sAddCovFlag );
      AddElementNoCdata( "DeductibleAmt", sDeductible );

      var sDesc = sClientCode + ": Deductible=" + sDeductible;

      // Now set the specific fields per coverage type.
      if ( bRental )
      {
          AddElementNoCdata( "LimitAmt", sRentalMaxVal );
          AddElementNoCdata( "LimitDailyAmt", sRentalDays );
          AddElementNoCdata( "MaximumDays", sRentalMaxDays );

          if ( sRentalMaxVal.length )
              sDesc += ", Maximum=" + sRentalMaxVal;
          if ( sRentalDays.length )
              sDesc += ", Days=" + sRentalDays;
          if ( sRentalMaxDays.length )
              sDesc += ", Max Days=" + sRentalMaxDays;
      }
      else
      {
          AddElementNoCdata( "LimitAmt", sLimit );
          AddElementNoCdata( "LimitDailyAmt", "" );
          AddElementNoCdata( "MaximumDays", "" );

          if ( sLimit.length )
              sDesc += ", Limit=" + sLimit;
      }

      AddElementNoCdata( "Description", sDesc );

      //CoverageGrid.datasrc = xmlCoveragesEntered;
      //CoverageGrid.CCDataPageSize = 50;
      CoverageGrid.refreshData();

      onCoverageCancel();
  }

  ///////////////////////////////////////////////////////////////////

  function getXml( sProcName, sRequest )
  {
      var objXML = null;
      var co = RSExecute( "/rs/RSADSAction.asp", "RadExecute", sProcName, sRequest );
      if (co.status == 0)
      {
          var sTblData = co.return_value;
          var listArray = sTblData.split("||");
          if ( listArray[1] == "0" )
              ServerEvent();
          else
          {
              var objXML = new ActiveXObject("Microsoft.XMLDOM");
              objXML.loadXML(listArray[0]);
          }
      }
      return objXML;
  }

]]>

</SCRIPT>

</HEAD>
<BODY unselectable="on" style="background:#FFFAEB; margin:0px; padding:0px; padding-left:5px; overflow:hidden;" tabIndex="-1" onload="initPage()">
<IE:APDStatus id="stat1" name="stat1" width="300" height="100" />
<xsl:choose>
  <xsl:when test="count(AssignmentType[@AssignmentTypeID='11']) &gt; 0"> <!-- Client has been setup for Mobile Electronics -->

    <IE:APDTabGroup id="tabGrpClaim" name="tabGrpClaim" many="false" width="645" height="547"
        preselectTab="0" stackable="false" CCDisabled="false" CCTabIndex="1" showADS="false">

      <IE:APDTab id="tabClaim" name="tabClaim" caption="Mobile Electronics Assignment" width1="50"
          tabPage="divClaimDetail" CCDisabled="false" CCTabIndex="2" hidden="false"  />
      <IE:APDTab id="tabGlass" name="tabGlass" caption="Glass Assignment" width1="50"
          tabPage="divGlassDetail" CCDisabled="false" CCTabIndex="3" hidden="true"  />
      <IE:APDTab id="tabSummary" name="tabSummary" caption="Summary" width1="50"
          tabPage="divSummaryDetail" CCDisabled="false" CCTabIndex="4" hidden="true"  />

      <IE:APDTabPage name="divClaimDetail" id="divClaimDetail" style="display:none">
          <xsl:call-template name="ClaimDetail"/>
      </IE:APDTabPage>
      <IE:APDTabPage name="divGlassDetail" id="divGlassDetail" style="display:none">
          <xsl:call-template name="GlassDetail"/>
      </IE:APDTabPage>
      <IE:APDTabPage name="divSummaryDetail" id="divSummaryDetail" style="display:none">
          <xsl:call-template name="SummaryDetail"/>
      </IE:APDTabPage>

    </IE:APDTabGroup>
    <br/><br/>

    <table border="0" cellpadding="2" cellspacing="0" style="width:645px;table-layout:fixed">
      <colgroup>
        <col width="*"/>
        <col width="100"/>
        <col width="106"/>
        <col width="106"/>
      </colgroup>
      <tr>
        <td colspan="2"><span style="height:15px;width:15px;border:1px solid #00BFFF;background-color:#FFFFFF"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Required fields</td>
        <td>
          <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="100"
            CCDisabled="false" CCTabIndex="91" onButtonClick="onCancel()" />
        </td>
        <td>
          <IE:APDButton id="btnSubmit" name="btnSubmit" value="Continue" width="100"
            CCDisabled="false" CCTabIndex="90" onButtonClick="onContinue()" />
        </td>
      </tr>
    </table>

    <!-- Mobile Electronics Web Assignment template - coverages XML-->
    <!-- Data collected by the web page will be stuffed at the time of submission -->
    <xml id="xmlCoveragesEntered" name="xmlCoveragesEntered">
      <Root/>
    </xml>

    <xml id="xmlUserList" name="xmlUserList">
      <Root>
        <xsl:copy-of select="Office"/>
      </Root>
    </xml>

    <!-- This data island will be initialized from the xmlWebAssignmentTemplate below each time stuffWAXMLData is called. This will allow stuffWAXMLData to be
         called multiple times without 'growing'. -->
    <xml id="xmlWebAssignment" name="xmlWebAssignment"/>

    <!-- Mobile Electronics Web Assignment template -->
    <!-- Data collected by the web page will be stuffed at the time of submission -->
    <xml id="xmlWebAssignmentTemplate" name="xmlWebAssignmentTemplate">
      <WebAssignment version='300'>
      	<Claim>
      		<NewClaimFlag>1</NewClaimFlag>
      		<NoticeMethodID>5</NoticeMethodID>
      		<DataSource>Mobile Electronics Assignment</DataSource>
      		<InsuredPhone></InsuredPhone>
      		<InsuredName></InsuredName>
      		<InsuredPhoneSumm></InsuredPhoneSumm>
      		<Mileage></Mileage>
      		<Remarks></Remarks>
      		<RentalInstructions></RentalInstructions>
      		<CustomScriptingMemo></CustomScriptingMemo>
      	</Claim>
      	<Vehicle ProgramShop='0'>
      		<PrimaryDamage><xsl:value-of select="/Root/Reference[@List='ImpactPoint' and @Name='Unknown']/@ReferenceID"/></PrimaryDamage>
      		<SecondaryDamage></SecondaryDamage>
      		<ImpactLocations><xsl:value-of select="/Root/Reference[@List='ImpactPoint' and @Name='Unknown']/@ReferenceID"/></ImpactLocations>
          <Mileage></Mileage>
      		<ShopLocationID></ShopLocationID>
      		<ShopSearchLogID></ShopSearchLogID>
      		<SelectedShopRank></SelectedShopRank>
      		<SelectedShopScore></SelectedShopScore>
          <LicensePlateNumber></LicensePlateNumber>
      		<ShopName></ShopName>
      		<ShopAddress1></ShopAddress1>
      		<ShopAddress2></ShopAddress2>
      		<ShopCity></ShopCity>
      		<ShopState></ShopState>
      		<ShopZip></ShopZip>
      		<ShopPhone></ShopPhone>
      		<ShopFax></ShopFax>
          <ComprehensiveLimitAmt></ComprehensiveLimitAmt>
      		<CoverageProfileUiCD></CoverageProfileUiCD>
          <LimitAmt></LimitAmt>
      		<PrimaryDamageDescription>Unknown</PrimaryDamageDescription>
      		<SecondaryDamageDescription></SecondaryDamageDescription>
      		<AssignmentTypeIDDescription></AssignmentTypeIDDescription>
      		<VehicleNumber>1</VehicleNumber>
      		<DrivingDirections></DrivingDirections>
      		<RentalInstructions></RentalInstructions>
      	</Vehicle>
      </WebAssignment>
    </xml>
  </xsl:when>
  <xsl:otherwise>
    <div style="font:8pt Tahoma;color:#FF0000;text-align:center;font-weight:bold">
    Insurance company (id: <xsl:value-of select="@InsuranceCompanyID"/>) is not setup for Mobile Electronics assignments. Please contact your supervisor.
    </div>
  </xsl:otherwise>
</xsl:choose>

<!-- Coverages reference data -->
<xml id="xmlCoverageTypes" name="xmlCoverageTypes">
  <Root>
    <xsl:copy-of select="/Root/CoverageType"/>
  </Root>
</xml>

<!-- Temporary holder for the Reference number from ACG to be copied to clipboard -->
<TEXTAREA ID="RefNumberHolder" STYLE="display:none;"></TEXTAREA>

<!-- This Data Island will read the Scripting XML from the SRC file -->
<xml name="xmlAPDScripting" id="xmlAPDScripting" src="APDScripting.xml" ondatasetcomplete="xmlInit()" />

</BODY>
</HTML>
</xsl:template>

<!-- ======================================================================== ==
      MOBILE ELECTRONICS INPUT TAB
  == ======================================================================== -->

<xsl:template name="ClaimDetail">
   <div style="position:absolute;top:232px;left:93px">
       <IE:APDDataGrid id="CoverageGrid" CCDataSrc="xmlCoveragesEntered"
           CCDataPageSize="3" showAlternateColor="false" altColor="#E6E6FA"
           CCWidth="400px" CCHeight="85" showHeader="false" gridLines="horizontal" keyField="" required="true">
         <IE:APDDataGridHeader>
           <IE:APDDataGridTR>
             <IE:APDDataGridTD width="375px" caption="Description" fldName="Description" DataType="text" sortable="true" CCStyle=""/>
           </IE:APDDataGridTR>
         </IE:APDDataGridHeader>
         <IE:APDDataGridBody>
           <IE:APDDataGridTR ondblclick="">
             <IE:APDDataGridTD>
               <IE:APDDataGridFld fldName="Description" />
             </IE:APDDataGridTD>
           </IE:APDDataGridTR>
         </IE:APDDataGridBody>
       </IE:APDDataGrid>
   </div>
  <div>
    <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;border-collapse:collapse;width:100%">
      <colgroup>
        <col width="86px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td colspan="2">
          <table border="0" cellpadding="0" cellspacing="0">
            <colgroup>
              <col width="86px"/>
              <col width="150px"/>
              <col width="25px"/>
              <col width="86px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td>Assigned by:</td>
              <td>
                <IE:APDCustomSelect id="selAssignedBy" name="selAssignedBy" displayCount="6" direction="1" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="5" required="true" width="200" sort1="true" onChange="getAssignedToInfo()">
                  <xsl:for-each select="Office[@ClientOfficeId != 'FCS']/OfficeUser">
                    <xsl:sort select="@NameLast" data-type="text" order="ascending"/>
                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value">U|<xsl:value-of select="@UserID"/></xsl:attribute>
                      <xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/>
                    </IE:dropDownItem>
                  </xsl:for-each>
                </IE:APDCustomSelect>
              </td>
              <td/>
              <td>Company Code:</td>
              <td>
                <IE:APDCustomSelect id="selCompanyCode" name="selCompanyCode" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="6" required="true" width="230" onChange="">
                  <xsl:if test="$Debug=1"><xsl:attribute name="selectedIndex">1</xsl:attribute></xsl:if>
                  <IE:dropDownItem style="display:none" value="004">004 - Joint Underwriting Association</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="010">010 - Allstate Insurance Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="020">020 - Northbrook Property &amp; Casualty</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="021">021 - Northbrook Indemnity Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="022">022 - Northbrook National</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="023">023 - Insured Lloyds of London Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="025">025 - Northbrook EX &amp; SU</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="027">027 - Allstate Fire &amp; Casualty Insurance</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="060">060 - Allstate Indemnity Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="063">063 - Allstate Texas Lloyds Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="064">064 - Deerbrook Insurance Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="065">065 - Allstate Property &amp; Casualty</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="066">066 - First Insurance Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="068">068 - Texas County Mutual</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="070">070 - Allstate Floridian</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="085">085 - Allstate Floridian Indemnity Company</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="090">090 - Allstate Canada</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="095">095 - Allstate New Jersey</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="100">100 - California Earthquake Authority</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="101">101 - Hawaii Hurricane Relief Fund</IE:dropDownItem>
                  <IE:dropDownItem style="display:none" value="328">328 - OA City Mut Fire</IE:dropDownItem>
                </IE:APDCustomSelect>
              </td>
              <td style="display:none">
                <IE:APDCustomSelect id="selAssignerRelation" name="selAssignerRelation" value="14" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="7" required="true" width="150" onChange="">
                  <xsl:for-each select="Reference[@List='CallerRelationToInsured']">
                    <xsl:sort select="@Name" data-type="text" order="ascending"/>
                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                      <xsl:value-of select="@Name"/>
                    </IE:dropDownItem>
                  </xsl:for-each>
                </IE:APDCustomSelect>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td>Insured Name:<sup style="font-size:9px; color:#4169E1;">1</sup></td>
        <td>
          <IE:APDInput id="txtInsuredFirstName" name="txtInsuredFirstName" size="25" maxLength="50" required="false" canDirty="false" CCDisabled="false" CCTabIndex="8" onChange="">
            <xsl:if test="$Debug=1"><xsl:attribute name="value">Ted</xsl:attribute></xsl:if>
          </IE:APDInput>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <IE:APDInput id="txtInsuredLastName" name="txtInsuredLastName" size="35" maxLength="50" required="false" canDirty="false" CCDisabled="false" CCTabIndex="9" onChange="">
            <xsl:if test="$Debug=1"><xsl:attribute name="value">Pattison</xsl:attribute></xsl:if>
          </IE:APDInput>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <sup style="font-size:9px; color:#4169E1;">1</sup>
          <IMG unselectable="on" src="/images/spacer.gif" width="3" height="1" border="0" alt=""/>
          <span style="font-size: 11px; color: #4169E1;">Required if Insured is an individual</span>
        </td>
      </tr>

      <tr valign="top">
        <td colspan="2">
          <IE:APDRadioGroup id="rbgPrimaryContactPhone" name="rbgPrimaryContactPhone" required="false" canDirty="false" CCDisabled="false"/>
          <table border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed;width:100%">
            <colgroup>
              <col width="330px"/>
              <col width="*"/>
            </colgroup>
            <tr valign="top">
              <td>
                <table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:4px;">
                  <colgroup>
                    <col width="87px"/>
                    <col width="150px"/>
                    <col width="75px"/>
                  </colgroup>
                  <tr>
                    <td>Business Name:<sup style="font-size:9px; color:#4169E1;">2</sup></td>
                    <td>
                      <IE:APDInput id="txtInsuredBusinessName" name="txtInsuredBusinessName" size="40" maxLength="50" required="false" canDirty="false" CCDisabled="false" CCTabIndex="10" onchange="" >
                        <xsl:if test="$Debug=1"><xsl:attribute name="value">Microsort Press</xsl:attribute></xsl:if>
                      </IE:APDInput>
                    </td>
                  </tr>
                  <tr>
                    <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                    <td align="center">
                      <sup style="font-size:9px; color:#4169E1;">2</sup>
                      <IMG unselectable="on" src="/images/spacer.gif" width="3" height="1" border="0" alt=""/>
                      <span style="font-size: 11px; color: #4169E1;">Required if Insured is a business</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Day Phone:</td>
                    <td>
                      <IE:APDInputPhone id="txtDayPhone" name="txtDayPhone" required="true" canDirty="false"
                          showExtension="true" CCDisabled="false" CCTabIndex="11" onChange="" >
                        <xsl:if test="$Debug=1"><xsl:attribute name="value">4565455444</xsl:attribute></xsl:if>
                      </IE:APDInputPhone>
                    </td>
                  </tr>
                  <tr>
                    <td>Alternate Phone:</td>
                    <td>
                      <IE:APDInputPhone id="txtAltPhone" name="txtAltPhone" required="false" canDirty="false"
                          showExtension="true" CCDisabled="false" CCTabIndex="12" onChange="" />
                    </td>
                  </tr>
                </table>
              </td>
              <td>
                <table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:4px;">
                  <colgroup>
                    <col width="50px"/>
                    <col width="*"/>
                  </colgroup>
                  <tr>
                    <td>Address:</td>
                    <td>
                      <IE:APDInput id="txtAddress1" name="txtAddress1" size="45" maxLength="50" required="true"
                          canDirty="false" CCDisabled="false" CCTabIndex="13" >
                        <xsl:if test="$Debug=1"><xsl:attribute name="value">123 Main</xsl:attribute></xsl:if>
                      </IE:APDInput>
                    </td>
                  </tr>
                  <tr>
                    <td/>
                    <td>
                      <IE:APDInput id="txtAddress2" name="txtAddress2" size="45" maxLength="50" required="false"
                          canDirty="false" CCDisabled="false" CCTabIndex="14" />
                    </td>
                  </tr>
                  <tr>
                    <td>City:</td>
                    <td>
                      <IE:APDInput id="txtAddressCity" name="txtAddressCity" size="25" maxLength="30" required="true"
                          canDirty="false" CCDisabled="false" CCTabIndex="16" >
                        <xsl:if test="$Debug=1"><xsl:attribute name="value">Fort Myers</xsl:attribute></xsl:if>
                      </IE:APDInput>
                    </td>
                  </tr>
                  <tr>
                    <td>State:</td>
                    <td>
                      <IE:APDCustomSelect id="selAddressState" name="selAddressState" displayCount="6" blankFirst="false"
                          width="125" canDirty="false" CCDisabled="false" CCTabIndex="17" required="true" onChange="">
                        <xsl:if test="$Debug=1"><xsl:attribute name="selectedIndex">9</xsl:attribute></xsl:if>
                        <xsl:for-each select="/Root/Reference[@List='ContractState']">
                          <IE:dropDownItem style="display:none">
                            <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                            <xsl:value-of select="@Name"/>
                          </IE:dropDownItem>
                        </xsl:for-each>
                      </IE:APDCustomSelect>
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      Zip:
                      <IE:APDInputNumeric id="txtAddressZip" name="txtAddressZip" precision="5" scale="0"
                          neg="false" int="true" required="true" canDirty="false" CCDisabled="false"
                          CCTabIndex="15" onChange="getCityState()" >
                        <xsl:if test="$Debug=1"><xsl:attribute name="value">33919</xsl:attribute></xsl:if>
                      </IE:APDInputNumeric>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td>Claim Number:</td>
        <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="110px"/>
              <col width="70px"/>
              <col width="120px"/>
              <col width="70px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td>
                <IE:APDInput id="txtClaimNumber" name="txtClaimNumber" size="12" maxLength="10"
                    required="true" canDirty="false" CCDisabled="false" CCTabIndex="18"
                    onfocusout="duplicateClaimCheck( this, false )">
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">1021450497</xsl:attribute></xsl:if>
                </IE:APDInput>
              </td>
              <td>Loss Date:</td>
              <td>
                <IE:APDInputDate id="txtLossDate" name="txtLossDate" type="date" futureDate="true" required="true" canDirty="false" CCDisabled="false" CCTabIndex="19" onChange="" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">05/10/2005</xsl:attribute></xsl:if>
                </IE:APDInputDate>
              </td>
              <td>Loss State:</td>
              <td>
                <IE:APDCustomSelect id="selLossState" name="selLossState" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="20" required="true" width="150">
                  <xsl:if test="$Debug=1"><xsl:attribute name="selectedIndex">1</xsl:attribute></xsl:if>
                  <xsl:for-each select="ContractStates">
                    <xsl:sort select="@Name" data-type="text" order="ascending"/>
                    <xsl:variable name="StateCode"><xsl:value-of select="@StateCode"/></xsl:variable>
                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="@StateCode"/></xsl:attribute>
                      <xsl:value-of select="/Root/Reference[@List='ContractState' and @ReferenceID=$StateCode]/@Name"/>
                    </IE:dropDownItem>
                  </xsl:for-each>
                </IE:APDCustomSelect>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td>Year:</td>
        <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="110px"/>
              <col width="70px"/>
              <col width="120px"/>
              <col width="70px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td>
                <IE:APDInputNumeric id="txtYear" name="txtYear" precision="4" scale="0" neg="false" int="true" required="true" canDirty="false" CCDisabled="false" CCTabIndex="21" onChange="" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">2005</xsl:attribute></xsl:if>
                </IE:APDInputNumeric>
              </td>
              <td>Make:</td>
              <td>
                <IE:APDInput id="txtMake" name="txtMake" size="15" maxLength="50" required="true" canDirty="false" CCDisabled="false" CCTabIndex="22" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">Ford</xsl:attribute></xsl:if>
                </IE:APDInput>
              </td>
              <td>Model:</td>
              <td>
                <IE:APDInput id="txtModel" name="txtModel" size="15" maxLength="50" required="true" canDirty="false" CCDisabled="false" CCTabIndex="23" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">Mustang</xsl:attribute></xsl:if>
                </IE:APDInput>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td>VIN:<sup style="font-size:9px; color:#4169E1;">3</sup></td>
        <td>
          <IE:APDInput id="txtVIN" name="txtVIN" size="17" maxLength="17" required="true" canDirty="false" CCDisabled="false" CCTabIndex="24" onchange="checkVINLength()">
            <xsl:if test="$Debug=1"><xsl:attribute name="value">123456789</xsl:attribute></xsl:if>
          </IE:APDInput>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <button style="border:0px;background-color:transparent;height:18px;width:26px;display:none" onclick="showNADA()" tabIndex="16"><img src="images/nada1.gif"/></button>
          <IMG unselectable="on" src="/images/spacer.gif" width="6" height="1" border="0" alt=""/>
          <sup style="font-size:9px; color:#4169E1;">3</sup>
          <IMG unselectable="on" src="/images/spacer.gif" width="3" height="1" border="0" alt=""/>
          <span style="font-size:11px; color:#4169E1;">at least the last 6 digits of the VIN is required</span>
        </td>
      </tr>

      <tr>
        <td>Line #:</td>
        <td>
          <IE:APDInputNumeric id="txtLineNo" name="txtLineNo" precision="2" scale="0" neg="false" int="true" required="true" canDirty="false" CCDisabled="false" CCTabIndex="25" onChange="" >
            <xsl:if test="$Debug=1"><xsl:attribute name="value">99</xsl:attribute></xsl:if>
          </IE:APDInputNumeric>
        </td>
      </tr>

      <tr valign="top" style="padding-top:4px;">
        <td colspan="2" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="90px"/>
              <col width="*"/>
              <col width="130px"/>
            </colgroup>
            <tr valign="top" style="height:96px;">
              <td>Coverages:</td>
              <td style="width:400px">
              </td>
              <td>
                <table border="0" cellspacing="0" cellpadding="1">
                  <tr>
                    <td>
                      <IE:APDButton id="btnAddCoverage" name="btnAddCoverage" value="Add New Coverage" width="120" CCDisabled="false" CCTabIndex="26" onButtonClick="addCoverage()" />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <IE:APDButton id="btnDelCoverage" name="btnDelCoverage" value="Remove Selected" width="120" CCDisabled="false" CCTabIndex="27" onButtonClick="delCoverage()" />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr valign="top" style="padding-top:4px;">
        <td colspan="2" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="90px"/>
              <col width="*"/>
            </colgroup>
            <tr valign="top">
              <td>ME Loss Details:</td>
              <td>
                <IE:APDTextArea id="txtLossDetails" name="txtLossDetails" maxLength="150" width="535" height="32" required="false" canDirty="false" CCDisabled="false" CCTabIndex="28" onChange="" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">Test ME Loss Details Test ME Loss Details Test ME Loss Details Test ME Loss Details Test ME Loss Details Test ME Loss Details Test ME Loss Details Tes</xsl:attribute></xsl:if>
                </IE:APDTextArea>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr style="padding-top:6px; height:26px;">
        <td colspan="2">
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="110px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td>
                <IE:APDCheckBox id="chkGlassDamage" name="chkGlassDamage" caption="Glass Damage" CCDisabled="false" required="false" CCTabIndex="29" alignment="1" canDirty="false" />
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr style="padding-top:5px;">
        <td colspan="2">
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="110px"/>
              <col width="*"/>
            </colgroup>
            <tr valign="top">
              <td>
                <IE:APDCheckBox id="chkAdditionalDamage" name="chkAdditionalDamage" caption="Body Damage" CCDisabled="false" required="false" CCTabIndex="30" alignment="1" canDirty="false" onchange="displayAddDamage()" />
              </td>
              <td>
                <IE:APDTextArea id="txtAdditionalDamageComments" name="txtAdditionalDamageComments" maxLength="200" width="510" height="48" required="false" canDirty="false" CCDisabled="true" CCTabIndex="31" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">Test Additional Damage Comments Test Additional Damage Comments Test Additional Damage Comments Test Additional Damage Comments Test Additional Damage Comments Test Additional Damage Comments Test Add</xsl:attribute></xsl:if>
                </IE:APDTextArea>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr valign="top" style="padding-top:4px;">
        <td colspan="2" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="110px"/>
              <col width="*"/>
            </colgroup>
            <tr valign="top">
              <td>Additional Comments:<br/><span style="font-size:7pt">(max. 300 characters)</span></td>
              <td>
                <IE:APDTextArea id="txtComments" name="txtComments" maxLength="300" width="514" height="64" required="false" canDirty="false" CCDisabled="false" CCTabIndex="32" onChange="" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test Comments Test C</xsl:attribute></xsl:if>
                </IE:APDTextArea>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

<!-- ======================================================================== ==
      COVERAGE POP-UP
  == ======================================================================== -->

  <div name="coveragePopUp" id="coveragePopUp"
    style="color:#000000;display:none;position:absolute;background-color:#FFFFFF;height:100px;width:300px;z-order:1;align:middle;left:280;top:245;">
    <table border="2" bordercolor="White" cellspacing="0" cellpadding="0" style="height:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF">
      <tr style="height:21px;text-align:center;font-weight:bold;background-color:#6495ed;color:#FFFFFF">
        <td>New Coverage</td>
      </tr>
      <tr valign="top">
        <td >
          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:100%">
            <tr valign="top" style="padding-top:4px;">
              <td width="88" align="right" style="padding-right:4px">Coverage Code:</td>
              <td>
                <IE:APDCustomSelect id="selCoverageClientCode" name="selCoverageClientCode" displayCount="4" blankFirst="false"
                    canDirty="false" CCDisabled="false" CCTabIndex="33" required="true" width="200" onchange="onChangeCoverageClientCode()">
                  <xsl:if test="$Debug=1"><xsl:attribute name="selectedIndex">0</xsl:attribute></xsl:if>
                  <xsl:for-each select="/Root/CoverageType">
                    <xsl:sort select="@AdditionalCoverageFlag" data-type="text" order="ascending"/>
                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:attribute>
                      <xsl:value-of select="@ClientCode"/>:<xsl:value-of select="@Name"/>
                    </IE:dropDownItem>
                  </xsl:for-each>
                </IE:APDCustomSelect>
              </td>
            </tr>
            <tr valign="top" style="padding-top:4px;">
              <td align="right" style="padding-right:4px">Deductible:</td>
              <td>
                <IE:APDInputCurrency id="txtDeductible" name="txtDeductible" width="" precision="7" scale="2" symbol="$" required="true" canDirty="false" CCDisabled="false" CCTabIndex="51" onChange="" >
                  <xsl:if test="$Debug=1"><xsl:attribute name="value">500</xsl:attribute></xsl:if>
                </IE:APDInputCurrency>
              </td>
            </tr>
            <tr valign="top">
              <td colspan="2">
                <div name="divCoverageNormal" id="divCoverageNormal">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr valign="top" style="padding-top:4px;">
                      <td width="88" align="right" style="padding-right:4px">Limit:</td>
                      <td>
                        <IE:APDInputCurrency id="txtLimit" name="txtLimit" width="" precision="7" scale="2" symbol="$" canDirty="false" CCDisabled="false" CCTabIndex="52" onChange="" >
                          <xsl:if test="$Debug=1"><xsl:attribute name="value">35500</xsl:attribute></xsl:if>
                        </IE:APDInputCurrency>
                      </td>
                    </tr>
                  </table>
                </div>
                <div name="divCoverageRental" id="divCoverageRental">
                  <table border="0" cellpadding="0" cellspacing="0" >
                    <tr valign="top" style="padding-top:4px;">
                      <td width="88" align="right" style="padding-right:4px">Rental Days:</td>
                      <td>
                        <IE:APDInputNumeric id="txtRentalDays" name="txtRentalDays" precision="2" scale="0" neg="false" int="true" canDirty="false" CCDisabled="false" CCTabIndex="53" onChange="" >
                          <xsl:if test="$Debug=1"><xsl:attribute name="value">3</xsl:attribute></xsl:if>
                        </IE:APDInputNumeric>
                      </td>
                    </tr>
                    <tr valign="top" style="padding-top:4px;">
                      <td align="right" style="padding-right:4px">Maximum Days:</td>
                      <td>
                        <IE:APDInputNumeric id="txtRentalMaxDays" name="txtRentalMaxDays" precision="2" scale="0" neg="false" int="true" canDirty="false" CCDisabled="false" CCTabIndex="54" onChange="" >
                          <xsl:if test="$Debug=1"><xsl:attribute name="value">10</xsl:attribute></xsl:if>
                        </IE:APDInputNumeric>
                      </td>
                    </tr>
                    <tr valign="top" style="padding-top:4px;">
                      <td align="right" style="padding-right:4px">Rental Maximum:</td>
                      <td>
                        <IE:APDInputCurrency id="txtRentalMaxVal" name="txtRentalMaxVal" width="" precision="7" scale="2" symbol="$" canDirty="false" CCDisabled="false" CCTabIndex="55" onChange="" >
                          <xsl:if test="$Debug=1"><xsl:attribute name="value">500</xsl:attribute></xsl:if>
                        </IE:APDInputCurrency>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
            <tr valign="top" align="right" style="padding-top:4px;padding-bottom:4px">
              <td> </td>
              <td align="right">
                <IE:APDButton id="btnCoverageCancel" name="btnCoverageCancel" value="Cancel" width="60"
                  CCDisabled="false" CCTabIndex="64" onButtonClick="onCoverageCancel()" />
                <IE:APDButton id="btnCoverageSave" name="btnCoverageSave" value="Save" width="60"
                  CCDisabled="false" CCTabIndex="63" onButtonClick="onCoverageSave()" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

</xsl:template>

<!-- ======================================================================== ==
      GLASS ASSIGNMENT TAB
  == ======================================================================== -->

<xsl:template name="GlassDetail">
  <table border="0" cellspacing="0" cellpadding="3" style="table-layout:fixed;border-collapse:collapse;width:100%">
    <colgroup>
      <col width="120"/>
      <col width="*"/>
    </colgroup>

    <tr>
      <td colspan="2" style="font:10pt Tahoma;font-weight:bold;color:#0000FF">Claim successfully submitted to APD.</td>
    </tr>
    <tr>
      <td style="font:11pt Tahoma;font-weight:bold">LYNX ID:</td>
      <td style="font:11pt Tahoma;font-weight:bold"><div id="glsSummaryLynxID"  name="glsSummaryLynxID"/></td>
    </tr>
    <tr>
      <td>Submitted on:</td>
      <td><div id="glsSummaryCreatedDate"  name="glsSummaryCreatedDate"/></td>
    </tr>
    <tr>
      <td>Claims Rep:</td>
      <td><div id="glsSummaryClaimsRep"  name="glsSummaryClaimsRep"/></td>
    </tr>
    <tr>
      <td>Phone:</td>
      <td><div id="glsSummaryClaimsRepPhone"  name="glsSummaryClaimsRepPhone"/></td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td><div id="glsSummaryClaimsRepEmail"  name="glsSummaryClaimsRepEmail"/></td>
    </tr>
    <tr>
      <td>Claim Number:</td>
      <td>
        <IE:APDInput id="txtClaimNumber2" name="txtClaimNumber2" size="12" maxLength="10"
            required="true" canDirty="false" CCDisabled="false" CCTabIndex="18"
            onfocusout="duplicateClaimCheck( this, true )">
        </IE:APDInput>
      </td>
    </tr>
    <tr>
      <td colspan="2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>

    <tr>
      <td valign="top">
        <IE:APDButton id="btnSubmitGlass" name="btnSubmitGlass" value="Send to Glass" width="100" CCDisabled="false" CCTabIndex="70" onButtonClick="submitGlassClaimToAGC()" />
      </td>
      <td>
        <table border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" >
          <tr>
            <td>
              <IE:APDTextArea id="txtGlassScript" name="txtGlassScript" width="495" height="130" required="false" canDirty="false" CCDisabled="true" CCTabIndex="71" />
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <IE:APDRadioGroup id="rbgGlassDispatched" name="rbgGlassDispatched" required="false"
            canDirty="false" CCDisabled="false">

          <IE:APDRadio id="rbGlassDispatched" name="rbGlassDispatched" caption="Glass Dispatched"
              value="1" groupName="rbgGlassDispatched" checkedValue="1" uncheckedValue="0" required="false"
              CCDisabled="false" CCTabIndex="72" onChange="setGlassDispatched( true )" />

        </IE:APDRadioGroup>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" border="1" cellspacing="0" cellpadding="3" style="border-collapse:collapse">
          <tr>
            <td>
              <table width="100%" >
                <colgroup>
                  <col width="90"/>
                  <col width="130"/>
                  <col width="90"/>
                  <col width="*"/>
                </colgroup>
                <tr>
                  <td>Dispatch Number:</td>
                  <td>
                    <IE:APDInputNumeric id="txtDispatch" name="txtDispatch" precision="12" scale="0" neg="false" int="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="73" onChange="" >
                      <xsl:if test="$Debug=1"><xsl:attribute name="value">543536789838</xsl:attribute></xsl:if>
                    </IE:APDInputNumeric>
                  </td>
                  <td>Glass Shop:</td>
                  <td>
                    <IE:APDInput id="txtShopName" name="txtShopName" size="30" maxLength="40" required="false" canDirty="false" CCDisabled="false" CCTabIndex="74" >
                      <xsl:if test="$Debug=1"><xsl:attribute name="value">Test Safelite Test Safelite Test Safelit</xsl:attribute></xsl:if>
                    </IE:APDInput>
                  </td>
                </tr>
                <tr>
                  <td>Shop Phone:</td>
                  <td>
                    <IE:APDInputPhone id="txtShopPhone" name="txtShopPhone" required="false" canDirty="false" showExtension="false" CCDisabled="false" CCTabIndex="75" onChange="" >
                      <xsl:if test="$Debug=1"><xsl:attribute name="value">4565455444</xsl:attribute></xsl:if>
                    </IE:APDInputPhone>
                  </td>
                  <td>Appointment:</td>
                  <td colspan="5">
                    <IE:APDInputDate id="txtAppointment" name="txtAppointment" type="datetime"
                      futureDate="true" required="false" canDirty="false" CCDisabled="false"
                      showSeconds="false" CCTabIndex="76" >
                      <!-- <xsl:if test="$Debug=1"><xsl:attribute name="value">05/10/2006</xsl:attribute></xsl:if> -->
                    </IE:APDInputDate>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <IE:APDRadio id="rbGlassDispatched" name="rbGlassDispatched" caption="No Glass Dispatched"
          value="1" groupName="rbgGlassDispatched" checkedValue="0" uncheckedValue="1" required="false"
          CCDisabled="false" CCTabIndex="77" onChange="setGlassDispatched( false )" />
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" border="1" cellspacing="0" cellpadding="3" style="border-collapse:collapse">
          <tr>
            <td>
              <table width="100%" >
                <colgroup>
                  <col width="90"/>
                  <col width="130"/>
                  <col width="90"/>
                  <col width="*"/>
                </colgroup>
                <tr>
                  <td valign="top">No Glass Dispatched Reason:</td>
                  <td valign="top" colspan="3">
                    <IE:APDTextArea id="txtNoDispatchReason" name="txtNoDispatchReason" maxLength="100" width="380" height="32" required="false" canDirty="false" CCDisabled="false" CCTabIndex="78" onChange="" >
                      <xsl:if test="$Debug=1"><xsl:attribute name="value">Test No Glass Dispatched Reason Test No Glass Dispatched Reason Test No Glass Dispatched Reason Test</xsl:attribute></xsl:if>
                    </IE:APDTextArea>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

  </table>
</xsl:template>

<!-- ======================================================================== ==
      CLAIM SUMMARY TAB
  == ======================================================================== -->

<xsl:template name="SummaryDetail">
  <table border="0" cellpadding="2" cellspacing="0">
    <colgroup>
      <col width="100px" style="font-weight:bold"/>
      <col width="400px"/>
    </colgroup>
    <tr>
      <td colspan="2"><div style="padding:0px;margin:0px;font:14pt Tahoma;color:#0000FF;border:0px;border-bottom:1px solid #C0C0C0">Mobile Electronics Claim Submission Result</div></td>
    </tr>
    <tr>
      <td colspan="2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="font:10pt Tahoma;font-weight:bold;color:#0000FF">Claim successfully submitted.</td>
    </tr>
    <tr>
      <td style="font:11pt Tahoma;font-weight:bold">LYNX ID:</td>
      <td style="font:11pt Tahoma;font-weight:bold"><div id="clmSummaryLynxID"  name="clmSummaryLynxID"/></td>
    </tr>
    <tr>
      <td>Submitted on:</td>
      <td><div id="clmSummaryCreatedDate"  name="clmSummaryCreatedDate"/></td>
    </tr>
    <tr>
      <td>Claims Rep:</td>
      <td><div id="clmSummaryClaimsRep"  name="clmSummaryClaimsRep"/></td>
    </tr>
    <tr>
      <td>Phone:</td>
      <td><div id="clmSummaryClaimsRepPhone"  name="clmSummaryClaimsRepPhone"/></td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td><div id="clmSummaryClaimsRepEmail"  name="clmSummaryClaimsRepEmail"/></td>
    </tr>
    <tr>
      <td colspan="2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
        <DIV class="div_popup" id="ScriptingAll" style="font-family:'Times New Roman', Times, serif; font-size:13px; font-weight:normal; background-color: #FFFFE1; color: #000000; border: 1px solid #0B198C; padding: 4px; FILTER: progid:DXImageTransform.Microsoft.shadow(direction=135,color=#8E8E8E,strength=3); ">
        </DIV>
      </td>
    </tr>
  </table>
</xsl:template>

</xsl:stylesheet>
