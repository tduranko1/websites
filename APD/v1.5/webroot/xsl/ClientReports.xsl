<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimBilling">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="View Reports"/>

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R')">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
          <tr>
            <td align="center">
              <font color="#ff0000"><strong>You do not have sufficient permission to view the Client Reports.
              <br/>Please contact administrator for permissions.</strong></font>
            </td>
          </tr>
        </table>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="LynxId"/>
<xsl:param name="ClaimStatus"/>
<xsl:param name="InfoCRUD"/>
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimBillingGetDetailXML,ClaimBilling.xsl, 6790   -->
<HEAD>

<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,6);
		  event.returnValue=false;
		  };	
 </script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  var bReportLoaded = false;
  var iShowCoverageType = null;
  var iShowOffice = null;
  var iShowServiceChannel = null;
  var iShowShowAllClients = null;
  var iShowShowUserDetails = null;
  var iShowState = null;
  var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
  var sReportName = "";
  var sState = "";
  var sDate = "";
  var sInsName = "";
  var sReportFriendlyName = "";
  var sShowAllClients = "1"; // 1 = Show All clients; 0 = Do not show all clients
  var iExtBrowser = null;
    
  <![CDATA[
    function initPage(){
      try {
        stat1.style.top = (document.body.offsetHeight - 75) / 2;
        stat1.style.left = (document.body.offsetWidth - 240) / 2;
        //top.closeClaim();
      } catch (e) {
        ClientError(e.description);
      }
    }

    function getReport(){
      try {
        if (csReports.selectedIndex == -1) {
          ClientWarning("Please select the report you would like to view.");
          return;
        }
        if (csInsurance.selectedIndex == -1){
          ClientWarning("Please select the Insurance Company for which you would like to view the report.");
          return;
        }
        if (txtYear.value == ""){
          ClientWarning("Please enter a year value.");
          return;
        }
        ifrmReport.frameElement.style.display = "none";
        stat1.Show("Preparing report. Please wait...");
        window.setTimeout("getReport2()", 100);
      } catch(e) {
        ClientError(e.description)
      }
    }

    function getCriteria(){
      try {
        if (csReports.selectedIndex != -1 && csInsurance.selectedIndex != -1) {
          ifrmReportCriteria.frameElement.style.display = "none";
          stat1.Show("Loading report criteria. Please wait...");
          bReportLoaded = true;
          frmGetReportCriteria.reportID.value = csReports.value;
          frmGetReportCriteria.InsuranceCompanyID.value = (csInsurance.selectedIndex != -1 ? csInsurance.value : "");
          frmGetReportCriteria.submit();
        }
        //window.setTimeout("getCriteria2()", 100);
      } catch(e) {
        ClientError(e.description);
      }
    }
    
    function reportReady(){
      try {
        if (bReportLoaded) {
          if (ifrmReportCriteria.frameElement.readyState == "complete") {
            ifrmReportCriteria.frameElement.style.display = "inline";
            stat1.Hide();
          }
        }
      } catch (e) {
        ClientError(e.description)
      }
    }
 
 	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
 
  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:#FFFFFF; border:0px; overflow:hidden;margin:2px;padding:0px;" onLoad="initPage()" tabIndex="-1">
  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;width:100%;">
  <colgroup>
    <col width="*"/>
    <col width="175px"/>
  </colgroup>
  <tr valign="bottom">
  <td>
  <table border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td colspan="2" style="font-size:18px;">Client Reports</td>
    </tr>
    <tr>
      <td colspan="2"><img src="/images/background_top.gif" height="2px"/></td>
    </tr>
    <tr>
      <td>Report Name:</td>
      <td>
        <IE:APDCustomSelect id="csReports" name="csReports" displayCount="7" canDirty="false" required="true" CCTabIndex="2" onChange="getCriteria()">
          <xsl:for-each select="Report">
            <xsl:sort select="@DisplayOrder" data-type="number" order="ascending"/> 
            <IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ClientReportID"/></xsl:attribute>
            <xsl:value-of select="@Description"/>
          </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr name="rowInsurance" id="rowInsurance">
      <td>Insurance Company:</td>
      <td>
        <IE:APDCustomSelect id="csInsurance" name="csInsurance" displayCount="6" canDirty="false" required="true" CCTabIndex="1" onChange="getCriteria()">
          <xsl:for-each select="/Root/Reference[@ListName='InsuranceCompany']">
            <IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ListCode"/></xsl:attribute>
            <xsl:value-of select="@ListValue"/>
          </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <!-- <tr>
      <td>Month:</td>
      <td>
        <IE:APDCustomSelect id="csMonth" name="csMonth" displayCount="6" canDirty="false" CCTabIndex="3" onChange="">
            <IE:dropDownItem style="display:none" value="1">January</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="2">February</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="3">March</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="4">April</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="5">May</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="6">June</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="7">July</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="8">August</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="9">September</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="10">October</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="11">November</IE:dropDownItem>
            <IE:dropDownItem style="display:none" value="12">December</IE:dropDownItem>
        </IE:APDCustomSelect>                
      </td>
    </tr>
    <tr>
      <td>Year:</td>
      <td>
        <IE:APDInputNumeric id="txtYear" name="txtYear" precision="4" scale="0" neg="false" int="true" min="2000" max="3000" required="false" canDirty="false" CCTabIndex="4" />
      </td>
    </tr>
    <tr name="rowState" id="rowState">
      <td>State:</td>
      <td>
        <IE:APDCustomSelect id="csState" name="csState" displayCount="6" canDirty="false" CCTabIndex="2" width="125" blankFirst="true" onChange="">
            <IE:dropDownItem style="display:none" value="">Contracted States</IE:dropDownItem>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr name="rowServiceChannel" id="rowServiceChannel">
      <td>Service Channel:</td>
      <td>
        <IE:APDCustomSelect id="csServiceChannel" name="csServiceChannel" displayCount="6" canDirty="false" CCTabIndex="3" blankFirst="true" onChange="">
          <xsl:for-each select="/Root/Reference[@ListName='Service Channel']">
            <IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ListCode"/></xsl:attribute>
            <xsl:value-of select="@ListValue"/>
            </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr name="rowCoverageType" id="rowCoverageType">
      <td>Coverage Type:</td>
      <td>
        <IE:APDCustomSelect id="csCoverageType" name="csCoverageType" displayCount="6" canDirty="false" CCTabIndex="4" blankFirst="true" onChange="">
          <xsl:for-each select="/Root/Reference[@ListName='Coverage Type']">
            <IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ListCode"/></xsl:attribute>
            <xsl:value-of select="@ListValue"/>
            </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr name="rowOffice" id="rowOffice">
      <td>Office:</td>
      <td>
        <IE:APDCustomSelect id="csOffice" name="csOffice" displayCount="6" canDirty="false" CCTabIndex="5" width="150" blankFirst="true" onChange="">
            <IE:dropDownItem style="display:none" value="">All Offices</IE:dropDownItem>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr name="rowUser" id="rowUser">
      <td>Carrier Representative:</td>
      <td>
        <IE:APDCustomSelect id="csUser" name="csUser" displayCount="6" canDirty="false" CCTabIndex="6" blankFirst="true" onChange="">
            <IE:dropDownItem style="display:none" value="">All Representatives</IE:dropDownItem>
        </IE:APDCustomSelect>
      </td>
    </tr> -->
  </table>
  </td>
  <td valign="bottom">
    <!-- <table border="0" cellspacing="0" cellpadding="2">
      <tr name="rowShowAllUsers" id="rowShowAllUsers">
        <td>  
          <IE:APDCheckBox id="chkShowUsers" name="chkShowUsers" caption='Show User Details' value="1" CCTabIndex="-1" canDirty="false" />
          <br/>
        </td>
      </tr>
      <tr name="rowShowAllClients" id="rowShowAllClients">
        <td>  
          <IE:APDCheckBox id="chkAllClients" name="chkAllClients" caption='Show "All Clients" section' value="1" CCTabIndex="-1" canDirty="false" />
          <br/>
        </td>
      </tr>
      <tr>
        <td>          
          <IE:APDCheckBox id="chkOpenExternal" name="chkOpenExternal" caption="View Report in separate window" value="0" CCTabIndex="-1" canDirty="false" />
          <br/>
        </td>
      </tr>
      <tr>
        <td>          
          <IE:APDButton id="btnGetReport" name="btnGetReport" value="View Report" CCTabIndex="4" CCDisabled="true" onButtonClick="getReport()"/>
        </td>
      </tr>
    </table> -->
  </td>
  </tr>
  </table>
  <br/>
  <IFRAME id="ifrmReportCriteria" name="ifrmReportCriteria" src="/blank.asp?ReportCategory=ClientReports" onreadyStateChange="reportReady()" style="position:absolute;top:80px;width:100%; height:430px; border:0px solid #000000; position:absolute; background:transparent; overflow:hidden;" allowtransparency="true" >
  </IFRAME>
  <!-- <IFRAME id="ifrmReport" name="ifrmReport" src="/blank.asp" onreadyStateChange="reportReady()" style="position:absolute;top:230px;width:740px; height:280px; border:1px solid #000000; position:absolute; background:transparent; overflow:hidden;" allowtransparency="true" >
  </IFRAME> -->

  <xml id="xmlReports" name="xmlReports">
    <xsl:copy-of select="/"/>
  </xml>
  <form name="frmGetReportCriteria" id="frmGetReportCriteria" method="post" target="ifrmReportCriteria" action="/reports/ReportCriteria.asp">
    <input type="hidden" name="reportID" id="reportID"/>
    <input type="hidden" name="InsuranceCompanyID" id="InsuranceCompanyID"/>
	<input type="hidden" name="ReportCategory" id="ReportCategory" value="ClientReports"/>
  </form>
  <!-- <form name="frmGetReport" id="frmGetReport" method="post" target="ifrmReport" action="/reports/getReport.asp">
    <input type="hidden" name="reportFileName" id="reportFileName"/>
    <input type="hidden" name="storedProc" id="storedProc"/>
    <input type="hidden" name="staticParams" id="staticParams"/>
    <input type="hidden" name="reportParams" id="reportParams"/>
    <input type="hidden" name="friendlyName" id="friendlyName"/>
  </form> -->
  <IE:APDStatus id="stat1" name="stat1" width="250" height="75" />
</BODY>
</HTML>

</xsl:template>
</xsl:stylesheet>
