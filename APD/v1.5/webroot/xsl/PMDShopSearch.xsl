<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="http://local.com/mynamespace"
    id="PMDShopSearch">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:decimal-format NaN="0"/>

<xsl:param name="ShopCRUD" select="Shop"/>

<msxsl:script language="JScript" implements-prefix="js">
<![CDATA[
    var colInsCo = new ActiveXObject("Scripting.Dictionary");

    function addInsCo(sInsCo){
      if (colInsCo.exists(sInsCo) == false){
        colInsCo.Add(sInsCo, sInsCo);
      }
      return null;
    }
    
    function existsInsCo(sInsCo){
      var bRet = colInsCo.exists(sInsCo)
      if (bRet == false){
        addInsCo(sInsCo);
      }
      return bRet;
    }
    

    function clearInsCo(sInsCo){
      colInsCo.RemoveAll();
      return "";
    }
    
]]>
</msxsl:script>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDMarketAnalysisGetListXML,PMDMarketAnalysis.xsl,35   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>
  
  <!-- CLIENT SCRIPTS -->
<!--   <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT> -->
  <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
  
  <SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
            document.onhelp=function(){  
  		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,23);
  		  event.returnValue=false;
  		  };	
  </script>
  
  
  <!-- Page Specific Scripting -->
  <SCRIPT language="JavaScript">
  var gsProgramManagerUserID = '<xsl:value-of select="@ProgramManagerUserID"/>';
  var bDataAvailable = false;
  var recCount = 0;
  var iCurPage = 0;
  var iPgCount = 0;
  var iPageSize = 10;
  var bMouseInToolTip = false;
  var oPrevCol = null;
  var oCurRow = null;
  var oShopPopup = window.createPopup();
  var gsShopCRUD = "<xsl:value-of select="$ShopCRUD"/>";
  <![CDATA[

  //init the table selections, must be last
  function initPage(){
  	divShopList.scrollTop = parent.gsMAScroll;
    btnFirst.value = unescape("%u2039") + unescape("%u2039");
    btnPrev.value = unescape("%u2039");
    btnNext.value = unescape("%u203A");
    btnLast.value = unescape("%u203A") + unescape("%u203A");
    
    divFilter.style.visibility = "";
    divFilter.style.top = document.body.offsetHeight - divFilter.offsetHeight - divPages.offsetHeight - 10;
    divFilter.style.left = (document.body.offsetWidth - divFilter.offsetWidth) / 2;
    divFilter.style.display = "none";
    if (typeof(parent.hideProgress) == "function")
      parent.hideProgress();
  }
  
  //update the selected elements from the table row
  function GridSelect(oRow){
      var srcElement = event.srcElement;
      var iCount = 10;
      while (srcElement.tagName != "TR" && iCount > 0)
        srcElement = srcElement.parentElement;
        
      var oTR = srcElement;
      var iNumber = parseInt(oTR.cells[0].innerText, 10);
      
      if (iNumber > 0) {
        var sShopLocationID;
        var sShopID;
        if (xmlSearchData){
          var oShopNode = xmlSearchData.selectSingleNode("/Root/Shop[@Number=" + iNumber + "]");
          if (oShopNode) {
            sShopLocationID = oShopNode.getAttribute("ShopLocationID");
            
            NavToPMDDetail(sShopLocationID);
          }
        }
      }
      event.cancelBubble = true;
      event.returnValue = false;
    //parent.window.navigate("PMDShopLevel.asp?ShopLocationID=" + strShopLocationID + "&DistrictManagerUserID=" + parent.selDistrictMgr.value + "&ProgramManagerUserID=" + gsProgramManagerUserID + "&Scroll=" + divShopList.scrollTop);
  }
  
  function setTableSource(oXML){
    try {
    if (oXML) {
      oXML.setProperty("SelectionLanguage", "XPath");
      recCount = oXML.XMLDocument.firstChild.childNodes.length;
      tblSort1.dataSrc = "#" + oXML.id;
      bDataAvailable = true;
      tblSort1.onreadystatechange = tblDataReadyStateChange;
      if (recCount > 0) {
        divShopList.style.display = "inline";
        divPages.style.display = "inline";
        divNoData.style.display = "none";
        iCurPage = 1;
        iPgCount = Math.ceil(recCount / iPageSize);
        txtPage.value = iCurPage + " of " + iPgCount;
        disablePageNav(false);
      } else {
        if (oXML.id == "xmlSearchFiltered") {
          divNoData.innerHTML = "Filter resulted in 0 records.";
        } else {
          divPages.style.display = "none";
          divNoData.innerHTML = "No data to display.";
        }
        txtPage.value = "0 of 0";
        divShopList.style.display = "none";
        divNoData.style.display = "inline";
        disablePageNav(true);
      }
    }
    } catch(e) {alert(e.description);}
  }
  
  function disablePageNav(bDisabled){
    btnFirst.disabled = btnPrev.disabled = btnNext.disabled = btnLast.disabled = txtPage.disabled = btnGoPage.disabled = bDisabled;
  }
  
  function tblDataReadyStateChange(){
    if (!bDataAvailable) return;
    if (tblSort1) {
      //alert(tblSort1.tBodies[0].rows.length);
      var oRows = tblSort1.rows;
      var SupplementOnly10, FinalOnly10, SupplementOnlyOver10, FinalOnlyOver10;
      if (oRows) {
        for (var i = 0; i < oRows.length; i++) {
          if (oRows[i] && oRows[i].cells) {
            oRows[i].style.backgroundColor = (i % 2 ? "#fff7e5" : "#ffffff");
          }
        }
      }
    }
  }

    function sortTable(oTD, sTable, sSortFldName, sDataType) {
      if (recCount < 1) return;
        
      var sSortOrder = (oTD ? oTD.getAttribute("sortOrder") : "ascending");
      var oXML = null;
      
      if (sTable != "" ) {
        oTbl = document.getElementById(sTable);
        if (oTbl) {
          sXMLSrc = oTbl.dataSrc;
          sXMLSrc = sXMLSrc.substr(1);
        }
      } else
        return;

      if (sXMLSrc != "")
        oXML = document.getElementById(sXMLSrc);
      
        
      if (sSortFldName != "" && oXML && oTbl) {

          bSorting = true;
          
          var sFldName = sSortFldName;
          if (sSortOrder == "ascending")
             sSortOrder = "descending";
          else
             sSortOrder = "ascending";
             
          if (sSortFldName == "DefaultOrder")
            sSortOrder = "ascending"
        
          var sColumnType = (sDataType == "number" ? "number" : "text");
        
          var iCount = 0;
          var objTmp = null;
          if (sFldName) {
                var sDataSrc = sXMLSrc;
                var objXML = oXML; 
                if (objXML) {
        
                   var sFirstNode = objXML.XMLDocument.documentElement.firstChild.nodeName;
        
                   oTbl.dataSrc = "";
                   var objXSL = new ActiveXObject("Msxml2.DOMDocument");
                   if (objXSL) {
                      objXSL.async = false;
        
                      var sXSL = '<?xml version="1.0" encoding="UTF-8"?>' +
                                  '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
        
                                  '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>' +
        
                                  '<xsl:template match="/Root">' +
                                     '<xsl:element name="Root">' +
                                     '<xsl:apply-templates select="' + sFirstNode + '">' +
                                        '<xsl:sort select="@' + sFldName + '" data-type="' + sColumnType + '" ' +
                                        (sColumnType == 'text' ? 'case-order="lower-first"' : '') +
                                        ' order="' + sSortOrder + '" />' +
                                     '</xsl:apply-templates>' +
                                     '</xsl:element>' +
                                  '</xsl:template>' +
        
                                  '<xsl:template match="' + sFirstNode + '">' +
                                     '<xsl:copy-of select="."/>' +
                                  '</xsl:template>' +
                                  '</xsl:stylesheet>';
                      //window.clipboardData.setData("Text", sXSL);
                      objXSL.loadXML(sXSL);
        
                      //oXML.loadXML(oXML.transformNode(objXSL));
                      oXML.transformNodeToObject(objXSL, oXML);
                      oTbl.dataSrc = "#" + sDataSrc;
        
                      //reset the page.
                      iCurPage = 1;

                      //display the sort icon
                      divSort.style.display = "inline";
                      
                      txtPage.value = iCurPage + " of " + iPgCount;

                      if (sSortOrder == "ascending")
                        divSort.firstChild.src = "/images/ascending.gif";
                      else
                        divSort.firstChild.src = "/images/descending.gif";
                        
                      if (oTD) {
                        var x = oTD.offsetLeft + oTD.offsetWidth - divSort.offsetWidth;
                        var y = ((oTD.offsetTop + oTD.offsetHeight) - divSort.offsetHeight) / 2 + oTD.offsetTop + 3;
                        divSort.style.left = x - 3;
                        divSort.style.top = y;
                        
                        oTD.setAttribute("sortOrder", sSortOrder);
                      }
                      bSorting = false;
                   }
                }
          }
      }
    }

    function showPage(sVal) {
      var bPageMoved = false;
      if (tblSort1 && tblSort1.dataSrc != "") {
        switch(sVal) {
          case -1: //move to first page
            if (iCurPage > 1) {
              tblSort1.firstPage();
              iCurPage = 1;
              bPageMoved = true;
            }
            break;
          case -2: //move to previous page
            if (iCurPage > 1) {
              tblSort1.previousPage();
              iCurPage--;
              bPageMoved = true;
            }
            break;
          case -3: //move to next page
            if (iCurPage < iPgCount) {
              tblSort1.nextPage();
              iCurPage++;
              bPageMoved = true;
            }
            break;
          case -4: //move to last page
            if (iCurPage < iPgCount) {
              for (var i = 0; i < (iPgCount - iCurPage); i++) {
                tblSort1.nextPage();
              }
              
              iCurPage = iPgCount;
              bPageMoved = true;
            }
            break;
          default: //move to any page
            var iPgDiff = parseInt(sVal, 10) - iCurPage;
            if (iPgDiff > 0) {
              for (var i = 0; i < iPgDiff; i++)
                 tblSort1.nextPage();
              bPageMoved = true;
            } else {
              iPgDiff *= -1;
              for (var i = 0; i < iPgDiff; i++)
                 tblSort1.previousPage();
              bPageMoved = true;
            }
            iCurPage = parseInt(sVal, 10);
            break;
        }

        if (iCurPage < iPgCount)
          iRecInCurPage = iPageSize;
        else
          iRecInCurPage = (recCount - (iPgCount - 1) * iPageSize);
          
        iTaskRefreshedTryCount = 1;
        if (bPageMoved == true) {
          if (iCurPage > iPgCount) iCurPage = iPgCount;
          txtPage.value = iCurPage + " of " + iPgCount;
        }
      }
    }

    function gotoPage(){
      var iPage;
      var sPageTxt = txtPage.value;
      if (sPageTxt.indexOf("of") > 0) {
        sPageTxt = sPageTxt.substr(0, sPageTxt.indexOf("of"));
      }
      
      if (isNaN(sPageTxt)){
        alert("Invalid page number specified. Expecting a numeric value for page number.");
        return;
      }
      
      iPage = parseInt(sPageTxt, 10);

      if (iPage <= 0) iPage = 1;
      if (iPage > iPgCount) iPage = iPgCount;
      if (iPage != iCurPage)
        showPage(iPage);
      else
        txtPage.value = iCurPage + " of " + iPgCount;
    }
    
    function doPageScroll(){
      if (event.wheelDelta >= 120)
        showPage(-2);
      else if (event.wheelDelta <= -120)
        showPage(-3);   
    }
    
    function showFilter(){
      divFilter.style.display = "inline";
      txtShopName.focus();
    }
    
    function hideFilter(){
      divFilter.style.display = "none";
    }
    
    function doFilter(){
      var sShopName = "";
      var sCity = "";
      var sStateCD = "";
      var sStateName = "";
      var sZip = "";
      var oFilteredXML, oXML;
      var sFilterDesc = "";
      
      sShopName = txtShopName.value.Trim();
      sCity = txtCity.value.Trim();
      
      if (selState.selectedIndex != -1) {
        sStateCD = selState.value.Trim();
        sStateName = selState.options[selState.selectedIndex].innerText;
      }
      
      var aCriteria = new Array();
      var sCriteria = "";

      if (sShopName != ""){
        sShopName = sShopName.replace(/[\']/g, "");
        aCriteria.push("contains(@ShopName, '" + sShopName + "')");
        sFilterDesc = "Shop Name='" + txtShopName.value.Trim() + "'";
      }

      if (sCity != ""){
        sCity = sCity.replace(/[\']/g, "");
        aCriteria.push("contains(@ShopCity, '" + sCity + "')");
        sFilterDesc = "City='" + sCity + "'";
      }

      if (sStateCD != "") {
        aCriteria.push("(contains(@ShopState, '" + sStateCD.toLowerCase() + "') or contains(@ShopState, '" + sStateName.toLowerCase() + "'))");
        sFilterDesc += (sFilterDesc != "" ? " and " : "") + "State='" + sStateName + "'";
      }

      sCriteria = aCriteria.join(" and ");
      
      if (sCriteria == "") {
        clearFilter(); return;
      } else
        sCriteria = "/Root/ShopLocation[" + sCriteria + "]";
      
      //alert(sCriteria);
      //return;
      
      oXML = document.getElementById("xmlSearchData");
      oFilteredXML = document.getElementById("xmlSearchFiltered");

      //tblSort1.dataSrc = "";
      try {
      var objXSL = new ActiveXObject("Msxml2.DOMDocument");
      if (objXSL) {
        objXSL.async = false;
        
        var sXSL = '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">\n' +
        
                    '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>\n' +
        
                    '<xsl:template match="/Root">\n' +
                       '<xsl:element name="Root">\n' +
                       '<xsl:apply-templates select="' + sCriteria + '">\n' +
                       '</xsl:apply-templates>\n' +
                       '</xsl:element>\n' +
                    '</xsl:template>\n' +
        
                    '<xsl:template match="ShopLocation">\n' +
                       '<xsl:copy-of select="."/>\n' +
                    '</xsl:template>\n' +
                    '</xsl:stylesheet>';
        objXSL.loadXML(sXSL);
        if (objXSL.parseError.errorCode == 0) {
          oXML.transformNodeToObject(objXSL, oFilteredXML);
          setTableSource(oFilteredXML)
          spnFilter.innerHTML = "<b>Filter:</b>&nbsp;" + sFilterDesc;
          btnClearFilter.style.display = "inline";
        }
      }
      } catch (e) {alert(e.description)}
      hideFilter();
    }
    
    function clearFilter() {
      setTableSource(xmlSearchData);
      txtCity.value = "";
      selState.selectedIndex = -1;
      btnClearFilter.style.display = "none";
      spnFilter.innerHTML = "";
      hideFilter();
    }
    
    function openShop(oTR){
      try {
      event.cancelBubble = true;
      event.returnValue = false;
      } catch (e) {}
      if (gsShopCRUD.indexOf('R') == -1){
        ClientWarning("You do not have permission to view Shop Profile. Please contact your supervisor.");
        return;
      }
      var sShopLocationID;
      var sShopID = "";
      sShopLocationID = parseInt(oTR.cells[10].innerText, 10);
      
      NavToShop('S', sShopLocationID, sShopID);
    }
    
    function showAssignments(obj, iDays){
      if (obj && parseInt(obj.innerText, 10) > 0 ){
        if (oPrevCol === obj) return;
        var iShopLocationID = obj.parentElement.cells[10].innerText;
        
        tblToolTipSummary.dataSrc = "#xml_" + iShopLocationID + "_" + iDays + "_Summary";
        tblToolTipRepair.dataSrc = "#xml_" + iShopLocationID + "_" + iDays + "_Repair";
        tblToolTipCashOut.dataSrc = "#xml_" + iShopLocationID + "_" + iDays + "_CashOut";
        tblToolTipTotalLoss.dataSrc = "#xml_" + iShopLocationID + "_" + iDays + "_TotalLoss";
        
        tblToolTip.style.height = "";
        toolTip.style.height = "";
        
        toolTip.style.display = "inline";
        var iLeft = iTop = 0;
        if ((event.y + toolTip.offsetHeight) > divShopList.offsetHeight)
          iTop = event.y - toolTip.offsetHeight + tblHead.offsetHeight;
        else
          iTop = event.y + 5;
        iLeft = obj.offsetLeft - toolTip.offsetWidth + 10;//event.x - toolTip.offsetWidth - 5;
        
        toolTip.style.top = (iTop > 15 ? iTop : 15);
        toolTip.style.left = iLeft;

        
        bMouseInToolTip = true;
        oPrevCol = obj;
        
        window.setTimeout("checkToolTipSize()", 250);
      } else {
        bMouseInToolTip = false;
        hideAssignments();
      }
    }
    
    function hideAssignments(){
      if (bMouseInToolTip == false) {
        toolTip.style.display = "none";
        bMouseInToolTip = false;
        oPrevCol = null;
      }
    }

    function keepToolTip(){
      bMouseInToolTip = true;
    }
    
    function checkToolTipSize(){
      if (document.body.offsetHeight - (toolTip.offsetTop + toolTip.offsetHeight) < 0) {
        //var iHeight = document.body.offsetHeight - toolTip.offsetTop - 10;
        var iHeight = divShopList.offsetHeight - toolTip.offsetTop;
        toolTip.style.height = iHeight;
        tblToolTip.style.height = "100%";
      }
    }
    
    function showShopContext(obj){
      oCurRow = obj.parentElement;
      var lefter = event.offsetX;
      var topper = event.offsetY;
      event.returnValue = false;
      event.cancelBubble = true;
      oShopPopup.document.body.innerHTML = divContextMenu.innerHTML;
      oShopPopup.show(lefter, topper, 153, 43, obj);
    }
    
    function openAssignments(oTR){
      try {
      event.cancelBubble = true;
      event.returnValue = false;
      } catch (e) {}
      var sShopLocationID;
      sShopLocationID = parseInt(oTR.cells[10].innerText, 10);
      
      NavToPMDDetail(sShopLocationID);
    }
    
    function openAllAssignments(){
      var sShopLocationID;
      var sShopID;
      if (oCurRow){
        openAssignments(oCurRow);
      }
    }

    function openShopProfile(){
      var sShopLocationID;
      var sShopID;
      if (gsShopCRUD.indexOf('R') == -1){
        ClientWarning("You do not have permission to view Shop Profile. Please contact your supervisor.");
        return;
      }
      if (oCurRow){
        oShopPopup.hide();
        openShop(oCurRow);
      }
    }
    
  ]]>
  </SCRIPT>

  <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
  
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="backgro1und:transparent;overflow:hidden" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;" onmouseenter="bMouseInToolTip=false;hideAssignments()">
  <colgroup>
    <col width="282px"/>
    <col width="140px"/>
    <col width="59px"/>
    <col width="180px"/>
    <col width="62px"/>
    <col width="62px"/>
    <col width="62px"/>
    <col width="62px"/>
    <col width="69px"/>
    <col width="31px"/>
  </colgroup>
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="2"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'ShopName', 'text')"> Shop Name </TD>
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="3"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'ShopCity', 'text')"> City </TD>
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="4"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'ShopState', 'text')"> State </TD>
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="4"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'ProgramManager', 'text')"> Program Manager </TD>
      <TD colspan="4" unselectable="on" class="TableSortHeader" > Active Assignments w/in </TD>
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="Date" sIndex="9"  onclick="sortTable(this, 'tblSort1', 'LastREIDateString', 'text')"> Last ReI </TD>
      <TD rowspan="2" unselectable="on" class="TableSortHeader" type="Number" sIndex="10" > ReI  Req</TD>
	  </TR>    
    <TR>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="6"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'AssignCountPast7', 'number')">7 Days</TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="6"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'AssignCountPast30', 'number')">30 Days</TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="6"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'AssignCountPast90', 'number')">90 Days</TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="6"  nowrap="nowrap" onclick="sortTable(this, 'tblSort1', 'AssignCountPast365', 'number')">365 Days</TD>
    </TR>
  </TBODY>
</TABLE>
<div id="divSort" name="divSort" style="position:absolute;top:0px;left:0px;display:none"><img src="/images/ascending.gif"/></div>
<div id="divNoData" name="divNoData" class="TableData" style="display:none;height:188px;border:0px;width:100%;padding:10px;text-align:center;color:#FF0000;font-weight:bold">No data to display.</div>
<div id="divContextMenu" style="position:absolute;top:15;left:15;display:none">
  <div style="width:150px;border:1px solid #000000;background-color:#FFFFFF;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
    <table border="0" cellpadding="3" cellspacing="0" style="font:11px Tahoma, Arial;width:100%;cursor:hand;background-color:#FFFFFF">
      <tr>
        <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.openAllAssignments()">Display all Assignments</td>
      </tr>
      <tr>
        <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.openShopProfile()">Open Shop Profile</td>
      </tr>
    </table>
  </div>
</div>
<DIV unselectable="on" id="divShopList" class="autoflowTable" style="height:202px;border:0px solid #FF0000;overflow:hidden" onmousewheel="doPageScroll()" onmouseenter="bMouseInToolTip=false;hideAssignments()">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;border:1px solid outset;table-layout:fixed;" dataPageSize="10">
    <colgroup>
      <col width="279"/>
      <col width="139"/>
      <col width="58" style="text-align:center"/>
      <col width="180"/>
      <col width="61" style="text-align:center"/>
      <col width="61" style="text-align:center"/>
      <col width="61" style="text-align:center"/>
      <col width="61" style="text-align:center"/>
      <col width="68" style="text-align:center"/>
      <col width="28" style="text-align:center"/>
      <col style="display:none"/>
    </colgroup>
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <!-- <xsl:for-each select="Shop"><xsl:call-template name="ShopListInfo"/></xsl:for-each> -->
      <tr unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" style="height:19px;" ondblclick="openAssignments(this)">
        <td unselectable="on" ondblclick="event.returnValue=false;event.cancelBubble=true;" oncontextmenu="showShopContext(this)">
          <span unselectable="on" datafld="ShopName" class="spanNoWrap">
            <xsl:if test="contains($ShopCRUD, 'R') = true()">
              <xsl:attribute name="style">color:#0000FF</xsl:attribute>
              <xsl:attribute name="onClick">openShop(this.parentElement.parentElement)</xsl:attribute>
            </xsl:if>
          </span>
        </td>
        <td unselectable="on"><span unselectable="on" datafld="ShopCity" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="ShopState" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="ProgramManager" class="spanNoWrap"/></td>
        <td unselectable="on" onmouseenter="showAssignments(this, 7)" onmouseleave="hideAssignments()"><span unselectable="on" datafld="AssignCountPast7" class="spanNoWrap"/></td>
        <td unselectable="on" onmouseenter="showAssignments(this, 30)" onmouseleave="hideAssignments()"><span unselectable="on" datafld="AssignCountPast30" class="spanNoWrap"/></td>
        <td unselectable="on" onmouseenter="showAssignments(this, 90)" onmouseleave="hideAssignments()"><span unselectable="on" datafld="AssignCountPast90" class="spanNoWrap"/></td>
        <td unselectable="on" onmouseenter="showAssignments(this, 365)" onmouseleave="hideAssignments()"><span unselectable="on" datafld="AssignCountPast365" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="LastREIDate" class="spanNoWrap"/></td>
        <td unselectable="on"><img dataFld="ReIReq" style="height:13px;width:13px;"/></td>
        <td><span dataFld="ShopLocationID"/></td>
      </tr>
    </TBODY>
  </TABLE>
</DIV>
  <div id="divPages" name="divPages" style="padding:0px;">
    <table style="width:100%;padding:0px;margin:0px;margin-top:0px;table-layout:fixed;border:0px;border-top:1px solid #C0C0C0;padding-top:0px;background-color:#DCDCDC" cellspacing="0" cellpadding="0" border="0">
      <colgroup>
        <col width="21px"/>
        <col width="21px"/>
        <col width="*"/>
        <col width="30px"/>
        <col width="80px"/>
        <col width="20px"/>
        <col width="36px" style="padding:0px;padding-left:10px"/>
        <col width="27px"/>
        <col width="27px"/>
        <col width="27px"/>
      </colgroup>
      <tr>
        <td>
          <button id="btnFilter" name="btnFilter" onclick="showFilter()" tabIndex="6" style="border:0px;background-color:transparent;height:21px;width:26px;" title="Filter data...">
          <img src="/images/filter.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>
          <button id="btnClearFilter" name="btnClearFilter" style="display:none;border:0px;background-color:transparent;height:21px;width:26px;" onclick="clearFilter()" title="Clear Filter"><img src="/images/RemoveFilter.gif"/></button>
        </td>
        <td style="padding-top:2px;padding-bottom:2px;padding-left:2px;">
          <span id="spnFilter" name="spnFilter" style="padding:0px;padding-left:5px;"/>
        </td>
        <td>Page:</td>
        <td style="padding-top1:2px">
          <input type="text" id="txtPage" name="txtPage" size="12" maxLength="10" style="border:1px solid inset;font-size:9pt;text-align:center"/>
        </td>
        <td style="padding-top1:2px">
          <button id="btnGoPage" name="btnGoPage" onclick="gotoPage()" tabIndex="8" style="border:0px;background-color:transparent" title="Go to page...">
          <img src="/images/go.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>
          <button id="btnFirst" name="btnFirst" onclick="showPage(-1)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to first page">
           &lt; &lt;
          </button>
        </td>
        <td>
          <button id="btnPrev" name="btnPrev" onclick="showPage(-2)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to previous page">
            &lt;
          </button>
        </td>
        <td>
          <button id="btnNext" name="btnNext" onclick="showPage(-3)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to next page">
            &gt;
          </button>
        </td>
        <td>
          <button id="btnLast" name="btnLast" onclick="showPage(-4)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to last page">
            &gt;&gt;
          </button>
        </td>
      </tr>
    </table>
  </div>
</DIV>
<div id="divFilter" name="divFilter" style="position:absolute;top:10px;left:10px;border:1px solid #000000;visibility:hidden;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
  <div style="height:75px;width:250px;background-color:#F0E68C;">
  <table border="0" cellspacing="0" cellpadding="3" style="margin:3px">
    <tr>
      <td><b>Shop Name:</b></td>
      <td colspan="3">
        <input type="text" id="txtShopName" name="txtShopName" size="50" maxLength="45"/>
      </td>
    </tr>
    <tr>
      <td><b>City:</b></td>
      <td colspan="3">
        <input type="text" id="txtCity" name="txtCity" size="50" maxLength="45"/>
      </td>
    </tr>
    <tr>
      <td><b>State:</b></td>
      <td colspan="3">
        <select name="selState" id="selState">
          <option value=""/>
          <xsl:for-each select="/Root/Reference[@List='StateList']">
          <option>
            <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
            <xsl:value-of select="@Name"/>
          </option>
          </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td><button class="formbutton" style="border:1px solid outset;width:55px" onclick="doFilter()">Filter</button></td>
      <td><button class="formbutton" style="border:1px solid outset;width:75px" onclick="clearFilter()">Clear Filter</button></td>
      <td colspan="2" align="right">
        <button class="formbutton" style="border:1px solid outset;width:55px" onclick="hideFilter()">Close</button>
      </td>
    </tr>
  </table>
  </div>
</div>
  <div id="toolTip" name="toolTip" style="position:absolute;top:0px;left:0px;display:none;height1:150px;width:250px;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideAssignments()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideAssignments()">
    <table id="tblToolTip" border="0" cellspacing="0" cellpadding="0" style="height1:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF" onreadystatechange="checkToolTipSize()">
      <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;">
        <td style="color:#FFFFFF">Assignment Breakdown</td>
        <td><span id="spnShopLocationID"/></td>
      </tr>
      <tr valign="top">
        <td colspan="2">
          <div id="divToolTipFlow" style="overflow:auto;height:100%;width:100%;padding:3px;">
            <table id="tblToolTipSummary" border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;width:100%">
              <colgroup>
                <col width="50px"/>
                <col width="*"/>
              </colgroup>
              <tr>
                <td style="font-weight:bold">Repairable:</td>
                <td>  
                  <span class="DataValNoWrap" style="cursor:default" dataFld="Repairable"/>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  (<span class="DataValNoWrap" style="cursor:default" dataFld="RepairableAmount"/>)
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <table id="tblToolTipRepair" border="0" cellpadding="2" cellspacing="0" style="border:0px;border-bottom:1px dotted #C0C0C0; border-collapse:collapse;width:100%">
                    <colgroup>
                      <col width="25" style="text-align:right"/>
                      <col width="5"/>
                      <col width="*"/>
                    </colgroup>
                    <tr>
                      <td><span dataFld="count"/></td>
                      <td/>
                      <td><span class="DataValNoWrap" dataFld="name"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="font-weight:bold">Cash Out:</td>
                <td>  
                  <span class="DataValNoWrap" style="cursor:default" dataFld="CashOut"/>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <table id="tblToolTipCashOut" border="0" cellpadding="2" cellspacing="0" style="border:0px;border-bottom:1px dotted #C0C0C0; border-collapse:collapse;width:100%">
                    <colgroup>
                      <col width="25" style="text-align:right"/>
                      <col width="5"/>
                      <col width="*"/>
                    </colgroup>
                    <tr>
                      <td><span dataFld="count"/></td>
                      <td/>
                      <td><span class="DataValNoWrap" dataFld="name"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="font-weight:bold">Total Loss:</td>
                <td>  
                  <span class="DataValNoWrap" style="cursor:default" dataFld="TotalLoss"/>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <table id="tblToolTipTotalLoss" border="0" cellpadding="2" cellspacing="0" style="border:0px;border-bottom:1px dotted #C0C0C0; border-collapse:collapse;width:100%">
                    <colgroup>
                      <col width="25" style="text-align:right"/>
                      <col width="5"/>
                      <col width="*"/>
                    </colgroup>
                    <tr>
                      <td><span dataFld="count"/></td>
                      <td/>
                      <td><span class="DataValNoWrap" dataFld="name"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>

<xml id="xmlSearchData" name="xmlSearchData" ondatasetcomplete="setTableSource(this)">
  <Root>
    <xsl:for-each select="/Root/ShopLocation">
      <xsl:sort select="@ShopName"/>
      <xsl:variable name="LastReinspectionDate" select="user:UTCConvertDateByNodeType(.,'LastReinspectionDate', 'A')"/>
      <xsl:variable name="ProgramManagerUserID" select="@ProgramManagerUserID"/>
      <ShopLocation>
        <xsl:copy-of select="@*"/>
        <xsl:attribute name="LastREIDate">
          <xsl:if test = "$LastReinspectionDate != '01/01/1900'"><xsl:value-of select="$LastReinspectionDate"/></xsl:if>
        </xsl:attribute>
        <xsl:attribute name="LastREIDateString">
          <!-- yyyymmdd format -->
          <xsl:if test = "$LastReinspectionDate != '01/01/1900'"><xsl:value-of select="concat(substring($LastReinspectionDate, 7), substring($LastReinspectionDate, 1, 2), substring($LastReinspectionDate, 4, 2))"/></xsl:if>
        </xsl:attribute>
        <xsl:attribute name="LastClaimInsComp">
          <xsl:variable name="LastClaimInsComp"><xsl:value-of select="@LastInsuranceID"/></xsl:variable>
          <xsl:value-of select="/Root/InsuranceCompany[@InsuranceCompanyID = $LastClaimInsComp]/@Name"/>
        </xsl:attribute>
        <xsl:attribute name="ReIReq">
          <xsl:choose>
            <xsl:when test="@REIRequested = '1'">/images/cbcheck.png</xsl:when>
            <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ShopLocation>
    </xsl:for-each>
  </Root>
</xml>

<xsl:for-each select="/Root/ShopLocation">  
  <xsl:call-template name="SummaryData">
    <xsl:with-param name="days">7</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="SummaryData">
    <xsl:with-param name="days">30</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="SummaryData">
    <xsl:with-param name="days">90</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="SummaryData">
    <xsl:with-param name="days">365</xsl:with-param>
  </xsl:call-template>
</xsl:for-each>

<xml id="xmlSearchFiltered" name="xmlSearchFiltered">
</xml>
</BODY>
</HTML>
</xsl:template>

<xsl:template name="SummaryData">
  <xsl:param name="days"/>
  <xsl:variable name="AssignCount">
    <xsl:choose>
      <xsl:when test="number($days)=7"><xsl:value-of select="@AssignCountPast7"/></xsl:when>
      <xsl:when test="number($days)=30"><xsl:value-of select="@AssignCountPast30"/></xsl:when>
      <xsl:when test="number($days)=90"><xsl:value-of select="@AssignCountPast90"/></xsl:when>
      <xsl:when test="number($days)=365"><xsl:value-of select="@AssignCountPast365"/></xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="ShopLocationID"><xsl:value-of select="@ShopLocationID"/></xsl:variable>
  <xsl:if test="number($AssignCount) &gt; 0">
    <xml>
      <xsl:attribute name="id"><xsl:value-of select="concat('xml_', @ShopLocationID, '_', $days, '_Summary')"/></xsl:attribute>
      <Root>
        <Summary>
          <xsl:attribute name="Repairable"><xsl:value-of select="count(Assignment[@Disposition='Repairable' and number(@DaysSinceAssignment) &lt; number($days)])"/></xsl:attribute>
          <xsl:attribute name="RepairableAmount"><xsl:value-of select="format-number(sum(Assignment[@Disposition='Repairable' and number(@DaysSinceAssignment) &lt; number($days)]/@EstimateAmount), '$##,###,##0.00')"/></xsl:attribute>
          <xsl:attribute name="CashOut"><xsl:value-of select="count(Assignment[@Disposition='Cash Out' and number(@DaysSinceAssignment) &lt; number($days)])"/></xsl:attribute>
          <xsl:attribute name="TotalLoss"><xsl:value-of select="count(Assignment[@Disposition='Total Loss' and number(@DaysSinceAssignment) &lt; number($days)])"/></xsl:attribute>
        </Summary>
      </Root>
    </xml>
    <!-- Repairable summary -->
    <xml>
      <xsl:attribute name="id"><xsl:value-of select="concat('xml_', @ShopLocationID, '_', $days, '_Repair')"/></xsl:attribute>
      <Root>
        <xsl:value-of select="js:clearInsCo()"/>
        <xsl:for-each select="Assignment[number(@DaysSinceAssignment) &lt; number($days) and @Disposition='Repairable']">
          <xsl:sort select="@InsuranceCompanyName"/>
            <xsl:if test="js:existsInsCo(string(@InsuranceCompanyID)) = false()">
              <xsl:variable name="InsCoID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:variable>
              <xsl:element name="InsuranceCo">
                <xsl:attribute name="count"><xsl:value-of select="count(../Assignment[number(@DaysSinceAssignment) &lt; number($days) and @InsuranceCompanyID=$InsCoID and @Disposition='Repairable'])"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@InsuranceCompanyName"/></xsl:attribute>
              </xsl:element>
            </xsl:if>
        </xsl:for-each>
      </Root>
    </xml>

    <!-- Cash Out summary -->
    <xml>
      <xsl:attribute name="id"><xsl:value-of select="concat('xml_', @ShopLocationID, '_', $days, '_CashOut')"/></xsl:attribute>
      <Root>
        <xsl:value-of select="js:clearInsCo()"/>
        <xsl:for-each select="Assignment[number(@DaysSinceAssignment) &lt; number($days) and @Disposition='Cash Out']">
          <xsl:sort select="@InsuranceCompanyName"/>
            <xsl:if test="js:existsInsCo(string(@InsuranceCompanyID)) = false()">
              <xsl:variable name="InsCoID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:variable>
              <xsl:element name="InsuranceCo">
                <xsl:attribute name="count"><xsl:value-of select="count(../Assignment[number(@DaysSinceAssignment) &lt; number($days) and @InsuranceCompanyID=$InsCoID and @Disposition='Cash Out'])"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@InsuranceCompanyName"/></xsl:attribute>
              </xsl:element>
            </xsl:if>
        </xsl:for-each>
      </Root>
    </xml>

    <!-- Total Loss summary -->
    <xml>
      <xsl:attribute name="id"><xsl:value-of select="concat('xml_', @ShopLocationID, '_', $days, '_TotalLoss')"/></xsl:attribute>
      <Root>
        <xsl:value-of select="js:clearInsCo()"/>
        <xsl:for-each select="Assignment[number(@DaysSinceAssignment) &lt; number($days) and @Disposition='Total Loss']">
          <xsl:sort select="@InsuranceCompanyName"/>
            <xsl:if test="js:existsInsCo(string(@InsuranceCompanyID)) = false()">
              <xsl:variable name="InsCoID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:variable>
              <xsl:element name="InsuranceCo">
                <xsl:attribute name="count"><xsl:value-of select="count(../Assignment[number(@DaysSinceAssignment) &lt; number($days) and @InsuranceCompanyID=$InsCoID and @Disposition='Total Loss'])"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@InsuranceCompanyName"/></xsl:attribute>
              </xsl:element>
            </xsl:if>
        </xsl:for-each>
      </Root>
    </xml>
  </xsl:if>
</xsl:template>
</xsl:stylesheet>