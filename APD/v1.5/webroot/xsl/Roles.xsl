<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="UserPermissionCRUD" select="User Permission"/>

<!-- <xsl:param name="CRUDUserPermission"/> -->

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function formatDateTime(strDate)
     {
        var strRet = "";
        var strTmp = "";
        var aTmp;
        if (strDate != "") {
            strTmp = strDate.split("T")[0];
            aTmp = strTmp.split("-");
            strRet = aTmp[1] + "/" + aTmp[2] + "/" + aTmp[0];
            strTmp = strDate.split("T")[1];
            aTmp = strTmp.split(":");
            strRet += " " + aTmp[0] + "/" + aTmp[1] + "/" + aTmp[2];
        }
        return strRet;
     }
  ]]>
</msxsl:script>

<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="substring($UserPermissionCRUD, 3, 1) = 'U'">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="UserPermissionCRUD" select="$UserPermissionCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to view roles.
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserPermissionCRUD"/>
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/formvalid.js"/>
<SCRIPT language="JavaScript" src="/js/tablesort.js"/>
<SCRIPT language="JavaScript" src="/js/grid.js"/>
<SCRIPT language="JavaScript" src="/js/images.js"/>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
<![CDATA[
	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

  var curRow = null;
  var bInsertMode = false;

  function PageInit() {
    resizePage();
    /*var sCRUD = parent.sCRUDInfo;
    if ((sCRUD == undefined) || (sCRUD.indexOf("C") == -1) || (sCRUD.indexOf("R") == -1)){
        ClientWarning("Access Denied to view Role Information. Please contact your supervisor");
        window.navigate("../blank.asp");
    }*/
  }
  
  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }

  function RoleGridSelect(oRow)
  {
    try {
      if (curRow === oRow) return;

      if (curRow) {
        if (typeof(ifrmRoleDetail.isDirty) == "function") {
          if (ifrmRoleDetail.isDirty()) {
              var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
              if (sSave == "Yes"){
                  if (!btnSave()) return true;
              }
          }
        }
      }
      
      if (curRow)
        curRow.style.backgroundColor = "#FFFFFF";
        
  	  var strRoleID = oRow.cells[3].innerText;
  	  ifrmRoleDetail.frameElement.src = "RoleDetail.asp?RoleID=" + strRoleID;

      curRow = oRow;
      curRow.style.backgroundColor = "#FFD700";
      saveBG = curRow.style.backgroundColor;
    	//document.location="UserDetail.asp?UserID=" + strUserID;
    }
    catch(e) {
      handleJSError("RoleGridSelect", e)
    }


    /*try {
  	  var strRoleID = oRow.cells[3].innerText;
  	  parent.document.location="RoleDetail.asp?RoleID=" + strRoleID;
    }
    catch(e) {
      handleJSError("RoleGridSelect", e)
    }*/
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
	// Handle click of Add button
	function ADS_Pressed(sAction, sName)
	{
    try {
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;

  		var oDiv = document.getElementById(sName);

  		if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}
    
  function btnAdd(){
    if (curRow) {
      if (ifrmRoleDetail.isDirty()) {
          var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
          if (sSave == "Yes"){
              if (!btnSave()) return true;
          }
      }
      saveBG = "";
      GridMouseOut(curRow);
      curRow = null;
    }
    ifrmRoleDetail.frameElement.src = "RoleDetail.asp?RoleID=New";
    bInsertMode = true;
  }
  
  function btnSave(){
    if (curRow) {
      if (typeof(ifrmRoleDetail.ADS_Pressed) == "function")
        ifrmRoleDetail.ADS_Pressed("Update", "content11");
    }
    else if (bInsertMode) {
      if (typeof(ifrmUsrDetail.ADS_Pressed) == "function")
        ifrmRoleDetail.ADS_Pressed("Update", "content11");
      bInsertMode = false;
    }
    return true;
  }
    
  function resizePage(){
    var ifrmHeight = ifrmRoleDetail.frameElement.offsetHeight;
    var iClientHeight = document.body.offsetHeight;
    //window.status = iClientHeight - ifrmHeight;
    tblRoles.parentElement.style.height = iClientHeight - ifrmHeight - 60;
    crudDivUsers.style.top = ifrmRoleDetail.frameElement.parentElement.offsetTop;
  }
]]>
</SCRIPT>

<title>APD User and Role Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="InitFields();PageInit();" style="overflow:hidden;background:transparent;margin:0px;padding:0px;width:100%;height:100%;overflow:hidden" tabIndex="-1"  onresize="resizePage()">
<!-- Start Tabs1 -->
  <table cellspacing="0" cellpadding="0" border="0" style="height:100%;width:100%">
    <tr valign="top">
      <td>
        <DIV unselectable="on" style="">
  
    <!-- Start Role List -->
          <TABLE class="UserMiscInputs" onClick="sortColumn(event, 'tblRoles')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;width:100%" >
              <colgroup>
                  <col width="81px"/>
                  <col width="300px"/>
                  <col width="270px"/>
              </colgroup>
              <TBODY>
          		<tr class="QueueHeader" style="height:21px">
           			<td class="TableSortHeader" type="String">Enabled</td>
           			<td class="TableSortHeader" type="String">Role Name</td>
           			<td class="TableSortHeader" type="String">Business Unit</td>
          		</tr>
              </TBODY>
          </TABLE>
          <DIV unselectable="on" class="autoflowTable" style="width:100%; height:267px; overflow: auto;">
          	<TABLE id="tblRoles" class="GridTypeTable" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;width:100%">
              <colgroup>
                  <col width="81px"/>
                  <col width="300px"/>
                  <col width="260px"/>
              </colgroup>
          	  <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                <xsl:for-each select="/Root/Role">
                <TR onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' onClick='RoleGridSelect(this)' style='height:21px;'>
                <!-- <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute> -->
                <!-- <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                <xsl:attribute name="bgcolor">#fff7e5</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                <xsl:attribute name="bgcolor">#ffffff</xsl:attribute>
                </xsl:otherwise>
                </xsl:choose> -->
                  <td align="center" style="border:1px ridge;">
                    <xsl:choose>
                      <xsl:when test="@EnabledFlag = 1">
                        <xsl:text>Yes</xsl:text>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:text>No</xsl:text>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td style="border:1px ridge;"><xsl:value-of select="@Name"/></td>
                  <td align="center" style="border:1px ridge;">
                    <xsl:choose>
                      <xsl:when test="@BusinessCD = 'C'">
                        <xsl:text>Claim Unit</xsl:text>
                      </xsl:when>
                      <xsl:when test="@BusinessCD = 'S'">
                        <xsl:text>Shop Management Unit</xsl:text>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td style="display:none"><xsl:value-of select="@RoleID"/> </td>
                </TR>
                </xsl:for-each>
              </TBODY>
            </TABLE>
          </DIV>
        </DIV>
        <br/>
      </td>
    </tr>
    <tr style="height:275px;" valign="top">
      <td style="overflow:auto">
        <div align="right" unselectable="on" id="crudDivUsers" name="crudDivUsers" style="position:absolute;top:0;left:0;width:95%;margin-right:20px;margin-bottom:2px;">
          <!-- <xsl:if test="(substring($UserPermissionCRUD, 1, 1) = 'C')">
          <img name="btnAdd1" id="btnAdd1" src="/images/but_ADD_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_ADD_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_ADD_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_ADD_down.png'" onclick="btnAdd()" style="cursor:hand"/> 
          </xsl:if> 
          <img src="/images/spacer.gif" width="3px" height="1px"/> -->
          <xsl:if test="substring($UserPermissionCRUD, 3, 1) = 'U'">
          <img name="btnSave1" id="btnSave1" src="/images/but_SAVE_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_SAVE_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_SAVE_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_SAVE_down.png'" onclick="btnSave()" style="cursor:hand"/>
          </xsl:if>
        </div>
        <iframe name="ifrmRoleDetail" id="ifrmRoleDetail" src="/blank.asp" style="width:100%;height:100%;background:transparent;" allowtransparency="true"/>
      </td>
    </tr>
  </table>
</BODY>
</html>
</xsl:template>

</xsl:stylesheet>
