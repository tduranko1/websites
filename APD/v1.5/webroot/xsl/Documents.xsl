<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Documents">
 
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function GetFileSize(strFile) {
      var fso, sImgInfo;
      sImgInfo = "";
      fso = new ActiveXObject("Scripting.FileSystemObject");
      strFile = strFile.replace(/\\\\/g, "\\");
      if (fso.FileExists(strFile)){
        var f = fso.GetFile(strFile);
        if (f){
          sImgInfo += "; " + Math.round(f.size / 1024) + " kb";
        }
      }
      else {
        sImgInfo = "Not Found";
      }
      
      return sImgInfo;
    }    
    function UTCConvertDate(vDate) {
      var vYear = vDate.substr(0,4);
      var vMonth = vDate.substr(5,2);
      var vDay = vDate.substr(8,2);
      vDate = vMonth + '/' + vDay  + '/' + vYear
      return vDate;
    }
    
  ]]>
</msxsl:script>

<xsl:param name="ImageRootDir"/>
<xsl:param name="EntityName"/>
<xsl:param name="EntityNumber"/>
<xsl:param name="viewMode"/>
<xsl:param name="selectThumbnail"/>
 
<xsl:template match="/Root">  
  <xsl:choose>
    <xsl:when test="count(Document[@DocumentID != 0]) &gt; 0">
      <xsl:call-template name="mainHTML"/>
    </xsl:when>
    <xsl:otherwise>
      <HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspDocumentGetDetailXML,Documents.xsl,6123,216,'O',999   -->

        <head>
          <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        </head>
        <BODY class="bodyAPDSub" unselectable="on" bgcolor="#FFFFFF" tabIndex="-1" style="overflow:hidden;margin:0px;padding:0px;border:0px;">
          <table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
            <tr>
              <td align="center" >
                No Documents available.
              </td>
            </tr>
          </table>
        </BODY>
      </HTML>
    </xsl:otherwise>
  </xsl:choose>
  
</xsl:template>
 
<xsl:template name="mainHTML"> 
 
<HTML>
 
<HEAD>
  <TITLE>APD Document Viewer</TITLE>
  
  <LINK rel="stylesheet" href="/css/Documents.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,4);
		  event.returnValue=false;
		  };	
  </script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript"> 
var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
var img = new Array();
var imgCreatedDate = new Array();
var imgDocType = new Array();
var giMode = 1;
var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
var newWin = null;
var curDocShown = null;
var iViewMode = '<xsl:value-of select="$viewMode"/>';
var iSelectThumbnail = '<xsl:value-of select="$selectThumbnail"/>';

<xsl:for-each select="Document">
  img.push(gsImageRootDir + '<xsl:value-of select="@ImageLocation"/>');
  imgCreatedDate.push('<xsl:value-of select="@ImageDate"/>');
  <xsl:variable name="docTypeID" select="@DocumentTypeID"/>
  imgDocType.push('<xsl:value-of select="js:cleanString(string(/Root/Reference[@List='DocumentType' and @ReferenceID = $docTypeID]/@Name))"/>');
</xsl:for-each>
  
<![CDATA[
  
  function initPage(){
    thumbNail.style.filter = "FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#303030,strength=3)";
    viewer.style.filter = "FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#303030,strength=3)";

    loadImages();
    //showMode(2);

    if (window.self === window.top) {
      btnPin.firstChild.src = "images/pin.gif";
    }
      
    try {
      if (iViewMode == '')
        resizeViewer();
      else
        showMode(iViewMode);
        
      if (iSelectThumbnail != ''){
        var tn = document.all["tn" + iSelectThumbnail];
        if (tn)
          displayImage(tn);
      }
    } catch (e) {}
  }
  
  function loadImages(){
    var doc = document.all;
    var fso, sImgInfo;
    //fso = new ActiveXObject("Scripting.FileSystemObject");
    for (var i = 1; i <= img.length; i++){
      sImgInfo = "";
      //sExt = fso.GetExtensionName(img[i-1]);
      sExt = "";
      var iPos = img[i-1].lastIndexOf(".");
      if (iPos != -1) {
        sExt = img[i-1].substr(iPos+1, img[i-1].length).toLowerCase();
      }
      var objImg = doc["img" + i];
      
      objImg.src = "Thumbnail.asp?Doc=" + img[i-1];
      objImg.FileExt = sExt;
      objImg.imgIndex = (i - 1);
      if (sExt != "gif" && sExt != "jpg" && sExt != "bmp" && sExt != "png" && sExt != "emf" && sExt != "wmf"){
        objImg.style.display = "none";
        var s = "<SPAN class='thumbNailInfo' style='border:0px;text-align:center;color:#4169E1;height:100%;width:100%'>" + imgDocType[i-1] + "</SPAN>";
        objImg.insertAdjacentHTML("afterEnd", s);
        //objImg.title = imgDocType[i-1];
      }

      /*if (imgCreatedDate[i-1] != ""){
        var sDt = imgCreatedDate[i-1];
        var sDt1 = sDt.split("T");
        var sDt2 = sDt1[0].split("-");
        if (sDt2.length == 3){
          sImgInfo = sDt2[1] + "/" + sDt2[2] + "/" + sDt2[0];
        }
      }

      if (fso.FileExists(img[i-1])){
        var f = fso.GetFile(img[i-1]);
        if (f){
          sImgInfo += "; " + Math.round(f.size / 1024) + " kb";
        }
      }
      else {
        sImgInfo = "<font style='color:#FF0000'>Not Found</font>";
      }*/
      //objImg.parentElement.parentElement.nextSibling.noWrap = true;
      //objImg.parentElement.parentElement.nextSibling.innerHTML = sImgInfo;
    }
  }
  
  function displayImage(obj){
    var objImg = obj.firstChild.firstChild.firstChild;
    var objIndex = objImg.getAttribute("imgIndex");
    if (!isNaN(parseInt(objIndex))) {
      divLoading.style.visibility = "visible";
      otherPreview.frameElement.src = "/EstimateDocView.asp?docPath=" + img[parseInt(objIndex)];
      if (curDocShown) {
        curDocShown.className = "thumbNail";
      }
      obj.className = "thumbNailSelected";

      myScrollIntoView(obj, document.all["thumbNail"]);
      curDocShown = obj;
    }
  }
  
  function showMode(iMode){
      switch(iMode){
        case 1:
          if (giMode != 1){
            thumbNail.style.width = "240px";
            thumbNail.lastChild.noWrap = false;
            mode1.cells[1].appendChild(mode2.cells[0].firstChild);
            mode1.cells[0].appendChild(mode2.cells[1].firstChild);
            mode2.cells[0].innerHTML = "";
            mode2.cells[1].innerHTML = "";
    
            mode1.style.display = "inline";        
            mode2.style.display = "none";        
            
            thumbNail.className = "thumbNailHTile";
            viewer.style.height = "98%";
            mode1.cells[1].firstChild.className = "viewerHTile";
            mode1.cells[1].firstChild.lastChild.className = "viewerDivHTile";
            mode1.cells[1].firstChild.lastChild.noWrap = false;
            thumbNail.parentElement.style.display = "inline";
            btnThumbNail.style.display = "inline";
            giMode = 1;
          }
          else {
            if (thumbNail.parentElement.style.display == "none") {
              thumbNail.parentElement.style.display = "inline";
              btnThumbNail.style.display = "inline";
            }
          }
          break;
        case 2:
          if (giMode != 2) {
            mode2.cells[1].appendChild(mode1.cells[0].firstChild);
            mode2.cells[0].appendChild(mode1.cells[1].firstChild);
            mode1.cells[0].innerHTML = "";
            mode1.cells[1].innerHTML = "";
            mode2.cells[0].firstChild.className = "viewerVTile";
            mode2.cells[0].firstChild.lastChild.className = "viewerDivVTile";
            mode2.cells[1].firstChild.className = "thumbNailVTile";
            mode2.cells[1].firstChild.lastChild.className = "thumbNailDivVTile";
            mode2.cells[1].firstChild.lastChild.noWrap = true;
    
            mode1.style.display = "none";
            mode2.style.display = "inline";
            thumbNail.parentElement.style.display = "inline";
            btnThumbNail.style.display = "inline";
            giMode = 2;
          }
          else {
            if (thumbNail.parentElement.style.display == "none") {
              thumbNail.parentElement.style.display = "inline";
              btnThumbNail.style.display = "inline";
            }
          }
          break;
      }

      resizeViewer();
  }
  
  function resizeViewer() {
    try{
      switch (giMode) {
        case 1:
          thumbNail.noWrap = false;
          //thumbNail.style.height = document.body.clientHeight - thumbNail.previousSibling.offsetHeight - 18;
          //thumbNail.previousSibling.style.width = thumbNail.offsetWidth - 3;
          if (thumbNail.offsetWidth > 0) {
            viewer.style.width = document.body.clientWidth - thumbNail.offsetWidth - 35;
            divPreview.style.width = document.body.clientWidth - thumbNail.offsetWidth - 37; 
          }
          else {
            viewer.style.width = document.body.clientWidth - 25;
            divPreview.style.width = document.body.clientWidth - 27; 
          }
          divPreview.style.height = viewer.clientHeight - viewer.firstChild.clientHeight - 18;
          divPreview.firstChild.style.height = viewer.clientHeight - viewer.firstChild.clientHeight - 18;
          break;
        case 2:
          thumbNail.style.width = document.body.clientWidth - 15;
          thumbNail.noWrap = true;
          if (thumbNail.offsetHeight > 0)
            viewer.style.height = document.body.clientHeight - thumbNail.offsetHeight - 20;
          else
            viewer.style.height = document.body.clientHeight - 13;
          viewer.style.width = document.body.clientWidth - 15; 
          viewer.style.overflow = "hidden";
          //viewer.lastChild.style.width = document.body.clientWidth - 15;
          viewer.lastChild.style.height = viewer.clientHeight - 15;
          //divPreview.style.width = viewer.offsetWidth - 21;
          divPreview.style.width = viewer.offsetWidth - 5; 
          divPreview.style.height = viewer.clientHeight - viewer.firstChild.clientHeight - 18; //-3
          divPreview.firstChild.style.height = viewer.clientHeight - viewer.firstChild.clientHeight - 18;
          //viewer.firstChild.scrollIntoView(true);
          break;
      }
      divLoading.style.top = viewer.offsetTop + viewer.offsetHeight / 2;
      divLoading.style.left = viewer.offsetLeft + viewer.offsetWidth / 2;
    } catch(e) {}
  }
  
  function togglePinned(){
    var objImg = btnPin.firstChild;
    if (objImg.src.indexOf("/images/pinned.gif") != -1) {
      var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
      var parentLoc = window.parent.location.href;
      var sURL = window.location.href;
      sURL += "&viewMode=" + giMode; 
      if (curDocShown)
        sURL += "&selectThumbnail=" + curDocShown.id;
        
      if (parentLoc.toLowerCase().indexOf("claimvehicle.asp") != -1){
        //alert(sURL);
        newWin = window.showModalDialog(sURL, null, sDlgProp + sDlgSize);
      }
      else {
        objImg.src = "/images/pin.gif";
        btnPin.title = "Detach Photo Viewer";
        newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
        newWin.opener = window.self;
      }
      //window.status = 
      //var sDlgSize = "dialogHeight:" + document.body.offsetHeight + "px;dialogWidth:" + document.body.offsetWidth + "px;";
    }
    else {
      objImg.src = "/images/pinned.gif";
      btnPin.title = "Close Detached Photo Viewer";
      if (newWin != null)
        newWin.close();
      window.close();
    }
  }
  
  function cleanup(){
    if (window.self === window.top) {
      if (window.opener) {
        var openerBtnPin = window.opener.document.all["btnPin"];
        if (openerBtnPin)
          openerBtnPin.firstChild.src = "/images/pinned.gif";
      }
    }
  }
  
  function showThumbNail(){
    if ((thumbNail.parentElement.style.display == "inline") || thumbNail.parentElement.style.display == "") {
      thumbNail.parentElement.style.display = "none";
      btnThumbNail.style.display = "none";
      resizeViewer();
    }
  }
  
  function otherPreview_onload(){
    if (otherPreview.frameElement.src.indexOf("blank.asp") == -1) {
      if (otherPreview.btnClose) {
        if (otherPreview.btnClose.parentElement.parentElement.children.length == 1)
          otherPreview.btnClose.parentElement.parentElement.style.display = "none";
        else
          otherPreview.btnClose.style.display = "none";
      }
      otherPreview.frameElement.document.body.style.border = "0px";
    }
    if (otherPreview.frameElement.readyState == "complete")
      divLoading.style.visibility = "hidden";
  }

  var iCount = 0;
  var bScrolling = false;
  var iScrollMode = 0; // 1=right; 2=left;
  function myScrollIntoView(obj, objWithScroll){
    //window.status = iScrollMode;
    switch (giMode) {
      case 1: //vertical mode - default
        var iScrollPos = (obj.offsetTop + obj.offsetHeight) - objWithScroll.scrollTop - objWithScroll.clientHeight + 10; //10 -> margin * 2 -> 5 * 2;
        if (iScrollPos > 0){
          obj.scrollIntoView(false);
        }
        else {
          if (Math.abs(iScrollPos) > Math.abs(obj.offsetHeight - objWithScroll.clientHeight))
            obj.scrollIntoView(true);
        }
        break;
      case 2: //horizontal mode
        var iScrollPos = (obj.offsetLeft + obj.offsetWidth) - objWithScroll.scrollLeft - objWithScroll.clientWidth; //10 -> margin * 2 -> 5 * 2;
        if (iScrollPos > 0){
          //objWithScroll.doScroll("scrollbarPageRight");
          if (iScrollMode != 2) {
            objWithScroll.doScroll("scrollbarRight");
            if (iCount < 25 && !bScrolling) {
              iCount++; iScrollMode = 1;
              myScrollIntoView(obj, objWithScroll);
            }
            iScrollMode = 0;iCount = 0;
          }
        }
        else {
          if (Math.abs(iScrollPos) > Math.abs(obj.offsetWidth - objWithScroll.clientWidth)) {
            if (iScrollMode != 1) {
              objWithScroll.doScroll("scrollbarLeft");
              if (iCount < 25) {
                iCount++; iScrollMode = 2;
                myScrollIntoView(obj, objWithScroll);
              }
            }
          }
          iScrollMode = 0;iCount = 0;
        }
        break;
    }
  }
  
  window.onresize = resizeViewer;
]]>
 
</SCRIPT>
 
</HEAD>
 
<BODY class="bodyAPDSub" unselectable="on" onload="initPage()" onunload="cleanup()" bgcolor="#FFFFFF" tabIndex="-1" style="overflow:hidden;margin:0px;padding:0px;border:0px;">
  <table id="mode1" name="mode1" cellpadding="5" cellspacing="0" border="0" style="height:100%">
    <tr valign="top">
      <td unselectable="on" style="width:295px;">
        
        <div id="thumbNail" name="thumbNail" class="thumbNailHTile" unselectable="on">
          <div class="sectionTitle">Thumbnail images</div>
          <div class="thumbNailDivHTile">
            <xsl:apply-templates select="Document">
              <xsl:with-param name="RootDir" select="$ImageRootDir"/>
            </xsl:apply-templates>
          </div>
        </div>
      </td>
      <td unselectable="on" style="width:*">
        <div id="viewer" name="viewer" class="viewerHTile" unselectable="on">
          <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
              <tr style="background-color:#c0c0c0;">
                <td width="150px" nowrap="">
                  <button id="btnPin" name="btnPin" class="toolbarButton" onclick="togglePinned()" title="Unpin/Pin from/to Documents tab"><img src="/images/pinned.gif" width="18" height="18" border="0" style="cursor:hand;"/></button>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <button id="btnMode1" name="btnMode1" class="toolbarButton" onclick="showMode(1)" title="Tile Vertically/View Thumbnail"><img src="/images/VTile.gif" width="18" height="18" border="0" style="cursor:hand;"/></button>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <button id="btnMode2" name="btnMode2" class="toolbarButton" onclick="showMode(2)" title="Tile Horizontally/View Thumbnail"><img src="/images/HTile.gif" width="18" height="18" border="0" style="cursor:hand;"/></button>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <button id="btnThumbNail" name="btnThumbNail" class="toolbarButton" onclick="showThumbNail()" title="Hide Thumbnails"><img src="/images/ViewThumbNail.gif" width="18" height="18" border="0" style="cursor:hand;"/></button>
                </td>
                <!--<td class="sectionTitle" width="*" nowrap="">Full size image</td>-->
                <td class="sectionTitle" width="100px" nowrap="">
                  <strong>LYNX Id: </strong> <xsl:value-of select="@LynxID"/>
                </td>
                <td class="sectionTitle" align="right" width="100px" nowrap="">
                  <xsl:if test="$EntityName != ''">
                    <strong><xsl:value-of select="$EntityName"/> No.: </strong> <xsl:value-of select="$EntityNumber"/>
                  </xsl:if>
                </td>
              </tr>
            </table>
            <center></center>
          </div>
          <div id="divPreview" class="viewerDivHTile">
            <!--<img id="fullImage" name="fullImage" style="display:none;margin:3px;" unselectable="on"/>-->
              <!--<OBJECT id="imgPreview" tabIndex="-1" classid="clsid:50F16B26-467E-11D1-8271-00C04FC3183B" style="width:100%;height:100%"></OBJECT>-->
              <iframe id="otherPreview" src="/blank.asp?ReportCategory=Documents" style="width:100%;height:100%;z-index:1" onload="otherPreview_onload()"/>
          </div>
        </div>  
      </td>
    </tr>
  </table>

  <table id="mode2" name="mode2" cellpadding="5" cellspacing="0" border="0" style="width:100%;display:none;">
    <tr>
      <td></td>
    </tr>
    <tr valign="top">
      <td></td>
    </tr>
  </table>  
  <div id="divLoading" name="divLoading" style="position:absolute;visibility:hidden;height:65px;width:300px;background-color:#FFF8DC;border:1px solid #000000;color:#4169E1;text-align:center;padding:25px;font-family:Tahoma;font-size:11px;z-index:999">
    <strong>Downloading document. Please wait... </strong>
  </div>
</BODY>
</HTML> 
 
</xsl:template>
 
<xsl:template match="Document">
  <xsl:param name="RootDir"/>
  <span class="thumbNail" unselectable="on" onclick="displayImage(this)">
    <!-- <xsl:attribute name="name">tn<xsl:value-of select="position()"/></xsl:attribute>
    <xsl:attribute name="id">tn<xsl:value-of select="position()"/></xsl:attribute> -->
    <xsl:attribute name="name">tn<xsl:value-of select="@DocumentID"/></xsl:attribute>
    <xsl:attribute name="id">tn<xsl:value-of select="@DocumentID"/></xsl:attribute>
    <center unselectable="on">
      <span class="thumbNailImage">
      <img  unselectable="on">
        <xsl:attribute name="name">img<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="id">img<xsl:value-of select="position()"/></xsl:attribute>
      </img>
      </span>
    </center>
    <xsl:if test="@DirectionToPayFlag = '1' or @FinalEstimateFlag = '1'">
      <span class="thumbNailInfo">
        <xsl:if test="@DirectionToPayFlag = '1'">Direction To Pay</xsl:if>
        <xsl:if test="@FinalEstimateFlag = '1'">
          <xsl:if test="@DirectionToPayFlag = '1'">/</xsl:if>Final Estimate
        </xsl:if>
      </span>
    </xsl:if>
    <span class="thumbNailInfo" title="Created Date; File Size">
      <!-- <xsl:value-of select="concat($RootDir, @ImageLocation)"/> -->
      <xsl:variable name="FileInfo" select="user:GetFileSize(concat($RootDir, @ImageLocation))"/>
      <xsl:choose>
        <xsl:when test="$FileInfo = 'Not Found'">
          <xsl:attribute name="style">color:#FF0000</xsl:attribute>
          Not Found
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="user:UTCConvertDate(string(@CreatedDate))"/> <xsl:value-of select="$FileInfo"/>
        </xsl:otherwise>
      </xsl:choose>
      <!--<xsl:attribute name="title"><xsl:value-of select="concat($RootDir, @ImageLocation)"/></xsl:attribute>
      img<xsl:value-of select="position()"/>-->
    </span>
  </span>
</xsl:template>
 
</xsl:stylesheet>
