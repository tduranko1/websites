<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimBilling">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:variable name="PartnerCRUD">CRUD</xsl:variable> 

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($PartnerCRUD, 'R')">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="PartnerCRUD"><xsl:value-of select="$PartnerCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
          <tr>
            <td align="center">
              <font color="#ff0000"><strong>You do not have sufficient permission to view Unprocessed Partner Data.
              <br/>Please contact administrator for permissions.</strong></font>
            </td>
          </tr>
        </table>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="PartnerCRUD"/>
<HTML>

<HEAD>

<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  var giRecCount = <xsl:value-of select="count(/Root/PartnerRecord)"/>;
          
  <![CDATA[
    
    function initPage(){
      
    }

    function SendData(){
      var sRequest = "";
      
      ProcessingIndicator()
      
      for (var i = 1; i <= giRecCount; i++){
        var strLynxID = eval("txtLynxID" + i).value;
        var strVehicleNumber = eval("txtVehicleNumber" + i).value
        if (strLynxID != "" && strVehicleNumber != "")
          sRequest += eval("tdPartnerTransID" + i).getAttribute("PartnerTransID") + "," + strLynxID + "," + strVehicleNumber + ";"          
      }
      
      if (sRequest != ""){
        frmPartnerData.action += "?act=fix&data=" + sRequest;
        frmPartnerData.submit();
      }
      else
        ClientInfo("Please enter the LYNX ID and the Vehicle Number for any items you wish to attach to the specified claim");
    }
    
    function ProcessingIndicator(){
      lblProcessing.style.display = "inline";
    	lblProcessing.innerText = "Processing";
    	gsSearchingIntervalID = window.setInterval("if (lblProcessing.innerText == 'Processing.............') lblProcessing.innerText = 'Processing'; lblProcessing.innerText += '.'", 750);
    }
     
  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:#FFFFFF; border:0px; overflow:hidden;margin:2px;padding:0px;" onLoad="initPage()" tabIndex="-1">
  <h2>Unprocessed Partner Data</h2>
  <FORM id="frmPartnerData" method="post" style="overflow:hidden"></FORM>
  <DIV id="CPScrollTable" style="position:relative; width:100%; padding:3px;">
    <SPAN>
      <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort2')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
        <colgroup>
          <col width="60px"/>
          <col width="34px"/>
          <col width="43px"/>
          <col width="111px"/>
          <col width="78px"/>
          <col width="101px"/>
          <col width="89px"/>
          <col width="88px"/>
          <col width="115px"/>      
        </colgroup>
    
        <tr unselectable="on" class="QueueHeader">
          <td unselectable="on" class="TableSortHeader">LYNX ID</td>
          <td unselectable="on" class="TableSortHeader">Veh No.</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Source</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Type</td>
          <td unselectable="on" class="TableSortHeader" type="Date">Date</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Claim Number</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Insured Name</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Owner Name</td>
          <td unselectable="on" class="TableSortHeader" type="CaseInsensitiveString">Vehicle Description</td>
        </tr>
      </TABLE> 
    </SPAN>
    <DIV unselectable="on" class="autoflowTable" style="height:240px; border-right:0;">
      <TABLE unselectable="on" id="tblSort" class="GridTypeTable" cellspacing="1" border="0" cellpadding="0" style="table-layout:fixed;">
        <colgroup>
          <col width="57px"/>
          <col width="33px"/>
          <col width="42px"/>
          <col width="110px"/>
          <col width="77px"/>
          <col width="100px"/>
          <col width="88px"/>
          <col width="87px"/>
          <col width="115px"/>      
        </colgroup>
        
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="PartnerRecord">
            <xsl:call-template name="PartnerRecord"/>
          </xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
  </DIV>    
        
  <TABLE cellpadding="0" cellspacing="0" border="0">
    <TR>
      <TD>
        <IE:APDButton id="btnSubmit" value="Submit" width="80px" onButtonClick="SendData()"/>
        <img src="/images/spacer.gif" style="width:20"/>
        <label id="lblProcessing" style="color:blue; font-weight:600; display:none"></label>
      </TD>      
    </TR>
    <xsl:if test="count(/Root/Errors/Error) &gt; 0">
      <TR>
        <TD><image src="/images/spacer.gif" height="10"/></TD>
      </TR>    
      <TR>
        <TD>
          <!-- Exceptions Table - details any problems that occurred while attempting to fix-up pass-thru data on a Partner record. -->
          <DIV id="CPScrollTable" style="position:relative; width:735; padding:3px;">
            <SPAN>
              <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                <tr unselectable="on" class="QueueHeader">
                  <td unselectable="on" class="TableSortHeader">Exceptions</td>
                </tr>
              </TABLE>
            </SPAN>
            <DIV unselectable="on" class="autoflowTable" style="height:120px;">
              <TABLE class="GridTypeTable" cellspacing="1" border="0" cellpadding="0" style="table-layout:fixed; width:729px;">
                <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                  <xsl:for-each select="/Root/Errors/Error">
                    <TR unselectable="on" >
                      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
                      <TD class="GridTypeTD" width="100%" style="text-align:left;"><xsl:value-of select="."/></TD>
                    </TR>
                  </xsl:for-each>
                </TBODY>   
              </TABLE>
            </DIV>
          </DIV> 
        </TD>
      </TR>
    </xsl:if>
  </TABLE> 
  
      
</BODY>
</HTML>

</xsl:template>

<xsl:template name="PartnerRecord">
  <!--<TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Claim' onClick='GridClick(this)' onDblClick='GridSelect(this)' style="21px">-->
  <TR unselectable="on" style="21px">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <td class="GridTypeTD">
      <IE:APDInputNumeric id="txtLynxID" name="LynxID" scale="0" precision="7" width="45">
        <xsl:attribute name="id">txtLynxID<xsl:value-of select="position()"/></xsl:attribute>
      </IE:APDInputNumeric>
    </td>
    <td class="GridTypeTD">
      <IE:APDInputNumeric id="txtVehicleNumber" name="VehicleNumber" scale="0" precision="3" width="20">
        <xsl:attribute name="id">txtVehicleNumber<xsl:value-of select="position()"/></xsl:attribute>
      </IE:APDInputNumeric>
    </td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@TransactionSource != ''"><xsl:value-of select="@TransactionSource"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
    </td>
    <td class="GridTypeTD">
      <xsl:attribute name="id">tdPartnerTransID<xsl:value-of select="position()"/></xsl:attribute>
      <xsl:attribute name="PartnerTransID"><xsl:value-of select="@PartnerTransID"/></xsl:attribute>
      <xsl:choose>
        <xsl:when test="@TransactionType != ''"><xsl:value-of select="@TransactionType"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>      
    </td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@TransactionDate != ''"><xsl:value-of select="@TransactionDate"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>        
    </td>    
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@ClaimNo != ''"><xsl:value-of select="@ClaimNo"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>      
    </td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@InsdLN != ''"><xsl:value-of select="@InsdLN"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>      
    </td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@OwnerLN != ''"><xsl:value-of select="@OwnerLN"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>      
    </td>    
    <td class="GridTypeTD">
      <xsl:value-of select="@Year"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@Make"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@Model"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@BodyStyle"/>
    </td>    
  </TR>
</xsl:template>

</xsl:stylesheet>
