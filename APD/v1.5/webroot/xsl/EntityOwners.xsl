<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

  <xsl:template match="Root">
    <xml id="xmlEntityOwners" name="xmlEntityOwners" ondatasetcomplete="if (typeof(showEntityOwners) == 'function') showEntityOwners()">
    <Root>
      <xsl:for-each select="Entity[@Name != 'Claim']">
        <xsl:sort select="@Name" data-type="text" order="descending"/>
        <xsl:sort select="@ClaimAspectNumber" data-type="number" order="ascending"/>
        <Entity>
          <Name>
            <xsl:choose>
              <xsl:when test="@ClaimAspectNumber = 0">
                <xsl:value-of select="@Name"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat(@Name, ' ', @ClaimAspectNumber)"/>
              </xsl:otherwise>
            </xsl:choose>
          </Name>
          <Owner><xsl:value-of select="concat(@OwnerRepFirstName, ' ', @OwnerRepLastName)"/></Owner>
          <Analyst><xsl:value-of select="concat(@AnalystRepFirstName, ' ', @AnalystRepLastName)"/></Analyst>
          <Support><xsl:value-of select="concat(@SupportRepFirstName, ' ', @SupportRepLastName)"/></Support>
          <EntityNum><xsl:value-of select="@ClaimAspectNumber"/></EntityNum>
        </Entity>
      </xsl:for-each>
    </Root>
    </xml>
  </xsl:template>

</xsl:stylesheet>
