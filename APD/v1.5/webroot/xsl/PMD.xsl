<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:user="http://mycompany.com/mynamespace"
		xmlns:session="http://lynx.apd/session-manager"
		id="PMDMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSessionGetUserDetailXML,PMD.xsl, 'csr0901'   -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>
<xsl:param name="WindowID"/>
<xsl:param name="ShopID"/>
<xsl:param name="ClaimView"/>
<xsl:param name="LynxID"/>
<xsl:param name="AssignmentID"/>
<xsl:param name="BeginDate"/>

<xsl:include href="APDMenu.xsl"/>

<xsl:template match="/Root">

<xsl:variable name="Context" select="session:XslUpdateSession('Context','')"/>
<xsl:variable name="ViewCRD" select="session:XslUpdateSession('ViewCRD', string(User/DesktopPermission/@CRD))"/>
<xsl:variable name="ViewPMD" select="session:XslUpdateSession('ViewPMD', string(User/DesktopPermission/@PMD))"/>
<xsl:variable name="ViewUAD" select="session:XslUpdateSession('ViewUAD', string(User/DesktopPermission/@UAD))"/>
<xsl:variable name="ViewMED" select="session:XslUpdateSession('ViewMED', string(User/DesktopPermission/@MED))"/>

<!-- Make sure we have a valid user -->
<xsl:if test="count(User)=1 and User/DesktopPermission/@PMD=1">

	<xsl:variable name="UserID" select="User/@UserID"/>
	<xsl:variable name="SupervisorUserID" select="User/@SupervisorUserID"/>
	<xsl:variable name="SuperUserFlag" select="User/@SuperUserFlag"/>
	<xsl:variable name="SupervisorFlag" select="User/@SupervisorFlag"/>
	<xsl:variable name="RoleID" select="User/Role[@PrimaryRoleFlag = '1']/@RoleID"/>
	<xsl:variable name="RoleName" select="User/Role[@PrimaryRoleFlag = '1']/@Name"/>

  <xsl:value-of select="session:XslUpdateSession( 'UserID', string($UserID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRole', string($RoleName) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRoleID', string($RoleID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorUserID', string($SupervisorUserID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorFlag', string($SupervisorFlag) )"/>

	<HTML>

  <HEAD>
   <TITLE id="tTitle"><xsl:value-of select="$ShopID"/> Program Manager Desktop - <xsl:value-of select="$Environment"/></TITLE>
   <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
   <LINK rel="stylesheet" href="/css/Menu.css" type="text/css"/>
   <LINK rel="stylesheet" href="/css/officexp/Menu.css" type="text/css" id="menuStyleSheet"/>

   <script type="text/javascript" src="/js/browser.js"></script>
   <script type="text/javascript" src="/js/menu3.js"></script>
   <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
   <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
   </script>

    <!-- Page Specific Scripting -->
    <SCRIPT language="JavaScript">
   var gsShopID = "<xsl:value-of select='$ShopID'/>";
   var gsAssignmentID = "<xsl:value-of select='$AssignmentID'/>";
   var gsBeginDate = "<xsl:value-of select='$BeginDate'/>";
<![CDATA[
    //init the table selections, must be last
    function initPage(){
      self.resizeTo("1022", "699");
      var sUrl = gsShopID == "" ? "PMDMain.asp" : "PMDShopAssignment.asp?ShopLocationID=" + gsShopID;
      sUrl += (((gsShopID != "") && (gsAssignmentID != "")) ? "&AssignmentID=" + gsAssignmentID + "&BeginDate=" + gsBeginDate : "");
      document.frames["oIframe"].frameElement.src = sUrl;
      
      obj = document.getElementById("oIframe");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
    }

    function AdminMenu(bEnable, nMenu){
      var oMenu = document.getElementById("adminMenu");
  	  if (bEnable == true)
        oMenu.rows[nMenu-1].className="";
      else
        oMenu.rows[nMenu-1].className="disabled";
      forceRebuild(oMenu);
	  }

    function chkBeforeUnloadWindow(){
      if (oIframe.gbDirtyFlag == true)
          event.returnValue = "The information on this page has changed.";
    }

    if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
]]>
    </SCRIPT>

 </HEAD>

<BODY unselectable="on" onLoad="initPage()" onbeforeunload="chkBeforeUnloadWindow()" style="background-color:#336699;	color:#000000; font-family:Tahoma,Arial,Helvetica,sans-serif;	font-size:11px;	background-image:url(/images/fundo_default.gif); margin:0px; border:0px;">


 <TABLE unselectable="on" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
   <TR>
     <TD unselectable="on" width="100%" valign="top">
           
       <xsl:call-template name="APDMenu">  <!-- Menu bar -->
         <xsl:with-param name="Desktop">
           <xsl:choose>
             <xsl:when test="$ShopID=''">PMD</xsl:when>
             <xsl:otherwise>PGM</xsl:otherwise>
           </xsl:choose>
         </xsl:with-param>
         <xsl:with-param name="Permission"><xsl:value-of select="User/DesktopPermission/@Territory"/></xsl:with-param>
		   </xsl:call-template>
     </TD>
   </TR>
   <TR>
  	 <TD unselectable="on" width="100%">
       <IMG unselectable="on" src="/images/spacer.gif" width="1" height="4" border="0" alt=""/>
    </TD>
  </TR>
  <TR>
  	<TD unselectable="on" height="100%" width="100%">

      <IFRAME id="ifrmPMDShopSearch" name="oIframe" src="/blank.asp" style="width:100%; height:100%; border:0px; padding:2px; visibility:hidden; position:absolute; background:transparent; overflow:hidden" allowtransparency="true" >
      </IFRAME>

    </TD>
  </TR>
  <TR>
  	<TD unselectable="on">

     	<IMG unselectable="on" src="/images/spacer.gif" width="1" height="4" border="0" alt=""/>

    </TD>
  </TR>
  <TR>
  	<TD unselectable="on" width="100%">

			<img src="/images/apdlogo.gif" alt="" width="154" height="36" hspace="10" vspace="10" border="0"/>

    </TD>
  </TR>
</TABLE>

</BODY>
</HTML>



</xsl:if>

<xsl:if test="count(User)=0 or User/DesktopPermission/@PMD=0">
	<xsl:text>Please check your login information.  If you continue to have problems, contact your supervisor.</xsl:text>
</xsl:if>
</xsl:template>
</xsl:stylesheet>
