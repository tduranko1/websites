<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:js="urn:the-xml-files:xslt"
  xmlns:IE="http://mycompany.com/mynamespace">

  <xsl:import href="msxsl/msjs-client-library-htc_apdnet.js"/>
  <xsl:import href="msxsl/msxsl-function-library.xsl"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>

  <!-- Permissions params - names must end in 'CRUD' -->
  <!-- <xsl:param name="DocumentCRUD" select="Document"/> -->

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="InsCoId"/>
  <xsl:param name="LynxId"/>
  <xsl:param name="UserId"/>
  <xsl:param name="VehNum"/>

  <xsl:template match="/Root">
    <html>
      <head>
        <title>APD Custom Forms</title>

        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
        <link rel="stylesheet" href="/css/apdgrid3.css" type="text/css"/>

        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"/>

        <script language="JavaScript">

          var strLynxID = "<xsl:value-of select="$LynxId"/>";
          var strUserID = "<xsl:value-of select="$UserId"/>";
          var strVehNum = "<xsl:value-of select="$VehNum"/>";

          var strPolicyNumber = "<xsl:value-of select="/Root/@PolicyNumber"/>";
          var strClientClaimNumber = "<xsl:value-of select="/Root/@ClientClaimNumber"/>";
          var strInsCoID = "<xsl:value-of select="@InsuranceCompanyID"/>";
          var strLossState = "<xsl:value-of select="/Root/@LossState"/>";
          var strUserName = "<xsl:value-of select="/Root/@UserName"/>";
          var strUserPhone = "<xsl:value-of select="/Root/@UserPhone"/>";
          var strUserEmail = "<xsl:value-of select="/Root/@UserEmail"/>";

          var objDestinations = new Array();
          var objAttachments = new Array();

          var objFrames = new Array();
          var strFormDocumentType = "";
          var strFormEventID = "";
          var strVehicleNum = "";
          var blnAutoBundling = "0";
          var blnClaimLoopback = false;
          var strClaimAspectServiceChannelID = "";
          var strShopState = "";
          <!--Changes By glsd451-->
          var strDataSet = "";
          var strSequenceNumb = "";

          <![CDATA[
         function __pageInit(){
            try {
              stat1.style.top = (document.body.offsetHeight - 75) / 2;
              stat1.style.left = (document.body.offsetWidth - 240) / 2;
            } catch (e) {}
            
            // auto select the vehicle the user is assigned to
            var objVehicles = xmlData.selectNodes("/Root/Vehicle[@AnalystUserID='" + strUserID + "']");
            if (objVehicles && objVehicles.length == 1){
               csVehicles.value = objVehicles[0].getAttribute("ClaimAspectID");
               window.setTimeout("loadServiceChannel()", 100);
            } else {
               // no vehicle is assigned to this user. see if only vehicle is present for this claim.
               var objVehicles = xmlData.selectNodes("/Root/Vehicle");
               if (objVehicles && objVehicles.length == 1){
                  csVehicles.selectedIndex = 0;
                  window.setTimeout("loadServiceChannel()", 100);
               }
            }
        }
         
         function loadServiceChannel(){
             if(document.getElementById("csDataSet") != null)
             {            
             csDataSet.CCDisabled = true; 
             }
            if (csVehicles.readyState != "complete") {
               window.setTimeout("loadServiceChannel()", 100);
               return;
            }
            
            var strClaimAspectID = csVehicles.value;
			 
            strVehicleNum = csVehicles.text.split(":")[0];
            csServiceChannel.Clear();
            
            var objServiceChannels = xmlData.selectNodes("/Root/Vehicle[@ClaimAspectID='" + strClaimAspectID + "']/ServiceChannel");
            if (objServiceChannels){
               var iServiceChannels = objServiceChannels.length;
               for (var i = 0; i < iServiceChannels; i++){
                  csServiceChannel.AddItem(objServiceChannels[i].getAttribute("ServiceChannelCD"), 
                                           objServiceChannels[i].getAttribute("ServiceChannelDesc"));
               }
               
               if (iServiceChannels == 1)
                  csServiceChannel.selectedIndex = 0;
            }
         }
         
         function loadForms(){
            btnShow.CCDisabled = true;
            var strClaimAspectID = (csVehicles.selectedIndex == -1 ? "" : csVehicles.value);
            var strServiceChannelCD = (csServiceChannel.selectedIndex == -1 ? "" : csServiceChannel.value);
            //var strShopState = "";

            var objVehSC = xmlData.selectSingleNode("/Root/Vehicle[@ClaimAspectID = '" + strClaimAspectID + "']/ServiceChannel[@ServiceChannelCD = '" + strServiceChannelCD + "']");
	    var objOffice = xmlData.selectNodes("/Root")
            if (objVehSC) {
               strShopState = objVehSC.getAttribute("ShopState");
               strClaimAspectServiceChannelID = objVehSC.getAttribute("ClaimAspectServiceChannelID");
            }
            
            csForm.Clear();
            
            /*var objForms = xmlData.selectNodes("/Root/Form[(@LossStateCode = '' and @ShopStateCode = '' and @ServiceChannelCD = '') or " + 
                                                          "((@LossStateCode = '' or @LossStateCode = '" + strLossState + "') and " +
                                                          " (@ShopStateCode = '' or @ShopStateCode = '" + strShopState + "') and " +
                                                          " (@ServiceChannelCD = '' or @ServiceChannelCD = '" + strServiceChannelCD + "'))]");*/
														  
			      var invoiceId =  xmlData.selectSingleNode("/Root/Vehicle[@ClaimAspectID = '" + strClaimAspectID + "']").getAttribute("InvoiceID");

            // select the most matched form
            var objForms = xmlData.selectNodes("/Root/Form[(@ServiceChannelCD='' or @ServiceChannelCD='" + strServiceChannelCD + "') and (@LossStateCode='" + strLossState + "' and @ShopStateCode='" + strShopState + "')]");
            if (objForms){
               for (var i = 0; i < objForms.length; i++) {   
                  if (objForms[i].getAttribute("Name") == 'QBE Memo Bill') { 
					            if (invoiceId != null) {
						             csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name"));
					            }
				          }	else {	
					            csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name"));
				          }
               }
            }

            var objForms = xmlData.selectNodes("/Root/Form[(@ServiceChannelCD='' or @ServiceChannelCD='" + strServiceChannelCD + "') and (@LossStateCode='" + strLossState + "' and @ShopStateCode='')]");
            if (objForms) {
               for (var i = 0; i < objForms.length; i++) {
                  if (objForms[i].getAttribute("Name") == 'QBE Memo Bill') { 
					            if (invoiceId != null) {
						              if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
						                  csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
						              }
					            }
				          } else {	
					            if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
						              csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
						          }
				          }
               }
            }

            /*
            var objForms = xmlData.selectNodes("/Root/Form[(@ServiceChannelCD='' or @ServiceChannelCD='" + strServiceChannelCD + "') and (@LossStateCode='' and @ShopStateCode='" + strShopState + "')]");
            if (objForms) {
               for (var i = 0; i < objForms.length; i++) {
                  if (objForms[i].getAttribute("Name") == 'QBE Memo Bill') { 
					            if (invoiceId != null) {
						              if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
						                  csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
						              }
					            }
				          }	else	{
            					if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
              						csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
						          }
				          }
              }
           }
           */
           
            var objForms = xmlData.selectNodes("/Root/Form[(@ServiceChannelCD='' or @ServiceChannelCD='" + strServiceChannelCD + "') and (@LossStateCode='' and @ShopStateCode='')]");
            if (objForms) {
                for (var i = objForms.length - 1; i >= 0 ; i--)  {
				            if (objForms[i].getAttribute("Name") == 'QBE Memo Bill')	{ 
					              if (invoiceId != null)	{
						                if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
						                    csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
						                }
					              }
				            }	else	{
              					if ( strInsCoID == 387) {
						                if( objForms[i].getAttribute("Name") == 'Westfield BCIF Request') {
							                  if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
								                    csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
                								}
						                }						
					              }	else	{
						                if (csForm.GetValueFromText(objForms[i].getAttribute("Name")) == null) {
                                if (strInsCoID == 515 && objForms[i].getAttribute("Name") == 'BCIF Request' ) {
                                } else {
					if((objOffice[0].getAttribute("NameFirst") == "AOI" && objOffice[0].getAttribute("NameLast") == "Quick Claims" && objForms[i].getAttribute("Name") == "AOI DA Agreed 1st Party Notification") || objForms[i].getAttribute("Name") != "AOI DA Agreed 1st Party Notification")
                                    		csForm.AddItem(objForms[i].getAttribute("FormID"), objForms[i].getAttribute("Name")); 
                                    //alert(objForms[i].getAttribute("Name"));            
                                }
							              }
					              }
				            }
                }
              }
            }
         
           
         function enableForm(){                              
            var strcsFormtext = csForm.text;
            if(strcsFormtext == "Desk Audit Summary Report")
            {           
            if(document.getElementById("csDataSet") == null)
             {
              btnShow.CCDisabled = true; 
              ClientWarning("The Custom Forms is disabled since there is no Estimate/Supplement documents.")
             }
             else
             {
              btnShow.CCDisabled = false; 
             }
            }
            else
            {
              btnShow.CCDisabled = false; 
            }
         }                       
        
        function enableFormDA(){
        btnShow.CCDisabled = true;  
         var strDSSelectedText = csForm.text;       
         var strSelectedVechicle = document.getElementById("csVehicles").value;
         if(strDSSelectedText == "Desk Audit Summary Report")
         {
                csDataSet.CCDisabled = false;   
                csDataSet.selectedIndex = -1;       
                 btnShow.CCDisabled = true;  
               
         }
         else
         {
            btnShow.CCDisabled = false;
         }
         if(strDSSelectedText != "Desk Audit Summary Report")
         {
           csDataSet.selectedIndex = -1;
           csDataSet.CCDisabled = true;
           btnShow.CCDisabled = false;
         }
         
          var objDocuments = xmlData.selectNodes("/Root/DataSet[@ClaimAspectID='" + strSelectedVechicle + "']");
  
            if (objDocuments)
            {
               var iDocuments = objDocuments.length;
             
               csDataSet.Clear();
               for (var i = 0; i < iDocuments; i++)
               {              
                 if (objDocuments[i].getAttribute("Name") == "Estimate" && objDocuments[i].getAttribute("ClaimAspectID") == strSelectedVechicle)
                 {
                 csDataSet.AddItem(objDocuments[i].getAttribute("Name"),objDocuments[i].getAttribute("Name"));
                 }
               
                 if (objDocuments[i].getAttribute("Name") == "Supplement" && objDocuments[i].getAttribute("ClaimAspectID") == strSelectedVechicle)
                 {
                  csDataSet.AddItem(objDocuments[i].getAttribute("Name")+ ":" + objDocuments[i].getAttribute("SupplementSeqNumber"),objDocuments[i].getAttribute("Name")+ ":" + objDocuments[i].getAttribute("SupplementSeqNumber"));
                 }  
               }
             }
         
        }        
         
         function showFormData(){
            var strServiceChannelCD = (csServiceChannel.selectedIndex == -1 ? "" : csServiceChannel.value);
            //var strShopState = "";
            objFrames = new Array();
            
            objFrames = new Array();
            divFormView.innerHTML = "";
            
            objDestinations = new Array();
            objAttachments = new Array();
            blnClaimLoopback = false;
            
            if (csForm.selectedIndex != -1){
               var strFormID = csForm.value;
               
               blnClaimLoopback = false;
               
               var objForm = xmlData.selectSingleNode("/Root/Form[@FormID = '" + strFormID + "']");
               if (objForm){
                  strFormDocumentType = objForm.getAttribute("DocumentTypeName");
                  strFormEventID = (objForm.getAttribute("EventID") ? objForm.getAttribute("EventID") : "");
                  blnAutoBundling = objForm.getAttribute("AutoBundlingFlag");
                  
                  //btnPreview.CCDisabled = (blnAutoBundling == "1");

                  //show the parent form
                  buildFormView(strFormID, "", objForm.getAttribute("Name"), objForm.getAttribute("HTMLPath"));
                  //show supplement forms
                  var objForms = xmlData.selectNodes("/Root/Form[@FormID='" + strFormID + "']/Supplement[(@LossStateCode = '' and @ShopStateCode = '' and @ServiceChannelCD = '') or " + 
                                                                                                       "((@LossStateCode = '' or @LossStateCode = '" + strLossState + "') and (@ShopStateCode = '' or @ShopStateCode = '" + strShopState + "') and (@ServiceChannelCD = '' or @ServiceChannelCD = '" + strServiceChannelCD + "'))]");
                  if (objForms){
                     for (var i = 0; i < objForms.length; i++) {
                        buildFormView(strFormID, 
                                      objForms[i].getAttribute("FormSupplementID"), 
                                      objForms[i].getAttribute("Name"), 
                                      objForms[i].getAttribute("HTMLPath"));
                     }
                  }
               }
            }
         }
         
         function previewForm(){
            if (YesNoMessage("Confirmation", "This can sometime take few minutes. \n\nDo you want to continue?") == "Yes"){
               var objXML = getData();

               /*if (objXML)
                  window.clipboardData.setData("Text", objXML.xml);
               return;*/

               if (typeof(objXML) == "object"){
                  enablePreviewAndSave(false);
                  divFormName.innerText = csForm.text + " Preview:";
                  divPreview.style.display = "inline";
                  stat1.Show("Please wait...");
                  //alert(document.getElementById("ifrmGen").style);
                  document.getElementById("ifrmGen").style.visibility = "hidden";
                  formGen.txtAction.value = "preview";
                  //alert(objXML.xml);
                  formGen.txtXML.value = objXML.xml;
                  formGen.submit();
               }
            }
         }
         
         function saveForm(){
            var objXML = getData();

            if (typeof(objXML) == "object"){
              /* if (objXML)
                  window.clipboardData.setData("Text", objXML.xml);
               return;*/
               stat1.Show("Please wait...");
               enablePreviewAndSave(false);
               document.getElementById("ifrmGen").style.visibility = "hidden";
               formGen.txtAction.value = "";
               //alert(objXML.xml);
               formGen.txtXML.value = objXML.xml;
               formGen.submit();
            }
         }
         
         function closeDialog(){
            window.close();
         }
         
         function buildFormView(strFormID, strSupplementID, strFormName, strHTMLPath){
            var strHTML = "";
            var strMainFormColor = "#6495ED";
            var strSuppFormColor = "#B0C4DE";
            var strFrameID = "ifrm" + "_" + strFormID + "_" + strSupplementID;
            
            
            strHTML += "<div id='divForm" + strFormID + "' style='font:bold;background-color:" + (strSupplementID != "" ? strSuppFormColor : strMainFormColor) + ";color:#FFFFFF;padding:5px;'>" + strFormName + "</div>" +
                        "<div><IFRAME id='" + strFrameID + "' src='" + (strHTMLPath == "" ? "blank.asp" : strHTMLPath)+ "'  onreadystatechange='resizeMe(this)' style='width:100%; height:100px; top:0px; left:0px;' allowtransparency='true'></IFRAME></div>";
            
            divFormView.insertAdjacentHTML("beforeEnd", strHTML);
            var objForm = document.getElementById(strFrameID);
            if (objForm)
               objFrames.push(objForm);
            else 
               alert("Cannot get a handle of the frame.");
         }
         
         function resizeForms(objForm){
            if (objForm){
               var iFrmHeight = objForm.contentWindow.document.body.scrollHeight + 50;
               objForm.style.height = iFrmHeight;
            }
         }
         
         function initForm(objForm){
               var strCurFrameID = objForm.id;               
                var strCsDataSet = "";                
                var DataSetText = "";
               if(document.getElementById("csDataSet") == null)
               {
               strCsDataSet = "";
               }
               else
               {
               strCsDataSet = csDataSet.value     
               DataSetText = csDataSet.text.split(":")[1];              
               }
               if(DataSetText == undefined)
               {               
                DataSetText = "";                
               }
                                           
               var iFrmHeight = objForm.contentWindow.document.body.scrollHeight + 50;
               var iFrmHeight2 = objForm.contentWindow.document.body.offsetHeight + 50;

               enablePreviewAndSave(true);

               objForm.style.height = iFrmHeight;
               var strIFrameID = objForm.id.split("_");
               strFormID = csForm.value;
               strSupplementFormID = strIFrameID[2];
               strPath = "/Root/Form[@FormID = '" + strFormID + "']";
               if (strSupplementFormID != "")
                  strPath += "/Supplement[@FormSupplementID = '" + strSupplementFormID + "']";
               
               //if (strSupplementFormID == "") objFrames = new Array();             
               
              //Changes by glsd451
              
               var objXML = xmlData.selectSingleNode(strPath);
		
               if (objXML){              
                  if (typeof(objForm.contentWindow.setFormParam) == "function"){
                     var obj = { UserID: strUserID,
                                 LynxID: strLynxID,
                                 Vehicles: (csVehicles.selectedIndex == -1 ? "" : csVehicles.text),
                                 ClaimAspectID: (csVehicles.selectedIndex == -1 ? "" : csVehicles.value),
                                 ServiceChannelCD : (csServiceChannel.selectedIndex == -1 ? "" : csServiceChannel.value),
                                 DataSet: strCsDataSet,
                                 SequenceNumber: DataSetText,
                                 FormID: strFormID,
                                 SupplementFormID: strSupplementFormID,
                                 SP: objXML.getAttribute("SQLProcedure"),
                                 PDFform: objXML.getAttribute("PDFPath"),
                                 dataMining: (strSupplementFormID != "" ? 0 : objXML.getAttribute("DataMiningFlag")),
                                 iFrameObj: objForm
                               };
                     objForm.contentWindow.setFormParam(obj);
                     /*var iFrames = objFrames.length;
                     var blnFound = false;
                     for (var i = 0; i < iFrames; i++){
                        if (objFrames[i].id == strCurFrameID)
                           blnFound = true;
                     }
                     if (blnFound == false)
                        objFrames.push(objForm);*/
                  }
               } else {
                  ClientWarning("Unable to retrieve the form UI data. Contact Helpdesk.")
               }               
         }
         
         function resizeMe(objForm){
            if (objForm.readyState == "complete"){
               initForm(objForm);
            }
         }
         
         function enablePreviewAndSave(blnEnabled){
            //alert(blnEnabled);
            btnSave.CCDisabled = !(blnEnabled);
            btnPreview.CCDisabled = !(blnEnabled);
            //btnPreview.CCDisabled = (blnAutoBundling == "1");
         }
         
         function getData(){
            var objXML = document.createElement("XML");
            var objRoot;
            var objFormData;
            objXML.loadXML("<CustomForm/>");
            objRoot = objXML.selectSingleNode("/CustomForm");
            if (objRoot) {
               //add processing instructions
               objInstructions = objXML.createElement("ProcessingInstructions");
               objInstructions.setAttribute("filename", csForm.text + "_" + strLynxID + strClaimAspectServiceChannelID + ".pdf");
               objAPDNode = objXML.createElement("APD");
               if (strFormDocumentType) objAPDNode.setAttribute("documentType", strFormDocumentType);
               if (strFormEventID) objAPDNode.setAttribute("eventID", strFormEventID);
               if (strUserID) objAPDNode.setAttribute("userid", strUserID);
               if (strLynxID) objAPDNode.setAttribute("Lynxid", strLynxID);
               if (strVehicleNum) objAPDNode.setAttribute("vehNum", strVehicleNum);
               objAPDNode.setAttribute("PolicyNumber", strPolicyNumber);
               objAPDNode.setAttribute("ClientClaimNumber", strClientClaimNumber);
               objAPDNode.setAttribute("ClaimAspectServiceChannelID", strClaimAspectServiceChannelID);   
               if (blnAutoBundling == "1") objAPDNode.setAttribute("direction", "O");
               objInstructions.appendChild(objAPDNode);
               if (objAttachments != null){
                  if (objAttachments.length > 0) xmlAttachments = objXML.createElement("Attachments");
                  for (var i = 0; i < objAttachments.length; i++){
                     if (objAttachments[i].attachmentPath != ""){
                        if (objAttachments[i].attachmentPath == "embed"){
                           objAttachment = objXML.createElement("Attachment");
                           objAttachment.setAttribute("path") = "embed";
                           objAttachment.appendChild(objXML.createCDATASection(objAttachments[i].embedData));
                           xmlAttachments.appendChild(objAttachment);
                        } else {
                           objAttachment = objXML.createElement("Attachment");
                           objAttachment.setAttribute("path") = objAttachments[i].attachmentPath;
                           xmlAttachments.appendChild(objAttachment);
                        }
                     }
                  }
                  if (objAttachments.length > 0) objInstructions.appendChild(xmlAttachments);
               }

               if (objDestinations != null){
                  if (objDestinations.length > 0) xmlDestination = objXML.createElement("Destinations");
                  for (var i = 0; i < objDestinations.length; i++){
                     switch (objDestinations[i].destinationType){
                        case "FAX":
                           objFax = objXML.createElement("Fax");
                           objFax.setAttribute("name") = objDestinations[i].destinationName;
                           objFax.setAttribute("number") = objDestinations[i].destinationValue;
                           xmlDestination.appendChild(objFax);
                           break;
                        case "EMAIL":
                           objEmail = objXML.createElement("Email");
                           objEmail.setAttribute("from") = objDestinations[i].EmailFrom;
                           objEmail.setAttribute("to") = objDestinations[i].EmailTo;
                           objEmail.setAttribute("replyto") = objDestinations[i].EmailFrom;
                           objEmail.setAttribute("subject") = objDestinations[i].EmailSubject;
                           var objEmailBody = objXML.createElement("Body");
                           objEmailBody.appendChild(objXML.createCDATASection(objDestinations[i].EmailBody));
                           objEmail.appendChild(objEmailBody);
                           xmlDestination.appendChild(objEmail);
                           break;
                        case "FILE":
                           objCopy = objXML.createElement("File");
                           objCopy.setAttribute("path") = objDestinations[i].destinationValue;
                           objCopy.setAttribute("overwrite") = objDestinations[i].destinationOverwrite + "";
                           xmlDestination.appendChild(objCopy);
                     }
                  }
                  if (objDestinations.length > 0) objInstructions.appendChild(xmlDestination);
               }
               objRoot.appendChild(objInstructions);
               var iFrames = objFrames.length;
               var blnFormDataValid = true;
               for (var i = 0; i < iFrames; i++){
                  blnFormDataValid = true;
                  if (typeof(objFrames[i].contentWindow.validateFormData) == "function")
                     blnFormDataValid = objFrames[i].contentWindow.validateFormData();
                     
                  if (blnFormDataValid == false){
                     //Form data is invalid. Form will take care of throwing the necessary warning dialog. 
                     // We will just exit.
                     //alert("Form invalid");
                     return;
                  }
                  objFormData = null;
                  if (typeof(objFrames[i].contentWindow.getFormData) == "function"){
                     objFormData = objFrames[i].contentWindow.getFormData("customForms");
                  }
                  if (objFormData){
                     objRoot.appendChild(objFormData.firstChild.firstChild);
                  }
               }
            }
            return objXML;
         }
         function closePreview(){
            divPreview.style.display = "none";
            enablePreviewAndSave(true);
         }
         
         function showPreviewFrame(){
            var obj = document.getElementById("ifrmGen")
            if (obj.readyState == "complete"){
               obj.style.visibility = "visible";
               enablePreviewAndSave(false);
               try {
               stat1.Hide();
               } catch (e) {}
            }
         }
         
         function linkForms(){
            for (var i = 0; i < objFrames.length; i++){
               if (typeof(objFrames[i].contentWindow.linkForm) == "function"){
                  objFrames[i].contentWindow.linkForm();
               }
            }
         }
         
         function clearDestinations(){
            objDestinations = new Array();
         }
         
         function addFaxDestination(strName, strNumber){
            objDestinations.push({
                                    destinationType: "FAX",
                                    destinationName: strName,
                                    destinationValue: strNumber
                                 });
         }
         
         function addEmailDestination(strFrom, strTo, strSubject, strBody){
            debugger;
            objDestinations.push({
                                    destinationType: "EMAIL",
                                    EmailFrom: strFrom,
                                    EmailTo: strTo,
                                    EmailSubject: strSubject,
                                    EmailBody: strBody
                                 });
         }

         function addFileCopyDestination(strFolder, blnOverwrite){
            objDestinations.push({
                                    destinationType: "FILE",
                                    destinationName: "",
                                    destinationValue: strFolder,
                                    destinationOverride: blnOverwrite
                                 });
         }
         
         function clearAttachments(){
            objAttachments = new Array();
         }
         
         function addAttachment(strAttachment){
            objAttachments.push({
                                 attachmentPath: strAttachment
                                });
         }
         
         function addClaimInfoAttachment(){
            objAttachments.push({   attachmentPath: "embed",
                                    embedData: "<APD documenttype=\"" + strFormDocumentType + "\" " +
                                                    "userid=\"" + strUserID + "\" " +
                                                    "Lynxid=\"" + strLynxID + "\" " +
                                                    "vehNum=\"" + strVehicleNum + "\" " +
                                                    "direction=\"O\" " +
                                                    "filename=\"$JOBFILE$\" " +
                                                    "PolicyNumber=\"" + strPolicyNumber + "\" " +
                                                    "ClientClaimNumber=\"" + strClientClaimNumber + "\" " +
                                                    "/>"
                                });
         }
         
         function addClaimLoopback(){
            blnClaimLoopback = true;
         }
         
        ]]>
        </script>

      </head>
      <body unselectable="on" scroll="no" leftmargin="10" topmargin="10" bottommargin="5" rightmargin="5" >
        <IE:APDStatus class="APDStatus" id="stat1" name="stat1" width="240" height="75"/>
        <table border="0" cellpadding="2" cellspacing="0" style="width:100%;table-layout:fixed">
          <colgroup>
            <col width="110px"/>
            <col width="*"/>
            <col width="110px"/>
          </colgroup>
          <tr>
            <td>LYNX Id:</td>
            <td colspan="2">
              <xsl:value-of select="@LynxID"/>
              <b>
                <font color="#D8D8D8">
                  &#160;&#160;&#160;&#160;<xsl:value-of select="RunTime"/>
                </font>
              </b>
            </td>
          </tr>
          <tr>
            <td>Vehicle:</td>
            <td colspan="2">
              <IE:APDCustomSelect id="csVehicles" name="csVehicles" value="" displayCount="5" width="250" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="false" onChange="loadServiceChannel()">
                <xsl:for-each select="Vehicle">
                  <xsl:sort select="@ClaimAspectNumber" data-type="number" order="ascending"/>
                  <IE:dropDownItem value="" style="display:none">
                    <xsl:attribute name="value">
                      <xsl:value-of select="@ClaimAspectID"/>
                    </xsl:attribute>
                    <xsl:choose>
                      <xsl:when test="@OwnerBusinessName != ''">
                        <xsl:value-of select="concat(@ClaimAspectNumber, ': ', @OwnerBusinessName)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat(@ClaimAspectNumber, ': ', @OwnerNameFirst, ' ', @OwnerNameLast)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Service Channel:</td>
            <td colspan="2">
              <IE:APDCustomSelect id="csServiceChannel" name="csServiceChannel" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" required="false" width="150" onChange="loadForms()">
                <!-- <IE:dropDownItem value="" imgSrc="" style="display:none"></IE:dropDownItem> -->
              </IE:APDCustomSelect>
            </td>
          </tr>

          <!--Changes By glsd451-->
          <xsl:choose>
            <xsl:when test="not(DataSet[@ServiceChannelCD= 'DA'])">
              <tr>
                <td>Form:</td>
                <td>
                  <IE:APDCustomSelect id="csForm" name="csForm" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="3" required="false" width="250" onChange="enableForm()">
                    <!-- load only those forms that are neutral -->
                    <xsl:for-each select="Form[@LossStateCode = '' and @ShopStateCode = '' and @ServiceChannelCD = '']">
                      <xsl:sort select="@Name" data-type="text" order="ascending"/>
                      <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@FormID"/>
                        </xsl:attribute>
                        <xsl:value-of select="@Name"/>
                      </IE:dropDownItem>
                    </xsl:for-each>
                  </IE:APDCustomSelect>
                </td>
                <td>
                  <div style="position:absolute;top:68px;left:400px">
                    <IE:APDButton id="btnShow" name="btnShow" value="Show Form" width="" CCDisabled="false" CCTabIndex="4" onButtonClick="showFormData()"/>
                  </div>
                </td>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <tr>
                <td>Form:</td>
                <td>
                  <IE:APDCustomSelect id="csForm" name="csForm" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="3" required="false" width="250" onChange="enableFormDA()">
                    <!-- load only those forms that are neutral -->
                    <xsl:for-each select="Form[@LossStateCode = '' and @ShopStateCode = '' and @ServiceChannelCD = '']">
                      <xsl:sort select="@Name" data-type="text" order="ascending"/>
                      <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@FormID"/>
                        </xsl:attribute>
                        <xsl:value-of select="@Name"/>
                      </IE:dropDownItem>
                    </xsl:for-each>
                  </IE:APDCustomSelect>
                </td>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="DataSet[@ServiceChannelCD= 'DA']">
            <tr>
              <td>Data Set:</td>

              <td>
                <IE:APDCustomSelect id="csDataSet" name="csDataSet" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="3" required="false" width="250" onChange="enableForm()">
                  <!-- load only those forms that are neutral -->
                  <!--<xsl:for-each select="DataSet[@DuplicateFlag='0']">
                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value">
                        <xsl:value-of select="@Name"/>
                      </xsl:attribute>
                      <xsl:choose>
                        <xsl:when test="@Name= 'Estimate'">
                          <xsl:value-of select="@Name"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="concat(@Name,':',@SupplementSeqNumber)"/>
                        </xsl:otherwise>
                      </xsl:choose>-->
                  <!--</IE:dropDownItem>
                  </xsl:for-each>-->
                </IE:APDCustomSelect>
              </td>
              <td>
                <div style="position:absolute;top:89px;left:400px">
                  <IE:APDButton id="btnShow" name="btnShow" value="Show Form" width="" CCDisabled="false" CCTabIndex="4" onButtonClick="showFormData()"/>
                </div>
              </td>
            </tr>
          </xsl:if>
          <!--New Selector Added by glsd451-->
          <tr>
            <td colspan="3">Data:</td>
          </tr>
          <tr>
            <td colspan="3">
              <div id="divFormView" style="height:312px;width:780px;border:1px solid #C0C0C0;overflow:auto">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <IE:APDButton id="btnPreview" name="btnPreview" value="Preview" width="" CCDisabled="true" CCTabIndex="6" onButtonClick="previewForm()"/>
            </td>
            <td colspan="2">
              <div style="position:absolute;top:446px;left:670px">
                <IE:APDButton id="btnSave" name="btnSave" value="Save" width="" CCDisabled="true" CCTabIndex="7" onButtonClick="saveForm()"/>
                <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="" CCDisabled="false" CCTabIndex="8" onButtonClick="closeDialog()"/>
              </div>
            </td>
          </tr>
        </table>
        <form id="formGen" name="formGen" method="POST" action="CustomFormProcess.asp" target="ifrmGen">
          <input type="hidden" name="txtAction" id="txtAction"/>
          <input type="hidden" name="txtXML" id="txtXML"/>
        </form>
        <div id="divPreview" name="divPreview" style="position:absolute;top:8px;left:8px;background-color:#FFFFFF;display:none">
          <table border="0" cellpadding="2" cellspacing="0">
            <tr>
              <td>
                <div id="divFormName">Preview:</div>
              </td>
              <td style="text-align:right">
                <button onclick="closePreview()" style="margin:0px;padding:0px;background-color:transparent;border:0px;" title="Go back">
                  <img src="images/arrowleft.gif"/>
                </button>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <iframe name="ifrmGen" id="ifrmGen" height="440px" width="790px" scrolling="yes" onreadystatechange="showPreviewFrame()"></iframe>
              </td>
            </tr>
          </table>
        </div>
        <xml id="xmlData">
          <xsl:copy-of select="/Root"/>
        </xml>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
