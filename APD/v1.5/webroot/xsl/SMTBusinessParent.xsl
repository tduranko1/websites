<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTBusinessParent">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTBusinessParentGetDetailXML,SMTBusinessParent.xsl,0   -->

<HEAD>
  <TITLE>Business Parent</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT LANGUAGE="javascript">
top.layerEntityData.style.visibility = "hidden";

if (parent.gsMode != "parentwizard"){		<!-- don't set these in 'wizard' modes as it destroys state -->
  top.gsCancelSearchType = "P";
  top.gsBusinessParentID = '<xsl:value-of select="@BusinessParentID"/>';
}

var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function InitPage(){
  if (frmParent.selBusParent && frmParent.txtBusinessParentID.value == '0') 
    frmParent.selBusParent.selectedIndex = -1;
    
  if (parent.gsMode == "parentwizard"){
    parent.btnCancel.style.display = "inline";
    parent.btnCancel.style.visibility = "visible";
	  parent.btnCancel.disabled = false;
  }
  parent.btnDelete.style.display = "none";

  if (gsShopCRUD.indexOf("U") > -1){
    parent.btnSave.style.visibility = "visible";
    parent.btnSave.disabled = false;
  }
  else
    parent.btnSave.style.visibility = "hidden";

  onpasteAttachEvents()

  var aInputs = null;
  var obj = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
    if (aInputs[i].type=="text"){
      aInputs[i].attachEvent("onchange", attachDirty);
      aInputs[i].attachEvent("onkeypress", attachDirty);
      aInputs[i].attachEvent("onpropertychange", attachDirty);
      if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
    }
  }
  aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
    aInputs[i].attachEvent("onchange", attachDirty);
    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }
}

function Cancel(){
	parent.btnCancel.disabled = true;
	parent.btnSave.disabled = true;
	parent.window.navigate(top.CancelURL());
}

function selBusParent_onchange(){
	window.navigate("SMTBusinessParent.asp?BusinessParentID=" + frmParent.selBusParent.value);
}

function txtAddressZip_onBlur(){
	IsValidZip(frmParent.txtAddressZip);
  ValidateZip(frmParent.txtAddressZip, 'selState', 'txtAddressCity');
  ValidateZipToCounty(frmParent.txtAddressZip, 'txtAddressCounty');
}

function ValidateAllPhones() {
  if (validatePhoneSections(document.all.txtPhoneAreaCode, document.all.txtPhoneExchangeNumber, document.all.txtPhoneUnitNumber) == false) return false;
  if (validatePhoneSections(document.all.txtFaxAreaCode, document.all.txtFaxExchangeNumber, document.all.txtFaxUnitNumber) == false) return false;
  if (isNumeric(document.all.txtPhoneExtensionNumber.value) == false){
      parent.ClientWarning("Invalid numeric value specified. Please try again.");
      document.all.txtPhoneExtensionNumber.focus();
      document.all.txtPhoneExtensionNumber.select();
      return false;
  }
  if (isNumeric(document.all.txtFaxExtensionNumber.value) == false){
      parent.ClientWarning("Invalid numeric value specified. Please try again.");
      document.all.txtFaxExtensionNumber.focus();
      document.all.txtFaxExtensionNumber.select();
      return false;
  }
  validateAreaCodeSplit(document.all.txtPhoneAreaCode, document.all.txtPhoneExchangeNumber);
  validateAreaCodeSplit(document.all.txtFaxAreaCode, document.all.txtFaxExchangeNumber);
  return true;
}

function attachDirty() {
  parent.gbDirtyFlag = true;
}

function IsDirty(){
	Save();
}

function SetDirty(){
  attachDirty();  // added for compatibility
}

function Save(){
  if (frmParent.txtName.value == ""){
      parent.ClientWarning("A value is required in the \"Name\" field.");
      frmParent.txtName.focus();
      return false;
  }
  if (ValidateAllPhones() == false) return false;
  parent.btnSave.disabled = true;
  parent.btnCancel.disabled = true;
  var mode = frmParent.txtBusinessParentID.value == '0' ? "insert" : "update"
  frmParent.action += "?mode=" + mode;
  frmParent.submit();
  parent.gbDirtyFlag = false;
}

]]>
</SCRIPT>

<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
</HEAD>

<BODY class="bodyAPDsubSub" onload="InitPage()" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<div style="background-color:#FFFAEB; height:475px;">

<IMG src="/images/spacer.gif" width="6" height="12" border="0" />

<form name="frmParent" method="post">
  <!-- Render Business Info Data -->
  <xsl:apply-templates select="BusinessParent"><xsl:with-param name="ShopCRUD" select="$ShopCRUD"/></xsl:apply-templates>

  <input type="hidden" name="BusinessParentID" id="txtBusinessParentID">
    <xsl:attribute name="value"><xsl:value-of select="@BusinessParentID"/></xsl:attribute>
  </input>

  <input type="hidden" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="BusinessParent/@SysLastUpdatedDate"/></xsl:attribute>
  </input>
  <input type="hidden" name="SysLastUserID">
    <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtCallBack" name="CallBack"/>
</form>

<IMG src="/images/spacer.gif" width="1" height="4" border="0"/>
</div>

</BODY>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</HTML>

</xsl:template>


<xsl:template match="BusinessParent">
  <xsl:param name="ShopCRUD"/>

  <xsl:if test="contains($ShopCRUD, 'U')">
    <table>
      <tr>
        <td width="10"></td>
        <td width="70">Parent:</td>
        <td>
          <select id="selBusParent" onchange="selBusParent_onchange()">
            <xsl:for-each select="/Root/Reference[@ListName='Parent']">
		          <xsl:call-template name="BuildSelectOptions">
		            <xsl:with-param name="current" select="/Root/@BusinessParentID"/>
		          </xsl:call-template>
		        </xsl:for-each>
          </select>
      </td>
    </tr>
  </table>
  </xsl:if>

<table border="0">
  <tr>
    <td>
      <table>
        <tr>
		      <td><strong><span>*</span></strong></td>
          <td width="70">Name:</td>
          <td>
            <input type="text" name="Name" id="txtName" class="inputFld" style="width:200">
			        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessParent']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		        </input>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>Address1:</td>
          <td>
            <input type="text" name="Address1" id="txtAddress1" class="inputFld" style="width:200">
			        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessParent']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>Address2:</td>
          <td>
            <input type="text" name="Address2" id="txtAddress2" class="inputFld" style="width:200">
			        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessParent']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>Zip:</td>
          <td>
            <input type="text" name="AddressZip" id="txtAddressZip" class="inputFld" maxlength="5" onblur="txtAddressZip_onBlur()" onkeypress="NumbersOnly(window.event)">
			        <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>City:</td>
          <td>
            <input type="text" name="AddressCity" id="txtAddressCity" class="inputFld">
			        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessParent']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>State:</td>
          <td>
            <select name="AddressState" id="selState" class="inputFld">
			        <option></option>
              <xsl:for-each select="/Root/Reference[@ListName='State']">
			          <xsl:call-template name="BuildSelectOptions">
				          <xsl:with-param name="current"><xsl:value-of select="/Root/BusinessParent/@AddressState"/></xsl:with-param>
			          </xsl:call-template>
			        </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
		      <td></td>
          <td>County:</td>
          <td>
            <input type="text" name="AddressCounty" id="txtAddressCounty" class="inputFld">
				      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessParent']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
			      </input>
          </td>
        </tr>
		    <tr>
          <td colspan="3"><strong><SPAN >*</SPAN> Required</strong></td>
        </tr>
      </table>
    </td>
	  <td width="20px"></td> <!-- spacer column -->
    <td valign="top">
      <table>
        <tr>
          <td width="70">Phone:</td>
          <td nowrap="nowrap" align="left">
            <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
          </td>
        </tr>
        <tr>
          <td>Fax:</td>
          <td nowrap="nowrap" align="middle">
            <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</xsl:template>

</xsl:stylesheet>
