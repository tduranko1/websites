<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDMarketAnalysis">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDMarketAnalysisGetListXML,PMDMarketAnalysis.xsl,35   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>
  
  <!-- CLIENT SCRIPTS -->
<!--   <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT> -->
  <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
  
  <SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
            document.onhelp=function(){  
  		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,23);
  		  event.returnValue=false;
  		  };	
  </script>
  
  
  <!-- Page Specific Scripting -->
  <SCRIPT language="JavaScript">
  var gsProgramManagerUserID = '<xsl:value-of select="@ProgramManagerUserID"/>';
  var bDataAvailable = false;
  var recCount = 0;
  var iCurPage = 0;
  var iPgCount = 0;
  var iPageSize = 25;
  <![CDATA[

  //init the table selections, must be last
  function initPage(){
  	divShopList.scrollTop = parent.gsMAScroll;
    btnFirst.value = unescape("%u2039") + unescape("%u2039");
    btnPrev.value = unescape("%u2039");
    btnNext.value = unescape("%u203A");
    btnLast.value = unescape("%u203A") + unescape("%u203A");
    
    divFilter.style.visibility = "";
    divFilter.style.top = document.body.offsetHeight - divFilter.offsetHeight - divPages.offsetHeight - 10;
    divFilter.style.left = (document.body.offsetWidth - divFilter.offsetWidth) / 2;
    divFilter.style.display = "none";
    if (typeof(parent.hideProgress) == "function")
      parent.hideProgress();
  }
  
  //update the selected elements from the table row
  function GridSelect(oRow){
      var srcElement = event.srcElement;
      var iCount = 10;
      while (srcElement.tagName != "TR" && iCount > 0)
        srcElement = srcElement.parentElement;
        
      var oTR = srcElement;
      var iNumber = parseInt(oTR.cells[0].innerText, 10);
      
      if (iNumber > 0) {
        var sShopLocationID;
        var sShopID;
        if (xmlSearchData){
          var oShopNode = xmlSearchData.selectSingleNode("/Root/Shop[@Number=" + iNumber + "]");
          if (oShopNode) {
            sShopLocationID = oShopNode.getAttribute("ShopLocationID");
            
            NavToPMDDetail(sShopLocationID);
          }
        }
      }
      event.cancelBubble = true;
      event.returnValue = false;
    //parent.window.navigate("PMDShopLevel.asp?ShopLocationID=" + strShopLocationID + "&DistrictManagerUserID=" + parent.selDistrictMgr.value + "&ProgramManagerUserID=" + gsProgramManagerUserID + "&Scroll=" + divShopList.scrollTop);
  }
  
  function setTableSource(oXML){
    try {
    if (oXML) {
      oXML.setProperty("SelectionLanguage", "XPath");
      recCount = oXML.XMLDocument.firstChild.childNodes.length;
      tblSort1.dataSrc = "#" + oXML.id;
      bDataAvailable = true;
      tblSort1.onreadystatechange = tblDataReadyStateChange;
      if (recCount > 0) {
        divShopList.style.display = "inline";
        divPages.style.display = "inline";
        divNoData.style.display = "none";
        iCurPage = 1;
        iPgCount = Math.ceil(recCount / iPageSize);
        txtPage.value = iCurPage + " of " + iPgCount;
        disablePageNav(false);
      } else {
        if (oXML.id == "xmlSearchFiltered") {
          divNoData.innerHTML = "Filter resulted in 0 records.";
        } else {
          divPages.style.display = "none";
          divNoData.innerHTML = "No data to display.";
        }
        txtPage.value = "0 of 0";
        divShopList.style.display = "none";
        divNoData.style.display = "inline";
        disablePageNav(true);
      }
    }
    } catch(e) {alert(e.description);}
  }
  
  function disablePageNav(bDisabled){
    btnFirst.disabled = btnPrev.disabled = btnNext.disabled = btnLast.disabled = txtPage.disabled = btnGoPage.disabled = bDisabled;
  }
  
  function tblDataReadyStateChange(){
    if (!bDataAvailable) return;
    if (tblSort1) {
      //alert(tblSort1.tBodies[0].rows.length);
      var oRows = tblSort1.rows;
      var SupplementOnly10, FinalOnly10, SupplementOnlyOver10, FinalOnlyOver10;
      if (oRows) {
        for (var i = 0; i < oRows.length; i++) {
          if (oRows[i] && oRows[i].cells) {
            oRows[i].style.backgroundColor = (i % 2 ? "#fff7e5" : "#ffffff");
            SupplementOnly10 = oRows[i].cells[12].innerText;
            FinalOnly10 = oRows[i].cells[13].innerText;
            SupplementOnlyOver10 = oRows[i].cells[14].innerText;
            FinalOnlyOver10 = oRows[i].cells[15].innerText;
            
            if (parseInt(SupplementOnly10, 10) > 0 || parseInt(FinalOnly10, 10) > 0) {
              oRows[i].cells[6].style.color = "#FF0000";
              oRows[i].cells[6].title = "Supplements: " + SupplementOnly10 + "\nFinals: " + FinalOnly10;
            } else {
              oRows[i].cells[6].style.color = "#000000";
              oRows[i].cells[6].title = "";
            }

            if (parseInt(SupplementOnlyOver10, 10) > 0 || parseInt(FinalOnlyOver10, 10) > 0) {
              oRows[i].cells[7].style.color = "#FF0000";
              oRows[i].cells[7].title = "Supplements: " + SupplementOnlyOver10 + "\nFinals: " + FinalOnlyOver10;
            } else {
              oRows[i].cells[7].style.color = "#000000";
              oRows[i].cells[7].title = "";
            }
          }
        }
      }
    }
  }

    function sortTable(sTable, sSortFldName, sDataType) {
      if (recCount < 1) return;
      var oTD = null;
      if (tblHead) {
        var oTHead = tblHead.rows;
        if (oTHead) {
          var oCells = oTHead[0].cells;
          for (var i = 0; i < oCells.length; i++) {
            var sScript = oCells[i].onclick;
            if (sScript != null) sScript = sScript.toString();
            if (sScript != null && sScript.indexOf(sSortFldName) > 0) {
              oTD = oCells[i];
              break;
            }
          }
        }
      } else 
        oTD = event.srcElement;
        
      var sSortOrder = (oTD ? oTD.getAttribute("sortOrder") : "ascending");
      var oXML = null;
      
      if (sTable != "" ) {
        oTbl = document.getElementById(sTable);
        if (oTbl) {
          sXMLSrc = oTbl.dataSrc;
          sXMLSrc = sXMLSrc.substr(1);
        }
      } else
        return;

      if (sXMLSrc != "")
        oXML = document.getElementById(sXMLSrc);
      
        
      if (sSortFldName != "" && oXML && oTbl) {

          bSorting = true;
          
          var sFldName = sSortFldName;
          if (sSortOrder == "ascending")
             sSortOrder = "descending";
          else
             sSortOrder = "ascending";
             
          if (sSortFldName == "DefaultOrder")
            sSortOrder = "ascending"
        
          var sColumnType = (sDataType == "number" ? "number" : "text");
        
          var iCount = 0;
          var objTmp = null;
          if (sFldName) {
                var sDataSrc = sXMLSrc;
                var objXML = oXML; 
                if (objXML) {
        
                   var sFirstNode = objXML.XMLDocument.documentElement.firstChild.nodeName;
        
                   oTbl.dataSrc = "";
                   var objXSL = new ActiveXObject("Msxml2.DOMDocument");
                   if (objXSL) {
                      objXSL.async = false;
        
                      var sXSL = '<?xml version="1.0" encoding="UTF-8"?>' +
                                  '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
        
                                  '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>' +
        
                                  '<xsl:template match="/Root">' +
                                     '<xsl:element name="Root">' +
                                     '<xsl:apply-templates select="' + sFirstNode + '">' +
                                        '<xsl:sort select="@' + sFldName + '" data-type="' + sColumnType + '" ' +
                                        (sColumnType == 'text' ? 'case-order="lower-first"' : '') +
                                        ' order="' + sSortOrder + '" />' +
                                     '</xsl:apply-templates>' +
                                     '</xsl:element>' +
                                  '</xsl:template>' +
        
                                  '<xsl:template match="' + sFirstNode + '">' +
                                     '<xsl:copy-of select="."/>' +
                                  '</xsl:template>' +
                                  '</xsl:stylesheet>';
                      //window.clipboardData.setData("Text", sXSL);
                      objXSL.loadXML(sXSL);
        
                      //oXML.loadXML(oXML.transformNode(objXSL));
                      oXML.transformNodeToObject(objXSL, oXML);
                      oTbl.dataSrc = "#" + sDataSrc;
        
                      //reset the page.
                      iCurPage = 1;

                      //display the sort icon
                      divSort.style.display = "inline";
                      
                      txtPage.value = iCurPage + " of " + iPgCount;

                      if (sSortOrder == "ascending")
                        divSort.firstChild.src = "/images/ascending.gif";
                      else
                        divSort.firstChild.src = "/images/descending.gif";
                        
                      if (oTD) {
                        var x = oTD.offsetLeft + oTD.offsetWidth - divSort.offsetWidth;
                        var y = ((oTD.offsetTop + oTD.offsetHeight) - divSort.offsetHeight) / 2 + oTD.offsetTop + 3;
                        divSort.style.left = x - 3;
                        divSort.style.top = y;
                        
                        oTD.setAttribute("sortOrder", sSortOrder);
                      }
                      bSorting = false;
                   }
                }
          }
      }
    }

    function showPage(sVal) {
      var bPageMoved = false;
      if (tblSort1 && tblSort1.dataSrc != "") {
        switch(sVal) {
          case -1: //move to first page
            if (iCurPage > 1) {
              tblSort1.firstPage();
              iCurPage = 1;
              bPageMoved = true;
            }
            break;
          case -2: //move to previous page
            if (iCurPage > 1) {
              tblSort1.previousPage();
              iCurPage--;
              bPageMoved = true;
            }
            break;
          case -3: //move to next page
            if (iCurPage < iPgCount) {
              tblSort1.nextPage();
              iCurPage++;
              bPageMoved = true;
            }
            break;
          case -4: //move to last page
            if (iCurPage < iPgCount) {
              for (var i = 0; i < (iPgCount - iCurPage); i++) {
                tblSort1.nextPage();
              }
              
              iCurPage = iPgCount;
              bPageMoved = true;
            }
            break;
          default: //move to any page
            var iPgDiff = parseInt(sVal, 10) - iCurPage;
            if (iPgDiff > 0) {
              for (var i = 0; i < iPgDiff; i++)
                 tblSort1.nextPage();
              bPageMoved = true;
            } else {
              iPgDiff *= -1;
              for (var i = 0; i < iPgDiff; i++)
                 tblSort1.previousPage();
              bPageMoved = true;
            }
            iCurPage = parseInt(sVal, 10);
            break;
        }

        if (iCurPage < iPgCount)
          iRecInCurPage = iPageSize;
        else
          iRecInCurPage = (recCount - (iPgCount - 1) * iPageSize);
          
        iTaskRefreshedTryCount = 1;
        if (bPageMoved == true) {
          //txtPage.CCDisabled = false;
          if (iCurPage > iPgCount) iCurPage = iPgCount;
          txtPage.value = iCurPage + " of " + iPgCount;
          //txtPage.CCDisabled = true;
          //disableControls(false);
        }
      }
    }

    function gotoPage(){
      var iPage;
      var sPageTxt = txtPage.value;
      if (sPageTxt.indexOf("of") > 0) {
        sPageTxt = sPageTxt.substr(0, sPageTxt.indexOf("of"));
      }
      
      if (isNaN(sPageTxt)){
        alert("Invalid page number specified. Expecting a numeric value for page number.");
        return;
      }
      
      iPage = parseInt(sPageTxt, 10);

      if (iPage <= 0) iPage = 1;
      if (iPage > iPgCount) iPage = iPgCount;
      if (iPage != iCurPage)
        showPage(iPage);
      else
        txtPage.value = iCurPage + " of " + iPgCount;
    }
    
    function doPageScroll(){
      if (event.wheelDelta >= 120)
        showPage(-2);
      else if (event.wheelDelta <= -120)
        showPage(-3);   
    }
    
    function showFilter(){
      divFilter.style.display = "inline";
      txtCity.focus();
    }
    
    function hideFilter(){
      divFilter.style.display = "none";
    }
    
    function doFilter(){
      var sCity = "";
      var sStateCD = "";
      var sStateName = "";
      var sZip = "";
      var oFilteredXML, oXML;
      var sFilterDesc = "";
      
      sCity = txtCity.value.Trim();
      
      if (selState.selectedIndex != -1) {
        sStateCD = selState.value.Trim();
        sStateName = selState.options[selState.selectedIndex].innerText;
      }
      
      sZip = txtZip.value.Trim();
      
      var aCriteria = new Array();
      var sCriteria = "";
      if (sCity != ""){
        aCriteria.push("contains(@AddressCitySrch, '" + sCity + "')");
        sFilterDesc = "City='" + sCity + "'";
      }

      if (sStateCD != "") {
        aCriteria.push("(contains(@AddressStateSrch, '" + sStateCD.toLowerCase() + "') or contains(@AddressStateSrch, '" + sStateName.toLowerCase() + "'))");
        sFilterDesc += (sFilterDesc != "" ? " and " : "") + "State='" + sStateName + "'";
      }

      if (sZip != "") {
        aCriteria.push("contains(@AddressZip, '" + sZip + "')");
        sFilterDesc += (sFilterDesc != "" ? " and " : "") + "Zip='" + sZip + "'";
      }
        
      if (selInsurance.selectedIndex != -1){
        if (selInsurance.value != "") {
          aCriteria.push("contains(@InsuranceIDList, '" + selInsurance.value + ",')");
          sFilterDesc += (sFilterDesc != "" ? " and " : "") + "Insurance Company='" + selInsurance.options[selInsurance.selectedIndex].innerText + "'";
        }
      }
     
      sCriteria = aCriteria.join(" and ");
      
      if (sCriteria == "") {
        clearFilter(); return;
      } else
        sCriteria = "Shop[" + sCriteria + "]";
      
      //alert(sCriteria);
      //return;
      
      oXML = document.getElementById("xmlSearchData");
      oFilteredXML = document.getElementById("xmlSearchFiltered");

      //tblSort1.dataSrc = "";
      try {
      var objXSL = new ActiveXObject("Msxml2.DOMDocument");
      if (objXSL) {
        objXSL.async = false;
        
        var sXSL = '<?xml version="1.0" encoding="UTF-8"?>' +
                    '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
        
                    '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>' +
        
                    '<xsl:template match="/Root">' +
                       '<xsl:element name="Root">' +
                       '<xsl:apply-templates select="' + sCriteria + '">' +
                       '</xsl:apply-templates>' +
                       '</xsl:element>' +
                    '</xsl:template>' +
        
                    '<xsl:template match="Shop">' +
                       '<xsl:copy-of select="."/>' +
                    '</xsl:template>' +
                    '</xsl:stylesheet>';
        window.clipboardData.setData("Text", sXSL);
        objXSL.loadXML(sXSL);
        if (objXSL.parseError.errorCode == 0) {
          oXML.transformNodeToObject(objXSL, oFilteredXML);
          setTableSource(oFilteredXML)
          spnFilter.innerHTML = "<b>Filter:</b>&nbsp;" + sFilterDesc;
          btnClearFilter.style.display = "inline";
        }
      }
      } catch (e) {alert(e.description)}
      hideFilter();
    }
    
    function clearFilter() {
      setTableSource(xmlSearchData);
      txtCity.value = "";
      selState.selectedIndex = -1;
      selInsurance.selectedIndex = -1;
      txtZip.value = "";
      btnClearFilter.style.display = "none";
      spnFilter.innerHTML = "";
      hideFilter();
    }
    
    function openShop(){
      var srcElement = event.srcElement;
      var iCount = 10;
      while (srcElement.tagName != "TR" && iCount > 0)
        srcElement = srcElement.parentElement;
        
      var oTR = srcElement;
      var iNumber = parseInt(oTR.cells[0].innerText, 10);
      
      if (iNumber > 0) {
        var sShopLocationID;
        var sShopID;
        if (xmlSearchData){
          var oShopNode = xmlSearchData.selectSingleNode("/Root/Shop[@Number=" + iNumber + "]");
          if (oShopNode) {
            sShopLocationID = oShopNode.getAttribute("ShopLocationID");
            sShopID = oShopNode.getAttribute("ShopID");
            
            NavToShop('S', sShopLocationID, sShopID);
          }
        }
      }
      event.cancelBubble = true;
      event.returnValue = false;
    }
  ]]>
  </SCRIPT>

  <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
  
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="backgro1und:transparent;overflow:hidden" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
	    <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="0" width="50" nowrap="nowrap" style="display:none"> No. </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="1" width="50" nowrap="nowrap" onclick="sortTable('tblSort1', 'ShopLocationID', 'number')"> ID </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="2" width="280" nowrap="nowrap" onclick="sortTable('tblSort1', 'Name', 'text')"> Shop Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="3" width="136" nowrap="nowrap" onclick="sortTable('tblSort1', 'AddressCity', 'text')"> City </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="4" width="60" nowrap="nowrap" onclick="sortTable('tblSort1', 'AddressState', 'text')"> State </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="5" width="134" nowrap="nowrap" onclick="sortTable('tblSort1', 'LastClaimInsComp', 'text')"> Insurance Company of last assignment </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="6" width="61" nowrap="nowrap" onclick="sortTable('tblSort1', 'Assignment60', 'number')"> Assign w/in 60 Days </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="7" width="62" nowrap="nowrap" onclick="sortTable('tblSort1', 'Estimate10', 'number')"> Estimates w/in 10 Days </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="8" width="61" nowrap="nowrap" onclick="sortTable('tblSort1', 'EstimateOver10', 'number')"> Estimates 10-60 Days </TD>
      <TD unselectable="on" class="TableSortHeader" type="Date" sIndex="9" width="69" onclick="sortTable('tblSort1', 'LastREIDateString', 'text')"> Last ReI </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="10" width="29" > Pot. ReI </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="11" width="58" onclick="sortTable('tblSort1', 'ProgramScore', 'number')"> Shop Score </TD>
	  </TR>    
  </TBODY>
</TABLE>
<div id="divSort" name="divSort" style="position:absolute;top:0px;left:0px;display:none"><img src="/images/ascending.gif"/></div>
<div id="divNoData" name="divNoData" class="TableData" style="display:none;height:482px;border:0px;width:100%;padding:10px;text-align:center;color:#FF0000;font-weight:bold">No data to display.</div>
<DIV unselectable="on" id="divShopList" class="autoflowTable" style="height:482px;border:0px solid #FF0000" onmousewheel="doPageScroll()">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;border:1px solid outset;table-layout:fixed;" dataPageSize="25">
    <colgroup>
      <col width="48" style="display:none"/>
      <col width="48" />
      <col width="279"/>
      <col width="135"/>
      <col width="58"/>
      <col width="133"/>
      <col width="61" style="text-align:center"/>
      <col width="61" style="text-align:center"/>
      <col width="59" style="text-align:center"/>
      <col width="68" style="text-align:center"/>
      <col width="28" style="text-align:center"/>
      <col width="58" style="text-align:center"/>
      <col width="0" style="display:none"/>
      <col width="0" style="display:none"/>
      <col width="0" style="display:none"/>
      <col width="0" style="display:none"/>
    </colgroup>
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <!-- <xsl:for-each select="Shop"><xsl:call-template name="ShopListInfo"/></xsl:for-each> -->
      <tr unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" onclick="GridSelect(this, true)" style="height:18px;">
        <td unselectable="on"><span unselectable="on" datafld="Number" class="spanNoWrap" /></td>
        <td unselectable="on" style="color:#0000FF" onclick="openShop()"><span unselectable="on" datafld="ShopLocationID" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="Name" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="AddressCity" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="AddressState" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="LastClaimInsComp" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="Assignment60" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="Estimate10" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="EstimateOver10" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="LastREIDate" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="" class="spanNoWrap"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</span></td>
        <td unselectable="on"><span unselectable="on" datafld="ProgramScore" class="spanNoWrap"/></td>
        <td unselectable="on"><span unselectable="on" datafld="SupplementOnly10"/></td>
        <td unselectable="on"><span unselectable="on" datafld="FinalOnly10"/></td>
        <td unselectable="on"><span unselectable="on" datafld="SupplementOnlyOver10"/></td>
        <td unselectable="on"><span unselectable="on" datafld="FinalOnlyOver10"/></td>
      </tr>
    </TBODY>
  </TABLE>
</DIV>
  <div id="divPages" name="divPages" style="padding:0px;">
    <table style="width:100%;padding:0px;margin:0px;margin-top:3px;table-layout:fixed;border:0px;border-top:1px solid #C0C0C0;padding-top:0px;background-color:#DCDCDC" cellspacing="0" cellpadding="0" border="0">
      <colgroup>
        <col width="21px"/>
        <col width="21px"/>
        <col width="*"/>
        <col width="30px"/>
        <col width="80px"/>
        <col width="20px"/>
        <col width="36px" style="padding:0px;padding-left:10px"/>
        <col width="27px"/>
        <col width="27px"/>
        <col width="27px"/>
      </colgroup>
      <tr>
        <td>
          <button id="btnFilter" name="btnFilter" onclick="showFilter()" tabIndex="6" style="border:0px;background-color:transparent;height:21px;width:26px;" title="Filter data...">
          <img src="/images/filter.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>
          <button id="btnClearFilter" name="btnClearFilter" style="display:none;border:0px;background-color:transparent;height:21px;width:26px;" onclick="clearFilter()" title="Clear Filter"><img src="/images/RemoveFilter.gif"/></button>
        </td>
        <td style="padding-top:2px;padding-bottom:2px;padding-left:2px;">
          <span id="spnFilter" name="spnFilter" style="padding:0px;padding-left:5px;"/>
        </td>
        <td>Page:</td>
        <td style="padding-top:2px">
          <input type="text" id="txtPage" name="txtPage" size="12" maxLength="10" style="border:1px solid inset;font-size:9pt;text-align:center"/>
        </td>
        <td style="padding-top1:2px">
          <button id="btnGoPage" name="btnGoPage" onclick="gotoPage()" tabIndex="8" style="border:0px;background-color:transparent" title="Go to page...">
          <img src="/images/go.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>
          <button id="btnFirst" name="btnFirst" onclick="showPage(-1)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to first page">
           &lt; &lt;
          </button>
        </td>
        <td>
          <button id="btnPrev" name="btnPrev" onclick="showPage(-2)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to previous page">
            &lt;
          </button>
        </td>
        <td>
          <button id="btnNext" name="btnNext" onclick="showPage(-3)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to next page">
            &gt;
          </button>
        </td>
        <td>
          <button id="btnLast" name="btnLast" onclick="showPage(-4)" tabIndex="8" style="font-weight:bold;border:1px solid outset;background-color:transparent;height:21px;width:26px;" title="Go to last page">
            &gt;&gt;
          </button>
        </td>
      </tr>
    </table>
  </div>
</DIV>
<div id="divFilter" name="divFilter" style="position:absolute;top:10px;left:10px;border:1px solid #000000;visibility:hidden;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
  <div style="height:75px;width:250px;background-color:#F0E68C;">
  <table border="0" cellspacing="0" cellpadding="3" style="margin:3px">
    <tr>
      <td><b>City:</b></td>
      <td colspan="3">
        <input type="text" id="txtCity" name="txtCity" size="50" maxLength="45"/>
      </td>
    </tr>
    <tr>
      <td><b>State:</b></td>
      <td>
        <select name="selState" id="selState">
          <option value=""/>
          <xsl:for-each select="/Root/StateList">
          <option>
            <xsl:attribute name="value"><xsl:value-of select="@StateCD"/></xsl:attribute>
            <xsl:value-of select="@Name"/>
          </option>
          </xsl:for-each>
        </select>
      </td>
      <td><b>Zip:</b></td>
      <td>
        <input type="text" id="txtZip" name="txtZip" size="10" maxLength="10"/>
      </td>
    </tr>
    <tr>
      <td><b>Ins. Cmp.:</b></td>
      <td colspan="3">
        <select name="selInsurance" id="selInsurance">
          <option value=""/>
          <xsl:for-each select="/Root/InsuranceCompany">
          <option>
            <xsl:attribute name="value"><xsl:value-of select="@InsuranceCompanyID"/></xsl:attribute>
            <xsl:value-of select="@Name"/>
          </option>
          </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td><button style="border:1px solid outset;width:55px" onclick="doFilter()">Filter</button></td>
      <td><button style="border:1px solid outset;width:75px" onclick="clearFilter()">Clear Filter</button></td>
      <td colspan="2" align="right">
        <button style="border:1px solid outset;width:55px" onclick="hideFilter()">Close</button>
      </td>
    </tr>
  </table>
  </div>
</div>

<xml id="xmlSearchData" name="xmlSearchData" ondatasetcomplete="setTableSource(this)">
  <Root>
    <xsl:for-each select="/Root/Shop">
      <xsl:variable name="LastReinspectionDate" select="user:UTCConvertDateByNodeType(.,'LastReinspectionDate', 'A')"/>
      <Shop>
        <xsl:copy-of select="@*"/>
        <xsl:attribute name="LastREIDate">
          <xsl:if test = "$LastReinspectionDate != '01/01/1900'"><xsl:value-of select="$LastReinspectionDate"/></xsl:if>
        </xsl:attribute>
        <xsl:attribute name="LastREIDateString">
          <!-- yyyymmdd format -->
          <xsl:if test = "$LastReinspectionDate != '01/01/1900'"><xsl:value-of select="concat(substring($LastReinspectionDate, 7), substring($LastReinspectionDate, 1, 2), substring($LastReinspectionDate, 4, 2))"/></xsl:if>
        </xsl:attribute>
        <xsl:attribute name="LastClaimInsComp">
          <xsl:variable name="LastClaimInsComp"><xsl:value-of select="@LastInsuranceID"/></xsl:variable>
          <xsl:value-of select="/Root/InsuranceCompany[@InsuranceCompanyID = $LastClaimInsComp]/@Name"/>
        </xsl:attribute>
      </Shop>
    </xsl:for-each>
  </Root>
</xml>

<xml id="xmlSearchFiltered" name="xmlSearchFiltered">
</xml>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>