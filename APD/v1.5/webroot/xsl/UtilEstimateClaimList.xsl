<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="UtilClaimList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<html>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT labguage="JavaScript">

  var gsCurrSelectedRow = "";
  var gsCurrSavedRowBGColor = "";

<![CDATA[

  function PageInit()
  {
    if (document.getElementById("CNScrollTable"))
	    resizeScrollTable(document.getElementById("CNScrollTable"));
  }

  function GridSelect(oRow)
  {
    var sClaimAspectID = oRow.cells[5].innerText;
    var sLynxID = oRow.cells[0].innerText;
    var sLynxID = sLynxID.substring(0, sLynxID.indexOf('-'));

    if (sClaimAspectID != '')
    {
      var cellsLength;
      if (gsCurrSelectedRow != "")
      {
        cellsLength = gsCurrSelectedRow.cells.length;
        for (var i = 0; i < cellsLength; i++)
          gsCurrSelectedRow.cells[i].style.backgroundColor = gsCurrSavedRowBGColor;
      }
      
      gsCurrSelectedRow = oRow;
      gsCurrSavedRowBGColor = oRow.cells[0].style.backgroundColor;
      cellsLength = oRow.cells.length;
      for (var i = 0; i < cellsLength; i++)
        oRow.cells[i].style.backgroundColor = "#DDDDDD";

      parent.document.getElementById("ifrmDocViewer").src = "blank.asp";
      parent.document.getElementById("ifrmEstimateDetails").src = "blank.asp";
      parent.document.getElementById("ifrmEstimateList").src = "UtilEstimateDocList.asp?ClaimAspectID=" + sClaimAspectID + "&LynxID=" + sLynxID;
    }
    else
    {
      ClientWarning("Unable to retrieve Claim number. Please try again.");
      return;
    }
  }

  function resizeScrollTable(oElement)
  {
    var head = oElement.firstChild;
    var headTable = head.firstChild;
    var body = oElement.lastChild;
    var bodyTable = body.firstChild;
  
    body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);
  
    var scrollBarWidth = body.offsetWidth - body.clientWidth;
  
    // set width of the table in the head
    //headTable.style.width = Math.max(0, Math.max(bodyTable.offsetWidth + scrollBarWidth, oElement.clientWidth));
  
    // go through each cell in the head and resize
    var headCells = headTable.rows[0].cells;
    if (bodyTable.rows.length > 0)
    {
      var bodyCells = bodyTable.rows[0].cells;
        var iLength = bodyCells.length;
      for (var i = 0; i < iLength; i++)
      {
        if (bodyCells[i].style.display != "none")
        {
          headCells[i].style.width = bodyCells[i].offsetWidth;
          if (i == (headCells.length-1)) bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 );
        }
      }
    }
  }

]]>
  
</SCRIPT>

<head>
	<title>APD Estimate Cleanup Utility - Claim List</title>
</head>
<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" onLoad="PageInit();">

<xsl:variable name="ClaimCount">
  <xsl:value-of select="count(Claim)"/>
</xsl:variable>

<xsl:if test="$ClaimCount = 0">
  <SPAN class="boldtext" style="position:absolute; top:10px; left:10px;">
    <br/>
    <xsl:text disable-output-escaping="yes">
      Sorry, no results returned. Please check your search values and try again.
    </xsl:text>
  </SPAN>
</xsl:if>

<xsl:if test="$ClaimCount &gt; 0">

  <DIV id="CNScrollTable" style="width:100%;">
    <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="1" cellpadding="0">
      <TBODY>
        <TR unselectable="on" class="QueueHeader" style="height:22px">
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="0" width="71" nowrap="nowrap" title="Click to sort">LYNX ID</TD>
      	  <TD unselectable="on" class="TableSortHeader" type="Date" sIndex="1" width="160" nowrap="nowrap" title="Click to sort">Received</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="2" width="180" nowrap="nowrap" title="Click to sort">Status</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="3" width="180" nowrap="nowrap" title="Click to sort">Type</TD>
      	  <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="4" width="70" nowrap="nowrap" title="Click to sort">Reviewed</TD>
      	</TR>
      </TBODY>
    </TABLE>
    <DIV unselectable="on" class="autoflowTable" style="height:378px;">
      <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="Claim" >
            <xsl:call-template name="ClaimInfo"/>
    	  </xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
  </DIV>

</xsl:if>

</body>
</html>
</xsl:template>


<!-- Gets the list of claims -->
<xsl:template name="ClaimInfo">
  <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Claim' onClick='GridSelect(this)' onDblClick='GridSelect(this)' style="height:18px" title="Click to select claim">
  <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <TD unselectable="on" align="center" width="85" class="GridTypeTD">
  		<xsl:value-of select="@LynxID"/>
    </TD>
    <TD unselectable="on" align="center" width="95" class="GridTypeTD">
      <xsl:choose>
  	    <xsl:when test="@ReceivedDate = ''"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
  	    <xsl:otherwise>
          <xsl:value-of select="js:UTCConvertDate(string(current()/@ReceivedDate))" />
        </xsl:otherwise>
      </xsl:choose>
    </TD>
    <TD unselectable="on" align="center" width="70" class="GridTypeTD">
      <xsl:choose>
  	    <xsl:when test="@Status = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:when>
  	    <xsl:otherwise>
          <xsl:if test="@Status = 'Open'">Open</xsl:if>
          <xsl:if test="@Status = 'Claim Closed'">Closed</xsl:if>
          <xsl:if test="@Status = 'Claim Cancelled'">Cancelled</xsl:if>
          <xsl:if test="@Status = 'Claim Voided'">Voided</xsl:if>
          <xsl:if test="@Status = 'Closed - Subro Pending'">Closed</xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TD>
    <TD unselectable="on" align="center" width="150" class="GridTypeTD">
      <xsl:choose>
  	    <xsl:when test="@Type = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:when>
  	    <xsl:otherwise>
          <xsl:value-of select="@Type"/>
        </xsl:otherwise>
      </xsl:choose>
    </TD>
    <TD unselectable="on" align="center" width="45" class="GridTypeTD">
      <xsl:choose>
  	    <xsl:when test="@ReviewedFlag = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:when>
  	    <xsl:otherwise>
          <xsl:if test="@ReviewedFlag = '1'">Yes</xsl:if>
          <xsl:if test="@ReviewedFlag = '0'">No</xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TD>
    <TD unselectable="on" style="display:none">
  		<xsl:value-of select="@ClaimAspectID"/>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>
