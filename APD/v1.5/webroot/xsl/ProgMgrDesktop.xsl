<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ProgMgrDesktop">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspProgManagerDeskGetListXML,ProgMgrDesktop.xsl,36   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>
  <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


</HEAD>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

<![CDATA[

//init the table selections, must be last
function initPage()
{
//  resizeScrollTable(document.getElementById("SLScrollTable"));
}

//update the selected elements from the table row
function GridSelect(oRow)
{

}

//Takes a string and removes the leading and trailing spaces and also any extra spaces in the string. IE only.
function remove_XS_whitespace(item)
{
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
  for (index = 0; index < item_length; index++)
  {
    if (item.value.charAt(index) != ' ')
    {
      tmp += item.value.charAt(index);
    }
    else
    {
      if (tmp.length > 0)
      {
        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1)
        {
          tmp += item.value.charAt(index);
        }
      }
    }
  }
  item.value = tmp;
}


]]>
</SCRIPT>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent;" onLoad="initPage();">

<TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
  <TR unselectable="on">
    <TD unselectable="on" nowrap="nowrap">Shop Name:</TD>
    <TD unselectable="on">
      <INPUT class="InputField" type="text" id="txtShopName" size="30" maxlength="50" value="" onBlur="remove_XS_whitespace(this)" />
    </TD>
    <TD unselectable="on"><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></TD>
    <TD unselectable="on" nowrap="nowrap">City:</TD>
    <TD unselectable="on">
      <INPUT class="InputField" type="text" id="txtShopCity" size="20" maxlength="50" value="" onBlur="remove_XS_whitespace(this)" />
    </TD>
    <TD unselectable="on"><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></TD>
    <TD unselectable="on" nowrap="nowrap">Zip:</TD>
    <TD unselectable="on">
      <INPUT class="InputField" type="text" id="txtShopState" size="3" maxlength="5" value="" onBlur="remove_XS_whitespace(this)" />
    </TD>
    <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    <TD unselectable="on" width="100%" align="center" nowrap="nowrap">
      <INPUT unselectable="on" type="button" name="ShopSearchButton" value="Search Shop" onClick=";" class="formbutton" />
    </TD>
  </TR>
</TABLE>

<IMG unselectable="on" src="/images/spacer.gif" border="0" width="1" height="4" />

<TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblShopList')" width="100%" border="1" cellspacing="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" width="160" rowspan="2"> Shop Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" width="100" rowspan="2" nowrap="nowrap"> City </TD>
      <TD unselectable="on" class="TableSortHeader" width="111" colspan="3" nowrap="nowrap" onClick="" style="cursor:default;"> Assignments Sent </TD>
      <TD unselectable="on" class="TableSortHeader" width="111" colspan="3" nowrap="nowrap" onClick="" style="cursor:default;"> Estimates Rcvd </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" width="72" rowspan="2"> Reinsp. Satisfaction </TD>
      <TD unselectable="on" class="TableSortHeader" type="Date" width="52" rowspan="2"> Last Reinsp. </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" width="56" rowspan="2"> Potential Reinsp. </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" width="100%" rowspan="2"> Shop Score </TD>
    </TR>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-5 </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-10 </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-30 </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-5 </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-10 </TD>
      <TD unselectable="on" class="TableSortHeader" type="Number" nowrap="nowrap"> 0-30 </TD>
    </TR>
  </TBODY>
</TABLE>
<DIV unselectable="on" class="autoflowTable" style="width: 100%; height: 280px;">
  <TABLE unselectable="on" id="tblShopList" class="GridTypeTable" width="100%" border="0" cellspacing="1" cellpadding="0">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">

      <xsl:for-each select="Shop" >
        <xsl:call-template name="ShopListInfo"/>
      </xsl:for-each>

    </TBODY>
  </TABLE>
</DIV>

</BODY>
</HTML>
</xsl:template>

  <!-- Gets the list of shops -->
  <xsl:template name="ShopListInfo">
    <TR unselectable="on" onDblClick='GridSelect(this)'>
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD style="display:none">
        <xsl:value-of select="@ShopLocationID"/>
      </TD>
      <TD unselectable="on" width="158" class="GridTypeTD" style="text-align:left" nowrap="nowrap">
        <xsl:value-of select="@Name"/>
      </TD>
      <TD unselectable="on" width="99" class="GridTypeTD" style="text-align:left" nowrap="nowrap">
        <xsl:value-of select="@AddressCity"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Assignment5"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Assignment10"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Assignment30"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Estimate5"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Estimate10"/>
      </TD>
      <TD unselectable="on" width="36" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@Estimate30"/>
      </TD>
      <TD unselectable="on" width="71" class="GridTypeTD" nowrap="nowrap">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" width="51" class="GridTypeTD" nowrap="nowrap">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" width="55" class="GridTypeTD" nowrap="nowrap">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" width="100%" class="GridTypeTD" nowrap="nowrap">
        <xsl:value-of select="@ProgramScore"/>
      </TD>
    </TR>
  </xsl:template>


</xsl:stylesheet>