<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function space( iCount )
     {
       var strRet = "";
       for (var i = 0; i < iCount; i++)
        strRet += "&nbsp;&nbsp;&nbsp;";
       return strRet;
     }
  ]]>
</msxsl:script>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>
<xsl:param name="Environment"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDMainGetListXML,PMDMain.xsl -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,23);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

var gsUserID = '<xsl:value-of select="$UserId"/>';

<![CDATA[

//init the table selections, must be last
function initPage(){
  divStatus.style.left = (document.body.offsetWidth - 150) / 2 - 75;
  selProgramMgr_onchange();
}

function selProgramMgr_onchange(val){
  divStatus.style.top = tblActiveQueue.offsetTop + (tblActiveQueue.offsetHeight - 75) / 2;
  showProgress();
  window.setTimeout("selProgramMgr_onchange2(" + val + ")", 150);
}

function selProgramMgr_onchange2(val){
  var sUserIDs = selProgramMgr.value + ",";
  var iCurLevel = selProgramMgr.options[selProgramMgr.selectedIndex].level;
  for (var i = selProgramMgr.selectedIndex + 1; i < selProgramMgr.options.length; i++){
    if (selProgramMgr.options[i].level > iCurLevel)
      sUserIDs += selProgramMgr.options[i].value + ",";
    else
      break;
  }
  document.frames["ifrmPMDMarketAnalysis"].frameElement.src = "PMDActiveQueue.asp?ProgramManagerUserID=" + sUserIDs;
  obj = document.getElementById("ifrmPMDMarketAnalysis");
  obj.style.visibility = "visible";
  obj.style.display = "inline";	
}

function clearResultsDiv(){
  document.frames["ifrmPMDMarketAnalysis"].frameElement.src = "/blank.asp";
  document.frames["ifrmPMDMarketAnalysis"].frameElement.style.visibility="hidden";
}

//Takes a string and removes the leading and trailing spaces and also any extra spaces in the string. IE only.
function remove_XS_whitespace(item){
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
  for (index = 0; index < item_length; index++)  {
    if (item.value.charAt(index) != ' '){
      tmp += item.value.charAt(index);
    }
    else{
      if (tmp.length > 0){
        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1){
          tmp += item.value.charAt(index);
        }
      }
    }
  }
  item.value = tmp;
}


function NumbersOnly(event){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}

function showProgress(strMsg){
  divStatus.firstChild.cells[0].innerText = (strMsg != "" && strMsg != undefined ? strMsg : "Please wait...");
  divStatus.style.display = "inline";
}

function hideProgress() {
  divStatus.style.display ="none";
}

function doMaxMin(){
  if (imgMaxMin.src.indexOf("more") > 0){
    imgMaxMin.src = "/images/showless.gif";
    imgMaxMin.title = "Restore Active Work Queue";
    divShopSearch.style.display = "none";
    ifrmPMDMarketAnalysis.frameElement.style.height = "552px";
    if (typeof(ifrmPMDMarketAnalysis.setHeight) == "function")
      ifrmPMDMarketAnalysis.setHeight();
  } else {
    imgMaxMin.src = "/images/showmore.gif";
    imgMaxMin.title = "Maximize Active Work Queue";
    ifrmPMDMarketAnalysis.frameElement.style.height = "258px";
    divShopSearch.style.display = "inline";
    if (typeof(ifrmPMDMarketAnalysis.setHeight) == "function")
      ifrmPMDMarketAnalysis.setHeight();
  }
}

function shopSearch(){
  document.frames["ifrmPMDShopSearch"].frameElement.src = "/blank.asp"
  divStatus.style.top = divShopSearch.offsetTop + (divShopSearch.offsetHeight - 75) / 2;
  showProgress();  
  window.setTimeout("shopSearch2()", 100);
}

function shopSearch2(){
  var bAllTerritories = 0;
  if (chkAllTerritories.checked)
    bAllTerritories = 1;
  document.frames["ifrmPMDShopSearch"].frameElement.src = "PMDShopSearch.asp?ProgramManagerUserID=" + gsUserID + 
                                                          "&ShopName=" + escape(txtShopName.value) + 
                                                          "&ShopCity=" + escape(txtShopCity.value) + 
                                                          "&ShopState=" + escape(selState.value) + 
                                                          "&ShopZip=" + escape(txtShopZip.value) + 
                                                          "&AllTerritories=" + bAllTerritories;
}

/*function checkSearchProgress(){
  if (document.frames["ifrmPMDShopSearch"].frameElement.readyState == "complete")
    hideProgress();
}*/

function refreshQueue(){
  if (selProgramMgr.selectedIndex != -1){
    document.frames["ifrmPMDMarketAnalysis"].frameElement.src = "/blank.asp"
    selProgramMgr_onchange(selProgramMgr.value);
  }
}

function doDefaultSort(){
  var oFrm = document.frames["ifrmPMDMarketAnalysis"];
  if (typeof(oFrm.sortDefault) == "function")
    oFrm.sortDefault();
}

]]>

</SCRIPT>
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;overflow:hidden;" onLoad="initPage();">
<div id="divStatus" name="divStatus" style="z-index:1;position:absolute;top:0px;left:0px;padding:5px;border:1px solid #C0C0C0;height:75px;width:250px;background-color:#FFF8DC;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#C0C0C0,strength=3);display:none;">
  <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #C0C0C0;height:100%;width:100%;background-color:#FFFFFF">
    <tr>
      <td style="font-weight:bold;text-align:center;">Loading data...</td>
    </tr>
    <td style="font-weight:bold;text-align:center;">
      <img src="/images/progress.gif"/>
    </td>
  </table>
</div>
<!-- if User is a DM use UserID, if User is a PM use User's SupervisorID, otherwise use 0 for no selection-->
<xsl:variable name="DistrictManagerUserID">
  <xsl:choose>
    <xsl:when test="boolean(/Root/DistrictManager[@UserID=$UserId])"><xsl:value-of select="$UserId"/></xsl:when>
    <xsl:when test="boolean(/Root/ProgramManager[@UserID=$UserId])"><xsl:value-of select="/Root/ProgramManager[@UserID=$UserId]/@SupervisorUserID"/></xsl:when>
    <xsl:otherwise>0</xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<table id="tblActiveQueue" unselectable="on" width="1013px" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;">
  <colgroup>
    <col width="125px"/>
    <col width="*"/>
    <col width="115px"/>
    <col width="250px"/>
    <col width="75px" style="text-align:right"/>
  </colgroup>
  <tr unselectable="on" style="height:26px">
    <td unselectable="on">Active Work Queue</td>
    <td>
      <button style="border:0px;padding:0px;margin:0px;background:transparent;" onclick="doDefaultSort()"><img id="imgDefaultSort" src="/images/defaultsort.gif" title="Sort the Active Queue by the default order"/></button>
    </td>
    <td unselectable="on">Program Manager:</td>
    <td unselectable="on">
      <select name="selProgramMgr" id="selProgramMgr" style="width:100%" onchange="selProgramMgr_onchange(this.value)">
        <xsl:for-each select="DistrictManager">
          <xsl:sort select="@NameLast"/>
          <xsl:variable name="curUserID"><xsl:value-of select="@UserID"/></xsl:variable>
          <xsl:variable name="Level">1</xsl:variable>
          <xsl:call-template name="showUser">
            <xsl:with-param name="curUserID"><xsl:value-of select="$UserId"/></xsl:with-param>
            <xsl:with-param name="Level"><xsl:value-of select="$Level"/></xsl:with-param>
            <xsl:with-param name="thisUserID"><xsl:value-of select="@UserID"/></xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
      </select>
    </td>
    <td style="padding:0px;padding-right:3px;">
      <button style="border:0px;padding:0px;margin:0px;background:transparent;" onclick="refreshQueue()"><img src="/images/refresh.gif" title="Refresh Active Work Queue"/></button>
      <button style="border:0px;padding:0px;margin:0px;background:transparent;" onclick="doMaxMin()"><img id="imgMaxMin" src="/images/showmore.gif" title="Maximize Active Work Queue"/></button>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="5">
      <IFRAME id="ifrmPMDMarketAnalysis" src="/blank.asp" style="width:100%; height:258; border:0px; visibility1:hidden; background-color:#FFFFFF; overflow:hidden" allowtransparency="false" >
      </IFRAME>
    </td>
  </tr>
</table>
<div id="divShopSearch" class="ShopAssignTableSrh" style="position:absolute;top:294px;border:0px solid #FF0000;overflow:hidden">
  <table border="0" cellspacing="0" cellpadding="2">
    <colgroup>
      <col width="75px"/>
      <col width="220px"/>
      <col width="35px"/>
      <col width="200px"/>
      <col width="35px"/>
      <col width="125px"/>
      <col width="25px"/>
      <col width="75px"/>
      <col width="100px"/>
    </colgroup>
    <tr>
      <td><b>Shop Name:</b></td><td><input id="txtShopName" class="inputFld" type="text" size="30" maxLength="50"/></td>
      <td><b>City:</b></td><td><input id="txtShopCity" class="inputFld" type="text" size="25" maxLength="50"/></td>
      <td><b>State:</b></td>
      <td>
        <select id="selState" style="width:120px">
          <option value=""></option>
          <xsl:for-each select="/Root/State">
            <xsl:sort select="DisplayOrder" data-type="number"/>
            <option>
              <xsl:attribute name="value"><xsl:value-of select="@StateCode"/></xsl:attribute>
              <xsl:value-of select="@StateName"/>
            </option>
          </xsl:for-each>
        </select>
      </td>
      <td><b>Zip:</b></td>
      <td><input id="txtShopZip" type="text" size="5" maxLength="5" class="inputFld" onkeypress="NumbersOnly(event)"/></td>
      <td><input type="checkbox" id="chkAllTerritories">All Territories</input></td>
      <td><input type="button" class="formbutton" value="Search" onclick="shopSearch()"/></td>
    </tr>
  </table>
  <IFRAME id="ifrmPMDShopSearch" src="/blank.asp" style="width:1011px; height:260; border:1px; visibility1:hidden; background-color:#FFFFFF; overflow:hidden" allowtransparency="false" onreadystatechange1="checkSearchProgress()" >
  </IFRAME>  
</div>

<xml id="xmlData">
  <xsl:copy-of select="/Root"/>
</xml>

</BODY>
</HTML>


</xsl:template>

<xsl:template name="showUser">
  <xsl:param name="curUserID"/>
  <xsl:param name="Level"/>
  <xsl:param name="thisUserID"/>
  <xsl:if test="count(/Root/ProgramManager[@UserID=$thisUserID]) = 0 or count(/Root/ProgramManager[@UserID=$thisUserID and @SupervisorUserID = '']) = 1">
    <option>
  		<xsl:if test="@UserID = $curUserID">
  			<xsl:attribute name="selected"/>
  		</xsl:if>
  
  		<xsl:attribute name="value"><xsl:value-of select="$thisUserID"/></xsl:attribute>
      <xsl:attribute name="level"><xsl:value-of select="$Level"/></xsl:attribute>
  		<xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/>
  	</option>
    
      <xsl:call-template name="showSubUser">
        <xsl:with-param name="curUserID"><xsl:value-of select="$curUserID"/></xsl:with-param>
        <xsl:with-param name="thisUserID"><xsl:value-of select="$thisUserID"/></xsl:with-param>
        <xsl:with-param name="Level"><xsl:value-of select="number($Level) + 1"/></xsl:with-param>
      </xsl:call-template>
    
	</xsl:if>
</xsl:template>

<xsl:template name="showSubUser">
  <xsl:param name="curUserID"/>
  <xsl:param name="thisUserID"/>
  <xsl:param name="Level"/>
  <xsl:for-each select="/Root/ProgramManager[@SupervisorUserID = $thisUserID]">
    <xsl:sort select="@NameLast"/>
    <option>
  		<xsl:if test="@UserID = $curUserID">
  			<xsl:attribute name="selected"/>
  		</xsl:if>
  		<xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>
      <xsl:attribute name="level"><xsl:value-of select="$Level"/></xsl:attribute>
  		<xsl:value-of disable-output-escaping="yes" select="user:space(number($Level))"/><xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/>
  	</option>
    <xsl:call-template name="showSubUser">
      <xsl:with-param name="curUserID"><xsl:value-of select="$curUserID"/></xsl:with-param>
      <xsl:with-param name="thisUserID"><xsl:value-of select="@UserID"/></xsl:with-param>
      <xsl:with-param name="Level"><xsl:value-of select="number($Level) + 1"/></xsl:with-param>
    </xsl:call-template>
  </xsl:for-each>
</xsl:template>


</xsl:stylesheet>
