<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<!-- APDXSL:/LynxApdDev/,'DevXSL','NoteDetails',1,2600,1,1  -->
<xsl:template match="/">
  <html>
    <head>
      <title>Reassign Current Claim</title>
      <link rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
      <link rel="stylesheet" href="/css/tabs.css" type="text/css"/>
      <link rel="stylesheet" href="/css/coolselect.css" type="text/css" />

      <style type="text/css">
          A {color:black; text-decoration:none;}
      </style>

      <xsl:variable name="UserId"/>

      <script language="JavaScript" src="js/CoolSelect.js"></script>  
	  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
      <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
      </script>

	  
    </head>
    <body bgcolor="#FFFAEB" text="#000000" onUnload="">
      <div style="position:absolute; z-index:1; top:10px; left:10px;">
        <form name="form1" method="post" action="ClaimReassignmentDialog.asp" target="thisWin">
          <table width='100%' border='1' cellspacing='2' cellpadding='0'>
            <tr>
              <td width="16%">Name</td>
              <td width="34%">UserID</td>
            </tr>
            <xsl:apply-templates select="Root|//User">
              <xsl:sort select="@NameLast"/>
              <xsl:sort select="@NameFirst"/>
            </xsl:apply-templates>
          </table>
        </form>
      </div>
    </body>
  </html>
</xsl:template>

<xsl:template match="Root|User">
    <tr>
      <td width="16%">
        <xsl:value-of disable-output-escaping="yes" select="@NameLast"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of disable-output-escaping="yes" select="@NameFirst"/>
      </td>
      <td width="34%">
        <xsl:value-of disable-output-escaping="yes" select="@UserID"/>
      </td>
    </tr>
</xsl:template>

</xsl:stylesheet>
