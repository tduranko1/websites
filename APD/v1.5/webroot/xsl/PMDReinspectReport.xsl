<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDReinspectForm">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
	<xsl:variable name="SupervisorForm" select="@SupervisorForm"/>
	
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectFormGetDetailXML,PMDReinspectReport.xsl,777,0,33 -->

<HEAD>
  <TITLE>Reinspection Report</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" bgcolor="#FFFAEB" style="margin-right:none">
  <div id="rptText" style="position:absolute; left:10px;">
	
	<table cellspacing="0" cellpadding="0" border="0" width="670" style="border-bottom:4px double #000000;margin-bottom:8px;">
      <tr>
        <td><div style="font:bolder 12pt Tahoma;">Reinspection Report</div></td>
        <td><div style="font:bolder 12pt Tahoma;" align="right">LYNX Services</div></td>
      </tr>
    </table>
	
	<IMG src="/images/spacer.gif" width="1" height="10" border="0" />
	
	<table cellspacing="0" cellpadding="0" border="0" width="670">
	  <tr style="background-color:silver;">
        <td width="3px"></td>
		<td width="165px" nowrap="nowrap"><strong>Program Mgr:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="Reference[@ListName='ProgramManagers' and @ReferenceID=//Reinspect/@ProgramManagerUserID]/@Name"/></td>
        <td width="155px" nowrap="nowrap"><strong>Shop Rep:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</strong><xsl:value-of select="Reinspect/@ShopRepName"/></td>
        <td width="130px" nowrap="nowrap"><strong>ReI Date:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</strong><xsl:value-of select="Reinspect/@ReinspectionDate"/></td>
        <td></td>
      </tr>
	</table>
	
	<IMG src="/images/spacer.gif" width="1" height="4" border="0" />
	
	<!-- get ReI header info -->
	<xsl:apply-templates select="Reinspect"/>					<!-- get form data -->
	  
	
	<!-- ReI Detail Item Header Row -->  
    <table cellspacing="0" cellpadding="3" border="1" width="670" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px;">
	  <TR unselectable="on" style="background-color:silver">
	    <TD unselectable="on" width="60" align="center"><strong>Item</strong></TD>
		<TD unselectable="on" width="120" align="center"><strong>Exception Code</strong></TD>
		<TD unselectable="on" width="60" align="center"><strong>Estimate Item</strong></TD>
		<TD unselectable="on"  align="center"><strong>Description</strong></TD>
		<TD unselectable="on" width="80" align="center"><strong>Estimate Amt</strong></TD>
		<TD unselectable="on" width="80" align="center"><strong>ReI Amt</strong></TD>
		<TD unselectable="on" width="80" align="center"><strong>Difference</strong></TD>
	  </TR>
		
	  <!-- get ReI detail items -->		
	  <xsl:for-each select="Reinspect/ReinspectDetail" >
        <xsl:call-template name="ReinspectDetail"/>
      </xsl:for-each>
	</table>
    
	<IMG src="/images/spacer.gif" width="1" height="30" border="0" />
	 
	<table cellspacing="0" cellpadding="3" border="1" width="670" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px;">
	  <tr>
	    <td valign="top"><strong>Comments</strong></td>
	  </tr>
	  <tr>
	    <td colspan="2">
		  <xsl:choose>
		  	<xsl:when test="Reinspect/@Comment != ''">
		      <xsl:value-of select="Reinspect/@Comment"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		  </xsl:choose>
		</td>
      </tr>
	</table>
	
	<IMG src="/images/spacer.gif" width="1" height="30" border="0" />
	
	<table width="670" style="table-layout:fixed;">
	  <tr>
	    <td nowrap="nowrap" width="120px">Program Manager:</td>
		<td width="230px"></td>
		<td width="60px"></td>
		<td width="50px">Date:</td>
		<td width="200px"></td>
	  </tr>
	  <tr>
	    <td></td>
		<td><hr/></td>
		<td></td>
		<td></td>
		<td><hr/></td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap">Shop Representative:</td>
		<td></td>
	  </tr>
	  <tr>
	    <td></td>
		<td><hr/></td>
		<td></td>
		<td></td>
		<td></td>
	  </tr>
	</table> 
 </div> <!-- rptText -->
</BODY>
</HTML>

</xsl:template>


<xsl:template match="Reinspect">

<table cellpadding="0" cellspacing="0" border="0" style="table-layout:fixed">
<tr>
  <td valign="top" width="380">
	<table cellspacing="0" cellpadding="3" border="1" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px; table-layout:fixed;">
	  <tr>
	    <td width="100" nowrap="nowrap"><strong>LYNX ID</strong> </td>
		<td nowrap="nowrap" width="270">
		  	<xsl:choose>
				<xsl:when test="@LynxID != ''">
					<xsl:value-of select="@LynxID"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>	
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Ins Company</strong> </td>
		<td nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@InsuranceCompany != ''">
					<xsl:value-of select="@InsuranceCompany"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Claim Rep</strong> </td>
		<td nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@ClaimRep != ''">
					<xsl:value-of select="@ClaimRep"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Estimate Date</strong></td>
	    <td nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@EstimateDate != ''">
					<xsl:value-of select="@EstimateDate"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
      </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Supplement</strong></td>
		<td nowrap="nowrap"><xsl:value-of select="@Supplement"/></td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Driveable</strong></td>
		<td nowrap="nowrap">
			<xsl:choose>
				<xsl:when test="@DrivableFlag = ''">
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="@DrivableFlag = 0">No</xsl:when>
						<xsl:otherwise>Yes</xsl:otherwise>							
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Shop Name</strong></td>
		<td nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@ShopName != ''">
					<xsl:value-of select="@ShopName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
      </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Estimator Name</strong></td>
		<td nowrap="nowrap">
			<xsl:choose>
				<xsl:when test="@EstimatorName != ''">
					<xsl:value-of select="@EstimatorName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	</table>
  </td>
  <td width="10px"></td> <!-- spacer column -->
  
    <td width="335" valign="top" align="left">
    <table cellspacing="0" cellpadding="3" border="1" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px; table-layout:fixed;">
	  <tr>
	    <td width="110" nowrap="nowrap"><strong>Repair Status</strong></td>
	    <td nowrap="nowrap" width="150">
		  <xsl:choose>
		  	<xsl:when test="@RepairStatusCD != ''">
			  <xsl:value-of select="/Root/Reference[@ListName='RepairStatus' and @ReferenceID=/Root/Reinspect/@RepairStatusCD]/@Name"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		  </xsl:choose>
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>ReI Class</strong></td>
	    <td nowrap="nowrap">
		  <xsl:choose>
		  	<xsl:when test="@ReinspectClassCD != ''">
		      <xsl:value-of select="/Root/Reference[@ListName='ReinspectionClass' and @ReferenceID=/Root/Reinspect/@ReinspectClassCD]/@Name"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		  </xsl:choose>
		</td>
      </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>ReI Type</strong></td>
	    <td nowrap="nowrap">
		  <xsl:choose>
		  	<xsl:when test="@ReinspectTypeCD != ''">
		      <xsl:value-of select="/Root/Reference[@ListName='ReinspectionType' and @ReferenceID=/Root/Reinspect/@ReinspectTypeCD]/@Name"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		  </xsl:choose>
		</td>
      </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Reinspector</strong></td>
	    <td nowrap="nowrap">
		  <xsl:choose>
		  	<xsl:when test="@ReinspectorName != ''">
		      <xsl:value-of select="@ReinspectorName"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		  </xsl:choose>
		</td>
	  </tr>
	</table>

	<IMG src="/images/spacer.gif" width="1" height="30" border="0" />
	
	<table cellspacing="0" cellpadding="3" border="1" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px; table-layout:fixed;">
	  <tr>
	    <td nowrap="nowrap" width="140px"><strong>Base Estimate Amt</strong></td>
		<td width="70" style="text-align:right;">
		  <xsl:value-of select="@BaseEstimateAmt"/>
		</td>
	  </tr>
	  <!--<tr>
	    <td cnowrap="nowrap"><strong>Corrected Estimate</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@CorrectedEstimate"/>
		</td>
	  </tr>-->
	  <tr>
	    <td nowrap="nowrap"><strong>Total Additions</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@TotalAdditions"/>
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Total Subtractions</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@TotalSubtractions"/>
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Actual Difference</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@DifferenceActual"/>
		</td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><strong>Absolute Difference</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@DifferenceAbsolute"/>
		</td>
	  </tr>
	  <tr>
	    <!--<td class="labelColumn" nowrap="nowrap">Satisfactory Amt: </td>-->
		<td nowrap="nowrap"><strong>Corrected Estimate</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@SatisfactoryDollars"/>
		</td>
	  </tr>
	  <tr>
	  	<td nowrap="nowrap"><strong>Percent Satisfactory</strong></td>
		<td style="text-align:right;">
		  <xsl:value-of select="@PercentSatisfactory"/>
		</td>
	  </tr>
	  
	</table>
  </td>
</tr>
<tr><td height="20"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td></tr> <!-- spacer row -->
</table>  
</xsl:template>



<xsl:template name="ReinspectDetail">
  
  <TR unselectable="on">
    <TD unselectable="on" align="center"><xsl:value-of select="position()"/></TD>
	<TD unselectable="on">
	  <xsl:choose>
	  	<xsl:when test="@ReinspectExceptionID != ''">
			<xsl:variable name="ReIException" select="@ReinspectExceptionID"/>
      		<xsl:value-of select="@ReinspectExceptionID"/>-
			<xsl:value-of select="/Root/Reference[@ListName='ExceptionCodes' and @ReferenceID=$ReIException]/@Name"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" align="center">
	  <xsl:choose>
	  	<xsl:when test="@EstimateDetailNumber != '0'">
      		<xsl:value-of select="@EstimateDetailNumber"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" style="text-align:left">
      <xsl:choose>
	  	<xsl:when test="@Comment != ''">
      		<xsl:value-of select="@Comment"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" style="text-align:right">
      <xsl:choose>
	  	<xsl:when test="@EstimateAmt != ''">
      		<xsl:value-of select="@EstimateAmt"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" style="text-align:right">
      <xsl:choose>
	  	<xsl:when test="@ReinspectAmt != ''">
      		<xsl:value-of select="@ReinspectAmt"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
	</TD>
    <TD unselectable="on" style="text-align:right">
	  <xsl:choose>
	  	<xsl:when test="@Difference != ''">
      		<xsl:value-of select="@Difference"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </TD> 
  </TR> 
</xsl:template>
  
</xsl:stylesheet>

