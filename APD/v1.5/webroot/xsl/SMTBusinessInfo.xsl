<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTBusinessInfo">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="Deleted"/>
<xsl:param name="mode"/>

<xsl:template match="/Root">

<xsl:if test="$Deleted='yes'">
  <script>
    top.window.close();
  </script>
</xsl:if>

<xsl:variable name="BusinessInfoID" select="@BusinessInfoID"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTBusinessInfoGetDetailXML,SMTBusinessInfo.xsl, 3152   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>

<!-- CLIENT SCRIPTS -->
<xsl:if test="$mode != 'wizard'"><SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT></xsl:if>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

var gbProgramShop = "FALSE";
var gbReferralShop = "FALSE";

var gsMode = '<xsl:value-of select="$mode"/>'
var gsPageFile = "SMTBusinessInfo.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';
var gsProgramShopCount = '<xsl:value-of select="@ProgramShopCount"/>';

<xsl:if test="$mode != 'wizard'">		<!-- don't set these in 'wizard' modes as it destroys state -->
  top.gsEIN = '<xsl:value-of select="BusinessInfo/@FedTaxId"/>';
  top.gsCancelSearchType = "B";
  top.gsBusinessInfoID = '<xsl:value-of select="$BusinessInfoID"/>';
  top.gsBusinessParentID = "<xsl:value-of select='BusinessInfo/@BusinessParentID'/>";


 <!-- prep data to be sent to Header Info tab -->
 top.gsInfoName = top.StringPrep('<xsl:value-of select='translate(translate(BusinessInfo/@Name, "&apos;", "|"), "\", "/")'/>');
 top.gsInfoAddress = top.StringPrep('<xsl:value-of select='translate(translate(BusinessInfo/@Address1, "&apos;", "|"), "\", "/")'/>' + ' ' + '<xsl:value-of select='translate(translate(BusinessInfo/@Address2, "&apos;", "|"), "\", "/")'/>');

 <xsl:variable name="gsCity">
   <xsl:value-of select="normalize-space(BusinessInfo/@AddressCity)"/>
   <xsl:if test="BusinessInfo/@AddressCity != '' and BusinessInfo/@AddressState != ''">, </xsl:if>
   <xsl:value-of select="BusinessInfo/@AddressState"/>
   <xsl:if test="BusinessInfo/@AddressCity != '' or BusinessInfo/@AddressState != ''"><xsl:text>  </xsl:text></xsl:if>
   <xsl:value-of select="BusinessInfo/@AddressZip"/>
 </xsl:variable>

  top.gsInfoCity = top.StringPrep('<xsl:value-of select='translate(translate($gsCity, "&apos;", "|"), "\", "/")'/>');
  top.gsInfoPhone = '<xsl:value-of select="BusinessInfo/@PhoneAreaCode"/>' + "-" + '<xsl:value-of select="BusinessInfo/@PhoneExchangeNumber"/>' + "-" + '<xsl:value-of select="BusinessInfo/@PhoneUnitNumber"/>';

  <!-- end Header data prep -->
</xsl:if>

<![CDATA[

//init the table selections, must be last
function initPage(){
  
	if (gsMode != 'wizard')
		InitEditPage();
	else{
		SetWizNames();
		parent.frameLoaded(1);
	}

	onpasteAttachEvents()

	//set dirty flags associated with all inputs and selects and checkboxes.
	var aInputs = null;
	var obj = null;

	aInputs = document.all.tags("INPUT");
	for (var i=0; i < aInputs.length; i++){
		if (gsShopCRUD.indexOf("U") == -1)
			aInputs[i].setAttribute("disabled", "true");
    
		if (aInputs[i].type=="text"){
			aInputs[i].attachEvent("onchange", SetDirty);
			aInputs[i].attachEvent("onkeyup", SetDirty);
			aInputs[i].attachEvent("onpropertychange", SetDirty);
		}

		if (aInputs[i].type=="checkbox")
			aInputs[i].attachEvent("onclick", SetDirty);
	}

	aInputs = document.all.tags("SELECT");
	for (var i=0; i < aInputs.length; i++){
		aInputs[i].attachEvent("onchange", SetDirty);

		if (gsShopCRUD.indexOf("U") == -1)
			aInputs[i].setAttribute("disabled", "true");
	}

	if (gsProgramShopCount > 0) FlagRequiredFields(1);
	if (gsShopCRUD.indexOf("U") != -1) frmBusinessInfo.txtFedTaxID.focus();
  
	parent.gbDirtyFlag = false;  
}

function InitEditPage(){
	//top.gsPageFile = gsPageFile;
	parent.btnCancel.style.visibility = "hidden";

	if (gsShopCRUD.indexOf("U") > -1){
		parent.btnSave.style.display = "inline";
		parent.btnSave.style.visibility = "visible";
		parent.btnSave.disabled = false;
	}
	else
		parent.btnSave.style.display = "none";

	if (gsShopCRUD.indexOf("D") > -1){
		parent.btnDelete.style.display = "inline";
		parent.btnDelete.style.visibility = "visible";
		parent.btnDelete.disabled = false;
	}
	else
		parent.btnDelete.style.display = "none";

	parent.document.all.tab11.className = "tabactive1";
	parent.gsPageName = "Business Info";
	
	top.SetInfoHeader("  Business ID:", top.gsBusinessInfoID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone);
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function txtAddressZip_onBlur(){
	IsValidZip(frmBusinessInfo.txtAddressZip);
	ValidateZip(frmBusinessInfo.txtAddressZip, 'selState', 'AddressCity');
	ValidateZipToCounty(frmBusinessInfo.txtAddressZip, 'AddressCounty');
}

function txtFedTaxID_onBlur(){
	if (frmBusinessInfo.txtFedTaxID.dirty == true){
		ValidateFedTaxID(document.all.txtFedTaxID);
		frmBusinessInfo.txtFedTaxID.dirty = "false";
	}
}

function ValidateAllPhones() {

	if (validatePhoneSections(document.all.PhoneAreaCode, document.all.PhoneExchangeNumber, document.all.PhoneUnitNumber) == false) return false;
		if (validatePhoneSections(document.all.FaxAreaCode, document.all.FaxExchangeNumber, document.all.FaxUnitNumber) == false) return false;
  
	if (isNumeric(document.all.PhoneExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified. Please try again.");
		document.all.PhoneExtensionNumber.focus();
		document.all.PhoneExtensionNumber.select();
		return false;
	}

	if (isNumeric(document.all.FaxExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified. Please try again.");
		document.all.FaxExtensionNumber.focus();
		document.all.FaxExtensionNumber.select();
		return false;
	}

	validateAreaCodeSplit(document.all.PhoneAreaCode, document.all.PhoneExchangeNumber);
	validateAreaCodeSplit(document.all.FaxAreaCode, document.all.FaxExchangeNumber);
	return true;
}

function IsDirty(){
	Save();
}

function ValidatePage(){

	if (gsMode == "wizard"){
		if (frmBusinessInfo.rdProgramShop.checked == false && frmBusinessInfo.rdNonProgramShop.checked == false){
			parent.ClientWarning("Please indicate whether or not Shops associated with this Business are to be Program Shops.")
			return false;
		}
		
		if (frmBusinessInfo.rdProgramShop.checked == true && frmBusinessInfo.txtFedTaxID.value == ""){
			parent.ClientWarning("An EIN is required for all Businesses with associated Program Shops.")
			frmBusinessInfo.txtFedTaxID.focus();
			return false;
		}
		
		if (frmBusinessInfo.rdReferralShop.checked == false && frmBusinessInfo.rdNonReferralShop.checked == false){
			parent.ClientWarning("Please indicate whether or not Shops associated with this Business are to be Referral Shops.")
			return false;
		}
		
		if (frmBusinessInfo.rdReferralShop.checked == true && frmBusinessInfo.txtFedTaxID.value == ""){
			parent.ClientWarning("An EIN is required for all Businesses with associated Referral Shops.")
			frmBusinessInfo.txtFedTaxID.focus();
			return false;
		}
	}
	else{
		if (gsProgramShopCount > 0 && frmBusinessInfo.txtFedTaxID.value == ""){
			var sMsg = "There are " + gsProgramShopCount + " Program Shop(s) associated with this Business.";
			sMsg += "  An EIN is required for all Businesses with associated Program Shops."
			parent.ClientWarning(sMsg);
			frmBusinessInfo.txtFedTaxID.focus();
			return false;
		}
	}

	if (!ValidateFedTaxID(frmBusinessInfo.txtFedTaxID)) return false;

	if (frmBusinessInfo.Name.value == ""){
		parent.ClientWarning("A value is required in the \"Name\" field.");
		frmBusinessInfo.txtName.focus();
		return false;
	}

	if (!ValidateAllPhones()) return false;

	return true;
}

function Save(){
	
	if (!ValidatePage()) return false;
		parent.btnSave.disabled = true;
		
	frmBusinessInfo.action += "?mode=update";
	frmBusinessInfo.submit();
	parent.gbDirtyFlag = false;
}

function Delete(){

	var msg = "Are you sure you want to delete this Business and all its Shops?  Delete cannot be undone.  Click 'Yes' to delete.";
	var sDelete = YesNoMessage("Business Maintenance", msg)
	
	if (sDelete == "Yes"){
		parent.btnDelete.disabled = true;
		frmBusinessInfo.action += "?mode=delete";
		frmBusinessInfo.submit();
		parent.gbDirtyFlag = false;
	}
}

function txtFedTaxID_onKeyPress(){

	if (event.keyCode == 45 || event.keyCode == 46){ 
		event.returnValue = false;
		return false;
	}    
    
	NumbersOnly(window.event); 
	this.dirty=true;
}

function GridMouseOver(oObject){
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
}

function GridMouseOut(oObject){
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
}

function rdProgramShop_onclick(obj){

	if (obj.id == "rdProgramShop"){
		gbProgramShop = "TRUE";
		FlagRequiredFields(1);
	} else {
		gbProgramShop = "FALSE";
		FlagRequiredFields(0);
	}
}

function rdReferralShop_onclick(obj){

	if (obj.id == "rdReferralShop"){
		gbReferralShop = "TRUE";
		FlagRequiredFields(1);
	} else {
		gbReferralShop = "FALSE";
		FlagRequiredFields(0);
	}
}

function FlagRequiredFields(bProgramFlag){

	if ( gbReferralShop == "TRUE" || gbProgramShop == "TRUE") 
		document.all.spFedTaxID.style.visibility = "visible";  
	else
		document.all.spFedTaxID.style.visibility = "hidden";  

}

]]>
<xsl:if test="$mode='wizard'">
	function SetWizNames(){
		frmBusinessInfo.txtPhoneAreaCode.setAttribute("wizname", "BPhoneAreaCode");
		frmBusinessInfo.txtPhoneExchangeNumber.setAttribute("wizname", "BPhoneExchangeNumber");
		frmBusinessInfo.txtPhoneUnitNumber.setAttribute("wizname", "BPhoneUnitNumber");
		frmBusinessInfo.txtPhoneExtensionNumber.setAttribute("wizname", "BPhoneExtensionNumber");
		frmBusinessInfo.txtFaxAreaCode.setAttribute("wizname", "BFaxAreaCode");
		frmBusinessInfo.txtFaxExchangeNumber.setAttribute("wizname", "BFaxExchangeNumber");
		frmBusinessInfo.txtFaxUnitNumber.setAttribute("wizname", "BFaxUnitNumber");
		frmBusinessInfo.txtFaxExtensionNumber.setAttribute("wizname", "BFaxExtensionNumber");
	}
</xsl:if>

</SCRIPT>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" onLoad="initPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
  <div style="background-color:#FFFAEB;height:475px;">
   <IMG src="/images/spacer.gif" width="6" height="12" border="0" />
   <!-- Render Business Data -->
   <FORM id="frmBusinessInfo" method="post">
     <xsl:apply-templates select="BusinessInfo">
       <xsl:with-param name="BusinessInfoID" select="$BusinessInfoID"/>
       <xsl:with-param name="mode" select="$mode"/>
     </xsl:apply-templates>

	   <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
	     <xsl:attribute name="value"><xsl:value-of select="BusinessInfo/@SysLastUpdatedDate"/></xsl:attribute>
	   </input>

     <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
	     <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
	   </input>
	   <input type="hidden" id="txtBusinessInfoID" name="BusinessInfoID">
		   <xsl:attribute name="value"><xsl:value-of select="@BusinessInfoID"/></xsl:attribute>
	   </input>
     <input type="hidden" id="txtCallBack" name="CallBack"/>
	   <input type="hidden" id="txtEnabledFlag" wizname="BEnabledFlag" value="1"/>
   </FORM>
   <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

   <xsl:if test="count(Duplicate) &gt;= 1"><xsl:call-template name="Duplicates"/></xsl:if>
  </div>

  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</BODY>
</HTML>
</xsl:template>

<!-- Gets Business Info -->
<xsl:template match="BusinessInfo">
  <xsl:param name="BusinessInfoID"/>
  <xsl:param name="mode"/>
    <table border="0" cellspacing="0" cellpadding="0" style="overflow:clip;">
      <tr>
        <td>
          <table cellpadding="1" cellspacing="0" border="0">
			<xsl:if test="$mode='wizard'">
				<tr>
					<td><strong><span>*</span></strong></td>
					<td nowrap="nowrap">Program Shop:</td>
					<td nowrap="nowrap"><input type="radio" name="rdoPgm" id="rdProgramShop" onclick="rdProgramShop_onclick(this)"></input>Yes</td>
		        </tr>
		        <tr>
					<td></td>
		            <td></td>
		            <td>
						<input type="radio" name="rdoPgm" id="rdNonProgramShop" onclick="rdProgramShop_onclick(this)"></input>No
						<input type="hidden" id="txtProgramFlag" class="inputFld"/>
					</td>
				</tr>
				<tr>
					<td><strong><span>*</span></strong></td>
					<td nowrap="nowrap">Referral Shop:</td>
					<td nowrap="nowrap"><input type="radio" name="rdoRef" id="rdReferralShop" onclick="rdReferralShop_onclick(this)"></input>Yes</td>
				</tr>
		        <tr>
					<td></td>
		            <td></td>
		            <td>
						<input type="radio" name="rdoRef" id="rdNonReferralShop" onclick="rdReferralShop_onclick(this)"></input>No
						<input type="hidden" id="txtReferralFlag" class="inputFld"/>
					</td>
				</tr>

            </xsl:if>
            <tr>
              <td><span id="spFedTaxID" style="visibility:hidden; font-weight:bold;">*</span></td>
              <td nowrap="on">SSN/EIN:</td>
              <td>
                <input type="text" id="txtFedTaxID" name="FedTaxId" wizname="BFedTaxId" class="inputFld" maxlength="9" dirty="false" onblur="txtFedTaxID_onBlur()" onchange="this.dirty=true;" onkeypress="txtFedTaxID_onKeyPress()">
				          <xsl:attribute name="value"><xsl:value-of select="@FedTaxId"/></xsl:attribute>
			          </input>
              </td>
            </tr>
            <tr>
              <td><span style="font-weight:bold;">*</span></td>
              <td width="70">Name:</td>
              <td>
                <input type="text" id="txtName" name="Name" wizname="BName" autocomplete="off" class="inputFld" style="width:200">
				          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
	  	            <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>Address1: </td>
              <td>
                <input type="text" id="txtAddress1" name="Address1" wizname="BAddress1" class="inputFld" style="width:200">
				          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
	  	            <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>Address2: </td>
              <td>
                <input type="text" id="txtAddress2" name="Address2" wizname="BAddress2" class="inputFld" style="width:200">
				          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
	  	            <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>Zip: </td>
              <td>
                <input type="text" id="txtAddressZip" name="AddressZip" wizname="BAddressZip" class="inputFld" onblur="txtAddressZip_onBlur()" maxlength="5" onkeypress="NumbersOnly(window.event)">
				          <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>City: </td>
              <td>
                <input type="text" id="txtAddressCity" name="AddressCity" wizname="BAddressCity" class="inputFld">
				          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
	  	            <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>State: </td>
              <td>
				        <select id="selState" name="AddressState" wizname="BAddressState">
				          <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='State']">
				            <xsl:call-template name="BuildSelectOptions">
					          <xsl:with-param name="current"><xsl:value-of select="/Root/BusinessInfo/@AddressState"/></xsl:with-param>
					          </xsl:call-template>
			            </xsl:for-each>
				        </select>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>County:</td>
              <td>
                <input type="text" id="txtAddressCounty" name="AddressCounty" wizname="BAddressCounty" class="inputFld">
				          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
	  	            <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
				        </input>
              </td>
            </tr>
            <tr>
              <td><IMG src="/images/spacer.gif" width="6" height="6" border="0" /></td>
            </tr>
          </table>
        </td>
        <td width="20px"></td> <!-- spacer column -->
        <td valign="top">
          <table cellpadding="1" cellspacing="0" border="0" width="390px">
            <tr>
              <td></td>
              <td>Phone:</td>
              <td nowrap="nowrap">
                <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
				      </td>
            </tr>
            <tr>
              <td></td>
              <td>Fax:<span></span></td>
              <td nowrap="nowrap">
                <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>Parent:</td>
              <td>
                <select id="selBusParent" name="BusinessParentID" wizname="BBusinessParentID">
                  <option value=""></option>
					        <xsl:for-each select="/Root/Reference[@ListName='Parent']">
				            <xsl:call-template name="BuildSelectOptions">
					            <xsl:with-param name="current"><xsl:value-of select="/Root/BusinessInfo/@BusinessParentID"/></xsl:with-param>
					          </xsl:call-template>
			            </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td><IMG src="/images/spacer.gif" width="6" height="6" border="0" /></td>
            </tr>
            <tr>
              <td><strong><span>*</span></strong></td>
              <td>Business Type:</td>
              <td>
                <select id="selBusinessType" name="BusinessTypeCD" wizname="BBusinessTypeCD">
                  <xsl:for-each select="/Root/Reference[@ListName='BusinessType']">
				            <xsl:call-template name="BuildSelectOptions">
					            <xsl:with-param name="current"><xsl:value-of select="/Root/BusinessInfo/@BusinessTypeCD"/></xsl:with-param>
					          </xsl:call-template>
			            </xsl:for-each>
                </select>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</xsl:template>

<xsl:template name="Duplicates">
  <DIV id="CNScrollTable" style="width:100%">

    <TABLE>
      <TR unselectable="on" class="QueueHeader">
        <TD colspan="4" class="TableSortHeader" style="cursor:default; color:red;">
          The following Businesses have the same EIN as the current Business.  EINs must be unique.  This problem
          will have to be corrected before any information on this page can be saved.
        </TD>
      </TR>
    </TABLE>

	  <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
      <TBODY>
        <TR unselectable="on" class="QueueHeader">
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="60" style="cursor:default"> ID </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="80" style="cursor:default"> EIN </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="100%" style="cursor:default"> Business Name </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="200" style="cursor:default"> Business Address </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="150" style="cursor:default"> City, State </TD>
			  </TR>
      </TBODY>
		</TABLE>

	  <DIV unselectable="on" class="autoflowTable" style="width:100%; height:130px;">
      <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="Duplicate">
            <xsl:call-template name="DuplicateDetail"/>
          </xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
  </DIV>
</xsl:template>

<xsl:template name="DuplicateDetail">
  <TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <TD unselectable="on" width="58" class="GridTypeTD">
      <a target="_parent">
        <xsl:attribute name="href">SMTDetailLevel.asp?SearchType=B&amp;BusinessInfoID=<xsl:value-of select="@BusinessInfoID"/></xsl:attribute>
        <xsl:value-of select="@BusinessInfoID"/>
      </a>
    </TD>
    <TD unselectable="on" width="80" class="GridTypeTD"><xsl:value-of select="@FedTaxID"/></TD>
    <TD unselectable="on" width="100%" class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@Name"/></TD>
    <TD unselectable="on" width="200" class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@Address1"/></TD>
    <TD unselectable="on" width="150" class="GridTypeTD" style="text-align:left;">
      <xsl:value-of select="@AddressCity"/>
      <xsl:if test="@AddressCity != '' and @AddressState != ''"><xsl:text>, </xsl:text></xsl:if>
      <xsl:value-of select="@AddressState"/>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>

