<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    id="METotals">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>

<xsl:template match="/Root">

  <!-- <xsl:param name="VehicleCRUD"/> -->
  <xsl:variable name="dataDisabled">
    <xsl:choose>
      <xsl:when test="contains($VehicleCRUD, 'U') = true()">false</xsl:when>
      <xsl:otherwise>true</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

<HTML>

<HEAD>
<TITLE>Mobile Electronics Totals and Disposition</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<STYLE type="text/css">
Input {
	font-family: Arial, sans-serif;
	font-weight: normal;
	font-size: 8pt;
	}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>

<SCRIPT language="JavaScript">

  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsClaimAspectID = "<xsl:value-of select="/Root/@ClaimAspectID"/>";
  var gsInsuranceCompanyID = "<xsl:value-of select="/Root/@InsuranceCompanyID"/>";
  var gsSysLastUpdatedDate = "<xsl:value-of select="/Root/MEInfo/@SysLastUpdatedDate"/>";
  var gsClaimDeductible = parseInt("<xsl:value-of select="/Root/@ClaimDeductible"/>", 10);
  var gsCoverageProfileCD = "<xsl:value-of select="/Root/@CoverageProfileCD"/>";

<![CDATA[

  function pageInit(){
    if (Stat1) {
      Stat1.style.top = (document.body.offsetHeight - 75) / 2;
      Stat1.style.left = (document.body.offsetWidth - 240) / 2;
    }
    
    if (gsCoverageProfileCD != "" && gsCoverageProfileCD != "COMP"){
      ClientWarning("Coverage for this entity is not set to Comprehensive. " + gsCoverageProfileCD + " deductible is used for calculation on this screen.")
    }
  }
  
  function onSave(){
    if (validateData()){
      btnSave.CCDisabled = true;
      btnCancel.CCDisabled = true;
      
      Stat1.Show("Saving changes. Please wait...");
      window.setTimeout("onSave2()", 50);
    }
  }
  
  function onSave2() {
    var sRequest =  "ClaimAspectID=" + gsClaimAspectID +
                    "&CoverageLimitBodyAmt=" + txtCoverageLimitBodyAmt.value + 
                    "&CoverageLimitMEAmt=" + txtCoverageLimitMEAmt.value +
                    "&DeductibleBodyAmt=" + txtDeductibleBodyAmt.value +
                    "&DeductibleGlassAmt=" + txtDeductibleGlassAmt.value +
                    "&DeductibleMEAmt=" + txtDeductibleMEAmt.value +
                    "&DispositionBodyCD=" + (selDispositionBodyCD.selectedIndex == -1 ? "" : selDispositionBodyCD.value) +
                    "&DispositionGlassCD=" + (selDispositionGlassCD.selectedIndex == -1 ? "" : selDispositionGlassCD.value) +
                    "&DispositionMECD=" + (selDispositionMECD.selectedIndex == -1 ? "" : selDispositionMECD.value) +
                    "&EndorsementDeductibleAmt=" + txtEndorsementDeductibleAmt.value +
                    "&EndorsementFlag=" + chkEndorsementFlag.value +
                    "&InvoiceBodyGrossAmt=" + txtInvoiceBodyGrossAmt.value +
                    "&InvoiceBodyNetAmt=" + txtInvoiceBodyNetAmt.value +
                    "&InvoiceGlassGrossAmt=" + txtInvoiceGlassGrossAmt.value +
                    "&InvoiceGlassNetAmt=" + txtInvoiceGlassNetAmt.value +
                    "&InvoiceMEGrossAmt=" + txtInvoiceMEGrossAmt.value +
                    "&InvoiceMENetAmt=" + txtInvoiceMENetAmt.value +
                    "&PaymentChannelBodyCD=" + (selPaymentChannelBodyCD.selectedIndex == -1 ? "" : selPaymentChannelBodyCD.value) +
                    "&PaymentChannelMECD=" + (selPaymentChannelMECD.selectedIndex == -1 ? "" : selPaymentChannelMECD.value) +
                    "&ProgramTypeCD=" + (selProgramTypeCD.selectedIndex == -1 ? "" : selProgramTypeCD.value) +
                    "&EndorsementLimitAmt=" + txtEndorsementLimitAmt.value +
                    "&CoverageExceptionMEFlag=" + chkCoverageExceptionMEFlag.value +
                    "&CashOutBodyDate=" + dteCashOutBodyDate.value +
                    "&CashOutGlassDate=" + dteCashOutGlassDate.value +
                    "&CashOutMEDate=" + dteCashOutMEDate.value +
                    "&VIRFeeAmt=" + txtVIRFeeAmt.value +
                    "&UserID=" + gsUserID +
                    "&SysLastUpdatedDate=" + gsSysLastUpdatedDate;

    //alert(sRequest); return;

    var sProc = "uspMETotalsUpdDetails";

    var aRequests = new Array();
    aRequests.push( { procName : sProc,
                      method   : "executespnp",
                      data     : sRequest }
                  );
    var objRet = XMLSave(makeXMLSaveString(aRequests));

    if (objRet.code == 0)
    {
      sRequest = "ClaimAspectID=" + gsClaimAspectID + 
                 "&InsuranceCompanyID=" + gsInsuranceCompanyID;

      sProc = "uspMETotalsGetDetailsXML";
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "executespnpasxml",
                        data     : sRequest }
                    );

      var objRet = XMLSave(makeXMLSaveString(aRequests));
      if (objRet.code == 0) {
          var objXML = objRet.xml;
          var UserNode = objXML.documentElement.selectSingleNode("//Root/MEInfo");
          gsSysLastUpdatedDate = UserNode.getAttribute("SysLastUpdatedDate");
          
          window.returnValue="";
          window.close();
      }
    }
    
    Stat1.Hide();
  }
  
  function validateData(){
    var blnRet;
    blnRet = true;
    if (xmlData){
      var strDisposition, strGrossAmt, strDedAmt, strNetAmt;
      var strMessage = "";

      //check Body Disposition rule.
      if (selDispositionBodyCD.selectedIndex != -1){
        strDisposition = selDispositionBodyCD.value;
        if (strDisposition == 'RC' || strDisposition == 'CO' || strDisposition == 'PCO'){
          if (txtInvoiceBodyGrossAmt.value == ""){
            strMessage += "* Body Gross Amount must be completed.<br/>";
          }
          if (txtDeductibleBodyAmt.value == ""){
            strMessage += "* Body Deductible must be completed.<br/>";
          }
          if (txtInvoiceBodyNetAmt.value == ""){
            strMessage += "* Body Net Amount must be completed.<br/>";
          }
          if (selProgramTypeCD.selectedIndex == -1 || (selProgramTypeCD.value != "LS" && selProgramTypeCD.value != "NON")){
            strMessage += "* Program Type must be either Lynx Select or Non-Program Shop<br/>";
          }
        }
      }
      
      //check ME Disposition rule.
      if (selDispositionMECD.selectedIndex != -1){
        strDisposition = selDispositionMECD.value;
        if (strDisposition == 'RC' || strDisposition == 'CO' || strDisposition == 'PCO'){
          if (txtInvoiceMEGrossAmt.value == ""){
            strMessage += "* ME Gross Amount must be completed.<br/>";
          }
          if (txtDeductibleMEAmt.value == ""){
            strMessage += "* ME Deductible must be completed.<br/>";
          }
          if (txtInvoiceMENetAmt.value == ""){
            strMessage += "* ME Net Amount must be completed.<br/>";
          }
        }
      }
      
      //check Glass Disposition rule.
      if (selDispositionGlassCD.selectedIndex != -1){
        strDisposition = selDispositionGlassCD.value;
        if (strDisposition == 'RC' || strDisposition == 'CO' || strDisposition == 'PCO'){
          if (txtDeductibleGlassAmt.value == ""){
            strMessage += "* Glass Deductible must be completed.<br/>";
          }
        }
      }

      // If Body Gross or Deductible is not blank, gross - deductible must be equal to net
      if (txtInvoiceBodyGrossAmt.value != "" || txtDeductibleBodyAmt.value != ""){
        if (Math.round((getFloatValue(txtInvoiceBodyGrossAmt.value) - getFloatValue(txtDeductibleBodyAmt.value))*100)/100 != getFloatValue(txtInvoiceBodyNetAmt.value)) {
          strMessage += "* Body Net Amount must be equal to Body Gross - Body Deductible.<br/>";
        }
      }

      // If Glass Gross or Deductible is not blank, gross - deductible must be equal to net
      if (txtInvoiceGlassGrossAmt.value != "" || txtDeductibleGlassAmt.value != ""){
        if (Math.round((getFloatValue(txtInvoiceGlassGrossAmt.value) - getFloatValue(txtDeductibleGlassAmt.value))*100)/100 != getFloatValue(txtInvoiceGlassNetAmt.value)) {
          strMessage += "* Glass Net Amount must be equal to Glass Gross - Glass Deductible.<br/>";
        }
      }

      // If ME Gross or Deductible is not blank, gross - deductible must be equal to net
      if (txtInvoiceMEGrossAmt.value != "" || txtDeductibleMEAmt.value != ""){
        if (Math.round((getFloatValue(txtInvoiceMEGrossAmt.value) - getFloatValue(txtDeductibleMEAmt.value))*100)/100 != getFloatValue(txtInvoiceMENetAmt.value)) {
          strMessage += "* ME Net Amount must be equal to ME Gross - ME Deductible.<br/>";
        }
      }
      
      // If ME Limit is greater than zero, gross - deductible must be equal to net
      if (parseFloat(txtCoverageLimitMEAmt.value) > 0){
        if (Math.round((parseFloat(txtInvoiceMEGrossAmt.value) - parseFloat(txtDeductibleMEAmt.value))*100)/100 != parseFloat(txtInvoiceMENetAmt.value)) {
          strMessage += "* ME Net Amount must be equal to ME Gross - ME Deductible.<br/>";
        }
      }
      
      //Endorsement check
      if ((chkEndorsementFlag.value != 1) && ((txtDeductibleBodyAmt.value != "") && (txtDeductibleGlassAmt.value != "") && (txtDeductibleMEAmt.value != ""))){
        
        var iDedTotals = 0;
        iDedTotals += parseFloat(txtDeductibleBodyAmt.value == "" ? 0 : txtDeductibleBodyAmt.value);
        iDedTotals += parseFloat(txtDeductibleGlassAmt.value == "" ? 0 : txtDeductibleGlassAmt.value);
        iDedTotals += parseFloat(txtDeductibleMEAmt.value == "" ? 0 : txtDeductibleMEAmt.value);
        
        if (iDedTotals > 0){
          if (iDedTotals != parseFloat(gsClaimDeductible)) {
            strMessage += "* Sum of Body (" + txtDeductibleBodyAmt.value + "), Glass (" + txtDeductibleGlassAmt.value + ") and ME (" + txtDeductibleMEAmt.value + ") deductibles must be equal to Claim deductible ($" + gsClaimDeductible + ").<br/>"
          }
        }
      }
      
      if (selProgramTypeCD.value == "NON"){
        if (selPaymentChannelBodyCD.value == "CCC"){
          strMessage += "* For a Non-Program Type the Body payment channel cannot be CCC Pathways.<br/>";
        }
      }
      
      strDisposition = selDispositionBodyCD.value;
      if (strDisposition == 'CO' || strDisposition == 'PCO'){
        if (dteCashOutBodyDate.value == ""){
          strMessage += "* Body Cash Out date cannot be empty.<br/>";
        }
      }
      
      strDisposition = selDispositionGlassCD.value;
      if (strDisposition == 'CO' || strDisposition == 'PCO'){
        if (dteCashOutGlassDate.value == ""){
          strMessage += "* Glass Cash Out date cannot be empty.<br/>";
        }
      }
      
      strDisposition = selDispositionMECD.value;
      if (strDisposition == 'CO' || strDisposition == 'PCO'){
        if (dteCashOutMEDate.value == ""){
          strMessage += "* ME Cash Out date cannot be empty.<br/>";
        }
      }
      
      if (strMessage != ""){
        ClientWarning("Please fix the following and try again.<br/><br/>" + strMessage);
        blnRet = false
      }
    }
    
    return blnRet;
  }

  function updateBodyNetAmt(){
    if (txtInvoiceBodyGrossAmt.value != "" && txtDeductibleBodyAmt.value != ""){
      txtInvoiceBodyNetAmt.value = Math.round((parseFloat(txtInvoiceBodyGrossAmt.value) - parseFloat(txtDeductibleBodyAmt.value))*100)/100;
    }
  }
  
  function updateGlassNetAmt(){
    if (txtInvoiceGlassGrossAmt.value != "" && txtDeductibleGlassAmt.value != ""){
      txtInvoiceGlassNetAmt.value = Math.round((parseFloat(txtInvoiceGlassGrossAmt.value) - parseFloat(txtDeductibleGlassAmt.value))*100)/100;
    }
  }

  function updateMENetAmt(){
    if (txtInvoiceMEGrossAmt.value != "" && txtDeductibleMEAmt.value != ""){
      txtInvoiceMENetAmt.value = Math.round((parseFloat(txtInvoiceMEGrossAmt.value) - parseFloat(txtDeductibleMEAmt.value))*100)/100;
    }
  }

  function getFloatValue(x)
  {
    if (x == "") x = 0;
    return Math.round(parseFloat(x)*100)/100;  //round to 2 decimal places
  }


  function onCancel(){
    window.returnValue="";
    window.close()
  }

]]>

</SCRIPT>

</HEAD>
<BODY unselectable="on" style="padding:5px;overflow:hidden;" tabIndex="-1" onload="pageInit()">
  <IE:APDStatus id="Stat1" name="Stat1" width="240" height="75"/>
  <table  width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
    <colgroup>
      <col width="50%"/>
      <col width="50%"/>
    </colgroup>
    <tr valign="top">
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
          <colgroup>
            <col width="82px"/>
            <col width="*"/>
          </colgroup>
          
          <tr>
            <td colspan="2"><b>Body</b></td>
          </tr>
          <tr>
            <td colspan="2"><img style="height:2px;width:100%" src="images/background_top.gif"/></td>
          </tr>
          <tr>
            <td>Limits:</td>
            <td>
              <IE:APDInputCurrency id="txtCoverageLimitBodyAmt" name="txtCoverageLimitBodyAmt" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="1" onChange="" >
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@CoverageLimitBodyAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Invoice (Gross):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceBodyGrossAmt" name="txtInvoiceBodyGrossAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="2" onChange="updateBodyNetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceBodyGrossAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Ded. applied:</td>
            <td>
              <IE:APDInputCurrency id="txtDeductibleBodyAmt" name="txtDeductibleBodyAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="3" onChange="updateBodyNetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DeductibleBodyAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Invoice (Net):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceBodyNetAmt" name="txtInvoiceBodyNetAmt" value="" precision="9" scale="2" width="81" neg="true" required="false" canDirty="true" CCTabIndex="4" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceBodyNetAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Disposition:</td>
            <td>
              <IE:APDCustomSelect id="selDispositionBodyCD" name="selDispositionBodyCD" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="5" required="false" width="93" dropDownWidth="150" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DispositionBodyCD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='DispositionBodyCD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Payment Chnl:</td>
            <td>
              <IE:APDCustomSelect id="selPaymentChannelBodyCD" name="selPaymentChannelBodyCD" value="" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="6" required="false" width="93" dropDownWidth="100" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@PaymentChannelBodyCD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='PaymentChannelBodyCD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Cash Out Date:</td>
            <td>
              <IE:APDInputDate id="dteCashOutBodyDate" name="dteCashOutBodyDate" type="date" futureDate="false" required="false" canDirty="false" CCTabIndex="7" onChange="">
                <xsl:attribute name="value">
                  <xsl:if test="/Root/MEInfo/@CashOutBodyDate != '1900-01-01T00:00:00'">
                    <xsl:value-of select="js:formatSQLDate(string(/Root/MEInfo/@CashOutBodyDate))"/>
                  </xsl:if>
                </xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputDate>
            </td>
          </tr>
          <tr><td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td></tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
          <colgroup>
            <col width="85px"/>
            <col width="*"/>
          </colgroup>
          
          <tr>
            <td colspan="2"><b>Glass</b></td>
          </tr>
          <tr>
            <td colspan="2"><img style="height:2px;width:100%" src="images/background_top.gif"/></td>
          </tr>
          <tr>
            <td>Invoice (Gross):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceGlassGrossAmt" name="txtInvoiceGlassGrossAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="8" onChange="updateGlassNetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceGlassGrossAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Ded. applied:</td>
            <td>
              <IE:APDInputCurrency id="txtDeductibleGlassAmt" name="txtDeductibleGlassAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="9" onChange="updateGlassNetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DeductibleGlassAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Invoice (Net):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceGlassNetAmt" name="txtInvoiceGlassNetAmt" value="" precision="9" scale="2" width="81" neg="true" required="false" canDirty="true" CCTabIndex="10" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceGlassNetAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Disposition:</td>
            <td>
              <IE:APDCustomSelect id="selDispositionGlassCD" name="selDispositionGlassCD" value="" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="11" required="false" width="93" dropDownWidth="150" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DispositionGlassCD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='DispositionGlassCD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Cash Out Date:</td>
            <td>
              <IE:APDInputDate id="dteCashOutGlassDate" name="dteCashOutGlassDate" type="date" futureDate="false" required="false" canDirty="false" CCTabIndex="12" onChange="">
                <xsl:attribute name="value">
                  <xsl:if test="/Root/MEInfo/@CashOutGlassDate != '1900-01-01T00:00:00'">
                    <xsl:value-of select="js:formatSQLDate(string(/Root/MEInfo/@CashOutGlassDate))"/>
                  </xsl:if>
                </xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputDate>
            </td>
          </tr>
        </table>
      </td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
          <colgroup>
            <col width="85px"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td colspan="2"><b>Mobile Electronics</b></td>
          </tr>
          <tr>
            <td colspan="2"><img style="height:2px;width:100%" src="images/background_top.gif"/></td>
          </tr>
          <tr>
            <td>Limits:</td>
            <td>
              <IE:APDInputCurrency id="txtCoverageLimitMEAmt" name="txtCoverageLimitMEAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="13" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@CoverageLimitMEAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Invoice (Gross):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceMEGrossAmt" name="txtInvoiceMEGrossAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="14" onChange="updateMENetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceMEGrossAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Ded. applied:</td>
            <td>
              <IE:APDInputCurrency id="txtDeductibleMEAmt" name="txtDeductibleMEAmt" value="" precision="9" scale="2" width="81" required="false" canDirty="true" CCTabIndex="15" onChange="updateMENetAmt()">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DeductibleMEAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Invoice (Net):</td>
            <td>
              <IE:APDInputCurrency id="txtInvoiceMENetAmt" name="txtInvoiceMENetAmt" value="" precision="9" scale="2" width="81" neg="true" required="false" canDirty="true" CCTabIndex="16" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@InvoiceMENetAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Disposition:</td>
            <td>
              <IE:APDCustomSelect id="selDispositionMECD" name="selDispositionMECD" value="" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="17" required="false" width="93" dropDownWidth="150" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@DispositionMECD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='DispositionMECD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Payment Chnl:</td>
            <td>
              <IE:APDCustomSelect id="selPaymentChannelMECD" name="selPaymentChannelMECD" value="" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="18" required="false" width="93" dropDownWidth="100" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@PaymentChannelMECD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='PaymentChannelMECD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>Cash Out Date:</td>
            <td>
              <IE:APDInputDate id="dteCashOutMEDate" name="dteCashOutMEDate" type="date" futureDate="false" required="false" canDirty="false" CCTabIndex="19" onChange="">
                <xsl:attribute name="value">
                  <xsl:if test="/Root/MEInfo/@CashOutMEDate != '1900-01-01T00:00:00'">
                    <xsl:value-of select="js:formatSQLDate(string(/Root/MEInfo/@CashOutMEDate))"/>
                  </xsl:if>
                </xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputDate>
            </td>
          </tr>
          <tr><td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td></tr>
        </table>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
          <colgroup>
            <col width="85px"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td colspan="2">
              <IE:APDCheckBox id="chkEndorsementFlag" name="chkEndorsementFlag" caption="Endorsement" value="0" width="" required="false" CCTabIndex="20" alignment="" canDirty="" onBeforeChange="" onChange="" onAfterChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@EndorsementFlag"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDCheckBox>
            </td>
          </tr>
          <tr>
            <td>Deductible:</td>
            <td>
              <IE:APDInputCurrency id="txtEndorsementDeductibleAmt" name="txtEndorsementDeductibleAmt" value="" precision="9" scale="2" required="false" canDirty="true" CCTabIndex="21" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@EndorsementDeductibleAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td>Limits:</td>
            <td>
              <IE:APDInputCurrency id="txtEndorsementLimitAmt" name="txtEndorsementLimitAmt" value="" precision="9" scale="2" required="false" canDirty="true" CCTabIndex="22" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@EndorsementLimitAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr><td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td></tr>
        </table>        


        <table width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
          <colgroup>
            <col width="85px"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td>Program Type:</td>
            <td>
              <IE:APDCustomSelect id="selProgramTypeCD" name="selProgramTypeCD" value="" displayCount="6" direction="" blankFirst="true" canDirty="true" CCTabIndex="23" required="false" width="85" dropDownWidth="110" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@ProgramTypeCD"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@ListName='ProgramTypeCD']">
                  <xsl:sort select="@Name"/>
                	<IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceId"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td>VIR Fee:</td>
            <td>
              <IE:APDInputCurrency id="txtVIRFeeAmt" name="txtVIRFeeAmt" value="" precision="9" scale="2" required="false" canDirty="true" CCTabIndex="24" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@VIRFeeAmt"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <IE:APDCheckBox id="chkCoverageExceptionMEFlag" name="chkCoverageExceptionMEFlag" caption="Any ME Not Covered" value="0" width="" required="false" CCTabIndex="25" alignment="" canDirty="" onBeforeChange="" onChange="" onAfterChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/MEInfo/@CoverageExceptionMEFlag"/></xsl:attribute>
                <xsl:if test="$dataDisabled = 'true'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDCheckBox>
            </td>
          </tr>
        </table>        
      </td>
    </tr>
    <tr style="height:6px;">
      <td colspan="2" style="border-bottom:1px solid #C0C0C0"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
    <tr align="right">
      <td></td>
      <td align="right" nowrap="true">
        <IE:APDButton id="btnSave" name="btnSave" value="Save" width="75" CCTabIndex="31" onButtonClick="onSave()">
          <xsl:if test="$dataDisabled = 'true'">
            <xsl:attribute name="CCDisabled">true</xsl:attribute>
          </xsl:if>
        </IE:APDButton>
        <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="75" CCTabIndex="32" onButtonClick="onCancel()"/>
      </td>
    </tr>
  </table>
  <xml id="xmlData" name="xmlData">
    <xsl:copy-of select="/Root"/>
  </xml>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
