<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="Territory Manager">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />


<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>
<xsl:param name="TerritoryCRUD" select="Territory"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>


<xsl:template match="/Root">
  
<HTML>

<!--            APP _PATH                            SessionKey                   USERID          SP                   XSL       PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTTerritoryGetDetailXML,SMTShopInfo.xsl-->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/pmd.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/smtvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/pmd.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->


<script language="javascript">
var gsUserID = '<xsl:value-of select="$UserId"/>';
var gsPageFile = "SMTShopInfo.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';
var gsTerritoryCRUD = '<xsl:value-of select="$TerritoryCRUD"/>';
var gbNoChange = false;

<![CDATA[

function csTerritory_onchange(sTerrID){
  var i, oXML, oOption;
  
  for (i=selTerritoryState.options.length-1; i>-1; i--)
    selTerritoryState.options.remove(i);
    
  oXML = xmlTerritory.selectNodes("/Root/State");
  
  for (i=0; i<oXML.length; i++){
    oOption = document.createElement("OPTION");
    
    if (oXML[i].getAttribute("TerritoryID") == sTerrID)
      selTerritoryState.options.add(oOption);
      
    oOption.innerText = oXML[i].getAttribute("StateValue");
    oOption.value = oXML[i].getAttribute("StateCode");    
  }
  
  oXml = xmlTerritory.selectSingleNode("/Root/Territory[@TerritoryID=" + sTerrID + "]");  

  gbNoChange = true;
  if (gsTerritoryCRUD.indexOf("U") == -1) csProgramManager.CCDisabled = false;
  if (oXml && oXml.getAttribute("ProgramManagerUserID") != '')
    csProgramManager.value = oXml.getAttribute("ProgramManagerUserID");
  else
    csProgramManager.selectedIndex = -1;   
  if (gsTerritoryCRUD.indexOf("U") == -1) csProgramManager.CCDisabled = true;
  
  gbNoChange = false;
}

function csProgramManager_onchange(sPMID){
  if (gbNoChange) return true;

  var oXML = xmlTerritory.selectSingleNode("/Root/Territory[@TerritoryID=" + csTerritory.value + "]");
     
  if (oXML){
    oXML.setAttribute("ProgramManagerUserID", sPMID);
    
    var newAttribute;
    newAttribute = xmlTerritory.createAttribute("Dirty");
    newAttribute.value = "yes";
    oXML.setAttributeNode(newAttribute);
  }
}

function Add(){
  var oOption, oXML;
  
  for (var i=selState.options.length-1; i>-1; i--){
    if (selState.options[i].selected==true){
      oOption = document.createElement("OPTION");
      if (selTerritoryState.options.length == 0)
        selTerritoryState.options.add(oOption);
      else{
        for (var x=selTerritoryState.options.length-1; x>=0; x--){
          if (selTerritoryState.options[x].innerText < selState.options[i].innerText){
            selTerritoryState.options.add(oOption, x+1);
            break;
          }
          if (x==0){
            selTerritoryState.options.add(oOption, x);
            break;
          }
        }    
      }  
      oOption.innerText = selState.options[i].innerText;
      oOption.value = selState.options[i].value;
      
      oXML = xmlTerritory.selectSingleNode("/Root/State[@StateCode='" + selState.options[i].value + "']");
      
      if (oXML) {
        oXML.setAttribute("TerritoryID", csTerritory.value);
                
        var newAttribute;
        newAttribute = xmlTerritory.createAttribute("Dirty");
        newAttribute.value = "yes";
        oXML.setAttributeNode(newAttribute);
      }
      
      selState.options.remove(i);
    }       
  }
}

function Remove(){
  var oXML, oOption;
  
  for (var i=selTerritoryState.options.length-1; i>=0; i--){
    if (selTerritoryState.options[i].selected == true){
      oXML = xmlTerritory.selectSingleNode("/Root/State[@StateCode='" + selTerritoryState.options[i].value + "']");
      if (oXML){
        oXML.setAttribute("TerritoryID", "");
       
        var newAttribute;
        newAttribute = xmlTerritory.createAttribute("Dirty");
        newAttribute.value = "yes";
        oXML.setAttributeNode(newAttribute);        
      }
          
      oOption = document.createElement("OPTION");
  
      if (selState.options.length == 0)
        selState.options.add(oOption);
      else{
        for (var x=selState.options.length-1; x>=0; x--){
          if (selState.options[x].innerText < selTerritoryState.options[i].innerText){
            selState.options.add(oOption, x+1);
            break;
          }
          if (x==0){
            selState.options.add(oOption, x);
            break;
          }
        }
      }    
      
      oOption.value = selTerritoryState.options[i].value;
      oOption.innerText = selTerritoryState.options[i].innerText;
      selTerritoryState.options.remove(i);
    }
  }
}



function Save(){
  if (selState.options.length > 0){ 
    ClientWarning("There are states unassigned to any territory.  Please assign all states to a territory before saving.");
    return;
  }

  btnSave.CCDisabled = true;
  bAddSuccess = false;
  
  var sProc = "uspSMTTerritoryUpdDetail";
  var sRequest = "";
  
  var oXML = xmlTerritory.documentElement.selectNodes("/Root/Territory[@Dirty='yes']");
  if (oXML){
    if (oXML.length > 0){
      sRequest += "Territories=";
      for (var i=0; i<oXML.length; i++)
        sRequest += oXML[i].getAttribute("TerritoryID") + "," + oXML[i].getAttribute("ProgramManagerUserID") + ";";
    }
  }
  
  var oXML = xmlTerritory.documentElement.selectNodes("/Root/State[@Dirty='yes']");
  if (oXML){
    if (oXML.length > 0){
      sRequest += sRequest == "" ? "States=" : "&States=";
      for (var i=0; i<oXML.length; i++)
        sRequest += oXML[i].getAttribute("StateCode") + "," + oXML[i].getAttribute("TerritoryID") + ";";
    }
  }
    
  if (sRequest == ""){
    btnSave.CCDisabled = false;
    return;
  }
    
  sRequest += "&UserID=" + gsUserID;
  
  //alert(sRequest);
  //return;
  
  var aRequests = new Array();
  aRequests.push( { procName : sProc,
                    method   : "ExecuteSpNpAsXML",
                    data     : sRequest }
                );
  var sXMLRequest = makeXMLSaveString(aRequests);
  var objRet = XMLSave(sXMLRequest);
  if (objRet && objRet.code == 0 && objRet.xml)
    bAddSuccess = true;
  
  var sInfo = bAddSuccess ? "Update Successful!" : "Update Failed.  Please try again.  If problem persists contact IT.";
  ClientInfo(sInfo);
  
  ClearDirty();
  btnSave.CCDisabled = false;
}
 
function CheckDirty(){
  var oXML
  
  oXML = xmlTerritory.documentElement.selectNodes("/Root/Territory[@Dirty='yes']");
  if (oXML){
    if (oXML.length > 0) return true;
  }
  
  oXML = xmlTerritory.documentElement.selectNodes("/Root/State[@Dirty='yes']");
  if (oXML){
    if (oXML.length > 0) return true;
  }

  return false; 
}


function ClearDirty(){
  var i;
  var oXML
  
  oXML = xmlTerritory.documentElement.selectNodes("/Root/Territory[@Dirty='yes']");
  
  
  if (oXML){
  
    for (i=0; i<oXML.length; i++)
      oXML[i].setAttribute("Dirty", "no");
  }
  
  oXML = xmlTerritory.documentElement.selectNodes("/Root/State[@Dirty='yes']");
  if (oXML){
    for (i=0; i<oXML.length; i++)
      oXML[i].setAttribute("Dirty", "no");
  } 
  
}
 
function chkBeforeUnloadWindow(){
    if (CheckDirty()){
      event.returnValue = "There are unsaved changes to the information on this page.";
    }
}


]]>


</script>

</HEAD>

<body class="bodyAPDsub" onbeforeunload="chkBeforeUnloadWindow()" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<span style="position:absolute; top:3; left:154; font-size:16pt;">Territory Manager</span>
<div style="position:absolute; left:35; top:30; width:425; height:325;">
    
  
  <span style="position:absolute; top:5; left:2; font-weight:bold;">Territory</span>
  <IE:APDCustomSelect id="csTerritory" width="150" selectedIndex="0" canDirty="false" onchange="csTerritory_onchange(this.value)" style="position:absolute; top:25; left:2;">
    <xsl:for-each select="Territory">
      <IE:dropDownItem style="display:none">
        <xsl:attribute name="value"><xsl:value-of select="@TerritoryID"/></xsl:attribute>
        <xsl:value-of select="@Name"/>
      </IE:dropDownItem>
    </xsl:for-each>
  </IE:APDCustomSelect>
  
  <span style="position:absolute; top:5; left:240; font-weight:bold;">Program Manager</span>
  <IE:APDCustomSelect id="csProgramManager" width="150" canDirty="false" onchange="csProgramManager_onchange(this.value, true)" style="position:absolute; top:25; left:240;">
    <xsl:attribute name="value"><xsl:value-of select="/Root/Territory[@TerritoryID='1']/@ProgramManagerUserID"/></xsl:attribute>
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
    <IE:dropDownItem value="0" style="display:none">*Unassigned*</IE:dropDownItem>
    <xsl:for-each select="ProgramManager">
      <IE:dropDownItem style="display:none">
        <xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>
        <xsl:value-of select="@Name"/>
      </IE:dropDownItem>
    </xsl:for-each>
  </IE:APDCustomSelect>
  
  <span style="position:absolute; top:55; left:2; font-weight:bold;">States</span>
  <select id="selTerritoryState" size="15" multiple="true" style="position:absolute; top:75; left:2; width:175;">
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
    <xsl:for-each select="State[@TerritoryID='1']">
      <option>
        <xsl:attribute name="value"><xsl:value-of select="@StateCode"/></xsl:attribute>
        <xsl:value-of select="@StateValue"/>
      </option>
    </xsl:for-each>  
  </select>
  
  <IE:APDButton value="&lt;&lt;&lt;" id="Add" onButtonClick="Add()" style="position:absolute; left:189; top:150;">
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
  </IE:APDButton>
  
  <IE:APDButton value="&gt;&gt;" id="Remove" onButtonClick="Remove()" style="position:absolute; left:189; top:170;">
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
  </IE:APDButton>
  
  <span style="position:absolute; top:55; left:240; font-weight:bold;">Unassigned States</span>
  <select id="selState" size="15" multiple="true" style="position:absolute; top:75; left:240; width:175;">
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
    <xsl:for-each select="State[@TerritoryID = '']">
      <option>
        <xsl:attribute name="value"><xsl:value-of select="@StateCode"/></xsl:attribute>
        <xsl:value-of select="@StateValue"/>
      </option>
    </xsl:for-each>  
  </select>
  
  <IE:APDButton id="btnCancel" value="Cancel" width="80" onclick="window.close()" style="position:absolute; top:302; left:128;"></IE:APDButton>
  <IE:APDButton id="btnSave" value="Save" width="80" onButtonClick="Save()" style="position:absolute; top:302; left:212;">
    <xsl:if test="contains($TerritoryCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
  </IE:APDButton>
</div>

<xml id="xmlTerritory" ondatasetcomplete="xmlTerritory.setProperty('SelectionLanguage', 'XPath');">
  <xsl:copy-of select="/Root"/>
</xml>

</body>
</HTML>
</xsl:template>


</xsl:stylesheet>
