<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimPropertyList">

<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<ms:script language="JScript" implements-prefix="js">
<![CDATA[

  function MainTabs(ctx, pos, PropNum, ClaimAspectID)
  {
    var strPropertyName = ctx[0].selectSingleNode("@Name").nodeValue;
    var strLocationAddress1 = ctx[0].selectSingleNode("@LocationAddress1").nodeValue;
    var strClosed = ctx[0].selectSingleNode("@ClosedStatus").nodeValue;

    var strRet = "<SPAN unselectable='on' id='tab1"+pos+"' class='tab1' onClick='TabChange("+pos+","+PropNum+","+ClaimAspectID+")' propClosed='" + strClosed + "'>";

    if (strClosed == "1")
        strRet += "<img src='/images/closedExposure.gif' title='Vehicle Closed'/><img src='/images/spacer.gif' style='height:1px; width:5px'/>";

    if (strPropertyName.length > 0)
        strRet += PropNum + ": ";
    else
        strRet += "Prop" + PropNum + ": ";
		var strValue = "";
        if (strPropertyName.length > 0 && strLocationAddress1.length > 0)
            strValue = strPropertyName + " - " + strLocationAddress1;
        else
            strValue = strPropertyName + strLocationAddress1;
		if (strValue.length > 17)
			strValue = strValue.substr(0,15) + ".."

    strRet += strValue +"</SPAN>";
    return strRet;
  }

  function ContentTabs(ctx, pos, PropNum)
  {
    var lynxid = ctx[0].nodeValue;
    var sVisible = "hidden";

    var strRet = "<DIV unselectable='on' class='content1' id='content1"+pos+"' style='background-color:#FFFAEB; width:742px; height:492px;'>";
    strRet += "<IFRAME src='blank.asp' name='oIframe"+pos+"' id='oIframe"+pos+"' onreadystatechange='top.IFStateChange(this);' ";
    var sVisible = "hidden";
    strRet += "style='border : 0px; visibility:"+sVisible+" ;position:absolute; width:730px; height:480px; background : transparent;overflow:hidden' allowtransparency='true'></IFRAME></DIV>";
    return strRet;
  }

]]>
</ms:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="PropertyCRUD" select="Property"/>
<xsl:param name="DocumentCRUD" select="Document"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="PropContext"/>
<xsl:param name="ClaimAspectID"/>

<xsl:template match="/">

<xsl:variable name="LynxID" select="/Root/@LynxID" />

<xsl:variable name="InsuranceCompanyName" select="session:XslGetSession('InsuranceCompanyName')"/>
<xsl:variable name="ClientClaimNumber" select="session:XslGetSession('ClientClaimNumber')"/>
<xsl:variable name="InsuredNameFirst" select="session:XslGetSession('InsuredNameFirst')"/>
<xsl:variable name="InsuredNameLast" select="session:XslGetSession('InsuredNameLast')"/>
<xsl:variable name="LossDate" select="session:XslGetSession('LossDate')"/>
<xsl:variable name="CoverageLimitWarningInd" select="session:XslGetSession('CoverageLimitWarningInd')"/>

<xsl:variable name="ClaimRestrictedFlag" select="session:XslGetSession('RestrictedFlag')"/>
<xsl:variable name="ClaimOpen" select="session:XslGetSession('ClaimOpen')"/>
<xsl:variable name="CSRNameFirst" select="session:XslGetSession('OwnerUserNameFirst')"/>
<xsl:variable name="CSRNameLast" select="session:XslGetSession('OwnerUserNameLast')"/>
<xsl:variable name="InsuredBusinessName" select="session:XslGetSession('InsuredBusinessName')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="DemoFlag" select="session:XslGetSession('DemoFlag')"/>
<xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>

<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimPropertyGetListXML,ClaimPropertyList.xsl,6366, 145   -->

<HEAD>
<TITLE>Property Info</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<style>
    .toolbarButton {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border:0px;
    }
    .toolbarButtonOver {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border-top:1px solid #D3D3D3;
      border-left:1px solid #D3D3D3;
      border-bottom:1px solid #696969;
      border-right:1px solid #696969;
    }

    .toolbarButtonOut {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border:0px;
    }
</style>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,24);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="JavaScript">

var gsWindowID = "<xsl:value-of select="$WindowID"/>";
var gsLynxID = "<xsl:value-of select="$LynxID"/>";
var gsInsuranceCompanyID = '<xsl:value-of select="$InsuranceCompanyID"/>';
var gsPropCount = "<xsl:value-of select="count(/Root/Property)"/>";
var gsPropContext = "<xsl:value-of select="$PropContext"/>";
var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
var gsClaimID = "<xsl:value-of select="$ClientClaimNumber"/>";
var gsIFname = "<xsl:value-of select="js:cleanString(string($InsuredNameFirst))"/>";
var gsILname = "<xsl:value-of select="js:cleanString(string($InsuredNameLast))"/>";
var gsLossDate = "<xsl:value-of select="js:UTCConvertDate(string($LossDate))"/>";
var gsInsco = "<xsl:value-of select="js:cleanString(string($InsuranceCompanyName))"/>";
var gsClaimRestrictedFlag = "<xsl:value-of select="$ClaimRestrictedFlag"/>";
var gsClaimOpen = "<xsl:value-of select="$ClaimOpen"/>";
var gsCSRNameFirst = '<xsl:value-of select="js:cleanString(string($CSRNameFirst))"/>';
var gsCSRNameLast = '<xsl:value-of select="js:cleanString(string($CSRNameLast))"/>';
var gsIBusiness = "<xsl:value-of select="js:cleanString(string($InsuredBusinessName))"/>";
var bAddProperty = false;
var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
var bPageReadOnly = <xsl:value-of select="$pageReadOnly"/>;
var gsDemoFlag = "<xsl:value-of select="$DemoFlag"/>";
var gsDocumentCRUD = "<xsl:value-of select="$DocumentCRUD"/>";

var giProp1stPartyCount = "<xsl:value-of select="count(/Root/Property[@ExposureCD='1'])"/>";
var giProp3rdPartyCount = "<xsl:value-of select="count(/Root/Property[@ExposureCD='3'])"/>";
var giPropNAPartyCount = "<xsl:value-of select="count(/Root/Property[@ExposureCD='N'])"/>";
var gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

if (parent.gsCoverageLimitWarningInd)
  parent.gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

<![CDATA[

var gsPropNumIndex = 1;
var gsPropNum = 0;
var gbPropClosed = false;

function TabChange(index, PropNum, ClaimAspectID)
{
  gsPropNumIndex = index;
  gsPropNum = PropNum;

  //scroll the tab list automatically
  var curTab = document.all["tab1" + index];

  tabs1.scrollLeft = 0;
  if (curTab && tabScroll.style.display == "inline")
    if (((curTab.offsetLeft + curTab.offsetWidth) >= tabs1.offsetWidth))
        curTab.scrollIntoView(true);

  if (curTab && curTab.getAttribute("propClosed") == "1")
    gbPropClosed = true;
  else
    gbPropClosed = false;


  document.frames[index-1].frameElement.style.visibility="visible";
  for (var x=1; x <= gsPropCount; x++)
  {
    if (x != index)
    {
      document.frames[x-1].frameElement.style.visibility="hidden";
      document.frames[x-1].frameElement.src = "blank.asp";
    }
    else
    {
      document.frames[index-1].frameElement.src = "ClaimProperty.asp?WindowID=" + gsWindowID + "&LynxID="+gsLynxID+"&ClaimAspectID="+ClaimAspectID;
      top.setSB(0,top.sb);
    }

  }
}

function PropertyDelete()
{
  document.frames[gsPropNumIndex-1].ADS_Pressed("Delete", "DescriptionInfo");
}

//Notification from APDSelect.asp that the main tabs have changed
function ParentMainTabChange()
{
	document.frames[gsPropNumIndex-1].MainTabChange();
}



function PageInit()
{
  top.setClaimHeader(gsCSRNameFirst, gsCSRNameLast, gsLynxID, gsClaimID, gsIFname, gsILname, gsLossDate, gsInsco, gsClaimRestrictedFlag, gsClaimOpen, gsClaimStatus, gsIBusiness, gsDemoFlag, gsInsuranceCompanyID);
  top.SetMainTab(3)
  if (gsClaimStatus == "Claim Closed" ||
      gsClaimStatus == "Claim Cancelled" ||
      gsClaimStatus == "Claim Voided"){
    top.PropertiesMenu(false,2);
    top.PropertiesMenu(false,3);
  } else {
    top.PropertiesMenu(true,2);
    top.PropertiesMenu(true,3);
  }

  if (gsDocumentCRUD.indexOf("C") == -1)
    top.DocumentsMenu(false, 1);
  
  if (bPageReadOnly == true) {
    var str = (gsClaimStatus == "Claim Voided" ? "voided" : "cancelled");
    if (bAddProperty == true) {
      ClientWarning("This claim has been " + str + " and does not have any property listed." );
      btnAddProperty.disabled = true;
      return;
    }
  }
  if (bAddProperty == true) return;

  top.gaPropClaimantCount = new Array();

  var objTabLast = tabs1.lastChild;
  if ((objTabLast.offsetLeft + objTabLast.offsetWidth) > tabs1.offsetWidth)
    tabScroll.style.display = "inline";

  var index = 1;
  var oTbl = document.getElementById("tblPropCross");
  if (oTbl){
      if (gsClaimAspectID != "")
      {
        var x;
        var bFound = false;
        for(x=0; x < gsPropCount; x++)
        {
          if (oTbl.rows[x].cells[1].innerText == gsPropContext) {
            bFound = true;
            break;
          }
        }
        if (bFound){
            index = oTbl.rows[x].cells[0].innerText;
        }
        TabChange(index, gsPropContext, gsClaimAspectID);

      }
      else
        TabChange(index, oTbl.rows[0].cells[1].innerText, oTbl.rows[0].cells[2].innerText);

      var objTab = document.getElementById("tab1"+index);
      objTab.depressTab();
      top.setSB(100,top.sb);
  }
  var objTab = document.getElementById("tab1"+index);
  objTab.depressTab();

}

function PageUnload()
{
  top.PropertiesMenu(false,2);
  top.PropertiesMenu(false,3);
}

function AddProperty()
{
  window.navigate( 'claimPropertyInsert.asp?WindowID=' + gsWindowID );
}

function updateTabContent(){
    var objCurTab = document.all["tab1" + gsPropNumIndex];
    if (objCurTab) {

        var frmProp = document.frames[gsPropNumIndex - 1];
        if (frmProp){
            var strPropertyName = frmProp.document.all["txtPropName"].value;
            var strLocAddr1 = frmProp.document.all["txtPropAddress1"].value;
    		var strValue = "";
            if (strPropertyName.length > 0 && strLocAddr1.length > 0)
                strValue = strPropertyName + " - " + strLocAddr1;
            else
                strValue = strPropertyName + strLocAddr1;

    		if (strValue.length > 17)
    			strValue = strValue.substr(0,15) + ".."

            if (strValue.length > 0)
                objCurTab.innerText = gsPropNum + ": " + strValue;
            else {
                objCurTab.innerText = "Prop" + gsPropNum + ":";
            }
            return;
        }
        /*strFirstName = strFirstName.substr(0,1);
		if (strLastName.length > 15)
			strLastName = strLastName.substr(0,15) + "..";

        if (strBusiness != "")
            if (strBusiness.length > 15)
                strBusiness = strBusiness.substr(0, 15) + "..";
        if (strBusiness != "")
            objCurTab.innerText = gsVehNumIndex + ": " + strBusiness;
        else if (strFirstName != "" && strLastName != "")
            objCurTab.innerText = gsVehNumIndex + ": " + strFirstName + ". " + strLastName;
        else if (strFirstName != "" || strLastName != "")
            objCurTab.innerText = gsVehNumIndex + ": " + strFirstName + strLastName;
        else
            objCurTab.innerText = "Veh " + gsVehNumIndex;*/
    }
}

if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="tabInit(false);PageInit();" onUnload="PageUnload();" tabIndex="-1">
<xsl:choose>
    <xsl:when test="(count(/Root/Property) = 1) and (/Root/Property/@PropertyNumber='0')">
        <xsl:choose>
            <!-- <xsl:when test="not(contains($PropertyCRUD, 'R'))">
                <div style="width:100%;text-align:center">
                <br/>
                <font color="#FF0000">You do not have permissions to view this tab.</font>
                <br/>
                Please contact your supervisor.
                </div>
                <script language="javascript">
                    setTimeout("top.PropertiesMenu(false, 1)", 1); //disable the property list menu.
                    setTimeout("top.PropertiesMenu(false, 2)", 1); //disable the add property menu.
                    setTimeout("top.PropertiesMenu(false, 3)", 1); //disable the remove property menu.
                </script>
            </xsl:when> -->
            <xsl:when test="not(contains($PropertyCRUD, 'C'))">
                <div style="width:100%;text-align:center">
                <br/>
                <font color="#FF0000">You do not have permissions to create a new Property.</font>
                <br/>
                Please contact your supervisor.
                </div>
                <script language="javascript">
                    setTimeout("top.PropertiesMenu(false, 2)", 1); //disable the add property menu.
                    setTimeout("top.PropertiesMenu(false, 3)", 1); //disable the remove property menu.
                </script>
            </xsl:when>
            <xsl:when test="contains($PropertyCRUD, 'C')">
                <!-- this makes sure that user does not update property 0, instead allows him to add one.-->
                <input id="btnAddProperty" name="btnAddProperty" type="button" value="Add Property" onclick="AddProperty()" class="formbutton"/>
                <script language="javascript">
                  bAddProperty = true;
                </script>
            </xsl:when>
         </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
        <xsl:if test="not(contains($PropertyCRUD, 'C'))">
            <script language="javascript">
                setTimeout("top.PropertiesMenu(false, 2)", 1); //disable the add property menu.
            </script>
        </xsl:if>
        <xsl:if test="not(contains($PropertyCRUD, 'D'))">
            <script language="javascript">
                setTimeout("top.PropertiesMenu(false, 3)", 1); //disable the remove property menu.
            </script>
        </xsl:if>
        <!-- Start Tabs -->
          <DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">
            <xsl:if test="boolean(/Root/Property)">
              <div id="tabScroll" style="position:absolute;top:3px; left:710px;width:35px;z-index:200;display:none">
                <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButtonOut'" onmousedown="tabs1.doScroll('pageleft');this.fireEvent('onblur');"><span style="font:10pt Marlett">3</span></button>
                <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButtonOut'" onmousedown="tabs1.doScroll('pageright');this.fireEvent('onblur');"><span style="font:10pt Marlett">4</span></button>
              </div>
              <DIV unselectable="on" id="tabs1" class="apdtabs" style="width:703px;overflow:hidden;top:-1px;">
                <SPAN id="tab10" unselectable="on" style="width:0px;height:0px;"></SPAN>
                <xsl:for-each select="/Root/Property"  >
                  <xsl:sort data-type="number" select="@PropertyNumber" order="ascending" />
                  <xsl:value-of disable-output-escaping="yes" select="js:MainTabs(., position(), string(@PropertyNumber), string(@ClaimAspectID))"/>
                </xsl:for-each>
              </DIV>  <!-- Start Property Info -->

              <xsl:for-each select="/Root/Property"  >
                <xsl:sort data-type="number" select="@PropertyNumber" order="ascending" />
                <xsl:value-of disable-output-escaping="yes" select="js:ContentTabs(/Root/@LynxID, position(), string(@PropertyNumber))"/>
              </xsl:for-each>

            </xsl:if>
          </DIV>
        <!-- End Tabs -->

        <TABLE id="tblPropCross" style="display:none">
          <xsl:for-each select="/Root/Property"  >
          <xsl:sort data-type="number" select="@PropertyNumber" order="ascending" />
          <TR>
            <TD style="display:none"><xsl:value-of select="position()"/></TD>
            <TD style="display:none"><xsl:value-of select="@PropertyNumber"/></TD>
            <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
          </TR>
          </xsl:for-each>
        </TABLE>
    </xsl:otherwise>
</xsl:choose>

</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>
