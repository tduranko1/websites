<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTBusinessSearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="UserID"/>
<xsl:param name="PageID"/>
<xsl:param name="ShopsMoved"/>
<xsl:param name="CallBack"/>
<xsl:param name="ShopCRUD" select="Shop"/>


<xsl:template match="/Root">
  
  <xsl:variable name="EntityID" select="@ExclusionEntityID"/>
  
  <xsl:if test="$ShopsMoved='1'"><script>parent.parent.window.navigate("SMTDetailLevel.asp?SearchType=B&amp;BusinessInfoID=" + top.gsBusinessInfoID)</script></xsl:if>
  <xsl:if test="$CallBack!=''"><script>parent.window.navigate(unescape('<xsl:value-of select="$CallBack"/>'))</script></xsl:if>
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTEntitySearchGetListXML,SMTBusinessSearchResults.xsl, 'S', null, null, null, null, null, null, 239, null, null, 1, null, 19, 'D'   -->

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <style>
  	A{
		color:blue
	}
	A:hover{
		color : #B56D00;
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-weight : bold;
		font-size : 10px;
		cursor : hand;
	}	
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsSearchType = '<xsl:value-of select="@SearchType"/>';
var gsCount = '<xsl:value-of select="count(Entity)"/>';
var gsEntityID = '<xsl:value-of select="$EntityID"/>';
var gsPageID = '<xsl:value-of select="$PageID"/>';

<![CDATA[

function initPage(){
  parent.ShowCount(gsCount);
	if (gsCount > 0) parent.document.frames["ifrmShopList"].frameElement.style.visibility = "visible";
	parent.btnSearch.disabled = false;
	parent.window.clearInterval(parent.gsSearchingIntervalID);	
  if (gsPageID == "BusinessIncludeShop") frmBusinessList.txtBusinessInfoID.value = top.gsBusinessInfoID;
  
  var aInputs = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++){    
    if (aInputs[i].type=="checkbox")
	  aInputs[i].attachEvent("onclick", Setdirty);    
  }
}

function GridMouseOver(oObject){
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
}

function GridMouseOut(oObject){
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
}  

var giCheckCount = 0;
function CountChecks(obj){
  if (obj.checked == true) 
    giCheckCount++;
  else 
    giCheckCount--;
    
  parent.parent.btnSave.disabled = giCheckCount > 0 ? false : true;
}

function Setdirty(){
	parent.parent.gbDirtyFlag = true;
}

function Save(){
	parent.parent.btnSave.disabled=true;
  
  if (gsPageID == "DealerAddBusiness"){
    frmBusinessList.action += "?mode=save&PageID=DealerAddBusiness&SearchType=B&EntityID=" + gsEntityID;
    if (parent.txtCallBack){
      frmBusinessList.action += "&CallBack=" + escape(parent.txtCallBack.value);
    }
  }
  else{
    var sMsg = "Clicking 'Yes' will cause the window below to close upon successful completion of the shop move! \n\n";    
    sMsg += "This action will move the selected Shops to the current Business (displayed on the 'Info' tab at the bottom of the screen).";
    sMsg += "  Any Business left with no Shops after the move will be removed from the system."
    sMsg += "\n \n Are you sure you wish to move these Shops?";
    var response = parent.parent.YesNoMessage("Move Shops", sMsg, 205);
    if (response != "Yes"){
      parent.parent.btnSave.disabled=false;
      return false;
    }
    frmBusinessList.action += "?mode=save&PageID=BusinessIncludeShop&SearchType=S";
    if (parent.txtCallBack){
      if (parent.txtCallBack.value != "")
        frmBusinessList.action += "&CallBack=" + escape("SMTDetailLevel.asp?SearchType=B&ShopID=&BusinessInfoID=" & top.BusinessInfoID);
    }
  }
    
          
	frmBusinessList.submit();
  if (gsPageID != "DealerAddBusiness") top.window.close();
  try{
    parent.parent.gbDirtyFlag = false;
  }
  catch(e){}
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();">
<form id="frmBusinessList" method="post" style="overflow:hidden">
  <DIV id="CNScrollTable">
    <TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
      <TBODY>
        <TR unselectable="on" class="QueueHeader">
    	    <TD unselectable="on" class="TableSortHeader" sIndex="99" width="42" nowrap="nowrap" style="cursor:default; height:20;"> Add </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="52" nowrap="nowrap" style="cursor:default;"> ID </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="218" nowrap="nowrap" style="cursor:default;"> Name </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="213" nowrap="nowrap" style="cursor:default;"> Address </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="136" nowrap="nowrap" style="cursor:default;"> City </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> Zip </TD>
        </TR>
      </TBODY>
    </TABLE>
    
    <DIV unselectable="on" id="divResultList" class="autoflowTable">
      <xsl:choose>
        <xsl:when test="$PageID='DealerAddBusiness'"><xsl:attribute name="style">height:330px;</xsl:attribute></xsl:when>
        <xsl:when test="$PageID='BusinessIncludeShop'"><xsl:attribute name="style">height:330px;</xsl:attribute></xsl:when>
      </xsl:choose>
      
      <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="Entity">
            <xsl:call-template name="SearchResults">
              <xsl:with-param name="SearchType" select="/Root/@SearchType"/>
              <xsl:with-param name="Page" select="$PageID"/>
            </xsl:call-template>
          </xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
  </DIV>

  <input type="hidden" name="SysLastUserID"><xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute></input>
  <xsl:choose>
    <xsl:when test="$PageID='DealerAddBusiness'">
      <input type="hidden" name="DealerID"><xsl:attribute name="value"><xsl:value-of select="$EntityID"/></xsl:attribute></input>
    </xsl:when>
    <xsl:when test="$PageID='BusinessIncludeShop'">
      <input type="hidden" name="BusinessInfoID" id="txtBusinessInfoID"/>
    </xsl:when>
  </xsl:choose>
  <input type="hidden" name="SearchType" value="B"/>
</form>

</BODY>
</HTML>
</xsl:template>

  <!-- Gets the search results -->
  <xsl:template name="SearchResults">
    <xsl:param name="SearchType"/>
    <xsl:param name="Page"/>
    <!--<script>alert("<xsl:value-of select='$SearchType'/>")</script>-->
    <TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <td unselectable="on" class="GridTypeTD" width="40">
	      <input type="checkbox" name="chkAdd" onclick="CountChecks(this)">
          <xsl:attribute name="value"><xsl:value-of select="position()"/></xsl:attribute>
          <xsl:if test="contains($ShopCRUD, 'U') = false"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
        </input>
      </td>
	    <TD unselectable="on" class="GridTypeTD" width="50" style="text-align:left">
        <xsl:choose>
			    <xsl:when test = "@EntityID != ''"><xsl:value-of select="@EntityID"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
		    <input type="hidden" name="EntityID"><xsl:attribute name="value"><xsl:value-of select="@EntityID"/></xsl:attribute></input>			
        <input type="hidden" name="SysLastUpdatedDate"><xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute></input>			
      </TD>
      <TD unselectable="on" width="217" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
		      <xsl:when test = "@Name != ''">
			      <a>
		          <xsl:attribute name="href">
			          <xsl:choose>
                  <xsl:when test="$PageID='DealerAddBusiness'">javascript:NavToShop('B', <xsl:value-of select='@EntityID'/>, 0)</xsl:when>
                  <xsl:when test="$PageID='BusinessIncludeShop'">
                    javascript:NavToShop('S', <xsl:value-of select='@EntityID'/>, <xsl:value-of select='@ShopBusinessID'/>)
                  </xsl:when>
                </xsl:choose>
			        </xsl:attribute>
		          <xsl:value-of select="@Name"/>
		        </a>
		      </xsl:when>
		      <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="213" style="text-align:left">
	      <xsl:choose>
			    <xsl:when test = "@Address1 != ''"><xsl:value-of select="@Address1"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
      <TD unselectable="on" width="134" class="GridTypeTD" style="text-align:left">
	      <xsl:value-of select="@AddressCity"/>
		    <xsl:if test="@AddressCity != '' and @AddressState != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
		    <xsl:value-of select="@AddressState"/>
		    <xsl:if test="normalize-space(@AddressCity) = '' and normalize-space(@AddressState) = ''"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
	    </TD>
      <TD unselectable="on" class="GridTypeTD" width="39" style="text-align:left">
	      <xsl:choose>
			    <xsl:when test = "@AddressZip != ''"><xsl:value-of select="@AddressZip"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
    </TR>
  </xsl:template>

</xsl:stylesheet>