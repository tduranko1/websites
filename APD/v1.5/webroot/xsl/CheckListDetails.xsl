<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <msxsl:script language="JScript" implements-prefix="local">

      function ClaimAspectToPertainsTo( strTypeName, strNumber )
      {
          return ( strNumber == "0" ) ? strTypeName : strTypeName + " " + strNumber;
      }

      function GetDefaultContext( strContext )
      {
          return ( strContext == "" ) ? "clm" : strContext;
      }

  </msxsl:script>

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="TaskCRUD" select="Task"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="RawContext"/>
  <xsl:param name="LynxId"/>
  <xsl:param name="UserId"/>
  <xsl:param name="OwnerUserId"/>
  <xsl:param name="WindowID"/>

  <!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspDiaryGetDetailXML,CheckListDetails.xsl, 2024, 6790, 33   -->

  <xsl:template match="/">

    <xsl:variable name="PertainsTo">
      <xsl:value-of select="/Root/DiaryTask/@ClaimAspectCode"/>
      <xsl:if test="/Root/DiaryTask/@ClaimAspectNumber != 0">
        <xsl:value-of select="/Root/DiaryTask/@ClaimAspectNumber"/>
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="ClaimAspectServiceChannelID">
      <xsl:value-of select="/Root/DiaryTask/@ClaimAspectServiceChannelID"/>
    </xsl:variable>

    <html>
      <head>

        <title>Task Details</title>
        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css" />
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
    		<script type="text/javascript">
          document.onhelp=function(){
    		    if (strCheckListID == "0" ) {  //insert new task
    		      RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,11);
    		      event.returnValue=false;
    		    } else {           // edit existing task
    		      RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,14);
    		      event.returnValue=false;
    		    }
    		  };
    		</script>
        <script type="text/javascript" language="JavaScript1.2"  src="/js/apdcontrols.js"></script>
        <script type="text/javascript" language="JavaScript1.2"  src="/js/datepicker2.js"></script>
        <script language="JavaScript">

            window.name = "thisWin";

			var strAlarmDate = '<xsl:value-of select="/Root/DiaryTask/@AlarmDate" />';
            var orgAlarmDate = new Date( formatSQLDateTime( strAlarmDate ) );
            var strLynxID = '<xsl:value-of select="$LynxId"/>';
			var strCheckListID = '<xsl:value-of select="/Root/DiaryTask/@CheckListID"/>';
            var strChannelID = '<xsl:value-of select="/Root/DiaryTask/@ClaimAspectServiceChannelID"/>';
            var strAssignedUserID = '<xsl:value-of select="/Root/DiaryTask/@AssignedUserID"/>';
            var strContext = '<xsl:value-of select="local:GetDefaultContext(string($RawContext))"/>';
            var strCRUD = '<xsl:value-of select="$TaskCRUD"/>';
            var gsPertainsTo = '<xsl:value-of select="$PertainsTo"/>';
            var gsUserID = '<xsl:value-of select="$UserId"/>';
            var gsSysLastUpdateDate = "<xsl:value-of select="/Root/DiaryTask/@SysLastUpdatedDate"/>";
            var gsClaimAspectID = "";
            var gsClaimAspectServiceChannelID = "";
            var gsUserHasSupervisor = "<xsl:value-of select="/Root/@hasSupervisor"/>";

            <![CDATA[
			
			//--------------------------------------//
			//-- 18Dec2012 -- TVD Please Wait Fix --//
			//--------------------------------------//
			var pageLoaded;
			function doAgain() {
				try {
					pageLoaded = document.getElementById('btnCancel');
				    __pageInit();
				} 
				catch(err) {
					pageLoading.style.display = 'block';
					document.getElementById('btnCancel').disabled=true;
					document.getElementById('btnSave').disabled=true;
					document.getElementById('selPertainsTo').disabled=true;
					document.getElementById('selChannel').disabled=true;
					document.getElementById('selTaskID').disabled=true;
					try {
						document.getElementById('selAssignTo').disabled=true;
					}
					catch(err) {
					}
					window.setTimeout("doAgain()", 3000); 
				}
			}
			//--  18Dec2012 -- TVD END --//

            //function initPage()
            function __pageInit()
            {
				//--------------------------------------//
				//-- 18Dec2012 -- TVD Please Wait Fix --//
				//--------------------------------------//
				var pageLoading = document.getElementById('pageLoading');
				pageLoading.style.display = 'none';
				document.getElementById('btnCancel').disabled=false;
				document.getElementById('btnSave').disabled=false;
				document.getElementById('selPertainsTo').disabled=false;
				document.getElementById('selChannel').disabled=false;
				document.getElementById('selTaskID').disabled=false;
				try {
					document.getElementById('selAssignTo').disabled=false;
				}
				catch(err) {
				}
				//--  18Dec2012 -- TVD END --//

 			/*if (__blnPageInitialized == false){
                  window.setTimeout("initPage()", 250);
                  return;
              }*/
              
              var oTask = xmlData.selectSingleNode("/Root/DiaryTask");
              if (oTask){
                refreshChannelList();
                refreshTaskList();
                selTaskID.SelectItem(null, oTask.getAttribute("TaskName"));
              }

              if (parseInt(strCheckListID, 10) > 0){
                selPertainsTo.CCDisabled = selTaskID.CCDisabled = true;
              } else
                selPertainsTo.setFocus();

              setCRUD();

              selPertainsTo.setFocus();
            }

            function setCRUD(){
              if (strCRUD.indexOf("U") == -1){ //no update permission
                selPertainsTo.CCDisabled = selTaskID.CCDisabled = txtAlarmDate.CCDisabled = txtDescription.CCDisabled = btnSave.CCDisabled = true;
              }

              if (strCRUD.indexOf("R") == -1){ //no read permission
                doCancel();
              }
            }

            function taskSelected(){
              //do this only for new tasks
              if (parseInt(strCheckListID, 10) > 0) return;

              var oTask = xmlData.selectSingleNode("/Root/Reference[@List='TaskList' and @ReferenceID='" + selTaskID.value + "' and @ClaimAspectID='" + gsClaimAspectID + "']");
              if (oTask){
                var sAlarmDate = oTask.getAttribute("EscalationDate");

                if (sAlarmDate == ""){
                  var dt = new Date();
                  var iMonth = dt.getMonth() + 1; //JS dates start with month 0
                  sAlarmDate = ( iMonth < 10 ? "0" + iMonth : iMonth) + "/" + (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate()) + "/" + dt.getFullYear() + " 23:59:59";
                  txtAlarmDate.value = sAlarmDate;
                } else {
                  txtAlarmDate.value = formatSQLDateTime(sAlarmDate);
                }
                
                //gsClaimAspectID = oTask.getAttribute("ClaimAspectID");

                //alert(oTask.getAttribute("DefaultAssignmentPoolFunctionCD"));
                var sDefaultPool = oTask.getAttribute("DefaultAssignmentPoolFunctionCD");

                if (xmlData.selectSingleNode("/Root/Reference[@List='UserPool' and @Name='" + sDefaultPool + "']")){
                  selAssignTo.value = "SELF";
                } else {
                  switch(sDefaultPool){
                    case "OWN":
                      selAssignTo.value = sDefaultPool;
                      break;
                    case "ALST":
                      selAssignTo.value = sDefaultPool;
                      break;
                    case "SPRT":
                      selAssignTo.value = "SPRT";
                      break;
                    default:
                      selAssignTo.selectedIndex = -1;
                      break;
                  }
                }
              }
            }

            function refreshChannelList()
            {
              var sPertainsTo = selPertainsTo.value;
              var oPertainsToNode = xmlData.selectSingleNode("/Root/Reference[@List='PertainsTo' and @PertainsTo='" + sPertainsTo + "']");
              if (oPertainsToNode){
                gsClaimAspectID = oPertainsToNode.getAttribute("ReferenceID");
                var bDisabled = selChannel.CCDisabled;
                selChannel.CCDisabled = false;
                selChannel.Clear();
                selTaskID.Clear();
                var oChannels = xmlData.selectNodes("/Root/Reference[@List='ServiceChannel' and @ClaimAspectID='" + gsClaimAspectID + "']");
                for (var i = 0; i < oChannels.length; i++){
                  selChannel.AddItem(oChannels[i].getAttribute("ReferenceID"), oChannels[i].getAttribute("Name"), "", i);
                }
                if (parseInt(strChannelID, 10) > 0) {
                  selChannel.value = parseInt(strChannelID, 10);
                }
                if (document.getElementById("selAssignTo"))
                  selAssignTo.selectedIndex = -1;
                selChannel.CCDisabled = bDisabled;
              }
            }

            function refreshTaskList()
            {
                gsClaimAspectServiceChannelID = selChannel.value;
                selTaskID.Clear();
                var oTasks = xmlData.selectNodes("/Root/Reference[@List='TaskList' and @ClaimAspectServiceChannelID='"
                    + gsClaimAspectServiceChannelID + "']");
                for (var i = 0; i < oTasks.length; i++){
                    selTaskID.AddItem( oTasks[i].getAttribute("ReferenceID"),
                        oTasks[i].getAttribute("Name"), "", i);
                }
                if (parseInt(strCheckListID, 10) > 0) {
                  //selTaskID.value = parseInt(strCheckListID, 10);
                  var strTaskName = xmlData.selectSingleNode("/Root/DiaryTask/@TaskName").text;
                  selTaskID.value = selTaskID.GetValueFromText(strTaskName);
                  if (selTaskID.selectedIndex == -1){                     
                     //task list does not have the item. This could be because the user is trying to change a system generated task.
                     // we will add it to the drop down.
                     selTaskID.AddItem( strCheckListID, strTaskName, "", selTaskID.Options.length + 1);
                     selTaskID.value = selTaskID.GetValueFromText(strTaskName);
                  }
                }
                if (document.getElementById("selAssignTo"))
                  selAssignTo.selectedIndex = -1;
            }

            function doSave(){
              var sProc = "";
              var sRequest = "";
              var aRequests = new Array();
              var sMethod = "";

              if (parseInt(strCheckListID, 10) == 0){
                if (selPertainsTo.selectedIndex == -1){
                  ClientWarning("Please select a value for Pertains To.");
                  return;
                }
                if (selTaskID.selectedIndex == -1){
                  ClientWarning("Please select a Task.  If the Task selection list is empty, click cancel and then try to enter the note again.");
                  return;
                }
                if (selAssignTo.selectedIndex == -1){
                  ClientWarning("Please select a Assign To.");
                  return;
                }
              }

              // Alarm date is required
              if (txtAlarmDate.value == ""){
                ClientWarning("Please enter the Alarm Date.");
                return;
              }

              var dtNow = new Date();

              // Alarm date cannot be in the past
              if ( txtAlarmDate.date.getTime() < dtNow.getTime() ){
                ClientWarning("Alarm Date cannot be in the past.");
                return;
              }

							/*// Was a snooze entered for the alarm date?
							var bAddedNote = false;
							if ( ( parseInt( strCheckListID, 10 ) > 0 ) && ( txtAlarmDate.date.getTime() > orgAlarmDate.getTime() ) )
							{
          				var strReq = "CheckListSnoozeReason.asp?UserID=" + gsUserID + "&CheckListID=" + strCheckListID +
											"&AlarmDateNew=" + txtAlarmDate.value + "&AlarmDateOld=" + formatDateTime( orgAlarmDate );

          				var bResult = window.showModalDialog( strReq, window,
											"dialogHeight:230px; dialogWidth:340px; resizable:no; status:no; help:no; center:yes;" );

									if ( bResult == "false" )
									{
											ClientWarning( "You must enter a reason in order to delay a task." );
											return;
									}
									bAddedNote = true;
							}*/

              if (parseInt(strCheckListID, 10) > 0)
							{
                sProc = "uspDiaryUpdDetail";
                sRequest = "ChecklistID=" + strCheckListID +
                            "&AlarmDate=" + txtAlarmDate.value +
                            "&AssignedUserID=" + strAssignedUserID +
                            "&UserTaskDescription=" + escape(txtDescription.value) +
                            "&UserID=" + gsUserID +
                            "&SysLastUpdatedDate=" + gsSysLastUpdateDate;
                sMethod = "ExecuteSpNpAsXML";
              } else {
                sProc = "uspDiaryInsDetail";
                sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                            "&TaskID=" + selTaskID.value +
                            "&AlarmDate=" + txtAlarmDate.value +
                            "&AssignmentPoolFunctionCD=" + selAssignTo.value +
                            "&SupervisorFlag=" + chkAssigntoSupervisor.value +
                            "&UserTaskDescription=" + escape(txtDescription.value) +
                            "&UserID=" + gsUserID;
                sMethod = "ExecuteSpNp";
              }

              //alert(sProc + "\n" + sRequest); return;

              aRequests.push( { procName : sProc,
                                method   : sMethod,
                                data     : sRequest }
                            );
              var sXMLRequest = makeXMLSaveString(aRequests);
              //alert(sXMLRequest);

              var objRet = XMLSave(sXMLRequest);

              if (sMethod == "ExecuteSpNpAsXML"){
                if (objRet && objRet.code == 0 && objRet.xml) {
                  dialogArguments.refreshCheckListWindow(false);
									/*if ( bAddedNote == true && typeof(top.refreshCheckListWindow) == "function" )
      							top.refreshCheckListWindow( top.vLynxID, top.vUserID );*/
                  window.close();
                }
              } else {
                if (objRet && objRet.code == 0) {
                  dialogArguments.refreshCheckListWindow(false);
									/*if ( bAddedNote == true && typeof(top.refreshCheckListWindow) == "function" )
      							top.refreshCheckListWindow( top.vLynxID, top.vUserID );*/
                  window.close();
                }
              }
            }

            function doCancel(){
              window.returnValue = "false";
              window.close();
            }

            function setSupervisor(){
              var bDisableSupervisor = false;
              var oPertainsTo = xmlData.selectSingleNode("/Root/Reference[@List='PertainsTo' and @ReferenceID='" + gsClaimAspectID + "']");
              switch (selAssignTo.value){
                case "SELF":
                  if (gsUserHasSupervisor != "1"){
                    bDisableSupervisor = true;
                  }
                  break;
                case "OWN":
                  if (oPertainsTo){
                    bDisableSupervisor = (oPertainsTo.getAttribute("FOHasSupervisor") != "1");
                  } else {
                    bDisableSupervisor = true;
                  }
                  break;
                case "ALST":
                  if (oPertainsTo){
                    bDisableSupervisor = (oPertainsTo.getAttribute("FAHasSupervisor") != "1");
                  } else {
                    bDisableSupervisor = true;
                  }
                  break;
                case "SPRT":
                  if (oPertainsTo){
                    bDisableSupervisor = (oPertainsTo.getAttribute("FSHasSupervisor") != "1");
                  } else {
                    bDisableSupervisor = true;
                  }
                  break;
              }
              //alert(bDisableSupervisor);
              if (bDisableSupervisor){
                chkAssigntoSupervisor.CCDisabled = false;
                chkAssigntoSupervisor.value = 0;
                chkAssigntoSupervisor.CCDisabled = true;
                chkAssigntoSupervisor.title = selAssignTo.text + " has no supervisor";
              } else {
                chkAssigntoSupervisor.CCDisabled = false;
                chkAssigntoSupervisor.value = 0;
                chkAssigntoSupervisor.title = "";
              }
            }

            var bWithinOnChangeEvent = false;

						// This event handled to clear the delay combo box when the date widget is used.
            function OnChangeDateInput()
						{
                if ( !bWithinOnChangeEvent )
                {
                    bWithinOnChangeEvent = true;
                    try
                    {
                        selDelayBy.SetTextFromValue( "none", " " );
                        selDelayBy.selectedIndex = 0;
                    }
                    catch ( e ) { }
                    bWithinOnChangeEvent = false;
                }
						}

						// User changed the selection in the Delay By combo box.
						function OnChangeDelayBy()
						{
                if ( !bWithinOnChangeEvent )
                {
                    bWithinOnChangeEvent = true;
                    try
                    {
                        // Change the "Delay By" text to "No Delay".
                        selDelayBy.SetTextFromValue( "none", "No Delay" );

                        // Calculate the new time that we are delayed till.
                        var dtNow = new Date();
                        var dtTime = new Date();
                        dtTime.setTime( orgAlarmDate.getTime() );

                        // Start from now if the alarm is expired.
                        if ( dtTime.valueOf() < dtNow.valueOf() )
                            dtTime = dtNow;

                        if ( selDelayBy.value == "none" )
                            dtTime.setTime( orgAlarmDate.getTime() );
                        else
                        {
                            if ( selDelayBy.value == "hour2" )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * 2 ) );
                            else if ( selDelayBy.value == "hour4" )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * 4 ) );
                            else if ( selDelayBy.value == "day1" )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * 24 ) );
                            else if ( selDelayBy.value == "day2" )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * 24 * 2 ) );

                            // Wrap the time around if it is put after business hours.
                            if ( ( dtTime.getHours() > 20 ) || ( ( dtTime.getHours() == 20 ) && ( dtTime.getMinutes() != 0 ) ) )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * 12 ) );

                            // If somehow it fell short of 8 am, put it right at 8.
                            if ( dtTime.getHours() < 8 )
                                dtTime.setTime( dtTime.getTime() + ( 1000 * 60 * 60 * ( 8 - dtTime.getHours() ) ) );
                        }

                        txtAlarmDate.date = dtTime;
                    }
                    catch ( e ) { alert( "CheckListDetails.xsl::OnChangeDelayBy() " + e.message ); }

                    bWithinOnChangeEvent = false;
                }
						}


            ]]>

        </script>

      </head>
      <body style="margin:0px;padding:0px;" onload1="initPage()">
        <div style="position:absolute; z-index:1; top:5px; left:5px;">
            <xsl:apply-templates select="/Root/DiaryTask">
              <xsl:with-param name="LynxID"><xsl:value-of select="$LynxId"/></xsl:with-param>
              <xsl:with-param name="TaskCRUD"><xsl:value-of select="$TaskCRUD"/></xsl:with-param>
              <xsl:with-param name="RawContext"><xsl:value-of select="$RawContext"/></xsl:with-param>
            </xsl:apply-templates>
        </div>

        <!-- 18Dec2012 TVD Please Wait Fix -->
		<div id="pageLoading" style="position:absolute; z-index:1; top:240px; left:75px; color:Red">
			Loading, Please wait....
		</div>

        <!-- Data Island for hiding 'reference' xml -->
        <xml id="xmlData"><xsl:copy-of select="/Root"/></xml>


	  <script language="JavaScript">
		doAgain();
	  </script>
	</body>
    </html>
  </xsl:template>

  <xsl:template match="DiaryTask">

    <xsl:param name="LynxID"/>
    <xsl:param name="TaskCRUD"/>
    <xsl:param name="RawContext"/>

    <xsl:value-of select="js:SetCRUD(string($TaskCRUD))"/>

    <!-- Variables -->
    <xsl:variable name="Context">
      <xsl:value-of select="local:GetDefaultContext(string($RawContext))"/>
    </xsl:variable>
    <xsl:variable name="CheckListID">
      <xsl:value-of select="@CheckListID"/>
    </xsl:variable>

    <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
      <colgroup>
        <col width="65px"/>
        <col width="184px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td>Pertains To</td>
        <td colspan="2">
          <xsl:variable name="pertainsTo"><xsl:value-of select="concat(@ClaimAspectCode, @ClaimAspectNumber)"/></xsl:variable>
          <IE:APDCustomSelect id="selPertainsTo" name="selPertainsTo" displayCount="5"
              blankFirst="false" canDirty="false" CCTabIndex="6" required="true" width="150" onChange="refreshChannelList()">
            <xsl:attribute name="value"><xsl:value-of select="$pertainsTo"/></xsl:attribute>
            <xsl:if test="count(/Root/Reference[@List='PertainsTo' and @PertainsTo != 'clm'])=1">
              <xsl:attribute name="selectedIndex">0</xsl:attribute>
            </xsl:if>
            <xsl:if test="$CheckListID!=0">
              <xsl:attribute name="CCDisabled">true</xsl:attribute>
            </xsl:if>
            <xsl:for-each select="/Root/Reference[@List='PertainsTo' and @PertainsTo != 'clm']">
          	<IE:dropDownItem imgSrc="" style="display:none">
              <xsl:attribute name="value"><xsl:value-of select="@PertainsTo"/></xsl:attribute>
              <xsl:value-of select="@Name"/>
            </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Channel</td>
        <td colspan="2">
          <IE:APDCustomSelect id="selChannel" name="selChannel" displayCount="5"
              blankFirst="false" canDirty="false" CCTabIndex="7" required="true" width="150" onChange="refreshTaskList()">
            <xsl:if test="$CheckListID!=0">
              <xsl:attribute name="CCDisabled">true</xsl:attribute>
            </xsl:if>
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Task</td>
        <td colspan="2">
          <IE:APDCustomSelect id="selTaskID" name="selTaskID" displayCount="5"
            blankFirst="false" canDirty="false" CCTabIndex="8" required="true" sorted="true" width="253" onChange="taskSelected()">
            <!-- <xsl:attribute name="value"><xsl:value-of select="$CheckListID"/></xsl:attribute>
            <xsl:for-each select="/Root/Reference[@List='TaskList']">
          	<IE:dropDownItem imgSrc="" style="display:none">
              <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
              <xsl:value-of select="@Name"/>
            </IE:dropDownItem>
            </xsl:for-each> -->
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td valign="top">Alarm Date</td>
        <td>
          <IE:APDInputDate id="txtAlarmDate" name="txtAlarmDate" type="datetime"
            futureDate="true" required="true" canDirty="false" CCDisabled="false" CCTabIndex="9" onChange="OnChangeDateInput()" >
            <xsl:if test="@AlarmModificationAllowedFlag='0'">
              <xsl:attribute name="CCDisabled">true</xsl:attribute>
            </xsl:if>
            <xsl:if test="@AlarmDate != '1900-01-01T00:00:00'">
              <xsl:attribute name="value"><xsl:value-of select="js:formatSQLDateTime(string(@AlarmDate))"/></xsl:attribute>
            </xsl:if>
          </IE:APDInputDate>
        </td>
        <td>
          <xsl:if test="$CheckListID!=0">
          	<!--<IE:APDCustomSelect id="selDelayBy" name="selDelayBy" 
							displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false"
              CCTabIndex="10" required="false" width="70" onChange="OnChangeDelayBy()"
							selectedIndex="0">

          	  <IE:dropDownItem value="none" style="display:none">Delay By:</IE:dropDownItem>
          	  <IE:dropDownItem value="hour2" style="display:none">2 Hours</IE:dropDownItem>
          	  <IE:dropDownItem value="hour4" style="display:none">4 Hours</IE:dropDownItem>
          	  <IE:dropDownItem value="day1" style="display:none">1 Day</IE:dropDownItem>
          	  <IE:dropDownItem value="day2" style="display:none">2 Days</IE:dropDownItem>
          	</IE:APDCustomSelect>-->
          </xsl:if>
          <xsl:if test="$CheckListID=0"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
        </td>
      </tr>
      <xsl:if test="$CheckListID=0">
      <tr>
        <td>Assign To</td>
        <td colspan="2">
          <table border="0" cellpadding="0" cellspacing="0">
            <colgroup>
              <col width="160"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td>
                <IE:APDCustomSelect id="selAssignTo" name="selAssignTo" displayCount="5"
                  blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="11" required="true"
									width="150" onChange="setSupervisor()">
                	<IE:dropDownItem value="SELF" style="display:none">Self</IE:dropDownItem>
                	<IE:dropDownItem value="OWN" style="display:none">File Owner</IE:dropDownItem>
                	<IE:dropDownItem value="ALST" style="display:none">File Analyst</IE:dropDownItem>
                	<IE:dropDownItem value="SPRT" style="display:none">File Support</IE:dropDownItem>
                </IE:APDCustomSelect>
              </td>
              <td>
                <IE:APDCheckBox id="chkAssigntoSupervisor" name="chkAssigntoSupervisor" caption="Supervisor" value="0" required="false" CCTabIndex="12" canDirty="false"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      </xsl:if>
      <tr>
        <td valign="top">Comment</td>
        <td colspan="2">
          <IE:APDTextArea id="txtDescription" name="txtDescription" width="250" height="100" canDirty="false" CCDisabled="false" CCTabIndex="13" onChange="" >
            <xsl:attribute name="value"><xsl:value-of select="@UserTaskDescription"/></xsl:attribute>
            <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata[@Entity='DiaryTask']/Column[@Name='UserTaskDescription']/@MaxLength"/></xsl:attribute>
            <xsl:attribute name="required"><xsl:value-of select="/Root/Metadata[@Entity='DiaryTask']/Column[@Name='UserTaskDescription']/@Nullable"/></xsl:attribute>
          </IE:APDTextArea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table border="0" cellpadding="0" cellspacing="0" style="width:100%;table-layout:fixed">
            <colgroup>
              <col width="*"/>
              <col width="150px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
              <td>
                <IE:APDButton id="btnSave" name="btnSave" value="Save" width="" CCDisabled="false" CCTabIndex="14" onButtonClick="doSave()"/>
                <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="" CCDisabled="false" CCTabIndex="15" onButtonClick="doCancel()"/>
              </td>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>
</xsl:stylesheet>


