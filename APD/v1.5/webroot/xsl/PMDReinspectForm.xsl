<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    id="PMDReinspectForm">

<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ClaimCRUD" select="Claim"/>
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="ImageRootDir"/>

<xsl:template match="/Root">

  <xsl:variable name="SupervisorFlag" select="session:XslGetSession('SupervisorFlag')"/>

	<xsl:variable name="SupervisorForm" select="@SupervisorForm"/>

	<!-- FormLocked - Used to detrmine if Reinspection values will be displayed as editable or static and whether edit control buttons
	       will be displayed at all.  This variable may also be used to prevent the edit control buttons from displaying when the record is unlocked,
		   but on Reinspection record exists yet*.  The value of FormLocked will be 0 when:
			 -  Reinspect record is locked and there is no override permission
			 -  *No Reinspect record exists for the selected estimate/supplement
			 -	Reinspect record exists and logged in UserID does not match the ReinspectorUserID
	-->
	<xsl:variable name="FormLocked">
		<xsl:choose>
			<xsl:when test="(Reinspect/@LockedFlag = 1 or (Reinspect/@ReinspectorUserID != $UserID and Reinspect/@ReinspectorUserID != 0)) and Reinspect/@EditLockedReinspection = 0">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectFormGetDetailXML,PMDReinspectForm.xsl,777,0, 33  -->

<HEAD>
  <TITLE>Reinspection</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family1 : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight1 : bold;
		  font-size1 : 10px;
		  cursor : hand;
	  }
  </style>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  var gsDocumentID = '<xsl:value-of select="@DocumentID"/>';
  var gsSupervisorForm = '<xsl:value-of select="@SupervisorForm"/>';
  var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var gsImageLocation = '<xsl:value-of select="Reinspect/@ImageLocation"/>';
  var gsSupervisorFlag = '<xsl:value-of select="$SupervisorFlag"/>';
  var gsSupFormAvail = '<xsl:value-of select="@SupFormAvail"/>';
  var gbLocked = <xsl:value-of select="Reinspect/@LockedFlag = '1'"/>;
  var gsUserID = "<xsl:value-of select="$UserID"/>";

<![CDATA[

//init the table selections, must be last
function initPage(){
  if (typeof(parent.hideProgress) == "function")
    parent.hideProgress();

	parent.resizeScrollTable(document.getElementById("CNScrollTable"));

	parent.gsImageRootDir = gsImageRootDir;
	parent.gsImageLocation = gsImageLocation;

	parent.gsPageName = gsSupervisorForm == 0 ? "Reinspection Form" : "Supervisor Form";
	tab11.className = gsSupervisorForm == 0 ? "tabactive1" : "tab1";
	/*if (gsSupFormAvail > 0 || gsSupervisorFlag == 1){
		tab12.className = gsSupervisorForm == 1 ? "tabactive1" : "tab1";
	}*/

  //onpasteAttachEvents()

  //set dirty flags associated with all inputs and selects and checkboxes.
  var aInputs = null;
  var obj = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
    if (aInputs[i].type=="text")
      aInputs[i].attachEvent("onchange", SetDirty);

	if (aInputs[i].type=="text")
      aInputs[i].attachEvent("onkeypress", SetDirty);

    if (aInputs[i].type=="checkbox")
	  aInputs[i].attachEvent("onclick", SetDirty);
  }
  aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++)
    aInputs[i].attachEvent("onchange", SetDirty);
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function UpLevel(){
	/*var sHREF = "PMDEstimateList.asp?AssignmentID=" + parent.gsAssignmentID;
	frmReinspect.txtCallBack.value = sHREF;
	if (parent.CheckDirty())
		window.navigate(sHREF);*/
  //window.history.back();
  if (typeof(parent.restoreEstimate) == "function")
    parent.restoreEstimate();
}

function CheckDate(obj){
  	if (obj.value == "") return true;
    if (obj.value.length < 6 || parent.chkdate(obj) == false ) {
		parent.ClientWarning("The entered date is invalid. Please enter dates in 'mm/dd/yyyy' format");
        obj.select();
        return false;
    }
    return true;
}

function OnGridSelect(oRow){
	var objDetailNum = frmReinspectDetail.selEstimateDetailNumber ? frmReinspectDetail.selEstimateDetailNumber : frmReinspectDetail.txtEstimateDetailNumber;
	objDetailNum.value = oRow.cells[2].innerText;
	frmReinspectDetail.selException.value = oRow.cells[1].innerText
	frmReinspectDetail.txtPosition.value = oRow.cells[0].innerText;
	frmReinspectDetail.txtReinspectID.value = oRow.cells[0].getAttribute("ReinspectID");
	frmReinspectDetail.txtSysLastUpdatedDate.value = oRow.cells[0].getAttribute("SysLastUpdatedDate");
	frmReinspectDetail.txtItem.value = oRow.cells[0].getAttribute("DetailNumber");
	frmReinspectDetail.txtDescription.value = oRow.cells[3].innerText;
	frmReinspectDetail.txtEstimate.value = oRow.cells[4].innerText;
	frmReinspectDetail.txtReinspection.value = oRow.cells[5].innerText;
}

function txtComments_onkeypress(obj, event){
	if (obj.value.length > obj.getAttribute("maxlength") - 1){
		event.returnValue = false;
		return;
	}
}

function btnCalc_onclick(mode){
	var sRetVal;
	var sHREF = "PMDReinspectCalc.asp?DocumentID=" + gsDocumentID + "&mode=" + mode;
	sRetVal = window.showModalDialog(sHREF, "", "dialogHeight:408px; dialogWidth:499px; resizable:no; status:no; help:no; center:yes;");
	if (sRetVal != undefined){
		var arrRet = sRetVal.split("|");
		if (arrRet[1] == 'rei')
			frmReinspectDetail.txtReinspection.value = arrRet[0];
		else
			frmReinspectDetail.txtEstimate.value = arrRet[0];
	}
}

function Calibrate(){
	window.showModalDialog("PMDReinspectCalibration.asp?DocumentID=" + gsDocumentID,"","dialogHeight:480px;dialogWidth:785px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
}

function btnReport_onclick(){
 	var sHREF = "PMDReportViewer.asp?Report=reinspect&ShopLocationID=" + parent.gsShopLocationID + "&Args=" + gsDocumentID + "|" + gsSupervisorForm;
	window.showModalDialog(sHREF, "", "dialogHeight:700px; dialogWidth:780px; resizable:no; status:no; help:no; center:yes;");
}

function btnViewEstimate_onclick(){
	parent.ShowEstimateViewer();
}

function btnPhoto_onclick(){
	parent.ShowPhotoViewer();
}

function IsDirty(){
	SaveForm();
}

function SaveForm(){

	var comment = frmReinspect.txtComments;
	var maxLen = comment.getAttribute("maxlength");
  
	if (comment.value.length > maxLen){
		comment.value = comment.value.substr(0, maxLen);
		parent.ClientWarning("The maximum length for a comment is " + maxLen + ".  Your comment has been trimmed to this length.  To continue click 'Save' again.");
		return;
	}

	if (!CheckDate(frmReinspect.txtReinspectionDate)) return;
	if (frmReinspect.txtReinspectionDate.value == ''){
		parent.ClientWarning("Please enter a Reinspection Date.");
		frmReinspect.txtReinspectionDate.select();
		return;
	}

	if (frmReinspect.selReinspectionClass.selectedIndex <= 0){
		parent.ClientWarning("Please enter a Reinspection Class.");
		frmReinspect.selReinspectionClass.focus();
		return;
	}

	if (frmReinspect.selRepairStatus.selectedIndex <= 0){
		parent.ClientWarning("Please enter a Repair Status.");
		frmReinspect.selRepairStatus.focus();
		return;
	}

	if (frmReinspect.selReinspector && frmReinspect.selReinspector.selectedIndex <= 0){
		parent.ClientWarning("Please select a Reinspector for this Reinspection.");
		frmReinspect.selReinspector.focus();
		return;
	}

	if (frmReinspect.txtBaseEstimate.value == ''){
		parent.ClientWarning("Please enter a Base Estimate Amount.");
		frmReinspect.txtBaseEstimate.select();
		return;
	}

  if (gbLocked){
    parent.ClientWarning("This Reinspection is marked completed. Saving changes will unmark the completed status and you have to set the form complete again later.")
    frmReinspect.LockedFlag.value = "0";
  }

	if (frmReinspect.txtReinspectID.value != "0")
		frmReinspect.action += "&proc=uspPMDReinspectUpdDetail";
	else
		frmReinspect.action += "&proc=uspPMDReinspectInsDetail";

  frmReinspect.txtShopLocationID.value = parent.gsShopLocationID;

	disableButtons(true);
  //frmReinspect.btnSaveForm.disabled = true;
  if (typeof(parent.showProgress) == "function")
    parent.showProgress();
	window.setTimeout("frmReinspect.submit()", 200);
	parent.gbDirtyFlag = false;
}


function ProcessForm(event){
	switch(event.srcElement.id){
		case "btnDeleteForm":
			if (parent.YesNoMessage("Reinspection Form", "Are you sure you wish to delete this form and all of its line items?") == "No") return;
			//frmReinspect.btnDeleteForm.disabled = true;
      disableButtons(true);
			frmReinspect.action += "&proc=uspPMDReinspectDel";
      if (typeof(parent.showProgress) == "function")
        parent.showProgress();
			window.setTimeout("frmReinspect.submit()", 200);
			break;
		case "btnNewDetail":
			frmReinspectDetail.txtSysLastUpdatedDate.value = "";
			frmReinspectDetail.txtPosition.value = "";
			frmReinspectDetail.txtItem.value= "";
			frmReinspectDetail.selException.selectedIndex = 0;
			frmReinspectDetail.txtDescription.value = "";
			frmReinspectDetail.txtEstimate.value = "";
			frmReinspectDetail.txtReinspection.value = "";

			if (frmReinspectDetail.txtEstimateDetailNumber)
				frmReinspectDetail.txtEstimateDetailNumber.value = "";
			else
				frmReinspectDetail.selEstimateDetailNumber.selectedIndex = 0;
			break;
		case "btnSaveDetail":
			if (frmReinspectDetail.selException.selectedIndex == 0 || frmReinspectDetail.txtEstimate.value == "" || frmReinspectDetail.txtReinspection.value ==""){
				parent.ClientWarning ("Please be sure the following items are completed: Exception Code, Estimate Amt, and Reinspection Amt");
				return;
			}

			var est = parseFloat(frmReinspectDetail.txtEstimate.value);
			var rein = parseFloat(frmReinspectDetail.txtReinspection.value);

			if ((frmReinspectDetail.selException.value == 'MD' || frmReinspectDetail.selException.value == 'UE') && (rein <= est)){
				parent.ClientWarning("When Exception Code 'Missed Damage' or 'Underestimated' is selected, the Reinspection Amt must be greater than the Estimate Amt.");
				return;
			}

			if (frmReinspectDetail.selException.value != 'MD' && frmReinspectDetail.selException.value != 'UE' && (rein >= est)){
				parent.ClientWarning("When an Exception Code other than 'Missed Damage' or 'Underestimated' is selected, the Reinspection Amt must be less than the Estimate Amt.");
				return;
			}

			var obj = frmReinspectDetail.txtEstimateDetailNumber ? frmReinspectDetail.txtEstimateDetailNumber : frmReinspectDetail.selEstimateDetailNumber;
			if (parseInt(obj.value) > 32767){
				parent.ClientWarning("The maximum allowable value for 'Estimate Item' is 32767");
				obj.focus();
				return;
			}

			var sum = 0.00;
			for (i=0; i<tblSort1.rows.length; i++){
				if (obj.value == tblSort1.rows[i].cells[2].innerText && frmReinspectDetail.txtItem.value == ""){
					parent.ClientWarning("A reinspection item has already been entered for Estimate Item " + obj.value);
					return
				}
				sum += parseFloat(tblSort1.rows[i].cells[4].innerText);
			}

			if (frmReinspectDetail.txtItem.value == "")
				sum += parseFloat(frmReinspectDetail.txtEstimate.value);

			if (sum > frmReinspect.txtBaseEstimate.value){
				parent.ClientWarning("The sum of the line item Estimate Amounts exceeds the Base Estimate Amount");
				return;
			}

      if (gbLocked){
        parent.ClientWarning("This Reinspection is marked completed. Saving changes will unmark the completed status and you have to set the form complete again later.")
      }
			if (frmReinspectDetail.txtItem.value != "")
				frmReinspectDetail.action += "&proc=uspPMDReinspectDetailUpdDetail";
			else
				frmReinspectDetail.action += "&proc=uspPMDReinspectDetailInsDetail";

      disableButtons(true);
      //frmReinspectDetail.btnSaveDetail.disabled = true;
      if (typeof(parent.showProgress) == "function")
        parent.showProgress();
			window.setTimeout("frmReinspectDetail.submit()", 200);
			break;
		case "btnDeleteDetail":
			if (frmReinspectDetail.txtItem.value != ""){
        if (gbLocked){
          parent.ClientWarning("This Reinspection is marked completed. Saving changes will unmark the completed status and you have to set the form complete again later.")
        }
				frmReinspectDetail.action += "&proc=uspPMDReinspectDetailDel";
				disableButtons(true);
        //frmReinspectDetail.btnDeleteDetail.disabled = true;
        if (typeof(parent.showProgress) == "function")
          parent.showProgress();
				window.setTimeout("frmReinspectDetail.submit()", 200);
			}
			else
				parent.ClientWarning("You must first select a line item to be deleted.");
			break;
	}
}

function CheckMoneyPaste(event){
  var s = clipboardData.getData("text");
  
  if (isNaN(s)){
    event.returnValue = false;
    parent.ClientWarning("The non-numeric value, '" + s + "', can not be pasted into this field.")
    return;
  }
  
  if (Number(s) > 9999999)
    event.returnValue = false;
    parent.ClientWarning("The largest value that can be pasted into this field is 9,999,999 (no commas).");
    return;  
}

function CheckMoney(event){
	var obj = event.srcElement;
	var idx = obj.value.indexOf(".")

	if ((event.keyCode < 48  && event.keyCode != 46) || event.keyCode > 57){
		event.returnValue = false;
		return;
	}

	if (idx > -1){
		if (event.keyCode == 46){			// allow only one decimal point
			event.returnValue = false;
			return;
		}
	}
	else{
		if (obj.value.length > 6){
			event.returnValue = false;
			return;
		}
	}
  
  
}

function NumbersOnly(event, obj){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
	if (obj.value.length > 4) event.returnValue = false;
}

function chkLocked_onclick(){
	frmReinspect.txtLocked.value = frmReinspect.chkLocked.checked == 1 ? 1 : 0;
}

function TabChange(sSupForm){
	var sHREF = "PMDReinspectForm.asp?SupForm=" + sSupForm + "&DocumentID=" + gsDocumentID
	frmReinspect.txtCallBack.value = sHREF;
	if (parent.CheckDirty())
		window.navigate(sHREF);

}

function PostForm() {
	// get the comments
  var sURL = "/frmComments.htm";
  var sArgs = "dialogWidth:350px;dialogHeight:165px;scrollbars:no;center:yes;location:no;directories:no;status:no;menubar:no;toolbar:no;resizable:no;help:no";
  var sRet = window.showModalDialog(sURL, "", sArgs);
  
  if (frmReinspect.txtReinspectID.value != "0")
		frmReinspect.action += "&proc=uspPMDReinspectUpdDetail";
  frmReinspect.txtCreateDocument.value = "true";
	frmReinspect.btnPost.disabled = true;
  frmReinspect.txtComments2Rep.value = sRet;
  if (typeof(parent.showProgress) == "function")
    parent.showProgress();
  
  window.setTimeout("frmReinspect.submit()", 200);
	parent.gbDirtyFlag = false;  
}

function FormComplete(obj){
  //obj.disabled = true;
  if (obj.value == "Complete"){
    frmReinspect.LockedFlag.value = "1";
  }
  SaveForm();
}

function Send2Shop(){
  frmReinspect.txtFaxDocument.value = "true";
	frmReinspect.btnPost.disabled = true;
  if (typeof(parent.showProgress) == "function")
    parent.showProgress();
  
  window.setTimeout("frmReinspect.submit()", 200);
}

function disableButtons(bDisabled){
  if (frmReinspect.btnFormStart) frmReinspect.btnFormStart.disabled = bDisabled;
  if (frmReinspect.btnPost) frmReinspect.btnPost.disabled = bDisabled;
  if (frmReinspect.btnSend2Shop) frmReinspect.btnSend2Shop.disabled = bDisabled;
  if (frmReinspect.btnCalibrate) frmReinspect.btnCalibrate.disabled = bDisabled;
  if (frmReinspect.btnViewEstimate) frmReinspect.btnViewEstimate.disabled = bDisabled;
  if (frmReinspect.btnPhoto) frmReinspect.btnPhoto.disabled = bDisabled;
  if (frmReinspect.btnReport) frmReinspect.btnReport.disabled = bDisabled;
  if (frmReinspect.btnDeleteForm) frmReinspect.btnDeleteForm.disabled = bDisabled;
  if (frmReinspect.btnSaveForm) frmReinspect.btnSaveForm.disabled = bDisabled;
  
  if (document.getElementById("frmReinspectDetail")){
    if (frmReinspectDetail.btnCalcEst) frmReinspectDetail.btnCalcEst.disabled = bDisabled;
    if (frmReinspectDetail.btnCalcReI) frmReinspectDetail.btnCalcReI.disabled = bDisabled;
    if (frmReinspectDetail.btnNewDetail) frmReinspectDetail.btnNewDetail.disabled = bDisabled;
    if (frmReinspectDetail.btnDeleteDetail) frmReinspectDetail.btnDeleteDetail.disabled = bDisabled;
    if (frmReinspectDetail.btnSaveDetail) frmReinspectDetail.btnSaveDetail.disabled = bDisabled;
  }
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="initPage();" bgcolor="#FFFAEB" style="margin-right:none;">

<!-- Reinspection/Supervisor Form tabs -->
<DIV unselectable="on" id="tabs1" class="apdtabs" style="position:absolute; z-index:2; top: 2px; left: 6px;">
  <SPAN unselectable="on" id="tab11" class="tabactive1" style="cursor:hand" onclick="TabChange(0)">Reinspection Form</SPAN>
  <!-- <xsl:if test="$SupervisorFlag = 1 or @SupFormAvail &gt; 0">
    <SPAN unselectable="on" id="tab12" class="tab1" style="cursor:hand" onclick="TabChange(1)">Supervisor Form</SPAN>
  </xsl:if> -->
</DIV>

<DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:21px; width:1005px;">
  <TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
    <tr unselectable="on">
      <td unselectable="on" width="125">Reinspection Status:</td>
      <td unselectable="on">
  		  <xsl:choose>
  		    <xsl:when test="Reinspect/@ReinspectID != 0 and $FormLocked = 1">
            <xsl:attribute name="style">color:#FF0000</xsl:attribute>
            Completed
          </xsl:when>
  		    <xsl:when test="Reinspect/@ReinspectID != 0 and $FormLocked = 0">
            <xsl:attribute name="style">color:#008000</xsl:attribute>
            In Progress
          </xsl:when>
  		    <xsl:when test="Reinspect/@ReinspectID = 0">
            New
          </xsl:when>          
  			  <xsl:otherwise>Unknown</xsl:otherwise>
        </xsl:choose>
      </td>
      <td unselectable="on"><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></td>
	  <td unselectable="on" align="right"><IMG src="/images/smrefresh.gif" onClick="UpLevel()" border="0" width="16" height="16" alt="Reload Estimate List" style="cursor:hand;" /></td>
	  <td unselectable="on" width="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
  </TABLE>

  <!-- get RI header info -->
  <DIV unselectable="on" style="height:228px; overflow:hidden">
    <FORM id="frmReinspect" method="post">
	  <xsl:attribute name="action">
		PMDReinspectForm.asp?DocumentID=<xsl:value-of select="@DocumentID"/>&amp;SupForm=<xsl:value-of select="$SupervisorForm"/>
	  </xsl:attribute>

	  <input type="hidden" name="UserID"><xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute></input>
	  <input type="hidden" id="txtCallBack" name="CallBack"/>
    <input type="hidden" id="txtCreateDocument" name="CreateDocument" value="false"/>
    <input type="hidden" id="txtFaxDocument" name="FaxDocument" value="false"/>
    <input type="hidden" id="txtShopLocationID" name="ShopLocationID"/>
    <input type="hidden" id="txtDocumentID" name="DocumentID">
      <xsl:attribute name="value"><xsl:value-of select="/Root/@DocumentID"/></xsl:attribute>
    </input>
    <input type="hidden" id="txtComments2Rep" name="Comments2Rep"></input>
  
	  <xsl:apply-templates select="Reinspect">					<!-- get form data -->
  		<xsl:with-param name="UserID" select="$UserID"/>
  		<xsl:with-param name="FormLocked" select="$FormLocked"/>
      <xsl:with-param name="ClaimCRUD" select="$ClaimCRUD"/>
      <xsl:with-param name="ShopCRUD" select="$ShopCRUD"/>
	  </xsl:apply-templates>

	  <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

	  <TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
  		<tr unselectable="on">
        <td>
    			<xsl:if test="$FormLocked=0">
    			  <input type="button" id="btnFormStart" class="formButton" style="width:80;" onclick="FormComplete(this)">
    			    <xsl:attribute name="value">
    				  <xsl:choose>
    				    <xsl:when test="Reinspect/@ReinspectID != 0">Complete</xsl:when>
    					<xsl:otherwise>Start</xsl:otherwise>
    				  </xsl:choose>
    				</xsl:attribute>
    			  </input>
    		    </xsl:if>
        </td>
		  <td unselectable="on" colspan="6" align="right">
      <xsl:if test="Reinspect/@ReinspectID != 0">
        <input type="button" id="btnPost" value="Post to Claim" class="formButton" style="width:125;" onclick="PostForm()"/>
      </xsl:if>
      <xsl:if test="Reinspect/@ReinspectID != 0">
        <input type="button" id="btnSend2Shop" value="Send to Shop" class="formButton" style="width:125;" onclick="Send2Shop()"/>
      </xsl:if>
			<xsl:if test="$SupervisorForm = 1 and Reinspect/@ReinspectID != 0">
			  <input type="button" id="btnCalibrate" value="Calibrate" class="formButton" style="width:80;" onclick="Calibrate()"/>
			</xsl:if>
			<input type="button" id="btnViewEstimate" value="Estimate" class="formButton" style="width:80;" onclick="btnViewEstimate_onclick()"/>
		    <input type="button" id="btnPhoto" value="Photos" class="formButton" style="width:80;" onclick="btnPhoto_onclick()"/>
			<xsl:if test="Reinspect/@ReinspectID != 0">
			  <input type="button" id="btnReport" value="Report" class="formButton" style="width:80;" onclick="btnReport_onclick()"/>
			  <xsl:if test="$FormLocked = 0">
			    <input type="button" id="btnDeleteForm" value="Delete" class="formButton" style="width:80;" onclick="ProcessForm(event)"/>
			  </xsl:if>
			</xsl:if>
			<xsl:if test="Reinspect/@ReinspectID != 0">
			  <input type="button" id="btnSaveForm" class="formButton" value="Save" style="width:80;" onclick="SaveForm()"/>
      </xsl:if>
		  </td>
		</tr>
	  </TABLE>
	</FORM>
  </DIV>

  <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

  <DIV id="CNScrollTable">
	<TABLE id="tblHead" unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed; width:100%">
      <TBODY>
        <tr unselectable="on" class="QueueHeader">
		  <td unselectable="on" class="TableSortHeader" width="60" style="cursor:default;"> ReI Item </td>
		  <td unselectable="on" class="TableSortHeader" width="60" style="cursor:default;"> Exc Code </td>
		  <td unselectable="on" class="TableSortHeader" width="60" style="cursor:default;"> Est Item </td>
		  <td unselectable="on" class="TableSortHeader" width="100%" style="cursor:default;"> ReI Comment </td>
		  <td unselectable="on" class="TableSortHeader" width="80" style="cursor:default;"> Est Amt </td>
		  <td unselectable="on" class="TableSortHeader" width="80" style="cursor:default;"> ReI Amt </td>
		  <td unselectable="on" class="TableSortHeader" width="80" style="cursor:default;"> Difference </td>
		  <td unselectable="on" class="TableSortHeader" width="0"></td>  <!--scroll bar spacer -->
	    </tr>
	  </TBODY>
    </TABLE>

	<DIV unselectable="on" class="autoflowTable" style="width:100%; height:190px;">
      <TABLE id="tblSort1" unselectable="on" class="GridTypeTable" border="0" cellspacing="1" cellpadding="1" style="table-layout:fixed; width:100%">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="Reinspect/ReinspectDetail" >
            <xsl:sort select="@DetailNumber" data-type="number"/>
            <xsl:call-template name="ReinspectDetail">
			        <xsl:with-param name="FormLocked" select="$FormLocked"/>
			      </xsl:call-template>
          </xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
 </DIV>

 <DIV unselectable="on" class="ShopAssignTableSrh" style="width:100%; height:77px; overflow:hidden;">
   <xsl:if test="Reinspect/@ReinspectID != 0">		<!-- only show buttons if there is a reinspect record and it is unlocked -->
	 <FORM id="frmReinspectDetail" method="post">
	   <xsl:attribute name="action">
		 PMDReinspectForm.asp?DocumentID=<xsl:value-of select="@DocumentID"/>&amp;SupForm=<xsl:value-of select="$SupervisorForm"/>
	   </xsl:attribute>
	   <input type="hidden" name="ReinspectID" id="txtReinspectID"><xsl:attribute name="value"><xsl:value-of select="//Reinspect/@ReinspectID"/></xsl:attribute></input>
	   <input type="hidden" name="EstimateDocumentID"><xsl:attribute name="value"><xsl:value-of select="@DocumentID"/></xsl:attribute></input>
	   <input type="hidden" name="DetailNumber" id="txtItem"/>
	   <input type="hidden" name="UserID"><xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute></input>
	   <input type="hidden" name="SysLastUpdatedDate" id="txtSysLastUpdatedDate" value=""/>

	   <div style="position:relative; left:7px;">
	   <TABLE unselectable="on" border="0" id="tblInput">

	     <tr>
		   <td width="56px" class="labelColumn">ReI Item</td>
		   <td width="116px" class="labelColumn">Exc Code</td>
		   <td width="500px" class="labelColumn">Est Item</td>
		   <td width="240px" class="labelColumn" colspan="2">
		   	 <IMG src="/images/spacer.gif" width="17" height="4" border="0"/>
		     <input type="button" id="btnCalcEst" class="formButton" value="Est Amt" style="width:76;" onclick="btnCalc_onclick('est')"/>
		     <IMG src="/images/spacer.gif" width="4" height="4" border="0"/>
		     <input type="button" id="btnCalcReI" class="formButton" value="ReI Amt" style="width:76;" onclick="btnCalc_onclick('rei')"/>
		   </td>
		 </tr>
	     <tr>
		   <td><input type="text" id="txtPosition" readonly="readonly" class="inputFld" style="width:56; text-align:right; background-color:#dddddd; color:black;"/></td>
		   <td unselectable="on">
		     <select id="selException" name="ReinspectExceptionCode" class="inputFld" style="width:116">
			   <option></option>
			   <xsl:for-each select="Reference[@ListName='ExceptionCodes']">
         <xsl:sort select="@Name"/>
				 <xsl:call-template name="BuildSelectOptions"/>
			   </xsl:for-each>
			 </select>
		   </td>
		   <td unselectable="on">
			 <xsl:choose>
			   <xsl:when test="count(Reference[@ListName='EstimateDetailNumber']) > 0">
				 <select id="selEstimateDetailNumber" name="EstimateDetailNumber" class="inputFld" style="width:100%">
				   <option></option>
				   <xsl:for-each select="Reference[@ListName='EstimateDetailNumber']">
            <xsl:sort select="@ReferenceID" data-type="number"/>
					 <xsl:call-template name="BuildSelectOptions"/>
				   </xsl:for-each>
				 </select>
			  </xsl:when>
			  <xsl:otherwise>
				<input type="text" id="txtEstimateDetailNumber" name="EstimateDetailNumber" class="inputFld" style="width:56;text-align:right;" onkeypress="NumbersOnly(event, this)"/>					
			  </xsl:otherwise>
			</xsl:choose>
		  </td>
		  <td unselectable="on" colspan="2">
		    <IMG src="/images/spacer.gif" width="17" height="4" border="0"/>
		    <input type="text" id="txtEstimate" name="EstimateAmt" maxlength="10" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="width:76; text-align:right;"/>
		    <IMG src="/images/spacer.gif" width="4" height="4" border="0"/>
			<input type="text" id="txtReinspection" name="ReinspectAmt" maxlength="10" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="width:76; text-align:right;"/>
		  </td>
		</tr>
		<tr>
		  <td colspan="2" align="right" class="labelColumn">ReI Comment:</td>
		  <td>
		    <input type="text" id="txtDescription" name="Comment" class="inputFld" style="width:100%">
			  <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity = 'ReinspectDetail']/Column[@Name = 'Comment']/@MaxLength"/></xsl:attribute>
			</input>
		  </td>
		  <td unselectable="on" colspan="2" align="right">
		    <IMG src="/images/spacer.gif" width="15" height="4" border="0"/>
		   	<input type="button" id="btnNewDetail" value="New" class="formButton" style="width:79;" onclick="ProcessForm(event)"/>
			  <xsl:if test="count(Reinspect/ReinspectDetail) != 0">	<!-- only show delete button if there are reinspect_detail records -->
				<input type="button" id="btnDeleteDetail" value="Delete" class="formButton" style="width:79;" onclick="ProcessForm(event)"/>
			  </xsl:if>
			 <input type="button" id="btnSaveDetail" value="Save" class="formButton" style="width:79;" onclick="ProcessForm(event)"/>
		  </td>
	    </tr>			
	  </TABLE>
	  </div>
	</FORM>
  </xsl:if>
</DIV>
	  
</DIV>

</BODY>
</HTML>
</xsl:template>


<xsl:template match="Reinspect">
	<xsl:param name="UserID"/>
	<xsl:param name="FormLocked"/>
  <xsl:param name="ClaimCRUD"/>
  <xsl:param name="ShopCRUD"/>

<input type="hidden" name="ReinspectID" id="txtReinspectID"><xsl:attribute name="value"><xsl:value-of select="@ReinspectID"/></xsl:attribute></input>
<input type="hidden" name="EstimateDocumentID"><xsl:attribute name="value"><xsl:value-of select="@EstimateDocumentID"/></xsl:attribute></input>
<input type="hidden" name="ClaimAspectID"><xsl:attribute name="value"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute></input>
<input type="hidden" name="SysLastUpdatedDate" id="txtSysLastUpdatedDate"><xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute></input>
				 
    
<IMG src="/images/spacer.gif" width="1" height="4" border="0"/>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
<tr>
  <td width="10" rowspan="2"></td> <!-- spacer column -->
  <td width="230" rowspan="2" valign="top">
	<table border="0" cellpadding="3" cellspacing="3" style="table-layout:fixed">
	  <tr>
	    <td width="95px" class="labelColumn" nowrap="nowrap">LYNX ID: </td>
		  <td width="135px" height="18px" bgcolor="#E3E3E3" nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@LynxID != ''">
          <xsl:choose>
            <xsl:when test="contains($ClaimCRUD, 'R') = true()">
              <a>
                <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
                <xsl:attribute name="href">javascript:NavToClaim(<xsl:value-of select="@LynxID"/>)</xsl:attribute>
                <xsl:value-of select="@LynxID"/>
              </a>
            </xsl:when>
            <xsl:otherwise>  
              <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
              <xsl:value-of select="@LynxID"/>
            </xsl:otherwise>
          </xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>	
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Ins Company: </td>
		<td bgcolor="#E3E3E3" nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@InsuranceCompany != ''">
					<xsl:value-of select="@InsuranceCompany"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Claim Rep: </td>
		<td bgcolor="#E3E3E3" nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@ClaimRep != ''">
					<xsl:value-of select="@ClaimRep"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">
			Estimate Date:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</td>
	    <td bgcolor="#E3E3E3" nowrap="nowrap">
		  	<xsl:choose>
				<xsl:when test="@EstimateDate != ''">
					<xsl:value-of select="@EstimateDate"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:otherwise>
			</xsl:choose>		
		</td>
      </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Supplement: </td>
		<td bgcolor="#E3E3E3" nowrap="nowrap">
		   <xsl:value-of select="@Supplement"/>
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Driveable: </td>
		<td bgcolor="#E3E3E3" nowrap="nowrap">
			<xsl:choose>
				<xsl:when test="@DrivableFlag = ''">
					<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="@DrivableFlag = 0">No</xsl:when>
						<xsl:otherwise>Yes</xsl:otherwise>							
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>		
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">
		  <!-- <xsl:choose>
		    <xsl:when test="@LockedFlag = 1">
			  <xsl:attribute name="style">color:red</xsl:attribute>Form Locked
			</xsl:when>
			<xsl:when test="$FormLocked = 0">Lock Form:</xsl:when>
			<xsl:otherwise></xsl:otherwise>			
		  </xsl:choose> -->
		</td>
		<td>
		  <!-- <xsl:if test="$FormLocked = 0">
		    <input type="checkbox" id="chkLocked" onclick="chkLocked_onclick()">
			  <xsl:if test = "@LockedFlag = 1 and @EditLockedReinspection = 1">
			    <xsl:attribute name="checked"/>			  
			  </xsl:if>
			  </input>
		  </xsl:if> -->
	      <input type="hidden" id="txtLocked" name="LockedFlag">
		    <xsl:attribute name="value"><xsl:value-of select="@LockedFlag"/></xsl:attribute>
		    </input>
		</td>
	  </tr>
	</table>
	
  </td>
  <td width="10px" rowspan="2"></td> <!-- spacer column -->
  <td valign="top" align="center">
    <table>
	  <xsl:choose>
	    <xsl:when test="$FormLocked = 1">
		  <xsl:attribute name="cellpadding">3</xsl:attribute>
		  <xsl:attribute name="cellspacing">3</xsl:attribute>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:attribute name="cellpadding">1</xsl:attribute>
		  <xsl:attribute name="cellspacing">1</xsl:attribute>
		</xsl:otherwise>
	  </xsl:choose>
	  
	  <tr>
	    <td class="labelColumn">
		  *ReI Date:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	    </td>
        <td unselectable="on"  width="120" align="left" nowrap="nowrap">
          <!-- <xsl:choose>
		        <xsl:when test="$FormLocked = 1">
			        <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			        <xsl:attribute name="nowrap"/>
			        <xsl:value-of select="@ReinspectionDate"/>
              <INPUT type="hidden" id="txtReinspectionDate"  name="ReinspectionDate" >
                <xsl:attribute name="value"><xsl:value-of select="@ReinspectionDate"/></xsl:attribute>
              </INPUT>
			      </xsl:when>
			      <xsl:otherwise> -->
		          <INPUT type="text" id="txtReinspectionDate"  name="ReinspectionDate" class="inputFld" style="width:95">
			          <xsl:attribute name="value"><xsl:value-of select="@ReinspectionDate"/></xsl:attribute>
			        </INPUT>
			        <A  href='#' onclick='ShowCalendar(dimgReinspectionDate,txtReinspectionDate)' >
		            <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgReinspectionDate' src='/images/calendar.gif'/>
		          </A>
		          <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
            <!-- </xsl:otherwise>
	        </xsl:choose> -->
        </td>
	  </tr>
	  
	  <tr>
	    <td class="labelColumn">
			*ReI Class:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</td>
	    <td>
		  <!-- <xsl:choose>
		    <xsl:when test="$FormLocked = 1">
			  <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			  <xsl:attribute name="nowrap"/>
			  <xsl:value-of select="/Root/Reference[@ListName='ReinspectionClass' and @ReferenceID = /Root/Reinspect/@ReinspectClassCD]/@Name"/>
        <INPUT type="hidden" id="txtReinspectionClass"  name="ReinspectClassCD" >
          <xsl:attribute name="value"><xsl:value-of select="@ReinspectClassCD"/></xsl:attribute>
        </INPUT>
			</xsl:when>
			<xsl:otherwise> -->
		      <select id="selReinspectionClass" name="ReinspectClassCD" class="inputFld" style="width:130">
			    <option></option>
		        <xsl:for-each select="/Root/Reference[@ListName='ReinspectionClass']">
				  <xsl:call-template name="BuildSelectOptions">
					<xsl:with-param name="current" select="/Root/Reinspect/@ReinspectClassCD"/>
				  </xsl:call-template>
			    </xsl:for-each>
		      </select>
	        <!-- </xsl:otherwise>
	      </xsl:choose>   -->
		</td>
      </tr>
	  <tr>
	    <td class="labelColumn">*ReI Type: </td>
	    <td>
		  <!-- <xsl:choose>
		    <xsl:when test="$FormLocked = 1">
			  <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			  <xsl:attribute name="nowrap"/>
			  <xsl:value-of select="/Root/Reference[@ListName='ReinspectionType' and @ReferenceID = /Root/Reinspect/@ReinspectTypeCD]/@Name"/>
        <INPUT type="hidden" id="txtReinspectionType"  name="ReinspectTypeCD" >
          <xsl:attribute name="value"><xsl:value-of select="@ReinspectTypeCD"/></xsl:attribute>
        </INPUT>
			</xsl:when>
			<xsl:otherwise> -->
		    <select id="selReinspectionType" name="ReinspectTypeCD" class="inputFld" style="width:130">
		      <xsl:choose>
				    <xsl:when test='/Root/@SupervisorForm=1'><option value="SUP" selected="selected">Supervisor</option></xsl:when>
				    <xsl:otherwise>
					    <xsl:for-each select="//Reference[@ListName='ReinspectionType' and @Name != 'Supervisor']">
						    <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="//Reinspect/@ReinspectTypeCD"/></xsl:call-template>
					    </xsl:for-each>
				    </xsl:otherwise>
			    </xsl:choose>
		    </select> 
			<!-- </xsl:otherwise>
	      </xsl:choose> -->
		</td>
      </tr>
	  <tr>
	    <td width="110" class="labelColumn">*Repair Status: </td>
	    <td>
		  <!-- <xsl:choose>
		    <xsl:when test="$FormLocked = 1">
			  <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			  <xsl:attribute name="nowrap"/>
			  <xsl:value-of select="/Root/Reference[@ListName='RepairStatus' and @ReferenceID = /Root/Reinspect/@RepairStatusCD]/@Name"/>
        <INPUT type="hidden" id="txtRepairStatus"  name="RepairStatusCD" >
          <xsl:attribute name="value"><xsl:value-of select="@RepairStatusCD"/></xsl:attribute>
        </INPUT>
			</xsl:when>
			<xsl:otherwise> -->
		      <select id="selRepairStatus" name="RepairStatusCD" class="inputFld" style="width:130">
			    <option></option>
		  	    <xsl:for-each select="//Reference[@ListName='RepairStatus']">
    				  <xsl:call-template name="BuildSelectOptions">
    					<xsl:with-param name="current" select="//Reinspect/@RepairStatusCD"/>
    				  </xsl:call-template>
			      </xsl:for-each>
		      </select>
		    <!-- </xsl:otherwise>
	      </xsl:choose> -->
	    </td>
	  </tr>
	  <tr>
	    <td class="labelColumn">*Reinspector: </td>
	    <td>
		  <xsl:choose>
		  	<xsl:when test="//Reinspect/@ReinspectorUserID = 0">
		  		<select id="selReinspector" name="ReinspectorUserID" class="inputFld" style="width:130">
		    		<option></option>
					<xsl:for-each select="//Reference[@ListName='ProgramManagers']">
						<xsl:call-template name="BuildSelectOptions">
							<xsl:with-param name="current" select="$UserID"/>
						</xsl:call-template>
					</xsl:for-each>
		  		</select>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
				<xsl:attribute name="nowrap"/>
				<xsl:value-of select="@ReinspectorName"/>
				<input type="hidden" name="ReinspectorUserID">
				  <xsl:attribute name="value"><xsl:value-of select="@ReinspectorUserID"/></xsl:attribute>
				</input>
			</xsl:otherwise>
		  </xsl:choose>
		  <!--<input type="text" id="tbReinspector" name="ReinspectorID" class="inputFld"/>-->
		</td>
	  </tr>
	  	  
	</table>
  </td>
  <td width="10"></td> <!-- spacer column -->
  <td valign="top">
	<table cellpadding="2" cellspacing="2" style="table-layout:fixed">
	  <tr>
	    <td class="labelColumn" nowrap="nowrap" width="70px">Shop:</td>
		  <td bgcolor="#E3E3E3" nowrap="nowrap" height="18px">
		  	<xsl:choose>
  				<xsl:when test="@ShopName != ''">
            <xsl:choose>
              <xsl:when test="contains($ShopCRUD, 'R') = true()">
    					  <a>
                  <xsl:attribute name="href">javascript:NavToShop('S', <xsl:value-of select="@ShopID"/>, <xsl:value-of select="@ShopBusinessID"/>)</xsl:attribute>
                  <xsl:value-of select="@ShopName"/>
                </a>
              </xsl:when>
              <xsl:otherwise><xsl:value-of select="@ShopName"/></xsl:otherwise>
            </xsl:choose>
  				</xsl:when>
				  <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
			  </xsl:choose>		
		  </td>
    </tr>
	  <tr>
	    <td class="labelColumn">Prg Mgr:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	    <td bgcolor="#E3E3E3" nowrap="nowrap">
		    <xsl:value-of select="//Reference[@ListName='ProgramManagers' and @ReferenceID = //Reinspect/@ProgramManagerUserID]/@Name"/>
		  </td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Estimator: </td>
		  <td bgcolor="#E3E3E3" nowrap="nowrap"><xsl:value-of select="@AppraiserName"/></td>
	  </tr>
	  <tr>
	    <td class="labelColumn">Shop Rep:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
        <td unselectable="on"  width="120" align="left" nowrap="nowrap">
          <!-- <xsl:choose>
		        <xsl:when test="$FormLocked = 1">
			        <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			        <xsl:attribute name="nowrap"/>
			        <xsl:value-of select="@ShopRepName"/>
              <INPUT type="hidden" id="txtShopRepName"  name="ShopRepName" >
                <xsl:attribute name="value"><xsl:value-of select="@ShopRepName"/></xsl:attribute>
              </INPUT>
			      </xsl:when>
			      <xsl:otherwise> -->
		          <input type="text" id="txtShopRepName"  name="ShopRepName" class="inputFld" style="width:152">
	              <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Reinspect']/Column[@Name='ShopRepName']/@MaxLength"/></xsl:attribute>
	  	          <xsl:attribute name="value"><xsl:value-of select="@ShopRepName"/></xsl:attribute>
		          </input>
			      <!-- </xsl:otherwise>
		      </xsl:choose> -->
        </td>
	  </tr>
	</table>
  </td>
  <td rowspan="2" width="10" valign="top" align="right">
    <table border="0" cellspacing="0" cellpadding="0">
	  <tr><td height="27px" class="labelColumn" align="right">*</td></tr>
	</table>
  </td> <!-- spacer column -->
  <td rowspan="2" width="200" valign="top" align="left">
    <table border="0" width="100%" cellspacing="2" cellpadding="2">
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Base Estimate Amt:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		  <td id="tdBaseEstimate">
		    <xsl:if test="$FormLocked = 1 or /Root/@SummaryBaseAvail = 1">
    			<xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
    			<xsl:attribute name="nowrap"/>
    			<xsl:attribute name="style">text-align:right</xsl:attribute>
    			<xsl:value-of select="@BaseEstimateAmt"/>
		    </xsl:if>
		  
		  <input id="txtBaseEstimate" name="BaseEstimateAmt" class="inputFld" style="text-align:right;width:68px;" onkeypress="CheckMoney(event)" onpaste="CheckMoneyPaste(event)">
		    <xsl:attribute name="type">
			    <xsl:choose>
			      <xsl:when test="$FormLocked = 1 or /Root/@SummaryBaseAvail = 1">hidden</xsl:when>
				    <xsl:otherwise>text</xsl:otherwise>
			    </xsl:choose>
			  </xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="@BaseEstimateAmt"/></xsl:attribute>
	    </input>
		</td>
	</tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Total Additions: </td>
		<td bgcolor="#E3E3E3" width="100" style="text-align:right;">
		  <xsl:value-of select="@TotalAdditions"/>
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Total Subtractions: </td>
		<td bgcolor="#E3E3E3" style="text-align:right;">
		  <xsl:value-of select="@TotalSubtractions"/>
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Actual Difference: </td>
		<td bgcolor="#E3E3E3" style="text-align:right;">
		  <xsl:value-of select="@DifferenceActual"/>
		</td>
	  </tr>
	  <tr>
	    <td class="labelColumn" nowrap="nowrap">Absolute Difference: </td>
		<td bgcolor="#E3E3E3" style="text-align:right;">
		  <xsl:value-of select="@DifferenceAbsolute"/>
		</td>
	  </tr>
	  <tr>
	    <!--<td class="labelColumn" nowrap="nowrap">Satisfactory Amt: </td>-->
		<td class="labelColumn" nowrap="nowrap">Corrected Estimate: </td>
		<td bgcolor="#E3E3E3" style="text-align:right;">
		  <xsl:value-of select="@SatisfactoryDollars"/>
		</td>
	  </tr>
	  <tr>
	  	<td class="labelColumn" nowrap="nowrap">Percent Satisfactory: </td>
		<td bgcolor="#E3E3E3" style="text-align:right;">
		  <xsl:value-of select="@PercentSatisfactory"/>
		</td>
	  </tr>
	  
	</table>
  </td>
  <td rowspan="2" width="20px"></td><!-- spacer column -->
</tr>
<tr>
  <td colspan="3">
    <table border="0" style="table-layout:fixed">
	  <tr>
	    <td width="3px"></td>
	    <td width="83px" valign="top" class="labelColumn">Comments:</td>
	    <td width="379px" height="50px" valign="top">
		  <!-- <xsl:choose>
		    <xsl:when test="$FormLocked = 1">
			  <xsl:attribute name="bgcolor">#E3E3E3</xsl:attribute>
			  <xsl:value-of select="@Comment"/>
        <INPUT type="hidden" id="txtComments"  name="Comment" >
          <xsl:attribute name="value"><xsl:value-of select="@Comment"/></xsl:attribute>
        </INPUT>
			</xsl:when>
			<xsl:otherwise> -->
		      <textarea id="txtComments" name="Comment" rows="3" onkeypress="txtComments_onkeypress(this, event); SetDirty()" style="width:100%">
		  	    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Reinspect']/Column[@Name = 'Comment']/@MaxLength"/></xsl:attribute>
		  	    <xsl:value-of select="@Comment"/>
		      </textarea>
			<!-- </xsl:otherwise>
		  </xsl:choose> -->
		</td>
      </tr>
	</table>
  </td>
</tr>

</table>  
</xsl:template>



<xsl:template name="ReinspectDetail">
  <xsl:param name="FormLocked"/>

  <tr unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this)">
    <xsl:attribute name="onclick">OnGridSelect(this)</xsl:attribute>
    <xsl:attribute name="bgColor">
	  <xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/>
	</xsl:attribute>
    <td unselectable="on" width="58" class="GridTypeTD">
	  <xsl:attribute name="ReinspectID">
	  	<xsl:value-of select="@ReinspectID"/>
	  </xsl:attribute>
	  <xsl:attribute name="SysLastUpdatedDate">
	  	<xsl:value-of select="@SysLastUpdatedDate"/>
	  </xsl:attribute>
	  <xsl:attribute name="DetailNumber">
	  	<xsl:value-of select="@DetailNumber"/>
	  </xsl:attribute>
	  
	  <xsl:value-of select="position()"/>
    </td>
	<td unselectable="on" width="58" class="GridTypeTD">
	  <xsl:choose>
	  	<xsl:when test="@ReinspectExceptionID != ''">
      		<xsl:value-of select="@ReinspectExceptionID"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </td>
    <td unselectable="on" width="58" class="GridTypeTD">
	  <xsl:choose>
	  	<xsl:when test="@EstimateDetailNumber != '0'">
      		<xsl:value-of select="@EstimateDetailNumber"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </td>
    <td unselectable="on" width="100%" class="GridTypeTD" style="text-align:left">
      <xsl:choose>
	  	<xsl:when test="@Comment != ''">
      		<xsl:value-of select="@Comment"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </td>
    <td unselectable="on" width="78" class="GridTypeTD" style="text-align:right">
      <xsl:choose>
	  	<xsl:when test="@EstimateAmt != ''">
      		<xsl:value-of select="@EstimateAmt"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </td>
    <td unselectable="on" width="79" class="GridTypeTD" style="text-align:right">
      <xsl:choose>
	  	<xsl:when test="@ReinspectAmt != ''">
      		<xsl:value-of select="@ReinspectAmt"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
	</td>
    <td unselectable="on" width="79" class="GridTypeTD" style="text-align:right">
	  <xsl:choose>
	  	<xsl:when test="@Difference != ''">
      		<xsl:value-of select="@Difference"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		</xsl:otherwise>
	  </xsl:choose>
    </td> 
  </tr> 
</xsl:template>
  
<xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
		
		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>
		
		<xsl:value-of select="@Name"/>	
	</option>
</xsl:template>
  
</xsl:stylesheet>

