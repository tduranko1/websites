<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTWebSignupsSearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTWebSignupGetListXML,SMTWebSignupSearchResults.xsl, '', '', 'FL', '', '', '', '', '', '', ''   -->

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsCount = '<xsl:value-of select="count(Shop)"/>';

<![CDATA[

function initPage(){
	parent.ShowCount(gsCount);
	divResultList.scrollTop = top.gsScroll;	
}

function GridSelect(oRow){
  top.gsScroll = divResultList.scrollTop;
  sShopLoadID = oRow.cells[0].innerText;
  parent.window.navigate("SMTWebSignupInfo.asp?ShopLoadID=" + sShopLoadID);
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" width="52" nowrap="nowrap" style="height:20; cursor:default;"> ID </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="258" nowrap="nowrap" style="cursor:default;"> Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="246" nowrap="nowrap" style="cursor:default;"> Address </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="136" nowrap="nowrap" style="cursor:default;"> City </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="47" style="cursor:default;"> Zip </TD>
    </TR>
  </TBODY>
</TABLE>

<DIV unselectable="on" id="divResultList" class="autoflowTable" style="height:365px;">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <xsl:for-each select="Shop"><xsl:call-template name="SearchResults"/></xsl:for-each>
    </TBODY>
  </TABLE>
</DIV>
</DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the search results -->
  <xsl:template name="SearchResults">
    <TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" onclick="GridSelect(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD unselectable="on" class="GridTypeTD" width="50" style="text-align:left">
        <xsl:choose>
			    <xsl:when test = "@ShopLoadID != ''"><xsl:value-of select="@ShopLoadID"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>				
      </TD>
      <TD unselectable="on" width="257" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
			    <xsl:when test = "@Name != ''"><xsl:value-of select="@Name"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="245" style="text-align:left">
	      <xsl:choose>
			    <xsl:when test = "@Address1 != ''"><xsl:value-of select="@Address1"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
      <TD unselectable="on" width="135" class="GridTypeTD" style="text-align:left">
	      <xsl:choose>
			    <xsl:when test = "@AddressCity != ''"><xsl:value-of select="@AddressCity"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
        <xsl:if test="@AddressCity != '' and normalize-space(@AddressState) != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
        <xsl:choose>
			    <xsl:when test = "normalize-space(@AddressState) != ''"><xsl:value-of select="@AddressState"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
      <TD unselectable="on" width="45" class="GridTypeTD">
        <xsl:choose>
			    <xsl:when test = "@AddressZip != ''"><xsl:value-of select="@AddressZip"/></xsl:when>
			    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		    </xsl:choose>
      </TD>
	  </TR>
  </xsl:template>

</xsl:stylesheet>