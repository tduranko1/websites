<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
	id="SMTShopOEMDiscount">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="ShopID"/>
<xsl:param name="mode"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopOEMDiscountGetDetailXML,SMTOEMDiscount.xsl,394   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->


<script language="javascript">

var gsUserID = '<xsl:value-of select="$UserId"/>';
var gsMode = '<xsl:value-of select="$mode"/>';
var gsPageFile = "SMTShopOEMDiscount.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

  function InitPage(){
  	if (gsMode != "wizard")
  		InitEditPage();
  	else
  		parent.frameLoaded(7);
  
  	if (gsShopCRUD.indexOf("U") != -1) frmDiscount.txtDomestic.focus();
  	onpasteAttachEvents();
  
  	//set dirty flags associated with all inputs and selects.
      var aInputs = null;
      var obj = null;
      aInputs = document.all.tags("INPUT");
      for (var i=0; i < aInputs.length; i++) {
        if (aInputs[i].type=="text"){
  			  aInputs[i].attachEvent("onchange", SetDirty);
  			  aInputs[i].attachEvent("onkeypress", SetDirty);
          aInputs[i].attachEvent("onpropertychange", SetDirty);
        }
        if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
      }
  }
  
  function InitEditPage(){
  	top.gsPageFile = gsPageFile;
  	parent.btnDelete.style.visibility = "hidden";
  
    if (gsShopCRUD.indexOf("U") > -1){
      parent.btnSave.style.visibility = "visible";
      parent.btnSave.disabled = false;
    }
    else
  	  parent.btnSave.style.visibility = "hidden";
  
  	parent.tab26.className = "tabactive1";
  	parent.gsPageName = "Shop OEM Discount";
  }
  
  function CheckPct(event){
  	var obj = eval("document.all." + event.srcElement.id);
  	var idx = obj.value.indexOf(".")
  
  	if (event.keyCode == 45){				// no negatives
  		event.returnValue = false;
  		return;
  	}
  
  	if (idx > -1){
  		if (obj.value.length - idx > 2){	// allow only two decimal places
  			event.returnValue = false;
  			return;
  		}
  
  		if (event.keyCode == 46){			// allow only one decimal point
  			event.returnValue = false;
  			return;
  		}
  	}
  }
  
  function CheckDiscount(event){
  	var obj = eval("document.all." + event.srcElement.id);
  	if (obj.value > 100){
  		parent.ClientWarning ("Percentage must be 100 or less.");
  		event.returnValue = false;
  		obj.select();
  		return;
  	}
  }
  
  function btnFill_onclick(event){
      var domDisc = document.all.txtDomestic.value;
      var impDisc = document.all.txtImport.value;
  	var oRows = document.all.tblDiscount.rows;
  	var oPct;
  
  	if (domDisc > 100){
  		ClientWarning ("Percentage must be 100 or less.");
  		event.returnValue = false;
  		document.all.txtDomestic.select();
  		return;
  	}
  
  	if (impDisc > 100){
  		ClientWarning ("Percentage must be 100 or less.");
  		event.returnValue = false;
  		document.all.txtImport.select();
  		return;
  	}
  
  	for(i = 0; i < oRows.length; i++){
  		oPct = eval("document.all.txtDiscountPct" + i);
  		if (oRows[i].cells[0].innerText == "Y"){
  			if (domDisc != "")
  				oPct.value = domDisc;
  		}
  		else{
  			if (impDisc != "")
  				oPct.value = impDisc;
  		}
  	}
  }
  
  function btnClear_onclick(){
  	for(i = 0; i < document.all.tblDiscount.rows.length; i++){
  		eval("document.all.txtDiscountPct" + i).value = "";
  	}
  	document.all.txtDomestic.value = "";
  	document.all.txtImport.value = "";
  }
  
  function IsDirty(){
  	Save();
  }
  
  function Save(){
  	//validate
    var elms = document.all.tags("INPUT");
  	for(var i = 0; i < elms.length; i++){
  		if (elms[i].type == "text")
  			if (isNaN(elms[i].value)){
                  parent.ClientWarning("Invalid Numeric value specified. Please try again.");
                  elms[i].select();
                  elms[i].focus();
                  return false;
        }
  	}
        
    parent.btnSave.disabled = true;
  	frmDiscount.action += "?mode=update&ShopID=" + top.gsShopID;
  	frmDiscount.submit();
  	parent.gbDirtyFlag = false;
    
  }
    
  function SetDirty(){
  	parent.gbDirtyFlag = true;
  }
  
  function BuildParams(){
  	var sParams = "";
  	var oPct;
  	var oRows = document.all.tblDiscount.rows;
  
  	for(i = 0; i < oRows.length; i++){
  		oPct = eval("document.all.txtDiscountPct" + i).value;
  
  		if (oPct != ""){
  			sParams += eval("document.all.txtMakeID" + i).value + "-" + oPct + "|";
  		}
  	}
  
  	//return sParams != "" ? "'" + sParams + "'," : "DEFAULT,";
  	return "OEMDiscount=" + sParams + "&";
  }

]]>

</script>

</HEAD>

<body class="bodyAPDsub" onload="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

<div style="background-color:#FFFAEB; height:475px;">


<form id="frmDiscount" method="post" style="overflow:hidden">

  <div id="divDiscountHeaders" style="position:absolute;width:350px;height:25px;top:30px;left:170px;">
  <table  cellpadding="2" cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" >
     <tr>
       <td width="70px" class="TableSortHeader" style="cursor:default">Domestic</td>
       <td width="200px" class="TableSortHeader" style="cursor:default">Make</td>
       <td width="70px" class="TableSortHeader" style="cursor:default">Discount %</td>
   </tr>
  </table>
</div>

<div id="divDiscount" border="2" class="autoflowdiv" style="position:absolute;height:380px;width:366px;top:46px;left:170px;">
  <table id="tblDiscount" cellpadding="2" cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" WIDTH="100%" nowrap="on">
    <xsl:apply-templates select="Make"/>
  </table>
</div>

<div id="divPageControls" style="position:absolute;width:145px;top:60px;left:565;">
  <table border="0">
    <tr>
      <td colspan="2" align="center"><b>Discount</b></td>
    </tr>
    <tr>
      <td align="center">Domestic</td>
      <td align="center">Import</td>
    </tr>
    <tr>
      <td>
        <input type="text" id="txtDomestic" size="7" maxlength="6" class="inputFld" style="text-align:right" onkeyup="CheckDiscount(event)" onkeypress="NumbersOnly(event);CheckPct(event)"/>
      </td>
      <td>
        <input type="text" id="txtImport" size="7" maxlength="6" class="inputFld" style="text-align:right" onkeyup="CheckDiscount(event)" onkeypress="NumbersOnly(event);CheckPct(event)"/>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="button" id="btnFill" name="btnFill" class="formButton" value="Fill Discount" onclick="btnFill_onclick(event)"/>
      </td>
    </tr>
    <tr>
      <td><IMG src="/images/spacer.gif" width="6" height="12" border="0" /></td> <!-- spacer row -->
    </tr>
    <tr>
      <td colspan="2" style="width:100px">
        <input type="button" id="btnClear" name="btnClear" class="formButton" value="Clear All" onclick="btnClear_onclick()"/>
      </td>
    </tr>
  </table>

</div>

  <input type="hidden" id="txtShopID" name="ShopID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
    <xsl:attribute name="value"><xsl:value-of select="$UserId"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="Billing/@SysLastUpdatedDate"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtCallBack" name="CallBack"/>
</form>
</div>

<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</body>
</HTML>
</xsl:template>

<xsl:template match="Make">
  <tr>
    <!-- alternating grid colors -->
    <xsl:attribute name="bgcolor">
  	  <xsl:choose>
    		<xsl:when test="position() mod 2 = 1"><xsl:text>#fff7e5</xsl:text></xsl:when>
    		<xsl:otherwise>white</xsl:otherwise>
  	  </xsl:choose>
  	</xsl:attribute>

	<!-- grid data -->
    <td width="70px" align="center">
      <xsl:choose>
        <xsl:when test="@DomesticFlag=1">Y</xsl:when>							                   <!-- DomesticFlag       -->
        <xsl:otherwise>N</xsl:otherwise>
      </xsl:choose>
    </td>
    <td width="200px">
      <input type="hidden" name="VehicleMakeID">
        <xsl:attribute name="id">txtMakeID<xsl:value-of select="position() - 1"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@VehicleMakeID"/></xsl:attribute>         <!-- VehicleMakeID      -->
      </input>
      <input type="hidden" name="SysLastUpdatedDate">
        <xsl:attribute name="id">txtSysLastUpdatedDate<xsl:value-of select="position() - 1"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute>    <!-- SysLastUpdatedDate -->
      </input>

	  <xsl:value-of select="@Name"/>								                                <!-- Name               -->
    </td>
    <td width="70px" align="center">
      <input type="text" name="DiscountPct" size="7" class="inputFld" maxlength="6" style="text-align:right" onblur="CheckDiscount(event)" onkeypress="NumbersOnly(event);CheckPct(event)"  onbeforepaste="onBeforePasteNumbersOnly()" onpaste="onPasteNumbersOnly()"  ondrag="event.returnValue=false;">
        <xsl:attribute name="id">txtDiscountPct<xsl:value-of select="position() - 1"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@DiscountPct"/></xsl:attribute>           <!-- DiscountPct        -->
      </input>
    </td>
  </tr>

</xsl:template>

</xsl:stylesheet>
