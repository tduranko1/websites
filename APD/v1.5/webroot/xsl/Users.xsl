<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="UserProfileCRUD" select="User Profile"/>
<xsl:param name="UserPermissionCRUD" select="User Permission"/>


<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="(substring($UserProfileCRUD, 2, 1) = 'R') or ((/Root/@UserID = '-1') and (substring($UserProfileCRUD, 1, 1) = 'C'))">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="UserProfileCRUD" select="$UserProfileCRUD"/>
              <xsl:with-param name="UserPermissionCRUD" select="$UserPermissionCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to view user information. 
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserProfileCRUD"/>
  <xsl:param name="UserPermissionCRUD"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,32);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var sCRUDUserProfile = "<xsl:value-of select="$UserProfileCRUD"/>";
	var sCRUDUserPermission = "<xsl:value-of select="$UserPermissionCRUD"/>";
<![CDATA[
	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

  var curRow = null;
  var bInsertMode = false;
  // Page Initialize
  function PageInit() { 
    resizePage();
    var sURL = window.location.href;
    var preSelUserID = "";
    if (sURL.indexOf("?") != -1) {
      var sQueryString = sURL.substring(sURL.indexOf("?") + 1, sURL.length);
      var aQueryString = sQueryString.split("&");
      var iLength = aQueryString.length;
      for (var i = 0; i < iLength; i++) {
        if (aQueryString[i].split("=")[0].toLowerCase() == "sel") {
          preSelUserID = aQueryString[i].split("=")[1];
          break;
        }
      }
      if(preSelUserID != "" && !isNaN(parseInt(preSelUserID)))
        preselect(preSelUserID);
    }
    setTimeout("tdUserName.fireEvent('onclick')", 100);
    
  }
  
  function preselect(preSelUserID){
    if (parseInt(preSelUserID) > 0) {
      if (tblSort2) {
        var iLength = tblSort2.rows.length;
        for (var i = 0; i < iLength; i++) {
          if (tblSort2.rows[i].cells[7].innerText == preSelUserID) {
            tblSort2.rows[i].scrollIntoView(false);
            UserGridSelect(tblSort2.rows[i]);
            break;
          }
        }
      }
    }
  }

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }

  function UserGridSelect(oRow)
  {
    try {
      if (curRow === oRow) return;

      if (curRow) {
        if (typeof(ifrmUsrDetail.isDirty) == "function") {
          if (ifrmUsrDetail.isDirty()) {
              var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
              if (sSave == "Yes"){
                  if (!btnSave()) return true;
              }
          }
        }
      }
      
      if (curRow)
        curRow.style.backgroundColor = "#FFFFFF";
        
  	  var strUserID = oRow.cells[7].innerText;
      ifrmUsrDetail.frameElement.src = "UserDetail.asp?UserID=" + strUserID;
      
      curRow = oRow;
      curRow.style.backgroundColor = "#FFD700";
      saveBG = curRow.style.backgroundColor;
    	//document.location="UserDetail.asp?UserID=" + strUserID;
    }
    catch(e) {
      handleJSError("UserGridSelect", e)
    }
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
	// Handle click of Add button
	function ADS_Pressed(sAction, sName)
	{
    try {
  		//if (inADS_Pressed == true) return;
  		//inADS_Pressed = true;

  		var oDiv = document.getElementById(sName);

  		if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		//inADS_Pressed = false;
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}

  function btnAdd(){
    if (curRow) {
      if (ifrmUsrDetail.isDirty()) {
          var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
          if (sSave == "Yes"){
              if (!btnSave()) return true;
          }
      }
      saveBG = "";
      GridMouseOut(curRow);
      curRow = null;
    }
    ifrmUsrDetail.frameElement.src = "UserDetail.asp?UserID=New";
    bInsertMode = true;
  }
  
  function btnSave(){
    /*if (curRow) {
      if (typeof(ifrmUsrDetail.ADS_Pressed) == "function")
        ifrmUsrDetail.ADS_Pressed("Update", "Users");
    }
    else if (bInsertMode) {
      if (typeof(ifrmUsrDetail.ADS_Pressed) == "function")
        ifrmUsrDetail.ADS_Pressed("Update", "Users");
      bInsertMode = false;
    }*/
    if (typeof(ifrmUsrDetail.ADS_Pressed) == "function")
      ifrmUsrDetail.ADS_Pressed("Update", "Users");
    return true;
  }
  
  function resizePage(){
    var ifrmHeight = ifrmUsrDetail.frameElement.offsetHeight;
    var iClientHeight = document.body.offsetHeight;
    //window.status = iClientHeight - ifrmHeight;
    tblSort2.parentElement.style.height = iClientHeight - ifrmHeight - 55;
    crudDivUsers.style.top = ifrmUsrDetail.frameElement.parentElement.offsetTop;
  }
  
  if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
      document.onclick = top.hideAllMenuScriptlets;
  
]]>
</SCRIPT>

<title>APD User and Role Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="InitFields(); PageInit()" style="background-color:#FFFFFF;margin:0px;padding:0px;width:100%;height:100%;overflow:hidden" tabIndex="-1" onresize="resizePage()">

  <table cellspacing="0" cellpadding="0" border="0" style="height:100%;width:100%">
    <tr valign="top">
      <td>
        <DIV id="Users" name="Users" style="width:100%;">
            <xsl:attribute name="CRUD"><xsl:value-of select="$UserProfileCRUD"/></xsl:attribute>
            <span >
                <TABLE unselectable="on" onClick="sortColumn(event, 'tblSort2')" class="ClaimMiscInputs" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                  <colgroup>
                      <col width="96px"/>
                      <col width="*"/>
                      <col width="64px"/>
                      <col width="160px"/>
                      <col width="175px"/>
                      <col width="175px"/>
                      <col width="100px"/>
                  </colgroup>
                  <TBODY>
                		<tr class="QueueHeader" style="height:21px">
                 			<td class="TableSortHeader" type="String">Csr Number</td>
                 			<td name="tdUserName" id="tdUserName" class="TableSortHeader" type="CaseInsensitiveString">User Name</td>
                 			<td class="TableSortHeader" type="String">Active</td>
                 			<td class="TableSortHeader" type="String">Active/Inactive Since</td>
                 			<td class="TableSortHeader" type="String">Assignment Pool</td>
                 			<td class="TableSortHeader" type="String">Primary Role</td>
                 			<td class="TableSortHeader" type="String">Supervisor</td>
                		</tr>
                  </TBODY>
                </TABLE>
            </span>
            <DIV unselectable="on" class="autoflowTable" style="width:100%; height: 267px;" id="grid1" name="grid1">
                <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;">
                  <colgroup>
                      <col width="94px"/>
                      <col width="*"/>
                      <col width="64px"/>
                      <col width="159px"/>
                      <col width="174px"/>
                      <col width="174px"/>
                      <col width="80px"/>
                  </colgroup>
                  <TBODY> <!--  bgColor1="ffffff" bgColor2="fff7e5" -->
                    <xsl:for-each select="/Root/User">
                      <xsl:if test="count(Application[@ApplicationID=$APDAppID]) &gt; 0">
                        <TR onClick='UserGridSelect(this)' onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' style='height:21px;'> 
                            <!-- <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute> -->
                            <td align="center" style="border:1px ridge;"><xsl:value-of select="Application[@ApplicationID=$APDAppID]/@LogonId"/></td>
                            <td style="border:1px ridge;"><xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/></td>
                            <td align="center" style="border:1px ridge;">
                                <xsl:choose>
                                    <xsl:when test="Application[@ApplicationID=$APDAppID]/@AccessEndDate = ''">
                                        <xsl:text>Yes</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>No</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td align="center" style="border:1px ridge;">
                                <xsl:call-template name="formatDateTime">
                                    <xsl:with-param name="sDateTime">
                                        <xsl:value-of select="Application[@ApplicationID=$APDAppID]/@AccessBeginDate"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </td>
                            <td style="border:1px ridge;">
                                <xsl:variable name="AssignmentPoolList">
                                  <xsl:for-each select="AssignmentPool">
                                    <xsl:value-of select="@Name"/>
                                    <xsl:if test="position() != last()">, </xsl:if>
                                  </xsl:for-each>
                                </xsl:variable>
                                <xsl:if test="string-length($AssignmentPoolList) &gt; 30">
                                  <xsl:attribute name="Title">
                                    <xsl:value-of select="$AssignmentPoolList"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="Style">
                                    <xsl:text>cursor:help</xsl:text>
                                  </xsl:attribute>
                                </xsl:if>
                                <xsl:choose>
                                  <xsl:when test="$AssignmentPoolList = ''">
                                    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                                  </xsl:when>
                                  <xsl:when test="string-length($AssignmentPoolList) &gt; 30">
                                    <xsl:value-of select="substring($AssignmentPoolList, 1, 30)"/><xsl:text>...</xsl:text>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="$AssignmentPoolList"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td style="border:1px ridge;">
                              <xsl:value-of select="@PrimaryRoleName"/>
                            </td>
                            <td align="center" style="border:1px ridge;">
                                <xsl:choose>
                                    <xsl:when test="@SupervisorFlag = 1">
                                        <xsl:text>Yes</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>No</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td style="display:none"><xsl:value-of select="@UserID"/></td>
                        </TR>
                      </xsl:if>
                    </xsl:for-each>
                  </TBODY>
                </TABLE>
            </DIV>
        </DIV>
        <br/>
      </td>
    </tr>
    <tr style="height:275px;" valign="top">
      <td>
        <div align="right" unselectable="on" id="crudDivUsers" name="crudDivUsers" style="position:absolute;top:0;left:0;width:98%;margin-right:20px;margin-top:2px;">
          <xsl:if test="(substring($UserProfileCRUD, 1, 1) = 'C')">
          <img name="btnAdd1" id="btnAdd1" src="/images/but_ADD_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_ADD_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_ADD_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_ADD_down.png'" onclick="btnAdd()" style="cursor:hand"/>
          </xsl:if>
          <img src="/images/spacer.gif" width="3px" height="1px"/>
          <xsl:if test="(substring($UserProfileCRUD, 2, 1) = 'R') or (substring($UserPermissionCRUD, 3, 1) = 'U')">
          <img name="btnSave1" id="btnSave1" src="/images/but_SAVE_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_SAVE_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_SAVE_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_SAVE_down.png'" onclick="btnSave()" style="cursor:hand"/>
          </xsl:if>
        </div>
        <iframe name="ifrmUsrDetail" id="ifrmUsrDetail" src="/blank.asp" style="width:100%;height:100%;background:transparent;" allowtransparency="true"/>
      </td>
    </tr>
  </table>
</BODY>
</html>
</xsl:template>

<xsl:template name="formatDateTime">
    <xsl:param name="sDateTime"/>

    <xsl:variable name="sYear" select="substring($sDateTime, 1,4)"/>
    <xsl:variable name="sMonth" select="substring($sDateTime, 6,2)"/>
    <xsl:variable name="sDay" select="substring($sDateTime, 9,2)"/>
    
    <xsl:value-of select="concat($sMonth, '/', $sDay, '/', $sYear)"/>
    
    <xsl:variable name="sHH1" select="substring($sDateTime, 12,2)"/>
    <xsl:if test="number($sHH1) &gt; 12">
        <xsl:variable name="sHH" select="format-number(number($sHH1) - 12, '00')"/>
        <xsl:variable name="sAMPM" select="PM"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ' PM')"/>
        <!-- <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' PM')"/> -->
    </xsl:if>
    <xsl:if test="number($sHH1) &lt; 13">
        <xsl:variable name="sHH" select="$sHH1"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ' AM')"/>
        <!-- <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' AM')"/> -->
    </xsl:if>
</xsl:template>
</xsl:stylesheet>
