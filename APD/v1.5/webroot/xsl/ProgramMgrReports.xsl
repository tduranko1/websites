<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimBilling">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<!-- NONE -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="View Reports"/>
<!-- <xsl:param name="CRUDInfo">CRUD</xsl:param> -->

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R')">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
          <tr>
            <td align="center">
              <font color="#ff0000"><strong>You do not have sufficient permission to view the client reports.
              <br/>Please contact administrator for permissions.</strong></font>
            </td>
          </tr>
        </table>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="LynxId"/>
<xsl:param name="ClaimStatus"/>
<xsl:param name="InfoCRUD"/>
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimBillingGetDetailXML,ClaimBilling.xsl, 6790   -->
<HEAD>

<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  var bReportLoaded = false;
  var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
  var sReportName = "";
  var sState = "";
  var sDate = "";
  var sInsName = "";
  var sReportFriendlyName = "";
  var sShowAllClients = "1"; // 1 = Show All clients; 0 = Do not show all clients
  var iExtBrowser = null;
    
  <![CDATA[
    function initPage(){
      try {
        window.status = "";
        stat1.style.top = (document.body.offsetHeight - 75) / 2;
        stat1.style.left = (document.body.offsetWidth - 240) / 2;
      } catch (e) {
        ClientError(e.description);
      }
    }

    function getCriteria(){
      try {
        if (csReports.selectedIndex == -1) {
          ClientWarning("Please select the report you would like to view.");
          csReports.setFocus();
          return;
        }
        
        ifrmReportCriteria.frameElement.style.display = "none";
        stat1.Show("Loading report criteria. Please wait...");
        bReportLoaded = true;
        frmGetReportCriteria.reportID.value = csReports.value;
        //frmGetReportCriteria.InsuranceCompanyID.value = (csInsurance.selectedIndex != -1 ? csInsurance.value : "");
        frmGetReportCriteria.submit();
        //window.setTimeout("getCriteria2()", 100);
      } catch(e) {
        ClientError(e.description);
      }
    }

    function reportReady(){
      try {
        if (bReportLoaded) {
          if (ifrmReportCriteria.frameElement.readyState == "complete") {
            ifrmReportCriteria.frameElement.style.display = "inline";
            stat1.Hide();
          }
        }
      } catch (e) {
        ClientError(e.description)
      }
    }
	
    if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
 
  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:#FFFFFF; border:0px; overflow:hidden;margin:2px;padding:0px;" onLoad="initPage()" tabIndex="-1">
  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;width:100%">
      <colgroup>
        <col width="110px"/>
        <col width="*"/>
      </colgroup>
    <tr>
      <td colspan="2" style="font-size:18px;">Program Manager Reports</td>
    </tr>
    <tr>
      <td colspan="2"><img src="/images/background_top.gif" height="2px"/></td>
    </tr>
    <tr>
      <td>Report Name:</td>
      <td>
        <IE:APDCustomSelect id="csReports" name="csReports" displayCount="7" canDirty="false" CCTabIndex="1" onChange="getCriteria()">
          <xsl:for-each select="Report">
            <xsl:sort select="@DisplayOrder" data-type="number" order="ascending"/> 
            <IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ClientReportID"/></xsl:attribute>
            <xsl:value-of select="@Description"/>
          </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
  </table>
  <br/>
  <IFRAME id="ifrmReportCriteria" name="ifrmReportCriteria" src="/blank.asp" onreadyStateChange="reportReady()" style="position:absolute;top:55px;width:100%; height:545px; border:0px solid #000000; position:absolute; background:transparent; overflow:hidden;" allowtransparency="true" >
  </IFRAME>

  <xml id="xmlReports" name="xmlReports">
    <xsl:copy-of select="/"/>
  </xml>
  <form name="frmGetReportCriteria" id="frmGetReportCriteria" method="post" target="ifrmReportCriteria" action="/reports/ReportCriteria.asp">
    <input type="hidden" name="reportID" id="reportID"/>
  </form>
  <IE:APDStatus id="stat1" name="stat1" width="250" height="75" />
</BODY>
</HTML>

</xsl:template>
</xsl:stylesheet>
