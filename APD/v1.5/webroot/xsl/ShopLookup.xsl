<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ShopLookup">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="ZipCode"/>
<xsl:param name="City"/>
<xsl:param name="State"/>
<xsl:param name="UserID"/>
<xsl:param name="LynxID"/>
<xsl:param name="VehicleNumber"/>
<xsl:param name="InsuranceCompanyID"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="LocationName"/>
<xsl:param name="ShopRemarks"/>
<xsl:param name="AssignmentTo"/>
<xsl:param name="MEChildWin"/>
<xsl:param name="getUser"/>
<xsl:param name="ClaimAspectServiceChannelID"/>
<xsl:param name="Warranty"/>

<xsl:template match="/Root">
  
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspSMTShopSearchReferenceGetListXML,ShopLookup.xsl, 6335   -->
<HEAD>

<TITLE>Shop Lookup</TITLE>

<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
  
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
  
<SCRIPT language="JavaScript">
  
  var strZipCode = "<xsl:value-of select="$ZipCode"/>";
  var strCity = unescape("<xsl:value-of select="js:cleanString($City)"/>");
  var strState = unescape("<xsl:value-of select="js:cleanString($State)"/>");
  var strUserID = "<xsl:value-of select="$UserID"/>";
  var strLynxID = "<xsl:value-of select="$LynxID"/>";
  var strVehicleNumber = "<xsl:value-of select="$VehicleNumber"/>";
  var strInsuranceCompanyID = "<xsl:value-of select="$InsuranceCompanyID"/>";
  var strClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
  var strLocationName = "<xsl:value-of select="js:cleanString(string($LocationName))"/>";
  var strShopRemarks = "<xsl:value-of select="js:cleanString(string($ShopRemarks))"/>";
  var inDialogView = false;
  var strAssignmentTo = "<xsl:value-of select="$AssignmentTo"/>";
  var strMEChildWin = "<xsl:value-of select="$MEChildWin"/>";
  var strGetUser = "<xsl:value-of select="$getUser"/>";
  var strClaimAspectServiceChannelID = "<xsl:value-of select="$ClaimAspectServiceChannelID"/>";
  var strWarranty = "<xsl:value-of select="$Warranty"/>";

  <![CDATA[
    function onClosePage(){
  if (window.opener != null && !window.opener.closed) {
                if (typeof window.opener.HideModalDiv == 'function' || typeof window.opener.HideModalDiv == 'object') {
                    window.opener.HideModalDiv();
					window.opener.location.href = window.opener.location.href;
                }
            }
  }
  function LoadModalDiv(){
  if (window.opener != null && !window.opener.closed) {
                if (typeof window.opener.LoadModalDiv == 'function' || typeof window.opener.HideModalDiv == 'object') {
                    window.opener.LoadModalDiv();
                }
            }
  }
  
  function initPage()
  {
   LoadModalDiv();
    txtZipCode.value =   strZipCode.replace(/\s*$/, "");
    txtShopCity.value =  strCity.replace(/\s*$/, "");
    csShopState.value = strState;
    txtShopName.value = strLocationName.replace(/\s*$/, "");
    txtLynxID.innerText = strLynxID;
    
    strShopRemarks = escape(remTA.value);
	// alert(top.location);
	// alert(self.location);
	// alert(top.frames.length);
	// alert(top.frames[0].location);

    if (window.top == window.self) 
	 { 
	   document.frames["ifrmShopList"].frameElement.style.height="440px";
     document.frames["ifrmShopList"].frameElement.style.width="730px";
     inDialogView = true;
	   btnClose.style.visibility = 'hidden';
	 }
	 else
	 {
	   document.frames["ifrmShopList"].frameElement.style.height="218px";
     document.frames["ifrmShopList"].frameElement.style.width="710px";
     inDialogView = false;
	 }  
	
    if (typeof(parent.CancelAssignedShop) != "function" && strMEChildWin != "true" && strGetUser != "false")
    {
      GetUserDetails();
    }
  
    if (strMEChildWin == "true")
    {
      csShopType.selectedIndex = 1;
      csShopType.CCDisabled = true;
    }

    if (txtZipCode.value != "")
    {
      callShopSearchByDistance();
    }
    else
    {
      errormsg.innerHTML = "&curren;&curren;&curren; Invalid Search &curren;&curren;&curren; &nbsp; &nbsp; A ZIP code is requred to conduct a ZIP code search.";
    }
    if (stat1){
        stat1.style.top = (document.body.offsetHeight - 75) / 2;
        stat1.style.left = (document.body.offsetWidth - 240) / 2;      
    }
  }
  
  function GetUserDetails()
  {
    
    var co = RSExecute("/rs/RSUserDetails.asp", "GetUserDetails", strLynxID);
    if (co.status == 0)
    {
      var sTblData = co.return_value;
      var listArray = sTblData.split("||");
      listArray = listArray[0].split(",");
      //strUserID = listArray[0];
      txtCSRName.innerText = listArray[1]+" "+listArray[2];
      var sCSRPhone = listArray[3]+"-"+listArray[4]+"-"+listArray[5];
      if (sCSRPhone.indexOf("null") == -1)
        txtCSRPhone.innerText = sCSRPhone;
    }
    else ServerEvent();
  }
  
  function Search(){
    strLocationName = txtShopName.value.replace(/\s*$/, "");
    strCity = txtShopCity.value.replace(/\s*$/, "");
    strState = csShopState.selectedIndex > -1 ? csShopState.value : "";
    
    if (rbDistanceSearch.value == 2)
     callShopSearchByDistance();
    else
      callShopSearchByName();
  }
  
  
  function callShopSearchByDistance()
  {   
    var strRetVal;
    
    errormsg.innerHTML = "Searching...  One moment, please.";
    clearResultsDiv();
   
    if (txtZipCode.value == "") { 
      if (strCity == "" || csShopState.value == ""){
        errormsg.innerHTML = "&curren; &curren; &curren; Invalid Search &curren;&curren;&curren; &nbsp; &nbsp; A ZIP Code or a City and State is required to conduct a Distance code search.";
        return;
      }
      else{
        var co = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspShopZipByCityStateXML", "ShopCity=" + escape(strCity) + "&ShopState=" + csShopState.value);
      
        if (co.status == 0){    
          var sTblData = co.return_value;
          var listArray = sTblData.split("||");
          if ( listArray[1] == "0" )
            ServerEvent();
          else{
            var objXML = new ActiveXObject("Microsoft.XMLDOM");
            objXML.loadXML(listArray[0]); 
            
            var oCity = objXML.documentElement.selectNodes("/Root/City");
            if (oCity.length > 1){
              strRetVal = window.showModalDialog("CityPrimaryZip.asp?City=" +  escape(strCity) + "&State=" + csShopState.value + "&UserID=" + strUserID, "", "dialogHeight:190px; dialogWidth:400px; resizable:no; status:no; help:no; center:yes;");
              if (isNaN(strRetVal)){
                errormsg.innerHTML = "&curren; &curren; &curren; Invalid Search &curren;&curren;&curren; &nbsp; &nbsp; A ZIP Code or a City and State is required to conduct a Distance search.  No City was selected";
                return;
              }
              txtZipCode.value = strRetVal;   
            }
            else{
              txtZipCode.value = objXML.documentElement.selectSingleNode("/Root").getAttribute("PrimaryZip");
            }
          }
        }
      }
    }
    
    sPage = "ShopList.asp?ZipCode="+ txtZipCode.value +"&ShopTypeCode="+ txtShopSearchTypeCD.value +"&UserID=" + strUserID +
            "&LynxID="+ strLynxID + "&VehicleNumber="+ strVehicleNumber + "&ClaimAspectID=" + strClaimAspectID +
            "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + 
            "&InsuranceCompanyID="+ strInsuranceCompanyID +"&ShopRemarks="+ strShopRemarks +
            "&AssignmentTo=" + strAssignmentTo + "&MEChildWin=" + strMEChildWin;
    //alert(sPage);return;
    document.frames["ifrmShopList"].frameElement.src = sPage;
    document.frames["ifrmShopList"].frameElement.style.visibility="visible";
    obj = document.getElementById("txtZipCode");
    obj.focus();
    
    errormsg.innerHTML = "";
  }
  
  
  function callShopSearchByName()
  { 
    errormsg.innerHTML = "Searching...  One moment, please.";
    clearResultsDiv();    
    
    if ( strState != "")
    {
      sPage = "ShopList.asp?SrhFlag=1&ShopName="+ escape(strLocationName) +"&ShopCity="+ escape(strCity) +"&ShopState="+ strState +
              "&ShopTypeCode="+ txtAllShopSearchTypeCD.value +"&UserID="+ strUserID +"&LynxID="+ strLynxID +
              "&VehicleNumber="+ strVehicleNumber + "&ClaimAspectID=" + strClaimAspectID + "&InsuranceCompanyID="+ strInsuranceCompanyID +
              "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + 
              "&ShopRemarks="+ strShopRemarks + "&AssignmentTo=" + strAssignmentTo + "&MEChildWin=" + strMEChildWin;
      //alert(sPage);return;
      document.frames["ifrmShopList"].frameElement.src = sPage;
      document.frames["ifrmShopList"].frameElement.style.visibility="visible";
      obj = document.getElementById("txtShopName");
      obj.focus();
    }
    else
    {
      errormsg.innerHTML = "&curren;&curren;&curren; Invalid Search &curren;&curren;&curren; &nbsp; &nbsp; A state must be selected.";
      return;
    }
    errormsg.innerHTML = "";
  }
  
  
  function clearResultsDiv()
  {
    document.frames["ifrmShopList"].frameElement.src = "blank.asp";
    document.frames["ifrmShopList"].frameElement.style.visibility="hidden";
  }
  
  
  function divClose()
  {
    parent.ResizeAssignShop();
  }
  
  
  function AssignedShop(aryShopInfo)
  {
    if (window != top && typeof(parent.AssignedShop) == "function")
    {
      parent.AssignedShop(aryShopInfo);
    }
    else
    {
      ClientInfo( "Shop has been selected." );

      if (btnClose && btnClose.style.visibility != 'hidden') {
       btnClose.click();
     } else {
        window.close();
     }
    }
  }
  
    
  function csShopType_change(){
    switch(csShopType.value){
      case "":
        txtShopSearchTypeCD.value = "A";
        break;
      case "1":
        txtShopSearchTypeCD.value = "P";
        break;
      case "0":
        txtShopSearchTypeCD.value = "N";
        break;
    }  	
  }
  
  
  if (typeof(parent.ResizeAssignShop) == "function")
  {
    if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
  }

  if (typeof(parent.CancelAssignedShop) != "function")
  {
    obj = document.getElementById("tblCSRInfo");
    if (obj)
      obj.style.display = "inline";
  }

  if (typeof(parent.CancelAssignedShop) != "function")
  {
    try {
    if (btnClose && btnClose != undefined)
      btnClose.style.visibility = 'hidden';
    } catch (e) {}
  } 
  
  ]]>
  </SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;background-color:#FFFFFF;padding:0px;margin:0px;" onload="initPage()" onunload="onClosePage()" tabIndex="-1">
   <IE:APDStatus id="stat1" name="stat1" width="240" height="75" context="" />
  
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<DIV unselectable="on" id="ShopLayout" style="position:absolute; left:0px; top:0px">

<INPUT type="hidden" id="txtAllShopSearchTypeCD" value="A" />
  
<TABLE unselectable="on" width="700" border="0" cellspacing="0" cellpadding="0">
  <TR unselectable="on" id="tblCSRInfo" style="display:none">
    <TD unselectable="on">

      <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2"	style="border-bottom:#CCCCCC solid 1px;	border-left: #FFFFCC solid 1px;	border-right: #CCCCCC solid 1px;	border-top: #FFFFCC solid 1px; background-color:#FFF7E5; color:#000066;">
        <TR unselectable="on">
          <TD unselectable="on"><img src="/images/spacer.gif" width="10" height="16" border="0"/></TD>
          <TD unselectable="on" class="boldtext" nowrap="nowrap">LynxID:</TD>
          <TD unselectable="on" nowrap="nowrap"><SPAN id="txtLynxID"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN></TD>
          <TD unselectable="on"><img src="/images/spacer.gif" width="20" height="1" border="0"/></TD>
          <TD unselectable="on" class="boldtext" nowrap="nowrap">Rep.:</TD>
          <TD unselectable="on" nowrap="nowrap"><SPAN id="txtCSRName"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN></TD>
          <TD unselectable="on"><img src="/images/spacer.gif" width="20" height="1" border="0"/></TD>
          <TD unselectable="on" class="boldtext" nowrap="nowrap">Phone:</TD>
          <TD unselectable="on" nowrap="nowrap"><SPAN id="txtCSRPhone"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN></TD>
          <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
      </TABLE>

    </TD>
  </TR>

  <TR unselectable="on">
    <TD unselectable="on">
    <!-- <img src="/images/spacer.gif" width="1" height="6" border="0"/><br/> -->

      <TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="1">
        <colgroup>
          <col width="60" align="right"/>
          <col width="300"/>
          <col width="160"/>
          <col width="*"/>
        </colgroup>
        <TR unselectable="on">
          <TD unselectable="on" nowrap="nowrap">Shop Name:</TD>
          <TD unselectable="on">
            <IE:APDInput id="txtShopName" name="txtShopName" canDirty="false" maxlength="50" size="50" CCTabIndex="1"></IE:APDInput>
          </TD>
          <TD unselectable="on" nowrap="nowrap">
             <IE:APDCustomSelect id="csShopType" name="csShopType" displayCount="3" width="150" canDirty="false" blankFirst="false" CCTabIndex="2" onChange="csShopType_change()" value="1">
              <xsl:for-each select="Reference[@ListName='ShopType']">
                <IE:dropDownItem style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                </IE:dropDownItem>
              </xsl:for-each>
            </IE:APDCustomSelect>
            <INPUT type="hidden" id="txtShopSearchTypeCD" value="P" style="display:none"/>
          </TD>
          <TD unselectable="on" nowrap="nowrap">
             Search By:
            <IE:APDRadioGroup id="rbSearchGrp" name="rbSearchGrp" canDirty="false">
              <!-- <span onclick="rbNameSearch.value=1;rbDistanceSearch.value=0;" onmouseover="this.style.cursor='hand'" onmouseout="this.style.cursor='normal'"> -->
                <IE:APDRadio id="rbNameSearch" name="rbNameSearch" caption="Name" value="0" canDirty="false" groupName="rbSearchGrp" checkedValue="1" uncheckedValue="0" CCTabIndex="6"/>
                <!-- <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Name
              </span> -->
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <!-- <span onclick="rbNameSearch.value=0;rbDistanceSearch.value=2;" onmouseover="this.style.cursor='hand'" onmouseout="this.style.cursor='normal'">   -->
                <IE:APDRadio id="rbDistanceSearch" name="rbDistanceSearch" caption="Distance" value="2" canDirty="false" groupName="rbSearchGrp" checkedValue="2" uncheckedValue="0" CCTabIndex="6"/>
                <!-- <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Distance
              </span> -->
            </IE:APDRadioGroup>
          </TD>
        </TR>
        <TR unselectable="on">
          <TD unselectable="on" nowrap="nowrap">City:</TD>
          <TD unselectable="on" colspan="2">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td style="padding-right:8px;">
                  <IE:APDInput id="txtShopCity" name="txtShopCity" canDirty="false" maxlength="50" size="20" CCTabIndex="3"></IE:APDInput>
                </td>
                <td style="padding-right:3px;">State:</td>
                <td style="padding-right:8px;">
                  <IE:APDCustomSelect id="csShopState" name="csShopState" canDirty="false" displayCount="10" width="130" blankFirst="true" CCTabIndex="4">
                    <xsl:for-each select="Reference[@ListName='State']">
                      <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                        <xsl:value-of select="@Name"/>
                      </IE:dropDownItem>
                    </xsl:for-each>
                  </IE:APDCustomSelect>
                </td>
                <td style="padding-right:3px;">Zip Code:</td>
                <td style="padding-right:3px;">
                  <IE:APDInput id="txtZipCode" name="txtZipCode" canDirty="false" maxlength="5" size="8" CCTabIndex="5"></IE:APDInput>
                </td>
              </tr>
            </table>
          </TD>
          <TD unselectable="on" nowrap="nowrap" align="left">
          
            <IE:APDButton id="btnClose" name="CloseButton" value="Close" onButtonClick="divClose()"/>
            <IE:APDButton id="btnSearch" name="SearchButton" value="Search" onButtonClick="Search()"/>
          </TD>
        </TR>
        <TR unselectable="on">
          <TD unselectable="on" nowrap="nowrap" colspan="4"><img src="/images/spacer.gif" width="1" height="1" border="0"/></TD>
        </TR>
      </TABLE>

    </TD>
  </TR>
</TABLE>

<img src="/images/spacer.gif" width="1" height="1" border="0"/><br />

<SPAN class="boldtext" id="errormsg" style="position:absolute; left:10px; width:680px;"></SPAN>

<IFRAME id="ifrmShopList" src="blank.asp" style='width:700px; height:218px; border:1px; visibility:hidden; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
</IFRAME>
</DIV>

<TEXTAREA name="remTA" style="display:none"><xsl:value-of select="$ShopRemarks"/></TEXTAREA>

<SCRIPT> 
  if (strMEChildWin != "true" &amp;&amp; typeof(parent.CancelAssignedShop) != "function" &amp;&amp; strGetUser == "true")
  {
    obj = document.getElementById("tblCSRInfo");
    obj.style.display = "inline";
  }
</SCRIPT>


<SCRIPT>
  if (strMEChildWin != "true" &amp;&amp; typeof(parent.CancelAssignedShop) != "function" &amp;&amp; strGetUser == "true")
  {
    if (btnClose)
      btnClose.style.visibility = 'hidden';
  } 
</SCRIPT>

</BODY>
</HTML>

</xsl:template>
 
</xsl:stylesheet>
