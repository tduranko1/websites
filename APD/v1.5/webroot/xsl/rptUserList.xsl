<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:decimal-format NaN="0"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
  <table cellpadding="0" cellspacing="0" border="0" name="tblUsers" id="tblUsers">
  <xsl:for-each select="User">
    <xsl:sort select="@NameFirst"/>
    <xsl:sort select="@NameLast"/>
    <tr style="height:18px;" valign="top">
      <xsl:attribute name="UserID"><xsl:value-of select="@UserID"/></xsl:attribute>
      <xsl:attribute name="SupervisorFlag"><xsl:value-of select="@SupervisorFlag"/></xsl:attribute>
      <td style="padding-left:5px;">
        <!-- <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox_htc('chkUser', concat(@NameFirst, ' ', @NameLast), '0', string(@UserID), '0', string(@UserID), '', 1, 1)"/> -->
        <IE:APDCheckBox value="0" canDirty="false">
          <xsl:attribute name="id"><xsl:value-of select="concat('chkUser', string(@UserID))"/></xsl:attribute>
          <xsl:attribute name="name"><xsl:value-of select="concat('chkUser', string(@UserID))"/></xsl:attribute>
          <xsl:attribute name="caption"><xsl:value-of select="concat(@NameFirst, ' ', @NameLast)"/></xsl:attribute>
          <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() + 1"/></xsl:attribute>
        </IE:APDCheckBox>
      </td>
      <!-- <td style="display:none"><xsl:value-of select="@UserID"/></td>
      <td><xsl:value-of select="@CSRNo"/></td> -->
    </tr>
  </xsl:for-each>
  </table>
  <xsl:call-template name="makeArray"/>
</xsl:template>


<xsl:template name="makeArray">
  <script language="javascript">
    var aUserList = new Array();
  <xsl:for-each select="User[@SupervisorFlag = '1' or boolean(@SupervisorID) = false() ]"><xsl:sort select="@NameFirst"/><xsl:sort select="@NameLast"/>
      aUserList.push(new Array("<xsl:value-of select="@UserID"/>", "<xsl:call-template name="subordinates"/>"));
  </xsl:for-each>
  </script>
</xsl:template>

<xsl:template name="subordinates">
  <xsl:variable name="supervisorID" select="@UserID"/>
  <xsl:for-each select="/Root/User[@SupervisorID = $supervisorID]"><xsl:sort select="@NameFirst"/><xsl:sort select="@NameLast"/><xsl:value-of select="@UserID"/>|<xsl:value-of select="@SupervisorFlag"/>,</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
