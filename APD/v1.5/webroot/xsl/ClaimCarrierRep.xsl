<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="ClaimLossType">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="CarrierCRUD" select="Claim"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxID"/>
<xsl:param name="UserID"/>
<xsl:param name="CarrierRepUserID"/>
<xsl:param name="SysLastUpdatedDate"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL    PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{6D65EFC5-52B6-4302-84C4-080E14C8900B},19,uspClaimCarrierRepGetListXML,ClaimCarrierRep.xsl, 999   -->

<HEAD>
<TITLE>Carrier Representative</TITLE>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>

<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>



<SCRIPT language="JavaScript">
	var gsCarrierCRUD = '<xsl:value-of select="$CarrierCRUD"/>';
  var gsLynxID = '<xsl:value-of select="$LynxID"/>';
  var gsUserID = '<xsl:value-of select="$UserID"/>';
  var gsCarrierRepUserID = '<xsl:value-of select="$CarrierRepUserID"/>';
  var gsSysLastUpdatedDate = '<xsl:value-of select="$SysLastUpdatedDate"/>';  
  
<![CDATA[

  function pageInit(){
    if (stat1){
      stat1.style.top = (document.body.offsetHeight - 75) / 2;
      stat1.style.left = (document.body.offsetWidth - 240) / 2;
    }
  }

  function onOk() {
    var selOffice = document.getElementById('selCarrierOffice');
    var selRep = document.getElementById('selCarrierRep')
    var iRepID = selRep.value;

    var sRequest = "LynxID=" + gsLynxID +
                   "&CarrierRepUserID=" + iRepID +
                   "&UserID=" + gsUserID +
                   "&SysLastUpdatedDate=" + gsSysLastUpdatedDate;
                   
    var sProc = "uspClaimCarrierUpdDetail";
    var aRequests = new Array();;
    aRequests.push( { procName : sProc,
                      method   : "ExecuteSpNpAsXML",
                      data     : sRequest }
                  );
    var sXMLRequest = makeXMLSaveString(aRequests);
    var objRet = XMLSave(sXMLRequest);
    var sRet = "";
    if (objRet && objRet.code == 0 && objRet.xml){
      var oXML = objRet.xml;
      var oUserRet = oXML.selectSingleNode("//Root/Carrier");
      if (oUserRet){
        var strXPath = "//Office[OfficeUser/@UserID='" + iRepID + "']";
        var OfficeNode = xmlReference.documentElement.selectSingleNode(strXPath);
        sOfficeName = OfficeNode.getAttribute("OfficeName");
        sRet = sOfficeName + "||" + iRepID + "||" + oUserRet.getAttribute("SysLastUpdatedDate");
      }
    } else {
      ServerEvent();
    }
    window.returnValue = sRet;
    window.close();
  }
  
  function onOfficeChange(){
    selCarrierRep.Clear();
    if (selCarrierOffice.value != "*"){
      stat1.Show("Please wait...");
      window.setTimeout("showOfficeUsers(" + selCarrierOffice.value + ")", 100);
    } else {
      stat1.Show("Please wait...");
      window.setTimeout("showAllUsers()", 100);
    }
  }
  
  function showOfficeUsers(sOfficeID){
      var oNode = xmlReference.selectSingleNode("//Office[@OfficeID='" + sOfficeID + "']");
      if (oNode){
        for (var i = 0; i < oNode.childNodes.length; i++)
          selCarrierRep.AddItem(oNode.childNodes[i].getAttribute("UserID"), oNode.childNodes[i].getAttribute("NameLast") + ", " + oNode.childNodes[i].getAttribute("NameFirst") + (oNode.childNodes[i].getAttribute("Enabled")=="0" ? " [Inactive]" : ""));
        selCarrierRep.Sort();
        btnOk.CCDisabled="true";
      }
      stat1.Hide();
  }
  
  function showAllUsers(){
    var oNode = xmlReference.selectNodes("//OfficeUser");
    if (oNode){
      for (var i = 0; i < oNode.length; i++)
        selCarrierRep.AddItem(oNode[i].getAttribute("UserID"), oNode[i].getAttribute("NameLast") + ", " + oNode[i].getAttribute("NameFirst") + (oNode[i].getAttribute("Enabled")=="0" ? " [Inactive]" : ""));
      selCarrierRep.Sort();
      btnOk.CCDisabled="true";
    }
    stat1.Hide();
  }
  
  function onCancel(){
    window.returnValue="";
    window.close()
  }

]]>
</SCRIPT>
</HEAD>
<BODY unselectable="on" class="bodyAPDSub" style="margin:0px 0px 0px 0px; padding:5px" onLoad="pageInit()">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

  <!-- Loss Type XML Data -->
  <DIV unselectable="on" style="background-color:#FFFFFF; border:0px solid #333377; ">
    <xml id="xmlReference"><Root><xsl:copy-of select="/Root"/></Root></xml>
    <xsl:call-template name="CarrierRep"/>
  </DIV>
  <IE:APDStatus id="stat1" name="stat1" width="240" height="75" />
</BODY>
</HTML>
</xsl:template>

<xsl:template name="CarrierRep">
  <DIV id="CarrierRep" style="width:350px; height:80px;">
    <xsl:variable name="UserOfficeID"><xsl:value-of select="/Root/Office[OfficeUser[@UserID = $CarrierRepUserID]]/@OfficeID"/></xsl:variable>
    <TABLE border="0" cellspacing="0" cellpadding="3">
      <colgroup>
        <col width="75px"/>
        <col width="300px"/>
      </colgroup>
      <TR valign="middle">
        <TD nowrap="">Carrier Office</TD>
        <TD>
          <IE:APDCustomSelect id="selCarrierOffice" name="selCarrierOffice" displayCount="6" blankFirst="false" canDirty="false" CCTabIndex="1" required="false" width="275" onChange="onOfficeChange()">
            <xsl:attribute name="value"><xsl:value-of select="$UserOfficeID"/></xsl:attribute>
          	<IE:dropDownItem value="*" style="display:none">All</IE:dropDownItem>
            <xsl:for-each select="/Root/Office">
              <xsl:sort select="@OfficeName" data-type="text"/>
              <IE:dropDownItem style="display:none">
                <xsl:attribute name="value"><xsl:value-of select="@OfficeID"/></xsl:attribute>
                <xsl:value-of select="@OfficeName"/>
              </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        </TD>
      </TR>
      <TR>
        <TD>Carrier Rep</TD>
        <TD>
          <IE:APDCustomSelect id="selCarrierRep" name="selCarrierRep" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" width="275" onChange="btnOk.CCDisabled='false'">
            <xsl:attribute name="value"><xsl:value-of select="$CarrierRepUserID"/></xsl:attribute>
            <xsl:for-each select="/Root/Office[@OfficeID = $UserOfficeID]/OfficeUser">
              <xsl:sort select="@NameLast" data-type="text"/>
              <IE:dropDownItem style="display:none">
                <xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>
                <xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
                <xsl:if test="@Enabled='0'"> [Inactive]</xsl:if>
              </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        
        </TD>
      </TR>
      <TR>
        <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
      <TR>
        <TD><xsl:attribute name="nowrap"/>
          <img src="/images/spacer.gif" alt="" width="1" height="4" border="0"/>
        </TD>
      </TR>
      <TR align="right">
        <TD colspan="2" style="text-align:center">
          <table border="0" cellpadding="0" cellspacing="0">
            <colgroup>
              <col width="*"/>
              <col width="175"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td></td>
              <td>
                <IE:APDButton id="btnOk" name="btnOk" value="OK" width="75" CCDisabled="true" CCTabIndex="3" onButtonClick="onOk()"/>
                <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="75" CCDisabled="false" CCTabIndex="4" onButtonClick="onCancel()"/>
              </td>
              <td></td>
            </tr>
          </table>
        </TD>
      </TR>
    </TABLE>
  </DIV>
</xsl:template>
</xsl:stylesheet>
