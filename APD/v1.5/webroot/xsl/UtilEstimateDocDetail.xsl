<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    id="UtilDocumentDetail">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="DocumentID"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="LynxID"/>
<xsl:param name="UserId"/>
<xsl:param name="EventUtilEstimateUpdate"/>
<xsl:param name="VehicleNumber"/>

<xsl:template match="/Root">

<html>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<STYLE type="text/css">
  IE\:APDButton {behavior:url("/behaviors/APDButton.htc")}
  IE\:APDCustomSelect {behavior:url("/behaviors/APDCustomSelect.htc")}
  IE\:APDInputCurrency {behavior:url("/behaviors/APDInputCurrency.htc")}
</STYLE>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formutilities.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT labguage="JavaScript">

	var gsDocumentID = "<xsl:value-of select="$DocumentID"/>";
	var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
	var gsUserID = "<xsl:value-of select="$UserId"/>";
	var gsLynxID = "<xsl:value-of select="$LynxID"/>";
	var gsVehNumber = "<xsl:value-of select="$VehicleNumber"/>";
	var gsDocumentVANSourceFlag = "<xsl:value-of select="Estimate/@DocumentVANSourceFlag"/>";
  var gsEventUtilEstimateUpdate = '<xsl:value-of select="$EventUtilEstimateUpdate"/>';
  var gsSetTotal;
  var gsTotalWarnOK = false;

<![CDATA[

  function PageInit()
  {
    if (gsDocumentVANSourceFlag != 1)
      updTotals();
    
    gsSetTotal = parent.document.getElementById("ifrmEstimateList").contentWindow.getSetTotal();
	  document.getElementById("txtDocumentTypeID").setFocus();
  }

  function updDocumentType()
  {
	  if (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10")
    {
      document.getElementById("trDuplicateFlag").style.display = "inline";
      document.getElementById("trEstimateTypeCD").style.display = "inline";
      document.getElementById("trAgreedPriceMetCD").style.display = "inline"
      document.getElementById("tblTotals").style.display = "inline";
    }
    else
    {
      document.getElementById("trDuplicateFlag").style.display = "none";
      document.getElementById("trEstimateTypeCD").style.display = "none";
      document.getElementById("trAgreedPriceMetCD").style.display = "none"
      document.getElementById("tblTotals").style.display = "none";
    }

	  if (document.getElementById("txtDocumentTypeID").value == "10")
      document.getElementById("txtSupplementSeqNumber").style.display = "inline";
    else
    {
      document.getElementById("txtSupplementSeqNumber").style.display = "none";
      document.getElementById("txtSupplementSeqNumber").value = "0";
    }
  }

  function updTotals()
  {
    var loNetTotalAmt = document.getElementById("txtNetTotalAmt");
	var Total = Number(document.getElementById("txtRepairTotalAmt").value) - Number(document.getElementById("txtBettermentAmt").value) - Number(document.getElementById("txtDeductibleAmt").value) - Number(document.getElementById("txtOtherAdjustmentAmt").value);
    if (Total == "") Total = "0";
    loNetTotalAmt.CCDisabled = false;
    loNetTotalAmt.value = Total;
    loNetTotalAmt.CCDisabled = true;
  }

	function chkTotalDiff()
	{
    if (gsTotalWarnOK == true)
      return true;
    
    var repairValue = parseFloat(document.getElementById("txtRepairTotalAmt").value);
    var setTotalValue = parseFloat(gsSetTotal);
    if (setTotalValue != NaN)
    {
      var relPercent = setTotalValue * 0.3;
      if (repairValue > setTotalValue+relPercent || repairValue < setTotalValue-relPercent)
      {
        var sRet = YesNoMessage("Total Repair Warning", "The value entered differs from the highest Total value by +/- 30%. Press YES to continue, or NO to cancel.");
        if (sRet != "Yes")
        {
          gsTotalWarnOK = false;
          document.getElementById("txtRepairTotalAmt").setFocus();
          return false;
        }
        else
        {
          gsTotalWarnOK = true;
          return true;
        }
      }
    }
  }

	var inADS_Pressed = false; // flagging for buttons pressed
	function btnSaveClick()
	{
	  var lobtnSave = document.getElementById("btnSave");
    lobtnSave.CCDisabled = true;
    inUpload_Pressed = true;

    var sWarnUser = "";

    if (gsDocumentVANSourceFlag != 1)
    {
      if (document.getElementById("txtDocumentTypeID").value != "10")
        document.getElementById("txtSupplementSeqNumber").value = "0";
    }
  
    if (document.getElementById("txtDocumentTypeID").hasItemSelected(false) == false)
      sWarnUser +=  "<li> A Document Type must be specified.</li><br>";
    
	  if (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10")
    {
      if (gsDocumentVANSourceFlag != 1)
      {
        if (document.getElementById("txtDocumentTypeID").value == "10" && (document.getElementById("txtSupplementSeqNumber").hasItemSelected(false) == false || document.getElementById("txtSupplementSeqNumber").value == "0"))
          sWarnUser +=  "<li> Select a Supplement number from the drop-down.</li><br>";
      }
  
      if (document.getElementById("txtDuplicateFlag").hasItemSelected(false) == false)
        sWarnUser += "<li> Duplicate selection is required.</li><br>";
  
      if (document.getElementById("txtEstimateTypeCD").hasItemSelected(false) == false)
        sWarnUser += "<li> Original/Audit selection is required.</li><br>";
      
      if (document.getElementById("txtAgreedPriceMetCD").hasItemSelected(false) == false && document.getElementById("txtEstimateTypeCD").value == "A" && document.getElementById("txtDuplicateFlag").value == "0")
        sWarnUser += "<li> Price Agreed selection is required for non-duplicate audited documents.</li><br>";

      if (gsDocumentVANSourceFlag != 1 && document.getElementById("txtDuplicateFlag").value == "0")
      {
        if (document.getElementById("txtRepairTotalAmt").value == "")
          sWarnUser += "<li> Repair Total amount is required.</li><br>";
    
        if (document.getElementById("txtDeductibleAmt").value == "")
          sWarnUser += "<li> Deductible amount is required.</li><br>";
    
        if (document.getElementById("txtOtherAdjustmentAmt").value == "")
          sWarnUser += "<li> Other Adjustment amount is required.</li><br>";
      }
    }
    else
    {
      document.getElementById("txtDuplicateFlag").value = "";
      document.getElementById("txtEstimateTypeCD").value = "";
    }

    if (sWarnUser != '')
    {
      ClientWarning(sWarnUser);
      lobtnSave.CCDisabled = false;
      inUpload_Pressed = false;
      gsFile = "";
      return false;
    }
    
	  if ((document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10") && (gsDocumentVANSourceFlag != 1))
    {
      if (document.getElementById("txtNetTotalAmt").value < 0)
      {
        var sRet = YesNoMessage("Negative Net Total Warning", "The Net Total value is negative and will be changed to $0.00. Press YES to continue, or NO to cancel and correct the values entered.");
        if (sRet != "Yes")
        {
          document.getElementById("txtRepairTotalAmt").setFocus();
          return false;
        }
        else
        {
      	  var loNetTotalAmt = document.getElementById("txtNetTotalAmt");
          loNetTotalAmt.CCDisabled = false;
          loNetTotalAmt.value = "0.00";
          loNetTotalAmt.CCDisabled = true;
        }
      }
    }

    //check for duplication
    var dupFlag = document.getElementById("txtDuplicateFlag").value;
    var docType = document.getElementById("txtDocumentTypeID").value;

    if ( dupFlag == 0 && (docType == 3 || docType == 10) )
    {
      var docTypeText = document.getElementById("txtDocumentTypeID").text;
      var docSeq = document.getElementById("txtSupplementSeqNumber").value;
      var estType = document.getElementById("txtEstimateTypeCD").text;
      var chkDup = parent.document.getElementById("ifrmEstimateList").contentWindow.chkEstimates(docTypeText, docSeq, estType, gsDocumentID);
      
      if (chkDup)
      {
        ClientWarning(chkDup);
        lobtnSave.CCDisabled = false;
        inADS_Pressed = false;
        return false;
      }
    }

    if (gsDocumentVANSourceFlag != 1)
    {
  	  if (chkTotalDiff() == false)
        return;
    }
    
    var sAdjTotal = Number(document.getElementById("txtDeductibleAmt").value) + Number(document.getElementById("txtOtherAdjustmentAmt").value);
    sAdjTotal = Math.round(sAdjTotal*100)/100; //round to 2 decimal places

    var lsAgreedPriceMetCD;

    if (document.getElementById("txtAgreedPriceMetCD").value == null)
      lsAgreedPriceMetCD = "";
    else
      lsAgreedPriceMetCD = document.getElementById("txtAgreedPriceMetCD").value;

    //var sProcName = "uspUtilEstimateUpdDetail";
    var sProcName = "uspEstimateQuickUpdDetail";
    var sRequest =  "DocumentID=" + gsDocumentID +
                    "&ClaimAspectID=" + gsClaimAspectID +
                    "&AdjustmentTotalAmt=" + sAdjTotal +
                    "&DeductibleAmt=" + document.getElementById("txtDeductibleAmt").value +
                    "&DocumentTypeID=" + document.getElementById("txtDocumentTypeID").value +
                    "&DuplicateFlag=" + document.getElementById("txtDuplicateFlag").value +
                    "&EstimateTypeCD=" + document.getElementById("txtEstimateTypeCD").value +
                    "&NetTotalAmt=" + document.getElementById("txtNetTotalAmt").value +
                    "&OtherAdjustmentAmt=" + document.getElementById("txtOtherAdjustmentAmt").value +
                    "&RepairTotalAmt=" + document.getElementById("txtRepairTotalAmt").value +
					          "&BettermentTotalAmt=" + document.getElementById("txtBettermentAmt").value +
                    "&SupplementSeqNumber=" + document.getElementById("txtSupplementSeqNumber").value +
                    "&AgreedPriceMetCD=" + lsAgreedPriceMetCD +
                    "&UserID=" + gsUserID;

    var oReturn = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProcName, sRequest);

    lobtnSave.CCDisabled = false;
    inADS_Pressed = false;

    if (oReturn.status != -1)
    {
      sProcName = "uspWorkflowNotifyEvent";
      sRequest = "EventID=" + gsEventUtilEstimateUpdate + 
                 "&ClaimAspectID=" + gsClaimAspectID +
                 "&Description=" + "Estimate for Vehicle " + gsVehNumber + " updated" +
                 "&UserID=" + gsUserID;

      var oEventReturn = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProcName, sRequest);

      parent.document.getElementById("ifrmEstimateList").src = "UtilEstimateDocList.asp?ClaimAspectID=" + gsClaimAspectID + "&LynxID=" + gsLynxID + "&ReloadFlag=1";
      parent.document.getElementById("ifrmDocViewer").src = "blank.asp";
      parent.document.getElementById("ifrmEstimateDetails").src = "blank.asp";
    }
    else
    {
      ServerEvent();
      return;
    }
  }

]]>
  
</SCRIPT>

<head>
	<title>APD Estimate Cleanup Utility - Document Details</title>
</head>
<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" onLoad="updDocumentType(); PageInit();">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

  <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="2">
    <TR style="height:26px;">
      <TD unselectable="on" align="center" colspan="2" nowrap="">
        <SPAN style="color: #000066; font-weight: bold; font-size: 12px;">Estimate Details</SPAN>
      </TD>
      <td nowrap="" align="right" colspan="3" unselectable="on">
        <span style="height:24px; position:relative;" unselectable="on">
          <IE:APDButton id="btnSave" name="btnSave" value="Save" CCDisabled="false" onButtonClick="btnSaveClick()" title="Save" CCTabIndex="10"/>
        </span>
      </td>
    </TR>
    <TR>
      <TD unselectable="on" width="55%" valign="top" colspan="2" nowrap="">

        <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="2">
          <TR>
            <TD unselectable="on" nowrap="">
              <strong>Document Type:</strong>
            </TD>
            <TD unselectable="on" nowrap="">
              <IE:APDCustomSelect id="txtDocumentTypeID" name="DocumentTypeID" width="130" blankFirst="true" canDirty="true" displayCount="8" required="true" onChange="updDocumentType()" CCTabIndex="1">
               <xsl:attribute name="value" ><xsl:value-of select="Estimate/@DocumentTypeID"/></xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
                <xsl:for-each select="/Root/Reference[@List='DocumentType']">
                  <!-- do not show Final Estimate, Preliminary Estimate, First Report -->
                  <xsl:if test="@ReferenceID != '4' and @ReferenceID != '14' and @ReferenceID != '15'">
                	  <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                      <xsl:value-of select="@Name"/>
                    </IE:dropDownItem>
                  </xsl:if>
                </xsl:for-each>
              </IE:APDCustomSelect>
      
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      
              <IE:APDCustomSelect id="txtSupplementSeqNumber" name="SupplementSeqNumber" width="34" blankFirst="false" canDirty="true" displayCount="8" required="true" style="display:none" CCTabIndex="2">
               <xsl:attribute name="value" ><xsl:value-of select="Estimate/@SupplementSeqNumber"/></xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              	  <IE:dropDownItem value="0" style="display:none"></IE:dropDownItem>
              	  <IE:dropDownItem value="1" style="display:none">1</IE:dropDownItem>
              	  <IE:dropDownItem value="2" style="display:none">2</IE:dropDownItem>
              	  <IE:dropDownItem value="3" style="display:none">3</IE:dropDownItem>
              	  <IE:dropDownItem value="4" style="display:none">4</IE:dropDownItem>
              	  <IE:dropDownItem value="5" style="display:none">5</IE:dropDownItem>
              	  <IE:dropDownItem value="6" style="display:none">6</IE:dropDownItem>
              	  <IE:dropDownItem value="7" style="display:none">7</IE:dropDownItem>
              	  <IE:dropDownItem value="8" style="display:none">8</IE:dropDownItem>
              	  <IE:dropDownItem value="9" style="display:none">9</IE:dropDownItem>
              	  <IE:dropDownItem value="10" style="display:none">10</IE:dropDownItem>
              </IE:APDCustomSelect>
            </TD>
          </TR>
          <TR id="trDuplicateFlag">
            <TD unselectable="on" nowrap=""><strong>Duplicate:</strong></TD>
            <TD unselectable="on" nowrap="">
              <IE:APDCustomSelect id="txtDuplicateFlag" name="DuplicateFlag" blankFirst="true" canDirty="true" displayCount="8" required="true" CCTabIndex="3" >
               <xsl:attribute name="value" ><xsl:value-of select="Estimate/@DuplicateFlag"/></xsl:attribute>
            	  <IE:dropDownItem value="0" style="display:none">No</IE:dropDownItem>
            	  <IE:dropDownItem value="1" style="display:none">Yes</IE:dropDownItem>
              </IE:APDCustomSelect>
            </TD>
          </TR>
          <TR id="trEstimateTypeCD">
            <TD unselectable="on" nowrap="">
              <strong>Original/Audited:</strong>
            </TD>
            <TD unselectable="on" nowrap="">
              <IE:APDCustomSelect id="txtEstimateTypeCD" name="EstimateTypeCD" blankFirst="true" canDirty="true" displayCount="8" required="true" CCTabIndex="4">
               <xsl:attribute name="value" ><xsl:value-of select="Estimate/@EstimateTypeCD"/></xsl:attribute>
                <xsl:for-each select="/Root/Reference[@List='EstimateTypeCD']">
              	  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </TD>
          </TR>
          <TR id="trAgreedPriceMetCD" style="display:none">
            <TD unselectable="on" nowrap="">
              <strong>Price Agreed:</strong>
            </TD>
            <TD unselectable="on" nowrap="">
              <IE:APDCustomSelect id="txtAgreedPriceMetCD" name="AgreedPriceMetCD" blankFirst="true" canDirty="true" displayCount="8" required="true" CCTabIndex="5" >
               <xsl:attribute name="value" ><xsl:value-of select="Estimate/@AgreedPriceMetCD"/></xsl:attribute>
                <xsl:for-each select="/Root/Reference[@List='AgreedPriceMetCD']">
              	  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </TD>
          </TR>
        </TABLE>

      </TD>
      <TD unselectable="on" width="45%" valign="top" colspan="2" nowrap="">

        <TABLE unselectable="on" id="tblTotals" width="100%" border="0" cellspacing="0" cellpadding="2">
          <TR>
            <TD unselectable="on" nowrap=""><strong>Repair Total:</strong></TD>
            <TD unselectable="on" nowrap="">
              <IE:APDInputCurrency id="txtRepairTotalAmt" name="RepairTotalAmt" neg="false" precision="9" scale="2" required="true" canDirty="true" onChange="chkTotalDiff(); updTotals()" CCTabIndex="6">
                <xsl:attribute name="value"><xsl:value-of select="Estimate/@RepairTotalAmt"/></xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </TD>
          </TR>
		  
		   <TR><TD unselectable='on' nowrap=''><strong>Betterment:</strong></TD>
		  <TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
          <IE:APDInputCurrency id="txtBettermentAmt" name="BettermentAmt" neg="true" precision="8" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="7">
		    <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="Estimate/@BettermentAmt = '' and Estimate/@DocumentVANSourceFlag != '1'"> 
                      <xsl:text disable-output-escaping="yes">0</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Estimate/@BettermentAmt"/> 
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
          </IE:APDInputCurrency>
		  </TD>
		   </TR>
		  
          <TR>
            <TD unselectable="on" nowrap=""><strong>Deductible:</strong>
              <xsl:variable name="ClaimAspectID" select="/Root/@ClaimAspectID"/>
              ($<xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ClaimAspectID=$ClaimAspectID]/@DeductibleAmt"/>)
            </TD>
            <TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
              <IE:APDInputCurrency id="txtDeductibleAmt" name="DeductibleAmt" neg="false" precision="9" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="8">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="Estimate/@DocumentVANSourceFlag = '1' or Estimate/@SummaryExistsFlag = '1'">
                      <xsl:value-of select="Estimate/@DeductibleAmt"/>
                    </xsl:when>
                    <xsl:when test="Estimate/@SummaryExistsFlag = '0'">
                      <xsl:choose>
                        <xsl:when test="Estimate/@DeductibleAmt != ''">
                          <xsl:value-of select="Estimate/@DeductibleAmt"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ClaimAspectID=$ClaimAspectID]/@DeductibleAmt"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </TD>
          </TR>
          <TR>
            <TD unselectable="on" nowrap=""><strong>Other Adjustments:</strong></TD>
            <TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
              <IE:APDInputCurrency id="txtOtherAdjustmentAmt" name="OtherAdjustmentAmt" neg="true" precision="8" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="9">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="Estimate/@OtherAdjustmentAmt = '' and Estimate/@DocumentVANSourceFlag != '1'">
                      <xsl:text disable-output-escaping="yes">0</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Estimate/@OtherAdjustmentAmt"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test="Estimate/@DocumentVANSourceFlag = '1'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputCurrency>
            </TD>
          </TR>
          <TR>
            <TD unselectable="on" nowrap=""><strong>Net Total:</strong></TD>
            <TD unselectable="on" nowrap="">
              <IE:APDInputCurrency id="txtNetTotalAmt" name="NetTotalAmt" precision="9" scale="2" CCDisabled="true" CCTabIndex="-1">
                <xsl:attribute name="value"><xsl:value-of select="Estimate/@NetTotalAmt"/></xsl:attribute>
              </IE:APDInputCurrency>
            </TD>
          </TR>
        </TABLE>

      </TD>
    </TR>
  </TABLE>

</body>
</html>
</xsl:template>

</xsl:stylesheet>
