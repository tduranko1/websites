<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Insurance">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>
<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function formatcurrency(useValue)
    {
        var strValue = new String(useValue);
        if (strValue.indexOf('.') == -1)
            strValue = strValue + '.00';
        else if (strValue.indexOf('.') == strValue.length - 2)
            strValue = strValue + '0';
        else
            strValue = strValue.substring(0, strValue.indexOf('.') + 3);

        if (strValue.indexOf('.') == 0)
            return '0' + strValue;
        else
            return strValue;
    }
  ]]>
</msxsl:script>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Client Configuration"/>

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R') and count(/Root/ClaimAspectTypes) > 0">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
            <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
          <tr>
            <td align="center">
          <xsl:choose>
            <xsl:when test="count(/Root/ClaimAspectTypes) = 0">
              <font color="#ff0000"><strong>This insurance company does not have either Claim or Property or Vehicle in the supported Claim Aspect list.
              <br/><br/>Please select these appropriately in the Claim Aspect tab and try again.</strong></font>
            </xsl:when>
            <xsl:otherwise>
              <font color="#ff0000"><strong>You do not have sufficient permission to view this page.
              <br/>Please contact administrator for permissions.</strong></font>
            </xsl:otherwise>
          </xsl:choose>
            </td>
          </tr>
        </table>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="InfoCRUD"/>
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspAdmInsFeesGetListXML,refInsFeeBilling.xsl, 145   -->


<HEAD>
<TITLE>Data Administration: Insurance Companies</TITLE>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="Javascript">
    var CATCount = <xsl:value-of select="count(/Root/Reference[@List='AspectType'])"/>;
    var sBillingModelCD = "<xsl:value-of select="@BillingModelCD"/>";
    var aServiceIDs = new Array(<xsl:for-each select="Service">"<xsl:value-of select="@ServiceID"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    var bLoadingFees = false;
    var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
    var iInsuranceID = "<xsl:value-of select="@InsuranceCompanyID"/>";
<![CDATA[

    var gbDirty = false;
    var curFeeRow = null;
    var iNewCount = 0;
    var curAdminRow = null;
    var blnRefreshing = false;
    var iClientFeeID = null;
    var iAdminRowID = null;
    var oCurAdminTD = null;
    var isAdminFeeDirty = false;

    function pageInit(){
      try {
        feeStat.style.top = (document.body.offsetHeight - 75) / 2;
        feeStat.style.left = (document.body.offsetWidth - 240) / 2;
        
        if (sCRUD.indexOf("C") == -1)
          btnAddFee.setAttribute("CCDisabled", true, 0);
        //if (sCRUD.indexOf("U") == -1)
        //  disableControls(true);
        //if (sCRUD.indexOf("D") == -1)
          btnDeleteFee.setAttribute("CCDisabled", true, 0);
          btnSaveFee.setAttribute("CCDisabled", true, 0);
        
        window.setTimeout("initList()", 250);
        
        tabFees2.CCDisabled = true;
      } catch (e) {
        ClientError("pageInit(): " + e.description);
      }
    }

    function initList(){
      if (!tabFeeDetail2.isReady) {
        setTimeout("initList()", 500);
        return;
      }
      tabFeeDetail2.CCDisabled = true;
      tblSort2.disabled = false;
    }
    
    function disableControls(bDisabled){
      tabFees2.CCDisabled = bDisabled;
      imgMoveUp.disabled = bDisabled;
      imgMoveDown.disabled = bDisabled;
      btnAddFee.setAttribute("CCDisabled", bDisabled, 0);
      btnDeleteFee.setAttribute("CCDisabled", bDisabled, 0);
      btnSaveFee.setAttribute("CCDisabled", bDisabled, 0);
    }
    
    function FeeGridClick(oRow) {
      try {
        if (sCRUD.indexOf("R") == -1) return;
        if (tblSort2.disabled) return;
        if (oRow) {
          if (oRow == curFeeRow) return;

          if (tabFees2.CCDisabled == true)
            tabFees2.CCDisabled = false;

          tabFeeDetail2.CCDisabled = false;
          if (curFeeRow) {
            if (tabFees2.isDirty || isAdminFeeDirty) {
              if (validateFeeInfo() == true)
                updateXML(curFeeRow);
              else
                return;
            }
            curFeeRow.style.backgroundColor = "#FFFFFF";
          }

          oRow.style.backgroundColor = "#FFD700";
          curFeeRow = oRow;
          bLoadingFees = true;
          setCanDirty(false);
          if (sCRUD.indexOf("D") != -1)
            btnDeleteFee.setAttribute("CCDisabled", false, 0);
          if (sCRUD.indexOf("U") != -1)
            btnSaveFee.setAttribute("CCDisabled", false, 0);
          resetInfo();
          loadFeeInfo(oRow);
          setMoveUpDown(oRow.parentElement.parentElement, oRow);

          //alert(oRow.lastChild.innerText);
        }
      } catch(e) {
        ClientError("FeeGridClick(): " + e.description);
      }
    }

    function loadFeeInfo() {
      feeStat.Show("Loading Fee Data...");
      window.setTimeout("loadFeeInfo2()", 150);
    }

    function loadFeeInfo2() {
      try {
        oRow = curFeeRow;
        var iFeeInvoiceable = 0;
        if (oRow) {
          var iFeeID = oRow.lastChild.innerText;
          if (iFeeID != "") {
            if (xmlFees) {
              if (tabFeeDetail2.selected == false)
                tabFees2.SelectTab(0);
              tabFeeDetail2.disablePage(false);
              var oFeeNode = xmlFees.selectSingleNode("/Root/ClientFee[@ClientFeeID='" + iFeeID + "']");
              if (oFeeNode) {
                txtFeeDescription.value = oFeeNode.getAttribute("Description");
                txtCurAmount.value = oFeeNode.getAttribute("FeeAmount");
                txtInstructions.value = oFeeNode.getAttribute("FeeInstructions");
                chkInvoice.value = iFeeInvoiceable = oFeeNode.getAttribute("ItemizeFlag");
                chkInvoiceable.value = (oFeeNode.getAttribute("HideFromInvoiceFlag") == 0 ? "1" : "0");
                txtInvDesc.value = oFeeNode.getAttribute("InvoiceDescription");
                if (oFeeNode.getAttribute("CategoryCD") == "H") rbHandlingFee.value = 1;
                if (oFeeNode.getAttribute("CategoryCD") == "A") rbAddtlFee.value = 2;
                if (document.all["rbAspectClaim"]) rbAspectClaim.value = oFeeNode.getAttribute("ClaimAspectTypeID");
                if (document.all["rbAspectVehicle"]) rbAspectVehicle.value = oFeeNode.getAttribute("ClaimAspectTypeID");
                if (document.all["rbAspectProperty"]) rbAspectProperty.value = oFeeNode.getAttribute("ClaimAspectTypeID");
                txtFeeEffectiveFrom.value = (oFeeNode.getAttribute("EffectiveStartDate") ? oFeeNode.getAttribute("EffectiveStartDate") : "");
                txtFeeEffectiveTo.value = (oFeeNode.getAttribute("EffectiveEndDate") ? oFeeNode.getAttribute("EffectiveEndDate") : "");
                csApplies.value = oFeeNode.getAttribute("AppliesWhenCD");
                
                tabAdminFees.CCDisabled = true;
              }

              var oIncludedServices = xmlFees.selectNodes("/Root/IncludedServices[@ClientFeeID='" + iFeeID + "']");
              if (oIncludedServices) {
                for (i = 0; i < oIncludedServices.length; i++) {
                  var iServiceID = oIncludedServices[i].getAttribute("ServiceID");
                  var chk = document.all["chkServices" + iServiceID];
                  if (chk)
                    chk.value = 1;

                  var iRequired = oIncludedServices[i].getAttribute("RequiredFlag");
                  var chk = document.all["chkServicesRequired" + iServiceID];
                  if (chk)
                    chk.value = iRequired;

                  var iInvoiceable = oIncludedServices[i].getAttribute("ItemizeFlag");
                  var chk = document.all["chkServicesInvoiceable" + iServiceID];
                  if (chk)
                      chk.value = iInvoiceable;

                  if (iServiceID == 3)
                     tabAdminFees.CCDisabled = false;
                }
              }

              if (chkInvoice.value == 1)
                enableServiceInvoiceable(false);
              else
                enableServiceInvoiceable(true);
                
              //load admin fees
              //clear the admin fee table
               var oRows = tblAdminFees.rows;
               /*var iRows = 0;
               if (oRows)
                  iRows = oRows.length;
               //reset admin fee data
               for (var i = iRows - 1; i >= 0; i--){
                  oRows[i].cells[0].innerText = "Not Set";
                  oRows[i].cells[2].innerText = "";
                  oRows[i].cells[4].innerText = " ";
                  //oRows[i].cells[5].innerText = "";
                  oRows[i].cells[6].innerText = "";
               }*/

              var oAdminFees = xmlFees.selectNodes("/Root/AdminFee[@ClientFeeID='" + iFeeID + "']");
              for (var i = 0; i < oAdminFees.length; i++){
               var strAdminFeeCd = oAdminFees[i].getAttribute("AdminFeeCD");
               var strAdminFeeAmt = oAdminFees[i].getAttribute("Amount");
               var strMSA = oAdminFees[i].getAttribute("MSA");
               var strState = oAdminFees[i].getAttribute("State");
               
               if (strAdminFeeAmt && isNaN(strAdminFeeAmt) == false){
                  if (strAdminFeeAmt.indexOf(".") == -1) strAdminFeeAmt += ".00";
                  if (strMSA == "Flat" || strState == "Flat"){
                     var oTR = document.getElementById("trAdminFeesFlat");
                     if (oTR){
                        oTR.cells[0].innerText = (strAdminFeeCd == "I" ? "Insurance" : "Shop");
                        oTR.cells[2].innerText = strAdminFeeAmt;
                        oTR.cells[4].innerText = strAdminFeeCd;
                        oTR.cells[5].innerText = "Flat";
                        oTR.cells[6].innerText = iFeeID;
                     }
                  } else if (strMSA != "" && strState == ""){
                     var oTR = document.getElementById("trAdminFeesMSA" + strMSA);
                     if (oTR){
                        oTR.cells[0].innerText = (strAdminFeeCd == "I" ? "Insurance" : "Shop");
                        oTR.cells[2].innerText = strAdminFeeAmt;
                        oTR.cells[4].innerText = strAdminFeeCd;
                        oTR.cells[5].innerText = strMSA;
                        oTR.cells[6].innerText = iFeeID;
                     }
                  } else if (strMSA == "" && strState != ""){
                     var oTR = document.getElementById("trAdminFeesState" + strState);
                     if (oTR){
                        oTR.cells[0].innerText = (strAdminFeeCd == "I" ? "Insurance" : "Shop");
                        oTR.cells[2].innerText = strAdminFeeAmt;
                        oTR.cells[4].innerText = strAdminFeeCd;
                        oTR.cells[5].innerText = strState;
                        oTR.cells[6].innerText = iFeeID;
                     }
                  }
               }
              }
            }
          }
        }
      } catch (e) {
        ClientError("loadFeeInfo(): " + e.description);
      } finally {
        feeStat.Hide();
        tabFees2.contentSaved();
        setCanDirty(true);

        if (sCRUD.indexOf("U") == -1)
          tabFeeDetail2.disablePage(true);
        bLoadingFees = false;
      }
    }
    
    function resetInfo(){
      try {
        txtFeeDescription.value = "";
        txtCurAmount.value = "";
        txtInstructions.value = "";
        chkInvoice.value = 1;
        chkInvoiceable.value = 1;
        txtInvDesc.value = "";
        rbHandlingFee.value = 0;
        rbAddtlFee.value = 0;
        txtFeeEffectiveFrom.value = "";
        txtFeeEffectiveTo.value = "";
        csApplies.value = "C";
        if (document.all["rbAspectClaim"]) rbAspectClaim.value = 99;
        if (document.all["rbAspectVehicle"]) rbAspectVehicle.value = 99;
        if (document.all["rbAspectProperty"]) rbAspectProperty.value = 99;
        if (aServiceIDs) {
          for (i = 0; i < aServiceIDs.length; i++) {
            var chk = document.all["chkServices" + aServiceIDs[i]];
            if (chk)
              chk.value = 0;
            chk = document.all["chkServicesRequired" + aServiceIDs[i]];
            if (chk)
              chk.value = 0;
            chk = document.all["chkServicesInvoiceable" + aServiceIDs[i]];
            if (chk)
              chk.value = 0;
          }
        }
         var oRows = tblAdminFees.rows;
         var iRows = 0;
         if (oRows)
            iRows = oRows.length;
         //reset admin fee data
         for (var i = iRows - 1; i >= 0; i--){
            oRows[i].cells[0].innerText = "Not Set";
            oRows[i].cells[2].innerText = "";
            oRows[i].cells[4].innerText = " ";
            //oRows[i].cells[5].innerText = "";
            oRows[i].cells[6].innerText = "";
         }
      } catch (e) {
        ClientError("resetInfo(): " + e.description);
      }
    }

    function enableServiceInvoiceable(bDisabled){
      try {
        if (aServiceIDs) {
          for (i = 0; i < aServiceIDs.length; i++) {
            var chk = document.all["chkServicesInvoiceable" + aServiceIDs[i]];
            if (chk) {
              if (bDisabled) {
                chk.setAttribute("canDirty", false, 0);
                chk.setAttribute("value", 0, 0);
                chk.setAttribute("canDirty", true, 0);
              }
              chk.setAttribute("CCDisabled", bDisabled, 0);
            }
          }
        }
      } catch (e) {
        ClientError("enableServiceInvoiceable(): " + e.description);
      }
    }

    function updateServiceInvoiceable() {
      if (!chkInvoice.isDirty) return;
      if (chkInvoice.value == "1")
        enableServiceInvoiceable(false);
      else
        enableServiceInvoiceable(true);
    }

    function setCanDirty(bCanDirty) {
      try {
        txtFeeDescription.canDirty = rbHandlingFee.canDirty = rbAddtlFee.canDirty =
          txtCurAmount.canDirty = txtInstructions.canDirty = chkInvoice.canDirty = txtInvDesc.canDirty = 
          txtFeeEffectiveFrom.canDirty = txtFeeEffectiveTo.canDirty = csApplies.canDirty = chkInvoiceable.canDirty = bCanDirty;
        if (document.all["rbAspectClaim"]) rbAspectClaim.canDirty = bCanDirty;
        if (document.all["rbAspectVehicle"]) rbAspectVehicle.canDirty = bCanDirty;
        if (document.all["rbAspectProperty"]) rbAspectProperty.canDirty = bCanDirty;
        if (aServiceIDs) {
          for (i = 0; i < aServiceIDs.length; i++) {
            var chk = document.all["chkServices" + aServiceIDs[i]];
            if (chk)
              chk.canDirty = bCanDirty;
            chk = document.all["chkServicesRequired" + aServiceIDs[i]];
            if (chk)
              chk.canDirty = bCanDirty;
            chk = document.all["chkServicesInvoiceable" + aServiceIDs[i]];
            if (chk)
              chk.canDirty = bCanDirty;
          }
        }
      } catch (e) {
        ClientError("setCanDirty(): " + e.description);
      }
    }

    function updateXML(oRow) {
      try {
        if (oRow) {
          var iFeeID = oRow.lastChild.innerText;
          if (iFeeID != "") {
            if (xmlFees) {
              var oFeeNode = xmlFees.selectSingleNode("/Root/ClientFee[@ClientFeeID='" + iFeeID + "']");
              if (!oFeeNode) {
                oFeeNode = xmlFees.createElement("ClientFee");
                oFeeNode.setAttribute("ClientFeeID", iFeeID);
                oFeeNode.setAttribute("EnabledFlag", "1");
                oFeeNode.setAttribute("SysLastUpdatedDate", "");
                oFeeNode.setAttribute("DisplayOrder", oRow.rowIndex + 1);
                xmlFees.documentElement.appendChild(oFeeNode);
              }
              if (oFeeNode) {
                gbDirty = true;
                oFeeNode.setAttribute("ClientFeeID", iFeeID);
                oFeeNode.setAttribute("Description", txtFeeDescription.value);
                oFeeNode.setAttribute("FeeAmount", txtCurAmount.value);
                oFeeNode.setAttribute("FeeInstructions", txtInstructions.value);
                oFeeNode.setAttribute("HideFromInvoiceFlag", (chkInvoiceable.value == "1" ? 0 : 1));
                oFeeNode.setAttribute("ItemizeFlag", chkInvoice.value);
                oFeeNode.setAttribute("InvoiceDescription", txtInvDesc.value);
                if (document.all["rbHandlingFee"] && rbHandlingFee.value == 1)
                  oFeeNode.setAttribute("CategoryCD", "H");
                if (document.all["rbAddtlFee"] && rbAddtlFee.value == 2)
                  oFeeNode.setAttribute("CategoryCD", "A");

                if (document.all["rbAspectClaim"] && rbAspectClaim.checked == true) oFeeNode.setAttribute("ClaimAspectTypeID", rbAspectClaim.checkedValue);
                if (document.all["rbAspectVehicle"] && rbAspectVehicle.checked == true) oFeeNode.setAttribute("ClaimAspectTypeID", rbAspectVehicle.checkedValue);
                if (document.all["rbAspectProperty"] && rbAspectProperty.checked == true) oFeeNode.setAttribute("ClaimAspectTypeID", rbAspectProperty.checkedValue);

                oFeeNode.setAttribute("EffectiveStartDate", (txtFeeEffectiveFrom.value ? txtFeeEffectiveFrom.value : ""));
                oFeeNode.setAttribute("EffectiveEndDate", (txtFeeEffectiveTo.value ? txtFeeEffectiveTo.value : ""));
                oFeeNode.setAttribute("AppliesWhenCD", csApplies.value);
              }
              var oIncludedServices = xmlFees.selectNodes("/Root/IncludedServices[@ClientFeeID='" + iFeeID + "']");
              if (oIncludedServices) {
                // delete the old ones
                oIncludedServices.removeAll();
                  // add the new ones
                if (aServiceIDs) {
                  for (i = 0; i < aServiceIDs.length; i++) {
                    var chk = document.all["chkServices" + aServiceIDs[i]];
                    if (chk && chk.value == 1) {
                      var oIncludedService = xmlFees.createElement("IncludedServices");
                      oIncludedService.setAttribute("ClientFeeID", iFeeID);
                      oIncludedService.setAttribute("ServiceID", chk.parentElement.parentElement.lastChild.innerText);

                      chk = document.all["chkServicesRequired" + aServiceIDs[i]];
                      if (chk)
                        oIncludedService.setAttribute("RequiredFlag", chk.value);

                      chk = document.all["chkServicesInvoiceable" + aServiceIDs[i]];
                      if (chk)
                        oIncludedService.setAttribute("ItemizeFlag", chk.value);

                      // note: new nodes will be added to the last. This will not affect the xml document structure.
                      xmlFees.documentElement.appendChild(oIncludedService);
                    }
                  }
                }
              }
              var oAdminFees = xmlFees.selectNodes("/Root/AdminFee[@ClientFeeID='" + iFeeID + "']");
              if (oAdminFees){
               //delete the old ones
               oAdminFees.removeAll();
               var oRows = tblAdminFees.rows;
               //create fresh
               for (i = 0; i < oRows.length - 1; i++){
                  if (oRows[i].cells[4].innerText.Trim() != ""){
                     var oAdminFee = xmlFees.createElement("AdminFee");
                     oAdminFee.setAttribute("ClientFeeID") = iFeeID;
                     oAdminFee.setAttribute("AdminFeeCD") = oRows[i].cells[4].innerText;
                     oAdminFee.setAttribute("Amount") = oRows[i].cells[2].innerText;
                     if (oRows[i].cells[1].innerText.indexOf("Flat") != -1){
                        oAdminFee.setAttribute("MSA", "Flat");
                        oAdminFee.setAttribute("State", "Flat");
                     } else if (oRows[i].cells[1].innerText.indexOf("MSA") != -1){
                        oAdminFee.setAttribute("MSA", oRows[i].cells[5].innerText);
                        oAdminFee.setAttribute("State", "");
                     } else if (oRows[i].cells[1].innerText.indexOf("State") != -1){
                        oAdminFee.setAttribute("MSA", "");
                        oAdminFee.setAttribute("State", oRows[i].cells[5].innerText);
                     }
                     //alert(oAdminFee.xml);
                     xmlFees.documentElement.appendChild(oAdminFee);
                  }
               }
              }
            }
          }
        }
        //window.clipboardData.setData("Text", xmlFees.xml);
      } catch (e) {
        ClientError("updateXML(): " + e.description);
      }
    }

    function ServiceSelectionChanged(objRow){
      try {
        if (objRow && objRow.tagName == "TD") {
          var iServiceID = objRow.parentElement.lastChild.innerText;
          var iServiceSelected = 0;
          var chk = document.all["chkServices" + iServiceID];
          if (chk) {
            iServiceSelected = chk.value;
            if (iServiceSelected == 0) {
              chk = document.all["chkServicesRequired" + iServiceID];
              if (chk && chk.value == 1)
                chk.value = iServiceSelected;
              chk = document.all["chkServicesInvoiceable" + iServiceID];
              if (chk && chk.value == 1)
                chk.value = iServiceSelected;
              if (iServiceID == 3)
               tabAdminFees.CCDisabled = true;
            } else {
              if (iServiceID == 3)
               tabAdminFees.CCDisabled = false;
            }
          }
        }
      } catch (e) {
        ClientError("ServiceSelectionChanged(): " + e.description)
      } finally {
      }
    }

    function btnAddNewFeeClick(){
      try {
        if (validateFeeInfo() == false) return;
        if (tblSort2) {
          var oNewRow = tblSort2.insertRow();
          oNewRow.style.cssText = "height:21px;cursor:hand;";
          oNewRow.onclick = new Function ("", "FeeGridClick(this)");

          var oCell = oNewRow.insertCell();
          oCell.className = "GridTypeTD";
          oCell.style.cssText = "text-align:left;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;width:245px;";
          oCell.innerText = " ";

          oCell = oNewRow.insertCell();
          oCell.className = "GridTypeTD";
          oCell.style.textAlign = "right";
          oCell.innerText = " ";

          oCell = oNewRow.insertCell();
          oCell.innerText = "New" + oNewRow.rowIndex; //new clientFeeID: "New" + rowindex
          oNewRow.scrollIntoView(false);
          FeeGridClick(oNewRow);
          iNewCount++;
        }
      } catch (e) {
        ClientError("btnAddFeeClick(): " + e.description);
      } finally {
      }
    }

    function btnDeleteFeeClick(){
      try {
        if (curFeeRow) {
          var sRet = YesNoMessage("Confirmation", "Do you want to mark the selected fee item for deletion?.\nNote: You will have click the Save button to save the deleted fee information.");
          if (sRet != "Yes")
              return;

          var iFeeID = curFeeRow.lastChild.innerText;
          if (iFeeID != "") {
            if (xmlFees) {
              oNode = xmlFees.selectSingleNode("/Root/ClientFee[@ClientFeeID='" + iFeeID + "']");
              if (oNode) {
                var iDisplayOrder = parseInt(oNode.getAttribute("DisplayOrder"), 10);
                oNode.setAttribute("EnabledFlag", "0");
                oNode.setAttribute("DisplayOrder", "0");
                //update the display order
                updateDisplayOrder();
              }
              //window.clipboardData.setData("Text", xmlFees.xml);
              var iRowIndex = curFeeRow.rowIndex;
              var iRowMax = tblSort2.rows.length;
              tblSort2.deleteRow(curFeeRow.rowIndex);
              curFeeRow = null;
              //alert(iRowIndex + "; " + iRowMax);
              switch (iRowIndex) {
                case 0:
                  if (tblSort2.rows.length > 0)
                    FeeGridClick(tblSort2.rows[0]);
                  else {
                    setCanDirty(false);
                    resetInfo();
                    tabFees2.contentSaved();
                    btnDeleteFee.setAttribute("CCDisabled", true, 0);
                  }
                  break;
                case iRowMax:
                  FeeGridClick(tblSort2.rows[tblSort2.rows.length - 1]);
                  break;
                default:
                  FeeGridClick(tblSort2.rows[iRowIndex - 1]);
                  break;
              }
              if (iFeeID.indexOf("New") != -1)
                iNewCount--;
            }
          }
        }
      } catch (e) {
        ClientError("btnDeleteFeeClick(): " + e.description);
      } finally {
        //alert(xmlFees.xml);
      }
    }

    function btnSaveFeeClick(){
      if (curFeeRow) {
        feeStat.Show("Saving Fee changes...");
        window.setTimeout("btnSaveFeeClick2()", 150);
      }
    }

    function btnSaveFeeClick2(){
      try {
        if (xmlFees) {
          //update any changes made to the current row.
          if (curFeeRow) {
            if (!validateFeeInfo()) return false;
            updateXML(curFeeRow);
          }

          var oClientFees = xmlFees.selectNodes("/Root/ClientFee");
          var sRequest = "";
          var aRequests = new Array();
          if (oClientFees) {
            for (var i = 0; i < oClientFees.length; i++) {
              var iFeeID = oClientFees[i].getAttribute("ClientFeeID");
              var sProc = "";
              sRequest = "";
              if (iFeeID.indexOf("New") == -1) {
                sProc = "uspAdmInsuranceFeeUpdDetail";
                sRequest += "ClientFeeID=" + iFeeID + "&";
              } else
                sProc = "uspAdmInsuranceFeeInsDetail";

              sRequest += "ClaimAspectTypeID=" + oClientFees[i].getAttribute("ClaimAspectTypeID") +
                          "&InsuranceCompanyID=" + iInsuranceID +
                          "&CategoryCD=" + oClientFees[i].getAttribute("CategoryCD") +
                          "&Description=" + escape(oClientFees[i].getAttribute("Description")) +
                          "&DisplayOrder=" + oClientFees[i].getAttribute("DisplayOrder") +
                          "&EnabledFlag=" + oClientFees[i].getAttribute("EnabledFlag") +
                          "&FeeAmount=" + oClientFees[i].getAttribute("FeeAmount") +
                          "&HideFromInvoiceFlag=" + oClientFees[i].getAttribute("HideFromInvoiceFlag") +
                          "&FeeInstructions=" + escape(oClientFees[i].getAttribute("FeeInstructions")) +
                          "&ItemizeFlag=" + oClientFees[i].getAttribute("ItemizeFlag") +
                          "&InvoiceDescription=" + escape(oClientFees[i].getAttribute("InvoiceDescription")) +
                          "&EffectiveStartDate=" + (oClientFees[i].getAttribute("EffectiveStartDate") ? oClientFees[i].getAttribute("EffectiveStartDate") : "") +
                          "&EffectiveEndDate=" + (oClientFees[i].getAttribute("EffectiveEndDate") ? oClientFees[i].getAttribute("EffectiveEndDate") : "") +
                          "&AppliesWhenCD=" + oClientFees[i].getAttribute("AppliesWhenCD") +
                          "&SysLastUserID=" + parent.curUser +
                          "&SysLastUpdatedDate=" + oClientFees[i].getAttribute("SysLastUpdatedDate");
              var oIncludedServices = xmlFees.selectNodes("/Root/IncludedServices[@ClientFeeID='" + iFeeID + "']");
              if (oIncludedServices) {
                var sIncludedServices = "";
                for (var j = 0; j < oIncludedServices.length; j++) {
                  sIncludedServices += oIncludedServices[j].getAttribute("ServiceID") + "," +
                                       oIncludedServices[j].getAttribute("RequiredFlag") + "," +
                                       oIncludedServices[j].getAttribute("ItemizeFlag") + ";";
                }
                //remove the last semi-colon
                if (sIncludedServices != "")
                  sIncludedServices = sIncludedServices.substr(0, sIncludedServices.length - 1);
                sRequest += "&IncludedServices=" + sIncludedServices;
              }

              var oAdminFees = xmlFees.selectNodes("/Root/AdminFee[@ClientFeeID='" + iFeeID + "']");
              if (oAdminFees) {
                var sAdminFees = "";
                for (var j = 0; j < oAdminFees.length; j++) {
                  sAdminFees += oAdminFees[j].getAttribute("AdminFeeCD") + "," +
                                       oAdminFees[j].getAttribute("Amount") + "," +
                                       oAdminFees[j].getAttribute("MSA") + "," +
                                       oAdminFees[j].getAttribute("State") + ";";
                }
                //remove the last semi-colon
                if (sAdminFees != "")
                  sAdminFees = sAdminFees.substr(0, sAdminFees.length - 1);
                sRequest += "&AdminFees=" + sAdminFees;
              }

              aRequests.push( { procName : sProc,
                                method   : "ExecuteSpNpAsXML",
                                data     : sRequest }
                            );
            }

            var sXMLRequest = makeXMLSaveString(aRequests);
            //alert(sXMLRequest); return;
            var objRet = XMLSave(sXMLRequest);
            
            if (objRet && objRet.code == 0 && objRet.xml) {
              document.location.reload();
            }
          }
        }
      } catch (e) {
        ClientError("btnSaveFeeClick(): " + e.description);
      } finally {
        feeStat.Hide();
      }
    }

    function FeeDescChange(){
      if (curFeeRow && !bLoadingFees) {
        txtInvDesc.value = curFeeRow.cells[0].innerText = txtFeeDescription.value;
      }
    }

    function feeAmountChange(){
      if (curFeeRow && !bLoadingFees) {
        curFeeRow.cells[1].innerText = txtCurAmount.value;
      }
    }

    function isDirty() {
      return (tabFees2.isDirty || gbDirty);
    }

    function validateFeeInfo() {
      if (curFeeRow) {
        if (txtFeeDescription.value.trim() == "") {
          ClientWarning("Fee description is a required field and cannot be empty. Please try again.");
          txtFeeDescription.setFocus();
          return false;
        }

        if (rbCategoryGrp.value == 0 || rbCategoryGrp.value == null) {
          ClientWarning("Category is a required field. Please select one of the options and try again.");
          return false;
        }

        if (rbAspectGrp.value == 99 || rbAspectGrp.value == null) {
          ClientWarning("Aspect type is a required field. Please select one of the options and try again.");
          return false;
        }

        if (txtCurAmount.value == "") {
          ClientWarning("Fee amount is a required field. Please try again.");
          txtCurAmount.setFocus();
          return false;
        }

        if(txtFeeEffectiveFrom.date && txtFeeEffectiveTo.date) {
          if (txtFeeEffectiveTo.date < txtFeeEffectiveFrom.date) {
            ClientWarning("Fee Effective To date cannot be before the From date");
            txtFeeEffectiveTo.setFocus();
            return false;
          }
        }
        
        if (csApplies.selectedIndex == -1) {
          ClientWarning("Please select a value for 'Effective Date compares to' from the list.");
          return false;
        }
        
        if (aServiceIDs) {
          var bServiceChecked = false;

          for (i = 0; i < aServiceIDs.length; i++) {
            var chk = document.all["chkServices" + aServiceIDs[i]];
            if (chk && chk.value == 1) {
              bServiceChecked = true;
              break;
            }
          }
          if (bServiceChecked == false) {
            ClientWarning("Please select at least one Service and try again.");
            tabFees2.SelectTab(1);
            return false;
          }
        }
        var oRows = tblAdminFees.rows;
        for (var i = 0; i < oRows.length - 1; i++){
         strPaidBy = oRows[i].cells[4].innerText;
         strAmount = oRows[i].cells[2].innerText;
         strAppliesTo = oRows[i].cells[5].innerText;
         if (strAppliesTo.trim() == ""){
            ClientWarning("System Error. Unable to determine the 'Based On' value for " + oRows[i].cells[1].innerText);
            return false;
         }
         if (parseFloat(strAmount) >= 0 && strPaidBy.trim() == ""){
            ClientWarning("Paid By setting is missing for " + oRows[i].cells[1].innerText);
            return false;
         }
         if (strPaidBy.trim() != "" && strAmount == ""){
            ClientWarning("Admin fee not defined for " + oRows[i].cells[1].innerText);
            return false;
         }
        }
      }
      return true;
    }

    function moveRow(dir){
    	//get the table name of the row that was clicked
        var oRow = curFeeRow;
        var retRow = null;
    	  var oTable = oRow.parentElement.parentElement;
        var iRows = oTable.rows.length;
        var idx = oRow.rowIndex;

        switch (dir){
            case -1: // move up
                retRow = oTable.moveRow(idx, idx-1);
                break;
            case 1: // move down
                retRow = oTable.moveRow(idx, idx+1);
                break;
        }
        oRow.scrollIntoView(false);
        setMoveUpDown(oTable, oRow);
        updateDisplayOrder();
        gbDirty = true;
    }

    function setMoveUpDown(oTbl, oRow){

        if (sCRUD.indexOf("U") == -1) return;

        var iRows = oTbl.rows.length;
        var idx = oRow.rowIndex + 1;
        if (idx == 1 && iRows == 1) {
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveUp.disabled = true;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveDown.disabled = true;
            return;
        }

        if (idx == 1 && idx < iRows){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveUp.disabled = true;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
            return;
        }

        if (idx == iRows && idx > 0){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveDown.disabled = true;
            return;
        }

        if (idx > 1 && idx < iRows) {
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
            return;
        }
    }

    function updateDisplayOrder() {
      feeStat.Show("Updating display order...");
      window.setTimeout("updateDisplayOrder2()", 150);
    }

    function updateDisplayOrder2() {
      try {
        if (curFeeRow) {
          var oTable = curFeeRow.parentElement.parentElement;
          if (oTable && xmlFees) {
            var oRows = oTable.rows;
            var iRows = oRows.length;
            for (var i = 0; i < iRows; i++) {
              var iFeeID = oRows[i].lastChild.innerText;
              var oClientFeeNode = xmlFees.selectSingleNode("/Root/ClientFee[@ClientFeeID='" + iFeeID + "']");
              if (oClientFeeNode) {
                oClientFeeNode.setAttribute("DisplayOrder", oRows[i].rowIndex + 1);
              }
            }
          }
        }
      } catch (e) {
        ClientError("updateDisplayOrder(): " + e.description);
      } finally {
        feeStat.Hide();
        gbDirty = true;
      }
    }
    
    function refreshDetails(){
      if (curFeeRow) {
        if (sCRUD.indexOf("U") == -1)
          tabFeeDetail2.disablePage(true);
        else
          tabFeeDetail2.disablePage(false);
        if (sCRUD.indexOf("R") != -1)
          tabFeeServices.disablePage(false);
      }
    }
    
    function refreshServices(){
      if (curFeeRow) {
        if (sCRUD.indexOf("U") == -1)
          disableServices();
        else
          tabFeeServices.disablePage(false);
        if (sCRUD.indexOf("R") != -1)
          tabFeeDetail2.disablePage(false);
      }
    }
    
    function disableServices(){
      if (tabFeeServices.isReady)
        tabFeeServices.disablePage(true);
      else
        setTimeout("disableServices()", 250);
    }
    
    function refreshAdminFee(){
    }
	
	function btnCopyFeeClick(){
		try {
        if (xmlFees) {
          var sRequest = "";
          var aRequests = new Array();
          var iFeeID;
          
          if (curFeeRow) {
            var iFeeID = curFeeRow.lastChild.innerText;
            if (iFeeID.indexOf("New") != -1) {
              ClientWarning("Cannot copy the newly created fee. Please save the changes and try again.");
              return;
            }
            
            if (iNewCount > 0){
              ClientWarning("Please save the new fee changes and try again.");
              return;
            }
            
            if (iFeeID != "") {
              var sProc = "uspAdmInsuranceFeeCopy";
              
              sRequest += "ClientFeeID=" + iFeeID +
                          "&SysLastUserID=" + parent.curUser;
                 
                                     
              aRequests.push( { procName : sProc,
                                method   : "ExecuteSpNpAsXML",
                                data     : sRequest }
                            );
                            
              var sXMLRequest = makeXMLSaveString(aRequests);
              
              var objRet = XMLSave(sXMLRequest);
              
              if (objRet && objRet.code == 0 && objRet.xml) {
                document.location.reload();
              }              
            }
          }
          else{
            ClientWarning("Please select the fee you wish to copy.");
          }
        }
      } catch (e) {
        ClientError("btnCopyFeeClick(): " + e.description);
      } finally {
        feeStat.Hide();
      }	
	}
   
   function showPaidBy(){
      blnRefreshing = true;
      divAdminFeeAmount.style.display = "none";
      var oTD = event.srcElement;
      var iTop, iLeft;
      iTop = oTD.offsetTop - tblAdminFees.parentElement.scrollTop + 58;
      iLeft = oTD.offsetLeft - tblAdminFees.parentElement.scrollLeft + 347;
      divAdminFeePaidBy.style.display = "inline";
      divAdminFeePaidBy.style.top = iTop;
      divAdminFeePaidBy.style.left = iLeft;
      divAdminFeePaidBy.style.width = oTD.offsetWidth;
      divAdminFeePaidBy.style.height = oTD.offsetHeight;
      csPaidBy.value = oTD.parentElement.cells[4].innerText;
      csPaidBy.setFocus();
      oCurAdminTD = oTD;
      blnRefreshing = false;
   }
   
   function showAdminFeeAmount(){
      blnRefreshing = true;
      divAdminFeePaidBy.style.display = "none";
      var oTD = event.srcElement;
      var iTop, iLeft;
      iTop = oTD.offsetTop - tblAdminFees.parentElement.scrollTop + 58;
      iLeft = oTD.offsetLeft - tblAdminFees.parentElement.scrollLeft + 358;
      divAdminFeeAmount.style.display = "inline";
      divAdminFeeAmount.style.top = iTop;
      divAdminFeeAmount.style.left = iLeft;
      divAdminFeeAmount.style.width = oTD.offsetWidth;
      divAdminFeeAmount.style.height = oTD.offsetHeight;
      txtAdminAmount.value = oTD.parentElement.cells[2].innerText;
      txtAdminAmount.setFocus();
      oCurAdminTD = oTD;
      blnRefreshing = false;
   }
   
   function hideDynamicEdit(){
      divAdminFeePaidBy.style.display = "none";
      divAdminFeeAmount.style.display = "none";
      oCurAdminTD = null;
   }
   
   function updateAdminFeeAmount(){
      if (oCurAdminTD){
         if (blnRefreshing) return;
         oCurAdminTD.innerText = txtAdminAmount.value;
         divAdminFeeAmount.style.display = "none";
         isAdminFeeDirty = true;
      }
   }
   
   function updatePaidBy(){
      if (oCurAdminTD){
         if (blnRefreshing) return;
         switch (csPaidBy.value){
            case "S":
               oCurAdminTD.innerText = "Shop";
               break;
            case "I":
               oCurAdminTD.innerText = "Insurance";
               break;
            default:
               oCurAdminTD.innerText = "Not Set";
               oCurAdminTD.parentElement.cells[2].innerText = "";
         }
         oCurAdminTD.parentElement.cells[4].innerText = csPaidBy.value;
         divAdminFeePaidBy.style.display = "none";
         isAdminFeeDirty = true;
      }
   }
]]>

</SCRIPT>
</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;margin:0px;" tabIndex="-1" >
  <IE:APDStatus id="feeStat" name="feeStat" width="240" height="75"/>
  <table border="0" cellspacing="0" cellpadding="1" style="table-layout:fixed">
    <colgroup>
      <col width="340px"/>
    </colgroup>
    <tr valign="top">
      <td >
        <DIV id="CPScrollTable" style="width:100%;">
            <span >
                <DIV align="right" style="height:18px;width:325px;" unselectable="on" >
                    <img id="imgMoveUp" src="/images/arrowup.gif" disabled="" title="move up the display order" style="cursor:hand;filter:Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(-1)"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<img id="imgMoveDown" src="/images/arrowup.gif" disabled="" title="move down the display order" style="cursor:hand;filter:FlipV Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(1)"/>
                </DIV>
                <TABLE unselectable="on" class="ClaimMiscInputs" width="325px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                    <colgroup>
                        <col width="250px"/>
                        <col width="75px"/>
                    </colgroup>
                    <TR unselectable="on" class="QueueHeader" style="height:28px">
                        <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Name </TD>
                        <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Amount </TD>
                    </TR>
                </TABLE>
            </span>
            <DIV unselectable="on" class="autoflowTable" style="width: 325px; height: 172px;overflow:auto;" id="grid1" name="grid1">
                <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="300px" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;" disabled="true">
                    <colgroup>
                        <col width="247px"/>
                        <col width="55px"/>
                        <col width="0px" style="display:none"/>
                        <col width="0px" style="display:none"/>
                    </colgroup>
                    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                        <xsl:for-each select="ClientFee">
                          <xsl:sort select="@DisplayOrder" data-type="number"/>
                            <xsl:call-template name="ClientFee"></xsl:call-template>
                        </xsl:for-each>
                    </TBODY>
                </TABLE>
            </DIV>
        </DIV>
      </td>
    </tr>
  </table>
  <span style="position:absolute;top:0px;left:548px;">
  	<IE:APDButton id="btnCopyFee" name="btnCopyFee" value="Copy" CCDisabled="false" onButtonClick="btnCopyFeeClick()" title="Create a copy of this fee and its related services"/>
    <IE:APDButton id="btnAddFee" name="btnAddFee" value="New" CCDisabled="false" onButtonClick="btnAddNewFeeClick()" title="Add new fee"/>
    <IE:APDButton id="btnDeleteFee" name="btnDeleteFee" value="Delete" CCDisabled="true" onButtonClick="btnDeleteFeeClick()" title="Delete selected fee"/>
    <IE:APDButton id="btnSaveFee" name="btnSaveFee" value="Save" CCDisabled="false" onButtonClick="btnSaveFeeClick()" title="Save fee changes"/>
  </span>
  <span style="position:absolute;top:3px;left:340px">
        <IE:APDTabGroup id="tabFees2" name="tabFees2" width="620px" height="217px" preselectTab="0" many="true" showADS="false" onTabLoadComplete="pageInit()" onTabBeforeDeselect="hideDynamicEdit()">
          <IE:APDTab id="tabFeeDetail2" name="tabFeeDetail2" caption="Details" tabPage="divFeeDetailTab" onTabSelect="refreshDetails()" />
          <IE:APDTab id="tabFeeServices" name="tabFeeServices" caption="Services" tabPage="divFeeServices" onTabSelect="refreshServices()" />
          <IE:APDTab id="tabAdminFees" name="tabAdminFees" caption="Admin Fee" CCDisabled="true" tabPage="divAdminFee" onTabSelect="refreshAdminFee()"/>
          <!-- Tab Page definitions -->
          <IE:APDTabPage name="divFeeDetailTab" id="divFeeDetailTab" style="display:none">
          <TABLE border="0" cellspacing="0" cellpadding="2" style="margin:0px;padding-top:0px;padding-bottom:0px;table-layout:fixed;" unselectable="on">
            <colgroup>
              <col width="85px"/>
              <col width="*"/>
            </colgroup>
              <TR>
                  <TD unselectable="on"  >Description:</TD>
                  <TD unselectable="on">
                      <IE:APDInput id="txtFeeDescription" name="txtFeeDescription" value="" size="48" maxLength="50" canDirty="false" CCTabIndex="11" onchange="FeeDescChange()"/>
                  </TD>
              </TR>
              <TR>
                  <TD unselectable="on"  >Category:</TD>
                  <TD unselectable="on">
                      <IE:APDRadioGroup id="rbCategoryGrp" name="rbCategoryGrp" canDirty="false">
                        <IE:APDRadio id="rbHandlingFee" name="rbHandlingFee" caption="Handling Fee" value="0" groupName="rbCategoryGrp" checkedValue="1" uncheckedValue="0" CCTabIndex="12" onChange="" />
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        <IE:APDRadio id="rbAddtlFee" name="rbAddtlFee" caption="Additional Fee" value="0" groupName="rbCategoryGrp" checkedValue="2" uncheckedValue="0" CCTabIndex="12" onChange="" />
                      </IE:APDRadioGroup>
                  </TD>
              </TR>
              <TR>
                  <TD unselectable="on"  >Aspect Type:</TD>
                  <TD unselectable="on">
                      <IE:APDRadioGroup id="rbAspectGrp" name="rbAspectGrp" canDirty="false">
                        <xsl:if test="count(ClaimAspectTypes[@ClaimAspectTypeName='Claim']) = 1 and contains('C,B', @BillingModelCD)">
                        <IE:APDRadio id="rbAspectClaim" name="rbAspectClaim" caption="Claim" value="99" groupName="rbAspectGrp" uncheckedValue="99" CCTabIndex="13" onChange="" >
                          <xsl:attribute name="checkedValue"><xsl:value-of select="ClaimAspectTypes[@ClaimAspectTypeName='Claim']/@ClaimAspectTypeID"/></xsl:attribute>
                        </IE:APDRadio>
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        </xsl:if>
                        <xsl:if test="count(ClaimAspectTypes[@ClaimAspectTypeName='Vehicle']) = 1 and contains('E,B', @BillingModelCD)">
                        <IE:APDRadio id="rbAspectVehicle" name="rbAspectVehicle" caption="Vehicle" value="99" groupName="rbAspectGrp" uncheckedValue="99" CCTabIndex="13" onChange="" >
                          <xsl:attribute name="checkedValue"><xsl:value-of select="ClaimAspectTypes[@ClaimAspectTypeName='Vehicle']/@ClaimAspectTypeID"/></xsl:attribute>
                        </IE:APDRadio>
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        </xsl:if>
                        <xsl:if test="count(ClaimAspectTypes[@ClaimAspectTypeName='Property']) = 1 and contains('E,B', @BillingModelCD)">
                        <IE:APDRadio id="rbAspectProperty" name="rbAspectProperty" caption="Property" value="99" groupName="rbAspectGrp" uncheckedValue="99" CCTabIndex="13" onChange="">
                          <xsl:attribute name="checkedValue"><xsl:value-of select="ClaimAspectTypes[@ClaimAspectTypeName='Property']/@ClaimAspectTypeID"/></xsl:attribute>
                        </IE:APDRadio>
                        </xsl:if>
                      </IE:APDRadioGroup>
                  </TD>
              </TR>
              <TR>
                  <TD unselectable="on" >Amount:</TD>
                  <TD unselectable="on">
                      <IE:APDInputCurrency id="txtCurAmount" name="txtCurAmount" value="" precision="12" scale="2" canDirty="false" CCTabIndex="15" onChange="feeAmountChange()" />
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      <IE:APDCheckBox id="chkInvoiceable" name="chkInvoiceable" caption="Invoiceable" value="0" width="" CCTabIndex="16" canDirty="false"  />
                  </TD>
              </TR>
              <TR valign="top">
                  <TD >Instructions:</TD>
                  <TD unselectable="on">
                      <IE:APDTextArea id="txtInstructions" name="txtInstructions" maxLength="250" width="260" height="19" canDirty="false" CCTabIndex="16" />
        
                  </TD>
              </TR>
              <TR>
                  <TD >
                    <IE:APDCheckBox id="chkInvoice" name="chkInvoice" caption="Itemize" value="0" CCTabIndex="17" canDirty="false" onChange="updateServiceInvoiceable()"/>
                  </TD>
                  <TD unselectable="on">
                    Invoice Description:<br/>
                      <IE:APDTextArea id="txtInvDesc" name="txtInvDesc" maxLength="500" width="260" height="19" canDirty="false" CCTabIndex="18" />
                  </TD>
              </TR>
              <TR>
                  <TD >Effective From:</TD>
                  <TD unselectable="on">
                      <IE:APDInputDate id="txtFeeEffectiveFrom" name="txtFeeEffectiveFrom" type="date" futureDate="true" canDirty="false" CCTabIndex="19" />
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      To:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      <IE:APDInputDate id="txtFeeEffectiveTo" name="txtFeeEffectiveTo" type="date" futureDate="true" canDirty="false" CCTabIndex="20" />
                  </TD>
              </TR>
              <TR style="padding:0px;padding-left:2px;">
                  <TD >Effective Date compared to:</TD>
                  <TD unselectable="on">
                      <IE:APDCustomSelect id="csApplies" name="csApplies" canDirty="false" CCTabIndex="21" >
                        <xsl:for-each select="/Root/AppliesWhen">
                      	  <IE:dropDownItem value="C" style="display:none">
                            <xsl:attribute name="value"><xsl:value-of select="@Code"/></xsl:attribute>
                            <xsl:value-of select="@Name"/> Creation</IE:dropDownItem>
                        </xsl:for-each>
                      </IE:APDCustomSelect>
                  </TD>
              </TR>
          </TABLE>
          </IE:APDTabPage>
        
          <IE:APDTabPage id="divFeeServices" name="divFeeServices" style="display:none">
            <xsl:variable name="BillingModelCD2">
              <xsl:choose>
                <xsl:when test="@BillingModelCD = 'B'">C,E</xsl:when>
                <xsl:otherwise><xsl:value-of select="@BillingModelCD"/></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <TABLE unselectable="on" class="ClaimMiscInputs" width12="325px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;margin:0px;">
                <colgroup>
                    <col width="255px"/>
                    <col width="40px"/>
                    <col width="45px"/>
                </colgroup>
                <TR unselectable="on" class="QueueHeader" style="height:28px">
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Service </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Requi red </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Item-ize </TD>
                </TR>
            </TABLE>
            <span style="width:358px;height:155px;overflow:auto;">
            <table border="0" cellspacing="0" cellpadding="2" class="GridTypeTable" style="table-layout:fixed" id="tblServices" name="tblServices">
              <colgroup>
                <col width="255px"/>
                <col width="40px" style="text-align:center"/>
                <col width="45px" style="text-align:center"/>
                <col style="display:none;"/>
              </colgroup>
              <xsl:for-each select="Service[contains($BillingModelCD2, @BillingModelCD)]">
              <xsl:sort select="@DisplayOrder" data-type="number"/>
              <tr style="height:21px;">
                <td class="GridTypeTD" nowrap=""  style="text-align:left">
                  <IE:APDCheckBox value="0" canDirty="false" width="255" onafterchange="ServiceSelectionChanged(this.parentElement)">
                    <xsl:attribute name="id">chkServices<xsl:value-of select="@ServiceID"/></xsl:attribute>
                    <xsl:attribute name="name">chkServices<xsl:value-of select="@ServiceID"/></xsl:attribute>
                    <xsl:attribute name="caption"><xsl:value-of select="@Name"/></xsl:attribute>
                  </IE:APDCheckBox>
                </td>
                <td class="GridTypeTD" nowrap="" >
                  <IE:APDCheckBox value="0" canDirty="false">
                    <xsl:attribute name="id">chkServicesRequired<xsl:value-of select="@ServiceID"/></xsl:attribute>
                    <xsl:attribute name="name">chkServicesRequired<xsl:value-of select="@ServiceID"/></xsl:attribute>
                  </IE:APDCheckBox>
                </td>
                <td class="GridTypeTD" nowrap="" >
                  <IE:APDCheckBox value="0" canDirty="false">
                    <xsl:attribute name="id">chkServicesInvoiceable<xsl:value-of select="@ServiceID"/></xsl:attribute>
                    <xsl:attribute name="name">chkServicesInvoiceable<xsl:value-of select="@ServiceID"/></xsl:attribute>
                  </IE:APDCheckBox>
                </td>
                <td style="display:none"><xsl:value-of select="@ServiceID"/></td>
              </tr>
              </xsl:for-each>
            </table>
            </span>
          </IE:APDTabPage>
          
          <IE:APDTabPage id="divAdminFee" name="divAdminFee" style="display:none">
            <TABLE unselectable="on" class="ClaimMiscInputs" width12="325px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;margin:0px;">
                <colgroup>
                    <col width="110px"/>
                    <col width="160px"/>
                    <col width="100px"/>
                </colgroup>
                <TR unselectable="on" class="QueueHeader" style="height:28px">
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Paid By </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Based on </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Amount </TD>
                </TR>
            </TABLE>
            <span style="width:400px;height:158px;overflow:auto;" onscroll="hideDynamicEdit()">
            <table id="tblAdminFees" name="tblAdminFees" border="1" cellspacing="1" cellpadding="2" class="GridTypeTable" style="table-layout:fixed;border-collapse:collapse">
              <colgroup>
                <col width="107px" style="text-align:left;padding-left:5px;cursor:hand"/>
                <col width="158px" style="text-align:left"/>
                <col width="98px" style="text-align:right;padding-right:5px;cursor:hand"/>
                <col style="display:none"/>
                <col style="display:none"/>
                <col style="display:none"/>
                <col style="display:none"/>
              </colgroup>
              <tr style="height:24px;" id="trAdminFeesFlat">
               <td style="text-align:left" onclick="showPaidBy()">Not Set</td>
               <td style="text-align:left">Flat Fee</td>
               <td onclick="showAdminFeeAmount()">
               </td>
               <td>1</td>
               <td></td>
               <td>Flat</td>
               <td></td>
              </tr>
              <xsl:for-each select="/Root/MSA[@Code != ' ']">
              <xsl:sort select="@Name" data-type="text"/>
              <tr id="trAdminFeeFlat" style="height:24px;">
               <xsl:attribute name="id"><xsl:value-of select="concat('trAdminFeesMSA', @Code)"/></xsl:attribute>
               <td style="text-align:left" onclick="showPaidBy()">Not Set</td>
               <td style="text-align:left"><xsl:value-of select="concat('MSA: ', @Name)"/></td>
               <td onclick="showAdminFeeAmount()"></td>
               <td><xsl:value-of select="position() + 1"/></td>
               <td></td>
               <td><xsl:value-of select="@Code"/></td>
               <td></td>
              </tr>
              </xsl:for-each>
              <xsl:variable name="myPosition" select="count(/Root/MSA[@Code != ' ']) + 1"/>
              <xsl:for-each select="/Root/States">
              <xsl:sort select="@Name" data-type="text"/>
              <tr style="height:24px;">
               <xsl:attribute name="id"><xsl:value-of select="concat('trAdminFeesState', @Code)"/></xsl:attribute>
               <td style="text-align:left" onclick="showPaidBy()">Not Set</td>
               <td style="text-align:left"><xsl:value-of select="concat('State: ', @Name)"/></td>
               <td onclick="showAdminFeeAmount()"></td>
               <td><xsl:value-of select="position() + $myPosition"/></td>
               <td></td>
               <td><xsl:value-of select="@Code"/></td>
               <td></td>
              </tr>
              </xsl:for-each>
            </table>
            </span>
         </IE:APDTabPage>
        </IE:APDTabGroup>
  </span>
  
  <div id="divAdminFeePaidBy" style="position:absolute;top:0px;left:0px;display:none">
    <IE:APDCustomSelect id="csPaidBy" name="csPaidBy" canDirty="false" CCTabIndex="32" onChange="updatePaidBy()">
      <IE:dropDownItem style="display:none" value=" ">Not Set</IE:dropDownItem>
      <xsl:for-each select="/Root/AdminFeeCD[@Code != 'M']">
    	  <IE:dropDownItem style="display:none">
          <xsl:attribute name="value"><xsl:value-of select="@Code"/></xsl:attribute>
          <xsl:value-of select="@Name"/></IE:dropDownItem>
      </xsl:for-each>
    </IE:APDCustomSelect>
  </div>
  
  <div id="divAdminFeeAmount" style="position:absolute;top:0px;left:0px;display:none">
    <IE:APDInputCurrency id="txtAdminAmount" name="txtAdminAmount" value="" precision="9" scale="2" canDirty="false" CCTabIndex="31" onchange="updateAdminFeeAmount()"/>
  </div>

  <xml id="xmlFees" name="xmlFees" ondatasetcomplete="xmlFees.setProperty('SelectionLanguage', 'XPath');">
    <xsl:copy-of select="/"/>
  </xml>
</BODY>
</HTML>
</xsl:template>

<xsl:template name="ClientFee">
    <xsl:variable name="FeeID"><xsl:value-of select="@FeeID"/></xsl:variable>
    <tr style="height:21px;cursor:hand;" onClick="FeeGridClick(this)">
        <td class="GridTypeTD" nowrap="" style="text-align:left;overflow:hidden;white-space:nowrap;text-overflow:ellipsis"><xsl:value-of disable-output-escaping="yes" select="@Description"/></td>
        <td class="GridTypeTD" nowrap="" style="text-align:right"><xsl:value-of select="user:formatcurrency(string(@FeeAmount))"/></td>
        <td><xsl:value-of select="@DisplayOrder"/></td>
        <td><xsl:value-of select="@ClientFeeID"/></td>
    </tr>
</xsl:template>


</xsl:stylesheet>
