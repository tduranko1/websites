<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  id="DamagePick2">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/">

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspClaimSearchParmGetDetailXML,ClaimSearch.xsl   -->

<HTML>
<HEAD>
<TITLE>Damage Pick</TITLE>
<!-- STYLES -->
<LINK rel="STYLESHEET" href="/css/apdcontrols.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
  var aPrior, aCurrent, sMode;
  var bInitialLoad = false;
  var sRet = "";
<![CDATA[
  function pageInit(){
    var strArgs =  window.location.search;
	 while(strArgs.charAt(0) === '?')
		{
		 strArgs = strArgs.substr(1);
		}
		//alert(strArgs);
    var strArgsAry = strArgs.split("|");
    aCurrent = strArgsAry[0].split(",");
    aPrior = strArgsAry[1].split(",");
    sMode = strArgsAry[2];
    
    bInitialLoad = true;
    window.setTimeout("initData()", 100);
    //alert(sMode);
    //alert(strArgs);
  }
  
  function initData(){
    if (sMode.indexOf("Previous") != -1)
      tabImpact.SelectTab(1);
    else
      tabImpact.SelectTab(0);
  }
  
  function showData(sTabName){
    resetSecondaryImpact();
    switch(sTabName){
      case "new":
        selPrimaryImpact.CCDisabled = false;
        for (var i = 0; i < aCurrent.length; i++){
          if (aCurrent[i].indexOf("(Primary)") != -1){
            var sImpact = aCurrent[i].replace(/\(Primary\)/g, "");
            selPrimaryImpact.SelectItem(null, sImpact);
          } else {
            selectSecondaryImpact(aCurrent[i].Trim());
          }
        }
        break;
      case "previous":
        selPrimaryImpact.selectedIndex = -1;
        selPrimaryImpact.CCDisabled = true;
        for (var i = 0; i < aPrior.length; i++){
          selectSecondaryImpact(aPrior[i].Trim());
        }
        break;
    }
    bInitialLoad = false;
  }
  
  function resetSecondaryImpact(){
    var oOptions = selSecondaryImpact.options;
    for (var i = 0; i < oOptions.length; i++){
      oOptions[i].selected = false;
    }
  }
  
  function selectSecondaryImpact(sTxt){
    var oOptions = selSecondaryImpact.options;
    for (var i = 0; i < oOptions.length; i++){
      if (oOptions[i].text == sTxt){
        oOptions[i].selected = true;
        break;
      }
    }
  }
  
  function updateData(){
    if (bInitialLoad) return;
    if (tabNewImpact.selected)
      sTabName = "new"
    else
      sTabName = "prior";
      
    var aData = new Array();
    var oOptions = selSecondaryImpact.options;
    var sPrimaryImpact = "";
    switch (sTabName){
      case "new":
        if (selPrimaryImpact.selectedIndex != -1) {
          aData.push(selPrimaryImpact.text + " (Primary)");
          sPrimaryImpact = selPrimaryImpact.text
        }
        
        for (var i = 0; i < oOptions.length; i++){
          if (oOptions[i].selected && oOptions[i].text != sPrimaryImpact){
            aData.push(oOptions[i].text);
          }
        }
        aCurrent = aData;
        break;
      case "prior":
        for (var i = 0; i < oOptions.length; i++){
          if (oOptions[i].selected){
            aData.push(oOptions[i].text);
          }
        }
        aPrior = aData;
        break;
    }
  }
  
  function doSave(){
    updateData();
    sRet = aCurrent.join(", ") + ", |" + aPrior.join(", ") + ", ";
    window.close();
  }
  
  function doCancel(){
    sRet = "";
    window.close();
  }
  
  function onClose(){
    //window.returnValue = sRet;
	window.opener.getImpactData(sRet);
  }

]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="border:0px;padding:5px" onload="pageInit()" onbeforeunload="onClose()">
<IE:APDTabGroup id="tabImpact" name="tabImpact" many="false" width="500" height="350" CCDisabled="false" CCTabIndex="-1" showADS="false" onTabBeforeDeselect="updateData()">
  <IE:APDTab id="tabNewImpact" name="tabNewImpact" caption="New Damage" CCDisabled="false" CCTabIndex="1" hidden="false" onTabSelect="showData('new')"/>
  <IE:APDTab id="tabPriorImpact" name="tabPriorImpact" caption="Previous Damage" CCDisabled="false" CCTabIndex="2" hidden="false" onTabSelect="showData('previous')"/>
</IE:APDTabGroup>
  <div name="divNewImpact" id="divNewImpact" style="position:absolute;top:30px;left:12px;height:300px;width:490px;border:0px solid #FF0000">
    <table border="0" cellpadding="5" cellspacing="0" style="width:100%">
      <colgroup>
        <col width="50%"/>
        <col width="50%"/>
      </colgroup>
      <tr>
        <td><b>Primary Impact:</b></td>
		  <td style="padding-top:10px;">
          <b>Secondary Impact:</b><br/>
          (Discrete selection = Ctrl + Item; 
           Continuous selection = Shift + Item)
        </td>
      </tr>
      <tr valign="top">
        <td>
          <IE:APDCustomSelect id="selPrimaryImpact" name="selPrimaryImpact" class="APDCustomSelect" displayCount="6" canDirty="false" CCDisabled="false" CCTabIndex="3" required="true" width="215">
          	<xsl:for-each select="/Root/Impact">
              <xsl:sort select="@DisplayOrder" data-type="number"/>
              <IE:dropDownItem style="display:none">
                <xsl:attribute name="value"><xsl:value-of select="@ImpactID"/></xsl:attribute>
                <xsl:value-of select="@Name"/>
              </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        </td>
        <td>
          <select id="selSecondaryImpact" name="selSecondaryImpact" multiple="true" size="20" style="font:11px Tahoma,Arial,Verdana;width:235px">
          	<xsl:for-each select="/Root/Impact">
              <xsl:sort select="@DisplayOrder" data-type="number"/>
              <option style="padding:3px;">
                <xsl:attribute name="value"><xsl:value-of select="@ImpactID"/></xsl:attribute>
                <xsl:value-of select="@Name"/>
              </option>
            </xsl:for-each>
          </select>
        </td>
      </tr>
    </table>
  </div>
  <div style="position:absolute;top:360px;left:360px">
    <IE:APDButton id="btnSave" name="btnSave" class="APDButton" value="Save" CCDisabled="false" CCTabIndex="5" onButtonClick="doSave()"/>
    <IE:APDButton id="btnCancel" name="btnCancel" class="APDButton" value="Cancel" CCDisabled="false" CCTabIndex="6" onButtonClick="doCancel()"/>
  </div>

</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
