<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
	id="Billing">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="ShopID"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopInsuranceGetListXML,SMTShopInsurance.xsl,436   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>
  
  <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
  
  <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>
  
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>

<!-- Page Specific Scripting -->

<script language="javascript">

var gsUserID = '<xsl:value-of select="$UserID"/>';
var gsBillingID = '<xsl:value-of select="@BillingID"/>';
var gsPageFile = "SMTShopInsurance.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function InitPage(){
	top.gsPageFile = gsPageFile;
  parent.btnDelete.style.visibility = "hidden";

  if (gsShopCRUD.indexOf("U") > -1){
    parent.btnSave.style.visibility = "visible";
	  parent.btnSave.disabled = false;
  }
  else
    parent.btnSave.style.visibility = "hidden";


	parent.tab27.className = "tabactive1";
	parent.gsPageName = "Shop Insurance";
	onpasteAttachEvents();

	var aInputs = document.all.tags("INPUT");
    for (var i=0; i < aInputs.length; i++) {
        aInputs[i].attachEvent("onclick", SetDirty);
        if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
    }
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function IsDirty(){
	Save();
}

function ProhibitDuplicates(obj){
  var sQualifier = obj.id.substr(0, 10) == "chkExclude" ? "chkInclude" : "chkExclude";
  var oChk = eval("document.all." + sQualifier + obj.InsuranceCompanyID);
  if(obj.value == 1) oChk.value = 0;
  parent.gbDirtyFlag = true;
}

function Save(){
	var obj= document.all.tblInsurance;
	var cExBox, cInBox;
	var sInsuranceExcludeIDs = "";
  var sInsuranceIncludeIDs = "";
    
	for (i=0; i<obj.rows.length;i++){
		cExBox = obj.rows[i].cells[0].children[0]
    cInBox = obj.rows[i].cells[1].children[0]
    
    if (cExBox.value == 1)
			sInsuranceExcludeIDs += cExBox.getAttribute("InsuranceCompanyID") + ",";
      
    if (cInBox.value == 1)
			sInsuranceIncludeIDs += cInBox.getAttribute("InsuranceCompanyID") + ",";
	}
  
  frmInsurance.txtInsuranceExcludeIDs.value = sInsuranceExcludeIDs;
  frmInsurance.txtInsuranceIncludeIDs.value = sInsuranceIncludeIDs;
  
	parent.btnSave.disabled = true;
	frmInsurance.action += "?mode=update";
	frmInsurance.submit();
	parent.gbDirtyFlag = false;
}

]]>

</script>

</HEAD>

<body class="bodyAPDsub" onload="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>


<div style="background-color:#FFFAEB; height:450px;">
  <IMG src="/images/spacer.gif" width="6" height="6" border="0" />

  <div style="position:absolute; left:10; top:10;">
    <b>Include</b> - This is an <b>EXCLUSIVE</b> include.  If Include is checked for any insurance company, then this shop may only appear as a 
    search result for insurance companies where Include is checked.  If Include is not checked for any insurance company, then this shop may appear 
    as a search result for any insurance company where Exclude is not checked.
    <br/>
    <b>Exclude</b> - This shop will not appear as a search result for any insurance company where Exclude is checked.
  </div>
  
  <div style="position:absolute; left:100;top:60;">
    <div id="divDiscountHeaders" style="position:absolute;width:500px;height:25px;top:15;z-index:100;">
      <table  cellpadding="2" cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" >
         <tr>
           <td width="100" class="TableSortHeader" style="cursor:default">
             <xsl:attribute name="title">Exclude this shop from receiving work from 'checked' insurance companies.</xsl:attribute>
             Exclude
           </td>
           <td width="100" class="TableSortHeader" style="cursor:default">
             <xsl:attribute name="title">Include this shop in 'checked' insurance companies' pool of work assignment shops.</xsl:attribute>
             Include
           </td>
           <td width="300" class="TableSortHeader" style="cursor:default">InsuranceCompany</td>
        </tr>
      </table>
    </div>
  
    <div id="divInsurance" class="autoflowdiv" style="position:absolute; height:375; width:517; top:33;">
      <table id="tblInsurance" cellpadding="2"  cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" WIDTH="500" nowrap="on">
        <colgroup>
          <col width="100" align="center"/>
          <col width="100" align="center"/>
          <col width="300"/>
        </colgroup>
        <xsl:apply-templates select="Insurance"/>
      </table>
    </div>
  </div>
  
  <!--
  <div style="position:absolute; top:410;left:94;width:517;">
    <table border="0" width="500">
      <colgroup>
        <col width="100" align="center"/>
        <col width="100" align="left"/>
        <col width="300"/>
      </colgroup>
      <tr>
        <td>
          <IE:APDButton value="Fill" width="45"></IE:APDButton>
          <IE:APDButton value="Clear" width="45"></IE:APDButton>
        </td>
        <td>
          <IE:APDButton value="Fill" width="45"></IE:APDButton>
          <IE:APDButton value="Clear" width="45"></IE:APDButton>
        </td>
        <td></td>
      </tr>
    </table>
  </div>
  -->
  
  <form id="frmInsurance" method="post" style="overflow:hidden"> 
    <input type="hidden" id="txtInsuranceExcludeIDs" name="InsuranceExcludeIDs"/>
    <input type="hidden" id="txtInsuranceIncludeIDs" name="InsuranceIncludeIDs"/>
    <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate"/>
    
    <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
      <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
    </input>
    <input type="hidden" id="txtShopID" name="ShopID">
      <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
    </input>
    <input type="hidden" id="txtCallBack" name="CallBack"></input>
  </form>
</div>

<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</body>
</HTML>
</xsl:template>

<xsl:template match="Insurance">
  <tr>
    <xsl:attribute name="bgcolor">
	    <xsl:choose>
  		<xsl:when test="position() mod 2 = 1"><xsl:text>#fff7e5</xsl:text></xsl:when>
  		<xsl:otherwise><xsl:text>white</xsl:text></xsl:otherwise>
  	  </xsl:choose>
  	</xsl:attribute>
    <td>
      <IE:APDCheckBox canDirty="true" onchange="ProhibitDuplicates(this)">
        <xsl:if test="boolean(@ExcludeFlag=1)"><xsl:attribute name="value">1</xsl:attribute></xsl:if>
        <xsl:if test="contains($ShopCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
    	  <xsl:attribute name="id"><xsl:value-of select="concat('chkExclude', @InsuranceCompanyID)"/></xsl:attribute>
    		<xsl:attribute name="InsuranceCompanyID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:attribute>
      </IE:APDCheckBox>
	  </td>
    <td>
      <IE:APDCheckBox canDirty="true" onchange="ProhibitDuplicates(this)">
        <xsl:if test="boolean(@IncludeFlag=1)"><xsl:attribute name="value">1</xsl:attribute></xsl:if>
        <xsl:if test="contains($ShopCRUD, 'U') = false"><xsl:attribute name="CCDisabled">true</xsl:attribute></xsl:if>
    	  <xsl:attribute name="id"><xsl:value-of select="concat('chkInclude', @InsuranceCompanyID)"/></xsl:attribute>
    		<xsl:attribute name="InsuranceCompanyID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:attribute>
      </IE:APDCheckBox>
	  </td>
	  <td>
      <xsl:value-of select="@Name"/>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>
