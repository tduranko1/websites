<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ShopInfo">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="ShopCRUD" select="Shop"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserID"/>
  <xsl:param name="Deleted"/>
  <xsl:param name="mode"/>
  <xsl:param name="EntityType"/>
  <xsl:param name="BusinessInfoID"/>

  <xsl:template match="/Root">
    <xsl:if test="$Deleted='yes'">
      <script>
        var sShopID;
        for (i=0; i &lt; parent.gaShopIDs.length; i++){
        if (parent.gaShopIDs[i] != parent.gsShopID){
        sShopID = parent.gaShopIDs[i];
        break;
        }
        }
        parent.window.navigate("SMTDetailLevel.asp?SearchType=S&amp;ShopID=" + sShopID);
      </script>
    </xsl:if>


    <HTML>

      <!--            APP _PATH                            SessionKey                   USERID          SP                   XSL       PARAMETERS TO SP     -->
      <!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopInfoGetDetailXML,SMTShopInfo.xsl,2025   -->

      <HEAD>
        <TITLE>Shop Maintenance</TITLE>

        <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


        <!-- CLIENT SCRIPTS -->
        <xsl:if test="$mode != 'businfo' and $mode != 'shop'">
          <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
        </xsl:if>
        <SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
          RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
          event.returnValue=false;
          };
        </script>
        <xsl:if test="$mode != 'wizard'">
          <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
        </xsl:if>
        <!-- Page Specific Scripting -->


        <script language="javascript">

          var gsUserID = '<xsl:value-of select="$UserID"/>';
          var gsMode = '<xsl:value-of select="$mode"/>';
          var gsShopCount = '<xsl:value-of select="@ShopCount"/>';
          var gsPageFile = "SMTShopInfo.asp";
          var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';
          var giProgramManagerUserID;

          var gbProgramShop = "<xsl:choose>
            <xsl:when test="//ShopInfo/@ProgramFlag='1'">TRUE</xsl:when>
            <xsl:otherwise>FALSE</xsl:otherwise>
          </xsl:choose>";
          var gbReferralShop = "<xsl:choose>
            <xsl:when test="//ShopInfo/@ReferralFlag='1'">TRUE</xsl:when>
            <xsl:otherwise>FALSE</xsl:otherwise>
          </xsl:choose>";


          <xsl:if test="$mode != 'shop' and $mode != 'businfo'">
            <!-- don't set these in 'wizard' modes as it destroys state -->
            top.gsCancelSearchType = "S";
            top.gsBillingID = '<xsl:value-of select="ShopInfo/@BillingID"/>';
            top.gsBusinessInfoID = '<xsl:value-of select="ShopInfo/@BusinessInfoID"/>';
            top.gsShopID = '<xsl:value-of select="@ShopID"/>';
            top.gsEIN = '<xsl:value-of select="ShopInfo/@EIN"/>';

            <!-- prep data to be sent to Header Info tab -->
            top.gsInfoName = top.StringPrep('<xsl:value-of select='translate(translate(ShopInfo/@Name, "&apos;", "|"), "\", "/")'/>');
            top.gsInfoAddress = top.StringPrep('<xsl:value-of select='translate(translate(ShopInfo/@Address1, "&apos;", "|"), "\", "/")'/>' + ' ' + '<xsl:value-of select='translate(translate(ShopInfo/@Address2, "&apos;", "|"), "\", "/")'/>');

            <xsl:variable name="gsCity">
              <xsl:value-of select="normalize-space(ShopInfo/@AddressCity)"/>
              <xsl:if test="ShopInfo/@AddressCity != '' and ShopInfo/@AddressState != ''">, </xsl:if>
              <xsl:value-of select="ShopInfo/@AddressState"/>
              <xsl:if test="ShopInfo/@AddressCity != '' or ShopInfo/@AddressState != ''">
                <xsl:text>  </xsl:text>
              </xsl:if>
              <xsl:value-of select="ShopInfo/@AddressZip"/>
            </xsl:variable>

            top.gsInfoCity = top.StringPrep('<xsl:value-of select='translate(translate($gsCity, "&apos;", "|"), "\", "/")'/>');
            top.gsInfoPhone = '<xsl:value-of select="ShopInfo/@PhoneAreaCode"/>' + "-" + '<xsl:value-of select="ShopInfo/@PhoneExchangeNumber"/>' + "-" + '<xsl:value-of select="ShopInfo/@PhoneUnitNumber"/>';
            <!-- end Header data prep -->
          </xsl:if>

          <![CDATA[

function InitPage(){

	giProgramManagerUserID = frmShop.selProgramManager.value;
	
	if (parent.ShopTabChange != null && top.gsShopID == "")
		parent.ShopTabChange(parent.tab41, 'shop')

	onpasteAttachEvents();
	if (gsMode == "shop" || gsMode == "businfo"){
  		SetWizNames();
		document.all.tdShopAvailability.style.visibility="hidden";
		document.all.tdShopAvailable.style.visibility="hidden";
		document.all.tdShopNotAvailable.style.visibility="hidden";
		parent.frameLoaded(3);
	}
	else
		InitEditPage();


	//set dirty flags associated with all inputs and selects and checkboxes.
	var aInputs = null;
	var obj = null;
	aInputs = document.all.tags("INPUT");
	for (var i=0; i < aInputs.length; i++) {
		if (aInputs[i].type=="text"){
			aInputs[i].attachEvent("onchange", SetDirty);
			aInputs[i].attachEvent("onkeypress", SetDirty);
			aInputs[i].attachEvent("onpropertychange", SetDirty);
		}
        
		if (aInputs[i].type=="checkbox" || aInputs[i].type=="radio")
			aInputs[i].attachEvent("onclick", SetDirty);

		if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
	}

	aInputs = document.all.tags("SELECT");

	for (var i=0; i < aInputs.length; i++){
		aInputs[i].attachEvent("onchange", SetDirty);
		if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
	}
	
	if (gsShopCRUD.indexOf("U") != -1)
		frmShop.txtName.focus();
	else
		frmShop.txtDrivingDirections.disabled = true;

	selDataReviewStatusCD_onChange(frmShop.selDataReviewStatusCD.value);

	if (frmShop.rdProgramShop.checked == false){
		frmShop.selProgramManager.selectedIndex = 0;
	}

	if (frmShop.rdProgramShop.checked == true)
		FlagRequiredFields(1);
	else
		FlagRequiredFields(0) ;

	if (gsMode != "businfo" && gsMode != "shop" && !top.gbShopOpen) top.initSubWindows();
}

function InitEditPage(){

	top.gsPageFile = gsPageFile;
	parent.btnCancel.style.visibility = "hidden";
	
	if (gsShopCRUD.indexOf("U") > -1){
		parent.btnSave.style.visibility = "visible";
		parent.btnSave.disabled = false;
	}
	else
		parent.btnSave.style.visibility = "hidden";

	if (gsShopCount > 1 && gsShopCRUD.indexOf("D") > -1){
		parent.btnDelete.style.display = "inline";
		parent.btnDelete.style.visibility = "visible";
		parent.btnDelete.disabled = false;
	}
	else
		parent.btnDelete.style.display = "none";

	parent.tab21.className = "tabactive1";
	parent.gsPageName = "Shop Info";

	top.SetInfoHeader("  Shop ID:", top.gsShopID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone)
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function txtAddressZip_onBlur(){
	ValidateZip(frmShop.txtAddressZip, 'selState', 'txtAddressCity');
	ValidateZipToCounty(frmShop.txtAddressZip, 'txtAddressCounty');
}

function CheckAssignments(){
	if (frmShop.txtMaxWeekly.value.length > 0)
		if (isNaN(frmShop.txtMaxWeekly.value) || parseInt(frmShop.txtMaxWeekly.value) > 255 || parseInt(frmShop.txtMaxWeekly.value) < 0){
			parent.ClientWarning("Max Weekly Assignments can have a value between 0 and 255.")
			frmShop.txtMaxWeekly.focus();
			frmShop.txtMaxWeekly.select();
			event.returnValue = false;
		}
}

function CheckProgramFlag() {
	if (frmShop.txtProgramScore.value.length > ""){
		if (isNaN(frmShop.txtProgramScore.value) || (parseInt(frmShop.txtProgramScore.value) > 100) || (parseInt(frmShop.txtProgramScore.value) < 0)){
			parent.ClientWarning("Invalid program score.  Program score can have a value between 0 and 100.");
			frmShop.txtProgramScore.focus();
			frmShop.txtProgramScore.select();
			event.returnValue = false;
		}
	}
}

function NoNegatives(event){
	if (event.keyCode == 45)
		event.returnValue = false;
}

function checkNumber() {

	if (isNaN(event.srcElement.value)) {
		parent.ClientWarning("Invalid numeric or decimal value specified.");
		event.srcElement.focus();
		event.srcElement.select();
	}

	switch(event.srcElement.id){
		case "txtLatitude":
			if (Math.abs(frmShop.txtLatitude.value) > 90){
				parent.ClientWarning("Latitude must be between -90 and 90 degrees.");
				event.srcElement.select();
			}
			break;
		case "txtLongitude":
			if (Math.abs(frmShop.txtLongitude.value) > 180){
				parent.ClientWarning("Longitude must be between -180 and 180 degrees.");
				event.srcElement.select();
			}
			break;
	}
}

function btnFill_onclick(){
	if ((frmShop.txtStartTime.value != "") && (frmShop.txtEndTime.value != "")) {
		if (frmShop.txtEndTime.value <= frmShop.txtStartTime.value){
			parent.ClientWarning("The Hours of Operation Start time (left) must be less than the End time (right).");
			frmShop.txtStartTime.select();
			return;
		}

		//Set all start times from Monday to Friday
		frmShop.txtMondayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtTuesdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtWednesdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtThursdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtFridayEndTime.value = frmShop.txtEndTime.value;

		//Set all end  times from Monday to Friday
		frmShop.txtMondayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtTuesdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtWednesdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtThursdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtFridayStartTime.value = frmShop.txtStartTime.value;
	}
	else if ((frmShop.txtEndTime.value == "") && (frmShop.txtStartTime.value == "")){
		//Set all start times from Monday to Friday
		frmShop.txtMondayEndTime.value = "";
		frmShop.txtTuesdayEndTime.value = "";
		frmShop.txtWednesdayEndTime.value = "";
		frmShop.txtThursdayEndTime.value = "";
		frmShop.txtFridayEndTime.value = "";

		//Set all end  times from Monday to Friday
		frmShop.txtMondayStartTime.value = "";
		frmShop.txtTuesdayStartTime.value = "";
		frmShop.txtWednesdayStartTime.value = "";
		frmShop.txtThursdayStartTime.value = "";
		frmShop.txtFridayStartTime.value = "";
	}
	else{
		parent.ClientWarning("The Hours of Operation Start and End times must both contain valid military times or both be empty.");
		frmShop.txtStartTime.select();
	}
}

function ValidateAllPhones() {

	if (validatePhoneSections(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber, frmShop.txtPhoneUnitNumber) == false) return false;
		if (validatePhoneSections(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber, frmShop.txtFaxUnitNumber) == false) return false;
	
	if (isNumeric(frmShop.txtPhoneExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified.");
		frmShop.txtPhoneExtensionNumber.focus();
		frmShop.txtPhoneExtensionNumber.select();
		return false;
	}

	if (isNumeric(frmShop.txtFaxExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified.");
		frmShop.txtFaxExtensionNumber.focus();
		frmShop.txtFaxExtensionNumber.select();
		return false;
	}

	validateAreaCodeSplit(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber);
	validateAreaCodeSplit(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber);
	return true;
}

function ValidatePage(){

    if (frmShop.rdProgramShop.checked == false && frmShop.rdNonProgramShop.checked == false){
		parent.ClientWarning("Please indicate whether or not this is to be a Program Shop.");
		return false;
    }

    if (frmShop.rdReferralShop.checked == false && frmShop.rdNonReferralShop.checked == false){
		parent.ClientWarning("Please indicate whether or not this is to be a Referral Shop.");
		return false;
    }

	if (frmShop.txtName.value == ""){
		parent.ClientWarning("A value is required in the 'Name' field.");
		frmShop.txtName.focus();
		return false;
	}

	if (frmShop.rdProgramShop.checked == true || frmShop.rdReferralShop.checked == true){
		if (frmShop.selProgramManager.value == ""){
			parent.ClientWarning("A 'Program Manager' must be selected for Program and Referral Shops.");
			frmShop.selProgramManager.focus();
			return false;
		}

		if (frmShop.selWorkmanshipWarranty.selectedIndex <= 0){
			parent.ClientWarning("A warranty period must be selected for both 'Warranty' types for Program  and Referral Shops.");
			frmShop.selWorkmanshipWarranty.focus();
			return false;
		}

		if (frmShop.selRefinishWarranty.selectedIndex <= 0){
			parent.ClientWarning("A warranty period must be selected for both 'Warranty' types for Program  and ReferralShops.");
			frmShop.selRefinishWarranty.focus();
			return false;
		}

		if (frmShop.txtFaxAreaCode.value == "" || frmShop.txtFaxExchangeNumber.value == "" || frmShop.txtFaxUnitNumber.value == ""){
			parent.ClientWarning("A fax number is required for all Program and Referral Shops in order to submit assignments to those shops.");
			frmShop.txtFaxAreaCode.focus();
			return false;
		}

		if (frmShop.selDataReviewStatusCD.value != "A"){
			parent.ClientWarning("The Data Review Status must be set to 'Approved' for Program and Referral Shops.");
			frmShop.selDataReviewStatusCD.focus();
			return false;
		}
    }

	if (frmShop.txtProgramScore.value.length > 0){
		if (isNaN(frmShop.txtProgramScore.value) || (parseInt(frmShop.txtProgramScore.value) > 100) || (parseInt(frmShop.txtProgramScore.value) < 0)){
			parent.ClientWarning("Invalid program score.  Program score can have a value between 0 and 100.");
			frmShop.txtProgramScore.focus();
			frmShop.txtProgramScore.select();
			return false;
		}
    }

    if (frmShop.txtMaxWeekly.value.length > 0)
		if (isNaN(frmShop.txtMaxWeekly.value)){
			parent.ClientWarning("Invalid Max Weekly Assignments.");
			frmShop.txtMaxWeekly.focus();
			frmShop.txtMaxWeekly.select();
			return false;
		}

    if (frmShop.selCommMethod.value != "" && frmShop.txtPreferredCommunicationAddress.value == ""){
		parent.ClientWarning("If a communication method is selected a communication address must also be selected.");
		frmShop.txtPreferredCommunicationAddress.focus();
		return false;
    }

    if (frmShop.selCommMethod.value == "" && frmShop.txtPreferredCommunicationAddress.value != ""){
		parent.ClientWarning("Please specify a communication method.");
		frmShop.selCommMethod.focus();
		return false;
    }

    var CommMethDirty = frmShop.selCommMethod.getAttribute("dirty");
    var CommAddrDirty = frmShop.txtPreferredCommunicationAddress.getAttribute("dirty");
    
    if (CommMethDirty == "true" || CommAddrDirty == "true"){
		var coObj = RSExecute("/rs/RSValidateComm.asp", "IsValidComm", frmShop.txtShopID.value, frmShop.selCommMethod.value, frmShop.txtPreferredCommunicationAddress.value);
		retArray = ValidateRS( coObj );
		
		if (retArray[1] == 1){
			if (retArray[0] == "false"){
				var dlgFeatures = "dialogHeight:285px;dialogWidth:480px;center:yes;help:no;resizable:no;scroll:no;status:no;";
				var retVal = window.showModalDialog("SMTCommMethodConfirm.asp?ShopID=" + frmShop.txtShopID.value + "&Method=" + frmShop.selCommMethod.value + "&Addr=" + escape(frmShop.txtPreferredCommunicationAddress.value), "", dlgFeatures);

				if (retVal.toLowerCase() != "ok"){
					frmShop.txtPreferredCommunicationAddress.focus();
					return false;
				}
			}
		}
    }

	var comment = frmShop.txtDrivingDirections;
	var maxLen = comment.getAttribute("maxlength");
	  
	if (comment.value.length > maxLen){
		comment.value = comment.value.substr(0, maxLen);
		parent.ClientWarning("The maximum length for Landmarks (driving directions) is " + maxLen + ".  Your comment has been trimmed to this length.  To continue click 'Save' again.");
		return false;
	}

    //validate phone numbers
    if (ValidateAllPhones() == false)
		return false;
	
	// validate location hours
	var badHoursMsg = "The Hours of Operation Start time (left) must be less than the End time (right)."

	if ((frmShop.txtMondayStartTime.value != "" || frmShop.txtMondayEndTime.value != "") &&
		(frmShop.txtMondayEndTime.value <= frmShop.txtMondayStartTime.value || frmShop.txtMondayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtMondayStartTime.select();
			return false;
	}

	if ((frmShop.txtTuesdayStartTime.value != "" || frmShop.txtTuesdayEndTime.value != "") &&
	    (frmShop.txtTuesdayEndTime.value <= frmShop.txtTuesdayStartTime.value || frmShop.txtTuesdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtTuesdayStartTime.select();
			return false;
	}

	if ((frmShop.txtWednesdayStartTime.value != "" || frmShop.txtWednesdayEndTime.value != "") &&
		(frmShop.txtWednesdayEndTime.value <= frmShop.txtWednesdayStartTime.value || frmShop.txtWednesdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtWednesdayStartTime.select();
			return false;
	}

	if ((frmShop.txtThursdayStartTime.value != "" || frmShop.txtThursdayEndTime.value != "") &&
		(frmShop.txtThursdayEndTime.value <= frmShop.txtThursdayStartTime.value || frmShop.txtThursdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtThursdayStartTime.select();
			return false;
	}

	if ((frmShop.txtFridayStartTime.value != "" || frmShop.txtFridayEndTime.value != "") &&
		(frmShop.txtFridayEndTime.value <= frmShop.txtFridayStartTime.value || frmShop.txtFridayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtFridayStartTime.select();
			return false;
	}

	if ((frmShop.txtSaturdayStartTime.value != "" || frmShop.txtSaturdayEndTime.value != "") &&
		(frmShop.txtSaturdayEndTime.value <= frmShop.txtSaturdayStartTime.value || frmShop.txtSaturdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtSaturdayStartTime.select();
			return false;
	}

	if ((frmShop.txtSundayStartTime.value != "" || frmShop.txtSundayEndTime.value != "") &&
		(frmShop.txtSundayEndTime.value <= frmShop.txtSundayStartTime.value || frmShop.txtSundayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtSundayStartTime.select();
			return false;
	}
     
    return true;
  }

function IsDirty(){

	Save();
}

 function EmailProgramManager(){

	var iProgramManagerUserID = frmShop.selProgramManager.value;

    if (iProgramManagerUserID > 0 && iProgramManagerUserID != giProgramManagerUserID){

		var sShopInfo = frmShop.txtShopID.value  + "||" +
                    escape(frmShop.txtName.value) + "||" +
                    escape(frmShop.txtAddress1.value + " " + frmShop.txtAddress2.value) + "||" +
                    escape(frmShop.txtAddressCity.value.replace(/\s*$/, "")) + ", " +
                    escape(frmShop.selState.value) + "  " +
                    frmShop.txtAddressZip.value + "||" +
                    frmShop.txtPhoneAreaCode.value + "-" +
                    frmShop.txtPhoneExchangeNumber.value + "-" +
                    frmShop.txtPhoneUnitNumber.value;

		var co = RSExecute("/rs/RSShopMaint.asp", "NotifyProgramManager", gsUserID, iProgramManagerUserID, sShopInfo);
    	//alert(co.return_value);

		if (co.status == 0){
			sTblData = co.return_value;
			var listArray = sTblData.split("||");
			
			if ( listArray[1] == "0" ){
				ServerEvent();
			}
			else{
				parent.ClientInfo(listArray[0] + " will be notified by email of new Shop Assignment");
			}
		}
		else ServerEvent();
	}
 }

function Save(){
	parent.btnSave.disabled = true;
	parent.btnDelete.disabled = true;

    if (!ValidatePage()){
		parent.btnSave.disabled = false;
		parent.btnDelete.disabled = false;
		return false;
    }
	
    frmShop.action += "?mode=update";

    EmailProgramManager();
	
	frmShop.submit();
	parent.gbDirtyFlag = false;
}

function Delete(){

	var msg = "Are you sure you want to delete this Location?  Delete cannot be undone.  Click 'Yes' to delete.";
	var sDelete = YesNoMessage("Shop", msg)
	
	if (sDelete == "Yes"){
		parent.btnDelete.disabled = true;
		frmShop.action += "?mode=delete"
		frmShop.submit();
		parent.gbDirtyFlag = false;
  	}
}

function txtDrivingDirections_onkeypress(obj, event){

	if (obj.value.length > obj.getAttribute("maxlength") - 1){
		event.returnValue = false;
		return;
	}
}

function selDataReviewStatusCD_onChange(val){

	switch (val){
		case "U":
			frmShop.txtDataReviewUserID.value = "";
			frmShop.txtDataReviewDate.value = "";
			break;
		case "I":
		case "A":
			frmShop.txtDataReviewUserID.value = gsUserID;
			frmShop.txtDataReviewDate.value = GetUTCDate();
			break;
	}
}

function CheckSSNEIN(){

    if (document.all.rdProgramShop.checked == true) return;

    switch (gsMode){
		case "businfo":
			if (parent.document.frames["wizIframe1"].document.all.txtFedTaxID.value == ""){
				parent.ClientWarning("You must first return to Step 1 and enter a valid SSN/EIN");
				document.all.rdProgramShop.blur()
				return false;
			}
			break;
		case "shop":
			if (top.gsEIN == ""){
				var sMsg = "The Business you are attempting to add this Program Shop to does not have an EIN."
				sMsg += "  All Program Shops must be associated with a Business which has a valid EIN.  Please"
				sMsg += " Cancel the wizard, go to the Busness tab and enter a valid EIN for this Business."
				parent.ClientWarning(sMsg);
				document.all.rdProgramShop.blur()
				return false;
			}
			break;
		default:
			if (top.gsEIN == ""){
				parent.ClientWarning("You must first go to the Business tab and enter an EIN for this Business.");
				frmShop.rdProgramShop.blur()
				return false;
			}
    }
}

function  rdShopAvailable_onclick(obj) {
	frmShop.txtShopAvailable.value = obj.value;
}


function rdProgramShop_onclick(obj){

	frmShop.txtProgramFlag.value = obj.value;

	if (obj.id == "rdProgramShop")
		gbProgramShop = "TRUE";
	else
		gbProgramShop = "FALSE";


	//if (obj.id == "rdProgramShop"){
	
	if ( gbReferralShop == "TRUE" || gbProgramShop == "TRUE") {
		if (gsShopCRUD.indexOf("U") != -1){
			frmShop.selProgramManager.disabled = false;
			frmShop.txtProgramScore.disabled = false;
		  }
		FlagRequiredFields(1);
	}
	else{
		if ( gbReferralShop != "TRUE" && gbProgramShop != "TRUE") {
         frmShop.selProgramManager.selectedIndex = 0;
      }
		frmShop.selProgramManager.disabled = true;
		frmShop.txtProgramScore.disabled = true;
		FlagRequiredFields(0);
	}
}

function rdReferralShop_onclick(obj){

	frmShop.txtReferralFlag.value = obj.value;

	if (obj.id == "rdReferralShop")
		gbReferralShop = "TRUE";
	else
		gbReferralShop = "FALSE";
	

	//if (obj.id == "rdReferralShop"){
   
	if ( gbReferralShop == "TRUE" || gbProgramShop == "TRUE") {
		if (gsShopCRUD.indexOf("U") != -1){
			frmShop.selProgramManager.disabled = false;
			frmShop.txtProgramScore.disabled = false;
		  }
		FlagRequiredFields(1);
	}
	else{
		if ( gbReferralShop != "TRUE" && gbProgramShop != "TRUE") {
         frmShop.selProgramManager.selectedIndex = 0;
      }
		frmShop.selProgramManager.disabled = true;
		frmShop.txtProgramScore.disabled = true;
		FlagRequiredFields(0);
	}
}

function FlagRequiredFields(bProgramFlag){
    var sVisibility ;
    var sColor;
    var bState;

    if (gsMode == 'shop' || gsMode == 'businfo') frmShop.txtShopAvailable.value = 1;

    if (bProgramFlag == 1){
		sVisibility = "visible";
		sColor = "black";
		bState = false;
    }
    else{
		sVisibility = "hidden";
		sColor = "gray";
		bState = true;
    }

	document.all.spPgmMgrAsterick.style.visibility = sVisibility;
	document.all.spFaxAsterick.style.visibility = sVisibility;
	document.all.spWorkmanshipAsterick.style.visibility = sVisibility;
	document.all.spRefinishAsterick.style.visibility = sVisibility;
	document.all.spDataReviewAsterick.style.visibility = sVisibility;
	document.all.tdProgramManager.style.color = sColor;
	document.all.tdProgramScore.style.color = sColor;
	
	if (gsShopCRUD.indexOf("U") != -1) frmShop.selProgramManager.disabled = bState;
	if (gsShopCRUD.indexOf("U") != -1) frmShop.txtProgramScore.disabled = bState;
}


function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
}

]]>

          <xsl:if test="$mode='businfo' or $mode='shop'">
            <!-- we are in one of the wizards -->
            function SetWizNames(){
            frmShop.txtPhoneAreaCode.setAttribute("wizname", "SPhoneAreaCode");
            frmShop.txtPhoneExchangeNumber.setAttribute("wizname", "SPhoneExchangeNumber");
            frmShop.txtPhoneUnitNumber.setAttribute("wizname", "SPhoneUnitNumber");
            frmShop.txtPhoneExtensionNumber.setAttribute("wizname", "SPhoneExtensionNumber");
            frmShop.txtFaxAreaCode.setAttribute("wizname", "SFaxAreaCode");
            frmShop.txtFaxExchangeNumber.setAttribute("wizname", "SFaxExchangeNumber");
            frmShop.txtFaxUnitNumber.setAttribute("wizname", "SFaxUnitNumber");
            frmShop.txtFaxExtensionNumber.setAttribute("wizname", "SFaxExtensionNumber");
            }
          </xsl:if>

        </script>

      </HEAD>

      <body class="bodyAPDsub" onLoad="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
        <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

        <div style="background-color:#FFFAEB; height:450px;">

          <IMG src="/images/spacer.gif" width="6" height="3" border="0" />

          <form id="frmShop" method="post" style="overflow:hidden">

            <!-- Get Shop Info -->
            <xsl:apply-templates select="ShopInfo"/>

            <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
              <xsl:attribute name="value">
                <xsl:value-of select="$UserID"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
              <xsl:attribute name="value">
                <xsl:value-of select="ShopInfo/@SysLastUpdatedDate"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtShopHoursSysLastUpdatedDate" name="ShopHoursSysLastUpdatedDate">
              <xsl:attribute name="value">
                <xsl:value-of select="ShopInfo/ShopHours/@SysLastUpdatedDate"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtShopHoursSysLastUserID" name="ShopHoursSysLastUserID">
              <xsl:attribute name="value">
                <xsl:value-of select="$UserID"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtBillingID" name="BillingID">
              <xsl:attribute name="value">
                <xsl:value-of select="ShopInfo/@BillingID"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtBusinessInfoID" name="BusinessInfoID" wizname="BusinessInfoID">
              <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="$mode='businfo' or $mode='shop'">
                    <xsl:value-of select="$BusinessInfoID"/>
                  </xsl:when>
                  <!-- wizard mode -->
                  <xsl:otherwise>
                    <xsl:value-of select="ShopInfo/@BusinessInfoID"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </input>

            <input type="hidden" id="txtShopID" name="ShopID" class="inputFld" tabindex="-1" readonly="readonly" width="60" style="background-color:#eeeeee;">
              <xsl:attribute name="value">
                <xsl:value-of select="@ShopID"/>
              </xsl:attribute>
            </input>
            <input type="hidden" id="txtCallBack" name="CallBack"></input>
            <input type="hidden" id="txtEnabledFlag" wizname="SEnabledFlag" value="1"/>

            <xsl:if test="$mode='shop'">
              <!-- we are in the Shop wizard.  Add dummy fields for non defaulted parameters  -->
              <!--     in the Business Info portion of the wizard.  -->
              <input type="hidden" wizname="BBusinessTypeCD" value="C"/>
              <input type="hidden" wizname="BName" value="Bogus"/>
              <input type="hidden" wizname="BPPersonnelTypeID1" value="1"/>
              <input type="hidden" wizname="BPName1" value="BubbaBogus"/>
            </xsl:if>

          </form>
        </div>

        <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

      </body>
    </HTML>
  </xsl:template>

  <xsl:template match="ShopInfo">
    <xsl:if test="@ShopID != '0'">

      <div style="position:absolute; top:2; left:0; width:700; table-layout:fixed; overflow:hidden;">
        <table border="0" cellpadding="0" cellspacing="0">
          <colgroup>
            <col width="60px"/>
            <col width="100px"/>
            <col width="55px"/>
            <col width="75px"/>
            <col width="200px"/>
            <col width="250px"/>
          </colgroup>
          <tr style="height:10px;">
            <td style="font:bold;">Territory:</td>
            <td nowrap="nowrap">
              <span style="height:13px; overflow:hidden;">
                <xsl:choose>
                  <xsl:when test="@Territory != ''">
                    <xsl:value-of select="@Territory"/>
                  </xsl:when>
                  <xsl:otherwise>None</xsl:otherwise>
                </xsl:choose>
              </span>
            </td>
            <td nowrap="nowrap" style="font:bold; overflow:hidden;">Terr Mgr:</td>
            <td nowrap="nowrap" style="overflow:hidden;">
              <div style="white-space: nowrap; overflow:hidden;">
                <xsl:choose>
                  <xsl:when test="@TerritoryManagerUserID != '' and @TerritoryManagerUserID != '0'">
                    <xsl:value-of select="@TerritoryManager"/>
                  </xsl:when>
                  <xsl:otherwise>*Unassigned*</xsl:otherwise>
                </xsl:choose>
              </div>
            </td>
            <td align="right" nowrap="nowrap" style="overflow:hidden;">
              <b>Last Assignment Date:</b>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:choose>
                <xsl:when test="@LastAssignmentDate != ''">
                  <xsl:value-of select="concat(substring(@LastAssignmentDate, 6, 2),'/',substring(@LastAssignmentDate, 9, 2),'/',substring(@LastAssignmentDate, 1, 4))"/>
                </xsl:when>
                <xsl:otherwise>None</xsl:otherwise>
              </xsl:choose>
            </td>
            <td align="right" nowrap="nowrap" style="overflow:hidden;">
              <b>Last Reinspection Date:</b>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:choose>
                <xsl:when test="@LastReIDate != '1900-01-01T00:00:00'">
                  <xsl:value-of select="concat(substring(@LastReIDate, 6, 2),'/',substring(@LastReIDate, 9, 2),'/',substring(@LastReIDate, 1, 4))"/>
                </xsl:when>
                <xsl:otherwise>None</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="color:red; font-weight:bold;">
              <xsl:if test="@CEIProgramFlag = 1">CEI Program ID:</xsl:if>
            </td>
            <td colspan="4" style="color:red;">
              <xsl:if test="@CEIProgramFlag = 1">
                <xsl:value-of select="@CEIProgramID"/>
              </xsl:if>
            </td>
          </tr>
        </table>
      </div>

    </xsl:if>

    <TABLE cellSpacing="0" cellPadding="0" border="0" width="725" style="table-layout:fixed;">
      <!-- master table -->
      <TR>
        <colgroup>
          <col width="280"/>
          <col width="5"/>
          <col width="305"/>
          <col width="5"/>
          <col width="135"/>
        </colgroup>
      </TR>
      <TR>
        <TD>
          <IMG src="/images/spacer.gif" width="6" height="1" border="0" />
        </TD>
      </TR>

      <TR>
        <TD valign="top">
          <!-- column 1 : shop info -->
          <TABLE border="0" cellspacing="0" cellpadding="1" style="table-layout:fixed">
            <TR>
              <TD colspan="3">
                <TABLE border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed">
                  <TR>
                    <TD nowrap="nowrap" width="100" id="tdShopAvailability" style="visibility:visible;">Shop Availability:</TD>
                    <TD nowrap="nowrap" id="tdShopAvailable" style="visibility:visible;">
                      <input type="radio" id="rdShopAvailable" name="ShopAvailable" value="1" onclick="rdShopAvailable_onclick(this)" tabindex="-1">
                        <xsl:if test="@AvailableForSelectionFlag = 1">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>Yes
                    </TD>
                    <TD id="tdShopNotAvailable" style="visibility:visible;">
                      <input type="radio" id="rdShopNotAvailable" name="ShopAvailable" value="0" onclick="rdShopAvailable_onclick(this)" tabindex="-1">
                        <xsl:if test="@AvailableForSelectionFlag = 0">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>No
                      <input type="hidden" id="txtShopAvailable" name="AvailableForSelectionFlag" wizname="SAvailableForSelectionFlag" class="inputFld" onchange="SetDirty()">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@AvailableForSelectionFlag"/>
                        </xsl:attribute>
                      </input>
                    </TD>
                  </TR>

                  <TR>
                    <TD nowrap="nowrap" width="100">Program Shop:</TD>
                    <TD nowrap="nowrap">
                      <input type="radio" id="rdProgramShop" name="ProgramShop" value="1" onclick="rdProgramShop_onclick(this)" onfocus="CheckSSNEIN()" tabindex="-1">
                        <xsl:if test="@ProgramFlag = 1">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>Yes
                    </TD>
                    <TD>
                      <input type="radio" id="rdNonProgramShop" name="ProgramShop" value="0" onclick="rdProgramShop_onclick(this)" tabindex="-1">
                        <xsl:if test="@ProgramFlag = 0">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>No
                      <input type="hidden" id="txtProgramFlag" name="ProgramFlag" wizname="SProgramFlag" class="inputFld" onchange="SetDirty()">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@ProgramFlag"/>
                        </xsl:attribute>
                      </input>
                    </TD>
                  </TR>

                  <TR>
                    <TD nowrap="nowrap" width="100">Referral Shop:</TD>
                    <TD nowrap="nowrap">
                      <input type="radio" id="rdReferralShop" name="ReferralShop" value="1" onclick="rdReferralShop_onclick(this)" onfocus="CheckSSNEIN()" tabindex="-1">
                        <xsl:if test="@ReferralFlag = 1">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>Yes
                    </TD>
                    <TD>
                      <input type="radio" id="rdNonReferralShop" name="ReferralShop" value="0" onclick="rdReferralShop_onclick(this)" tabindex="-1">
                        <xsl:if test="@ReferralFlag = 0">
                          <xsl:attribute name="checked"/>
                        </xsl:if>
                      </input>No
                      <input type="hidden" id="txtReferralFlag" name="ReferralFlag" wizname="SReferralFlag" class="inputFld" onchange="SetDirty()">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@ReferralFlag"/>
                        </xsl:attribute>
                      </input>
                    </TD>
                  </TR>

                </TABLE>
              </TD>
            </TR>

            <TR>
              <colgroup>
                <col width="10"/>
                <col width="65"/>
                <col width="205"/>
              </colgroup>
              <TD>
                <strong>*</strong>
              </TD>
              <TD>Name:</TD>
              <TD nowrap="nowrap">
                <input type="text" id="txtName" name="Name" wizname="SName" autocomplete="off" size="36" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Name']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Name"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Address1:</TD>
              <TD>
                <input type="text" id="txtAddress1" name="Address1" wizname="SAddress1" size="36" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address1']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Address1"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Address2:</TD>
              <TD>
                <input type="text" id="txtAddress2" name="Address2" wizname="SAddress2" size="36" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address2']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Address2"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Zip:</TD>
              <TD noWrap="nowrap">
                <input type="text" id="txtAddressZip" name="AddressZip" wizname="SAddressZip" size="9" class="inputFld" maxlength="5" onkeypress="NumbersOnly(event)" onblur="txtAddressZip_onBlur()">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@AddressZip"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>City:</TD>
              <TD>
                <input type="text" id="txtAddressCity" name="AddressCity" wizname="SAddressCity" size="20" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCity']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@AddressCity"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>State:</TD>
              <TD nowrap="nowrap">
                <select id="selState" name="AddressState" wizname="SAddressState" class="inputFld">
                  <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='State']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current">
                        <xsl:value-of select="/Root/ShopInfo/@AddressState"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>County:</TD>
              <TD>
                <input type="text" id="txtAddressCounty" name="AddressCounty" wizname="SAddressCounty" size="20" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCounty']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@AddressCounty"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Phone:</TD>
              <TD noWrap="nowrap">
                <xsl:call-template name="ContactNumbers">
                  <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
                </xsl:call-template>
              </TD>
            </TR>
            <TR>
              <TD>
                <span id="spFaxAsterick" style="visibility:hidden; font-weight:bold">*</span>
              </TD>
              <TD>Fax:</TD>
              <TD noWrap="nowrap">
                <xsl:call-template name="ContactNumbers">
                  <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
                </xsl:call-template>
              </TD>
              <TD noWrap="nowrap"></TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Email:</TD>
              <TD noWrap="nowrap">
                <input type="text" id="txtEmailAddress" name="EmailAddress" wizname="SEmailAddress" size="36" class="inputFld" onbeforedeactivate="checkEMail(this)">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='EmailAddress']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@EmailAddress"/>
                  </xsl:attribute>
                </input>
              </TD>
              <TD noWrap="nowrap"></TD>
            </TR>
            <TR>
              <TD></TD>
              <TD noWrap="nowrap">Web Site:</TD>
              <TD noWrap="nowrap">
                <input type="text" id="txtWebSiteAddress" name="WebSiteAddress" wizname="SWebSiteAddress" size="36" class="inputFld" onbeforedeactivate="checkURL(this)">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='WebSiteAddress']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@WebSiteAddress"/>
                  </xsl:attribute>
                </input>
              </TD>
              <TD noWrap="nowrap"></TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Latitude / Longitude:</TD>
              <TD valign="bottom">
                <input type="text" id="txtLatitude" name="Latitude" wizname="SLatitude" size="12" class="inputFld" onkeypress="NumbersOnly(event)" onblur="checkNumber()">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Latitude']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Latitude"/>
                  </xsl:attribute>
                </input>
                /
                <input type="text" id="txtLongitude" name="Longitude" wizname="SLongitude" size="12" class="inputFld" onkeypress="NumbersOnly(event)" onblur="checkNumber()">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Longitude']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Longitude"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD colspan="2" noWrap="nowrap">DMV Facility #:</TD>
              <TD noWrap="nowrap">
                <input type="text" id="txtDMVFacility" name="DMVFacility" wizname="SDMVFacility" size="36" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='DMVFacilityNumber']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@DMVFacilityNumber"/>
                  </xsl:attribute>
                </input>
              </TD>
              <TD noWrap="nowrap"></TD>
            </TR>
            <TR>
              <TD colspan="2" noWrap="nowrap">Sales Tax #:</TD>
              <TD noWrap="nowrap">
                <input type="text" id="txtSalesTaxNumber" name="SalesTaxNumber" wizname="SSalesTaxNumber" size="36" class="inputFld">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='SalesTaxNumber']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@SalesTaxNumber"/>
                  </xsl:attribute>
                </input>
              </TD>
              <TD noWrap="nowrap"></TD>
            </TR>
          </TABLE>
        </TD>
        <!-- end of column 1 -->
        <TD noWrap="nowrap">
          <!-- spacer column -->
        </TD>
        <TD valign="top">
          <!-- column 2 Shop Program info -->
          <TABLE border="0" cellspacing="0" cellpadding="1" style="table-layout:fixed">
            <colgroup>
              <col width="10"/>
              <col width="140"/>
              <col width="155"/>
            </colgroup>

            <TR>
              <TD></TD>
              <TD id="tdProgramScore" nowrap="nowrap">Program Score:</TD>
              <TD nowrap="nowrap">
                <input type="text" id="txtProgramScore" name="ProgramScore" wizname="SProgramScore" size="4" class="inputFld" onChange="CheckProgramFlag();" onkeypress="NumbersOnly(event)">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='ProgramScore']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@ProgramScore"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD>
                <span id="spPgmMgrAsterick" style="visibility:hidden; font-weight:bold">*</span>
              </TD>
              <TD id="tdProgramManager" nowrap="nowrap">Program Manager:</TD>
              <TD>
                <select id="selProgramManager" name="ProgramManagerUserID" wizname="SProgramManagerUserID" class="inputFld" style="width:100%">
                  <option></option>
                  <option value="0">
                    <xsl:if test="@ProgramManagerUserID=0">
                      <xsl:attribute name="selected"/>
                    </xsl:if>
                    *Territory Manager*
                  </option>
                  <xsl:for-each select="/Root/Reference[@ListName='ProgramManager']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current" select="/Root/ShopInfo/@ProgramManagerUserID"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <!--<TD nowrap="nowrap">Certified First:</TD>
            <TD nowrap="nowrap">
              <input type="checkbox" id="cbCertifiedFirstFlag" class="inputFld" onclick="txtCertFirstFlag.value=this.checked?1:0">
			          <xsl:if test="@CertifiedFirstFlag = 1"><xsl:attribute name="checked"/></xsl:if>
			        </input>
              <input type="hidden" id="txtCertFirstFlag" class="inputFld" name="CertifiedFirstFlag" wizname="SCertifiedFirstFlag">
			          <xsl:attribute name="value"><xsl:value-of select="@CertifiedFirstFlag"/></xsl:attribute>
			        </input>
            </TD>-->
              <TD nowrap="nowrap"></TD>
              <TD nowrap="nowrap">
                <input type="hidden" id="txtCertFirstFlag" class="inputFld" name="CertifiedFirstFlag" wizname="SCertifiedFirstFlag" value="0"></input>
              </TD>
            </TR>
            <!--<TR>
            <TD></TD>-->
            <!--<TD nowrap="nowrap">Certified First ID:</TD>
            <TD nowrap="nowrap">
              <input type="text" id="txtCertifiedFirstID" name="CertifiedFirstID" wizname="SCertifiedFirstId" class="inputFld" size="15">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='CertifiedFirstID']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@CertifiedFirstID"/></xsl:attribute>
			        </input>
            </TD>-->
            <!--<TD nowrap="nowrap"></TD>
            <TD nowrap="nowrap">
				<input type="hidden" id="txtCertifiedFirstID" name="CertifiedFirstID" wizname="SCertifiedFirstId" class="inputFld" size="15" value=""></input>
            </TD>
          </TR>-->
            <TR>
              <TD>
                <span id="spWorkmanshipAsterick" style="visibility:hidden; font-weight:bold">*</span>
              </TD>
              <TD nowrap="nowrap">Workmanship Warranty:</TD>
              <TD>
                <select id="selWorkmanshipWarranty" name="WarrantyPeriodWorkmanshipCD" wizname="WarrantyPeriodWorkmanshipCD" class="inputFld">
                  <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodWorkmanshipCD"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD>
                <span id="spRefinishAsterick" style="visibility:hidden; font-weight:bold">*</span>
              </TD>
              <TD nowrap="nowrap">Refinish Warranty:</TD>
              <TD>
                <select id="selRefinishWarranty" name="WarrantyPeriodRefinishCD" wizname="WarrantyPeriodRefinishCD" class="inputFld">
                  <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodRefinishCD"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap">Max Weekly Assignments:</TD>
              <TD nowrap="nowrap">
                <input type="text" id="txtMaxWeekly" name="MaxWeeklyAssignments" wizname="SMaxWeeklyAssignments" size="15" class="inputFld" onkeypress="NumbersOnly(event)" onblur="CheckAssignments()">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='MaxWeeklyAssignments']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@MaxWeeklyAssignments"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD>
                <span id="spDataReviewAsterick" style="visibility:hidden; font-weight:bold">*</span>
              </TD>
              <TD nowrap="nowrap">
                <label id="lblDataReviewStatus">Data Review Status:</label><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </TD>
              <TD>
                <select id="selDataReviewStatusCD" name="DataReviewStatusCD" wizname="SDataReviewStatusCD" class="inputFld" onChange="selDataReviewStatusCD_onChange(this.value);">
                  <xsl:for-each select="/Root/Reference[@ListName='DataReviewStatus']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current">
                        <xsl:choose>
                          <xsl:when test="/Root/ShopInfo/@DataReviewStatusCD != ''">
                            <xsl:value-of select="/Root/ShopInfo/@DataReviewStatusCD"/>
                          </xsl:when>
                          <xsl:otherwise>N</xsl:otherwise>
                        </xsl:choose>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
                <input type="hidden" id="txtDataReviewUserID" name="DataReviewUserID" wizname="SDataReviewUserID" class="inputFld">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@DataReviewUserID"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" id="txtDataReviewDate" name="DataReviewDate" wizname="SDataReviewDate">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@DataReviewDate"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Pref Estimate Package:</TD>
              <TD>
                <select id="selPreferredEstimatePackageID" name="PreferredEstimatePackageID" wizname="SPreferredEstimatePackageID" class="inputFld">
                  <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='EstimatePackage']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredEstimatePackageID"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD>Communication Method:</TD>
              <TD>
                <select id="selCommMethod" name="PreferredCommunicationMethodID" wizname="SPreferredCommunicationMethodID" dirty="false" class="inputFld" onchange="this.dirty='true'">
                  <option></option>
                  <xsl:for-each select="/Root/Reference[@ListName='CommunicationMethod']">
                    <xsl:call-template name="BuildSelectOptions">
                      <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredCommunicationMethodID"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </select>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap">Communication Address:</TD>
              <TD>
                <input type="text" id="txtPreferredCommunicationAddress" name="PreferredCommunicationAddress" wizname="SPreferredCommunicationAddress" size="16" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='PreferredCommunicationAddress']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@PreferredCommunicationAddress"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap">AutoVerse ID:</TD>
              <TD>
                <input type="text" id="txtAutoVerseId" name="AutoVerseId" wizname="SAutoVerseId" size="16" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AutoVerseId']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@AutoVerseId"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>

              <TD></TD>
              <TD nowrap="nowrap">
                <input type="hidden" id="txtPPGCTSCustomerFlag" class="inputFld" name="PPGCTSCustomerFlag" wizname="SPPGCTSCustomerFlag" value="0"></input>
              </TD>
              <TD nowrap="nowrap">
                <input type="hidden" id="txtPPGCTSFlag" class="inputFld" name="PPGCTSFlag" wizname="SPPGCTSFlag" value="0"></input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap"></TD>
              <TD>
                <input type="hidden" id="txtPPGCTSID" name="PPGCTSID" wizname="SPPGCTSID" size="24" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'" value=""></input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap"></TD>
              <TD>
                <input type="hidden" id="selPPGCTSLevel" name="PPGCTSLevelCD" wizname="SPPGCTSLevelCD" dirty="false" value=""></input>
                <!--<select id="selPPGCTSLevel" name="PPGCTSLevelCD" wizname="SPPGCTSLevelCD" dirty="false" sclass="inputFld" onchange="this.dirty='true'">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='PPGCTSLevel']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@PPGCTSLevelCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>-->
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap">Registration #:</TD>
              <TD>
                <input type="text" id="txtRegistrationNumber" name="RegistrationNumber" wizname="SRegistrationNumber" size="24" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='RegistrationNumber']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@RegistrationNumber"/>
                  </xsl:attribute>
                </input>
              </TD>
            </TR>
            <TR>
              <TD></TD>
              <TD nowrap="nowrap">Registration Exp. Dt.:</TD>
              <TD>
                <input type="text" id="txtRegistrationExpDate" name="RegistrationExpDate" wizname="SRegistrationExpDate" size="12" maxlength="10" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'" onblur="CheckDate(this)">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='RegistrationExpDate']/@MaxLength"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@RegistrationExpDate"/>
                  </xsl:attribute>
                </input>
                <img id="imgRegExpDate" src="/images/calendar.gif" onclick="ShowCalendar(this, txtRegistrationExpDate)"/>

              </TD>

            </TR>

            <!--<TR>
			<TD></TD>
			<TD nowrap="nowrap">Targeting Shop:</TD>
			
			<TD>
				<input type="checkbox" id="txtCertFirstFlag" name="CertifiedFirstFlag" class="inputFld" value="1">

					
					<xsl:if test="@CertifiedFirstFlag = 1"><xsl:attribute name ="checked">checked</xsl:attribute>
					</xsl:if>
				</input>
					
					
				
					
			</TD>
		</TR>-->


            <TR>
              <TD></TD>
              <TD valign="top" nowrap="nowrap">Targeting Comment:</TD>

              <TD>

                <!--<textarea maxlength="50" onkeyup="return ismaxlength(this)" id="txtCertifiedFirstID" name="CertifiedFirstID" wizname="SCertifiedFirstID" cols="20" rows="3"  onkeypress="this.dirty='true">					 
					<xsl:value-of select="@CertifiedFirstID"/>
				</textarea>-->


                <select id="txtCertifiedFirstID"  name="CertifiedFirstID" wizname="SCertifiedFirstID" dirty="false" class="inputFld" onchange="this.dirty='true'">
                  <option></option>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Notified'">
                      <option value="Notified" selected="selected">Notified</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Notified">Notified</option>
                    </xsl:otherwise>
                  </xsl:choose>

                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Registered'">
                      <option value="Registered" selected="selected">Registered</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Registered">Registered</option>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Phoned'">
                      <option value="Phoned" selected="selected">Phoned</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Phoned">Phoned</option>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Pending Application'">
                      <option value="Pending Application" selected="selected">Pending Application</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Pending Application">Pending Application</option>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Qualifying'">
                      <option value="Qualifying" selected="selected">Qualifying</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Qualifying">Qualifying</option>
                    </xsl:otherwise>
                  </xsl:choose>

                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Declined'">
                      <option value="Declined" selected="selected">Declined</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Declined">Declined</option>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Rejected'">
                      <option value="Rejected" selected="selected">Rejected</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Rejected">Rejected</option>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID = 'Activated'">
                      <option value="Activated" selected="selected">Activated</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="Activated">Activated</option>
                    </xsl:otherwise>
                  </xsl:choose>

                  <!--
					<xsl:attribute name="value">
						<xsl:value-of select="@CertifiedFirstID"/>
					</xsl:attribute>
-->
                  <xsl:choose>
                    <xsl:when test="@CertifiedFirstID != ''">
                      <xsl:value-of select="@CertifiedFirstID"/>
                    </xsl:when>
                  </xsl:choose>
                </select>

                <!--<input type="text" id="txtCertifiedFirstID1" name="CertifiedFirstID" wizname="SCertifiedFirstID"  style="height:60px"  maxlength="50" dirty="false" class="inputFld" onchange="this.dirty='true'" onkeypress="this.dirty='true'">
					<xsl:attribute name="value">
						<xsl:value-of select="@CertifiedFirstID"/>
					</xsl:attribute>
				</input>-->



              </TD>
            </TR>

          </TABLE>
        </TD>
        <!-- end of column 2 -->
        <TD nowrap="nowrap" width="5px">
          <!-- spacer column -->
        </TD>
        <TD valign="top">
          <!-- column 3 -->
          <TABLE cellpadding="0" cellspacing="0" border="0" style="margin:0px;margin-left:3px;">
            <TR>
              <TD>
                <xsl:apply-templates select="ShopHours"/>
              </TD>
            </TR>
          </TABLE>
        </TD>
        <!-- end of column 3 -->
      </TR>
    </TABLE>
    <!-- end of master table -->


    <!-- Landmarks -->
    <div style="position:absolute; top:400; left:10;">
      <table border="0">
        <tr valign="top">
          <td width="55">Landmarks:</td>
          <td align="left">
            <textarea id="txtDrivingDirections" name="DrivingDirections" wizname="SDrivingDirections" cols="92" rows="5" onkeypress="txtDrivingDirections_onkeypress(this, event)">
              <xsl:attribute name="maxlength">
                <xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='DrivingDirections']/@MaxLength"/>
              </xsl:attribute>
              <xsl:value-of select="@DrivingDirections"/>
            </textarea>
          </td>
        </tr>
      </table>
    </div>


    <!-- Specialties -->
    <xsl:if test="count(ShopSpecialty) > 0">
      <div style="position:absolute; left:580; top: 375;">
        <table border="0">
          <tr>
            <td nowrap="nowrap" align="center">
              <strong>
                <xsl:choose>
                  <xsl:when test="count(ShopSpecialty) = 1">Specialty</xsl:when>
                  <xsl:otherwise>Specialties</xsl:otherwise>
                </xsl:choose>
              </strong>
            </td>
          </tr>
          <tr>
            <td>
              <DIV  border="0" class="autoflowdiv" style="height:70px;width=115px;">
                <xsl:call-template name="ShopSpecialties"/>
              </DIV>
            </td>
          </tr>
        </table>
      </div>
    </xsl:if>

  </xsl:template>

  <xsl:template match="ShopHours">
    <table>
      <TR>
        <TD>
          <IMG src="/images/spacer.gif" width="6" height="4" border="0" />
        </TD>
      </TR>
      <TR>
        <TD noWrap="nowrap" align="middle" colSpan="2" class="boldtext">Hours of Operation:</TD>
      </TR>
      <TR>
        <TD nowrap="nowrap">
          <input type="button" id="btnFill" value="  Fill  " onclick="btnFill_onclick()" class="formButton"/>
        </TD>
        <TD noWrap="nowrap">
          <input type="text" id="txtStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)"/>
          -
          <input type="text" id="txtEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)"/>
        </TD>
      </TR>
      <TR>
        <TD>Mon:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtMondayStartTime" name="OperatingMondayStartTime" wizname="SHOperatingMondayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingMondayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtMondayEndTime" name="OperatingMondayEndTime" wizname="SHOperatingMondayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingMondayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Tue:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtTuesdayStartTime" name="OperatingTuesdayStartTime" wizname="SHOperatingTuesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingTuesdayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtTuesdayEndTime" name="OperatingTuesdayEndTime" wizname="SHOperatingTuesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingTuesdayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Wed:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtWednesdayStartTime" name="OperatingWednesdayStartTime" wizname="SHOperatingWednesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingWednesdayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtWednesdayEndTime" name="OperatingWednesdayEndTime" wizname="SHOperatingWednesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingWednesdayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Thu:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtThursdayStartTime" name="OperatingThursdayStartTime" wizname="SHOperatingThursdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingThursdayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtThursdayEndTime" name="OperatingThursdayEndTime" wizname="SHOperatingThursdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingThursdayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Fri:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtFridayStartTime" name="OperatingFridayStartTime" wizname="SHOperatingFridayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingFridayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtFridayEndTime" name="OperatingFridayEndTime" wizname="SHOperatingFridayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingFridayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Sat:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtSaturdayStartTime" name="OperatingSaturdayStartTime" wizname="SHOperatingSaturdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingSaturdayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtSaturdayEndTime" name="OperatingSaturdayEndTime" wizname="SHOperatingSaturdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingSaturdayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
      <TR>
        <TD>Sun:</TD>
        <TD vAlign="top" noWrap="nowrap">
          <input type="text" id="txtSundayStartTime" name="OperatingSundayStartTime" wizname="SHOperatingSundayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayStartTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingSundayStartTime"/>
            </xsl:attribute>
          </input>
          -
          <input type="text" id="txtSundayEndTime" name="OperatingSundayEndTime" wizname="SHOperatingSundayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
            <xsl:attribute name="maxlength">
              <xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayEndTime']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@OperatingSundayEndTime"/>
            </xsl:attribute>
          </input>
        </TD>
      </TR>
    </table>
  </xsl:template>

  <xsl:template name="ShopSpecialties">
    <table cellpadding="2"  cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" WIDTH="100%" nowrap="nowrap">
      <xsl:for-each select="ShopSpecialty">
        <tr>
          <xsl:attribute name="bgcolor">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">
                <xsl:text>#fff7e5</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>white</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <td width="90%">
            <xsl:value-of select="@Name"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

</xsl:stylesheet>
