﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  xmlns:user="http://mycompany.com/mynamespace"
  xmlns:js="urn:the-xml-files:xslt"
  id="AdvWorkQueueView">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library-htc.js"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="WindowID"/>
  <xsl:param name="UserId"/>

  <xsl:template match="/Root">

    <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
    <!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspClaimSearchParmGetDetailXML,ClaimSearch.xsl   -->

    <HTML>
      <HEAD>
        <TITLE>Work Queue View</TITLE>
        <!-- STYLES -->
        <LINK rel="STYLESHEET" href="/css/apdcontrols.css" type="text/css"/>
        <LINK href="/css/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <LINK href="/css/style.css" rel="stylesheet" type="text/css" />
        <LINK href="/css/prettify.css" rel="stylesheet" type="text/css" />

        <LINK rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />

        <!-- for radio buttons -->
        <STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

        <!-- CLIENT SCRIPTS -->
        <SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
        <script src="/js/jquery.multiselect.js" type="text/javascript"></script>
        <script type="text/javascript">


          document.onhelp=function(){
          RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,27);
          event.returnValue=false;
          };
        </script>
        <script type="text/javascript">
          $(document).ready(function () {
          var myselect = $('#my-select');
          $('#my-select').multiselect({
          includeSelectAlloption: true
          });
          });
          function GetSecondaryCheckListUsers() {
          var j = [];
          $('#my-select :selected').each(function (i, selected) {
          j[i] = $(selected).val();
          });
          return j;
          }

        </script>

        <!-- Page Specific Scripting -->
        <SCRIPT language="JavaScript">
          var vWindowID = "<xsl:value-of select="$WindowID"/>";
          var vUserID = "<xsl:value-of select="$UserId"/>";

          <![CDATA[

var sSearchCriteria = "";

function pageInit(){
 rbCorrectionID.checked = true;

}
function ResendUncheck()
{
if(rbResendID.checked == true)
  rbResendID.checked = false;
}
function CorrectionUncheck()
{
if(rbCorrectionID.checked == true)
  rbCorrectionID.checked = false;
}


function SendStatus(){

var ReturnAdvWorkQueueValue = '';
var SelectedValue = ''; 
   if (rbResendID.checked == true){
    SelectedValue = "Resend";
    }
   else if (rbCorrectionID.checked == true){
     SelectedValue = "Correction";
   }
    else{
        alert('Please select Resend or Correction option')
        return;
    }
   
   ReturnInvoiceStatusOptionval = { UserSeleted : SelectedValue  };
           
       window.returnValue = ReturnInvoiceStatusOptionval; 
       window.close();

}

function CancelStatus(){
var SelectedValue = "Cancel";
       ReturnAdvWorkQueueValue = { UserSeleted: SelectedValue };
           
       window.returnValue = ReturnAdvWorkQueueValue; 
       window.close();
}




]]>
        </SCRIPT>
      </HEAD>
      <style>
        table, .tablerow th , .tablerow td {
        border: 1px solid black;
        border-collapse: collapse;
        text-align : Center;
        }
        .run.th {
        padding: 5px;
        text-align: center;
        font-weight:bold;
        }
        .boldtd {
        padding: 5px;
        text-align: center;
        font-weight:bold;
        }


      </style>
      <BODY class="bodyAPDSub" unselectable="on" style="border:0px;padding:5px" onload="pageInit()">
        <p style="color:black">
          <b>Claim assignment activity indicates this bundle has been previously sent. Please select one of the bundle dispositions shown, to designate the billing intent of this current bundle.</b>
        </p>

        <table class="tablerow" style="width:100%">
          <tr class="run">
            <th style="border-top-style: none;
    border-bottom-style: none;"></th>
            <th colspan="5">Description</th>
          </tr>
          <tr>
            <td style="border-top-style: none;font-weight:bold;" >Invoice Type</td>
            <td  class="boldtd">InvoiceID</td>
            <td class="boldtd">Dispatch number</td>
            <td class="boldtd">Fee</td>
            <td class="boldtd">Indemnity</td>
          </tr>
          <tr>
            <td  style="border-top-style: none; algin:left;">
              <div align="left">
                <input type="radio" id='rbResendID' name="rbResend" onClick="CorrectionUncheck()"/>
                <lable >Resend</lable>
              </div>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@InvoiceID != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@InvoiceID"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@InvoiceID"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@DispatchNumber != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@DispatchNumber"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@DispatchNumber"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td>
              <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@Amount"/>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@Amount != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and @InvoiceType!='Correction' and @InvoiceType!='Original']/@Amount"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td style="border-top-style: none; text-align:center;">
              <div align="left">
                <input type="radio" id='rbCorrectionID' name="rbCorrectionID" onClick="ResendUncheck()"/>
                <lable>Correction</lable>
              </div>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@InvoiceID != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@InvoiceID"/>
                </xsl:when>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='F' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@InvoiceID != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@InvoiceID"/>
                </xsl:when>
              </xsl:choose>
             <!-- <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@InvoiceID"/>-->
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@DispatchNumber != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@DispatchNumber"/>
                </xsl:when>
                <xsl:when test="/Root/Invoice[@ItemTypeCD='F' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@DispatchNumber != ''">
                  <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@DispatchNumber"/>
                </xsl:when>
              </xsl:choose>
            </td>
            <td>
              <xsl:value-of select="/Root/Invoice[@ItemTypeCD='F' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@Amount"/>
            </td>
            <td>
              <xsl:value-of select="/Root/Invoice[@ItemTypeCD='I' and (@InvoiceType ='Correction' or @InvoiceType ='Original')]/@Amount"/>
            </td>
          </tr>
        </table>
        <div align="Right" style="border: 1px solid white;" >
          <IE:APDButton class="APDButton" style="align:right;" id="btnSend" name="btnSend" value="Ok" border="border: 1px solid white;" width="75" CCDisabled="false" CCTabIndex="5" onButtonClick="SendStatus()"/>
          <IE:APDButton class="APDButton"  style="align:right;" id="btnCancel" name="btnCancel" value="Cancel" border="border: 1px solid white;" width="75" CCDisabled="false" CCTabIndex="6" onButtonClick="CancelStatus()"/>
        </div>
      </BODY>
    </HTML>
  </xsl:template>

</xsl:stylesheet>
