<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="DocumentSource">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Reference"/>

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R')">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
            <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        </head>
        <body class="bodyAPDSub" unselectable="on" bgcolor="#FFFFFF">
        <center>
            <font color="#ff0000"><strong>You do not have sufficient permission to view this page.
            <br/>Please contact administrator for permissions.</strong></font>
        </center>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="InfoCRUD"/>

<HTML>

<HEAD>
<TITLE>Data Administration: Document Source</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="VBScript">
    function trimString(str)
        trimString = Trim(str)
    end function
</SCRIPT>
<SCRIPT language="Javascript">
<![CDATA[

    var curRow = null;
    var gbDirty = false;
    //var sMode = "";
    var sCRUD = "____";

    function pageInit(){
        parent.checkCorrectPage(window.location.href);
        initCRUD();
        initSelectBoxes();
        curEnabledFlagDiv.onclick = curEnabledClick;//.attachEvent("onclick", "curEnabledClick");
        selcurEstPkg.ondeactivate = curEstPkgChange;
        disableCurData(true);
    }

    function initCRUD(){
        sCRUD = CPScrollTable.getAttribute("CRUD");
        if (sCRUD.indexOf("C") == -1){
            btnAdd1.disabled = true;
            btnAdd1.style.visibility = "hidden";
        }
            //btnAdd1.disabled = true;
        if (sCRUD.indexOf("U") == -1){
            btnAdd1.disabled = true;
            btnSave1.disabled = true;
            btnAdd1.style.visibility = "hidden";
            btnSave1.style.visibility = "hidden";
            imgMoveUp.style.visibility = "hidden";
            imgMoveDown.style.visibility = "hidden";
            tblSort2.disabled = true;
        }
    }

    function curEnabledClick(){
        if (curRow){
            if (curRow.readOnly) return;
            //CheckBoxChange(curRow.cells[1].firstChild.firstChild);
            //alert(CheckBoxGetValue(curRow.cells[1].firstChild));
            if (curEnabledFlagDiv.disabled) return;

            if (curEnabledFlag.value == 1){
                curRow.cells[1].firstChild.firstChild.firstChild.src = "/images/cbcheckro.png";
                curRow.cells[1].firstChild.lastChild.value = "1";
            }
            else{
                curRow.cells[1].firstChild.firstChild.firstChild.src = "/images/cbreadonly.png";
                curRow.cells[1].firstChild.lastChild.value = "0";
            }
            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function txtCurNameChange(){
        if (curRow) {
            if (curRow.readOnly) return;

            //remove any trailing and leading spaces
            remove_XS_whitespace(txtCurName);
            if (txtCurName.value == "")
                curRow.cells[2].innerText = " "; //this will make sure that the cell has a border.
            else
                curRow.cells[2].innerText = txtCurName.value;

            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function curEstPkgChange(){
        if (curRow){
            if (curRow.readOnly) return;
            var sEstPkg = selcurEstPkg.options[selcurEstPkg.selectedIndex].innerText;
            curRow.cells[3].innerText = ((sEstPkg.length > 0) ? sEstPkg : " ");
            curRow.cells[6].innerText = selcurEstPkg.options[selcurEstPkg.selectedIndex].value;
            //window.status = selcurEstPkg.options[selcurEstPkg.selectedIndex].value;
            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function GridClick(oRow)
    {
    	if (!oRow) return;

    	//get the table name of the row that was clicked
    	var oTable = oRow.parentElement.parentElement;
    	//based on the table name update the form elements to the selectd row
    	if(oTable)
    	{
            resetRowCursor(oTable);

            oRow.cells[0].innerHTML = "<img src='/images/arrowright.gif'>"
            oRow.style.backgroundColor = "#FFD700";

            setMoveUpDown(oTable, oRow);
            //oRow.scrollIntoView(false);
            curRow = oRow;

            disableCurData(false);
            curEnabledFlagDiv.disabled = false;
            curData.disabled = true;
            clearCurData();

            //if (oRow.rowAttr != "new") {

                txtCurName.value = oRow.cells[2].innerText;

                var oEnabled = document.all["EnabledFlag" + oRow.rowNo];
                if (oEnabled){
                    if (oEnabled.value == 1){
                        if (curEnabledFlag.value == '0'){
                            CheckBoxChange(curEnabledFlagDiv.firstChild);
                            curEnabledFlagDiv.firstChild.onblur();
                        }
                    }
                    else {
                        if (curEnabledFlag.value == '1'){
                            CheckBoxChange(curEnabledFlagDiv.firstChild);
                            curEnabledFlagDiv.firstChild.onblur();
                        }
                    }
                }

                selcurEstPkg.optionselect(trimString(oRow.cells[3].innerText), null);
            //}
            if ((sCRUD.indexOf("U") == -1) || (oRow.readOnly)){
                disableCurData(true);
                curEnabledFlagDiv.disabled = true;
                return;
            }
            //curData.disabled = false;
            disableCurData(false);
            if (!curData.disabled){
                curEnabledFlagDiv.firstChild.focus();
            }
    	}
    }

    function clearCurData(){
        txtCurName.value = "";
        selcurEstPkg.optionselect(null, 0);
        if (curEnabledFlag.value == '1') {
            CheckBoxChange(curEnabledFlagDiv.firstChild);
            curEnabledFlagDiv.firstChild.onblur();
        }
    }

    function disableCurData(val){
        if (sCRUD.indexOf("U") == -1){
            btnSave1.disabled = true;
            val = true;
        }

        curData.disabled = val;
        txtCurName.disabled = val;
        selcurEstPkg.disabled = val;
        /*if (curRow){
            if (curRow.readOnly)
                btnDelete1.disabled = true;
            else
                btnDelete1.disabled = val;
        }*/

    }

    function resetRowCursor(oTbl){
        var iRows = oTbl.rows.length;
        for (var i = 0; i < iRows; i++) {
            oTbl.rows[i].cells[0].innerHTML = "&nbsp;";
            /*if (oTbl.rows[i].readOnly)
                oTbl.rows[i].style.backgroundColor = "#FFEFD5";
            else*/
                oTbl.rows[i].style.backgroundColor = "#FFFFFF";
            //oTbl.rows[i].style.color = "#000000";
        }
    }

    function setMoveUpDown(oTbl, oRow){

        if (sCRUD.indexOf("U") == -1) return;

        var iRows = oTbl.rows.length;
        var idx = -1;
        for (var i = 0; i < iRows; i++) {
            if (oTbl.rows[i] == oRow){
                idx = i + 1;
                break;
            }
        }

        if (idx == 1 && idx < iRows){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveUp.disabled = true;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
        }

        if (idx == iRows && idx > 0){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveDown.disabled = true;
        }

        if (idx > 1 && idx < iRows) {
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
        }
    }

    function moveRow(dir){
    	//get the table name of the row that was clicked
        var oRow = curRow;
        var retRow = null;
    	var oTable = oRow.parentElement.parentElement;
        var iRows = oTable.rows.length;
        var idx = -1;
        for (var i = 0; i < iRows; i++) {
            if (oTable.rows[i] == oRow){
                idx = i;
                break;
            }
        }

        switch (dir){
            case -1: // move up
                if (oTable.rows[idx].rowAttr != "new")
                    oTable.rows[idx].rowAttr = "upd";
                if (oTable.rows[idx - 1].rowAttr != "new")
                    oTable.rows[idx - 1].rowAttr = "upd";
                retRow = oTable.moveRow(idx, idx-1);
                break;
            case 1: // move down
                if (oTable.rows[idx].rowAttr != "new")
                    oTable.rows[idx].rowAttr = "upd";
                if (oTable.rows[idx + 1].rowAttr != "new")
                    oTable.rows[idx + 1].rowAttr = "upd";
                retRow = oTable.moveRow(idx, idx+1);
                break;
        }
        oRow.scrollIntoView(false);
        setMoveUpDown(oTable, oRow);
        gbDirty = true;
    }

    function btnAdd(){
        divButtons.disabled = true;
        var oNewRow = tblSort2.insertRow(-1);
        var oNewCell = null;
        for (i = 0; i < 9; i++){
            oNewCell = oNewRow.insertCell();
            oNewCell.innerHTML = "&nbsp;";
            oNewCell.className="GridTypeTD";
            if (i > 3){
                oNewCell.style.display = "none";
            }
        }
        oNewRow.unselectable="on";
        oNewRow.RowType = "DocumentSource";
        oNewRow.rowNo = oNewRow.rowIndex + 1;
        oNewRow.cells[4].innerText = oNewRow.rowIndex + 1;
        oNewRow.rowAttr = "new";
        oNewRow.onclick = new Function("", "GridClick(this)");
        oNewRow.style.height = "21px";
        oNewRow.style.cursor = "hand";
        oNewRow.cells[2].style.textAlign = "left";
        oNewRow.cells[3].style.textAlign = "left";

        var oNewChkBx = AddCheckBox("EnabledFlag", "1", "0", "0", (oNewRow.rowIndex + 1), "1", -1, 1);
        oNewRow.cells[1].innerHTML = oNewChkBx;
        GridClick(oNewRow);
        oNewRow.scrollIntoView(false);
        divButtons.disabled = false;
        //setTimeout("curEnabledFlagDiv.firstChild.focus()", 10);
    }

    function btnSave(){
    	var retArray = new Array;
        var bRefresh = false;


    	try
    	{
            var sProc, sRequest, i, iRowNo;
            var objRow, sRowAttrib;
            var sEnabled;
            var sEstimatePackageID;
            var iLength = tblSort2.rows.length;
            //validate all the rows.
            for (i=0; i < iLength; i++){
                objRow = tblSort2.rows[i];
                if (!ValidateRow(objRow)) {
                    GridClick(objRow);
                    return false;
                }
            }

            divButtons.disabled = true;
    		setTimeout(ShowSB40,1);
            var iLength = tblSort2.rows.length;
            for (i=0; i < iLength; i++){
                sProc = ""; sRequest = "";
                objRow = tblSort2.rows[i];
                sRowAttrib = objRow.getAttribute("rowAttr");
                iRowNo = objRow.getAttribute("rowNo");
                sEnabled = document.all["EnabledFlag" + iRowNo].value;
                sEstimatePackageID = (parseInt(objRow.cells[6].innerText) > 0 ? objRow.cells[6].innerText : "");

                if (sRowAttrib) {
                    switch (sRowAttrib){
                        case "new":
                            sProc = "uspRefDocumentSourceInsDetail";
                            sRequest = "EstimatePackageID=" + sEstimatePackageID + "&" +
                                       "DisplayOrder=" + (objRow.rowIndex + 1) + "&" +
                                       "EnabledFlag=" + sEnabled + "&" +
                                       "Name=" + escape(objRow.cells[2].innerText) + "&" +
                                       "SysLastUserID=" + curUser;
                            break;
                        case "del":
                            sProc = "";
                            break;
                        case "upd":
                            sProc = "uspRefDocumentSourceUpdDetail";
                            sRequest = "DocumentSourceID=" + objRow.cells[5].innerText + "&" +
                                       "EstimatePackageID=" + sEstimatePackageID + "&" +
                                       "DisplayOrder=" + (objRow.rowIndex + 1) + "&" +
                                       "EnabledFlag=" + sEnabled + "&" +
                                       "Name=" + escape(objRow.cells[2].innerText) + "&" +
                                       "SysLastUserID=" + curUser + "&" +
                                       "SysLastUpdatedDate=" + objRow.cells[8].innerText;
                            break;
                    }
                    //alert(sProc + "\n" + sRequest);
                    if (sProc != "" && sRequest != ""){
                		var coObj = RSExecute("/rs/RSADSAction.asp", "RadExecute", sProc, sRequest );
                		retArray = ValidateRS( coObj );
                        if (retArray[1] != "0"){
                            //update the last Updated date...
                            var objXML = new ActiveXObject("Microsoft.XMLDOM");
                            objXML.loadXML(retArray[0]);
                            var rootNode = objXML.documentElement.selectSingleNode("/Root");

                            var DocNode = rootNode.selectSingleNode("Document/@SysLastUpdatedDate");
                            if (DocNode){
                                var sLastUpdatedDate = DocNode.nodeValue;
                                objRow.cells[8].innerText = sLastUpdatedDate;
                            }
                            var DocNode = rootNode.selectSingleNode("Document/@DocumentSourceID");
                            if (DocNode){
                                var sDocumentSourceID = DocNode.nodeValue;
                                objRow.cells[5].innerText = sDocumentSourceID;
                            }
                            objRow.rowAttr = "";
                            gbDirty = false;
                        }
                		setTimeout(ShowSB80,2);
                    }
                }
            }
            setTimeout(ShowSB100,300);

            divButtons.disabled = false;

    		return retArray;
    	}
    	catch(e)
    	{
            alert(e.message);
    		retArray[0] = e.message;
    		retArray[1] = 0;
            divButtons.disabled = false;
    		setTimeout(ShowSB100,1);
    		return retArray;
    	}

    }

    function btnDelete(){
        /*divButtons.disabled = true;
        if(curRow){

            if (curRow.readOnly) return;

            var sSave = YesNoMessage("Are you sure?", "Do you want to delete this Document Source?");
            if (sSave == "Yes"){
                curRow.rowAttr = "del";
                curRow.style.visibility = "hidden";
                curRow.style.display = "none";
                if (curRow.rowIndex <= tblSort2.rows.length - 1 && curRow.rowIndex > 0){
                    GridClick(tblSort2.rows[curRow.rowIndex - 1])
                }
                else {
                    if (tblSort2.rows.length > curRow.rowIndex)
                        GridClick(tblSort2.rows[curRow.rowIndex + 1]);
                }
            }
        }
        divButtons.disabled = false;*/
    }

    function ValidateRow(oRow) {
        if (oRow.cells[2].innerText == " ") {
            alert("Name is required. Please enter a valid Name");
            return false;
        }
        return true;
    }

    /*function checkDirty() {
        if (gbDirty) {
            var sSave = YesNoMessage("Need to Save", "Some Information on this page has changed. Do you want to save the changes");
            if (sSave == "Yes"){
                btnSave();
            }
        }
    }*/
	
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
]]>

</SCRIPT>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden;" onload="pageInit();" topmargin="3px" leftmargin="8px" tabIndex="-1">
    <SPAN align="right" unselectable="on" id="divButtons" name="divButtons" style="position:absolute;top:3px;left:600px;">
        <img name="btnAdd1" id="btnAdd1" src="/images/but_ADD_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_ADD_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_ADD_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_ADD_down.png'" onclick="btnAdd()" style="cursor:hand"/>
        <img src="/images/spacer.gif" width="3px" height="1px"/>
        <img name="btnSave1" id="btnSave1" src="/images/but_SAVE_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_SAVE_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_SAVE_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_SAVE_down.png'" onclick="btnSave()" style="cursor:hand"/>
        <!-- <input type="button" class="formButton" name="btnAdd1" id="btnAdd1" value="Add" onclick="btnAdd()"/> -->
        <!-- <input type="button" class="formButton" name="btnUpdate1" id="btnUpdate1" value="Update" onclick="btnUpdate()"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
        <!-- <input type="button" class="formButton" name="btnDelete1" id="btnDelete1" value="Delete" onclick="btnDelete()" disabled=""/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
        <!-- <input type="button" class="formButton" name="btnSave1" id="btnSave1" value="Save" onclick="btnSave()"/> -->
        <!-- <input type="button" class="formButton" name="btnCancel1" id="btnCancel1" value="Cancel" style="visibility:hidden;" onclick="btnCancel()"/> -->
    </SPAN>
    <DIV align="right" style="height:18px;" unselectable="on">
        <img id="imgMoveUp" src="/images/arrowup.gif" disabled="" title="move up the display order" style="cursor:hand;filter:Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(-1)"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<img id="imgMoveDown" src="/images/arrowup.gif" disabled="" title="move down the display order" style="cursor:hand;filter:FlipV Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(1)"/>
    </DIV>
    <DIV id="CPScrollTable" style="width:710px" >
        <xsl:attribute name="CRUD"><xsl:value-of select="$InfoCRUD"/></xsl:attribute>
        <span >
            <TABLE unselectable="on" class="ClaimMiscInputs" width="720px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                <colgroup>
                    <col width="24px"/>
                    <col width="73px"/>
                    <col width="400px"/>
                    <col width="224px"/>
                </colgroup>
                <TR unselectable="on" class="QueueHeader" style="height:28px">
                    <TD unselectable="on" class="TableSortHeader" type="Number"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
                    <TD unselectable="on" class="TableSortHeader" type="Number"> Enabled </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Name </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Estimate Package </TD>
                </TR>
            </TABLE>
        </span>
        <DIV unselectable="on" class="autoflowTable" style="width: 720px; height: 310px;">
            <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="699px" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;">
                <colgroup>
                    <col width="21px"/>
                    <col width="73px"/>
                    <col width="398px"/>
                    <col width="194px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                </colgroup>
                <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                    <xsl:for-each select="Document"	>
                        <xsl:call-template name="DocumentRows"></xsl:call-template>
                    </xsl:for-each>
                </TBODY>
            </TABLE>
        </DIV>
    </DIV>
    <!-- <span style="border:1px solid gray;margin-top:5px;height:15x;width:15px;background-color:#A9A9A9"></span><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<strong>System Maintained</strong> -->
    <br/>
    <br/>
    <DIV unselectable="on" style="border:1px solid gray;padding:5px;width=720px" disabled="true" id="curData" name="curData">
        <span unselectable="on" style="position:relative;top:-12px;left:10px; background-color:white;padding:5px;"><strong>Current Selection</strong></span>
        <TABLE border="0" cellspacing="0" cellpadding="3" style="margin-left:15px;" unselectable="on">
            <TR>
                <TD unselectable="on" >Enabled</TD>
                <TD unselectable="on" ><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('curEnabledFlag','0','1','0','', '0', 1)"/></TD>
            </TR>
            <TR>
                <TD unselectable="on" >Name</TD>
                <TD unselectable="on" >
                    <input type="text" name="txtCurName" id="txtCurName" onchange="txtCurNameChange()">
                        <xsl:variable name="iSize"><xsl:value-of select="/Root/Metadata/Column[@Name='Name']/@MaxLength"/></xsl:variable>
                        <xsl:choose>
                            <xsl:when test="$iSize != ''">
                                <xsl:attribute name="size"><xsl:value-of select="number($iSize) + 1"/></xsl:attribute>
                                <xsl:attribute name="MaxLength"><xsl:value-of select="number($iSize)"/></xsl:attribute>
                                <xsl:attribute name="class">InputField</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="class">InputReadOnlyField</xsl:attribute>
                                <xsl:attribute name="disabled"/>
                                <xsl:attribute name="readonly"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </input>
                </TD>
            </TR>
            <TR>
                <TD unselectable="on" >Estimate Package</TD>
                <TD unselectable="on" >
                    <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('curEstPkg',2,'',0,1,5,/Root/Reference,'EstimatePackageName','EstimatePackageID',150)"/>
                </TD>
            </TR>
        </TABLE>
    </DIV>
</BODY>
</HTML>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</xsl:template>

<xsl:template name="DocumentRows">
    <TR unselectable="on" RowType="DocumentSource" onClick="GridClick(this)" style="height:21px;cursor:hand;"><!-- onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" -->
        <xsl:attribute name="rowNo"><xsl:value-of select="position()"/></xsl:attribute>
        <xsl:if test="@SysMaintainedFlag = 1">
            <xsl:attribute name="readOnly">true</xsl:attribute>
                <!-- <xsl:attribute name="style">color:#F5F5F5;</xsl:attribute> -->
        </xsl:if>
        <TD unselectable="on" class="GridTypeTD" nowrap=""><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" class="GridTypeTD" nowrap="">
        <xsl:if test="@SysMaintainedFlag = 1">
            <xsl:attribute name="style">color:#A9A9A9;</xsl:attribute>
        </xsl:if>
            <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('EnabledFlag',string(@EnabledFlag),'1','0',position(),1)"/>
        </TD>
        <TD unselectable="on" class="GridTypeTD" style="text-align:left;">
            <xsl:if test="@SysMaintainedFlag = 1">
                <xsl:attribute name="style">color:#A9A9A9;text-align:left</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@Name"/>
        </TD>
        <TD unselectable="on" class="GridTypeTD" style="text-align:left;">
            <xsl:if test="@SysMaintainedFlag = 1">
                <xsl:attribute name="style">color:#A9A9A9;text-align:left</xsl:attribute>
            </xsl:if>
            <xsl:variable name="estPackageID"><xsl:value-of select="@EstimatePackageID"/></xsl:variable>
            <xsl:variable name="estPackage"><xsl:value-of select="/Root/Reference[@EstimatePackageID=$estPackageID]/@EstimatePackageName "/></xsl:variable>
            <xsl:choose>
                <xsl:when test="$estPackage = ''">
                    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$estPackage"/>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD style="display:none;"><xsl:value-of select="@DisplayOrder"/></TD>
        <TD style="display:none;"><xsl:value-of select="@DocumentSourceID"/></TD>
        <TD style="display:none;"><xsl:value-of select="@EstimatePackageID"/></TD>
        <TD style="display:none;"><xsl:value-of select="@SysLastUserID"/></TD>
        <TD style="display:none;"><xsl:value-of select="@SysLastUpdateDate"/></TD>
    </TR>
</xsl:template>


</xsl:stylesheet>
