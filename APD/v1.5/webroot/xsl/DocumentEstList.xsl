<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimDocument">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>

<xsl:template match="/Root">

<xsl:variable name="LynxID" select="/Root/@LynxID" />

<HTML>
<HEAD>
<TITLE>Documents List</TITLE>

<SCRIPT language="JavaScript">

var gsLynxID = "<xsl:value-of select="$LynxID"/>";
var gsWindowID = "<xsl:value-of select="$WindowID"/>";
var gsUserID = "<xsl:value-of select="$UserId"/>";

<![CDATA[

  function chkEstimates( docTypeText, docSeq, estType, DocumentID, serviceChannel )
  {
    var lMessage = "";
    var tblRows = document.getElementById("tblEstimates").rows;
    var liRowsLength = tblRows.length;
    for (var i = 0; i < liRowsLength; i++)
    {
      if ( ( serviceChannel == ldTrim(tblRows[i].cells[5].innerText) ) &&
          ( docSeq == ldTrim(tblRows[i].cells[1].innerText) ) )
      {
        if (ldTrim(tblRows[i].cells[3].innerText) == estType && ldTrim(tblRows[i].cells[2].innerText) == "No")
        {
          if (ldTrim(tblRows[i].cells[0].innerText) != DocumentID)
          {
            if (docTypeText == "Supplement")
              lMessage = "An <i>" + estType + "</i> " + docTypeText + " " + docSeq + " already exists.<br>There can only be one <i>" + estType + "</i> " + docTypeText + " " + docSeq +". All other <i>" + estType + "</i> " + docTypeText + " " + docSeq + " must be marked as duplicate(s).";
            else
              lMessage = "An <i>" + estType + "</i> " + docTypeText + " already exists.<br>There can only be one <i>" + estType + "</i> " + docTypeText +". All other <i>" + estType + "</i> " + docTypeText + " must be marked as duplicate(s).";
          }
        }
      }
    }
    if (lMessage != "")
      return lMessage;
    else
      return false;
  }

  function getUpperNetTotal()
  {
    var sHighTotal = "";
    var sNewHighTotal = "";
    var tblRows = document.getElementById("tblEstimates").rows;
    var liRowsLength = tblRows.length;
    for (var i = 0; i < liRowsLength; i++)
    {
      if (ldTrim(tblRows[i].cells[2].innerText) != "Yes")
      {
        var strCellTotalVal = ldTrim(tblRows[i].cells[4].innerText);
        if (strCellTotalVal != "" || strCellTotalVal != 0)
        {
          sHighTotal = Number(sHighTotal);
          sNewHighTotal = Number(strCellTotalVal);
          if (sNewHighTotal > sHighTotal)
            sHighTotal = sNewHighTotal;
        }
      }
    }
    parent.gsHighestNetTotal = sHighTotal;
  }


  function ldTrim(strText)
  {
    while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
      strText = strText.substring(1, strText.length);

    while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
      strText = strText.substring(0, strText.length-1);

    return strText;
  }

]]>
</SCRIPT>

</HEAD>
<BODY onLoad="getUpperNetTotal()">

<TABLE id="tblEstimates" border="1">
 <TBODY>
      <xsl:for-each select="Document[@DocumentTypeID='3' or @DocumentTypeID='10']" >
        <xsl:call-template name="Documents"/>
      </xsl:for-each>
 </TBODY>
</TABLE>

</BODY>
</HTML>

</xsl:template>

  <xsl:template name="Documents">
    <TR>
      <TD><xsl:value-of select="@DocumentID"/></TD>
      <TD><xsl:value-of select="@SupplementSeqNumber"/></TD>
      <TD>
        <xsl:if test="@DuplicateFlag = '1'">Yes</xsl:if>
        <xsl:if test="@DuplicateFlag = '0'">No</xsl:if>
        <xsl:if test="@DuplicateFlag = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
      </TD>
			<TD><xsl:value-of select="/Root/Reference[@List='EstimateType' and @ReferenceID=current()/@EstimateTypeCD]/@Name"/></TD>
      <TD><xsl:value-of select="@EstimateNetAmt"/></TD>
      <TD><xsl:value-of select="@ClaimAspectServiceChannelID"/></TD>
    </TR>
  </xsl:template>

</xsl:stylesheet>
