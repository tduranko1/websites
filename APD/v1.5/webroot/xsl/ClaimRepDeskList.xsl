<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  xmlns:js="urn:the-xml-files:xslt"
  xmlns:local="http://local.com/mynamespace"
  xmlns:session="http://lynx.apd/session-manager"
  id="ClaimRepDesk">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<msxsl:script language="JScript" implements-prefix="local">

    // Returns an integer based upon date and role logic.
    function getDateRoleState( date, complete_role )
    {
        var state = 0;
        var cur_date = new Date();
        var due_date = new Date( date );

        if ( ( due_date.valueOf() - ( 3600000 * 24 ) ) &gt; cur_date.valueOf() )
            state = 4;  // overdue
        else if ( due_date.valueOf() &gt; cur_date.valueOf() )
            state = 2;  // due within 24 hours

        if ( complete_role == '0' )
            state += 1;

        return state;
    }

</msxsl:script>
<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="TaskCRUD" select="Task"/>
<xsl:param name="ReassignExposureCRUD" select="Reassign Exposure"/>
<xsl:variable name="SupervisorFlag" select="session:XslGetSession('SupervisorFlag')"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<HTML>
<HEAD>
<TITLE></TITLE>

<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid2.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/claimnavigate.js"></SCRIPT>
<script type="text/javascript"> 
      document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,3);
		  event.returnValue=false;
		  };	
</script>
 

  <style>
    .TableHeader {
      font-family: Tahoma, Verdana;
      font-size: 11px;
      font-weight:bold;
      text-align:center;
      background-color: #E4E6EC;
      color: #00008B;
      height:18px;
    }
    .TaskHeader {
      font-family: Tahoma, Verdana;
      font-size: 11px;
      font-weight:bold;
      text-align:center;
      height:18px;      
      background-color: #6495ED;
      color: #FFFFFF;
    }
    .TableData {
      font-family: Tahoma, Verdana;
      font-size: 8pt;
      height1:18px;
      border: 1px solid #C0C0C0;
      border-bottom: 1px dotted #C0C0C0;
      border-top: 0px;
    }
    .TableTaskData {
      font-family: Tahoma, Verdana;
      font-size: 8pt;
      height1:18px;
      text-align:left;
      border: 1px solid #C0C0C0;
      border-bottom: 1px solid #000000;
    }
    .HeaderServiceChannelPrimary {
      FILTER: progid:DXImageTransform.Microsoft.Gradient(gradientType=1,startColorStr=#F0E68C,endColorStr=#FFFFFF)
    }
    .HeaderServiceChannel {
      FILTER: progid:DXImageTransform.Microsoft.Gradient(gradientType=1,startColorStr=#99CCFF,endColorStr=#FFFFFF)
      ;#E6E6FA
    }
    
  </style>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
  var vUserID = "<xsl:value-of select="$UserId"/>";
  var refreshTimerID = null;
  var iTimerInterval = 60 * 60 * 1000; // 60 minutes
  var iCurPage = 1;
  var iPgCount = 1;
  var iPageSize = 10;
  var objCheckListPopup = window.createPopup();
  var vCheckListCRUD = "<xsl:value-of select="$TaskCRUD"/>";
  var vCheckListLynxID = null;
  var iRecInCurPage = 0;
  var recCount = 0;
  var iTaskRefreshedTryCount = 0;
  var iAutoRefreshCancelCounter = 15;
  var refreshConfirmTimer;
  var bSorting = false;
  var bInitialPageNav = false;
  var bMouseInToolTip = false;
  var iSearchFoundPosition = 0;
  var gsSupervisorFlag = '<xsl:value-of select="$SupervisorFlag"/>';
  var strLoginUser = "<xsl:value-of select="/Root/@UserID"/>";
  var strQueueUser = "<xsl:value-of select="/Root/@SubordinateUserID"/>";
  var strExternalNotesReviewTaskID = "<xsl:value-of select="/Root/@ExternalNotesReviewTaskID"/>";
  var strProbablyClosureReviewTaskID = "<xsl:value-of select="/Root/@ProbablyClosureReviewTaskID"/>";
  var blnPageInit = false;

  <![CDATA[
    function pageInit() {
      btnFirst.value = unescape("%u2039") + unescape("%u2039");
      btnPrev.value = unescape("%u2039");
      btnNext.value = unescape("%u203A");
      btnLast.value = unescape("%u203A") + unescape("%u203A");
      
      divAutoRefresh.style.top = (document.body.offsetHeight - 45) / 2;
      divAutoRefresh.style.left = (document.body.offsetWidth - 250) / 2;
      
      top.setSB(100,top.sb)
      //alert(gsSupervisorFlag);

      startPageRefreshTimer();
      blnPageInit = true;
    }
    
    function startPageRefreshTimer() {
      stopPageRefreshTimer();
      refreshTimerID = window.setInterval("refreshPage()", iTimerInterval );
    }
    
    function stopPageRefreshTimer() {
      if (refreshTimerID > 0)
        window.clearInterval(refreshTimerID);
    }
    
    function showRefreshConfirm() {
      divAutoRefresh.style.display = "inline";
      spnRefreshCounter.innerText = iAutoRefreshCancelCounter + " ";
      refreshConfirmTimer = window.setInterval("refreshConfirmCount()", 1000 );
    }
    
    function refreshConfirmCount() {
      if (iAutoRefreshCancelCounter == 1) {
        window.clearInterval(refreshConfirmTimer);
        if (typeof(parent.showStatus) == "function")
          parent.showStatus("Refreshing data...");
        window.setTimeout("windowReload()", 100);
        iAutoRefreshCancelCounter = 15;
        return;
      }
      iAutoRefreshCancelCounter--;
      spnRefreshCounter.innerText = iAutoRefreshCancelCounter + " ";
    }
    
    function refreshPage() {
      stopPageRefreshTimer();
      showRefreshConfirm();
    }
    
    function cancelRefresh() {
      if (refreshConfirmTimer > 0) {
        window.clearInterval(refreshConfirmTimer);
        iAutoRefreshCancelCounter = 15;
        divAutoRefresh.style.display = "none";
        stopPageRefreshTimer();
        startPageRefreshTimer();
      }
    }
    
    function initData(){
      if (xmlVehList) {
        xmlVehList.setProperty("SelectionLanguage", "XPath");
        recCount = xmlVehList.XMLDocument.firstChild.childNodes.length;
        if (recCount > 0) {
          tblData.style.display = "inline";
          try {
          tblData.dataPageSize = iPageSize;
          } catch (e){}
          tblData.dataSrc = "#xmlVehList";
        } else {
          divNoData.style.display = "inline";
          tblData.style.display = "none";
          disableControls(true);
          if (typeof(parent.hideStatus) == "function")
            parent.hideStatus();
          if (blnPageInit == false)
            window.setTimeout("pageInit()", 100);
          return;
        }

        iCurPage = 1;
        
        iPgCount = Math.ceil(recCount / iPageSize);
        
        if (iCurPage < iPgCount)
          iRecInCurPage = iPageSize;
        else
          iRecInCurPage = (recCount - (iPgCount - 1) * iPageSize);
          
        iTaskRefreshedTryCount = 1;

        txtPage.value = iCurPage + " of " + iPgCount;
        
        //parent.curPage = iCurPage;
        
        if (typeof(parent.hideStatus) == "function")
          parent.hideStatus();
          
        if ((bSorting == false) && (parent.sortColumn && parent.sortColumn != "") && (parent.sortOrder && parent.sortOrder != "")) {
          var sSortFldName = parent.sortColumn;
          if (tblHeader) {
            var oTHead = tblHeader.rows;
            if (oTHead) {
              var oCells = oTHead[0].cells;
              for (var i = 0; i < oCells.length; i++) {
                var sScript = oCells[i].onclick;
                if (sScript != null) sScript = sScript.toString();
                if (sScript != null && sScript.indexOf(sSortFldName) > 0) {
                  oTD = oCells[i];
                  oTD.sortOrder = (parent.sortOrder == "ascending" ? "descending" : "ascending");
                  break;
                }
              }
            }
          }
          sortTable('tblData', sSortFldName, parent.dataType, 'xmlVehList');
        }
        
        if ((bSorting == false) && (parent.curPage && parent.curPage > 0)) {
          //showPage(parent.curPage);
          bInitialPageNav = true;
        }
      }
    }
    
    function showHideTask(obj) {
      var oSrcElement;
      if (obj)
        oSrcElement = obj;
      else
        oSrcElement = event.srcElement;
        
      var bShow = false;

      if (oSrcElement && oSrcElement.tagName == "IMG") {
        if (oSrcElement.src.indexOf("plus.gif") == -1) {
          oSrcElement.src = "/images/plus.gif";
          bShow = false;
        } else {
          oSrcElement.src = "/images/minus.gif";
          bShow = true;
        }
        /*var sClaimAspectID = oSrcElement.parentElement.parentElement.cells[1].innerText;
        var sLynxID = oSrcElement.parentElement.parentElement.cells[2].innerText;
        var sPositionID = oSrcElement.parentElement.parentElement.cells[3].innerText;
        alert(oSrcElement.parentElement.parentElement.tagName);*/
        
        var oRow = oSrcElement.parentElement.parentElement.parentElement.rows[0];
        var strClaimAspectServiceChannelID = oRow.cells[2].innerText;
        //alert(strClaimAspectServiceChannelID);
        
        try {
        var oDivTask = oSrcElement.parentElement.parentElement.parentElement.rows[1].cells[1].firstChild;
        if (oDivTask) {
          if (bShow) {
            //oDivTask.style.display = "inline";
            oDivTask.style.display = "inline";
            oDivTask.firstChild.dataSrc = "#xmlVehTask" + strClaimAspectServiceChannelID;
          } else {
            oDivTask.style.display = "none";
            oDivTask.firstChild.dataSrc = "";
          }
        }
        } catch (e) {alert(e.description)}
      }
    }
    
    function showPage(sVal) {
      var bPageMoved = false;
      if (tblData && tblData.dataSrc != "") {
        cancelRefresh();
        switch(sVal) {
          case -1: //move to first page
            if (iCurPage > 1) {
              //disableControls(true);
              //reset the task detail
              resetTask();
              
              tblData.firstPage();
              iCurPage = 1;
              bPageMoved = true;
            }
            break;
          case -2: //move to previous page
            if (iCurPage > 1) {
              //disableControls(true);
              //reset the task detail
              resetTask();
              
              tblData.previousPage();
              iCurPage--;
              bPageMoved = true;
            }
            break;
          case -3: //move to next page
            if (iCurPage < iPgCount) {
              //disableControls(true);
              //reset the task detail
              resetTask();
              
              tblData.nextPage();
              iCurPage++;
              bPageMoved = true;
            }
            break;
          case -4: //move to last page
            if (iCurPage < iPgCount) {
              //disableControls(true);
              //reset the task detail
              resetTask();
              
              for (var i = 0; i < (iPgCount - iCurPage); i++) {
                tblData.nextPage();
              }
              
              //tblData.lastPage();
              iCurPage = iPgCount;
              bPageMoved = true;
            }
            break;
          default: //move to any page
            //reset the task detail
            resetTask();
            var iPgDiff = parseInt(sVal, 10) - iCurPage;
            if (iPgDiff > 0) {
              for (var i = 0; i < iPgDiff; i++)
                 tblData.nextPage();
              bPageMoved = true;
            } else {
              iPgDiff *= -1;
              for (var i = 0; i < iPgDiff; i++)
                 tblData.previousPage();
              bPageMoved = true;
            }
            iCurPage = parseInt(sVal, 10);
            break;
        }

        parent.curPage = iCurPage;
        
        if (iCurPage < iPgCount)
          iRecInCurPage = iPageSize;
        else
          iRecInCurPage = (recCount - (iPgCount - 1) * iPageSize);
          
        iTaskRefreshedTryCount = 1;
        if (bPageMoved == true) {
          //txtPage.CCDisabled = false;
          if (iCurPage > iPgCount) iCurPage = iPgCount;
          txtPage.value = iCurPage + " of " + iPgCount;
          
          //txtPage.CCDisabled = true;
          //disableControls(false);
        }
      }
    }
    
    function disableControls(bDisabled) {
      btnFirst.CCDisabled = btnPrev.CCDisabled =  btnNext.CCDisabled =
      btnLast.CCDisabled = txtPage.CCDisabled = txtRecPerPage.CCDisabled = 
      btnGoPage.disabled = bDisabled;
    }
    
    function tblDataReadyStateChange(){
      if (tblData.readyState == "complete") {
        if (bInitialPageNav == true && parent.curPage != 1) {
          bInitialPageNav = false;
          showPage(parent.curPage);
          return;
        } else 
          bInitialPageNav = false;
        //window.setTimeout("refreshTasks()", 100);
        window.setTimeout("refreshSC()", 100);
        window.setTimeout("pageInit()", 100);
      }
    }
    
    function tblSCReadyStateChange(oSrc){
      if (oSrc.readyState != "complete") return;
      var oRows = oSrc.rows;
	   if (oRows) {
         for (var i = 0; i < oRows.length; i+=2){
            var oRow = oRows[i];
            var strClaimAspectServiceChannelID = oRow.cells[2].innerText;
   	      var strOverDueCount = oRow.cells[3].innerText;
   	      var strDueTodayCount = oRow.cells[4].innerText;
   	      var strSCPrimary = oRow.cells[5].innerText;
   	      if (strClaimAspectServiceChannelID != ""){
            
              var oImg = oRow.cells[0].firstChild;
              if (oImg && oImg.tagName == "IMG") {
                  if (parseInt(strOverDueCount, 10) > 0 || parseInt(strDueTodayCount, 10) > 0){
      	              showHideTask(oImg);
      	              oImg.onclick = "";
      	              oImg.style.cursor = "default";
                  } else {
      	            oImg.onclick = showHideTask;
      	            oImg.style.cursor = "hand";
                  }
              }
   	        if (strSCPrimary == "1"){
   	          oRow.cells[1].className = "HeaderServiceChannelPrimary";
   	        } else {
   	          oRow.cells[1].className = "HeaderServiceChannel";
   	        }
   	      }
         }
	   }
    }
    
    function resetTask() {
      var oImgTask = document.getElementsByName("imgTaskDisplay");
      if (oImgTask) {
        for (var i = 0; i < oImgTask.length; i++) {
          if (oImgTask[i].src.indexOf("minus.gif") > 0) {
            showHideTask(oImgTask[i]);
            //var oDivTask = oImgTask[i].nextSibling.nextSibling.nextSibling;
            //oDivTask.firstChild.dataSrc = "";
          }
        }
      }
    }
    
    function refreshSC(){
      var oTREntity = tblData.tBodies;
      var sAltText = "";
      if (oTREntity) {
         initAPDControlObjectHierarchy(tblData);
        for (var i = 0; i < oTREntity.length; i++) {
          var otblSC = oTREntity[i].rows[5].cells[0].firstChild;
          var sClaimAspectID = oTREntity[i].rows[0].cells[8].innerText;
          if (otblSC){
            otblSC.dataSrc = "#xmlVehSC" + sClaimAspectID;
          }
        }
      }
    }
    
    function refreshTasks() {
      var oTREntity = tblData.tBodies;
      var sAltText = "";
      if (oTREntity) {
        /*if (iRecInCurPage != oTREntity.length && iTaskRefreshedTryCount < 2) {
          window.setTimeout("refreshTasks()", 250);
          iTaskRefreshedTryCount++;
          return;
        }*/

        for (var i = 0; i < oTREntity.length; i++) {
          var iOverDue = oTREntity[i].rows[0].cells[9].innerText;
          var iDueToday = oTREntity[i].rows[0].cells[10].innerText;
          var oImg = oTREntity[i].rows[1].cells[0].firstChild;
          if (parseInt(iOverDue, 10) > 0 || parseInt(iDueToday, 10) > 0) {
            if (oImg && oImg.tagName == "IMG") {
              showHideTask(oImg);
              oImg.onclick = "";
              oImg.style.cursor = "default";
            }
          } else {
            oImg.onclick = showHideTask;
            oImg.style.cursor = "hand";
          }
          
        }
      }
    }

    function markManagerialTasks(oTbl){
      var strColor = "";
      var strFontWeight = "";
      var iManagerialTask;
      var iTaskID;
      if (oTbl && oTbl.readyState == "complete" && oTbl.dataSrc != ""){
        var oTaskRows = oTbl.rows;
        if (oTaskRows.length > 0){
          for (var i = 1; i < oTaskRows.length; i++){
              iManagerialTask = oTaskRows[i].cells[13].innerText;
              iTaskID = oTaskRows[i].cells[14].innerText;
              
              if (iManagerialTask == "1"){
                  strColor = "#0000FF";
              } else if (iTaskID == strExternalNotesReviewTaskID){
                  strColor = "#008000";
              } else if (iTaskID == strProbablyClosureReviewTaskID){
                  strColor = "#A52A2A";
              } else {
                  strColor = "#000000";
              }
              strFontWeight = ((iManagerialTask == "1") || (iTaskID == strExternalNotesReviewTaskID || iTaskID == strProbablyClosureReviewTaskID) ? "bold" : "normal");
              var oCells = oTaskRows[i].cells;
              for (j = 1 ; j < 3; j++){
                if (oCells[j].firstChild.tagName == "SPAN"){
                  oCells[j].firstChild.style.fontWeight = strFontWeight;
                  oCells[j].firstChild.style.color = strColor;
                }
              }
          }
        }
      }
    }
    
    function NavToClaim() {
      event.cancelBubble = true;
      if (event.srcElement.tagName == "IMG") {
        var oTR = event.srcElement;
        var iMax = 10;
        do {
          oTR = oTR.parentElement;
          iMax--;
        } while (oTR.tagName != "TR" && iMax > 0)
        
        
        //inner table
        oTR = oTR.parentElement.parentElement.parentElement;
        
        //now get the row containing this inner table
        var iMax = 10;
        do {
          oTR = oTR.parentElement;
          iMax--;
        } while (oTR.tagName != "TR" && iMax > 0)

        if (oTR && oTR.tagName == "TR") {
          var sLynxID = oTR.cells[11].innerText;
          if (sLynxID != "") {
            NavOpenWindow( "claim", sLynxID, "" );
          }
        }
      }
    }
    
    function NavToEntity(obj) {
      var oTR;
      event.cancelBubble = true;
      if (obj.tagName == "TR") {
        if (obj.name == "trTaskRow") 
          oTR = obj.previousSibling.previousSibling.previousSibling.previousSibling.previousSibling;
        else if (obj.name == "trTaskRow0") 
          oTR = obj.previousSibling.previousSibling;
        else if (obj.name == "trTaskRow1") 
          oTR = obj.previousSibling.previousSibling.previousSibling.previousSibling;
        else
          oTR = obj;

        var sLynxID = oTR.cells[11].innerText;
        var sEntityNum = oTR.cells[12].innerText;
        var sEntityType = oTR.cells[13].innerText;
        if (sLynxID != "" && sEntityNum != "" && sEntityType != "") {
          NavOpenWindow( sEntityType, sLynxID, sEntityNum );
        }
      }
    }
    
    function NavToEntity_Task(obj) {
      event.cancelBubble = true;
      var oTR = obj;
      if (oTR && oTR.tagName == "TR") {
        var sLynxID = oTR.cells[9].innerText;
        var sEntityNum = oTR.cells[10].innerText;
        var sEntityType = oTR.cells[7].innerText;
        if (sLynxID != "" && sEntityNum != "" && sEntityType != "") {
          NavOpenWindow( sEntityType, sLynxID, sEntityNum );
        }
      }
    }
    
    function showContext() {
      event.cancelBubble = true;
      event.returnValue = false;
      var oTR = event.srcElement;
      var iMax = 10;
      do {
        oTR = oTR.parentElement;
        iMax--;
      } while (oTR.tagName != "TR" && iMax > 0)
      
      if (oTR && oTR.tagName == "TR") {
        var sEntityType = oTR.cells[7].innerText;
        var sTaskID = oTR.cells[8].innerText;
        var sLynxID = oTR.cells[9].innerText;
        var sEntityNum = oTR.cells[10].innerText;
        var sState = oTR.cells[11].innerText;
        var sAlarmModificationFlag = oTR.cells[12].innerText;
        if (sEntityType != "")
          checkListContextMenu(sEntityType, sLynxID, sEntityNum, sTaskID, sState, sAlarmModificationFlag);
      }
    }

    function checkListContextMenu( strEntity, strLynxID, strNumber, strTaskID, intState, sAlarmModificationFlag )  {
      var lefter = event.offsetX + 10;
      var topper = event.offsetY + 10;
  
      var oDropDownNode = checkListPopupMenu.cloneNode(true);
      
      var oEditTaskNode = oDropDownNode.firstChild.cells[0];
      if (oEditTaskNode.tagName != "TD") return;

      if (sAlarmModificationFlag == "1"){
        oEditTaskNode.onclick = "parent.editCheckListItem('" + strLynxID + "','" + strTaskID + "'); return false;";
      } else {
        oEditTaskNode.onclick = "";
        oEditTaskNode.onmouseover = "";
        oEditTaskNode.onmouseout = ""
        oEditTaskNode.style.color = "#C0C0C0";
      }
      
      var oNavigateNode = oDropDownNode.firstChild.cells[1];
      if (oNavigateNode.tagName != "TD") return;
      oNavigateNode.onclick = "parent.checkListNavigateTo('" + strEntity + "','" + strLynxID + "','" + strNumber + "' ); return false;"
      // Customize the navigate description.
      var strNavigate = 'Navigate To ' + strEntity;
      if ( strNumber != "0" ) strNavigate += " " + strNumber;

      oNavigateNode.children[1].innerText = strNavigate;
      
      // Pick an icon for the navigate item.
      switch( strEntity )
      {
          case "Vehicle": oNavigateNode.children[0].src = "/images/icon_vehicle_closed.gif"; break;
          case "Property": oNavigateNode.children[0].src = "/images/icon_property_closed.gif"; break;
          default: oNavigateNode.children[0].src = "/images/icon_claim_closed.gif"; break;
      }
      
      objCheckListPopup.document.body.innerHTML = oDropDownNode.innerHTML;
      objCheckListPopup.show(lefter, topper, 183, 49, event.srcElement);
    }
  
    // User clicked on table row.
    function editCheckListItem( strLynxID, strTaskID )
    {
      objCheckListPopup.hide();
      var strReq = "CheckList.asp?WindowID=" + gsWindowID + "&Entity=CheckListDetails&TaskID=" + strTaskID + "&LynxID=" + strLynxID;
      window.showModalDialog( strReq, window, "dialogHeight:260px; dialogWidth:350px; resizable:no; status:no; help:no; center:yes;" );
    }
  
  
    function checkListNavigateTo( strEntity, strLynxID, strNumber )
    {
      objCheckListPopup.hide();

      NavOpenWindow( strEntity, strLynxID, strNumber );
    }
    
    function refreshCheckListWindow() {
      if (typeof(parent.showStatus) == "function")
        parent.showStatus("Refreshing data...");
      window.setTimeout("windowReload()", 100);
    }
    
    function windowReload(){
      window.location.reload();
    }

    function gotoPage(){
      var iPage;
      try {
      btnFirst.setFocus();
      var sPageTxt = txtPage.value;
      if (sPageTxt.indexOf("of") > 0) {
        sPageTxt = sPageTxt.substr(0, sPageTxt.indexOf("of")).Trim();
      }
      
      if (isNaN(sPageTxt)){
        ClientWarning("Invalid page number specified. Expecting a numeric value for page number.");
        return;
      }
      iPage = parseInt(sPageTxt, 10);
      if (iPage <= 0) iPage = 1;
      if (iPage > iPgCount) iPage = iPgCount;
      if (iPage != iCurPage)
        showPage(iPage);
      else
        txtPage.value = iCurPage + " of " + iPgCount;
      } catch (e) {}
    }
    
    function sortTable(sTable, sSortFldName, sDataType, sXMLSrc) {
      if (recCount < 1) return;
      var oTD = null;

      var ddlAscDsc;       
      if(sSortFldName.indexOf("-ASC")>0){
      ddlAscDsc= "descending"
      }      
      if(sSortFldName.indexOf("-DSC")>0){
      ddlAscDsc= "ascending"
      }      
      sSortFldName=sSortFldName.replace("-ASC","").replace("-DSC","");

      if (tblHeader) {
        var oTHead = tblHeader.rows;
        if (oTHead) {
          var oCells = oTHead[0].cells;
          for (var i = 0; i < oCells.length; i++) {
            var sScript = oCells[i].onclick;
            if (sScript != null) sScript = sScript.toString();
            if (sScript != null && sScript.indexOf(sSortFldName) > 0) {
              oTD = oCells[i];
              break;
            }
          }
        }
      } else 
        oTD = event.srcElement;
        
      var sSortOrder = (oTD ? oTD.getAttribute("sortOrder") : "ascending");
      var oXML = null;
      
      if (ddlAscDsc=="ascending" || ddlAscDsc=="descending"){
        sSortOrder=ddlAscDsc;
       } 

      if (sXMLSrc != "")
        oXML = document.getElementById(sXMLSrc);
      
      if (sTable != "" )
        oTbl = document.getElementById(sTable);
        
      if (sSortFldName != "" && oXML && oTbl) {

          bSorting = true;
          cancelRefresh();
          
          var sFldName = sSortFldName;
          if (sSortOrder == "ascending")
             sSortOrder = "descending";
          else
             sSortOrder = "ascending";
             
          if (sSortFldName == "DefaultOrder")
            sSortOrder = "ascending"
        
          var sColumnType = (sDataType == "number" ? "number" : "text");
        
          var iCount = 0;
          var objTmp = null;
          if (sFldName) {
                var sDataSrc = sXMLSrc;
                var objXML = oXML; 
                if (objXML) {
        
                   var sFirstNode = objXML.XMLDocument.documentElement.firstChild.nodeName;
        
                   oTbl.dataSrc = "";
                   var objXSL = new ActiveXObject("Msxml2.DOMDocument");
                   if (objXSL) {
                      objXSL.async = false;
        
                      var sXSL = '<?xml version="1.0" encoding="UTF-8"?>' +
                                  '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
        
                                  '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>' +
        
                                  '<xsl:template match="/Root">' +
                                     '<xsl:element name="Root">' +
                                     '<xsl:apply-templates select="' + sFirstNode + '">' +
                                        '<xsl:sort select="' + sFldName + '" data-type="' + sColumnType + '" ' +
                                        (sColumnType == 'text' ? 'case-order="lower-first"' : '') +
                                        ' order="' + sSortOrder + '" />' +
                                     '</xsl:apply-templates>' +
                                     '</xsl:element>' +
                                  '</xsl:template>' +
        
                                  '<xsl:template match="' + sFirstNode + '">' +
                                     '<xsl:copy-of select="."/>' +
                                  '</xsl:template>' +
                                  '</xsl:stylesheet>';
                      objXSL.loadXML(sXSL);
        
                      //oXML.loadXML(oXML.transformNode(objXSL));
                      oXML.transformNodeToObject(objXSL, oXML);
                      oTbl.dataSrc = "#" + sDataSrc;
        
                      //reset the page.
                      iCurPage = 1;
                      parent.curPage = iCurPage;
                      parent.sortColumn = sSortFldName;
                      parent.sortOrder = sSortOrder;
                      parent.dataType = sColumnType;
                      
                      //display the sort icon
                      divSort.style.display = "inline";

                      if (sSortOrder == "ascending")
                        divSort.firstChild.src = "/images/ascending.gif";
                      else
                        divSort.firstChild.src = "/images/descending.gif";
                        
                      if (oTD) {
                        var x = oTD.offsetLeft + oTD.offsetWidth - divSort.offsetWidth;
                        var y = ((oTD.offsetTop + oTD.offsetHeight) - divSort.offsetHeight) / 2 + oTD.offsetTop + 3;
                        divSort.style.left = x;
                        divSort.style.top = y+20;
                        
                        oTD.setAttribute("sortOrder", sSortOrder);
                      }
                      bSorting = false;
                   }
                }
          }
      }
    }

    function updateRecPerPage(){
      iPageSize = txtRecPerPage.value;
      if (iPageSize < 1) {
        iPageSize = 10;
        txtRecPerPage.value = iPageSize;
      }
      resetTask();
      initData();
      refreshTasks();
    }
    
    function doUpdate(){
      btnFirst.setFocus(); 
      window.setTimeout("txtRecPerPage.contentSaved()", 200);
      window.setTimeout("txtRecPerPage.setFocus()", 500);
    }
    
    function showToolTip(objSrc){
      var oTR = objSrc.parentElement;
      var sLynxID = oTR.cells[11].innerText;
      var sEntityNum = oTR.cells[12].innerText;
      objSrc.style.backgroundColor = "#AFEEEE";
      toolTip.firstChild.rows[0].cells[1].innerText = sLynxID;
      //alert(toolTip.firstChild.tagName);
      //tblOwnerClaim.dataSrc  = "#xmlLynxID_" + sLynxID + "_Clm";
      tblOwnerVehPrp.dataSrc  = "#xmlLynxID_" + sLynxID + "_VehPrp";
      toolTip.style.display = "inline";
      if ((event.y + toolTip.offsetHeight) > divData.offsetHeight)
        toolTip.style.top = event.y - toolTip.offsetHeight;
      else
        toolTip.style.top = event.y;
      //toolTip.style.left = event.x - toolTip.offsetWidth;
      toolTip.style.left = objSrc.offsetLeft - toolTip.offsetWidth + 20;
      bMouseInToolTip = true;
      window.setTimeout("highLightEntityNum(" + sEntityNum + ")", 100)
    }
    
    function hideToolTip(){
      if (bMouseInToolTip == false) {
        toolTip.getElementsByTagName("div")[0].firstChild.dataSrc = "";
        toolTip.style.display = "none";
      }
    }
    
    function keepToolTip(){
      bMouseInToolTip = true;
    }
    
    function highLightEntityNum(sEntityNum){
      //alert(oTblVehPrp.length);
      for ( var i = 0; i < tblOwnerVehPrp.rows.length; i++){
        if (tblOwnerVehPrp.rows[i].cells[2].innerText == sEntityNum){
          tblOwnerVehPrp.rows[i].style.backgroundColor = "#AFEEEE";
          var obj = tblOwnerVehPrp.rows[i];
          var iScrollPos = (obj.offsetTop + obj.offsetHeight) - divToolTipHolder.scrollTop - divToolTipHolder.clientHeight;
          var iCount = 0;
          if (iScrollPos > 0) {
             while (iScrollPos > 0 && iCount < 500) {
                divToolTipHolder.doScroll("down");
                iScrollPos = (obj.offsetTop + obj.offsetHeight) - divToolTipHolder.scrollTop - divToolTipHolder.clientHeight + 5; //add 15 pixel to display a portion of the next tab
                iCount++;
             }
          } else {
             iScrollPos = obj.offsetTop - divToolTipHolder.scrollTop;
             while (iScrollPos < 0 && iCount < 500) {
                divToolTipHolder.doScroll("up");
                iScrollPos = obj.offsetTop - divToolTipHolder.scrollTop - 5; //subract 15 pixel to display a portion of the previous tab
                iCount++;
             }
          }
        } else {
          tblOwnerVehPrp.rows[i].style.backgroundColor = "#FFFFFF";
        }
      }
    }
    
    function searchText(){
      var sDataSrc = tblData.dataSrc;
      sDataSrc = sDataSrc.substring(1);
      if (sDataSrc != "" && document.getElementById(sDataSrc)) {
        var oXML;
        var sFindCol = "";
        oXML = document.getElementById(sDataSrc);
        if (selFind.selectedIndex != -1)
          sFindCol = selFind.value;
        if (sFindCol != "") {
          var oRoot;
          var oNode;
          
          oRoot = oXML.selectSingleNode("/Root");
          oNode = oXML.selectSingleNode("/Root/Entity[LynxID='" + txtFindVal.value + "' and position() > " + iSearchFoundPosition + "]");
          for (var i = iSearchFoundPosition; i < oRoot.childNodes.length; i++) {
            if (oRoot.childNodes[i] === oNode) {
              var iPage = Math.floor(i / iPageSize) + 1;
              iSearchFoundPosition = i + 1;
              showPage(iPage);
              var iRow = i % iPageSize;
              //window.setTimeout("tblData.rows[" + iRow + "].scrollIntoView(true)", 1000);
              break;
            }
          }
        }
      }
    }
    
    function findText() {
      iSearchFoundPosition = 0;
      searchText();
    }
    
    function findNextText() {
      searchText();
    }
    
    function ReassignExposure(){
      var oExposureSelects = document.getElementsByName("chkSelectExposure");
      var sSelectedClaimAspectID = "";
      var sClaimAspectID = "";
      var sFirstLynxID = "";
      if (oExposureSelects){
        //alert(oExposureSelects[0].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.tagName);
        for (i = 0; i < oExposureSelects.length; i++){
          if (oExposureSelects[i].value == 1){
            sClaimAspectID = oExposureSelects[i].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.cells[8].innerText;
            if (sClaimAspectID != "" && isNaN(sClaimAspectID) == false)
              sSelectedClaimAspectID += sClaimAspectID + ",";
            if (sFirstLynxID == "")
              sFirstLynxID = oExposureSelects[i].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.cells[11].innerText;
          }
        }
        
        //alert(sSelectedClaimAspectID); return;
        if (sSelectedClaimAspectID != ""){
          var sDimensions = "dialogHeight:265px; dialogWidth:300px; ";
          var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
          var strRet = window.showModalDialog("ClaimReAssign.asp?LynxID=" + sFirstLynxID + "&claimAspectID=" + sSelectedClaimAspectID + "&Entity=Vehicle&Desc=" + escape("Reassign multiple vehicles"), "", sDimensions + sSettings);
          if (strRet != "-1")
            windowReload();
        } else 
          ClientWarning("No vehicles selected for reassignment.");
      }
    }
    
    function selectAllExposure(){
      var oExposureSelects = document.getElementsByName("chkSelectExposure");
      var iSelectAll = chkSelectAllExposure.value;
      if (oExposureSelects){
        for (i = 0; i < oExposureSelects.length; i++){
          oExposureSelects[i].onchange = "";
          oExposureSelects[i].value = iSelectAll;
          oExposureSelects[i].onchange = resetSelectAll;
        }
      }
    }
    
    function resetSelectAll(){
      chkSelectAllExposure.onchange = "";
      chkSelectAllExposure.value = 0;
      chkSelectAllExposure.onchange = selectAllExposure;
    }
    
  ]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" style="border:0px;margin:4px;padding:0px;overflow:hidden;cursor:default" tabIndex="-1" oncontextmenu1="event.returnValue=false">
  <xsl:if test="contains($ReassignExposureCRUD, 'U') = true() and $SupervisorFlag = '1'">
    <div style="position:absolute;top:6px;left:12px;">
        <IE:APDCheckBox id="chkSelectAllExposure" name="chkSelectAllExposure" class="APDCheckBox" caption="" value="0" CCDisabled="false" required="false" CCTabIndex="1" canDirty="false" title="Select all for Reassignment" onchange="selectAllExposure()"/>
    </div>
  </xsl:if>        
        <div style="padding-left:25px">
          <label style="font-family:Tahoma,Verdana,Arial;font-size:11px;font-weight:bold">
            Additional Sort by:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </label>
          <select style="padding-bottom: 1px; padding-left: 3px; width: 183px; text-overflow: ellipsis; padding-right: 1px; font-family: Tahoma, Verdana, Arial; white-space: nowrap; font-size: 11px; overflow: hidden; cursor: hand; padding-top: 1px;" width="200" maxwidth="235" displayCount="10" blankFirst="false" canDirty="false" ExpRequired="true" CCDisabled="false" onchange="sortTable('tblData', this.value, 'text', 'xmlVehList')">
	    <option value="">Select</option>
            <option value="InsuranceCompany-ASC">Insurance Company Name (ASC)</option>
            <option value="InsuranceCompany-DSC">Insurance Company Name (DSC)</option>
            <option value="PolicyHolder-ASC">Insured Name (ASC)</option>
            <option value="PolicyHolder-DSC">Insured Name (DSC)</option>
            <option value="Claimant-ASC">Claimant (ASC)</option>
            <option value="Claimant-DSC">Claimant (DSC)</option>
            <option value="IntakeStartDate-DSC">Newest to Oldest</option>
            <option value="IntakeStartDate-ASC">Oldest to Newest</option>
            <option value="Drivable-DSC">Drivable</option>
            <option value="Drivable-ASC">Non-Drivable</option>
            <option value="Location-ASC">Location/State (ASC)</option>
            <option value="Location-DSC">Location/State (DSC)</option>
            <option value="Timezone-ASC">Timezone (ASC)</option>
            <option value="Timezone-DSC">Timezone (DSC)</option>
            <option value="IsAudatex-ASC">Audatex/HQ shops (ASC)</option>
            <option value="IsAudatex-DSC">Audatex/HQ shops (DSC)</option>
            <option value="ByShop-ASC">ByShop (ASC)</option>
            <option value="ByShop-DSC">ByShop (DSC)</option>
          </select>
        </div>
  <table id="tblHeader" name="tblHeader" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;border-color:darkgray">
    <colgroup>
      <col width="60px" style="text-align:left"/>
      <col width="75px" style="text-align:center"/>
      <col width="150px"/>
      <col width="250px"/>
      <col width="125px"/>
      <col width="218px"/>
      <col width="150px" style="display:none"/>
      <col width="65px" style="text-align:center"/>
    </colgroup>
    <thead>
    	<tr>
    		<td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'DefaultOrder', 'number', 'xmlVehList')" title="Click for default sort"></td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'LynxID', 'number', 'xmlVehList')" title="Click to sort">LYNX ID</td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'InsuranceCompany', 'text', 'xmlVehList')" title="Click to sort">Insurance Company</td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'VehicleOwner', 'text', 'xmlVehList')" title="Click to sort">Vehicle Owner</td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'ClientClaimNumber', 'text', 'xmlVehList')" title="Click to sort">Client Claim #</td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'VehicleInfo', 'text', 'xmlVehList')" title="Click to sort">Vehicle Info (Y/M/M)</td>
        <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'AssignedTo', 'text', 'xmlVehList')" title="Click to sort">Assigned To</td>
        <td class="TableHeader" unselectable="on">Owners</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'PolicyHolder', 'text', 'xmlVehList')" title="Click to sort">Insured Name</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'Claimant', 'text', 'xmlVehList')" title="Click to sort">Claimant</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'IntakeStartDate', 'text', 'xmlVehList')" title="Click to sort">Created Date</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'Drivable', 'text', 'xmlVehList')" title="Click to sort">Drivable</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'Location', 'text', 'xmlVehList')" title="Click to sort">Location/State</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'Timezone', 'text', 'xmlVehList')" title="Click to sort">Timezone</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'IsAudatex', 'text', 'xmlVehList')" title="Click to sort">Audatex/HQ Shops</td>
              <td style1="display:none; border-style: none;border-width: 0px;" unselectable="on" onclick="sortTable('tblData', 'ByShop', 'text', 'xmlVehList')" title="Click to sort">By Shop</td>
    	</tr>
    </thead>
  </table>
  <div id="divSort" name="divSort" style="position:absolute;top:0px;left:0px;display:none"><img src="/images/ascending.gif"/></div>
  <div id="divData" name="divData" style="overflow:auto;height:406px;border:0px solid #FF0000">
    <table name="tblData" id="tblData" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;border-color:darkgray;display:none" onreadystatechange="if (tblData.readyState == 'complete') tblDataReadyStateChange()">
      <colgroup>
        <col width="60px" style="text-align:center"/>
        <col width="75px" style="text-align:center"/>
        <col width="150px"/>
        <col width="250px"/>
        <col width="125px"/>
        <col width="218px"/>
        <col width="150px" style="display:none"/>
        <col width="65px" style="text-align:center"/>
      </colgroup>
      <!-- <tbody> -->
      	<tr ondblclick="NavToEntity(this)">
              <td class="TableTaskData" rowspan="6" valign="top" style="text-align:center">
              <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td rowspan="2">
                    <xsl:if test="contains($ReassignExposureCRUD, 'U') = true() and $SupervisorFlag = '1'">
                      <IE:APDCheckBox id="chkSelectExposure" name="chkSelectExposure" class="APDCheckBox" caption="" value="0" CCDisabled="false" required="false" CCTabIndex="1" canDirty="false" title="Select Exposure for Reassignment"/>
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </xsl:if>
                  </td>
                  <td>
                    <img datafld="ClaimFolderImage" title="Navigate to claim" ondblclick="NavToClaim()" align="absmiddle"/>
                  </td>
                </tr>
                <tr>
                  <td>
                     <img datafld="DrivableImage" title="Drivable Indicator" align="absmiddle"/>
                  </td>
                </tr>
              </table>
          </td>
          <td class="TableData" rowspan="5"><span datafld="LynxIDwithEntity" class="DataValNoWrap" unselectable="on" style="font-weight:bold;color:#0000FF"/></td>
          <td class="TableData"><span datafld="InsuranceCompany" class="DataValNoWrap" unselectable="on"/></td>
          <td class="TableData"><span datafld="VehicleOwner" class="DataValNoWrap" unselectable="on"/></td>
          <td class="TableData"><span datafld="ClientClaimNumber" class="DataValNoWrap" unselectable="on"/></td>
          <td class="TableData"><span datafld="VehicleInfo" class="DataValNoWrap" unselectable="on"/></td>
          <td class="TableData"><span datafld="AssignedTo" class="DataValNoWrap" unselectable="on"/></td>
          <td class="TableData" onmouseenter="showToolTip(this)" onmousemove="keepToolTip()" onmouseleave="this.style.backgroundColor='#FFFFFF';bMouseInToolTip = false;window.setTimeout('hideToolTip()', 1000)">
            <img datafld="ImgClaimOwner"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <!-- <img datafld="ImgEntityOwner"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
            <!-- <span datafld="UserNotAOwner" style="font-size:15px;font-weight:bold;color:#FF0000" title="You are not a Owner/Analyst/Support of this entity"/> -->
          </td>
          <td class="TableData" style="display:none"><span datafld="ClaimAspectID"/></td>
          <td class="TableData" style="display:none" name="tdOverdue"><span datafld="OverdueCount"/></td>
          <td class="TableData" style="display:none"><span datafld="DueTodayCount"/></td>
          <td class="TableData" style="display:none"><span datafld="LynxID"/></td>
          <td class="TableData" style="display:none"><span datafld="EntityNum"/></td>
          <td class="TableData" style="display:none"><span datafld="EntityType"/></td>
          <td class="TableData" style="display:none"><span datafld="ClaimOwner"/>;<span datafld="EntityOwner"/></td>
           </tr>
            <!--New Sorting Requirements-->
            <tr>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'PolicyHolder', 'text', 'xmlVehList')" title="Click to sort">Insured Name</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'Claimant', 'text', 'xmlVehList')" title="Click to sort">Claimant</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'IntakeStartDate', 'text', 'xmlVehList')" title="Click to sort">Created Date</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'Drivable', 'text', 'xmlVehList')" title="Click to sort">Drivable</td>
            </tr>
            <tr name="trTaskRow0" ondblclick="NavToEntity(this)">
              <td class="TableData">
                <span datafld="PolicyHolder" class="DataValNoWrap" unselectable="on"/>
              </td>
              <td class="TableData">
                <span datafld="Claimant" class="DataValNoWrap" unselectable="on"/>
              </td>   
              <td class="TableData">
                <span datafld="IntakeStartDate" class="DataValNoWrap" unselectable="on"/>
              </td>
              <td class="TableData">
                <span datafld="Drivable" class="DataValNoWrap" unselectable="on"/>
              </td>
            </tr>
            <tr>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'Location', 'text', 'xmlVehList')" title="Click to sort">Location/State</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'Timezone', 'text', 'xmlVehList')" title="Click to sort">Timezone</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'IsAudatex', 'text', 'xmlVehList')" title="Click to sort">Audatex/HQ Shops</td>
              <td class="TableHeader" style1="cursor:hand" unselectable="on" onclick="sortTable('tblData', 'ByShop', 'text', 'xmlVehList')" title="Click to sort">By Shop</td>
            </tr>
            <tr name="trTaskRow1" ondblclick="NavToEntity(this)">
              <td class="TableData">
                <span datafld="Location" class="DataValNoWrap" unselectable="on"/>
              </td>
              <td class="TableData">
                <span datafld="Timezone" class="DataValNoWrap" unselectable="on"/>
              </td>
              <td class="TableData">
                <span datafld="IsAudatex" class="DataValNoWrap" unselectable="on"/>
              </td>
              <td class="TableData">
                <span datafld="ByShop" class="DataValNoWrap" unselectable="on"/>
              </td>
        </tr>      
        <tr name="trTaskRow" ondblclick="NavToEntity(this)" valign="top">
          <!-- <td class="TableData">&nbsp;</td> -->
          <td colspan="8" class="TableTaskData">
            <table name="tblSC" border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;background-color:#FFFFFF;border1:1px solid #C0C0C0;border-bottom1:2px solid #000000;border-right1:2px solid #000000;" onreadystatechange="tblSCReadyStateChange(this)">
              <colgroup>
                <col width="15px"/>
                <col width="860px"/>
                <col style="display:none"/>
                <col style="display:none"/>
                <col style="display:none"/>
                <col style="display:none"/>
              </colgroup>
              <tr unselectable="on">
                <td unselectable="on">
                  <img name="imgTaskDisplay" unselectable="on" src="/images/plus.gif" ondblclick="event.returnValue=false;" onclick="showHideTask()" valign="absmiddle" style="cursor:hand"/>
                </td>
                <td unselectable="on">
                  <span style="padding:0px;" unselectable="on">
                    <span name="SCName" datafld="ServiceChannelCodeDesc" style="padding:0px; padding-right:10px;font-weight:bold;width:145px;overflow:hidden;text-overflow:ellipsis;  white-space:nowrap" unselectable="on"/>
                     Task List (Overdue: <span id="taskMsg" name="taskMsg" datafld="OverdueCount" style="color:#FF0000;font-weight:bold" unselectable="on"/>; Today: <span id="taskMsg" name="taskMsg" datafld="DueTodayCount" style="color:#32CD32;font-weight:bold" unselectable="on"/>;  Future: <span id="taskMsg" name="taskMsg" datafld="FutureCount" style="color:#0302D1;font-weight:bold" unselectable="on"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span dataFld="NextFutureTaskDate" style="color:0302D1"/>)
                   </span>
                </td>
                <td><span dataFld="ClaimAspectServiceChannelID"/></td>
                <td><span dataFld="OverdueCount"/></td>
                <td><span dataFld="DueTodayCount"/></td>
                <td><span dataFld="ServiceChannelPrimaryFlag"/></td>
              </tr>
              <tr>
                <td></td>
                <td colspan="4">
                  <div style="display:none" unselectable="on">
                    <table name="tblTask" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;background-color:#FFFFFF;border:1px solid #C0C0C0;border-bottom:2px solid #000000;border-right:2px solid #000000;" onreadystatechange="markManagerialTasks(this)">
                      <colgroup>
                        <col width="30px" style="text-align:center"/>
                        <col width="75px" style="text-align:center"/>
                        <col width="245px"/>
                        <col width="265px"/>
                        <col width="150px"/>
                        <col width="150px" style="display:none"/>
                        <col width="80px" style="text-align:center"/>
                      </colgroup>
                      <thead>
                      	<tr>
                      		<td class="TaskHeader" unselectable="on"></td>
                      		<td class="TaskHeader" unselectable="on">Due Date</td>
                          <td class="TaskHeader" unselectable="on">Task Name</td>
                          <td class="TaskHeader" unselectable="on">Comment</td>
                          <td class="TaskHeader" unselectable="on">Assigned To</td>
                          <td class="TaskHeader" unselectable="on">Assigned By</td>
                          <td class="TaskHeader" unselectable="on">Assigned On</td>
                      	</tr>
                      </thead>
                    	<tr style="cursor:hand" ondblclick="NavToEntity_Task(this)" oncontextmenu="showContext()">
                    		<td class="TableData"><img src="/images/spacer.gif" datafld="AlarmImage"/></td>
                    		<td class="TableData"><span datafld="AlarmDate" unselectable="on"/></td>
                        <td class="TableData"><span datafld="TaskName" unselectable="on"/></td>
                        <td class="TableData"><span datafld="UserTaskDescription" unselectable="on"/></td>
                        <td class="TableData"><span datafld="AssignedUserName" unselectable="on"/></td>
                        <td class="TableData"><span datafld="CreatedUserName" unselectable="on"/></td>
                        <td class="TableData"><span datafld="CreatedDate" unselectable="on"/></td>
                        <td style="display:none"><span datafld="Entity"/></td>
                        <td style="display:none"><span datafld="CheckListID"/></td>
                        <td style="display:none"><span datafld="LynxID"/></td>
                        <td style="display:none"><span datafld="EntityNum"/></td>
                        <td style="display:none"><span datafld="State"/></td>
                        <td style="display:none"><span datafld="AlarmModificationAllowedFlag"/></td>
                        <td style="display:none"><span datafld="ManagerialTask"/></td>
                        <td style="display:none"><span datafld="TaskID"/></td>
                    	</tr>
                    </table>
                  </div>                  
                </td>
              </tr>
            </table>
            
            
            <!-- <img name="imgTaskDisplay" src="/images/plus.gif" ondblclick="event.returnValue=false;" onclick="showHideTask()" valign="absmiddle" style="cursor:hand"/>
            <span style="padding:0px;padding-left:5px;" unselectable="on">Task List (Overdue: <span id="taskMsg" name="taskMsg" datafld="OverdueCount" style="color:#FF0000;font-weight:bold" unselectable="on"/>; Today: <span id="taskMsg" name="taskMsg" datafld="DueTodayCount" style="color:#32CD32;font-weight:bold" unselectable="on"/>;  Future: <span id="taskMsg" name="taskMsg" datafld="FutureCount" style="color:#0302D1;font-weight:bold" unselectable="on"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span dataFld="NextFutureTaskDate" style="color:0302D1"/>)</span>
            <div style="display:none" unselectable="on">
              <table name="tblTask" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;background-color:#FFFFFF;border:1px solid #C0C0C0;border-bottom:2px solid #000000;border-right:2px solid #000000;" onreadystatechange="markManagerialTasks(this)">
                <colgroup>
                  <col width="30px" style="text-align:center"/>
                  <col width="75px" style="text-align:center"/>
                  <col width="245px"/>
                  <col width="265px"/>
                  <col width="150px"/>
                  <col width="150px" style="display:none"/>
                  <col width="80px" style="text-align:center"/>
                </colgroup>
                <thead>
                	<tr>
                		<td class="TableHeader2" unselectable="on"></td>
                		<td class="TableHeader2" unselectable="on">Due Date</td>
                    <td class="TableHeader2" unselectable="on">Task Name</td>
                    <td class="TableHeader2" unselectable="on">Comment</td>
                    <td class="TableHeader2" unselectable="on">Assigned To</td>
                    <td class="TableHeader2" unselectable="on">Assigned By</td>
                    <td class="TableHeader2" unselectable="on">Assigned On</td>
                	</tr>
                </thead>
              	<tr style="cursor:hand" ondblclick="NavToEntity_Task(this)" oncontextmenu="showContext()">
              		<td class="TableData"><img src="/images/spacer.gif" datafld="AlarmImage"/></td>
              		<td class="TableData"><span datafld="AlarmDate" unselectable="on"/></td>
                  <td class="TableData"><span datafld="TaskName" unselectable="on"/></td>
                  <td class="TableData"><span datafld="UserTaskDescription" unselectable="on"/></td>
                  <td class="TableData"><span datafld="AssignedUserName" unselectable="on"/></td>
                  <td class="TableData"><span datafld="CreatedUserName" unselectable="on"/></td>
                  <td class="TableData"><span datafld="CreatedDate" unselectable="on"/></td>
                  <td style="display:none"><span datafld="Entity"/></td>
                  <td style="display:none"><span datafld="CheckListID"/></td>
                  <td style="display:none"><span datafld="LynxID"/></td>
                  <td style="display:none"><span datafld="EntityNum"/></td>
                  <td style="display:none"><span datafld="State"/></td>
                  <td style="display:none"><span datafld="AlarmModificationAllowedFlag"/></td>
                  <td style="display:none"><span datafld="ManagerialTask"/></td>
              	</tr>
              </table>
            </div> -->
          </td>
          <td class="TableData" style="display:none"><span datafld="ClaimAspectID"/></td>
          <td class="TableData" style="display:none"><span datafld="LynxID"/></td>
          <td class="TableData" style="display:none"><span datafld="DefaultOrder"/></td>
        </tr>
      <!-- </tbody> -->
    </table>
    <div id="divNoData" name="divNoData" class="TableData" style="display:none;border:0px;width:100%;padding:10px;text-align:center;color:#FF0000;font-weight:bold">No data to display.</div>
  </div>
  <div id="divPages" name="divPages">
    <table style="width:100%;padding:0px;margin:0px;margin-top:3px;table-layout:fixed;border:0px;border-top:1px solid #C0C0C0;padding-top:0px;" cellspacing="0" cellpadding="0" border="0">
      <colgroup>
        <col width="100px"/>
        <col width="75px"/>
        <col width="*" style="text-align:right"/>
        <col width="65px"/>
        <col width="130px"/>
        <col width="14px"/>
        <col width="135px"/>
        <col width="10px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="30px"/>
        <col width="75px"/>
        <col width="20px"/>
        <col width="36px" style="padding:0px;padding-left:10px"/>
        <col width="27px"/>
        <col width="27px"/>
        <col width="27px"/>
      </colgroup>
      <tr>
        <td style="padding-top:2px">Records per page:</td>
        <td style="padding-top:2px">
          <IE:APDInputNumeric id="txtRecPerPage" name="txtRecPerPage" class="APDInputNumeric" precision="2" scale="0" neg="false" int="true" canDirty="false" CCTabIndex="1" value="10" onkeypress="if (event.keyCode == 13) doUpdate()" onChange="updateRecPerPage()"/>
        </td>
        <td>
        </td>
        <td>
          <!-- Search for: -->
        </td>
        <td>
          <!-- <IE:APDCustomSelect id="selFind" name="selFind" class="APDCustomSelect" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" required="false" onChange="">
          	<IE:dropDownItem value="LynxIDwithEntity" style="display:none">LYNX Id</IE:dropDownItem>
          	<IE:dropDownItem value="InsuranceCompany" style="display:none">Insurance Company</IE:dropDownItem>
          	<IE:dropDownItem value="PolicyHolder" style="display:none">Policy Holder</IE:dropDownItem>
          	<IE:dropDownItem value="ClientClaimNumber" style="display:none">Client Claim #</IE:dropDownItem>
          	<IE:dropDownItem value="VehicleInfo" style="display:none">Vehicle Info</IE:dropDownItem>
          	<IE:dropDownItem value="AssignedTo" style="display:none">Assigned To</IE:dropDownItem>
          </IE:APDCustomSelect> -->
        </td>
        <td style="text-align:center"><!-- = --></td>
        <td>
          <!-- <IE:APDInput id="txtFindVal" name="txtFindVal" class="APDInput" size="25" maxLength="50" width="125" required="false" canDirty="false" CCDisabled="false" CCTabIndex="3" /> -->
        </td>
        <td>
          <!-- <button onclick="findText()" tabIndex="4"  style="border:0px;background-color:transparent" title="Find">
            <img src="/images/find.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
          <button onclick="findNextText()" tabIndex="5"  style="border:0px;background-color:transparent" title="Find Next">
            <img src="/images/findnext.gif" valign="absmiddle" style="cursor:hand"/>
          </button> -->
        </td>
        <td>
          <xsl:if test="contains($ReassignExposureCRUD, 'U') = true() and $SupervisorFlag = '1'">
            <IE:APDButton id="btnReassign" name="btnReassign" class="APDButton" value="Reassign" width="" CCTabIndex="6" onButtonClick="ReassignExposure()"/>
          </xsl:if>
        </td>
        <td>
          <button onclick="refreshCheckListWindow()" tabIndex="7" style="border:0px;background-color:transparent" title="Refresh">
          <img src="/images/refresh.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>Page:</td>
        <td style="padding-top:2px">
          <IE:APDInput id="txtPage" name="txtPage" class="APDInput" size="10" maxLength="10" required="false" canDirty="false" CCDisabled="false" CCTabIndex="8"/>
        </td>
        <td style="padding-top1:2px">
          <button id="btnGoPage" name="btnGoPage" onclick="gotoPage()" tabIndex="9" style="border:0px;background-color:transparent" title="Go to page...">
          <img src="/images/go.gif" valign="absmiddle" style="cursor:hand"/>
          </button>
        </td>
        <td>
          <IE:APDButton id="btnFirst" name="btnFirst" class="APDButton" value="&lt;&lt;" CCDisabled="false" CCTabIndex="10" onButtonClick="showPage(-1)"/>
        </td>
        <td>
          <IE:APDButton id="btnPrev" name="btnPrev" class="APDButton" value="&lt;" CCDisabled="false" CCTabIndex="11" onButtonClick="showPage(-2)"/>
        </td>
        <td>
          <IE:APDButton id="btnNext" name="btnNext" class="APDButton" value="&gt;" CCDisabled="false" CCTabIndex="12" onButtonClick="showPage(-3)"/>
        </td>
        <td>
          <IE:APDButton id="btnLast" name="btnLast" class="APDButton" value="&gt;&gt;" CCDisabled="false" CCTabIndex="13" onButtonClick="showPage(-4)"/>
        </td>
      </tr>
    </table>
  </div>
  
  <div id="checkListPopupMenu" style="display:none;">
    <table unselectable="on" cellspacing="1" cellpadding="0" width="180" height="44" style="border:1px solid #666666;table-layout:fixed;cursor:hand;font-family:verdana;font-size:11px;background-color:#FFFFFF;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
      <tr unselectable="on">
        <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
            onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
            onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
            onclick="EditOnClick">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <img src="/images/notepad_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          Edit Task
        </td>
      </tr>
      <tr unselectable="on">
        <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
            onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
            onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
            onclick="NavigateOnClick">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <img src="/images/navigate_image.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <span>Navigate To</span>
        </td>
      </tr>
    </table>
  </div>
  
  <div id="divAutoRefresh" name="divAutoRefresh" style="position:absolute;top:10px;left:10px;border:2px solid #000000;padding:5px;background-color:#FFFACD;height:45px;width:250px;display:none;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
    <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
      <colgroup>
        <col width="*"/>
        <col width="50px"/>
      </colgroup>
      <tr valign="middle">
        <td>Auto refresh in <span id="spnRefreshCounter" name="spnRefreshCounter"></span> seconds.</td>
        <td>
          <IE:APDButton id="btnCancel" name="btnCancel" class="APDButton" value="Cancel" onButtonClick="cancelRefresh()"/>
        </td>
      </tr>
    </table>
  </div>
  
  <div id="toolTip" name="toolTip" style="position:absolute;top:0px;left:0px;display:none;height:150px;width:250px;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideToolTip()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideToolTip()">
    <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF">
      <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;color:#FFFFFF">
        <td>Entity Owners</td>
        <td></td>
      </tr>
      <tr valign="top">
        <td colspan="2">
          <div id="divToolTipHolder" style="overflow:auto;height:100%;width:100%;padding:3px;">
              <table id="tblOwnerVehPrp" border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;width:100%">
                <colgroup>
                  <col width="75px"/>
                  <col width="*"/>
                </colgroup>
                <tr valign="top">
                  <td style="border-bottom:1px dotted #C0C0C0">
                     <span datafld="Name" class="DataValNoWrap" style="cursor:default;font-weight:bold"/>
                     <br/>
                     <span datafld="ServiceChannels" class="DataValNoWrap" style="cursor:default;padding:0px;padding-top:3px;"/>
                  </td>
                  <td style="border-bottom:1px dotted #C0C0C0">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <colgroup>
                        <col width="8px"/>
                        <col width="*"/>
                      </colgroup>
                      <tr>
                        <td style="color:#4B0082">O:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                        <td><span datafld="Owner" class="DataValNoWrap" style="cursor:default;color:#4B0082" title="File Owner"/></td>
                      </tr>
                      <tr>
                        <td style="color:#D2691E">A:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                        <td><span datafld="Analyst" class="DataValNoWrap" style="cursor:default;color:#D2691E" title="File Analyst"/></td>
                      </tr>
                      <tr>
                        <td style="color:#8A2BE2">S:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                        <td><span datafld="Support" class="DataValNoWrap" style="cursor:default;color:#8A2BE2" title="File Support"/></td>
                      </tr>
                    </table>
                  </td>
                  <td style="display:none"><span datafld="EntityNum" class="DataValNoWrap"/></td>
                </tr>
              </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
  
  <xsl:variable name="QueueUser"><xsl:value-of select="/Root/@SubordinateUserID"/></xsl:variable>
  <!-- Build the Vehicle list by the default sort order -->
  <xml id="xmlVehList" name="xmlVehList" ondatasetcomplete="initData()">
    <xsl:variable name="CurrentDateTime"><xsl:value-of select="/Root/@CurrentDateTime"/></xsl:variable>
    <!-- Sort items as per spec -->
    <!-- Get all the managerial task claims -->
    <xsl:variable name="managerialTasks">
        <xsl:for-each select="/Root/ClaimAspect[@HasManagerialTasks = '1']">
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>    

    <!-- External Notes Review Required Task to the user -->
    <xsl:variable name="ExternalNotesReviewTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountExternalNotes != '0']">
          <!-- Show the new claims with no assignment first -->
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>

    <!-- Escalation Review Required to the user -->
    <xsl:variable name="EscalationReviewTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountEscalationReview != '0']">
          <!-- Show the new claims with no assignment first -->
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>

    <!-- Probably Closure Review tasks assigned to the user -->
    <xsl:variable name="ProbablyClosureReviewTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountProbablyClosureReview != '0']">
          <!-- Show the Probably Closure Review tasks -->
          <xsl:sort select="@AssignmentSentFlag" data-type="number" order="ascending"/>
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>    

    <!-- Estimate Threshold tasks assigned to the user -->
    <xsl:variable name="EstimateThresholdTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountEstimateThreshold != '0']">
          <!-- Show the Estimate Threshold tasks -->
          <xsl:sort select="@AssignmentSentFlag" data-type="number" order="ascending"/>
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>    
    
    <!-- New claims assigned to the user -->
    <xsl:variable name="UserNewClaims">
        <xsl:for-each select="/Root/ClaimAspect[@NewlyAssigned = '1']">
          <!-- Show the new claims with no assignment first -->
          <xsl:sort select="@AssignmentSentFlag" data-type="number" order="ascending"/>
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EstimateThresholdTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>    

    <!-- Escalated claims assigned to the user -->
    <xsl:variable name="UserEscalatedClaims">
        <xsl:for-each select="/Root/ClaimAspect[@PriorityFlag = '1']">
          <!-- Show the new claims with no assignment first -->
          <xsl:sort select="@AssignmentSentFlag" data-type="number" order="ascending"/>
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EstimateThresholdTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserNewClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>    

    <!-- Review Estimate Tasks assigned to the user -->
    <xsl:variable name="UserReviewEstimateTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountReviewEstimate != '0']">
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EstimateThresholdTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserNewClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserEscalatedClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>
    
    <!-- Other claims with task assigned to the user -->
    <xsl:variable name="OtherUserTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountDueFuture!='0' or @TaskCountDueToday!='0' or @TaskCountOverDue!='0']">
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EstimateThresholdTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserNewClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserEscalatedClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserReviewEstimateTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>

    <!-- Claims with no tasks -->
    <xsl:variable name="ClaimsWithNoTasks">
        <xsl:for-each select="/Root/ClaimAspect[@TaskCountDueFuture='0' or @TaskCountDueToday='0' or @TaskCountOverDue='0']">
          <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
          <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
          <xsl:sort select="@NewlyAssigned" data-type="number" order="descending"/>
          <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:if test="count(msxsl:node-set($managerialTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EscalationReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($EstimateThresholdTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserNewClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserEscalatedClaims)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and 
                        count(msxsl:node-set($UserReviewEstimateTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0 and
                        count(msxsl:node-set($OtherUserTasks)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="sortedList">
        <xsl:for-each select="msxsl:node-set($managerialTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($ExternalNotesReviewTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($EscalationReviewTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($ProbablyClosureReviewTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($EstimateThresholdTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($UserNewClaims)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($UserEscalatedClaims)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($UserReviewEstimateTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($OtherUserTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <xsl:for-each select="msxsl:node-set($ClaimsWithNoTasks)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>    
    
    <!-- Combine the sorted list with the items that does not match the sort spec -->
    <xsl:variable name="fullSortedList">
        <!-- Sorted items -->
        <xsl:for-each select="msxsl:node-set($sortedList)/ClaimAspect">
            <xsl:copy-of select="."/>
        </xsl:for-each>
        <!-- Missed items -->
        <xsl:for-each select="/Root/ClaimAspect">
            <xsl:variable name="origClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
            <xsl:if test="count(msxsl:node-set($sortedList)/ClaimAspect[@ClaimAspectID = $origClaimAspectID]) = 0">
                <xsl:copy-of select="."/>
            </xsl:if>
        </xsl:for-each>
    </xsl:variable>
    
    <Root>
        <xsl:for-each select="msxsl:node-set($fullSortedList)/ClaimAspect">
            <xsl:call-template name="reorderedEntity"/>
        </xsl:for-each>
    </Root>
  </xml>
  
  <xsl:for-each select="/Root/ClaimAspect">
    <xsl:sort select="@PriorityFlag" data-type="number" order="descending"/>
    <xsl:sort select="@TaskDueDateMinutes" data-type="number" order="descending"/>
    <!-- For each vehicle, build the service channel -->
    <xsl:variable name="LynxID"><xsl:value-of select="@LynxID"/></xsl:variable>
    <xsl:variable name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
    <xsl:variable name="ClaimAspectIDClaim"><xsl:value-of select="@ClaimAspectIDClaim"/></xsl:variable>
    <xml>
      <xsl:attribute name="id">xmlVehSC<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
      <xsl:attribute name="name">xmlVehSC<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
      <Root>
        <xsl:for-each select="ServiceChannel">
        <xsl:sort select="@ServiceChannelPrimaryFlag" data-type="number" order="descending"/>
        <xsl:sort select="@ServiceChannelDesc" data-type="text" order="ascending"/>
          <xsl:variable name="CASCId"><xsl:value-of select="@ClaimAspectServiceChannelID"/></xsl:variable>
          <ServiceChannel>
            <ClaimAspectServiceChannelID><xsl:value-of select="@ClaimAspectServiceChannelID"/></ClaimAspectServiceChannelID>
            <ClaimAspectID><xsl:value-of select="@ClaimAspectID"/></ClaimAspectID>
            <ServiceChannelPrimaryFlag><xsl:value-of select="@ServiceChannelPrimaryFlag"/></ServiceChannelPrimaryFlag>
            <ServiceChannelActive><xsl:value-of select="@ServiceChannelActive"/></ServiceChannelActive>
            <ServiceChannelCode><xsl:value-of select="@ServiceChannelCode"/></ServiceChannelCode>
            <ServiceChannelCodeDesc><xsl:value-of select="@ServiceChannelCodeDesc"/></ServiceChannelCodeDesc>
            <OverdueCount>
              <xsl:value-of select="count(../DiaryTask[@CheckListID &gt; 0 and @ClaimAspectServiceChannelID=$CASCId and @AlarmStatus='overdue' and @AssignedUserID = $QueueUser])"/>
            </OverdueCount>
            <DueTodayCount>
              <xsl:value-of select="count(../DiaryTask[@CheckListID &gt; 0 and @ClaimAspectServiceChannelID=$CASCId and @AlarmStatus='today' and @AssignedUserID = $QueueUser])"/>
            </DueTodayCount>
            <FutureCount>
               <xsl:value-of select="count(../DiaryTask[@CheckListID &gt; 0 and @ClaimAspectServiceChannelID=$CASCId and @AlarmStatus='future' and @AssignedUserID = $QueueUser])"/>
            </FutureCount>
            
            <xsl:variable name="FutureTaskDatesList">
            <xsl:for-each select="../DiaryTask[(@ClaimAspectServiceChannelID=$CASCId) and (@AlarmStatus='future') and (@CheckListID &gt; 0) and (@AlarmDate!='1900-01-01T00:00:00') and (@AssignedUserID = $QueueUser)]">
              <xsl:sort select="@AlarmDate" data-type="text" order="ascending"/>
              <xsl:value-of select="js:formatSQLDateTime(string(@AlarmDate))"/>;
            </xsl:for-each>
            </xsl:variable>
            
            <xsl:variable name="NextFutureTaskDate"><xsl:value-of select="substring-before($FutureTaskDatesList, ';')"/></xsl:variable>
            
            <NextFutureTaskDate>
              <xsl:choose>
                <xsl:when test="$NextFutureTaskDate != ''">[Next Future task due on: <xsl:value-of select="$NextFutureTaskDate"/>]</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </NextFutureTaskDate>
          </ServiceChannel>
        </xsl:for-each>        
      </Root>
    </xml>   
     
    <!-- For each vehicle and service channel, build the task list -->
    <xsl:for-each select="ServiceChannel">
      <xml>
        <xsl:attribute name="id">xmlVehTask<xsl:value-of select="@ClaimAspectServiceChannelID"/></xsl:attribute>
        <xsl:attribute name="name">xmlVehTask<xsl:value-of select="@ClaimAspectServiceChannelID"/></xsl:attribute>
        <xsl:variable name="CASCId"><xsl:value-of select="@ClaimAspectServiceChannelID"/></xsl:variable>
        <Root>
          <xsl:for-each select="../DiaryTask[@CheckListID &gt; 0 and @ClaimAspectServiceChannelID=$CASCId]">
          <xsl:sort select="@AlarmDate" data-type="text" order="ascending"/>
          
          <xsl:variable name="AlarmDateTime">
            <xsl:value-of select="js:formatSQLDateTime( string(@AlarmDate) )"/>
          </xsl:variable>
          <xsl:variable name="State">
            <xsl:value-of select="local:getDateRoleState( string($AlarmDateTime), string(@CompletionAllowedFlag) )"/>
          </xsl:variable>
          <xsl:variable name="entityClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:variable name="showTask">
            <xsl:choose>
              <!-- if the login user is the owner of vehicle then show all tasks -->
              <xsl:when test="$QueueUser = /Root/Owner//Entity[@ClaimAspectID=$entityClaimAspectID]/@OwnerUserID">true</xsl:when>
              <!-- if the login user is not the owner of the vehicle and if the task is explicitly assigned to him, then show that task only -->
              <xsl:when test="$QueueUser != /Root/Owner//Entity[@ClaimAspectID=$entityClaimAspectID]/@OwnerUserID and $QueueUser = @AssignedUserID">true</xsl:when>
              <!-- hide all other tasks -->
              <xsl:otherwise>false</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          
          <xsl:if test="$showTask = 'true'">
          <DiaryTask>
            <CheckListID><xsl:value-of select="@CheckListID"/></CheckListID>
            <ClaimAspectID><xsl:value-of select="../@ClaimAspectID"/></ClaimAspectID>
            <LynxID><xsl:value-of select="../@LynxID"/></LynxID>
            <AssignedUserName><xsl:value-of select="@AssignedUserName"/></AssignedUserName>
            <CreatedUserName><xsl:value-of select="@CreatedUserName"/></CreatedUserName>
            <ClaimAspectNumber><xsl:value-of select="@ClaimAspectNumber"/></ClaimAspectNumber>
            <TaskName><xsl:value-of select="@TaskName"/></TaskName>
            <AlarmDate><xsl:value-of select="js:formatSQLDateTime(string(@AlarmDate))"/></AlarmDate>
            <CreatedDate><xsl:value-of select="js:formatSQLDateTime(string(@CreatedDate))"/></CreatedDate>
            <UserTaskDescription><xsl:value-of select="@UserTaskDescription"/></UserTaskDescription>
            <AlarmModificationAllowedFlag><xsl:value-of select="@AlarmModificationAllowedFlag"/></AlarmModificationAllowedFlag>
            <CompletionAllowedFlag><xsl:value-of select="@CompletionAllowedFlag"/></CompletionAllowedFlag>
            <ManagerialTask><xsl:value-of select="@ManagerialTask"/></ManagerialTask>
            <TaskID><xsl:value-of select="@TaskID"/></TaskID>
            <AlarmImage>
              <xsl:choose>
                  <xsl:when test="@AlarmStatus = 'overdue' and @CompletionAllowedFlag = '1'">/images/tasklist_action_00_red.gif</xsl:when>
                  <xsl:when test="@AlarmStatus = 'overdue' and @CompletionAllowedFlag = '0'">/images/tasklist_noaction_00_red.gif</xsl:when>
                  <xsl:when test="@AlarmStatus = 'today' and @CompletionAllowedFlag = '1'">/images/tasklist_action_01_green.gif</xsl:when>
                  <xsl:when test="@AlarmStatus = 'today' and @CompletionAllowedFlag = '0'">/images/tasklist_noaction_01_green.gif</xsl:when>
                  <xsl:when test="@AlarmStatus = 'future' and @CompletionAllowedFlag = '1'">/images/tasklist_action_02_blue.gif</xsl:when>
                  <xsl:when test="@AlarmStatus = 'future' and @CompletionAllowedFlag = '0'">/images/tasklist_noaction_02_blue.gif</xsl:when>
              </xsl:choose>
            </AlarmImage>
            <Entity>
              <xsl:choose>
                <xsl:when test="@ClaimAspectID=$ClaimAspectIDClaim">Claim</xsl:when>
                <xsl:otherwise><xsl:value-of select="../@ClaimAspectType"/></xsl:otherwise>
              </xsl:choose>
            </Entity>
            <EntityNum>
              <xsl:choose>
                <xsl:when test="@ClaimAspectID=$ClaimAspectIDClaim">0</xsl:when>
                <xsl:otherwise><xsl:value-of select="../@ClaimAspectNumber"/></xsl:otherwise>
              </xsl:choose>
            </EntityNum>
            <State><xsl:value-of select="$State"/></State>
            <SysLastUpdatedDate><xsl:value-of select="@SysLastUpdatedDate"/></SysLastUpdatedDate>
          </DiaryTask>
          </xsl:if>
          </xsl:for-each>
        </Root>
      </xml>
    </xsl:for-each>
  </xsl:for-each>
  
  <!-- <xsl:for-each select="/Root/Owner">
    <xml>
      <xsl:attribute name="id">xmlLynxID_<xsl:value-of select="@LynxID"/>_Clm</xsl:attribute>
      <xsl:attribute name="name">xmlLynxID_<xsl:value-of select="@LynxID"/>_Clm</xsl:attribute>
      
      <Root>
        <xsl:for-each select="Entity[@Name='Claim']">
        <Entity>
          <Name>
            <xsl:value-of select="@Name"/>
          </Name>
          <Owner><xsl:value-of select="@OwnerName"/></Owner>
        </Entity>
        </xsl:for-each>
      </Root>
    </xml>
  </xsl:for-each> -->

  <!-- Build the Entity Owner list -->
  <xsl:for-each select="/Root/Owner">
    <xml>
      <xsl:attribute name="id">xmlLynxID_<xsl:value-of select="@LynxID"/>_VehPrp</xsl:attribute>
      <xsl:attribute name="name">xmlLynxID_<xsl:value-of select="@LynxID"/>_VehPrp</xsl:attribute>
      <xsl:variable name="Lynxid"><xsl:value-of select="@LynxID"/></xsl:variable>
      <Root>
        <xsl:for-each select="Entity[@Name != 'Claim']">
        <Entity>
          <Name>
            <xsl:value-of select="@Name"/>
            <xsl:choose>
              <xsl:when test="@EntityNumber != '0'"> - <xsl:value-of select="@EntityNumber"/></xsl:when>
            </xsl:choose>
          </Name>
          <Owner><xsl:value-of select="@OwnerName"/></Owner>
          <Analyst><xsl:value-of select="@AnalystName"/></Analyst>
          <Support><xsl:value-of select="@SupportName"/></Support>
          <EntityNum><xsl:value-of select="@EntityNumber"/></EntityNum>
          <ServiceChannels><xsl:for-each select="/Root/ClaimAspect[@LynxID=$Lynxid]/ServiceChannel[@ServiceChannelActive='1']">
               <xsl:sort select="@ServiceChannelPrimaryFlag" data-type="number" order="descending"/>
               <xsl:value-of select="@ServiceChannelCode"/>
               <xsl:if test="position() &lt; last()">, </xsl:if>
            </xsl:for-each></ServiceChannels>
        </Entity>
        </xsl:for-each>
      </Root>
    </xml>
  </xsl:for-each>

</BODY>
</HTML>

</xsl:template>

<xsl:template name="reorderedEntity">
   <xsl:variable name="QueueUser"><xsl:value-of select="/Root/@SubordinateUserID"/></xsl:variable>
      <Entity>
        <DefaultOrder><xsl:value-of select="position()"/></DefaultOrder>
        <LynxID><xsl:value-of select="@LynxID"/></LynxID>
        <EntityNum><xsl:value-of select="@ClaimAspectNumber"/></EntityNum>
        <EntityType><xsl:value-of select="@ClaimAspectType"/></EntityType>
        <LynxIDwithEntity><xsl:value-of select="@LynxID"/>-<xsl:value-of select="@ClaimAspectNumber"/></LynxIDwithEntity>
        <!--New Sorting Requirements-->
      <Drivable>
        <xsl:choose>
          <xsl:when test="@DriveableFlag = '1'">
            <xsl:text>YES</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>NO</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </Drivable>
      <SysLastUpdatedDate>
        <xsl:value-of select="@SysLastUpdatedDate"/>
      </SysLastUpdatedDate>
      <IntakeStartDate>
        <xsl:value-of select="@IntakeStartDate"/>
      </IntakeStartDate>
      <Location>
        <xsl:value-of select="@LocationState"/>
      </Location>
      <TimeZone>
        <xsl:value-of select="@LocationZip"/>
      </TimeZone>
      <IsAudatex>
        <xsl:choose>
          <xsl:when test="@ShopType='HQ'">HQ Shop</xsl:when>
          <xsl:when test="@ShopType='ADP'">Audatex Shop</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@ShopType"/>
          </xsl:otherwise>
        </xsl:choose>
      </IsAudatex>
      <ByShop>
        <xsl:value-of select="@ShopName"/>
      </ByShop>
        <ClaimAspectID><xsl:value-of select="@ClaimAspectID"/></ClaimAspectID>
        <InsuranceCompany><xsl:value-of select="@InsuranceCompanyName"/></InsuranceCompany>
        <PolicyHolder><xsl:value-of select="@InsuredName"/></PolicyHolder>
        <Claimant><xsl:value-of select="@Claimant"/></Claimant>
        <VehicleOwner><xsl:value-of select="@OwnerName"/></VehicleOwner>
        <ClientClaimNumber><xsl:value-of select="@ClientClaimNumber"/></ClientClaimNumber>
        <VehicleInfo><xsl:value-of select="@ClaimAspectDescription"/></VehicleInfo>
        <AssignedTo><xsl:value-of select="@LynxRepNameClaim"/></AssignedTo>
        <ClaimFolderImage>
          <xsl:choose>
            <xsl:when test="@PriorityFlag = '1'">/images/priorityclaim.gif</xsl:when>
            <xsl:when test="@NewlyAssigned = '1'">/images/newclaim.gif</xsl:when>
            <xsl:otherwise>/images/openclaim.gif</xsl:otherwise>
          </xsl:choose>
        </ClaimFolderImage>
        <DrivableImage>
          <xsl:choose>
            <xsl:when test="@DriveableFlag = '1'">/images/spacer.gif</xsl:when>
            <xsl:otherwise>/images/nondrivable.gif</xsl:otherwise>
          </xsl:choose>
        </DrivableImage>
        <OverdueCount><xsl:value-of select="@TaskCountOverDue"/></OverdueCount>
        <DueTodayCount><xsl:value-of select="@TaskCountDueToday"/></DueTodayCount>
        <FutureCount><xsl:value-of select="@TaskCountDueFuture"/></FutureCount>
        
        <xsl:variable name="FutureTaskDatesList">
        <xsl:for-each select="DiaryTask[(@AlarmStatus='future') and (@CheckListID &gt; 0) and (@AlarmDate!='1900-01-01T00:00:00') and (@AssignedUserID = $QueueUser)]">
          <xsl:sort select="@AlarmDate" data-type="text" order="ascending"/>
          <xsl:value-of select="js:formatSQLDateTime(string(@AlarmDate))"/>;
        </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="NextFutureTaskDate"><xsl:value-of select="substring-before($FutureTaskDatesList, ';')"/></xsl:variable>
        
        <NextFutureTaskDate>
          <xsl:choose>
            <xsl:when test="$NextFutureTaskDate != ''">[Next Future task due on: <xsl:value-of select="$NextFutureTaskDate"/>]</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </NextFutureTaskDate>
        <DueDateDays><xsl:value-of select="@TaskDueDateMinutes"/></DueDateDays>
        <ClaimOwner><xsl:value-of select="@LynxRepNameClaim"/></ClaimOwner>
        <EntityOwner><xsl:value-of select="@LynxRepNameExposure"/></EntityOwner>
        <ImgClaimOwner>
          <xsl:choose>
            <xsl:when test="@UserNotAOwner = '1'">/images/nouser.gif</xsl:when>
            <xsl:otherwise>/images/1user.gif</xsl:otherwise>
          </xsl:choose>
        </ImgClaimOwner>
        <ImgEntityOwner>
          <xsl:choose>
            <xsl:when test="@LynxRepNameClaim != @LynxRepNameExposure">/images/1user.gif</xsl:when>
            <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
          </xsl:choose>
        </ImgEntityOwner>        
        <UserNotAOwner>
          <xsl:choose>
            <xsl:when test="@UserNotAOwner = '1'">!</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </UserNotAOwner>
      </Entity>
</xsl:template>

</xsl:stylesheet>