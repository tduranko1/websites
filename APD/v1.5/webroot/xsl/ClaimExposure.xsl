<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="Insurance">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="InsID"/>
<xsl:param name="mode"/>
<xsl:param name="LynxID"/>
<xsl:param name="UserID"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="ClaimStatus"/>
<xsl:param name="ClaimOwner"/>
<xsl:param name="SubordinateNames"/>
<xsl:param name="SubordinateIDs"/>

<xsl:param name="ReopenExpCRUD" select="Action:Reopen Exposure"/>
<xsl:param name="CloseExpCRUD" select="Action:Close Exposure"/>
<xsl:param name="ReassignCRUD" select="Reassign Claim"/>

<xsl:template match="/Root">

<HTML>

<HEAD>
<TITLE>
    <xsl:choose>
       <xsl:when test="$mode='open'">
        Reopen Exposure
       </xsl:when>
       <xsl:when test="$mode='close'">
        Close Exposure
       </xsl:when>
    </xsl:choose>
</TITLE>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid3.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,46);
		  event.returnValue=false;
		  };	
</script>


<SCRIPT language="Javascript">

    var sReOpenCRUD = '<xsl:value-of select="$ReopenExpCRUD"/>';
    var sCloseCRUD = '<xsl:value-of select="$CloseExpCRUD"/>';
    var sReassignCRUD = '<xsl:value-of select="$ReassignCRUD"/>';
    var sMode = '<xsl:value-of select="$mode"/>';
    var sLynxId = '<xsl:value-of select="$LynxID"/>';
    var sUserId = '<xsl:value-of select="$UserID"/>';
    var sClmAspectID = '<xsl:value-of select="$ClaimAspectID"/>';
    var sClaimStatus = '<xsl:value-of select="$ClaimStatus"/>';

    var sClaimOwner = '<xsl:value-of select="js:cleanString(string($ClaimOwner))"/>';
    var oExposures;
    var bNoPermission = false;

<![CDATA[

  function pageInit(){
    if (sReOpenCRUD.indexOf("U") == -1){
        btnOK.CCDisabled = true;
        showNoPermission();
        bNoPermission = true;
    }
  }
  
  function showNoPermission(){
      msg.style.top = 0;
      msg.style.left = 0;
      msg.style.width = 440;
      if (sMode == "open")
        msg.style.height = 250;
      else
        msg.style.height = 250;
      msg.style.display = "inline";
  }

  function initActiveUsers(){
    if (bNoPermission) return;
    oExposures = xmlExposures.selectNodes("/Root/Exposure")
    var oOwnerPools = xmlActiveUsers.selectNodes("/Root/Reference[@List='AssignmentPool' and @FunctionCD='OWN']");
    var oAnalystPools = xmlActiveUsers.selectNodes("/Root/Reference[@List='AssignmentPool' and @FunctionCD='ALST']");
    var oSupportPools = xmlActiveUsers.selectNodes("/Root/Reference[@List='AssignmentPool' and @FunctionCD='SPRT']");
    
    for (var i = 0; i < oExposures.length; i++){
      var sClaimAspectID = oExposures[i].getAttribute("ClaimAspectID");
      var sFOUserID  = oExposures[i].getAttribute("FOUserID");
      var sFAUserID  = oExposures[i].getAttribute("FAUserID");
      var sFSUserID  = oExposures[i].getAttribute("FSUserID");
      var oSelOwnerAssignTo = document.getElementById("selOwnerAssignTo" + sClaimAspectID);
      if (oSelOwnerAssignTo){
        oSelOwnerAssignTo.Clear();
        //check if the user performing the reopen is part of the File Owner pool
        if (isUserInPool(sUserId, oOwnerPools))
          oSelOwnerAssignTo.AddItem(sUserId, "Self");
        
        //check if the entity owner is part of the file owner pool
        if ((sFOUserID != sUserId) && isUserInPool(sFOUserID, oOwnerPools))
          oSelOwnerAssignTo.AddItem(sFOUserID, oExposures[i].getAttribute("FOUserNameLast") + ", " + oExposures[i].getAttribute("FOUserNameFirst"));
          
        // add all the file owner pool names
        for (var j = 0; j < oOwnerPools.length; j++)
          oSelOwnerAssignTo.AddItem("O_" + oOwnerPools[j].getAttribute("ReferenceID"), oOwnerPools[j].getAttribute("Name"));
      }
      
      var oSelAnalystAssignTo = document.getElementById("selAnalystAssignTo" + sClaimAspectID);
      if (oSelAnalystAssignTo){
        oSelAnalystAssignTo.Clear();
        //check if the user performing the reopen is part of the File Analyst pool
        if (isUserInPool(sUserId, oAnalystPools))
          oSelAnalystAssignTo.AddItem(sUserId, "Self");
        
        //check if the entity analyst is part of the file owner pool
        if ((sFAUserID != sUserId) && isUserInPool(sFAUserID, oAnalystPools))
          oSelAnalystAssignTo.AddItem(sFAUserID, oExposures[i].getAttribute("FAUserNameLast") + ", " + oExposures[i].getAttribute("FAUserNameFirst"));
          
        // add all the file analyst pool names
        for (var j = 0; j < oAnalystPools.length; j++)
          oSelAnalystAssignTo.AddItem("A_" + oAnalystPools[j].getAttribute("ReferenceID"), oAnalystPools[j].getAttribute("Name"));
      }

      var oSelSupportAssignTo = document.getElementById("selSupportAssignTo" + sClaimAspectID);
      if (oSelSupportAssignTo){
        oSelSupportAssignTo.Clear();
        //check if the user performing the reopen is part of the File Support pool
        if (isUserInPool(sUserId, oSupportPools))
          oSelSupportAssignTo.AddItem(sUserId, "Self");
        
        //check if the entity support is part of the file owner pool
        if ((sFSUserID != sUserId) && isUserInPool(sFSUserID, oSupportPools))
          oSelSupportAssignTo.AddItem(sFSUserID, oExposures[i].getAttribute("FSUserNameLast") + ", " + oExposures[i].getAttribute("FSUserNameFirst"));
          
        // add all the file support pool names
        for (var j = 0; j < oSupportPools.length; j++)
          oSelSupportAssignTo.AddItem("S_" + oSupportPools[j].getAttribute("ReferenceID"), oSupportPools[j].getAttribute("Name"));
      }
    }
  }
  
  function isUserInPool(strUserID, oPool){
    var bRet = false;
    if (oPool && (strUserID != "")){      
      for (var i = 0; i < oPool.length; i++){
        if (xmlActiveUsers.selectSingleNode("/Root/User[@UserID='" + strUserID + "']/UserPool[@AssignmentPoolID='" + oPool[i].getAttribute("ReferenceID") + "']")){
          bRet = true;
          break;
        }
      }
    }
    return bRet;
  }
  
  function enableFunction(strClaimAspectID){  
    var ochkEntity = document.getElementById("chk" + strClaimAspectID);
    if (ochkEntity){      
      var ochkOwner, ochkAnalyst, ochkSupport;
      ochkOwner = document.getElementById("chkOwner" + strClaimAspectID);
      ochkAnalyst = document.getElementById("chkAnalyst" + strClaimAspectID);
      ochkSupport = document.getElementById("chkSupport" + strClaimAspectID);
      var bDisabled = (ochkEntity.value == 0);
      if (bDisabled){
        if (ochkOwner && ochkOwner.value == 1) ochkOwner.value = 0;
        if (ochkAnalyst && ochkAnalyst.value == 1) ochkAnalyst.value = 0;
        if (ochkSupport && ochkSupport.value == 1) ochkSupport.value = 0;
      }
      if (ochkOwner) ochkOwner.CCDisabled = bDisabled;
      if (ochkAnalyst) ochkAnalyst.CCDisabled = bDisabled;
      if (ochkSupport) ochkSupport.CCDisabled = bDisabled;
      
      /*if (bDisabled == false){
        var oExposure = xmlExposures.selectSingleNode("/Root/Exposure[@ClaimAspectID='" + strClaimAspectID + "']");
        if (oExposure.getAttribute("FOUserID") == sUserId){
          if (ochkOwner) ochkOwner.value = 1
        }
        if (oExposure.getAttribute("FAUserID") == sUserId){
          if (ochkAnalyst) ochkAnalyst.value = 1
        }
        if (oExposure.getAttribute("FSUserID") == sUserId){
          if (ochkSupport) ochkSupport.value = 1
        }
      }*/
    }
  }
  
  function enableAssignTo(strFunction, strClaimAspectID){
    var ochkFunction = document.getElementById("chk" + strFunction + strClaimAspectID);
    if (ochkFunction){
      var oAssignTo = document.getElementById("sel" + strFunction + "AssignTo" + strClaimAspectID);
      if (oAssignTo){
        var bDisabled = (ochkFunction.value == 0);
        if (bDisabled)
          oAssignTo.selectedIndex = -1;
        oAssignTo.CCDisabled = bDisabled;
        if (ochkFunction.value == 1){
          if (oAssignTo.Options.length == 1){
            //preselect the only one item
            oAssignTo.selectedIndex = 0;
          }
          
          var oExposure = xmlExposures.selectSingleNode("/Root/Exposure[@ClaimAspectID='" + strClaimAspectID + "']");
          var strFunctionUser;
          if (oExposure){
            switch(strFunction){
              case "Owner":
                strFunctionUserID = oExposure.getAttribute("FOUserID");
                preSelectAssignTo(strFunctionUserID, oAssignTo);
                break;
              case "Analyst":
                strFunctionUserID = oExposure.getAttribute("FAUserID");
                preSelectAssignTo(strFunctionUserID, oAssignTo);
                break;
              case "Support":
                strFunctionUserID = oExposure.getAttribute("FSUserID");
                preSelectAssignTo(strFunctionUserID, oAssignTo);
                break;
            }
          }
        }
      }
    }
  }
  
  function preSelectAssignTo(strFunctionUserID, oSelAssignTo){
    //preselect Self
    if (strFunctionUserID == sUserId){
      oSelAssignTo.value = sUserId;
    } else if (oSelAssignTo.GetTextFromValue(sUserId)) {
      //preselect self
      oSelAssignTo.value = sUserId;
    } else if (oSelAssignTo.GetTextFromValue(strFunctionUserID)){
      //current assigned user is part of the list. preselect
      oSelAssignTo.value = strFunctionUserID;
    } else {
      //preselect the first available pool
      oSelAssignTo.selectedIndex = 0;
    }
  }
  
  function doReopen(){
    var strClaimAspectID;
    var strEntityName;
    var sProc = "";
    var sRequest = "";
    var aRequests = new Array();
    var sMethod = "";
    var strReopenList = "";
    var bClaimAspectAdded = false;
    
    if (txtComments.value.Trim() == ""){
      ClientWarning("APD System requires a non-blank comment to reopen the selected Exposures. Please enter comments and try reopen again.");
      return;
    }
    
    //build the reopen list
    if (oExposures){
      for (var i = 0; i < oExposures.length; i++){
        strClaimAspectID = oExposures[i].getAttribute("ClaimAspectID");
        strEntityName = oExposures[i].getAttribute("Name");
        var sFOUserID  = oExposures[i].getAttribute("FOUserID");
        var sFAUserID  = oExposures[i].getAttribute("FAUserID");
        var sFSUserID  = oExposures[i].getAttribute("FSUserID");

        var ochkEntity = document.getElementById("chk" + strClaimAspectID);
        bClaimAspectAdded = false;
        if (ochkEntity && ochkEntity.value == 1){
          var ochkOwner = document.getElementById("chkOwner" + strClaimAspectID);
          var ochkAnalyst = document.getElementById("chkAnalyst" + strClaimAspectID);
          var ochkSupport = document.getElementById("chkSupport" + strClaimAspectID);
          
          if (ochkOwner) {
            if (ochkOwner.value == 1){
              var oSelOwnerAssignTo = document.getElementById("selOwnerAssignTo" + strClaimAspectID);
              if (oSelOwnerAssignTo && oSelOwnerAssignTo.selectedIndex == -1){
                ClientWarning("Please select Owner Assign To for " + strEntityName);
                return;
              }
              
              if (oSelOwnerAssignTo.value.indexOf("_") != -1){
                strReopenList += strClaimAspectID + ",OWN,," + oSelOwnerAssignTo.value.split("_")[1] + "|";
              } else
                strReopenList += strClaimAspectID + ",OWN," + oSelOwnerAssignTo.value + ",|";
              bClaimAspectAdded = true;
            } else {
              strReopenList += strClaimAspectID + ",,,|";
              bClaimAspectAdded = true;
            }
          } 


          if (ochkAnalyst) {
            if (ochkAnalyst.value == 1){
              var oSelAnalystAssignTo = document.getElementById("selAnalystAssignTo" + strClaimAspectID);
              if (oSelAnalystAssignTo && oSelAnalystAssignTo.selectedIndex == -1){
                ClientWarning("Please select Analyst Assign To for " + strEntityName);
                return;
              }
              
              if (oSelAnalystAssignTo.value.indexOf("_") != -1){
                strReopenList += strClaimAspectID + ",ALST,," + oSelAnalystAssignTo.value.split("_")[1] + "|";
              } else
                strReopenList += strClaimAspectID + ",ALST," + oSelAnalystAssignTo.value + ",|";
              bClaimAspectAdded = true;
            } else {
              if (!bClaimAspectAdded) strReopenList += strClaimAspectID + ",,,|";
            }
          } 


          if (ochkSupport) {
            if (ochkSupport.value == 1){
              var oSelSupportAssignTo = document.getElementById("selSupportAssignTo" + strClaimAspectID);
              if (oSelSupportAssignTo && oSelSupportAssignTo.selectedIndex == -1){
                ClientWarning("Please select Support Assign To for " + strEntityName);
                return;
              }
              
              if (oSelSupportAssignTo.value.indexOf("_") != -1){
                strReopenList += strClaimAspectID + ",SPRT,," + oSelSupportAssignTo.value.split("_")[1] + "|";
              } else
                strReopenList += strClaimAspectID + ",SPRT," + oSelSupportAssignTo.value + ",|";
            } else {
              if (!bClaimAspectAdded) strReopenList += strClaimAspectID + ",,,|";
            }
          } 
        }
      }
    }
    
    if (strReopenList.length == 0){
      ClientWarning("No Entity has been selected for reopen.");
      return;
    }
    
    sProc = "uspWorkflowReopenExposure";
    sMethod = "ExecuteSpNp";
    sRequest = "LynxID=" + sLynxId +
               "&ClaimAspectList=" + sClmAspectID + ",,,|" + strReopenList + 
               "&Comments=" + escape(txtComments.value) + 
               "&UserID=" + sUserId + 
               "&ApplicationCD=APD";
               
     aRequests.push( { procName : sProc,
               method   : sMethod,
               data     : sRequest }
           );
           
      var sXMLRequest = makeXMLSaveString(aRequests);
      //alert(sXMLRequest); return;
      
      var objRet = XMLSave(sXMLRequest);
      
      if (objRet && objRet.code == 0) {
        window.returnValue = "OK";
        window.close();
      }                
     
    
  }
  
  function doClose(){
    window.close();
  }

]]>

</SCRIPT>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="margin:5px;padding:0px;overflow:hidden" tabIndex="-1" onload="pageInit()">
    <span id="msg" name="msg" style="position:absolute;top:0;left:0;display:none;background-color:#FFFFFF;">
        <table border="0" cellspacing="0" cellpadding="0" unselectable="on" style="height:100%">
            <tr>
                <td align="center">
                    <strong><font color="#FF0000">You do not have permission to access this page. Please contact your supervisor.</font></strong>
                </td>
            </tr>
        </table>
    </span>
    <span id="spnMain" name="spnMain">
      <table border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
        <colgroup>
          <col width="75px"/>
          <col width="350px"/>
        </colgroup>
        <tr>
          <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
              <colgroup>
                <col width="105px"/>
                <col width="150px"/>
                <col width="150px"/>
              </colgroup>
              <tr>
                <td class="Header">Exposure</td>
                <td class="Header">Current User</td>
                <td class="Header">Assign To</td>
              </tr>
            </table>
            <div style="overflow:auto;width:425px;height:155px;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
                <colgroup>
                  <col width="105px"/>
                  <col width="150px"/>
                  <col width="150px"/>
                </colgroup>
                <xsl:for-each select="Exposure">
                <tr style="height:23px">
                  <td colspan="3">
                    <IE:APDCheckBox CCDisabled="false" required="false" canDirty="false">
                      <xsl:attribute name="id">chk<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">chk<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="caption"><xsl:value-of select="concat(@Name, ' (', @CurrentServiceChannelCD, ')')"/></xsl:attribute>
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() + 10"/></xsl:attribute>
                      <xsl:attribute name="onChange">enableFunction(<xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
                    </IE:APDCheckBox>
                  </td>
                </tr>
                <tr style="height:18px">
                  <td style="padding-left:18px">
                    <IE:APDCheckBox caption="Owner" CCDisabled="true" required="false" canDirty="false" onBeforeChange="" onChange="" onAfterChange="" >
                      <xsl:attribute name="id">chkOwner<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">chkOwner<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 1"/></xsl:attribute>
                      <xsl:attribute name="onChange">enableAssignTo('Owner', <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
                    </IE:APDCheckBox>                    
                  </td>
                  <td>
                    <xsl:variable name="ActiveStatus">
                      <xsl:if test="@FOIsActive='0'">(Inactive)</xsl:if>
                    </xsl:variable>
                    <span class="DataValNoWrap" style="cursor:default">
                      <xsl:if test="@FOIsActive='0'">
                        <xsl:attribute name="style">color:#FF0000</xsl:attribute>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="@FOUserNameLast != ''">
                          <xsl:value-of select="concat(@FOUserNameLast, ', ', @FOUserNameFirst, ' ', $ActiveStatus)"/>
                        </xsl:when>
                        <xsl:otherwise>Not Assigned</xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </td>
                  <td>
                    <IE:APDCustomSelect blankFirst="false" canDirty="false" CCDisabled="true" required="false" width="150" dropdownWidth="200" onChange="">
                      <xsl:attribute name="id">selOwnerAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">selOwnerAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>         
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 2"/></xsl:attribute>
                    </IE:APDCustomSelect>
                  </td>
                </tr>
                <tr style="height:18px">
                  <td style="padding-left:18px">
                    <IE:APDCheckBox caption="Analyst" CCDisabled="true" required="false" canDirty="false" onBeforeChange="" onChange="" onAfterChange="" >
                      <xsl:attribute name="id">chkAnalyst<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">chkAnalyst<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 3"/></xsl:attribute>
                      <xsl:attribute name="onChange">enableAssignTo('Analyst', <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
                    </IE:APDCheckBox>                    
                  </td>
                  <td>
                    <xsl:variable name="ActiveStatus">
                      <xsl:if test="@FAIsActive='0'">(Inactive)</xsl:if>
                    </xsl:variable>
                    <span class="DataValNoWrap" style="cursor:default">
                      <xsl:if test="@FAIsActive='0'">
                        <xsl:attribute name="style">color:#FF0000</xsl:attribute>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="@FAUserNameLast != ''">
                          <xsl:value-of select="concat(@FAUserNameLast, ', ', @FAUserNameFirst, ' ', $ActiveStatus)"/>
                        </xsl:when>
                        <xsl:otherwise>Not Assigned</xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </td>
                  <td>
                    <IE:APDCustomSelect blankFirst="false" canDirty="false" CCDisabled="true" required="false" width="150" dropdownWidth="200" onChange="">
                      <xsl:attribute name="id">selAnalystAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">selAnalystAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>                      
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 4"/></xsl:attribute>
                    </IE:APDCustomSelect>
                  </td>
                </tr>
                <tr style="height:18px">
                  <td style="padding-left:18px">
                    <IE:APDCheckBox caption="Support" CCDisabled="true" required="false" canDirty="false" onBeforeChange="" onChange="" onAfterChange="" >
                      <xsl:attribute name="id">chkSupport<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">chkSupport<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 5"/></xsl:attribute>
                      <xsl:attribute name="onChange">enableAssignTo('Support', <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
                    </IE:APDCheckBox>                    
                  </td>
                  <td>
                    <xsl:variable name="ActiveStatus">
                      <xsl:if test="@FSIsActive='0'">(Inactive)</xsl:if>
                    </xsl:variable>
                    <span class="DataValNoWrap" style="cursor:default">
                      <xsl:if test="@FSIsActive='0'">
                        <xsl:attribute name="style">color:#FF0000</xsl:attribute>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="@FSUserNameLast != ''">
                          <xsl:value-of select="concat(@FSUserNameLast, ', ', @FSUserNameFirst, ' ', $ActiveStatus)"/>
                        </xsl:when>
                        <xsl:otherwise>Not Assigned</xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </td>
                  <td>
                    <IE:APDCustomSelect blankFirst="false" canDirty="false" CCDisabled="true" required="false" width="150" dropdownWidth="200" onChange="">
                      <xsl:attribute name="id">selSupportAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
                      <xsl:attribute name="name">selSupportAssignTo<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>                      
                      <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() * 10 + 6"/></xsl:attribute>
                    </IE:APDCustomSelect>
                  </td>
                </tr>
                </xsl:for-each>
              </table>
            </div>
          </td>
        </tr>
        <tr style="height:10px">
          <td colspan="3"/>
        </tr>
        <xsl:variable name="ExposureCount"><xsl:value-of select="count(/Root/Exposure)"/></xsl:variable>
        <tr valign="top">
          <td>Comments:</td>
          <td>
            <IE:APDTextArea id="txtComments" name="txtComments" maxLength="250" width="350" height="40" required="true" canDirty="false" CCDisabled="false" onChange="" >
              <xsl:attribute name="CCTabIndex"><xsl:value-of select="number($ExposureCount) * 10 + 10"/></xsl:attribute>
            </IE:APDTextArea>
          </td>
        </tr>
        <tr>
          <td/>
          <td>
            <IE:APDButton id="btnOk" name="btnOk" value="OK" CCDisabled="false" onButtonClick="doReopen()">
              <xsl:attribute name="CCTabIndex"><xsl:value-of select="number($ExposureCount) * 10 + 11"/></xsl:attribute>
            </IE:APDButton>
            <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" CCDisabled="false" onButtonClick="doClose()">
              <xsl:attribute name="CCTabIndex"><xsl:value-of select="number($ExposureCount) * 10 + 12"/></xsl:attribute>
            </IE:APDButton>
          </td>
        </tr>
      </table>
    </span>
    
    <xml id="xmlExposures" name="xmlExposures">
      <xsl:copy-of select="/"/>
    </xml>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
