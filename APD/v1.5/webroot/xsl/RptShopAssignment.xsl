<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function formatDate(strDate)
     {
        var strRet = "";
        var strTmp = "";
        var aTmp;
        if (strDate != "") {
            strTmp = strDate.split("T")[0];
            aTmp = strTmp.split("-");
            strRet = aTmp[1] + "/" + aTmp[2] + "/" + aTmp[0];
        }
        return strRet;
     }
    function formatDateTime(strDate)
     {
        var strRet = "";
        var strTmp = "";
        var aTmp;
        if (strDate != "") {
            strTmp = strDate.split("T")[0];
            aTmp = strTmp.split("-");
            strRet = aTmp[1] + "/" + aTmp[2] + "/" + aTmp[0];
            strRet += " " + strDate.split("T")[1].split(".")[0];
        }
        return strRet;
     }
    function formatPhone(strPhone)
     {
        var strRet = strPhone;
        if (strPhone != "") {
        	var re = /^(\d{3})(\d{3})(\d{4})/;
        	var ret = strPhone.match(re);
        	if (ret != null && ret[1] != null && ret[2] != null && ret[3] != null)
                strRet = "(" + ret[1] + ") " + ret[2] + "-" + ret[3];
        }
        return strRet;
     }
     function concatString(str1, str2)
     {
        if ((str1.length > 0) && (str2.length > 0))
            return str1 + ", " + str2;
        else
            return str1 + str2;
     }
  ]]>
</msxsl:script>

<xsl:template match="/Assignment">

<xsl:choose>
    <xsl:when test="/Assignment/@ErrorDesc[.!='']">
        <!-- Display Error Message -->
        <font color="#0000ff"><strong><xsl:value-of select="/Assignment/@ErrorDesc"/></strong></font>
    </xsl:when>
    <xsl:otherwise>
        <!-- Display Report -->        
        <div align="left" style="width:725px;margin-bottom:4px;">
        <input type="button" class="formbutton" onclick="printPage()" value="Print" id="btnPrint"/>
        </div>
        <div id="rptText" style="width:725px;height:340px;background-color:#FFFFFF;border:1px solid #c0c0c0;border-bottom:3px solid #000000;border-right:3px solid #000000;padding:5px;overflow:auto;">
        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
            <tr>
                <td><div style="font:bolder 12pt Tahoma;">Shop Assignment Report</div></td>
                <td><div style="font:bolder 12pt Tahoma;" align="right">LYNX Services</div></td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td><strong>LYNX ID:   </strong><xsl:value-of select="/Assignment/@LynxId"/></td>
                <td></td>
                <td><strong>Vehicle Number:   </strong><xsl:value-of select="/Assignment/@VehicleNumber"/></td>
                <td></td>
                <td align="right"><strong>Report Date:   </strong><xsl:value-of select="user:formatDateTime(string(/Assignment/@CurrentDateTime))"/></td>
            </tr>
        </table>
        <table cellspacing="5" cellpadding="0" border="0">
            <tr valign="top">
                <td>
                    <!-- Screen 1-->
                    <table cellspacing="0" cellpadding="2" border="1" bordercolor="#000000" style="border-collapse:collapse;border-bottom:3px solid;border-right:3px solid;" width="300px">
                        <colgroup>
                            <col width="125px"/>
                            <col/>
                        </colgroup>
                        <tr>
                            <td colspan="2" style="background-color:#c0c0c0;" align="center"><strong>Assignments (Screen 1)</strong></td>
                        </tr>
                        <tr>
                            <td>Claim Office</td>
                            <td>FTM</td>
                        </tr>
                        <tr>
                            <td>Date Reported</td>
                            <td><xsl:value-of select="user:formatDate(string(/Assignment/Claim/@NoticeDate))"/></td>
                        </tr>
                        <tr>
                            <td>Date of Loss</td>
                            <td><xsl:value-of select="user:formatDate(string(/Assignment/Claim/@LossDate))"/></td>
                        </tr>
                        <tr>
                            <td>Internal Assignment</td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="/Assignment/@DestinationType[.='Shop']">
                                        <xsl:text>N</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>Y</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td>Assigned to</td>
                            <td><!--<xsl:value-of select="/Assignment/Shop/@ShopName"/>--></td>
                        </tr>
                        <tr>
                            <td>Adjuster</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Assigned By</td>
                            <td><!--<xsl:value-of select="user:concatString(string(/Assignment/LynxRep/@RepNameLast), string(/Assignment/LynxRep/@RepNameFirst))"/>--></td>
                        </tr>
                        <tr>
                            <td>Work Assigned</td>
                            <td><!--
                                <xsl:choose>
                                    <xsl:when test="/Assignment/Shop/@ShopName[.!='']">
                                        <xsl:text>Y</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>N</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>-->
                            </td>
                        </tr>
                        <tr>
                            <td>Type of Loss</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@CoverageType"/></td>
                        </tr>
                        <tr>
                            <td>Claim Number</td>
                            <td><xsl:value-of select="/Assignment/@LynxId"/></td>
                        </tr>
                        <tr>
                            <td>Policy Number</td>
                            <td><xsl:value-of select="/Assignment/Claim/@PolicyNumber"/></td>
                        </tr>
                        <tr>
                            <td>Deductible Amount</td>
                            <td><xsl:if test="/Assignment/Coverage/@Deductible[.!= '']">
                                $
                                </xsl:if>
                                <xsl:value-of select="/Assignment/Coverage/@Deductible"/></td>
                        </tr>
                        <tr>
                            <td>Insured Owns Vehicle</td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="((/Assignment/Claim/@InsuredFirstName[.= /Assignment/Owner/@OwnerFirstName]) and (/Assignment/Claim/@InsuredLastName[.= /Assignment/Owner/@OwnerLastName]))">
                                        <xsl:text>Y</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>N</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                    </table>
                    <!-- Screen 1 -->
                </td>
                <td>
                    <!-- Screen 2 -->
                    <table cellspacing="0" cellpadding="2" border="1" bordercolor="#000000" style="border-collapse:collapse;border-bottom:3px solid;border-right:3px solid;" width="300px">
                        <colgroup>
                            <col width="125px"/>
                            <col/>
                        </colgroup>
                        <tr>
                            <td colspan="2" style="background-color:#c0c0c0;" align="center"><strong>Assignments (Screen 2)</strong></td>
                        </tr>
                        <tr>
                            <td>Insured</td>
                            <td><xsl:value-of select="user:concatString(string(/Assignment/Claim/@InsuredLastName), string(/Assignment/Claim/@InsuredFirstName))"/></td>
                        </tr>
                        <tr>
                            <td>Claimant</td>
                            <td><xsl:value-of select="user:concatString(string(/Assignment/Owner/@OwnerLastName), string(/Assignment/Owner/@OwnerFirstName))"/></td>
                        </tr>
                        <tr>
                            <td>Owners Address</td>
                            <td><xsl:value-of select="user:concatString(string(/Assignment/Owner/@OwnerAddress1), string(/Assignment/Owner/@OwnerAddress2))"/></td>
                        </tr>
                        <tr>
                            <td>City/State/Zip</td>
                            <td><xsl:value-of select="user:concatString(user:concatString(string(/Assignment/Owner/@OwnerCity), string(/Assignment/Owner/@OwnerState)), string(/Assignment/Owner/@OwnerZip))"/></td>
                        </tr>
                        <tr>
                            <td>Day Phone/Ext</td>
                            <td>
                                <xsl:value-of select="user:formatPhone(string(/Assignment/Owner/@OwnerDayPhone))"/>
                                <xsl:if test="/Assignment/Owner/@OwnerDayPhoneExtension[.!= '']">
                                    x<xsl:value-of select="/Assignment/Owner/@OwnerDayPhoneExtension"/>
                                </xsl:if>
                            </td>
                        </tr>
                        <tr>
                            <td>Other Phone/Ext</td>
                            <td>
                                <xsl:value-of select="user:formatPhone(string(/Assignment/Owner/@OwnerAlternatePhone))"/>
                                <xsl:if test="/Assignment/Owner/@OwnerAlternatePhoneExtension[.!= '']">
                                    x<xsl:value-of select="/Assignment/Owner/@OwnerAlternatePhoneExtension"/>
                                </xsl:if>
                            </td>
                        </tr>
                        <tr>
                            <td>VIN</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@Vin"/></td>
                        </tr>
                        <tr>
                            <td>Year</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@VehicleYear"/></td>
                        </tr>
                        <tr>
                            <td>Make</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@Make"/></td>
                        </tr>
                        <tr>
                            <td>Model</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@Model"/></td>
                        </tr>
                        <tr>
                            <td>Body Style</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@BodyStyle"/></td>
                        </tr>
                        <tr>
                            <td>Odometer</td>
                            <td><xsl:value-of select="round(/Assignment/Vehicle/@Mileage)"/></td>
                        </tr>
                        <tr>
                            <td>License #/State</td>
                            <td><xsl:value-of select="concat(/Assignment/Vehicle/@LicensePlateNo, '/', /Assignment/Vehicle/@LicensePlateState)"/></td>
                        </tr>
                        <tr>
                            <td>Color</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@Color"/></td>
                        </tr>
                    </table>
                    <!-- Screen 2 -->
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <!-- Screen 3 -->
                    <table cellspacing="0" cellpadding="2" border="1" bordercolor="#000000" style="border-collapse:collapse;border-bottom:3px solid;border-right:3px solid;">
                        <colgroup>
                            <col width="125px"/>
                            <col/>
                        </colgroup>
                        <tr>
                            <td colspan="2" style="background-color:#c0c0c0;" align="center"><strong>Assignments (Screen 3)</strong></td>
                        </tr>
                        <tr>
                            <td>Primary Impact</td>
                            <td>
                                <!--<xsl:if test="/Assignment/Vehicle/@PointOfImpactCode[.!= '']">-->
                                <xsl:if test="/Assignment/Vehicle[@PointOfImpactCode != '']">
                                    <xsl:value-of select="/Assignment/Vehicle/@PointOfImpactCode"/>
                                    <xsl:text> : </xsl:text>
                                </xsl:if>
                                <xsl:value-of select="/Assignment/Vehicle/@PointOfImpactPrimary"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Secondary Impact</td>
                            <td><xsl:value-of select="/Assignment/Vehicle/@PointOfImpactSecondary"/></td>
                        </tr>
                        <tr>
                            <td>Impact Notes</td>
                            <td><xsl:value-of select="/Assignment/Claim/AccidentDescription"/></td>
                        </tr>
                        <tr>
                            <td>Drivable</td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="(/Assignment/Vehicle/@Driveable[.= '1'])">
                                        <xsl:text>Y</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>N</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td>Rental Car in Use</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Total Loss</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Vehicle Location</td>
                            <td><xsl:value-of select="user:concatString(string(/Assignment/Vehicle/@LocationAddress1), string(/Assignment/Vehicle/@LocationAddress2))"/></td>
                        </tr>
                        <tr>
                            <td>City/State/Zip</td>
                            <td><xsl:value-of select="user:concatString(user:concatString(string(/Assignment/Vehicle/@LocationCity), string(/Assignment/Vehicle/@LocationState)), string(/Assignment/Vehicle/@LocationZip))"/></td>
                        </tr>
                        <tr>
                            <td>Telephone</td>
                            <td>
                                <xsl:value-of select="user:formatPhone(string(/Assignment/Vehicle/@LocationPhone))"/>
                                <xsl:if test="/Assignment/Vehicle/@LocationPhoneExtension[.!= '']">
                                    x<xsl:value-of select="/Assignment/Vehicle/@LocationPhoneExtension"/>
                                </xsl:if>
                            </td>
                        </tr>
                        <tr>
                            <td>Instructs</td>
                            <td><xsl:value-of select="user:formatPhone(string(/Assignment/Vehicle/@ShopRemarks))"/></td>
                        </tr>
                        <tr>
                            <td>Appointment Date</td>
                            <td></td>
                        </tr>
                    </table>
                    <!-- Screen 3 -->
                </td>
            </tr>
        </table>       
        </div>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>