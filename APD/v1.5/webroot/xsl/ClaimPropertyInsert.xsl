<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt"
		xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
		xmlns:js="urn:the-xml-files:xslt"
		id="ClaimProperty">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="PropertyCRUD" select="Property"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxID"/>
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/>

<!-- <xsl:variable name="LynxID" select="/Root/@LynxID" /> -->
<xsl:variable name="PropertyNumber" select="/Root/@PropertyNumber" />
<xsl:variable name="PropertyLastUpdatedDate" select="/Root/Property/@SysLastUpdatedDate"/>
<xsl:choose>
    <xsl:when test="not(contains($PropertyCRUD, 'C'))">
        <html>
        <head>
        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        </head>
        <body unselectable="on" class="bodyAPDSub">
        <div style="width:100%;text-align:center">
        <br/>
        <font color="#FF0000">You do not have permissions to add a new Property.</font>
        <br/>
        Please contact your supervisor.
        </div>
        </body>
        </html>
    </xsl:when>
    <xsl:otherwise>
        <HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimPropertyGetDetailXML,ClaimVehicleInsert.xsl,6366,0,145   -->

        <HEAD>
        <TITLE>Property Details</TITLE>

        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

        <STYLE type="text/css">
        	  A {color:black; text-decoration:none;}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
        <SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
        <SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
		<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
		<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,24);
		  event.returnValue=false;
		  };	
		</script>

        <SCRIPT language="JavaScript">

          var gsWindowID = '<xsl:value-of select="$WindowID"/>';
        	var gsLynxID = '<xsl:value-of select="$LynxID"/>';
          var gsUserID = '<xsl:value-of select="$UserId"/>';
        	var gsPropNum = '<xsl:value-of select="$PropertyNumber"/>';
          var gsPropertyLastUpdatedDate = "<xsl:value-of select="$PropertyLastUpdatedDate"/>";
        	var gsCRUD = '<xsl:value-of select="$PropertyCRUD"/>';
          var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";

        <![CDATA[

        	// Tooltip Variables to set:
        	messages= new Array()
        	// Write your descriptions in here.
        	messages[0]="Double Click to Expand"
        	messages[1]="Double Click to Close"

        	// button images for ADD/DELETE/SAVE
        	preload('buttonAdd','/images/but_ADD_norm.png')
        	preload('buttonAddDown','/images/but_ADD_down.png')
        	preload('buttonAddOver','/images/but_ADD_over.png')
        	preload('buttonDel','/images/but_DEL_norm.png')
        	preload('buttonDelDown','/images/but_DEL_down.png')
        	preload('buttonDelOver','/images/but_DEL_over.png')
        	preload('buttonSave','/images/but_SAVE_norm.png')
        	preload('buttonSaveDown','/images/but_SAVE_down.png')
        	preload('buttonSaveOver','/images/but_SAVE_over.png')

        	// Page Initialize
        	function PageInit()
        	{
        		if (gsCRUD.indexOf("C") == -1)
        		{
              ClientWarning("You do not have rights to create a new Property.  See your supervisor.");
              document.parentWindow.frameElement.src="blank.asp";
        		} else if (gsClaimStatus == "Claim Cancelled" || gsClaimStatus == "Claim Voided") {
              if (gsClaimStatus == "Claim Cancelled")
                var str = "cancelled";
              else
                var str = "voided";

              ClientWarning("You cannot add a new property to this claim because the claim has been " + str + ".");
              document.parentWindow.frameElement.src="blank.asp";
            }


        		top.HideMainTabs();
        	}

        	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
          var objDivWithButtons = null;

          	// ADS Button pressed
        	function ADS_Pressed(sAction, sName)
        	{
            if (ExposureCD.value == ""){
              ClientWarning("Party is a required field. Please pick one from the list.");
              return;
            }
        		if (!gbDirtyFlag) return; //no change made to the page
            if (inADS_Pressed == true) return;
        		inADS_Pressed = true;
            objDivWithButtons = divWithButtons;
            disableADS(objDivWithButtons, true);

        		var oDiv = document.getElementById(sName)

        		if (sAction == "Update")
        		{
          		var sRequest = GetRequestString(sName);

          		sRequest += "PropertySysLastUpdatedDate=" + gsPropertyLastUpdatedDate + "&";
          		//sRequest += "PropertyNumber=" + gsPropNum + "&";
          		sRequest += "LynxID=" + gsLynxID + "&";
          		sRequest += "UserID=" + gsUserID + "&";
        			sRequest += "NotifyEvent=1&";

          		var retArray = DoCrudAction(oDiv, "Insert", sRequest);

              if (retArray[1] != "0")
          		{
          			//update the last Updated date...

        				var objXML = new ActiveXObject("Microsoft.XMLDOM");
        				objXML.loadXML(retArray[0]);
        				var objNodeList = objXML.documentElement.selectNodes("/Root/Property");
        				var objNode;
                var nLength = objNodeList.length;
        				// get last one added
        		    for (var n = 0; n < (nLength - 1); n++)
        		    {
        					objNode = objNodeList.nextNode();
        				}
        				//last one
        				objNode = objNodeList.nextNode();
        				var sPropNum = objNode.selectSingleNode( "@PropertyNumber" ).text;
                var sClaimAspect = objNode.selectSingleNode( "@ClaimAspectID" ).text;
                document.parentWindow.frameElement.src="ClaimPropertyList.asp?WindowID=" + gsWindowID + "&LynxID=" + gsLynxID + "&PropContext=" + sPropNum + "&ClaimAspectID=" + sClaimAspect;
        			}
        		}

        		inADS_Pressed = false;
            disableADS(objDivWithButtons, false);
            objDivWithButtons = null;
        	}

          var objCoverageCodes = new Array();
          function onExposureChange(){
            onSelectChange(selExposureCD);
            var sExposureCD = selExposureCD.options[selExposureCD.selectedIndex].value;
            if (objCoverageCodes.length == 0){
              for(var i = 0; i < selCoverageProfileCD.options.length; i++) {
                var objCoverage = new Object();
                objCoverage.value = selCoverageProfileCD.options[i].value;
                objCoverage.text = selCoverageProfileCD.options[i].innerText;
                objCoverageCodes.push(objCoverage);
              }
            }
              for(var i = selCoverageProfileCD.options.length - 1; i > 0; i--)
                selCoverageProfileCD.remove(i);

            switch(sExposureCD) {
              case "1":
                for (var i = 0; i < objCoverageCodes.length; i++){
                  if (objCoverageCodes[i].text == "Comprehensive" || objCoverageCodes[i].text == "Collision")
                    selCoverageProfileCD.add(objCoverageCodes[i]);
                }
                break;
              case "3":
                for (var i = 0; i < objCoverageCodes.length; i++){
                  if (objCoverageCodes[i].text == "Liability")
                    selCoverageProfileCD.add(objCoverageCodes[i]);
                }
                break;
              case "N":
                break;
            }
          }
        	if (document.attachEvent)
        		document.attachEvent("onclick", top.hideAllMenuScriptlets);
        	else
        		document.onclick = top.hideAllMenuScriptlets;


        ]]>
        </SCRIPT>

        </HEAD>
        <BODY unselectable="on" class="bodyAPDSub" onLoad="initSelectBoxes(); InitFields(); PageInit();" style="overflow:hidden;">
        <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
        <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
        <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'">
          <xsl:value-of select="js:SetCRUD( '_R__' )"/>
          <xsl:value-of select="js:setPageDisabled()"/>
        </xsl:if>

        <DIV unselectable="on" style="position:absolute; visibility: visible; top: 10px; left: 0px">
          <DIV unselectable="on" id="DescriptionInfoTab" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 0px; left: 6px;">
            Add New Property - Description</DIV>

          <DIV unselectable="on" id="ClaimPropertyInsert" name="ClaimPropertyInsert" Insert="uspClaimPropertyInsDetail" class="SmallGrouping" onfocusin="DivOnFocusIn(this)"  style="position:absolute; left:3px; top:16px; z-index:1">
            <xsl:attribute name="CRUD">
              <xsl:choose>
                <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
                <xsl:otherwise><xsl:value-of select="$PropertyCRUD"/></xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
        		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Property/@SysLastUpdatedDate"/></xsl:attribute>

          <!-- Start Description Info -->
            		<xsl:for-each select="Property" >
            			<xsl:call-template name="DescriptionInfo"/>
            		</xsl:for-each>

          <!-- Start Property Contact Info -->
        		<TABLE width="100%">
        			<TR>
                <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        			</TR>
        	    <TR>
                <TD unselectable="on" align="center">
        					<img src="\images\background_top_flip.gif" width="45%" />
        					<img src="\images\background_top.gif" width="45%" />
        				</TD>
              </TR>
        			<TR>
                <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Contact Info</TD>
        			</TR>
        		</TABLE>

        		<xsl:for-each select="Property/Contact" >
        			<xsl:call-template name="PropertyContactInfo"/>
        		</xsl:for-each>

          <!-- Start Remarks to Claims Rep Info -->
        		<TABLE width="100%">
        			<TR>
                <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        			</TR>
        	    <TR>
                <TD unselectable="on" align="center">
        					<img src="\images\background_top_flip.gif" width="45%" />
        					<img src="\images\background_top.gif" width="45%" />
        				</TD>
              </TR>
        			<TR>
                <TD unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Remarks to Claims Rep</TD>
        			</TR>
        		</TABLE>

        		<xsl:for-each select="Property" >
        			<xsl:call-template name="RemarksToClaimsRepInfo"/>
        		</xsl:for-each>

          <SCRIPT>ADS_Buttons('ClaimPropertyInsert');</SCRIPT>

          </DIV>
        </DIV>

        <DIV id="divTooltip"> </DIV>
        </BODY>
        </HTML>
    </xsl:otherwise>

</xsl:choose>
</xsl:template>



	<!-- Gets the Description Info -->
  <xsl:template name="DescriptionInfo" >
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" valign="top">Desc.:</TD>
        <TD unselectable="on" colspan="3">
					<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropDesc',55,4,'PropertyDescription',.,'','',1)"/>
        </TD>
        <TD unselectable="on" valign="top">Damage:</TD>
        <TD unselectable="on" colspan="5">
					<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropDamage',55,4,'DamageDescription',.,'','',2)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on" colspan="8">
					<IMG unselectable="on" src="images/spacer.gif" border="0" height="2" width="1"/>
				</TD>
      </TR>
      <TR>
        <TD unselectable="on">Name:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropName',25,'Name',.,'','',3)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on">Address:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropAddress1',25,'LocationAddress1',.,'','',8)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on">City:</TD>
        <TD unselectable="on">
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropCity',15,'LocationCity',.,'','',10)"/>
        </TD>
      <TD unselectable="on">Party:</TD>
      <TD>
        <xsl:variable name="sExposureCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('ExposureCD',2,'onExposureChange',string($sExposureCD),1,3,/Root/Reference[@List='Exposure'],'Name','ReferenceID','100','','','',11,1)"/>
      </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneAC',1,'LocationAreaCode',.,'',10,4)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneEX',1,'LocationExchangeNumber',.,'',10,5)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneNM',2,'LocationUnitNumber',.,'',10,6)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneXT',3,'LocationExtensionNumber',.,'',10,7)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropAddress2',25,'LocationAddress2',.,'','',9)"/>
        </TD>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropState',2,'LocationState',.,'',12,11)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropZip',7,'LocationZip',.,'',10,12)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtPropState', 'txtPropCity');"></xsl:text>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      <TD unselectable="on">Coverage:</TD>
      <TD>
        <xsl:variable name="rtCov" select="@CoverageProfileCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('CoverageProfileCD',2,'onSelectChange',string($rtCov),1,2,/Root/Reference[@List='CoverageProfile'],'Name','ReferenceID','90','','','',13,1)"/>
      </TD>
      </TR>
      <TR>
        <TD colspan="6"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Estimate Damage:</TD>
        <TD>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropEst',10,'DamageAmt',.,'','',14)"/>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

	<!-- Gets the Property Contact Info -->
  <xsl:template name="PropertyContactInfo" >
      <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropNameTitle',3,'NameTitle',.,'','',15)"/>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContNameFirst',15,'NameFirst',.,'','',16)"/>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContNameLast',15,'NameLast',.,'','',17)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Address:</TD>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContAddress1',15,'Address1',.,'','',22)"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContAddress2',15,'Address2',.,'',6,23)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:
          </TD>
          <TD unselectable="on">
					  <xsl:variable name="rtContactRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_1',2,'onSelectChange',number($rtContactRelToIns),1,5,/Root/Reference[@List='ContactRelationToInsured'],'Name','ReferenceID',0,45,'','',27)"/>
					</TD>
        </TR>
        <TR>
          <TD unselectable="on">Phone:</TD>
          <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneAC',1,'DayAreaCode',.,'',10,18)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneEX',1,'DayExchangeNumber',.,'',10,19)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneNM',2,'DayUnitNumber',.,'',10,20)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneXT',3,'DayExtensionNumber',.,'',10,21)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
          <TD unselectable="on">City:</TD>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContCity',15,'AddressCity',.,'','',24)"/>

          </TD>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContState',2,'AddressState',.,'',12,25)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContZip',7,'AddressZip',.,'',10,26)"/>
						<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtPropContState', 'txtPropContCity');"></xsl:text>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

	<!-- Gets the Remarks to Claims Rep Info -->
  <xsl:template name="RemarksToClaimsRepInfo" >
      <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropRemarks',140,3,'Remarks',.,'','',29)"/>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>


</xsl:stylesheet>
