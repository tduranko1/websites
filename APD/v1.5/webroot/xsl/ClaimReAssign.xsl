<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="ClaimReAssign">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ReassignExposureCRUD" select="Reassign Exposure"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxId"/>
<xsl:param name="UserId"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="AssignmentID"/>
<xsl:param name="Entity"/>
<xsl:param name="Description"/>
<xsl:param name="CheckListID"/>
<xsl:param name="AlarmDate"/>
<xsl:param name="SysLastUpdatedDate"/>


<xsl:template match="/">

<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspOrgChartgetListXML,ClaimReAssign.xsl,'10'   -->
<HEAD>
<xsl:variable name="PageTitle">
   <xsl:choose>
    <xsl:when test="$Entity='Claim'">
      Transfer Claim Ownership
    </xsl:when>
    <xsl:when test="$Entity='Vehicle' and $Description = 'Reassign multiple vehicles'">
      Reassign Multiple Vehicles
    </xsl:when>
    <xsl:when test="$Entity='Vehicle' and $Description != 'Reassign multiple vehicles'">
      Reassign Vehicle
    </xsl:when>
    <xsl:when test="$Entity='Task'">
      Reassign Task
    </xsl:when>
    <xsl:otherwise>
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>
<TITLE><xsl:value-of select="$PageTitle"/></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">
  A {color:black; text-decoration:none;}
</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
    document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
    };	
</script>



<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var gsLynxID = "<xsl:value-of select="$LynxId"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
  var gsAssignmentID = "<xsl:value-of select="$AssignmentID"/>";
  var gsDescription = unescape("<xsl:value-of select="$Description"/>");
  var gsEntity = "<xsl:value-of select="$Entity"/>";
  var gsCheckListID = "<xsl:value-of select="$CheckListID"/>";
  var gsAlarmDate = "<xsl:value-of select="$AlarmDate"/>";
  var gsSysLastUpdatedDate = "<xsl:value-of select="$SysLastUpdatedDate"/>";
  var gsUserSupervisor = "<xsl:value-of select="/Root/@SupervisorFlag"/>";
  var gsReassignExposureCRUD = "<xsl:value-of select="$ReassignExposureCRUD"/>";
  

<![CDATA[

//init the table selections, must be last
//function initPage()
function __pageInit()
{
  if (gsEntity == "Vehicle" && gsReassignExposureCRUD.indexOf("U") == -1){
    ClientWarning("You do not have permission to reassign exposure. Please contact your supervisor.");
    doClose();
    return;
  }
  if (gsEntity == "Vehicle" && gsDescription != 'Reassign multiple vehicles')
    divDesc.innerText = gsDescription;
  else if(gsEntity == "Vehicle" && gsDescription == 'Reassign multiple vehicles')
    divDesc.innerText = "";
  if (gsAlarmDate != "")
    gsAlarmDate = extractSQLDateTime(gsAlarmDate);

  Stat1.style.top = (document.body.offsetHeight - 75) / 2;
  Stat1.style.left = (document.body.offsetWidth - 240) / 2;
  alert(gsAssignmentID);
}

function onUserChange()
{
	RBAction.change(2,'');
}

function ReAssignClaim()
{
  var strUserID, iAssignToSupervisor;
  var oClaimAspect;
  
  iAssignToSupervisor = 0;
  if (gsEntity == "Vehicle" && selPools.selectedIndex == -1) {
    ClientWarning("Please choose a Function and try again.");
		return;
	}

  if (rgAssign.value == null || rgAssign.value == 0)
	{
    ClientWarning("Please choose an assignment option and try again.");
		return;
	}
  
  switch (rgAssign.value) {
    case 1: // Auto-assign
      strUserID = "";
      break;
    case 2: // assign to self
      strUserID = gsUserID;
      break;
    case 3: // assign to the list
      if (selUsers.selectedIndex == -1) {
        ClientWarning("Please select an user from the list and try again.");
        return;
      }
      strUserID = selUsers.value;
      break;
    case 4: //assign Task to Function
      if (selFunctionCD.selectedIndex == -1){
        ClientWarning("Please select a function from the list and try again.");
        selFunctionCD.setFocus();
        return;
      }
      oClaimAspect = xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + gsClaimAspectID + "']");
      if (oClaimAspect){
        switch (selFunctionCD.value){
          case "OWN":
            strUserID = oClaimAspect.getAttribute("OwnerUserID");
            break;
          case "ALST":
            strUserID = oClaimAspect.getAttribute("AnalystUserID");
            break;
          case "SPRT":
            strUserID = oClaimAspect.getAttribute("SupportUserID");
            break;
        }
        
        if (strUserID == ""){
          ClientError("Unable to determine the function user. Please contact IT.")
          return;
        }
        
        if (strUserID == "0"){
          ClientWarning("This entity does not have a " + selFunctionCD.text + ".");
          return;
        }
      }
      
      iAssignToSupervisor = chkAssigntoSupervisor.value;
      break;
    case 5: //assign Task to specific user
      if (selUsers.selectedIndex == -1) {
        ClientWarning("Please select an user from the list and try again.");
        return;
      }
      strUserID = selUsers.value;
      break;
    default:
      ClientError("Unable to find the assignment option selected by the user. Please contact IT.")
      return;
  }
  
  if (gsEntity == "Vehicle" && rgAssignTasks.value == null) {
    ClientWarning("Please choose a task option and try again.");
		return;
	}
  
  Stat1.Show("Please wait...");
  window.setTimeout("ReAssignClaim2('" + strUserID + "', '" + iAssignToSupervisor + "')", 150);
}

function ReAssignClaim2(strUserID, iAssignToSupervisor){
  var bAssignTasks = 0;
  var sProc, sRequest;
  var aRequests = new Array();
  var sMethod = "";
  
  if (gsEntity == "Claim" || gsEntity == "Vehicle" || gsEntity == "Property")  {
    if  (gsEntity == "Vehicle") {
      if (selPools.selectedIndex == -1){
        ClientWarning("Please choose a Function from the list and try again.");
        return;
      }
  
      if (rgAssignTasks.value == null) {
        ClientWarning("Please choose a task option and try again.");
    		return;
    	} else
        bAssignTasks = (rgAssignTasks.value == 1) ? 1 : 0;      
    }
    
    sProc = "uspWorkFlowAssignClaim";
    sMethod = "ExecuteSpNp";
    var aClaimAspects = gsClaimAspectID.split(",");
    var sAssignmentPoolID = selPools.value;
    for (var i = 0; i < aClaimAspects.length; i++){
      if (aClaimAspects[i] != "" && isNaN(aClaimAspects[i]) == false){
        sRequest = "ClaimAspectID=" + aClaimAspects[i] +
                    "&AssigningUserID=" + gsUserID +
                    "&AssignmentPoolID=" + sAssignmentPoolID +
                    "&UserID=" + strUserID +
                    "&InitialAssignment=0" + 
                    "&NotifyEvent=1" +
                    "&AssignTasks=" + bAssignTasks;

        //add the request to the array of Requests
        aRequests.push( { procName : sProc,
                  method   : sMethod,
                  data     : sRequest }
              );
      }
    }
  } else {
    sProc = "uspDiaryUpdDetail";
    sMethod = "ExecuteSpNpAsXML";
    bAssignTasks = 1;
    var sAssignmentPoolFunctionCD;
    switch(rgFunction){
      case 1:
        sAssignmentPoolFunctionCD = "OWN";
        break;
      case 2:
        sAssignmentPoolFunctionCD = "ALST";
        break;
      case 3:
        sAssignmentPoolFunctionCD = "SPRT";
        break;
    }
    
    if (gsCheckListID != "" && isNaN(gsCheckListID) == false){
    sRequest = "ChecklistID=" + gsCheckListID +
                "&AlarmDate=" + gsAlarmDate +
                "&AssignedUserID=" + strUserID +
                "&AssignToSupervisor=" + iAssignToSupervisor +
                "&UserTaskDescription=" + escape(gsDescription) +
                "&ReassignTaskFlag=" + bAssignTasks +
                "&UserID=" + gsUserID +
                "&SysLastUpdatedDate=" + gsSysLastUpdatedDate;

      //add the request to the array of Requests
      aRequests.push( { procName : sProc,
                method   : sMethod,
                data     : sRequest }
            );
    }
  }
  
  var sXMLRequest = makeXMLSaveString(aRequests);
  //alert(sXMLRequest); Stat1.Hide(); return;
  
  var objRet = XMLSave(sXMLRequest);
  
  if (sMethod == "ExecuteSpNpAsXML"){
    if (objRet && objRet.code == 0 && objRet.xml) {
      window.returnValue = gsLynxID;
      if (gsEntity == "Task")
        window.dialogArguments.refreshCheckListWindow(false)
      window.close();
    }
  } else {
    if (objRet && objRet.code == 0) {
      window.returnValue = "";
      window.close();
    }                
  }
  Stat1.Hide();
}

function enableAssignUser(){
  var oUsers = null;
  var oUserInPool = null;
  var sFunctionCD = "";
  var bDisableAssign = true;
  
  if (rgAssign.value > 0)
    rgAssign.value = 1;

  //load the users for the function
  selUsers.Clear();
  if (gsEntity == "Task"){
    switch(rgFunction.value){
      case 1:
        sFunctionCD = "OWN";
        break;
      case 2:
        sFunctionCD = "ALST";
        break;
      case 3:
        sFunctionCD = "SPRT";
        break;
    }
    rbSelfAssign.CCDisabled = rbAssignTo.CCDisabled = false;
  } else {
    var oPool = xmlAssignmentPools.selectSingleNode("/Root/AssignmentPool[@AssignmentPoolID='" + selPools.value + "']");
    if (oPool){
      sFunctionCD = oPool.getAttribute("FunctionCD");
    }
    //alert(selPools.selectedIndex);
    bDisableAssign = (selPools.selectedIndex == -1);
    rbAutoAssign.CCDisabled = bDisableAssign;
    rbSelfAssign.CCDisabled = bDisableAssign;
    rbAssignTo.CCDisabled = bDisableAssign;
    
    oUsers = xmlUsers.selectNodes("/Root/User[Pool/@AssignmentPoolID='" + selPools.value + "']");
    oUserInPool = xmlUsers.selectSingleNode("/Root/User[@UserID='" + gsUserID + "' and Pool/@AssignmentPoolID = '" + selPools.value + "']");
    
    if (oUsers && (oUsers.length > 0) && (selPools.selectedIndex != -1)){
      rbAssignTo.CCDisabled = false;
      for (var i = 0; i < oUsers.length; i++){
        selUsers.AddItem(oUsers[i].getAttribute("UserID"), oUsers[i].getAttribute("NameLast") + ", " + oUsers[i].getAttribute("NameFirst"));
      }  
      selUsers.selectedIndex = -1;
    } else {
      rbAssignTo.CCDisabled = true;
    }
    
    if (oUserInPool || gsUserSupervisor == "1"){
      rbSelfAssign.CCDisabled = false;
    } else {
      rbSelfAssign.CCDisabled = true;
    }
  }
}

function enableUsers(){
  try {
  if(rgAssign.value == 3){
    selUsers.CCDisabled = false;
  } else if(rgAssign.value == 4) {
    selFunctionCD.CCDisabled = false;
    selUsers.CCDisabled = true;
  } else if (rgAssign.value == 5) {
    selUsers.CCDisabled = false;
    chkAssigntoSupervisor.CCDisabled = false;
    chkAssigntoSupervisor.value = 0;
    chkAssigntoSupervisor.CCDisabled = true;
    selFunctionCD.CCDisabled = true;
  } else {
    selUsers.CCDisabled = true;
    if (document.getElementById("selFunctionCD")) {
      selFunctionCD.CCDisabled = true;
      chkAssigntoSupervisor.CCDisabled = false;
      chkAssigntoSupervisor.value = 0;
      chkAssigntoSupervisor.CCDisabled = true;
    }
  }
  } catch (e) {alert(e.description)}
  if (gsEntity != "Task") {
    rbAssignTasks.CCDisabled = rbNoAssignTasks.CCDisabled = false;
  }
}

function setSupervisor(){
  var bHasSupervisor = false;
  var oClaimAspect = xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + gsClaimAspectID + "']");
  if (oClaimAspect){
    switch(selFunctionCD.value){
      case "OWN":
        bHasSupervisor = (oClaimAspect.getAttribute("FOhasSupervisor") == "1")
        break;
      case "ALST":
        bHasSupervisor = (oClaimAspect.getAttribute("FAhasSupervisor") == "1")
        break;
      case "SPRT":
        bHasSupervisor = (oClaimAspect.getAttribute("FShasSupervisor") == "1")
        break;
    }
  }
  
  if (bHasSupervisor == false){
    chkAssigntoSupervisor.CCDisabled = false;
    chkAssigntoSupervisor.value = 0;
    chkAssigntoSupervisor.CCDisabled = true;
    chkAssigntoSupervisor.title = selFunctionCD.text + " does not have a supervisor.";
  } else {
    chkAssigntoSupervisor.CCDisabled = false;
    chkAssigntoSupervisor.title = "";
  }
}

function doClose(){
  window.returnValue = "-1";
  window.close();
}

]]>
</SCRIPT>
</HEAD>

<BODY class1="bodyAPDSub" unselectable="on" style="background:transparent;border:0px;margin:0px;overflow:hidden" margin="0" topmargin="0" leftmargin="0">
<IE:APDStatus id="Stat1" name="Stat1" width="240" height="75"/>

<IE:APDRadioGroup id="rgFunction" name="rgFunction" required="false" canDirty="false" CCDisabled="false" onafterchange="enableAssignUser()">
</IE:APDRadioGroup>
<IE:APDRadioGroup id="rgAssign" name="rgAssign" required="false" canDirty="false" CCDisabled="false" onafterchange="enableUsers()">
</IE:APDRadioGroup>
<IE:APDRadioGroup id="rgAssignTasks" name="rgAssignTasks" required="false" canDirty="false" CCDisabled="false">
</IE:APDRadioGroup>

<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;width:100%;">
  <colgroup>
    <col width="75px"/>
    <col width="*"/>
  </colgroup>
  <tr>
    <td colspan="2" style="font-weight:bold;color:#0000FF;text-align:center;font-size:12px;background-color:#C0C0C0;Border:1px outset;padding:5px;">
   <xsl:choose>
    <xsl:when test="$Entity='Claim'">
      Transfer Claim Ownership
    </xsl:when>
    <xsl:when test="$Entity='Vehicle' and $Description != 'Reassign multiple vehicles'">
      Reassign Vehicle
    </xsl:when>
    <xsl:when test="$Entity='Vehicle' and $Description = 'Reassign multiple vehicles'">
      Reassign Multiple Vehicles
    </xsl:when>
    <xsl:otherwise>
    </xsl:otherwise>
  </xsl:choose>
    </td>
  </tr>
  <tr>
    <td colspan="2"  style="font-weight:bold;color:#0000FF;text-align:center;padding-top:3px;"><div id="divDesc" name="divDesc">LYNX ID: <xsl:value-of select="$LynxId"/></div></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;width:100%;margin:5px;">
  <colgroup>
    <col width="100px"/>
    <col width="*"/>
  </colgroup>  
  <xsl:if test="$Entity != 'Task'">
  <tr style="height:26px">
    <td colspan="2"  style="font-weight:bold;color:#0000FF;padding-top:3px;">Assignment Pool</td>
  </tr>
    <tr style="height:18px">
      <td colspan="2" style="padding-left:15px">
          <IE:APDCustomSelect id="selPools" name="selPools" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="false" width="250" onChange="enableAssignUser()">
            <xsl:for-each select="/Root/AssignmentPool">
  	        <IE:dropDownItem style="display:none">
              <xsl:attribute name="value"><xsl:value-of select="@AssignmentPoolID"/></xsl:attribute>
              <xsl:value-of select="@Name"/>
            </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
      </td>
    </tr>
    <tr style="height:18px">
      <td colspan="2"  style="font-weight:bold;color:#0000FF;padding-top:3px;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
  </xsl:if>
  <tr style="height:18px">
    <td colspan="2" style="padding-left:5px">
      <xsl:choose>
        <xsl:when test="$Entity = 'Task'">
          <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
              <td>
                <IE:APDRadio id="rbSelfAssign" name="rbSelfAssign" caption="Assign To Self" groupName="rgAssign" checkedValue="2" uncheckedValue="0" CCTabIndex="2" CCDisabled="false"/>
              </td>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>          
            </tr>
          </table>
        </xsl:when>
        <xsl:otherwise>
          <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
              <td>
                <IE:APDRadio id="rbAutoAssign" name="rbAutoAssign" caption="Auto Assign" groupName="rgAssign" checkedValue="1" uncheckedValue="0" CCTabIndex="1" CCDisabled="true"/>
              </td>
              <td>
                <IE:APDRadio id="rbSelfAssign" name="rbSelfAssign" caption="Assign to Self" groupName="rgAssign" checkedValue="2" uncheckedValue="0" CCTabIndex="2" CCDisabled="true"/>
              </td>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>          
            </tr>
          </table>
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </tr>
  <!--<tr style="height:30px">
    <td colspan="2"  style="font-weight:bold;color:#0000FF;text-align:center;padding-top:3px;"></td>
  </tr>
  <tr style="height:18px">
    <td style="padding-left:15px">
        <IE:APDRadio id="rbAutoAssign" name="rbAutoAssign" caption="Auto Assign" groupName="rgAssign" checkedValue="1" uncheckedValue="0" CCTabIndex="4" CCDisabled="true"/>
    </td>
  </tr>
  <tr style="height:18px">
    <td colspan="2" style="padding-left:15px">
        <IE:APDRadio id="rbSelfAssign" name="rbSelfAssign" caption="Assign to Self" groupName="rgAssign" checkedValue="2" uncheckedValue="0" CCTabIndex="5" CCDisabled="true"/>
    </td>
  </tr> -->
  
  <xsl:choose>
    <xsl:when test="$Entity='Task'">
      <tr style="height:23px">
        <td style="padding-left:5px">
            <IE:APDRadio id="rbAssignTo" name="rbAssignTo" caption="Assign To" groupName="rgAssign" checkedValue="4" uncheckedValue="0" CCTabIndex="6" CCDisabled="false"/>
        </td>
        <td>
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <IE:APDCustomSelect id="selFunctionCD" name="selFunctionCD" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="7" required="false" width="115" onChange="setSupervisor()">
                    <IE:dropDownItem value="OWN" style="display:none">File Owner</IE:dropDownItem>
                    <IE:dropDownItem value="ALST" style="display:none">File Analyst</IE:dropDownItem>
                    <IE:dropDownItem value="SPRT" style="display:none">File Support</IE:dropDownItem>
                  </IE:APDCustomSelect>
                  
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </td>
                <td>
                  <IE:APDCheckBox id="chkAssigntoSupervisor" name="chkAssigntoSupervisor" caption="Supervisor" value="0" required="false" CCTabIndex="5" CCDisabled="true" canDirty="false"/>
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <tr style="height:23px">
        <td style="padding-left:5px">
            <IE:APDRadio id="rbAssignToUser" name="rbAssignToUser" caption="Assign To User" groupName="rgAssign" checkedValue="5" uncheckedValue="0" CCTabIndex="6" >
              <xsl:if test="/Root/@SupervisorFlag != '1'">
                <xsl:attribute name="CCDisabled">true</xsl:attribute>
              </xsl:if>
            </IE:APDRadio>
        </td>
        <td>
            <IE:APDCustomSelect id="selUsers" name="selUsers" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="7" required="false" width="175" dropdownWidth1="150" onChange="">
              <xsl:for-each select="/Root/User">
              <xsl:sort select="@NameLast"/>
    	        <IE:dropDownItem style="display:none">
                <xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>
                <xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/>
              </IE:dropDownItem>
              </xsl:for-each>
            </IE:APDCustomSelect>
        </td>
      </tr>
    </xsl:when>
    <xsl:otherwise>
      <tr style="height:18px">
        <td colspan="2" style="padding-left:5px">
            <IE:APDRadio id="rbAssignTo" name="rbAssignTo" caption="Assign To" groupName="rgAssign" checkedValue="3" uncheckedValue="0" CCTabIndex="6" CCDisabled="true"/>
        </td>
      </tr>
      
      <tr style="height:18px">
        <td colspan="2" style="padding-left:25px">
            <IE:APDCustomSelect id="selUsers" name="selUsers" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="7" required="false" width="250" onChange="">
            </IE:APDCustomSelect>
        </td>
      </tr>  
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:if test="$Entity='Vehicle'">
    <tr style="height:26px">
      <td colspan="2"  style="font-weight:bold;color:#0000FF;center;padding-top:3px;">Tasks</td>
    </tr>
    
    <tr style="height:18px">
      <td colspan="2" style="padding-left:5px">
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
          <tr>
            <td>
              <IE:APDRadio id="rbAssignTasks" name="rbAssignTasks" caption="Reassign Tasks" groupName="rgAssignTasks" checkedValue="1" uncheckedValue="0" CCTabIndex="8" CCDisabled="true"/>
            </td>
            <td>
              <IE:APDRadio id="rbNoAssignTasks" name="rbNoAssignTasks" caption="Do Not Reassign Tasks" groupName="rgAssignTasks" checkedValue="2" uncheckedValue="0" CCTabIndex="8" CCDisabled="true"/>
            </td>
          </tr>
        </table>        
      </td>
    </tr>
  </xsl:if>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:250px">
  <tr>
    <td style="padding-left:50px">
      <IE:APDButton id="btnAssign" name="btnAssign" value="Assign" CCTabIndex="9" CCDisabled="false" onButtonClick="ReAssignClaim()"/>
    </td>
    <td>
      <IE:APDButton id="btnClose" name="btnClose" value="Close" CCTabIndex="10" onButtonClick="doClose()"/>
    </td>
  </tr>
</table>        

<xml id="xmlUsers" name="xmlUsers">
  <Root>
    <xsl:copy-of select="/Root/User"/>
  </Root>
</xml>

<xml id="xmlAssignmentPools" name="xmlAssignmentPools">
  <Root>
    <xsl:copy-of select="/Root/AssignmentPool"/>
  </Root>
</xml>


<xml id="xmlClaimAspect" name="xmlClaimAspect">
  <Root>
    <xsl:copy-of select="/Root/ClaimAspect"/>
  </Root>
</xml>
</BODY>
</HTML>

	</xsl:template>


</xsl:stylesheet>

