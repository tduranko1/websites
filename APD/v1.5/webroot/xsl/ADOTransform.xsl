<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
   xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
   xmlns:rs='urn:schemas-microsoft-com:rowset'
   xmlns:z='#RowsetSchema'
   version="1.0">

  <!--<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>-->
  <xsl:output />

  <xsl:template match="rs:data">
      <xsl:element name="data">
         <xsl:apply-templates select="node()"/>
      </xsl:element>
  </xsl:template>

  <xsl:template match="z:row">
    <xsl:element name="row">
      <xsl:copy-of select="@*"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="@*">
      <xsl:attribute name="name"><xsl:value-of select="name(.)"/></xsl:attribute>
      <xsl:value-of select="."/>
  </xsl:template>
  
</xsl:stylesheet>