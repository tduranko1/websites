<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
<div align="left" style="width:725px;margin-bottom:4px;">
<input type="button" class="formbutton" onclick="printPage()" value="Print" id="btnPrint"/>
</div>
<div id="rptText" style="width:725px;height:300px;background-color:#FFFFFF;border:1px solid #c0c0c0;border-bottom:3px solid #000000;border-right:3px solid #000000;padding:5px;overflow:auto;">
<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
    <tr>
        <td><div style="font:bolder 12pt Tahoma;">Program Shop Report</div></td>
        <td><div style="font:bolder 12pt Tahoma;" align="right">LYNX Services</div></td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <colgroup>
        <col width="65px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="55px"/>
        <col width="100px"/>
        <col/>
    </colgroup>
    <tr>
        <td><strong>From Date:</strong></td>
        <td><xsl:value-of select="@FromDate"/></td>
        <td> </td>
        <td><strong>To Date:</strong></td>
        <td><xsl:value-of select="@ToDate"/></td>
        <td> </td>
        <td align="right"><strong>Report Date:  </strong><xsl:value-of select="@ReportDate"/></td>
    </tr>
</table>
<br/>
<xsl:for-each select="Report">
<table cellspacing="0" cellpadding="3" border="1" width="100%" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px;">
    <colgroup>
        <col width="100px"/>
        <col width="150px"/>
        <col width="*"/>
    </colgroup>
    <tr style="background-color:silver">
        <td><strong>LYNX ID:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="concat(@LynxID, '-', @VehicleNumber)"/></td>
        <td><strong>Claim Date:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@ClaimDate"/></td>
        <td><strong>Claim Rep:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@ClaimRep"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="margin-left:15px">
                <table cellspacing="0" cellpadding="2" border="0" width="100%">
                    <colgroup>
                        <col width="110px"/>
                        <col width="200px"/>
                        <col width="15px"/>
                        <col width="100px"/>
                        <col width="*"/>
                    </colgroup>
                    <tr>
                        <td><strong>Claim Number:</strong></td>
                        <td><xsl:value-of select="@ClaimNumber"/></td>
                        <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                        <td><strong>Vehicle Owner:</strong></td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="string-length(@BusinessName) &gt; 0">
                                    <xsl:value-of select="@BusinessName"/>
                                </xsl:when>
                                <xsl:when test="string-length(@VehicleOwnerLastName) &gt; 0 and string-length(@VehicleOwnerFirstName) &gt; 0">
                                    <xsl:value-of select="concat(@VehicleOwnerLastName, ', ', @VehicleOwnerFirstName)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@VehicleOwnerLastName"/>
                                    <xsl:value-of select="@VehicleOwnerFirstName"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><strong>Year, Make, Model:</strong></td>
                        <td colspan="4"><xsl:value-of select="@VehicleInfo"/></td>
                    </tr>
                    <tr>
                        <td><strong>Shop Name:</strong></td>
                        <td><xsl:value-of select="@ShopName"/></td>
                        <td></td>
                        <td><strong>Shop City, State:</strong></td>
                        <td><xsl:value-of select="@ShopCityState"/></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
</xsl:for-each>
<!-- <table cellspacing="0" cellpadding="3" border="1" style="border:1px solid #000000;border-collapse:collapse;">
    <colgroup>
        <col width="75px"/>
        <col width="75px"/>
        <col width="100px"/>
        <col width="200px"/>
        <col width="200px"/>
        <col width="200px"/>
    </colgroup>
    <tr align="center" style="border:1px solid #000000">
        <td><strong>Date</strong></td>
        <td><strong>Lynx ID</strong></td>
        <td><strong>Claim Number</strong></td>
        <td><strong>Insured/Claimant</strong></td>
        <td><strong>FNOL Rep.</strong></td>
        <td><strong>Claim Rep.</strong></td>
    </tr>
      <xsl:for-each select="Claim" >
        <tr valign="top">
            <td><xsl:value-of select="@Date"/></td>
            <td><xsl:value-of select="@LynxID"/></td>
            <td><xsl:value-of select="@ClaimNumber"/></td>
            <td><xsl:value-of select="@InsuredName"/></td>
            <td><xsl:value-of select="@UserName"/></td>
            <td><xsl:value-of select="@ClaimRep"/></td>
        </tr>
      </xsl:for-each>

</table> -->
</div>
</xsl:template>
</xsl:stylesheet>