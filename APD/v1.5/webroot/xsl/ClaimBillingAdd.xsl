<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimBilling">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Client Billing"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="LynxId"/>
<xsl:param name="Entity"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>

<xsl:choose>
  <xsl:when test="contains($InfoCRUD, 'C')">
    <xsl:call-template name="mainPage">
      <xsl:with-param name="LynxId" select="$LynxId"/>
      <xsl:with-param name="UserId" select="$UserId"/>
      <xsl:with-param name="ClaimStatus" select="$ClaimStatus"/>
      <xsl:with-param name="Entity" select="$Entity"/>
      <xsl:with-param name="InfoCRUD" select="$InfoCRUD"/>
    </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
        <html>
        <head>
          <title>Add Billing</title>
            <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
            <STYLE type="text/css">
                IE\:APDButton {behavior:url(/behaviors/APDButton.htc)}
            </STYLE>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
          <tr>
            <td align="center">
              <font color="#ff0000"><strong>You do not have sufficient permission to add billing information.
              <br/>Please contact administrator for permissions.</strong></font>
            </td>
          </tr>
          <tr style="height:50px">
            <td>
              <span style="position:absolute;left:490px;">
                <IE:APDButton id="btnCancel" name="btnCancel" value="Close" onButtonClick="window.returnValue='CANCEL';window.close()"/>
              </span>
            </td>
          </tr>
        </table>
        </body>
        </html>
  </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="LynxId"/>
<xsl:param name="UserId"/>
<xsl:param name="ClaimStatus"/>
<xsl:param name="Entity"/>
<xsl:param name="InfoCRUD"/>
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimBillingGetDetailXML,ClaimBilling.xsl, 6790   -->
<HEAD>

<TITLE>Add Billing</TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,36);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var gsLynxID = "<xsl:value-of select="$LynxId"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var gsBillingModel = "<xsl:value-of select="@BillingModelCD"/>";
  var gsEntity = "<xsl:value-of select="$Entity"/>";
  var gsEntityName = "<xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@Name"/>";
  var gsEntityServiceChannelCD = "<xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@ServiceChannelCD"/>";
  var gsPartyCD = "<xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@ExposureCD"/>";
  var sChannelItems = "";
  var sDispositionItems = "";
  var iOldFeeIDSelected = null;
  var aFeeServices = new Array();
  var sBillingClaimAspectID = "<xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@ClaimAspectID"/>";
  var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
  
  <![CDATA[
    function __pageInit(){
      try {
        billingStat.style.top = (document.body.offsetHeight - 75) / 2;
        billingStat.style.left = (document.body.offsetWidth - 240) / 2;
      } catch (e) {
        ClientError("An error occured while executing client side function pageInit(): \nError description:" + e.description);
      }
      if (xmlBilling.parseError.errorCode != 0){
         ClientWarning("This screen has an malformed data. Please contact IT with the following message.<br/><br/>xmlBilling is malformed. Reason: " + xmlBilling.parseError.reason + " at Line " + xmlBilling.parseError.line + ", position: " + xmlBilling.parseError.linepos);
         window.close;
         return;
      }
    }

    function xmlInit(){
      xmlBilling.setProperty('SelectionLanguage', 'XPath');
      var oChannels = xmlBilling.selectNodes("/Root/Reference[@List='ServiceChannelCD']");
      if (oChannels){
        for (var i = 0; i < oChannels.length; i++)
          if (oChannels[i].getAttribute("ReferenceID") != "?" && oChannels[i].getAttribute("ReferenceID") != "1P" && oChannels[i].getAttribute("ReferenceID") != "3P" && (xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + sBillingClaimAspectID + "']/ClaimAspectServiceChannel[@ServiceChannelCD='" + oChannels[i].getAttribute("ReferenceID") +"']") != null))
            sChannelItems += '<IE:dropDownItem value="' + oChannels[i].getAttribute("ReferenceID") + '" >' + oChannels[i].getAttribute("Name") + '</IE:dropDownItem>\n';
      }
      
      var oDispositions = xmlBilling.selectNodes("/Root/Reference[@List='DispositionTypeCD']");
      if (oDispositions){
        for (var i = 0; i < oDispositions.length; i++)
          if (oDispositions[i].getAttribute("ReferenceID") != "?")
            sDispositionItems += '<IE:dropDownItem value="' + oDispositions[i].getAttribute("ReferenceID") + '" >' + oDispositions[i].getAttribute("Name") + '</IE:dropDownItem>\n';
      }
    }

    function loadServices(){
      if (csFee.value == iOldFeeIDSelected) return;
      billingStat.Show("Loading Services Data...");
      window.setTimeout("loadServices2()", 150);
    }

    function loadServices2(){
      try {
        var bContinue = false;
        var bAddDisabled = true;


        
        
        if (xmlBilling) {
          var oRows = tblServices.rows;
          var iRows = oRows.length;
          //delete existing service rows
          for (var i = iRows - 1; i >= 0; i--)
            tblServices.deleteRow(i);
          //tblServices.removeAll();
          aFeeServices = new Array();
          //load instructions for the currently selected fee
          txtFeeComments.value = "";
          var oClientFee = xmlBilling.selectSingleNode("/Root/Reference[@List='ClientFee' and @ReferenceID='" + csFee.value + "']");
          if (oClientFee) {
            txtFeeComments.CCDisabled = false;
            txtFeeComments.value = oClientFee.getAttribute("FeeInstructions");
            txtFeeComments.CCDisabled = true;
          }
          btnAdd.CCDisabled = true;



          // load the services for the currently selected fee.
          //var oServices = xmlBilling.selectNodes("/Root/Reference[List='ClientFeeDefinition' and ReferenceID='" + csFee.value + "']");
          var oServices = xmlBilling.selectNodes("/Root/Reference[@List='ClientFeeDefinition' and @ReferenceID='" + csFee.value + "']");

          if (oServices) {
            for (var i = 0; i < oServices.length; i++){
              iServiceID = oServices[i].getAttribute("ServiceID");
              var iRequired = oServices[i].getAttribute("RequiredFlag");
              var bDisableService = false;
                // Commented the following to allow addition of duplicate Additional fees and not handling fees.
                /*var oServiceAlreadyAdded = xmlBilling.selectSingleNode("/Root/Billing[@ClaimAspectID=" + sBillingClaimAspectID + "]/BillingService[@ServiceID=" + iServiceID + "]");

                if (oServiceAlreadyAdded) {
                  if (!bContinue) {
                    if (iRequired == 0) {
                      var sRet = YesNoMessage("Confirmation", "You have selected a fee with a service that was already performed. If you choose Yes, the service that was already performed will be unavailable. \n\nDo you want to continue?");
                      bDisableService = true;
                    } else {
                      if (gsBillingModel != "C") {
                        ClientWarning("Cannot add this fee. One of its services is marked as required and this service was already performed.")
                        sRet = "No";
                      } else
                        sRet = "Yes";
                    }
                    if (sRet != "Yes") {
                      var oRows = tblServices.rows;
                      var iRows = oRows.length;
                      for (var i = iRows - 1; i >= 0; i--)
                        tblServices.deleteRow(i);
                      csFee.selectedIndex = -1;
                      btnAdd.CCDisabled = true;
                      return;
                    }
                    bContinue = true;
                  }
                }*/

              if (!bDisableService) bAddDisabled = false;
              var oService = xmlBilling.selectSingleNode("/Root/Reference[@List='Service' and @ReferenceID='" + iServiceID + "']");
              if (oService) {
                var sPartyCD = oService.getAttribute("PartyCD");

                // if the Service party CD does not match up with the Entities party CD then skip it.
                if (gsBillingModel == "E" && gsPartyCD != sPartyCD && sPartyCD != "") continue;

                var oNewRow = tblServices.insertRow();
                //oNewRow.style.height = "21px";
                oNewRow.insertCell();
                oNewRow.insertCell();
                oNewRow.insertCell();
                oNewRow.insertCell();
                oNewRow.insertCell();

                var sExposureItems = "";
                var oExposures = null;

                if (sPartyCD != "")
                  oExposures = xmlBilling.selectNodes("/Root/ClaimAspect[@ExposureCD = '" + sPartyCD + "']");
                else
                  oExposures = xmlBilling.selectNodes("/Root/ClaimAspect[@EntityCode='clm' or (@ExposureCD = '1' or @ExposureCD = '3' or @ExposureCD = '')]");

                if (oExposures){
                  for (var j = 0; j < oExposures.length; j++)
                    sExposureItems += '	<IE:dropDownItem value="' + oExposures[j].getAttribute("ClaimAspectID") + '" >' + oExposures[j].getAttribute("Name") + '</IE:dropDownItem>';
                }

                var iExposureRequiredFlag = oService.getAttribute("ExposureRequiredFlag");

                oNewRow.cells[0].innerHTML = '<IE:APDRadioGroup id="rbGrp' + iServiceID + '" name="rbGrp' + iServiceID + '" class="APDRadioGroup" canDirty="false" CCDisabled="' + ((bDisableService || iRequired == '1') ? 'true' : 'false') + '" disabledService="' + bDisableService + '">' +
                                                '<IE:APDRadio id="rbYes' + iServiceID + '" name="rbYes' + iServiceID + '" class="APDRadio" caption="Yes" groupName="rbGrp' + iServiceID + '" checkedValue="1" uncheckedValue="0" CCDisabled="' + ((bDisableService || iRequired == '1') ? 'true' : 'false') + '" value="' + (iRequired == '1' ? '1' : '0') + '" />&nbsp;&nbsp;&nbsp;&nbsp;' +
                                                '<IE:APDRadio id="rbNo' + iServiceID + '" name="rbNo' + iServiceID + '" class="APDRadio" caption="No" groupName="rbGrp' + iServiceID + '" checkedValue="2" uncheckedValue="0" CCDisabled="' + ((bDisableService || iRequired == '1') ? 'true' : 'false') + '" />' +
                                              '</IE:APDRadioGroup>';
                oNewRow.cells[1].innerText = oService.getAttribute("Name");
                aFeeServices.push(iServiceID + "||" + oService.getAttribute("Name"));
                if (gsBillingModel == "E"){
                  var sExposureHTML = gsEntityName;
                } else {
                  var sExposureHTML = '<IE:APDCustomSelect id="csExposure' + iServiceID + '" name="csExposure' + iServiceID + '" displayCount="5" blankFirst="false" canDirty="false" width="100" ExpRequired="' + (iExposureRequiredFlag == '1' ? 'true' : 'false') + '" CCDisabled="' + (bDisableService ? 'true' : 'false') + '" onChange="checkChannel(csExposure' + iServiceID + ')">' +
                                        sExposureItems +
                                      '</IE:APDCustomSelect>';
                }
                oNewRow.cells[2].innerHTML = sExposureHTML;
                var sServiceChannelCD = oService.getAttribute("ServiceChannelCD");
                var sDispositionTypeCD = oService.getAttribute("DispositionTypeCD");
                if (gsEntity.indexOf("veh") != -1 || gsEntity.indexOf("prp") != -1) {
                  //if (gsEntityServiceChannelCD != "" && sServiceChannelCD != "" && sServiceChannelCD != gsEntityServiceChannelCD && sServiceChannelCD != "1P" && sServiceChannelCD != "3P")
                  //  ClientWarning("Service channel for "  + gsEntityName + " and service: " + oService.getAttribute("Name") + "\" does not match. Service channel for " + gsEntityName + " will be updated to match the Service channel of the Service when you add this fee information.");
                  if (sServiceChannelCD != "" && sServiceChannelCD != "?" && sServiceChannelCD != "1P" && sServiceChannelCD != "3P" && xmlClaimAspect.selectSingleNode("/Root/ClaimAspect/ClaimAspectServiceChannel[@ServiceChannelCD='" + sServiceChannelCD +"']") == null){
                     ClientWarning("Cannot add this fee. The service channel code [" + sServiceChannelCD + "] for " + oService.getAttribute("Name") + " is not part of the vehicle.");
                     csFee.selectedIndex = -1;
                     return;
                  }
                }

                var sXPath;
                
                if (sServiceChannelCD == "?")
                  oNewRow.cells[3].innerHTML = '<IE:APDCustomSelect id="csChannel' + iServiceID + '" name="csChannel' + iServiceID + '" displayCount="5" blankFirst="false" canDirty="false" width="100" CCDisabled="' + (bDisableService ? 'true' : 'false') + '" onChange="checkExposure(csChannel' + iServiceID + ')" value="' + sServiceChannelCD + '">' +
                                              sChannelItems +
                                             '</IE:APDCustomSelect>';
                else if (sServiceChannelCD == ""){
                  oNewRow.cells[4].innerHTML = "";
                  oNewRow.cells[4].setAttribute("ServiceChannelCD", "null");
                }
                else{
                  sXPath = "/Root/Reference[@List='ServiceChannelCD' and @ReferenceID='" + sServiceChannelCD + "']";
                  var oServiceChannelName = xmlBilling.selectSingleNode(sXPath);
                  oNewRow.cells[3].innerHTML = (oServiceChannelName) ? oServiceChannelName.getAttribute("Name") : "";
                  oNewRow.cells[3].setAttribute("ServiceChannelCD", sServiceChannelCD);
                }
                
                
                if (sDispositionTypeCD == "?")                           
                  oNewRow.cells[4].innerHTML = '<IE:APDCustomSelect id="csDisposition' + iServiceID + '" name="csDisposition' + iServiceID + '" displayCount="5" blankFirst="false" canDirty="false" width="100" CCDisabled="' + (bDisableService ? 'true' : 'false') + '" value="' + sDispositionTypeCD + '">' +
                                              sDispositionItems +
                                             '</IE:APDCustomSelect>';
                else if (sDispositionTypeCD == ""){
                  oNewRow.cells[4].innerHTML = "";
                  oNewRow.cells[4].setAttribute("DispositionTypeCD", "null");
                }
                else{
                  sXPath = "/Root/Reference[@List='DispositionTypeCD' and @ReferenceID='" + sDispositionTypeCD + "']"
                  var oDispositionTypeName = xmlBilling.selectSingleNode(sXPath);
                  oNewRow.cells[4].innerHTML = (oDispositionTypeName) ? oDispositionTypeName.getAttribute("Name") : "";
                  oNewRow.cells[4].setAttribute("DispositionTypeCD", sDispositionTypeCD);
                }
              }
              if (!bAddDisabled) btnAdd.CCDisabled = false;
            }
            iOldFeeIDSelected = csFee.value;
          }
        }
      } catch (e) {
        ClientError("An error occured while executing client side function loadServices2(): \nError description:" + e.description);
      } finally {
        billingStat.Hide();
      }
    }

    function checkExposure(objCSChannel){
      var iServiceID = objCSChannel.id.substr(9);
      var sChannelCD = objCSChannel.value;
      if (sChannelCD == "1P" || sChannelCD == "3P") {
        var csExposure = document.all["csExposure" + iServiceID];
        if (csExposure) {
          var sClaimAspectID = csExposure.value;
          if (xmlBilling) {
            var oClaimAspect = xmlBilling.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID=" + sClaimAspectID + "]");
            if (oClaimAspect) {
              if (oClaimAspect.getAttribute("ExposureCD") + "P" != sChannelCD) {
                ClientWarning("Service channel for " + csExposure.text + " cannot be " + objCSChannel.text);
                objCSChannel.onchange = "";
                objCSChannel.selectedIndex = -1;
                objCSChannel.onchange = new Function("", "checkExposure(csChannel" + iServiceID + ")");
              }
            }
          }
        }
      }
    }

    function checkChannel(objCSExposure){
      var iServiceID = objCSExposure.id.substr(10);
      var csChannel = document.all["csChannel" + iServiceID];
      if (csChannel) {
        csChannel.selectedIndex = -1;
      }
    }

    function btnCancelClick(){
      window.returnValue = "CANCEL";
      window.close();
    }

    function btnAddClick() {
      billingStat.Show("Adding Billing info...");
      window.setTimeout("btnAddClick2()", 150);
    }

    function btnAddClick2() {
      try {
        if (addBillingInfo() == true) {
          window.returnValue = "OK";
          window.close();
        }
      } catch (e) {
        ClientError("An error occured while executing client side function btnAddClick2(): \nError description:" + e.description);
      } finally {
        billingStat.Hide();
      }
    }

    function addBillingInfo() {
      try {
        var bAddSuccess = false;
        var sInclServices = "";
        var aInclServices = new Array();
        if (validateBilling() == true) {
          if ((sBillingClaimAspectID != "") && (aFeeServices && aFeeServices.length > 0)){

            for (var i = 0; i < aFeeServices.length; i++) {
              var iServiceID = aFeeServices[i].split("||")[0];
              var csExposure = document.all["csExposure" + iServiceID];
              var csServiceChannel = document.all["csChannel" + iServiceID];
              var csDisposition = document.all["csDisposition" + iServiceID];
              var oRow = tblServices.rows[i];
              var rbGrp = document.all["rbGrp" + iServiceID];
              
              if (rbGrp && rbGrp.value != 1) continue; // service not performed. no need to save this info.
              //alert("iServiceID:" + iServiceID + "\ncsExposure:" + csExposure + "\ncsServiceChannel:" + csServiceChannel);
              if (iServiceID != "") {
                aInclServices.push(iServiceID);

                if (gsBillingModel == "E")
                  aInclServices.push(sBillingClaimAspectID);
                else if (csExposure){
                  aInclServices.push(csExposure.value);
                } else {
                  ClientError("Could not determine the user selected Exposure Code for service id:" + iServiceID);
                }

                if (csServiceChannel){
                  aInclServices.push(csServiceChannel.value);
                } else 
                  if (oRow){
                    aInclServices.push(oRow.cells[3].getAttribute("ServiceChannelCD"));
                  }
                  else{
                    ClientError("Could not determine the user selected service channel for service id:" + iServiceID);
                }
                
                if (csDisposition){
                  aInclServices.push(csDisposition.value);
                } else if (oRow)
                    aInclServices.push(oRow.cells[4].getAttribute("DispositionTypeCD"));
                  else{
                    ClientError("Could not determine the user disposition type channel for service id:" + iServiceID);
                }                
              }
            }

            var sRequest = "";
            var sProc = "uspInvoiceInsDetail";
            sRequest = "ClaimAspectID=" + sBillingClaimAspectID +
                       "&ClientFeeID=" + csFee.value +
                       "&IncServices=" + aInclServices.join(",") + 
                       "&ItemTypeCD=F" + 
                       "&SysLastUserID=" + gsUserID +
                       "&NotifyEvent=1";
            //alert(sRequest);
            //return;
            var aRequests = new Array();;
            aRequests.push( { procName : sProc,
                              method   : "ExecuteSpNpAsXML",
                              data     : sRequest }
                          );
            var sXMLRequest = makeXMLSaveString(aRequests);
            var objRet = XMLSave(sXMLRequest);
            if (objRet && objRet.code == 0 && objRet.xml)
              bAddSuccess = true;
          } else
            ClientError("ClaimAspectID could be determined for the entity:" + gsEntity + " [" + gsLynxID + "]" );
        }
      } catch (e) {
        ClientError("An error occured while executing client side function addBillingInfo(): \nError description:" + e.description);
      } finally {
      }
      return bAddSuccess;
    }

    function validateBilling(){
      try {
        var bValid = false;
        var iYesCount = 0;
        if (aFeeServices) {
          for (var i = 0; i < aFeeServices.length; i++) {
            var iServiceID = aFeeServices[i].split("||")[0];
            var sServiceName = aFeeServices[i].split("||")[1];
            //check if user selected yes or no for the service performed.
            var rbGrp = document.all["rbGrp" + iServiceID];
            if (rbGrp) {
              if (rbGrp.disabledService == "false" && rbGrp.value == null) {
                ClientWarning("You must indicate if a service was performed or not. \n\nPlease indicate if \"" + sServiceName + "\" was performed or not.")
                bValid = false;
                break;
              } else if (rbGrp.value == 1){
                iYesCount++;
                var csExposure = document.all["csExposure" + iServiceID];
                if (csExposure) {
                  if (csExposure.selectedIndex == -1) {
                    ClientWarning("You must select an exposure for each services performed. \n\nPlease select an exposure for the service \"" + sServiceName + "\"");
                    bValid = false;
                    break;
                  }
                  //check if this service was billed for this exposure
                  var oAlreadyBilled = xmlBilling.selectSingleNode("/Root/Billing/BillingService[@ServiceID=" + iServiceID + " and @ClaimAspectID=" + csExposure.value + "]");
                  if (oAlreadyBilled) {
                    ClientWarning(sServiceName + " has already billed for " + csExposure.text + ". Cannot bill the same service twice for the same exposure.");
                    bValid = false;
                    break;
                  }

                }

                var csServiceChannel = document.all["csChannel" + iServiceID];
                if (csServiceChannel) {
                  if (csServiceChannel.selectedIndex == -1) {
                    ClientWarning("You must select a service channel for each services performed. \n\nPlease select a service channel for the service \"" + sServiceName + "\"");
                    bValid = false;
                    break;
                  }
                  if (csServiceChannel.value == "1P" || csServiceChannel.value == "3P") {
                    try {
                      var oAspectTypeID = xmlBilling.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID=" + csExposure.value + "]");
                    } catch (e) {}
                    if (oAspectTypeID && oAspectTypeID.getAttribute("ClaimAspectTypeID") != "9"){
                      ClientWarning("Incorrect Service Channel specified for the " + csExposure.text + "[Service: " + sServiceName + "]. 1st Party and 3rd Party can be assigned to vehicle exposures only.");
                      bValid = false;
                      break;
                    }
                  }
                }
                bValid = true;
              }
            }
          }
          if (bValid && aFeeServices.length > 0 && iYesCount == 0){
            ClientWarning("You must select Yes to atleast one service.");
            bValid = false;
          }
        }
      } catch (e) {
        ClientError("An error occured while executing client side function validateBilling(): \nError description:" + e.description);
      } finally {
      }
      return bValid;
    }
  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;background-color:#FFFFFF;padding:0px;margin:5px;">
  <xsl:call-template name="AddBilling">
    <xsl:with-param name="Entity" select="$Entity"/>
  </xsl:call-template>
  <xml id="xmlBilling" name="xmlBilling" ondatasetcomplete="xmlInit()">
    <xsl:copy-of select="/"/>
  </xml>
  <xml id="xmlClaimAspect">
   <Root>
      <xsl:copy-of select="/Root/ClaimAspect"/>
   </Root>
  </xml>
  <IE:APDStatus id="billingStat" name="billingStat" width="240" height="75"/>
</BODY>
</HTML>
</xsl:template>

  <xsl:template name="AddBilling">
    <xsl:param name="Entity"/>
    <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="75px"/>
        <col width="375px"/>
      </colgroup>
      <tr>
        <td colspan="2" style="font-size:18px;font-weight:bold">
          Billing for 
          <xsl:choose>
            <xsl:when test="contains($Entity, 'clm')">
              LYNX ID <xsl:value-of select=" @LynxID"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@Name"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td>Fee:</td>
        <td>
          <xsl:variable name="ClaimAspectID">
            <xsl:value-of select="/Root/ClaimAspect[@EntityCode=$Entity]/@ClaimAspectID"/>
          </xsl:variable>
          <xsl:variable name="ClaimAspectTypeID">
            <xsl:value-of select="/Root/ClaimAspect[@EntityCode=$Entity]/@ClaimAspectTypeID"/>
          </xsl:variable>
          <xsl:variable name="countPrevHandlingFee">
            <xsl:value-of select="count(/Root/Invoice[@ItemTypeCD = 'F' and @FeeCategoryCD = 'H' and @ClaimAspectID=$ClaimAspectID])"/>
          </xsl:variable>
    		  <xsl:variable name="EntityServiceChannelCD">
    		    <xsl:value-of select="/Root/ClaimAspect[@EntityCode=$Entity]/@AssignmentTypeServiceChannelCD"/>
    		  </xsl:variable>          
          <xsl:variable name="CurrentHandlingFeeSvcChannelCD">
            <xsl:value-of select="/Root/Invoice[@ClaimAspectID=$ClaimAspectID and @ItemTypeCD='F' and @FeeCategoryCD='H']/InvoiceService[position()=1]/@ServiceChannelCD"/>
          </xsl:variable>

          <!-- 10Jun2019 TVD - Debugging  -->
          <xsl:variable name="curHandlingFeeAmount">
            <xsl:value-of select="/Root/Invoice[@ClaimAspectID=$ClaimAspectID and @ItemTypeCD='F' and @FeeCategoryCD='H']/@Amount"/>
          </xsl:variable>

          <!-- 10Jun2019 TVD - Determin if Fees are Pos or Neg  -->
			    <xsl:variable name="iFeeTotal">
            <xsl:value-of select="sum(/Root/Invoice[@ClaimAspectID=$ClaimAspectID and @FeeCategoryCD='H']/@Amount)"/>
          </xsl:variable>

          <!-- 12Jun2019 TVD - Count of Fees  -->
			    <!-- xsl:variable name="iCntOfFees" select="0"/ -->

          <!-- xsl:variable name="aFeeNames" select="concat(@Description, ' CANCEL')"/-->
          <!-- 10Jun2019 TVD - Create and array of Current Fees -->
          <xsl:variable name="sLastFeeName">
            <xsl:choose>
              <xsl:when test="1=1">
                <xsl:for-each select="/Root/Invoice[@FeeCategoryCD='H']" > 
                  <xsl:if test="position() = last()">
                    <!-- xsl:value-of select="concat(@Description, ' CANCEL')"/ -->
                    <xsl:value-of select="@Description"/>
                  </xsl:if>
                </xsl:for-each>
              </xsl:when> 
            </xsl:choose>
          </xsl:variable>
          
          <!-- xsl:value-of select="$sLastFeeName"/ -->
          <!-- script>alert('<xsl:value-of select="@CategoryCD"/>')</script -->
          
          <IE:APDCustomSelect id="csFee" name="csFee" displayCount="10" width="265" blankFirst="false" canDirty="false" CCTabIndex="1" onChange="loadServices()">
            <!-- list all client fee in the reference pertaining to the claimAspectTypeID for which we are adding the billing -->
                       
            <!-- xsl:for-each select="Reference[@List='ClientFee' and @ClaimAspectTypeID = $ClaimAspectTypeID and not(contains(@Name, 'CANCEL'))]" -->
            <xsl:for-each select="Reference[@List='ClientFee' and @ClaimAspectTypeID = $ClaimAspectTypeID]">

              <xsl:sort select="@DisplayOrder" data-type="number"/>
              <xsl:variable name="FeeName"><xsl:value-of select="@Name"/></xsl:variable>
              <xsl:variable name="FeeID" select="@ReferenceID"/>
              
              <!-- Get the current fee service id -->
              <xsl:variable name="curFeeServiceID">
                 <xsl:value-of select="/Root/Reference[@List='ClientFeeDefinition' and @ReferenceID=$FeeID]/@ServiceID"/>
              </xsl:variable>
              
              <!-- Using the current fee id get the service channel code -->
              <xsl:variable name="curFeeServiceChannelCD">
                 <xsl:value-of select="/Root/Reference[@List='Service' and @ReferenceID=$curFeeServiceID]/@ServiceChannelCD"/>
              </xsl:variable>

              <!-- <script>alert('<xsl:value-of select="@Name"/> : <xsl:value-of select="@CategoryCD"/> : <xsl:value-of select="$countPrevHandlingFee"/>')</script>
              <script>alert('<xsl:value-of select="$CurrentHandlingFeeSvcChannelCD"/> : <xsl:value-of select="$EntityServiceChannelCD"/> : <xsl:value-of select="$countPrevHandlingFee"/>')</script> -->

              <!-- Remove Cancels that do not have a positive fee -->
              <!-- script>alert('<xsl:value-of select="@Name"/>')</script -->

              <!-- Display only those fees with its service channel matching the entity service channel or fee service channel is empty -->
              <xsl:if test="count(/Root/Reference[@List='Service' and (@ServiceChannelCD = $EntityServiceChannelCD or @ServiceChannelCD = '') and @ReferenceID = /Root/Reference[@List='ClientFeeDefinition' and @ReferenceID=$FeeID]/@ServiceID]/@ServiceChannelCD) &gt; 0">
                <!-- $DisplayFee encapsulates all business rules pertaining to whether or not fees should be displayed for selection --> 
                  <xsl:variable name="DisplayFee">
                  <xsl:choose>                

                    <!-- 06Jun2019 TVD - Adjust Fees -->
                    <!-- xsl:when test="(@CategoryCD = 'H') and ($curHandlingFeeAmount &gt; 0) "-->
                    <xsl:when test="(@CategoryCD = 'H')">
                      <xsl:choose>
                        <!-- 06Jun2019 TVD - Remove/Add Fees based on iFeeTotal being negative or positive -->
                        <xsl:when test="contains(@Name, 'CANCEL') and contains(@Name, $sLastFeeName) and ($iFeeTotal &gt; 0)">true</xsl:when>  
                        <!-- xsl:when test="contains(@Name, 'Program Shop - Cash Out CANCEL') and ($iFeeTotal &gt; 0)">true</xsl:when -->  
                        <!-- xsl:when test="contains(@Name, $sLastFeeName) and ($iFeeTotal &gt; 0)">true</xsl:when -->  
                        <xsl:when test="(not(contains(@Name, 'CANCEL')) and ($iFeeTotal &lt; 1))">true</xsl:when>  
                      </xsl:choose>
                    </xsl:when> 

                    <!-- this is an additional fee and a handling fee HAS already been billed.  -->
                    <xsl:when test="(@CategoryCD = 'A') and ($countPrevHandlingFee &gt; 0)">                      
                      <xsl:choose>
                        <!-- The current service channel does not match the service channel of the existing handling fee. -->
                        <xsl:when test="$CurrentHandlingFeeSvcChannelCD != $EntityServiceChannelCD">false</xsl:when>  
                      
                        <!-- this specific additional fee has NOT yet been billed  -->
                        <xsl:when test="0 = count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID]/InvoiceService[@ServiceID=(/Root/Reference[@List='ClientFeeDefinition' and @ReferenceID=$FeeID]/@ServiceID)])">true</xsl:when>
                        
                        <!-- this specific additional fee contains at least 1 service that CAN be billed multiple times -->
                        <xsl:when test="@ReferenceID=/Root/Reference[@List='ClientFeeDefinition' and (@ServiceID=/Root/Reference[@List='Service' and @MultipleBillingFlag=1]/@ReferenceID)]/@ReferenceID">true</xsl:when>
                        
                        <!-- do NOT display this fee -->
                        <!-- xsl:otherwise>false</xsl:otherwise -->
                      </xsl:choose>
                    </xsl:when>      
                  
                    <!-- do NOT display this fee -->
                    <!--xsl:otherwise>true</xsl:otherwise-->                  
                  </xsl:choose>                
                </xsl:variable>

                <xsl:if test="$DisplayFee='true'">                
                  <!-- Now everything is filtered. Add this fee item -->
                    <!-- 06Jun2019 - TVD - Show the correct Fee data in the Dropown -->
                    <!-- xsl:if test="not(contains(@Name, 'CANCEL'))" -->
                      
                	    <IE:dropDownItem style="display:none">
                          <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                          <xsl:value-of select="@Name"/>
                      </IE:dropDownItem>
                    
                    <!-- /xsl:if -->
                </xsl:if>
              </xsl:if>
            </xsl:for-each>

      </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          Fee Comments:<br/>
          <IE:APDTextArea id="txtFeeComments" name="txtFeeComments" width="675" height="60" canDirty="false" CCDisabled="true" >
            <xsl:attribute name="value"><xsl:value-of select="@FeeInstructions"/></xsl:attribute>
          </IE:APDTextArea>
        </td>
      </tr>
    </table>

    <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="100"/>
        <col width="200"/>
        <col width="125"/>
        <col width="120"/>
        <col width="120"/>
      </colgroup>
      <tr class="QueueHeader" style="height:21px;">
        <td class="TableSortHeader">Performed?</td>
        <td class="TableSortHeader">Service</td>
        <td class="TableSortHeader">Exposure</td>
        <td class="TableSortHeader">Channel</td>
        <td class="TableSortHeader">Exp. Disp.</td>
      </tr>
    </table>
    <div style="height:108px;overflow-x:hidden;overflow-y:auto;border:1px solid #C0C0C0">
      <table name="tblServices" id="tblServices" border="0" cellspacing="0" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;">
        <colgroup>
          <col width="100px"/>
          <col width="200px"/>
          <col width="125px"/>
          <col width="125px"/>
          <col width="125px"/>
        </colgroup>
        <tr style="height:21px;">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </div>
    <span style="position:absolute;left:575px;padding:0px;padding-top:5px;">
      <IE:APDButton id="btnAdd" name="btnAdd" value="Add" CCDisabled="true" onButtonClick="btnAddClick()"/>
      <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" onButtonClick="btnCancelClick()"/>
    </span>
</xsl:template>
</xsl:stylesheet>
