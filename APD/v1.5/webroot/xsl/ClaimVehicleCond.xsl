<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicle">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>
  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    function GetFileSize(strFile) {
      var fso, sImgInfo;
      sImgInfo = "";
      fso = new ActiveXObject("Scripting.FileSystemObject");
      strFile = strFile.replace(/\\\\/g, "\\");
      if (fso.FileExists(strFile)){
        var f = fso.GetFile(strFile);
        if (f){
          sImgInfo += Math.round(f.size / 1024) + " kb";
        }
      }
      else {
        sImgInfo = "Not Found";
      }
      
      return sImgInfo;
    }    

    function UTCConvertDate(vDate) {
      var vYear = vDate.substr(0,4);
      var vMonth = vDate.substr(5,2);
      var vDay = vDate.substr(8,2);
      vDate = vMonth + '/' + vDay  + '/' + vYear
      return vDate;
    }
    
    function removeDblQuotes(str){
        var strRet = "";
        if (str) {
          if (str.length > 0) {
            strRet = str;
            strRet = strRet.replace(/[\"]/g,""); // remove any double quotes
          }
        }
        return strRet;
    }
    
  ]]>
  </msxsl:script>

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="VehicleCRUD" select="Vehicle"/>
  <xsl:param name="InvolvedCRUD" select="Involved"/>
  <xsl:param name="EstimateCRUD" select="Estimate"/>
  <xsl:param name="DocumentCRUD" select="Document"/>

  <xsl:param name="ReopenExpCRUD" select="Action:Reopen Exposure"/>
  <xsl:param name="CloseExpCRUD" select="Action:Close Exposure"/>
  <xsl:param name="TransferClaimCRUD" select="Transfer Claim Exposure"/>
  <xsl:param name="ReassignExpCRUD" select="Reassign Exposure"/>
  <xsl:param name="CancelExpCRUD" select="Cancel Exposure"/>
  <xsl:param name="VoidExpCRUD" select="Void Exposure"/>
  <xsl:param name="ServiceChannelCRUD" select="Service Channel"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="ImageRootDir"/>
  <xsl:param name="ImageRootArchiveDir"/>
  <xsl:param name="IsCellEmailFlag"/>
  <xsl:param name="VehClaimAspectID"/>

  <xsl:template match="/Root">

    <xsl:variable name="VehClaimAspectID" select="session:XslUpdateSession('VehClaimAspectID', $VehClaimAspectID)"/>
    <xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>
    <xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>
    <xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
    <xsl:variable name="LynxID" select="session:XslGetSession('LynxID')"/>
    <xsl:variable name="ClaimAspectID" select="session:XslGetSession('ClaimAspectID')"/>

    <xsl:variable name="VehicleNumber" select="/Root/@VehicleNumber" />
    <xsl:variable name="PertainsTo" select="/Root/@Context" />
    <xsl:variable name="ContextSupported" select="/Root/@ContextSupportedFlag" />
    <xsl:variable name="VehicleLastUpdatedDate" select="/Root/Vehicle/@SysLastUpdatedDate"/>
    <xsl:variable name="ClaimAspectLastUpdatedDate" select="/Root/Vehicle/@ClaimAspectSysLastUpdatedDate"/>
    <!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->
    <xsl:variable name="pageReadOnly" select="string(boolean(/Root/Vehicle/@Status='Vehicle Cancelled' or /Root/Vehicle/@Status='Vehicle Voided'))"/>

    <xsl:variable name="AdjVehicleCRUD">
      <xsl:choose>
        <xsl:when test="contains($VehicleCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
        <xsl:when test="contains($VehicleCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
        <xsl:when test="contains($VehicleCRUD, 'RU') = true()">_RU_</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$VehicleCRUD"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="AdjInvolvedCRUD">
      <xsl:choose>
        <xsl:when test="contains($InvolvedCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
        <xsl:when test="contains($InvolvedCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$InvolvedCRUD"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="AdjEstimateCRUD">
      <xsl:choose>
        <xsl:when test="contains($EstimateCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
        <xsl:when test="contains($EstimateCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
        <xsl:when test="contains($VehicleCRUD, 'R') = true()">_R__</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$EstimateCRUD"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="AdjDocumentCRUD">
      <xsl:choose>
        <xsl:when test="contains($DocumentCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
        <xsl:when test="contains($DocumentCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
        <xsl:when test="contains($DocumentCRUD, 'R') = true()">_R__</xsl:when>
        <xsl:otherwise>____</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <HTML>

      <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
      <!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimVehicleGetDetailXML,ClaimVehicle.xsl,4253,145   -->

      <HEAD>
        <TITLE>Vehicle Details</TITLE>

        <LINK rel="stylesheet" href="/css/apdcontrolslabourrate.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdgrid2.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/documents2.css" type="text/css"/>

        <STYLE type="text/css">
          A {color:black; text-decoration:none;}
          .nowrap {text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:100%;cursor:default;}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/zipcodeutils.js"></SCRIPT>
        <!-- <SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT> -->
        <script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
          RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
          event.returnValue=false;
          };
        </script>

        <SCRIPT language="JavaScript">

          var gsVehicleCRUD = '<xsl:value-of select="$VehicleCRUD"/>';
          var gsInvolvedCRUD = '<xsl:value-of select="$InvolvedCRUD"/>';
          var gsEstimateCRUD = '<xsl:value-of select="$EstimateCRUD"/>';
          var gsEstimateCRUD = '<xsl:value-of select="$DocumentCRUD"/>';
          var sAssignmentCRUD = '<xsl:value-of select="$AdjVehicleCRUD"/>'

          //CRUDs
          var sReopenExpCRUD = "<xsl:value-of select="$ReopenExpCRUD"/>";
          var sCloseExpCRUD = "<xsl:value-of select="$CloseExpCRUD"/>";
          var sReassignExpCRUD = "<xsl:value-of select="$ReassignExpCRUD"/>";
          var sCancelExpCRUD = "<xsl:value-of select="$CancelExpCRUD"/>";
          var sVoidExpCRUD = "<xsl:value-of select="$VoidExpCRUD"/>";
          var sServiceChannelCRUD = "<xsl:value-of select="$ServiceChannelCRUD"/>";

          var gsLynxID = '<xsl:value-of select="$LynxID"/>';
          var gsWindowID = '<xsl:value-of select="$WindowID"/>';

          //var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
          var gsVehNum = "<xsl:value-of select="substring(/Root/@Context, 4, 5)"/>";
          var gsUserID = '<xsl:value-of select="$UserId"/>';
          var gsInsuranceCompanyID = '<xsl:value-of select="$InsuranceCompanyID"/>';
          var gsVehicleLastUpdatedDate = '<xsl:value-of select="$VehicleLastUpdatedDate"/>';
          var gsClaimAspectLastUpdatedDate = '<xsl:value-of select="$ClaimAspectLastUpdatedDate"/>';
          var gsSaftyDeviceCount = '<xsl:value-of select="count(/Root/Reference[@List='SafetyDevice'])"/>';
          var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
          var gsImageRootArchiveDir = '<xsl:value-of select="$ImageRootArchiveDir"/>';

          var IsCellEmailFlag = '<xsl:value-of select="$IsCellEmailFlag"/>';
          var gsDocPath;
          var gsClaimAspectID = '<xsl:value-of select="@ClaimAspectID"/>';
          var gsCountNotes = '<xsl:value-of select="@CountNotes"/>';
          var gsCountTasks = '<xsl:value-of select="@CountTasks"/>';
          var gsCountBilling = '<xsl:value-of select="@CountBilling"/>';
          var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
          var bPageReadOnly = <xsl:value-of select="$pageReadOnly"/>;
          <xsl:variable name="ExposureCD" select="Vehicle/@ExposureCD"/>
          var gsParty = "<xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID=$ExposureCD]/@Name"/>";
          var gsExposureCD = "<xsl:value-of select="/Root/Vehicle/@ExposureCD"/>";
          var giInvInsuredCount = "<xsl:value-of select="count(/Root/Vehicle/Involved/InvolvedType[@InvolvedTypeName = 'Insured'])"/>";
          var giInvVehicleClaimantCount = "<xsl:value-of select="count(/Root/Vehicle/Involved/InvolvedType[@InvolvedTypeName = 'Claimant'])"/>";
          var bAssignmentLoaded = false;
          var sStatus = "<xsl:value-of select="/Root/Vehicle/@Status"/>";
          var sReadOnly = "<xsl:value-of select="$pageReadOnly"/>";
          var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
          var newWin = null;
          var strCurrentClaimAspectServiceChannelID = "";
          var strCloseServiceChannelCRUD = "";
          var winException = null;
          var blnSharing = false;
          var blnWarrantyLoaded = false;

          var sArchived = "<xsl:value-of select="/Root/Vehicle[@ClaimAspectID]/Document/@Archived"/>";

          <xsl:variable name="LocationCity">
            <xsl:choose>
              <xsl:when test="/Root/Vehicle/@LocationCity != ''">
                <xsl:value-of select="/Root/Vehicle/@LocationCity"/>
              </xsl:when>
              <xsl:when test="count(/Root/Vehicle/Involved[@InvolvedID != '0']) &gt; 0">
                <xsl:value-of select="/Root/Vehicle/Involved[@InvolvedID != '0']/@AddressCity"/>
              </xsl:when>
            </xsl:choose>
          </xsl:variable>

          <!--<xsl:variable name ="DisplayAlert">-->
          <xsl:variable name="CurrentAssignmentTypeID1">
            <xsl:value-of select="/Root/Vehicle/@InitialAssignmentTypeID"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $CurrentAssignmentTypeID1 and @Name='Choice Shop Assignment']">
              ChoiceShopAlert()
            </xsl:when>
          </xsl:choose>
          <!--</xsl:variable>-->

          <xsl:variable name="LocationState">
            <xsl:choose>
              <xsl:when test="/Root/Vehicle/@LocationState != '' and /Root/Vehicle/@LocationState != '  ' ">
                <xsl:value-of select="/Root/Vehicle/@LocationState"/>
              </xsl:when>
              <xsl:when test="count(/Root/Vehicle/Involved[@InvolvedID != '0']) &gt; 0">
                <xsl:value-of select="/Root/Vehicle/Involved[@InvolvedID != '0']/@AddressState"/>
              </xsl:when>
            </xsl:choose>
          </xsl:variable>

          <xsl:variable name="LocationZip">
            <xsl:choose>
              <xsl:when test="/Root/Vehicle/@LocationZip != ''">
                <xsl:value-of select="/Root/Vehicle/@LocationZip"/>
              </xsl:when>
              <xsl:when test="count(/Root/Vehicle/Involved[@InvolvedID != '0']) &gt; 0">
                <xsl:value-of select="/Root/Vehicle/Involved[@InvolvedID != '0']/@AddressZip"/>
              </xsl:when>
            </xsl:choose>
          </xsl:variable>

          var sAC = "<xsl:value-of select="/Root/Vehicle/@LocationAreaCode"/>";
          var sEX = "<xsl:value-of select="/Root/Vehicle/@LocationExchangeNumber"/>";
          var sCTY = "<xsl:value-of select="js:encode(string($LocationCity))"/>";
          var sST = "<xsl:value-of select="js:encode(string($LocationState))"/>";
          var sZIP = "<xsl:value-of select="js:encode(string($LocationZip))"/>";
          // list of variables to refer to Vehicle Location Info
          var lCTY = "<xsl:value-of select="js:encode(string(/Root/Vehicle/@LocationCity))"/>";
          var lST = "<xsl:value-of select="js:encode(string(/Root/Vehicle/@LocationState))"/>";
          var lZIP = "<xsl:value-of select="js:encode(string(/Root/Vehicle/@LocationZip))"/>";

          var iInvolvedID = null;

          var blnPSExists = "<xsl:choose>
            <xsl:when test="/Root/Vehicle/ClaimAspectServiceChannel[@ServiceChannelCD='PS']/@ClaimAspectServiceChannelID != ''">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>";

          var blnRRPExists = "<xsl:choose>
            <xsl:when test="/Root/Vehicle/ClaimAspectServiceChannel[@ServiceChannelCD='RRP']/@ClaimAspectServiceChannelID != ''">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>";

          <![CDATA[
  function pageInit(){
    //set the current vehicles claim aspect id to the top window. Used by the 
    // Vehicle Reassign.
    top.sVehicleClaimAspectID = gsClaimAspectID;

    if (typeof(parent.setVehicleInfo) == "function")
      parent.setVehicleInfo();
    top.sVehicleNum = gsVehNum;
    parent.gsVehNum = gsVehNum;

    if (typeof(top.highLightEntityNum) == "function")
      top.highLightEntityNum(gsVehNum);

    top.VehiclesMenu(false, 1); // Complete Service Channel
    top.VehiclesMenu(false, 2); // Cancel Service Channel
    top.VehiclesMenu((sReopenExpCRUD.indexOf("U") != -1), 3); // Reactivate Service Channel
    
    top.VehiclesMenu((sReopenExpCRUD.indexOf("U") != -1 && blnPSExists == 1), 4); // Reactivate Service Channel
    
    /*if (sStatus == "Open") {

      //Add  
        top.VehiclesMenu(false, 2);
      //remove vehicle
        top.VehiclesMenu((gsCountNotes == "0" && gsCountTasks == "0" && gsCountBilling == "0" && gsVehicleCRUD.indexOf("D") != -1),3);
      
      //Reassign vehicle
        top.VehiclesMenu((sReassignExpCRUD.indexOf("U") != -1), 5);
        
      //Close vehicle
        top.VehiclesMenu((sCloseExpCRUD.indexOf("U") != -1), 7);

      //disable reopen because the vehicle is already open
      top.VehiclesMenu(false, 8);

      //Cancel vehicle
        top.VehiclesMenu((sCancelExpCRUD.indexOf("U") != -1), 10);

      //Void vehicle
        top.VehiclesMenu((sVoidExpCRUD.indexOf("U") != -1), 11);      
    } else if (sStatus.indexOf("Closed") != -1) {
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(true, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    } else if (sStatus.indexOf("Cancelled") != -1) {
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(true, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    } else if (sStatus.indexOf("Voided") != -1) {
      top.VehiclesMenu(false, 5); // Reassign
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(false, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    }*/
    if (sStatus == "Open") {

      //Reassign vehicle
        top.VehiclesMenu((sReassignExpCRUD.indexOf("U") != -1), 6);
        
      //Cancel vehicle
        top.VehiclesMenu((sCancelExpCRUD.indexOf("U") != -1), 7);

      //Void vehicle
      if (top.blnSupervisorFlag == "1"){
         top.VehiclesMenu((sVoidExpCRUD.indexOf("U") != -1), 8);
      } else 
        top.VehiclesMenu(false, 8);
    } else {
      top.VehiclesMenu(false, 7); // Cancel
      top.VehiclesMenu(false, 8); // Void
    }    

    top.gsCountNotes = gsCountNotes;
    top.gsCountTasks = gsCountTasks;
    top.gsCountBilling = gsCountBilling;
    top.gsEntityCRUD = gsVehicleCRUD;

    top.gsInvInsuredCount = giInvInsuredCount;
    if (top.gaVehClaimantCount){
      top.gaVehClaimantCount[gsVehNum] = giInvVehicleClaimantCount;
    }
    
    if (document.getElementById("btnRepairEndConfirm")) {
      if (RepairEndDate.value != "" && btnRepairEndConfirm){
        var dtRepairEnd = new Date(RepairEndDate.value);
        var dtToday = new Date();
  
        if (dtRepairEnd > dtToday)
          btnRepairEndConfirm.style.display = "none";
      } else if (RepairEndDate.value == "" ||RepairStartDate.value == "" || RepairStartDate.CCDisabled == true)
        btnRepairEndConfirm.style.display = "none";
    }

    if (document.getElementById("btnRepairStartConfirm")) {
      if (RepairStartDate.value != ""){
        var dtRepairStart = new Date(RepairStartDate.value);
        var dtToday = new Date();
        
        if (dtRepairStart > dtToday)
          btnRepairStartConfirm.style.display = "none";
      } else
        btnRepairStartConfirm.style.display = "none";
    }
    
    //set the vehicle year min and max.
    var dtNow = new Date();
    VehicleYear.min = 1900;
    VehicleYear.max = dtNow.getFullYear() + 1;

    //refresh the diary and notes
    if (top.blnInitialLoad == false){
       if (typeof(top.refreshNotesWindow) == "function")
         window.setTimeout("top.refreshNotesWindow( top.vLynxID, top.vUserID)", 100);
       if (typeof(top.refreshCheckListWindow) == "function")
         window.setTimeout("top.refreshCheckListWindow( top.vLynxID, top.vUserID )", 500);
    } else {
      top.blnInitialLoad = false;
    }
      
    if (typeof(parent.hideMask) == "function")
      window.setTimeout("parent.hideMask()", 100);
    
    if (stat1){
        stat1.style.top = (document.body.offsetHeight - 75) / 2;
        stat1.style.left = (document.body.offsetWidth - 240) / 2;      
    }
    
    try {
      var strPreOpenVehTab = top.strPreOpenVehTab;
      top.strPreOpenVehTab = "";
    } catch (e) {}
    
    if (strPreOpenVehTab != ""){
      var oTabs = tabVehicleDetail.tabs;
      if (oTabs) {
         for (var i = 0; i < oTabs.length; i++){
            if (oTabs[i].id == strPreOpenVehTab){
               window.setTimeout("tabVehicleDetail.SelectTab(" + oTabs[i].TabIdx + ")", 100);
               break;
            }
         }
      }
    }
    //Adding app_variable code for rentalMgt button
    var strFunctionName = "GetApplicationVar";
    var strDataToSend = "?VariableName=RentalMgt_Flag";
    RentalMgtHttpGet(strFunctionName, strDataToSend);
  }
  
  function RentalMgtHttpGet(strFunctionName, strDataToSend) {
  try{
  document.getElementById("btnRental").style.visibility = "hidden";
    if (typeof window.parent.parent.GetApdWebServiceURL == 'function' || typeof window.parent.parent.GetApdWebServiceURL == 'object')
        var strServiceURL = window.parent.parent.GetApdWebServiceURL();
    if (strServiceURL != "" && strServiceURL != null && strServiceURL != undefined) {
        var getURL = strServiceURL + "/" + strFunctionName + strDataToSend;
        var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
        //dataToSend = "StoredProcedure=uspWorkflowSendShopAssignWSXML" + "&Parameters=" + strAssignmentID;
        objXMLHTTP.open("GET", getURL, false);
        objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        objXMLHTTP.send();

        if (objXMLHTTP.responseXML.text == "True" || objXMLHTTP.responseXML.text == "TRUE")
			  {
			  if(document.getElementById("btnRental") != null)
                document.getElementById("btnRental").style.visibility = "visible";
				}
            else{
			if(document.getElementById("btnRental") != null)
                document.getElementById("btnRental").style.visibility = "hidden";
				}
    }
	}
	catch(e){
	}
}
  
  function validateOnSubmit()
  {
    if((blnPSExists==1 || blnRRPExists==1) && IsCellEmailFlag=='true')
    {
        var PreferredContact=document.getElementById("PrefMethodUpd").innerText;
        var CellPhoneCarrier=document.getElementById("CellPhoneCarrier").innerText;    
        var Email=document.getElementById("EmailAddress").value;
        var Cell=document.getElementById("Cell").value; 
        
        document.getElementById("lblEmailReqiured").style.display = 'none';
        document.getElementById("lblCellReqiured").style.display = 'none';
        document.getElementById("lblCellCarrierReqiured").style.display = 'none';
        
        CellPhoneCarrier=CellPhoneCarrier.replace(/^\s+|\s+$/g,'')

        if(PreferredContact=="E-mail6")
            document.getElementById("lblEmailReqiured").style.display = 'block';
        else if(PreferredContact=="Cell6")
         {
          document.getElementById("lblCellReqiured").style.display = 'block';
          document.getElementById("lblCellCarrierReqiured").style.display = 'block';
          }
          
          if(PreferredContact=="E-mail6" && Email=="")
          {            
            ClientWarning("Please enter the EmailAddress.");
            return false;
          }
          else if(PreferredContact=="Cell6" && Cell=="" && CellPhoneCarrier=="6")
          {
            
            ClientWarning("CellPhoneCarrier and Cell Number Required.");
            return false;
          }
          else if(PreferredContact=="Cell6" && Cell=="")
          {
             ClientWarning("Please enter the Cell.");
            return false;
          }
          else if(PreferredContact=="Cell6" && CellPhoneCarrier=="6")
          {
             ClientWarning("Please choose the CellPhoneCarrier.");
            return false;
          }
          else
            return true;
      }
      else
        return true;
  }
  
    function getInvolvedDetail(){
    if (InvolvedList.selectedIndex == -1) return;
    var aInvolved;
    var iFrm;
    aInvolved = InvolvedList.value.split(",");
    iInvolvedID = aInvolved[0];
    sAC = aInvolved[1];
    sEX = aInvolved[2];
    sCTY = aInvolved[3];
    sST = aInvolved[4];
    sZIP = aInvolved[5];
    if (sZIP.indexOf("|") != -1)
      sZIP = sZIP.substring(0, sZIP.indexOf("|"));
    iFrm = document.all["frmInvolved"];
    if (iFrm) 
      iFrm.src = "VehicleInvolvedCond.asp?WindowID=" + gsWindowID + "&InvolvedID=" + iInvolvedID + "&ExposureCD=" + gsExposureCD + "&pageReadOnly=" + sReadOnly; // + "&LynxID=" + gsLynxID + "&ClaimAspectID=" + claimAspectID;
  }
  
  function selectInvolved(){
    if (tabInvolved.isReady == true) {
      if (document.getElementById("InvolvedList")) {
        if (InvolvedList.selectedIndex == -1 && InvolvedList.Options.length >= 1) {
          //select the first involved in the list
          InvolvedList.selectedIndex = 0;
        } else {
          iInvolvedID = -1;
        }
      } else
        window.setTimeout("selectInvolved()", 150);
    } else {
      window.setTimeout("selectInvolved()", 150);
    }
  }
  
  function ShowImpact(strImpactDesc)
  {
    var strReturn = document.getElementById("ImpactArea").innerText + "|" + document.getElementById("PriorImpactArea").innerText + "|" + strImpactDesc;
    //alert(strReturn);
    strReturn = window.showModalDialog("/DamagePick2.asp",strReturn,"dialogHeight: 410px; dialogWidth: 520px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");

    if (strReturn && strReturn != "")
    {
      var sArray = strReturn.split("|");
      SaveImageImpact(sArray[0], sArray[1]);
    }
  }

  var strImpactPoints = "";
  var strPrevImpactPoints = "";

  function SaveImageImpact(sAreaImpact,sPrevAreaImpact)
  {
    ImpactArea.CCDisabled = false;
    PriorImpactArea.CCDisabled = false;
    ImpactArea.value = sAreaImpact.substr(0, (sAreaImpact.length - 2));
    PriorImpactArea.value = sPrevAreaImpact.substr(0, (sPrevAreaImpact.length - 2));
    ImpactArea.CCDisabled = true;
    PriorImpactArea.CCDisabled = true;

    var sArray = sAreaImpact.split(",");

    xmlReference.setProperty("SelectionLanguage", "XPath");
    ImpactPoints.value = ImpactNodes(sArray);
    sArray = sPrevAreaImpact.split(",");
    PriorImpactPoints.value = ImpactNodes(sArray);
    
  }

  function ImpactNodes(sArray)
  {
    var strPrimaryID = "";
    var strXPath = "";
    var objNode;
    var strIDs = "";
    var strNode="";

      var iLength = sArray.length;
      for (var i=0; i < iLength; i++)
      {
        if (sArray[i].Trim() != "")
        {
          if (sArray[i].indexOf("(Primary)") != -1)
          {
            strNode = sArray[i].Trim();
						var strTmp = strNode.substring(0, strNode.indexOf("(Primary)")-1);
            strXPath = '//Reference[@List="Impact"][@Name="' + strTmp + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strPrimaryID = objNode.value;
          }
          else
          {
            strXPath = '//Reference[@List="Impact"][@Name="' + sArray[i].Trim() + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strIDs += String(objNode.value) + ",";
          }
        }
      }

      if (strPrimaryID != "")
        strIDs = strPrimaryID + "," + strIDs;
      return strIDs;
  }
  
  // NADA module popup code
  function callNada()
  {
    if (gsVehicleCRUD.indexOf("U") == -1 || gsVehicleCRUD.indexOf("R") == -1) return;
    var myreq = "";
    var strNada = "";
    var strRetVal;
    var arrRetVal;
    var arrNameValue;
    var control;

    myreq = "&strVIN=" + VIN.value;
    if (strNada != "null")
      myreq += "&strVehId=" + strNada +
                "&strMake=" + Make.value +
                "&strModel=" + Model.value +
                "&strYear=" + VehicleYear.value +
                "&strBody=" + BodyStyle.value +
                "&txtMileage=" + Mileage.value +
                "&strReadOnly=" + bPageReadOnly +
                "&strZip=" + sZIP;              
    
    // The object "myObject" is sent to the modal window.
    strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
      return false;

    arrRetVal = strRetVal.split('||');
    var iLength = arrRetVal.length;
    for(var j=0; j < iLength; j++){
      arrNameValue = arrRetVal[j].split('--');

      switch(arrNameValue[0]){
        case 'year':
          VehicleYear.value = arrNameValue[1];
          break;
        case 'make':
          arrNameValue[1] = arrNameValue[1].substr(0, 15);  // 15 character limit imposed by database
          Make.value = arrNameValue[1];
          break;
        case 'model':
          arrNameValue[1] = arrNameValue[1].substr(0, 15);
          Model.value = arrNameValue[1];
          break;
        case 'body':
          arrNameValue[1] = arrNameValue[1].substr(0, 15);
          BodyStyle.value = arrNameValue[1];
          break;
        case 'vin':
          if (arrNameValue[1] != "")
            VIN.value = arrNameValue[1];
          break;
        case 'value':
          BookValueAmt.value = arrNameValue[1].substr(1, arrNameValue[1].length);
          break;
        case 'miles':
          Mileage.value = arrNameValue[1];
          break;
      }
    }
  }
  
  function addInvolved(){
    InvolvedList.selectedIndex = (InvolvedList.Options.length - 1);
  }
  
  function checkInvolvedDirty(){
    if (event.fromTab.name == "tabInvolved") {
      if (typeof(frmInvolved.isDirty) == "function" && frmInvolved.isDirty()) {
        var strRet = YesNoMessage("Need to Save", "Information on the Involved tab has changed.\nTo save the changes click Yes and then click on Save button. Click No to discard the changes.");
         switch (strRet) {
            case "Yes":
               event.returnValue = false;
               break;
            case "No":
               frmInvolved.document.location.reload();
               event.returnValue = true;
               break;
            default:
               event.returnValue = false;
               break;
         }        
      }
    }
  }
  
  function saveInvolved(){
//alert("SAved Clicked...");
    var iFrm;
    iFrm = document.all["frmInvolved"];
    if (iFrm) {
      event.saveXMLString = frmInvolved.getSaveData();
      event.returnValue = false;
      event.errHandler = false;
    }
  }
  
  function afterSaveInvolved(){
    var iFrm;
    var sSelInvolved = "";
    iFrm = document.all["frmInvolved"];
    if (iFrm) {
      var oXML = event.returnXML;
      if (oXML){
        var oNewInvolved = oXML.documentElement.selectSingleNode("//Root/NewVehicleInvolved");
        if (oNewInvolved) {
          var iInvolvedID1 = oNewInvolved.getAttribute("InvolvedID");
          var oInvolvedList = oXML.documentElement.selectNodes("//Root/VehicleInvolved[@InvolvedID='" + iInvolvedID1 + "']/VehicleInvolvedType[@InvolvedTypeName='Insured']");
          if(oInvolvedList && oInvolvedList.length > 0) {
            frmInvolved.document.location.reload();
            //tabInvolved.contentSaved();
            parent.document.location.reload();
            return;
          }
        }
      }
      if (iInvolvedID == 0) {
        //var oXML = event.returnXML;
        if (oXML){
          var oNewInvolved = oXML.documentElement.selectSingleNode("//Root/NewVehicleInvolved");
          if (oNewInvolved) {
            iInvolvedID = oNewInvolved.getAttribute("InvolvedID");
          }

          //refresh the Involved List
          var oInvolvedList = oXML.documentElement.selectNodes("//Root/VehicleInvolved");
          if (oInvolvedList) {
            InvolvedList.Clear();
            var sValue, sInvolvedList, sDisplay;
            var oInvolvedTypes;
            for (var i = 0; i < oInvolvedList.length; i++){
              sValue = oInvolvedList[i].getAttribute("InvolvedID") + "," +
                        oInvolvedList[i].getAttribute("DayAreaCode") + "," +
                        oInvolvedList[i].getAttribute("DayExchangeNumber") + "," +
                        oInvolvedList[i].getAttribute("AddressCity") + "," +
                        oInvolvedList[i].getAttribute("AddressState") + "," +
                        oInvolvedList[i].getAttribute("AddressZip");
              sInvolvedList = "";
              oInvolvedTypes = oInvolvedList[i].selectNodes("VehicleInvolvedType");
              if(oInvolvedTypes){
                for (var j=0; j < oInvolvedTypes.length; j++)
                  sInvolvedList += oInvolvedTypes[j].getAttribute("InvolvedTypeName") + ",";
                //remove the last comma
                sInvolvedList = sInvolvedList.substring(0, sInvolvedList.length - 1);
              }
              sDisplay = oInvolvedList[i].getAttribute("NameFirst") + " " +
                          oInvolvedList[i].getAttribute("NameLast") + " [" +
                          sInvolvedList + "]";
              InvolvedList.AddItem(sValue, sDisplay, null, "last");
            }
            InvolvedList.AddItem("0,,,,,", "-- Add New --", null, "last");
          }
          
          var oOptions = InvolvedList.Options;
          for (var i= 0; i < oOptions.length; i++) {
            if (oOptions[i].value.indexOf(iInvolvedID + ",") != -1) {
              InvolvedList.selectedIndex = i;
              break;
            }
          }
          getInvolvedDetail();
        }
      } else {
        var oXML = event.returnXML;
        var iOldIndex = InvolvedList.selectedIndex;
        if (oXML){
          var oInvolved = oXML.documentElement.selectSingleNode("//Root/VehicleInvolved[@InvolvedID='" + iInvolvedID + "']");
          if (oInvolved) {
            var oInvolvedTypes = oInvolved.selectNodes("VehicleInvolvedType");
            InvolvedList.RemoveItem(iOldIndex);
            var sInvolvedList = "";
            if(oInvolvedTypes){
              for (var i=0; i < oInvolvedTypes.length; i++)
                sInvolvedList += oInvolvedTypes[i].getAttribute("InvolvedTypeName") + ",";
              //remove the last comma
              sInvolvedList = sInvolvedList.substring(0, sInvolvedList.length - 1);
            }
            var sValue = oInvolved.getAttribute("InvolvedID") + "," +
                          oInvolved.getAttribute("DayAreaCode") + "," +
                          oInvolved.getAttribute("DayExchangeNumber") + "," +
                          oInvolved.getAttribute("AddressCity") + "," +
                          oInvolved.getAttribute("AddressState") + "," +
                          oInvolved.getAttribute("AddressZip");
            var sDisplay = oInvolved.getAttribute("NameFirst") + " " +
                        oInvolved.getAttribute("NameLast") + " [" +
                        sInvolvedList + "]";
            InvolvedList.AddItem(sValue, sDisplay, null, iOldIndex);
            InvolvedList.selectedIndex = iOldIndex;
          }
        }        
        frmInvolved.document.location.reload();
      }
    }
  }
  
  function delInvolved(){
    if (InvolvedList.selectedIndex == -1 || iInvolvedID <= 0) return;
    if (InvolvedList.text.indexOf("Insured") != -1) {
      ClientWarning("Can not delete the Insured Involved!");
      return;
    }

    var strRet = YesNoMessage("Confirm Delete", "Do you want to delete the selected Involved?");
     switch (strRet) {
        case "Yes":
          var sProc, sRequest;
      
          sRequest = "InvolvedID=" + iInvolvedID +
                      "&ClaimAspectID=" + gsClaimAspectID +
                      "&UserID=" + gsUserID +
                      "&SysLastUpdatedDate=" + frmInvolved.document.getElementById("SysLastUpdatedDate").value;
                      
          sProc = "uspVehicleInvolvedDel";
          
          var aRequests = new Array();
          aRequests.push( { procName : sProc,
                            method   : "executespnp",
                            data     : sRequest }
                        );
          var objRet = XMLSave(makeXMLSaveString(aRequests));
          if (objRet.code == 0) {
            InvolvedList.RemoveItem(InvolvedList.selectedIndex);
            iFrm = document.all["frmInvolved"];
            if (iFrm) 
              iFrm.src = "/blank.asp";
          }
           break;
        default:
           event.returnValue = false;
           break;
     }            
  }
  
  function VehicleDelete(){
    if (gsVehNum != "1") {
      if (gsClaimStatus == "Claim Cancelled" ||
          gsClaimStatus == "Claim Voided") {
          ClientWarning("Cannot delete when the claim has been cancelled or voided.");
          return;
      }

      var sIdVehicle = "";
      if (VehicleYear.value != "" && Make.value != "")
        sIdVehicle += " (" + VehicleYear.value+ " " + Make.value+ ")";

      var sVehicleDelConfirm = YesNoMessage("Are you sure that you want to delete Vehicle " + gsVehNum + sIdVehicle+ "?",
                                            "Press YES to delete this Vehicle, or NO to cancel.");
      if (sVehicleDelConfirm == "Yes")
      {
        var sProc;
        var sRequest = "SysLastUpdatedDate=" + gsClaimAspectLastUpdatedDate + "&";
        sRequest += "VehicleNumber=" + gsVehNum + "&";
        sRequest += "LynxID=" + gsLynxID + "&";
        sRequest += "UserID=" + gsUserID + "&";
        sRequest += "ClaimAspectID=" + gsClaimAspectID + "&=";
        sRequest += "NotifyEvent=1&";
        
        sProc = "uspClaimVehicleDel";
        
        var aRequests = new Array();
        aRequests.push( { procName : sProc,
                          method   : "executespnp",
                          data     : sRequest }
                      );
        //alert(makeXMLSaveString(aRequests));
        //return;
        var objRet = XMLSave(makeXMLSaveString(aRequests));
        if (objRet.code == 0)
          return true;
        else
          return false;
        
      }
    } else {
      ClientWarning("You can not remove Vehicle 1 in a Claim!");
    }    
  }
  
  function reloadTab(){
    switch (tabVehicleDetail.activetab.id) {
      case "tabEstimates":
        parent.loadTab = "Estimates";
        break;
      case "tabDocuments":
        parent.loadTab = "Documents";
    }
    document.location.reload();
  }
  
  function preSelectVehTab() {
    switch (parent.loadTab) {
      case "Estimates":
        tabVehicleDetail.SelectTab(tabEstimates.TabIdx);
        break;
      case "Documents":
        parent.loadTab = "";
        if (tabDocuments)
          tabVehicleDetail.SelectTab(tabDocuments.TabIdx);
        break;
    }
    parent.loadTab = "";
  }
  
  function reloadPage(){
    document.location.reload();
  }
  
  function updateVIN()
  {
    if (EstimateVIN.value == "")
      return;
    else
      VIN.value = EstimateVIN.value;
  }

  /*function ShowMETotals()
  {
    var strQuery = "METotals.asp?ClaimAspectID=" + gsClaimAspectID + "&InsuranceCompanyID=" + gsInsuranceCompanyID;
    var sDimensions = "dialogHeight:400px; dialogWidth:420px;"
    var sSettings = "resizable:no; scroll:yes; status:no; help:no; center:yes; unadorned:yes"
    var sReturn = window.showModalDialog(strQuery, '', sDimensions + sSettings); 
  }*/
  
  function addServiceChannel(){
    if (typeof(parent.addServiceChannel) == "function") {
      parent.addServiceChannel();
    }
  }
  
  function initClientServiceChannels(){
    var objSC = parent.selClientServiceChannel;
    if (objSC){
      if (objSC.readyState == "complete")
         objSC.Clear();
      else {
         window.setTimeout("initClientServiceChannels()", 100);
         return;
      }
      
      var oAvailableSC = xmlClientServiceChannels.selectNodes("/Root/Reference[@List='ClientServiceChannels']");
      for(var i = 0; i < oAvailableSC.length; i++){
         var oExistingSC = xmlExistingServiceChannels.selectSingleNode("/Root/ClaimAspectServiceChannel[@ServiceChannelCD = '" + oAvailableSC[i].getAttribute("ReferenceID") + "']");
         if (oExistingSC == null)
            objSC.AddItem(oAvailableSC[i].getAttribute("ReferenceID"), oAvailableSC[i].getAttribute("Name"), "", "");
      }
    }
  }
  
  function IsClaimAspectServiceChannelExist(strServiceChannelCD){
      var blnRet = true;
      
      var oSC = xmlExistingServiceChannels.selectSingleNode("/Root/ClaimAspectServiceChannel[@ServiceChannelCD='" + strServiceChannelCD + "']");
      if (oSC)
         blnRet = true;
      else
         blnRet = false;
         
      return blnRet;
  }
  
   function makePrimary(obj, strClaimAspectServiceChannelID, strSysLastUpdatedDate){
      if (strClaimAspectServiceChannelID != "" && isNaN(strClaimAspectServiceChannelID) == false){
         if (YesNoMessage("Confirm Action", "Do you want to make " + tabVehicleDetail.activetab.caption + " as the Primary Service Channel?") == "Yes"){
            obj.CCDisabled = true;
            window.setTimeout("makePrimary2(" + strClaimAspectServiceChannelID + ", '" + strSysLastUpdatedDate + "')", 100);
         }
      }
   }
   
   function makePrimary2(strClaimAspectServiceChannelID, strSysLastUpdatedDate){
      var sRequest = "";
      var sProc = "uspClaimVehicleSetPrimaryChannel";
      sRequest = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID +
                 "&ClaimAspectID=" + gsClaimAspectID +
                 "&UserID=" + gsUserID +
                 "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
      //alert(sRequest);
      //return;
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "ExecuteSpNpAsXML",
                        data     : sRequest }
                    );
      var sXMLRequest = makeXMLSaveString(aRequests);
      var objRet = XMLSave(sXMLRequest);
      if (objRet && objRet.code == 0 && objRet.xml) {
         var oRetXML = objRet.xml;
         if (oRetXML){
            document.location.reload();
         }
      } else {               
         ClientWarning("Unable to set " + tabVehicleDetail.activetab.caption + " as Primary Service Channel. Please contact Helpdesk.");
      }
   }
   
   function initSC(strClaimAspectServiceChannelID){
      var bFound = false;
      if (strClaimAspectServiceChannelID != "" && isNaN(strClaimAspectServiceChannelID) == false){
         strCurrentClaimAspectServiceChannelID = strClaimAspectServiceChannelID;
         var oTabGrp = document.getElementById("tabSCGrp" + strClaimAspectServiceChannelID);
         if (oTabGrp && oTabGrp.activetab == undefined ){
            applyAPDBehavior(oTabGrp);
            //init the tab.
            //oTabGrp.initTabGroup();
            
            if (oTabGrp.isReady == false){
               window.setTimeout("initSC(" + strCurrentClaimAspectServiceChannelID + ")", 100);
               return;
            }

             try {
               var strPreOpenVehSCTab = top.strPreOpenVehSCTab;
               top.strPreOpenVehSCTab = "";
             } catch (e) {}
             
             if (strPreOpenVehSCTab != ""){
               var oTabs = oTabGrp.tabs;
               if (oTabs) {
                  for (var i = 0; i < oTabs.length; i++){
                     //alert(oTabs[i].id + "\n" + strPreOpenVehSCTab);
                     if (oTabs[i].id == strPreOpenVehSCTab){
                        //alert("tabSCGrp" + strClaimAspectServiceChannelID + ".SelectTab(" + oTabs[i].TabIdx + ")");
                        window.setTimeout("tabSCGrp" + strClaimAspectServiceChannelID + ".SelectTab(" + oTabs[i].TabIdx + ")", 100);
                        return;
                     }
                  }
               }
             }
             
            window.setTimeout("tabSCGrp" + strClaimAspectServiceChannelID + ".SelectTab(0)", 100); //select the assignments tab
         }
      }      
   }
   
   function initDocuments(){
      var strClaimAspectServiceChannelID = strCurrentClaimAspectServiceChannelID;
      if (strClaimAspectServiceChannelID != "" && isNaN(strClaimAspectServiceChannelID) == false){
         // check if the Documents tab has initialized completely.
         var oTabGrp = document.getElementById("tabSCGrp" + strClaimAspectServiceChannelID);
         if (oTabGrp){
            var oActiveTab = oTabGrp.activetab;
            if (oActiveTab) {
               if (oActiveTab.isReady == false){
                  // Documents tab is still initializing. Time out now and try again later.
                  window.setTimeout("initDocuments()", 100);
                  return;
               }
            }
         }
         var oGrid = document.getElementById("GrdCoverage_" + strClaimAspectServiceChannelID);
         if (oGrid) {
            // check if the Grid control has initialized.
            if (oGrid.readyState != "complete"){
               // Grid is still initializing. Time out now and try again later.
               window.setTimeout("initDocuments()", 100);
               return;
            }
            oGrid.refreshData();
         }
         var oDivDoc = document.getElementById("divDocs" + strClaimAspectServiceChannelID);
         if (oDivDoc){
            if (oDivDoc.getAttribute("loaded") != true){
               //init the document thumbnails for this service channel
               var oXML = document.getElementById("xmlSCDocs_" + strClaimAspectServiceChannelID);
               if (oXML) {
                  var oDocs = oXML.selectNodes("/Root/Document");
                  for (var i = 0; i < oDocs.length; i++){
                     var strImageLocation = oDocs[i].getAttribute("ImageLocation");

                     // -- 18Jan2016 - TVD - Image Archiving
                     var sArchived = oDocs[i].getAttribute("Archived");
                     strImageLocation = strImageLocation.replace(/\\\\/g, "\\");
                     var iPos = strImageLocation.lastIndexOf(".");
                     var sExt = "";
                     var objImg = document.getElementById("img" + oDocs[i].getAttribute("DocumentID"));
                     if (iPos != -1) {
                       sExt = strImageLocation.substr(iPos+1, strImageLocation.length).toLowerCase();
                     }
                     if(objImg != null)
                     {
                       if (sExt != "gif" && sExt != "jpg" && sExt != "bmp" && sExt != "png" && sExt != "emf" && sExt != "wmf"){
                         objImg.style.display = "none";
                         var s = "<SPAN class='thumbNailInfo' style='border:0px;text-align:center;color:#4169E1;height:100%;width:100%'>" + oDocs[i].getAttribute("DocumentTypeName") + "</SPAN>";
                         objImg.insertAdjacentHTML("afterEnd", s);
                       } else {
                            
                            // -- 18Jan2016 - TVD - Image Archiving
                              if (sArchived=='0')
	                      {
                              	var strLoc = "Thumbnail.asp?Doc=" + gsImageRootDir + strImageLocation;
	                      }
	                      else
	                      {
                            	var strLoc = "Thumbnail.asp?Doc=" + gsImageRootArchiveDir + strImageLocation;
	                      }
                          
                          //strLoc = strLoc.replace(/\\/g, "\\\\");
                          objImg.src = strLoc;
                          objImg.FileExt = sExt;
                          objImg.imgIndex = (i - 1);                  
                       }
                     }
                  }
                  oDivDoc.setAttribute("loaded", true);
               }
            }
         }
      }
      
      var oDivAssignmentHistory = document.getElementById("divAssignmentHistory" + strClaimAspectServiceChannelID)
      if (oDivAssignmentHistory)
         oDivAssignmentHistory.style.display = "none";
         
      
      var oDivAssignmentSave = document.getElementById("divAssignmentSave" + strClaimAspectServiceChannelID)
      if (oDivAssignmentSave)
         oDivAssignmentSave.style.display = "none";
   }
   
   function initConcessions(){
      var strClaimAspectServiceChannelID = strCurrentClaimAspectServiceChannelID;

      var oDivAssignmentHistory = document.getElementById("divAssignmentHistory" + strClaimAspectServiceChannelID)
      if (oDivAssignmentHistory)
         oDivAssignmentHistory.style.display = "none";
         
      
      var oDivAssignmentSave = document.getElementById("divAssignmentSave" + strClaimAspectServiceChannelID)
      if (oDivAssignmentSave)
         oDivAssignmentSave.style.display = "none";
         
      // calculate the concession totals
      updateConcessionTotals(strCurrentClaimAspectServiceChannelID);
   }
function  callRental()
  {
  
  var strPage = "RentalCoverage.Aspx";
  var sRequest = "?ClaimAspectID=" + gsClaimAspectID +
			 "&LynxID=" + gsLynxID +
             "&InscCompID=" + gsInsuranceCompanyID +
             "&UserID=" + gsUserID ;
                 
				 
   strRetVal = window.showModelessDialog(strPage+sRequest, "", "dialogHeight:600px; dialogWidth:1050px; resizable:no; status:no; help:no; center:yes;");
   }
  function displayImage(obj, strClaimAspectServiceChannelID, strDocumentID, strBundleDocumentName, strArchived){
      if (blnSharing) return;
      var oXML = document.getElementById("xmlSCDocs_" + strClaimAspectServiceChannelID);
      if (oXML) {
         var oDoc = oXML.selectSingleNode("/Root/Document[@DocumentID='" + strDocumentID + "']");
         
         var strImageLocation = oDoc.getAttribute("ImageLocation");
         strImageLocation = strImageLocation.replace(/\\\\/g, "\\");
         
         if (strBundleDocumentName != "Estimate EMS")
          {
                // -- 18Jan2016 - TVD - Image Archiving
                if (strArchived=='0')
                {
                    var strURL = "/EstimateDocView.asp?docPath=" + gsImageRootDir + strImageLocation;
	        }
	        else
	        {
                    var strURL = "/EstimateDocView.asp?docPath=" + gsImageRootArchiveDir + strImageLocation;
	        }
                window.showModelessDialog(strURL, "", "dialogHeight:490px;dialogWidth:800px;center:yes;help:no;resizable:yes;scroll:no;status:no;");
          }
          else
          {
            if (curDocShown) {
              curDocShown.className = "thumbNail";
            }
              obj.className = "thumbNailSelected";
              ClientWarning("This file type cannot be displayed in APD."); 
          }
         if (curDocShown) {
           curDocShown.className = "thumbNail";
         }
         obj.className = "thumbNailSelected";
      }

      var oDivDoc = document.getElementById("divDocs" + strClaimAspectServiceChannelID);
      if (oDivDoc){
         //myScrollIntoView(obj, oDivDoc);
         obj.DocumentID = strDocumentID;
         curDocShown = obj;
      }
  }
  
  var iCount = 0;
  var bScrolling = false;
  var iScrollMode = 0; // 1=right; 2=left;
  curDocShown = null;
  
  function myScrollIntoView(obj, objWithScroll){
     var iScrollPos = (obj.offsetLeft + obj.offsetWidth) - objWithScroll.scrollLeft - objWithScroll.clientWidth; //10 -> margin * 2 -> 5 * 2;
     if (iScrollPos < 0){
       //objWithScroll.doScroll("scrollbarPageRight");
       if (iScrollMode != 2) {
         objWithScroll.doScroll("scrollbarRight");
         if (iCount < 25 && !bScrolling) {
           iCount++; iScrollMode = 1;
           myScrollIntoView(obj, objWithScroll);
         }
         iScrollMode = 0;iCount = 0;
       }
     }
     else {
       if (Math.abs(iScrollPos) > Math.abs(obj.offsetWidth - objWithScroll.clientWidth)) {
         if (iScrollMode != 1) {
           objWithScroll.doScroll("scrollbarLeft");
           if (iCount < 25) {
             iCount++; iScrollMode = 2;
             myScrollIntoView(obj, objWithScroll);
           }
         }
       }
       iScrollMode = 0;iCount = 0;
     }
  }

  function togglePinned(objImg){
    //if (objImg.src.indexOf("/images/pinned.gif") != -1) {
      var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
      var strActiveTab = tabVehicleDetail.activetab.id;
      var strClaimAspectServiceChannelID = strActiveTab.split("_")[1];

      var sURL = "Documents.asp?WindowID=" + gsWindowID + 
                 "&InsuranceID=" + gsInsuranceCompanyID + 
                 "&LynxID=" + gsLynxID + 
                 "&ClaimAspectID=" + gsClaimAspectID + 
                 "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID + 
                 "&EntityName=Vehicle" + 
                 "&EntityNumber=" + gsVehNum + 
                 "&viewMode=2"; 
      if (curDocShown)
        sURL += "&selectThumbnail=" + curDocShown.getAttribute("DocumentID");
        
        //objImg.src = "/images/pin.gif";
        //objImg.title = "Detach Photo Viewer";
         if (newWin != null)
           newWin.close();
        newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
        newWin.opener = window.self;
    /*}
    else {
      objImg.src = "/images/pinned.gif";
      objImg.title = "Close Detached Photo Viewer";
      if (newWin != null)
        newWin.close();
    }*/
  }
  
  function showEstimate(strGridID){
      var oGrid = document.getElementById(strGridID);
      if (oGrid){
         var oSelRow = oGrid.selectedRow;
         if (oSelRow && oSelRow.objXML){
            var oXML = oSelRow.objXML;
            if (oXML){
               var iFullSummaryExistsFlag = oXML.getAttribute("FullSummaryExistsFlag");
               var strDocumentID = oXML.getAttribute("DocumentID");

               switch(iFullSummaryExistsFlag){
                  case "0":
                     //ShowShortEstimate(this.parentElement.parentElement, "<xsl:value-of select="@DocumentID"/>", "<xsl:value-of select="@VANFlag"/>", "<xsl:value-of select="@ImageLocation"/>");
                     ShowShortEstimate(oGrid, oXML);
                     break;
                  case "1":
                     parent.ShowEstimate(strDocumentID, 1);
                     break;
               }
               if (strCurrentClaimAspectServiceChannelID != "" && isNaN(strCurrentClaimAspectServiceChannelID) == false){
                  var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
                  if (oDivAssgn){
                     oDivAssgn.setAttribute("frameLoaded", false);
                     var oFrm = oDivAssgn.firstChild;
                     if (oFrm){
                        oFrm.src = "blank.asp";
                     }
                  }
               }
            }
         }
      }
  }
  
  //function ShowShortEstimate(oRow, sDocumentID, bVANflag, ImageLocation) {
  function ShowShortEstimate(oGrid, oXML) {
      if (oGrid && oXML){
         var strDocumentID = oXML.getAttribute("DocumentID");
         var strImageLocation  = oXML.getAttribute("ImageLocation");

          // -- 18Jan2016 - TVD - Image Archiving
          if (sArchived=='0')
	        {
             strImageLocation = gsImageRootDir.replace(/\\/g, "\\\\") + strImageLocation; //strImageLocation.replace(/\\\\/g, "\\");
	        }
	        else
	        {
             strImageLocation = gsImageRootArchiveDir.replace(/\\/g, "\\\\") + strImageLocation; //strImageLocation.replace(/\\\\/g, "\\");
	        }


         if (strDocumentID != "") {
            //-- 02Sep2014 - TVD - Classic version -->
            var sURL = "DocumentEstDetails.asp?EstimateUpd=1&ClaimAspectID=" + gsClaimAspectID + "&DocumentID=" + strDocumentID + "&UserID=" + gsUserID + "&ImageLocation=" + strImageLocation + "&LynxId=" + gsLynxID + "&InsuranceCompanyID=" + gsInsuranceCompanyID;
            var strReturn = window.showModalDialog(sURL,"","dialogHeight:608px; dialogWidth:860px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
            //alert(sURL);
            
            //-- 02Sep2014 - TVD - Dot Net -->
            //var sURL = "DocumentEstDetails.aspx?EstimateUpd=1&ClaimAspectID=" + gsClaimAspectID + "&DocumentID=" + strDocumentID + "&UserID=" + gsUserID + "&ImageLocation=" + strImageLocation + "&LynxId=" + gsLynxID + "&InsuranceCompanyID=" + gsInsuranceCompanyID;
            //var strReturn = window.showModalDialog(sURL,"","dialogHeight:608px; dialogWidth:860px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
         }
         if (strReturn && strReturn != "" && strReturn.substr(0, 2) != "0|") {//user clicked on save
            var sArray = strReturn.split("|");
            if (sArray[6] != "1") {
               if (sArray[0] != ""){
                  var iSupSeqNo = parseInt(sArray[0].substr(1), 10);
                  //alert(iSupSeqNo);
                  if (iSupSeqNo == 0){
                     oXML.setAttribute("SequenceNum", "E01");
                  } else {
                     oXML.setAttribute("SequenceNum", "S" + (iSupSeqNo < 10 ? "0" + iSupSeqNo : iSupSeqNo));
                  }
                  oXML.setAttribute("DocumentTypeName", (sArray[0].substr(0,1) == "E" ? "Estimate" : "Supplement"));
                  oXML.setAttribute("SupplementSeqNumber", parseInt(sArray[0].substr(1), 10));
               }
               
               if (sArray[1] != "")
                  oXML.setAttribute("GrossEstimateAmt", sArray[1]);

               if (sArray[2] != "")
                  oXML.setAttribute("NetEstimateAmt", sArray[2]);

               if (sArray[3] == "O"){
                  oXML.setAttribute("OAIndicator", "Original");
                  oXML.setAttribute("EstimateTypeCD", "O");
               } else if (sArray[3] == "A") {
                  oXML.setAttribute("OAIndicator", "Audited");
                  oXML.setAttribute("EstimateTypeCD", "A");
               }

               if (sArray[4] == "1"){
                  oXML.setAttribute("DupIndicator", "Yes");
                  oXML.setAttribute("DuplicateFlag", "1");
               } else if (sArray[4] == "0"){
                  oXML.setAttribute("DupIndicator", "No");
                  oXML.setAttribute("DuplicateFlag", "0");
               }

               if (sArray[5] == "Y"){
                  oXML.setAttribute("PriceAgreedIndicator", "Yes");
                  oXML.setAttribute("AgreedProceMetCD", "Y");
               } else if (sArray[5] == "N"){
                  oXML.setAttribute("PriceAgreedIndicator", "No");
                  oXML.setAttribute("AgreedProceMetCD", "N");
               }

               if (sArray[7] != "")
                  oXML.setAttribute("SysLastUpdatedDateEstimate", sArray[7]);

               oGrid.refresh();
               
               disableCurrentAssignment(sArray[8] == "true");

               if (strCurrentClaimAspectServiceChannelID != "" && isNaN(strCurrentClaimAspectServiceChannelID) == false){
                  var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
                  if (oDivAssgn){
                     oDivAssgn.setAttribute("frameLoaded", false);
                     var oFrm = oDivAssgn.firstChild;
                     if (oFrm){
                        oFrm.src = "blank.asp";
                     }
                  }
               }
            }
         } else if (strReturn.substr(0, 2) == "0|") { //user clicked on close
            var sArray = strReturn.split("|");
            disableCurrentAssignment(sArray[1] == "true");
         }
      }
  }
  
  function disableCurrentAssignment(bDisabled){
    /*if (bDisabled == true){
      CurrentAssignmentTypeID.CCDisabled = true;
    } else if (bPageReadOnly == false)
      CurrentAssignmentTypeID.CCDisabled = false;*/
  }
  
  function showAssignment(){
      var strClaimAspectServiceChannelID = strCurrentClaimAspectServiceChannelID;	
      if (strClaimAspectServiceChannelID != "" && isNaN(strClaimAspectServiceChannelID) == false){
         var oDivAssgn = document.getElementById("divSCAssgn" + strClaimAspectServiceChannelID);
         if (oDivAssgn && oDivAssgn.getAttribute("frameLoaded") != true){
           stat1.Show("Please wait...");
            window.setTimeout("showAssignment2()", 300);
         }
      }
      
      var oDivAssignmentHistory = document.getElementById("divAssignmentHistory" + strClaimAspectServiceChannelID)
      if (oDivAssignmentHistory)
         oDivAssignmentHistory.style.display = "inline";
         
      
      var oDivAssignmentSave = document.getElementById("divAssignmentSave" + strClaimAspectServiceChannelID)	
      if (oDivAssignmentSave)
         oDivAssignmentSave.style.display = "inline";
  }
  
  function showAssignment2(){
      var strURL  = "VehicleShopAssignment.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + "&ClaimAspectServiceChannelID=" + strCurrentClaimAspectServiceChannelID + "&pageReadOnly=" + sReadOnly;
      var obj = xmlExistingServiceChannels.selectSingleNode("//ClaimAspectServiceChannel[@ClaimAspectServiceChannelID='" + strCurrentClaimAspectServiceChannelID + "']");     
	  if (obj) {
         if (obj.getAttribute("ServiceChannelCD") == "TL") {
            strURL = "TLDetails.htm";
         }
      }
	  var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
      var oFrm = oDivAssgn.firstChild;
      if (oFrm){
         oFrm.src = strURL;
         oDivAssgn.setAttribute("frameLoaded", true);
      }
      else { window.setTimeout("showAssignment2()", 100); }
  }
  
  function viewAssignmentHistory(){
      var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
      var oFrm = oDivAssgn.firstChild;
      if (oFrm){
         if (typeof(oFrm.contentWindow.viewHistory) == "function")
            oFrm.contentWindow.viewHistory();
      }
  }
  
  function saveAssignment(){
      stat1.Show("Saving Data ...");
      window.setTimeout("saveAssignment2()", 100);
  }
  
  function saveAssignment2(){
      var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
      var oFrm = oDivAssgn.firstChild;
      if (oFrm){
         if (typeof(oFrm.contentWindow.saveAssignment) == "function") {
            oFrm.contentWindow.saveAssignment();
         } else {
            stat1.Hide();
         }
      } else {
         stat1.Hide();
      }
  }
  
  function disableButton(strBtnName, blnDisabled){
      var oBtn = document.getElementById(strBtnName + strCurrentClaimAspectServiceChannelID);
      if(oBtn) {
         oBtn.CCDisabled = blnDisabled;
      }
  }
  
  function disableAssignmentButtons(blnDisabled){
      /*var oAssignmentTab = document.getElementById("tabSCAssignment" + strCurrentClaimAspectServiceChannelID);
      if (oAssignmentTab) {
        // alert(oAssignmentTab.CRUD);
         oAssignmentTab.CRUD = blnCRUD;
      }
      
      var oSCGrp = document.getElementById("tabSCGrp" + strCurrentClaimAspectServiceChannelID);
      if (oSCGrp) oSCGrp.RefreshADS();*/
      
      var oBtn = document.getElementById("btnAssignmentHistory_" + strCurrentClaimAspectServiceChannelID);
      if (oBtn) {
         oBtn.CCDisabled = blnDisabled;
      }

      var oBtn = document.getElementById("btnSaveAssignment_" + strCurrentClaimAspectServiceChannelID);
      if (oBtn) {
         oBtn.CCDisabled = blnDisabled;
      }
  }
  
  function resetAssignmentCRUD(){
  }
  
  function cancelAssignedShop(){
  }
  
  function setReferenceNumber(strReferenceNumber){
      var oDiv = document.getElementById("divReferenceNo" + strCurrentClaimAspectServiceChannelID);
      if (oDiv){
         oDiv.innerText = strReferenceNumber;
      }
  }
  
  function updateSCMenuContext(){
      var oActiveTab = tabVehicleDetail.activetab;
      
      if (oActiveTab){
         var strActiveTabID = oActiveTab.id;
         if (strActiveTabID.indexOf("tabSC_") != -1){
            try {
               if (xmlExistingServiceChannels){
               } else {
                  window.setTimeout("updateSCMenuContext()", 100);
                  return;
               }
            } catch (e) {
               window.setTimeout("updateSCMenuContext()", 100);
               return;
            }
            var objSC = xmlExistingServiceChannels.selectSingleNode("/Root/ClaimAspectServiceChannel[@ClaimAspectServiceChannelID='" + strCurrentClaimAspectServiceChannelID +"']");
            if (objSC) {
               var strStatus = objSC.getAttribute("StatusName");
               //Cancel Service Channel.
               top.VehiclesMenu((sCancelExpCRUD.indexOf("U") != -1), 2);
               //Complete Service Channel
               top.VehiclesMenu((sCloseExpCRUD.indexOf("U") != -1), 1);
	       top.strDispositionTypeCD = objSC.getAttribute("DispositionTypeCD");
	       top.strServiceChannelCD = objSC.getAttribute("ServiceChannelCD");
            }
            
            top.strCurrentClaimAspectServiceChannelID = strCurrentClaimAspectServiceChannelID;
            top.strCurrentServiceChannelName = oActiveTab.caption;
         } else {
            top.VehiclesMenu(false, 1);
            top.VehiclesMenu(false, 2);
            
            top.strCurrentClaimAspectServiceChannelID = "";
            top.strCurrentServiceChannelName = "";
	    top.strDispositionTypeCD = "";
	    top.strServiceChannelCD = "";
         }
      }
  }
  
  function refreshVehicle(){
      var oActiveTab = tabVehicleDetail.activetab;
      
      if (oActiveTab){
         var strActiveTabID = oActiveTab.id;
         top.strPreOpenVehTab = strActiveTabID;
      }

      var oTabGrp = document.getElementById("tabSCGrp" + strCurrentClaimAspectServiceChannelID);
      if (oTabGrp){
         var oActiveTab = oTabGrp.activetab;
         
         if (oActiveTab){
            var strActiveTabID = oActiveTab.id;
            top.strPreOpenVehSCTab = strActiveTabID;
         }
      }
      document.location.reload();
  }
  
  function updateCoveragesAppliedClaim(strClaimCoverageID, iDeductibleChange, iLimitChange){
      if (typeof(parent.updateClaimCoverages) == "function") {
         parent.updateClaimCoverages(strClaimCoverageID, iDeductibleChange, iLimitChange);
      }
  }

  function checkFrameLoaded(){
    switch (tabVehicleDetail.activetab.id){
      case "tabInvolved":
        var iFrm;
        iFrm = document.all["frmInvolved"];
        if (iFrm) {
          if (iFrm.readyState != "complete"){
            ClientWarning("Please wait for the Involved information to display completely and try again.");
            event.returnValue = false;
            return;
          }
          if (typeof(frmInvolved.validateData) == "function")
            event.returnValue = frmInvolved.validateData();
        }
        break;
    }
  }
  
  function doConcessionAdd(){
      var objXML = document.getElementById("xmlSCConcession_" + strCurrentClaimAspectServiceChannelID);
      var blnTLConcessionExists = false;
      if (objXML){
         var objTLConcessionExists = objXML.selectNodes("//Concession[@TypeDescription='Indemnity' and @ReasonDescription='Total Loss']");
         blnTLConcessionExists = (objTLConcessionExists.length >= 1)
      }
      var sURL = "ConcessionDetail.htm";
      var obj = {
         ClaimAspectServiceChannelConcessionID: "",
         ClaimAspectServiceChannelID : strCurrentClaimAspectServiceChannelID,
         ConcessionReasonID: "",
         ConcessionAmount: "",
         Comments: "",
         UserID: gsUserID,
         TLConcessionExists: blnTLConcessionExists
      };
      var objReturn = window.showModalDialog(sURL,obj,"dialogHeight:250px; dialogWidth:400px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
      if (objReturn && typeof(objReturn) == "object") {//user clicked on save
         updateConcessionGrid(objReturn, "A");
      }
  }

  function doConcessionEdit(){
      var objGrid = document.getElementById("GrdConcession_" + strCurrentClaimAspectServiceChannelID);
      if (objGrid){
         var objSelRow = objGrid.selectedRow;
         if (objSelRow.objXML){
            var objXML = document.getElementById("xmlSCConcession_" + strCurrentClaimAspectServiceChannelID);
            var blnTLConcessionExists = false;
            if (objXML){
               var objTLConcessionExists = objXML.selectNodes("//Concession[@TypeDescription='Indemnity' and @ReasonDescription='Total Loss']");
               
               blnTLConcessionExists = (objTLConcessionExists.length >= 1)
            }

            var sURL = "ConcessionDetail.htm";
            var obj = {
               ClaimAspectServiceChannelConcessionID: objSelRow.objXML.getAttribute("ClaimAspectServiceChannelConcessionID"),
               ClaimAspectServiceChannelID : objSelRow.objXML.getAttribute("ClaimAspectServiceChannelID"),
               ConcessionReasonID: objSelRow.objXML.getAttribute("ConcessionReasonID"),
               ConcessionAmount: objSelRow.objXML.getAttribute("Amount"),
               Comments: objSelRow.objXML.getAttribute("Comments"),
               UserID: gsUserID,
               TLConcessionExists: blnTLConcessionExists
            };
            var objReturn = window.showModalDialog(sURL,obj,"dialogHeight:250px; dialogWidth:400px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
            if (objReturn && typeof(objReturn) == "object") {//user clicked on save
               updateConcessionGrid(objReturn, "E");
            }
         }
      }
  }

  function doConcessionDelete(){
      var objGrid = document.getElementById("GrdConcession_" + strCurrentClaimAspectServiceChannelID);
      if (objGrid){
         var objSelRow = objGrid.selectedRow;
         if (objSelRow.objXML){
            var strRet = YesNoMessage("Confirm Delete", "Do you want to delete this concession?");
            if (strRet == "Yes"){
               var strClaimAspectServiceChannelConcessionID = objSelRow.objXML.getAttribute("ClaimAspectServiceChannelConcessionID");
               var sProc = "uspConcessionDelDetail";
               var sRequest = "ClaimAspectServiceChannelConcessionID=" + strClaimAspectServiceChannelConcessionID +
                              "&UserID=" + gsUserID;
      
               var sMethod = "ExecuteSpNpAsXML";
               
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : sMethod,
                                 data     : sRequest }
                             );
               
               var sXMLRequest = makeXMLSaveString(aRequests);
               //alert(sXMLRequest); return;
               
               var objRet = XMLSave(sXMLRequest);
               if (objRet && objRet.code == 0 && objRet.xml) {
                  var oRetXML = objRet.xml;
                  if (oRetXML){
                     var objRet = oRetXML.selectSingleNode("//Root/Concession");
                     if (objRet){
                        if (objRet.getAttribute("EnabledFlag") == "0"){
                           updateConcessionGrid(objRet, "D");
                        } else {
                           ClientWarning("Unable to delete concession details. Please contact Helpdesk.");
                        }
                     }
                  }
               } else {               
                  ClientWarning("Unable to delete concession details. Please contact Helpdesk.");
               }
            }
         }
      }
  }
  
  function updateConcessionGrid(obj, strChangeMode){
      switch (strChangeMode){
         case "A":
            var objXML = document.getElementById("xmlSCConcession_" + strCurrentClaimAspectServiceChannelID);
            if (objXML){
               var oNode = objXML.createElement("Concession");
               oNode.setAttribute("ClaimAspectServiceChannelConcessionID", obj.ClaimAspectServiceChannelConcessionID);
               oNode.setAttribute("ClaimAspectServiceChannelID", obj.ClaimAspectServiceChannelID);
               oNode.setAttribute("ConcessionReasonID", obj.ConcessionReasonID);
               oNode.setAttribute("CreatedDate", obj.CreatedDate);
               oNode.setAttribute("CreatedDateFormatted", formatSQLDateTime(obj.CreatedDate));
               oNode.setAttribute("TypeDescription", obj.TypeDescription);
               oNode.setAttribute("ReasonDescription", obj.ReasonDescription);
               oNode.setAttribute("Amount", obj.Amount);
               oNode.setAttribute("Comments", obj.Comments);
               objXML.firstChild.appendChild(oNode);
               var objGrid = document.getElementById("GrdConcession_" + strCurrentClaimAspectServiceChannelID);
               if (objGrid)
                  objGrid.refresh();
               updateConcessionTotals(strCurrentClaimAspectServiceChannelID);
            }
            break;
            
         case "E":
            //alert(formatSQLDateTime(obj.CreatedDate));
            var objGrid = document.getElementById("GrdConcession_" + strCurrentClaimAspectServiceChannelID);
            if (objGrid){
               var objSelRow = objGrid.selectedRow;
               if (objSelRow){
                  strClaimAspectServiceChannelConcessionID = objSelRow.objXML.getAttribute("ClaimAspectServiceChannelConcessionID");
                  //alert(strClaimAspectServiceChannelConcessionID);
                  var objXML = document.getElementById("xmlSCConcession_" + strCurrentClaimAspectServiceChannelID);
                  if (objXML){
                     var objRow = objXML.selectSingleNode("//Concession[@ClaimAspectServiceChannelConcessionID = '" + strClaimAspectServiceChannelConcessionID + "']");
                     if (objRow){
                        objRow.setAttribute("ConcessionReasonID", obj.ConcessionReasonID);
                        objRow.setAttribute("TypeDescription", obj.TypeDescription);
                        objRow.setAttribute("ReasonDescription", obj.ReasonDescription);
                        objRow.setAttribute("CreatedDate", obj.CreatedDate);
                        objRow.setAttribute("CreatedDateFormatted", formatSQLDateTime(obj.CreatedDate));
                        objRow.setAttribute("Amount", obj.Amount);
                        objRow.setAttribute("Comments", obj.Comments);
                        objGrid.refresh();
                        updateConcessionTotals(strCurrentClaimAspectServiceChannelID);
                     } else {
                        document.location.reload();
                     }
                  }
               }
            }
            break;
         case "D":
            var objGrid = document.getElementById("GrdConcession_" + strCurrentClaimAspectServiceChannelID);
            if (objGrid){
               var objSelRow = objGrid.selectedRow;
               if (objSelRow){
                  strClaimAspectServiceChannelConcessionID = objSelRow.objXML.getAttribute("ClaimAspectServiceChannelConcessionID");
                  var objXML = document.getElementById("xmlSCConcession_" + strCurrentClaimAspectServiceChannelID);
                  if (objXML){
                     var objRow = objXML.selectSingleNode("//Concession[@ClaimAspectServiceChannelConcessionID = '" + strClaimAspectServiceChannelConcessionID + "']");
                     if (objRow){
                        objXML.firstChild.removeChild(objRow);
                        objGrid.refresh();
                        updateConcessionTotals(strCurrentClaimAspectServiceChannelID);
                     }
                  }
               }
            }
            break;
      }
  }
  
   
   function updateConcessionTotals(strClaimAspectServiceChannelID){
      var objXML = document.getElementById("xmlSCConcession_" + strClaimAspectServiceChannelID);
      if (objXML){
         var objIndemnityConcessions = objXML.selectNodes("//Concession[@TypeDescription = 'Indemnity']");
         var total = 0.0;
         for (i = 0; i < objIndemnityConcessions.length; i++){
            total += parseFloat(objIndemnityConcessions[i].getAttribute("Amount"));
         }
         objIndemnityConcessions = null;
         divIndemnityConcessionTotal.innerText = "$ " + formatCurrencyString(total);

         /*var objRentalConcessions = objXML.selectNodes("//Concession[@TypeDescription = 'Rental']");
         var total = 0.0;
         for (i = 0; i < objRentalConcessions.length; i++){
            total += parseFloat(objRentalConcessions[i].getAttribute("Amount"));
         }
         divRentalConcessionTotal.innerText = "$ " + formatCurrencyString(total);
         objRentalConcessions = null;*/
      }
   }
   
   function enableAssignmentProfile(){
      var obj = document.getElementById("btnAssignmentProfile_" + strCurrentClaimAspectServiceChannelID);
      if (obj){
         obj.CCDisabled = false;
      }
   }
   
   function showAssignmentProfile(){
      if (winException){
         try {winException.close();} catch (e) {}
      }
      var oDivAssgn = document.getElementById("divSCAssgn" + strCurrentClaimAspectServiceChannelID);
      var oFrm = oDivAssgn.firstChild;
      if (oFrm){
         if (typeof(oFrm.contentWindow.getLocationInfo) == "function"){
            var args = oFrm.contentWindow.getLocationInfo();

            //var args = {InsuranceCompanyID: 99, InsuranceCompanyName: "XYZ", State: "TX", County: "LUBBOCK", City: "SLATOR" };
            winException = window.showModelessDialog("exceptionreference.htm", args, "dialogHeight:740px;dialogWidth:265px;dialogLeft;status:no;scroll:no;help:no");
         }
      }
   }
   
   function closeChildDialogs(){
      if (winException){
         try {winException.close();} catch (e) {}
      }
   }
   
   function shareDocument(){
      var src = event.srcElement.id;
      var documentID = src.substr(8);

      var sProc;
      var sRequest = "DocumentID=" + documentID + "&ApprovedFlag=" + event.srcElement.value;
                     
      sProc = "uspShareDocument";
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "executespnp",
                        data     : sRequest }
                    );
      var sXMLRequest = makeXMLSaveString(aRequests);
      //alert(sXMLRequest);
      AsyncXMLSave(sXMLRequest);
      
      blnSharing = true;
      
      window.setTimeout("blnSharing=false", 250);
   }
   
   function loadWarranty(){
      if (blnWarrantyLoaded == false) {
         iFrm = document.all["frmWarranty"];
         if (iFrm) 
           iFrm.src = "/ClaimVehicleWarranty.htm";
      }
   }
  function  ChoiceShopAlert()
  {
  alert("Please note this vehicle is assigned to a Choice Shop and is being processed in Hyperquest.\nPlease do not attempt to work this vehicle in APD.");
  }
  function createHQJob(iInsuranceCompanyID, strGridID) {
      var oGrid = document.getElementById(strGridID);
      if (oGrid){
         var oSelRow = oGrid.selectedRow;
         if (oSelRow && oSelRow.objXML){
            var oXML = oSelRow.objXML;
            if (oXML){
                //window.clipboardData.setData("Text", oXML.xml);
                var iDocumentID = oXML.getAttribute("DocumentID");
                var iHyperquestStatusID = oXML.getAttribute("HyperquestStatusID");
                var sDocumentSourceName = oXML.getAttribute("DocumentSourceName");
                //alert(iDocumentID);
                //alert(iInsuranceCompanyID);
                //alert(iHyperquestStatusID);
                //alert(sDocumentSourceName);

                //--------------------------------//
                // HyperquestID's:
                //        0 = Unprocessed
                //        1 = Processing
                //        2 = Transformed
                //        3 = Archived
                //        4 = Transmitted
                //        5 = Processed
                //--------------------------------//
                if (sDocumentSourceName != 'HyperQuest') { 
                  if (iHyperquestStatusID == 0) {
                       var strReq = "../Hyperquest.aspx" + "?InscCompID=" + iInsuranceCompanyID + " &DocumentID=" + iDocumentID;
                       var arrDocView = window.showModalDialog(strReq, 'True' , 'dialogHeight:95px;dialogWidth:350px;center:yes;help:no;resizable:no;scroll:no;status:no;');
                       window.location.reload();
                  } else {
                    alert('Document already processed to Hyperquest.');
                  }
                }
            }
        }
      }
  }


]]>

        </SCRIPT>

      </HEAD>
      <BODY unselectable="on" style="margin:0px;padding:0px;overflow:hidden;" tabIndex="-1" onload="pageInit()" onunload="closeChildDialogs()">
        <!-- <div style="position:absolute;top:2px;left:505px;">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col width="40px" style="font-weight:bold"/>
      <col width="85px"/>
    </colgroup>
    <tr>
      <td>Party:</td>
      <td><span class="nowrap">
        <xsl:attribute name="title"><xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID = /Root/Vehicle/@ExposureCD]/@Name"/></xsl:attribute>
        <xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID = /Root/Vehicle/@ExposureCD]/@Name"/>
        </span>
      </td>
    </tr>
  </table>
</div> -->
        <IE:APDStatus id="stat1" name="stat1" width="240" height="75" />

        <IE:APDTabGroup id="tabVehicleDetail" name="tabVehicleDetail" many="false" width="723px" height="315" CCTabIndex="102" showADS="true" preselectTab="0"  onTabBeforeDeselect="checkInvolvedDirty(event)" onTabSelect="updateSCMenuContext()" onTabBeforeSave="checkFrameLoaded()" onTabLoadComplete="preSelectVehTab()">

          <IE:APDTab id="tabDesc" name="tabDesc" caption="Description" tabPage="divVehicleDesc" CCDisabled="false" CCTabIndex="103" CRUD="_RU_" updStoredProc="uspCondVehicleDescUpdDetail|executespnpasxml" onTabValidate="" onTabAfterSave="">
            <xsl:attribute name="CRUD">
              <xsl:value-of select="$AdjVehicleCRUD"/>
            </xsl:attribute>
            <xsl:attribute name="userID">
              <xsl:value-of select="$UserId"/>
            </xsl:attribute>
            <xsl:attribute name="lastUpdatedDate">
              <xsl:value-of select="/Root/Vehicle/@SysLastUpdatedDate"/>
            </xsl:attribute>
          </IE:APDTab>

          <IE:APDTab id="tabLocation" name="tabLocation" caption="Vehicle Location" tabPage="divVehicleLocation" CCDisabled="false" CCTabIndex="103" CRUD="_RU_" updStoredProc="uspVehicleLocationUpdDetail|executespnpasxml">
            <xsl:attribute name="CRUD">
              <xsl:value-of select="$AdjVehicleCRUD"/>
            </xsl:attribute>
            <xsl:attribute name="userID">
              <xsl:value-of select="$UserId"/>
            </xsl:attribute>
            <xsl:attribute name="lastUpdatedDate">
              <xsl:value-of select="/Root/Vehicle/@SysLastUpdatedDate"/>
            </xsl:attribute>
          </IE:APDTab>

          <IE:APDTab id="tabContact" name="tabContact" caption="Contact" tabPage="divVehicleContact" onTabValidate="validateOnSubmit()" iTabIndex="validateOnSubmit()" CCDisabled="false" CCTabIndex="103" CRUD="_RU_" updStoredProc="uspVehicleContactUpdDetail|executespnpasxml">
            <xsl:attribute name="CRUD">
              <xsl:value-of select="$AdjVehicleCRUD"/>
            </xsl:attribute>
            <xsl:attribute name="userID">
              <xsl:value-of select="$UserId"/>
            </xsl:attribute>
            <xsl:attribute name="lastUpdatedDate">
              <xsl:value-of select="/Root/Vehicle/Contact/@SysLastUpdatedDate"/>
            </xsl:attribute>
          </IE:APDTab>

          <IE:APDTab id="tabInvolved" name="tabInvolved" caption="Involved" tabPage="divVehicleInvolved" CCDisabled="false" CCTabIndex="103" onTabSelect="selectInvolved()" onTabAdd="addInvolved()" onTabSave="saveInvolved()" onTabAfterSave="afterSaveInvolved()" updStoredProc="null|null" onTabDelete="delInvolved()">
            <xsl:attribute name="CRUD">
              <xsl:value-of select="$AdjInvolvedCRUD"/>
            </xsl:attribute>
            <xsl:attribute name="userID">
              <xsl:value-of select="$UserId"/>
            </xsl:attribute>
          </IE:APDTab>

          <xsl:if test="Vehicle/@WarrantyExistsFlag = '1'">
            <IE:APDTab id="tabWarranty" name="tabWarranty" caption="Warranty" tabPage="divVehicleWarranty" CCDisabled="false" CCTabIndex="103" onTabSelect="loadWarranty()" >
              <xsl:attribute name="CRUD">_R__</xsl:attribute>
              <xsl:attribute name="userID">
                <xsl:value-of select="$UserId"/>
              </xsl:attribute>
            </IE:APDTab>
          </xsl:if>

          <xsl:for-each select="Vehicle/ClaimAspectServiceChannel">
            <xsl:sort select="@PrimaryFlag" data-type="number" order="descending"/>
            <xsl:sort select="@ServiceChannelName" data-type="text" order="ascending"/>
            <xsl:variable name="tabID">
              <xsl:value-of select="concat('tabSC_', @ClaimAspectServiceChannelID)"/>
            </xsl:variable>
            <xsl:variable name="pageID">
              <xsl:value-of select="concat('divSC_', @ClaimAspectServiceChannelID)"/>
            </xsl:variable>
            <IE:APDTab CCDisabled="false" CCTabIndex="103" >
              <xsl:attribute name="id">
                <xsl:value-of select="$tabID"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="$tabID"/>
              </xsl:attribute>
              <xsl:attribute name="caption">
                <xsl:if test="@PrimaryFlag = '1'">
                  <xsl:value-of select="concat('*', '  ')"/>
                </xsl:if>
                <xsl:value-of select="@ServiceChannelName"/>
              </xsl:attribute>
              <xsl:attribute name="tabPage">
                <xsl:value-of select="$pageID"/>
              </xsl:attribute>
              <xsl:attribute name="onTabSelect">
                initSC(<xsl:value-of select="@ClaimAspectServiceChannelID"/>)
              </xsl:attribute>
              <xsl:attribute name="imgSrc">
                <xsl:choose>
                  <xsl:when test="@StatusName = 'Complete'">/images/closedExposure.gif</xsl:when>
                  <xsl:when test="@StatusName = 'Cancelled'">/images/Cancelled.gif</xsl:when>
                  <xsl:when test="@StatusName = 'Voided'">/images/Voided.gif</xsl:when>
                </xsl:choose>
              </xsl:attribute>
            </IE:APDTab>
          </xsl:for-each>

          <IE:APDTabPage name="divVehicleDesc" id="divVehicleDesc" style="display:none">
            <xsl:call-template name="VehicleDesc">
              <xsl:with-param name="VehicleCRUD" select="$AdjVehicleCRUD"/>
              <xsl:with-param name="ServiceChannelCRUD" select="$ServiceChannelCRUD"/>
            </xsl:call-template>
          </IE:APDTabPage>

          <IE:APDTabPage name="divVehicleLocation" id="divVehicleLocation" style="display:none">
            <xsl:call-template name="VehicleLocation">
              <xsl:with-param name="VehicleCRUD" select="$AdjVehicleCRUD"/>
            </xsl:call-template>
          </IE:APDTabPage>

          <IE:APDTabPage name="divVehicleContact" id="divVehicleContact" style="display:none">
            <xsl:call-template name="VehicleContact">
              <xsl:with-param name="VehicleCRUD" select="$AdjVehicleCRUD"/>
            </xsl:call-template>
          </IE:APDTabPage>

          <IE:APDTabPage name="divVehicleInvolved" id="divVehicleInvolved" style="display:none">
            <xsl:call-template name="VehicleInvolved">
              <xsl:with-param name="InvolvedCRUD" select="$AdjInvolvedCRUD"/>
            </xsl:call-template>
          </IE:APDTabPage>

          <IE:APDTabPage name="divVehicleWarranty" id="divVehicleWarranty" style="display:none">
            <xsl:call-template name="VehicleWarranty"/>
          </IE:APDTabPage>

          <xsl:for-each select="Vehicle/ClaimAspectServiceChannel">
            <xsl:variable name="pageID">
              <xsl:value-of select="concat('divSC_', @ClaimAspectServiceChannelID)"/>
            </xsl:variable>
            <IE:APDTabPage style="display:none">
              <xsl:attribute name="id">
                <xsl:value-of select="$pageID"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="$pageID"/>
              </xsl:attribute>
              <xsl:call-template name="ServiceChannelDetails">
                <xsl:with-param name="RootDir">
                  <xsl:value-of select="$ImageRootDir"/>
                </xsl:with-param>
                <xsl:with-param name="RootDirArchive">
                  <xsl:value-of select="$ImageRootArchiveDir"/>
                </xsl:with-param>
                <xsl:with-param name="VehicleCRUD" select="$AdjVehicleCRUD"/>
                <xsl:with-param name="UserId" select="$UserId"/>
              </xsl:call-template>
            </IE:APDTabPage>
          </xsl:for-each>
        </IE:APDTabGroup>

        <input type="hidden" name="LocationName" id="LocationName">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Vehicle/@LocationName"/>
          </xsl:attribute>
        </input>

        <xml id="xmlReference">
          <Root>
            <xsl:copy-of select="/Root/Reference[@List='Impact']"/>
          </Root>
        </xml>
        <xml id="xmlInvolvedType">
          <Root>
            <xsl:copy-of select="/Root/Reference[@List='InvolvedType']"/>
          </Root>
        </xml>
        <xml id="xmlClientServiceChannels" ondatasetcomplete="initClientServiceChannels()">
          <Root>
            <xsl:copy-of select="/Root/Reference[@List='ClientServiceChannels']"/>
          </Root>
        </xml>
        <xml id="xmlExistingServiceChannels">
          <Root>
            <xsl:copy-of select="/Root/Vehicle/ClaimAspectServiceChannel"/>
          </Root>
        </xml>

        <xsl:for-each select="Vehicle/ClaimAspectServiceChannel">
          <xsl:variable name="pageID">
            <xsl:value-of select="concat('divSC_', @ClaimAspectServiceChannelID)"/>
          </xsl:variable>
          <xsl:variable name="curServiceChannelCD">
            <xsl:value-of select="@ServiceChannelCD"/>
          </xsl:variable>
          <!-- Estimate XML for the service channel -->
          <xml>
            <xsl:attribute name="id">
              <xsl:value-of select="concat('xmlSCEst_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <xsl:attribute name="name">
              <xsl:value-of select="concat('xmlSCEst_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <Root>
              <xsl:for-each select="/Root/Vehicle/Document[(@ServiceChannelCD = $curServiceChannelCD) and (@EstimateTypeFlag = '1')]">
                <xsl:sort select="@CreatedDate" data-type="text" order="descending"/>
                <Document>
                  <xsl:copy-of select="@*"/>
                  <xsl:attribute name="ReceivedDateFormatted">
                    <xsl:value-of select="js:formatSQLDateTime(string(@ReceivedDate))"/>
                  </xsl:attribute>
                  <xsl:variable name="DocumentType">
                    <xsl:choose>
                      <xsl:when test="number(@SupplementSeqNumber) = 0 or @SupplementSeqNumber = ''">E</xsl:when>
                      <xsl:otherwise>S</xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:variable name="SuppSeqNo">
                    <xsl:choose>
                      <xsl:when test="number(@SupplementSeqNumber) = 0 or @SupplementSeqNumber = ''">01</xsl:when>
                      <xsl:when test="number(@SupplementSeqNumber) &lt; 10">
                        <xsl:value-of select="concat('0', @SupplementSeqNumber)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="@SupplementSeqNumber"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:attribute name="SequenceNum">
                    <xsl:value-of select="concat($DocumentType, $SuppSeqNo)"/>
                  </xsl:attribute>
                  <xsl:attribute name="OAIndicator">
                    <xsl:choose>
                      <xsl:when test="@EstimateTypeCD = 'O'">Original</xsl:when>
                      <xsl:when test="@EstimateTypeCD = 'A'">Audited</xsl:when>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:attribute name="DupIndicator">
                    <xsl:choose>
                      <xsl:when test="@DuplicateFlag = '0'">No</xsl:when>
                      <xsl:when test="@DuplicateFlag = '1'">Yes</xsl:when>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:attribute name="PriceAgreedIndicator">
                    <xsl:choose>
                      <xsl:when test="@AgreedPriceMetCD = 'Y'">Yes</xsl:when>
                      <xsl:when test="@AgreedPriceMetCD = 'N'">No</xsl:when>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:attribute name="SummaryIndicatorImg">
                    <xsl:choose>
                      <xsl:when test="@FullSummaryExistsFlag = '1'">/images/ElectronicEst.gif</xsl:when>
                      <xsl:when test="@FullSummaryExistsFlag = '0'">/images/ManualEst.gif</xsl:when>
                    </xsl:choose>
                  </xsl:attribute>

                  <xsl:attribute name="HyperquestImg">
                    <xsl:choose>
                      <xsl:when test="@DocumentSourceName != 'HyperQuest' and @ServiceChannelCD = 'DA'">
                        <xsl:choose>
                          <xsl:when test="@HyperquestStatus = ''">/images/HQNew.png</xsl:when>
                          <xsl:when test="@HyperquestStatus = 'PROCESSED'">/images/HQRecv.png</xsl:when>
                          <xsl:when test="@HyperquestStatus = 'UNPROCESSED'">/images/HQNew.png</xsl:when>
                          <xsl:when test="@HyperquestStatus = 'CANCELLED'">/images/HQCan.png</xsl:when>
                          <xsl:when test="@HyperquestStatus = 'PROCESSING'">/images/HQTrans.png</xsl:when>
                          <xsl:otherwise>
                            /images/HQNew.png
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        /images/HQBlank.png
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>

                  <xsl:attribute name="DocumentID">
                    <xsl:value-of select="@DocumentID"/>
                  </xsl:attribute>
                  <xsl:attribute name="DocumentSourceName">
                    <xsl:value-of select="@DocumentSourceName"/>
                  </xsl:attribute>
                </Document>
              </xsl:for-each>
            </Root>
          </xml>

          <xml>
            <xsl:attribute name="id">
              <xsl:value-of select="concat('xmlSCConcession_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <xsl:attribute name="name">
              <xsl:value-of select="concat('xmlSCConcession_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <Root>
              <xsl:for-each select="/Root/Vehicle/ClaimAspectServiceChannel[@ServiceChannelCD = $curServiceChannelCD]/Concession">
                <xsl:sort select="@CreatedDate" data-type="text" order="descending"/>
                <xsl:text disable-output-escaping="yes">&lt;Concession </xsl:text>
                <xsl:apply-templates select="@*"/> CreatedDateFormatted="<xsl:value-of select="js:formatSQLDateTime(string(@CreatedDate))"/>" /<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
              </xsl:for-each>
            </Root>
          </xml>

          <!-- Document XML for the service channel -->
          <xml>
            <xsl:attribute name="id">
              <xsl:value-of select="concat('xmlSCDocs_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <xsl:attribute name="name">
              <xsl:value-of select="concat('xmlSCDocs_', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <Root>
              <xsl:copy-of select="/Root/Vehicle/Document[(@ServiceChannelCD = $curServiceChannelCD) and (@EstimateTypeFlag = '0')]"/>
            </Root>
          </xml>
        </xsl:for-each>

      </BODY>
    </HTML>
  </xsl:template>

  <xsl:template name="VehicleDesc">
    <xsl:param name="VehicleCRUD"/>
    <xsl:param name="ServiceChannelCRUD"/>
    <xsl:variable name="dataDisabled">
      <xsl:choose>
        <xsl:when test="contains($VehicleCRUD, 'U') = true()">false</xsl:when>
        <xsl:otherwise>true</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <IE:APDInput id="ClaimAspectID" name="ClaimAspectID" canDirty="false" style="display:none">
      <xsl:attribute name="value">
        <xsl:value-of select="/Root/Vehicle/@ClaimAspectID"/>
      </xsl:attribute>
    </IE:APDInput>
    <xsl:variable name="AssignmentDisabled">
      <xsl:value-of select="$dataDisabled = 'true' or /Root/Vehicle/@ActiveReinspection = '1'"/>
    </xsl:variable>
    <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;table-layout:fixed" unselectable="on">
      <colgroup>
        <col width="235px"/>
        <col width="15px"/>
        <col width="*"/>
      </colgroup>
      <tr valign="top" unselectable="on">
        <td unselectable="on">
          <table cellpadding="1" cellspacing="0" border="0" style="border-collapse:collapse;table-layout:fixed" unselectable="on">
            <colgroup>
              <col width="100px"/>
              <col width="150px"/>
            </colgroup>
            <tr unselectable="on">
              <td unselectable="on">Year:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputNumeric('VehicleYear', /Root/Vehicle, 'VehicleYear', 'false', '', string($dataDisabled), 104, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Make:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Make', /Root/Vehicle, 'Make', '125', string($dataDisabled), 105, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Model:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Model', /Root/Vehicle, 'Model', '125', string($dataDisabled), 106, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Body Style:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BodyStyle', /Root/Vehicle, 'BodyStyle', '125', string($dataDisabled), 107, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Color:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Color', /Root/Vehicle, 'Color', '125', string($dataDisabled), 108, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">VIN:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('VIN', /Root/Vehicle, 'VIN', '125', string($dataDisabled), 109, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">
                VIN (Est):
                <xsl:if test="/Root/Vehicle/@EstimateVIN != '' and /Root/Vehicle/@VIN != /Root/Vehicle/@EstimateVIN">
                  <img align="absbottom" src="/images/edit.gif" alt="Update VIN" width="14" height="14" border="0" onClick="updateVIN()" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" >
                    <xsl:if test="$dataDisabled = 'true'">
                      <xsl:attribute name="disabled"/>
                    </xsl:if>
                  </img>
                </xsl:if>
              </td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('EstimateVIN', /Root/Vehicle, 'EstimateVIN', '125', string($dataDisabled), 109, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">License Plate:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LicensePlateNumber', /Root/Vehicle, 'LicensePlateNumber', '110', string($dataDisabled), 110, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">License Plate State:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('LicensePlateState', /Root/Vehicle, 'LicensePlateState', 'State', 'true', '6', string($dataDisabled), 111, 'true', '', '131', '', 'true', '')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Odometer Reading:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputNumeric('Mileage', /Root/Vehicle, 'Mileage', 'false', '', string($dataDisabled), 112, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Book Value:</td>
              <td unselectable="on">
                <table border="0" cellspacing="0" cellpadding="0" unselectable="on">
                  <tr unselectable="on">
                    <td unselectable="on">
                      <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputCurrency('BookValueAmt', /Root/Vehicle, 'BookValueAmt', '90', string($dataDisabled), 113, 'true')"/>
                    </td>
                    <td unselectable="on">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </td>
                    <td unselectable="on">
                      <xsl:if test="boolean(contains($VehicleCRUD,'R'))">
                        <img valign="bottom" src="/images/nada1.gif"  onClick="callNada()" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" >
                          <xsl:if test="$dataDisabled = 'true'">
                            <xsl:attribute name="disabled"/>
                          </xsl:if>
                        </img>
                      </xsl:if>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">Rental Days Authorized:</td>
              <td unselectable="on">
				  <table cellspacing="0" cellpadding="0" border="0">
					  <tr>
						  <td>
							  <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputNumeric('RentalDaysAuthorized', /Root/Vehicle, 'RentalDaysAuthorized', 'false', '', string($dataDisabled), 115, 'true')"/>
						  </td>
						  <td>
							  <IE:APDButton id="btnRental" name="btnRental" class="APDButton" value="Rental" CCDisabled="false" onButtonClick="callRental()"/>
						  </td>
					  </tr>
				  </table>
			  </td>
			
	
            </tr>
            <tr valign="top" unselectable="on">
              <td unselectable="on">Rental Instructions:</td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('RentalInstructions', /Root/Vehicle, 'RentalInstructions', 35, '130', string($dataDisabled), 116, 'true')"/>
              </td>
            </tr>
          </table>
        </td>
        <td/>
        <td>
          <table cellpadding="1" cellspacing="0" border="0" style="border-collapse:collapse;table-layout:fixed" unselectable="on">
            <colgroup>
              <col width="155px"/>
              <col width="30px"/>
              <col width="155px"/>
              <col width="30px"/>
              <col width="*"/>
            </colgroup>
            <tr unselectable="on">
              <td colspan="2" unselectable="on">Impact Area:</td>
              <td colspan="2" unselectable="on">Prior Damage Area:</td>
              <td  unselectable="on">
                <span style="font-weight:bold">Party:</span>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID = /Root/Vehicle/@ExposureCD]/@Name"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td unselectable="on">
                <xsl:variable name="PrimaryImpactID">
                  <xsl:value-of select="/Root/Vehicle/Impact[@ImpactID != '0' and @CurrentImpactFlag='1' and @PrimaryImpactFlag='1']/@ImpactID"/>
                </xsl:variable>
                <xsl:variable name="ImpactArea">
                  <xsl:if test="$PrimaryImpactID != ''">
                    <xsl:value-of select="concat(string(/Root/Reference[@List='Impact' and @ReferenceID=$PrimaryImpactID]/@Name), ' (Primary), ')"/>
                  </xsl:if>
                  <xsl:for-each select="/Root/Vehicle/Impact[@ImpactID != '0' and @CurrentImpactFlag='1' and @PrimaryImpactFlag='0']">
                    <xsl:variable name="CurrentImpactID">
                      <xsl:value-of select="@ImpactID"/>
                    </xsl:variable><xsl:value-of select="/Root/Reference[@List='Impact' and @ReferenceID=$CurrentImpactID]/@Name"/>,
                  </xsl:for-each>
                </xsl:variable>
                <xsl:value-of disable-output-escaping="yes" select="js:APDTextArea('ImpactArea', substring($ImpactArea, 0, string-length($ImpactArea) - 1), '', 35, 150, 'true', string($dataDisabled), -1, 'true')"/>
                <IE:APDInput id="ImpactPoints" name="ImpactPoints" canDirty="false" style="display:none">
                  <xsl:attribute name="value">
                    <xsl:value-of select="/Root/Vehicle/Impact[@CurrentImpactFlag='1' and @PrimaryImpactFlag='1']/@ImpactID"/>,<xsl:for-each select="/Root/Vehicle/Impact[@CurrentImpactFlag='1' and @PrimaryImpactFlag='0']">
                      <xsl:value-of select="@ImpactID"/>
                      <xsl:if test="position() != last()">,</xsl:if>
                    </xsl:for-each>
                  </xsl:attribute>
                </IE:APDInput>
              </td>
              <td unselectable="on">
                <xsl:if test="boolean(contains($VehicleCRUD,'U'))">
                  <IMG src="/images/next_button.gif" onClick="ShowImpact('New Damage')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="117">
                    <xsl:if test="$dataDisabled = 'true'">
                      <xsl:attribute name="disabled"/>
                    </xsl:if>
                  </IMG>
                </xsl:if>
              </td>
              <td unselectable="on">
                <xsl:variable name="PriorImpactArea">
                  <xsl:for-each select="/Root/Vehicle/Impact[@ImpactID != '0' and @PriorImpactFlag='1']">
                    <xsl:variable name="PriorImpactID">
                      <xsl:value-of select="@ImpactID"/>
                    </xsl:variable><xsl:value-of select="/Root/Reference[@List='Impact' and @ReferenceID=$PriorImpactID]/@Name"/>,
                  </xsl:for-each>
                </xsl:variable>
                <xsl:value-of disable-output-escaping="yes" select="js:APDTextArea('PriorImpactArea', string($PriorImpactArea), '', 35, 150, 'true', string($dataDisabled), -1, 'true')"/>
                <IE:APDInput id="PriorImpactPoints" name="PriorImpactPoints" canDirty="false" style="display:none">
                  <xsl:attribute name="value">
                    <xsl:for-each select="/Root/Vehicle/Impact[@PriorImpactFlag='1']">
                      <xsl:value-of select="@ImpactID"/>
                      <xsl:if test="position() != last()">,</xsl:if>
                    </xsl:for-each>
                  </xsl:attribute>
                </IE:APDInput>
              </td>
              <td unselectable="on">
                <xsl:if test="boolean(contains($VehicleCRUD,'U'))">
                  <IMG src="/images/next_button.gif" onClick="ShowImpact('Previous Damage')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="118">
                    <xsl:if test="$dataDisabled = 'true'">
                      <xsl:attribute name="disabled"/>
                    </xsl:if>
                  </IMG>
                </xsl:if>
              </td>
              <td unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCheckBox('DriveableFlag', 'Drivable', /Root/Vehicle, 'DriveableFlag', '', string($dataDisabled), 119, 'true', 1, 'true')"/>
              </td>
            </tr>
            <tr unselectable="on">
              <td colspan="5">
                <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
                  <colgroup>
                    <col width="130px"/>
                    <col width="135px"/>
                    <col width="30px"/>
                    <col width="60px"/>
                    <col width="105px"/>
                  </colgroup>
                  <tr>
                    <td>Initial Assignment Type:</td>
                    <td>
                      <IE:APDLabel id="InitialAssignmentType" name="InitialAssignmentType" width="195" CCDisabled="false" >
                        <xsl:variable name="CurrentAssignmentTypeID">
                          <xsl:value-of select="/Root/Vehicle/@InitialAssignmentTypeID"/>
                        </xsl:variable>
                        <xsl:attribute name="value">
                          <xsl:value-of select="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $CurrentAssignmentTypeID]/@Name"/>
                        </xsl:attribute>
                      </IE:APDLabel>
                    </td>
                    <td>
                    </td>
                    <td>Coverage:</td>
                    <td>
                      <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('CoverageProfileCD', /Root/Vehicle, 'CoverageProfileCD', 'CoverageProfile', 'false', '', string($dataDisabled), 120, 'true', '', 100, '', 'true', '')"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr unselectable="on">
              <td colspan="3" unselectable="on">Service Channel Summary:</td>
              <td colspan="2" unselectable="on">
                <IE:APDButton id="btnAddServiceChannel" name="btnAddServiceChannel" value="New Channel" width="90" CCTabIndex="120" onButtonClick="addServiceChannel()">
                  <xsl:attribute name="CCDisabled">
                    <xsl:choose>
                      <xsl:when test="(boolean(contains($ServiceChannelCRUD, 'C')) = true()) and (/Root/Vehicle/@Status = 'Open')">false</xsl:when>
                      <xsl:otherwise>true</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                </IE:APDButton>
              </td>
            </tr>
            <tr unselectable="on">
              <td colspan="5" unselectable="on">
                <table cellpadding="1" cellspacing="0" border="1" style="border-collapse:collapse;table-layout:fixed">
                  <colgroup>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="70px"/>
                    <col width="45px"/>
                    <col width="55px"/>
                    <col width="87px"/>
                  </colgroup>
                  <tr unselectable="on">
                    <td class="Header" rowspan="2" unselectable="on">Channel</td>
                    <td class="Header" rowspan="2" unselectable="on">Status</td>
                    <td class="Header" rowspan="2" unselectable="on">Est. Amt.</td>
                    <td class="Header" colspan="3" unselectable="on">Monies Applied</td>
                  </tr>
                  <tr>
                    <td class="Header" unselectable="on">Cov.</td>
                    <td class="Header" unselectable="on">Ded.</td>
                    <td class="Header" unselectable="on">Overage/Lmt.</td>
                  </tr>
                </table>
                <div style="100%;height:93px;overflow:auto;border:1px solid #C0C0C0">
                  <table cellpadding="2" cellspacing="0" border="1" style="border-collapse:collapse;table-layout:fixed" unselectable="on">
                    <colgroup>
                      <col width="99px"/>
                      <col width="100px"/>
                      <col width="70px" style="text-align:right"/>
                      <col width="165px"/>
                    </colgroup>
                    <xsl:for-each select="/Root/Vehicle/ClaimAspectServiceChannel">
                      <xsl:sort select="@PrimaryFlag" data-type="number" order="descending"/>
                      <xsl:sort select="@ServiceChannelCD" data-type="text" order="ascending"/>
                      <xsl:variable name="ServiceChannelCD">
                        <xsl:value-of select="@ServiceChannelCD"/>
                      </xsl:variable>
                      <xsl:variable name="DispositionTypeCD">
                        <xsl:value-of select="@DispositionTypeCD"/>
                      </xsl:variable>
                      <tr style1="height:21px;" unselectable="on">
                        <td unselectable="on">
                          <xsl:value-of select="/Root/Reference[@List='ServiceChannelCD' and @ReferenceID=$ServiceChannelCD]/@Name"/>
                        </td>
                        <td unselectable="on">
                          <!-- <xsl:choose>
                           <xsl:when test="@StatusName != ''"><xsl:value-of select="@StatusName"/></xsl:when>
                           <xsl:otherwise>
                              <xsl:value-of select="/Root/Reference[@List='DispositionTypeCD' and @ReferenceID=$DispositionTypeCD]/@Name"/>
                           </xsl:otherwise>
                        </xsl:choose> -->
                          <xsl:value-of select="@StatusName"/>
                          <!--<xsl:if test="@StatusID &gt;= 900">
                           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                           <xsl:value-of select="/Root/Reference[@List='DispositionTypeCD' and @ReferenceID=$DispositionTypeCD]/@Name"/>
                        </xsl:if> -->
                        </td>
                        <td unselectable="on">
                          <xsl:if test="@CurEstGrossRepairTotal != ''">
                            <xsl:value-of select="concat('$ ', @CurEstGrossRepairTotal)"/>
                          </xsl:if>
                        </td>
                        <td unselectable="on">
                          <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed;border-collapse:collapse">
                            <colgroup>
                              <col width="46px"/>
                              <col width="54px"/>
                              <col width="58px"/>
                            </colgroup>
                            <xsl:for-each select="Coverage[@DeductibleAppliedAmt != '' or @LimitAppliedAmt != '']">
                              <xsl:sort select="@ClientCode" data-type="text" order="ascending"/>
                              <xsl:sort select="@CoverageTypeCD" data-type="text" order="ascending"/>
                              <tr>
                                <td>
                                  <xsl:choose>
                                    <xsl:when test="@ClientCode != ''">
                                      <xsl:value-of select="@ClientCode"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="@CoverageTypeCD"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </td>
                                <td>
                                  <xsl:value-of select="concat(@DeductibleAppliedAmt, ' / ', @DeductibleAmt)"/>
                                </td>
                                <td>
                                  <xsl:value-of select="concat(@LimitAppliedAmt, ' / ', @LimitAmt)"/>
                                  <!-- <xsl:if test="@LimitAppliedAmt != '' and number(@LimitAppliedAmt) &gt; 0">
                                       <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                                       <xsl:value-of select="concat(' ', @LimitAppliedAmt)"/>
                                    </xsl:if> -->
                                </td>
                              </tr>
                            </xsl:for-each>
                          </table>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>
              </td>
            </tr>
            <tr unselectable="on">
              <td colspan="5" unselectable="on">Vehicle Remarks:</td>
            </tr>
            <tr unselectable="on">
              <td colspan="5" unselectable="on">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('Remarks', /Root/Vehicle, 'Remarks', 35, '456', string($dataDisabled), 121, 'true')"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="VehicleLocation">
    <xsl:param name="VehicleCRUD"/>
    <xsl:variable name="dataDisabled">
      <xsl:choose>
        <xsl:when test="contains($VehicleCRUD, 'U') = true()">false</xsl:when>
        <xsl:otherwise>true</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <IE:APDInput id="ClaimAspectID" name="ClaimAspectID" canDirty="false" style="display:none">
      <xsl:attribute name="value">
        <xsl:value-of select="/Root/Vehicle/@ClaimAspectID"/>
      </xsl:attribute>
    </IE:APDInput>
    <table cellpadding="1" cellspacing="0" border="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="45px"/>
        <col width="230px"/>
        <col width="105px"/>
        <col width="140px"/>
        <col width="35px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td>Location:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LocationName',/Root/Vehicle,'LocationName','215',string($dataDisabled), 120, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>Address:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LocationAddress1',/Root/Vehicle,'LocationAddress1','215',string($dataDisabled), 121, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LocationAddress2',/Root/Vehicle,'LocationAddress2','215',string($dataDisabled), 122, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>City:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LocationCity',/Root/Vehicle,'LocationCity','150',string($dataDisabled), 123, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>State:</td>
        <td>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('LocationState', /Root/Vehicle, 'LocationState', 'State', 'true', '6', string($dataDisabled), 124, 'true', '', '125', '', 'true', '')"/>
              </td>
              <td style="padding:0px;padding-left:10px">Zip:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('LocationZip', /Root/Vehicle, 'LocationZip', '50', string($dataDisabled), 125, 'true', 'true', 'ValidateZip(LocationZip, LocationCity, LocationState)')"/>
              </td>
            </tr>
          </table>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>Phone:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Location', /Root/Vehicle, 'LocationAreaCode', 'LocationExchangeNumber', 'LocationUnitNumber', 'LocationExtensionNumber', '', 'true', string($dataDisabled), 126, 'true', 'false')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="VehicleContact">
    <xsl:param name="VehicleCRUD"/>
    <xsl:variable name="dataDisabled">
      <xsl:choose>
        <xsl:when test="contains($VehicleCRUD, 'U') = true()">false</xsl:when>
        <xsl:otherwise>true</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <IE:APDInput id="ClaimAspectID" name="ClaimAspectID" canDirty="false" style="display:none">
      <xsl:attribute name="value">
        <xsl:value-of select="/Root/Vehicle/@ClaimAspectID"/>
      </xsl:attribute>
    </IE:APDInput>
    <table cellpadding="1" cellspacing="0" border="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="45px"/>
        <col width="230px"/>
        <col width="105px"/>
        <col width="140px"/>
        <col width="35px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td>Name:</td>
        <td>
          <table border="0" cellpadding="0" cellspacing="0" style="padding-right:1px;">
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameTitle', /Root/Vehicle/Contact, 'NameTitle', '25', string($dataDisabled), 105, 'true')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameFirst', /Root/Vehicle/Contact, 'NameFirst', '87', string($dataDisabled), 106, 'true')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameLast', /Root/Vehicle/Contact, 'NameLast', '89', string($dataDisabled), 107, 'true')"/>
            </td>
          </table>
        </td>
        <td>Relation to Ins.:</td>
        <td colspan="3">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('InsuredRelationID', /Root/Vehicle/Contact, 'InsuredRelationID', 'ContactRelationToInsured', 'true', '6', string($dataDisabled), 114, 'true', '', 200, '', 'true')"/>
        </td>
      </tr>
      <tr>
        <td>Address:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address1', /Root/Vehicle/Contact, 'Address1', '215', string($dataDisabled), 108, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address2', /Root/Vehicle/Contact, 'Address2', '215', string($dataDisabled), 109, 'true')"/>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>City:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressCity', /Root/Vehicle/Contact, 'AddressCity', '150', string($dataDisabled), 110, 'true')"/>
        </td>
        <td></td>
        <td>

        </td>
        <td>Day:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Day', /Root/Vehicle/Contact, 'DayAreaCode', 'DayExchangeNumber', 'DayUnitNumber', 'DayExtensionNumber', '', 'true', string($dataDisabled), 117, 'true', 'false')"/>
        </td>
      </tr>
      <tr>
        <td>State:</td>
        <td>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('AddressState', /Root/Vehicle/Contact, 'AddressState', 'State', 'true', '6', string($dataDisabled), 111, 'true', '', '125', '', 'true', '')"/>
              </td>
              <td style="padding:0px;padding-left:10px">Zip:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressZip', /Root/Vehicle/Contact, 'AddressZip', '50', string($dataDisabled), 112, 'true', 'true', 'ValidateZip(AddressZip, AddressCity, AddressState)')"/>
              </td>
            </tr>
          </table>
        </td>
        <td>Best Number to Call:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('BestContactPhoneCD', /Root/Vehicle/Contact, 'BestContactPhoneCD', 'BestContactPhone', 'true', '6', string($dataDisabled), 115, 'true', '', '125', '', 'true')"/>
        </td>
        <td>Night:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Night', /Root/Vehicle/Contact, 'NightAreaCode', 'NightExchangeNumber', 'NightUnitNumber', 'NightExtensionNumber', '', 'true', string($dataDisabled), 118, 'true', 'false')"/>
        </td>
      </tr>
      <tr>
        <td>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>E-mail:</td>
              <td>
                <span id="lblEmailReqiured" style="font-weight: normal; font-size: 11pt;display:none;">*</span>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('EmailAddress', /Root/Vehicle/Contact, 'EmailAddress', '215', string($dataDisabled), 113, 'true', 'true', 'checkEMail()')"/>
        </td>
        <td>Best Time to Call:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BestContactTime', /Root/Vehicle/Contact, 'BestContactTime', '120', string($dataDisabled), 116, 'true')"/>
        </td>
        <td>Alt.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Alternate', /Root/Vehicle/Contact, 'AlternateAreaCode', 'AlternateExchangeNumber', 'AlternateUnitNumber', 'AlternateExtensionNumber', '', 'true', string($dataDisabled), 119, 'true', 'false', 'true', '')"/>
        </td>
      </tr>
      <xsl:if test="/Root/Vehicle/ClaimAspectServiceChannel[@ServiceChannelCD='PS'] or /Root/Vehicle/ClaimAspectServiceChannel[@ServiceChannelCD='RRP']">
        <tr>

          <td colspan="2">
            <xsl:if test="$IsCellEmailFlag='true'">
              Preferred method of status updates:
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('PrefMethodUpd', /Root/Vehicle/Contact, 'PrefMethodUpd', 'PrefMethodUpd', 'false', '6', string($dataDisabled), 115, 'true', '', '90', '', 'true','validateOnSubmit()')"/>
            </xsl:if>
          </td>
          <td>
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <xsl:if test="$IsCellEmailFlag='true'">
                    Cell phone carrier:
                  </xsl:if>
                </td>
                <td>
                  <xsl:if test="$IsCellEmailFlag='true'">
                    <span id="lblCellCarrierReqiured" style="font-weight: normal; font-size: 11pt;display:none;">*</span>
                  </xsl:if>
                </td>
              </tr>
            </table>
          </td>
          <td>
            <xsl:if test="$IsCellEmailFlag='true'">
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('CellPhoneCarrier', /Root/Vehicle/Contact, 'CellPhoneCarrier', 'CellPhoneCarrier', 'true', '6', string($dataDisabled), 115, 'true', '', '125', '', 'false')"/>
            </xsl:if>
          </td>

          <td>
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  Cell:
                </td>
                <td>
                  <span id="lblCellReqiured" style="font-weight: normal; font-size: 11pt;display:none;">*</span>
                </td>
              </tr>
            </table>
          </td>
          <td>
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Cell', /Root/Vehicle/Contact, 'CellAreaCode', 'CellExchangeNumber', 'CellUnitNumber', 'Null', '', 'false', string($dataDisabled), 119, 'true', 'false', 'true', '')"/>
          </td>
        </tr>

      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template name="VehicleInvolved">
    <xsl:param name="InvolvedCRUD"/>
    <div style="width=100%;height=277px;;border=1px solid #000000;margin-top:12px;padding:0px;padding-left:5px;padding-top:20px;">
      <IFRAME name="frmInvolved" id="frmInvolved" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background1:transparent;" allowtransparency1="true"/>
    </div>
    <IE:APDCustomSelect id="InvolvedList" name="InvolvedList" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="105" width="300" style="position:absolute;top:5px;left:20px;" onchange="getInvolvedDetail()">
      <xsl:for-each select="/Root/Vehicle/Involved[@InvolvedID != '0']">
        <xsl:variable name="InvolvedTypes">
          <xsl:for-each select="InvolvedType">
            <xsl:value-of select="@InvolvedTypeName"/>,
          </xsl:for-each>
        </xsl:variable>
        <IE:dropDownItem style="display:none">
          <xsl:attribute name="value">
            <xsl:value-of select="@InvolvedID"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@DayAreaCode"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@DayExchangeNumber"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@AddressCity"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@AddressState"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@AddressZip"/>
            <xsl:if test="not(position()=last())">
              <xsl:text>|</xsl:text>
            </xsl:if>
          </xsl:attribute>
          <xsl:value-of select="concat(string(@NameFirst), ' ', string(@NameLast), ' [', substring($InvolvedTypes, 0, string-length($InvolvedTypes)-1), ']')"/>
        </IE:dropDownItem>
      </xsl:for-each>
      <xsl:if test="contains($InvolvedCRUD, 'C') = true()">
        <IE:dropDownItem value="0,,,,," style="display:none">-- Add New --</IE:dropDownItem>
      </xsl:if>
    </IE:APDCustomSelect>
  </xsl:template>

  <xsl:template name="VehicleWarranty">
    <div style="width:100%;height:284px;padding:0px;">
      <IFRAME name="frmWarranty" id="frmWarranty" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background1:transparent;" allowtransparency1="true"/>
    </div>
  </xsl:template>

  <xsl:template name="ServiceChannelDetails">
    <xsl:param name="RootDir"/>
    <xsl:param name="RootDirArchive"/>
    <xsl:param name="VehicleCRUD"/>
    <xsl:param name="UserId"/>
    <xsl:variable name="curServiceChannelCD">
      <xsl:value-of select="@ServiceChannelCD"/>
    </xsl:variable>
    <div id="divGlsACGRefNo" name="divGlsACGRefNo" style="position:absolute;top:1px;left:340px;width:300px;font: 11px Tahoma, Arial, Verdana;">
      <table border="0" cellspacing="0" cellpadding="2" style="border-collapse:collapse">
        <colgroup>
          <col width="125px"/>
          <col width="110px" style="font-weight:bold"/>
          <col width="*"/>
        </colgroup>
        <tr>
          <td>
            <xsl:if test="@PrimaryFlag != '1'">
              <IE:APDButton value="Make Primary" width="100" CCDisabled="false" CCTabIndex="600">
                <xsl:attribute name="id">
                  <xsl:value-of select="concat('btnMakePrimary', @ClaimAspectServiceChannelID)"/>
                </xsl:attribute>
                <xsl:attribute name="name">
                  <xsl:value-of select="concat('btnMakePrimary', @ClaimAspectServiceChannelID)"/>
                </xsl:attribute>
                <xsl:attribute name="onButtonClick">
                  makePrimary(this, <xsl:value-of select="@ClaimAspectServiceChannelID"/>, '<xsl:value-of select="@SysLastUpdatedDate"/>')
                </xsl:attribute>
              </IE:APDButton>
            </xsl:if>
          </td>
          <xsl:if test="@ServiceChannelCD = 'GL'">
            <td>AGC Reference ID:</td>
            <td>
              <div>
                <xsl:attribute name="id">
                  <xsl:value-of select="concat('divReferenceNo', @ClaimAspectServiceChannelID)"/>
                </xsl:attribute>
                <xsl:attribute name="name">
                  <xsl:value-of select="concat('divReferenceNo', @ClaimAspectServiceChannelID)"/>
                </xsl:attribute>
                <xsl:value-of select="@ReferenceID"/>
              </div>
            </td>
          </xsl:if>
        </tr>
      </table>
    </div>
    <xsl:if test="$curServiceChannelCD = 'DA' or $curServiceChannelCD = 'DR' or $curServiceChannelCD = 'PS' or $curServiceChannelCD = 'RRP'">
      <div style="position:absolute;top:3px;left:275px;width:75px;display:none">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('divAssignmentHistory', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <IE:APDButton value="History" CCDisabled="true" CCTabIndex="" onButtonClick="viewAssignmentHistory()">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('btnAssignmentHistory_', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('btnAssignmentHistory_', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
        </IE:APDButton>
      </div>
    </xsl:if>
    <xsl:if test="$curServiceChannelCD = 'DA' or $curServiceChannelCD = 'DR' or $curServiceChannelCD = 'PS'  or $curServiceChannelCD = 'RRP'">
      <div style="position:absolute;top:3px;left:445px;width:100px;">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('divAssignmentProfile', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <IE:APDButton value="Profile" CCDisabled="true" CCTabIndex="" width="50" onButtonClick="showAssignmentProfile()">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('btnAssignmentProfile_', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('btnAssignmentProfile_', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
        </IE:APDButton>
      </div>
    </xsl:if>
    <xsl:if test="contains($VehicleCRUD, 'U') = true()">
      <div style="position:absolute;top:3px;left:660px;width:50px;display:none;">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('divAssignmentSave', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <xsl:variable name="CurrentAssignmentTypeID2">
          <xsl:value-of select="/Root/Vehicle/@InitialAssignmentTypeID"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $CurrentAssignmentTypeID2 and @Name='Choice Shop Assignment']">
            <IE:APDButton value="Save" CCDisabled="true" style="Display:none" CCTabIndex="" onButtonClick="saveAssignment()">
            </IE:APDButton>
          </xsl:when>
          <xsl:otherwise>
            <IE:APDButton value="Save" CCDisabled="true" CCTabIndex="" onButtonClick="saveAssignment()">
              <xsl:attribute name="id">
                <xsl:value-of select="concat('btnSaveAssignment_', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="concat('btnSaveAssignment_', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
            </IE:APDButton>
          </xsl:otherwise>
        </xsl:choose>
      </div>
    </xsl:if>
    <div style="width:100%;height:100%">
      <IE:APDTabGroup many="false" width="711px" height="285" CCTabIndex="600" showADS="false" preselectTab="-1" autoInit="false">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('tabSCGrp', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of select="concat('tabSCGrp', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>

        <IE:APDTab CCDisabled="false" CCTabIndex="601" onTabSelect="showAssignment()">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('tabSCAssignment', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('tabSCAssignment', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="tabPage">
            <xsl:value-of select="concat('divSCAssignment', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="$curServiceChannelCD = 'TL'">
              <xsl:attribute name="caption">Details</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="caption">Assignment</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </IE:APDTab>

        <IE:APDTab caption="Documents" CCDisabled="false" CCTabIndex="602" updStoredProc="" onTabValidate="" onTabAfterSave="" onTabSelect="initDocuments()">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('tabSCDocuments', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('tabSCDocuments', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="tabPage">
            <xsl:value-of select="concat('divSCDocuments', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
        </IE:APDTab>

        <IE:APDTab caption="Concessions" CCDisabled="false" CCTabIndex="603" updStoredProc="" onTabValidate="" onTabAfterSave="" onTabSelect="initConcessions()">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('tabSCConcessions', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('tabSCConcessions', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="tabPage">
            <xsl:value-of select="concat('divSCConcessions', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
        </IE:APDTab>

        <IE:APDTabPage style="display:none">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('divSCAssignment', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('divSCAssignment', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <div>
            <xsl:attribute name="id">
              <xsl:value-of select="concat('divSCAssgn', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <xsl:attribute name="name">
              <xsl:value-of select="concat('divSCAssgn', @ClaimAspectServiceChannelID)"/>
            </xsl:attribute>
            <IFRAME name="ifrmSCAssignment" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background1:transparent;" allowtransparency1="true">
            </IFRAME>
          </div>
        </IE:APDTabPage>

        <IE:APDTabPage style="display:none">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('divSCDocuments', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('divSCDocuments', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>

          <div style="width:100%;height:120px;padding:0px;padding-bottom:3px;">
            <xsl:call-template name="VehicleEstimates"/>
          </div>
          <div style="width:100%;height:128px;border:1px solid #000000">
            <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
              <colgroup>
                <col width="24px"/>
                <col width="*"/>
              </colgroup>
              <tr class="sectionTitle" style="height:16px;padding:0px;margin:0px;">
                <td>
                  <img src="/images/pinned.gif" width1="18" height1="18" border="0" style="cursor:hand;" onclick="togglePinned(this)" title="Unpin/Pin from/to Documents tab"/>
                </td>
                <td>Thumbnail Images</td>
              </tr>
            </table>

            <xsl:choose>
              <xsl:when test="count(/Root/Vehicle/Document[@ServiceChannelCD = $curServiceChannelCD and @EstimateTypeFlag = '0']) &gt; 0">
                <xsl:call-template name="showDocs">
                  <xsl:with-param name="RootDir">
                    	<xsl:value-of select="$RootDir"/>
                  </xsl:with-param>
                  <xsl:with-param name="RootDirArchive">
                  	<xsl:value-of select="$RootDirArchive"/>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <table border="0" cellspacing="0" cellpadding="2" style="width:100%;height:108px">
                  <tr style="height:18px;color:#FF0000;font-weight:bold;text-align:center" valign="center">
                    <td>No Documents</td>
                  </tr>
                </table>

              </xsl:otherwise>
            </xsl:choose>
          </div>
        </IE:APDTabPage>

        <IE:APDTabPage style="display:none">
          <xsl:attribute name="id">
            <xsl:value-of select="concat('divSCConcessions', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="concat('divSCConcessions', @ClaimAspectServiceChannelID)"/>
          </xsl:attribute>
          <div style="width:100%;height:120px;padding:0px;padding-bottom:3px;">
            <xsl:call-template name="VehicleConcessions"/>
          </div>
        </IE:APDTabPage>
      </IE:APDTabGroup>
    </div>
  </xsl:template>

  <xsl:template name="VehicleEstimates">
    <div id="divGridHolder" style="width:100%;height:122px;overflow:hidden">
      <IE:APDDataGrid CCDataPageSize="4" showAlternateColor="false" altColor="#E6E6FA" CCWidth="700px" CCHeight="107" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick1="divGridHolder.focus()">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('GrdCoverage_', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <xsl:attribute name="CCDataSrc">
          <xsl:value-of select="concat('xmlSCEst_', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <IE:APDDataGridHeader>
          <IE:APDDataGridTR>
            <IE:APDDataGridTD width="118px" caption="Recd./Attach Date" fldName="ReceivedDateFormatted" DataType="" sortable="true" CCStyle=""/>
            <IE:APDDataGridTD width="60px" caption="Summary" fldName="FullSummaryExistsFlag" DataType="" sortable="true" CCStyle="text-align:center"/>
            <IE:APDDataGridTD width="33px" caption="Seq." fldName="SequenceNum" DataType="number" sortable="true" CCStyle="text-align:center"/>
            <IE:APDDataGridTD width="75px" caption="Source" fldName="DocumentSourceName" DataType="number" sortable="true" CCStyle="text-align:center"/>
            <IE:APDDataGridTD width="75px" caption="Gross $" fldName="GrossEstimateAmt" DataType="number" sortable="true" CCStyle="text-align:right"/>
            <IE:APDDataGridTD width="75px" caption="Net $" fldName="NetEstimateAmt" DataType="number" sortable="true" CCStyle="text-align:right"/>
            <IE:APDDataGridTD width="85px" caption="Orig./Audited" fldName="OAIndicator" DataType="number" sortable="true" CCStyle="text-align:center"/>
            <IE:APDDataGridTD width="40px" caption="Dup." fldName="DupIndicator" DataType="number" sortable="true" CCStyle="text-align:center"/>
            <IE:APDDataGridTD width="50px" caption="Agreed" fldName="PriceAgreedIndicator" DataType="number" sortable="true" CCStyle="text-align:center"/>
          </IE:APDDataGridTR>
        </IE:APDDataGridHeader>
        <IE:APDDataGridBody>
          <IE:APDDataGridTR>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="ReceivedDateFormatted" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="SummaryIndicatorImg" displayType="image" title="Double-click to view the document">
                <xsl:attribute name="ondblclick">
                  showEstimate("<xsl:value-of select="concat('GrdCoverage_', @ClaimAspectServiceChannelID)"/>")
                </xsl:attribute>
              </IE:APDDataGridFld>

              <xsl:if test="@HyperquestClientFlag = '1'">
                <IE:APDDataGridFld fldName="HyperquestImg" displayType="image" title="Double-click to view the document">
                  <xsl:if test="/Root/Vehicle/@Status = 'Open'">
                    <xsl:attribute name="ondblclick">
                      createHQJob("<xsl:value-of select='/Root/@InsuranceCompanyID' />","<xsl:value-of select="concat('GrdCoverage_', @ClaimAspectServiceChannelID)"/>")
                    </xsl:attribute>
                  </xsl:if>
                </IE:APDDataGridFld>
              </xsl:if>

            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="SequenceNum" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="DocumentSourceName" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="GrossEstimateAmt" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="NetEstimateAmt" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="OAIndicator" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="DupIndicator" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="PriceAgreedIndicator" />
            </IE:APDDataGridTD>
          </IE:APDDataGridTR>
        </IE:APDDataGridBody>
      </IE:APDDataGrid>
    </div>
  </xsl:template>

  <xsl:template name="VehicleConcessions">
    <div style="padding:0px;padding-bottom:1px">
      <table border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
        <colgroup>
          <col width="85px"/>
          <col width="100px"/>
          <col width="70px"/>
          <col width="100px"/>
          <col width="175px"/>
          <col width="55px"/>
          <col width="55px"/>
          <col width="55px"/>
        </colgroup>
        <tr>
          <td>Indemnity Total:</td>
          <td>
            <div id="divIndemnityConcessionTotal">$0.00</div>
          </td>
          <td>
            <!-- Rental Total: -->
          </td>
          <td>
            <div id="divRentalConcessionTotal">
              <!-- $0.00 -->
            </div>
          </td>
          <td></td>
          <td>
            <IE:APDButton value="Add" width="50" CCTabIndex="1">
              <xsl:attribute name="id">
                <xsl:value-of select="concat('btnConcessionAdd', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="concat('btnConcessionAdd', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="onButtonClick">
                <xsl:value-of select="concat('doConcessionAdd(', @ClaimAspectServiceChannelID, ')')"/>
              </xsl:attribute>
              <xsl:attribute name="CCDisabled">
                <xsl:choose>
                  <xsl:when test="@StatusName = 'Active'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </IE:APDButton>
          </td>
          <td>
            <IE:APDButton value="Edit" width="50" CCTabIndex="2">
              <xsl:attribute name="id">
                <xsl:value-of select="concat('btnConcessionEdit', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="concat('btnConcessionEdit', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="onButtonClick">
                <xsl:value-of select="concat('doConcessionEdit(', @ClaimAspectServiceChannelID, ')')"/>
              </xsl:attribute>
              <xsl:attribute name="CCDisabled">
                <xsl:choose>
                  <xsl:when test="@StatusName = 'Active'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </IE:APDButton>
          </td>
          <td>
            <IE:APDButton value="Delete" width="50" CCTabIndex="3">
              <xsl:attribute name="id">
                <xsl:value-of select="concat('btnConcessionDel', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="concat('btnConcessionDel', @ClaimAspectServiceChannelID)"/>
              </xsl:attribute>
              <xsl:attribute name="onButtonClick">
                <xsl:value-of select="concat('doConcessionDelete(', @ClaimAspectServiceChannelID, ')')"/>
              </xsl:attribute>
              <xsl:attribute name="CCDisabled">
                <xsl:choose>
                  <xsl:when test="@StatusName = 'Active'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </IE:APDButton>
          </td>
        </tr>
      </table>
    </div>
    <div id="divGridConcessionHolder" style="width:100%;height:230px;overflow:hidden;border:0px solid #FF0000">
      <IE:APDDataGrid CCDataPageSize="9" showAlternateColor="false" altColor="#E6E6FA" CCWidth="700px" CCHeight="212" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick1="divGridHolder.focus()">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('GrdConcession_', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <xsl:attribute name="CCDataSrc">
          <xsl:value-of select="concat('xmlSCConcession_', @ClaimAspectServiceChannelID)"/>
        </xsl:attribute>
        <IE:APDDataGridHeader>
          <IE:APDDataGridTR>
            <IE:APDDataGridTD width="118px" caption="Last Updated" fldName="CreatedDate" DataType="" sortable="true" CCStyle=""/>
            <IE:APDDataGridTD width="150px" caption="Type" fldName="TypeDescription" DataType="" sortable="true" CCStyle=""/>
            <IE:APDDataGridTD width="300px" caption="Reason" fldName="ReasonDescription" DataType="" sortable="true" CCStyle=""/>
            <IE:APDDataGridTD width="80px" caption="Amount" fldName="Amount" DataType="number" sortable="true" CCStyle="text-align:right"/>
          </IE:APDDataGridTR>
        </IE:APDDataGridHeader>
        <IE:APDDataGridBody>
          <IE:APDDataGridTR ondblclick="doConcessionEdit()">
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="CreatedDateFormatted" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="TypeDescription" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="ReasonDescription" />
            </IE:APDDataGridTD>
            <IE:APDDataGridTD>
              <IE:APDDataGridFld fldName="Amount" />
            </IE:APDDataGridTD>
          </IE:APDDataGridTR>
        </IE:APDDataGridBody>
      </IE:APDDataGrid>
    </div>
  </xsl:template>

  <xsl:template name="showDocs">
    <xsl:param name="RootDir"/>
    <xsl:param name="RootDirArchive"/>
    <xsl:variable name="curServiceChannelCD">
      <xsl:value-of select="@ServiceChannelCD"/>
    </xsl:variable>
    <xsl:variable name="curClaimAspectServiceChannelID">
      <xsl:value-of select="@ClaimAspectServiceChannelID"/>
    </xsl:variable>
    <div style="width:100%;height:108px;overflow-x:auto;overflow-y:none">
      <xsl:attribute name="id">
        <xsl:value-of select="concat('divDocs', @ClaimAspectServiceChannelID)"/>
      </xsl:attribute>
      <table border="0" cellspacing="0" cellpadding="2">
        <tr>
          <xsl:for-each select="/Root/Vehicle/Document[@ServiceChannelCD = $curServiceChannelCD and @EstimateTypeFlag = '0']">
            <xsl:sort select="@DocumentID" data-type="number" order="ascending"/>
            <td>
              <span class="thumbNail" unselectable="on">
                <xsl:attribute name="name">
                  <xsl:value-of select="concat('tn', @DocumentID)"/>
                </xsl:attribute>
                <xsl:attribute name="id">
                  <xsl:value-of select="concat('tn', @DocumentID)"/>
                </xsl:attribute>
                <xsl:attribute name="onclick">
                  displayImage(this, "<xsl:value-of select="$curClaimAspectServiceChannelID"/>", "<xsl:value-of select="@DocumentID"/>", "<xsl:value-of select="@DocumentTypeName"/>", "<xsl:value-of select="@Archived"/>")
                </xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@DocumentTypeName = 'Photograph'">
                    <center unselectable="on">
                      <span>
                        <IE:APDCheckBox caption="" width="" CCDisabled="false" required="false" CCTabIndex="-1" canDirty="false" onchange="shareDocument();" title="Mark document for Policy Holder view">
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('chkShare', @DocumentID)"/>
                          </xsl:attribute>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('chkShare', @DocumentID)"/>
                          </xsl:attribute>
                          <xsl:attribute name="value">
                            <xsl:value-of select="@ApprovedFlag"/>
                          </xsl:attribute>
                        </IE:APDCheckBox>
                      </span>
                      <span class="thumbNailImage">
                        <img  unselectable="on">
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('img', @DocumentID)"/>
                          </xsl:attribute>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('img', @DocumentID)"/>
                          </xsl:attribute>
                        </img>
                      </span>
                    </center>
                  </xsl:when>
                  <xsl:otherwise>
                    <center unselectable="on">
                      <span class="thumbNailImage">
                        <img  unselectable="on">
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('img', @DocumentID)"/>
                          </xsl:attribute>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('img', @DocumentID)"/>
                          </xsl:attribute>
                        </img>
                      </span>
                    </center>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="@DirectionToPayFlag = '1' or @FinalEstimateFlag = '1'">
                  <span class="thumbNailInfo">
                    <xsl:if test="@DirectionToPayFlag = '1'">DTP</xsl:if>
                    <xsl:if test="@FinalEstimateFlag = '1'">
                      <xsl:if test="@DirectionToPayFlag = '1'">/</xsl:if>FE
                    </xsl:if>
                  </span>
                </xsl:if>
                <span class="thumbNailInfo" title="Created Date; File Size">
                  <!-- <xsl:value-of select="concat($RootDir, @ImageLocation)"/> -->

            	<xsl:variable name="FileInfo">
                <xsl:if test="@Archived = '1'">
			<xsl:value-of select="user:GetFileSize(concat($RootDirArchive, @ImageLocation))"/>	
		</xsl:if>
                <xsl:if test="@Archived = '0'">
			<xsl:value-of select="user:GetFileSize(concat($RootDir, @ImageLocation))"/>	
		</xsl:if>
            	</xsl:variable>


                  <xsl:choose>
                    <xsl:when test="$FileInfo = 'Not Found'">
                      <xsl:attribute name="style">color:#FF0000</xsl:attribute>
                      Not Found
                    </xsl:when>
                    <xsl:otherwise>
                      <div>
                        <xsl:value-of select="user:UTCConvertDate(string(@CreatedDate))"/>
                      </div>
                      <div>
                        <xsl:value-of select="$FileInfo"/>
                      </div>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </span>
            </td>
          </xsl:for-each>
        </tr>
      </table>
    </div>
  </xsl:template>
  <xsl:template match="@*">
    <xsl:value-of select="name(.)"/>="<xsl:value-of disable-output-escaping="no" select="user:removeDblQuotes(string(.))"/>"
  </xsl:template>
</xsl:stylesheet>

