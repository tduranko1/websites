<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  id="ClaimRepDesk">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<HTML>
<HEAD>
<TITLE></TITLE>

<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,3);
		  event.returnValue=false;
		  };	
</script>
 


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

  var vWindowID = "<xsl:value-of select="$WindowID"/>";
  var vUserID = "<xsl:value-of select="$UserId"/>";
  var sortColumn;
  var sortOrder;
  var dataType;
  var curPage;
  var iScrollPos;

  <![CDATA[

  //init the table selections, must be last
  var refreshNewTimerID = null;

  function initPage()
  {
    RefreshClaims();

    stat1.style.top = (document.body.offsetHeight - 75) / 2;
    stat1.style.left = (document.body.offsetWidth - 240) / 2;
    
    top.setSB(100,top.sb)
  }
  
  var flagSearch = false;

  function ShowSearch()
  {
    if (flagSearch == true)
      document.getElementById("SearchResultsInfo").style.visibility = "inherit";
    flagSearch = false;
  }

  var gbErrorMode = false;

  function RefreshClaims()
  {
    if (gbErrorMode == false)
      if (document.frames["ifrmNewOpenClaim"].document.URL.indexOf("blank.asp") == -1)
        document.frames["ifrmNewOpenClaim"].document.location.reload();
      else
        superViewSelNew();
        //document.frames["ifrmClaimNew"].frameElement.src ="/ClaimRepDeskNew.asp?WindowID=" + vWindowID;
  }


  function InErrorMode(bError)
  {
    gbErrorMode = bError;  
  }

  var bShowClosedClaims = true;
  function SearchClosedClaim()
  {
    btnSearch.setFocus();
      if (txtDays.value >= 0)
      {
        if (isNaN(txtDays.value)) {
          ClientWarning("Invalid Number of Days specfied.");
          txtDays.focus();
          return;
        }
        bShowClosedClaims = true;
        document.frames["ifrmClosedClaim"].frameElement.src ="/ClosedClaimSearch.asp?WindowID=" + vWindowID + "&days=" + txtDays.value;
        showClosedClaims();
      }
      else
      {
          ClientWarning("Invalid Search Criteria!");
          document.getElementById("ClosedClaimResultsInfo").style.visibility = "hidden";
          document.frames["ifrmClosedClaim"].frameElement.src ="/blank.asp"
      }
    txtDays.setFocus();
  }

  function showClosedClaims(){
      if (bShowClosedClaims)
          document.getElementById("ClosedClaimResultsInfo").style.visibility = "visible";
      else
          document.getElementById("ClosedClaimResultsInfo").style.visibility = "hidden";
  }

  function superViewSelNew()
  {
      if ( gbErrorMode != false )
          return;

      sortColumn = "";
      sortOrder = "";
      dataType = "";
      curPage = 1;
      iScrollPos = 0;
      var vSubordinateUserID = vUserID;
      if (document.getElementById("csSuperViewLevel"))
        vSubordinateUserID = document.getElementById("csSuperViewLevel").value;
      if (vSubordinateUserID == null) vSubordinateUserID = vUserID;
      var vSubordinatesFlag = 0;
      if (document.getElementById("csUserOptions"))
        vSubordinatesFlag = csUserOptions.value;
      //var vSubordinateUserID = ;//vSubordinateUserID.substr( 1, vSubordinateUserID.length - 1 );
      top.SubordinateUserID = vSubordinateUserID;
      top.SubordinatesFlag = vSubordinatesFlag;
      
      document.frames["ifrmNewOpenClaim"].frameElement.src =
          "/ClaimRepDeskList.asp?WindowID=" + vWindowID + 
          "&SubordinateUserID=" + vSubordinateUserID + 
          "&SubordinatesFlag=" + vSubordinatesFlag;
      showStatus("Loading data...");
  }
  
  function showStatus(sMsg) {
    try {
    stat1.Show(sMsg);
    } catch (e) {}
  }
  
  function hideStatus() {
    try {
    stat1.Hide();
    } catch (e) {}
  }
  
  function stopRefresh() {
    var curTab = event.fromTab.id;
    
    switch (curTab) {
      case "tabNewOpenClaims":  
        if (typeof(ifrmNewOpenClaim.startPageRefreshTimer) == "function")
          ifrmNewOpenClaim.startPageRefreshTimer();
        break;
      case "tabClosedClaims":
        if (typeof(ifrmNewOpenClaim.stopPageRefreshTimer) == "function")
          ifrmNewOpenClaim.stopPageRefreshTimer();
        break;
    }
  }


  if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" style="background:transparent;border:0px;margin:4px;padding:0px" onLoad="popupInit();initPage();" onUnload1="if (refreshNewTimerID > 0) window.clearInterval(refreshNewTimerID);" tabIndex="-1" >
<IE:APDStatus id="stat1" name="stat1" class="APDStatus" width="240" height="75" />
<IE:APDTabGroup id="tabClmRep" name="tabClmRep" many="false" width="990px" height="505px" preselectTab="0" stackable="false" CCDisabled="false" CCTabIndex="1" showADS="false" onTabSelect="stopRefresh()" >
  <IE:APDTab id="tabNewOpenClaims" name="tabNewOpenClaims" caption="New/Open Claims" tabPage="divNewOpenClaims" CCDisabled="false" CCTabIndex="1"/>
  <IE:APDTab id="tabClosedClaims" name="tabClosedClaims" caption="Closed Claims" tabPage="divClosedClaims" CCDisabled="false" CCTabIndex="2"/>

  <IE:APDTabPage name="divNewOpenClaims" id="divNewOpenClaims" style="display:none">
    <IFRAME id="ifrmNewOpenClaim" name="ifrmNewOpenClaim" 
        onreadystatechange='top.IFStateChange(this);' 
        style='width:100%; height:475px; border:0px; visibility:inherit; position:absolute; overflow:hidden;'
        allowtransparency='true'>
        <xsl:attribute name="src">blank.asp</xsl:attribute>
    </IFRAME>
  </IE:APDTabPage>

  <IE:APDTabPage name="divClosedClaims" id="divClosedClaims" style="display:none">
    <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px; margin-left:10px;">
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap"> Days Closed: </TD>
        <TD unselectable="on">
          <IE:APDInputNumeric id="txtDays" name="txtDays" value="30" precision="3" scale="0" neg="false" int="true" canDirty="false" CCTabIndex="10" onChange="" onKeypress="if (event.keyCode==13) SearchClosedClaim();"/>
          <!-- <INPUT type="text" name="txtDays" class="InputField" size="8" onKeypress="if (event.keyCode==13) SearchClosedClaim(); else NumbersOnly(event);" allowDecimal="false" allowMinus="false" tabindex="1" maxlength="9" default="true" value="30"/> -->
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" colspan="2" width="100%" nowrap="nowrap">
          <IE:APDButton id="btnSearch" name="btnSearch" value="Search" CCTabIndex="11" onButtonClick="SearchClosedClaim()"/>
          <!-- <input type="button" class="formbutton" onclick="SearchClosedClaim();" value="Search" tabindex="6"/> -->
        </TD>
      </TR>
    </TABLE>
  
    <DIV unselectable="on" id="ClosedClaimResultsInfo" style="position:absolute; visibility:hidden; left:0px; top:33px; z-index:1; width:100%; height:450px; border:0px #FF0000;">
      <IFRAME id="ifrmClosedClaim" name="ifrmClosedClaim" 
          onreadystatechange='top.IFStateChange(this); ShowSearch();' 
          style='width:100%; height:450px; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden;'
          allowtransparency='true'>
          <xsl:attribute name="src">ClosedClaimSearch.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;days=30</xsl:attribute>
      </IFRAME>
    </DIV>
  </IE:APDTabPage>
</IE:APDTabGroup>


  <DIV id="superViewDiv" style="position:absolute;top:1px;left:690px;">
      <!-- <xsl:if test="@SupervisorFlag != '1'">
        <xsl:attribute name="style">position:absolute;top:1px;left:690px;display:none</xsl:attribute>
      </xsl:if>
      <xsl:if test="@SupervisorFlag = '1'">
        <xsl:attribute name="style">position:absolute;top:1px;left:690px;</xsl:attribute>
      </xsl:if> -->
      <table border="0" cellpadding="0" cellspacing="1">  
        <tr>
          <td style="font-weight:bold">View:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
          <td style="padding-right:3px;">
            <IE:APDCustomSelect id="csSuperViewLevel" name="csSuperViewLevel" width="150" maxwidth="235" displayCount="10" blankFirst="false" canDirty="false" ExpRequired="true" CCDisabled="false" onChange="superViewSelNew()" CCTabIndex="3">            
              <xsl:attribute name="value"><xsl:value-of select="$UserId"/></xsl:attribute>
              
              <xsl:for-each select="User">
        	      <xsl:sort select="@NameLast"/>
                <IE:dropDownItem style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>
                  <xsl:if test="@SupervisorFlag='1'">
                    <xsl:choose>
                      <xsl:when test="count(//User[@SupervisorID = current()/@UserID and @SupervisorFlag = '1']) &gt; 0">
                        <xsl:attribute name="imgSrc">/images/2User.gif</xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="imgSrc">/images/1User.gif</xsl:attribute>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:if>
                  <xsl:value-of select="concat(@NameLast, ', ' , @NameFirst)"/>
                </IE:dropDownItem>
              </xsl:for-each>
            </IE:APDCustomSelect>
          </td>
          <td>+</td>
          <td>
            <IE:APDCustomSelect id="csUserOptions" name="csUserOptions" value="0" type="list" displayCount="5" blankFirst="false" canDirty="false" CCTabIndex="4" width="100" onChange="superViewSelNew()">
              <xsl:if test="@SupervisorFlag != '1'">
                <xsl:attribute name="CCDisabled">true</xsl:attribute>
              </xsl:if>
            	<IE:dropDownItem value="0" style="display:none">&amp;nbsp;</IE:dropDownItem>
            	<IE:dropDownItem value="1" style="display:none">Direct Reports</IE:dropDownItem>
            	<IE:dropDownItem value="2" style="display:none">All Subordinates</IE:dropDownItem>
            </IE:APDCustomSelect>
          </td>
        </tr>
      </table>
  </DIV>

<!-- Tooltip Div -->
<DIV unselectable="on" id="divTooltip"> </DIV>
</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>

