<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    id="VehicleShopAssignment">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>
  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    function ShowHideShop(iType, strShopId)
     {
      if (iType == 0)
      {
        if (strShopId == '')
          return "Visibility:inherit;display:inline";
        else
           return "Visibility:hidden;display:none";
      }

      if (iType == 1)
      {
        if (strShopId == 0)
          return "Visibility:hidden;display:none";
        else
           return "Visibility:inherit;display:inline";
      }
     }
  ]]>
  </msxsl:script>

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="VehicleCRUD" select="Vehicle"/>
  <xsl:param name="ShopCRUD" select="Shop"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="EventVehicleAssignmentSentToShopID"/>
  <xsl:param name="EventAppraiserAssignmentSent"/>
  <xsl:param name="LynxID"/>
  <xsl:param name="pageReadOnly"/>
  <xsl:param name="AssignmentSequenceNumber"/>

  <xsl:template match="/Root">

    <xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
    <!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->
    <xsl:variable name="AssignmentSequenceNumber" select="session:XslGetSession('AssignmentSequenceNumber')"/>
    <xsl:variable name="AdjVehicleCRUD">
      <xsl:choose>
        <xsl:when test="contains($VehicleCRUD, 'R') = true() and $pageReadOnly='true'">_R__</xsl:when>
        <xsl:when test="contains($VehicleCRUD, 'R') = false() and $pageReadOnly='true'">____</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$VehicleCRUD"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="VehicleNumber" select="/Root/@VehicleNumber" />
    <xsl:variable name="VanId" select="/Root/ServiceChannel/Assignment/@VANAssignmentStatusID" />
    <xsl:variable name="AssignmentLastUpdatedDate" select="ServiceChannel/Assignment/@SysLastUpdatedDate"/>


    <HTML>

      <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
      <!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspVehicleAssignGetDetailXML,VehicleShopAssignment.xsl,5101,145   -->

      <HEAD>
        <TITLE>Shop Assignment</TITLE>

        <LINK rel="stylesheet" href="/css/apdcontrolslabourrate.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/APDGrid.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="/css/jquery.autocomplete.css" />

        <STYLE type="text/css">
          A {color:#0000FF; text-decoration:none; cursor:hand}
          A:Hover {color:#0000FF; text-decoration:underline;cursor:hand}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/VehAssignment.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
        <script language="JavaScript" src="/js/jquery/jquery.min.js" type="text/javascript"></script>
        <script language="JavaScript" src="/js/jquery/jquery.autocomplete.min.js" type="text/javascript"></script>
        <script language="JavaScript" src="/js/jquery/jquery.ajaxqueue.js" type="text/javascript"></script>
        <script language="JavaScript" src="/js/jquery/jquery.bgiframe.min.js" type="text/javascript"></script>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
          RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
          event.returnValue=false;
          };
        </script>
        <SCRIPT language="JavaScript">
          //-- 04Dec2012 -- TVD Please Wait Fix --/
          var pageLoaded = 0;

          var gsLynxID = '<xsl:value-of select="$LynxID"/>';
          var gsUserID = '<xsl:value-of select="$UserId"/>';
          //var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
          var gsInvolvedCRUD = "<xsl:value-of select="$AdjVehicleCRUD"/>";
          <!--var gsAssignmentSequenceNumber = "<xsl:value-of select="$AssignmentSequenceNumber"/>";-->
          var gsVanId = '<xsl:value-of select="$VanId"/>';
          var gsAssignmentLastUpdatedDate = "<xsl:value-of select="$AssignmentLastUpdatedDate"/>";
          var sShopRemarks;

          var gsBusinessId = '<xsl:value-of select="/Root/Shop/@BusinessID"/>';
          var gsClaimAspectID = '<xsl:value-of select="@ClaimAspectID"/>';
          var gsInsuranceCompanyID = '<xsl:value-of select="/Root/@InsuranceCompanyID"/>';
          <!--var gsAssignmentID ='<xsl:value-of select="/Root/ServiceChannel/Assignment/@AssignmentID"/>'-->
          var gsAssignmentID = 0
          var gsAssignmentID1 = 0
          var gsShopId = 0
          <!-- var gsShopId = '<xsl:value-of select="/Root/ServiceChannel/Assignment/@ShopLocationID"/>'; -->

          <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
            gsShopId = '<xsl:value-of select="@ShopLocationID"/>';
          </xsl:for-each>

          <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
            gsAssignmentID = '<xsl:value-of select="@AssignmentID"/>';
          </xsl:for-each>
          <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='2']">
            gsAssignmentID1 = '<xsl:value-of select="@AssignmentID"/>';
          </xsl:for-each>

          <!--<xsl:choose>
          <xsl:when test="(/Root/ServiceChannel/Assignment[@AssignmentTypeCD='SHOP']) and (/Root/ServiceChannel/Assignment[@AssignmentTypeCD='LDAU'])" >
            <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentTypeCD='LDAU']">
              var gsAssignmentID1 = '<xsl:value-of select="@AssignmentID"/>';
            </xsl:for-each>
          </xsl:when>
          </xsl:choose>-->

          <xsl:choose>
            <xsl:when test="(/Root/ServiceChannel/Assignment/Shop/@ProgramType = 'CEI') or (/Root/ServiceChannel/Assignment/Shop/@Name = '* LYNX Out of Program Shop')" >
              var gbCEIFlag = true;
            </xsl:when>
            <xsl:otherwise>
              var gbCEIFlag = false;
            </xsl:otherwise>
          </xsl:choose>
          var gsVehicleAssignmentSentToShopCD = '<xsl:value-of select="$EventVehicleAssignmentSentToShopID"/>';
          var gsAppraiserAssignmentSentEventID = '<xsl:value-of select="$EventAppraiserAssignmentSent"/>';
          var gsShopName = escape("<xsl:value-of select="js:cleanString(string(/Root/ServiceChannel/Assignment/Shop/@Name))"/>");
          var gsLDAUAppraiserTypeCD = "<xsl:value-of select="/Root/@LDAUAppraiserTypeCD"/>";
          var gsLDAUID = "<xsl:value-of select="/Root/@LDAUID"/>";
          var gsAssignmentTypeCD = "<xsl:value-of select="ServiceChannel/Assignment/@AssignmentTypeCD"/>";
          var gsMEAppraiserTypeCD = "<xsl:value-of select="/Root/@MEAppraiserTypeCD"/>";
          var gsMEID = "<xsl:value-of select="/Root/@MEID"/>";
          var gsGLID = "<xsl:value-of select="/Root/@GLID"/>";

          var iTotalDeductible = 0;
          var iTotalDeductibleApplied = 0;
          var iTotalLimit = 0;
          var gsClaimAspectServiceChannelID = "<xsl:value-of select="/Root/ServiceChannel/@ClaimAspectServiceChannelID"/>";
          var blnAddMode = false;
          var strDedutiblesApplied = "<xsl:value-of select="sum(/Root/ServiceChannel/CoverageApplied/@DeductibleAppliedAmt)"/>";
          var strLimitsApplied = "<xsl:value-of select="sum(/Root/ServiceChannel/CoverageApplied/@LimitAppliedAmt)"/>";
          var strCurEstDocumentID = "<xsl:value-of select="/Root/ServiceChannel/Assignment/@CurEstDocumentID"/>";
          var strCurEstGrossAmt = "<xsl:value-of select="/Root/ServiceChannel/Assignment/@CurEstGrossRepairTotal"/>";
          var strServiceChannelCD = "<xsl:value-of select="/Root/ServiceChannel/@ServiceChannelCD"/>";
          var strRepairStartDate = "<xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkStartDate))"/>";
          var strRepairEndDate = "<xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkEndDate))"/>";
          var strReferenceID = "<xsl:value-of select="/Root/ServiceChannel/Assignment/@ReferenceID"/>";
          var strLossDate = "<xsl:value-of select="js:formatSQLDateTime(string(/Root/@LossDate))"/>";
          var blnRepairReason = false;
          var bMouseInToolTip = false;
          var strFaxStatus = "<xsl:value-of select="/Root/ServiceChannel/@FaxAssignmentStatus"/>";
          var strDrivable = "<xsl:value-of select="/Root/Vehicle/@DriveableFlag"/>";
          var strRentalCoverage = "<xsl:value-of select="/Root/Vehicle/@RentalCoverage"/>";
          var blnMessageChanging = false;
          var strInsuranceCompanyName = "<xsl:value-of select="js:cleanString(string(/Root/@InsuranceCompanyName))"/>";
          var strInsuredClaimantName = "<xsl:value-of select="js:cleanString(string(/Root/Vehicle/@InsuredClaimantName))"/>";
          var strClaimOwnerName = "<xsl:value-of select="js:cleanString(string(/Root/@ClaimOwnerName))"/>";
          var strClaimOwnerPhone = "<xsl:value-of select="js:cleanString(string(/Root/@ClaimOwnerPhone))"/>";
          var strAssignmentRemarks = "";
          var lsCityData = null;

          <!-- 
            14Dec2011 - TVD 
            Changed xpath to get the repair information from the Shop Assignment
          -->
          var strShopLocationCity = '<xsl:value-of select="js:cleanString(string(/Root/@RepairLocationCity))"/>';
          var strShopLocationCounty = '<xsl:value-of select="js:cleanString(string(/Root/@RepairLocationCounty))"/>';
          var strShopLocationState = '<xsl:value-of select="js:cleanString(string(/Root/@RepairLocationState))"/>';

          //<xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
            //  alert("--> " + '<xsl:value-of select="/Shop/@AddressState"/>');
            //  alert("--> " + '<xsl:value-of select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']/Shop/@AddressState"/>');
            //  alert("--> " + '<xsl:value-of select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']/Shop/@AddressCounty"/>');
            //
          </xsl:for-each>

          var lbCancel = false;

          var strAssignmentTypeId = '<xsl:value-of select="/Root/ServiceChannel/Assignment/@AssignmentTypeID"/>';

          try {
          var gsVehNum = parent.parent.gsVehNum;
          } catch (e) {}
          var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
          var gsPageReadOnly = "<xsl:value-of select="$pageReadOnly"/>";
          var strOldCity = "";
          var oCitiesCache = null;
          var oStatesCache = null;
          var lsCityData = null;
          <!--Get Communication Method to handle HyperQuest-->
          var strCommunicationMethod = "<xsl:value-of select="js:cleanString(string(/Root/ServiceChannel/Assignment/Shop/@PreferredCommunicationMethodID))"/>";


          <![CDATA[
   // Select a new shop. Calls AssignShop in ClaimVehicle

    //-- 04Dec2012 -- TVD Please Wait Fix --/
	if (pageLoaded==0){
		window.setTimeout("doAgain()", 10000); 
	}

    //-- 04Dec2012 -- TVD Please Wait Fix --/
	function doAgain() {
		if (parent.stat1){
			$(document).ready(function(){
			   __pageInit();
			});
		}
	}

	function __pageInit(){
      strAssignmentRemarks = txtAssignmentRemarks.value;
      
      if (typeof(parent.disableButton) == "function"){
         parent.disableButton("btnAssignmentHistory_", false);
         if (gsAssignmentID != "" && isNaN(gsAssignmentID) == false)
            parent.disableButton("btnSaveAssignment_", false);
      }
      //if (strShopLocationState != "" && typeof(parent.enableAssignmentProfile) == "function"){
         parent.enableAssignmentProfile(gsClaimAspectServiceChannelID);
      //}

       if (gsAssignmentID != "" && isNaN(gsAssignmentID) == false){
          
          if ((strServiceChannelCD != "GL") || (strServiceChannelCD == "GL" && strReferenceID == "")){
              btnSelectShop.style.display = "inline";
          } 
              
          if (strServiceChannelCD == "DA") {
              btnShopSearch.style.visibility = "visible";
              btnSelectShop.style.display = "none";
          } 
          
          if (strServiceChannelCD == "PS") {
            btnShopSearch.style.visibility = gsAssignmentID != 0 ? "hidden" : "visible";
            btnSelectShop.style.display = "inline"
          }

          if (strServiceChannelCD == "CS") {
            btnShopSearch.style.visibility = gsAssignmentID != 0 ? "hidden" : "visible";
            btnSelectShop.style.display = "none"
          }

          if (strServiceChannelCD == "RRP") {
            btnShopSearch.style.visibility = gsAssignmentID != 0 ? "hidden" : "visible";
            btnSelectShop.style.display = "inline"
          }
          
          if (strServiceChannelCD == "IA"){
          }
       }
       

      if (document.getElementById("btnRepairEnd")) {
         if (txtRepairEndDate.value != "" && btnRepairEnd){
            var dtRepairEnd = new Date(txtRepairEndDate.value);
            var dtToday = new Date();
            
            //if (dtRepairEnd > dtToday)
            //   btnRepairEnd.style.display = "none";
            if (dtRepairEnd <= dtToday)
               btnRepairEnd.style.display = "inline";
         } else if (txtRepairEndDate.value == "" || txtRepairStartDate.value == "" || txtRepairStartDate.CCDisabled == true)
            btnRepairEnd.style.display = "none";
      }
      
      if (document.getElementById("btnRepairStart")) {
         if (txtRepairStartDate.value != ""){
            var dtRepairStart = new Date(txtRepairStartDate.value);
            var dtToday = new Date();
            
            //if (dtRepairStart > dtToday)
            //   btnRepairStart.style.display = "none";
            if (dtRepairStart <= dtToday)
               btnRepairStart.style.display = "inline";
         } else
            btnRepairStart.style.display = "none";
            
            
      }

      if (document.getElementById("txtRepairEndDate"))
         if (top.blnSupervisorFlag == "1"){
            txtRepairEndDate.CCDisabled = false;
         }

      if (document.getElementById("txtRepairStartDate"))
         if (top.blnSupervisorFlag == "1"){
            txtRepairStartDate.CCDisabled = false;
         }


      if ((gsAssignmentID == "") && (strFaxStatus == "Cancellation Sent")){
         var objLastAssignment = xmlHistory.selectSingleNode("/Root/Assignment");
         var strAssignmentTypeCD, strAppraiserName;
         if (objLastAssignment){
            strAssignmentTypeCD = objLastAssignment.getAttribute("AssignmentTypeCD");
            strAppraiserName = objLastAssignment.getAttribute("AppraiserName");
            txtFaxStatus.value = strFaxStatus + " to " + strAppraiserName;
         }
      }

      try {
         var strCurEstNetAmt = txtCurEstNetAmt.value.substring(1).Trim();
         if (strCurEstNetAmt == "")
            window.setTimeout("calculateNetAmt()", 150);
      } catch (e) {}

      $(document).ready(function(){
         pageInit2();
      });

      if (parent.stat1){
         parent.stat1.Hide();
      }

	  //-- 04Dec2012 -- TVD Please Wait Fix --/
	  pageLoaded = 1;
  }
   
   function pageInit2(){
       if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR"){  
         if (strShopLocationCity != "" && strShopLocationCounty != "" && strShopLocationState != ""){
            lsCityData = [strShopLocationCity, strShopLocationState, strShopLocationCounty];
         }
         try {
         $("#txtRepairLocationCity").val(txtRepairLocationCity2.value);
         $("#txtRepairLocationCity").attr("disabled", txtRepairLocationCity2.CCDisabled);
         txtRepairLocationCity2.style.display = "none";
         $("#txtRepairLocationCity").css("display", "inline");
         } catch (e) {alert("Error: " + e.description)}
          $("#txtRepairLocationZip").bind("blur", function(e){
            if ($("#txtRepairLocationZip").val() != ""){
               lbZipCodeResolved = false;
               /*$.get("ziplookup.asp?z=" + $("#txtRepairLocationZip").val(), function(data){
                  if (data != "" && data != "||"){
                     zipResolve(data);
                  } else {
                     if (lbCancel == false)
                        alert("Zip Code could not be resolved. Please type a valid zip code, or enter the City, State, and County.");
                     //$("#txtRepairZipCode").val("");
                  }
               });*/
               var sProc;
               var sRequest = "ZipCode=" + $("#txtRepairLocationZip").val();
                              
               sProc = "uspZipCodeDetailXML";
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : "executespnpasxml",
                                 data     : sRequest }
                             );
               var sXMLRequest = makeXMLSaveString(aRequests);
               AsyncXMLSave(sXMLRequest, zipResolve, zipResolveFailed);
            }
          });
          
         $("#txtRepairLocationCity").bind("keypress", function(e){
            lsCityData = null;
            if (txtRepairLocationState.selectedIndex != -1) txtRepairLocationState.selectedIndex = -1;
            if (txtRepairLocationCounty.selectedIndex != -1) txtRepairLocationCounty.selectedIndex = -1;
         });
          $("#txtRepairLocationCity").bind("blur", function(e){
            if ($("#txtRepairLocationCity").val() != ""){
               var city = $("#txtRepairLocationCity").val();
               city = jQuery.trim(city).toUpperCase();
               if (city == strOldCity) return;
               $("#txtRepairLocationCity").val(city);
               $.get("csclookup.asp?cty=" + city, function(data){
                  oCitiesCache = null;
                  if (data != ""){
                     var matches = data.split("\n");
                     var strAdded = "";
                     var strState = "";
                     
                     if (oStatesCache == null){
                        oStatesCache = txtRepairLocationState.Options.slice(0);
                     }
                     
                     for (var i = txtRepairLocationState.Options.length - 1; i >= 0 ; i--){
                        txtRepairLocationState.RemoveItem(i);
                     }
                     
                     for (var i = txtRepairLocationCounty.Options.length - 1; i >= 0 ; i--){
                        txtRepairLocationCounty.RemoveItem(i);
                     }

                     for (var i = 0; i < matches.length; i++){
                        data2 = matches[i].split("|");
                        var strCity = data2[0];
                        var strState = data2[1];
                        if (strCity == city && strAdded.indexOf(strState + "|") == -1){
                           for (j = 0; j < oStatesCache.length; j++){
                              if (oStatesCache[j].value == strState) {
                                 txtRepairLocationState.AddItem(oStatesCache[j].value, oStatesCache[j].text);
                                 strAdded += strState + "|";
                              }
                           }
                        }
                     }
                     oCitiesCache = matches;

                     if (txtRepairLocationState.Options.length == 1) { 
                        txtRepairLocationState.selectedIndex = 0;
                        populateCounty();
                     } else {
                        if (lsCityData == null) {
                           txtRepairLocationState.selectedIndex = -1;
                        } else {
                           txtRepairLocationState.value = lsCityData[1]; 
                           populateCounty();
                        }
                     }
                     strOldCity = city;

                  } else {
                     if (lbCancel == false){
                        alert("City could not be resolved. Please ener a valid City.");
                        $("#txtRepairLocationCity").focus();
                     }
                  }
               });
            }
          });

          if ($("#txtRepairLocationCity").val() != ""){
            $("#txtRepairLocationCity").trigger("blur");
          }
       }
       
//?????? TEMP TVD ????????
//var oAssignment = document.getElementById("btnAssignment");
//oAssignment.CCDisabled = false;
   }
   
   // This function added for handle the HQ RESEND ASSIGNMENT Process
   // Done by Glsd451 on 28Apr2015
   function AssignmentProcess(resendFlag, assignmentFlag)
   {
   //alert(resendFlag);
     var sProc;
               var sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
               "&InsuranceCompanyID=" + gsInsuranceCompanyID;
                //alert(sRequest);              
               sProc = "uspVehicleAssignGetDetailXML";
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : "executespnpasxml",
                                 data     : sRequest }
                             );
               var objRet = XMLSave(makeXMLSaveString(aRequests));

      if (objRet && objRet.code == 0 && objRet.xml)
      {
         var oRetXML = objRet.xml;
         var strAssignmentStatus, strServiceChannelCD;
         
         var oAssignment = oRetXML.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/ServiceChannel/Assignment");
         if(oAssignment)
          strAssignmentStatus = oAssignment.getAttribute("VANAssignmentStatusName");
         
         var oServiceChannel = oRetXML.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/ServiceChannel");
         if (oServiceChannel)
         {     
            strServiceChannelCD = oServiceChannel.getAttribute("ServiceChannelCD");
            
            if(strCommunicationMethod == "17" && resendFlag == 1)
              {              
                var strJobID = oServiceChannel.getAttribute("JobID");
                var strJobStatus = oServiceChannel.getAttribute("JobStatus");
                
                if(strJobStatus == "UNPROCESSED" || strJobStatus == "SENDING")
                {
                    if (YesNoMessage("Confirm Fax Assignment", "Assignment already sent waiting to process. Do you want to send the fax? Select Yes to send fax and electronic assignment, press no to send only the electronic assignment.") == "Yes")
                       FaxAssignment(strServiceChannelCD);
                }
                else if (strJobStatus == "FAILED")
                {
                  var strResult;
                  strResult = YesNoMessage("Confirm Fax Assignment", "Do you want to send the fax? Select Yes to send fax and electronic assignment, press no to send only the electronic assignment.");
                  
                  if (strResult == "Yes")
                  {
                      sRequest = "vJobID=" + strJobID +
                                "&VJobStatus=" + "UnProcessed";
                      sProc = "uspHyperquestUpdateJobDetails";
                    
                      var aRequests = new Array();
                          aRequests.push( { procName : sProc,
                                        method   : "ExecuteSpNp",
                                        data     : sRequest }
                                     );
                                     
                      FaxAssignment(strServiceChannelCD);
                  }
                  else if(strResult == "No")
                  {
                      sRequest = "vJobID=" + strJobID +
                                "&VJobStatus=" + "UnProcessed";
                      sProc = "uspHyperquestUpdateJobDetails";
                    
                      var aRequests = new Array();
                          aRequests.push( { procName : sProc,
                                        method   : "ExecuteSpNp",
                                        data     : sRequest }
                                     );
                  }
                    
                    
                }
                else if (strJobStatus == "PROCESSED")
                {
                  if (YesNoMessage("Confirm Fax Assignment", "Electronic assignment already sent.  Do you want to send the fax? Select Yes to send fax assignment.") == "Yes")
                    FaxAssignment(strServiceChannelCD);
                }
                else if (strJobStatus == "")
                {
                  SendAssignment(resendFlag,'');              
                }                
                window.location.reload();
              }
              else
                SendAssignment(resendFlag,'');
          }
      }
   }
  
  
  function FaxAssignment(strServiceChannelCD)
  {
  
      var strStaffAppraiser = "false";
        if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR" || (strServiceChannelCD == "RRP" && strServiceChannelCD == "LDAU")) {
            strStaffAppraiser = "true";
        }
       sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + "\" userID=\"" + gsUserID + "\" staffAppraiser=\""+ strStaffAppraiser +"\" hqAssignment=\"" + false + "\"/></Root>"
        //alert("calling com..\n" + sRequest);
        var objRet = XMLServerExecute("AssignShop.asp", sRequest);
        
        //alert(objRet.code);
        
        }

        function populateCounty() {
        if (oCitiesCache != null){
        var data;
        var city = $("#txtRepairLocationCity").val().toUpperCase();
        var state = txtRepairLocationState.value;
        var strAdded = "";

        for (var i = txtRepairLocationCounty.Options.length - 1; i >= 0 ; i--){
        txtRepairLocationCounty.RemoveItem(i);
        }
        for (var i = 0; i < oCitiesCache.length; i++){
            data = oCitiesCache[i].split("|");
            if (data.length == 3){
               data[2] = data[2].substr(0, data[2].length - 1);
               if (data[0] == city && data[1] == state && strAdded.indexOf(data[2] + "|") == -1){
                  try {
                  txtRepairLocationCounty.AddItem(data[2], data[2]);
                  strAdded += data[2] + "|";
                  } catch (e) {}
               }
            }
         }
         
         if (strShopLocationCounty != ""){//edit. populate the county from database. could be user entered.
            if (txtRepairLocationCounty.GetValueFromText(strShopLocationCounty) == null){
               txtRepairLocationCounty.AddItem(strShopLocationCounty, strShopLocationCounty + " [Override]");
            }
            txtRepairLocationCounty.value = strShopLocationCounty;
         } else {
            if (txtRepairLocationCounty.Options.length == 1) { 
               txtRepairLocationCounty.selectedIndex = 0;
            } else {
               if (lsCityData == null)
                  txtRepairLocationCounty.selectedIndex = -1;
               else
                  txtRepairLocationCounty.value = lsCityData[2];
            }
         }
      }
   }

   function zipResolve(objXML){
      if (objXML && objXML.xml){
         var objData = objXML.xml.selectSingleNode("//Result/Root/Detail");
         if (objData){
            txtRepairLocationCity.value = objData.getAttribute("City");
            txtRepairLocationState.value = objData.getAttribute("StateCode");
            lsCityData = [objData.getAttribute("City"), objData.getAttribute("StateCode"), objData.getAttribute("County")];
            $("#txtRepairLocationCity").trigger("blur");
         }
      }
   }
   
   function zipResolveFailed(){
   }
   
   function calculateNetAmt(){
      if ((strCurEstDocumentID != "") && (parseFloat(strCurEstGrossAmt) > 0) ){
         var dblDeductions = 0;
         if ((isNaN(strDedutiblesApplied) == false) && (strDedutiblesApplied != "")){
            dblDeductions += parseFloat(strDedutiblesApplied);
            txtDedAppliedAmt.value = "$ " + formatCurrencyString(strDedutiblesApplied);
         }
         if ((isNaN(strLimitsApplied) == false) && (strLimitsApplied != "")){
            txtLimitAppliedAmt.value = "$ " + formatCurrencyString(strLimitsApplied);
         }
         
         //alert(parseFloat(strCurEstGrossAmt) - dblDeductions);
         txtCurEstNetAmt.value = "$ " + formatCurrencyString(parseFloat(strCurEstGrossAmt) - dblDeductions);
      }
   }
   
   function reCalculateDedLimitsApplied(){
      var oCoveragesApplied = xmlCoveragesApplied.selectNodes("/Root/CoverageApplied");
      if (oCoveragesApplied && oCoveragesApplied.length > 0) {
         var dblDedutiblesApplied = 0;
         var dblLimitsApplied = 0;
         
         for (var i = 0; i < oCoveragesApplied.length; i++){
            dblDedutiblesApplied += parseFloat(oCoveragesApplied[i].getAttribute("DeductibleAppliedAmt"));
            dblLimitsApplied += parseFloat(oCoveragesApplied[i].getAttribute("LimitAppliedAmt"));
         }
      }
      strDedutiblesApplied = formatCurrencyString(dblDedutiblesApplied);
      strLimitsApplied = formatCurrencyString(dblLimitsApplied);
      
      calculateNetAmt();
   }
   
   function centerDiv(obj){
         with (obj){
            style.top = (document.body.offsetHeight - obj.offsetHeight) / 2;
            style.left = (document.body.offsetWidth - obj.offsetWidth) / 2;
            //style.visibility = "visible";
            //style.display = "none";
            firstChild.style.filter = "";
         }
   }
   
   function addCoverageApplied(){
      blnAddMode = true;
      divMaskAll.style.display = "inline";
      divCoverageApplied.style.display = "inline";
      //divCoverageApplied.firstChild.style.display = "inline";
      centerDiv(divCoverageApplied);

      selCoverage.CCDisabled = txtDeductible.CCDisabled = 
         txtLimit.CCDisabled = chkPartialCoverageFlag.CCDisabled = false;
      selCoverage.selectedIndex = -1;
      txtDeductible.value = "";
      txtLimit.value = "";
      chkPartialCoverageFlag.value = 0;      
   }
   
   function editCoverageApplied(){
      var oSelRow = grdCoverages.selectedRow;
      if (oSelRow && oSelRow.objXML){
         var oXML = oSelRow.objXML;
         if (oXML){
            divMaskAll.style.display = "inline";
            divCoverageApplied.style.display = "inline";
            //divCoverageApplied.firstChild.style.display = "inline";
            centerDiv(divCoverageApplied);
            
            selCoverage.CCDisabled = txtDeductible.CCDisabled = 
               txtLimit.CCDisabled = chkPartialCoverageFlag.CCDisabled = false;
            
            var strClaimCoverageID = oXML.getAttribute("ClaimCoverageID");
            
            selCoverage.value = strClaimCoverageID;
            txtLimit.value = oXML.getAttribute("LimitAppliedAmt");
            chkPartialCoverageFlag.value = oXML.getAttribute("PartialCoverageFlag");

            selCoverage.CCDisabled = true;
         }
      } else {
         ClientWarning("Please select a row to edit");
      }
   }
   
   function delCoverageApplied(){
      var oSelRow = grdCoverages.selectedRow;
      if (oSelRow && oSelRow.objXML){
         var oXML = oSelRow.objXML;
         if (oXML){
               if (YesNoMessage("Confirm Delete", "Do you want to delete the selected coverage?. Click Yes to delete.") == "Yes"){               
               //var oXML = oSelRow.objXML;
                  var strClaimCoverageID = "";
                  var sRequest = "";
                  var sProc = "uspClaimAspectServiceChannelCoverageDel";
                  strClaimCoverageID = oXML.getAttribute("ClaimCoverageID");
                  var iDeductibleAppliedAmt = parseFloat(oXML.getAttribute("DeductibleAppliedAmt"));
                  var iLimitAppliedAmt = parseFloat(oXML.getAttribute("LimitAppliedAmt"));
                  
                  if (isNaN(iDeductibleAppliedAmt)) iDeductibleAppliedAmt = 0;
                  if (isNaN(iLimitAppliedAmt)) iLimitAppliedAmt = 0;

                  sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                             "&ClaimCoverageID=" + strClaimCoverageID +
                             "&DocumentID=" + strCurEstDocumentID +
                             "&UserID=" + gsUserID;
                  //alert(sRequest);
                  //return;
                  var aRequests = new Array();
                  aRequests.push( { procName : sProc,
                                    method   : "ExecuteSpNp",
                                    data     : sRequest }
                                );
                  var sXMLRequest = makeXMLSaveString(aRequests);
                  var objRet = XMLSave(sXMLRequest);
                  if (objRet && objRet.code == 0 && objRet.xml) {
                     /*var oRoot = xmlCoveragesApplied.selectSingleNode("/Root");
                     if (oRoot) {
                        oRoot.removeChild(oXML);
                        grdCoverages.refreshData();
                        
                        reCalculateDedLimitsApplied();
                     }*/
                     //update the claim coverage with the changes
                     if (typeof(parent.updateCoveragesAppliedClaim) == "function") {
                        parent.updateCoveragesAppliedClaim(strClaimCoverageID, (-1 * iDeductibleAppliedAmt), (-1 * iLimitAppliedAmt))
                     }
                     
                     if (typeof(parent.refreshVehicle) == "function") {
                        parent.refreshVehicle();
                        return;
                     } else
                        document.location.reload();
                     
                  } else {               
                    document.location.reload();
                  }
               }                  
         }
      }
   }
   
   function saveCoverage(){
      var iAvailableDedAmt = 0;
      var iTotalClaimCoverageDedApplied = 0;
      var strMessage = "";
      var strSysLastUpdatedDate = "";
      var oXML;
      
      var oSelRow = grdCoverages.selectedRow;
      if (oSelRow && oSelRow.objXML){
         oXML = oSelRow.objXML;
         if (oXML){
            strSysLastUpdatedDate = oXML.getAttribute("SysLastUpdatedDate");
         }
      } else {
         if (blnAddMode == false){
            var oCoverageApplied = xmlCoveragesApplied.selectSingleNode("/Root/CoverageApplied[@ClaimCoverageID='" + selCoverage.value + "']");
            if (oCoverageApplied)
               strSysLastUpdatedDate = oCoverageApplied.getAttribute("SysLastUpdatedDate");
         }
      }
      if (selCoverage.selectedIndex == -1){
         ClientWarning("Please select a coverage from the list.");
         return;
      }
      
      var oClaimCoverages = xmlClaimCoverages.selectNodes("/Root/ClaimCoveragesApplied[@ClaimCoverageID='" + selCoverage.value + "' and @ClaimAspectServiceChannelID != '" + gsClaimAspectServiceChannelID + "']");
      
      if (oClaimCoverages && oClaimCoverages.length > 0){
         for (var i = 0; i <  oClaimCoverages.length; i++){
            iTotalClaimCoverageDedApplied += parseFloat(oClaimCoverages[i].getAttribute("DeductibleAppliedAmt"));
            var strCoverageName = "";
            var oClaimCoverage = xmlReference.selectSingleNode("/Root/Reference[@List = 'ClaimCoverage' and @ReferenceID = '" + oClaimCoverages[i].getAttribute("ClaimCoverageID")+ "']");
            if (oClaimCoverage){
               strCoverageName = oClaimCoverage.getAttribute("Name");
               var strClientCoverageTypeID = oClaimCoverage.getAttribute("ClientCoverageTypeID");
               if (strCoverageName == ""){
                  var oClientCoverageType = xmlReference.selectSingleNode("/Root/Reference[@List = 'ClientCoverageType' and @ReferenceID = '" + strClientCoverageTypeID + "']");
                  if (oClientCoverageType){
                     strCoverageName = oClientCoverageType.getAttribute("Name");
                  }
               }
            }
            strMessage += strCoverageName + " [" + oClaimCoverages[i].getAttribute("PertainsTo") + " / " + oClaimCoverages[i].getAttribute("ServiceChannelName") + "] = " + oClaimCoverages[i].getAttribute("DeductibleAppliedAmt") + " (-)<br/>";
         }
      }
      
      if (parseFloat(txtDeductible.value) > (iTotalDeductible - iTotalClaimCoverageDedApplied)){
         ClientWarning("New Deductible amount cannot be more than $" + (iTotalDeductible - iTotalClaimCoverageDedApplied) + "<br/>" + "Total Deductible: $" + iTotalDeductible + "<br/>" + strMessage);
         return;
      }
      
      var sProc, sRequest;
      
      sProc = "uspClaimAspectServiceChannelCoverageUpdDetail";
      sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                 "&ClaimCoverageID=" + selCoverage.value +
                 "&DocumentID=" + strCurEstDocumentID +
                 "&DeductibleAppliedAmt=" + txtDeductible.value +
                 "&LimitAppliedAmt=" + txtLimit.value +
                 "&PartialCoverageFlag=" + chkPartialCoverageFlag.value +
                 "&UserID=" + gsUserID +
                 "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
                 
      //if (blnAddMode != true){
         //sRequest += "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
      //}
      
      //alert(sRequest);
      //return;
      
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                      method   : "ExecuteSpNpAsXML",
                      data     : sRequest }
                  );
      var objRet = XMLSave(makeXMLSaveString(aRequests));
      if (objRet && objRet.code == 0 && objRet.xml) {
         var oRetXML = objRet.xml;
         if (oRetXML){
            //refresh the vehicle so that all other service channel coverage applied can be upto date.
            if (typeof(parent.refreshVehicle) == "function") {
               parent.refreshVehicle();
               return;
            } else
               document.location.reload();
         }
      }
      
      divMaskAll.style.display = "none";
      divCoverageApplied.style.display = "none";
      blnAddMode = false;
   }
   
   function cancelCoverage(){
      divMaskAll.style.display = "none";
      divCoverageApplied.style.display = "none";
      blnAddMode = false;
   }
   
   function getTotalDeductible(){
      txtTotalDeductible.CCDisabled = false;
      txtDeductibleApplied.CCDisabled = false;

      iTotalDeductible = 0;
      iTotalDeductibleApplied = 0;

      var oCoverage = xmlCoveragesApplied.selectSingleNode("/Root/CoverageApplied[@ClaimCoverageID='" + selCoverage.value + "']")

      if (oCoverage) 
         blnAddMode = false;
      else
         blnAddMode = true;
      
      var oClaimCoverage = xmlReference.selectSingleNode("/Root/Reference[@List='ClaimCoverage' and @ReferenceID='" + selCoverage.value + "']");      
      if (oClaimCoverage){
         txtTotalDeductible.value = oClaimCoverage.getAttribute("DeductibleAmt");

         iTotalDeductible = parseFloat(oClaimCoverage.getAttribute("DeductibleAmt"));
         iTotalLimit = parseFloat(oClaimCoverage.getAttribute("LimitAmt"));
         var iDedApplied = 0;
         
         //var oAppliedNodes = xmlCoveragesApplied.selectNodes("/Root/CoverageApplied[@ClaimCoverageID='" + selCoverage.value + "']");
         var oAppliedNodes = xmlClaimCoverages.selectNodes("/Root/ClaimCoveragesApplied[@ClaimCoverageID='" + selCoverage.value + "' and @ClaimAspectServiceChannelID != '" + gsClaimAspectServiceChannelID + "']");
         if (oAppliedNodes && oAppliedNodes.length > 0){
            for (var i = 0; i < oAppliedNodes.length; i++){
               iDedApplied = oAppliedNodes[i].getAttribute("DeductibleAppliedAmt");
               iTotalDeductibleApplied += (isNaN(iDedApplied) ? 0 : parseFloat(iDedApplied));
            }
         }
         
         if (blnAddMode == true){
            txtDeductible.value = iTotalDeductible - iTotalDeductibleApplied;
            txtDeductibleApplied.value = iTotalDeductibleApplied;
         } else {
            if (oCoverage) {
               txtDeductible.value = oCoverage.getAttribute("DeductibleAppliedAmt");
               txtDeductibleApplied.value = iTotalDeductibleApplied;
            }
         }
      } else {
         txtTotalDeductible.value = "";
         txtDeductibleApplied.value = "";
      }
      txtTotalDeductible.CCDisabled = true;
      txtDeductibleApplied.CCDisabled = true;
   }
   
   function viewHistory(){
      divMaskAll.style.display = "inline";
      divAssignmentHistory.style.display = "inline";
      centerDiv(divAssignmentHistory);
   }
   
   function closeAssignmentHistory(){
      divMaskAll.style.display = "none";
      divAssignmentHistory.style.display = "none";
   }
   
   function saveAssignment(){
      var blnNeedRefresh = false;
      if (validateRepairDates() == false) {
         if (parent.stat1){
            parent.stat1.Hide();
         }
         return;
      }
      
      if (strServiceChannelCD == "DA" ||
          strServiceChannelCD == "DR"){
         if (txtRepairLocationCity.value == "" ||
             txtRepairLocationState.selectedIndex == -1 ||
             txtRepairLocationCounty.value == ""){
            ClientWarning("Repair City, State and County are required.");
            if (parent.stat1){
               parent.stat1.Hide();
            }
            return;
         }
      }
      
      if (strAssignmentTypeId == "15")
      {
         if (txtRepairShopName.value == ""){
            ClientWarning("Shop Name is required.");
            if (parent.stat1) parent.stat1.Hide();
            return;
         }
      }
       
      if (document.getElementById("txtRepairStartDate") && document.getElementById("txtRepairEndDate"))
         blnNeedRefresh = (txtRepairStartDate.isDirty || txtRepairEndDate.isDirty);
         
      if (RepairScheduleChangeReasonID.value != "" || RepairScheduleChangeReason.value != "")
         blnNeedRefresh = true;
         
      if (parseFloat(txtEffDedSent.value) > 1000){
         if (YesNoMessage("Confirm Effective Deductible", "Effective Deductible Sent value is greater than $1000. Is this value correct?") != "Yes"){
            return;
         }
      }
         
      var sProc, sRequest;
      sProc = "uspAssignmentUpdRemarks";
      sRequest = "AssignmentID=" + gsAssignmentID +
                "&AssignmentRemarks=" + escape(txtAssignmentRemarks.value) + 
                "&EffectiveDeductibleSentAmt=" + txtEffDedSent.value +
                "&DispositionTypeCD=" + (selDispositionCD.selectedIndex == -1 ? "" : selDispositionCD.value);
                
      switch (strServiceChannelCD){
         case "DA": //no key dates for DA
            sRequest += "&RepairLocationCity=" + txtRepairLocationCity.value +
                        "&RepairLocationCounty=" + txtRepairLocationCounty.value +
                        "&RepairLocationState=" + txtRepairLocationState.value;
            break;
         case "DR": //no key dates for DR
            sRequest += "&RepairLocationCity=" + txtRepairLocationCity.value +
                        "&RepairLocationCounty=" + txtRepairLocationCounty.value +
                        "&RepairLocationState=" + txtRepairLocationState.value;
            break;
         case "GL":
            sRequest += "&WorkStartDate=" + txtRepairStartDate.value +
                        "&WorkEndDate=" + txtRepairEndDate.value +
                        "&CashOutDate=" + txtCashOutDate.value + 
                        "&AppraiserInvoiceDate=" + txtInvoiceReceivedDate.value + 
                        "&RepairScheduleChangeReasonID=" + RepairScheduleChangeReasonID.value + 
                        "&RepairScheduleChangeReason=" + escape(RepairScheduleChangeReason.value);
            break;
         case "ME":
            sRequest += "&WorkStartDate=" + txtRepairStartDate.value +
                        "&WorkEndDate=" + txtRepairEndDate.value +
                        "&CashOutDate=" + txtCashOutDate.value + 
                        "&AppraiserInvoiceDate=" + txtInvoiceReceivedDate.value + 
                        "&RepairScheduleChangeReasonID=" + RepairScheduleChangeReasonID.value + 
                        "&RepairScheduleChangeReason=" + escape(RepairScheduleChangeReason.value);
            break;
         case "PS":
            sRequest += "&WorkStartDate=" + txtRepairStartDate.value +
                        "&WorkEndDate=" + txtRepairEndDate.value +
                        "&CashOutDate=" + txtCashOutDate.value + 
                        "&InspectionDate=" + txtInspectionDate.value + 
                        "&AppraiserInvoiceDate=" + txtInvoiceReceivedDate.value + 
                        "&RepairScheduleChangeReasonID=" + RepairScheduleChangeReasonID.value + 
                        "&RepairScheduleChangeReason=" + escape(RepairScheduleChangeReason.value);
            break;
         case "CS":
            sRequest += "&WorkStartDate=" + txtRepairStartDate.value +
                        "&WorkEndDate=" + txtRepairEndDate.value +
                        "&CashOutDate=" + txtCashOutDate.value + 
                        "&InspectionDate=" + txtInspectionDate.value + 
                        "&AppraiserInvoiceDate=" + txtInvoiceReceivedDate.value + 
                        "&RepairScheduleChangeReasonID=" + RepairScheduleChangeReasonID.value + 
                        "&RepairScheduleChangeReason=" + escape(RepairScheduleChangeReason.value);
            break;
         case "RRP":
            sRequest += "&InspectionDate=" + txtInspectionDate.value + 
                        "&SourceApplicationPassThruData=" + 
                        "&RepairScheduleChangeReasonID=" + RepairScheduleChangeReasonID.value + 
                        "&RepairScheduleChangeReason=" + escape(RepairScheduleChangeReason.value);
            break;
      }
      
      if (strAssignmentTypeId == "15"){
         var delimiter = String.fromCharCode(255);
         var sShopInfo = txtRepairShopName.value + delimiter + 
                                 txtRepairShopAddress.value + delimiter +
                                 txtRepairLocationZip.value + delimiter +
                                 txtRepairLocationCity.value + delimiter +
                                 txtRepairLocationState.value + delimiter +
                                 txtRepairLocationCounty.value + delimiter +
                                 txtRepairShopPhone.value + delimiter +
                                 txtRepairShopFax.value + delimiter +
                                 txtRepairShopEmailAddress.value + delimiter +
                                 txtPursuitInspectionDate.value;

         sRequest += "&InspectionDate=" + txtPursuitInspectionDate.value +
                     "&SourceApplicationPassThruData=" + escape(sShopInfo);
         
      }
      
      
      sRequest += "&UserID=" + gsUserID +
                  "&SysLastUpdatedDate=" + gsAssignmentLastUpdatedDate ;
      
      //alert(sRequest); return;
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                      method   : "executespnpasxml",
                      data     : sRequest }
                  );
      var sXMLRequest = makeXMLSaveString(aRequests);

      var objRet = XMLSave(sXMLRequest);
      if (objRet && objRet.code == 0 && objRet.xml) {
         var oRetXML = objRet.xml;
         if (oRetXML){
            
            if (blnNeedRefresh) {
                //refresh the diary and notes
                if (typeof(top.refreshNotesWindow) == "function")
                  window.setTimeout("top.refreshNotesWindow( top.vLynxID, top.vUserID)", 100);
                if (typeof(top.refreshCheckListWindow) == "function")
                  window.setTimeout("top.refreshCheckListWindow( top.vLynxID, top.vUserID )", 200);

               window.setTimeout("window.document.location.reload()", 300);
               return;
            }
            
            gsAssignmentLastUpdatedDate = oRetXML.selectSingleNode("//Root/Assignment/@SysLastUpdatedDate").text;
            //alert(gsAssignmentLastUpdatedDate);
            
            txtAssignmentRemarks.contentSaved();
            txtEffDedSent.contentSaved();
            selDispositionCD.contentSaved();
            switch (strServiceChannelCD){
               case "DA": //no key dates for DA
                  txtRepairLocationCity2.contentSaved();
                  txtRepairLocationCounty.contentSaved();
                  txtRepairLocationState.contentSaved();
                  document.location.reload();
                  return;
                  break;
               case "DR": //no key dates for DR
                  txtRepairLocationCity2.contentSaved();
                  txtRepairLocationCounty.contentSaved();
                  txtRepairLocationState.contentSaved();
                  document.location.reload();
                  return;
                  break;
               case "GL":
                  txtRepairStartDate.contentSaved();
                  txtRepairEndDate.contentSaved();
                  txtCashOutDate.contentSaved();
                  txtInvoiceReceivedDate.contentSaved();
                  break;
               case "ME":
                  txtRepairStartDate.contentSaved();
                  txtRepairEndDate.contentSaved();
                  txtCashOutDate.contentSaved();
                  txtInvoiceReceivedDate.contentSaved();
                  break;
               case "PS":
                  txtRepairStartDate.contentSaved();
                  txtRepairEndDate.contentSaved();
                  txtCashOutDate.contentSaved();
                  txtInspectionDate.contentSaved();
                  txtInvoiceReceivedDate.contentSaved();
                  break;
               case "CS":
                  txtRepairStartDate.contentSaved();
                  txtRepairEndDate.contentSaved();
                  txtCashOutDate.contentSaved();
                  txtInspectionDate.contentSaved();
                  txtInvoiceReceivedDate.contentSaved();
                  break;
               case "RRP":
                  txtInspectionDate.contentSaved();
                  break;
            }
            
         }
      }
      
      if (parent.stat1){
         parent.stat1.Hide();
      }
   }
   
   function showMoreRemarks(){
      divMaskAll.style.display = "inline";
      divAssignmentRemarksMore.style.display = "inline";
      centerDiv(divAssignmentRemarksMore);
      strAssignmentRemarks = txtAssignmentRemarks.value;
      if (txtAssignmentRemarks.value == ""){
         var strTemplateID = "";
         if (strDrivable == "1"){
            if (strRentalCoverage == "1"){
               strTemplateID = selShopRemarksTemplate.GetValueFromText("Drivable Vehicle with Rental Coverage");
               selRentalCompany.CCDisabled = false;
            } else {
               selRentalCompany.selectedIndex = -1;
               //if (gsInsuranceCompanyID != "158") selRentalCompany.CCDisabled = true;
               selRentalCompany.CCDisabled = false;
               strTemplateID = selShopRemarksTemplate.GetValueFromText("Drivable Vehicle without Rental Coverage");
            }
         } else {
            if (strRentalCoverage == "1"){
               strTemplateID = selShopRemarksTemplate.GetValueFromText("Non-Drivable Vehicle with Rental Coverage");
               selRentalCompany.CCDisabled = false;
            } else {
               selRentalCompany.selectedIndex = -1;
               //selRentalCompany.CCDisabled = true;
               selRentalCompany.CCDisabled = false;
               strTemplateID = selShopRemarksTemplate.GetValueFromText("Non-Drivable Vehicle without Rental Coverage");
            }
         }
         if (strTemplateID != ""){
            selShopRemarksTemplate.value = strTemplateID;
         }
      } else {
         selShopRemarksTemplate.selectedIndex = -1;
         selRentalCompany.selectedIndex = -1;
         txtAssignmentRemarksMore.value = txtAssignmentRemarks.value;
         txtAssignmentRemarksMore.setFocus();
      }
   }
   
   function shopTemplate(){
      if (selShopRemarksTemplate.selectedIndex != -1){
         strTemplateID = selShopRemarksTemplate.value;
         var objTemplate = xmlShopRemarkTemplates.selectSingleNode("//Reference[@ReferenceID='" + strTemplateID + "']");
         if (objTemplate) {
            var strDescription = objTemplate.getAttribute("Name");
            showMessage(strDescription);
         }
      }
   }

   function showMessage(strMessageTemplate){
      var strMessageTemplatePath = strMessageTemplate.split("|")[1];
      xmlMessageTemplate.src = strMessageTemplatePath;
   }

   function messageTemplateReady(){
      if ((xmlMessageTemplate.readyState != "complete")){
         window.setTimeout("messageTemplateReady()", 100);
         return;
      }
      
      if (selShopRemarksTemplate.selectedIndex != -1){
      
         xmlShopTemplateData.async = false;
         xmlShopTemplateData.loadXML("<Claim/>");
        
         var oRoot = xmlShopTemplateData.selectSingleNode("/Claim");
         oRoot.setAttribute("rental", selRentalCompany.text);
         oRoot.setAttribute("InsuranceCompanyName", strInsuranceCompanyName);
         oRoot.setAttribute("InsuredClaimant", strInsuredClaimantName);
         oRoot.setAttribute("ClaimOwnerName", strClaimOwnerName);
         oRoot.setAttribute("ClaimOwnerPhone", strClaimOwnerPhone);
         var strResult = xmlShopTemplateData.transformNode(xmlMessageTemplate);
         //alert(strResult);
         if (strResult != ""){
            txtAssignmentRemarksMore.value = (strAssignmentRemarks != "" ? strAssignmentRemarks + "\n" : "") + strResult;
            //blnMessageTemplateReady = true;
         }
      }
   }
   
   function closeMoreRemarks(){
      txtAssignmentRemarks.value = txtAssignmentRemarksMore.value;
      divMaskAll.style.display = "none";
      divAssignmentRemarksMore.style.display = "none";
   }
   
   function validateRepairDates(){
      if (strServiceChannelCD == "GL" ||
          strServiceChannelCD == "ME" ||
          strServiceChannelCD == "CS" ||
          strServiceChannelCD == "PS") {
         if (txtRepairStartDate.value == "" && txtRepairEndDate.value != "") {
            ClientWarning("Repair start date not specified. Please try again.");
            return false;
         }
         if (txtRepairStartDate.value !="" && txtRepairEndDate.value != "") {
            var dtStart = new Date(txtRepairStartDate.value);
            var dtEnd = new Date(txtRepairEndDate.value);
            if (dtEnd < dtStart) {
               ClientWarning("Repair end date cannot be before the repair start date. Please try again.");
               return false;
            }
         }
         
         if (txtRepairStartDate.date != null || txtRepairEndDate.date != null) {
            var dtOldStart = new Date(strRepairStartDate);
            var dtOldEnd = new Date(strRepairEndDate);
            var dtNewStart = txtRepairStartDate.date;
            var dtNewEnd = txtRepairEndDate.date;
            var dtLoss = new Date(strLossDate);
            
            if (dtNewStart && (dtNewStart.setHours(23, 59, 59) < dtLoss)){
               ClientWarning("Repair start date cannot be before the loss date.");
               return false;
            }
            if (dtNewEnd && (dtNewEnd.setHours(23, 59, 59) < dtLoss)){
               ClientWarning("Repair end date cannot be before the loss date.");
               return false;
            }
            
            if ((strRepairStartDate != "" && txtRepairStartDate.date.toString() != dtOldStart.toString()) ||
                (strRepairEndDate != "" && txtRepairEndDate.date.toString() != dtOldEnd.toString())) {
               if (RepairScheduleChangeReason.value == "") {
               //event.returnValue=false;
               //alert("show reason");
                  var strReturn = window.showModalDialog("/RepairChangeReason.asp?ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID, "","dialogHeight: 161px; dialogWidth: 332px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
                  //if (strReturn == undefined) return false;
                  /*while (typeof(strReturn) != "string") {
                     strReturn = window.showModalDialog("/RepairChangeReason.asp?ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID, "","dialogHeight: 161px; dialogWidth: 332px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
                  }*/
                  if ((typeof(strReturn) != "string") || (strReturn.Trim() == "")){
                     ClientWarning("You must enter a reason for the date change.");
                     return false;
                  }
                  var strParts = strReturn.split(String.fromCharCode(0));
                  RepairScheduleChangeReasonID.value = strParts[0];
                  RepairScheduleChangeReason.value = strParts[1];
                  if (strReturn.Trim() == "")
                     return false;
                  blnRepairReason = true;
               }
            }
         }
      }
      
      return true;
   }
   
  function confirmRepairStart() {
    if (txtRepairStartDate.isDirty == true || txtRepairEndDate.isDirty == true){
      ClientWarning("Please save the changes before confirming the Repair Start Date.");
      return;
    }
    
    if (txtRepairEndDate.value == ""){
      ClientWarning("Please enter a Repair End Date.");
      return;
    }
    
    var strRet = YesNoMessage("Confirm Repair Start", "Are you sure the repair on this vehicle has started. Once confirmed the repair start date cannot be changed.");
    switch (strRet) {
      case "Yes":
        var sProc;
        var sRequest;
        sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID + "&";
        sRequest += "RepairDateCD=S&";
        sRequest += "UserID=" + gsUserID;
        
        sProc = "uspRepairConfirmUpdDetail";
        
        var aRequests = new Array();
        aRequests.push( { procName : sProc,
                          method   : "executespnp",
                          data     : sRequest }
                      );
        //alert(makeXMLSaveString(aRequests));
        //return;
        var objRet = XMLSave(makeXMLSaveString(aRequests));
        if (objRet.code == 0)
          reloadPage();
        else
          return false;
        
        break;
    }
    
  }

  function confirmRepairEnd() {
    if (txtRepairEndDate.isDirty == true){
      ClientWarning("Please save the changes and try again.");
      return;
    }
    var strRet = YesNoMessage("Confirm Repair Completion", "Are you sure the repair on this vehicle has completed. Once confirmed the repair end date cannot be changed.");
    switch (strRet) {
      case "Yes":
        var sProc;
        var sRequest;
        sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID + "&";
        sRequest += "RepairDateCD=E&";
        sRequest += "UserID=" + gsUserID;
        
        sProc = "uspRepairConfirmUpdDetail";
        
        var aRequests = new Array();
        aRequests.push( { procName : sProc,
                          method   : "executespnp",
                          data     : sRequest }
                      );
        //alert(makeXMLSaveString(aRequests));
        //return;
        var objRet = XMLSave(makeXMLSaveString(aRequests));
        if (objRet.code == 0)
          reloadPage();
        else
          return false;
        
        break;
    }
  }

  function reloadPage(){
      if (parent.stat1) {
	  
         parent.stat1.Show("Please wait...");
      }
      window.setTimeout("reloadPage2()", 100);
  }
  
  function reloadPage2(){
    if (typeof(top.refreshNotesWindow) == "function")
      top.refreshNotesWindow( top.vLynxID, top.vUserID );
    if (typeof(top.refreshCheckListWindow) == "function")
      top.refreshCheckListWindow( top.vLynxID, top.vUserID );
    document.location.reload();
  }
  function SelectShop()
  {
    if (strServiceChannelCD == "PS" || strServiceChannelCD == "CS") {
      //if (btnSelectShop) btnSelectShop.CCDisabled = true;
      
      window.setTimeout("ShopSearch()", 100)
    } 
    else if (strServiceChannelCD == "RRP") {
      //if (btnSelectShop) btnSelectShop.CCDisabled = true;
      
      window.setTimeout("ShopSearch()", 100)
    } 
    else if (strServiceChannelCD == "DA") {
      if (parent.stat1){
         parent.stat1.Show("Please wait...");
      }
	  
	  //alert('here');
	  
      window.setTimeout("saveLDAU()", 100)
	  
	  //alert('after');
	  
    } else if (strServiceChannelCD == "IA" || strServiceChannelCD == "ME"){
      if (parent.stat1){
         parent.stat1.Show("Please wait...");
      }
      window.setTimeout("saveIA()", 100);
    } else if (strServiceChannelCD == "GL"){
      if (parent.stat1){
         parent.stat1.Show("Please wait...");
      }
      window.setTimeout("submit2Glass()", 100);
    }
  }
  
  function saveLDAU(){
      doAssignment(gsLDAUID);
  }

  function saveIA(){
      doAssignment(gsMEID);
  }
  
  function doAssignment(strAppraiserID){
    var sProc, sRequest;
    if (btnSelectShop) btnSelectShop.CCDisabled = true;

    sRemarks = escape(txtAssignmentRemarks.value);

    //select LDAU now
    sRequest = "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
               "&SelectOperationCD=S&UserId=" + gsUserID +
                "&AppraiserID=" + strAppraiserID +
                "&AssignmentRemarks=" + sRemarks +
                "&NotifyEvent=1";
                         
    //alert(sRequest); return;

     sProc = "uspWorkFlowSelectShop";
     
     var aRequests = new Array();
     aRequests.push( { procName : sProc,
                       method   : "executespnp",
                       data     : sRequest }
                   );
     //alert(makeXMLSaveString(aRequests));
     //return;
     var objRet = XMLSave(makeXMLSaveString(aRequests));
     if (objRet.code == 0 && objRet.xml) {
         var oRetXML = objRet.xml;
         //alert(oRetXML.xml);
         var oResult = oRetXML.selectSingleNode("//Result");
         if (oResult) {
            gsAssignmentID = oResult.text;
            //alert(gsAssignmentID);
          
            //now assignment LDAU
            sProc = "uspWorkflowAssignShop";
           
            sRequest = "AssignmentID=" + gsAssignmentID;
           
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                             method   : "executespnp",
                             data     : sRequest }
                         );
           //alert(sRequest);
           //alert(makeXMLSaveString(aRequests));
           //return;
            var objRet = XMLSave(makeXMLSaveString(aRequests));
            if (objRet.code == 0) {
               //now send the assigment
               sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + "\" userID=\"" + gsUserID + "\" staffAppraiser=\"true\" /></Root>"
               //alert("calling com..\n" + sRequest);
               var objRet = XMLServerExecute("AssignShop.asp", sRequest);
               if (objRet.code == 0){
                  var strRet;
                  if (objRet.xml){
                     var oResult = objRet.xml.selectSingleNode("/Root/Success");
                     if (oResult){
                        var strStatus = oResult.text;

                        if (strStatus.substring(0, 1) == "F")  ClientInfo("Electronic assignment to shop failed.");
                        if (strStatus.substring(1, 1) == "F") ClientInfo("Fax assignment to shop failed.");
                        
                        document.location.reload();
                     }
                  } else {
                     ClientWarning("Assignment failed.");
                  }
               }
            }
         }

     } else
       return false;
  }
  
   function ShopSearch() {
      sShopRemarks = escape(txtAssignmentRemarks.value);
      if (typeof(parent.disableAssignmentButtons) == "function"){
         parent.disableAssignmentButtons(true);
      }
      
      var sZIP = sCTY = sST = "";
      
	   if ( (parent.lCTY).Trim() != "" ) { 
         sZIP = parent.lZIP; sCTY = parent.lCTY; sST = parent.lST; 
      } else if ( (parent.lZIP).Trim() != "") { 
         sZIP = parent.lZIP; sCTY = parent.lCTY; sST = parent.lST; 
      }
      
      var sAssignmentTo;
      switch (strServiceChannelCD){
         case "DA":
            sAssignmentTo = "LDAU";
            break;
         case "ME":
            sAssignmentTo = "IA";
            break;
         case "PS":
            sAssignmentTo = "SHOP";
            break;
         case "CS":
            sAssignmentTo = "SHOP";
            break;
         case "RRP":
            sAssignmentTo = "SHOP" ;
            break;
      }
      
      var strURL = "ShopLookup.asp?AreaCode=" + parent.sAC + 
                    "&ExchangeNumber=" + parent.sEX + "&ZipCode=" + (isNaN(sZIP) ? "" : sZIP) + "&City=" + (sCTY == "" ? "" : sCTY) + "&State=" + sST +
                    "&UserID=" + gsUserID + "&LynxID=" + gsLynxID + "&VehicleNumber=" + parent.gsVehNum +
                    "&ClaimAspectID=" + gsClaimAspectID + "&ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + 
                    "&LocationName=" + (parent.LocationName.value == undefined ? "" : parent.LocationName.value) + "&AssignmentTo=" + sAssignmentTo +
                    "&getUser=false" + "&ShopRemarks=" + sShopRemarks;
      window.showModalDialog(strURL, "", "dialogHeight:400px;dialogWidth:720px;center:yes;resizable:no;status:no");
      window.location.reload();
      //parent.AssignShop(sShopRemarks, AssignmentTo.value);
      
      //disableButtons(false);
   }
  
  function disableButtons(bDisabled){
    var objAssign = document.getElementById("btnAssignment");
    if (objAssign)
      objAssign.CCDisabled = bDisabled;

    var objSelectShop = document.getElementById("btnSelectShop");
    if (objSelectShop)
      objSelectShop.CCDisabled = bDisabled;
    
    if (typeof(parent.disableButton) == "function"){
       parent.disableButton("btnAssignmentHistory_", bDisabled);
       parent.disableButton("btnSaveAssignment_", bDisabled);
    }
  }
  
  function ShopOnClick(){
    if (gsAssignmentTypeCD == "SHOP" || strServiceChannelCD == "RRP" )
      NavToShop("S", gsShopId, gsBusinessId);
  }
  
  function NavToShop(sSearchType, sEntityID, sShopBusinessID){
     // The parameter 'sShopBusinessID' is the Business (utb_shop) to which the Shop (utb_shop_location) belongs.  This is only needed for 
     // SearchType 'S'.  This is so the Business ID can be used in the window name rather than the ShopID.  This will prevent opening multiple 
     // sibling shops in different windows and then being able to change tabs in those windows possibly resulting in multiple windows with 
     // the same shop.  
      try{
         var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
         var sUrl = "ProgramMgr/SMT.asp?SearchType=" + sSearchType + "&EntityID=" + sEntityID;
         var sWinName;
         
         switch (sSearchType){
            case "S":
               sWinName = "SMTFrame_B" + sShopBusinessID;
               break;
            case "B":
               sWinName = "SMTFrame_B" + sEntityID;
               break;
            case "D":
               sWinName = "SMTFrame_D" + sEntityID;
               break;
            case "BusinessWiz":
            case "ShopWiz":
            case "DealerWiz":  
               ShowWizard(sSearchType);
               return;
               break;        
            default:
               throw "Invalid value was passed for sSearchType.  Value must be one of the following: 'S', 'B', or 'D'.";
         }
         
         if (sUrl != ""){      
            var oWin = window.open( sUrl, sWinName, sSettings );
            oWin.oCallingWin = top.window.self;  // send the new window a handle to this window.
            oWin.focus();
            //alert(oWin.oCallingWin.name);      
         }
      } catch ( e ){
         throw "VehicleShopAssignment.xsl:NavToShop( sSearchType='" + StrVal( sSearchType ) 
                                     + "', sEntityID='" + StrVal( sEntityID )
                                     + "', sShopBusinessID='" + StrVal( sShopBusinessID )
                                     + "' ) Error: " + e.message;
      }
   }
   
   function showToolTip(objSrc){  
      if (gsAssignmentID != "" && strServiceChannelCD == "PS" || strServiceChannelCD == "RRP" || strServiceChannelCD == "CS"){
         try {
         divShopDetail.style.display = "inline";
         bMouseInToolTip = true;         
         } catch (e) {}
      }
   }
   
   function hideToolTip(){
      if (bMouseInToolTip == false) {
        divShopDetail.style.display = "none";
      }
   }

    function keepToolTip(){
      bMouseInToolTip = true;
    }
    
    function getLocationInfo(){
      var strState;
      var strCity;
      var strCounty;
      if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR"){
         strState = txtRepairLocationState.value;
         strCity = txtRepairLocationCity.value;
         strCounty = txtRepairLocationCounty.value;
      } else {
         strState = strShopLocationState;
         strCity = strShopLocationCity;
         strCounty = strShopLocationCounty;
         
      }
      return {InsuranceCompanyID: gsInsuranceCompanyID, InsuranceCompanyName: strInsuranceCompanyName, State: strState, City: strCity, County: strCounty, ShopLocationID: gsShopId, ServiceChannelCD: strServiceChannelCD};
    }

    //function navigate(iLynxID, iUserID, iClaimAspectID, iClaimAspectServiceChannelID, iInsuranceCompanyID, sNavigate) {
    function navigate(iLynxID, iClaimAspectServiceChannelID) {
        //alert(iLynxID);
        alert(iClaimAspectServiceChannelID);
        //var strReq = "HQAssignmentDetails.aspx?LynxID=" + iLynxID + "&UserID=" + iUserID + "&ClaimAspectID=" + iClaimAspectID + "&ClaimAspectServiceChannelID=" + iClaimAspectServiceChannelID + "&InsuranceCompanyID=" + iInsuranceCompanyID;
        var strReq = "HQAssignmentDetails.aspx?LynxID=" + iLynxID + "&ClaimAspectServiceChannelID=" + iClaimAspectServiceChannelID;
        window.showModalDialog(strReq, "HQ Assignment Status", "dialogHeight:200px; dialogWidth:700px; resizable:no; status:no; help:no; center:yes;");
        return;
    }         
]]>

        </SCRIPT>

      </HEAD>
      <BODY unselectable="on" class="bodyAPDSubSub" onLoad1="window.setTimeout('pageInit()', 300)" tabIndex="-1" style="margin:0px;padding:0px;overflow:hidden">
        <xml id="xmlCoveragesApplied" name="xmlCoveragesApplied">
          <Root>
            <xsl:for-each select="/Root/ServiceChannel/CoverageApplied">
              <CoverageApplied>
                <xsl:copy-of select="@*"/>
                <xsl:variable name="ClaimCoverageID">
                  <xsl:value-of select="@ClaimCoverageID"/>
                </xsl:variable>
                <xsl:variable name="ClientCoverageTypeID">
                  <xsl:value-of select="/Root/Reference[@List='ClaimCoverage' and @ReferenceID = $ClaimCoverageID]/@ClientCoverageTypeID"/>
                </xsl:variable>
                <xsl:attribute name="CoverageName">
                  <xsl:if test="/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@ClientCode != ''">
                    <xsl:value-of select="concat(/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@ClientCode, ' - ')"/>
                  </xsl:if>
                  <xsl:choose>
                    <xsl:when test="/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@Name != ''">
                      <xsl:value-of select="/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@Name"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="/Root/Reference[@List='ClaimCoverage' and @ReferenceID = $ClaimCoverageID]/@SystemName"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="DeductibleAppliedConcat">
                  <xsl:value-of select="concat(@DeductibleAppliedAmt, ' / ', /Root/Reference[@List='ClaimCoverage' and @ReferenceID = $ClaimCoverageID]/@DeductibleAmt)"/>
                </xsl:attribute>
                <xsl:attribute name="LimitAppliedConcat">
                  <xsl:value-of select="concat(@LimitAppliedAmt, ' / ', /Root/Reference[@List='ClaimCoverage' and @ReferenceID = $ClaimCoverageID]/@LimitAmt)"/>
                </xsl:attribute>
                <xsl:attribute name="PartialCoverageFlagImg">
                  <xsl:choose>
                    <xsl:when test="@PartialCoverageFlag = '1'">/images/tick.gif</xsl:when>
                    <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </CoverageApplied>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlReference">
          <Root>
            <xsl:copy-of select="/Root/Reference"/>
          </Root>
        </xml>
        <xml id="xmlClaimCoverages">
          <Root>
            <xsl:copy-of select="/Root/ClaimCoveragesApplied"/>
          </Root>
        </xml>

        <xml id="xmlShopRemarkTemplates">
          <Root>
            <xsl:copy-of select="/Root/Reference[@List='ShopRemarkTemplates']"/>
          </Root>
        </xml>

        <xml id="xmlMessageTemplate" name="xmlMessageTemplate" ondatasetcomplete="messageTemplateReady()">
        </xml>

        <xml id="xmlShopTemplateData" name="xmlShopTemplateData" />


        <xml id="xmlHistory" name="xmlHistory">
          <Root>
            <xsl:for-each select="/Root/ServiceChannel/Assignments">
              <Assignment>
                <xsl:copy-of select="@*"/>
                <xsl:attribute name="AssignmentDateFormatted">
                  <xsl:value-of select="js:formatSQLDateTime(string(@AssignmentDate))"/>
                </xsl:attribute>
                <xsl:attribute name="CancellationDateFormatted">
                  <xsl:value-of select="js:formatSQLDateTime(string(@CancellationDate))"/>
                </xsl:attribute>
                <xsl:attribute name="SelectionDateFormatted">
                  <xsl:value-of select="js:formatSQLDateTime(string(@SelectionDate))"/>
                </xsl:attribute>
              </Assignment>
            </xsl:for-each>
          </Root>
        </xml>

        <xsl:call-template name="ShopAssignInfo">
          <xsl:with-param name="AdjVehicleCRUD" select="$AdjVehicleCRUD"/>
          <xsl:with-param name="ShopCRUD" select="$ShopCRUD"/>
        </xsl:call-template>
        <div id="divMaskAll" name="divMaskAll" style="display:none;position:absolute;top:0px;left:0px;height:100%;width:100%;background-color:#000000;FILTER: progid:DXImageTransform.Microsoft.Alpha( style=0,opacity=25);">
        </div>

        <div id="divCoverageApplied" name="divCoverageApplied" style="display:none;position:absolute;top:100px;left:100px;height:100px;width:280px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
          <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
            <colgroup>
              <col width="150px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td colspan="2" style="font-weight:bold">
                Apply Coverage
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold">
                <img src="images/background_top.gif" style="height:2px;width:125px"/>
                <input type="hidden" id="txtClaimCoverageID"/>
              </td>
            </tr>
            <tr>
              <td>Coverage:</td>
              <td>
                <IE:APDCustomSelect id="selCoverage" name="selCoverage" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="" required="true" width="115" dropDownWidth="" onChange="getTotalDeductible()">
                  <xsl:for-each select="/Root/Reference[@List='ClaimCoverage']">
                    <xsl:variable name="ClientCoverageTypeID">
                      <xsl:value-of select="@ClientCoverageTypeID"/>
                    </xsl:variable>
                    <IE:dropDownItem >
                      <xsl:attribute name="value">
                        <xsl:value-of select="@ReferenceID"/>
                      </xsl:attribute>
                      <xsl:if test="/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@ClientCode != ''">
                        <xsl:value-of select="concat(/Root/Reference[@List='ClientCoverageType' and @ReferenceID = $ClientCoverageTypeID]/@ClientCode, ' - ')"/>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="@Name != ''">
                          <xsl:value-of select="@Name"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="@SystemName"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </IE:dropDownItem>
                  </xsl:for-each>
                </IE:APDCustomSelect>
              </td>
            </tr>
            <tr>
              <td>Total Deductible:</td>
              <td>
                <IE:APDInputCurrency id="txtTotalDeductible" name="txtTotalDeductible" value="" width="100" precision="9" scale="2" symbol="$" required="false" canDirty="false" CCDisabled="false" CCTabIndex="" onChange="" />
              </td>
            </tr>
            <tr>
              <td>Total Ded. Applied:</td>
              <td>
                <IE:APDInputCurrency id="txtDeductibleApplied" name="txtDeductibleApplied" value="" width="100" precision="9" scale="2" symbol="$" required="false" canDirty="false" CCDisabled="false" CCTabIndex="" onChange="" />
              </td>
            </tr>
            <tr>
              <td>Ded. applied for this channel:</td>
              <td>
                <IE:APDInputCurrency id="txtDeductible" name="txtDeductible" value="" width="100" precision="9" scale="2" symbol="$" required="false" canDirty="false" CCDisabled="false" CCTabIndex="" onChange="" />
              </td>
            </tr>
            <tr>
              <td>Overage of Limits:</td>
              <td>
                <IE:APDInputCurrency id="txtLimit" name="txtLimit" value="" width="100" precision="9" scale="2" symbol="$" required="false" canDirty="false" CCDisabled="false" CCTabIndex="" onChange="" />
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <IE:APDCheckBox id="chkPartialCoverageFlag" name="chkPartialCoverageFlag" caption="Partial Coverage" value="" width="" CCDisabled="false" required="false" CCTabIndex="" alignment="" canDirty="false" onBeforeChange="" onChange="" onAfterChange="" />
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <IE:APDButton id="btnCovSave" name="btnCovSave" class="APDButton" value="Save" width="50" CCDisabled="false" CCTabIndex="153" onButtonClick="saveCoverage()"/>
                <IE:APDButton id="btnCovCancel" name="btnCovCancel" class="APDButton" value="Cancel" width="50" CCDisabled="false" CCTabIndex="154" onButtonClick="cancelCoverage()"/>
              </td>
            </tr>
          </table>
        </div>
        <div id="divAssignmentHistory" name="divAssignmentHistory" style="display:none;position:absolute;top:100px;left:100px;height:100px;width:520px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
          <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
            <colgroup>
              <col width="115px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td colspan="2" style="font-weight:bold">
                Assignment History
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold">
                <img src="images/background_top.gif" style="height:2px;width:125px"/>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <div style="position:relative;">
                  <IE:APDDataGrid id="grdHistory" CCDataSrc="xmlHistory" CCDataPageSize="5" showAlternateColor="false" altColor="#E6E6FA" CCWidth="507px" CCHeight="128" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick="">
                    <IE:APDDataGridHeader>
                      <IE:APDDataGridTR>
                        <IE:APDDataGridTD width="110px" caption="Selection Date" fldName="SelectionDate" DataType="" sortable="true" CCStyle=""/>
                        <IE:APDDataGridTD width="110px" caption="Assignment Date" fldName="AssignmentDate" DataType="number" sortable="true" CCStyle=""/>
                        <IE:APDDataGridTD width="110px" caption="Cancellation Date" fldName="CancellationDate" DataType="number" sortable="true" CCStyle=""/>
                        <IE:APDDataGridTD width="130px" caption="Appraiser Name" fldName="AppraiserName" DataType="number" sortable="true" CCStyle=""/>
                      </IE:APDDataGridTR>
                    </IE:APDDataGridHeader>
                    <IE:APDDataGridBody>
                      <IE:APDDataGridTR>
                        <IE:APDDataGridTD>
                          <IE:APDDataGridFld fldName="SelectionDateFormatted" />
                        </IE:APDDataGridTD>
                        <IE:APDDataGridTD>
                          <IE:APDDataGridFld fldName="AssignmentDateFormatted" />
                        </IE:APDDataGridTD>
                        <IE:APDDataGridTD>
                          <IE:APDDataGridFld fldName="CancellationDateFormatted" />
                        </IE:APDDataGridTD>
                        <IE:APDDataGridTD>
                          <IE:APDDataGridFld fldName="AppraiserName" />
                        </IE:APDDataGridTD>
                      </IE:APDDataGridTR>
                    </IE:APDDataGridBody>
                  </IE:APDDataGrid>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <IE:APDButton id="btnCloseHistory" name="btnCloseHistory" class="APDButton" value="Close" width="50" CCDisabled="false" CCTabIndex="153" onButtonClick="closeAssignmentHistory()"/>
              </td>
            </tr>
          </table>
        </div>
        <div id="divAssignmentRemarksMore" name="divAssignmentRemarksMore" style="display:none;position:absolute;top:100px;left:100px;height:175px;width:470px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
          <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
            <colgroup>
              <col width="150px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td style="font-weight:bold">
                Assignment Remarks
              </td>
              <td style="text-align:right">
                <img src="images/close.gif" style="cursor:hand" onclick="closeMoreRemarks()"/>
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold">
                <img src="images/background_top.gif" style="height:2px;width:125px"/>
              </td>
            </tr>
            <tr>
              <td>Template:</td>
              <td>
                <IE:APDCustomSelect id="selShopRemarksTemplate" name="selShopRemarksTemplate" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="false" width="250" onChange="shopTemplate()">

                  <xsl:for-each select="//Reference[@List='ShopRemarkTemplates']">
                    <xsl:sort select="@Name" data-type="text" order="ascending"/>

                    <IE:dropDownItem style="display:none">
                      <xsl:attribute name="value">
                        <xsl:value-of select="@ReferenceID"/>
                      </xsl:attribute>
                      <xsl:value-of select="substring-before(@Name, '|')"/>
                    </IE:dropDownItem>
                  </xsl:for-each>


                </IE:APDCustomSelect>
              </td>
            </tr>
            <tr>
              <td>Rental Company:</td>
              <td>
                <IE:APDCustomSelect id="selRentalCompany" name="selRentalCompany" displayCount="6" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" required="false" width="250" onChange="shopTemplate()">
                  <xsl:choose>
                    <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                      <IE:dropDownItem value="H" style="display:none">Rental Handled by Client</IE:dropDownItem>
                    </xsl:when>
                    <xsl:otherwise>
                      <IE:dropDownItem value="E" style="display:none">Enterprise</IE:dropDownItem>
                      <IE:dropDownItem value="H" style="display:none">Hertz</IE:dropDownItem>
                      <IE:dropDownItem value="H" style="display:none">Rental Handled by Client</IE:dropDownItem>
                      <IE:dropDownItem value="H" style="display:none">Rental Not Required</IE:dropDownItem>
                    </xsl:otherwise>
                  </xsl:choose>

                </IE:APDCustomSelect>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <IE:APDTextArea id="txtAssignmentRemarksMore" name="txtAssignmentRemarksMore" maxLength="" width="450" height="100" required="false" canDirty="false" CCDisabled="false" CCTabIndex="" onChange="" >
                  <xsl:attribute name="maxLength">
                    <xsl:value-of select="/Root/Metadata[@Entity='Assignment']/Column[@Name='AssignmentRemarks']/@MaxLength"/>
                  </xsl:attribute>
                </IE:APDTextArea>
              </td>
            </tr>
          </table>
        </div>

        <div id="divShopDetail" name="divShopDetail" style="position:absolute;top:0px;left:94px;display:none;height:250px;width:525px;overflow:auto;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideToolTip()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideToolTip()">
          <table border="0" cellspacing="2" cellpadding="0" style="height:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF;border-collapse:collapse;table-layout:fixed">
            <colgroup>
              <col width="150px"/>
              <col width="*"/>
              <col width="25px"/>
              <col width="55px"/>
              <col width="55px"/>
              <col width="20px"/>
            </colgroup>
            <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;color:#FFFFFF">
              <td colspan="12">Shop Detail</td>
            </tr>
            <tr>
              <td>Program Shop:</td>
              <td colspan="5">
                <IE:APDLabel id="ProgramShop" name="ProgramShop" width="50" CCDisabled="true" >
                  <xsl:choose>
                    <xsl:when test="(/Root/ServiceChannel/Assignment/Shop/@ProgramType = 'CEI')" >
                      <xsl:attribute name="value">CEI</xsl:attribute>
                    </xsl:when>
                    <!-- Standard LYNX program shop assignment. -->
                    <xsl:when test="/Root/ServiceChannel/Assignment/Shop/@ProgramType = 'LS'" >
                      <xsl:attribute name="value">Yes</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="(/Root/ServiceChannel/Assignment/Shop/@ProgramType != 'LS') and (/Root/ServiceChannel/Assignment/@ShopLocationID = 0)" >
                    </xsl:when>
                    <xsl:when test="/Root/ServiceChannel/Assignment/Shop/@ProgramType != 'LS'" >
                      <xsl:attribute name="value">No</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </IE:APDLabel>
              </td>
            </tr>
            <!-- Adding communcationcation method and pref estimate package fileds on the shop detail popup -->
            <!-- Done by Glsd451 on 21Apr2015 -->
            <tr>
              <td>Communication Method:</td>
              <td colspan="5">
                <xsl:variable name="ComMethodID">
                  <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@PreferredCommunicationMethodID"/>
                </xsl:variable>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('ShopNamee', /Root/Reference[@List='CommunicationMethod' and @ReferenceID = $ComMethodID], 'Name', '', 280, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>Pref Estimate Package:</td>
              <td colspan="5">
                <xsl:variable name="PrefEstPackageID">
                  <xsl:value-of select="/Root/ServiceChannel/Assignment/Shop/@PreferredEstimatePackageID"/>
                </xsl:variable>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('ShopNameee', /Root/Reference[@List='EstimatePackage' and @ReferenceID = $PrefEstPackageID], 'Name', '', 280, 'false')"/>
              </td>
            </tr>

            <tr>
              <td>Name:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('ShopName', /Root/ServiceChannel/Assignment/Shop, 'Name', '', 280, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>Address:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('Address1', /Root/ServiceChannel/Assignment/Shop, 'Address1', '', 280, 'false')"/>
              </td>
            </tr>
            <tr>
              <td></td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('Address2', /Root/ServiceChannel/Assignment/Shop, 'Address2', '', 280, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>City:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('AddressCity', /Root/ServiceChannel/Assignment/Shop, 'AddressCity', '', 180, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>State</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('AddressState', /Root/ServiceChannel/Assignment/Shop, 'AddressState', '', 105, 'false')"/>
              </td>
              <td>Zip:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('AddressZip', /Root/ServiceChannel/Assignment/Shop, 'AddressZip', '', 50, 'false')"/>
              </td>
              <td>MSA Code:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('MSACode', /Root/ServiceChannel/Assignment/Shop, 'MSACode', '', 15, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>Phone:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('ShopPhone', /Root/ServiceChannel/Assignment/Shop, 'PhoneAreaCode', 'PhoneExchangeNumber', 'PhoneUnitNumber', 'PhoneExtensionNumber', '', 'true', 'false', '', 'false', 'false', 'true', '')"/>
              </td>

              <td>Fax:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('ShopFax', /Root/ServiceChannel/Assignment/Shop, 'FaxAreaCode', 'FaxExchangeNumber', 'FaxUnitNumber', 'FaxExtensionNumber', '', 'true', 'false', '', 'false', 'false', 'true', '')"/>
              </td>
            </tr>
            <tr>
              <td colspan="6"></td>
            </tr>
            <tr>
              <td>Contact:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('ShopContactName', /Root/ServiceChannel/Assignment/Shop/ShopContact, 'Name', '', 280, 'false')"/>
              </td>
            </tr>
            <tr>
              <td>Phone:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('ShopContactPhone', /Root/ServiceChannel/Assignment/Shop/ShopContact, 'PhoneAreaCode', 'PhoneExchangeNumber', 'PhoneUnitNumber', 'PhoneExtensionNumber', '', 'true', 'false', '', 'false', 'false', 'true', '')"/>
              </td>
              <td>Email:</td>
              <td colspan="5">
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('ShopEmailAddress', /Root/ServiceChannel/Assignment/Shop, 'EmailAddress', '', 280, 'false')"/>
              </td>
            </tr>
          </table>
        </div>

        <xml id="xmlGlass">
          <WebAssignment version="100" SubmitGlassClaim="true">
            <Claim>
              <InsuranceCompanyID>
                <xsl:value-of select="/Root/@InsuranceCompanyID"/>
              </InsuranceCompanyID>
              <CoverageClaimNumber>
                <xsl:value-of select="/Root/Claim/@CoverageClaimNumber"/>
              </CoverageClaimNumber>
              <LossDate>
                <xsl:value-of select="js:formatSQLDate(string(/Root/Claim/@LossDate))"/>
              </LossDate>
              <LossDescription>
                <xsl:value-of select="/Root/Claim/@LossDescription"/>
              </LossDescription>
            </Claim>
            <Vehicle>
              <OwnerNameFirst>
                <xsl:value-of select="/Root/Vehicle/@OwnerNameFirst"/>
              </OwnerNameFirst>
              <OwnerNameLast>
                <xsl:value-of select="/Root/Vehicle/@OwnerNameLast"/>
              </OwnerNameLast>
              <VIN>
                <xsl:value-of select="/Root/Vehicle/@VIN"/>
              </VIN>
              <VehicleYear>
                <xsl:value-of select="/Root/Vehicle/@VehicleYear"/>
              </VehicleYear>
              <Make>
                <xsl:value-of select="/Root/Vehicle/@Make"/>
              </Make>
              <Model>
                <xsl:value-of select="/Root/Vehicle/@Model"/>
              </Model>
              <ContactBestPhoneCD>
                <xsl:value-of select="/Root/Vehicle/@ContactBestPhoneCD"/>
              </ContactBestPhoneCD>
              <OwnerPhone>
                <xsl:value-of select="/Root/Vehicle/@OwnerPhone"/>
              </OwnerPhone>
              <OwnerNightPhone>
                <xsl:value-of select="/Root/Vehicle/@OwnerNightPhone"/>
              </OwnerNightPhone>
              <OwnerAltPhone>
                <xsl:value-of select="/Root/Vehicle/@OwnerAltPhone"/>
              </OwnerAltPhone>
              <OwnerBestPhoneCD>
                <xsl:value-of select="/Root/Vehicle/@OwnerBestPhoneCD"/>
              </OwnerBestPhoneCD>
              <Address1>
                <xsl:value-of select="/Root/Vehicle/@Address1"/>
              </Address1>
              <AddressCity>
                <xsl:value-of select="/Root/Vehicle/@AddressCity"/>
              </AddressCity>
              <AddressState>
                <xsl:value-of select="/Root/Vehicle/@AddressState"/>
              </AddressState>
              <AddressZip>
                <xsl:value-of select="/Root/Vehicle/@AddressZip"/>
              </AddressZip>
              <DeductibleAmt>
                <xsl:choose>
                  <xsl:when test="/Root/Claim/@DeductibleAmt != ''">
                    <xsl:value-of select="/Root/Claim/@DeductibleAmt"/>
                  </xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </DeductibleAmt>
              <ShopRemarks></ShopRemarks>
              <SourceApplicationPassThruData></SourceApplicationPassThruData>
            </Vehicle>
          </WebAssignment>
        </xml>
      </BODY>
    </HTML>
  </xsl:template>

  <!-- Gets the Shop Assign Info -->
  <xsl:template name="ShopAssignInfo" >
    <xsl:param name="AdjVehicleCRUD"/>
    <xsl:param name="ShopCRUD"/>
    <xsl:variable name="dataDisabled">
      <xsl:choose>
        <xsl:when test="contains($AdjVehicleCRUD, 'U') = true() and /Root/ServiceChannel/Assignment/@AssignmentID != ''">false</xsl:when>
        <xsl:otherwise>true</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ServiceChannelCD">
      <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
        <xsl:value-of select="@ServiceChannelCD"/>
      </xsl:for-each>
      <!--<xsl:value-of select="/Root/ServiceChannel/@ServiceChannelCD"/>-->
    </xsl:variable>
    <xsl:variable name="AssignmentTypeCD">
      <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
        <xsl:value-of select="@AssignmentTypeCD"/>
      </xsl:for-each>
      <!--<xsl:value-of select="/Root/ServiceChannel/Assignment/@AssignmentTypeCD"/>-->
    </xsl:variable>
    <xsl:if test="/Root/ServiceChannel/Assignment/@AssignmentTypeID != 15">
      <div style="position:absolute;top:82px;left:292px;">
        <!-- <IE:APDDataGrid id="grdCoverages" CCDataSrc="xmlCoveragesApplied" CCDataPageSize="2" showAlternateColor="false" altColor="#E6E6FA" CCWidth="404px" CCHeight="63" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick="">
         <IE:APDDataGridHeader>
             <IE:APDDataGridTR>
                 <IE:APDDataGridTD width="128px" caption="Coverage" fldName="CoverageName" DataType="" sortable="false" CCStyle=""/>
                 <IE:APDDataGridTD width="75px" caption="Ded. Applied" fldName="DeductibleAppliedAmt" DataType="number" sortable="false" CCStyle="text-align:right"/>
                 <IE:APDDataGridTD width="75px" caption="Tot. Limit" fldName="LimitApplied" DataType="number" sortable="false" CCStyle="text-align:right"/>
                 <IE:APDDataGridTD width="78px" caption="Limit Applied" fldName="LimitAppliedAmt" DataType="number" sortable="false" CCStyle="text-align:right"/>
             </IE:APDDataGridTR>
         </IE:APDDataGridHeader>
         <IE:APDDataGridBody>
   			  <IE:APDDataGridTR ondblClick="editCoverageApplied()">
   					<IE:APDDataGridTD>
   					   <IE:APDDataGridFld fldName="CoverageName" />
   					</IE:APDDataGridTD>
   					<IE:APDDataGridTD>
   					   <IE:APDDataGridFld fldName="DeductibleApplied" />
   					</IE:APDDataGridTD>
   					<IE:APDDataGridTD>
   					  <IE:APDDataGridFld fldName="LimitApplied" />
   					</IE:APDDataGridTD>
   					<IE:APDDataGridTD>
   					  <IE:APDDataGridFld fldName="LimitAppliedAmt" />
   					</IE:APDDataGridTD>
   			  </IE:APDDataGridTR>
         </IE:APDDataGridBody>
     </IE:APDDataGrid>  -->

        <!--<xsl:choose>
          <xsl:when test="/Root/ServiceChannel/Assignment/@AssignmentID != ''">
            <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
              <xsl:attribute name="style">display:none</xsl:attribute>
            </xsl:if>-->
        <IE:APDDataGrid id="grdCoverages" CCDataSrc="xmlCoveragesApplied" CCDataPageSize="2" showAlternateColor="false" altColor="#E6E6FA" CCWidth="404px" CCHeight="63" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick="">
          <IE:APDDataGridHeader>
            <IE:APDDataGridTR>
              <IE:APDDataGridTD width="120px" caption="Coverage" fldName="CoverageName" DataType="" sortable="false" CCStyle=""/>
              <IE:APDDataGridTD width="100px" caption="Ded. Applied" fldName="DeductibleAppliedAmt" DataType="number" sortable="false" CCStyle="text-align:right"/>
              <IE:APDDataGridTD width="100px" caption="Overage/Limit" fldName="LimitApplied" DataType="number" sortable="false" CCStyle="text-align:right"/>
              <IE:APDDataGridTD width="38px" caption="Partial" fldName="PartialCoverageFlag" DataType="number" sortable="false" CCStyle="text-align:center"/>
            </IE:APDDataGridTR>
          </IE:APDDataGridHeader>
          <IE:APDDataGridBody>
            <IE:APDDataGridTR ondblClick="editCoverageApplied()">
              <IE:APDDataGridTD>
                <IE:APDDataGridFld fldName="CoverageName" />
              </IE:APDDataGridTD>
              <IE:APDDataGridTD>
                <IE:APDDataGridFld fldName="DeductibleAppliedConcat" />
              </IE:APDDataGridTD>
              <IE:APDDataGridTD>
                <IE:APDDataGridFld fldName="LimitAppliedConcat" />
              </IE:APDDataGridTD>
              <IE:APDDataGridTD>
                <IE:APDDataGridFld fldName="PartialCoverageFlagImg" displayType="image"/>
              </IE:APDDataGridTD>
            </IE:APDDataGridTR>
          </IE:APDDataGridBody>
        </IE:APDDataGrid>
        <!--</xsl:when>
          <xsl:otherwise>

          </xsl:otherwise>
        </xsl:choose>-->
      </div>
    </xsl:if>
    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="280"/>
        <col width="5"/>
        <col width="*"/>
      </colgroup>
      <tr valign="top">
        <td>
          <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
            <colgroup>
              <col width="78"/>
              <col width="200"/>
            </colgroup>
            <tr>
              <td onmouseenter="showToolTip(this)" onmousemove="keepToolTip()" onmouseleave="bMouseInToolTip = false;window.setTimeout('hideToolTip()', 1000)">
                <xsl:choose>
                  <xsl:when test="/Root/ServiceChannel/Assignment/Shop/@ShopID != '' and contains($ShopCRUD, 'R') = true()">
                    <a style="font-weight:bold" onclick="ShopOnClick()">Assgn. To:</a>
                  </xsl:when>
                  <xsl:otherwise>Assgn. To:</xsl:otherwise>
                </xsl:choose>

              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="$AssignmentTypeCD = 'LDAU' or $AssignmentTypeCD = 'IA'">
                    <!-- <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('txtAssignedTo', /Root/ServiceChannel/Assignment/Appraiser, 'Name', '', '300', 'false')"/> -->
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtAssignedTo', string(/Root/ServiceChannel/Assignment/Appraiser/@Name), '', 400, 'false')"/>
                  </xsl:when>
                  <xsl:when test="$AssignmentTypeCD = 'SHOP'">
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtAssignedTo', string(/Root/ServiceChannel/Assignment/Shop/@Name), '', '300', 'false')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="$ServiceChannelCD = 'DA'">
                        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtAssignedTo', string(/Root/Reference[@List='Appraisers' and @ReferenceID = /Root/@LDAUID]/@Name), '', '300', 'false')"/>
                      </xsl:when>
                      <xsl:when test="$ServiceChannelCD = 'GL'">
                        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtAssignedTo', string(/Root/Reference[@List='Appraisers' and @ReferenceID = /Root/@GLID]/@Name), '', '300', 'false')"/>
                      </xsl:when>
                      <xsl:when test="$ServiceChannelCD = 'ME'">
                        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtAssignedTo', string(/Root/Reference[@List='Appraisers' and @ReferenceID = /Root/@MEID]/@Name), '', '300', 'false')"/>
                      </xsl:when>
                      <xsl:when test="$ServiceChannelCD = 'PS'">
                        <IE:APDLabel id="txtAssignedTo" name="txtAssignedTo" value="LYNX Select Shop" height="" width="300" CCDisabled="false" />
                      </xsl:when>
                      <xsl:when test="$ServiceChannelCD = 'CS'">
                        <IE:APDLabel id="txtAssignedTo" name="txtAssignedTo" value="Choice Shop" height="" width="300" CCDisabled="false" />
                      </xsl:when>
                      <xsl:when test="$ServiceChannelCD = 'RRP'">
                        <IE:APDLabel id="txtAssignedTo" name="txtAssignedTo" value="LYNX Select Shop" height="" width="300" CCDisabled="false" />
                      </xsl:when>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
                <!-- <xsl:choose>
                        <xsl:when test="$ServiceChannelCD = 'DA'">
                           <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('txtAssignedTo', /Root/ServiceChannel/Assignment/Appraiser, 'Name', '', '200', 'false')"/>
                        </xsl:when>
                        <xsl:when test="$ServiceChannelCD = 'GL'">
                           <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('txtAssignedTo', /Root/ServiceChannel/Assignment/Appraiser, 'Name', '', '200', 'false')"/>
                        </xsl:when>
                        <xsl:when test="$ServiceChannelCD = 'ME'">
                           <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('txtAssignedTo', /Root/ServiceChannel/Assignment/Appraiser, 'Name', '', '200', 'false')"/>                           
                        </xsl:when>
                        <xsl:when test="$ServiceChannelCD = 'PS'">
                           <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('txtAssignedTo', /Root/ServiceChannel/Assignment/Shop, 'Name', '', '200', 'false')"/>
                        </xsl:when>
                     </xsl:choose> -->
              </td>
            </tr>
            <tr>
              <td>Contact:</td>
              <td>
                <xsl:choose>
                  <xsl:when test="$AssignmentTypeCD = 'LDAU' or $AssignmentTypeCD = 'IA'">
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtContact', string(/Root/ServiceChannel/Assignment/Appraiser/@NoName), '', '300', 'false')"/>
                  </xsl:when>
                  <xsl:when test="$AssignmentTypeCD = 'SHOP'">
                    <table border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
                      <colgroup>
                        <col width="150"/>
                        <col width="30"/>
                        <col width="30"/>
                      </colgroup>
                      <tr>
                        <td>
                          <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtContact', string(/Root/ServiceChannel/Assignment/Shop/ShopContact/@Name), '', '145', 'false')"/>
                        </td>

                        <xsl:if test="$AssignmentTypeCD = 'SHOP'">
                          <xsl:choose>
                            <xsl:when test="/Root/ServiceChannel/Assignment/@AssignmentID != ''">
                              <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                                <xsl:attribute name="style">display:none</xsl:attribute>
                              </xsl:if>
                            </xsl:when>
                            <xsl:otherwise>
                              <td>MSA:</td>
                              <td>
                                <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtMSACode', string(/Root/ServiceChannel/Assignment/Shop/@MSACode), '','100','false')"/>
                              </td>

                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:if>
                      </tr>


                    </table>
                  </xsl:when>
                  <xsl:otherwise>
                    <IE:APDLabel id="txtContact" name="txtContact" value="" height="" width="300" CCDisabled="" />
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td>Phone:</td>
              <td>
                <xsl:choose>
                  <xsl:when test="$AssignmentTypeCD = 'LDAU' or $AssignmentTypeCD = 'IA'">
                    <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtPhone', /Root/ServiceChannel/Assignment/Appraiser, 'PhoneAreaCode', 'PhoneExchangeNumber', 'PhoneUnitNumber', 'PhoneExtensionNumber', '', 'true', 'true', '', 'false', 'false', 'true', '')"/>
                  </xsl:when>
                  <xsl:when test="$AssignmentTypeCD = 'SHOP'">
                    <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtPhone', /Root/ServiceChannel/Assignment/Shop, 'PhoneAreaCode', 'PhoneExchangeNumber', 'PhoneUnitNumber', 'PhoneExtensionNumber', '', 'true', 'true', '', 'false', 'false', 'true', '')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <IE:APDLabel id="txtPhone" name="txtPhone" value="" height="" width="300" CCDisabled="" />
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td>Fax:</td>
              <td>
                <xsl:choose>
                  <xsl:when test="$AssignmentTypeCD = 'LDAU' or $AssignmentTypeCD = 'IA'">
                    <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtFax', /Root/ServiceChannel/Assignment/Appraiser, 'FaxAreaCode', 'FaxExchangeNumber', 'FaxUnitNumber', 'FaxExtensionNumber', '', 'true', 'true', '', 'false', 'false', 'true', '')"/>
                  </xsl:when>
                  <xsl:when test="$AssignmentTypeCD = 'SHOP'">
                    <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtFax', /Root/ServiceChannel/Assignment/Shop, 'FaxAreaCode', 'FaxExchangeNumber', 'FaxUnitNumber', 'FaxExtensionNumber', '', 'true', 'true', '', 'false', 'false', 'true', '')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <IE:APDLabel id="txtFax" name="txtFax" value="" height="" width="300" CCDisabled="" />
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <xsl:choose>
                <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                  <td>Email:</td>
                  <td>
                    <!--<xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtEmailStatus', string(/Root/ServiceChannel/Assignment/@CommunicationMethodIDEmail), '', '200', 'false')"/>-->
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtEmail', string(/Root/ServiceChannel/Assignment/Shop/@EmailAddress), '', 'true', 'false', '', 'false', 'false', 'true', '')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="style">display:none</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>

              <tr>
                  <td>
                    <xsl:variable name="ClaimAspectServiceChannelID" select="/Root/ServiceChannel/@ClaimAspectServiceChannelID"></xsl:variable>
                    <!-- a href="HQAssignmentDetails.aspx?LynxID={$Params}" style="font-weight:bold"-->
                    <!-- onchange="navigate(<%# DataBinder.Eval(Container.DataItem, "LynxID").ToString() %>, <%# DataBinder.Eval(Container.DataItem, "Root_UserID").ToString() %>, <%# DataBinder.Eval(Container.DataItem, "ClaimAspectID").ToString() %>, <%# DataBinder.Eval(Container.DataItem, "ServiceChannel_ClaimAspectServiceChannelID").ToString() %>, <%# DataBinder.Eval(Container.DataItem, "InsuranceCompanyID").ToString() %>, ddlNavigate1[0].value)" -->
                    <a href="" style="font-weight:bold" onclick="navigate({$LynxID}, {$ClaimAspectServiceChannelID})">
                      Elec. Status:
                    </a>
                  </td>
                  <td>
                  <!--<xsl:for-each select="/Root/ServiceChannel/Assignment">
                    <xsl:if test="AssignmentSequenceNumber='1'">
                      <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtVANStatus', string(@VANAssignmentStatusName), '', '200', 'false')"/>
                    </xsl:if>
                  </xsl:for-each>-->
                  <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtVANStatus', string(@VANAssignmentStatusName), '', '200', 'false')"/>
                  </xsl:for-each>
                  <!--<xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtVANStatus', string(/Root/ServiceChannel/Assignment/@VANAssignmentStatusName), '', '200', 'false')"/>-->
                </td>
              </tr>

            </tr>
            <tr>
              <td>Fax Status:</td>
              <td>
                <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
                  <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtFaxStatus', string(@FaxAssignmentStatusName), '', '200', 'false')"/>
                </xsl:for-each>
                <!--<xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtFaxStatus', string(/Root/ServiceChannel/Assignment/@FaxAssignmentStatusName ), '', '200', 'false')"/>-->
              </td>
            </tr>
            <!--<tr>
              <xsl:choose>
                <xsl:when test="/Root/ServiceChannel/Assignment/@AssignmentID != ''">
                  <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                    <xsl:attribute name="style">display:none</xsl:attribute>
                  </xsl:if>
                  <td>Elec. Status:</td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtVANStatus', string(/Root/ServiceChannel/Assignment/@VANAssignmentStatusName), '', '200', 'false')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>

                </xsl:otherwise>
              </xsl:choose>
            </tr>-->

            <tr>
              <td>Assgn. Dt.:</td>
              <td>
                <table border="0" cellpadding="0" cellspacing="0">
                  <colgroup>
                    <col width="95px"/>
                    <col width="30px"/>
                    <col width="75px"/>
                  </colgroup>
                  <tr>
                    <td>
                      <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentDate', /Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1'], 'AssignmentDate', 'date', 'false', 'true', '', 'false', 'true', '')"/>
                    </td>
                    <td>Prev.:</td>
                    <td>
                      <xsl:choose>
                        <xsl:when test="$AssignmentTypeCD = 'LDAU' or $AssignmentTypeCD = 'IA'">
                          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtLastAssignmentDate', /Root/ServiceChannel/Assignment/Appraiser, 'LastAssignedDate', 'date', 'false', 'true', '', 'false', 'true', '')"/>
                        </xsl:when>
                        <xsl:when test="$AssignmentTypeCD = 'SHOP'">
                          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtLastAssignmentDate', /Root/ServiceChannel/Assignment/Shop, 'LastAssignedDate', 'date', 'false', 'true', '', 'false', 'true', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtLastAssignmentDate', /Root/ServiceChannel/Assignment/Shop, 'LastAssignedDate', 'date', 'false', 'true', '', 'false', 'true', '')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>
              <td>Work Status:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtWorkStatus', string(/Root/ServiceChannel/Assignment/@WorkStatus), '', '200', 'false')"/>
              </td>
            </tr>
            <!--<tr>
              <xsl:choose>
                <xsl:when test="/Root/ServiceChannel/Assignment/@AssignmentID != ''">
                  <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                    <xsl:attribute name="style">display:none</xsl:attribute>
                  </xsl:if>
                  <td>Work Status:</td>
                  <td>
                    <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
                      <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtWorkStatus', string(@WorkStatus), '', '200', 'false')"/>
                    </xsl:for-each>
                  </td>
                </xsl:when>
                <xsl:otherwise>

                </xsl:otherwise>
              </xsl:choose>
            </tr>-->
            <!-- <tr>
                  <td>Eff. Ded. Sent:</td>
                  <td>
                     <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputCurrency('txtEffDedSent', /Root/ServiceChannel/Assignment, 'EffectiveDeductibleSentAmt', '100', string($dataDisabled), '801', 'true', '', '', '$', 'true', '')"/>
                  </td>
               </tr> -->
            <tr>
              <xsl:choose>
                <xsl:when test="/Root/ServiceChannel/Assignment/@AssignmentID != ''">
                  <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                    <xsl:attribute name="style">display:none</xsl:attribute>
                  </xsl:if>
                  <td>Eff. Ded. Sent:</td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputCurrency('txtEffDedSent', /Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1'], 'EffectiveDeductibleSentAmt', '100', string($dataDisabled), '801', 'true', '', '', '$', 'true', '')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>

                </xsl:otherwise>
              </xsl:choose>
            </tr>
            <tr valign="top">
              <td>
                Remarks:
                <xsl:if test="$dataDisabled = 'false'">
                  <img src="images/showless.gif" style="cursor:hand" onclick="showMoreRemarks()"/>
                </xsl:if>
              </td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('txtAssignmentRemarks', /Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1'], 'AssignmentRemarks', '30', '190', string($dataDisabled), 802, 'true', 'true', '')"/>
              </td>
            </tr>
            <tr>


              <td colspan="2">
                <xsl:variable name="AssignmentExists" >
                  <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
                    <xsl:choose>
                      <xsl:when test="@AssignmentID">
                        True
                      </xsl:when>
                      <xsl:otherwise>
                        False
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </xsl:variable>

                <xsl:variable name="DisplayeAssignment">
                  <xsl:choose>
                    <xsl:when test="/Root/ClaimCoveragesApplied[@ServiceChannelName='CS']">false</xsl:when>
                    <xsl:otherwise>true</xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>

                <xsl:choose>
                  <xsl:when test="(/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']/@AssignmentID != '') and ($AssignmentExists)">
                    <xsl:for-each select="/Root/ServiceChannel/Assignment[@AssignmentSequenceNumber='1']">
                      <xsl:if test="$DisplayeAssignment='true'">
                      <IE:APDButton id="btnAssignment" name="btnAssignment" CCTabIndex="803" width="125">
                        <xsl:if test="/Root/ServiceChannel/@ServiceChannelCD = 'GL'">
                          <xsl:attribute name="style">display:none</xsl:attribute>
                        </xsl:if>
                        <xsl:choose>
                          <xsl:when test="(@VANAssignmentStatusID = 11) and (@FaxAssignmentStatusID = 22)" >
                            <xsl:attribute name="onButtonClick">
                              <xsl:text disable-output-escaping="yes">AssignmentProcess('0','')</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="value">
                              <xsl:text disable-output-escaping="yes">Send Assignment</xsl:text>
                            </xsl:attribute>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:attribute name="onButtonClick">
                              <xsl:text disable-output-escaping="yes">AssignmentProcess('1','')</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="value">
                              <xsl:text disable-output-escaping="yes">Resend Assignment</xsl:text>
                            </xsl:attribute>

                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:attribute name="CCDisabled">
                          <xsl:choose>
                            <xsl:when test="contains($AdjVehicleCRUD, 'U') = true()">false</xsl:when>
                            <xsl:otherwise>true</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </IE:APDButton>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="$DisplayeAssignment='true'">
                      <IE:APDButton id="btnSelectShop" name="btnSelectShop" CCTabIndex="804" style="display:none" width="120">

                        <xsl:choose>
                          <xsl:when test="(@ShopLocationID = 0) or (@AssignmentID = 0) or (/Root/ServiceChannel/@ServiceChannelCD = 'GL' and @ReferenceID = '')">
                            <xsl:attribute name="value">
                              <xsl:text disable-output-escaping="yes">Select</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="onButtonClick">
                              <xsl:text disable-output-escaping="yes">SelectShop();</xsl:text>
                            </xsl:attribute>
                          </xsl:when>

                          <xsl:when test="(@AssignmentTypeCD='SHOP' and @ShopLocationID != '') 
                                                or 
                                                (@AssignmentTypeCD='LDAU' and 
                                                (/Root/@LDAUID = @AppraiserID or /Root/@LDAUID = @ShopLocationID))
                                                or @AssignmentTypeCD='IA'">

                            <xsl:attribute name="value">
                              <xsl:text disable-output-escaping="yes">Cancel Assignment</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="onButtonClick">
                              <xsl:text disable-output-escaping="yes">CancelAssignedShop();</xsl:text>
                            </xsl:attribute>
                          </xsl:when>
                        </xsl:choose>

                        <xsl:attribute name="CCDisabled">
                          <xsl:choose>
                            <xsl:when test="/Root/@ActiveReinspection='1'">true</xsl:when>
                            <xsl:when test="contains($AdjVehicleCRUD, 'U') = true()">false</xsl:when>
                            <xsl:otherwise>true</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </IE:APDButton>
                        </xsl:when>
                        <xsl:otherwise>
                          <IE:APDButton id="btnSelectShop" name="btnSelectShop" CCTabIndex="804" style="display:none" width="120">
                          </IE:APDButton>
                        </xsl:otherwise>
                      </xsl:choose>



                      <IE:APDButton name="btnShopSearch" id="btnShopSearch" value="Shop Search" width="85" CCTabIndex="806" onButtonClick="ShopSearch()" style="visibility:hidden"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <IE:APDButton id="btnSelectShop" name="btnSelectShop" value="Select" width="85" CCTabIndex="806" onButtonClick="SelectShop()"/>
                  </xsl:otherwise>
                </xsl:choose>

              </td>
            </tr>

          </table>
        </td>
        <td></td>
        <td valign="top">
          <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
            <colgroup>
              <col width="95"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td colspan="2">
                <div style="position:absolute;top:0px;left:300px;width:95px;margin:0px;margin-top:-3px;padding:0px;padding-left:5px;padding-right:5px;background-color:#FFFFFF">Current Estimate:</div>
                <div style="border:1px solid #C0C0C0;padding:0px;margin:0px;padding:3px;padding-top:7px;">
                  <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
                    <colgroup>
                      <col width="68"/>
                      <col width="*"/>
                      <col width="10"/>
                      <col width="80"/>
                      <col width="*"/>
                    </colgroup>
                    <tr>
                      <td>Gross Amt.:</td>
                      <td>
                        <!-- <IE:APDInput class="APDInput" id="txtCurEstGrossAmt" name="txtCurEstGrossAmt" size="6" maxLength="" width="75"  required="false" canDirty="false" CCDisabled="true" CCTabIndex="" >
                                 <xsl:attribute name="value"><xsl:value-of select="concat('$', /Root/ServiceChannel/Assignment/@CurEstGrossRepairTotal)"/></xsl:attribute>
                              </IE:APDInput> -->
                        <IE:APDLabel id="txtCurEstGrossAmt" name="txtCurEstGrossAmt" height="" width="100" CCDisabled="false" >
                          <xsl:attribute name="value">
                            <xsl:value-of select="concat('$ ', /Root/ServiceChannel/@CurEstGrossRepairTotal)"/>
                          </xsl:attribute>
                        </IE:APDLabel>
                      </td>
                      <td></td>
                      <td>Deduct applied:</td>
                      <td>
                        <IE:APDLabel id="txtDedAppliedAmt" name="txtDedAppliedAmt" height="" width="100" CCDisabled="false" >
                          <xsl:attribute name="value">
                            <xsl:value-of select="concat('$ ', /Root/ServiceChannel/@CurEstDeductiblesApplied)"/>
                          </xsl:attribute>
                        </IE:APDLabel>
                      </td>
                    </tr>
                    <tr>
                      <td>Net Amt.:</td>
                      <td>
                        <IE:APDLabel id="txtCurEstNetAmt" name="txtCurEstNetAmt" height="" width="100" CCDisabled="false" >
                          <xsl:attribute name="value">
                            <xsl:value-of select="concat('$ ', /Root/ServiceChannel/@CurEstNetRepairTotal)"/>
                          </xsl:attribute>
                        </IE:APDLabel>
                      </td>
                      <td></td>
                      <td>Limits applied:</td>
                      <td>
                        <IE:APDLabel id="txtLimitAppliedAmt" name="txtLimitAppliedAmt" height="" width="100" CCDisabled="false" >
                          <xsl:attribute name="value">
                            <xsl:value-of select="concat('$ ', /Root/ServiceChannel/@CurEstLimitsEffect)"/>
                          </xsl:attribute>
                        </IE:APDLabel>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
            <xsl:if test="/Root/ServiceChannel/Assignment/@AssignmentTypeID != 15">
              <tr>
                <td></td>
                <td>
                  <div style="position:absolute;top:60px;left:553px">
                    <IE:APDButton id="btnAdd" name="btnAdd" value="Add" width="" CCDisabled="false" CCTabIndex="808" onButtonClick="addCoverageApplied()"/>
                    <IE:APDButton id="btnEdit" name="btnEdit" value="Edit" width="" CCDisabled="false" CCTabIndex="809" onButtonClick="editCoverageApplied()"/>
                    <IE:APDButton id="btnDel" name="btnDel" value="Delete" width="" CCDisabled="false" CCTabIndex="810" onButtonClick="delCoverageApplied()"/>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div style="height:95px"></div>
                </td>
              </tr>
            </xsl:if>
            <tr>
              <td>Disposition:</td>
              <td>
                <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('selDispositionCD', /Root/ServiceChannel, 'DispositionTypeCD', 'DispositionCD', 'false', 5, string($dataDisabled), '811', 'true', '', '200', '', 'true', '')"/>
              </td>

              <!-- </tr>
                  <td>Disposition:</td>
                  <td>
                     <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('selDispositionCD', /Root/ServiceChannel, 'DispositionTypeCD', 'DispositionCD', 'false', 5, string($dataDisabled), '811', 'true', '', '200', '', 'true', '')"/>
                  </td> -->
            </tr>
            <tr>
              <td colspan="2">
                <xsl:choose>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'PS'">
                    <xsl:call-template name="PSKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'CS'">
                    <xsl:call-template name="PSKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'RRP'">
                    <xsl:call-template name= "RRPKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'DA' and /Root/ServiceChannel/Assignment/@AssignmentTypeID != 15">
                    <xsl:call-template name="DAKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'DA' and /Root/ServiceChannel/Assignment/@AssignmentTypeID = 15">
                    <xsl:call-template name="PursuitAuditFields">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'GL'">
                    <xsl:call-template name="GLKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'ME'">
                    <xsl:call-template name="MEKeyDates">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="/Root/ServiceChannel/@ServiceChannelCD = 'DR'">
                    <xsl:call-template name="DADRRepairLocation">
                      <xsl:with-param name="dataDisabled">
                        <xsl:value-of select="$dataDisabled"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <IE:APDTextArea id="RepairScheduleChangeReason" name="RepairScheduleChangeReason" maxLength="150" style="display:none"/>
    <input type="hidden" id="RepairScheduleChangeReasonID"/>
  </xsl:template>


  <xsl:template name="PSKeyDates">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="84px"/>
        <col width="100px"/>
        <col width="28px"/>
        <col width="72px"/>
        <col width="101px"/>
        <col width="25px"/>
      </colgroup>
      <!-- <tr>
         <td>Assign Rcvd.:</td>
         <td>
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentReceived', /Root/ServiceChannel/Assignment, 'AssignmentDate', 'date', 'false', string($dataDisabled), '', 'false', 'true', '')"/>
         </td>
         <td></td>
         <td>Inspection:</td>
         <td colspan="2">
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInspectionDate', /Root/ServiceChannel, 'InspectionDate', 'date', 'false', string($dataDisabled), '', 'true', 'true', '')"/>
         </td>
      </tr> -->
      <tr>
        <td>Inspection:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInspectionDate', /Root/ServiceChannel, 'InspectionDate', 'date', 'false', string($dataDisabled), '813', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>Orig. Est. Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtOrgEstDate', /Root/ServiceChannel, 'OriginalEstDate', 'date', 'false', 'false', '', 'false', 'true', '')"/>
        </td>
      </tr>

      <tr>
        <td>Repair Start Dt.:</td>
        <td>
          <xsl:variable name="repairStartDate">
            <xsl:if test="/Root/ServiceChannel/@WorkStartDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkStartDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairStartDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkStartConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairStartDate', string($repairStartDate), 'date', 'true', string($repairStartDisable), 'false', 814, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkStartConfirmFlag = '0') and (/Root/ServiceChannel/Assignment/@AssignmentID != '') and (/Root/ServiceChannel/@WorkStartDate != '1900-01-01T00:00:00') and (/Root/ServiceChannel/@WorkStartDate !='')">
            <button id="btnRepairStart" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairStart()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
        <td>End Dt.:</td>
        <td>
          <xsl:variable name="repairEndDate">
            <xsl:if test="/Root/ServiceChannel/@WorkEndDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkEndDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairEndDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkEndConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairEndDate', string($repairEndDate), 'date', 'true', string($repairEndDisable), 'false', 815, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkEndConfirmFlag = '0') and (/Root/ServiceChannel/@WorkStartConfirmFlag = '1') and (/Root/ServiceChannel/Assignment/@AssignmentID != '') and (/Root/ServiceChannel/@WorkEndDate != '1900-01-01T00:00:00') and (/Root/ServiceChannel/@WorkEndDate !='')">
            <button id="btnRepairEnd" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairEnd()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td>Final Est. Rcvd.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInvoiceReceivedDate', /Root/ServiceChannel, 'AppraiserInvoiceDate', 'date', 'false', string($dataDisabled), '816', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>C/O Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtCashOutDate', /Root/ServiceChannel, 'CashOutDate', 'date', 'false', string($dataDisabled), '817', 'true', 'true', '')"/>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="RRPKeyDates">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="84px"/>
        <col width="100px"/>
        <col width="28px"/>
        <col width="72px"/>
        <col width="101px"/>
        <col width="25px"/>
      </colgroup>
      <!-- <tr>
         <td>Assign Rcvd.:</td>
         <td>
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentReceived', /Root/ServiceChannel/Assignment, 'AssignmentDate', 'date', 'false', string($dataDisabled), '', 'false', 'true', '')"/>
         </td>
         <td></td>
         <td>Inspection:</td>
         <td colspan="2">
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInspectionDate', /Root/ServiceChannel, 'InspectionDate', 'date', 'false', string($dataDisabled), '', 'true', 'true', '')"/>
         </td>
      </tr> -->
      <tr>
        <td>Inspection:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes"  select="js:XSL_APDInputDate('txtInspectionDate', /Root/ServiceChannel, 'InspectionDate', 'date', 'true', string($dataDisabled), '813', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>Orig. Est. Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtOrgEstDate', /Root/ServiceChannel, 'OriginalEstDate', 'date', 'false', 'false', '', 'false', 'true', '')"/>
        </td>
      </tr>

      <tr>
        <td>Final Est. Rcvd.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInvoiceReceivedDate', /Root/ServiceChannel, 'AppraiserInvoiceDate', 'date', 'false', string($dataDisabled), '816', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>C/O Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtCashOutDate', /Root/ServiceChannel, 'CashOutDate', 'date', 'false', string($dataDisabled), '817', 'true', 'true', '')"/>
        </td>
      </tr>
    </table>
  </xsl:template>


  <xsl:template name="DAKeyDates">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="110px"/>
        <col width="201px"/>
      </colgroup>
      <!-- <tr>
         <td>Assignment Rcvd.:</td>
         <td>
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentReceived', /Root/ServiceChannel/Assignment, 'AssignmentDate', 'datetime', 'false', 'true', '', 'false', 'true', '')"/>
         </td>
      </tr> -->
      <tr>
        <td>Original Est. Dt.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtOrgEstDate', /Root/ServiceChannel, 'OriginalEstDate', 'datetime', 'false', 'false', '', 'false', 'true', '')"/>
        </td>
      </tr>
    </table>
    <xsl:call-template name="DADRRepairLocation">
      <xsl:with-param name="dataDisabled">
        <xsl:value-of select="$dataDisabled"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="GLKeyDates">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="84px"/>
        <col width="101px"/>
        <col width="35px"/>
        <col width="60px"/>
        <col width="101px"/>
        <col width="25px"/>
      </colgroup>
      <tr>
        <!-- <td>Assign Rcvd.:</td>
         <td>
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentReceived', /Root/ServiceChannel/Assignment, 'AssignmentDate', 'date', 'false', string($dataDisabled), '', 'false', 'true', '')"/>
         </td>
         <td></td> -->
        <td>Appt. Dt.:</td>
        <td colspan="5">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtApptDate', /Root/ServiceChannel, 'AppointmentDate', 'date', 'false', string($dataDisabled), '', 'true', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td>Repair Start Dt.:</td>
        <td>
          <xsl:variable name="repairStartDate">
            <xsl:if test="/Root/ServiceChannel/@WorkStartDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkStartDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairStartDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkStartConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairStartDate', string($repairStartDate), 'date', 'true', string($repairStartDisable), 'false', 814, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkStartConfirmFlag = '0') and (/Root/ServiceChannel/Assignment/@AssignmentID != '') and (/Root/ServiceChannel/@WorkStartDate != '1900-01-01T00:00:00') and (/Root/ServiceChannel/@WorkStartDate !='')">
            <button id="btnRepairStart" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairStart()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
        <td>End Dt.:</td>
        <td>
          <xsl:variable name="repairEndDate">
            <xsl:if test="/Root/ServiceChannel/@WorkEndDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkEndDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairEndDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkEndConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairEndDate', string($repairEndDate), 'date', 'true', string($repairEndDisable), 'false', 815, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkEndConfirmFlag = '0') and (/Root/ServiceChannel/@WorkStartConfirmFlag = '1') and (/Root/ServiceChannel/Assignment/@AssignmentID != '')">
            <button id="btnRepairEnd" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairEnd()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td>Final Est. Rcvd.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInvoiceReceivedDate', /Root/ServiceChannel, 'AppraiserInvoiceDate', 'date', 'false', string($dataDisabled), '820', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>C/O Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtCashOutDate', /Root/ServiceChannel, 'CashOutDate', 'date', 'false', string($dataDisabled), '821', 'true', 'true', '')"/>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="MEKeyDates">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="84px"/>
        <col width="101px"/>
        <col width="35px"/>
        <col width="60px"/>
        <col width="101px"/>
        <col width="25px"/>
      </colgroup>
      <!-- <tr>
         <td>Assign Rcvd.:</td>
         <td colspan="4">
            <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtAssignmentReceived', /Root/ServiceChannel/Assignment, 'AssignmentDate', 'date', 'false', string($dataDisabled), '', 'false', 'true', '')"/>
         </td>
      </tr> -->
      <tr>
        <td>Repair Start Dt.:</td>
        <td>
          <xsl:variable name="repairStartDate">
            <xsl:if test="/Root/ServiceChannel/@WorkStartDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkStartDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairStartDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkStartConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairStartDate', string($repairStartDate), 'date', 'true', string($repairStartDisable), 'false', 814, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkStartConfirmFlag = '0') and (/Root/ServiceChannel/Assignment/@AssignmentID != '') and (/Root/ServiceChannel/@WorkStartDate != '1900-01-01T00:00:00') and (/Root/ServiceChannel/@WorkStartDate !='')">
            <button id="btnRepairStart" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairStart()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
        <td>End Dt.:</td>
        <td>
          <xsl:variable name="repairEndDate">
            <xsl:if test="/Root/ServiceChannel/@WorkEndDate !='1900-01-01T00:00:00'">
              <xsl:value-of select="js:formatSQLDateTime(string(/Root/ServiceChannel/@WorkEndDate))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="repairEndDisable">
            <xsl:value-of select="boolean($dataDisabled = 'true' or /Root/ServiceChannel/@WorkEndConfirmFlag = '1')"/>
          </xsl:variable>

          <xsl:value-of disable-output-escaping="yes" select="js:APDInputDate('txtRepairEndDate', string($repairEndDate), 'date', 'true', string($repairEndDisable), 'false', 815, 'true', 'true', '')"/>
        </td>
        <td>
          <xsl:if test="(/Root/ServiceChannel/@WorkEndConfirmFlag = '0') and (/Root/ServiceChannel/@WorkStartConfirmFlag = '1') and  (/Root/ServiceChannel/Assignment/@AssignmentID != '')">
            <button id="btnRepairEnd" style="padding:0px;margin:0px;background-color:transparent;border:0px;height:12px;cursor:hand;display:none" onclick="confirmRepairEnd()">
              <img src="images/tick.gif"/>
            </button>
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td>Final Est. Rcvd.:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtInvoiceReceivedDate', /Root/ServiceChannel, 'AppraiserInvoiceDate', 'date', 'false', string($dataDisabled), '824', 'true', 'true', '')"/>
        </td>
        <td></td>
        <td>C/O Dt.:</td>
        <td colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputDate('txtCashOutDate', /Root/ServiceChannel, 'CashOutDate', 'date', 'false', string($dataDisabled), '825', 'true', 'true', '')"/>
        </td>
      </tr>
    </table>
  </xsl:template>
  <xsl:template name="DADRRepairLocation">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="65px"/>
        <col width="160px"/>
        <col width="40px"/>
        <col width="150px"/>
      </colgroup>
      <tr>
        <td>Repair Zip:</td>
        <td>
          <input type="text" id="txtRepairLocationZip" size="5" maxLength="5" style="border:1px solid #C0C0C0;padding-left:2px;padding-right:2px;padding-bottom:1px;height:18px;font-family:Tahoma,Verdana,Arial;font-size:11px;" tabIndex="50"/>
        </td>
        <td>City:</td>
        <td>
          <input type="text" id="txtRepairLocationCity" size="25" maxLength="75" style="border:1px solid #C0C0C0;padding-left:2px;padding-right:2px;padding-bottom:1px;height:18px;font-family:Tahoma,Verdana,Arial;font-size:11px;display:none" tabIndex="51"/>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairLocationCity2', /Root, 'RepairLocationCity', 120, string($dataDisabled), 50, 'true', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td>State:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('txtRepairLocationState', /Root, 'RepairLocationState', 'StateList', 'false', 6, string($dataDisabled), 52, 'false', 1, 140, '', true, 'populateCounty()')"/>
        </td>
        <td>County:</td>
        <td>
          <!-- <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairLocationCounty', /Root, 'RepairLocationCounty', 120, string($dataDisabled), 52, 'true', 'true', '')"/> -->
          <IE:APDCustomSelect id="txtRepairLocationCounty" name="txtRepairLocationCounty" blankFirst="false" canDirty="false" CCDisabled="false" type="edit" CCTabIndex="53" required="false" width="120" onChange="">
          </IE:APDCustomSelect>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="PursuitAuditFields">
    <xsl:param name="dataDisabled"/>
    <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
      <colgroup>
        <col width="65px"/>
        <col width="160px"/>
        <col width="40px"/>
        <col width="150px"/>
      </colgroup>
      <tr>
        <td>Insp. Dt.:</td>
        <td>
          <IE:APDInputDate id="txtPursuitInspectionDate" name="txtPursuitInspectionDate" type="date" futureDate="true" required="true" canDirty="false" CCDisabled="false" CCTabIndex="821" onChange="" >
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationInspectionDate"/>
            </xsl:attribute>
          </IE:APDInputDate>
        </td>
      </tr>
      <tr>
        <td>Shop Name:</td>
        <td colspan="3">
          <IE:APDInput id="txtRepairShopName" name="txtRepairShopName" value="" size="50" maxLength="150" width=""  required="false" canDirty="false" CCDisabled="false" CCTabIndex="822" >
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationName"/>
            </xsl:attribute>
          </IE:APDInput>
          <!--<xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairShopName', /Root, 'RepairLocationName', 120, string($dataDisabled), 50, 'true', 'true', '')"/>-->
        </td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">
          <IE:APDInput id="txtRepairShopAddress" name="txtRepairShopAddress" value="" size="50" maxLength="150" width=""  required="false" canDirty="false" CCDisabled="false" CCTabIndex="823" >
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationAddress"/>
            </xsl:attribute>
          </IE:APDInput>
          <!--<xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairShopAddress', /Root, 'RepairLocationAddress', 120, string($dataDisabled), 50, 'true', 'true', '')"/>-->
        </td>
      </tr>
      <tr>
        <td>Zip:</td>
        <td>
          <input type="text" id="txtRepairLocationZip" size="5" maxLength="5" style="border:1px solid #C0C0C0;padding-left:2px;padding-right:2px;padding-bottom:1px;height:18px;font-family:Tahoma,Verdana,Arial;font-size:11px;" tabIndex="824">
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationZip"/>
            </xsl:attribute>
          </input>
        </td>
        <td>City:</td>
        <td>
          <input type="text" id="txtRepairLocationCity" size="20" maxLength="75" style="border:1px solid #C0C0C0;padding-left:2px;padding-right:2px;padding-bottom:1px;height:18px;font-family:Tahoma,Verdana,Arial;font-size:11px;display:none" tabIndex="825"/>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairLocationCity2', /Root, 'RepairLocationCity', 120, string($dataDisabled), 50, 'true', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td>State:</td>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('txtRepairLocationState', /Root, 'RepairLocationState', 'StateList', 'false', 6, string($dataDisabled), 52, 'false', 1, 140, '', true, 'populateCounty()')"/>
        </td>
        <td>County:</td>
        <td>
          <!-- <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairLocationCounty', /Root, 'RepairLocationCounty', 120, string($dataDisabled), 52, 'true', 'true', '')"/> -->
          <IE:APDCustomSelect id="txtRepairLocationCounty" name="txtRepairLocationCounty" blankFirst="false" canDirty="false" CCDisabled="false" type="edit" CCTabIndex="826" required="false" width="120" onChange="">
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Phone:</td>
        <td>
          <IE:APDInputPhone id="txtRepairShopPhone" name="txtRepairShopPhone" required="false" canDirty="false" showExtension="true" includeMask="false" CCDisabled="false" CCTabIndex="827" onChange="">
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationPhone"/>
            </xsl:attribute>
          </IE:APDInputPhone>
          <!--<xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtRepairShopPhone', /Root, 'PhoneAreaCode', 'PhoneExchangeNumber', 'PhoneUnitNumber', 'PhoneExtensionNumber', 'RepairLocationPhone', 'true', 'false', '', 'false', 'false', 'true', '')"/>-->
        </td>
        <td>Fax:</td>
        <td>
          <IE:APDInputPhone id="txtRepairShopFax" name="txtRepairShopFax" value="" required="false" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="828" onChange="" >
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationFax"/>
            </xsl:attribute>
          </IE:APDInputPhone>
          <!--<xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('txtRepairShopFax', /Root, 'FaxAreaCode', 'FaxExchangeNumber', 'FaxUnitNumber', 'FaxExtensionNumber', 'RepairLocationFax', 'true', 'false', '', 'false', 'false', 'true', '')"/>-->
        </td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3">
          <IE:APDInput id="txtRepairShopEmailAddress" name="txtRepairShopEmailAddress" value="" size="50" maxLength="150" width=""  required="false" canDirty="false" CCDisabled="false" CCTabIndex="830" >
            <xsl:attribute name="value">
              <xsl:value-of select="/Root/@RepairLocationEmail"/>
            </xsl:attribute>
          </IE:APDInput>

          <!--<xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('txtRepairShopEmailAddress', /Root, 'RepairLocationEmail', 120, string($dataDisabled), 50, 'true', 'true', '')"/>-->
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
