<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="Notes">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="ShopLocationID"/>
<xsl:param name="UserID"/>
<xsl:param name="WindowID"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:variable name="Metadata"><xsl:copy-of select="/Root/Metadata"/></xsl:variable>

<HTML>

<HEAD>
<TITLE>Add Shop Document</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>

<script language="javascript">

  var gsWindowID = "<xsl:value-of select='$WindowID'/>";
  var gsShopLocationID = "<xsl:value-of select="$ShopLocationID"/>";
  var gsUserID = "<xsl:value-of select="$UserID"/>";
  var sRet = "";

<![CDATA[

  function pageInit(){
    if (oStat){
      oStat.style.left = (document.body.offsetWidth - 150) / 2 - 50;
      oStat.style.top = (document.body.offsetHeight - 75) / 2 - 25;
    }
  }

  function doClose(){
    sRet = "Close";
    window.close();
  }
  
  function doSave(){
    if (filUpload.value == ""){
      ClientWarning("Please select a file to upload");
      return;
    }
    if (selDocumentType.selectedIndex == -1){
      ClientWarning("Please select a Document Type.");
      return;
    }

    if (doFileUpload())
      sRet = "Save";
    window.close();
  }
  
  function onDlgClose(){
    window.returnValue = sRet;
  }

  function doFileUpload()
  {
    var aFiles = null;
    var sFileList = filUpload.value;
    aFiles = sFileList.split(";");
    
    //create an XML Document that will contain the individual file
    try {
    var oXML = new ActiveXObject("MSXML2.DOMDocument");
    } catch (e) {
      ClientWarning("The security settings on your computer does not conform to APD standards. Please contact Helpdesk.")
      return;
    }
    oXML.loadXML('<?xml version="1.0" ?> <root/>');
    oXML.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");
    
    txtComments.value = txtComments.value.replace(/<>/g, "");

    //add parameters to the root
    oRoot = oXML.documentElement.selectSingleNode("/root");
    if (oRoot) {
      oRoot.setAttribute("ShopLocationID", gsShopLocationID);
      oRoot.setAttribute("DocumentType", selDocumentType.text);
      oRoot.setAttribute("UserID", gsUserID);
      oRoot.setAttribute("EffectiveDate", txtEffectiveDate.value);
      oRoot.setAttribute("ExpirationDate", txtExpirationDate.value);
      oRoot.setAttribute("Comments", txtComments.value);
    } else {
      alert("Internal Error. No root node found.");
      return;
    }
    //create an ADO Stream to load the file
    var oADOStream = new ActiveXObject("ADODB.Stream");
    var oFSO = new ActiveXObject("Scripting.FileSystemObject");
    var sFile = "";
    
    for (var i = 0; i < aFiles.length; i++) {
      sFile = aFiles[i];
      sFile = sFile.replace(/\\/g, "\\\\"); //escape the back-slash
      sFile = sFile.replace(/\"/g, ""); // remove the double quote "

      var oFileNode = oXML.createElement("File");
      var oFile = oFSO.GetFile(sFile);
      var lFileLength = oFile.Size;
      oFile = null;

      oFileNode.setAttribute("FileName", oFSO.GetBaseName(sFile) + "." + oFSO.GetExtensionName(sFile));
      oFileNode.setAttribute("FileExt", oFSO.GetExtensionName(sFile));
      oFileNode.setAttribute("FileLength", lFileLength);
      oFileNode.dataType = "bin.base64";
      
      //read the file into the ADO Stream
      oADOStream.Type = 1; // Binary type
      oADOStream.Open();
      try {
        oADOStream.LoadFromFile(sFile);
      } catch (e) {
        ClientWarning("The security settings on your computer does not conform to APD standards. Please contact Helpdesk.")
        return;
      }
      
      //Store this file into the File node
      oFileNode.nodeTypedValue = oADOStream.Read(-1); // Read All data
      oADOStream.Close();
      oXML.documentElement.appendChild(oFileNode);
    }
    
    try {
    var oXMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
      ClientWarning("The security settings on your computer does not conform to APD standards. Please contact Helpdesk.")
      return;
    }
    oXMLHttp.open("POST","SMTDocumentUpload.asp",false);
    oXMLHttp.send(oXML);
    // show server message in message-area
    var sResponse = oXMLHttp.ResponseText;
    window.clipboardData.setData("Text", sResponse);
    if (isNaN(sResponse) == false)
      return true;
    else {
      ClientWarning(sResponse);
      return false;
    }
  }
  
]]>

</script>

</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0" onbeforeunload="onDlgClose()">
  <IE:APDStatus id="oStat" name="oStat" class="APDStatus" width="250" height="75" />
  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;" >
    <colgroup>
      <col width="100px" style="font-weight:bold"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td>Path to File:</td>
      <td>
        <IE:APDFileOpen id="filUpload" name="filUpload" width="250" 
          filterIndex="1" filter="All APD File,*.pdf;*.doc;*.tif;*.bmp;*.jpg;*.gif,Photographs,*.bmp;*.jpg;*.tif;*.gif" 
          canDirty="false" CCDisabled="false" dialogTitle="Select Files to Upload" required="true" tabIndex="1" multiple="false" maxFiles="1"
          onChange1="checkMultiple()"
          />        
      </td>
    </tr>
    <tr>
      <td>Type:</td>
      <td>
        <IE:APDCustomSelect id="selDocumentType" name="selDocumentType" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" required="true" width="250" onChange="">
          <xsl:for-each select="/Root/Reference[@ListName='DocumentType']">
        	<IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
            <xsl:value-of select="@Value"/>
          </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr>
      <td>Effective Date:</td>
      <td>
        <IE:APDInputDate id="txtEffectiveDate" name="txtEffectiveDate" type="date" futureDate="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="3" onChange="" />
      </td>
    </tr>
    <tr>
      <td>Expiration Date:</td>
      <td>
        <IE:APDInputDate id="txtExpirationDate" name="txtExpirationDate" type="date" futureDate="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="4" onChange="" />
      </td>
    </tr>
    <tr valign="top">
      <td>Comments:</td>
      <td>
        <IE:APDTextArea id="txtComments" name="txtComments" maxLength="500" width="250" height="50" required="false" canDirty="false" CCDisabled="false" CCTabIndex="5" onChange="" />
      </td>
    </tr>
    <tr>
      <td colspan="2" style="padding-left:125px">
        <IE:APDButton id="btnSave" name="btnSave" value="Save" width="75" CCDisabled="false" CCTabIndex="6" onButtonClick="doSave()"/>
        <IE:APDButton id="btnClose" name="btnClose" value="Close" width="75" CCDisabled="false" CCTabIndex="7" onButtonClick="doClose()"/>
      </td>
    </tr>
  </table>
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
