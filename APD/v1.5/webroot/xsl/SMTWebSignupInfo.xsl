<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ShopInfo">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Saved"/>
<xsl:param name="UserID"/>
<xsl:param name="ShopID"/>
<xsl:param name="ShopLocationID"/>

<xsl:template match="/Root">

<!-- if merge was completed, go to the newly merged shop -->
<xsl:if test="$ShopLocationID != ''">
  <SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
  <script>
    var sShopLocationID = '<xsl:value-of select="$ShopLocationID"/>';
    var sShopID = '<xsl:value-of select="$ShopID"/>';
    NavToShop('S', sShopLocationID, sShopID);
  </script>
</xsl:if>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID          SP                   XSL       PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopLoadGetDetailXML,SMTWebSignupInfo.xsl, 885  -->

<HEAD>
  <TITLE>Web Shop Load</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->


<script language="javascript">
	var gbDirtyFlag = false;
	var gsShopLoadID = '<xsl:value-of select="ShopLoad/@ShopLoadID"/>';
	var gsMode="merge";
	var gsMergeDate = '<xsl:value-of select="ShopLoad/@MergeDate"/>';
  
<![CDATA[

function InitPage(){

	onpasteAttachEvents();
	selGlassReplChgType_onchange();
	selTowChgType_onchange();
	selContactMethod_onchange(frmShop.selSPPreferredContactMethodID);
	selContactMethod_onchange(frmShop.selSLPPreferredContactMethodID);
	top.gsFramePageFile = "";
  
	//set dirty flags associated with all inputs and selects and checkboxes.
	var aInputs = null;
	var obj = null;
  
	aInputs = document.all.tags("INPUT");
	for (var i=0; i < aInputs.length; i++) {
		if (gsMergeDate == "1900-01-01T00:00:00"){
			if (aInputs[i].type=="text"){
				aInputs[i].attachEvent("onchange", SetDirty);
				aInputs[i].attachEvent("onkeypress", SetDirty);
				aInputs[i].attachEvent("onpropertychange", SetDirty);
			}
   
			if (aInputs[i].type=="checkbox")
				aInputs[i].attachEvent("onclick", SetDirty);
        
			if (aInputs[i].type=="radio")
				aInputs[i].attachEvent("onclick", SetDirty);
		}
		else{
			aInputs[i].readOnly = true;
			aInputs[i].disabled = true;
		}
	}
	
	aInputs = document.all.tags("SELECT");
	for (var i=0; i < aInputs.length; i++){
		if (gsMergeDate != "1900-01-01T00:00:00"){
			aInputs[i].readOnly = true;
			aInputs[i].disabled = true;
		}
		else
			aInputs[i].attachEvent("onchange", SetDirty);
	}  
  
	aInputs = document.all.tags("TEXTAREA");
	for (var i=0; i < aInputs.length; i++){
		if (gsMergeDate != "1900-01-01T00:00:00"){
			aInputs[i].readOnly = true;
			aInputs[i].disabled = true;
		}
		else
			aInputs[i].attachEvent("onchange", SetDirty);
	}  
  
	if (gsMergeDate != "1900-01-01T00:00:00"){
		frmShop.btnMerge.disabled = true;
		frmShop.btnSave.disabled = true;
	}
}

function SetDirty(){
	gbDirtyFlag = true;
}

function CheckDirty(){
	if (gbDirtyFlag) {
		var sSave = YesNoMessage("Web Signup Info", "Some information on this page has changed. Do you want to save the changes?");
		if (sSave == "Yes")
			return Save();
	}
	gbDirtyFlag = false;
	return true;
}

function Save(){
	frmShop.btnMerge.disabled = true;
	frmShop.btnSave.disabled = true;
  
	//set values on program and referral shop flags
	frmShop.chkProgramFlag.value =  frmShop.chkProgramFlag.checked
	frmShop.chkReferralFlag.value = frmShop.chkReferralFlag.checked  
  
	if (!ValidatePage()){
		frmShop.btnMerge.disabled = false;
		frmShop.btnSave.disabled = false;
		return false;
	}

	frmShop.action += "?act=save";
	frmShop.submit();
}

function Merge(){
	frmShop.btnMerge.disabled = true;
	frmShop.btnSave.disabled = true;
  
	//set values on program and referral shop flags
	frmShop.chkProgramFlag.value =  frmShop.chkProgramFlag.checked
	frmShop.chkReferralFlag.value = frmShop.chkReferralFlag.checked    
  
	if (frmShop.txtShopID.value == "" && frmShop.txtShopLocationID.value == "" && frmShop.rdAddShop.checked == false){
		parent.ClientWarning("Please indicate how you wish to Merge this data by making a selection in the Merge Info section.");
		frmShop.btnMerge.disabled = false;
		frmShop.btnSave.disabled = false;
		return false;
	} 
  
	if (!ValidateFedTaxID(frmShop.txtFedTaxID)){
		frmShop.btnMerge.disabled = false;
		frmShop.btnSave.disabled = false;
		return false;
	}
    
	if (!ValidatePage()){
		frmShop.btnMerge.disabled = false;
		frmShop.btnSave.disabled = false;
		return false;
	}
  
	  frmShop.action += gbDirtyFlag == true ? "?dirty=1&act=merge" : "?act=merge";
	  frmShop.submit();
}

function NoNegatives(event){
	if (event.keyCode == 45)
		event.returnValue = false;
}

function checkNumber() {

	if (isNaN(event.srcElement.value)) {
		parent.ClientWarning("Invalid numeric or decimal value specified.");
		event.srcElement.focus();
		event.srcElement.select();
	}

	switch(event.srcElement.id){
		case "txtLatitude":
			if (Math.abs(frmShop.txtLatitude.value) > 90){
				parent.ClientWarning("Latitude must be between -90 and 90 degrees.");
				event.srcElement.select();
			}
			break;
		case "txtLongitude":
			if (Math.abs(frmShop.txtLongitude.value) > 180){
				parent.ClientWarning("Longitude must be between -180 and 180 degrees.");
				event.srcElement.select();
			}
			break;
	}
}

function ValidateAllPhones() {
    
	if (validatePhoneSections(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber, frmShop.txtPhoneUnitNumber) == false) return false;
		if (validatePhoneSections(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber, frmShop.txtFaxUnitNumber) == false) return false;
  
  
	if (isNumeric(frmShop.txtPhoneExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified.");
		frmShop.txtPhoneExtensionNumber.focus();
		frmShop.txtPhoneExtensionNumber.select();
		return false;
	}

	if (isNumeric(frmShop.txtFaxExtensionNumber.value) == false){
		parent.ClientWarning("Invalid numeric value specified.");
		frmShop.txtFaxExtensionNumber.focus();
		frmShop.txtFaxExtensionNumber.select();
		return false;
	}
  
	validateAreaCodeSplit(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber);
	validateAreaCodeSplit(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber);
	
	return true;
}

function ValidatePage(){

    if (frmShop.selBusinessType.selectedIndex <= 0){
		parent.ClientWarning("A value must be selected for Business Type.");
		frmShop.selBusinessType.focus();
		return false;
    }
    
    if (frmShop.selRepairFacilityType.selectedIndex <= 0){
		parent.ClientWarning("A value must be selected for Repair Facility Type.");
		frmShop.selRepairFacilityType.focus();
		return false;
    }
	
	  if (frmShop.txtCertifiedFirstID.selectedIndex <= 0){
		parent.ClientWarning("A value must be selected for Targeting Comment Type.");
		frmShop.txtCertifiedFirstID.focus();
		return false;
    }
        
    if (frmShop.txtName.value.replace(/\s*$/, "") == ""){
		parent.ClientWarning("A value is required in the 'Name' field.");
		frmShop.txtName.value = "";
		frmShop.txtName.focus();
		return false;
	}

   //  if (frmShop.txtSPName.value.replace(/\s*$/, "") == ""){
   //	parent.ClientWarning("A Name must be entered for Shop Contact")
  //     frmShop.txtSPName.value = "";
  //	frmShop.txtSPName.focus();
 // 	return false;
 //  }

    if (frmShop.txtSLPName.value.replace(/\s*$/, "") == ""){
		parent.ClientWarning("A Name must be entered for Shop Contact")
		frmShop.txtSLPName.value = "";
		frmShop.txtSLPName.focus();
		return false;
    }
    
    if (frmShop.selContactType.selectedIndex <= 0){
		parent.ClientWarning("A value must be selected for Contact Type.");
		frmShop.selContactType.focus();
		return false;
    }
    
  //  if (frmShop.selContactType2.selectedIndex <= 0){
  //	parent.ClientWarning("A value must be selected for both Contact Types.");
  //	frmShop.selContactType2.focus();
  //	return false;
  //  }

    if (frmShop.selWorkmanshipWarranty.selectedIndex <= 0){
		parent.ClientWarning("A warranty period must be selected for both 'Warranty' types for Program Shops.");
		frmShop.selWorkmanshipWarranty.focus();
		return false;
    }

    if (frmShop.selRefinishWarranty.selectedIndex <= 0){
		parent.ClientWarning("A warranty period must be selected for both 'Warranty' types for Program Shops.");
		frmShop.selRefinishWarranty.focus();
		return false;
    }

    var comment = frmShop.txtDrivingDirections;
	var maxLen = comment.getAttribute("maxlength");
	
	if (comment.value.length > maxLen){
		comment.value = comment.value.substr(0, maxLen);
		parent.ClientWarning("The maximum length for Landmarks (driving directions) is " + maxLen + ".  Your comment has been trimmed to this length.  To continue click 'Save' again.");
		return false;
	}
    
    if (!ValidateContactMethod(frmShop.selSPPreferredContactMethodID)) return false;
    if (!ValidateContactMethod(frmShop.selSLPPreferredContactMethodID)) return false;
    
    
    if (frmShop.txtOEMDiscounts.value.length > 250){
		parent.ClientWarning("Text entered into OEM Discounts must be limited to no more than 250 characters.");
		frmShop.txtOEMDiscounts.focus();
		return false;
    }
 
    //validate phone numbers
    if (ValidateAllPhones() == false)
		return false;

	// validate location hours
	var badHoursMsg = "The Hours of Operation Start time (left) must be less than the End time (right)."

	if ((frmShop.txtMondayStartTime.value != "" || frmShop.txtMondayEndTime.value != "") &&
		(frmShop.txtMondayEndTime.value <= frmShop.txtMondayStartTime.value || frmShop.txtMondayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtMondayStartTime.select();
			return false;
	}

	if ((frmShop.txtTuesdayStartTime.value != "" || frmShop.txtTuesdayEndTime.value != "") &&
		(frmShop.txtTuesdayEndTime.value <= frmShop.txtTuesdayStartTime.value || frmShop.txtTuesdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtTuesdayStartTime.select();
			return false;
	}

	if ((frmShop.txtWednesdayStartTime.value != "" || frmShop.txtWednesdayEndTime.value != "") &&
		(frmShop.txtWednesdayEndTime.value <= frmShop.txtWednesdayStartTime.value || frmShop.txtWednesdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtWednesdayStartTime.select();
			return false;
	}

	if ((frmShop.txtThursdayStartTime.value != "" || frmShop.txtThursdayEndTime.value != "") &&
		(frmShop.txtThursdayEndTime.value <= frmShop.txtThursdayStartTime.value || frmShop.txtThursdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtThursdayStartTime.select();
			return false;
	}

	if ((frmShop.txtFridayStartTime.value != "" || frmShop.txtFridayEndTime.value != "") &&
		(frmShop.txtFridayEndTime.value <= frmShop.txtFridayStartTime.value || frmShop.txtFridayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtFridayStartTime.select();
			return false;
	}

	if ((frmShop.txtSaturdayStartTime.value != "" || frmShop.txtSaturdayEndTime.value != "") &&
		(frmShop.txtSaturdayEndTime.value <= frmShop.txtSaturdayStartTime.value || frmShop.txtSaturdayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtSaturdayStartTime.select();
			return false;
	}

	if ((frmShop.txtSundayStartTime.value != "" || frmShop.txtSundayEndTime.value != "") &&
		(frmShop.txtSundayEndTime.value <= frmShop.txtSundayStartTime.value || frmShop.txtSundayStartTime.value == "")){
			parent.ClientWarning(badHoursMsg);
			frmShop.txtSundayStartTime.select();
			return false;
	}
        
    return true;
}

function ValidateContactMethod(oSel){

	var sType = oSel.name.slice(0,2) == "SL" ? "SLP" : "SP";
        
	switch (oSel.options[oSel.selectedIndex].text){
		case "Email":
			oText = eval("frmShop.txt" + sType + "EmailAddress" );
			if (oText.value == ""){  
				parent.ClientWarning("You have selected a Contact Type of Email.  Please provide an Email Address.");
				oText.focus();
				return false;
			}
			break;
		case "Phone":
			oText = eval("frmShop.txt" + sType + "PhoneAreaCode");
			if (oText.value == ""){  
				parent.ClientWarning("You have selected a Contact Type of Phone.  Please provide a Phone Number.");
				oText.focus();
				return false;
			}
			break;
		case "Fax":
			oText = eval("frmShop.txt" + sType + "FaxAreaCode");
			if (oText.value == ""){  
				parent.ClientWarning("You have selected a Contact Type of Fax.  Please provide a Fax Number.");
				oText.focus();
				return false;
			}
			break;
		case "Cell Phone":
			oText = eval("frmShop.txt" + sType + "CellAreaCode");
			if (oText.value == ""){  
				parent.ClientWarning("You have selected a Contact Type of Cell.  Please provide a Cell Number.");
				oText.focus();
				return false;
			}
			break;
		case "Pager":
			oText = eval("frmShop.txt" + sType + "PagerAreaCode");
			if (oText.value == ""){  
				parent.ClientWarning("You have selected a Contact Type of Pager.  Please provide a Pager Number.");
				oText.focus();
				return false;
			}
			break;
	}
	
	return true;
}

function CheckNum(event){
	var obj = eval("frmShop." + event.srcElement.id);
	var idx = obj.value.indexOf(".")
	var pct = false;
	var prec;
	var code = event.keyCode;

  	if (obj.getAttribute("precision") != null)
    	prec = obj.getAttribute("precision");
  	else
    	prec = 2;

	if ((code < 48 && code != 46) || code > 57){ // limit to numerals and decimal point
		event.returnValue = false;
		return;
	}

	if (arguments[1])
		pct = true;

	if (code == 45){				// no negatives
		event.returnValue = false;
		return;
	}

	if (idx > -1){
		//if (obj.value.length - idx > prec){		// allow only proper number decimal places -- 1 for percentages, 2 otherwise
		//	event.returnValue = false;
		//	return;
		//}

		if (code == 46){			// allow only one decimal point
			event.returnValue = false;
			return;
		}
	}

	if (pct){								// ensure percentages do not exceed 100
		if (obj.value == 100){
			event.returnValue = false
			return;
		}

		if (obj.value == 10 && code!= 46 && code != 48 && idx == -1){
			event.returnValue = false;
			return;
		}

		if (obj.value > 10 && code != 46 && idx == -1){
			event.returnValue = false;
			return;
		}
	}
}

function txtDrivingDirections_onkeypress(obj, event){
  if (obj.value.length > obj.getAttribute("maxlength") - 1){
	  event.returnValue = false;
		return;
	}
 }

function selGlassReplChgType_onchange(){
	lblWindshield.style.display = "none";
	lblGlassDollar.style.display = "none";
	lblWindshieldPercent.style.display = "none";
	frmShop.txtWindshieldDiscount.style.display = "none";
	lblSideBack.style.display = "none";
	frmShop.txtGlassOutsourceSvcFee.style.display = "none";
	lblGlassPercent.style.display = "none";

	frmShop.txtSideBackGlassDiscount.style.display = "none";
	lblSubletFee.style.display = "none";
	frmShop.txtGlassSubletSvcFee.style.display = "none";
	lblSubletPct.style.display = "none";
	frmShop.txtGlassSubletSvcPct.style.display = "none";

	switch (frmShop.selGlassReplChgType.value){
		case "N":
			lblWindshield.style.display = "inline";
			frmShop.txtWindshieldDiscount.style.display = "inline";
			lblWindshieldPercent.style.display = "inline";
			lblSideBack.style.display = "inline";
			frmShop.txtSideBackGlassDiscount.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "S":
			lblSubletFee.style.display = "inline";
			lblGlassDollar.style.display = "inline";
			frmShop.txtGlassSubletSvcFee.style.display = "inline";
			lblSubletPct.style.display = "inline";
			frmShop.txtGlassSubletSvcPct.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "O":
			lblGlassDollar.style.display = "inline";
			frmShop.txtGlassOutsourceSvcFee.style.display = "inline";
			break;
	}
}

function selTowChgType_onchange(){
	if (frmShop.selTowChargeType.value == "F"){
		lblFlatFee.style.display = "inline";
		lblFFDollar.style.display= "inline";
		frmShop.txtFlatFee.style.display = "inline";
	}
	else{
		lblFlatFee.style.display = "none";
		lblFFDollar.style.display= "none";
		frmShop.txtFlatFee.style.display = "none";
	}
}

function txtFedTaxID_onBlur(){
	//if (frmShop.txtFedTaxID.dirty == true) ValidateFedTaxID(document.all.txtFedTaxID);
	//frmShop.txtFedTaxID.dirty = "false";
}

function txtFedTaxID_onKeyPress(){
  if (event.keyCode == 45 || event.keyCode == 46){ 
    event.returnValue = false;
    return false;
  }    
    
  NumbersOnly(window.event); 
  this.dirty=true;
}

function MergeAction_onclick(obj){
  frmShop.txtShopID.value = obj.getAttribute("shopid");
  frmShop.txtShopLocationID.value = obj.getAttribute("shoplocationid");
}

function selContactMethod_onchange(obj){
  try{
    var iType = obj.name.slice(0,2);
    var sMethod = obj.options[obj.selectedIndex].text;
    if (sMethod == "Cell Phone") sMethod = "Cell";
    
    eval("document.all.spEmail" + iType).style.visibility = "hidden";
    eval("document.all.spPhone" + iType).style.visibility = "hidden";
    eval("document.all.spFax" + iType).style.visibility = "hidden";
    eval("document.all.spPager" + iType).style.visibility = "hidden";
    eval("document.all.spCell" + iType).style.visibility = "hidden";
    if (sMethod == "Email" || sMethod == "Phone" || sMethod == "Fax" || sMethod == "Cell" || sMethod == "Pager")
      eval("document.all.sp" + sMethod + iType).style.visibility = "visible";
  }
  catch(e){}
}

function btnFill_onclick(){
	if ((frmShop.txtStartTime.value != "") && (frmShop.txtEndTime.value != "")) {
		if (frmShop.txtEndTime.value <= frmShop.txtStartTime.value){
			parent.ClientWarning("The Hours of Operation Start time (left) must be less than the End time (right).");
			frmShop.txtStartTime.select();
			return;
		}

		//Set all start times from Monday to Friday
		frmShop.txtMondayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtTuesdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtWednesdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtThursdayEndTime.value = frmShop.txtEndTime.value;
		frmShop.txtFridayEndTime.value = frmShop.txtEndTime.value;

		//Set all end  times from Monday to Friday
		frmShop.txtMondayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtTuesdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtWednesdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtThursdayStartTime.value = frmShop.txtStartTime.value;
		frmShop.txtFridayStartTime.value = frmShop.txtStartTime.value;
	}
	else if ((frmShop.txtEndTime.value == "") && (frmShop.txtStartTime.value == "")){
		//Set all start times from Monday to Friday
		frmShop.txtMondayEndTime.value = "";
		frmShop.txtTuesdayEndTime.value = "";
		frmShop.txtWednesdayEndTime.value = "";
		frmShop.txtThursdayEndTime.value = "";
		frmShop.txtFridayEndTime.value = "";

		//Set all end  times from Monday to Friday
		frmShop.txtMondayStartTime.value = "";
		frmShop.txtTuesdayStartTime.value = "";
		frmShop.txtWednesdayStartTime.value = "";
		frmShop.txtThursdayStartTime.value = "";
		frmShop.txtFridayStartTime.value = "";
	}
	else{
		parent.ClientWarning("The Hours of Operation Start and End times must both contain valid military times or both be empty.");
		frmShop.txtStartTime.select();
	}
}

function UpLevel(){
  	window.navigate("SMTMain.asp?PageID=WebSignups");
  }

]]>

</script>

</HEAD>

<BODY class="bodyAPDsub" onLoad="InitPage()" style="margin-top:0; margin-right:0; margin-bottom:0; overflow:hidden">
  <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
  <form id="frmShop" method="post" action="SMTWebSignupInfo.asp">
    <div style="background-color:#FFFAEB; width:750;">
      <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-bottom: .3cm ridge yellow;">
        <TR unselectable="on">
          <TD unselectable="on" width="250"><strong>Shop Load ID: </strong><xsl:value-of select="@ShopLoadID"/></TD>        
          <TD unselectable="on" height="25" style="color : #000066;"></TD>
          <TD><IMG src="/images/spacer.gif" width="138" height="4" border="0" /></TD>
          <TD width="100" style="color:red; font-weight:bold"><xsl:if test="ShopLoad/@MergeDate != '1900-01-01T00:00:00'">READ ONLY</xsl:if></TD>
          <TD><IMG src="/images/spacer.gif" width="138" height="4" border="0" /></TD>
        	<TD unselectable="on" align="right">
            <input type="button" id="btnMerge" value="Merge" class="formButton" style="width:80" onclick="Merge()"/>
            <input type="button" id="btnSave" value="Save" class="formButton" style="width:80" onClick="Save()"/>
          </TD> 
          <TD unselectable="on" align="right"><IMG src="/images/smrefresh.gif" onClick="UpLevel()" border="0" width="16" height="16" alt="Return to Search Results" style="cursor:hand;" /></TD>
        	<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
      </TABLE>
      <IMG src="/images/spacer.gif" width="188" height="4" border="0"/>
      <div style="height:510; width:750; overflow:auto"><xsl:apply-templates select="ShopLoad"/></div>
    </div>
    
    <input type="hidden" id="txtShopID" name="ShopID"/>
    <input type="hidden" id="txtShopLocationID" name="ShopLocationID"/>
    <input type="hidden" id="txtSysLastUserID" name="SysLastUserID"><xsl:value-of select="$UserID"/></input>
  </form>
  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</BODY>  
</HTML>


</xsl:template>


<xsl:template match="ShopLoad">
  
  <strong>Merge Info:</strong>
  <xsl:call-template name="MergeInfo"/>
  <hr/>
  <strong>Business Info:</strong>
  <xsl:call-template name="BusinessInfo"/>
  <hr/>
  <strong>Shop Info:</strong>
  <xsl:call-template name="ShopInfo"/>
  <hr/>
  <strong>Shop Contact:</strong>
  <xsl:call-template name="ShopPersonnel"/>
  <hr/>
  <strong>Shop Contact:</strong>
  <xsl:call-template name="BusinessPersonnel"/>
  <hr/>
  <strong>Shop Pricing:</strong>
  <xsl:call-template name="Pricing"/>
  <hr/>
  <strong>Shop OEM Discounts:</strong>
  <xsl:call-template name="OEMDiscounts"/>
  
  <IMG src="/images/spacer.gif" width="20" height="20" border="0" />
  <br/>
  
  <input type="hidden" id="txtShopLoadID" name="ShopLoadID"><xsl:attribute name="value"><xsl:value-of select="@ShopLoadID"/></xsl:attribute></input>
  <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute>
  </input>

</xsl:template>


<xsl:template name="MergeInfo">
  <TABLE border="0" width="680" syle="table-layout:fixed">
    <TR>
      <COLGROUP>
        <COL width="400"/>
        <COL width="30"/>
        <COL width="250" align="right"/>
      </COLGROUP>
      <TD>
        <table>
          <tr>
            <colgroup>
              <col width="15"/>
              <col width="340"/>
              <col width="45"/>
            </colgroup>
          </tr>
          <xsl:if test="count(/Root/ShopLocation) > 0">
            <tr><td colspan="3" style="font-weight:bold">Update this existing Shop</td></tr>
            <xsl:apply-templates select="/Root/ShopLocation"/>
          </xsl:if>
          <xsl:if test="count(/Root/Shop) > 0">
            <tr><td colspan="2" style="font-weight:bold">Add new Shop to this existing Business</td></tr>
            <xsl:apply-templates select="/Root/Shop"/>
          </xsl:if>
          <tr>
            <td>
              <input type="radio" id="rdAddShop" name="MergeAction" shopid="" shoplocationid="" onclick="MergeAction_onclick(this)"/>
            </td>
            <td style="color:darkblue; font-weight:bold">Create new Business and attach this Shop to it</td>
          </tr>
        </table>
        
        <img src="/images/spacer.gif" width="30" height="25" border="0"/>
      </TD>
      <TD></TD> <!-- spacer column -->
      <TD valign="top">
        <table>
          <colgroup>
            <col width="10"/>
            <col width="200"/>
            <col/>
          </colgroup>
          <tr>
            <td colspan="2" style="font-weight:bold;">Web Signup Record Status</td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td style="color:darkblue;">Applicant First Visited Site</td>
            <td>
              <xsl:choose>
                <xsl:when test="@SiteFirstVisitDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@SiteFirstVisitDate))"/></xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
              </xsl:choose>  
            </td>
          </tr>
          <tr>
            <td></td>
            <td style="color:darkblue;">Applicant Submitted Qualifications</td>
            <td>
              <xsl:choose>
                <xsl:when test="@QualificationsSubmitedDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@QualificationsSubmitedDate))"/></xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
              </xsl:choose> 
            </td>
          </tr>
          <tr>
            <td></td>
            <td style="color:darkblue;">Applicant Saved to Come Back Later</td>
            <td>
              <xsl:choose>
                <xsl:when test="@DataEntryDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@DataEntryDate))"/></xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
              </xsl:choose> 
            </td>
          </tr>
          <tr>
            <td></td>
            <td style="color:darkblue;">Applicant Final Submission</td>
            <td>
              <xsl:choose>
                <xsl:when test="@FinalSubmitDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@FinalSubmitDate))"/></xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td></td>
            <td style="color:darkblue;">Applicant Merged</td>
            <td>
              <xsl:choose>
                <xsl:when test="@MergeDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@MergeDate))"/></xsl:when>
                <xsl:otherwise>no</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
</xsl:template>

<xsl:template name="BusinessInfo">
  <table>
    <tr>
		<td width="5"></td>
		<td nowrap="on" width="55">SSN/EIN:</td>
		<td width="240">
			<input type="text" id="txtFedTaxID" name="FedTaxID" class="inputFld" dirty="false" maxlength="9" onchange="this.dirty=true;" onkeypress="txtFedTaxID_onKeyPress()">
				<xsl:attribute name="value"><xsl:value-of select="/Root/ShopLoad/@FedTaxId"/></xsl:attribute>
			</input>
		</td>
		<td><img src="/images/spacer.gif" width="30" height="5" border="0"/></td>
		<td width="10">*</td>
		<td nowrap="on" width="150">Business Type:</td>
		<td>
			<select id="selBusinessType" name="BusinessTypeCD">
				<option></option>
				<xsl:for-each select="/Root/Reference[@ListName='BusinessType']">
					<xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/ShopLoad/@BusinessTypeCD"/></xsl:call-template>
				</xsl:for-each>
			</select>
		</td>
    </tr>
    <tr>
		<td width="5"></td>
		<td>Program Shop:</td>
			<xsl:choose>
				<xsl:when test="/Root/ShopLoad/@SLProgramFlag=1">
					<td><input type="checkbox" id="chkProgramFlag" name="SLProgramFlag" class="inputFld" checked="yes"></input></td>
				</xsl:when>
				<xsl:otherwise>
					<td><input type="checkbox" id="chkProgramFlag" name="SLProgramFlag" class="inputFld"></input></td>
				</xsl:otherwise>
			</xsl:choose>		

		<!--
		<td><input type="checkbox" id="chkProgramShop" name="ProgramShop" class="inputFld" checked="yes"></input>
			<input type="text"><xsl:attribute name="value"><xsl:value-of select="/Root/ShopLoad/@SLProgramFlag"/></xsl:attribute></input>
		</td>
		-->
		
		<td><img src="/images/spacer.gif" width="30" height="5" border="0"/></td>
		<td width="10"></td>
		
		<td>Referral Shop:</td>
			<xsl:choose>
				<xsl:when test="/Root/ShopLoad/@SLReferralFlag=1">
					<td><input type="checkbox" id="chkReferralFlag" name="SLReferralFlag" class="inputFld" checked="yes"></input></td>
				</xsl:when>
				<xsl:otherwise>
					<td><input type="checkbox" id="chkReferralFlag" name="SLReferralFlag" class="inputFld"></input></td>
				</xsl:otherwise>
			</xsl:choose>				
	</tr>
  </table>
</xsl:template>

<xsl:template match="/Root/ShopLocation">
  <tr>
    <td valign="top">
      <input type="radio" name="MergeAction" shopid="" onclick="MergeAction_onclick(this)">
        <xsl:attribute name="id">rdMergeLocation<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="shoplocationid"><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
        <xsl:attribute name="shopid"><xsl:value-of select="@ShopID"/></xsl:attribute>
      </input>
    </td>
    <td style="color:darkblue">
      <strong><xsl:value-of select="@ShopLocationID"/></strong>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <strong><xsl:value-of select="@Name"/></strong><br/>
      <xsl:value-of select="@Address1"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@Address2"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@AddressCity"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@AddressState"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@AddressZip"/><br/>
      <xsl:value-of select="@PhoneAreaCode"/>-<xsl:value-of select="@PhoneExchangeNumber"/>-<xsl:value-of select="@PhoneUnitNumber"/>
    </td>
    <td valign="top">
      <table>
        <tr>
          <td width="20" valign="top"><xsl:if test="@CEIProgramFlag=1">
            <span style="color:red; font-weight:bold; font-size:9;">CEI</span></xsl:if>
          </td>
          <td width="5"></td>
          <td width="20" valign="top">
            <xsl:if test="@ProgramFlag=1">
              <span style="font-weight:bold; font-size:9;">LYN</span>
              <span style="color:red; font-weight:bold; font-size:9;">X</span>
            </xsl:if>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</xsl:template>

<xsl:template match="/Root/Shop">
  <tr>
    <td valign="top">
      <input type="radio" name="MergeAction" shoplocationid="" onclick="MergeAction_onclick(this)">
        <xsl:attribute name="id">rdAddLocation<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="shopid"><xsl:value-of select="@ShopID"/></xsl:attribute>
      </input>
    </td>
    <td style="color:darkblue">
      <strong><xsl:value-of select="@ShopID"/></strong>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <strong><xsl:value-of select="@Name"/></strong><br/>
      <xsl:value-of select="@Address1"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@Address2"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@AddressCity"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@AddressState"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:value-of select="@AddressZip"/><br/>
      <xsl:value-of select="@PhoneAreaCode"/>-<xsl:value-of select="@PhoneExchangeNumber"/>-<xsl:value-of select="@PhoneUnitNumber"/>
    </td>
  </tr>
</xsl:template>

<xsl:template name="ShopInfo">
  <TABLE cellSpacing="0" cellPadding="0" border="0" width="660" style="table-layout:fixed;">  <!-- master table -->
    <TR>
	    <colgroup>
	      <col width="335"/>
		    <col width="20"/>
		    <col width="362"/>
		  </colgroup>
    </TR>
	  <TR><TD><IMG src="/images/spacer.gif" width="6" height="6" border="0" /></TD></TR>
    <TR>
      <TD valign="top"><!-- column 1 : shop info -->
        <TABLE border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
	        <TR>
            <colgroup>
              <col width="10"/>
		          <col width="60"/>
			        <col width="215"/>
		        </colgroup>
            <TD>*</TD>
		        <TD>Name:</TD>
            <TD nowrap="nowrap">
		          <input type="text" id="txtName" name="Name" autocomplete="off" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
			        </input>
		        </TD>
          </TR>
          <TR>
            <TD></TD>
            <TD>Address1:</TD>
            <TD>
              <input type="text" id="txtAddress1" name="Address1" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
			        </input>
            </TD>
		      </TR>
          <TR>
            <TD></TD>
            <TD>Address2:</TD>
            <TD>
              <input type="text" id="txtAddress2" name="Address2" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>
            <TD></TD>
		        <TD>Zip:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtAddressZip" name="AddressZip" size="9" class="inputFld" maxlength="5" onkeypress="NumbersOnly(event)">
			          <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>
            <TD></TD>
		        <TD>City:</TD>
            <TD>
              <input type="text" id="txtAddressCity" name="AddressCity" size="20" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
	        <TR>
            <TD></TD>
	          <TD>State:</TD>
            <TD nowrap="nowrap">
              <select id="selState" name="AddressState" class="inputFld">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='State']">
			            <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/ShopLoad/@AddressState"/></xsl:call-template>
			          </xsl:for-each>
              </select>
		        </TD>
          </TR>
	        <TR>
            <TD></TD>
	          <TD>County:</TD>
            <TD>
              <input type="text" id="txtAddressCounty" name="AddressCounty" size="20" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
			        </input>
            </TD>
	        </TR>
          <TR>
            <TD></TD>
	          <TD>Phone:</TD>
            <TD noWrap="nowrap">
		          <xsl:call-template name="ContactNumbers2"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
		        </TD>
          </TR>
          <TR>
            <TD></TD>
            <TD>Fax:</TD>
            <TD noWrap="nowrap">
              <xsl:call-template name="ContactNumbers2"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD></TD>
            <TD>Email:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtEmailAddress" name="EmailAddress" size="37" class="inputFld" onbeforedeactivate="checkEMail(this)">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='EmailAddress']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD></TD>
            <TD noWrap="nowrap">Web Site:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtWebSiteAddress" name="WebSiteAddress" size="37" class="inputFld" onbeforedeactivate="checkURL(this)">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='WebSiteAddress']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@WebSiteAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
        </TABLE>
      </TD><!-- end of column 1 -->
      <TD noWrap="nowrap"><!-- spacer column --> </TD>
      <TD valign="top"> <!-- column 2 Shop Program info -->
        <TABLE border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
	        <TR>
		        <colgroup>
              <col width="10"/>
		          <col width="150"/>
		          <col width="225"/>
		        </colgroup>
          </TR>
          <TR>
            <TD>*</TD>
            <TD>Repair Facility Type:</TD>
            <TD>
              <select id="selRepairFacilityType" name="RepairFacilityTypeCD" class="inputFld">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='RepairFacilityType']">
			            <xsl:call-template name="BuildSelectOptions">
                    <xsl:with-param name="current" select="/Root/ShopLoad/@RepairFacilityTypeCD"/>
                  </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <!--<TR>
            <TD></TD>
            <TD nowrap="nowrap">Targeting Comment:</TD>
            <TD nowrap="nowrap">
              <input type="text" id="txtCertifiedFirstId" name="CertifiedFirstId" class="inputFld" size="15">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='CertifiedFirstId']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@CertifiedFirstId"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>-->

			<TR>
				<TD>*</TD>
				<TD valign="top" nowrap="nowrap">Targeting Comment:</TD>

				<TD>
					<select id="txtCertifiedFirstID"  name="CertifiedFirstID" wizname="SCertifiedFirstID" dirty="false" class="inputFld" onchange="this.dirty='true'">
						<option></option>
						
						<xsl:choose>
							<xsl:when test="@CertifiedFirstId = 'Registered'">
								<option value="Registered" selected="selected">Registered</option>
							</xsl:when>
							<xsl:otherwise>
								<option value="Registered">Registered</option>
							</xsl:otherwise>
						</xsl:choose>
						

						<xsl:choose>
							<xsl:when test="@CertifiedFirstId != ''">
								<xsl:value-of select="@CertifiedFirstId"/>
							</xsl:when>
						</xsl:choose>
					
					</select>



				</TD>
			</TR>
			<TR>
			<TD>*</TD>
            <TD nowrap="nowrap">Workmanship Warranty:</TD>
            <TD>
              <select id="selWorkmanshipWarranty" name="WarrantyPeriodWorkmanshipCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/ShopLoad/@WarrantyPeriodWorkmanshipCD"/></xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		      <TR>
            <TD>*</TD>
            <TD nowrap="nowrap">Refinish Warranty:</TD>
            <TD>
              <select id="selRefinishWarranty" name="WarrantyPeriodRefinishCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/ShopLoad/@WarrantyPeriodRefinishCD"/></xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD></TD>
            <TD>Pref Estimate Package:</TD>
            <TD>
              <select id="selPreferredEstimatePackageID" name="PreferredEstimatePackageID" class="inputFld">
			          <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='EstimatePackage']">
			            <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/ShopLoad/@PreferredEstimatePackageID"/></xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		    </TABLE>
        <TABLE cellpadding="0" cellspacing="0" border="0">
	        <TR>
            <td width="10"></td>
            <TD noWrap="nowrap" align="left" valign="top" colSpan="2" class="boldtext" width="150">
              <img src="/images/spacer.gif" width="148" height="1" border="0"/>
              Hours of Operation:
            </TD>
		        <TD>
	            <xsl:call-template name="ShopHours"/>
		        </TD>
		      </TR>
		    </TABLE>
      </TD> <!-- end of column 2 -->
    <TR>
    <TD colspan="3">
      <table border="0">
        <tr>
          <td width="55" valign="top">Landmarks:</td>
          <td align="left">
            <textarea id="txtDrivingDirections" name="DrivingDirections" cols="92" rows="6" onkeypress="txtDrivingDirections_onkeypress(this, event)">
              <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopLoad']/Column[@Name='DrivingDirections']/@MaxLength"/></xsl:attribute>
              <xsl:value-of select="@DrivingDirections"/>
            </textarea>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  </TR>
</TABLE>  <!-- end of master table -->
</xsl:template>

<xsl:template name="ShopHours">
<table>
  <TR>
    <TD nowrap="nowrap"><input type="button" id="btnFill" value="  Fill  " onclick="btnFill_onclick()" class="formButton"/></TD>
    <TD noWrap="nowrap">
      <input type="text" id="txtStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)"/>
      -
      <input type="text" id="txtEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)"/>
    </TD>
  </TR>
  <TR>
    <TD>Mon:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtMondayStartTime" name="OperatingMondayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingMondayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtMondayEndTime" name="OperatingMondayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingMondayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Tue:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtTuesdayStartTime" name="OperatingTuesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingTuesdayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtTuesdayEndTime" name="OperatingTuesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingTuesdayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Wed:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtWednesdayStartTime" name="OperatingWednesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingWednesdayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtWednesdayEndTime" name="OperatingWednesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingWednesdayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Thu:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtThursdayStartTime" name="OperatingThursdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingThursdayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtThursdayEndTime" name="OperatingThursdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingThursdayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Fri:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtFridayStartTime" name="OperatingFridayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingFridayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtFridayEndTime" name="OperatingFridayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingFridayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Sat:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSaturdayStartTime" name="OperatingSaturdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingSaturdayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSaturdayEndTime" name="OperatingSaturdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingSaturdayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Sun:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSundayStartTime" name="OperatingSundayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingSundayStartTime)"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSundayEndTime" name="OperatingSundayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="normalize-space(@OperatingSundayEndTime)"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
</table>


</xsl:template>

<xsl:template name="ShopPersonnel">
   <xsl:variable name="SLPPersonnelID" select="@SLPPersonnelID"/>

  <div style="height:95; width:675;">

	  <input type="hidden" id="txtSLPPersonnelID" name="SLPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@SLPPersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>

	  <input type="hidden" id="txtSLPGenderCD" name="SLPGenderCD"></input>

    <!--
	  <input type="hidden" id="txtSLPPersonnelTypeID" name="SLPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@SLPPersonnelTypeID != '0'"><xsl:value-of select="@SLPPersonnelTypeID"/></xsl:when>
          <xsl:otherwise>2</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>
    -->
    
    <input type="hidden" id="txtSLPShopManagerFlag" name="SLPShopManagerFlag"  class="inputFld">
      <xsl:attribute name="value"><xsl:value-of select="@SLPShopManagerFlag"/></xsl:attribute>
    </input>

  <table cellSpacing="0" width="675px" cellPadding="1" border="0">
    <tr>
      <td width="10px">*</td>
      <td>Name:</td>
      <td>
         <input type="text" id="txtSLPName" name="SLPName" size="51"  class="inputFld" autocomplete="off">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='ShopLoad']/Column[@Name='SLPName']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@SLPName"/></xsl:attribute>
		    </input>        
      </td>
      <td width="10px">*</td>
      <td>Contact Type:</td>
      <td>
        <select id="selContactType" name="SLPPersonnelTypeID" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='PersonnelType']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/ShopLoad/@SLPPersonnelTypeID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      <!--
        <input type="checkbox" id="cbSLPShopManagerFlag" onclick="txtSLPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@SLPShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>
		    </input>
        
		    <input type="hidden" id="txtSLPShopManagerFlag" name="SLPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@SLPShopManagerFlag"/></xsl:attribute>
		    </input>-->
      </td>
    </tr>
    <tr>
      <td><span id="spEmailSL" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Email:</td>
      <td>
	      <input id="txtSLPEmailAddress" name="SLPEmailAddress" size="51" class="inputFld" onbeforedeactivate="checkEMail(this)">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='ShopLoad']/Column[@Name='SLPEmailAddress']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@SLPEmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSLPPreferredContactMethodID" name="SLPPreferredContactMethodID" class="inputFld" onchange="selContactMethod_onchange(this)">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/ShopLoad/@SLPPreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td><span id="spPhoneSL" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Phone:</td>
      <td nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"><span id="spFaxSL" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Fax: </td>
      <td nowrap="nowrap">
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td><span id="spCellSL" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Cell:</td>
      <td>
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"><span id="spPagerSL" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Pager: </td>
      <td>
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template>

<xsl:template name="BusinessPersonnel">
   <xsl:variable name="PersonnelID" select="@SPPersonnelID"/>

  <div style="height:95; width:675;">

	  <input type="hidden" id="txtSPPersonnelID" name="SPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@SPPersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>

	  <input type="hidden" id="txtSPGenderCD" name="SPGenderCD"></input>

    <!--
	  <input type="hidden" id="txtSPPersonnelTypeID" name="SPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@PersonnelTypeID != '0'"><xsl:value-of select="@SPPersonnelTypeID"/></xsl:when>
          <xsl:otherwise>6</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>
    -->
    
    <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag"  class="inputFld">
      <xsl:attribute name="value"><xsl:value-of select="@SPShopManagerFlag"/></xsl:attribute>
    </input>

  <table cellSpacing="0" width="675px" cellPadding="1" border="0">
    <tr>
      <td width="10px"></td>
      <td>Name:</td>
      <td>
        <input type="text" id="txtSPName" name="SPName" size="51"  class="inputFld" autocomplete="off">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='ShopLoad']/Column[@Name='SPName']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@SPName"/></xsl:attribute>
		    </input>
      </td>
      <td width="10px"></td>
      <td>Contact Type:</td>
      <td>
        <select id="selContactType2" name="SPPersonnelTypeID" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='PersonnelType']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/ShopLoad/@SPPersonnelTypeID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
        
        <!--
        <input type="checkbox" id="cbSPShopManagerFlag" onclick="txtSPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@SPShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>
		    </input>
		    <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@SPShopManagerFlag"/></xsl:attribute>
		    </input>
        -->
      </td>
    </tr>
    <tr>
      <td><span id="spEmailSP" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Email:</td>
      <td>
	      <input id="txtSPEmailAddress" name="SPEmailAddress" size="51" class="inputFld" onbeforedeactivate="checkEMail(this)">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='ShopLoad']/Column[@Name='SPEmailAddress']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@SPEmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSPPreferredContactMethodID" name="SPPreferredContactMethodID" class="inputFld" onchange="selContactMethod_onchange(this)">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/ShopLoad/@SPPreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td><span id="spPhoneSP" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Phone:</td>
      <td nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"><span id="spFaxSP" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Fax: </td>
      <td nowrap="nowrap">
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td><span id="spCellSP" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Cell:</td>
      <td>
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"><span id="spPagerSP" style="visibility:hidden; font-weight:bold;">*</span></td>
      <td>Pager: </td>
      <td>
        <xsl:call-template name="ContactNumbers2">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template>

<xsl:template name="Pricing">
<TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
<TR>
<TD width="10px"></TD>
<TD width="30%" valign="top">
<table unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><b>Hourly Labor Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sheet Metal:</td>
          <td align="right">$
            <input type="text" id="txtSheetMetal" name="HourlyRateSheetMetal" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			         <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Refinishing:</td>
          <td align="right">$
            <input type="text" id="txtRefinish" name="HourlyRateRefinishing" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			        <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Unibody/Frame:</td>
          <td align="right">$
            <input type="text" id="txtUnibodyFrame" name="HourlyRateUnibodyFrame" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Mechanical:</td>
          <td align="right">$
            <input type="text" id="txtMechanical" name="HourlyRateMechanical" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td colspan="2">
      <table unselectable="on" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Refinish Materials Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Single Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly1" name="RefinishSingleStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax1" name="RefinishSingleStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Two Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly2" name="RefinishTwoStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageHourly"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax2" name="RefinishTwoStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax2" name="RefinishTwoStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageCCMaxHrs"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Three Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly3" name="RefinishThreeStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageHourly"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax3" name="RefinishThreeStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax3" name="RefinishThreeStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageCCMaxHrs"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
	  <div style="height:55">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Towing Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Charge Type:</td>
          <td align="right">
            <select id="selTowChargeType" name="TowInChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selTowChgType_onchange()">
              <option value=""></option>
      			  <xsl:for-each select="/Root/Reference[@ListName='TowInChargeType']">
      			    <xsl:call-template name="BuildSelectOptions">
      				  <xsl:with-param name="current" select="/Root/ShopLoad/@TowInChargeTypeCD"/>
      				</xsl:call-template>
      			  </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td><label id="lblFlatFee">Flat Fee:</label></td>
          <td align="right"><label id="lblFFDollar">$</label>
            <input type="text" id="txtFlatFee" name="TowInFlatFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@TowInFlatFee"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
	  </div>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Frame Setup Charges (Hours):</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>No Measurement:</td>
          <td align="right">
            <input type="text" id="txtPullNoMeasure" name="PullSetup" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@PullSetUp"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>With Measurement</td>
          <td align="right">
            <input type="text" id="txtPullMeasure" name="PullSetUpMeasure" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@PullSetUpMeasure"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="30%" valign="top">
<table width="100%" valign="top" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" valign="top" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Air Conditioning Service Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-12 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12Freon" name="R12EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRechargeFreon"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12NoFreon" name="R12EvacuateRecharge" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRecharge"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-134 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134Freon" name="R134EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRechargeFreon"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134NoFreon" name="R134EvacuateRecharge" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRecharge"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Stripe Replacement Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Tape Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtTapePerPanel" name="StripeTapePerPanel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerPanel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtTapePerSide" name="StripeTapePerSide" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerSide"/></xsl:attribute>
			</input>
          </td>
		</tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Paint Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerPanel" name="StripePaintPerPanel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerPanel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerSide" name="StripePaintPerSide" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerSide"/></xsl:attribute>
			</input>
          </td>
		</tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Glass Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Replacement Type:</td>
          <td align="right">
            <select id="selGlassReplChgType" name="GlassReplacementChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selGlassReplChgType_onchange()">
              <option value=""></option>
			        <xsl:for-each select="/Root/Reference[@ListName='GlassReplacementChargeType']">
			          <xsl:call-template name="BuildSelectOptions">
				          <xsl:with-param name="current" select="/Root/ShopLoad/@GlassReplacementChargeTypeCD"/>
				        </xsl:call-template>
			        </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
            <label id="lblWindshield">Windshield Discount:</label>
            <label id="lblSubletFee">Sublet Fee:</label>
          </td>
          <td align="right">
            <input type="text" id="txtWindshieldDiscount" name="DiscountPctWindshield" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctWindshield"/></xsl:attribute>
			</input>
			<label id="lblWindshieldPercent">%</label>
			<label id="lblGlassDollar">$</label>
            <input type="text" id="txtGlassSubletSvcFee" name="GlassSubletServiceFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event); frmShop.GlassSubletServicePct.value = '';" onchange="frmShop.GlassSubletServicePct.value = '';">
			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServiceFee"/></xsl:attribute>
			</input>
			<input type="text" id="txtGlassOutsourceSvcFee" name="GlassOutsourceServiceFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@GlassOutsourceServiceFee"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
	        <label id="lblSideBack">Side/Back Discount:</label>
            <label id="lblSubletPct">Sublet Percent:</label>
          </td>
          <td align="right">
            <input type="text" id="txtSideBackGlassDiscount" name="DiscountPctSideBackGlass" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctSideBackGlass"/></xsl:attribute>
			</input>
			<input type="text" id="txtGlassSubletSvcPct" name="GlassSubletServicePct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1); frmShop.GlassSubletServiceFee.value = '';" onchange="frmShop.GlassSubletServiceFee.value = '';">
			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServicePct"/></xsl:attribute>
			</input>
			<label id="lblGlassPercent">%</label>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="34%" valign="top">
<table border="0" width="235" cellpadding="0" cellspacing="0">
  <tr valign="top" width="100%">
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Recycled Parts:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Markup Percent:</td>
          <td align="right">
            <input type="text" id="txtRecycledPartsMarkup" name="PartsRecycledMarkupPct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@PartsRecycledMarkupPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="6px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Additional Repair Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Daily Storage:</td>
          <td align="right">$
            <input type="text" id="txtDailyStorage" name="DailyStorage" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@DailyStorage"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>2-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign2Wheel" name="AlignmentTwoWheel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentTwoWheel"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>4-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign4Wheel" name="AlignmentFourWheel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentFourWheel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Tire Mount and Balance:</td>
          <td align="right">$
            <input type="text" id="txtTireMountBalance" name="TireMountBalance" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@TireMountBalance"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Flex Additive:</td>
          <td align="right">$
            <input type="text" id="txtFlexAdditive" name="FlexAdditive" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@FlexAdditive"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Green):</td>
          <td align="right">$
            <input type="text" id="txtCoolantGreen" name="CoolantGreen" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoolantGreen"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Red):</td>
          <td align="right">$
            <input type="text" id="txtCoolantRed" name="CoolantRed" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoolantRed"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Undercoat:</td>
          <td align="right">$
            <input type="text" id="txtUndercoat" name="Undercoat" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@Undercoat"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Chip Guard:</td>
          <td align="right">$
            <input type="text" id="txtChipGuard" name="ChipGuard" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@ChipGuard"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Caulking/Seam Sealer:</td>
          <td align="right">$
            <input type="text" id="txtCaulkSeamSeal" name="CaulkingSeamSealer" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CaulkingSeamSealer"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Corrosion Protection:</td>
          <td align="right">$
            <input type="text" id="txtCorrosionProt" name="CorrosionProtection" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CorrosionProtection"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Cover Car:</td>
          <td align="right">$
            <input type="text" id="txtCoverCar" name="CoverCar" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoverCar"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Hazardous Waste:</td>
          <td align="right">$
            <input type="text" id="txtHazardousWaste" name="HazardousWaste" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@HazardousWaste"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Tax Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sales:</td>
          <td align="right">
            <input type="text" id="txtSalesTax" name="SalesTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@SalesTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>County:</td>
          <td align="right">
            <input type="text" id="txtCountyTax" name="CountyTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@CountyTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Municipal:</td>
          <td align="right">
            <input type="text" id="txtMunicipalTax" name="MunicipalTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@MunicipalTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Other:</td>
          <td align="right">
            <input type="text" id="txtOtherTax" name="OtherTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@OtherTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
</TR>
</TABLE>
</xsl:template>

<xsl:template name="OEMDiscounts">
<TABLE>
<TR>
<TD valign="top" width="170">
  <table border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td colspan="3">OEM Replacement Parts Discount:</td>
    </tr>
    <tr>
      <td width="25px"> </td>
      <td width="70px">Domestic:</td>
      <td>
        <input type="text" id="txtDiscountDomestic" name="DiscountDomestic" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
          <xsl:attribute name="value"><xsl:value-of select="@DiscountDomestic"/></xsl:attribute>
       </input>%
      </td>
      </tr>
    <tr>
      <td width="25px"></td>
      <td>Foreign:</td>
      <td>
        <input type="text" id="txtDiscountImport" name="DiscountImport" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
  			  <xsl:attribute name="value"><xsl:value-of select="@DiscountImport"/></xsl:attribute>
  			</input>%
      </td>
      </tr>
    </table>
  </TD>
  <TD width="20"></TD>
  <TD>
    If your shop provides discounts on OEM parts for foreign autos, domestic autos, or any specific makes, please provide that
    information in the box below.  (Input must be limited to 250 characters.)
    <br/>
    <textarea id="txtOEMDiscounts" name="OEMDiscounts" rows="4" cols="100" onpaste="onTextAreaPaste()"><xsl:value-of select="@OEMDiscounts"/></textarea>
  </TD>
  </TR>
  </TABLE>
</xsl:template>


<xsl:template name="Survey">
<TABLE border="0">
  <COLGROUP>
    <COL width="320" valign="top"/>
    <COL width="25"/>
    <COL width="345" valign="top"/>
  </COLGROUP>
  <TR>
    <TD>
      <table border="0" cellpadding="2" cellspacing="0">
		    <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Customer Service Requirements:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbCustomaryHoursFlag" onclick="txtCustomaryHours.value=this.checked==true?1:0;">
              <xsl:if test="@CustomaryHoursFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtCustomaryHours" name="CustomaryHoursFlag">
              <xsl:attribute name="value"><xsl:value-of select="@CustomaryHoursFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Customary business hours, Mon-Fri, 8-5.</td>
        </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbReceptionistFlag" onclick="txtReceptionist.value=this.checked==true?1:0;">
              <xsl:if test="@ReceptionistFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtReceptionist" name="ReceptionistFlag">
              <xsl:attribute name="value"><xsl:value-of select="@ReceptionistFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Employ receptionist/office personnel</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbMinimumWarrantyFlag" onclick="txtMinimumWarranty.value=this.checked==true?1:0;">
              <xsl:if test="@MinimumWarrantyFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtMinimumWarranty" name="MinimumWarrantyFlag">
              <xsl:attribute name="value"><xsl:value-of select="@MinimumWarrantyFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Min 3-yr warranty on repair, workmanship, refinish.</td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="7"/></td>
		    </tr>
        <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Business and Regulatory Requirements:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbBusinessRegsFlag" onclick="txtBusinessRegs.value=this.checked==true?1:0;">
              <xsl:if test="@BusinessRegsFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtBusinessRegs" name="BusinessRegsFlag">
              <xsl:attribute name="value"><xsl:value-of select="@BusinessRegsFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Compliance regarding licenses, permits, regulations</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbHazMatFlag" onclick="txtHazMat.value=this.checked==true?1:0;">
              <xsl:if test="@HazMatFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtHazMat" name="HazMatFlag">
              <xsl:attribute name="value"><xsl:value-of select="@HazMatFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Compliance regarding hazardous materials</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbNoFeloniesFlag" onclick="txtNoFelonies.value=this.checked==true?1:0;">
              <xsl:if test="@NoFeloniesFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtNoFelonies" name="NoFeloniesFlag">
              <xsl:attribute name="value"><xsl:value-of select="@NoFeloniesFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Bus. owner/s no felonies in the past seven years</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbBusinessStabilityFlag" onclick="txtBusinessStability.value=this.checked==true?1:0;">
               <xsl:if test="@BusinessStabilityFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtBusinessStability" name="BusinessStabilityFlag">
              <xsl:attribute name="value"><xsl:value-of select="@BusinessStabilityFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Bus. financial stability</td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="7"/></td>
		    </tr>
        <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Insurance requirements:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbEmployerLiabilityFlag" onclick="txtEmployerLiability.value=this.checked==true?1:0;">
               <xsl:if test="@EmployerLiabilityFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtEmployerLiability" name="EmployerLiabilityFlag">
              <xsl:attribute name="value"><xsl:value-of select="@EmployerLiabilityFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Min. of $100,000 Employer<xsl:text disable-output-escaping="yes">'</xsl:text>s Liability Ins.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbCommercialLiabilityFlag" onclick="txtCommercialLiability.value=this.checked==true?1:0;">
               <xsl:if test="@CommercialLiabilityFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtCommercialLiability" name="CommercialLiabilityFlag">
              <xsl:attribute name="value"><xsl:value-of select="@CommercialLiabilityFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Min. of $1,000,000 Commercial General Liability Ins.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbWorkersCompFlag" onclick="txtWorkersComp.value=this.checked==true?1:0;">
               <xsl:if test="@WorkersCompFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtWorkersComp" name="WorkersCompFlag">
              <xsl:attribute name="value"><xsl:value-of select="@WorkersCompFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Compliance regarding Workers Comp.</td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="7"/></td>
		    </tr>
        <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Minimum Repair Equipment:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbFrameDiagnosticEquipFlag" onclick="txtFrameDiagnosticEquip.value=this.checked==true?1:0;">
               <xsl:if test="@FrameDiagnosticEquipFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtFrameDiagnosticEquip" name="FrameDiagnosticEquipFlag">
              <xsl:attribute name="value"><xsl:value-of select="@FrameDiagnosticEquipFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Basic Unibody and Frame diagnostic equip.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbFrameAnchoringPullingEquipFlag" onclick="txtFrameAnchoringPullingEquip.value=this.checked==true?1:0;">
               <xsl:if test="@FrameAnchoringPullingEquipFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtFrameAnchoringPullingEquip" name="FrameAnchoringPullingEquipFlag">
              <xsl:attribute name="value"><xsl:value-of select="@FrameAnchoringPullingEquipFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Unibody and Frame 4-point anchoring and pulling equip.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbMIGWeldersFlag" onclick="txtMIGWelders.value=this.checked==true?1:0;">
               <xsl:if test="@MIGWeldersFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtMIGWelders" name="MIGWeldersFlag">
              <xsl:attribute name="value"><xsl:value-of select="@MIGWeldersFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">MIG welders <xsl:text disable-output-escaping="yes">-</xsl:text> 110 and 220 capabilities.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbRefinishCapabilityFlag" onclick="txtRefinishCapability.value=this.checked==true?1:0;">
               <xsl:if test="@RefinishCapabilityFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtRefinishCapability" name="RefinishCapabilityFlag">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishCapabilityFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Refinish capability for reproducing OEM type finish.<br/></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbMechanicalRepairFlag" onclick="txtMechanicalRepair.value=this.checked==true?1:0;">
               <xsl:if test="@MechanicalRepairFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtMechanicalRepair" name="MechanicalRepairFlag">
              <xsl:attribute name="value"><xsl:value-of select="@MechanicalRepairFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Provide mechanical repair services (in-house or sublet):<br/></td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="7"/></td>
		    </tr>
      </table>
    </TD>  
    
    <TD></TD><!-- spacer column -->
      
    <TD>
      <table>  
        <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Employee Training:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbCompetentEstimatorsFlag" onclick="txtCompetentEstimators.value=this.checked==true?1:0;">
               <xsl:if test="@CompetentEstimatorsFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtCompetentEstimators" name="CompetentEstimatorsFlag">
              <xsl:attribute name="value"><xsl:value-of select="@CompetentEstimatorsFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Estimators trainied, knowledgeable, <br/>proficient with estimating software of choice.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbEmployeeEducationFlag" onclick="txtEmployeeEducation.value=this.checked==true?1:0;">
               <xsl:if test="@EmployeeEducationFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtEmployeeEducation" name="EmployeeEducationFlag">
             <xsl:attribute name="value"><xsl:value-of select="@EmployeeEducationFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Education opportunities for their employees<br/>i.e. I-CAR training, ASE certification, equivalent training.</td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="18"/></td>
		    </tr>
		    <tr>
			    <td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">Electronic Estimate and Communication Capability:</p></td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbElectronicEstimatesFlag" onclick="txtElectronicEstimates.value=this.checked==true?1:0;">
               <xsl:if test="@ElectronicEstimatesFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtElectronicEstimates" name="ElectronicEstimatesFlag">
              <xsl:attribute name="value"><xsl:value-of select="@ElectronicEstimatesFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Electronic estimate w/ P-Page logic capability.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbDigitalPhotosFlag" onclick="txtDigitalPhotos.value=this.checked==true?1:0;">
               <xsl:if test="@DigitalPhotosFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtDigitalPhotos" name="DigitalPhotosFlag">
              <xsl:attribute name="value"><xsl:value-of select="@DigitalPhotosFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Digital photographs capability.</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbVANInetFlag" onclick="txtVANInet.value=this.checked==true?1:0;">
               <xsl:if test="@VANInetFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtVANInet" name="VANInetFlag">
              <xsl:attribute name="value"><xsl:value-of select="@VANInetFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Electronic connectivity (VAN or Internet connection).</td>
		    </tr>
		    <tr valign="top">
			    <td width="40" align="center">
            <input type="checkbox" id="cbEFTFlag" onclick="txtEFT.value=this.checked==true?1:0;">
               <xsl:if test="@EFTFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>
            <input type="hidden" id="txtEFT" name="EFTFlag">
              <xsl:attribute name="value"><xsl:value-of select="@EFTFlag"/></xsl:attribute>
            </input>
          </td>
			    <td class="login">Agree to accept EFT payments</td>
		    </tr>
		    <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="8"/></td>
		    </tr>
	    </table>

	    <table border="0" cellpadding="2" cellspacing="0">
		    <tr>
			    <td colspan="2"><p class="login" style="font-size:11pt; font-weight:bold;">Additional Information</p></td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdSecureStorageYes" name="rdSecureStorage" onclick="txtSecureStorage.value=1">
               <xsl:if test="@SecureStorageFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdSecureStorageNo" name="rdSecureStorage" onclick="txtSecureStorage.value=0">
               <xsl:if test="@SecureStorageFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtSecureStorage" name="SecureStorageFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@SecureStorageFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Secure area for vehicles awaiting/under repair.</td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdValidEPALicenseYes" name="rdValidEPALicense" onclick="txtValidEPALicense.value=1">
               <xsl:if test="@ValidEPALicenseFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdValidEPALicenseNo" name="rdValidEPALicense" onclick="txtValidEPALicense.value=0">
               <xsl:if test="@ValidEPALicenseFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtValidEPALicense" name="ValidEPALicenseFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@ValidEPALicenseFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Shop has a valid EPA License.</td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdHydraulicLiftYes" name="rdHydraulicLift" onclick="txtHydraulicLift.value=1">
               <xsl:if test="@HydraulicLiftFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdHydraulicLiftNo" name="rdHydraulicLift" onclick="txtHydraulicLift.value=0">
               <xsl:if test="@HydraulicLiftFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtHydraulicLift" name="HydraulicLiftFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@HydraulicLiftFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Hydraulic lift or ability to raise a vehicle</td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdICARYes" name="rdICAR" onclick="txtICAR.value=1">
               <xsl:if test="@ICARFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdICARNo" name="rdICAR" onclick="txtICAR.value=0">
              <xsl:if test="@ICARFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtICAR" name="ICARFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@ICARFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Min. 1 technician and 1 estimator completed<br/>I-CAR Collision Repair 2000 Course (8 parts).</td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdRefinishTrainedYes" name="rdRefinishTrained" onclick="txtRefinishTrained.value=1">
              <xsl:if test="@RefinishTrainedFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdRefinishTrainedNo" name="rdRefinishTrained" onclick="txtRefinishTrained.value=0">
              <xsl:if test="@RefinishTrainedFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtRefinishTrained" name="RefinishTrainedFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@RefinishTrainedFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Min. 1 refinish tech completed minimum<br/>training requirements for refinishing.</td>
		    </tr>
		    <tr valign="top">
			    <td class="login" nowrap="nowrap">
            <input type="radio" id="rdASEYes" name="rdASE" onclick="txtASE.value=1">
              <xsl:if test="@ASEFlag=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>Yes
            <input type="radio" id="rdASENo" name="rdASE" onclick="txtASE.value=0">
              <xsl:if test="@ASEFlag=0"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
            </input>No
            <input type="hidden" id="txtASE" name="ASEFlag">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@ASEFlag)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Min. 1 technician is ASE certified</td>
		    </tr>
        <tr>
			    <td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="8"/></td>
		    </tr>
		    <tr valign="top">
			    <td class="login" align="center">
            <input type="text" id="txtAge" name="Age" size="5" maxlength="5">
              <xsl:attribute name="value"><xsl:value-of select="normalize-space(@Age)"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
			    <td class="login">Number of years shop has been in business.</td>
		    </tr>
	    </table>
    </TD>
  </TR>
</TABLE>
</xsl:template>


<xsl:template name="ContactNumbers2">
  <xsl:param name="ContactMethod"/>
  <xsl:param name="EntityAbbrev"/>

  <input maxlength="3" size="2" onbeforedeactivate="checkAreaCode(this);" class="inputFld" wizard="yes">
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber,'A');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
      <xsl:when test="$EntityAbbrev='SP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SPPhoneAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SPFaxAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SPCellAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SPPagerAreaCode)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:when test="$EntityAbbrev='SLP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SLPPhoneAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SLPFaxAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SLPCellAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SLPPagerAreaCode)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@PhoneAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@FaxAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@CellAreaCode)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@PagerAreaCode)"/></xsl:when>
    	  </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:attribute>
  </input> -

  <input maxlength="3" size="2" onbeforedeactivate="checkPhoneExchange(this);" class="inputFld" wizard="yes">
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber,'E');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
      <xsl:when test="$EntityAbbrev='SP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SPPhoneExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SPFaxExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SPCellExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SPPagerExchangeNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:when test="$EntityAbbrev='SLP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SLPPhoneExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SLPFaxExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SLPCellExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SLPPagerExchangeNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@PhoneExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@FaxExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@CellExchangeNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@PagerExchangeNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:attribute>
  </input> -

  <input maxlength="4" size="3" onbeforedeactivate="checkPhoneNumber(this);" class="inputFld" wizard="yes">
    <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber,'U');</xsl:attribute>
	<xsl:attribute name="value">
    <xsl:choose>
      <xsl:when test="$EntityAbbrev='SP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SPPhoneUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SPFaxUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SPCellUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SPPagerUnitNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:when test="$EntityAbbrev='SLP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SLPPhoneUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SLPFaxUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SLPCellUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SLPPagerUnitNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
    	  <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@PhoneUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@FaxUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@CellUnitNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@PagerUnitNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:attribute>
  </input>

  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

  <input maxlength="5" size="5" onkeypress="NumbersOnly(window.event)" class="inputFld" wizard="yes">
    <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>
	<xsl:attribute name="value">
    <xsl:choose>
      <xsl:when test="$EntityAbbrev='SP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SPPhoneExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SPFaxExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SPCellExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SPPagerExtensionNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:when test="$EntityAbbrev='SLP'">
        <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@SLPPhoneExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@SLPFaxExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@SLPCellExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@SLPPagerExtensionNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
    	  <xsl:choose>
    	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="normalize-space(@PhoneExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="normalize-space(@FaxExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="normalize-space(@CellExtensionNumber)"/></xsl:when>
      		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="normalize-space(@PagerExtensionNumber)"/></xsl:when>
    	  </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:attribute>
  </input>
</xsl:template>
   
  
</xsl:stylesheet>
