<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="Claim">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function AdjustCRUD( strCrud)
     {
      var strRet = "_" + strCrud.substr(1);
       return strRet;
     }
  ]]>
</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ClaimCRUD" select="Claim"/>
<xsl:param name="CallerCRUD" select="Caller"/>
<xsl:param name="ContactCRUD" select="Claim Contact"/>
<xsl:param name="CarrierCRUD" select="Data:Carrier"/>
<xsl:param name="InsuredCRUD" select="Insured"/>
<xsl:param name="DocumentCRUD" select="Document"/>
<xsl:param name="WitnessCRUD" select="Witness"/>
<xsl:param name="PedestrianCRUD" select="Pedestrian"/>
<xsl:param name="CoverageCRUD" select="Coverage"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="ImageRootDir"/>
<xsl:param name="IMPSEmailBCC"/>

<xsl:template match="/Root">

<xsl:variable name="InsCo" select="session:XslGetSession('InsuranceCompanyName')"/>
<xsl:variable name="ClaimID" select="session:XslGetSession('ClientClaimNumber')"/>
<xsl:variable name="ClaimRestrictedFlag" select="session:XslGetSession('RestrictedFlag')"/>
<xsl:variable name="ClaimOpen" select="session:XslGetSession('ClaimOpen')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="CSRNameFirst" select="session:XslGetSession('OwnerUserNameFirst')"/>
<xsl:variable name="CSRNameLast" select="session:XslGetSession('OwnerUserNameLast')"/>
<xsl:variable name="InsuredBusinessName" select="session:XslGetSession('InsuredBusinessName')"/>
<xsl:variable name="DemoFlag" select="session:XslGetSession('DemoFlag')"/>
<xsl:variable name="CoverageLimitWarningInd" select="session:XslGetSession('CoverageLimitWarningInd')"/>
<xsl:variable name="Claim_ClaimAspectID" select="session:XslGetSession('Claim_ClaimAspectID')"/>

<xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>

<xsl:variable name="AdjustedDocumentPermissions" select="user:AdjustCRUD(string($DocumentCRUD))"/>

<xsl:variable name="LynxID" select="/Root/@LynxID" />
<xsl:variable name="InsuredFirst" select="/Root/Claim/Insured/@NameFirst"/>
<xsl:variable name="InsuredLast" select="/Root/Claim/Insured/@NameLast"/>
<xsl:variable name="LossDate" select="/Root/Claim/@LossDate"/>
<xsl:variable name="ClaimLastUpdatedDate" select="/Root/Claim/@SysLastUpdatedDate"/>
<!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->
<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Closed'))"/>

<!-- <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'"> -->
<xsl:if test="$ClaimStatus='Claim Closed'">
  <xsl:value-of select="js:setPageDisabled()"/>
</xsl:if>
<xsl:value-of select="js:SetCRUD( string($ClaimCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL    PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{6D65EFC5-52B6-4302-84C4-080E14C8900B},19,uspClaimGetDetailXML,Claim.xsl, 147468, null  -->

<HEAD>
<TITLE>Claim Info</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/showImage.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,1);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

	var gsClaimCRUD = '<xsl:value-of select="$ClaimCRUD"/>';
	var gsCallerCRUD = '<xsl:value-of select="$CallerCRUD"/>';
	var gsContactCRUD = '<xsl:value-of select="$ContactCRUD"/>';
	var gsCarrierCRUD = '<xsl:value-of select="$CarrierCRUD"/>';
	var gsInsuredCRUD = '<xsl:value-of select="$InsuredCRUD"/>';
	var gsDocumentCRUD = '<xsl:value-of select="$DocumentCRUD"/>';
	var gsWitnessCRUD = '<xsl:value-of select="$WitnessCRUD"/>';
//	var gsPedestrianCRUD = '<xsl:value-of select="$PedestrianCRUD"/>';
	var gsCoverageCRUD = '<xsl:value-of select="$CoverageCRUD"/>';

  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
  var gsLynxID = "<xsl:value-of select="$LynxID"/>";
  var gsClaimID = "<xsl:value-of select="js:cleanString(string($ClaimID))"/>";
  var gsIFname = "<xsl:value-of select="js:cleanString(string($InsuredFirst))"/>";
  var gsILname = "<xsl:value-of select="js:cleanString(string($InsuredLast))"/>";
	var gsIBusiness = "<xsl:value-of select="js:cleanString(string($InsuredBusinessName))"/>";
  var gsLossDate = "<xsl:value-of select="js:UTCConvertDate(string($LossDate))"/>";
  var gsInsco = "<xsl:value-of select="js:cleanString(string($InsCo))"/>";
  var gsClaimLastUpdatedDate = "<xsl:value-of select="$ClaimLastUpdatedDate"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsClaimRestrictedFlag = "<xsl:value-of select="$ClaimRestrictedFlag"/>";
  var gsClaimOpen = "<xsl:value-of select="$ClaimOpen"/>";
	var gsImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
  var gsCSRNameFirst = '<xsl:value-of select="js:cleanString(string($CSRNameFirst))"/>';
  var gsCSRNameLast = '<xsl:value-of select="js:cleanString(string($CSRNameLast))"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var gsInsuranceCompanyID = "<xsl:value-of select="Claim/@InsuranceCompanyID"/>";
  var bPageReadOnly = <xsl:value-of select="$pageReadOnly"/>
  var gsDemoFlag = "<xsl:value-of select="$DemoFlag"/>";
  var gsIMPSEmailBCC = "<xsl:value-of select="$IMPSEmailBCC"/>";
  var gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";
  var gsClaim_ClaimAspectID = "<xsl:value-of select="$Claim_ClaimAspectID"/>";
  
  var gsVehicleList = new Array(<xsl:for-each select="/Root/Vehicle">
                        <xsl:variable name="VehOwner">
                          <xsl:choose>
                            <xsl:when test="@BusinessName != ''"><xsl:value-of select="@BusinessName"/></xsl:when>
                            <xsl:when test="@NameFirst != ''"><xsl:value-of select="concat(substring(@NameFirst, 1, 1), '. ', @NameLast)"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="@NameLast"/></xsl:otherwise>
                          </xsl:choose>
                        </xsl:variable>
                        "<xsl:value-of select="@VehicleNumber"/>|<xsl:value-of select="js:escape_string(concat(@VehicleNumber, ': ' , string($VehOwner)))"/> (<xsl:value-of select="js:escape_string(concat(@VehicleYear, ' ', @Make, ' ', @Model))"/>)"<xsl:if test="position() != last()">, </xsl:if>
                      </xsl:for-each>
                     );

  var iEmailVehNum = "";

  if (parent.gsCoverageLimitWarningInd)
    parent.gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

<![CDATA[

  // Tooltip Variables to set:
  messages= new Array()
  // Write your descriptions in here.
  messages[0]="Double Click to Expand"
  messages[1]="Double Click to Close"
  messages[2]="This will expand for Loss Type selection."

  // button images for ADD/DELETE/SAVE
  preload('buttonAdd','/images/but_ADD_norm.png')
  preload('buttonAddDown','/images/but_ADD_down.png')
  preload('buttonAddOver','/images/but_ADD_over.png')
  preload('buttonDel','/images/but_DEL_norm.png')
  preload('buttonDelDown','/images/but_DEL_down.png')
  preload('buttonDelOver','/images/but_DEL_over.png')
  preload('buttonSave','/images/but_SAVE_norm.png')
  preload('buttonSaveDown','/images/but_SAVE_down.png')
  preload('buttonSaveOver','/images/but_SAVE_over.png')

  // Select changes to update dirty flag and changed border
  function onSelectChange(selobj)
  {
    gbDirtyFlag = true;
    // check to make sure if in a collection
    if (selobj[0]) selobj = selobj[0];
    selobj.children[0].style.borderColor='#805050';
  }

  // Witness selection box change
  function onWitnessChange(selobj)
  {
		//check if in insert mode and if so ignore change
    if (gbInsertMode == true)
		{
			setTimeout(selFixUpW,200);
		 	return;
		}

    if (selobj == null)
      selobj = document.getElementById("selWitness");

    var involvedID = selobj.options[selobj.selectedIndex].value;
    if (involvedID > 0)
      document.frames["ifrmWitness"].frameElement.src = "ClaimWitness.asp?WindowID=" + gsWindowID + "&InvolvedID="+involvedID;
    else
      document.frames["ifrmWitness"].frameElement.src = "blank.asp";
  }

	function selFixUpW()
	{
		selobj = document.getElementById("selWitness");
		selobj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";
	}

	//Notification from APDSelect.asp that the main tabs have changed
	function ParentMainTabChange()
	{
		MainTabChange();
	}

  // Page Initialize
  function PageInit()
  {

    top.SetMainTab(1);
    // the following two lines of code makes sure that the tab11 is currently under focus.
    tabsIntabs[0].tabArray[1].depressTab();
    tabsIntabs[0].tabArray[0].depressTab();

    tabsIntabs[0].tabs.onchange=tabChange;
    tabsIntabs[1].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
    tabsIntabs[1].tabs.onBeforechange=tabBeforeChange;

    top.sClaim_ClaimAspectID = gsClaim_ClaimAspectID;
    
    //this is to trap error messages when running stand-alone
    try
    {
      onWitnessChange();
    }catch(e) {}
    // set the info in the main folder
    top.setClaimHeader(gsCSRNameFirst, gsCSRNameLast, gsLynxID, gsClaimID, gsIFname, gsILname, gsLossDate, gsInsco, gsClaimRestrictedFlag, gsClaimOpen, gsClaimStatus, gsIBusiness, gsDemoFlag, gsInsuranceCompanyID);

    onpasteAttachEvents();
    attachPasteEvents(document.all.txtCompDeductAmt);
    attachPasteEvents(document.all.txtCollDeductAmt);
    attachPasteEvents(document.all.txtPropDeductAmt);
    attachPasteEvents(document.all.txtUIMDeductAmt);
    attachPasteEvents(document.all.txtRentalDays);
    attachPasteEvents(document.all.txtRentalDailyLimitAmt);
    attachPasteEvents(document.all.txtRentalLimitAmt);
    try {
      if (ctrDatetxtDateLoss)
        ctrDatetxtDateLoss.futureDate = false;
    } catch(e) {}
    
    if (gsDocumentCRUD.indexOf("C") == -1)
      top.DocumentsMenu(false, 1);

    top.setSB(100,top.sb);
  }

  // image popup code
  function ShowImage(strPath)
  {
    ShowImagePopup( gsImageRootDir + strPath );
  }

  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  var objDivWithButtons = null;
  // ADS Button pressed
  function ADS_Pressed(sAction, sName)
  {
    if (ValidatePhones(sName) == false) return;

    //validate the date of loss
    if (gsClaimCRUD.indexOf("U") != -1 && gsClaimCRUD.indexOf("R") != -1) {
        var sDateLoss = txtDateLoss.value;
        if (sDateLoss != "") {
            var dDateLoss = new Date(sDateLoss);
            var dToday = new Date();
            if (dDateLoss > dToday) {
                ClientWarning("Date of Loss cannot be in the future. Please try again");
                try{
                    ctrDatetxtDateLoss.select();
                    ctrDatetxtDateLoss.focus();
                }
                catch (e) {
                    //bring the claim tab up.
                    tabsIntabs[0].tabArray[0].depressTab();
                    ctrDatetxtDateLoss.select();
                    ctrDatetxtDateLoss.focus();
                }
                return;
            }
        }
    }

    if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
    if (inADS_Pressed == true) return;
    inADS_Pressed = true;
    objDivWithButtons = divWithButtons;
    disableADS(objDivWithButtons, true);

    var oDiv = document.getElementById(sName)
    if (sAction == "Update")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;
        if (gbInsertMode == true) sAction = "Insert";

        var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
        if (retArray[1] != "0")
        {
          //alert("Save Completed!");
          if (gbInsertMode == true)
          {
	          gbInsertMode = false;
            UpdateSelects(sType, retArray[0]);
            oDiv.Count++;
            UpdateTabTextCount(oDiv);
          }

          selObj = document.getElementById(oDiv.id + "Sel");
          selObj.style.borderColor = "#cccccc";
          gbInsertMode = false;
          if (oDiv.getAttribute("CRUD").indexOf("C") != -1)
            eval(sName+"_ADD.style.visibility='inherit'");
          if (oDiv.getAttribute("CRUD").indexOf("D") != -1)
            eval(sName+"_DEL.style.visibility='inherit'");
        }
      }
      else
      {
        var sRequest = GetRequestString(sName);
        sRequest += "ClaimSysLastUpdatedDate=" + gsClaimLastUpdatedDate + "&";
        sRequest += "LynxID=" + gsLynxID + "&";
        sRequest += "UserID=" + gsUserID + "&";

        var retArray = DoCrudAction(oDiv, sAction, sRequest);
        if (retArray[1] != "0")
        {
          //alert(sAction+" Completed! " + retArray[0]);

          //update the last Updated date...
          var objXML = new ActiveXObject("Microsoft.XMLDOM");

          objXML.loadXML(retArray[0]);
          var rootNode = objXML.documentElement.selectSingleNode("/Root");

          var ClaimNode = rootNode.selectSingleNode("Claim/@SysLastUpdatedDate");
          if (ClaimNode)
          {
            gsClaimLastUpdatedDate = ClaimNode.nodeValue;
            oDiv.LastUpdatedDate = gsClaimLastUpdatedDate;
            
            //keep Claim and Loss tabs in sync
            var oLoss = document.getElementById("content21");
            if (oLoss) oLoss.LastUpdatedDate = gsClaimLastUpdatedDate;
            var oClaim = document.getElementById("content11");
            if (oClaim) oClaim.LastUpdatedDate = gsClaimLastUpdatedDate;
          }

          var contextDate = rootNode.selectSingleNode(oDiv.name+"/@SysLastUpdatedDate")
          if (contextDate)
            oDiv.LastUpdatedDate = contextDate.nodeValue;

          var contextID = rootNode.selectSingleNode(oDiv.name+"/@InvolvedID")
          if (contextID)
            oDiv.ContextID = contextID.nodeValue;
        }
      }
    }
    else if (sAction == "Insert")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;

        var selObj = document.getElementById("sel"+sType)
        selObj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";

        document.frames["ifrm"+sType].frameElement.src = "Claim"+sType+".asp?WindowID=" + gsWindowID + "&InvolvedID=0";

        selObj = document.getElementById(oDiv.id + "Sel");
        selObj.style.borderColor = "#805050";

        eval(sName+"_ADD.style.visibility='hidden'");
        eval(sName+"_DEL.style.visibility='hidden'");

        if (oDiv.getAttribute("CRUD").indexOf("U") != -1)
          eval(sName+"_SAV.style.visibility='inherit'");

        gbInsertMode = true;
        gbDirtyFlag = true;
      }
    }
    else if (sAction == "Delete")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;

        var objSel = document.getElementById("sel"+sType);
        var sInvolvedName = objSel.firstChild.innerText;
        var sInvolved = sInvolvedName.substr(0, sInvolvedName.length-1);

        var sInvolvedConfirm = YesNoMessage("Are you sure that you want to delete " +sType+ " " +sInvolved+ "?",
                                            "Press YES to delete the selected " +sType+ ", or NO to cancel.");
        if (sInvolvedConfirm == "Yes")
        {
          var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
          if (retArray[1] != "0")
          {
            UpdateSelects(sType,retArray[0]);
            if (parseInt(oDiv.Count) > 0) oDiv.Count--;
            UpdateTabTextCount(oDiv);
						if (oDiv.Count == 0)
						{
				      eval(sName+"_SAV.style.visibility='hidden'");
				      eval(sName+"_DEL.style.visibility='hidden'");
						}
          }
        }
      }
    }
    inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;
  }

  // Re-load the Witness and Pedestrian select boxes from DXML
  function UpdateSelects(sType, sXML)
  {
    var objXML = new ActiveXObject("Microsoft.XMLDOM");
    objXML.loadXML(sXML);
    var objNodeList = objXML.documentElement.selectNodes("/Root/"+sType);
    var objSel = document.getElementById("sel"+sType);
    var selIndex = objNodeList.length == 0 ? 0 : objNodeList.length - 1;
    var strOptions = "";
    var nLength = objNodeList.length;
    for (var n = 0; n < nLength; n++)
    {
      var objNode = objNodeList.nextNode();
      var strSel = ( n == selIndex ) ? " selected" : "";
      strOptions += "<div class='option' value='"
        + objNode.selectSingleNode( "@InvolvedID" ).text + "'" + strSel + " nowrap>"
        + objNode.selectSingleNode( "@NameFirst" ).text + " "
        + objNode.selectSingleNode( "@NameLast" ).text + "</div>";
    }
    if (nLength == 0) strOptions = "<div class='option' value='' selected nowrap></div>";
    objSel.all.selectOptionDiv.innerHTML = strOptions;
    objSel.optionselect( null, selIndex );
  }

  // show the loss type window
  function ShowLossType(sVis)
  {
    if (bPageReadOnly == true) return;
    var sLossTypeText = document.getElementById('txtLossTypeText').value;
    var strRet = window.showModalDialog("claimLossType.asp?WindowID=" + gsWindowID + "&LynxID=" + gsLynxID + "&sel=" + sLossTypeText,"",
                                        "dialogHeight:250px;dialogWidth:400px;center:1;help:0;resizable:0;scroll:0;status:0");
    if (strRet == undefined) return;  //if the dialog is closed by the browser close button [X}, rather than the app close button
    var arystrRet = strRet.split("||");
    if (arystrRet[0] != "")
    {
      if (arystrRet[0] != document.getElementById('txtLossTypeText').value)
      {
        gbDirty = true;
        document.getElementById('txtLossTypeText').value = arystrRet[0];
        setControlDirty(document.getElementById('txtLossTypeText'));
      }
      LossTypeID.value = arystrRet[1];
    }
  }
  
  // show the claim carrier rep window
  function ShowClaimCarrierRep()
  {
    if (bPageReadOnly == true) return;
    var sURL = "ClaimCarrierRep.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + "&SysLastUpdatedDate=" + gsClaimLastUpdatedDate + 
                "&CarrierRepUserID=" + document.getElementById('txtCarrierRepUserID').value;

    var strRet = window.showModalDialog(sURL, "", "dialogHeight:170px;dialogWidth:405px;center:1;help:0;resizable:0;scroll:0;status:0");
                                        
    if (strRet == undefined) return;  //if the dialog is closed by the browser close button [X}, rather than the app close button
    var arystrRet = strRet.split("||");
    if (arystrRet[0] != ""){
      var sCarrierOffice = arystrRet[0];
      if (arystrRet[0] != document.getElementById('txtCarrierOffice').value)
        document.getElementById('txtCarrierOffice').value = arystrRet[0];
      gsClaimLastUpdatedDate = arystrRet[2];  
      
      // keep claim and loss tabs in sync.
      var oLoss = document.getElementById("content21");
      if (oLoss) oLoss.LastUpdatedDate = gsClaimLastUpdatedDate;
      var oClaim = document.getElementById("content11");
      if (oClaim) oClaim.LastUpdatedDate = gsClaimLastUpdatedDate;
      
      var co = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspSessionGetUserDetailXML", "UserID=" + arystrRet[1] + "&ApplicationCD=ALL");
    	if (co.status == 0){
        var sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
          ServerEvent();
        else{
          var objXML = new ActiveXObject("Microsoft.XMLDOM");

          objXML.loadXML(listArray[0]);
          var UserNode = objXML.documentElement.selectSingleNode("//Root/User");
          
          document.getElementById('txtCarrierRepUserID').value = UserNode.getAttribute("UserID");
          document.getElementById('txtCarrierRepNameTitle').value = UserNode.getAttribute("NameTitle");
          document.getElementById('txtCarrierRepNameFirst').value = UserNode.getAttribute("NameFirst");
          document.getElementById('txtCarrierRepNameLast').value = UserNode.getAttribute("NameLast");
          document.getElementById('txtCarrierRepEmail').value = UserNode.getAttribute("EmailAddress");
          document.getElementById('txtCarrierRepPhoneAreaCode').value = UserNode.getAttribute("PhoneAreaCode");
          document.getElementById('txtCarrierRepPhoneExchangeNumber').value = UserNode.getAttribute("PhoneExchangeNumber");
          document.getElementById('txtCarrierRepPhoneUnitNumber').value = UserNode.getAttribute("PhoneUnitNumber");
          document.getElementById('txtCarrierRepPhoneExtensionNumber').value = UserNode.getAttribute("PhoneExtensionNumber");
          document.getElementById('txtCarrierRepFaxUnitNumber').value = UserNode.getAttribute("FaxAreaCode");
          document.getElementById('txtCarrierRepFaxExchangeNumber').value = UserNode.getAttribute("FaxExchangeNumber");
          document.getElementById('txtCarrierRepFaxUnitNumber').value = UserNode.getAttribute("FaxUnitNumber");
          document.getElementById('txtCarrierRepFaxExtensionNumber').value = UserNode.getAttribute("FaxExtensionNumber");
        }
      }
      else ServerEvent();
    }
  }

  function tabBeforeChange(obj, tab)
  {
    //var relatedTab = tabsIntabs[tab.tabindex-1].activeTab;
    var relatedTab = obj
		// if still on the same tab just ignore
		if (obj == tab) return;
    return ValidatePhones(relatedTab.content.id);
  }

    function ValidatePhones(sName) {
        switch(sName) {
            case "content11": //Claim Info
                if (gsClaimCRUD.indexOf("U") != -1 && gsClaimCRUD.indexOf("R") != -1) {
                    var sDateLoss = txtDateLoss.value;
                    if (sDateLoss != "") {
                        var dDateLoss = new Date(sDateLoss);
                        var dToday = new Date();
                        if (dDateLoss > dToday) {
                            ClientWarning("Date of Loss cannot be in the future. Please try again");
                            ctrDatetxtDateLoss.select();
                            ctrDatetxtDateLoss.focus();
                            return false;
                        }
                    }
                }
                break;
            case "content12": //Caller Info
                if (validatePhoneSections(txtCallerDayAreaCode, txtCallerExchangeNumber, txtCallerDayUnitNumber) == false) return false;
                if (validatePhoneSections(txtCallerNightAreaCode, txtCallerNightExchangeNumber, txtCallerNightUnitNumber) == false) return false;
                if (validatePhoneSections(txtCallerAlternateAreaCode, txtCallerAlternateExchangeNumber, txtCallerAlternateUnitNumber) == false) return false;
                break;
            case "content13": //Insured Info
                if (validatePhoneSections(txtInsuredDayAreaCode, txtInsuredDayExchangeNumber, txtInsuredDayUnitNumber) == false) return false;
                if (validatePhoneSections(txtInsuredNightAreaCode, txtInsuredNightExchangeNumber, txtInsuredNightUnitNumber) == false) return false;
                if (validatePhoneSections(txtInsuredAlternateAreaCode, txtInsuredAlternateExchangeNumber, txtInsuredAlternateUnitNumber) == false) return false;
                break;
            case "content14": //Contact Info
                if (validatePhoneSections(txtAgentAreaCode, txtAgentExchangeNumber, txtAgentUnitNumber) == false) return false;
                if (validatePhoneSections(txtContactDayAreaCode, txtContactDayExchangeNumber, txtContactDayUnitNumber) == false) return false;
                if (validatePhoneSections(txtContactNightAreaCode, txtContactNightExchangeNumber, txtContactNightUnitNumber) == false) return false;
                if (validatePhoneSections(txtContactAlternateAreaCode, txtContactAlternateExchangeNumber, txtContactAlternateUnitNumber) == false) return false;
                break;
            case "content15": //Carrier Info
                if (validatePhoneSections(txtCarrierRepPhoneAreaCode, txtCarrierRepPhoneExchangeNumber, txtCarrierRepPhoneUnitNumber) == false) return false;
                if (validatePhoneSections(txtCarrierRepFaxAreaCode, txtCarrierRepFaxExchangeNumber, txtCarrierRepFaxUnitNumber) == false) return false;
                break;
            case "content16": //Witness Info
                var oDiv = document.getElementById("content16")
                var sType = oDiv.name;
                try {
                    return document.frames["ifrm"+sType].ValidatePhones();
                }
                catch (e) {return true;}
                break;
        }
        return true;
    }

	function emailCarrier()
	{
    if (iEmailVehNum == "") {
      if (gsVehicleList.length > 1) {
        showEntityPicker(0, 0, aEmail);
        return;
      } else {
        iEmailVehNum = gsVehicleList[0].split("|")[0];
      }
    }
    var _email = document.getElementById("txtCarrierRepEmail").value;
    var _subj = "Claim #: " + gsClaimID + " / Loss Date: " + gsLossDate.replace(/\//g, "-") + " / LYNX ID: " + gsLynxID + "-" + iEmailVehNum;
    var _body = "";
    document.location = "mailto:"+_email+"?bcc=" + gsIMPSEmailBCC + "&subject="+_subj+"&body="+_body;
  }

  function showEntityPicker(x, y, obj){
    var contextHtml="";
    contextHtml = '<div style="height:100%; width:100%;overflow:auto;border:1pt solid #666666">';
    contextHtml+='<TABLE unselectable="on" STYLE="BGCOLOR="#F9F8F7" WIDTH="100%" HEIGHT1="100%" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;height:18px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    var iVehNum, sVehInfo;
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7;text-align:center;font-weight:bold;background-color:#C0C0C0;font-color:#FFFFFF" >Pick an Entity</TD></TR>';
    for (var i = 0; i < gsVehicleList.length; i++) {
      iVehNum = gsVehicleList[i].split("|")[0];
      sVehInfo = unescape(gsVehicleList[i].split("|")[1]);
      contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i' + i + '" ONMOUSEOVER="document.all.i' + i + '.style.background=\'#B6BDD2\';document.all.i' + i + '.style.border=\'1pt solid #0A246A\';document.all.i' + i + '.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i' + i + '.style.background=\'#F9F8F7\';document.all.i' + i + '.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.setEmailVehNum(' + iVehNum + ');">&nbsp;' + sVehInfo + '</TD></TR>';
    }
    contextHtml+='</TABLE></div>';
    
    var iHeight = ((gsVehicleList.length < 4 ? gsVehicleList.length : 4) + 1) * 24 ;
    var oPopupBody = oPopup.document.body;
    oPopupBody.innerHTML = contextHtml;
    oPopup.show(0, 18, 250, iHeight, obj);
  }
  
  function setEmailVehNum(iVehNum){
    iEmailVehNum = iVehNum;
    emailCarrier();
  }
  
  function checkLimit(){
    if (typeof(parent.displayCoverageLimitWarining) == "function"){
      if ((parseFloat(CollisionLimitAmt.value) > 0 && parseFloat(CollisionLimitAmt.value) <= 10000) ||
          (parseFloat(ComprehensiveLimitAmt.value) > 0 && parseFloat(ComprehensiveLimitAmt.value) <= 10000 )||
          (parseFloat(LiabilityLimitAmt.value) > 0 && parseFloat(LiabilityLimitAmt.value) <= 10000) ||
          (parseFloat(UnderInsuredLimitAmt.value) > 0 && parseFloat(UnderInsuredLimitAmt.value) <= 10000) ||
          (parseFloat(UnInsuredLimitAmt.value) > 0 && parseFloat(UnInsuredLimitAmt.value) <= 10000) )
        parent.displayCoverageLimitWarining(true);
      else
        parent.displayCoverageLimitWarining(false);
    }
    if (parseFloat(CollisionLimitAmt.value) > 0 && parseFloat(CollisionLimitAmt.value) <= 10000)
      CollisionLimitWarningInd.style.display = "inline";
    else
      CollisionLimitWarningInd.style.display = "none";

    if (parseFloat(ComprehensiveLimitAmt.value) > 0 && parseFloat(ComprehensiveLimitAmt.value) <= 10000)
      ComprehensiveLimitWarningInd.style.display = "inline";
    else
      ComprehensiveLimitWarningInd.style.display = "none";

    if (parseFloat(LiabilityLimitAmt.value) > 0 && parseFloat(LiabilityLimitAmt.value) <= 10000)
      LiabilityLimitWarningInd.style.display = "inline";
    else
      LiabilityLimitWarningInd.style.display = "none";

    if (parseFloat(UnderInsuredLimitAmt.value) > 0 && parseFloat(UnderInsuredLimitAmt.value) <= 10000)
      UnderInsuredLimitWarningInd.style.display = "inline";
    else
      UnderInsuredLimitWarningInd.style.display = "none";

    if (parseFloat(UnInsuredLimitAmt.value) > 0 && parseFloat(UnInsuredLimitAmt.value) <= 10000)
      UnInsuredLimitWarningInd.style.display = "inline";
    else
      UnInsuredLimitWarningInd.style.display = "none";
    
  }
    
if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;


]]>
</SCRIPT></HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="tabInit(false,'#FFFFFF'); initSelectBoxes(); popupInit(); InitFields(); PageInit(); ">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>


<DIV unselectable="on" style="position:absolute; z-index:1; top: 0px; left: 0px;">
  <!-- Start Tabs1 -->
  <DIV unselectable="on" style="position:absolute; visibility: visible; top: 2px; left: 0px">
    <DIV unselectable="on" id="tabs1" class="apdtabs" >
      <SPAN id="tab11" class="tab1" unselectable="on">Claim</SPAN>
      <SPAN id="tab12" class="tab1" unselectable="on">Caller</SPAN>
      <SPAN id="tab13" class="tab1" unselectable="on">Insured</SPAN>
      <SPAN id="tab14" class="tab1" unselectable="on">Contact</SPAN>
      <SPAN id="tab15" class="tab1" unselectable="on">Carrier</SPAN>
      <SPAN id="tab16" class="tab1" unselectable="on">Witness (<xsl:value-of select="count(Claim/Witness[@InvolvedID &gt; 0])"/>)</SPAN>
    </DIV>

    <!-- Start Claim Info -->
    <DIV class="content1" id="content11" name="Claim" Update="uspClaimFNOLUpdDetail" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$ClaimCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/@SysLastUpdatedDate"/></xsl:attribute>

      <xsl:for-each select="Claim" >
        <xsl:call-template name="ClaimInfo"/>
      </xsl:for-each>

      <SCRIPT>ADS_Buttons('content11');</SCRIPT>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($CallerCRUD) )"/>
    <!-- Start Caller Info -->
    <DIV class="content1" id="content12" name="Caller" Update="uspClaimCallerUpdDetail" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$CallerCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/Caller/@SysLastUpdatedDate"/></xsl:attribute>
      <xsl:attribute name="ContextID"><xsl:value-of select="Claim/Caller/@InvolvedID"/></xsl:attribute>
      <xsl:attribute name="ContextName">Involved</xsl:attribute>

      <xsl:for-each select="Claim/Caller"  >
        <xsl:call-template name="CallerInfo"/>
      </xsl:for-each>

      <SCRIPT>ADS_Buttons('content12');</SCRIPT>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($InsuredCRUD) )"/>
    <!-- Start Insured Info -->
    <DIV class="content1" id="content13" name="Insured" Update="uspClaimInsuredUpdDetail" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)">
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$InsuredCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/Insured/@SysLastUpdatedDate"/></xsl:attribute>
      <xsl:attribute name="ContextID"><xsl:value-of select="Claim/Insured/@InvolvedID"/></xsl:attribute>
      <xsl:attribute name="ContextName">Involved</xsl:attribute>

      <xsl:for-each select="Claim/Insured" >
        <xsl:call-template name="InsuredInfo"/>
      </xsl:for-each>

      <SCRIPT>ADS_Buttons('content13');</SCRIPT>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($ContactCRUD) )"/>
    <!-- Start Contact Info -->
    <DIV class="content1" id="content14" name="Contact" Update="uspClaimContactUpdDetail" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$ContactCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/Contact/@SysLastUpdatedDate"/></xsl:attribute>
      <xsl:attribute name="ContextID"><xsl:value-of select="Claim/Contact/@InvolvedID"/></xsl:attribute>
      <xsl:attribute name="ContextName">Involved</xsl:attribute>

      <xsl:for-each select="Claim/Contact" >
        <xsl:call-template name="ContactInfo"/>
      </xsl:for-each>

      <SCRIPT>ADS_Buttons('content14');</SCRIPT>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($CarrierCRUD) )"/>
    <!-- Start Carrier Info -->
    <!--<DIV class="content1" id="content15" name="Carrier" Update="uspClaimCarrierUpdDetail" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >-->
    <DIV class="content1" id="content15" name="Carrier" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$CarrierCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/@SysLastUpdatedDate"/></xsl:attribute>
      <xsl:attribute name="ContextID"><xsl:value-of select="Claim/Carrier/@InvolvedID"/></xsl:attribute>
      <xsl:attribute name="ContextName">Involved</xsl:attribute>

      <xsl:for-each select="Claim/Carrier" >
        <xsl:call-template name="CarrierInfo"><xsl:with-param name="MyCRUD" select="$CarrierCRUD"/></xsl:call-template>
      </xsl:for-each>

    </DIV>

    <xsl:value-of select="js:setPageEnabled()"/>
    <xsl:choose>
        <xsl:when test="contains($WitnessCRUD, 'R')">
            <!-- This will allow the user to atleast browse thru the Witness List and the Witness page will be readonly. -->
            <xsl:value-of select="js:SetCRUD( '_RU_' )"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="js:SetCRUD( $WitnessCRUD )"/>
        </xsl:otherwise>
    </xsl:choose>
    <!-- Start Witness Info -->
    <DIV class="content1" id="content16" name="Witness" Update="uspClaimWitnessUpdDetail" Insert="uspClaimWitnessInsDetail" Delete="uspClaimWitnessDel" Many="true" style="height:140px; width:740px" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">_R__</xsl:when>
          <xsl:otherwise><xsl:value-of select="$WitnessCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="Count"><xsl:value-of select="count(Claim/Witness[@InvolvedID &gt; 0])"/></xsl:attribute>
      <DIV id="content16Sel" class="selBox" style="z-index:1; width:730px; height:120px;">
        <DIV style="position:relative; left:10px; top:-10px; z-index:100000">
          <xsl:variable name="WitnessName">
            <xsl:for-each select="Claim/Witness">
              <xsl:value-of select="@NameFirst"/>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="@NameLast"/>
              <xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="WitnessIDs">
            <xsl:for-each select="Claim/Witness">
              <xsl:value-of select="@InvolvedID"/>
              <xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('Witness',2,'onWitnessChange',0,1,6,string($WitnessName),'',string($WitnessIDs))"/>

        </DIV>
         <IFRAME id="ifrmWitness" src="blank.asp"  onreadystatechange='top.IFStateChange(this);' style='width:710px; height:100px; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
         </IFRAME>
      </DIV>
      <SCRIPT>ADS_Buttons('content16');</SCRIPT>
    </DIV>
    <xsl:if test="$pageReadOnly = 'true'">
      <xsl:value-of select="js:setPageDisabled()"/>
    </xsl:if>

  </DIV>
  <!-- End Tabs1 -->

  <!-- Start Tabs2 -->
  <DIV id="LossTab" style="position:absolute; visibility: visible; top: 172px; left: 0px">
    <DIV id="tabs2" class="apdtabs">
      <SPAN id="tab21" class="tab2" unselectable="on">Loss Info</SPAN>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($ClaimCRUD) )"/>
    <!-- Start Loss Info -->
    <DIV class="content2" id="content21" style="width:740px; visibility:visible" name="Loss" Update="uspClaimLossUpdDetail" onfocusin="DivOnFocusIn(this)" >
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$ClaimCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
     <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/@SysLastUpdatedDate"/></xsl:attribute>

      <xsl:for-each select="Claim" >
        <xsl:call-template name="LossInfo"><xsl:with-param name="MyCRUD" select="$ClaimCRUD"/></xsl:call-template>
      </xsl:for-each>
      <SCRIPT>ADS_Buttons('content21');</SCRIPT>
    </DIV>

  </DIV>
  <!-- End Tabs2 -->

  <!-- Start Tabs3 -->
  <DIV id="CoverageTab" style="position:absolute; visibility: visible; top: 342px; left: 0px">
    <DIV id="tabs3" class="apdtabs">
      <SPAN id="tab31" class="tab3" unselectable="on">Coverage Info</SPAN>
    </DIV>

    <xsl:value-of select="js:SetCRUD( string($CoverageCRUD) )"/>
    <!-- Start Coverage Info -->
      <DIV class="content3" id="content31" name="Coverage" Update="uspClaimCoverageUpdDetail" style="height:140px; width:740px; visibility:visible;" onfocusin="DivOnFocusIn(this)" >
    <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$CoverageCRUD"/></xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Claim/Coverage/@SysLastUpdatedDate"/></xsl:attribute>

      <xsl:for-each select="Claim/Coverage" >
        <xsl:call-template name="CoverageInfo"/>
      </xsl:for-each>

      <SCRIPT>ADS_Buttons('content31');</SCRIPT>
  </DIV>
  </DIV>
  <!-- End Tabs3 -->

<DIV id="divTooltip"> </DIV>
</DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the Claim Info -->
  <xsl:template name="ClaimInfo" >
      <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
        <TR>
          <TD unselectable="on">Policy:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPolicy',20,'PolicyNumber',.,'',1,1)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Date of Loss:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDateLoss',24,'LossDate',.,'',4,2)"/>
          </TD>
          <TD unselectable="on"></TD>
          <TD>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Submitted By:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCSRNo',7,'IntakeUserCsrNo',.)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCSRName',15,'IntakeUserFirstName',.)"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCSRName',15,'IntakeUserLastName',.)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on" vAlign="top" style="font-color:#c0c0c0; font-size:10px">
						ex. MM/DD/YYYY<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;HH:MM:SS A/PM
					</TD>
          <TD unselectable="on"></TD>
          <TD unselectable="on"></TD>
        </TR>
        <TR>
          <TD unselectable="on"></TD>
          <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCSRNo',45,'IntakeUserCompany',.)"/></TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Date of Notice:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDateOfNotice',25,'IntakeStartDate',.,'',4)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Time Completed:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTimeCompleted',25,'IntakeFinishDate',.,'',4)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
      </TABLE>
  </xsl:template>

  <!-- Gets the Caller Info -->
  <xsl:template name="CallerInfo" >
        <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD ><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNameTitle',2,'NameTitle',.,'','',3)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNameFirst',15,'NameFirst',.,'','',4)"/>
            <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNameLast',15,'NameLast',.,'','',5)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:
          </TD>
          <TD unselectable="on">
            <xsl:variable name="rtCallerRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_1',2,'onSelectChange',number($rtCallerRelToIns),1,5,/Root/Reference[@List='CallerRelationToInsured'],'Name','ReferenceID',0,114,'','',11)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Address:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAddress1',30,'Address1',.,'',1,6)"/>
          </TD>
         <TD unselectable="on" nowrap="">Notice Method:
          </TD>
          <TD unselectable="on">
            <xsl:variable name="rtContactMethod" select="/Root/Claim/@ClaimantContactMethodID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('ClaimantContactMethodID',2,'onSelectChange',number($rtContactMethod),1,3,/Root/Reference[@List='NoticeMethod'],'Name','ReferenceID',0,64,'','',12)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAddress2',30,'Address2',.,'',1,7)"/>
          </TD>
          <TD colspan="4" unselectable="on"><img src="images\background_top_flip.gif" width="204" /><img src="images\background_top.gif" width="204" /></TD>
        </TR>
        <TR>
          <TD unselectable="on">City:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerCity',15,'AddressCity',.,'',1,8)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Best Number to Call:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtCallerBCP" select="@BestContactPhoneCD"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtCallerBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',13,1)"/>
          </TD>
          <TD unselectable="on">Day:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerDayAreaCode',2,'DayAreaCode',.,'',10,15)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerExchangeNumber',2,'DayExchangeNumber',.,'',10,16)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerDayUnitNumber',2,'DayUnitNumber',.,'',10,17)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerDayExtensionNumber',3,'DayExtensionNumber',.,'',10,18)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">State:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerState',2,'AddressState',.,'',12,9)"/>
             <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerZip',10,'AddressZip',.,'',10,10)"/>
        <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtCallerState', 'txtCallerCity');"></xsl:text>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Best Time to Call:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtBestContactTime',20,'BestContactTime',.,'',1,14)"/>
          </TD>
          <TD unselectable="on">Night:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNightAreaCode',2,'NightAreaCode',.,'',10,19)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNightExchangeNumber',2,'NightExchangeNumber',.,'',10,20)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNightUnitNumber',2,'NightUnitNumber',.,'',10,21)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerNightExtensionNumber',3,'NightExtensionNumber',.,'',10,22)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </TD>
          <TD unselectable="on">Alt.:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAlternateAreaCode',2,'AlternateAreaCode',.,'',10,23)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAlternateExchangeNumber',2,'AlternateExchangeNumber',.,'',10,24)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,25)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtCallerAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCallerAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,26)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

  <!-- Gets the Insured Info -->
  <xsl:template name="InsuredInfo" >
      <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD  nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNameTitle',2,'NameTitle',.,'','',27)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNameFirst',15,'NameFirst',.,'','',28)"/>
            <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNameLast',15,'NameLast',.,'','',29)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>SSN/EIN:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxID',15,'FedTaxId',.,'',1,36)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on">Business:</TD>
          <TD >
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredBusinessName',40,'BusinessName',.,'',1,30)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Address:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAddress1',30,'Address1',.,'',1,31)"/>
          </TD>
          <TD colspan="4" unselectable="on"><img src="images\background_top_flip.gif" width="204" /><img src="images\background_top.gif" width="204" /></TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAddress2',30,'Address2',.,'',1,32)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Best Number to Call:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtInsuredBCP" select="@BestContactPhoneCD"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtInsuredBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',37,1)"/>
          </TD>
          <TD unselectable="on">Day:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredDayAreaCode',2,'DayAreaCode',.,'',10,39)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredDayExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredDayExchangeNumber',2,'DayExchangeNumber',.,'',10,40)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredDayUnitNumber',2,'DayUnitNumber',.,'',10,41)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredDayExtensionNumber',3,'DayExtensionNumber',.,'',10,42)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">City:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredCity',15,'AddressCity',.,'',1,33)"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Best Time to Call:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredBestTime',20,'BestContactTime',.,'',1,38)"/>
          </TD>
          <TD unselectable="on">Night:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNightAreaCode',2,'NightAreaCode',.,'',10,43)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNightExchangeNumber',2,'NightExchangeNumber',.,'',10,44)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNightUnitNumber',2,'NightUnitNumber',.,'',10,45)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredNightExtensionNumber',3,'NightExtensionNumber',.,'',10,46)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on" nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredState',2,'AddressState',.,'',12,34)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredZip',10,'AddressZip',.,'',10,35)"/>
            <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtInsuredState', 'txtInsuredCity');"></xsl:text>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Alt.:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAlternateAreaCode',2,'AlternateAreaCode',.,'',10,47)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAlternateExchangeNumber',2,'AlternateExchangeNumber',.,'',10,48)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,49)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInsuredAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,50)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>

      </TABLE>
  </xsl:template>

  <!-- Gets the Contact Info -->
  <xsl:template name="ContactInfo" >
      <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameTitle',2,'NameTitle',.,'',1,51)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameFirst',15,'NameFirst',.,'',1,52)"/>
            <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameLast',15,'NameLast',.,'',1,53)"/>
          </TD>
          <TD  unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtContactRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_2',2,'onSelectChange',number($rtContactRelToIns),1,3,/Root/Reference[@List='ContactRelationToInsured'],'Name','ReferenceID',0,114,'','',60)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
        <TR>
          <TD unselectable="on">Address:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress1',30,'Address1',.,'',1,54)"/>
          </TD>
          <TD  unselectable="on">
          <xsl:attribute name="nowrap"/>Insured's Agent:
          </TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuredAgent',20,'AgentName',..,'',1,61)"/>
          </TD>
          <TD  unselectable="on">
          <xsl:attribute name="nowrap"/>Phone:
          </TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgentAreaCode',2,'AgentAreaCode',..,'',10,62)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, AgentExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgentExchangeNumber',2,'AgentExchangeNumber',..,'',10,63)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, AgentUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgentUnitNumber',2,'AgentUnitNumber',..,'',10,64)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, AgentExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgentExtensionNumber',3,'AgentExtensionNumber',..,'',10,65)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress2',30,'Address2',.,'',1,55)"/>
          </TD>
          <TD colspan="4" unselectable="on"><img src="images\background_top_flip.gif" width="204" /><img src="images\background_top.gif" width="204" /></TD>        </TR>
        <TR>
          <TD unselectable="on">City:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactCity',15,'AddressCity',.,'',1,56)"/>
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Number to Call:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtContactBCP" select="@BestContactPhoneCD"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtContactBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',66,1)"/>
          </TD>
          <TD unselectable="on">Day:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayAreaCode',2,'DayAreaCode',.,'',10,68)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExchangeNumber',2,'DayExchangeNumber',.,'',10,69)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayUnitNumber',2,'DayUnitNumber',.,'',10,70)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExtensionNumber',3,'DayExtensionNumber',.,'',10,71)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactState',2,'AddressState',.,'',12,57)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactZip',10,'AddressZip',.,'',10,58)"/>
            <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtContactState', 'txtContactCity');"></xsl:text>
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Time to Call:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactBestContactTime',20,'BestContactTime',.,'',1,67)"/>
          </TD>
          <TD unselectable="on">Night:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightAreaCode',2,'NightAreaCode',.,'',10,72)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExchangeNumber',2,'NightExchangeNumber',.,'',10,73)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightUnitNumber',2,'NightUnitNumber',.,'',10,74)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExtensionNumber',3,'NightExtensionNumber',.,'',10,75)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on">E-mail:</TD>
          <TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmailAddress',30,'EmailAddress',.,'',10,59)"/><xsl:text disable-output-escaping="yes">onbeforedeactivate="checkEMail(this);"></xsl:text>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Alt.:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateAreaCode',2,'AlternateAreaCode',.,'',10,76)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExchangeNumber',2,'AlternateExchangeNumber',.,'',10,77)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,78)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,79)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

  <!-- Gets the Carrier Info -->
  <xsl:template name="CarrierInfo" >
    <xsl:param name="MyCRUD"/>
  
    <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Name:</TD>
        <TD unselectable="on">
          <xsl:if test="boolean(contains($MyCRUD,'R'))">
  					<IMG src="/images/next_button.gif" onClick="ShowClaimCarrierRep('visible')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="86">
              <xsl:if test="not(contains($MyCRUD, 'U'))">
                <xsl:attribute name="disabled"/>
                <xsl:attribute name="readonly"/>
                <xsl:attribute name="style">visibility:hidden</xsl:attribute>
              </xsl:if>
            </IMG>
          </xsl:if> 
           
          <INPUT class="InputReadonlyField" id="txtCarrierRepNameTitle" size="2" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@NameTitle"/></xsl:attribute>               
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT class="InputReadonlyField" id="txtCarrierRepNameFirst" size="20" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@NameFirst"/></xsl:attribute>               
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT class="InputReadonlyField" id="txtCarrierRepNameLast" size="34" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@NameLast"/></xsl:attribute>               
          </INPUT>
          
          <INPUT type="hidden" id="txtCarrierRepUserID" name="CarrierRepUserID" size="36" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@UserID"/></xsl:attribute>               
          </INPUT>     
        </TD>          
      </TR>
      <TR>
        <TD unselectable="on">Office:</TD>
        <TD>
          <INPUT class="InputReadonlyField" id="txtCarrierOffice" size="70" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@OfficeName"/></xsl:attribute>
          </INPUT>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><a id="aEmail" name="aEmail" href="Javascript:emailCarrier()" Title="Click to E-mail Carrier Representative" style="text-decoration:underline;">E-mail</a>:</TD>
        <TD unselectable="on">
          <INPUT class="InputReadonlyField" id="txtCarrierRepEmail" size="70" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
          </INPUT>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on">
          <INPUT class="InputReadonlyField" id="txtCarrierRepPhoneAreaCode" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@PhoneAreaCode"/></xsl:attribute>
          </INPUT>-
          <INPUT class="InputReadonlyField" id="txtCarrierRepPhoneExchangeNumber" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:attribute>
          </INPUT>-
          <INPUT class="InputReadonlyField" id="txtCarrierRepPhoneUnitNumber" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@PhoneUnitNumber"/></xsl:attribute>
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT class="InputReadonlyField" id="txtCarrierRepPhoneExtensionNumber" size="4" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:attribute>
          </INPUT>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Fax:</TD>
        <TD unselectable="on">
          <INPUT class="InputReadonlyField" id="txtCarrierRepFaxAreaCode" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@FaxAreaCode"/></xsl:attribute>
          </INPUT>-
          <INPUT class="InputReadonlyField" id="txtCarrierRepFaxExchangeNumber" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@FaxExchangeNumber"/></xsl:attribute>
          </INPUT>-
          <INPUT class="InputReadonlyField" id="txtCarrierRepFaxUnitNumber" size="3" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@FaxUnitNumber"/></xsl:attribute>
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT class="InputReadonlyField" id="txtCarrierRepFaxExtensionNumber" size="4" readonly="true" tabIndex="-1">
            <xsl:attribute name="value"><xsl:value-of select="@FaxExtensionNumber"/></xsl:attribute>
          </INPUT>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <!-- Gets the Loss Info -->
  <xsl:template name="LossInfo" >
		<xsl:param name="MyCRUD" />
    <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="726">
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Trip Purpose:</TD>
        <TD><xsl:attribute name="nowrap"/>
          <xsl:variable name="rtTrip" select="@TripPurposeCD"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('TripPurposeCD',2,'onSelectChange',string($rtTrip),1,2,/Root/Reference[@List='TripPurpose'],'Name','ReferenceID','','','','',80,1)"/>
        </TD>
        <TD colspan="4" unselectable="on"><xsl:attribute name="nowrap"/>Loss Type:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

				<xsl:if test="boolean(contains($MyCRUD,'R'))">
					<IMG src="/images/next_button.gif" onClick="ShowLossType('visible')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="86">
                        <xsl:if test="not(contains($MyCRUD, 'U'))">
                            <xsl:attribute name="disabled"/>
                            <xsl:attribute name="readonly"/>
                            <xsl:attribute name="style">visibility:hidden</xsl:attribute>
                        </xsl:if>
                    </IMG>
          <INPUT class="InputReadonlyField" id="txtLossTypeText" size="70" readonly="true" tabIndex="-1">
            <xsl:attribute name="value">
              <xsl:if test="boolean(/Root/Reference[@List='LossType' and current()/@LossTypeGrandParentID &gt; 0])">
                 <xsl:value-of select="/Root/Reference[@List='LossType' and @ReferenceID=current()/@LossTypeGrandParentID]/@Name"/>
                 <xsl:text> </xsl:text>-<xsl:text> </xsl:text>
              </xsl:if>
              <xsl:if test="boolean(/Root/Reference[@List='LossType' and current()/@LossTypeParentID &gt; 0])">
                 <xsl:value-of select="/Root/Reference[@List='LossType' and @ReferenceID=current()/@LossTypeParentID]/@Name"/>
                <xsl:text> </xsl:text>-<xsl:text> </xsl:text>
              </xsl:if>
               <xsl:value-of select="/Root/Reference[@List='LossType' and @ReferenceID=current()/@LossTypeID]/@Name"/>
            </xsl:attribute>
          </INPUT>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossType',10,'LossTypeID',.,'',6)"/>
        </xsl:if>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Loss Description:</TD>
        <TD colspan="3" rowspan="2"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtLossDescShort',58,2,'LossDescription',.,'','',81)"/>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Weather Conditions:</TD>
        <TD height="18">
          <xsl:variable name="rtWcond" select="@WeatherConditionID"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('WeatherConditionID',2,'onSelectChange',number($rtWcond),1,2,/Root/Reference[@List='WeatherCondition'],'Name','ReferenceID','','','','',86)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Road Type:</TD>
        <TD height="18">
          <xsl:variable name="rtRtype" select="@RoadTypeID"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('RoadTypeID',2,'onSelectChange',number($rtRtype),1,1,/Root/Reference[@List='RoadType'],'Name','ReferenceID','','','','',87)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Loss Location:</TD>
        <TD><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossAddress',30,'LossLocation',.,'',1,82)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><img src="/images/spacer.gif" alt="" width="80" height="1" border="0"/></TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Road Location:</TD>
        <TD>
          <xsl:variable name="rtRoadLocation" select="@RoadLocationID"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('RoadLocationID',2,'onSelectChange',number($rtRoadLocation),1,0,/Root/Reference[@List='RoadLocation'],'Name','ReferenceID',0,70,'','',88)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">City:</TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossCity',15,'LossCity',.,'',1,83)"/>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Police Dept.:</TD>
        <TD colspan="3"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPoliceDept',50,'PoliceDepartmentName',.,'',1,89)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossState',2,'LossState',.,'',12,83)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossZip',10,'LossZip',.,'',10,84)"/>
            <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtLossState', 'txtLossCity'); ValidateZipToCounty(this, 'txtCounty')"></xsl:text>
        </TD>
        <TD rowspan="2" valign="top" unselectable="on"><xsl:attribute name="nowrap"/>Remarks to Claim Rep:</TD>
        <TD colspan="3" rowspan="2" valign="top"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtRemarksToClaimRep',60,2,'Remarks',.,'','',90)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">County:</TD>
        <TD><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCounty',10,'LossCounty',.,'',1,85)"/>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <!-- Gets the Coverage Info -->
  <xsl:template name="CoverageInfo" >
    <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Claim Number:</TD>
        <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtClientClaimNumber',25,'ClientClaimNumber',.,'',1,91)"/></TD>
      </TR>
    </TABLE>
    <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;margin:0px;margin-top:10px;" ><!-- width="550" -->
      <colgroup>
        <col width="85"/>
        <col width="25"/>
        <col width="10"/>
        <col width="65"/>
        <col width="100"/>
        <col width="10"/>
        <col width="35"/>
        <col width="100"/>
        <col width="30"/>
        <col width="50"/>
        <col width="25"/>
        <col width="10"/>
        <col width="60"/>
        <col width="100"/>
      </colgroup>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Collision:</TD>
        <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('CollisionCoverageFlag',string(./@CollisionCoverageFlag),'1','0','','',92)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Deductible:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCollDeductAmt',15,'CollisionDeductibleAmt',.,'',1,93)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Limit:</TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCollLimitAmt',15,'CollisionLimitAmt',.,'',10,94)"/>
          <xsl:text disable-output-escaping="yes">onBlur="checkLimit();"></xsl:text>
        </TD>
        <TD unselectable="on">
          <img name="CollisionLimitWarningInd" id="CollisionLimitWarningInd" src="/images/exclamation_sm.gif">
            <xsl:choose>
              <xsl:when test="number(@CollisionLimitAmt) &gt; 0 and number(@CollisionLimitAmt) &lt;= 10000"></xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="style">display:none</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Rental:</TD>
        <TD unselectable="on"> <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('RentalCoverageFlag',string(./@RentalCoverageFlag),'1','0','','',107)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Days:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRentalDays',3,'RentalDays',.,'',1,108)"/></TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Comprehensive:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/> <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('ComprehensiveCoverageFlag',string(./@ComprehensiveCoverageFlag),'1','0','','',95)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Deductible:</TD>
        <TD><xsl:attribute name="nowrap"/><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompDeductAmt',15,'ComprehensiveDeductibleAmt',.,'',1,96)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Limit:</TD>
        <TD>
          <xsl:attribute name="nowrap"/><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompLimitAmt',15,'ComprehensiveLimitAmt',.,'',10,97)"/>
          <xsl:text disable-output-escaping="yes">onBlur="checkLimit();"></xsl:text>
        </TD>
        <TD colspan="4" unselectable="on">
          <img name="ComprehensiveLimitWarningInd" id="ComprehensiveLimitWarningInd" src="/images/exclamation_sm.gif">
            <xsl:choose>
              <xsl:when test="number(@ComprehensiveLimitAmt) &gt; 0 and number(@ComprehensiveLimitAmt) &lt;= 10000"></xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="style">display:none</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Daily Limit:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRentalDailyLimitAmt',15,'RentalDailyLimitAmt',.,'',1,109)"/></TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Liability:</TD>
        <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('LiabilityCoverageFlag',string(./@LiabilityCoverageFlag),'1','0','','',98)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Deductible:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLiabilityDeductAmt',15,'LiabilityDeductibleAmt',.,'',1,99)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Limit:</TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLiabilityLimitAmt',15,'LiabilityLimitAmt',.,'',10,100)"/>
          <xsl:text disable-output-escaping="yes">onBlur="checkLimit();"></xsl:text>
        </TD>
        <TD colspan="4" unselectable="on">
          <img name="LiabilityLimitWarningInd" id="LiabilityLimitWarningInd" src="/images/exclamation_sm.gif">
            <xsl:choose>
              <xsl:when test="number(@LiabilityLimitAmt) &gt; 0 and number(@LiabilityLimitAmt) &lt;= 10000"></xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="style">display:none</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Total Limit:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRentalLimitAmt',15,'RentalLimitAmt',.,'',1,110)"/></TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Underinsured:</TD>
        <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('UnderInsuredCoverageFlag',string(./@UnderInsuredCoverageFlag),'1','0','','',101)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Deductible:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtUIMDeductAmt',15,'UnderInsuredDeductibleAmt',.,'',1,102)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Limit:</TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtUIMLimitAmt',15,'UnderInsuredLimitAmt',.,'',10,103)"/>
          <xsl:text disable-output-escaping="yes">onBlur="checkLimit();"></xsl:text>
        </TD>
        <TD colspan="6" unselectable="on">
          <img name="UnderInsuredLimitWarningInd" id="UnderInsuredLimitWarningInd" src="/images/exclamation_sm.gif">
            <xsl:choose>
              <xsl:when test="number(@UnderInsuredLimitAmt) &gt; 0 and number(@UnderInsuredLimitAmt) &lt;= 10000"></xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="style">display:none</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Uninsured:</TD>
        <TD unselectable="on"><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('UnInsuredCoverageFlag',string(./@UnInsuredCoverageFlag),'1','0','','',104)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Deductible:</TD>
        <TD><xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtUMDeductAmt',15,'UnInsuredDeductibleAmt',.,'',1,105)"/></TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>Limit:</TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtUMLimitAmt',15,'UnInsuredLimitAmt',.,'',10,106)"/>
          <xsl:text disable-output-escaping="yes">onBlur="checkLimit();"></xsl:text>
        </TD>
        <TD colspan="6" unselectable="on">
          <img name="UnInsuredLimitWarningInd" id="UnInsuredLimitWarningInd" src="/images/exclamation_sm.gif">
            <xsl:choose>
              <xsl:when test="number(@UnInsuredLimitAmt) &gt; 0 and number(@UnInsuredLimitAmt) &lt;= 10000"></xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="style">display:none</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

</xsl:stylesheet>
