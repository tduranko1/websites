<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function formatDate(strDate)
     {
        var strRet = "";
        var strTmp = "";
        var aTmp;
        if (strDate != "") {
            strTmp = strDate.split("T")[0];
            aTmp = strTmp.split("-");
            strRet = aTmp[1] + "/" + aTmp[2] + "/" + aTmp[0];
        }
        return strRet;
     }
  ]]>
</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ReportCRUD" select="Reports:Open Billing"/>

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($ReportCRUD, 'R')">
        <div align="left" style="width:725px;margin-bottom:4px;">
        <input type="button" class="formbutton" onclick="printPage()" value="Print" id="btnPrint"/>
        </div>
        <div id="rptText" style="width:725px;height:320px;background-color:#FFFFFF;border:1px solid #c0c0c0;border-bottom:3px solid #000000;border-right:3px solid #000000;padding:5px;overflow:auto;">
        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
            <tr>
                <td><div style="font:bolder 12pt Tahoma;">Open Billing Report</div></td>
                <td><div style="font:bolder 12pt Tahoma;" align="right">LYNX Services</div></td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <colgroup>
                <col width="65px"/>
                <col width="100px"/>
                <col width="100px"/>
                <col width="55px"/>
                <col width="100px"/>
                <col />
            </colgroup>
            <tr>
                <td><strong>From Date:</strong></td>
                <td><xsl:value-of select="@FromDate"/></td>
                <td> </td>
                <td><strong>To Date:</strong></td>
                <td><xsl:value-of select="@ToDate"/></td>
                <td> </td>
                <td align="right"><strong>Report Date:  </strong><xsl:value-of select="@ReportDate"/></td>
            </tr>
        </table>
        <br/>
        <xsl:for-each select="Reference[@ListName='Insurance']">
          <xsl:sort select="@Name" data-type="text" order="ascending"/>
          <xsl:call-template name="InsuranceBilling"/>
        </xsl:for-each>
        <br/>
        <table cellspacing="0" cellpadding="3" border="0" style="border:0px solid #000000;border-collapse:collapse; width:100%">
          <tr>
            <td style="text-align:right;font-weight:bold">Grand Total:  <xsl:value-of select="format-number(sum(/Root/Billing/@Amount), '$###,###,###,##0.00')"/></td>
          </tr>
        </table>
        </div>
    </xsl:when>
    <xsl:otherwise>
        <font color="#ff0000"><strong>You do not have sufficient permission to view this report.
        <br/>Please contact administrator for permissions.</strong></font>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="InsuranceBilling">
  <xsl:variable name="InsuranceID"><xsl:value-of select="@InsuranceCompanyID"/></xsl:variable>
    <div style="font:bolder 10pt Tahoma;">
      <xsl:value-of select="@Name"/>
    </div>
        <table cellspacing="0" cellpadding="3" border="1" style="border:1px solid #000000;border-collapse:collapse;margin-bottom:1cm">
            <colgroup>
                <col width="75px"/>
                <col width="100px"/>
                <col width="200px"/>
                <col width="200px"/>
                <col width="100px"/>
                <col width="150px"/>
                <col width="75px"/>
            </colgroup>
            <tr align="center" style="border:1px solid #000000">
                <td><strong>LYNX ID</strong></td>
                <td><strong>Claim Number</strong></td>
                <td><strong>Insured</strong></td>
                <td><strong>Claimant</strong></td>
                <td><strong>Date of Billing Request</strong></td>
                <td><strong>Description</strong></td>
                <td><strong>Amount</strong></td>
            </tr>
              <xsl:for-each select="/Root/Billing[@InsuranceCompanyID=$InsuranceID]" >
                <tr>
                    <td><xsl:value-of select="@LynxID"/></td>
                    <td><xsl:value-of select="@ClaimNumber"/></td>
                    <td><xsl:value-of select="@InsuredName"/></td>
                    <td><xsl:value-of select="@ClaimantName"/></td>
                    <td><xsl:value-of select="user:formatDate(string(@DateBillingRequest))"/></td>
                    <td><xsl:value-of select="@Description"/></td>
                    <td align="right"><xsl:value-of select="format-number(@Amount, '$###,###,##0.00')"/></td>
                </tr>
              </xsl:for-each>
            <tr>
              <td colspan="6" style="text-align:right">Sub-total:</td>
              <td style="text-align:right;font-weight:bold"><xsl:value-of select="format-number(sum(/Root/Billing[@InsuranceCompanyID=$InsuranceID]/@Amount), '$###,###,###,##0.00')"/></td>
            </tr>

        </table>

</xsl:template>
</xsl:stylesheet>
