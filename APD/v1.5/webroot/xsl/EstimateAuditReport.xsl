<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDReinspectForm">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
	<xsl:variable name="SupervisorForm" select="@SupervisorForm"/>
	
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspEstimateAuditGetDetailXML,EstimateAuditReport.xsl,777  -->

<HEAD>
  <TITLE>Estimate Audit Report</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>
  
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" bgcolor="#FFFAEB" style="margin-right:none">
  <div id="rptText" style="position:absolute; left:10px;">
	
	<IMG src="/images/spacer.gif" width="1" height="4" border="0"/>
	
	<xsl:apply-templates select="Header"/>					
	
  <IMG src="/images/spacer.gif" width="1" height="30" border="0" />
  
  <TABLE unselectable="on" border="0" cellspacing="1" cellpadding="3" style="table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col unselectable="on" width="193"/>
      <col unselectable="on" width="55"/>
      <col unselectable="on" width="65"/>
      <col unselectable="on" width="100%"/>
    </colgroup>
    
    <TR unselectable="on">
	    <td unselectable="on" style="text-align:center;background-color:silver;font-weight:bold;">Audit Rule</td>
      <td unselectable="on" style="c;text-align:center;background-color:silver;font-weight:bold;">Estimate Detail Line</td>
      <td unselectable="on" style="text-align:center;background-color:silver;font-weight:bold;">Audit Weight</td>
      <td unselectable="on" style="text-align:center;background-color:silver;font-weight:bold;">Description</td>
    </TR>
    <xsl:apply-templates select="Discrepancy"/>
 </TABLE>
   
	
  <IMG src="/images/spacer.gif" width="1" height="30" border="0" />
	
	<table width="670" style="table-layout:fixed;">
	  <tr>
	    <td nowrap="nowrap" width="120px">Program Manager:</td>
  		<td width="230px"></td>
  		<td width="60px"></td>
  		<td width="50px">Date:</td>
  		<td width="200px"></td>
	  </tr>
	  <tr>
	    <td></td>
  		<td><hr noshade="" size="1"/></td>
  		<td></td>
  		<td></td>
  		<td><hr noshade="" size="1"/></td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap">Shop Representative:</td>
		  <td></td>
	  </tr>
	  <tr>
	    <td></td>
  		<td><hr noshade="" size="1"/></td>
  		<td></td>
  		<td></td>
  		<td></td>
	  </tr>
	</table> 
 </div> <!-- rptText -->
</BODY>
</HTML>

</xsl:template>


<xsl:template match="Header">
  <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
    <tr>
      <td><div style="font:bolder 18pt Tahoma;">Estimate Audit Report</div></td>
      <td><div style="font:bolder 18pt Tahoma;" align="right">LYNX Services</div></td>
    </tr>
  </table>
	
	<IMG src="/images/spacer.gif" width="1" height="10" border="0"/>
	
	<TABLE cellspacing="0" cellpadding="0" border="0">
    <colgroup>
      <col width="190"/>
      <col width="275"/>
      <col width="100%"/>
    </colgroup>
    <TR>
      <TD colspan="3" style="text-align:right">
        <table border="0">
          <colgroup>
            <col width="125"/>
            <col width="50"/>
          </colgroup>
          <tr>
            <td unselectable="on" style="font-size:18px"><strong>Audit Score:</strong></td>
            <td unselectable="on" style="font-size:18px"><xsl:value-of select="/Root/@AuditWeight"/></td>
          </tr>
        </table>      
        <br/>
      </TD>
    </TR>
	  <TR>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="115"/>
            <col width="75"/>
          </colgroup>
          <tr>
      		  <td nowrap="nowrap"><strong>Assignment Date:</strong></td>
            <td><xsl:value-of select="@AssignmentDate"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Estimate Received:</strong></td>
            <td><xsl:value-of select="@EstimateReceivedDate"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Estimate Type:</strong></td>
            <td><xsl:value-of select="@EstimateType"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Suppl. Seq.:</strong></td>
            <td>
              <xsl:choose>
                <xsl:when test="@SupplementSequenceNumber = '0'"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
                <xsl:otherwise><xsl:value-of select="@SupplementSequenceNumber"/></xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </TD>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="90"/>
            <col width="185"/>
          </colgroup>
          <tr>
            <td nowrap="nowrap"><strong>Shop:</strong></td>
            <td><xsl:value-of select="@ShopName"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Shop Contact:</strong></td>
            <td><xsl:value-of select="@ShopContact"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Shop Phone:</strong></td>
            <td><xsl:value-of select="@ShopPhone"/></td>
          </tr>
          <tr>
      		  <td nowrap="nowrap"><strong>Vehicle:</strong></td>
            <td><xsl:value-of select="normalize-space(@VehicleYMM)"/></td>
          </tr>
        </table>
      </TD>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="80"/>
            <col width="125"/>
            <tr>
              <td><strong>Ins. Co.:</strong></td>
              <td><xsl:value-of select="@InsuranceCompanyName"/></td>
            </tr>
            <tr>
              <td><strong>Claim No:</strong></td>
              <td><xsl:value-of select="@ClaimNumber"/></td>
            </tr>
            <tr>
              <td><strong>Claim Rep.:</strong></td>
              <td><xsl:value-of select="@ClaimRepName"/></td>
            </tr>
            <tr>
              <td><strong>Insured:</strong></td>
              <td><xsl:value-of select="@InsuredName"/></td>
            </tr>
            <tr>
              <td><strong>LynxID:</strong></td>
              <td><xsl:value-of select="@LynxID"/></td>
            </tr>
          </colgroup>
        </table>     
      </TD>
    </TR>
  </TABLE>
</xsl:template>


<xsl:template match="Discrepancy">
  <xsl:variable name="EstimateAuditRuleID" select="@EstimateAuditRuleID"/>
  
  <tr unselectable="on" bgcolor="white" valign="top" height1="80">
    <td class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@AuditRuleName"/></td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@EstimateDetailNumber != '0'"><xsl:value-of select="@EstimateDetailNumber"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
    </td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@AppliedWeight != ''"><xsl:value-of select="@AppliedWeight"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
    </td>
    <td class="GridTypeTD" style="text-align:left;">
      <xsl:choose>
        <xsl:when test="@AuditDescription != ''"><xsl:value-of select="@AuditDescription"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
     </td>
    <td class="GridTypeTD"></td>
  </tr>
</xsl:template>
  
</xsl:stylesheet>

