<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:decimal-format NaN="0"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
  <xsl:for-each select="User">
    <div style="page-break-after:always;margin-bottom:5px;">
      <table cellpadding="5" cellspacing="0" border="0" style="width:100%;border-collapse:collapse;border-bottom:0px solid #000000">
        <colgroup>
          <col width="50%"/>
          <col width="50%"/>
        </colgroup>
        <tr>
          <td colspan="2" style="text-align:center;background-color:#C0C0C0;font-size:12pt"><strong>APD Claim System Profile</strong></td>
        </tr>
        <tr valign="top">
          <td>
            <table cellpadding="2" cellspacing="0" border="1" style="width:100%;border:1px solid #000000;border-collapse:collapse">
              <colgroup>
                <col width="75px"/>
                <col width="*"/>
              </colgroup>
              <tr>
                <td style="text-align:center;background-color:#C0C0C0;" colspan="2"><strong>General Information</strong></td>
              </tr>
              <tr>
                <td><strong>CSR #:</strong></td>
                <td><xsl:value-of select="@CsrNo"/></td>
              </tr>            
              <tr>
                <td><strong>Name:</strong></td>
                <td><xsl:value-of select="concat(@NameFirst, ' ', @NameLast)"/></td>
              </tr>            
              <tr>
                <td><strong>Role:</strong></td>
                <td>
                  <xsl:variable name="priRole" select="Role[@PrimaryRoleFlag='1']/@RoleID"/>
                  <xsl:value-of select="/Root/Reference[@List='Role' and @ReferenceID=$priRole]/@Name"/>
                </td>
              </tr>            
              <tr>
                <td><strong>Supervisor:</strong></td>
                <td>
                  <xsl:variable name="supID" select="@SupervisorUserID"/>
                  <xsl:value-of select="/Root/Reference[@List='Supervisor' and @ReferenceID=$supID]/@Name"/>
                </td>
              </tr>            
              <tr>
                <td><strong>Phone:</strong></td>
                <td><xsl:value-of select="concat('(', @PhoneAreaCode, ') ', @PhoneExchangeNumber, '-', @PhoneUnitNumber, ' x ', @PhoneExtensionNumber)"/></td>
              </tr>            
              <tr>
                <td><strong>Fax:</strong></td>
                <td><xsl:value-of select="concat('(', @FaxAreaCode, ') ', @FaxExchangeNumber, '-', @FaxUnitNumber, ' x ', @FaxExtensionNumber)"/></td>
              </tr>            
              <tr>
                <td><strong>Email:</strong></td>
                <td><xsl:value-of select="@EmailAddress"/></td>
              </tr>            
              <tr valign="top">
                <td><strong>Schedule:</strong></td>
                <td>
                  <table cellpadding="3" cellspacing="0" border="1" style="border-collapse:collapse;border:1px solid #A9A9A9">
                    <tr>
                      <td style="font-weight:bold">Sun</td>
                      <td style="font-weight:bold">Mon</td>
                      <td style="font-weight:bold">Tue</td>
                      <td style="font-weight:bold">Wed</td>
                      <td style="font-weight:bold">Thu</td>
                      <td style="font-weight:bold">Fri</td>
                      <td style="font-weight:bold">Sat</td>
                    </tr>
                    <tr>
                      <td><xsl:choose><xsl:when test="@WorkDaySundayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDayMondayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDayTuesdayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDayWednesdayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDayThursdayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDayFridayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                      <td><xsl:choose><xsl:when test="@WorkDaySaturdayFlag='1'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                    </tr>
                  </table>
                </td>
              </tr>            
              <tr>
                <td><strong>Status</strong></td>
                <td>
                  <xsl:choose>
                    <xsl:when test="@AccessEndDate = '1900-01-01T00:00:00'">Active</xsl:when>
                    <xsl:otherwise>Inactive</xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>            
            </table>
          </td>
          <td>
            <table cellpadding="2" cellspacing="0" border="0" style="width:100%;border:1px solid #000000;border-collapse:collapse">
              <colgroup>
                <col width="75px"/>
                <col width="*"/>
              </colgroup>
              <tr>
                <td style="text-align:center;background-color:#C0C0C0;border-bottom:1px solid #000000" colspan="2"><strong>Claim Department Information</strong></td>
              </tr>
              <tr valign="top">
                <td><strong>Licenses:</strong></td>
                <td>
                  <xsl:variable name="stateCount" select="count(LicenseState)"/>
                  <xsl:variable name="stateRowsPerCol">
                    <xsl:choose>
                      <xsl:when test="$stateCount &lt; 10">1</xsl:when>
                      <xsl:otherwise><xsl:value-of select="ceiling(number($stateCount) div 2)"/></xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="$stateRowsPerCol = 1">
                      <xsl:for-each select="LicenseState">
                        <xsl:sort select="@StateCode"/>
                        <xsl:variable name="stateCode" select="@StateCode"/>
                        <xsl:value-of select="/Root/Reference[@List='LicenseState' and @ReferenceID=$stateCode]/@Name"/><br/>
                      </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                      <span style="width:50%;padding:3px;align:left;">
                        <xsl:for-each select="LicenseState">
                          <xsl:sort select="@StateCode"/>
                          <xsl:if test="position() &lt;= $stateRowsPerCol">
                            <xsl:variable name="stateCode" select="@StateCode"/>
                            <xsl:value-of select="/Root/Reference[@List='LicenseState' and @ReferenceID=$stateCode]/@Name"/><br/>
                          </xsl:if>
                        </xsl:for-each>
                      </span>
                      <span style="width:50%;padding:3px;align:left;">
                        <xsl:for-each select="LicenseState">
                          <xsl:sort select="@StateCode"/>
                          <xsl:if test="position() &gt; $stateRowsPerCol">
                            <xsl:variable name="stateCode" select="@StateCode"/>
                            <xsl:value-of select="/Root/Reference[@List='LicenseState' and @ReferenceID=$stateCode]/@Name"/><br/>
                          </xsl:if>
                        </xsl:for-each>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>            
            </table>            
          </td>
        </tr>
      </table>
      <table cellpadding="5" cellspacing="0" border="1" style="width:99%;border-collapse:collapse;border:1px solid #000000;margin:2px;">
        <colgroup>
          <col width="50%"/>
          <col width="50%"/>
        </colgroup>
        <tr>
          <td colspan="2" style="text-align:left;background-color:#C0C0C0;"><strong>Profile Overrides</strong></td>
        </tr>
        <tr valign="top">
            <xsl:variable name="profCount" select="count(Profile)"/>
            <xsl:variable name="profRowsPerCol" select="ceiling(number($profCount) div 2)"/>
          <td>
            <table cellpadding="2" cellspacing="0" border="0" style="border-collapse:collapse;border:1px solid #C0C0C0;width:100%">
              <colgroup>
                <col width="75px"/>
              </colgroup>
              <tr valign="top">
                <td style="border:1px solid #C0C0C0;text-align:center;background-color:#C0C0C0;"><strong>Overridden</strong></td>
                <td style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Category</strong></td>
                <td style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Value</strong></td>
              </tr>
              <xsl:for-each select="Profile">
                <xsl:sort select="@ProfileName"/>
              <xsl:if test="position() &lt;= number($profRowsPerCol)">
              <tr valign="top">
                <td style="border:1px solid #C0C0C0;text-align:center">
                  <xsl:choose>
                    <xsl:when test="@OverriddenFlag='1'">Yes</xsl:when>
                    <xsl:otherwise>No</xsl:otherwise>
                  </xsl:choose>
                </td>
                <td style="border:1px solid #C0C0C0"><xsl:value-of select="@ProfileName"/></td>
                <td style="border:1px solid #C0C0C0">
                  <xsl:choose>
                    <xsl:when test="@DataTypeCD='B'">
                      <xsl:choose>
                        <xsl:when test="@ProfileValue='1'">Yes</xsl:when>
                        <xsl:otherwise>No</xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>

                    <xsl:when test="@DataTypeCD='S'">
                      <xsl:variable name="profVal" select="@ProfileValue"/>
                      <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and @ReferenceID=$profVal]/@Name"/>
                    </xsl:when>

                    <xsl:when test="@DataTypeCD='N'">
                      <xsl:value-of select="@ProfileValue"/>
                    </xsl:when>
                  </xsl:choose>
                </td>
              </tr>
              </xsl:if>
              </xsl:for-each>   
            </table>            
          </td>
          <td>
            <table cellpadding="2" cellspacing="0" border="0" style="border-collapse:collapse;border:1px solid #C0C0C0;width:100%">
              <colgroup>
                <col width="75px"/>
              </colgroup>
              <tr valign="top">
                <td style="border:1px solid #C0C0C0;text-align:center;background-color:#C0C0C0;"><strong>Overridden</strong></td>
                <td style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Category</strong></td>
                <td style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Value</strong></td>
              </tr>
              <xsl:for-each select="Profile">
                <xsl:sort select="@ProfileName"/>
              <xsl:if test="position() &gt; number($profRowsPerCol)">
              <tr valign="top">
                <td style="border:1px solid #C0C0C0;text-align:center">
                  <xsl:choose>
                    <xsl:when test="@OverriddenFlag='1'">Yes</xsl:when>
                    <xsl:otherwise>No</xsl:otherwise>
                  </xsl:choose>
                </td>
                <td style="border:1px solid #C0C0C0"><xsl:value-of select="@ProfileName"/></td>
                <td style="border:1px solid #C0C0C0">
                  <xsl:choose>
                    <xsl:when test="@DataTypeCD='B'">
                      <xsl:choose>
                        <xsl:when test="@ProfileValue='1'">Yes</xsl:when>
                        <xsl:otherwise>No</xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>

                    <xsl:when test="@DataTypeCD='S'">
                      <xsl:variable name="profVal" select="@ProfileValue"/>
                      <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and @ReferenceID=$profVal]/@Name"/>
                    </xsl:when>

                    <xsl:when test="@DataTypeCD='N'">
                      <xsl:value-of select="@ProfileValue"/>
                    </xsl:when>
                  </xsl:choose>
                </td>
              </tr>
              </xsl:if>
              </xsl:for-each>   
            </table>            
          </td>
        </tr>
      </table>
      <table cellpadding="5" cellspacing="0" border="1" style="width:99%;border-collapse:collapse;border:1px solid #000000;margin:2px;">
        <colgroup>
          <col width="50%"/>
          <col width="50%"/>
        </colgroup>
        <tr>
          <td colspan="2" style="text-align:left;background-color:#C0C0C0;"><strong>Permission Overrides</strong></td>
        </tr>
        <tr>
          <td colspan="2"><strong>Permission Legend: </strong>C - Create; R - Read; U - Update; D - Delete</td>
        </tr>
        <tr valign="top">
            <xsl:variable name="permCount" select="count(Permission)"/>
            <xsl:variable name="RowsPerCol" select="ceiling(number($permCount) div 2)"/>
          <td>
            <table cellpadding="2" cellspacing="0" border="0" style="border-collapse:collapse;border:0px solid #C0C0C0;width:100%;">
              <colgroup>
                <col width="75px"/>
                <col width="*"/>
              </colgroup>
              <tr valign="middle">
                <td align="center" style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Permission</strong></td>
                <td align="center" style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Entity</strong></td>
              </tr>
              <xsl:for-each select="Permission">
                <xsl:sort select="@GroupCD"/>
                <xsl:sort select="@Name"/>
                <xsl:if test="position() &lt;= number($RowsPerCol)">
               <tr valign="top">
                <td align="center" style="border:1px solid #C0C0C0">
                      <xsl:choose>
                        <xsl:when test="@CreateFlag='0' and @ReadFlag='0' and @UpdateFlag='0' and @DeleteFlag='0' ">
                          No Access
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:if test="@CreateFlag='1'">C </xsl:if>
                          <xsl:if test="@ReadFlag='1'">R </xsl:if>
                          <xsl:if test="@UpdateFlag='1'">U </xsl:if>
                          <xsl:if test="@DeleteFlag='1'">D </xsl:if>
                        </xsl:otherwise>
                      </xsl:choose>
                </td>
                <td align="left" style="border:1px solid #C0C0C0">
                  <xsl:value-of select="@Name"/>
                </td>
              </tr> 
                </xsl:if>
              </xsl:for-each>   
            </table>
          </td>
          <td>
            <table cellpadding="2" cellspacing="0" border="0" style="border-collapse:collapse;border:0px solid #C0C0C0;width:100%;">
              <colgroup>
                <col width="75px"/>
                <col width="*"/>
              </colgroup>
              <tr valign="middle">
                <td align="center" style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Permission</strong></td>
                <td align="center" style="border:1px solid #C0C0C0;background-color:#C0C0C0;"><strong>Entity</strong></td>
              </tr>
              <xsl:for-each select="Permission">
                <xsl:sort select="@GroupCD"/>
                <xsl:sort select="@Name"/>
                <xsl:if test="position() &gt; number($RowsPerCol)">
               <tr valign="top">
                <td align="center" style="border:1px solid #C0C0C0">
                      <xsl:choose>
                        <xsl:when test="@CreateFlag='0' and @ReadFlag='0' and @UpdateFlag='0' and @DeleteFlag='0' ">
                          No Access
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:if test="@CreateFlag='1'">C </xsl:if>
                          <xsl:if test="@ReadFlag='1'">R </xsl:if>
                          <xsl:if test="@UpdateFlag='1'">U </xsl:if>
                          <xsl:if test="@DeleteFlag='1'">D </xsl:if>
                        </xsl:otherwise>
                      </xsl:choose>
                </td>
                <td align="left" style="border:1px solid #C0C0C0">
                  <xsl:value-of select="@Name"/>
                </td>
              </tr> 
                </xsl:if>
              </xsl:for-each>   
            </table>
          </td>
        </tr>
      </table>
      <table cellpadding="5" cellspacing="0" border="0" style="width:100%;border-collapse:collapse;border:0px solid #000000;">
        <colgroup>
          <col width="50%"/>
          <col width="50%"/>
        </colgroup>
        <tr>
          <td colspan="2" style="border-bottom:1px solid #C0C0C0">Supervisor Signature:</td>
        </tr>
        <tr>
          <td colspan="2" style="border-bottom:1px solid #C0C0C0">Date:</td>
        </tr>
      </table>
    </div>
  </xsl:for-each>
</xsl:template>
</xsl:stylesheet>
