<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ReactivateServiceChannel">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
  ]]>
</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>

<xsl:template match="/Root">


<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspVehicleAssignGetDetailXML,VehicleShopAssignment.xsl,5101,145   -->

<HEAD>
<TITLE>Reactivate Service Channel</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<STYLE type="text/css">
    A {color:navy; text-decoration:none;}
    A:Hover {color:navy; text-decoration:underline;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<SCRIPT language="JavaScript">

   var strUserID = "<xsl:value-of select="$UserID"/>";
  
<![CDATA[
  
   var strRet = "";
   
   function doReactivate(){
      if (txtComment.value.Trim() == ""){
         ClientWarning("APD system requires a comment. Please try again.");
         return;
      }
      var sProc;
      sProc = "uspWorkflowActivateServiceChannel";
      var aRequests = new Array();
      
      var objServiceChannels = xmlInactiveSC.selectNodes("/Root/Vehicle/InactiveServiceChannel");
      if (objServiceChannels){
         for (var i = 0; i < objServiceChannels.length; i++){
            var objChk = document.getElementById("ClaimAspectServiceChannelID_" + objServiceChannels[i].getAttribute("ClaimAspectServiceChannelID"));
            if (objChk && objChk.value == 1){
               var sRequest = "ClaimAspectServiceChannelID=" +
                              "&ServiceChannelReactivated=" +
                              "&ClaimAspectID=" + objServiceChannels[i].getAttribute("ClaimAspectID") + 
                              "&ServiceChannelCD=" + objServiceChannels[i].getAttribute("ServiceChannelCD") +
                              "&PrimaryFlag=" + objServiceChannels[i].getAttribute("PrimaryFlag") +
                              "&UserID=" + strUserID +
                              "&Notify=1" +
                              "&Comment=" + escape(txtComment.value);
                              
               
               aRequests.push( { procName : sProc,
                                 method   : "executespnp",
                                 data     : sRequest }
                             );
            }
         }
         if (aRequests.length > 0){
            var objRet = XMLSave(makeXMLSaveString(aRequests));
            if (objRet.code == 0) {
               strRet = "Refresh";
               window.close();
            }
         }
      }
   }
   
   function doCancel(){
      strRet = "";
      window.close();
   }
   
   function window_unload(){
      window.returnValue = strRet;
   }
  
]]>
    
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSubSub" onLoad="" onunload="window_unload()" tabIndex="-1" style="margin:0px;padding:10px;overflow:hidden">
   <div>
      <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
         <tr>
            <td style="font-size:12px;font-weight:bold">Reactivate Service Channel</td>
         </tr>
         <tr>
            <td>
               <img src="images/background_top.gif" style="height:2px;width:125px"/>
            </td>
         </tr>
      </table>      
   </div>
   <div style="height:100px;overflow:auto">
   <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <colgroup>
         <col width="15px"/>
         <col width="300px"/>
      </colgroup>
      <xsl:for-each select="/Root/Vehicle[count(InactiveServiceChannel) &gt; 0]">
      <tr>
         <td colspan="2" style="font-weight:bold"><xsl:value-of select="concat(@EntityName, ' ', @ClaimAspectNumber)"/></td>
      </tr>
      <xsl:for-each select="InactiveServiceChannel">
      <tr>
         <td></td>
         <td>
            <IE:APDCheckBox CCDisabled="false" required="false" CCTabIndex="1" canDirty="false" >
               <xsl:attribute name="id"><xsl:value-of select="concat('ClaimAspectServiceChannelID_', @ClaimAspectServiceChannelID)"/></xsl:attribute>
               <xsl:attribute name="name"><xsl:value-of select="concat('ClaimAspectServiceChannelID_', @ClaimAspectServiceChannelID)"/></xsl:attribute>
               <xsl:attribute name="caption"><xsl:value-of select="@ServiceChannelName"/></xsl:attribute>
            </IE:APDCheckBox>
         </td>
      </tr>
      </xsl:for-each>
      </xsl:for-each>
   </table>
   </div>
   
   <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <tr>
         <td style="font-size:12px;font-weight:bold">Comments:</td>
      </tr>
      <tr>
         <td>
            <IE:APDTextArea id="txtComment" name="txtComment" maxLength="1000" width="420" height="50" required="true" canDirty="false" CCDisabled="false" CCTabIndex="50" onChange="" />
         </td>
      </tr>
      <tr>
         <td>
            <IE:APDButton id="btnReactivate" name="btnReactivate" value="Reactivate" width="75" CCDisabled="false" CCTabIndex="51" onButtonClick="doReactivate()"/>
            <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="50" CCDisabled="false" CCTabIndex="51" onButtonClick="doCancel()"/>
         </td>
      </tr>
   </table>      
   
   <xml id="xmlInactiveSC">
      <Root>
         <xsl:copy-of select="/Root/Vehicle"/>
      </Root>
   </xml>
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
