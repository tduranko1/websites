<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDReinspection">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:param name="LocID"/>

<xsl:template match="/Root">

<xsl:variable name="ShopLocationID" select="/Root/@ShopLocationID"/>
<xsl:variable name="BeginDate" select="/Root/@BeginDate"/>
<xsl:variable name="EndDate" select="/Root/@EndDate"/>
<xsl:variable name="AssignmentCode" select="/Root/@AssignmentCode"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID                  SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectionGetDetailXML,PMDReinspection.xsl,2550, '02/02/2002', '04/30/2004', 'B'   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

parent.gsShopLocationID = '<xsl:value-of select="$ShopLocationID"/>';
parent.gsBeginDate = '<xsl:value-of select="user:UTCConvertDate(string($BeginDate))"/>';
parent.gsEndDate = '<xsl:value-of select="user:UTCConvertDate(string($EndDate))"/>';
parent.gsAssignmentCode = '<xsl:value-of select="$AssignmentCode"/>';

<![CDATA[

//init the table selections, must be last
function initPage()
{
  
  txtBeginDate.value = parent.gsBeginDate;
  txtEndDate.value = parent.gsEndDate;
  parent.resizeScrollTable(document.getElementById("CNScrollTable"));
  parent.document.all.tab13.className="tabactive1";
}

function GridSelect(oRow)
{
  parent.gsLynxID = oRow.cells[0].innerText;
  parent.gsClaimAspectID = oRow.cells[0].getAttribute("ClaimAspectID");
  parent.gsAssignmentID = oRow.cells[0].getAttribute("AssignmentID");
  parent.gsReferrer = "PMDReinspection.asp";
  window.navigate("PMDEstimateList.asp?AssignmentID=" + parent.gsAssignmentID);
}

]]>

</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="initPage();" bgcolor="#FFFAEB" style="margin-right:none">
  
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 5px; left: 6px;">
    Shop<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Assignment<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;List</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:21px; width:965px">
  
  <xsl:call-template name="DateSearch">   <!-- search bar -->
    <xsl:with-param name="AssignmentCode" select="$AssignmentCode"/>
  </xsl:call-template>

    <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

    <DIV id="CNScrollTable" style="width:100%"> 
	  <TABLE id="tblHead" unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
      <TBODY>
        <TR unselectable="on" class="QueueHeader">
        	<TD unselectable="on" class="TableSortHeader" width="60" sIndex="0" rowspan="2" type="Number">LYNX ID</TD>
        	<TD unselectable="on" class="TableSortHeader" width="100%" sIndex="99" rowspan="2" type="CaseInsensitiveString" style="cursor:default;"> Vehicle YMM </TD>
        	<TD unselectable="on" class="TableSortHeader" width="130" sIndex="99" rowspan="2" type="CaseInsensitiveString" style="cursor:default;"> Vehicle Owner </TD>
        	<TD unselectable="on" class="TableSortHeader" width="146" sIndex="99" rowspan="2" style="cursor:default;"> Insurance </TD>
          <TD unselectable="on" class="TableSortHeader" width="146" sIndex="99" colspan="2" style="cursor:default;"> Estimate Received </TD>
        	<TD unselectable="on" class="TableSortHeader" width="219" sIndex="99" colspan="3" style="cursor:default;"> Reinspection </TD>
        	<TD unselectable="on" class="TableSortHeader" width="0" sIndex="99" rowspan="2" style="cursor:default;"></TD> <!-- scroll bar spacer -->
	      </TR>
  		  <TR unselectable="on" class="QueueHeader">
    			<TD height="20" unselectable="on" class="TableSortHeader" width="70" sIndex="99" nowrap="nowrap" style="cursor:default;"> Original </TD>
    			<TD unselectable="on" class="TableSortHeader" width="70" sIndex="99" nowrap="nowrap" style="cursor:default;"> Suppl. </TD>
    			<TD unselectable="on" class="TableSortHeader" width="70" sIndex="1" nowrap="nowrap"> Date </TD>
    			<TD unselectable="on" class="TableSortHeader" width="70" sIndex="99" nowrap="nowrap" style="cursor:default;"> Type </TD>
    			<TD unselectable="on" class="TableSortHeader" width="70" sIndex="99" nowrap="nowrap" style="cursor:default;"> % Sat </TD>
  		  </TR>
        </TBODY>
      </TABLE>
      
	  
      <DIV unselectable="on" class="autoflowTable" style="width:100%; height:260px;">
        <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed">
          <TBODY bgColor1="ffffff" bgColor2="fff7e5">
            <xsl:for-each select="Shop/Assignment" ><xsl:call-template name="ShopAssignmentList"/></xsl:for-each>
          </TBODY>
        </TABLE>
      </DIV>	
	  </DIV>  
  </DIV>

  <DIV unselectable="on" style="position:absolute; left:0px; top:310px">
    <xsl:for-each select="Shop" ><xsl:call-template name="ShopInfo"/></xsl:for-each>
		<xsl:for-each select="Stats" ><xsl:call-template name="ReinspectionAnalysis"/></xsl:for-each>
  </DIV>   
  
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the list of Asssignments -->
  <xsl:template name="ShopAssignmentList">
    
    <TR unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this)" onclick="GridSelect(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <xsl:attribute name="title"><xsl:if test="@CancellationDate != ''">This Assignment has been cancelled but has Reinspection(s).  </xsl:if>Click a Vehicle to view its Estimate List.</xsl:attribute>
      <TD unselectable="on" width="58" class="GridTypeTD">
        <xsl:attribute name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute>  
        <xsl:attribute name="AssignmentID"><xsl:value-of select="@AssignmentID"/></xsl:attribute>      
        <xsl:choose>
          <xsl:when test="/Root/@ViewCRD=1">
            <a>
              <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
              <xsl:attribute name="href">javascript:NavToClaim(<xsl:value-of select="@LynxID"/>)</xsl:attribute>
              <xsl:value-of select="@LynxID"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
            <xsl:value-of select="@LynxID"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="100%" class="GridTypeTD">
        <xsl:attribute name="style">text-align:left;<xsl:if test="@CancellationDate != ''">color:red;</xsl:if></xsl:attribute>
	      <xsl:value-of select="@VehicleYear"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		    <xsl:value-of select="@VehicleMake"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		    <xsl:value-of select="@VehicleModel"/>
      </TD>
      <TD unselectable="on" width="128" class="GridTypeTD" style="text-align:left;">
        <xsl:choose>
          <xsl:when test="@OwnerBusinessName != ''" >
            <xsl:value-of select="@OwnerBusinessName"/>
          </xsl:when>
          <xsl:when test="@OwnerLastName != ''" >
            <xsl:value-of select="@OwnerLastName"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			      <xsl:value-of select="@OwnerFirstName"/>
          </xsl:when>
          <xsl:otherwise>
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="144" class="GridTypeTD">
        <xsl:value-of select="@InsuranceName"/>
      </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@OriginalEstimateDate=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="user:UTCConvertDateByNodeType(.,'OriginalEstimateDate','A')"/></xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@FirstSupplementDate=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="user:UTCConvertDateByNodeType(.,'FirstSupplementDate','A')"/></xsl:otherwise>
        </xsl:choose>
	    </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@ReinspectionDate=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="user:UTCConvertDateByNodeType(.,'ReinspectionDate','A')"/></xsl:otherwise>
        </xsl:choose>
  	  </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@ReinspectionType=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="@ReinspectionType"/></xsl:otherwise>
        </xsl:choose>  
  	  </TD>
      <TD unselectable="on" width="71" class="GridTypeTD">
  	    <xsl:choose>
          <xsl:when test="@ReinspectionPctSat=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="@ReinspectionPctSat"/></xsl:otherwise>
        </xsl:choose>  
  	  </TD> 
    </TR> 
  </xsl:template>

  
 <xsl:template name="ReinspectionAnalysis">

    <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top:70px; left:443px;">Reinspection Anlysis</DIV>
    <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:440px; top:86px;">
      <TABLE unselectable="on" border="0" cellspacing="1" cellpadding="1" style="text-align:center; table-layout:fixed;">
        <TR unselectable="on">
        <colgroup>
	        <col width="150" align="left"/>
	        <col width="70"/>
	        <col width="70"/>
	        <col width="70"/>
	        <col width="70"/>
	      </colgroup>
          <TD unselectable="on" class="smallText">
            <strong>(Last <xsl:value-of select="/Root/@StatRange"/> Days)</strong>
          </TD>
          <TD unselectable="on" colspan="2" class="TableHeader2"><SPAN class="boldtext">Drivable</SPAN></TD>
          <TD unselectable="on" colspan="2" class="TableHeader2"><SPAN class="boldtext">Non-Drivable</SPAN></TD>
        </TR>
        <TR unselectable="on">
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on" width="50" class="TableHeader2">#<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;ReIs</TD>
          <TD unselectable="on" width="60" class="TableHeader2">% Sat</TD>
          <TD unselectable="on" width="50" class="TableHeader2">#<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;ReIs</TD>
          <TD unselectable="on" width="60" class="TableHeader2">% Sat</TD>
        </TR>
        <TR unselectable="on" bgColor="#FDF5E6">
          <TD unselectable="on" class="boldtext" nowrap="nowrap">Before Repairs</TD>
          <TD unselectable="on">
            <xsl:choose>
			  	    <xsl:when test="@BeforeDrivableCount = '0'">
				        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				      </xsl:when>
				      <xsl:otherwise>
					      <xsl:value-of select="@BeforeDrivableCount"/>
			        </xsl:otherwise>
			  </xsl:choose>
			</TD>
            <TD unselectable="on">
			  <xsl:choose>
			  	<xsl:when test="@BeforeDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@BeforeDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
			</TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@BeforeNonDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@BeforeNonDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@BeforeNonDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@BeforeNonDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
          </TR>
          <TR unselectable="on" bgColor="#FFFFEE">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">During Repairs</TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@DuringDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@DuringDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@DuringDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@DuringDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@DuringNonDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@DuringNonDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@DuringNonDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@DuringNonDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
          </TR>
          <TR unselectable="on" bgColor="#FDF5E6">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">After Repairs/Total Loss</TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@AfterDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@AfterDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@AfterDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@AfterDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@AfterNonDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@AfterNonDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@AfterNonDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@AfterNonDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
          </TR>
		  <TR unselectable="on" bgColor="#FFFFEE">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">Supplement</TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@SupplDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@SupplDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@SupplDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@SupplDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@SupplNonDrivableCount = '0'">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@SupplNonDrivableCount"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
            <TD unselectable="on">
              <xsl:choose>
			  	<xsl:when test="@SupplNonDrivableSat = ''">
				  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@SupplNonDrivableSat"/>
			    </xsl:otherwise>
			  </xsl:choose>
            </TD>
          </TR>
        </TABLE>
      </DIV>

  </xsl:template>


</xsl:stylesheet>

