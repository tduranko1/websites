<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="AuditEstimateDetails">

<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="RoleID"/>

<xsl:template match="/Root">

<HTML>
<HEAD>
	<TITLE>
		Audit Estimate Values
	</TITLE>


<LINK rel="stylesheet" href="/css/apdmain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/coolselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/coolselect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<script type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/formats.js"></script>



	<script language="javascript">
		var gClaimAspectID = '<xsl:value-of select="@ClaimAspectID"/>';
		var gClaimAspectServiceChannelID = '<xsl:value-of select="@ClaimAspectServiceChannelID"/>';
		var gDisposition = '<xsl:value-of select="@Disposition"/>';
		var gRepairTotal = '<xsl:value-of select="Values/@RepairTotal"/>';

		// 09Jul2012 - TVD - New fields
		var gRoleID = "<xsl:value-of select="$RoleID"/>";
		var gEarlyBillingFlag = '<xsl:value-of select="/Root/@EarlyBillFlag"/>';
		var gOriginalGrossAmount = '<xsl:value-of select="/Root/Values/@OriginalGrossAmount"/>';
		
		var gAuditedOriginalGrossAmount = '<xsl:value-of select="/Root/Values/@AuditedOriginalGrossAmount"/>';
		var gAdditionalLineItemValues = '<xsl:value-of select="/Root/Values/@AdditionalLineItemValues"/>';
		var gMissingLineItemValues = '<xsl:value-of select="/Root/Values/@MissingLineItemValues"/>';
		var gLineItemValueCorrection = '<xsl:value-of select="/Root/Values/@LineItemValueCorrection"/>';
		var gsUserID = '<xsl:value-of select="@UserId"/>';
		var gDirtyFlag = false;

		// 09Jul2012 - TVD - Original Gross Amount Added
		var dCalcSavings = 0;
		var dCalcSavingPercent = 0;
		var iRC = 0 ;
  
		<![CDATA[

		function PageInit() {
			var dCalcAuditAmt;

			//document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(parseFloat(gRepairTotal));

			dCalcAuditAmt = parseFloat(gRepairTotal) + parseFloat(gAdditionalLineItemValues) + parseFloat(gMissingLineItemValues) + parseFloat(gLineItemValueCorrection);

			document.getElementById("txtAdditionalLineItemValues").value = formatCurrency(gAdditionalLineItemValues);
			document.getElementById("txtMissingLineItemValues").value = formatCurrency(gMissingLineItemValues);
			document.getElementById("txtLineItemValueCorrection").value = formatCurrency(gLineItemValueCorrection);

			document.getElementById("txtRepairTotal").value = formatCurrency(parseFloat(gRepairTotal));

			// 09Jul2012 - TVD - Original Gross Amount Added
			document.getElementById("txtOriginalGrossAmount").value = formatCurrency(parseFloat(gOriginalGrossAmount));
			//iRC = calulateSavings();

			if ( isNaN(dCalcAuditAmt) ) {
				document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(parseFloat(gRepairTotal));
			} else {
				document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(dCalcAuditAmt);
			}

			// enabled for dispotion of RC, CO, or blank
			if (gDisposition == 'RC' || gDisposition == 'CO' || gDisposition == "") {
			
				document.getElementById("txtAdditionalLineItemValues").disabled = false;
				document.getElementById("txtMissingLineItemValues").disabled = false;
				document.getElementById("txtLineItemValueCorrection").disabled = false;
				document.getElementById("btnSave").disabled = false;
				
			} else {

				document.getElementById("txtAdditionalLineItemValues").disabled = true;
				document.getElementById("txtMissingLineItemValues").disabled = true;
				document.getElementById("txtLineItemValueCorrection").disabled = true;
				document.getElementById("btnSave").disabled = true;
			}
			
			// 10Jul2012 - TVD - Test if already saved, if so disable the Save button.
			if (gRoleID == 7 || gRoleID == 9 || gRoleID == 13 ) {
				document.getElementById('btnSave').disabled = false;
				document.getElementById('btnSave').style.visibility='visible';
				document.getElementById('txtAdditionalLineItemValues').disabled = false;
				document.getElementById('txtMissingLineItemValues').disabled = false;
				document.getElementById('txtLineItemValueCorrection').disabled = false;
				if (gAdditionalLineItemValues == '') {
				}
				else {
					document.getElementById('btnSave').innerHTML = "ReSave";
				}
			}
			else {
				if (gAdditionalLineItemValues == '') {
					document.getElementById('btnSave').disabled = false;
					document.getElementById('btnSave').style.visibility='visible';
					document.getElementById('txtAdditionalLineItemValues').disabled = false;
					document.getElementById('txtMissingLineItemValues').disabled = false;
					document.getElementById('txtLineItemValueCorrection').disabled = false;
				} else {
					document.getElementById('btnSave').disabled = true;
					document.getElementById('lblMsg').innerHTML = "Estimate has already been saved and can not be saved again...";
					document.getElementById('lblMsg').style.color = "#0000CC";
					document.getElementById('btnSave').style.visibility='hidden';
					
					document.getElementById('txtAdditionalLineItemValues').disabled = true;
					document.getElementById('txtMissingLineItemValues').disabled = true;
					document.getElementById('txtLineItemValueCorrection').disabled = true;
				}
			}
			// 26Oct2012 - TVD - ReCalc
			iRC = calulateSavings();
		}

		// 09Jul2012 - TVD - Savings/Percentage Added
		function calulateSavings() {
			var dOriginalGrossAmount = Number(document.getElementById("txtOriginalGrossAmount").value);
			var dAuditedOriginalGrossAmount = Number(document.getElementById("txtAuditedOriginalGrossAmount").value);
			var dRepairTotal = Number(document.getElementById("txtRepairTotal").value);

			dCalcSavings =  dOriginalGrossAmount - dAuditedOriginalGrossAmount;
			dCalcSavingPercent = ((dCalcSavings/dOriginalGrossAmount)*100).toFixed(2)

			document.getElementById("txtSavings").value = formatCurrency(parseFloat(dCalcSavings));
			document.getElementById("txtSavingPercent").value =  dCalcSavingPercent;
			return 0;
		}
		
		
		function verifyNegative() {

			gDirtyFlag = true;
		
			var dValue = document.getElementById("txtAdditionalLineItemValues").value;
	
			if (isNaN(dValue)) {
				alert("Additional Line Item Value must be a negative number.");
				return;
			} else if (dValue == "") {
				dValue = 0;
			} else {
				dValue = parseFloat(dValue);

				if (dValue > 0) {
					dValue =- dValue;
				}
			}
			
			document.getElementById("txtAdditionalLineItemValues").value = formatCurrency(dValue);

			document.getElementById("txtAuditedOriginalGrossAmount").value = parseFloat(gRepairTotal) + parseFloat(dValue) + parseFloat(document.getElementById("txtMissingLineItemValues").value) + parseFloat(document.getElementById("txtLineItemValueCorrection").value);
			document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(document.getElementById("txtAuditedOriginalGrossAmount").value);

			// 10Jul2012 - TVD - Recalulate
			iRC = calulateSavings();
		}

		function verifyPositive() {
		
			gDirtyFlag = true;		
		
			var dValue = document.getElementById("txtMissingLineItemValues").value;
	
			if (isNaN(dValue)) {
				alert("Missing Line Item Value must be a positive number.");
				return;
			} else if (dValue == "") {
				dValue = 0;
			} else {
				dValue = parseFloat(dValue);

				if (dValue < 0) {
					dValue = Math.abs(dValue);
				}
			}
			
			document.getElementById("txtMissingLineItemValues").value = formatCurrency(dValue);
			
			document.getElementById("txtAuditedOriginalGrossAmount").value = parseFloat(gRepairTotal) + parseFloat(dValue) + parseFloat(document.getElementById("txtAdditionalLineItemValues").value) + parseFloat(document.getElementById("txtLineItemValueCorrection").value);
			document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(document.getElementById("txtAuditedOriginalGrossAmount").value);

			// 10Jul2012 - TVD - Recalulate
			iRC = calulateSavings();
		}
		
		function verifyNumber() {

			gDirtyFlag = true;		

			var dValue = document.getElementById("txtLineItemValueCorrection").value;
	
			if (isNaN(dValue)) {
				alert("Line Item Value Correction must be a positive or negative number.");
				return;
			} else if (dValue == "") {
				dValue = 0;
			}
			
			document.getElementById("txtLineItemValueCorrection").value = formatCurrency(dValue);
			
			document.getElementById("txtAuditedOriginalGrossAmount").value = parseFloat(gRepairTotal) + parseFloat(dValue);
			
			document.getElementById("txtAuditedOriginalGrossAmount").value = parseFloat(gRepairTotal) + parseFloat(dValue) + parseFloat(document.getElementById("txtAdditionalLineItemValues").value) + parseFloat(document.getElementById("txtMissingLineItemValues").value);
			document.getElementById("txtAuditedOriginalGrossAmount").value = formatCurrency(document.getElementById("txtAuditedOriginalGrossAmount").value);

			iRC = calulateSavings();
			}

		
		function formatCurrency(num) {
			num = isNaN(num) || num === '' || num === null ? 0.00 : num;
			return parseFloat(num).toFixed(2);
		}


		function SaveAll() {

			var dAuditOriginalGrossAmount;
			var dAddLineItems;
			var dMissingLineItems;
			var dLineItemValue;
			var sRequest;
			var sProc;

			sProc = "uspAuditEstimateValuesUpdDetail";

			dAuditOriginalGrossAmount = document.getElementById("txtAuditedOriginalGrossAmount").value;
			dAddLineItems = document.getElementById("txtAdditionalLineItemValues").value;
			dMissingLineItems = document.getElementById("txtMissingLineItemValues").value;
			dLineItemValue = document.getElementById("txtLineItemValueCorrection").value;
			
			sRequest = "ClaimAspectID=" + gClaimAspectID + "&";
			sRequest += "AuditedOriginalGrossAmount=" + dAuditOriginalGrossAmount + "&";			
			sRequest += "AdditionalLineItemValues=" + dAddLineItems + "&";
			sRequest += "MissingLineItemValues=" + dMissingLineItems + "&";
			sRequest += "LineItemValueCorrection=" + dLineItemValue;

			//sRequest += "UserID=" + gsUserID + "&";

			var aRequests = new Array();
			aRequests.push( { procName : sProc,
                            method   : "executespnp",
                            data     : sRequest }
                        );
			var objRet = XMLSave(makeXMLSaveString(aRequests));
          
			gDirtyFlag = false;
			
			window.close();
		}
		

		function windowClose() {
		
			if (gDisposition == 'RC' || gDisposition == 'CO' || gDisposition == "") {		

				// 10Jul2012 - TVD - Check to see if save button is active
				if (document.getElementById('btnSave').disabled = false) {
					document.getElementById('btnSave').focus();
				}
				
				if (gDirtyFlag) {
	
					var sSave = YesNoMessage("Save Before Close", "The Audit Estimate Values have changed.  Do you want to save?");
	
					if (sSave == "Yes") {
						SaveAll();
						gDirtyFlag = false;
						window.close();
						
					} else if (sSave == "No") {
						window.close();
						gDirtyFlag = false;
						
					} else {
						event.ReturnValue=false;
					}
				} else {		
					window.close();
				}

			} else {
				window.close();
			}
		
			
		}


		]]>
	</script>
  
</HEAD>

<BODY class="bodyAPDSub" style="background:transparent; border:0px; overflow:hidden;" tabIndex="-1" onLoad="PageInit()" onbeforeunload="windowClose()">

	<!-- Message -->
	<DIV id="ViewMessage" style="position:absolute; left:15px; top:6px;">
		<label id="lblMsg" name="lblMsg"></label>
	</DIV>

	<!-- Save Button -->
	<DIV id="ViewSave" style="position:absolute; left:370px; top:2px;">
		<button type="button" name="btnSave" id="btnSave" onClick="SaveAll()"  style="width:55px; height:20px; font: 12px Arial">Save</button>
	</DIV>
	
	<!-- Close Icon -->
	<DIV id="Close" style="position:absolute; left:430px; top:4px; vetical-align:text-top; cursor:hand;">
		<IMG src="/images/close.gif" width="18" height="18" border="0" align="absmiddle" onClick="windowClose()" title="Close (Return to Summary)" style="cursor:hand;"/>
	</DIV>

	<IMG src="/images/spacer.gif" width="1" height="10" border="0" />
  
	<TABLE border="0" cellspacing="1" cellpadding="3" style="border-collapse:collapse" width="430px">
	
		<TR>
			<TD colspan="2" style="text-align:center;background-color:silver;font-weight:bold;">Audit Estimate Values</TD>
		</TR>

		<!-- 09Jul2012 - TVD - Added new values for only certain users -->
		<TR>
			<TD width="350px">Original Gross Amount:</TD>
			<TD width="80px">
				<input type="text" id="txtOriginalGrossAmount" name="txtOriginalGrossAmount" style="width:80px; text-align: right" disabled="true"></input>
			</TD>
		</TR>
		
		<TR>
			<TD width="350px">Total Cost of Repair:</TD>
			<TD width="80px">
				<input type="text" id="txtRepairTotal" name="txtRepairTotal" style="width:80px; text-align: right" disabled="true"></input>
				<!--<IE:APDInput id="txtRepairTotal" name="txtRepairTotal" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" disabled="true" />-->
			</TD>
		</TR>
		
		<TR>
			<TD width="350px">Audited Original Gross Amount:</TD>
			<TD width="80px">
				<input type="text" id="txtAuditedOriginalGrossAmount" name="txtAuditedOriginalGrossAmount" style="width:80px; text-align: right" disabled="true"></input>
				<!--<IE:APDInput id="txtAuditedOriginalGrossAmount" name="txtAuditedOriginalGrossAmount" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" disabled="true" />-->
			</TD>
		</TR>
		<TR>
			<TD>Additional Line Item Values (Value will be Deducted):</TD>
			<TD>
				<input type="text" id="txtAdditionalLineItemValues" name="txtAdditionalLineItemValues" style="width:80px; text-align: right" onChange="verifyNegative()"></input>
				<!--<IE:APDInput id="txtAdditionalLineItemValues" name="txtAdditionalLineItemValues" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" onChange="verifyNegative()" />-->
			</TD>
		</TR>
		<TR>
			<TD>Missing Line Item Values (Value will be Added):</TD>
			<TD>
				<input type="text" id="txtMissingLineItemValues" name="txtMissingLineItemValues" style="width:80px; text-align: right" onChange="verifyPositive()"></input>
				<!--<IE:APDInput id="txtMissingLineItemValues" name="txtMissingLineItemValues" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" onChange="verifyPositive()" />-->
			</TD>
		</TR>
		<TR>
			<TD>Line Item Value Correction (Enter Net +/- Value of Adjustments):</TD>
			<TD>
				<input type="text" id="txtLineItemValueCorrection" name="txtLineItemValueCorrection" style="width:80px; text-align: right" onChange="verifyNumber()"></input>
				<!--<IE:APDInput id="txtLineItemValueCorrection" name="txtLineItemValueCorrection" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" onChange="verifyNumber()" />-->
			</TD>
		</TR>

		<!-- 09Jul2012 - TVD - Added new values for only certain users -->
		<TR>
			<TD width="350px" valign="Top">Savings:</TD>
			<TD width="80px">
				<input type="text" id="txtSavings" name="txtSavings" style="width:80px; text-align: right" disabled="true"></input>
				<!--<IE:APDInput id="txtRepairTotal" name="txtRepairTotal" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" disabled="true" />-->
			</TD>
		</TR>
		<TR>
			<TD width="350px" valign="Top">Percent Savings:</TD>
			<TD width="80px">
				<input type="text" id="txtSavingPercent" name="txtSavingPercent" style="width:80px; text-align: right" disabled="true"></input>
				<!--<IE:APDInput id="txtRepairTotal" name="txtRepairTotal" value="" maxLength="10" required="true" CCTabIndex="1" canDirty="true" disabled="true" />-->
			</TD>
		</TR>

	</TABLE>

</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>
