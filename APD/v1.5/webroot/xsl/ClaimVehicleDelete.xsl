<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicleDelete">

<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="VehContext"/>

<xsl:template match="/">

<xsl:variable name="LynxID" select="/Root/@LynxID" />

<HTML><!-- APDXSL:/APD_Development/APDSelect,'DevXSL','ClaimVehicleList',1, 5535  -->
<HEAD>
<TITLE>Vehicle Info</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<SCRIPT language="JavaScript">

var gsLynxID = "<xsl:value-of select="$LynxID"/>";
var gsVehCount = "<xsl:value-of select="count(/Root/Vehicle)"/>";

var gsCRUD = '<xsl:value-of select="$VehicleCRUD"/>';

<![CDATA[


function PageInit()
{
	if (gsCRUD.indexOf("D") == -1)
	{
    ClaimWarning("You do not have rights to remove a Vehicle.  See your supervisor.");
    document.parentWindow.frameElement.src="blank.asp";
	}

	top.HideMainTabs();
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" onLoad="PageInit();">

<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">

  <DIV id="DescriptionInfoTab" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 6px; left: 6px;">
    Remove Vehicle - List
	</DIV>
  <DIV id="ClaimVehicleInsert" class="SmallGrouping" name="ClaimVehicleInsert" style="position:absolute; left:3px; top:22px; z-index:1;" onfocusin="DivOnFocusIn(this)" >



	</DIV>

</DIV>


<!-- Start Tabs -->
	<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">
  	<xsl:if test="boolean(/Root/Vehicle)">

  		<DIV unselectable="on" id="tabs1" class="apdtabs">
    		<xsl:for-each select="/Root/Vehicle"	>
    			<xsl:sort data-type="number" select="@VehicleNumber" order="ascending" />
    			<xsl:value-of disable-output-escaping="yes" select="js:MainTabs(., position(), string(@VehicleNumber))"/>
    		</xsl:for-each>
  		</DIV>	<!-- Start Veh1 Info -->

  		<xsl:for-each select="/Root/Vehicle"	>
  			<xsl:sort data-type="number" select="@VehicleNumber" order="ascending" />
  			<xsl:value-of disable-output-escaping="yes" select="js:ContentTabs(/Root/@LynxID, position(), string(@VehicleNumber))"/>
  		</xsl:for-each>

  	</xsl:if>
	</DIV>
<!-- End Tabs -->

</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>
