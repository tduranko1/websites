<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes" method="text" indent="yes"/>
  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
		function crlf(){
			return String.fromCharCode(13, 10);
		}
    ]]>
  </msxsl:script>
  <xsl:template match="/Root">
    <xsl:param name="city" select="@city"/>
    <xsl:param name="state" select="@state"/>
    <xsl:param name="crlf" select="user:crlf()"/>
    <xsl:choose>
      <xsl:when test="$city != '' and $state != ''">
        <xsl:for-each select="Detail[substring(@city, 1, string-length($city)) = $city and @state=$state]">
          <xsl:value-of select="@city"/>|<xsl:value-of select="@state"/>|<xsl:value-of select="@county"/>
          <xsl:value-of select="$crlf"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="$state != ''">
        <xsl:for-each select="Detail[@state=$state]">
          <xsl:sort data-type="text" select="@county"/>
          <xsl:value-of select="concat('|',@state,'|',@county)"/>
          <xsl:value-of select="$crlf"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="Detail[substring(@city, 1, string-length($city)) = $city]">
          <xsl:value-of select="@city"/>|<xsl:value-of select="@state"/>|<xsl:value-of select="@county"/>
          <xsl:value-of select="$crlf"/>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>